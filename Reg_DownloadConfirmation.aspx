﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReLoginMaster.master" AutoEventWireup="true" CodeFile="Reg_DownloadConfirmation.aspx.cs" Inherits="Reg_DownloadConfirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="form-group" runat="server" id="divConfirmationMessage" visible="false">
        <div class="form-group row">
            <div class="col-md-12">
                <asp:Label ID="lblMessage" runat="server" Text="There is no confirmation letter to show." Font-Bold="true" ForeColor="Red" Visible="false"></asp:Label>
            </div>
        </div>
    </div>

    <div class="form-group" runat="server" id="divCompleteInvoices" visible="false">
        <div class="form-group row">
            <div class="col-md-12">
                When the purchase is completed, a receipt is sent to the email address on your IDEM 2020 Account.<br />
                A copy of your receipt can be downloaded by clicking <strong>'Select Receipt'</strong> from the box below, followed by the <strong>'Download Receipt'</strong> button.
                <br />
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                <asp:Label ID="lblCompleteInvoices" runat="server" CssClass="form-control-label" Text="Registration No. : " Font-Bold="true"></asp:Label>
            </div>
            <div class="col-md-5">
                <asp:DropDownList ID="ddlCompleteInvoices" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCompleteInvoices_SelectedIndexChanged"
                    CssClass="form-control"></asp:DropDownList>
            </div>
        </div>
    </div>

    <div class="form-group" runat="server" id="divIncompleteLink" visible="false">
        <div class="form-group row">
            <div class="col-md-12">
                Please complete your registration 
                <asp:HyperLink ID="hpLink" runat="server" Text="here." ></asp:HyperLink>
            </div>
        </div>
    </div>

</asp:Content>

