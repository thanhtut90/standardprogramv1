﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FoodJapanShowTicketCount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnShow_Click(object sender, EventArgs e)
    {
        Functionality fn = new Functionality();
        try
        {
            string sql = "Select IsNull(Sum(IsNull(od.OrderQty, 0)),0) CollectedTicketTotalNumber From tb_Onsite_PrintLog as prt "
                        + " Inner Join GetRegIndivAllByShowID('KKS355') r On prt.PRegno=r.Regno "
                        + " Inner Join tb_OrderDetials as od On prt.PRegno=od.RegId "
                        + " Where r.Invoice_status=1 And prt.ShowID='KKS355' And r.reg_UrlFlowID='F378'";
            lblTotalNumber.Text = fn.GetDataByCommand(sql, "CollectedTicketTotalNumber");
        }
        catch(Exception ex)
        { }
    }
}