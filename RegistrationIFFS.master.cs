﻿using Corpit.BackendMaster;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistrationIFFS : System.Web.UI.MasterPage
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            ShowController shwCtrl = new ShowController(fn);
            if (shwCtrl.checkValidShow(showid))
            {
                SiteSettings sSetting = new SiteSettings(fn, showid);
                sSetting.LoadBaseSiteProperties(showid);
                FlowControler flwObj = new FlowControler(fn, urlQuery);
                FlowMaster fMaster = flwObj.GetFlowMasterConfig(flwObj.FlowID);

                if (Request.Params["t"] != null)
                {
                    string admintype = cFun.DecryptValue(Request.QueryString["t"].ToString());
                    if (admintype == "iswebsettings")
                    {
                    }
                    else
                    {
                        if (sSetting.isRegisterClosed())
                        {
                            if (fMaster.isOnsite != 1)
                                Response.Redirect("RegClosed.aspx?SHW=" + cFun.EncryptValue(showid));
                        }
                    }
                }
                else
                {
                    if (sSetting.isRegisterClosed())
                    {
                        if (fMaster.isOnsite != 1)
                            Response.Redirect("RegClosed.aspx?SHW=" + cFun.EncryptValue(showid));
                    }
                }

                if (!IsPostBack)
                {
                    InitSiteSettings(showid);
                    LoadPageSetting();
                    stepsettings(urlQuery);
                }
                if (!string.IsNullOrEmpty(sSetting.GoogleAnalyticCode))
                    txtGoogleAnalyticCode.Text = sSetting.GoogleAnalyticCode;
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        SiteBanner.ImageUrl = sSetting.SiteBanner;
        Global.PageTitle = sSetting.SiteTitle;
        Global.Copyright = sSetting.SiteFooter;
        Global.PageBanner = sSetting.SiteBanner;

        if (!string.IsNullOrEmpty(sSetting.SiteCssBandlePath))
            BundleRefence.Path = "~/Content/" + sSetting.SiteCssBandlePath + "/";
    }
    private void LoadPageSetting()
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        SetPageSettting(flwObj.CurrIndexTiTle);
    }

    public void SetPageSettting(string title)
    {
        SiteHeader.Text = title;
    }

    #endregion

    #region step progress bar
    private void stepsettings(FlowURLQuery urlQuery)
    {
        try
        {
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
            FlowControler flwControl = new FlowControler(fn);
            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
            setStep(cFun.ParseInt(currentStep), flwMasterConfig.FlowType, flowid);
        }
        catch(Exception ex)
        { }
    }
    public void setStep(int pageindex, string flowType, string flowid)
    {
        if (flowType == SiteFlowType.FLOW_INDIVIDUAL)
        {
            FlowControler Flw = new FlowControler(fn);
            bool confExist = Flw.checkPageExist(flowid, SiteDefaultValue.constantConfName);
            if (!confExist)
            {
                if (pageindex == 1)
                {
                    divStep1.Attributes.Remove("class");
                    divStep1.Attributes.Add("class", "col-md-3 activestep");
                    divStep2.Attributes.Remove("class");
                    divStep2.Attributes.Add("class", "col-md-3");
                    divStep3.Attributes.Remove("class");
                    divStep3.Attributes.Add("class", "col-md-3");
                    divStep4.Attributes.Remove("class");
                    divStep4.Attributes.Add("class", "col-md-3");

                    //step1.Attributes.Remove("class");
                    //step1.Attributes.Add("class", "row setup-content step activeStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step hiddenStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step hiddenStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step hiddenStepInfo");

                    divIndividual.Visible = true;
                }
                else if (pageindex == 2)
                {
                    divStep1.Attributes.Remove("class");
                    divStep1.Attributes.Add("class", "col-md-3");
                    divStep2.Attributes.Remove("class");
                    divStep2.Attributes.Add("class", "col-md-3 activestep");
                    divStep3.Attributes.Remove("class");
                    divStep3.Attributes.Add("class", "col-md-3");
                    divStep4.Attributes.Remove("class");
                    divStep4.Attributes.Add("class", "col-md-3");

                    //step1.Attributes.Remove("class");
                    //step1.Attributes.Add("class", "row setup-content step hiddenStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step activeStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step hiddenStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step hiddenStepInfo");

                    divIndividual.Visible = true;
                }
                else if (pageindex == 3)
                {
                    divStep1.Attributes.Remove("class");
                    divStep1.Attributes.Add("class", "col-md-3");
                    divStep2.Attributes.Remove("class");
                    divStep2.Attributes.Add("class", "col-md-3");
                    divStep3.Attributes.Remove("class");
                    divStep3.Attributes.Add("class", "col-md-3 activestep");
                    divStep4.Attributes.Remove("class");
                    divStep4.Attributes.Add("class", "col-md-3");

                    //step1.Attributes.Remove("class");
                    //step1.Attributes.Add("class", "row setup-content step hiddenStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step hiddenStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step activeStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step hiddenStepInfo");

                    divIndividual.Visible = true;
                }
                else if (pageindex == 4)
                {
                    divStep1.Attributes.Remove("class");
                    divStep1.Attributes.Add("class", "col-md-3");
                    divStep2.Attributes.Remove("class");
                    divStep2.Attributes.Add("class", "col-md-3");
                    divStep3.Attributes.Remove("class");
                    divStep3.Attributes.Add("class", "col-md-3");
                    divStep4.Attributes.Remove("class");
                    divStep4.Attributes.Add("class", "col-md-3 activestep");

                    //step1.Attributes.Remove("class");
                    //step1.Attributes.Add("class", "row setup-content step hiddenStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step hiddenStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step hiddenStepInfo");
                    //step2.Attributes.Remove("class");
                    //step2.Attributes.Add("class", "row setup-content step activeStepInfo");

                    divIndividual.Visible = true;
                }
            }
        }
    }
    #endregion
}
