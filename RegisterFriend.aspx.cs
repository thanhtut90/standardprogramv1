﻿using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegisterFriend : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = string.Empty;
            string flowid = string.Empty;
            if (Request.QueryString["event"] != null && Request.QueryString["del"] != null)
            {
                tmpDataList tmpList = GetShowIDByEventName(Request.QueryString["event"].ToString());
                if (string.IsNullOrEmpty(tmpList.showID))
                    Response.Redirect("404.aspx");
                else
                {
                    showid = tmpList.showID;
                    flowid = tmpList.FlowID;
                    string page = "";
                    string step = "";
                    string grpNum = "";
                    FlowControler Flw = new FlowControler(fn);
                    Dictionary<string, string> nValues = Flw.GetNextRoute(flowid);
                    if (nValues.Count > 0)
                    {
                        page = nValues["nURL"].ToString();
                        step = nValues["nStep"].ToString();
                        flowid = nValues["FlowID"].ToString();
                    }
                    RegGroupObj rgp = new RegGroupObj(fn);
                    grpNum = rgp.GenGroupNumber(showid);
                    string route = Flw.MakeFullURL(page, flowid, showid, grpNum, step);
                    route += "&rmd=" + cFun.EncryptValue(Request.QueryString["del"].ToString());
                    Response.Redirect(route);
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }
    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }

    public class tmpDataList
    {
        public string showID { get; set; }
        public string FlowID { get; set; }
    }
    private tmpDataList GetShowIDByEventName(string eventName)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            cList.showID = "";
            cList.FlowID = "";
            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                cList.showID = dList.Rows[0]["showID"].ToString();
                cList.FlowID = dList.Rows[0]["FLW_ID"].ToString();
            }
        }
        catch { }
        return cList;
    }
}