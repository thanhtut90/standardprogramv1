﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Logging;
using System.Data;
using Corpit.Email;
using System.Globalization;

public partial class SuccessM : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cComFuz = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        SetSiteMaster(showid);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);

            insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actsuccess, urlQuery);

            RegGroupObj rgg = new RegGroupObj(fn);
            rgg.updateGroupCurrentStep(urlQuery);

            RegDelegateObj rgd = new RegDelegateObj(fn);
            rgd.updateDelegateCurrentStep(urlQuery);

            setRegisterAnotherShowButton();//***

            string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
            FlowControler flwControl = new FlowControler(fn);
            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);

            StatusSettings stuSettings = new StatusSettings(fn);

            string regstatus = "0";
            DataTable dt = new DataTable();
            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
            {
                dt = rgg.getRegGroupByID(GroupRegID, showid);
                if (dt.Rows.Count > 0)
                {
                    regstatus = dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                }

                rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                rgd.updateDelegateRegStatus(GroupRegID, stuSettings.Success, showid);
            }
            else//*flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL
            {
                rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                if (!string.IsNullOrEmpty(DelegateID))
                {
                    dt = rgd.getDataByGroupIDRegno(GroupRegID, DelegateID, showid);
                    if (dt.Rows.Count > 0)
                    {
                        regstatus = dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                    }

                    rgd.updateStatus(DelegateID, stuSettings.Success, showid);
                }
            }

            #region bindSuccessMessage
            lblSuccessMessage.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowSuccessMsg) ? flwMasterConfig.FlowSuccessMsg : "");
            if (string.IsNullOrEmpty(lblSuccessMessage.Text))
                PanelMsg.Visible = false;
            #endregion

            //if (cComFuz.ParseInt(regstatus) == stuSettings.Pending)
            {
                FlowControler flw = new FlowControler(fn, urlQuery);
                //  flw.SendCurrentStepEmail(urlQuery);
                EmailHelper esender = new EmailHelper();
                esender.SendCurrentFlowStepEmail(urlQuery);
            }

            try
            {
                CommonFuns cFuz = new CommonFuns();
                string DID = "";
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    DID = GroupRegID;
                }
                else
                    DID = DelegateID;
                HTMLTemplateControler htmlControl = new HTMLTemplateControler();
                string template = htmlControl.CreateAcknowledgeLetterHTMLTemplate(showid, DID);
                string rtnFile = htmlControl.CreatePDF(template, showid, DID, "Acknowledge");
                if (!string.IsNullOrEmpty(rtnFile))
                {
                    ShowPDF.Visible = true;
                    ShowPDF.Attributes["src"] = rtnFile;
                }
            }
            catch { }
        }
    }

    #region PageSetting
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
            Page.MasterPageFile = masterPage;
    }

    #endregion

    #region insertLogFlowAction
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        string step = cComFuz.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    #region  Register for another show for Cross-selling ISRRS and SG-ANZICS vice-visa[14-11-2018]
    static string delegateRegNo = "Delegate_RefRegno";
    static string iSRRS2019DelegateRegShowID = "NCE361";
    static string iSRRS2019DelegateRegFlowID = "F394";
    static string iSRRSProfDefault = "Allied Health Professionals";
    static string iSRRSProfDoctors = "Doctor";
    static string iSRRSProfAdministrative = "Administrative Professionals & Others";
    static string iSRRSProfNurses = "Nurse";
    static string iSRRSProfStudents = "Students**";
    static string iSRRS2019EventWebsite = "http://www.isrrs2019.sg/";
    static string SGANZICSDelegateRegShowID = "BFZ364";
    static string SGANZICSDelegateRegFlowID = "F399";
    static string SGANZICSProfDefault = "Allied Health Professionals in Intensive Care";
    static string SGANZICSProfDoctors = "Doctor";
    static string SGANZICSProfTraineeDoc = "Trainee Doctors**";
    static string SGANZICSProfLMICDoc = "LMIC Doctors";
    static string SGANZICSProfNurses = "Nurse";
    static string SGANZICSProfStudents = "Students**";
    static string SGANZICSProfLMICNurse = "LMIC Nurse";
    static string SGANZICSEventWebsite = "http://www.sg-anzics.com/";
    private void setRegisterAnotherShowButton()
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
        string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        if (!string.IsNullOrEmpty(showid))
        {
            if (!string.IsNullOrEmpty(DelegateID))
            {
                string crossShowID = "";
                string crossFlowID = "";
                if (showid == iSRRS2019DelegateRegShowID && flowid == iSRRS2019DelegateRegFlowID)
                {
                    crossShowID = SGANZICSDelegateRegShowID;
                    crossFlowID = SGANZICSDelegateRegFlowID;
                    btnReturnWebsite.HRef = iSRRS2019EventWebsite;
                }
                else if (showid == SGANZICSDelegateRegShowID && flowid == SGANZICSDelegateRegFlowID)
                {
                    crossShowID = iSRRS2019DelegateRegShowID;
                    crossFlowID = iSRRS2019DelegateRegFlowID;
                    btnReturnWebsite.HRef = SGANZICSEventWebsite;
                }

                if (!string.IsNullOrEmpty(crossShowID) && !string.IsNullOrEmpty(crossFlowID))
                {
                    RegAdditionalControler addCtrl = new RegAdditionalControler(fn);
                    string colleagueValue = addCtrl.getColleagueIDByRefValue(DelegateID, showid, flowid);
                    if (string.IsNullOrEmpty(colleagueValue))
                    {
                        ShowControler shwCtr = new ShowControler(fn);
                        Show shw = shwCtr.GetShow(crossShowID);
                        divRegisterAnotherShow.Visible = true;

                        btnRegisterAnotherShow.Visible = false;
                        btnRegisterAnotherShow.Text = "Register for " + shw.SHW_Name;
                        btnRegisterAnotherShow.Enabled = false;// true;
                    }
                }
            }
        }
    }
    protected void btnRegisterAnotherShow_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
        string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        if (!string.IsNullOrEmpty(showid))
        {
            if (!string.IsNullOrEmpty(DelegateID))
            {
                string crossShowID = "";
                string crossFlowID = "";
                bool isGoToISRRS = false;
                if (showid == iSRRS2019DelegateRegShowID && flowid == iSRRS2019DelegateRegFlowID)
                {
                    crossShowID = SGANZICSDelegateRegShowID;
                    crossFlowID = SGANZICSDelegateRegFlowID;
                    isGoToISRRS = false;
                }
                else if (showid == SGANZICSDelegateRegShowID && flowid == SGANZICSDelegateRegFlowID)
                {
                    crossShowID = iSRRS2019DelegateRegShowID;
                    crossFlowID = iSRRS2019DelegateRegFlowID;
                    isGoToISRRS = true;
                }

                if (!string.IsNullOrEmpty(crossShowID) && !string.IsNullOrEmpty(crossFlowID))
                {
                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    SetUpController setCtrl = new SetUpController(fn);
                    DataTable dt = new DataTable();
                    dt = rgd.getDataByGroupIDRegno(GroupRegID, DelegateID, showid);
                    if (dt.Rows.Count > 0)
                    {
                        rgd.groupid = dt.Rows[0]["RegGroupID"].ToString();
                        rgd.regno = dt.Rows[0]["Regno"].ToString();
                        rgd.con_categoryID = dt.Rows[0]["con_CategoryId"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["con_CategoryId"].ToString()) ? int.Parse(dt.Rows[0]["con_CategoryId"].ToString()) : 0) : 0;
                        string salutation = "0";
                        if (!String.IsNullOrEmpty(dt.Rows[0]["reg_Salutation"].ToString()))
                        {
                            string salName = setCtrl.getSalutationNameByID(dt.Rows[0]["reg_Salutation"].ToString(), showid);
                            salutation = setCtrl.getSalutationIDByName(salName, crossShowID);//***
                        }
                        if (string.IsNullOrEmpty(salutation))
                        {
                            salutation = "0";
                        }
                        rgd.salutation = salutation;
                        rgd.fname = dt.Rows[0]["reg_FName"].ToString();
                        rgd.lname = dt.Rows[0]["reg_LName"].ToString();
                        rgd.oname = dt.Rows[0]["reg_OName"].ToString();
                        rgd.passno = dt.Rows[0]["reg_PassNo"].ToString();
                        rgd.isreg = dt.Rows[0]["reg_isReg"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_isReg"].ToString()) ? int.Parse(dt.Rows[0]["reg_isReg"].ToString()) : 0) : 0;
                        rgd.regspecific = dt.Rows[0]["reg_sgregistered"].ToString();//MCR/SNB/PRN
                        rgd.idno = dt.Rows[0]["reg_IDno"].ToString();//MCR/SNB/PRN No.
                        rgd.staffid = dt.Rows[0]["reg_staffid"].ToString();//no use in design for this field
                        rgd.designation = dt.Rows[0]["reg_Designation"].ToString();
                        rgd.jobtitle = dt.Rows[0]["reg_Jobtitle_alliedstu"].ToString();//if Profession is Allied Health
                        string profession = "0";
                        if (!String.IsNullOrEmpty(dt.Rows[0]["reg_Profession"].ToString()))
                        {
                            /*check profession*/
                            string proforiginalID = dt.Rows[0]["reg_Profession"].ToString();
                            string profName = setCtrl.getProfessionNameByID(proforiginalID, showid);
                            string proforiginalName = setCtrl.getProfessionNameByID(proforiginalID, showid);
                            if (isGoToISRRS)
                            {
                                if (proforiginalName == SGANZICSProfTraineeDoc || proforiginalName == SGANZICSProfLMICDoc)
                                {
                                    profName = iSRRSProfDoctors;
                                }
                                else if(proforiginalName == SGANZICSProfLMICNurse)
                                {
                                    profName = iSRRSProfNurses;
                                }
                                else if (proforiginalName == SGANZICSProfDefault)
                                {
                                    profName = iSRRSProfDefault;
                                }
                            }
                            else
                            {
                                if (proforiginalName == iSRRSProfDefault)
                                {
                                    profName = SGANZICSProfDefault;
                                }
                                else if(proforiginalName == iSRRSProfAdministrative && dt.Rows[0]["reg_Country"].ToString() != "179")
                                {
                                    profName = SGANZICSProfDoctors;
                                }
                            }
                            profession = setCtrl.getProfessionIDByName(profName, crossShowID);//***
                        }
                        if (string.IsNullOrEmpty(profession))
                        {
                            profession = "0";
                        }
                        rgd.profession = profession;
                        rgd.department = dt.Rows[0]["reg_Department"].ToString();
                        string organization = "0";
                        if (!String.IsNullOrEmpty(dt.Rows[0]["reg_Organization"].ToString()))
                        {
                            string orgName = setCtrl.getOrganisationNameByID(dt.Rows[0]["reg_Organization"].ToString(), showid);
                            organization = setCtrl.getOrganisationIDByName(orgName, crossShowID);//***
                        }
                        if (string.IsNullOrEmpty(organization))
                        {
                            organization = "0";
                        }
                        rgd.organization = organization;
                        string institution = "0";
                        if (!String.IsNullOrEmpty(dt.Rows[0]["reg_Institution"].ToString()))
                        {
                            string instiName = setCtrl.getInstitutionNameByID(dt.Rows[0]["reg_Institution"].ToString(), showid);
                            institution = setCtrl.getInstitutionIDByName(instiName, crossShowID);//***
                        }
                        if (string.IsNullOrEmpty(institution))
                        {
                            institution = "0";
                        }
                        rgd.institution = institution;
                        rgd.address1 = dt.Rows[0]["reg_Address1"].ToString();
                        rgd.address2 = dt.Rows[0]["reg_Address2"].ToString();
                        rgd.address3 = dt.Rows[0]["reg_Address3"].ToString();
                        rgd.address4 = dt.Rows[0]["reg_Address4"].ToString();
                        rgd.city = dt.Rows[0]["reg_City"].ToString();
                        rgd.state = dt.Rows[0]["reg_State"].ToString();
                        rgd.postalcode = dt.Rows[0]["reg_PostalCode"].ToString();
                        rgd.country = dt.Rows[0]["reg_Country"].ToString();
                        rgd.rcountry = dt.Rows[0]["reg_RCountry"].ToString();
                        rgd.telcc = dt.Rows[0]["reg_Telcc"].ToString();
                        rgd.telac = dt.Rows[0]["reg_Telac"].ToString();
                        rgd.tel = dt.Rows[0]["reg_Tel"].ToString();
                        rgd.mobilecc = dt.Rows[0]["reg_Mobcc"].ToString();
                        rgd.mobileac = dt.Rows[0]["reg_Mobac"].ToString();
                        rgd.mobile = dt.Rows[0]["reg_Mobile"].ToString();
                        rgd.faxcc = dt.Rows[0]["reg_Faxcc"].ToString();
                        rgd.faxac = dt.Rows[0]["reg_Faxac"].ToString();
                        rgd.fax = dt.Rows[0]["reg_Fax"].ToString();
                        rgd.email = dt.Rows[0]["reg_Email"].ToString();
                        string affiliation = "0";
                        if (!String.IsNullOrEmpty(dt.Rows[0]["reg_Affiliation"].ToString()))
                        {
                            string affiName = setCtrl.getAffiliationNameByID(dt.Rows[0]["reg_Affiliation"].ToString(), showid);
                            affiliation = setCtrl.getAffiliationIDByName(affiName, crossShowID);//***
                        }
                        if (string.IsNullOrEmpty(affiliation))
                        {
                            affiliation = "0";
                        }
                        rgd.affiliation = affiliation;
                        string dietary = "0";
                        if (!String.IsNullOrEmpty(dt.Rows[0]["reg_Dietary"].ToString()))
                        {
                            string dietName = setCtrl.getDietaryNameByID(dt.Rows[0]["reg_Dietary"].ToString(), showid);
                            dietary = setCtrl.getDietaryIDByName(dietName, crossShowID);//***
                        }
                        if (string.IsNullOrEmpty(dietary))
                        {
                            dietary = "0";
                        }
                        rgd.dietary = dietary;
                        rgd.nationality = dt.Rows[0]["reg_Nationality"].ToString();
                        rgd.age = dt.Rows[0]["reg_Age"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_Age"].ToString()) ? int.Parse(dt.Rows[0]["reg_Age"].ToString()) : 0) : 0;
                        rgd.dob = dt.Rows[0]["reg_DOB"].ToString();
                        rgd.gender = dt.Rows[0]["reg_Gender"].ToString();
                        rgd.additional4 = dt.Rows[0]["reg_Additional4"].ToString();
                        rgd.additional5 = dt.Rows[0]["reg_Additional5"].ToString();
                        rgd.memberno = dt.Rows[0]["reg_Membershipno"].ToString();

                        rgd.vname = dt.Rows[0]["reg_vName"].ToString();
                        rgd.vdob = dt.Rows[0]["reg_vDOB"].ToString();
                        rgd.vpassno = dt.Rows[0]["reg_vPassno"].ToString();
                        rgd.vpassexpiry = dt.Rows[0]["reg_vPassexpiry"].ToString();
                        rgd.vpassissuedate = dt.Rows[0]["reg_vIssueDate"].ToString();
                        rgd.vembarkation = dt.Rows[0]["reg_vEmbarkation"].ToString();
                        rgd.varrivaldate = dt.Rows[0]["reg_vArrivalDate"].ToString();
                        rgd.vcountry = dt.Rows[0]["reg_vCountry"].ToString();

                        rgd.udfcname = dt.Rows[0]["UDF_CName"].ToString();
                        rgd.udfdeltype = dt.Rows[0]["UDF_DelegateType"].ToString();
                        rgd.udfprofcat = dt.Rows[0]["UDF_ProfCategory"].ToString();
                        rgd.udfprofcatother = dt.Rows[0]["UDF_ProfCategoryOther"].ToString();
                        rgd.udfcpcode = dt.Rows[0]["UDF_CPcode"].ToString();
                        rgd.udfcldept = dt.Rows[0]["UDF_CLDepartment"].ToString();
                        rgd.udfcaddress = dt.Rows[0]["UDF_CAddress"].ToString();
                        rgd.udfclcompany = dt.Rows[0]["UDF_CLCompany"].ToString();
                        rgd.udfclcompanyother = dt.Rows[0]["UDF_CLCompanyOther"].ToString();
                        rgd.udfccountry = dt.Rows[0]["UDF_CCountry"].ToString();

                        rgd.supname = dt.Rows[0]["reg_SupervisorName"].ToString();
                        rgd.supdesignation = dt.Rows[0]["reg_SupervisorDesignation"].ToString();
                        rgd.supcontact = dt.Rows[0]["reg_SupervisorContact"].ToString();
                        rgd.supemail = dt.Rows[0]["reg_SupervisorEmail"].ToString();

                        rgd.othersal = dt.Rows[0]["reg_SalutationOthers"].ToString();
                        rgd.otherprof = dt.Rows[0]["reg_otherProfession"].ToString();
                        rgd.otherdept = dt.Rows[0]["reg_otherDepartment"].ToString();
                        rgd.otherorg = dt.Rows[0]["reg_otherOrganization"].ToString();
                        rgd.otherinstitution = dt.Rows[0]["reg_otherInstitution"].ToString();

                        rgd.aemail = dt.Rows[0]["reg_aemail"].ToString();
                        rgd.isSMS = dt.Rows[0]["reg_isSMS"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_isSMS"].ToString()) ? int.Parse(dt.Rows[0]["reg_isSMS"].ToString()) : 0) : 0;

                        rgd.remark = dt.Rows[0]["reg_remark"].ToString();
                        rgd.remark_groupupload = dt.Rows[0]["reg_remarkGUpload"].ToString();
                        rgd.approvestatus = dt.Rows[0]["reg_approveStatus"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_approveStatus"].ToString()) ? int.Parse(dt.Rows[0]["reg_approveStatus"].ToString()) : 0) : 0;
                        rgd.createdate = dt.Rows[0]["reg_datecreated"].ToString();
                        rgd.recycle = dt.Rows[0]["recycle"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["recycle"].ToString()) ? int.Parse(dt.Rows[0]["recycle"].ToString()) : 0) : 0;
                        rgd.stage = dt.Rows[0]["reg_Stage"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_Stage"].ToString()) ? dt.Rows[0]["reg_Stage"].ToString() : "") : "";

                        rgd.showID = crossShowID;//***

                        string newregno = string.Empty;
                        string newgroupid = string.Empty;
                        int isSuccessSave = insertRegDelegateData(showid, flowid, rgd, DelegateID, crossFlowID, ref newregno, ref newgroupid);
                        if (isSuccessSave > 0)
                        {
                            string page = "";
                            string step = "";
                            FlowControler Flw = new FlowControler(fn);
                            Dictionary<string, string> nValues = Flw.GetNextRoute(crossFlowID);//***
                            if (nValues.Count > 0)
                            {
                                page = nValues["nURL"].ToString();
                                step = nValues["nStep"].ToString();
                                crossFlowID = nValues["FlowID"].ToString();//***
                            }
                            string route = Flw.MakeFullURL(page, crossFlowID, crossShowID, newgroupid, step, newregno);//***
                            Response.Redirect(route);
                        }
                        else
                        {
                            Response.Redirect("404.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("404.aspx");
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
        }
    }
    private int insertRegDelegateData(string showid, string flowid, RegDelegateObj rgd, string refRegno, string crossFlowID, ref string regno, ref string groupid)
    {
        int isSuccess = 0;
        try
        {
            RegAdditionalControler addCtrl = new RegAdditionalControler(fn);
            string colleagueValue = addCtrl.getColleagueIDByRefValue(refRegno, showid, flowid);
            if (string.IsNullOrEmpty(colleagueValue))
            {
                #region create delegate in tb_RegDelegate
                //create group in tb_RegGroup
                RegGroupObj rgp = new RegGroupObj(fn);
                groupid = rgp.GenGroupNumber(rgd.showID);// GetRunNUmber("GRP_KEY");
                rgp.createRegGroupIDOnly(groupid, 0, rgd.showID);

                regno = rgd.GenDelegateNumber(rgd.showID);

                rgd.groupid = groupid;
                rgd.regno = regno;
                rgd.con_categoryID = rgd.con_categoryID;
                rgd.salutation = rgd.salutation;
                rgd.fname = rgd.fname;
                rgd.lname = rgd.lname;
                rgd.oname = rgd.oname;
                rgd.passno = rgd.passno;
                rgd.isreg = rgd.isreg;
                rgd.regspecific = rgd.regspecific;//MCR/SNB/PRN
                rgd.idno = rgd.idno;//MCR/SNB/PRN No.
                rgd.staffid = rgd.staffid;//no use in design for this field
                rgd.designation = rgd.designation;
                rgd.jobtitle = rgd.jobtitle;//if Profession is Allied Health
                rgd.profession = rgd.profession;
                rgd.department = rgd.department;
                rgd.organization = rgd.organization;
                rgd.institution = rgd.institution;
                rgd.address1 = rgd.address1;
                rgd.address2 = rgd.address2;
                rgd.address3 = rgd.address3;
                rgd.address4 = rgd.address4;
                rgd.city = rgd.city;
                rgd.state = rgd.state;
                rgd.postalcode = rgd.postalcode;
                rgd.country = rgd.country;
                rgd.rcountry = rgd.rcountry;
                rgd.telcc = rgd.telcc;
                rgd.telac = rgd.telac;
                rgd.tel = rgd.tel;
                rgd.mobilecc = rgd.mobilecc;
                rgd.mobileac = rgd.mobileac;
                rgd.mobile = rgd.mobile;
                rgd.faxcc = rgd.faxcc;
                rgd.faxac = rgd.faxac;
                rgd.fax = rgd.fax;
                rgd.email = rgd.email;
                rgd.affiliation = rgd.affiliation;
                rgd.dietary = rgd.dietary;
                rgd.nationality = rgd.nationality;
                rgd.age = rgd.age;
                rgd.dob = rgd.dob;
                rgd.gender = rgd.gender;
                rgd.additional4 = rgd.additional4;
                rgd.additional5 = rgd.additional5;
                rgd.memberno = rgd.memberno;

                rgd.vname = rgd.vname;
                rgd.vdob = rgd.vdob;
                rgd.vpassno = rgd.vpassno;
                rgd.vpassexpiry = rgd.vpassexpiry;
                rgd.vpassissuedate = rgd.vpassissuedate;
                rgd.vembarkation = rgd.vembarkation;
                rgd.varrivaldate = rgd.varrivaldate;
                rgd.vcountry = rgd.vcountry;

                rgd.udfcname = rgd.udfcname;
                rgd.udfdeltype = rgd.udfdeltype;
                rgd.udfprofcat = rgd.udfprofcat;
                rgd.udfprofcatother = rgd.udfprofcatother;
                rgd.udfcpcode = rgd.udfcpcode;
                rgd.udfcldept = rgd.udfcldept;
                rgd.udfcaddress = rgd.udfcaddress;
                rgd.udfclcompany = rgd.udfclcompany;
                rgd.udfclcompanyother = rgd.udfclcompanyother;
                rgd.udfccountry = rgd.udfccountry;

                rgd.supname = rgd.supname;
                rgd.supdesignation = rgd.supdesignation;
                rgd.supcontact = rgd.supcontact;
                rgd.supemail = rgd.supemail;

                rgd.othersal = rgd.othersal;
                rgd.otherprof = rgd.otherprof;
                rgd.otherdept = rgd.otherdept;
                rgd.otherorg = rgd.otherorg;
                rgd.otherinstitution = rgd.otherinstitution;

                rgd.aemail = rgd.aemail;
                rgd.isSMS = rgd.isSMS;

                rgd.remark = refRegno;// rgd.remark;
                rgd.remark_groupupload = rgd.remark_groupupload;
                rgd.approvestatus = 0;// rgd.approvestatus;
                rgd.createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                rgd.recycle = rgd.recycle;
                rgd.stage = "1";

                rgd.showID = rgd.showID;
                isSuccess = rgd.saveRegDelegate();
                rgd.updateStep(regno, crossFlowID, "1", rgd.showID);
                saveRegAdditionalRegColleague(showid, flowid, refRegno, regno);
                #endregion
            }
        }
        catch (Exception ex)
        { }

        return isSuccess;
    }
    protected void saveRegAdditionalRegColleague(string showid, string flowid, string regno, string crossRegno)//regno=originalshowregisteredpersonRegno
    {
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string delegateid = regno;
            string currentStep = cComFuz.DecryptValue(urlQuery.CurrIndex);
            string delegateType = BackendRegType.backendRegType_Group;
            string ownerID = groupid;
            if (!string.IsNullOrEmpty(delegateid))
            {
                delegateType = BackendRegType.backendRegType_Delegate;
                ownerID = delegateid;
            }
            /*check day pass*/
            OrderControler oCtrl = new OrderControler(fn);
            string confDayPassType = oCtrl.GetDelegateOrderConfDayPassType(urlQuery, ConfDefaultValue.conf_MainItem);
            delegateType = confDayPassType;
            /*check day pass*/
            RegAdditionalObj regAddObj = new RegAdditionalObj();
            regAddObj.ad_ShowID = showid;
            regAddObj.ad_FlowID = flowid;
            regAddObj.ad_OwnerID = ownerID;
            regAddObj.ad_FlowStep = currentStep;
            regAddObj.ad_DelegateType = delegateType;
            regAddObj.ad_Value = crossRegno;
            regAddObj.ad_Type = RegAdditionalType.registerColleague;
            regAddObj.ad_NoteID = 0;
            RegAdditionalControler regAddCtrl = new RegAdditionalControler(fn);
            regAddCtrl.SaveRegAdditional(regAddObj);
        }
        catch (Exception ex)
        { }
    }
    #endregion

    protected void btnReturnWebsite_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
        string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        if (!string.IsNullOrEmpty(showid))
        {
            if (!string.IsNullOrEmpty(DelegateID))
            {
                if (showid == iSRRS2019DelegateRegShowID && flowid == iSRRS2019DelegateRegFlowID)
                {
                    Response.Redirect(iSRRS2019EventWebsite);
                }
                else if (showid == SGANZICSDelegateRegShowID && flowid == SGANZICSDelegateRegFlowID)
                {
                    Response.Redirect(SGANZICSEventWebsite);
                }
            }
        }
    }
}