﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="PreviewConfirmation.aspx.cs" Inherits="PreviewConfirmation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType virtualpath="~/Registration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script> 

    <link rel="stylesheet" type="text/css" href="css/jquery.countdown.css" />
    <style type="text/css">
        #defaultCountdown {
            width: 295px;
            height: 20px;
        }
        
    </style>

     <script type="text/javascript" src="scripts/jquery.plugin.js"></script>
    <script type="text/javascript" src="scripts/jquery.countdown.js"></script>

    <script type="text/javascript">

        function MakeRbExclusive(rb) {
            var rbs = $('#rbData :radio');
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i] != rb) rbs[i].checked = false;
            }
        }

        function Reload() {
            window.location.reload();
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
        </script>


    <style type="text/css">
        .style1
        {
            color: #3EBB96;
            font-family:Arial;
        }
    .style2
    {
        font-size: 14px;
        color: #3EBB96;
        text-decoration: none;
        font-family: Arial;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%--<table width="100%">
        <tr>
            <td colspan="4" style="text-align: right; padding-right: 20px;">
                <asp:Button runat="server" Text="Re-login" ID="btnRelogin" 
                    OnClick="btnRelogin_Click" Width="100px" Height="30px" CausesValidation="false" 
                    Visible="false" CssClass="style4" style="font-family: DINPro-Regular" />
                <div style="display: none;">
                    <asp:Timer ID="timerSession" runat="server" OnTick="timerSession_Tick" Interval="60000"></asp:Timer>
                    <asp:UpdatePanel ID="upTimer" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="lblTimer" runat="server" CssClass="style4" 
                                style="font-family: DINPro-Regular"></asp:TextBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerSession" EventName="tick" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div style="float: right;">
                    <div id="defaultCountdown" style="font-family: Calibri; font-size: 14px; padding: 2px;"></div>
                </div>
            </td>
        </tr>
    </table>--%>
    <div id="fcontent">
        <div style="padding: 0px 30px 30px 30px; width: auto; background-color: #F1F1F1; margin:0 auto; position: relative; border-radius: 0px">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <br/><br/>
                    <strong style="font-family:Arial;">Please review all details thoroughly and click "Next" to complete your submission.</strong>
                    <div id="lcontent">
                        <div id="divContactPerson" runat="server" visible="false">
                            <br />
                            <h3 class="style1">Contact Person Information</h3>
                            <br />
                            <table>
                                <tr id="trGSalutation" runat="server">
                                    <td><asp:Label ID="lblGSalutation" runat="server" Text="Title"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGSalutation" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGFName" runat="server">
                                    <td><asp:Label ID="lblGFName" runat="server" Text="First Name"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGFName" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGLName" runat="server">
                                    <td><asp:Label ID="lblGLName" runat="server" Text="Surname"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGLName" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGDesignation" runat="server">
                                    <td><asp:Label ID="lblGDesignation" runat="server" Text="Designation"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGDesignation" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGDepartment" runat="server">
                                    <td><asp:Label ID="lblGDepartment" runat="server" Text="Department"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGDepartment" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGCompany" runat="server">
                                    <td><asp:Label ID="lblGCompany" runat="server" Text="Company"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGCompany" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGIndustry" runat="server">
                                    <td><asp:Label ID="lblGIndustry" runat="server" Text="Industry"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGIndustry" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGAddress1" runat="server">
                                    <td><asp:Label ID="lblGAddress1" runat="server" Text="Address1"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGAddress1" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGAddress2" runat="server">
                                    <td><asp:Label ID="lblGAddress2" runat="server" Text="Address2"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGAddress2" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGAddress3" runat="server">
                                    <td><asp:Label ID="lblGAddress3" runat="server" Text="Address3"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGAddress3" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGCountry" runat="server">
                                    <td><asp:Label ID="lblGCountry" runat="server" Text="Country"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGCountry" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGRCountry" runat="server">
                                    <td><asp:Label ID="lblGRCountry" runat="server" Text="RCountry"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGRCountry" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGCity" runat="server">
                                    <td><asp:Label ID="lblGCity" runat="server" Text="City"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGCity" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGState" runat="server">
                                    <td><asp:Label ID="lblGState" runat="server" Text="State"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGState" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGPostalCode" runat="server">
                                    <td><asp:Label ID="lblGPostalCode" runat="server" Text="Postal Code"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGPostalCode" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGTel" runat="server">
                                    <td><asp:Label ID="lblGTel" runat="server" Text="Telephone"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGTel" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGMobile" runat="server">
                                    <td><asp:Label ID="lblGMobile" runat="server" Text="Mobile"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGMobile" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGFax" runat="server">
                                    <td><asp:Label ID="lblGFax" runat="server" Text="Fax"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGFax" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGEmail" runat="server">
                                    <td><asp:Label ID="lblGEmail" runat="server" Text="Email"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGEmail" runat="server" Text=""></asp:Label></td>
                                </tr>
                                 <tr id="trGAge" runat="server">
                                    <td><asp:Label ID="lblGAge" runat="server" Text="Age"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGAge" runat="server" Text=""></asp:Label></td>
                                </tr>
                                 <tr id="trGDOB" runat="server">
                                    <td><asp:Label ID="lblGDOB" runat="server" Text="Date of Birth"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGDOB" runat="server" Text=""></asp:Label></td>
                                </tr>
                                 <tr id="trGGender" runat="server">
                                    <td><asp:Label ID="lblGGender" runat="server" Text="Gender"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGGender" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGVisitDate" runat="server">
                                    <td><asp:Label ID="lblGVisitDate" runat="server" Text="Visit Date"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGVisitDate" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGVisitTime" runat="server">
                                    <td><asp:Label ID="lblGVisitTime" runat="server" Text="Visit Time"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGVisitTime" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trGPassword" runat="server">
                                    <td><asp:Label ID="lblGPassword" runat="server" Text="Password"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGPassword" runat="server" Text=""></asp:Label></td>
                                </tr>
                                 <tr id="trGAdditional4" runat="server">
                                    <td><asp:Label ID="lblGAdditional4" runat="server" Text="Additional4"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGAdditional4" runat="server" Text=""></asp:Label></td>
                                </tr>
                                 <tr id="trGAdditional5" runat="server">
                                    <td><asp:Label ID="lblGAdditional5" runat="server" Text="Additional5"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblGAdditional5" runat="server" Text=""></asp:Label></td>
                                </tr>
                            </table>
                        </div>

                        <div id="divCompany" runat="server" visible="false">
                            <br />
                            <h3 class="style1">Company Information</h3>
                            <br />
                            <table>
                                <tr id="trCName" runat="server">
                                    <td><asp:Label ID="lblCName" runat="server" Text="Name"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCName" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCAddress1" runat="server">
                                    <td><asp:Label ID="lblCAddress1" runat="server" Text="Address1"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCAddress1" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCAddress2" runat="server">
                                    <td><asp:Label ID="lblCAddress2" runat="server" Text="Address2"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCAddress2" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCAddress3" runat="server">
                                    <td><asp:Label ID="lblCAddress3" runat="server" Text="Address3"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCAddress3" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCCountry" runat="server">
                                    <td><asp:Label ID="lblCCountry" runat="server" Text="Country"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCCountry" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCCity" runat="server">
                                    <td><asp:Label ID="lblCCity" runat="server" Text="City"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCCity" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCState" runat="server">
                                    <td><asp:Label ID="lblCState" runat="server" Text="State"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCState" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCZipcode" runat="server">
                                    <td><asp:Label ID="lblCZipcode" runat="server" Text="Zip Code"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCZipcode" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCTel" runat="server">
                                    <td><asp:Label ID="lblCTel" runat="server" Text="Telephone"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCTel" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCFax" runat="server">
                                    <td><asp:Label ID="lblCFax" runat="server" Text="Fax"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCFax" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCEmail" runat="server">
                                    <td><asp:Label ID="lblCEmail" runat="server" Text="Email"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCEmail" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCWebsite" runat="server">
                                    <td><asp:Label ID="lblCWebsite" runat="server" Text="Website"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCWebsite" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCAdditional1" runat="server">
                                    <td><asp:Label ID="lblCAdditional1" runat="server" Text="Additional1"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCAdditional1" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCAdditional2" runat="server">
                                    <td><asp:Label ID="lblCAdditional2" runat="server" Text="Additional2"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCAdditional2" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCAdditional3" runat="server">
                                    <td><asp:Label ID="lblCAdditional3" runat="server" Text="Additional3"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCAdditional3" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCAdditional4" runat="server">
                                    <td><asp:Label ID="lblCAdditional4" runat="server" Text="Additional4"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCAdditional4" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr id="trCAdditional5" runat="server">
                                    <td><asp:Label ID="lblCAdditional5" runat="server" Text="Additional5"></asp:Label></td>
                                    <td>:</td>
                                    <td><asp:Label ID="_lblCAdditional5" runat="server" Text=""></asp:Label></td>
                                </tr>
                            </table>
                        </div>

                        <div id="divDelegate" runat="server" visible="false">
                            <br />
                            <h3 class="style1">Delegate Information</h3>
                            <br />
                            <div style="overflow-x:auto;overflow-y:hidden;" class="table-responsive">
                                <table style="width:100%;" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <%-- <tr>
                                                <th><asp:Label ID="lblRegistrationID" runat="server" Text="Registration ID"></asp:Label></th>
                                                <th>:</th>
                                                <th><asp:Label ID="_lblRegno" runat="server" Text="Label"></asp:Label></th>
                                            </tr>--%>
                                            <th>No.</th>

                                            <th id="trSalutation" runat="server" scope="row">
                                                <asp:Label ID="lblSal" runat="server" Text="Title"></asp:Label>
                                            </th>
                                            <th id="trFName" runat="server" scope="row">
                                                <asp:Label ID="lblFName" runat="server" Text="First Name"></asp:Label>
                                            </th>
                                            <th id="trLName" runat="server" scope="row">
                                                <asp:Label ID="lblLName" runat="server" Text="Surname"></asp:Label>
                                            </th>
                                            <th id="trOName" runat="server" scope="row">
                                                <asp:Label ID="lblOName" runat="server" Text="Other Name"></asp:Label>
                                            </th>
                                            <th id="trPassno" runat="server" scope="row">
                                                <asp:Label ID="lblPassno" runat="server" Text="NRIC/Passport No."></asp:Label>
                                            </th>
                                            <th id="trIsReg" runat="server" scope="row">
                                                <asp:Label ID="lblIsReg" runat="server" Text="Are you a Singapore registered doctor/nurse/pharmacist?"></asp:Label>
                                            </th>
                                            <th id="trRegSpecific" runat="server" scope="row">
                                                <asp:Label ID="lblRegSpecific" runat="server" Text="MCR/SNB/PRN"></asp:Label>
                                            </th>
                                            <th id="trIDNo" runat="server" scope="row">
                                                <asp:Label ID="lblIDNo" runat="server" Text="MCR/SNB/PRN No."></asp:Label>
                                            </th>
                                            <th id="trDesignation" runat="server" scope="row">
                                                <asp:Label ID="lblDesignation" runat="server" Text="Designation"></asp:Label>
                                            </th>
                                            <th id="trProfession" runat="server" scope="row">
                                                <asp:Label ID="lblProfession" runat="server" Text="Profession"></asp:Label>
                                            </th>
                                            <th id="trOrg" runat="server" scope="row">
                                                <asp:Label ID="lblOrg" runat="server" Text="Organization"></asp:Label>
                                            </th>
                                            <th id="trInstitution" runat="server" scope="row">
                                                <asp:Label ID="lblInstitution" runat="server" Text="Institution"></asp:Label>
                                            </th>
                                            <th id="trDept" runat="server" scope="row">
                                                <asp:Label ID="lblDept" runat="server" Text="Department"></asp:Label>
                                            </th>
                                            <th id="trAddress1" runat="server" scope="row">
                                                <asp:Label ID="lblAddress1" runat="server" Text="Address1"></asp:Label>
                                            </th>
                                            <th id="trAddress2" runat="server" scope="row">
                                                <asp:Label ID="lblAddress2" runat="server" Text="Address2"></asp:Label>
                                            </th>
                                            <th id="trAddress3" runat="server" scope="row">
                                                <asp:Label ID="lblAddress3" runat="server" Text="Address3"></asp:Label>
                                            </th>
                                            <th id="trAddress4" runat="server" scope="row">
                                                <asp:Label ID="lblAddress4" runat="server" Text="Address4"></asp:Label>
                                            </th>
                                            <th id="trCountry" runat="server" scope="row">
                                                <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                                            </th>
                                            <th id="trRCountry" runat="server" scope="row">
                                                <asp:Label ID="lblRCountry" runat="server" Text="RCountry"></asp:Label>
                                            </th>
                                            <th id="trCity" runat="server" scope="row">
                                                <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                                            </th>
                                            <th id="trState" runat="server" scope="row">
                                                <asp:Label ID="lblState" runat="server" Text="State"></asp:Label>
                                            </th>
                                            <th id="trPostal" runat="server" scope="row">
                                                <asp:Label ID="lblPostal" runat="server" Text="Postal Code"></asp:Label>
                                            </th>
                                            <th id="trTel" runat="server" scope="row">
                                                <asp:Label ID="lblTel" runat="server" Text="Telephone"></asp:Label>
                                            </th>
                                            <th id="trMobile" runat="server" scope="row">
                                                <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                                            </th>
                                            <th id="trFax" runat="server" scope="row">
                                                <asp:Label ID="lblFax" runat="server" Text="Fax"></asp:Label>
                                            </th>
                                            <th id="trEmail" runat="server" scope="row">
                                                <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                                            </th>
                                                <th id="trAffiliation" runat="server" scope="row">
                                                <asp:Label ID="lblAffiliation" runat="server" Text="Affiliation"></asp:Label>
                                            </th>
                                            <th id="trDietary" runat="server" scope="row">
                                                <asp:Label ID="lblDietary" runat="server" Text="Dietary"></asp:Label>
                                            </th>
                                            <th id="trNationality" runat="server" scope="row">
                                                <asp:Label ID="lblNationality" runat="server" Text="Nationality"></asp:Label>
                                            </th>
                                            <th id="trAge" runat="server" scope="row">
                                                <asp:Label ID="lblAge" runat="server" Text="Age"></asp:Label>
                                            </th>
                                            <th id="trDOB" runat="server" scope="row">
                                                <asp:Label ID="lblDOB" runat="server" Text="Date of Birth"></asp:Label>
                                            </th>
                                            <th id="trGender" runat="server" scope="row">
                                                <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                            </th>
                                            <th id="trMembershipNo" runat="server" scope="row">
                                                <asp:Label ID="lblMembershipNo" runat="server" Text="MembershipNo"></asp:Label>
                                            </th>
                                            <th id="trAdditional4" runat="server" scope="row">
                                                <asp:Label ID="lblAdditional4" runat="server" Text="Additional4"></asp:Label>
                                            </th>
                                            <th id="trAdditional5" runat="server" scope="row">
                                                <asp:Label ID="lblAdditional5" runat="server" Text="Additional5"></asp:Label>
                                            </th>

                                            <th id="trVName" runat="server" scope="row">
                                                <asp:Label ID="lblVName" runat="server" Text="Visitor Name"></asp:Label>
                                            </th>
                                            <th id="trVDOB" runat="server" scope="row">
                                                <asp:Label ID="lblVDOB" runat="server" Text="Visitor DOB"></asp:Label>
                                            </th>
                                            <th id="trVPass" runat="server" scope="row">
                                                <asp:Label ID="lblVPass" runat="server" Text="Visitor Passport No."></asp:Label>
                                            </th>
                                            <th id="trVPassExpiry" runat="server" scope="row">
                                                <asp:Label ID="lblVPassExpiry" runat="server" Text="Visitor Passport Expiry"></asp:Label>
                                            </th>
                                            <th id="trVCountry" runat="server" scope="row">
                                                <asp:Label ID="lblVCountry" runat="server" Text="Visitor Country"></asp:Label>
                                            </th>

                                            <th id="trUDF_CName" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CName" runat="server" Text="UDFC Name"></asp:Label>
                                            </th>
                                            <th id="trUDF_DelegateType" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_DelegateType" runat="server" Text="UDF Delegate Type"></asp:Label>
                                            </th>
                                            <th id="trUDF_ProfCategory" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_ProfCategory" runat="server" Text="UDF Prof Category"></asp:Label>
                                            </th>
                                            <th id="trUDF_CPcode" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CPcode" runat="server" Text="UDFC Postal Code"></asp:Label>
                                            </th>
                                            <th id="trUDF_CLDepartment" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CLDepartment" runat="server" Text="UDFCL Department"></asp:Label>
                                            </th>
                                            <th id="trUDF_CAddress" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CAddress" runat="server" Text="UDFC Address"></asp:Label>
                                            </th>
                                            <th id="trUDF_CLCompany" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CLCompany" runat="server" Text="UDFCL Company"></asp:Label>
                                            </th>
                                            <th id="trUDF_CCountry" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CCountry" runat="server" Text="UDFC Country"></asp:Label>
                                            </th>

                                            <th id="trSupName" runat="server" scope="row">
                                                <asp:Label ID="lblSupName" runat="server" Text="Supervisor Name"></asp:Label>
                                            </th>
                                            <th id="trSupDesignation" runat="server" scope="row">
                                                <asp:Label ID="lblSupDesignation" runat="server" Text="Supervisor Designation"></asp:Label>
                                            </th>
                                            <th id="trSupContact" runat="server" scope="row">
                                                <asp:Label ID="lblSupContact" runat="server" Text="Supervisor Contact"></asp:Label>
                                            </th>
                                            <th id="trSupEmail" runat="server" scope="row">
                                                <asp:Label ID="lblSupEmail" runat="server" Text="Supervisor Email"></asp:Label>
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <asp:Repeater ID="rptItem" runat="server" OnItemDataBound="rptitemdatabound">
                                            <ItemTemplate>
                                                <%--<table>--%>
                                                <%--<tr>
                                                    <td><asp:Label ID="lblRegistrationID" runat="server" Text="Registration ID"></asp:Label></td>
                                                    <td>:</td>
                                                    <td><asp:Label ID="_lblRegno" runat="server" Text="Label"></asp:Label></td>
                                                </tr>--%>
                                                <tr>
                                                    <td><%#Container.ItemIndex+1 %></td>

                                                    <td id="tdSalutation" runat="server"><asp:Label ID="_lblsal" runat="server"><%#Eval("reg_Salutation")%></asp:Label></td>

                                                    <td id="tdFName" runat="server"><asp:Label ID="_lblFName" runat="server" Text='<%#Eval("reg_FName")%>'></asp:Label></td>

                                                    <td id="tdLName" runat="server"><asp:Label ID="_lblLName" runat="server" Text='<%#Eval("reg_LName")%>'></asp:Label></td>

                                                    <td id="tdOName" runat="server"><asp:Label ID="_lblOName" runat="server" Text='<%#Eval("reg_OName")%>'></asp:Label></td>

                                                    <td id="tdPassno" runat="server"><asp:Label ID="_lblPassno" runat="server" Text='<%#Eval("reg_PassNo")%>'></asp:Label></td>

                                                    <td id="tdIsReg" runat="server"><asp:Label ID="_lblIsReg" runat="server"><%#Eval("reg_isReg") != null ? (Eval("reg_isReg").ToString() == "1" ? "Yes" : "No") : "No"%></asp:Label></td>

                                                    <td id="tdRegSpecific" runat="server"><asp:Label ID="_lblRegSpecific" runat="server" Text='<%#Eval("reg_sgregistered")%>'></asp:Label></td>

                                                    <td id="tdIDNo" runat="server"><asp:Label ID="_lblIDNo" runat="server" Text='<%#Eval("reg_IDno")%>'></asp:Label></td>

                                                    <td id="tdDesignation" runat="server"><asp:Label ID="_lblDesignation" runat="server" Text='<%#Eval("reg_Designation")%>'></asp:Label></td>

                                                    <td id="tdProfession" runat="server"><asp:Label ID="_lblProfession" runat="server" Text='<%#Eval("reg_Profession")%>'></asp:Label></td>

                                                    <td id="tdOrg" runat="server"><asp:Label ID="_lblOrg" runat="server" Text='<%#Eval("reg_Organization")%>'></asp:Label></td>

                                                    <td id="tdInstitution" runat="server"><asp:Label ID="_lblInstitution" runat="server" Text='<%#Eval("reg_Institution")%>'></asp:Label></td>

                                                    <td id="tdDept" runat="server"><asp:Label ID="_lblDept" runat="server" Text='<%#Eval("reg_Department")%>'></asp:Label></td>

                                                    <td id="tdAddress1" runat="server"><asp:Label ID="_lblAddress1" runat="server" Text='<%#Eval("reg_Address1")%>'></asp:Label></td>

                                                    <td id="tdAddress2" runat="server"><asp:Label ID="_lblAddress2" runat="server" Text='<%#Eval("reg_Address2")%>'></asp:Label></td>

                                                    <td id="tdAddress3" runat="server"><asp:Label ID="_lblAddress3" runat="server" Text='<%#Eval("reg_Address3")%>'></asp:Label></td>

                                                    <td id="tdAddress4" runat="server"><asp:Label ID="_lblAddress4" runat="server" Text='<%#Eval("reg_Address4")%>'></asp:Label></td>

                                                    <td id="tdCity" runat="server"><asp:Label ID="_lblCity" runat="server" Text='<%#Eval("reg_City")%>'></asp:Label></td>

                                                    <td id="tdState" runat="server"><asp:Label ID="_lblState" runat="server" Text='<%#Eval("reg_State")%>'></asp:Label></td>

                                                    <td id="tdPostal" runat="server"><asp:Label ID="_lblPostal" runat="server" Text='<%#Eval("reg_PostalCode")%>'></asp:Label></td>

                                                    <td id="tdCountry" runat="server"><asp:Label ID="_lblCountry" runat="server" Text='<%#Eval("reg_Country")%>'></asp:Label></td>

                                                    <td id="tdRCountry" runat="server"><asp:Label ID="_lblRCountry" runat="server" Text='<%#Eval("reg_RCountry")%>'></asp:Label></td>

                                                    <td id="tdTel" runat="server"><asp:Label ID="_lblTel" runat="server"><%#Eval("reg_Tel")%></asp:Label></td>

                                                    <td id="tdMobile" runat="server"><asp:Label ID="_lblMobile" runat="server"><%#Eval("reg_Mobile")%></asp:Label></td>

                                                    <td id="tdFax" runat="server"><asp:Label ID="_lblFax" runat="server"><%#Eval("reg_Fax")%></asp:Label></td>

                                                    <td id="tdEmail" runat="server"><asp:Label ID="_lblEmail" runat="server" Text='<%#Eval("reg_Email")%>'></asp:Label></td>

                                                    <td id="tdAffiliation" runat="server"><asp:Label ID="_lblAffiliation" runat="server" Text='<%#Eval("reg_Affiliation")%>'></asp:Label></td>

                                                    <td id="tdDietary" runat="server"><asp:Label ID="_lblDietary" runat="server" Text='<%#Eval("reg_Dietary")%>'></asp:Label></td>

                                                    <td id="tdNationality" runat="server"><asp:Label ID="_lblNationality" runat="server" Text='<%#Eval("reg_Nationality")%>'></asp:Label></td>

                                                    <td id="tdAge" runat="server"><asp:Label ID="_lblAge" runat="server" Text='<%#Eval("reg_Age")%>'></asp:Label></td>

                                                    <td id="tdDOB" runat="server"><asp:Label ID="_lblDOB" runat="server" Text='<%#Eval("reg_DOB")%>'></asp:Label></td>

                                                    <td id="tdGender" runat="server"><asp:Label ID="_lblGender" runat="server" Text='<%#Eval("reg_Gender")%>'></asp:Label></td>

                                                    <td id="tdMembershipNo" runat="server"><asp:Label ID="_lblMembershipNo" runat="server" Text='<%#Eval("reg_Membershipno")%>'></asp:Label></td>

                                                    <td id="tdAdditional4" runat="server"><asp:Label ID="_lblAdditional4" runat="server" Text='<%#Eval("reg_Additional4")%>'></asp:Label></td>

                                                    <td id="tdAdditional5" runat="server"><asp:Label ID="_lblAdditional5" runat="server" Text='<%#Eval("reg_Additional5")%>'></asp:Label></td>

                                                    <td id="tdVName" runat="server"><asp:Label ID="_lblVName" runat="server" Text='<%#Eval("reg_vName")%>'></asp:Label></td>

                                                    <td id="tdVDOB" runat="server"><asp:Label ID="_lblVDOB" runat="server" Text='<%#Eval("reg_vDOB")%>'></asp:Label></td>

                                                    <td id="tdVPass" runat="server"><asp:Label ID="_lblVPass" runat="server" Text='<%#Eval("reg_vPassno")%>'></asp:Label></td>

                                                    <td id="tdVPassExpiry" runat="server"><asp:Label ID="_lblVPassExpiry" runat="server" Text='<%#Eval("reg_vPassexpiry")%>'></asp:Label></td>

                                                    <td id="tdVCountry" runat="server"><asp:Label ID="_lblVCountry" runat="server" Text='<%#Eval("reg_vCountry")%>'></asp:Label></td>

                                                    <td id="tdUDF_CName" runat="server"><asp:Label ID="_lblUDF_CName" runat="server" Text='<%#Eval("UDF_CName")%>'></asp:Label></td>

                                                    <td id="tdUDF_DelegateType" runat="server"><asp:Label ID="_lblUDF_DelegateType" runat="server" Text='<%#Eval("UDF_DelegateType")%>'></asp:Label></td>

                                                    <td id="tdUDF_ProfCategory" runat="server"><asp:Label ID="_lblUDF_ProfCategory" runat="server"><%#Eval("UDF_ProfCategory")%></asp:Label></td>

                                                    <td id="tdUDF_CPcode" runat="server"><asp:Label ID="_lblUDF_CPcode" runat="server" Text='<%#Eval("UDF_CPcode")%>'></asp:Label></td>

                                                    <td id="tdUDF_CLDepartment" runat="server"><asp:Label ID="_lblUDF_CLDepartment" runat="server" Text='<%#Eval("UDF_CLDepartment")%>'></asp:Label></td>

                                                    <td id="tdUDF_CAddress" runat="server"><asp:Label ID="_lblUDF_CAddress" runat="server" Text='<%#Eval("UDF_CAddress")%>'></asp:Label></td>

                                                    <td id="tdUDF_CLCompany" runat="server"><asp:Label ID="_lblUDF_CLCompany" runat="server"><%#Eval("UDF_CLCompany")%></asp:Label></td>

                                                    <td id="tdUDF_CCountry" runat="server"><asp:Label ID="_lblUDF_CCountry" runat="server" Text='<%#Eval("UDF_CCountry")%>'></asp:Label></td>

                                                    <td id="tdSupName" runat="server"><asp:Label ID="_lblSupName" runat="server" Text='<%#Eval("reg_SupervisorName")%>'></asp:Label></td>

                                                    <td id="tdSupDesignation" runat="server"><asp:Label ID="_lblSupDesignation" runat="server" Text='<%#Eval("reg_SupervisorDesignation")%>'></asp:Label></td>

                                                    <td id="tdSupContact" runat="server"><asp:Label ID="_lblSupContact" runat="server" Text='<%#Eval("reg_SupervisorContact")%>'></asp:Label></td>

                                                    <td id="tdSupEmail" runat="server"><asp:Label ID="_lblSupEmail" runat="server" Text='<%#Eval("reg_SupervisorEmail")%>'></asp:Label></td>
                                                </tr>
                                            <%--</table>--%>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                            </div>
                        </div>

                        <table width="100%">
                            <tr>
                                <td colspan="3" style="border-top:0px solid #C8C8C8;">&nbsp;</td>
                            </tr>
                        </table>
                    </div>

                    <br />
                    <div id="rcontent" runat="server" >
                        <h3 class="style1">Congress Selection</h3>
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8  col-xs-12 ConfItemPanel ">
                                <asp:Label runat="server" ID="lblShowOrderList"></asp:Label>
                            </div>
                            <div class="row">
                                <div class=" col-xs-12 col-md-offset-3 col-md-7 text-right">
                                    <table class="table">
                                        <tr>
                                            <td>Sub Total:</td>
                                            <td>
                                                <asp:Label runat="server" ID="lblSubTotalCurr" Text="SGD"></asp:Label>  <asp:Label runat="server" ID="lblSubTotal" Text="0.00"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trAdminFee" runat="server" visible="false">
                                            <td><asp:Label ID="lblAdminFeeText" runat="server" Text="4"></asp:Label>% Credit Card (Admin Fee):</td>
                                            <td>
                                                <asp:Label runat="server" ID="lblAdminFeeCurr" Text="SGD"></asp:Label>  <asp:Label runat="server" ID="lblAdminFee" Text="0.00"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trGST" runat="server" visible="false">
                                            <td><asp:Label ID="lblGSTFeeText" runat="server" Text="7"></asp:Label>% Goods & Service Tax (GST):</td>
                                            <td>
                                                <asp:Label runat="server" ID="lblGSTCurr" Text="SGD"></asp:Label>  <asp:Label runat="server" ID="lblGstFee" Text="0.00"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trTTAdminFee" runat="server" visible="false">
                                            <td><asp:Label ID="lblTTAdminFeeText" runat="server" Text="25"></asp:Label> Bank Transaction Charges (Telegraphic Transfer):</td>
                                            <td>
                                                <asp:Label runat="server" ID="lblTTCurr" Text="SGD"></asp:Label>  <asp:Label runat="server" ID="lblTTAdminFee" Text="0.00"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Grand Total:</td>
                                            <td>
                                                <asp:Label runat="server" ID="lblGrandCurr" Text="SGD"></asp:Label>
                                                <asp:Label runat="server" ID="lblGrandTotal" Text="0.00"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <asp:Panel runat="server" ID="PanelPartial" Visible="false">
                                Paartial : <asp:TextBox runat="server" ID="txtPartialAmount"></asp:TextBox>
                            </asp:Panel>
                        </div>
                        <br />
 
                        <table width="100%">
                            <tr>
                                <td colspan="3" style="border-top:0px solid #C8C8C8;">&nbsp;</td>
                            </tr>
                        </table>
 
                        <div class="clear"></div>
                        <div class="margtop">
                            <h3>&nbsp;</h3>
                            <p> <asp:Label ID="lblmethod" runat="server" Text="Label" Visible="False"></asp:Label></p>
                            <asp:HiddenField ID="hfpaymethod" runat="server" Value="0" />
                        </div>
                    </div>

                    <div id="divTerm" runat="server" visible="false">
                        <div class="clear"></div>
                        <table width="100%">
                            <tr>
                                <td colspan="3" style="border-top:1px solid #C8C8C8;">&nbsp;</td>
                            </tr>
                        </table>
                        <asp:Label ID="lblTerms" runat="server"></asp:Label>
                        <br />
                    </div>

                    <div id="divPaymentMethod" runat="server" visible="false">
                        <br />
                        <h3 class="style1" runat="server" id="lblPaymentModeTitle" >Mode of Payment</h3>
                        <div>
                            <table>
                                <tr id="trCreditCard" runat="server" visible="false">
                                    <td valign="top">
                                        <asp:RadioButton ID="rbCreditCard" runat="server" GroupName="Method" OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" Text="Credit Card" />
                                        <br />
                                        <asp:Label ID="lblCreditCard" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trTT" runat="server" visible="false">
                                    <td valign="top">
                                        <asp:RadioButton ID="rbTT" runat="server" GroupName="Method" OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" Text="Telegraphic Transfer" />
                                        <br />
                                        <asp:Label ID="lblTT" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trWaived" runat="server" visible="false">
                                    <td valign="top">
                                        <asp:RadioButton ID="rbWaived" runat="server" GroupName="Method" OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" Text="Waived" />
                                        <br />
                                        <asp:Label ID="lblWaived" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trCheque" runat="server" visible="false">
                                    <td valign="top">
                                        <asp:RadioButton ID="rbCheque" runat="server" GroupName="Method" OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" Text="Local Cheque" />
                                        <br />
                                        <asp:Label ID="lblCheque" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>