﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Payment;
using Corpit.Logging;
using Corpit.BackendMaster;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Configuration;

public partial class ConfirmationV : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();
    private static string[] checkingvnuShowName = new string[] { "VNU_AGRI", "VNU_HORTI" };
    private static string[] checkingSARCShowName = new string[] { "SARC" };
    private static string[] checkingOSEAShowName = new string[] { "OSEA 2018" };
    private static string oseaCongressSelectionHeader = "Conference Selection";
    private static string vnuCongressSelectionHeader = "Item Selection";
    private static string fjPublicCongressSelectionHeader = "Order Summary";
    private static string fjPublicFlowName = "FoodJapanPublic";
    private static string[] checkingMDAShowName = new string[] { "MFA 2018", "MMA 2018", "OSHA 2018" };

    #region Contact Person (Group)
    static string _GSalutation = "Salutation";
    static string _GFname = "FName";
    static string _GLname = "LName";
    static string _GDesignation = "Designation";
    static string _GDepartment = "Department";
    static string _GCompany = "Company";
    static string _GIndustry = "Industry";
    static string _GAddress1 = "Address1";
    static string _GAddress2 = "Address2";
    static string _GAddress3 = "Address3";
    static string _GCity = "City";
    static string _GState = "State";
    static string _GPostalCode = "Postal Code";
    static string _GCountry = "Country";
    static string _GRCountry = "RCountry";
    static string _GTelcc = "Telcc";
    static string _GTelac = "Telac";
    static string _GTel = "Tel";
    static string _GMobilecc = "Mobilecc";
    static string _GMobileac = "Mobileac";
    static string _GMobile = "Mobile";
    static string _GFaxcc = "Faxcc";
    static string _GFaxac = "Faxac";
    static string _GFax = "Fax";
    static string _GEmail = "Email";
    static string _GEmailConfirmation = "Email Confirmation";
    static string _GVisitDate = "Visit Date";
    static string _GVisitTime = "Visit Time";
    static string _GPassword = "Password";
    static string _GOtherSalutation = "Other Salutation";
    static string _GOtherDesignation = "Other Designation";
    static string _GOtherIndustry = "Other Industry";

    static string _GGender = "Gender";
    static string _GDOB = "DOB";
    static string _GAge = "Age";
    static string _GAdditional4 = "Additional4";
    static string _GAdditional5 = "Additional5";
    #endregion

    #region Company
    static string _CName = "Name";
    static string _CAddress1 = "Address1";
    static string _CAddress2 = "Address2";
    static string _CAddress3 = "Address3";
    static string _CCity = "City";
    static string _CState = "State";
    static string _CZipCode = "Zip Code";
    static string _CCountry = "Country";
    static string _CTelcc = "Telcc";
    static string _CTelac = "Telac";
    static string _CTel = "Tel";
    static string _CFaxcc = "Faxcc";
    static string _CFaxac = "Faxac";
    static string _CFax = "Fax";
    static string _CEmail = "Email";
    static string _CEmailConfirmation = "Email Confirmation";
    static string _CWebsite = "Website";
    static string _CAdditional1 = "Additional1";
    static string _CAdditional2 = "Additional2";
    static string _CAdditional3 = "Additional3";
    static string _CAdditional4 = "Additional4";
    static string _CAdditional5 = "Additional5";
    #endregion

    #region Delegate
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    #endregion

    public string galaDetails = "";
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                string groupid = string.Empty;
                string regno = string.Empty;
                groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                regno = cFun.DecryptValue(urlQuery.DelegateID);
                if (!String.IsNullOrEmpty(groupid))
                {
                    updateCurStep(urlQuery);//***edit on 23-3-2018

                    setDynamicForm(flowid, showid);
                    SiteSettings st = new SiteSettings(fn, showid);
                    if (st.hasterm == YesOrNo.Yes)
                    {
                        divTerm.Visible = true;
                        bindTermsCondition(showid);
                    }
                    else
                    {
                        divTerm.Visible = false;
                    }

                    InvoiceControler iControler = new InvoiceControler(fn);
                    bindPaymentMode(showid);
                    if (iControler.checkPaymentMethodUsed(showid))
                    {
                        divPaymentMethod.Visible = true;
                    }
                    else
                    {
                        divPaymentMethod.Visible = false;
                    }

                    bindPersonnalInfo(groupid, regno, showid);
                    bindpromo();

                    bindConferenceCalPrice(showid, urlQuery);

                    //*Congress selection visible
                    Double total = Convert.ToDouble(lblGrandTotal.Text);
                    //OrderControler oControler = new OrderControler(fn);
                    //List<Order> oList = oControler.GetAllPendingOrder(groupid, regno);

                    if (total > 0)
                    {
                        rcontent.Visible = true;
                        divPaymentMethod.Visible = true;
                    }
                    else
                    {
                        FlowControler Flw = new FlowControler(fn);
                        bool confExist = Flw.checkPageExist(flowid, SiteDefaultValue.constantConfName);
                        if (!confExist)
                        {
                            rcontent.Visible = false;
                            divPaymentMethod.Visible = false;
                        }
                        divPaymentMethod.Visible = false;// Sithu [19-Jun-2018] hide PaymethPanel if Total =0
                    }
                    //*

                    setEditButtonsVisibility();//***
                    setBackButtonVisibility();//***(added on 15-7-2019)

                    bindFlowNote(showid, urlQuery);//***added on 25-6-2018

                    ShowControler shwCtr = new ShowControler(fn);
                    Show shw = shwCtr.GetShow(showid);
                    if (checkingvnuShowName.Contains(shw.SHW_Name))
                    {
                        lblCongressSelectionHeader.Text = vnuCongressSelectionHeader;
                    }
                    else if (checkingOSEAShowName.Contains(shw.SHW_Name))
                    {
                        lblCongressSelectionHeader.Text = oseaCongressSelectionHeader;
                    }
                    else
                    {
                        FlowControler Flw = new FlowControler(fn);
                        FlowMaster fMaster = Flw.GetFlowMasterConfig(flowid);
                        if(fMaster.FlowName == fjPublicFlowName)
                        {
                            lblCongressSelectionHeader.Text = fjPublicCongressSelectionHeader;
                        }
                    }


                    insertLogFlowAction(groupid, regno, rlgobj.actview, urlQuery);
                }
                else
                {
                    Response.Redirect("DefaultRegIndex.aspx");
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region bindConferenceCalPrice
    private void bindConferenceCalPrice(string showid, FlowURLQuery urlQuery)
    {
        SiteSettings st = new SiteSettings(fn, showid);
        #region Conference
        string subTotal = Number.zeroDecimal.ToString();
        lblSubTotalCurr.Text = st.SiteCurrency;
        lblDiscountCurr.Text = st.SiteCurrency;
        lblAdminFeeCurr.Text = st.SiteCurrency;
        lblGSTCurr.Text = st.SiteCurrency;
        lblGrandCurr.Text = st.SiteCurrency;

        lblShowOrderList.Text = LoadOrderedList(urlQuery, ref subTotal);
        subTotal = cFun.FormatCurrency(subTotal.ToString());
        lblSubTotal.Text = subTotal;
        //lblGrandTotal.Text = subTotal;

        //***added by th on 22-7-2018 (for showing discount)
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        FlowControler fCtrl = new FlowControler(fn);
        FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
        if (fmaster != null)
        {
            if (fmaster.isDiscountPriceDisplayed)
            {
                RegDelegateObj rgd = new RegDelegateObj(fn);
                string delegateno = cFun.DecryptValue(urlQuery.DelegateID);
                Tuple<string, string> responseMsg = rgd.getDelegateDiscount(delegateno, subTotal, flowid, showid);
                if (!string.IsNullOrEmpty(responseMsg.Item1) && !string.IsNullOrEmpty(responseMsg.Item2))
                {
                    trDiscount.Visible = true;
                    lblDiscountText.Text = responseMsg.Item1;
                    lblDiscountPrice.Text = responseMsg.Item2;
                }
            }
        }
        //***added by th on 22-7-2018 (for showing discount)

        if (st.AllowPartialPayment == YesOrNo.Yes)
        {
            PanelPartial.Visible = true;
        }

        decimal adminfee = cFun.ParseDecimal(Number.Zero);
        decimal gstfee = cFun.ParseDecimal(Number.Zero);

        gstfee = cFun.ParseDecimal(st.gstfee);
        if (rbCreditCard.Checked == true)
        {
            adminfee = cFun.ParseDecimal(st.adminfee);

        }
        //Sithu 2018-Jun-12

        if (rbCreditManual.Checked == true)
        {
            adminfee = cFun.ParseDecimal(st.adminfee);

        }
        decimal ttadminfee = cFun.ParseDecimal(Number.Zero);
        if (rbTT.Checked == true)
        {
            ttadminfee = cFun.ParseDecimal(st.ttAdminFee);
        }

        decimal subTotalPrice = 0;
        decimal.TryParse(subTotal, out subTotalPrice);
        decimal TotalPrice = subTotalPrice;

        decimal realAdminFee = Math.Round(subTotalPrice * adminfee, 2);
        lblAdminFeeText.Text = (adminfee * 100).ToString();
        lblAdminFee.Text = realAdminFee.ToString();
        TotalPrice += realAdminFee;

        decimal realTTAdminFee = subTotalPrice == Number.zeroDecimal ? Number.zeroDecimal : ttadminfee;//Math.Round(TotalPrice * ttadminfee, 2);
        if (isSameWithHostCountry()) // [Sithu,2018-Jun-20] TT admin chareg=0 ,if Host Country and Delegate Country Same
            realTTAdminFee = Number.zeroDecimal;
        lblTTAdminFeeText.Text = (ttadminfee).ToString();
        lblTTAdminFee.Text = realTTAdminFee.ToString();
        TotalPrice += realTTAdminFee;

        decimal realGstFee = Math.Round(TotalPrice * gstfee, 2);
        if (st.isUsedTruncatedGSTFee == "1")//***added by th 20-7-2018 for the case like 1.0472 to get 1.04
        {
            realGstFee = Math.Truncate(100 * (TotalPrice * gstfee)) / 100;
        }

        if (!string.IsNullOrEmpty(st.ShowHostCountry) && st.ShowHostCountry != "0")
        {
            if (!isSameWithHostCountry())//***added by th on 20-7-2018 If Host Country and Delegate Coutry(Singapore) is not same , GST Charge=0
            {
                realGstFee = Number.zeroDecimal;
            }
        }

        lblGSTFeeText.Text = (gstfee * 100).ToString();
        lblGstFee.Text = realGstFee.ToString();
        TotalPrice += realGstFee;
        string strTotalPrice = String.Format("{0:f2}", TotalPrice);
        if (st.isUsedTruncatedGSTFee == "1")//***added by th 20-7-2018 for the case like 1.0472 to get 1.04
        {
            TotalPrice = Math.Round(TotalPrice, 1, MidpointRounding.AwayFromZero);
            Decimal tPrice = 0;
            Decimal.TryParse(TotalPrice.ToString(), out tPrice); //Convert.ToDecimal(TotalPrice);
            strTotalPrice= String.Format("{0:f2}", tPrice);
        }
        lblGrandTotal.Text = strTotalPrice;// TotalPrice.ToString();

        showFeeByPaymentMethod(showid);
        #endregion
    }
    #endregion

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region Set Dynamic Form (set div visibility and tr visibility dynamically (generate dynamic form) according to the settings of tb_Form table where form_type='G'(for group contact person) or  form_type='C'(for company) or  form_type='D'(for delegate))
    protected void setDynamicForm(string flowid, string showid)
    {
        DataSet ds = new DataSet();

        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;

        try
        {
            #region Contact Person (Group)
            ds = frmObj.getDynFormForGroup();

            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
            {
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GSalutation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGSalutation.Visible = true;
                        lblGSalutation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGSalutation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GFname)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGFName.Visible = true;
                        lblGFName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGFName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GLname)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGLName.Visible = true;
                        lblGLName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGLName.Visible = false;
                    }

                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GDesignation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGDesignation.Visible = true;
                        lblGDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGDesignation.Visible = false;
                    }

                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GDepartment)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGDepartment.Visible = true;
                        lblGDepartment.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGDepartment.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GCompany)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGCompany.Visible = true;
                        lblGCompany.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGCompany.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GIndustry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGIndustry.Visible = true;
                        lblGIndustry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGIndustry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAddress1)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAddress1.Visible = true;
                        lblGAddress1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGAddress1.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAddress2)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAddress2.Visible = true;
                        lblGAddress2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGAddress2.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAddress3)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAddress3.Visible = true;
                        lblGAddress3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGAddress3.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GCity)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGCity.Visible = true;
                        lblGCity.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGCity.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GState)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGState.Visible = true;
                        lblGState.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGState.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GPostalCode)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGPostalCode.Visible = true;
                        lblGPostalCode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGPostalCode.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGCountry.Visible = true;
                        lblGCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GRCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGRCountry.Visible = true;
                        lblGRCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGRCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GTel)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGTel.Visible = true;
                        lblGTel.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGTel.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GMobile)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGMobile.Visible = true;
                        lblGMobile.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGMobile.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GFax)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGFax.Visible = true;
                        lblGFax.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGFax.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GEmail)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGEmail.Visible = true;
                        lblGEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGEmail.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAge)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAge.Visible = true;
                        lblGAge.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGAge.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GDOB)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGDOB.Visible = true;
                        lblGDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGDOB.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GGender)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGGender.Visible = true;
                        lblGGender.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGGender.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GVisitDate)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGVisitDate.Visible = true;
                        lblGVisitDate.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGVisitDate.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GVisitTime)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGVisitTime.Visible = true;
                        lblGVisitTime.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGVisitTime.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GPassword)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGPassword.Visible = true;
                        lblGPassword.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGPassword.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAdditional4)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAdditional4.Visible = true;
                        lblGAdditional4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGAdditional4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAdditional5)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAdditional5.Visible = true;
                        lblGAdditional5.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGAdditional5.Visible = false;
                    }
                }
            }
            #endregion

            #region Company
            ds = frmObj.getDynFormForCompany();

            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
            {
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);


                    if (isshow == 1)
                    {
                        trCName.Visible = true;
                        lblCName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAddress1)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAddress1.Visible = true;
                        lblCAddress1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCAddress1.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAddress2)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAddress2.Visible = true;
                        lblCAddress2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCAddress2.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAddress3)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAddress3.Visible = true;
                        lblCAddress3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCAddress3.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CCity)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCCity.Visible = true;
                        lblCCity.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCCity.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CState)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCState.Visible = true;
                        lblCState.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCState.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CZipCode)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCZipcode.Visible = true;
                        lblCZipcode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCZipcode.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCCountry.Visible = true;
                        lblCCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CTel)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCTel.Visible = true;
                        lblCTel.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCTel.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CFax)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCFax.Visible = true;
                        lblCFax.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCFax.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CEmail)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCEmail.Visible = true;
                        lblCEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCEmail.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CWebsite)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCWebsite.Visible = true;
                        lblCWebsite.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCWebsite.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAdditional1)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAdditional1.Visible = true;
                        lblCAdditional1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCAdditional1.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAdditional2)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAdditional2.Visible = true;
                        lblCAdditional2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCAdditional2.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAdditional3)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAdditional3.Visible = true;
                        lblCAdditional3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCAdditional3.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAdditional4)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAdditional4.Visible = true;
                        lblCAdditional4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCAdditional4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAdditional5)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAdditional5.Visible = true;
                        lblCAdditional5.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCAdditional5.Visible = false;
                    }
                }
            }
            #endregion

            #region Delegate
            ds = frmObj.getDynFormForDelegate();

            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
            {
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);


                    if (isshow == 1)
                    {
                        trSalutation.Visible = true;
                        lblSal.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSalutation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trFName.Visible = true;
                        lblFName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trFName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trLName.Visible = true;
                        lblLName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trLName.Visible = false;
                    }

                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trOName.Visible = true;
                        lblOName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trOName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trPassno.Visible = true;
                        lblPassno.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trPassno.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trIsReg.Visible = true;
                        lblIsReg.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trIsReg.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trRegSpecific.Visible = true;
                        lblRegSpecific.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trRegSpecific.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trIDNo.Visible = true;
                        lblIDNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trIDNo.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDesignation.Visible = true;
                        lblDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDesignation.Visible = false;
                    }

                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trProfession.Visible = true;
                        lblProfession.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trProfession.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDept.Visible = true;
                        lblDept.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDept.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trOrg.Visible = true;
                        lblOrg.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trOrg.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trInstitution.Visible = true;
                        lblInstitution.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trInstitution.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress1.Visible = true;
                        lblAddress1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress1.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress2.Visible = true;
                        lblAddress2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress2.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress3.Visible = true;
                        lblAddress3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress3.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress4.Visible = true;
                        lblAddress4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCity.Visible = true;
                        lblCity.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCity.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trState.Visible = true;
                        lblState.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trState.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trPostal.Visible = true;
                        lblPostal.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trPostal.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCountry.Visible = true;
                        lblCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trRCountry.Visible = true;
                        lblRCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trRCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trTel.Visible = true;
                        lblTel.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trTel.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trMobile.Visible = true;
                        lblMobile.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trMobile.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trFax.Visible = true;
                        lblFax.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trFax.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trEmail.Visible = true;
                        lblEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trEmail.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAffiliation.Visible = true;
                        lblAffiliation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAffiliation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDietary.Visible = true;
                        lblDietary.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDietary.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trNationality.Visible = true;
                        lblNationality.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trNationality.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAge.Visible = true;
                        lblAge.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAge.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDOB.Visible = true;
                        lblDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDOB.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGender.Visible = true;
                        lblGender.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGender.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trMembershipNo.Visible = true;
                        lblMembershipNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trMembershipNo.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAdditional4.Visible = true;
                        lblAdditional4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAdditional4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAdditional5.Visible = true;
                        lblAdditional5.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAdditional5.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVName.Visible = true;
                        lblVName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVDOB.Visible = true;
                        lblVDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVDOB.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVPass.Visible = true;
                        lblVPass.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVPass.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVPassExpiry.Visible = true;
                        lblVPassExpiry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVPassExpiry.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVCountry.Visible = true;
                        lblVCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVCountry.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CName.Visible = true;
                        lblUDF_CName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CName.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_DelegateType.Visible = true;
                        lblUDF_DelegateType.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_DelegateType.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_ProfCategory.Visible = true;
                        lblUDF_ProfCategory.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_ProfCategory.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CPcode.Visible = true;
                        lblUDF_CPcode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CPcode.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CLDepartment.Visible = true;
                        lblUDF_CLDepartment.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CLDepartment.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CAddress.Visible = true;
                        lblUDF_CAddress.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CAddress.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CLCompany.Visible = true;
                        lblUDF_CLCompany.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CLCompany.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CCountry.Visible = true;
                        lblUDF_CCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CCountry.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupName.Visible = true;
                        lblSupName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupName.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupDesignation.Visible = true;
                        lblSupDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupDesignation.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupContact.Visible = true;
                        lblSupContact.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupContact.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupEmail.Visible = true;
                        lblSupEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupEmail.Visible = false;
                    }
                }
            }
            #endregion
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region bindTermsCondition (bind terms&conditions data from tb_Terms table to chkTerms control)
    private void bindTermsCondition(string showid)
    {
        try
        {
            #region Comment bind Terms on chkTerms
            //CommonDataObj cmdObj = new CommonDataObj(fn);
            //DataTable dt = cmdObj.getTerms();
            //if (dt.Rows.Count > 0)
            //{
            //    foreach (DataRow dr in dt.Rows)
            //    {
            //        ListItem item = new ListItem(dr["terms_Desc"].ToString(), dr["terms_ID"].ToString());
            //        chkTerms.Items.Add(item);
            //    }
            //}
            #endregion

            SiteSettings st = new SiteSettings(fn, showid);
            lblTerms.Text = Server.HtmlDecode(st.termsTemplate);
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region bindPaymentMode (bind payment methods data from ref_PaymentMethod table to rbPaymentMode control)
    private void bindPaymentMode(string showID)
    {
        try
        {
            CommonDataObj cmdObj = new CommonDataObj(fn);
            DataTable dt = cmdObj.getPaymentMethods(showID);
            if (dt.Rows.Count > 0)
            {
                SiteSettings st = new SiteSettings(fn, showID);
                foreach (DataRow dr in dt.Rows)
                {
                    string usedid = dr["method_usedid"].ToString();

                    if (usedid == ((int)PaymentType.CreditCard).ToString())
                    {
                        trCreditCard.Visible = true;
                        lblCreditCard.Text = Server.HtmlDecode(st.creditcardText);
                    }
                    if (usedid == ((int)PaymentType.CreditCardManual).ToString())
                    {
                        trCreditManual.Visible = true;
                        lblCreditCardManual.Text = Server.HtmlDecode(st.creditcardManualText);
                    }

                    if (usedid == ((int)PaymentType.TT).ToString())
                    {
                        trTT.Visible = true;
                        lblTT.Text = Server.HtmlDecode(st.ttText);
                    }
                    if (usedid == ((int)PaymentType.Waved).ToString())
                    {
                        trWaived.Visible = true;
                        lblWaived.Text = Server.HtmlDecode(st.waivedText);
                    }
                    if (usedid == ((int)PaymentType.Cheque).ToString())
                    {
                        trCheque.Visible = true;
                        lblCheque.Text = Server.HtmlDecode(st.chequeText);
                    }
                }
            }
            else
            {      // lblPaymentModeTitle.Style.Add("display","none");
                lblPaymentModeTitle.Visible = false;
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region bindPersonnalInfo (bind contact person(group) info, company info, delegate info to repective controls if those data exist for current RegGroupID)
    protected void bindPersonnalInfo(string groupID, string regno, string showid)
    {
        string userid = string.Empty;

        try
        {
            #region Contact Person Info (Group)
            DataTable dtG = new DataTable();
            RegGroupObj rgg = new RegGroupObj(fn);
            dtG = rgg.getRegGroupByID(groupID, showid);
            bool isGroupRegType = false;
            string currentCountry = "";
            if (dtG.Rows.Count != 0)
            {
                string reggroupid = dtG.Rows[0]["RegGroupID"].ToString();
                string rg_salutation = dtG.Rows[0]["RG_Salutation"].ToString();
                string rg_contactfname = dtG.Rows[0]["RG_ContactFName"].ToString();
                string rg_contactlname = dtG.Rows[0]["RG_ContactLName"].ToString();
                string rg_designation = dtG.Rows[0]["RG_Designation"].ToString();
                string rg_department = dtG.Rows[0]["RG_Department"].ToString();
                string rg_company = dtG.Rows[0]["RG_Company"].ToString();
                string rg_industry = dtG.Rows[0]["RG_Industry"].ToString();
                string rg_address1 = dtG.Rows[0]["RG_Address1"].ToString();
                string rg_address2 = dtG.Rows[0]["RG_Address2"].ToString();
                string rg_address3 = dtG.Rows[0]["RG_Address3"].ToString();
                string rg_city = dtG.Rows[0]["RG_City"].ToString();
                string rg_stateprovince = dtG.Rows[0]["RG_StateProvince"].ToString();
                string rg_postalcode = dtG.Rows[0]["RG_PostalCode"].ToString();
                string rg_country = dtG.Rows[0]["RG_Country"].ToString();
                string rg_rcountry = dtG.Rows[0]["RG_RCountry"].ToString();
                string rg_telcc = dtG.Rows[0]["RG_Telcc"].ToString();
                string rg_telac = dtG.Rows[0]["RG_Telac"].ToString();
                string rg_tel = dtG.Rows[0]["RG_Tel"].ToString();
                string rg_mobilecc = dtG.Rows[0]["RG_Mobilecc"].ToString();
                string rg_mobileac = dtG.Rows[0]["RG_Mobileac"].ToString();
                string rg_mobile = dtG.Rows[0]["RG_Mobile"].ToString();
                string rg_faxcc = dtG.Rows[0]["RG_Faxcc"].ToString();
                string rg_faxac = dtG.Rows[0]["RG_Faxac"].ToString();
                string rg_fax = dtG.Rows[0]["RG_Fax"].ToString();
                string rg_contactemail = dtG.Rows[0]["RG_ContactEmail"].ToString();
                string rg_remark = dtG.Rows[0]["RG_Remark"].ToString();
                string rg_type = dtG.Rows[0]["RG_Type"].ToString();
                string rg_remarkgupload = dtG.Rows[0]["RG_RemarkGUpload"].ToString();
                string rg_salother = dtG.Rows[0]["RG_SalOther"].ToString();
                string rg_designationother = dtG.Rows[0]["RG_DesignationOther"].ToString();
                string rg_industryothers = dtG.Rows[0]["RG_IndustryOthers"].ToString();
                string rg_visitdate = dtG.Rows[0]["RG_VisitDate"].ToString();
                string rg_visittime = dtG.Rows[0]["RG_VisitTime"].ToString();
                string rg_password = dtG.Rows[0]["RG_Password"].ToString();
                string rg_ismultiple = dtG.Rows[0]["RG_IsMultiple"].ToString();
                string rg_referralcode = dtG.Rows[0]["RG_ReferralCode"].ToString();
                string rg_isfromsales = dtG.Rows[0]["RG_IsFromSales"].ToString();
                string rg_issendemail = dtG.Rows[0]["RG_IsSendEmail"].ToString();
                string rg_indsendemail_status = dtG.Rows[0]["RG_IndSendEmail_Status"].ToString();
                string rg_createddate = dtG.Rows[0]["RG_CreatedDate"].ToString();
                string recycle = dtG.Rows[0]["recycle"].ToString();
                string rg_stage = dtG.Rows[0]["RG_Stage"].ToString();

                string rg_age = dtG.Rows[0]["RG_Age"].ToString();
                string rg_dob = dtG.Rows[0]["RG_DOB"].ToString();
                string rg_gender = dtG.Rows[0]["RG_Gender"].ToString();
                string rg_additional4 = dtG.Rows[0]["RG_Additional4"].ToString();
                string rg_additional5 = dtG.Rows[0]["RG_Additional5"].ToString();

                _lblGSalutation.Text = bindSalutation(rg_salutation, rg_salother);
                _lblGFName.Text = rg_contactfname;
                _lblGLName.Text = rg_contactlname;
                _lblGDesignation.Text = rg_designation;
                _lblGDepartment.Text = rg_department;
                _lblGCompany.Text = rg_company;
                _lblGIndustry.Text = bindIndustry(rg_industry) + rg_industryothers;
                _lblGAddress1.Text = rg_address1;
                _lblGAddress2.Text = rg_address2;
                _lblGAddress3.Text = rg_address3;
                _lblGCity.Text = rg_city;
                _lblGState.Text = rg_stateprovince;
                _lblGPostalCode.Text = rg_postalcode;
                _lblGCountry.Text = bindCountry(rg_country);
                _lblGRCountry.Text = bindCountry(rg_rcountry);
                _lblGTel.Text = bindPhoneNo_Group(rg_telcc, rg_telac, rg_tel, "Tel");
                _lblGMobile.Text = bindPhoneNo_Group(rg_mobilecc, rg_mobileac, rg_mobile, "Mob");
                _lblGFax.Text = bindPhoneNo_Group(rg_faxcc, rg_faxac, rg_fax, "Fax");
                _lblGEmail.Text = rg_contactemail;
                _lblGAge.Text = rg_age;
                _lblGDOB.Text = convertDateOfBirth(rg_dob);
                _lblGGender.Text = rg_gender;
                _lblGVisitDate.Text = rg_visitdate;
                _lblGVisitTime.Text = rg_visittime;
                _lblGPassword.Text = rg_password;
                _lblGAdditional4.Text = rg_additional4;
                _lblGAdditional5.Text = rg_additional5;

                //if current RG_IsMultiple='G', set divContactPerson visibility true
                if (rg_ismultiple == SiteFlowType.FLOW_GROUP)
                {
                    divContactPerson.Visible = true;
                    isGroupRegType = true;
                    currentCountry = rg_country; // [20-Jun-2018] Sithu , Current Country
                }
                else
                {
                    divContactPerson.Visible = false;

                }
            }
            #endregion

            #region Company Info
            DataTable dtC = new DataTable();
            RegCompanyObj rgc = new RegCompanyObj(fn);
            dtC = rgc.getRegCompanyByID(groupID, showid);
            if (dtC.Rows.Count != 0)
            {
                divCompany.Visible = true;

                string reggroupid = dtC.Rows[0]["RegGroupID"].ToString();
                string rc_name = dtC.Rows[0]["RC_Name"].ToString();
                string rc_address1 = dtC.Rows[0]["RC_Address1"].ToString();
                string rc_address2 = dtC.Rows[0]["RC_Address2"].ToString();
                string rc_address3 = dtC.Rows[0]["RC_Address3"].ToString();
                string rc_city = dtC.Rows[0]["RC_City"].ToString();
                string rc_state = dtC.Rows[0]["RC_State"].ToString();
                string rc_zipcode = dtC.Rows[0]["RC_ZipCode"].ToString();
                string rc_country = dtC.Rows[0]["RC_Country"].ToString();
                string rc_telcc = dtC.Rows[0]["RC_Telcc"].ToString();
                string rc_telac = dtC.Rows[0]["RC_Telac"].ToString();
                string rc_tel = dtC.Rows[0]["RC_Tel"].ToString();
                string rc_faxcc = dtC.Rows[0]["RC_Faxcc"].ToString();
                string rc_faxac = dtC.Rows[0]["RC_Faxac"].ToString();
                string rc_fax = dtC.Rows[0]["RC_Fax"].ToString();
                string rc_email = dtC.Rows[0]["RC_Email"].ToString();
                string rc_website = dtC.Rows[0]["RC_Website"].ToString();
                string rc_createddate = dtC.Rows[0]["RC_CreatedDate"].ToString();
                string recycle = dtC.Rows[0]["recycle"].ToString();
                string rc_stage = dtC.Rows[0]["RC_Stage"].ToString();

                string rc_additional1 = dtC.Rows[0]["RC_Additional1"].ToString();
                string rc_additional2 = dtC.Rows[0]["RC_Additional2"].ToString();
                string rc_additional3 = dtC.Rows[0]["RC_Additional3"].ToString();
                string rc_additional4 = dtC.Rows[0]["RC_Additional4"].ToString();
                string rc_additional5 = dtC.Rows[0]["RC_Additional5"].ToString();

                _lblCName.Text = rc_name;
                _lblCAddress1.Text = rc_address1;
                _lblCAddress2.Text = rc_address2;
                _lblCAddress3.Text = rc_address3;
                _lblCCity.Text = rc_city;
                _lblCState.Text = rc_state;
                _lblCZipcode.Text = rc_zipcode;
                _lblCCountry.Text = bindCountry(rc_country);
                _lblCTel.Text = bindPhoneNo_Company(rc_telcc, rc_telac, rc_tel, "Tel");
                _lblCFax.Text = bindPhoneNo_Company(rc_faxcc, rc_faxac, rc_fax, "Fax");
                _lblCEmail.Text = rc_email;
                _lblCWebsite.Text = rc_website;
                _lblCAdditional1.Text = rc_additional1;
                _lblCAdditional2.Text = rc_additional2;
                _lblCAdditional3.Text = rc_additional3;
                _lblCAdditional4.Text = rc_additional4;
                _lblCAdditional5.Text = rc_additional5;
            }
            else
            {
                divCompany.Visible = false;
            }
            #endregion

            #region Delegatge Info
            DataTable dtD = new DataTable();
            RegDelegateObj rgd = new RegDelegateObj(fn);
            dtD = rgd.getRegDelegateByGroupID(groupID, showid);

            if (dtD.Rows.Count > 0)
            {
                divDelegate.Visible = true;

                rptItem.DataSource = dtD;
                rptItem.DataBind();

                rptDelegateTable.DataSource = dtD;
                rptDelegateTable.DataBind();

                #region Comment
                //string reggroupid = dtD.Rows[0]["RegGroupID"].ToString();
                //string con_categoryid = dtD.Rows[0]["con_CategoryId"].ToString();
                //string reg_salutation = dtD.Rows[0]["reg_Salutation"].ToString();
                //string reg_fname = dtD.Rows[0]["reg_FName"].ToString();
                //string reg_lname = dtD.Rows[0]["reg_LName"].ToString();
                //string reg_oname = dtD.Rows[0]["reg_OName"].ToString();
                //string passno = dtD.Rows[0]["reg_PassNo"].ToString();
                //string reg_isreg = dtD.Rows[0]["reg_isReg"].ToString();
                //string reg_sgregistered = dtD.Rows[0]["reg_sgregistered"].ToString();
                //string reg_idno = dtD.Rows[0]["reg_IDno"].ToString();
                //string reg_staffid = dtD.Rows[0]["reg_staffid"].ToString();
                //string reg_designation = dtD.Rows[0]["reg_Designation"].ToString();
                //string reg_profession = dtD.Rows[0]["reg_Profession"].ToString();
                //string reg_jobtitle_alliedstu = dtD.Rows[0]["reg_Jobtitle_alliedstu"].ToString();
                //string reg_department = dtD.Rows[0]["reg_Department"].ToString();
                //string reg_organization = dtD.Rows[0]["reg_Organization"].ToString();
                //string reg_institution = dtD.Rows[0]["reg_Institution"].ToString();
                //string reg_address1 = dtD.Rows[0]["reg_Address1"].ToString();
                //string reg_address2 = dtD.Rows[0]["reg_Address2"].ToString();
                //string reg_address3 = dtD.Rows[0]["reg_Address3"].ToString();
                //string reg_address4 = dtD.Rows[0]["reg_Address4"].ToString();
                //string reg_city = dtD.Rows[0]["reg_City"].ToString();
                //string reg_state = dtD.Rows[0]["reg_State"].ToString();
                //string reg_postalcode = dtD.Rows[0]["reg_PostalCode"].ToString();
                //string reg_country = dtD.Rows[0]["reg_Country"].ToString();
                //string reg_rcountry = dtD.Rows[0]["reg_RCountry"].ToString();
                //string reg_telcc = dtD.Rows[0]["reg_Telcc"].ToString();
                //string reg_telac = dtD.Rows[0]["reg_Telac"].ToString();
                //string reg_tel = dtD.Rows[0]["reg_Tel"].ToString();
                //string reg_mobcc = dtD.Rows[0]["reg_Mobcc"].ToString();
                //string reg_mobac = dtD.Rows[0]["reg_Mobac"].ToString();
                //string reg_mobile = dtD.Rows[0]["reg_Mobile"].ToString();
                //string reg_faxcc = dtD.Rows[0]["reg_Faxcc"].ToString();
                //string reg_faxac = dtD.Rows[0]["reg_Faxac"].ToString();
                //string reg_fax = dtD.Rows[0]["reg_Fax"].ToString();
                //string reg_email = dtD.Rows[0]["reg_Email"].ToString();
                //string reg_affiliation = dtD.Rows[0]["reg_Affiliation"].ToString();
                //string reg_dietary = dtD.Rows[0]["reg_Dietary"].ToString();
                //string reg_nationality = dtD.Rows[0]["reg_Nationality"].ToString();
                //string reg_membershipno = dtD.Rows[0]["reg_Membershipno"].ToString();
                //string reg_vname = dtD.Rows[0]["reg_vName"].ToString();
                //string reg_vdob = dtD.Rows[0]["reg_vDOB"].ToString();
                //string reg_vpassno = dtD.Rows[0]["reg_vPassno"].ToString();
                //string reg_vpassexpiry = dtD.Rows[0]["reg_vPassexpiry"].ToString();
                //string reg_vpassissuedate = dtD.Rows[0]["reg_vIssueDate"].ToString();
                //string reg_vembarkation = dtD.Rows[0]["reg_vEmbarkation"].ToString();
                //string reg_varrivaldate = dtD.Rows[0]["reg_vArrivalDate"].ToString();
                //string reg_vcountry = dtD.Rows[0]["reg_vCountry"].ToString();
                //string udf_delegatetype = dtD.Rows[0]["UDF_DelegateType"].ToString();
                //string udf_profcategory = dtD.Rows[0]["UDF_ProfCategory"].ToString();
                //string udf_profcategoryother = dtD.Rows[0]["UDF_ProfCategoryOther"].ToString();
                //string udf_cname = dtD.Rows[0]["UDF_CName"].ToString();
                //string udf_cpcode = dtD.Rows[0]["UDF_CPcode"].ToString();
                //string udf_cldepartment = dtD.Rows[0]["UDF_CLDepartment"].ToString();
                //string udf_caddress = dtD.Rows[0]["UDF_CAddress"].ToString();
                //string udf_clcompany = dtD.Rows[0]["UDF_CLCompany"].ToString();
                //string udf_clcompanyother = dtD.Rows[0]["UDF_CLCompanyOther"].ToString();
                //string udf_ccountry = dtD.Rows[0]["UDF_CCountry"].ToString();
                //string reg_supervisorname = dtD.Rows[0]["reg_SupervisorName"].ToString();
                //string reg_supervisordesignation = dtD.Rows[0]["reg_SupervisorDesignation"].ToString();
                //string reg_supervisorcontact = dtD.Rows[0]["reg_SupervisorContact"].ToString();
                //string reg_supervisoremail = dtD.Rows[0]["reg_SupervisorEmail"].ToString();
                //string reg_salutationothers = dtD.Rows[0]["reg_SalutationOthers"].ToString();
                //string reg_otherprofession = dtD.Rows[0]["reg_otherProfession"].ToString();
                //string reg_otherdepartment = dtD.Rows[0]["reg_otherDepartment"].ToString();
                //string reg_otherorganization = dtD.Rows[0]["reg_otherOrganization"].ToString();
                //string reg_otherinstitution = dtD.Rows[0]["reg_otherInstitution"].ToString();
                //string reg_aemail = dtD.Rows[0]["reg_aemail"].ToString();
                //string reg_remark = dtD.Rows[0]["reg_remark"].ToString();
                //string reg_remarkgupload = dtD.Rows[0]["reg_remarkGUpload"].ToString();
                //string re_issms = dtD.Rows[0]["reg_isSMS"].ToString();
                //string reg_approvestatus = dtD.Rows[0]["reg_approveStatus"].ToString();
                //string reg_datecreated = dtD.Rows[0]["reg_datecreated"].ToString();
                //string recycle = dtD.Rows[0]["recycle"].ToString();
                //string reg_stage = dtD.Rows[0]["reg_Stage"].ToString();

                //string reg_age = dtD.Rows[0]["reg_Age"].ToString();
                //string reg_dob = dtD.Rows[0]["reg_DOB"].ToString();
                //string reg_gender = dtD.Rows[0]["reg_Gender"].ToString();
                //string reg_additional4 = dtD.Rows[0]["reg_Additional4"].ToString();
                //string reg_additional5 = dtD.Rows[0]["reg_Additional5"].ToString();
                #endregion
            }
            else
            {
                divDelegate.Visible = false;
            }
            #endregion

            #region checkCountry
            if (!isGroupRegType)
            {
                RegDelegateObj rg = new RegDelegateObj(fn); // [20-Jun-2018] Sithu , Current Country
                DataTable rGate = rg.getRegDelegateByID(regno, showid);
                if (rGate.Rows.Count > 0)
                    currentCountry = rGate.Rows[0]["reg_Country"].ToString();
            }
            SiteSettings sSetting = new SiteSettings(fn, showid);
            if (sSetting.ShowHostCountry == currentCountry)
                regCountrySameWithShowCountry.Text = "1";
            else
                regCountrySameWithShowCountry.Text = "0";
            #endregion
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region rptitemdatabound (set repeater item cells' visibility dynamically according to the settings of tb_Form table where form_type='D')
    protected void rptitemdatabound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                // This event is raised for the header, the footer, separators, and items.
                // Execute the following logic for Items and Alternating Items.
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.HtmlControls.HtmlTableCell tdSalutation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSalutation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdFName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdFName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdLName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdLName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdOName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdOName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdPassno = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPassno");
                    System.Web.UI.HtmlControls.HtmlTableCell tdIsReg = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdIsReg");
                    System.Web.UI.HtmlControls.HtmlTableCell tdRegSpecific = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdRegSpecific");
                    System.Web.UI.HtmlControls.HtmlTableCell tdIDNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdIDNo");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDesignation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDesignation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdProfession = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdProfession");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDept = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDept");
                    System.Web.UI.HtmlControls.HtmlTableCell tdOrg = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdOrg");
                    System.Web.UI.HtmlControls.HtmlTableCell tdInstitution = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdInstitution");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress1 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress1");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress2 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress2");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress3 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress3");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress4 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress4");
                    System.Web.UI.HtmlControls.HtmlTableCell tdCity = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCity");
                    System.Web.UI.HtmlControls.HtmlTableCell tdState = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdState");
                    System.Web.UI.HtmlControls.HtmlTableCell tdPostal = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPostal");
                    System.Web.UI.HtmlControls.HtmlTableCell tdCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdRCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdRCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdTel = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdTel");
                    System.Web.UI.HtmlControls.HtmlTableCell tdMobile = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdMobile");
                    System.Web.UI.HtmlControls.HtmlTableCell tdFax = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdFax");
                    System.Web.UI.HtmlControls.HtmlTableCell tdEmail = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdEmail");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAffiliation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAffiliation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDietary = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDietary");
                    System.Web.UI.HtmlControls.HtmlTableCell tdNationality = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdNationality");

                    System.Web.UI.HtmlControls.HtmlTableCell tdAge = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAge");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDOB = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDOB");
                    System.Web.UI.HtmlControls.HtmlTableCell tdGender = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdGender");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAdditional4 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAdditional4");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAdditional5 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAdditional5");

                    System.Web.UI.HtmlControls.HtmlTableCell tdMembershipNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdMembershipNo");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVDOB = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVDOB");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVPass = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVPass");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVPassExpiry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVPassExpiry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_DelegateType = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_DelegateType");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_ProfCategory = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_ProfCategory");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CPcode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CPcode");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CLDepartment = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CLDepartment");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CAddress = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CAddress");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CLCompany = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CLCompany");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupDesignation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupDesignation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupContact = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupContact");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupEmail = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupEmail");

                    DataSet ds = new DataSet();
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    ds = frmObj.getDynFormForDelegate();

                    for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                    {
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);


                            if (isshow == 1)
                            {
                                tdSalutation.Visible = true;
                            }
                            else
                            {
                                tdSalutation.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdFName.Visible = true;
                            }
                            else
                            {
                                tdFName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdLName.Visible = true;

                            }
                            else
                            {
                                tdLName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdOName.Visible = true;
                            }
                            else
                            {
                                tdOName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdPassno.Visible = true;
                            }
                            else
                            {
                                tdPassno.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdIsReg.Visible = true;
                            }
                            else
                            {
                                tdIsReg.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdRegSpecific.Visible = true;
                            }
                            else
                            {
                                tdRegSpecific.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdIDNo.Visible = true;
                            }
                            else
                            {
                                tdIDNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDesignation.Visible = true;
                            }
                            else
                            {
                                tdDesignation.Visible = false;
                            }

                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdProfession.Visible = true;
                            }
                            else
                            {
                                tdProfession.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDept.Visible = true;
                            }
                            else
                            {
                                tdDept.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdOrg.Visible = true;
                            }
                            else
                            {
                                tdOrg.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdInstitution.Visible = true;
                            }
                            else
                            {
                                tdInstitution.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress1.Visible = true;
                            }
                            else
                            {
                                tdAddress1.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress2.Visible = true;
                            }
                            else
                            {
                                tdAddress2.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress3.Visible = true;
                            }
                            else
                            {
                                tdAddress3.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress4.Visible = true;
                            }
                            else
                            {
                                tdAddress4.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdCity.Visible = true;
                            }
                            else
                            {
                                tdCity.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdState.Visible = true;
                            }
                            else
                            {
                                tdState.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdPostal.Visible = true;
                            }
                            else
                            {
                                tdPostal.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdCountry.Visible = true;
                            }
                            else
                            {
                                tdCountry.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdRCountry.Visible = true;
                            }
                            else
                            {
                                tdRCountry.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdTel.Visible = true;
                            }
                            else
                            {
                                tdTel.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdMobile.Visible = true;
                            }
                            else
                            {
                                tdMobile.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdFax.Visible = true;
                            }
                            else
                            {
                                tdFax.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdEmail.Visible = true;
                            }
                            else
                            {
                                tdEmail.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAffiliation.Visible = true;
                            }
                            else
                            {
                                tdAffiliation.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDietary.Visible = true;
                            }
                            else
                            {
                                tdDietary.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdNationality.Visible = true;
                            }
                            else
                            {
                                tdNationality.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAge.Visible = true;
                            }
                            else
                            {
                                tdAge.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDOB.Visible = true;
                            }
                            else
                            {
                                tdDOB.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdGender.Visible = true;
                            }
                            else
                            {
                                tdGender.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAdditional4.Visible = true;
                            }
                            else
                            {
                                tdAdditional4.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAdditional5.Visible = true;
                            }
                            else
                            {
                                tdAdditional5.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdMembershipNo.Visible = true;
                            }
                            else
                            {
                                tdMembershipNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVName.Visible = true;
                            }
                            else
                            {
                                tdVName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVDOB.Visible = true;
                            }
                            else
                            {
                                tdVDOB.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVPass.Visible = true;
                            }
                            else
                            {
                                tdVPass.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVPassExpiry.Visible = true;
                            }
                            else
                            {
                                tdVPassExpiry.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVCountry.Visible = true;
                            }
                            else
                            {
                                tdVCountry.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CName.Visible = true;
                            }
                            else
                            {
                                tdUDF_CName.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_DelegateType.Visible = true;
                            }
                            else
                            {
                                tdUDF_DelegateType.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_ProfCategory.Visible = true;
                            }
                            else
                            {
                                tdUDF_ProfCategory.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CPcode.Visible = true;
                            }
                            else
                            {
                                tdUDF_CPcode.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CLDepartment.Visible = true;
                            }
                            else
                            {
                                tdUDF_CLDepartment.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CAddress.Visible = true;
                            }
                            else
                            {
                                tdUDF_CAddress.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CLCompany.Visible = true;
                            }
                            else
                            {
                                tdUDF_CLCompany.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CCountry.Visible = true;
                            }
                            else
                            {
                                tdUDF_CCountry.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupName.Visible = true;
                            }
                            else
                            {
                                tdSupName.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupDesignation.Visible = true;
                            }
                            else
                            {
                                tdSupDesignation.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupContact.Visible = true;
                            }
                            else
                            {
                                tdSupContact.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupEmail.Visible = true;
                            }
                            else
                            {
                                tdSupEmail.Visible = false;
                            }
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region rptDelegateTable_ItemDataBound (set repeater item cells' visibility dynamically according to the settings of tb_Form table where form_type='D')
    protected void rptDelegateTable_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                // This event is raised for the header, the footer, separators, and items.
                // Execute the following logic for Items and Alternating Items.
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    #region table rows
                    HtmlGenericControl trSrNo = (HtmlGenericControl)e.Item.FindControl("trSrNo");
                    HtmlGenericControl trDSalutation = (HtmlGenericControl)e.Item.FindControl("trDSalutation");
                    HtmlGenericControl trDFName = (HtmlGenericControl)e.Item.FindControl("trDFName");
                    HtmlGenericControl trDLName = (HtmlGenericControl)e.Item.FindControl("trDLName");
                    HtmlGenericControl trDOName = (HtmlGenericControl)e.Item.FindControl("trDOName");
                    HtmlGenericControl trDPassno = (HtmlGenericControl)e.Item.FindControl("trDPassno");
                    HtmlGenericControl trDIsReg = (HtmlGenericControl)e.Item.FindControl("trDIsReg");
                    HtmlGenericControl trDRegSpecific = (HtmlGenericControl)e.Item.FindControl("trDRegSpecific");
                    HtmlGenericControl trDIDNo = (HtmlGenericControl)e.Item.FindControl("trDIDNo");
                    HtmlGenericControl trDDesignation = (HtmlGenericControl)e.Item.FindControl("trDDesignation");
                    HtmlGenericControl trDProfession = (HtmlGenericControl)e.Item.FindControl("trDProfession");
                    HtmlGenericControl trDDept = (HtmlGenericControl)e.Item.FindControl("trDDept");
                    HtmlGenericControl trDOrg = (HtmlGenericControl)e.Item.FindControl("trDOrg");
                    HtmlGenericControl trDInstitution = (HtmlGenericControl)e.Item.FindControl("trDInstitution");
                    HtmlGenericControl trDAddress1 = (HtmlGenericControl)e.Item.FindControl("trDAddress1");
                    HtmlGenericControl trDAddress2 = (HtmlGenericControl)e.Item.FindControl("trDAddress2");
                    HtmlGenericControl trDAddress3 = (HtmlGenericControl)e.Item.FindControl("trDAddress3");
                    HtmlGenericControl trDAddress4 = (HtmlGenericControl)e.Item.FindControl("trDAddress4");
                    HtmlGenericControl trDCity = (HtmlGenericControl)e.Item.FindControl("trDCity");
                    HtmlGenericControl trDState = (HtmlGenericControl)e.Item.FindControl("trDState");
                    HtmlGenericControl trDPostal = (HtmlGenericControl)e.Item.FindControl("trDPostal");
                    HtmlGenericControl trDCountry = (HtmlGenericControl)e.Item.FindControl("trDCountry");
                    HtmlGenericControl trDRCountry = (HtmlGenericControl)e.Item.FindControl("trDRCountry");
                    HtmlGenericControl trDTel = (HtmlGenericControl)e.Item.FindControl("trDTel");
                    HtmlGenericControl trDMobile = (HtmlGenericControl)e.Item.FindControl("trDMobile");
                    HtmlGenericControl trDFax = (HtmlGenericControl)e.Item.FindControl("trDFax");
                    HtmlGenericControl trDEmail = (HtmlGenericControl)e.Item.FindControl("trDEmail");
                    HtmlGenericControl trDAffiliation = (HtmlGenericControl)e.Item.FindControl("trDAffiliation");
                    HtmlGenericControl trDDietary = (HtmlGenericControl)e.Item.FindControl("trDDietary");
                    HtmlGenericControl trDNationality = (HtmlGenericControl)e.Item.FindControl("trDNationality");

                    HtmlGenericControl trDAge = (HtmlGenericControl)e.Item.FindControl("trDAge");
                    HtmlGenericControl trDDOB = (HtmlGenericControl)e.Item.FindControl("trDDOB");
                    HtmlGenericControl trDGender = (HtmlGenericControl)e.Item.FindControl("trDGender");
                    HtmlGenericControl trDAdditional4 = (HtmlGenericControl)e.Item.FindControl("trDAdditional4");
                    HtmlGenericControl trDAdditional5 = (HtmlGenericControl)e.Item.FindControl("trDAdditional5");

                    HtmlGenericControl trDMembershipNo = (HtmlGenericControl)e.Item.FindControl("trDMembershipNo");
                    HtmlGenericControl trDVName = (HtmlGenericControl)e.Item.FindControl("trDVName");
                    HtmlGenericControl trDVDOB = (HtmlGenericControl)e.Item.FindControl("trDVDOB");
                    HtmlGenericControl trDVPass = (HtmlGenericControl)e.Item.FindControl("trDVPass");
                    HtmlGenericControl trDVPassExpiry = (HtmlGenericControl)e.Item.FindControl("trDVPassExpiry");
                    HtmlGenericControl trDVCountry = (HtmlGenericControl)e.Item.FindControl("trDVCountry");
                    HtmlGenericControl trDUDF_CName = (HtmlGenericControl)e.Item.FindControl("trDUDF_CName");
                    HtmlGenericControl trDUDF_DelegateType = (HtmlGenericControl)e.Item.FindControl("trDUDF_DelegateType");
                    HtmlGenericControl trDUDF_ProfCategory = (HtmlGenericControl)e.Item.FindControl("trDUDF_ProfCategory");
                    HtmlGenericControl trDUDF_CPcode = (HtmlGenericControl)e.Item.FindControl("trDUDF_CPcode");
                    HtmlGenericControl trDUDF_CLDepartment = (HtmlGenericControl)e.Item.FindControl("trDUDF_CLDepartment");
                    HtmlGenericControl trDUDF_CAddress = (HtmlGenericControl)e.Item.FindControl("trDUDF_CAddress");
                    HtmlGenericControl trDUDF_CLCompany = (HtmlGenericControl)e.Item.FindControl("trDUDF_CLCompany");
                    HtmlGenericControl trDUDF_CCountry = (HtmlGenericControl)e.Item.FindControl("trDUDF_CCountry");
                    HtmlGenericControl trDSupName = (HtmlGenericControl)e.Item.FindControl("trDSupName");
                    HtmlGenericControl trDSupDesignation = (HtmlGenericControl)e.Item.FindControl("trDSupDesignation");
                    HtmlGenericControl trDSupContact = (HtmlGenericControl)e.Item.FindControl("trDSupContact");
                    HtmlGenericControl trDSupEmail = (HtmlGenericControl)e.Item.FindControl("trDSupEmail");

                    HtmlGenericControl trEdit = (HtmlGenericControl)e.Item.FindControl("trEdit");

                    HtmlGenericControl trPromoCode = (HtmlGenericControl)e.Item.FindControl("trPromoCode");
                    #endregion

                    #region labels
                    Label lblDSal = (Label)e.Item.FindControl("lblDSal");
                    Label lblDFName = (Label)e.Item.FindControl("lblDFName");
                    Label lblDLName = (Label)e.Item.FindControl("lblDLName");
                    Label lblDOName = (Label)e.Item.FindControl("lblDOName");
                    Label lblDPassno = (Label)e.Item.FindControl("lblDPassno");
                    Label lblDIsReg = (Label)e.Item.FindControl("lblDIsReg");
                    Label lblDRegSpecific = (Label)e.Item.FindControl("lblDRegSpecific");
                    Label lblDIDNo = (Label)e.Item.FindControl("lblDIDNo");
                    Label lblDDesignation = (Label)e.Item.FindControl("lblDDesignation");
                    Label lblDProfession = (Label)e.Item.FindControl("lblDProfession");
                    Label lblDDept = (Label)e.Item.FindControl("lblDDept");
                    Label lblDOrg = (Label)e.Item.FindControl("lblDOrg");
                    Label lblDInstitution = (Label)e.Item.FindControl("lblDInstitution");
                    Label lblDAddress1 = (Label)e.Item.FindControl("lblDAddress1");
                    Label lblDAddress2 = (Label)e.Item.FindControl("lblDAddress2");
                    Label lblDAddress3 = (Label)e.Item.FindControl("lblDAddress3");
                    Label lblDAddress4 = (Label)e.Item.FindControl("lblDAddress4");
                    Label lblDCity = (Label)e.Item.FindControl("lblDCity");
                    Label lblDState = (Label)e.Item.FindControl("lblDState");
                    Label lblDPostal = (Label)e.Item.FindControl("lblDPostal");
                    Label lblDCountry = (Label)e.Item.FindControl("lblDCountry");
                    Label lblDRCountry = (Label)e.Item.FindControl("lblDRCountry");
                    Label lblDTel = (Label)e.Item.FindControl("lblDTel");
                    Label lblDMobile = (Label)e.Item.FindControl("lblDMobile");
                    Label lblDFax = (Label)e.Item.FindControl("lblDFax");
                    Label lblDEmail = (Label)e.Item.FindControl("lblDEmail");
                    Label lblDAffiliation = (Label)e.Item.FindControl("lblDAffiliation");
                    Label lblDDietary = (Label)e.Item.FindControl("lblDDietary");
                    Label lblDNationality = (Label)e.Item.FindControl("lblDNationality");

                    Label lblDAge = (Label)e.Item.FindControl("lblDAge");
                    Label lblDDOB = (Label)e.Item.FindControl("lblDDOB");
                    Label lblDGender = (Label)e.Item.FindControl("lblDGender");
                    Label lblDAdditional4 = (Label)e.Item.FindControl("lblDAdditional4");
                    Label lblDAdditional5 = (Label)e.Item.FindControl("lblDAdditional5");

                    Label lblDMembershipNo = (Label)e.Item.FindControl("lblDMembershipNo");
                    Label lblDVName = (Label)e.Item.FindControl("lblDVName");
                    Label lblDVDOB = (Label)e.Item.FindControl("lblDVDOB");
                    Label lblDVPass = (Label)e.Item.FindControl("lblDVPass");
                    Label lblDVPassExpiry = (Label)e.Item.FindControl("lblDVPassExpiry");
                    Label lblDVCountry = (Label)e.Item.FindControl("lblDVCountry");
                    Label lblDUDF_CName = (Label)e.Item.FindControl("lblDUDF_CName");
                    Label lblDUDF_DelegateType = (Label)e.Item.FindControl("lblDUDF_DelegateType");
                    Label lblDUDF_ProfCategory = (Label)e.Item.FindControl("lblDUDF_ProfCategory");
                    Label lblDUDF_CPcode = (Label)e.Item.FindControl("lblDUDF_CPcode");
                    Label lblDUDF_CLDepartment = (Label)e.Item.FindControl("lblDUDF_CLDepartment");
                    Label lblDUDF_CAddress = (Label)e.Item.FindControl("lblDUDF_CAddress");
                    Label lblDUDF_CLCompany = (Label)e.Item.FindControl("lblDUDF_CLCompany");
                    Label lblDUDF_CCountry = (Label)e.Item.FindControl("lblDUDF_CCountry");
                    Label lblDSupName = (Label)e.Item.FindControl("lblDSupName");
                    Label lblDSupDesignation = (Label)e.Item.FindControl("lblDSupDesignation");
                    Label lblDSupContact = (Label)e.Item.FindControl("lblDSupContact");
                    Label lblDSupEmail = (Label)e.Item.FindControl("lblDSupEmail");

                    Label lblDPromoCode = (Label)e.Item.FindControl("lblDPromoCode");

                    ShowControler shwCtr = new ShowControler(fn);
                    Show shw = shwCtr.GetShow(showid);
                    if (checkingOSEAShowName.Contains(shw.SHW_Name))
                    {
                        Label _lblDProfession = (Label)e.Item.FindControl("_lblDProfession");
                        Label _lblDOProfession = (Label)e.Item.FindControl("_lblDOProfession");
                        _lblDProfession.Visible = false;
                        _lblDOProfession.Visible = true;
                    }
                    #endregion

                    FlowControler fCtrl = new FlowControler(fn);
                    FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
                    if (fmaster != null)
                    {
                        if (fmaster.FlowType == SiteFlowType.FLOW_GROUP)
                        {
                            trSrNo.Visible = true;

                            if (fmaster.isMemberDelegateEditStep > 0)
                            {
                                trEdit.Visible = true;//*
                            }
                        }
                        else
                        {
                            trSrNo.Visible = false;
                            trEdit.Visible = false;//*
                        }
                    }

                    //***added by th on 13-7-2018
                    CategoryClass catClass = new CategoryClass();
                    string type = fmaster.FlowCategoryConfigType;
                    if ((type == catClass.flowcat_ByPromoCode.ToString() || type == catClass.flowcat_ByBusinessLogicWithPromo.ToString()) && fmaster.isPromoEnabled)//According to Business Logic (Customize) *Like SHBC
                    {
                        trPromoCode.Visible = true;
                    }
                    else
                    {
                        trPromoCode.Visible = false;

                        if (type == catClass.flowcat_ByPromoCode.ToString())
                        {
                            trPromoCode.Visible = true;
                            if (checkingMDAShowName.Contains(shw.SHW_Name))
                            {
                                lblDPromoCode.Text = "Promo Code";
                            }
                        }
                    }
                    //***added by th on 13-7-2018

                    DataSet ds = new DataSet();
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    ds = frmObj.getDynFormForDelegate();

                    #region Column Visibility
                    for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                    {
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);


                            if (isshow == 1)
                            {
                                trDSalutation.Visible = true;
                                lblDSal.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDSalutation.Visible = false;
                                lblDSal.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDFName.Visible = true;
                                lblDFName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDFName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDLName.Visible = true;
                                lblDLName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();

                            }
                            else
                            {
                                trDLName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDOName.Visible = true;
                                lblDOName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDOName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDPassno.Visible = true;
                                lblDPassno.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDPassno.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDIsReg.Visible = true;
                                lblDIsReg.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDIsReg.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDRegSpecific.Visible = true;
                                lblDRegSpecific.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDRegSpecific.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDIDNo.Visible = true;
                                lblDIDNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDIDNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDDesignation.Visible = true;
                                lblDDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDDesignation.Visible = false;
                            }

                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDProfession.Visible = true;
                                lblDProfession.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDProfession.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDDept.Visible = true;
                                lblDDept.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDDept.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDOrg.Visible = true;
                                lblDOrg.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDOrg.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDInstitution.Visible = true;
                                lblDInstitution.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDInstitution.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDAddress1.Visible = true;
                                lblDAddress1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDAddress1.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDAddress2.Visible = true;
                                lblDAddress2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDAddress2.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDAddress3.Visible = true;
                                lblDAddress3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDAddress3.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDAddress4.Visible = true;
                                lblDAddress4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDAddress4.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDCity.Visible = true;
                                lblDCity.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDCity.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDState.Visible = true;
                                lblDState.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDState.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDPostal.Visible = true;
                                lblDPostal.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDPostal.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDCountry.Visible = true;
                                lblDCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDCountry.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDRCountry.Visible = true;
                                lblDRCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDRCountry.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDTel.Visible = true;
                                lblDTel.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDTel.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDMobile.Visible = true;
                                lblDMobile.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDMobile.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDFax.Visible = true;
                                lblDFax.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDFax.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDEmail.Visible = true;
                                lblDEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDEmail.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDAffiliation.Visible = true;
                                lblDAffiliation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDAffiliation.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDDietary.Visible = true;
                                lblDDietary.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDDietary.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDNationality.Visible = true;
                                lblDNationality.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDNationality.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDAge.Visible = true;
                                lblDAge.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDAge.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDDOB.Visible = true;
                                lblDDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDDOB.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDGender.Visible = true;
                                lblDGender.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDGender.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDAdditional4.Visible = true;
                                lblDAdditional4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDAdditional4.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDAdditional5.Visible = true;
                                lblDAdditional5.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDAdditional5.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDMembershipNo.Visible = true;
                                lblDMembershipNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDMembershipNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDVName.Visible = true;
                                lblDVName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDVName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDVDOB.Visible = true;
                                lblDVDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDVDOB.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDVPass.Visible = true;
                                lblDVPass.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDVPass.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDVPassExpiry.Visible = true;
                                lblDVPassExpiry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDVPassExpiry.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDVCountry.Visible = true;
                                lblDVCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDVCountry.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDUDF_CName.Visible = true;
                                lblDUDF_CName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDUDF_CName.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDUDF_DelegateType.Visible = true;
                                lblDUDF_DelegateType.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDUDF_DelegateType.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDUDF_ProfCategory.Visible = true;
                                lblDUDF_ProfCategory.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDUDF_ProfCategory.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDUDF_CPcode.Visible = true;
                                lblDUDF_CPcode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDUDF_CPcode.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDUDF_CLDepartment.Visible = true;
                                lblDUDF_CLDepartment.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDUDF_CLDepartment.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDUDF_CAddress.Visible = true;
                                lblDUDF_CAddress.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDUDF_CAddress.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDUDF_CLCompany.Visible = true;
                                lblDUDF_CLCompany.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDUDF_CLCompany.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDUDF_CCountry.Visible = true;
                                lblDUDF_CCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDUDF_CCountry.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDSupName.Visible = true;
                                lblDSupName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDSupName.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDSupDesignation.Visible = true;
                                lblDSupDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDSupDesignation.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDSupContact.Visible = true;
                                lblDSupContact.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDSupContact.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                trDSupEmail.Visible = true;
                                lblDSupEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                            }
                            else
                            {
                                trDSupEmail.Visible = false;
                            }
                        }
                    }
                    #endregion
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region Conference (GetOrderedListWithTemplate)

    #region Order
    private string LoadOrderedList(FlowURLQuery urlQuery, ref string SubTotal)
    {
        string template = string.Empty;
        try
        {
            OrderControler oControl = new OrderControler(fn);
            template = oControl.GetOrderedListWithTemplate(urlQuery, ref SubTotal);
        }
        catch (Exception ex)
        { }

        return template;
    }
    #endregion

    #endregion

    #region NextClick (create invoice for all pending order list into tb_invoice table & get next route and redirect to next page according to the site flow settings (tb_site_flow table) or redirect to the payment link)
    protected void NextClick(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string flwID = cFun.DecryptValue(urlQuery.FlowID);
        string GroupRegID = cFun.DecryptValue(urlQuery.GoupRegID);
        string DelegateID = cFun.DecryptValue(urlQuery.DelegateID);
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);

        Boolean isvalidpage = isValid(showid, urlQuery);
        if (isvalidpage)
        {
            FlowControler fCon = new FlowControler(fn);
            FlowMaster flwMaster = fCon.GetFlowMasterConfig(flwID);
            string confSkip = flwMaster.FlowConfSkip;

            try
            {
                StatusSettings stuSettings = new StatusSettings(fn);

                Double total = Convert.ToDouble(lblGrandTotal.Text);

                InvoiceControler iControler = new InvoiceControler(fn);
                OrderControler oControler = new OrderControler(fn);
                string invStatus = stuSettings.Pending.ToString();
                //string PayType = ((int)PaymentType.Waved).ToString();
                List<Order> oList = oControler.GetAllPendingOrder(GroupRegID, DelegateID);

                saveRegAdditional(showid, DelegateID, urlQuery);//***added on 25-6-2018

                string emailaddress = cFun.EncryptValue(getRegistrantEmail(DelegateID, GroupRegID, flwID, showid));//***added on 19-11-2018

                int payType = 0;
                if (total > 0)
                {
                    if (iControler.checkPaymentMethodUsed(showid))//*check the show use the payment method
                    {
                        #region payment method used and buy conference
                        if (rbCreditCard.Checked == true || rbTT.Checked == true || rbWaived.Checked == true || rbCheque.Checked == true || rbCreditManual.Checked == true)
                        {
                            payType = Convert.ToInt32(Number.Zero);
                            if (rbCreditCard.Checked == true)
                            {
                                payType = (int)PaymentType.CreditCard;
                            }
                            else if (rbTT.Checked == true)
                            {
                                payType = (int)PaymentType.TT;
                            }
                            else if (rbWaived.Checked == true)
                            {
                                payType = (int)PaymentType.Waved;
                            }
                            else if (rbCheque.Checked == true)
                            {
                                payType = (int)PaymentType.Cheque;
                            }
                            else if (rbCreditManual.Checked == true)
                            {
                                payType = (int)PaymentType.CreditCardManual;
                            }
                            if (oList.Count > 0)
                            {
                                Invoice inv = new Invoice();
                                SiteSettings sSetting = new SiteSettings(fn, showid);
                                sSetting.LoadBaseSiteProperties(showid);
                                string InvOwnerID = iControler.DefineInvoiceOwnerID(flwID, GroupRegID, DelegateID); // Depend on FLow Type return GroupID or Delegate ID
                                string InvID = iControler.GenInvocieID(showid);
                                string InvNo = iControler.GenInvocieNumber(showid, InvOwnerID, sSetting.InvoiceRefMaxLength);
                                inv.InvoiceID = InvID;
                                inv.InvOwnerID = InvOwnerID;
                                inv.InvoiceNo = InvNo;
                                inv.RegGroupId = GroupRegID;
                                inv.GSTAmount = cFun.ParseDecimal(lblGstFee.Text);
                                inv.AdminFee = cFun.ParseDecimal(lblAdminFee.Text);
                                inv.OtherFee = cFun.ParseDecimal(lblTTAdminFee.Text);
                                inv.Discount = 0;
                                inv.Status = invStatus;
                                inv.PaymentMethod = payType.ToString();
                                inv.SubTotal = cFun.ParseDecimal(lblSubTotal.Text);
                                inv.GrandTotal = cFun.ParseDecimal(lblGrandTotal.Text);

                                bool isOK = iControler.RemovePendingInvoice(inv.InvOwnerID);

                                if (isOK) isOK = iControler.AddInvoice(inv);
                                if (isOK)
                                {
                                    oControler.UpdateInvoiceNuber(oList, InvID);
                                }

                                //bool makePayment = true;
                                if (payType == (int)PaymentType.CreditCard)
                                {
                                    PaymentControler pCtrl = new PaymentControler(fn);
                                    PaymentLog pLog = new PaymentLog();


                                    pLog.PayRefNo = InvNo;// Use InvoiceNo
                                    pLog.InvoiceID = inv.InvoiceID;
                                    pLog.Amount = inv.GrandTotal;
                                    pLog.RegGroupId = GroupRegID;
                                    pLog.RegID = DelegateID;
                                    pLog.UrlFlowID = cFun.DecryptValue(urlQuery.FlowID);
                                    pLog.UrlStep = cFun.DecryptValue(urlQuery.CurrIndex);
                                    pLog.UrlShowID = showid;
                                    bool sOK = pCtrl.LogPayment(pLog);
                                    if (sOK)
                                    {
                                        string url = pCtrl.GetPaymentUrl();
                                        if (string.IsNullOrEmpty(url))
                                        {
                                            Response.Redirect("404.aspx");
                                        }
                                        else
                                        {
                                            string project = cFun.EncryptValue(sSetting.SiteProjectID);
                                            string refNO = cFun.EncryptValue(inv.InvoiceNo);
                                            string amt = cFun.EncryptValue(pLog.Amount.ToString());
                                            string groupgID = GroupRegID;
                                            string delegatID = DelegateID;
                                            if (string.IsNullOrEmpty(delegatID)) delegatID = SiteDefaultValue.DefaultDelegateID;
                                            string userRef = cFun.EncryptValue(groupgID + "-" + delegatID);

                                            insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actnext + RegClass.strCreditCard, urlQuery);

                                            SiteSettings st = new SiteSettings(fn, showid);
                                            if (st.AllowPartialPayment == YesOrNo.Yes && !string.IsNullOrEmpty(txtPartialAmount.Text) && !string.IsNullOrWhiteSpace(txtPartialAmount.Text))
                                            {
                                                amt = txtPartialAmount.Text;
                                            }

                                            url += string.Format("?PTD={0}&&RFN={1}&VLT={2}&USR={3}&EML={4}", project, refNO, amt, userRef, emailaddress);
                                            Response.Redirect(url);
                                        }
                                    }
                                }
                                else if (payType == (int)PaymentType.TT)
                                {
                                    InvoiceControler iControl = new InvoiceControler(fn);
                                    iControl.UpdateInvoiceStatus(inv.InvoiceID, payType);

                                    insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actnext + RegClass.strTT, urlQuery);
                                }
                                else if (payType == (int)PaymentType.Waved)
                                {
                                    InvoiceControler iControl = new InvoiceControler(fn);
                                    iControl.UpdateInvoiceStatus(inv.InvoiceID, payType);

                                    oControler.UpdateOrderStatusSuccess(inv.InvoiceID);

                                    iControl.UpdateInvoiceLock(inv.InvoiceID, (int)InvoiceLock.Locaked); //Lock Invoice Frist ; For Multiple Payment//***

                                    insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actnext + RegClass.strWaived, urlQuery);
                                }
                                else if (payType == (int)PaymentType.Cheque)
                                {
                                    InvoiceControler iControl = new InvoiceControler(fn);
                                    iControl.UpdateInvoiceStatus(inv.InvoiceID, payType);

                                    insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actnext + RegClass.strCheque, urlQuery);
                                }
                                else if (payType == (int)PaymentType.CreditCardManual)
                                {
                                    InvoiceControler iControl = new InvoiceControler(fn);
                                    iControl.UpdateInvoiceStatus(inv.InvoiceID, payType);

                                    insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actnext + RegClass.strCreditCardManual, urlQuery);
                                }
                                FlowControler flwObj = new FlowControler(fn, urlQuery);
                                string showID = urlQuery.CurrShowID;
                                string page = flwObj.NextStepURL();
                                string step = flwObj.NextStep;
                                string FlowID = flwObj.FlowID;
                                string grpNum = "";
                                grpNum = urlQuery.GoupRegID;
                                string regno = urlQuery.DelegateID;
                                string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                                Response.Redirect(route, false);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('Please select payment.');</script>", false);
                        }
                        #endregion
                    }
                    else
                    {
                        #region no payment method used but buy conference
                        if (oList.Count > 0)
                        {
                            payType = (int)PaymentType.CreditCard;
                            SiteSettings sSetting = new SiteSettings(fn, showid);
                            sSetting.LoadBaseSiteProperties(showid);
                            Invoice inv = new Invoice();
                            string InvOwnerID = iControler.DefineInvoiceOwnerID(flwID, GroupRegID, DelegateID); // Depend on FLow Type return GroupID or Delegate ID
                            string InvID = iControler.GenInvocieID(showid);
                            string InvNo = iControler.GenInvocieNumber(showid, InvOwnerID, sSetting.InvoiceRefMaxLength);
                            inv.InvoiceID = InvID;
                            inv.InvOwnerID = InvOwnerID;
                            inv.InvoiceNo = InvNo;
                            inv.RegGroupId = GroupRegID;
                            inv.GSTAmount = cFun.ParseDecimal(lblGstFee.Text);
                            inv.AdminFee = cFun.ParseDecimal(lblAdminFee.Text);
                            inv.OtherFee = 0;
                            inv.Discount = 0;
                            inv.Status = invStatus;
                            inv.PaymentMethod = payType.ToString();
                            inv.SubTotal = cFun.ParseDecimal(lblSubTotal.Text);
                            inv.GrandTotal = cFun.ParseDecimal(lblGrandTotal.Text);

                            bool isOK = iControler.RemovePendingInvoice(inv.InvOwnerID);

                            if (isOK) isOK = iControler.AddInvoice(inv);
                            if (isOK)
                            {
                                oControler.UpdateInvoiceNuber(oList, InvID);
                            }

                            //*Making Payment with Credit Card
                            PaymentControler pCtrl = new PaymentControler(fn);
                            PaymentLog pLog = new PaymentLog();
                            //SiteSettings sSetting = new SiteSettings(fn, showid);
                            //sSetting.LoadBaseSiteProperties(showid);

                            pLog.PayRefNo = InvNo;
                            pLog.InvoiceID = inv.InvoiceID;
                            pLog.Amount = inv.GrandTotal;
                            pLog.RegGroupId = GroupRegID;
                            pLog.RegID = DelegateID;
                            pLog.UrlFlowID = cFun.DecryptValue(urlQuery.FlowID);
                            pLog.UrlStep = cFun.DecryptValue(urlQuery.CurrIndex);
                            pLog.UrlShowID = showid;
                            bool sOK = pCtrl.LogPayment(pLog);
                            if (sOK)
                            {
                                string url = pCtrl.GetPaymentUrl();
                                if (string.IsNullOrEmpty(url))
                                {
                                    Response.Redirect("404.aspx");
                                }
                                else
                                {
                                    string project = cFun.EncryptValue(sSetting.SiteProjectID);
                                    string refNO = cFun.EncryptValue(inv.InvoiceNo);
                                    string amt = cFun.EncryptValue(pLog.Amount.ToString());
                                    string groupgID = GroupRegID;
                                    string delegatID = DelegateID;
                                    if (string.IsNullOrEmpty(delegatID)) delegatID = SiteDefaultValue.DefaultDelegateID;
                                    string userRef = cFun.EncryptValue(groupgID + "-" + delegatID);

                                    insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actnext + RegClass.strCreditCard, urlQuery);

                                    SiteSettings st = new SiteSettings(fn, showid);
                                    if (st.AllowPartialPayment == YesOrNo.Yes && !string.IsNullOrEmpty(txtPartialAmount.Text) && !string.IsNullOrWhiteSpace(txtPartialAmount.Text))
                                    {
                                        amt = txtPartialAmount.Text;
                                    }

                                    url += string.Format("?PTD={0}&&RFN={1}&VLT={2}&USR={3}&EML={4}", project, refNO, amt, userRef, emailaddress);
                                    Response.Redirect(url);
                                }
                            }

                            FlowControler flwObj = new FlowControler(fn, urlQuery);
                            string showID = urlQuery.CurrShowID;
                            string page = flwObj.NextStepURL();
                            string step = flwObj.NextStep;
                            string FlowID = flwObj.FlowID;
                            string grpNum = "";
                            grpNum = urlQuery.GoupRegID;
                            string regno = urlQuery.DelegateID;
                            string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                            Response.Redirect(route, false);
                        }
                        #endregion
                    }
                }
                else
                {
                    if (oList.Count > 0)
                    {
                        payType = (int)PaymentType.Waved;

                        Invoice inv = new Invoice();
                        string InvOwnerID = iControler.DefineInvoiceOwnerID(flwID, GroupRegID, DelegateID); // Depend on FLow Type return GroupID or Delegate ID
                        string InvID = iControler.GenInvocieID(showid);
                        string InvNo = iControler.GenInvocieNumber(showid, InvOwnerID);
                        inv.InvoiceID = InvID;
                        inv.InvOwnerID = InvOwnerID;
                        inv.InvoiceNo = InvNo;
                        inv.RegGroupId = GroupRegID;
                        inv.GSTAmount = cFun.ParseDecimal(lblGstFee.Text);
                        inv.AdminFee = cFun.ParseDecimal(lblAdminFee.Text);
                        inv.OtherFee = 0;
                        inv.Discount = 0;
                        inv.Status = invStatus;
                        inv.PaymentMethod = payType.ToString();
                        inv.SubTotal = cFun.ParseDecimal(lblSubTotal.Text);
                        inv.GrandTotal = cFun.ParseDecimal(lblGrandTotal.Text);

                        bool isOK = iControler.RemovePendingInvoice(inv.InvOwnerID);

                        if (isOK) isOK = iControler.AddInvoice(inv);
                        if (isOK)
                        {
                            oControler.UpdateInvoiceNuber(oList, InvID);
                        }

                        InvoiceControler iControl = new InvoiceControler(fn);
                        iControl.UpdateInvoiceStatus(inv.InvoiceID, payType);

                        iControl.UpdateInvoiceLock(inv.InvoiceID, (int)InvoiceLock.Locaked); //Lock Invoice Frist ; For Multiple Payment//***

                        oControler.UpdateOrderStatusSuccess(inv.InvoiceID);//***

                        FlowControler flwObj = new FlowControler(fn, urlQuery);
                        string showID = urlQuery.CurrShowID;
                        string page = flwObj.NextStepURL();
                        string step = flwObj.NextStep;
                        string FlowID = flwObj.FlowID;
                        string grpNum = "";
                        grpNum = urlQuery.GoupRegID;
                        string regno = urlQuery.DelegateID;
                        string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                        Response.Redirect(route, false);
                    }
                    else
                    {
                        insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actwithoutorderinvoice, urlQuery);

                        FlowControler flwObj = new FlowControler(fn, urlQuery);
                        string showID = urlQuery.CurrShowID;
                        string page = flwObj.NextStepURL();
                        string step = flwObj.NextStep;
                        string FlowID = flwObj.FlowID;
                        string grpNum = "";
                        grpNum = urlQuery.GoupRegID;
                        string regno = urlQuery.DelegateID;
                        string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                        Response.Redirect(route, false);
                    }
                }
            }
            catch (Exception ex)
            {
                LogGenEmail lggenemail = new LogGenEmail(fn);
                lggenemail.type = GenLogDefaultValue.errorException;
                lggenemail.RefNumber = GroupRegID + "," + DelegateID;
                lggenemail.description = ex.Message;
                lggenemail.remark = RegClass.typeCmf + cFun.DecryptValue(urlQuery.FlowID);
                lggenemail.step = cFun.DecryptValue(urlQuery.CurrIndex);
                lggenemail.writeLog();
            }
        }
    }
    #endregion

    #region isValid (check valid if settings_name='hasterm' value is '1' otherwise not from tb_site_settings table)
    protected Boolean isValid(string showid, FlowURLQuery urlQuery)
    {
        Boolean isvalid = true;

        try
        {
            #region Old (Comment)
            //SiteSettings st = new SiteSettings(fn, showid);
            //if (st.hasterm == YesOrNo.Yes)
            //{
            //    int countTerms = chkTerms.Items.Count;
            //    int countCheckedTerms = chkTerms.Items.Cast<ListItem>().Count(li => li.Selected);
            //    if (countTerms != countCheckedTerms)
            //    {
            //        chkTerms.BackColor = System.Drawing.Color.LightPink;
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('Please accept declaration.');</script>", false);
            //        isvalid = false;
            //    }
            //}
            #endregion


            //***changed on 25-6-2018
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = UpdatePanel1.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = UpdatePanel1.FindControl("chk" + ftnObj.note_Type);
                                Control ctrlLbl = UpdatePanel1.FindControl("lblErr" + ftnObj.note_Type);
                                if (ctrlChk != null && ctrlLbl != null)
                                {
                                    CheckBoxList chkNote = UpdatePanel1.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    Label lblErrNote = UpdatePanel1.FindControl("lblErr" + ftnObj.note_Type) as Label;
                                    if (divFooter.Visible == true)
                                    {
                                        int countTerms = chkNote.Items.Count;
                                        if (countTerms > 0)
                                        {
                                            lblErrNote.Visible = false;
                                            string id = ftnObj.note_ID;
                                            int isSkip = 0;
                                            if (ftnObj != null)
                                            {
                                                isSkip = ftnObj.note_isSkip;
                                            }
                                            ListItem liItem = chkNote.Items.FindByValue(id);
                                            if (liItem != null)
                                            {
                                                if (isSkip == 0)
                                                {
                                                    if (liItem.Selected == false)
                                                    {
                                                        lblErrNote.Visible = true;
                                                        isvalid = false;
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('Please accept .');</script>", false);
                                                        return isvalid;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //***changed on 25-6-2018
        }
        catch (Exception ex)
        { }

        return isvalid;
    }
    #endregion

    #region radioPaymentsChange
    protected void radioPaymentsChange(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            showFeeByPaymentMethod(showid);

            bindConferenceCalPrice(showid, urlQuery);
        }
    }
    #endregion

    #region showFeeByPaymentMethod
    private void showFeeByPaymentMethod(string showid)
    {
        SiteSettings st = new SiteSettings(fn, showid);
        if (rbCreditCard.Checked == true)
        //||
        //(rbCreditCard.Checked == false && rbTT.Checked == false && rbWaived.Checked == false && rbCheque.Checked == false))
        {
            decimal adminfee = cFun.ParseDecimal(st.adminfee);
            if (adminfee == 0 || adminfee == (decimal)Number.zeroDecimal)
            {
                trAdminFee.Visible = false;
            }
            else
            {
                trAdminFee.Visible = true;
            }


            trTTAdminFee.Visible = false;
        }
        else if (rbCreditManual.Checked == true)
        {
            decimal adminfee = cFun.ParseDecimal(st.adminfee);
            if (adminfee == 0 || adminfee == (decimal)Number.zeroDecimal)
            {
                trAdminFee.Visible = false;
            }
            else
            {
                trAdminFee.Visible = true;
            }


            trTTAdminFee.Visible = false;
        }
        else if (rbTT.Checked == true)
        {
            trAdminFee.Visible = false;
            //  trGST.Visible = false;

            decimal ttadminfee = cFun.ParseDecimal(st.ttAdminFee);
            if (ttadminfee == 0 || ttadminfee == (decimal)Number.zeroDecimal)
            {
                trTTAdminFee.Visible = false;
            }
            else
            {
                if (!isSameWithHostCountry())  // [Sithu,2018-Jun-20] If Host Country and Delegate Coutry is same , TT Admin Charge=0
                    trTTAdminFee.Visible = true;
            }
        }
        else
        {
            trAdminFee.Visible = false;
            //    trGST.Visible = false;
            trTTAdminFee.Visible = false;
        }
        decimal gstfee = cFun.ParseDecimal(st.gstfee);
        if (gstfee == 0 || gstfee == (decimal)Number.zeroDecimal)
        {
            trGST.Visible = false;
        }
        else
        {
            trGST.Visible = true;
            if (!string.IsNullOrEmpty(st.ShowHostCountry) && st.ShowHostCountry != "0")
            {
                if (!isSameWithHostCountry())//***added by th on 20-7-2018 If Host Country and Delegate Coutry(Singapore) is not same , GST Charge=0
                {
                    trGST.Visible = false;
                }
            }
        }

    }
    #endregion

    #region PrevClick
    protected void PrevClick(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        string showID = urlQuery.CurrShowID;
        string FlowID = flwObj.FlowID;
        string currentStep = flwObj.CurrIndex;

        flwObj.getPreviousRoute(cFun.DecryptValue(FlowID), currentStep);
        string page = flwObj.CurrIndexModule;
        string step = flwObj.CurrIndex;
        string grpNum = "";
        grpNum = urlQuery.GoupRegID;
        string regno = urlQuery.DelegateID;

        string regType = BackendRegType.backendRegType_Delegate;
        FlowMaster flwMaster = flwObj.GetFlowMasterConfig(cFun.DecryptValue(FlowID));
        if (flwMaster.FlowType == SiteFlowType.FLOW_GROUP)
        {
            regType = BackendRegType.backendRegType_Group;
        }

        string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno, regType);
        Response.Redirect(route);
    }
    #endregion

    #region bindpromo
    protected void bindpromo()
    {
        //string userid = string.Empty;

        //DataSet dspromo = new DataSet();
        //CommonDataObj comObj = new CommonDataObj(fn);
        //dspromo = comObj.getPromo(userid);
        //if (dspromo.Tables[0].Rows.Count != 0)
        //{
        //    trdiscount.Visible = true;
        //}
        //else
        //{
        //    trdiscount.Visible = false;
        //}
    }
    #endregion

    #region bindName
    public string bindSalutation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getSalutationNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }

                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(name))
                {
                    name = otherValue;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindProfession(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getProfessionNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDepartment(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getDepartmentNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindOrganisation(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getOrganisationNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindInstitution(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getInstitutionNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindCountry(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                CountryObj conCtr = new CountryObj(fn);
                name = conCtr.getCountryNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindAffiliation(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getAffiliationNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDietary(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getDietaryNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindIndustry(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getIndustryNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindPromoCode(string groupid)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(groupid))
            {
                CommonDataObj cmdObj = new CommonDataObj(fn);
                DataSet dtPromo = cmdObj.getPromo(groupid);
                if(dtPromo.Tables[0].Rows.Count > 0)
                {
                    name = dtPromo.Tables[0].Rows[0]["Promo_Code"] != DBNull.Value ? dtPromo.Tables[0].Rows[0]["Promo_Code"].ToString() : "";
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    
    #endregion

    #region bindPhoneNo
    public string bindPhoneNo(string cc, string ac, string phoneno, string type)
    {
        string name = string.Empty;
        bool isShowCC = false, isShowAC = false, isShowPhoneNo = false;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                DataSet ds = new DataSet();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showid;
                frmObj.flowID = flowid;
                ds = frmObj.getDynFormForDelegate();

                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    if (type == "Tel")
                    {
                        #region type="Tel"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                    else if (type == "Mob")
                    {
                        #region type="Mob"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                    else if (type == "Fax")
                    {
                        #region Type="Fax"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                }

                if (isShowCC)
                {
                    name = "+" + cc;
                }
                if (isShowAC)
                {
                    name += " " + ac;
                }
                if (isShowPhoneNo)
                {
                    name += " " + phoneno;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion

    #region bindPhoneNo_Company
    public string bindPhoneNo_Company(string cc, string ac, string phoneno, string type)
    {
        string name = string.Empty;
        bool isShowCC = false, isShowAC = false, isShowPhoneNo = false;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                DataSet ds = new DataSet();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showid;
                frmObj.flowID = flowid;
                ds = frmObj.getDynFormForCompany();

                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    if (type == "Tel")
                    {
                        #region type="Tel"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CTelcc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CTelac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CTel)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                    else if (type == "Fax")
                    {
                        #region Type="Fax"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CFaxcc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CFaxac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CFax)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                }

                if (isShowCC)
                {
                    name = "+" + cc;
                }
                if (isShowAC)
                {
                    name += " " + ac;
                }
                if (isShowPhoneNo)
                {
                    name += " " + phoneno;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion

    #region bindPhoneNo_Group
    public string bindPhoneNo_Group(string cc, string ac, string phoneno, string type)
    {
        string name = string.Empty;
        bool isShowCC = false, isShowAC = false, isShowPhoneNo = false;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                DataSet ds = new DataSet();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showid;
                frmObj.flowID = flowid;
                ds = frmObj.getDynFormForGroup();

                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    if (type == "Tel")
                    {
                        #region type="Tel"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GTelcc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GTelac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GTel)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                    else if (type == "Mob")
                    {
                        #region type="Mob"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GMobilecc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GMobileac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GMobile)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                    else if (type == "Fax")
                    {
                        #region Type="Fax"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GFaxcc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GFaxac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GFax)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                }

                if (isShowCC)
                {
                    name = "+" + cc;
                }
                if (isShowAC)
                {
                    name += " " + ac;
                }
                if (isShowPhoneNo)
                {
                    name += " " + phoneno;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    #region Edit Buttons
    private void setEditButtonsVisibility()
    {
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string regno = cFun.DecryptValue(urlQuery.DelegateID);
            if (!string.IsNullOrEmpty(showid))
            {
                if (!String.IsNullOrEmpty(groupid))
                {
                    FlowControler fCtrl = new FlowControler(fn);
                    FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
                    if (fmaster != null)
                    {
                        if (fmaster.isMainDelegateEditStep > 0)
                        {
                            if (fmaster.FlowType == SiteFlowType.FLOW_GROUP)
                            {
                                btnGEditMainDelegate.Visible = true;
                                divEditMainDelegate.Attributes.Remove("class");
                                divSubmit.Attributes.Remove("class");
                                divSubmit.Attributes.Add("class", "col-lg-offset-4 col-lg-3 col-sm-offset-4 col-sm-3 center-block");
                            }
                            else
                            {
                                //ShowControler shwCtr = new ShowControler(fn);
                                //Show shw = shwCtr.GetShow(showid);
                                //if (checkingSARCShowName.Contains(shw.SHW_Name))
                                //{
                                //    btnPrev.Visible = true;
                                //    btnPrev.Attributes.Remove("class");
                                //    btnPrev.Attributes.Add("class", "col-lg-offset-3 col-lg-3 center-block");
                                //    divSubmit.Attributes.Remove("class");
                                //    divSubmit.Attributes.Add("class", "col-lg-3 center-block");
                                //}
                                //else
                                {
                                    btnEditMainDelegate.Visible = true;
                                    divEditMainDelegate.Attributes.Remove("class");
                                    divEditMainDelegate.Attributes.Add("class", "col-lg-offset-3 col-lg-3 center-block");
                                    divSubmit.Attributes.Remove("class");
                                    divSubmit.Attributes.Add("class", "col-lg-3 center-block");
                                }
                            }
                        }
                        if (fmaster.isCompanyEditStep > 0)
                        {
                            btnCompanyEdit.Visible = true;
                        }
                        if (fmaster.isVisiblePrintAtConfirmation)
                        {
                            divPrintPage.Visible = true;
                        }
                    }
                }
                else
                {
                    Response.Redirect("DefaultRegIndex.aspx");
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
        catch (Exception ex)
        { }
    }
    protected void btnEditMainDelegate_Click(object sender, EventArgs e)
    {
        try
        {
            string commandname = ((Button)sender).CommandName;
            if (commandname == "EditMainDelegate")
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                string flowid = cFun.DecryptValue(urlQuery.FlowID);
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string regno = cFun.DecryptValue(urlQuery.DelegateID);
                if (!string.IsNullOrEmpty(showid))
                {
                    FlowControler flwObj = new FlowControler(fn, urlQuery);
                    if (!String.IsNullOrEmpty(groupid))
                    {
                        FlowControler fCtrl = new FlowControler(fn);
                        FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
                        if (fmaster != null)
                        {
                            if (fmaster.isMainDelegateEditStep > 0)
                            {
                                string editStep = fmaster.isMainDelegateEditStep.ToString();
                                FlowControler editFlwCtrl = new FlowControler(fn, flowid, editStep);
                                string showID = urlQuery.CurrShowID;
                                string page = editFlwCtrl.CurrIndexModule;
                                string step = editStep;
                                string FlowID = urlQuery.FlowID;
                                string grpNum = "";
                                grpNum = urlQuery.GoupRegID;
                                string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, urlQuery.DelegateID);
                                Response.Redirect(route);
                            }
                        }
                    }
                    else
                    {
                        Response.Redirect("DefaultRegIndex.aspx");
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
        }
        catch (Exception ex)
        { }
    }
    protected void btnMemberDelegateEdit_Click(object sender, EventArgs e)
    {
        try
        {
            string regno = ((Button)sender).CommandArgument;
            string commandname = ((Button)sender).CommandName;
            if (commandname == "EditMemberDelegate")
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                string flowid = cFun.DecryptValue(urlQuery.FlowID);
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                if (!string.IsNullOrEmpty(showid))
                {
                    FlowControler flwObj = new FlowControler(fn, urlQuery);
                    if (!String.IsNullOrEmpty(groupid))
                    {
                        FlowControler fCtrl = new FlowControler(fn);
                        FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
                        if (fmaster != null)
                        {
                            if (fmaster.isMemberDelegateEditStep > 0)
                            {
                                string editStep = fmaster.isMemberDelegateEditStep.ToString();
                                FlowControler editFlwCtrl = new FlowControler(fn, flowid, editStep);
                                string showID = urlQuery.CurrShowID;
                                string page = editFlwCtrl.CurrIndexModule;
                                string step = editStep;
                                string FlowID = urlQuery.FlowID;
                                string grpNum = "";
                                grpNum = urlQuery.GoupRegID;
                                string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                                Response.Redirect(route);
                            }
                        }
                    }
                    else
                    {
                        Response.Redirect("DefaultRegIndex.aspx");
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
        }
        catch (Exception ex)
        { }
    }
    protected void btnCompanyEdit_Click(object sender, EventArgs e)
    {
        try
        {
            string commandname = ((Button)sender).CommandName;
            if (commandname == "EditCompany")
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                string flowid = cFun.DecryptValue(urlQuery.FlowID);
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string regno = cFun.DecryptValue(urlQuery.DelegateID);
                if (!string.IsNullOrEmpty(showid))
                {
                    FlowControler flwObj = new FlowControler(fn, urlQuery);
                    if (!String.IsNullOrEmpty(groupid))
                    {
                        FlowControler fCtrl = new FlowControler(fn);
                        FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
                        if (fmaster != null)
                        {
                            if (fmaster.isCompanyEditStep > 0)
                            {
                                string editStep = fmaster.isCompanyEditStep.ToString();
                                FlowControler editFlwCtrl = new FlowControler(fn, flowid, editStep);
                                string showID = urlQuery.CurrShowID;
                                string page = editFlwCtrl.CurrIndexModule;
                                string step = editStep;
                                string FlowID = urlQuery.FlowID;
                                string grpNum = "";
                                grpNum = urlQuery.GoupRegID;
                                string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step);
                                Response.Redirect(route);
                            }
                        }
                    }
                    else
                    {
                        Response.Redirect("DefaultRegIndex.aspx");
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #region show back button (added on 15-7-2019)
    private void setBackButtonVisibility()
    {
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string regno = cFun.DecryptValue(urlQuery.DelegateID);
            if (!string.IsNullOrEmpty(showid))
            {
                if (!String.IsNullOrEmpty(groupid))
                {
                    FlowControler fCtrl = new FlowControler(fn);
                    fCtrl.FillCurrentStepInfo(flowid, cFun.DecryptValue(urlQuery.CurrIndex));
                    btnPrev.Visible = false;
                    btnPrev.Attributes.Remove("class");
                    divPrev.Attributes.Remove("class");
                    if (fCtrl.ShowBackButton == "1")
                    {
                        btnPrev.Visible = true;
                        btnPrev.Attributes.Remove("class");
                        divPrev.Attributes.Remove("class");
                        if (btnEditMainDelegate.Visible == true)
                        {
                            divPrev.Attributes.Add("class", "col-lg-offset-2 col-lg-3 center-block");
                            btnPrev.Attributes.Add("class", "col-lg-offset-2 col-lg-3 center-block");
                            divEditMainDelegate.Attributes.Remove("class");
                            divEditMainDelegate.Attributes.Add("class", "col-lg-3 center-block");
                        }
                        else
                        {
                            divPrev.Attributes.Add("class", "col-lg-offset-3 col-lg-3 center-block");
                            btnPrev.Attributes.Add("class", "col-lg-offset-3 col-lg-3 center-block");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #endregion

    #region updateCurStep - Check already paid, if not update current step
    private void updateCurStep(FlowURLQuery urlQuery)
    {
        try
        {
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cFun.DecryptValue(urlQuery.DelegateID);
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj.Count == 0)
            {
                //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
                RegGroupObj rgg = new RegGroupObj(fn);
                rgg.updateGroupCurrentStep(urlQuery);

                //*update reg_urlFlowID(current flowid) and reg_Stage(current step) of tb_RegDelegate table
                RegDelegateObj rgd = new RegDelegateObj(fn);
                rgd.updateDelegateCurrentStep(urlQuery);
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region CheckHostCountry
    private bool isSameWithHostCountry()
    {
        bool isSame = false;

        if (regCountrySameWithShowCountry.Text == "1")
            isSame = true;

        return isSame;
    }
    #endregion

    #region bindFlowNote//***added on 25-6-2018
    private void bindFlowNote(string showid, FlowURLQuery urlQuery)
    {
        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = UpdatePanel1.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            divFooter.Visible = true;
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = UpdatePanel1.FindControl("chk" + ftnObj.note_Type);
                                if (ctrlChk != null)
                                {
                                    CheckBoxList chkFTRCHK = UpdatePanel1.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    ListItem newItem = new ListItem(displayTextTmpt, ftnObj.note_ID);
                                    chkFTRCHK.Items.Add(newItem);

                                    ShowControler shwCtr = new ShowControler(fn);
                                    Show shw = shwCtr.GetShow(showid);
                                    if (checkingOSEAShowName.Contains(shw.SHW_Name))
                                    {
                                        divFooter.Attributes.Add("align", "middle");
                                    }
                                }
                            }
                        }
                        else
                        {
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #region saveRegAdditional//***added on 25-6-2018
    protected void saveRegAdditional(string showid, string regno, FlowURLQuery urlQuery)
    {
        try
        {
            if (divFTRCHK.Visible == true)
            {
                int countTerms = chkFTRCHK.Items.Count;
                //int countCheckedTerms = chkFTRCHK.Items.Cast<ListItem>().Count(li => li.Selected);
                if (countTerms > 0)
                {
                    foreach (ListItem liItem in chkFTRCHK.Items)
                    {
                        string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                        string delegateid = regno;
                        string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
                        string delegateType = BackendRegType.backendRegType_Group;
                        string ownerID = groupid;
                        if (!string.IsNullOrEmpty(delegateid))
                        {
                            delegateType = BackendRegType.backendRegType_Delegate;
                            ownerID = delegateid;
                        }
                        RegAdditionalObj regAddObj = new RegAdditionalObj();
                        regAddObj.ad_ShowID = showid;
                        regAddObj.ad_FlowID = cFun.DecryptValue(urlQuery.FlowID);
                        regAddObj.ad_OwnerID = ownerID;
                        regAddObj.ad_FlowStep = currentStep;
                        regAddObj.ad_DelegateType = delegateType;
                        regAddObj.ad_Value = liItem.Selected == true ? "1" : "0";
                        regAddObj.ad_Type = FlowTemplateNoteType.FooterWithCheckBox;
                        regAddObj.ad_NoteID = cFun.ParseInt(liItem.Value);
                        RegAdditionalControler regAddCtrl = new RegAdditionalControler(fn);
                        regAddCtrl.SaveRegAdditional(regAddObj);
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region btnPrintPage_Click
    protected void btnPrintPage_Click(object sender, EventArgs e)
    {
    }
    #endregion

    #region convertDateOfBirth
    public string convertDateOfBirth(string dob)
    {
        string result = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(dob) && !string.IsNullOrWhiteSpace(dob))
            {
                result = DateTime.Parse(dob.Trim()).ToString("dd/MM/yyyy");
            }
        }
        catch(Exception ex)
        { }

        return result;
    }
    #endregion
    private string getRegistrantEmail(string regno, string groupid, string flowid, string showid)
    {
        string emailaddress = "";
        try
        {
            if (!string.IsNullOrEmpty(regno))
            {
                emailaddress = fn.GetDataByCommand("Select reg_Email From tb_RegDelegate Where ShowID='" + showid + "' And recycle=0 And reg_urlFlowID='" + flowid + "'"
                    + " And RegGroupID='" + groupid + "' And Regno='" + regno + "'", "reg_Email");
                if (emailaddress == "0")
                {
                    emailaddress = "";
                }
            }
            else
            {
                emailaddress = fn.GetDataByCommand("Select RG_ContactEmail From tb_RegGroup Where ShowID='" + showid + "' And recycle=0 And RG_urlFlowID='" + flowid + "'"
                    + " And RegGroupID='" + groupid + "'", "RG_ContactEmail");
                if (emailaddress == "0")
                {
                    emailaddress = "";
                }
            }
        }
        catch (Exception ex)
        { }
        return emailaddress;
    }
}