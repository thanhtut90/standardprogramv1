﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MTLSSynopsis.aspx.cs" Inherits="MTLSSynopsis" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/MTLSTemplate/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/MTLSTemplate/Site.css" rel="stylesheet" />
    <style>
        .divSPMain {
            padding: 20px;
            padding-top: 20px;
            text-align: justify;
            border-radius: 20px;
        }

        .divSPInfo {
            padding-top: 1px;
        }
    </style>
</head>
<body class="SiteMasterFrame">
    <form id="form1" runat="server">
        <div class="row SiteHeader">
            <asp:Label runat="server" ID="SiteHeader" Text="Synopsis"></asp:Label>
        </div>
        <asp:Panel runat="server" ID="PanelSess" Visible="false">
            <div class="container SiteContent">
                <div class="container  text-right" style="padding-left:0px;">
                    <div class="col-md-4 col-sm-3 text-left" style="padding-left:0px;">
                        <asp:ImageButton runat="server" ID="btnHome" ImageUrl="https://event-reg.biz/DefaultBanner/images/MTLS2019/MTLSLogo.png" Height="70" OnClick="btnHome_Click" />

                    </div>
                    <div class="col-md-8 col-sm-8 ">
                        <asp:ImageButton runat="server" ID="btnBack" ImageUrl="https://event-reg.biz/DefaultBanner/images/MTLS2019/btnBack.png" OnClick="btnBack_Click" Height="70" />

                    </div>
                </div>

                <div class="col-md-12" style="padding-top: 40px;">
                    <asp:Label runat="server" ID="lblTitle_EL" Style="font-weight: bold; font-size: 20px; letter-spacing: 2px; color: #525252"></asp:Label>
                </div>
                <div class="col-md-12" style="padding-top: 30px;">
                    <b style="font-size: 17px; color: #525252; display: none;">SCHOOL / ORGANISATION</b><br />
                    <asp:Label runat="server" ID="lblOrg_EL" Style="font-weight: bold; font-size: 15px; letter-spacing: 1px; color: #7f7f7f"></asp:Label>
                </div>

                <div class="row divSPMain">
                    <h2 style="text-decoration: underline; padding-left: 20px;">
                        <img src="https://event-reg.biz/DefaultBanner/images/MTLS2019/HderSynopsis.png" style="height: 60px;" /></h2>
                    <div class="col-md-2">
                    </div>

                    <div class="divSPInfo col-md-10">

                        <div class="col-md-12">
                            <asp:Label runat="server" ID="lblSynosis_EL"> </asp:Label>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px;">
                            <asp:Label runat="server" ID="lblSynosis_OL"> </asp:Label>
                        </div>
                    </div>
                </div>



            </div>
        </asp:Panel>
    </form>
</body>
</html>
