﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SendCETReg_EmailBlast : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }


    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        //string sql = "Select Top 150 reg_urlFlowID,recycle,reg_Status,reg_isSMS,Delegate_RefRegno,reg_Email,* From tb_RegPrePopulate Where ShowID='" + showid + "'"
        //    + " And (reg_isSMS Is Null Or reg_isSMS=0) Order By ID";
        //DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        //if (dt.Rows.Count > 0)
        {
            int sentcount = 0;
            string emailSendID = "10001";
            string emailSubject = "[Invitation to Register] CET Masters Series Symposium 2019 - 03 Oct 2019, Suntec Convention Centre";
            string emailFromName = "CET";
            string fullname = "Than Htut";
            string regUrl = "https://event-reg.biz/registration/eventreg?event=CET2019&CEI=" + emailSendID;
            string edmUrl = "https://event-reg.biz/registration/CETEDM.aspx?CEI=" + emailSendID;
            string isSuccess = sEmailSend(emailSubject, emailFromName, fullname, regUrl, edmUrl);
            Response.End();
            //foreach (DataRow dr in dt.Rows)
            //{
            //    string dbID = dr["ID"].ToString();
            //    string emailaddress = dr["EmailAddress"].ToString();

            //string UnsubscribeMDAUrl = "https://www.event-reg.biz/registration/UnsubscribeMDA?SHW=" + cFun.EncryptValue(showid) + "&r=" + cFun.EncryptValue(delegateRefRegno);
            //string isSuccess = sEmailSend(delegateRefRegno, regUrl, UnsubscribeMDAUrl, emailaddress, fullname, emailSubject, emailFromName);
            //if(isSuccess == "T")
            //{
            //    string updatesql = "Update tb_RegPrePopulate Set reg_isSMS=1 Where ShowID='" + showid + "' And Delegate_RefRegno='" + delegateRefRegno + "'";
            //    fn.ExecuteSQL(updatesql);
            //}

            //string sqlLog = String.Format("Insert Into tmpCETRegEDM_EmailSendLog (DBID,RegEmail,ShowID,Status)"
            //                    + " Values (N'{0}',N'{1}',N'{2}',N'{3}')",
            //                    dbID, emailaddress, "CET", isSuccess.ToString());
            //fn.ExecuteSQL(sqlLog);
            //sentcount++;

            //Response.End();
            //}
            ////Response.End();
            //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already sent the email.Count:" + sentcount + "');", true);
        }
    }

    public string sEmailSend(string emailSubject, string emailFromName, string fullname, string regUrl, string edmUrl)
    {
        string isSuccess = "F";
        try
        {
            Alpinely.TownCrier.TemplateParser tm = new Alpinely.TownCrier.TemplateParser();
            Alpinely.TownCrier.MergedEmailFactory factory = new Alpinely.TownCrier.MergedEmailFactory(tm);
            string template = HttpContext.Current.Server.MapPath("~/Template/CETEmail.html");

            var tokenValues = new Dictionary<string, string>
                {
                    {"regUrl",regUrl},
                    {"EDMUrl",edmUrl},
                    {"FullName",fullname},
                };
            MailMessage message = factory
            .WithTokenValues(tokenValues)
            .WithHtmlBodyFromFile(template)
            .Create();

            //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//***
            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;//***

            //var from = new MailAddress("registration@event-reg.biz", ddlShowID.SelectedItem.Text);
            var from = new MailAddress("admin@event-reg.biz", emailFromName);
            message.From = from;
            message.To.Add("thanhtut@corpit.com.sg");
            ////message.To.Add("ronald.kuik @sg.pico.com");
            ////message.To.Add("thanhtut@corpit.com.sg");
            //message.To.Add("wayne.ang@sg.pico.com");
            ////message.To.Add("constancechung@corpit.com.sg");
            message.Subject = emailSubject;
            SmtpClient emailClient = new SmtpClient("smtp.gmail.com");
            //System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("registration@event-reg.biz", "c0rpitblasT");
            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("admin@event-reg.biz", "c0rpitblasT");
            emailClient.UseDefaultCredentials = false;
            emailClient.EnableSsl = true;
            emailClient.Port = 587;//***
            emailClient.Credentials = SMTPUserInfo;
            emailClient.Send(message);

            isSuccess = "T";
        }
        catch (Exception ex)
        {
            isSuccess = ex.Message;// "F";
        }
        return isSuccess;
    }
}