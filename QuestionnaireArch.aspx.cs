﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class QuestionnaireArch : System.Web.UI.Page
{
    CommonFuns cFun = new CommonFuns();
    Functionality fn = new Functionality();
    LogActionObj rlgobj = new LogActionObj();
    List<QuestionItem> questions;
    List<AnswerItem> answers;

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string regno = cFun.DecryptValue(urlQuery.DelegateID);
        string groupid = cFun.DecryptValue(urlQuery.GoupRegID);

        SetUpQuestionAnswer(showid, flowid, regno, groupid);

        if (showid == RefShow.architectThaiShw)
        {
            btnBack.Text = "ย้อนกลับ";
            btnNext.Text = "ถัดไป";
        }

        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(showid))
            {
                LoadQuestionnaire(1);
                LoadQuestionnaire(2);
            }
            else
            {
                Response.Redirect("404.aspx");
            }
            lblIsLoad.Text = "0";
        }
        else
        {
            lblIsLoad.Text = "1";
        }
    }

    protected void SetUpQuestionAnswer(string showid, string flowid, string regno, string groupid)
    {
        try
        {
            DataTable dt = new DataTable();

            QuestionnaireControler qaController = new QuestionnaireControler(fn);
            //Getting all the questions
            dt = qaController.getAllQuestionnaireByShowID(showid);
            questions = new List<QuestionItem>();

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    questions.Add(new QuestionItem(dr["qa_questid"].ToString(), dr["quest_displayno"].ToString(), dr["question"].ToString(), dr["questinputtype"].ToString(), bool.Parse(dr["required"].ToString())));
                }
            }

            DataTable dtQAList = qaController.getQAListByShowID(showid);
            string qaid = string.Empty;
            string qaLogID = string.Empty;
            if (dtQAList.Rows.Count > 0)
            {
                qaid = dtQAList.Rows[0]["QA_Id"].ToString();
                string chkres = qaController.CheckQALogExist(dt.Rows[0]["QA_Id"].ToString(), regno, groupid, flowid, showid);
                if (!string.IsNullOrEmpty(chkres))
                {
                    qaLogID = chkres;
                }
            }

            for (int count = 1; count <= 6; count++)
            {
                string lblQnControl = string.Format("lblQn{0}", count);
                string divAnswersControl = string.Format("divAnswers{0}", count);

                var currentQnLabel = divQuestionnaire.FindControl(lblQnControl);
                var currentDivAnswers = divQuestionnaire.FindControl(divAnswersControl);

                //Getting all the answers to the each questions
                DataTable dtAnswer = qaController.getAllQAnswerByQuestion(showid, count.ToString());

                if (true && dtAnswer.Rows.Count > 0)
                {
                    var currentQuestion = new QuestionItem(dtAnswer.Rows[0]["qa_questid"].ToString(), dtAnswer.Rows[0]["quest_displayno"].ToString(), dtAnswer.Rows[0]["question"].ToString(), dtAnswer.Rows[0]["questinputtype"].ToString(), bool.Parse(dtAnswer.Rows[0]["required"].ToString()));

                    (currentQnLabel as Label).Attributes.Add("name", currentQuestion.questId);
                    (currentQnLabel as Label).Text = string.Format("{0}. {1}", currentQuestion.displayNo, currentQuestion.question);

                    foreach (DataRow dr in dtAnswer.Rows)
                    {
                        var currentAnswer = new AnswerItem(dr["qa_ansid"].ToString(), dr["parentansid"].ToString(), dr["answer"].ToString(), dr["inputtype"].ToString(), int.Parse(dr["sort_order"].ToString()), dr["LabelInfrontCheckBox"].ToString());

                        GenerateControl(qaid, qaLogID, ref currentDivAnswers, ref currentQuestion, ref currentAnswer);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void GenerateControl(string qaid, string qaLogID, ref Control container, ref QuestionItem question, ref AnswerItem answer)
    {
        QuestionnaireControler qaController = new QuestionnaireControler(fn);
        int i = 1;
        Panel answerPanel = new Panel();
        answerPanel.Attributes.Add("runat", "server");
        answerPanel.ID = "pnlAnswer" + "Q" + question.questId + "A" + answer.ansId;
        answerPanel.CssClass = "answer";

        Panel wrapperPanel = new Panel();
        wrapperPanel.CssClass = "col-md-4";

        string isLoad = lblIsLoad.Text;

        if (string.IsNullOrEmpty(answer.parentId))
        {
            if (question.inputType.Contains("Radio"))
            {
                CheckBox answerChkb = new CheckBox();
                answerChkb.Attributes.Add("runat", "server");
                answerChkb.AutoPostBack = true;
                answerChkb.CheckedChanged += new EventHandler(answer_CheckedChanged);
                answerChkb.InputAttributes.Add("groupname", "qn" + question.displayNo + "ans");
                answerChkb.InputAttributes.Add("name", "answer");
                answerChkb.LabelAttributes.CssStyle.Add("font-weight", "normal");
                answerChkb.LabelAttributes.CssStyle.Add("padding-left", "10px");
                answerChkb.LabelAttributes.CssStyle.Add("max-width", "90%");

                if (answer.ansId == "10000" || answer.ansId == "10067")
                {
                    answerChkb.LabelAttributes.CssStyle.Remove("padding-left");
                    answerChkb.LabelAttributes.CssStyle.Add("padding-left", "20px");
                    answerChkb.InputAttributes.Add("hidden", "");
                    answerChkb.InputAttributes.Add("disabled", "");
                }

                answerChkb.ID = "Q" + question.questId + "A" + answer.ansId;
                answerChkb.Text = answer.answer;
                if (!string.IsNullOrEmpty(answer.labelinfront))
                {
                    Label lblLabel = new Label();
                    lblLabel.Text = answer.labelinfront;
                    lblLabel.CssClass = "numbering";
                    lblLabel.Attributes.Add("runat", "server");
                    lblLabel.ID = "lbl" + "Q" + question.questId + "A" + answer.ansId;
                    answerPanel.Controls.Add(lblLabel);
                }
                answerPanel.Controls.Add(answerChkb);

                DataTable dtAlreadySelected = qaController.getQAResults(qaLogID, qaid, question.questId, answer.ansId);
                if (dtAlreadySelected.Rows.Count > 0)
                {
                    answerChkb.Checked = true;

                    List<string> selected = lblCurrentSelectedAnswers.Text.Split(',').ToList();
                    if (!selected.Contains(answerChkb.ID))
                    {
                        string selectedID = answerChkb.ID;
                        if (isLoad == "0")
                        {
                            lblCurrentSelectedAnswers.Text += (selectedID + ",");
                        }
                    }
                }
            }

            if (question.inputType.Contains("Checkbox"))
            {
                CheckBox answerChkb = new CheckBox();
                answerChkb.Attributes.Add("runat", "server");
                answerChkb.AutoPostBack = true;
                answerChkb.CheckedChanged += new EventHandler(answer_CheckedChanged);
                answerChkb.InputAttributes.Add("name", "answer");
                answerChkb.LabelAttributes.CssStyle.Add("font-weight", "normal");
                answerChkb.LabelAttributes.CssStyle.Add("padding-left", "5px");
                answerChkb.LabelAttributes.CssStyle.Add("max-width", "90%");
                answerChkb.ID = "Q" + question.questId + "A" + answer.ansId;
                answerChkb.Text = answer.answer;
                if (!string.IsNullOrEmpty(answer.labelinfront))
                {
                    Label lblLabel = new Label();
                    lblLabel.Text = answer.labelinfront;
                    lblLabel.CssClass = "numbering";
                    lblLabel.Attributes.Add("runat", "server");
                    lblLabel.ID = "lbl" + "Q" + question.questId + "A" + answer.ansId;
                    answerPanel.Controls.Add(lblLabel);
                }
                answerPanel.Controls.Add(answerChkb);
                
                DataTable dtAlreadySelected = qaController.getQAResults(qaLogID, qaid, question.questId, answer.ansId);
                if (dtAlreadySelected.Rows.Count > 0)
                {
                    answerChkb.Checked = true;

                    List<string> selected = lblCurrentSelectedAnswers.Text.Split(',').ToList();
                    if (!selected.Contains(answerChkb.ID))
                    {
                        string selectedID = answerChkb.ID;
                        if (isLoad == "0")
                        {
                            lblCurrentSelectedAnswers.Text += (selectedID + ",");
                        }
                    }
                }
            }

            if (answer.inputType.Contains("Text"))
            {
                TextBox tbOthers = new TextBox();
                tbOthers.Attributes.Add("runat", "server");
                tbOthers.ID = "tbOtherQ" + "Q" + question.questId + "A" + answer.ansId;
                tbOthers.CssClass = "form-control";
                tbOthers.Attributes.Add("style", "max-width: 270px;");
                answerPanel.Controls.Add(tbOthers);

                DataTable dtAlreadySelected = qaController.getQAResults(qaLogID, qaid, question.questId, answer.ansId);
                if (dtAlreadySelected.Rows.Count > 0)
                {
                    tbOthers.Text = dtAlreadySelected.Rows[0]["QA_AnswerText"].ToString();

                    List<string> selected = lblCurrentSelectedAnswers.Text.Split(',').ToList();
                    if (!selected.Contains("Q" + question.questId + "A" + answer.ansId))
                    {
                        string selectedID = "Q" + question.questId + "A" + answer.ansId;
                        if (isLoad == "0")
                        {
                            lblCurrentSelectedAnswers.Text += (selectedID + ",");
                        }
                    }
                }
            }

            if (question.displayNo == "3") //Question 3 
            {
                if (divCol1.Controls.Count < 6)
                {
                    divCol1.Controls.Add(answerPanel);
                }
                else if (divCol2.Controls.Count < 6)
                {
                    divCol2.Controls.Add(answerPanel);
                }
                else if (divCol3.Controls.Count < 6)
                {
                    divCol3.Controls.Add(answerPanel);
                }
            }
            else
            {
                container.Controls.Add(answerPanel);
            }
        }

        else if (question.displayNo == "1" && !string.IsNullOrEmpty(answer.parentId)) //subAnswers
        {
            Panel subAnswersPanel;
            string subAnswerPanelID = "SubAnsPanel" + answer.parentId;
            if (container.FindControl(subAnswerPanelID) != null)
            {
                subAnswersPanel = (container.FindControl(subAnswerPanelID) as Panel);
            }
            else
            {
                subAnswersPanel = new Panel();
                subAnswersPanel.ID = subAnswerPanelID;
                subAnswersPanel.Attributes.Add("style", "margin-left: 25px;");
                container.Controls.Add(subAnswersPanel);
            }

            Panel subPanel = new Panel();
            subPanel.Attributes.Add("style", "display: inline-block;margin-right: 20px;");

            //if (answer.inputType.Contains("Text"))
            //{
            //    TextBox tbOthers = new TextBox();
            //    tbOthers.ID = "tbOtherQ" + "Q" + question.questId + "A" + answer.ansId;
            //    tbOthers.CssClass = "form-control";
            //    tbOthers.Attributes.Add("style", "max-width: 270px;");
            //    subPanel.Controls.Add(tbOthers);
            //    subAnswersPanel.Controls.Add(subPanel);

            //    DataTable dtAlreadySelected = qaController.getQAResults(qaLogID, qaid, question.questId, answer.ansId);
            //    if (dtAlreadySelected.Rows.Count > 0)
            //    {
            //        tbOthers.Text = dtAlreadySelected.Rows[0]["QA_AnswerText"].ToString();

            //List<string> selected = lblCurrentSelectedAnswers.Text.Split(',').ToList();
            //if (!selected.Contains("Q" + question.questId + "A" + answer.ansId))
            //{
            //    string selectedID = "Q" + question.questId + "A" + answer.ansId;
            //if (isLoad == "0")
            //{
            //    lblCurrentSelectedAnswers.Text += (selectedID + ",");
            //}
            //}
            //    }
            //}
            //else
            {
                if (!string.IsNullOrEmpty(answer.parentId))
                {
                    CheckBox subAnswerChkb = new CheckBox();
                    subAnswerChkb.Attributes.Add("runat", "server");
                    subAnswerChkb.AutoPostBack = true;
                    subAnswerChkb.CheckedChanged += new EventHandler(answer_CheckedChanged);
                    //subAnswerChkb.InputAttributes.Add("groupname", "qn" + question.displayNo + "ans");
                    subAnswerChkb.InputAttributes.Add("groupname", "subans" + answer.parentId);
                    subAnswerChkb.InputAttributes.Add("name", ("subAnswer"));
                    subAnswerChkb.LabelAttributes.CssStyle.Add("font-weight", "normal");
                    subAnswerChkb.LabelAttributes.CssStyle.Add("padding-left", "5px");
                    subAnswerChkb.ID = "Q" + question.questId + "A" + answer.ansId;
                    subAnswerChkb.Text = answer.answer;
                    subPanel.Controls.Add(subAnswerChkb);
                    subAnswersPanel.Controls.Add(subPanel);

                    DataTable dtAlreadySelected = qaController.getQAResults(qaLogID, qaid, question.questId, answer.ansId);
                    if (dtAlreadySelected.Rows.Count > 0)
                    {
                        subAnswerChkb.Checked = true;

                        List<string> selected = lblCurrentSelectedAnswers.Text.Split(',').ToList();
                        if (!selected.Contains(subAnswerChkb.ID))
                        {
                            string selectedID = subAnswerChkb.ID;
                            if (isLoad == "0")
                            {
                                lblCurrentSelectedAnswers.Text += (selectedID + ",");
                            }
                        }
                    }
                }
            }
        }
    }

    protected void SaveAnswer(List<string> selectedAnswers, string selectedQuestionIDs)
    {
        try
        {
            DataTable dt = new DataTable();
            List<SqlParameter> pList = new List<SqlParameter>();
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string regno = cFun.DecryptValue(urlQuery.DelegateID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);

            QuestionnaireControler qaController = new QuestionnaireControler(fn);
            //Response.End();
            int currentQn = int.Parse(lblCurrentQn.Text);
            if (currentQn > 0 && currentQn < 7)
            {
                if (selectedAnswers.Count > 0)
                {
                    //Getting all the questions
                    dt = qaController.getQAListByShowID(showid);
                    if (dt.Rows.Count > 0)
                    {
                        string qaid = dt.Rows[0]["QA_Id"].ToString();
                        string qaLogID = string.Empty;

                        string chkres = qaController.CheckQALogExist(dt.Rows[0]["QA_Id"].ToString(), regno, groupid, flowid, showid);
                        if (string.IsNullOrEmpty(chkres))
                        {
                            RunNumber run = new RunNumber(fn);
                            qaLogID = RefQAID.RefQALogID + run.GetRunNUmber(RefQAID.RefQALogID);
                        }
                        else
                        {
                            qaLogID = chkres;
                        }

                        if (currentQn == 2)
                        {
                            if (string.IsNullOrEmpty(chkres))
                            {
                                QALogObj qalogObj = new QALogObj();
                                qalogObj.qalogid = qaLogID;
                                qalogObj.regno = regno;
                                qalogObj.reggroupid = groupid;
                                qalogObj.flowid = flowid;
                                qalogObj.showid = showid;
                                qalogObj.qaid = int.Parse(qaid);
                                int successQALog = qaController.SaveQALog(qalogObj);//*insert qaLog if not exist already
                            }
                        }

                        #region delete old result by questid
                        if (currentQn == 2)
                        {
                            for (int curid = 1; curid <= 2; curid++)
                            {
                                string currentQuestID = string.Empty;
                                string lblQnControl = string.Format("lblQn{0}", curid);
                                Label currentQnLabel = (Label)divQuestionnaire.FindControl(lblQnControl);
                                if (currentQnLabel != null)
                                {
                                    currentQuestID = currentQnLabel.Attributes["name"];
                                }
                                string divQuestControl = string.Format("divQn{0}", curid);
                                var currentDivAnswers = divQuestionnaire.FindControl(divQuestControl);
                                if (currentDivAnswers.Visible == true)
                                {
                                    if (!string.IsNullOrEmpty(currentQuestID))
                                    {
                                        int deleteQAResult = qaController.DeleteQALog(qaLogID, qaid, currentQuestID.TrimEnd(','));//*delete aqResult if alreayd exist at the first question
                                    }
                                }
                            }
                        }
                        else if (currentQn > 2 && currentQn < 7)
                        {
                            string currentQuestID = string.Empty;
                            string lblQnControl = string.Format("lblQn{0}", currentQn);
                            Label currentQnLabel = (Label)divQuestionnaire.FindControl(lblQnControl);
                            if (currentQnLabel != null)
                            {
                                currentQuestID = currentQnLabel.Attributes["name"];
                            }
                            string divQuestControl = string.Format("divQn{0}", currentQn);
                            var currentDivAnswers = divQuestionnaire.FindControl(divQuestControl);
                            if (currentDivAnswers.Visible == true)
                            {
                                if (!string.IsNullOrEmpty(currentQuestID))
                                {
                                    int deleteQAResult = qaController.DeleteQALog(qaLogID, qaid, currentQuestID.TrimEnd(','));//*delete aqResult if alreayd exist at the first question
                                }
                            }
                        }
                        #endregion

                        foreach (string answer in selectedAnswers) // Loop each selected answer and save/update in the database
                        {
                            //because the selected answer is stored as Q<Question Id>A<AnswerId>, so we have to split the answer string to get the question id
                            string selectedanswer = answer;

                            string questionid = selectedanswer.Split('A')[0].Replace("Q", "");
                            string answerid = selectedanswer.Split('A')[1];

                            DataTable dtCurrentAnswerDiv = qaController.getQAQuestionsByQuestID(questionid);
                            var currentDiv = divQuestionnaire.FindControl("divQn" + dtCurrentAnswerDiv.Rows[0]["Quest_DisplayNo"].ToString());

                            if (currentDiv.Visible == true)
                            {
                                string othervalue = string.Empty;
                                //*get textbox value
                                DataTable dtControls = qaController.getQAInputTypeByAnswerID(answerid);// (selectedanswer);
                                if (dtControls.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dtControls.Rows)
                                    {
                                        string inputType = dr["InputType"].ToString();
                                        if (inputType == "Text")
                                        {
                                            string controlID = "tbOtherQ" + "Q" + questionid + "A" + answerid;
                                            TextBox txt = (TextBox)currentDiv.FindControl(controlID);
                                            if (txt != null)
                                            {
                                                if (!string.IsNullOrEmpty(txt.Text) && !string.IsNullOrWhiteSpace(txt.Text))
                                                {
                                                    othervalue = txt.Text.Trim();
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                //*

                                RunNumber run = new RunNumber(fn);
                                string qaResultID = RefQAID.RefQAResultID + run.GetRunNUmber(RefQAID.RefQAResultID);

                                QAResultObj qaresultObj = new QAResultObj();
                                qaresultObj.qalogid = qaLogID;
                                qaresultObj.qaresultid = qaResultID;
                                qaresultObj.qaid = int.Parse(qaid);
                                qaresultObj.qaquestid = questionid;
                                qaresultObj.qaanswerid = answerid;
                                qaresultObj.qaanswertext = othervalue;
                                qaController.SaveQAResult(qaresultObj);

                                if(questionid == staticJobTitleQuestionnIDsArch.jQuestID_Eng || questionid == staticJobTitleQuestionnIDsArch.jQuestID_Thai)
                                {
                                    RegDelegateObj rgdObj = new RegDelegateObj(fn);
                                    DataTable dtAnswer = qaController.getQAInputTypeByAnswerID(answerid);
                                    string answerJobStr = string.Empty;
                                    if (dtAnswer.Rows.Count > 0)
                                    {
                                        answerJobStr= dtAnswer.Rows[0]["Answer"] != DBNull.Value ? dtAnswer.Rows[0]["Answer"].ToString() : "";
                                    }

                                    if (!string.IsNullOrEmpty(answerJobStr) && !string.IsNullOrWhiteSpace(answerJobStr))
                                    {
                                        int indx = answerJobStr.IndexOf(" ");
                                        answerJobStr = answerJobStr.Substring(indx);
                                        answerJobStr = answerJobStr.Trim();
                                    }

                                    int isSuccess = rgdObj.updateJobTitle(regno, answerJobStr, showid);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void LoadQuestionnaire(int qnNo)
    {
        lblCurrentQn.Text = qnNo.ToString();
        switch (qnNo)
        {
            #region Question 1
            case 1:
                {
                    divQuestionnaire.Attributes.Add("class", "container col-md-offset-1");
                    btnBack.Visible = false;
                    divQn1.Visible = true;
                    divQn2.Visible = true;
                    divQn3.Visible = false;
                    divQn4.Visible = false;
                    divQn5.Visible = false;
                    divQn6.Visible = false;
                }
                break;
            #endregion
            #region Question 2
            case 2:
                {
                    divQuestionnaire.Attributes.Add("class", "container col-md-offset-1");
                    btnBack.Visible = false;
                    divQn1.Visible = true;
                    divQn2.Visible = true;
                    divQn3.Visible = false;
                    divQn4.Visible = false;
                    divQn5.Visible = false;
                    divQn6.Visible = false;
                }
                break;
            #endregion
            #region Question 3
            case 3:
                {
                    divQuestionnaire.Attributes.Add("class", "container");
                    btnBack.Visible = true;
                    divQn1.Visible = false;
                    divQn2.Visible = false;
                    divQn3.Visible = true;
                    divQn4.Visible = false;
                    divQn5.Visible = false;
                    divQn6.Visible = false;
                }
                break;
            #endregion
            #region Question 4
            case 4:
                {
                    divQuestionnaire.Attributes.Add("class", "container col-md-offset-1");
                    btnBack.Visible = true;
                    divQn1.Visible = false;
                    divQn2.Visible = false;
                    divQn3.Visible = false;
                    divQn4.Visible = true;
                    divQn5.Visible = false;
                    divQn6.Visible = false;
                }
                break;
            #endregion
            #region Question 5
            case 5:
                {
                    divQuestionnaire.Attributes.Add("class", "container col-md-offset-1");
                    btnBack.Visible = true;
                    divQn1.Visible = false;
                    divQn2.Visible = false;
                    divQn3.Visible = false;
                    divQn4.Visible = false;
                    divQn5.Visible = true;
                    divQn6.Visible = false;
                }
                break;
            #endregion
            #region Question 6
            case 6:
                {
                    divQuestionnaire.Attributes.Add("class", "container col-md-offset-1");
                    btnBack.Visible = true;
                    divQn1.Visible = false;
                    divQn2.Visible = false;
                    divQn3.Visible = false;
                    divQn4.Visible = false;
                    divQn5.Visible = false;
                    divQn6.Visible = true;
                }
                break;
            #endregion
            #region Question 7
            case 7:
                {
                    divQuestionnaire.Attributes.Add("class", "container col-md-offset-1");
                    btnBack.Visible = true;
                    divQn1.Visible = false;
                    divQn2.Visible = false;
                    divQn3.Visible = false;
                    divQn4.Visible = false;
                    divQn5.Visible = false;
                    divQn6.Visible = false;
                }
                break;
            #endregion
            default:
                break;
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        int currentQn = int.Parse(lblCurrentQn.Text);

        List<string> selectedAnswers = lblCurrentSelectedAnswers.Text.TrimEnd(',').Split(',').ToList(); //get all the answers selected
        QuestionnaireControler qaController = new QuestionnaireControler(fn);
        string selectedQuestionIDs = string.Empty;
        string selectedAnswerIDs = string.Empty;
        foreach (string answer in selectedAnswers) // Loop each selected answer and save/update in the database
        {
            if (!string.IsNullOrEmpty(answer))
            {
                //because the selected answer is stored as Q<Question Id>A<AnswerId>, so we have to split the answer string to get the question id
                string questionid = answer.Split('A')[0].Replace("Q", "");
                string answerid = answer.Split('A')[1];
                if (!string.IsNullOrEmpty(questionid))
                {
                    selectedQuestionIDs += questionid + ",";
                }
                if (!string.IsNullOrEmpty(answerid))
                {
                    selectedAnswerIDs += answerid + ",";
                }
            }
        }

        string alertmessage = checkValidation(selectedQuestionIDs, selectedAnswerIDs);
        if (!string.IsNullOrEmpty(alertmessage))
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + alertmessage + "');", true);
            return;
        }
        else
        {
            SaveAnswer(selectedAnswers, selectedQuestionIDs);

            if (currentQn > 0 && currentQn < 6)
            {
                currentQn += 1;
                lblCurrentQn.Text = currentQn.ToString();
                LoadQuestionnaire(currentQn);
            }
            else
            {
                FlowControler flwObj = new FlowControler(fn, urlQuery);
                string showID = urlQuery.CurrShowID;
                string page = flwObj.NextStepURL();
                string step = flwObj.NextStep;
                string FlowID = flwObj.FlowID;
                string grpNum = "";
                string regno = cFun.DecryptValue(urlQuery.DelegateID);
                grpNum = urlQuery.GoupRegID;

                FlowControler flwController = new FlowControler(fn);
                FlowMaster flwConfig = flwController.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));

                if (flwConfig.FlowLimitDelegateQty == Number.One)
                {
                    string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, null, BackendRegType.backendRegType_Delegate);
                    Response.Redirect(route, false);
                }
                else
                {
                    string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno, BackendRegType.backendRegType_Delegate);
                    Response.Redirect(route, false);
                }
            }
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        int currentQn = int.Parse(lblCurrentQn.Text);

        //If the current question is the first question, go back to the previous step
        if (currentQn == 2)
        {
            FlowControler flwObj = new FlowControler(fn, urlQuery);
            string showID = urlQuery.CurrShowID;
            string page = flwObj.NextStepURL();
            string step = currentStep;
            string FlowID = flwObj.FlowID;
            string grpNum = "";
            string regno = cFun.DecryptValue(urlQuery.DelegateID);
            grpNum = urlQuery.GoupRegID;

            FlowControler flwController = new FlowControler(fn);
            FlowMaster flwConfig = flwController.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));

            if (flwConfig.FlowLimitDelegateQty == Number.One)
            {
                string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, null, BackendRegType.backendRegType_Delegate);
                Response.Redirect(route, false);
            }
            else
            {
                string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno, BackendRegType.backendRegType_Delegate);
                Response.Redirect(route, false);
            }
        }
        else
        {
            currentQn -= 1;
            lblCurrentQn.Text = currentQn.ToString();
            LoadQuestionnaire(currentQn);
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    protected void answer_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox input = (sender as CheckBox);
        if (lblCurrentSelectedAnswers.Text.Contains(input.ID))
        {
            List<string> selected = lblCurrentSelectedAnswers.Text.Split(',').ToList();

            if (!input.Checked)
            {
                selected.Remove(input.ID);
                string panelcontrolID = "pnlAnswer" + input.ID;
                Panel answerPanel = (Panel)divQuestionnaire.FindControl(panelcontrolID);
                if (answerPanel != null)
                {
                    string controlID = "tbOtherQ" + input.ID;
                    Control txtControl = (Control)answerPanel.FindControl(controlID);
                    if (txtControl != null && txtControl.GetType() == typeof(TextBox))
                    {
                        TextBox txt = (TextBox)txtControl;
                        txt.BorderColor = System.Drawing.Color.Empty;
                    }
                }
                lblCurrentSelectedAnswers.Text = string.Join(",", selected);
            }
        }
        else
        {
            string selectedID = input.ID;
            string groupName = (input.InputAttributes.Count > 0 && input.InputAttributes["groupname"] != null) ? input.InputAttributes["groupname"].ToString() : "";
            string parentId = (groupName.Contains("subans") && groupName.Split(new string[] { "subans" }, StringSplitOptions.None).Count() > 0) ? groupName.Split(new string[] { "subans" }, StringSplitOptions.None)[1].ToString() : "";

            if (!string.IsNullOrEmpty(parentId) && lblCurrentSelectedAnswers.Text.Contains(("A" + parentId)))
            {
                //Split the selectedAnswers in lblCurrentSelected then query for the answer that contain the parentId
                //to get the parentControlId that was selected
               string parentControlId = lblCurrentSelectedAnswers.Text.TrimEnd(',').Split(',').ToList().Where(str => str.Contains("A" + parentId)).First();

                lblCurrentSelectedAnswers.Text = lblCurrentSelectedAnswers.Text.Replace(parentControlId, selectedID);
            }
            else
            {
                lblCurrentSelectedAnswers.Text += (selectedID + ",");
            }
            //lblCurrentSelectedAnswers.Text += (selectedID + ",");
        }
        System.Diagnostics.Debug.WriteLine("[{0}] current selected ids is " + lblCurrentSelectedAnswers.Text, DateTime.Now);
    }

    private string checkValidation(string selectedQuestionIDs, string selectedAnswerIDs)
    {
        string alertmessage = string.Empty;
        QuestionnaireControler qaController = new QuestionnaireControler(fn);

        selectedQuestionIDs = selectedQuestionIDs.TrimEnd(',');
        selectedAnswerIDs = selectedAnswerIDs.TrimEnd(',');

        for (int count = 1; count < 7; count++)
        {
            string lblQnControl = string.Format("lblQn{0}", count);
            string divAnswersControl = string.Format("divAnswers{0}", count);
            string divQn = string.Format("divQn{0}", count);
            Label currentQnLabel = (Label)divQuestionnaire.FindControl(lblQnControl);
            var currentDivAnswers = divQuestionnaire.FindControl(divAnswersControl);
            string msgControlID = "lblAnswers" + count + "ErrorMessage";
            Label lblMsg = (Label)currentDivAnswers.FindControl(msgControlID);
            if (lblMsg != null)
            {
                lblMsg.Visible = false;
            }

            if (currentDivAnswers.Visible == true)
            {
                string currentQuestID = currentQnLabel.Attributes["name"];
                if (!selectedQuestionIDs.Contains(currentQnLabel.Attributes["name"]))
                {
                    lblMsg = (Label)currentDivAnswers.FindControl(msgControlID);
                    if (lblMsg != null)
                    {
                        lblMsg.Visible = true;
                    }
                    else
                    {
                        lblMsg.Visible = false;
                    }
                    alertmessage = "Please select at lease one for question no." + count + ".";
                    //break;
                }
                else
                {
                    int subselectedidCount = 0;
                    int subselectedTextidCount = 0;
                    DataTable dtSubAnswers = qaController.getQASubAnswersByParentAnswerID(selectedAnswerIDs);
                    if (dtSubAnswers.Rows.Count > 0)
                    {
                        string parentAnswer = string.Empty;
                        string answer = string.Empty;
                        string inputtypeRequired = string.Empty;
                        foreach (DataRow dr in dtSubAnswers.Rows)
                        {
                            string QAQuestId = dr["QA_QuestId"].ToString();
                            string inputType = dr["InputType"].ToString();
                            answer = dr["Answer"].ToString();
                            string QAAnsId = dr["QA_AnsId"].ToString();
                            string parentQAAnsId = dr["ParentAnsId"].ToString();
                            DataTable dtParentAnswer = qaController.getQAInputTypeByAnswerID(parentQAAnsId);
                            if (dtParentAnswer.Rows.Count > 0)
                            {
                                parentAnswer = dtParentAnswer.Rows[0]["Answer"].ToString();
                            }

                            inputtypeRequired = inputType;
                            if (inputType == "Text")
                            {
                                if (currentQuestID == QAQuestId)
                                {
                                    string panelcontrolID = "pnlAnswer" + "Q" + QAQuestId + "A" + QAAnsId;
                                    Panel answerPanel = (Panel)currentDivAnswers.FindControl(panelcontrolID);
                                    if (answerPanel != null)
                                    {
                                        string controlID = "tbOtherQ" + "Q" + QAQuestId + "A" + QAAnsId;
                                        TextBox txt = (TextBox)answerPanel.FindControl(controlID);
                                        if (txt != null)
                                        {
                                            if (!string.IsNullOrEmpty(txt.Text) && !string.IsNullOrWhiteSpace(txt.Text))
                                            {
                                                subselectedTextidCount += 1;
                                                txt.BorderColor = System.Drawing.Color.Empty;
                                            }
                                            else
                                            {
                                                txt.BorderColor = System.Drawing.Color.Red;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (currentQuestID == QAQuestId)
                                {
                                    string subQAAnsId = dr["QA_AnsId"].ToString();
                                    if (selectedAnswerIDs.Contains(subQAAnsId))
                                    {
                                        subselectedidCount += 1;
                                    }
                                }
                            }
                        }

                        if (inputtypeRequired == "Text")
                        {
                            if (subselectedTextidCount == 0)
                            {
                                lblMsg = (Label)currentDivAnswers.FindControl(msgControlID);
                                if (lblMsg != null)
                                {
                                    lblMsg.Visible = true;
                                }
                                else
                                {
                                    lblMsg.Visible = false;
                                }
                                alertmessage = "Please input for question " + parentAnswer + ".";
                                //break;
                            }
                        }
                        else
                        {
                            if (subselectedidCount == 0)
                            {
                                lblMsg = (Label)currentDivAnswers.FindControl(msgControlID);
                                if (lblMsg != null)
                                {
                                    lblMsg.Visible = true;
                                }
                                else
                                {
                                    lblMsg.Visible = false;
                                }
                                alertmessage = "Please select at lease one for question " + parentAnswer + ".";
                                //break;
                            }
                        }
                    }

                    DataTable dtControls = qaController.getQAInputTypeByAnswerID(selectedAnswerIDs);
                    if (dtControls.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtControls.Rows)
                        {
                            string QAQuestId = dr["QA_QuestId"].ToString();
                            string answer = dr["Answer"].ToString();
                            string QAAnsId = dr["QA_AnsId"].ToString();
                            string inputType = dr["InputType"].ToString();
                            if (inputType == "Text")
                            {
                                if (currentQuestID == QAQuestId)
                                {
                                    string panelcontrolID = "pnlAnswer" + "Q" + QAQuestId + "A" + QAAnsId;
                                    Panel answerPanel = (Panel)currentDivAnswers.FindControl(panelcontrolID);
                                    if (answerPanel != null)
                                    {
                                        string controlID = "tbOtherQ" + "Q" + QAQuestId + "A" + QAAnsId;
                                        Control txtControl = (Control)currentDivAnswers.FindControl(controlID);
                                        if (txtControl != null && txtControl.GetType() == typeof(TextBox))
                                        {
                                            TextBox txt = (TextBox)txtControl;
                                            if (string.IsNullOrEmpty(txt.Text) && string.IsNullOrWhiteSpace(txt.Text))
                                            {
                                                lblMsg = (Label)currentDivAnswers.FindControl(msgControlID);
                                                if (lblMsg != null)
                                                {
                                                    lblMsg.Visible = true;
                                                }
                                                else
                                                {
                                                    lblMsg.Visible = false;
                                                }
                                                alertmessage = "Please input for question " + answer + ".";
                                                txt.BorderColor = System.Drawing.Color.Red;
                                                //break;
                                            }
                                            else
                                            {
                                                txt.BorderColor = System.Drawing.Color.Empty;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return alertmessage;
    }
}