﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class RecommendFriendOSEA : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                bindFlowNote(showid, urlQuery);//***added on 25-6-2018
                BindGrid(showid);

                updateCurStep(urlQuery);//***edit on 23-3-2018

                insertLogFlowAction(cFun.DecryptValue(urlQuery.GoupRegID), cFun.DecryptValue(urlQuery.DelegateID), rlgobj.actview, urlQuery);
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    #region updateCurStep - Check already paid, if not update current step
    private void updateCurStep(FlowURLQuery urlQuery)
    {
        try
        {
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cFun.DecryptValue(urlQuery.DelegateID);
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj.Count == 0)
            {
                //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
                RegGroupObj rgg = new RegGroupObj(fn);
                rgg.updateGroupCurrentStep(urlQuery);

                //*update reg_urlFlowID(current flowid) and reg_Stage(current step) of tb_RegDelegate table
                RegDelegateObj rgd = new RegDelegateObj(fn);
                rgd.updateDelegateCurrentStep(urlQuery);
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region Grid
    protected void BindGrid(string showid)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        RecommendFriendControler rmdObj = new RecommendFriendControler(fn);
        List<RecommendFriendObj> lstRmd = rmdObj.getAllRecommendFriend(showid, urlQuery);
        if (lstRmd.Count > 0)
        {
            gvList.DataSource = lstRmd;
            gvList.DataBind();
            pnllist.Visible = true;
        }
        else
        {
            pnllist.Visible = false;
        }
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int ID = cFun.ParseInt(gvList.DataKeys[e.RowIndex].Value.ToString());
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string flowid = cFun.DecryptValue(urlQuery.FlowID);

        RecommendFriendControler rmdCtrlr = new RecommendFriendControler(fn);
        rmdCtrlr.deleteRecommendFriend(ID.ToString(), showid, flowid);

        BindGrid(showid);
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        gvList.EditIndex = e.NewEditIndex;
        BindGrid(showid);
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string regno = cFun.DecryptValue(urlQuery.DelegateID);
            string refno = !string.IsNullOrEmpty(regno) ? regno : groupid;
            int ID = cFun.ParseInt(gvList.DataKeys[e.RowIndex].Value.ToString());
            // Retrieve the row being edited.
            int index = gvList.EditIndex;
            GridViewRow row = gvList.Rows[index];
            TextBox t1 = row.FindControl("txtRName") as TextBox;
            TextBox t2 = row.FindControl("txtREmail") as TextBox;
            RecommendFriendObj objRmd = new RecommendFriendObj();
            objRmd.rmd_id = ID.ToString();
            objRmd.rmd_fullname = cFun.solveSQL(t1.Text.ToString());
            objRmd.rmd_email = cFun.solveSQL(t2.Text.ToString());
            objRmd.rmd_showid = showid;
            objRmd.rmd_flowid = flowid;
            objRmd.rmd_refRegno = refno;
            objRmd.rmd_regGroupID = groupid;

            RecommendFriendControler rmdCtrlr = new RecommendFriendControler(fn);
            bool isSuccess = rmdCtrlr.UpdateRecommendFriend(objRmd);
            if (isSuccess)
            {
                gvList.EditIndex = -1;
                BindGrid(showid);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success!');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fail!');", true);
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        gvList.EditIndex = -1;
        BindGrid(showid);
    }
    #endregion

    #region btnSubmit_Click
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string regno = cFun.DecryptValue(urlQuery.DelegateID);
            string refno = !string.IsNullOrEmpty(regno) ? regno : groupid;
            string name = cFun.solveSQL(txtName.Text.ToString());
            string emailaddress = cFun.solveSQL(txtEmail.Text.ToString());
            Boolean isvalidpage = isValidPage(showid, urlQuery);
            if (isvalidpage)
            {
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrWhiteSpace(name)
                    && !string.IsNullOrEmpty(emailaddress) && !string.IsNullOrWhiteSpace(emailaddress))
                {
                    try
                    {
                        RecommendFriendObj objRmd = new RecommendFriendObj();
                        objRmd.rmd_id = ID.ToString();
                        objRmd.rmd_fullname = name;
                        objRmd.rmd_email = emailaddress;
                        objRmd.rmd_showid = showid;
                        objRmd.rmd_flowid = flowid;
                        objRmd.rmd_refRegno = refno;
                        objRmd.rmd_regGroupID = groupid;

                        RecommendFriendControler rmdCtrlr = new RecommendFriendControler(fn);
                        bool isSuccess = rmdCtrlr.CreateRecommendFriend(objRmd);
                        if (isSuccess)
                        {
                            BindGrid(showid);
                            txtName.Text = "";
                            txtEmail.Text = "";
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success!');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fail!');", true);
                        }
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please fill in the field(s)!');", true);
                }
            }
        }
    }
    #endregion

    #region btnNext_Onclick
    protected void btnNext_Onclick(object sender, EventArgs e)
    {
        string actType = string.Empty;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string flwID = cFun.DecryptValue(urlQuery.FlowID);
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
        string regno = cFun.DecryptValue(urlQuery.DelegateID);
        if (!string.IsNullOrEmpty(showid))
        {
                FlowControler flwObj = new FlowControler(fn, urlQuery);
                string showID = urlQuery.CurrShowID;
                string page = flwObj.NextStepURL();
                string step = flwObj.NextStep;
                string FlowID = flwObj.FlowID;
                string grpNum = "";
                grpNum = urlQuery.GoupRegID;
                regno = urlQuery.DelegateID;

                insertLogFlowAction(cFun.DecryptValue(grpNum), cFun.DecryptValue(regno), actType + rlgobj.actnext, urlQuery);

                string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                Response.Redirect(route);
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }
    #endregion

    #region isValidPage//***added on 25-6-2018
    protected Boolean isValidPage(string showid, FlowURLQuery urlQuery)
    {
        Boolean isvalid = true;

        try
        {
            if (divFTRCHK.Visible == true)
            {
                int countTerms = chkFTRCHK.Items.Count;
                if (countTerms > 0)
                {
                    TemplateControler tmpCtrl = new TemplateControler(fn);
                    foreach (ListItem liItem in chkFTRCHK.Items)
                    {
                        string id = liItem.Value.Trim();
                        FlowTemplateNoteObj ftnObj = tmpCtrl.getNoteByID(id, showid, FlowTemplateNoteType.FooterWithCheckBox, urlQuery);
                        int isSkip = 0;
                        if (ftnObj != null)
                        {
                            isSkip = ftnObj.note_isSkip;
                        }

                        if (isSkip == 0)
                        {
                            if (liItem.Selected == false)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('Please accept .');</script>", false);
                                isvalid = false;
                                return isvalid;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return isvalid;
    }
    #endregion
    #region bindFlowNote//***added on 25-6-2018
    private void bindFlowNote(string showid, FlowURLQuery urlQuery)
    {
        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = UpdatePanel1.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            divFooter.Visible = true;
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox)
                            {
                                Control ctrlChk = UpdatePanel1.FindControl("chk" + ftnObj.note_Type);
                                if (ctrlChk != null)
                                {
                                    CheckBoxList chkFTRCHK = UpdatePanel1.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    ListItem newItem = new ListItem(displayTextTmpt, ftnObj.note_ID);
                                    chkFTRCHK.Items.Add(newItem);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #region saveRegAdditional//***added on 25-6-2018
    protected void saveRegAdditional(string showid, string regno, FlowURLQuery urlQuery)
    {
        try
        {
            if (divFTRCHK.Visible == true)
            {
                int countTerms = chkFTRCHK.Items.Count;
                //int countCheckedTerms = chkFTRCHK.Items.Cast<ListItem>().Count(li => li.Selected);
                if (countTerms > 0)
                {
                    foreach (ListItem liItem in chkFTRCHK.Items)
                    {
                        string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                        string delegateid = regno;
                        string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
                        string delegateType = BackendRegType.backendRegType_Group;
                        string ownerID = groupid;
                        if (!string.IsNullOrEmpty(delegateid))
                        {
                            delegateType = BackendRegType.backendRegType_Delegate;
                            ownerID = delegateid;
                        }
                        RegAdditionalObj regAddObj = new RegAdditionalObj();
                        regAddObj.ad_ShowID = showid;
                        regAddObj.ad_FlowID = cFun.DecryptValue(urlQuery.FlowID);
                        regAddObj.ad_OwnerID = ownerID;
                        regAddObj.ad_FlowStep = currentStep;
                        regAddObj.ad_DelegateType = delegateType;
                        regAddObj.ad_Value = liItem.Selected == true ? "1" : "0";
                        regAddObj.ad_Type = FlowTemplateNoteType.FooterWithCheckBox;
                        regAddObj.ad_NoteID = cFun.ParseInt(liItem.Value);
                        RegAdditionalControler regAddCtrl = new RegAdditionalControler(fn);
                        regAddCtrl.SaveRegAdditional(regAddObj);
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
}