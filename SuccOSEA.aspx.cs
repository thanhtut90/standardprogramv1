﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Logging;
using System.Data;
using Corpit.Email;
using Corpit.Site.Email;
using System.Globalization;
using System.Diagnostics;

public partial class SuccOSEA : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cComFuz = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();
    protected static string defaultFlowID_RegisterColleague = "F371";

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        SetSiteMaster(showid);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
            if (!string.IsNullOrEmpty(showid))
            {
                insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actsuccess, urlQuery);

                RegGroupObj rgg = new RegGroupObj(fn);
                rgg.updateGroupCurrentStep(urlQuery);

                RegDelegateObj rgd = new RegDelegateObj(fn);
                rgd.updateDelegateCurrentStep(urlQuery);

                string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
                FlowControler flwControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);

                StatusSettings stuSettings = new StatusSettings(fn);

                string regstatus = "0";
                DataTable dt = new DataTable();
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    dt = rgg.getRegGroupByID(GroupRegID, showid);
                    if (dt.Rows.Count > 0)
                    {
                        regstatus = dt.Rows[0]["RG_Status"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["RG_Status"].ToString()) ? dt.Rows[0]["RG_Status"].ToString() : "0") : "0";
                        //dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                    }

                    rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                    rgd.updateDelegateRegStatus(GroupRegID, stuSettings.Success, showid);
                }
                else//*flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL
                {
                    rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                    if (!string.IsNullOrEmpty(DelegateID))
                    {
                        dt = rgd.getDataByGroupIDRegno(GroupRegID, DelegateID, showid);
                        if (dt.Rows.Count > 0)
                        {
                            regstatus = dt.Rows[0]["reg_Status"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_Status"].ToString()) ? dt.Rows[0]["reg_Status"].ToString() : "0") : "0";
                            //dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                        }

                        rgd.updateStatus(DelegateID, stuSettings.Success, showid);
                    }
                }
                //
                #region Check Approve or Reject
                string status = "";
                bool isVIP = false;
                status = CheckApproveOrReject(GroupRegID, showid, DelegateID, ref isVIP);
                if (!isVIP)
                {
                    rgd.UpdateApproveRejectStatus(DelegateID, cComFuz.ParseInt(status), showid);
                }
                #endregion
                //
                #region bindSuccessMessage
                lblSuccessMessage.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowSuccessMsg) ? flwMasterConfig.FlowSuccessMsg : "");
                if (string.IsNullOrEmpty(lblSuccessMessage.Text))
                    PanelMsg.Visible = false;
                #endregion

                if (cComFuz.ParseInt(regstatus) == stuSettings.Pending)
                {
                    FlowControler flw = new FlowControler(fn, urlQuery);
                    EmailHelper esender = new EmailHelper();
                    //esender.SendCurrentFlowStepEmail(urlQuery);//***comment on 5-7-2018

                    #region Send Email by Condition(according to their requirement)
                    EmailHelper eHelper = new EmailHelper();
                    string eType = EmailHTMLTemlateType.Confirmation;
                    string emailRegType = BackendRegType.backendRegType_Delegate;// "D";
                    string emailCondition = string.Empty;
                    if (status == ((int)RegApproveRejectStatus.Rejected).ToString())
                    {
                        emailCondition = EmailSendConditionType.REJECT;
                    }
                    else if (status == ((int)RegApproveRejectStatus.VIP).ToString())
                    {
                        emailCondition = EmailSendConditionType.VIP;
                    }
                    else if (status == ((int)RegApproveRejectStatus.Confirmed).ToString())
                    {
                        emailCondition = EmailSendConditionType.CONFIRM;
                    }
                    bool isEmailSend = false;
                    if (!string.IsNullOrEmpty(emailCondition) || !string.IsNullOrWhiteSpace(emailCondition))
                    {
                        EmailDSourceKey sourcKey = new EmailDSourceKey();
                        string showID = showid;
                        string flowID = flowid;
                        string groupID = GroupRegID;
                        string delegateID = DelegateID;

                        sourcKey.AddKeyToList("ShowID", showID);
                        sourcKey.AddKeyToList("RegGroupID", groupID);
                        sourcKey.AddKeyToList("Regno", delegateID);
                        sourcKey.AddKeyToList("FlowID", flowID);
                        string updatedUser = string.Empty;
                        if (Session["userid"] != null)
                        {
                            updatedUser = Session["userid"].ToString();
                        }

                        if (emailCondition == EmailSendConditionType.VIP)//***
                        {
                            isEmailSend = eHelper.SendEmailFromBackend(eType, sourcKey, emailRegType, updatedUser);
                        }
                        else
                        {
                            isEmailSend = eHelper.SendEmailFromBackendByCondition(emailCondition, sourcKey, emailRegType, updatedUser);
                        }//***

                        #region Logging
                        LogGenEmail lggenemail = new LogGenEmail(fn);
                        LogActionObj lactObj = new LogActionObj();
                        lggenemail.type = LogType.generalType;
                        lggenemail.RefNumber = groupID + "," + DelegateID;
                        lggenemail.description = lactObj.actupdate + "reg_approveStatus";
                        lggenemail.remark = "Update from backend " + cComFuz.DecryptValue(delegateID) + " and Send " + emailCondition + " email(email sending status:" + isEmailSend + ")";
                        lggenemail.step = "frontend, success page";
                        lggenemail.writeLog();
                        #endregion
                    }
                    #endregion

                    #region RecommendAFriend//***added in 28-6-2018
                    if (!string.IsNullOrEmpty(status))
                    {
                        if (cComFuz.ParseInt(status) == (int)RegApproveRejectStatus.Confirmed || isVIP)
                        {
                            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                            {
                                btnRegisterColleague.Visible = true;
                                RecommendFriendControler rmdCtrl = new RecommendFriendControler(fn);
                                bool isUpdateStatus = rmdCtrl.updateStatusRecommendFriend(showid, (int)StatusValue.Success, urlQuery);
                                if (isUpdateStatus)
                                {
                                    List<RecommendFriendObj> lstRmd = rmdCtrl.getAllRecommendFriend(showid, urlQuery);
                                    if (lstRmd.Count > 0)
                                    {
                                        foreach (RecommendFriendObj rmdObj in lstRmd)
                                        {
                                            string recommendfriendStep = "3";//****
                                            string recommendfriendRegID = rmdObj.rmd_id;
                                            string fullUrl = "FLW=" + cComFuz.DecryptValue(flowid) + "&STP=" + cComFuz.DecryptValue(recommendfriendStep)
                                                            + "&GRP=" + cComFuz.DecryptValue(GroupRegID) + "&INV=" + cComFuz.DecryptValue(recommendfriendRegID)
                                                            + "&SHW=" + cComFuz.DecryptValue(showid);
                                            FlowURLQuery urlQueryRecommendFriend = new FlowURLQuery(fullUrl);
                                            esender.SendCurrentFlowStepEmail(urlQueryRecommendFriend);
                                            #region Logging
                                            LogGenEmail lggenemail = new LogGenEmail(fn);
                                            LogActionObj lactObj = new LogActionObj();
                                            lggenemail.type = LogType.generalType;
                                            lggenemail.RefNumber = GroupRegID + "," + DelegateID;
                                            lggenemail.description = "Send Recommend a Friend Email to " + recommendfriendRegID;
                                            lggenemail.remark = "Send Recommend a Friend Email to " + recommendfriendRegID;
                                            lggenemail.step = cComFuz.DecryptValue(urlQuery.CurrIndex);
                                            lggenemail.writeLog();
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }

                try
                {
                    CommonFuns cFuz = new CommonFuns();
                    string DID = "";
                    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                    {
                        DID = GroupRegID;
                    }
                    else
                        DID = DelegateID;
                    HTMLTemplateControler htmlControl = new HTMLTemplateControler();
                    string template = htmlControl.CreateAcknowledgeLetterHTMLTemplate(showid, DID);
                    string rtnFile = htmlControl.CreatePDF(template, showid, DID, "Acknowledge", false);
                    if (!string.IsNullOrEmpty(rtnFile))
                    {
                        ShowPDF.Visible = true;
                        ShowPDF.Attributes["src"] = rtnFile;
                    }
                }
                catch { }
            }
        }
    }

    #region PageSetting
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
            Page.MasterPageFile = masterPage;
    }

    #endregion

    #region insertLogFlowAction
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        string step = cComFuz.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion


    public string CheckApproveOrReject(string groupID, string showID, string regno, ref bool isVIP)
    {
        string rtn ="0";
        try
        {
            RegDelegateObj rgd = new RegDelegateObj(fn);
            DataTable dt = rgd.getDataByGroupIDRegno(groupID, regno, showID);
            if (dt.Rows.Count != 0)
            {
                int aStatus = 0;

                //***check isVIP
                string approveStatus = dt.Rows[0]["reg_approveStatus"] != DBNull.Value ? dt.Rows[0]["reg_approveStatus"].ToString() : "";
                if (approveStatus == ((int)RegApproveRejectStatus.VIP).ToString())
                {
                    isVIP = true;
                    aStatus = (int)RegApproveRejectStatus.VIP;
                }
                else
                {
                    string fullname = dt.Rows[0]["reg_FName"].ToString() + " " + dt.Rows[0]["reg_LName"].ToString();
                    fullname = fullname.Trim();
                    string jobTitle = dt.Rows[0]["reg_Address1"].ToString();
                    string address = dt.Rows[0]["reg_City"].ToString();
                    string tel = dt.Rows[0]["reg_tel"].ToString();
                    bool valid = true;
                    if (fullname == jobTitle)
                        valid = false;
                    //if (address.Length < 10)
                    //    valid = false;
                    if (tel.Length <= 3)
                        valid = false;
                    jobTitle = jobTitle.ToLower().Trim();
                    if (jobTitle == "freelance" || jobTitle == "homemaker" || jobTitle == "housewife" || jobTitle == "retire" || jobTitle == "retired" || jobTitle == "retiree" || jobTitle == "student" || jobTitle == "insurance" || jobTitle == "insurance agent" || jobTitle == "financial advisor" || jobTitle == "financial planner" || jobTitle == "unemployed")
                        valid = false;

                    if (valid)
                        aStatus = (int)RegApproveRejectStatus.Confirmed;
                    else
                        aStatus = (int)RegApproveRejectStatus.Rejected;
                }

                rtn = aStatus.ToString();
            }
        }
        catch {  }
        return rtn;

    }

    #region RegisterColleague
    protected void btnRegisterColleague_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
        string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        if (!string.IsNullOrEmpty(showid))
        {
            if (!string.IsNullOrEmpty(DelegateID))
            {
                RegDelegateObj rgd = new RegDelegateObj(fn);
                DataTable dt = new DataTable();
                dt = rgd.getDataByGroupIDRegno(GroupRegID, DelegateID, showid);
                if (dt.Rows.Count > 0)
                {
                    rgd.groupid = dt.Rows[0]["RegGroupID"].ToString();
                    rgd.regno = dt.Rows[0]["Regno"].ToString();
                    rgd.con_categoryID = dt.Rows[0]["con_CategoryId"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["con_CategoryId"].ToString()) ? int.Parse(dt.Rows[0]["con_CategoryId"].ToString()) : 0) : 0;
                    rgd.salutation = dt.Rows[0]["reg_Salutation"].ToString();
                    rgd.fname = dt.Rows[0]["reg_FName"].ToString();
                    rgd.lname = dt.Rows[0]["reg_LName"].ToString();
                    rgd.oname = dt.Rows[0]["reg_OName"].ToString();
                    rgd.passno = dt.Rows[0]["reg_PassNo"].ToString();
                    rgd.isreg = dt.Rows[0]["reg_isReg"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_isReg"].ToString()) ? int.Parse(dt.Rows[0]["reg_isReg"].ToString()) : 0) : 0;
                    rgd.regspecific = dt.Rows[0]["reg_sgregistered"].ToString();//MCR/SNB/PRN
                    rgd.idno = dt.Rows[0]["reg_IDno"].ToString();//MCR/SNB/PRN No.
                    rgd.staffid = dt.Rows[0]["reg_staffid"].ToString();//no use in design for this field
                    rgd.designation = dt.Rows[0]["reg_Designation"].ToString();
                    rgd.jobtitle = dt.Rows[0]["reg_Jobtitle_alliedstu"].ToString();//if Profession is Allied Health
                    rgd.profession = dt.Rows[0]["reg_Profession"].ToString();
                    rgd.department = dt.Rows[0]["reg_Department"].ToString();
                    rgd.organization = dt.Rows[0]["reg_Organization"].ToString();
                    rgd.institution = dt.Rows[0]["reg_Institution"].ToString();
                    rgd.address1 = dt.Rows[0]["reg_Address1"].ToString();
                    rgd.address2 = dt.Rows[0]["reg_Address2"].ToString();
                    rgd.address3 = dt.Rows[0]["reg_Address3"].ToString();
                    rgd.address4 = dt.Rows[0]["reg_Address4"].ToString();
                    rgd.city = dt.Rows[0]["reg_City"].ToString();
                    rgd.state = dt.Rows[0]["reg_State"].ToString();
                    rgd.postalcode = dt.Rows[0]["reg_PostalCode"].ToString();
                    rgd.country = dt.Rows[0]["reg_Country"].ToString();
                    rgd.rcountry = dt.Rows[0]["reg_RCountry"].ToString();
                    rgd.telcc = dt.Rows[0]["reg_Telcc"].ToString();
                    rgd.telac = dt.Rows[0]["reg_Telac"].ToString();
                    rgd.tel = dt.Rows[0]["reg_Tel"].ToString();
                    rgd.mobilecc = dt.Rows[0]["reg_Mobcc"].ToString();
                    rgd.mobileac = dt.Rows[0]["reg_Mobac"].ToString();
                    rgd.mobile = dt.Rows[0]["reg_Mobile"].ToString();
                    rgd.faxcc = dt.Rows[0]["reg_Faxcc"].ToString();
                    rgd.faxac = dt.Rows[0]["reg_Faxac"].ToString();
                    rgd.fax = dt.Rows[0]["reg_Fax"].ToString();
                    rgd.email = dt.Rows[0]["reg_Email"].ToString();
                    rgd.affiliation = dt.Rows[0]["reg_Affiliation"].ToString();
                    rgd.dietary = dt.Rows[0]["reg_Dietary"].ToString();
                    rgd.nationality = dt.Rows[0]["reg_Nationality"].ToString();
                    rgd.age = dt.Rows[0]["reg_Age"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_Age"].ToString()) ? int.Parse(dt.Rows[0]["reg_Age"].ToString()) : 0) : 0;
                    rgd.dob = dt.Rows[0]["reg_DOB"].ToString();
                    rgd.gender = dt.Rows[0]["reg_Gender"].ToString();
                    rgd.additional4 = dt.Rows[0]["reg_Additional4"].ToString();
                    rgd.additional5 = dt.Rows[0]["reg_Additional5"].ToString();
                    rgd.memberno = dt.Rows[0]["reg_Membershipno"].ToString();

                    rgd.vname = dt.Rows[0]["reg_vName"].ToString();
                    rgd.vdob = dt.Rows[0]["reg_vDOB"].ToString();
                    rgd.vpassno = dt.Rows[0]["reg_vPassno"].ToString();
                    rgd.vpassexpiry = dt.Rows[0]["reg_vPassexpiry"].ToString();
                    rgd.vpassissuedate = dt.Rows[0]["reg_vIssueDate"].ToString();
                    rgd.vembarkation = dt.Rows[0]["reg_vEmbarkation"].ToString();
                    rgd.varrivaldate = dt.Rows[0]["reg_vArrivalDate"].ToString();
                    rgd.vcountry = dt.Rows[0]["reg_vCountry"].ToString();

                    rgd.udfcname = dt.Rows[0]["UDF_CName"].ToString();
                    rgd.udfdeltype = dt.Rows[0]["UDF_DelegateType"].ToString();
                    rgd.udfprofcat = dt.Rows[0]["UDF_ProfCategory"].ToString();
                    rgd.udfprofcatother = dt.Rows[0]["UDF_ProfCategoryOther"].ToString();
                    rgd.udfcpcode = dt.Rows[0]["UDF_CPcode"].ToString();
                    rgd.udfcldept = dt.Rows[0]["UDF_CLDepartment"].ToString();
                    rgd.udfcaddress = dt.Rows[0]["UDF_CAddress"].ToString();
                    rgd.udfclcompany = dt.Rows[0]["UDF_CLCompany"].ToString();
                    rgd.udfclcompanyother = dt.Rows[0]["UDF_CLCompanyOther"].ToString();
                    rgd.udfccountry = dt.Rows[0]["UDF_CCountry"].ToString();

                    rgd.supname = dt.Rows[0]["reg_SupervisorName"].ToString();
                    rgd.supdesignation = dt.Rows[0]["reg_SupervisorDesignation"].ToString();
                    rgd.supcontact = dt.Rows[0]["reg_SupervisorContact"].ToString();
                    rgd.supemail = dt.Rows[0]["reg_SupervisorEmail"].ToString();

                    rgd.othersal = dt.Rows[0]["reg_SalutationOthers"].ToString();
                    rgd.otherprof = dt.Rows[0]["reg_otherProfession"].ToString();
                    rgd.otherdept = dt.Rows[0]["reg_otherDepartment"].ToString();
                    rgd.otherorg = dt.Rows[0]["reg_otherOrganization"].ToString();
                    rgd.otherinstitution = dt.Rows[0]["reg_otherInstitution"].ToString();

                    rgd.aemail = dt.Rows[0]["reg_aemail"].ToString();
                    rgd.isSMS = dt.Rows[0]["reg_isSMS"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_isSMS"].ToString()) ? int.Parse(dt.Rows[0]["reg_isSMS"].ToString()) : 0) : 0;

                    rgd.remark = dt.Rows[0]["reg_remark"].ToString();
                    rgd.remark_groupupload = dt.Rows[0]["reg_remarkGUpload"].ToString();
                    rgd.approvestatus = dt.Rows[0]["reg_approveStatus"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_approveStatus"].ToString()) ? int.Parse(dt.Rows[0]["reg_approveStatus"].ToString()) : 0) : 0;
                    rgd.createdate = dt.Rows[0]["reg_datecreated"].ToString();
                    rgd.recycle = dt.Rows[0]["recycle"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["recycle"].ToString()) ? int.Parse(dt.Rows[0]["recycle"].ToString()) : 0) : 0;
                    rgd.stage = dt.Rows[0]["reg_Stage"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_Stage"].ToString()) ? dt.Rows[0]["reg_Stage"].ToString() : "") : "";

                    rgd.showID = dt.Rows[0]["ShowID"].ToString();

                    string newregno = string.Empty;
                    string newgroupid = string.Empty;
                    int isSuccessSave = insertRegDelegateData(showid, flowid, rgd, DelegateID, ref newregno, ref newgroupid);
                    if(isSuccessSave > 0)
                    {
                        //string page = "";
                        //string step = "";
                        //FlowControler Flw = new FlowControler(fn);
                        //Dictionary<string, string> nValues = Flw.GetNextRoute(flowid);
                        //if (nValues.Count > 0)
                        //{
                        //    page = nValues["nURL"].ToString();
                        //    step = nValues["nStep"].ToString();
                        //    flowid = nValues["FlowID"].ToString();
                        //}
                        //string route = Flw.MakeFullURL(page, flowid, showid, newgroupid, step, newregno);
                        ShowControler shwCtr = new ShowControler(fn);
                        Show shw = shwCtr.GetShow(showid);
                        string route = "EventRegOSEA.aspx?event=" + shw.SHW_ID + "&reg=" + newregno;
                        Response.Redirect(route);
                    }
                    else
                    {
                        Response.Redirect("404.aspx");
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
        }
    }
    private int insertRegDelegateData(string showid, string flowid, RegDelegateObj rgd, string refRegno, ref string regno, ref string groupid)
    {
        int isSuccess = 0;
        try
        {
            #region create delegate in tb_RegDelegate
            //create group in tb_RegGroup
            RegGroupObj rgp = new RegGroupObj(fn);
            groupid = rgp.GenGroupNumber(showid);// GetRunNUmber("GRP_KEY");
            rgp.createRegGroupIDOnly(groupid, 0, showid);

            regno = rgd.GenDelegateNumber(showid);

            rgd.groupid = groupid;
            rgd.regno = regno;
            rgd.con_categoryID = rgd.con_categoryID;
            rgd.salutation = "";
            rgd.fname = "";
            rgd.lname = "";
            rgd.oname = rgd.oname;
            rgd.passno = rgd.passno;
            rgd.isreg = 0;// rgd.isreg;
            rgd.regspecific = rgd.regspecific;//MCR/SNB/PRN
            rgd.idno = "";// rgd.idno;//MCR/SNB/PRN No.
            rgd.staffid = rgd.staffid;//no use in design for this field
            rgd.designation = rgd.designation;
            rgd.jobtitle = rgd.jobtitle;//if Profession is Allied Health
            rgd.profession = "";
            rgd.department = rgd.department;
            rgd.organization = rgd.organization;
            rgd.institution = rgd.institution;
            rgd.address1 ="";
            rgd.address2 = "";
            rgd.address3 = rgd.address3;
            rgd.address4 = rgd.address4;
            rgd.city = rgd.city;
            rgd.state = rgd.state;
            rgd.postalcode = rgd.postalcode;
            rgd.country = rgd.country;
            rgd.rcountry = rgd.rcountry;
            rgd.telcc = rgd.telcc;
            rgd.telac = rgd.telac;
            rgd.tel = rgd.tel;
            rgd.mobilecc = rgd.mobilecc;
            rgd.mobileac = "";
            rgd.mobile = "";
            rgd.faxcc = rgd.faxcc;
            rgd.faxac = rgd.faxac;
            rgd.fax = rgd.fax;
            rgd.email = "";
            rgd.affiliation = rgd.affiliation;
            rgd.dietary = rgd.dietary;
            rgd.nationality = "";
            rgd.age = rgd.age;
            rgd.dob = rgd.dob;
            rgd.gender = rgd.gender;
            rgd.additional4 = "";// rgd.additional4;////OSEA Event Code
            rgd.additional5 = "";// rgd.additional5;////OSEA VIP Code
            rgd.memberno = "";// rgd.memberno;////OSEA Their Regno

            rgd.vname = rgd.vname;
            rgd.vdob = rgd.vdob;
            rgd.vpassno = rgd.vpassno;
            rgd.vpassexpiry = rgd.vpassexpiry;
            rgd.vpassissuedate = rgd.vpassissuedate;
            rgd.vembarkation = rgd.vembarkation;
            rgd.varrivaldate = rgd.varrivaldate;
            rgd.vcountry = rgd.vcountry;

            rgd.udfcname = rgd.udfcname;
            rgd.udfdeltype = rgd.udfdeltype;
            rgd.udfprofcat = rgd.udfprofcat;
            rgd.udfprofcatother = rgd.udfprofcatother;
            rgd.udfcpcode = rgd.udfcpcode;
            rgd.udfcldept = rgd.udfcldept;
            rgd.udfcaddress = rgd.udfcaddress;
            rgd.udfclcompany = rgd.udfclcompany;
            rgd.udfclcompanyother = rgd.udfclcompanyother;
            rgd.udfccountry = rgd.udfccountry;

            rgd.supname = rgd.supname;
            rgd.supdesignation = rgd.supdesignation;
            rgd.supcontact = rgd.supcontact;
            rgd.supemail = rgd.supemail;

            rgd.othersal = rgd.othersal;
            rgd.otherprof = "";// rgd.otherprof;
            rgd.otherdept = rgd.otherdept;
            rgd.otherorg = rgd.otherorg;
            rgd.otherinstitution = rgd.otherinstitution;

            rgd.aemail = rgd.aemail;
            rgd.isSMS = rgd.isSMS;

            rgd.remark = refRegno;// rgd.remark;
            rgd.remark_groupupload = rgd.remark_groupupload;
            rgd.approvestatus = 0;// rgd.approvestatus;
            rgd.createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            rgd.recycle = rgd.recycle;
            rgd.stage = "1";

            rgd.showID = showid;
            isSuccess = rgd.saveRegDelegate();
            if(flowid != defaultFlowID_RegisterColleague)//*****
            {
                flowid = defaultFlowID_RegisterColleague;
            }//*****
            rgd.updateStep(regno, flowid, "1", showid);
            saveRegAdditionalRegColleague(showid, refRegno, regno);
            #endregion
        }
        catch (Exception ex)
        { }

        return isSuccess;
    }
    protected void saveRegAdditionalRegColleague(string showid, string regno, string colleagueRegno)//regno=recommendpersonRegno
    {
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string delegateid = regno;
            string currentStep = cComFuz.DecryptValue(urlQuery.CurrIndex);
            string delegateType = BackendRegType.backendRegType_Group;
            string ownerID = groupid;
            if (!string.IsNullOrEmpty(delegateid))
            {
                delegateType = BackendRegType.backendRegType_Delegate;
                ownerID = delegateid;
            }
            RegAdditionalObj regAddObj = new RegAdditionalObj();
            regAddObj.ad_ShowID = showid;
            regAddObj.ad_FlowID = cComFuz.DecryptValue(urlQuery.FlowID);
            regAddObj.ad_OwnerID = ownerID;
            regAddObj.ad_FlowStep = currentStep;
            regAddObj.ad_DelegateType = delegateType;
            regAddObj.ad_Value = colleagueRegno;
            regAddObj.ad_Type = RegAdditionalType.registerColleague;
            regAddObj.ad_NoteID = 0;
            RegAdditionalControler regAddCtrl = new RegAdditionalControler(fn);
            regAddCtrl.SaveRegAdditional(regAddObj);
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region Additional Buttons
    protected void btnClose_Click(object sender, EventArgs e)
    {
        #region Old
        ////string jScript = "<script>window.close();</script>";
        ////ClientScript.RegisterClientScriptBlock(this.GetType(), "keyClientBlock", jScript);
        ////ScriptManager.RegisterStartupScript(this, GetType(), "keyClientBlock", jScript, true);

        ////this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);

        ////Response.Write("<script language='javascript'>window.open('','_self');window.close();</script>");

        ////Response.Write("<script type=\"text/javascript\">window.close();</script>");


        ////ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePage", "window.open('window.close();', '_self', null)", true);
        ////ScriptManager.RegisterStartupScript(this, this.GetType(), "callJSFunction", "close_window();", true);
        ////ScriptManager.RegisterStartupScript(this, this.GetType(), "Close_Window", "self.close();", true);

        //FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        //string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        //SiteSettings sSetting = new SiteSettings(fn, showid);
        //sSetting.LoadBaseSiteProperties(showid);

        //Process[] AllProcesses = Process.GetProcesses();
        //foreach (var process in AllProcesses)
        //{
        //    if (process.MainWindowTitle != "")
        //    {
        //        string s = process.ProcessName.ToLower();
        //        string windowtitle = process.MainWindowTitle;
        //        if (s == "iexplore" || s == "iexplorer" || s == "chrome" || s == "firefox")
        //        {
        //            if (windowtitle.Contains(sSetting.SiteTitle))
        //            {
        //                process.Kill();
        //            }
        //        }
        //    }
        //}
        #endregion
        Response.Redirect("https://www.osea-asia.com/");
    }
    #endregion
}