﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Reg_Conference : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();
    private static string ASVACShowID = "VSX365";
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());

            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string currentstage = Session["regstage"].ToString();

                if (!String.IsNullOrEmpty(groupid))
                {
                    string regno = string.Empty;
                    if (Session["Regno"] != null)
                    {
                        regno = Session["Regno"].ToString();
                    }

                    bindConferenceInvoice(showid, flowid, groupid, regno, urlQuery);
                    if (showid == ASVACShowID)
                    {
                        checkExistAdditionalItems(groupid, regno, flowid, showid, currentstage);
                    }
                }
                else
                {
                    Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
                }
            }
            else
            {
                Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMasterReLogin;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region Conference (GetOrderedListWithTemplate) //***edit on 23-3-2018
    private void bindConferenceInvoice(string showid, string flowid, string groupid, string regno, FlowURLQuery urlQuery)
    {
        string subTotal = Number.zeroDecimal.ToString();
        InvoiceControler invControler = new InvoiceControler(fn);
        decimal dcSubTotal = 0;

        #region bindPendingOrderList
        string pendingOrderList = LoadOrderedList(urlQuery, ref subTotal, "");
        dcSubTotal += cFun.ParseDecimal(subTotal);
        string invoiceOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
        Invoice inv = invControler.GetPendingInvoice(invoiceOwnerID);
        if (dcSubTotal > 0 || (!string.IsNullOrEmpty(pendingOrderList) && inv != null && !string.IsNullOrEmpty(inv.InvoiceID)))
        {
            HtmlGenericControl divrow = new HtmlGenericControl("div"), divcol = new HtmlGenericControl("div")
                                            , divformgroup = new HtmlGenericControl("div"), divcolblk = new HtmlGenericControl("div")
                                            , divaccordian1 = new HtmlGenericControl("div"), divaccordian2 = new HtmlGenericControl("div");
            HtmlGenericControl labelfor = new HtmlGenericControl("label");
            HtmlGenericControl a = new HtmlGenericControl("a");
            HtmlGenericControl ic = new HtmlGenericControl("i");

            //Header
            divrow = new HtmlGenericControl("div");
            divrow.Attributes.Add("class", "row");

            divcol = new HtmlGenericControl("div");
            divcol.Attributes.Add("class", "col-md-12");

            divformgroup = new HtmlGenericControl("div");
            divformgroup.Attributes.Add("class", "form-group formgroupBottom accordionDiv1");
            //divformgroup.Attributes.Add("id", "inv" + invoiceID);
            //divformgroup.Attributes.Add("href", "#collapse" + "inv" + invoiceID);
            //divformgroup.Attributes.Add("data-toggle", "collapse");
            //divformgroup.Attributes.Add("data-parent", "#accordion");
            a = new HtmlGenericControl("a");
            a.Attributes.Add("class", "form-group formgroupBottom accordionHeader1" + "" + " confAccordionHeaderTextStyle");//"collapsed"
            a.Attributes.Add("id", "invPending1");
            a.Attributes.Add("href", "#collapse" + "invPending1");
            a.Attributes.Add("data-toggle", "collapse");
            a.Attributes.Add("data-parent", "#accordion");
            divformgroup.Controls.Add(a);

            a = new HtmlGenericControl("a");
            a.Attributes.Add("class", "confAccordionHeaderTextStyle confAccordionPendingFont");//"collapsed"
            a.InnerText = "Invoice - " + "Pending";
            divformgroup.Controls.Add(a);
            divcol.Controls.Add(divformgroup);
            divrow.Controls.Add(divcol);
            divShowPendingOrderList.Controls.Add(divrow);

            //Content
            divaccordian1 = new HtmlGenericControl("div");
            divaccordian1.Attributes.Add("id", "collapse" + "invPending1");
            divaccordian1.Attributes.Add("class", "panel-collapse collapse" + " in" + " confAccordionContent");

            divrow = new HtmlGenericControl("div");
            divrow.Attributes.Add("class", "row");

            divcol = new HtmlGenericControl("div");
            divcol.Attributes.Add("class", "col-md-12");

            divformgroup = new HtmlGenericControl("div");
            divformgroup.Attributes.Add("class", "form-group formgroupBottom");

            divcolblk = new HtmlGenericControl("div");
            divcolblk.Attributes.Add("class", "col-md-2 col2Width");
            divcolblk.InnerHtml = "&nbsp;";
            divformgroup.Controls.Add(divcolblk);

            Label lblOrderList = new Label();
            lblOrderList.Attributes.Add("id", "orderlist" + "invPending1");
            lblOrderList.Attributes.Add("runat", "server");
            lblOrderList.Text = pendingOrderList;//***
            divformgroup.Controls.Add(lblOrderList);
            divcol.Controls.Add(divformgroup);
            divrow.Controls.Add(divcol);
            divaccordian1.Controls.Add(divrow);
            divShowPendingOrderList.Controls.Add(divaccordian1);

            dcSubTotal += cFun.ParseDecimal(subTotal);//***
        }
        #endregion

        #region bindPaidOrderList
        string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
        List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
        if (invListObj != null && invListObj.Count > 0)
        {
            string iscollasped = " collapsed";
            string contentin = "";
            if (dcSubTotal == 0)
            {
                iscollasped = "";
                contentin = " in";
            }

            int count = 1;
            foreach (Invoice invObj in invListObj)
            {
                string invoiceID = invObj.InvoiceID;
                string invoiceNo = invObj.InvoiceNo;
                string paymentMethod = invObj.PaymentMethod;

                HtmlGenericControl divrow = new HtmlGenericControl("div"), divcol = new HtmlGenericControl("div")
                                                , divformgroup = new HtmlGenericControl("div"), divcolblk = new HtmlGenericControl("div")
                                                , divaccordian1 = new HtmlGenericControl("div"), divaccordian2 = new HtmlGenericControl("div");
                HtmlGenericControl labelfor = new HtmlGenericControl("label");
                HtmlGenericControl a = new HtmlGenericControl("a");
                HtmlGenericControl ic = new HtmlGenericControl("i");

                //Header
                divrow = new HtmlGenericControl("div");
                divrow.Attributes.Add("class", "row");

                divcol = new HtmlGenericControl("div");
                divcol.Attributes.Add("class", "col-md-12");

                divformgroup = new HtmlGenericControl("div");
                divformgroup.Attributes.Add("class", "form-group formgroupBottom accordionDiv1");
                //divformgroup.Attributes.Add("id", "inv" + invoiceID);
                //divformgroup.Attributes.Add("href", "#collapse" + "inv" + invoiceID);
                //divformgroup.Attributes.Add("data-toggle", "collapse");
                //divformgroup.Attributes.Add("data-parent", "#accordion");
                a = new HtmlGenericControl("a");
                a.Attributes.Add("class", "form-group formgroupBottom accordionHeader1" + iscollasped + " confAccordionHeaderTextStyle");//
                a.Attributes.Add("id", "inv" + invoiceID);
                a.Attributes.Add("href", "#collapse" + "inv" + invoiceID);
                a.Attributes.Add("data-toggle", "collapse");
                a.Attributes.Add("data-parent", "#accordion");
                divformgroup.Controls.Add(a);

                a = new HtmlGenericControl("a");
                a.Attributes.Add("class", "confAccordionHeaderTextStyle");//"collapsed"
                a.InnerText = "Invoice - " + invoiceNo;
                divformgroup.Controls.Add(a);
                divcol.Controls.Add(divformgroup);
                divrow.Controls.Add(divcol);
                divShowOrderList.Controls.Add(divrow);

                //Content
                divaccordian1 = new HtmlGenericControl("div");
                divaccordian1.Attributes.Add("id", "collapse" + "inv" + invoiceID);
                divaccordian1.Attributes.Add("class", "panel-collapse collapse" + contentin + " confAccordionContent");//

                divrow = new HtmlGenericControl("div");
                divrow.Attributes.Add("class", "row");

                divcol = new HtmlGenericControl("div");
                divcol.Attributes.Add("class", "col-md-12");

                divformgroup = new HtmlGenericControl("div");
                divformgroup.Attributes.Add("class", "form-group formgroupBottom");

                divcolblk = new HtmlGenericControl("div");
                divcolblk.Attributes.Add("class", "col-md-2 col2Width");
                divcolblk.InnerHtml = "&nbsp;";
                divformgroup.Controls.Add(divcolblk);

                Label lblOrderList = new Label();
                lblOrderList.Attributes.Add("id", "orderlist" + invoiceID);
                lblOrderList.Attributes.Add("runat", "server");
                lblOrderList.Text = LoadOrderedList(urlQuery, ref subTotal, invoiceID);//***
                divformgroup.Controls.Add(lblOrderList);
                divcol.Controls.Add(divformgroup);
                divrow.Controls.Add(divcol);
                divaccordian1.Controls.Add(divrow);

                divrow = new HtmlGenericControl("div");
                divrow.Attributes.Add("class", "row confdivPayMethod");

                divcol = new HtmlGenericControl("div");
                divcol.Attributes.Add("class", "col-md-12");

                divformgroup = new HtmlGenericControl("div");
                divformgroup.Attributes.Add("class", "form-group");

                divcolblk = new HtmlGenericControl("div");
                divcolblk.Attributes.Add("class", "col-md-3");
                Label lbl = new Label();
                lbl.ID = "lblPaymentMethod";
                lbl.Attributes.Add("runat", "server");
                lbl.CssClass = "form-control-label confPayMethod";
                lbl.Text = "Payment Method";
                divcolblk.Controls.Add(lbl);
                divformgroup.Controls.Add(divcolblk);

                divcolblk = new HtmlGenericControl("div");
                divcolblk.Attributes.Add("class", "col-md-1");
                divcolblk.InnerText = ":";
                divformgroup.Controls.Add(divcolblk);

                divcolblk = new HtmlGenericControl("div");
                divcolblk.Attributes.Add("class", "col-md-3");
                lbl = new Label();
                lbl.ID = "lblPaymentMethod" + count;
                lbl.Attributes.Add("runat", "server");
                lbl.CssClass = "form-control-label confPayMethod";
                lbl.Text = bindPaymentMode(paymentMethod);//***
                divcolblk.Controls.Add(lbl);
                divformgroup.Controls.Add(divcolblk);
                divcol.Controls.Add(divformgroup);
                divrow.Controls.Add(divcol);
                divaccordian1.Controls.Add(divrow);
                divShowOrderList.Controls.Add(divaccordian1);

                dcSubTotal += cFun.ParseDecimal(subTotal);//***
                count++;
            }
        }
        #endregion

        subTotal = cFun.FormatCurrency(dcSubTotal.ToString());
        lblGrandTotal.Text = dcSubTotal.ToString();

        Double total = Convert.ToDouble(lblGrandTotal.Text);
        if (total > 0 || (!string.IsNullOrEmpty(pendingOrderList) && ((inv != null && !string.IsNullOrEmpty(inv.InvoiceID)) || (invListObj != null && invListObj.Count > 0))))
        {
            rcontent.Visible = true;
        }
        else
        {
            rcontent.Visible = false;
        }
    }
    private string LoadOrderedList(FlowURLQuery urlQuery, ref string SubTotal, string invoiceID)
    {
        string template = string.Empty;
        try
        {
            OrderControler oControl = new OrderControler(fn);
            template = oControl.GetOrderedListWithTemplate(urlQuery, ref SubTotal, invoiceID);
        }
        catch (Exception ex)
        { }

        return template;
    }
    private void checkExistAdditionalItems(string groupid, string regno, string flowid, string showid, string currentstage)
    {
        divSubmit.Visible = false;

        FlowControler flwObj = new FlowControler(fn, flowid, currentstage);
        string page = flwObj.CurrIndexModule;
        InvoiceControler invControler = new InvoiceControler(fn);
        StatusSettings statusSet = new StatusSettings(fn);
        string invStatus = invControler.getInvoiceStatus(regno);
        if (invStatus == statusSet.Success.ToString())
        {
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
            ConferenceControler confCtrl = new ConferenceControler(fn);
            ConfBuyableItemsList buyableConfItemsList = confCtrl.getAllBuyableConfDepConfItems(invOwnerID, flowid, showid);
            if (buyableConfItemsList != null)
            {
                //if (buyableConfItemsList.ConfItemsList != null)
                if (buyableConfItemsList.ConfItemsList.Count > 0)
                {
                    divSubmit.Visible = true;
                }
            }
        }
    }
    #endregion

    #region btnSubmit_Click
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        #region old (comment on 8-9-2019)
        //if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
        //{
        //    string groupid = Session["Groupid"].ToString();
        //    string flowid = Session["Flowid"].ToString();
        //    string showid = Session["Showid"].ToString();
        //    string currentstage = cFun.DecryptValue(urlQuery.CurrIndex);
        //    if (Session["Regno"] != null)
        //    {
        //        string regno = Session["Regno"].ToString();
        //        FlowControler flwObj = new FlowControler(fn, urlQuery);
        //        string page = flwObj.GetFrontendPageByStep(flowid,currentstage);

        //        if (!string.IsNullOrEmpty(page))
        //        {
        //            string step = currentstage;
        //            string route = flwObj.MakeFullURL(page, flowid, showid, groupid, step, regno, BackendRegType.backendRegType_Delegate);
        //            Response.Redirect(route, false);
        //        }
        //        else
        //        {
        //            Response.Redirect("404.aspx");
        //        }
        //    }
        //    else
        //    {
        //        Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
        //    }
        //}
        //else
        //{
        //    Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
        //}
        #endregion
        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["Regno"] != null)
        {
            string groupid = Session["Groupid"].ToString();
            string flowid = Session["Flowid"].ToString();
            string showid = Session["Showid"].ToString();
            string rID = Session["Regno"].ToString();
            string FlowID = cFun.EncryptValue(flowid);
            string showID = cFun.EncryptValue(showid);
            string grpNum = cFun.EncryptValue(groupid);
            string regno = cFun.EncryptValue(rID);
            FlowControler flwObj = new FlowControler(fn);
            flwObj.FillLoginStartIndex(FlowID);
            string route = flwObj.MakeFullURL(flwObj.CurrIndexModule, FlowID, showID, grpNum, flwObj.CurrIndex, regno);
            Response.Redirect(route);
        }
        else
        {
            Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
        }
    }
    #endregion

    #region bindPaymentMode (bind payment methods data from ref_PaymentMethod table to rbPaymentMode control) //***edit on 23-3-2018
    private string bindPaymentMode(string paymentMethod)
    {
        string paymentName = string.Empty;
        try
        {
            if (paymentMethod == ((int)PaymentType.CreditCard).ToString())
            {
                paymentName = RegClass.strCreditCard;
            }
            if (paymentMethod == ((int)PaymentType.TT).ToString())
            {
                paymentName = RegClass.strTT;
            }
            if (paymentMethod == ((int)PaymentType.Waved).ToString())
            {
                paymentName = RegClass.strWaived;
            }
            if (paymentMethod == ((int)PaymentType.Cheque).ToString())
            {
                paymentName = RegClass.strCheque;
            }
            //lblmethod.Text = paymentName;
        }
        catch (Exception ex)
        { }

        return paymentName;
    }
    #endregion

}