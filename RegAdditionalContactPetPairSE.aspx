﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="RegAdditionalContactPetPairSE.aspx.cs" Inherits="RegAdditionalContactPetPairSE" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" type="text/css" href="Content/StyleQA.css" />
     <style>
        .red
        {
            color:red;
        }
        /*.chkFooter
        {
            white-space:nowrap;
        }*/
        .chkFooter label
        {
            font-weight:unset !important;
            padding-left:5px !important;
            vertical-align:top !important;
        }
        .chkFooter input[type="checkbox"]
        {
            margin-top: 2px !important;
        }
        a:hover
        {
            text-decoration:none !important;
        }
        .rowLabel
        {
            padding-top: 7px;
        }
    </style>

    <script type="text/javascript">
        function validateDate(elementRef) {
                var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

                var dateValue = elementRef.value;

                if (dateValue.length != 10) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                // mm/dd/yyyy format... 
                var valueArray = dateValue.split('/');

                if (valueArray.length != 3) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthValue = parseFloat(valueArray[1]);
                var dayValue = parseFloat(valueArray[0]);
                var yearValue = parseFloat(valueArray[2]);

                if ((isNaN(monthValue) == true) || (isNaN(dayValue) == true) || (isNaN(yearValue) == true)) {
                    alert('Non-numeric entry detected\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                if (((yearValue % 4) == 0) && (((yearValue % 100) != 0) || ((yearValue % 400) == 0)))
                    monthDays[1] = 29;
                else
                    monthDays[1] = 28;

                if ((monthValue < 1) || (monthValue > 12)) {
                    alert('Invalid month entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthDaysArrayIndex = monthValue - 1;
                if ((dayValue < 1) || (dayValue > monthDays[monthDaysArrayIndex])) {
                    alert('Invalid day entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

            return true;
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divHDR1" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblHDR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR2" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblHDR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR3" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblHDR3" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>

            <div id="divReg" runat="server"  class="form-group">
                <div class="form-group row" runat="server" id="div2">
                    <div class="col-md-11 col-md-offset-1">
                    Stand Coordinator person to contact regarding general inquiries about the event
                    </div>
                </div>
                <div class="form-group row" runat="server" id="divFName">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblFName" runat="server" CssClass="form-control-label" Text="Name"></asp:Label><span class="red">*</span>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcFName" runat="server"
                        ControlToValidate="txtFName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revFName" runat="server" ControlToValidate="txtFName" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divLName">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblLName" runat="server" CssClass="form-control-label" Text="Position"></asp:Label><span class="red">*</span>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtLName" runat="server" CssClass="form-control" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcLName" runat="server"
                        ControlToValidate="txtLName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revLName" runat="server" ControlToValidate="txtLName" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divOName">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblOName" runat="server" CssClass="form-control-label" Text="E-mail"></asp:Label><span class="red">*</span>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtOName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcOName" runat="server"
                        ControlToValidate="txtOName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                        runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                        ControlToValidate="txtOName"
                        ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divTel">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblTel" runat="server" CssClass="form-control-label" Text="Phone"></asp:Label><span class="red">*</span>
                    </div>
                    <div class="col-md-5" runat="server" >
                        <div class="col-xs-3" runat="server" id="divTelcc" style="padding-left:0px;" visible="false">
                            <asp:TextBox ID="txtTelcc" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Country Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTelcc" runat="server"
                                TargetControlID="txtTelcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-3" runat="server" id="divTelac" style="padding-left:0px;" visible="false">
                            <asp:TextBox ID="txtTelac" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Area Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTelac" runat="server"
                                TargetControlID="txtTelac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-12" runat="server" id="divTelNo" style="padding-right:0px;padding-left:0px;">
                            <asp:TextBox ID="txtTel" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Telephone Number"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTel" runat="server"
                                TargetControlID="txtTel"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcTel" runat="server"
                        ControlToValidate="txtTel" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcTelcc" runat="server" Enabled="false"
                        ControlToValidate="txtTelcc" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcTelac" runat="server" Enabled="false"
                        ControlToValidate="txtTelac" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="div3">
                    <div class="col-md-11 col-md-offset-1">
                    CEO / Managing Director for VIP invitations by the government, ministries, and for C-level events
                    </div>
                </div>
                <div class="form-group row" runat="server" id="divIDNo">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblIDNo" runat="server" CssClass="form-control-label" Text="Name"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtIDNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcIDNo" runat="server" Enabled="false"
                        ControlToValidate="txtIDNo" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revIDNo" runat="server" ControlToValidate="txtIDNo" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divJobtitle">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblJobtitle" runat="server" CssClass="form-control-label" Text="Position"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtJobtitle" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcJobtitle" runat="server" Enabled="false"
                        ControlToValidate="txtJobtitle" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revJobtitle" runat="server" ControlToValidate="txtJobtitle" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divEmail">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text="E-mail"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:RequiredFieldValidator ID="vcEmail" runat="server" Enabled="false"
                        ControlToValidate="txtEmail" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="validateEmail"
                        runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                        ControlToValidate="txtEmail"
                        ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divFax">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblFax" runat="server" CssClass="form-control-label" Text="Phone"></asp:Label>
                    </div>
                    <div class="col-md-5" runat="server" >
                        <div class="col-xs-3" runat="server" id="divFaxcc" style="padding-left:0px;" visible="false">
                            <asp:TextBox ID="txtFaxcc" runat="server" CssClass="form-control" placeholder="Country Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFaxcc" runat="server"
                                TargetControlID="txtFaxcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-3" runat="server" id="divFaxac" style="padding-left:0px;" visible="false">
                            <asp:TextBox ID="txtFaxac" runat="server" CssClass="form-control" placeholder="Area Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFaxac" runat="server"
                                TargetControlID="txtFaxac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-12" runat="server" id="divFaxNo" style="padding-right:0px;padding-left:0px;">
                            <asp:TextBox ID="txtFax" runat="server" CssClass="form-control" placeholder="Phone Number"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFax" runat="server"
                                TargetControlID="txtFax"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcFax" runat="server" Enabled="false"
                        ControlToValidate="txtFax" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcFaxcc" runat="server" Enabled="false"
                        ControlToValidate="txtFaxcc" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcFaxac" runat="server" Enabled="false"
                        ControlToValidate="txtFaxac" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="div4">
                    <div class="col-md-11 col-md-offset-1">
                    PR / Marketing Manager for business media inquiries
                    </div>
                </div>
                <div class="form-group row" runat="server" id="divAddress1">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblAddress1" runat="server" CssClass="form-control-label" Text="Name"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAddress1" runat="server" Enabled="false"
                        ControlToValidate="txtAddress1" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAddress1" runat="server" ControlToValidate="txtAddress1" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress2">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblAddress2" runat="server" CssClass="form-control-label" Text="Position"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAddress2" runat="server" Enabled="false"
                        ControlToValidate="txtAddress2" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAddress2" runat="server" ControlToValidate="txtAddress2" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress3">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblAddress3" runat="server" CssClass="form-control-label" Text="E-mail"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAddress3" runat="server" Enabled="false"
                        ControlToValidate="txtAddress3" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                        runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                        ControlToValidate="txtAddress3"
                        ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divMobile">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblMobile" runat="server" CssClass="form-control-label" Text="Phone"></asp:Label>
                    </div>
                    <div class="col-md-5" runat="server" >
                        <div class="col-xs-3" runat="server" id="divMobcc" style="padding-left:0px;" visible="false">
                            <asp:TextBox ID="txtMobcc" runat="server" CssClass="form-control" placeholder="Country Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobcc" runat="server"
                                TargetControlID="txtMobcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-3" runat="server" id="divMobac" style="padding-left:0px;" visible="false">
                            <asp:TextBox ID="txtMobac" runat="server" CssClass="form-control" placeholder="Area Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobac" runat="server"
                                TargetControlID="txtMobac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-12" runat="server" id="divMobileNo" style="padding-right:0px;padding-left:0px;">
                            <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control" placeholder="Phone Number"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobile" runat="server"
                                TargetControlID="txtMobile"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcMob" runat="server" Enabled="false"
                        ControlToValidate="txtMobile" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcMobcc" runat="server" Enabled="false"
                        ControlToValidate="txtMobcc" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcMobac" runat="server" Enabled="false"
                        ControlToValidate="txtMobac" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="div5">
                    <div class="col-md-11 col-md-offset-1">
                    Spokesperson for interview requests
                    </div>
                </div>
                <div class="form-group row" runat="server" id="divCity">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblCity" runat="server" CssClass="form-control-label" Text="Name"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcCity" runat="server" Enabled="false"
                        ControlToValidate="txtCity" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revCity" runat="server" ControlToValidate="txtCity" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divState">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblState" runat="server" CssClass="form-control-label" Text="Position"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcState" runat="server" Enabled="false"
                        ControlToValidate="txtState" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revState" runat="server" ControlToValidate="txtState" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divPostalcode">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblPostalcode" runat="server" CssClass="form-control-label" Text="E-mail"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtPostalcode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcPostalcode" runat="server" Enabled="false"
                        ControlToValidate="txtPostalcode" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3"
                        runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                        ControlToValidate="txtAddress3"
                        ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>

                <div class="form-group row" runat="server" id="div1">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="Label1" runat="server" CssClass="form-control-label" Text="Phone"></asp:Label>
                    </div>
                    <div class="col-md-5" runat="server" >
                        <div class="col-xs-12" runat="server" id="divSpokenPhone" style="padding-right:0px;padding-left:0px;">
                            <asp:TextBox ID="txtSpokenPhone" runat="server" CssClass="form-control" placeholder="Phone Number"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                TargetControlID="txtSpokenPhone"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Enabled="false"
                        ControlToValidate="txtSpokenPhone" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>

            <br /><br />
            <div id="divFTRCHK1" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:CheckBoxList ID="chkFTRCHK1" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                        <asp:Label ID="lblErrFTRCHK1" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divFTR1" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblFTR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divFTR2" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblFTR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divFTRCHK" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:CheckBoxList ID="chkFTRCHK" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                        <%--<asp:CheckBox ID="chkFTRCHK" runat="server" />&nbsp;&nbsp;<asp:Label ID="lblFTRCHK" runat="server"></asp:Label>
                        <asp:Label ID="lblFTRCHKIsSkip" runat="server" Visible="false" Text="0"></asp:Label>--%>
                        <asp:Label ID="lblErrFTRCHK" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-group container" style="padding-top:20px;">
                  <asp:Panel runat="server" ID="PanelShowBackButton" Visible="false" >
                      <div class="col-lg-2 col-sm-offset-4 col-sm-2 center-block" >
                        <asp:Button runat="server" ID="btnBack" CssClass="btn MainButton btn-block" Text="Cancel" OnClick="btnBack_Click" CausesValidation="false"/>
                    </div>
                      <div class="col-lg-2  col-sm-2 center-block" >
                        <asp:Button runat="server" ID="btnSave2" CssClass="btn MainButton btn-block" Text="Next" OnClick="btnSave_Click" />
                    </div>
                  </asp:Panel>
                     <asp:Panel runat="server" ID="PanelWithoutBack" Visible="true" >
                    <div class="col-lg-offset-5 col-lg-3 col-sm-offset-4 col-sm-3 center-block" >
                        <asp:Button runat="server" ID="btnSave" CssClass="btn MainButton btn-block" Text="Next" OnClick="btnSave_Click" />
                    </div>
                   </asp:Panel>
                </div>
            </div>

            <asp:HiddenField ID="hfRegno" runat="server" />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

