﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
using Corpit.BackendMaster;
using System.Data;

public partial class RegistrationTransporta : System.Web.UI.MasterPage
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            ShowController shwCtrl = new ShowController(fn);
            if (shwCtrl.checkValidShow(showid))
            {
                SiteSettings sSetting = new SiteSettings(fn, showid);
                sSetting.LoadBaseSiteProperties(showid);

                if (Request.Params["t"] != null)
                {
                    string admintype = cFun.DecryptValue(Request.QueryString["t"].ToString());
                    if (admintype == BackendStaticValueClass.isWebSettings)
                    {
                    }
                    else
                    {
                        if (sSetting.isRegisterClosed())
                        {
                            Response.Redirect("RegClosed.aspx?SHW=" + cFun.EncryptValue(showid));
                        }
                    }
                }
                else
                {
                    if (sSetting.isRegisterClosed())
                    {
                        Response.Redirect("RegClosed.aspx?SHW=" + cFun.EncryptValue(showid));
                    }
                }

                if (!IsPostBack)
                {
                    InitSiteSettings(showid);
                    LoadPageSetting();
                }

                if (sSetting.hastimer == "1")
                {
                    timerSession.Enabled = true;
                    if (Session["timeout"] != null)
                    {
                        timerSession_Tick(this, null);
                    }
                    else
                    {
                        mpeExpired.Show();
                    }
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        SiteBanner.ImageUrl = sSetting.SiteBanner;
        Global.PageTitle = sSetting.SiteTitle;
        Global.Copyright = sSetting.SiteFooter;
        Global.PageBanner = sSetting.SiteBanner;

        if (!string.IsNullOrEmpty(sSetting.SiteCssBandlePath))
            BundleRefence.Path = "~/Content/" + sSetting.SiteCssBandlePath + "/";

        //Show hide Banner
        ShowControler showControler = new ShowControler(fn);
        Show sh = showControler.GetShow(showid);
        if (sh.HideShowBanner == "1")
            SiteBanner.Visible = false;
    }
    private void LoadPageSetting()
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        SetPageSettting(flwObj.CurrIndexTiTle);
    }

    public void SetPageSettting(string title)
    {
        SiteHeader.Text = title;
    }

    #endregion

    #region Timer
    protected void timerSession_Tick(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            if (Session["timeout"] != null)
            {
                SiteSettings st = new SiteSettings(fn, showid);
                if (st.hastimer_type == TimerType.isSecond)//*(TimerType.isSecond=1)
                {
                    DateTime dttimeout = Convert.ToDateTime(Session["timeout"].ToString());
                    int secondsleft = (int)decimal.Parse(dttimeout.Subtract(DateTime.Now).TotalSeconds.ToString()) + 1;

                    lblTimerSecond.Text = secondsleft.ToString();

                    if (Session["isextended"] == null)
                    {
                        if (secondsleft <= 2 && secondsleft > 1)
                        {
                            Session["isextended"] = "1";
                            mpeSession.Show();
                        }
                    }

                    if (secondsleft <= 0)
                    {
                        Session["timeout"] = null;
                        Session["isextended"] = null;

                        Session.Clear();
                        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri.ToString());
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "@#$", "getSecondsRemaining();", true);
                }
                else//*(TimerType.isMinute=2)
                {
                    #region With Minutes (Comment)
                    DateTime dttimeout = Convert.ToDateTime(Session["timeout"].ToString());
                    int minutesleft = (int)decimal.Parse(dttimeout.Subtract(DateTime.Now).TotalMinutes.ToString()) + 1;

                    lblTimer.Text = minutesleft.ToString();
                    if (Session["isextended"] == null)
                    {
                        if (minutesleft <= 2 && minutesleft > 1)
                        {
                            Session["isextended"] = "1";
                            mpeSession.Show();
                        }
                    }

                    if (minutesleft <= 0)
                    {
                        Session["timeout"] = null;
                        Session["isextended"] = null;

                        Session.Clear();
                        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri.ToString());
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "@#$", "getMinutesRemaining();", true);
                    #endregion
                }
            }
            else
            {
                String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    protected void ButtonPop_Click(object sender, EventArgs e)
    {
        mpeExpired.Show();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        mpeSession.Show();
    }

    protected void btnOkay_Click(object sender, EventArgs e)
    {
        Session["timeout"] = DateTime.Now.AddMinutes(12).ToString();
        timerSession_Tick(this, null);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "@#$", "getMinutesRemaining();", true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect("Welcome.aspx");
    }

    protected void btnStartAgain_Click(object sender, EventArgs e)
    {
        Response.Redirect("Welcome.aspx");
    }

    protected void btnVisit_Click(object sender, EventArgs e)
    {
        String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
        String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
    }
    #endregion
}
