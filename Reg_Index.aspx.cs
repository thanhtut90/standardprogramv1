﻿using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Reg_Index : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    private static string _vendorRegistration = "Vendor Registration";
    private static string pageTitle = "Re-login Detail";
    protected string showname;
    protected string flowDesc;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
        {
            Label mpLabel = (Label)Page.Master.FindControl("SiteHeader");
            if (mpLabel != null)
            {
                mpLabel.Text = pageTitle;//ReLoginStaticValueClass.regIndexName;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string currentstage = Session["regstage"].ToString();

                FlowControler Flw = new FlowControler(fn);

                bindMsg(flowid);//***added on 9-10-2018

                //***added on 7-9-2018
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);
                showname = shw.SHW_Name;//.ToLower();
                //***added on 7-9-2018

                //***added on 7-9-2018
                FlowMaster flwMaster = Flw.GetFlowMasterConfig(flowid);
                flowDesc = flwMaster.FlowDesc;
                //***added on 7-9-2018

                FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowid);
                string regType = BackendRegType.backendRegType_Delegate;
                if (flwMaster.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    regType = BackendRegType.backendRegType_Group;
                }

                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    string page = ReLoginStaticValueClass.regDelegateIndexPage;
                    if(shw.SHW_Name == _vendorRegistration)
                    {
                        page = "RegDelegate_IndexV.aspx";
                    }
                    currentstage = Flw.GetConirmationStage(flowid, SiteDefaultValue.constantRegDelegateIndexPage);

                    string route = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage, "", regType);
                    hpLink.NavigateUrl = route;

                    //***added on 9-10-2018 (set link in message template)
                    if (divReloginIndexMsg.Visible == true)
                    {
                        HtmlAnchor aModal = divReloginIndexMsg.FindControl("aLink") as HtmlAnchor;
                        if(aModal != null)
                        {
                            aModal.Attributes.Remove("href");
                            aModal.Attributes.Add("href", route);
                        }
                        aLink.NavigateUrl = route;
                    }
                    //***added on 9-10-2018 (set link in message template)
                }
                else
                {
                    bool confExist = Flw.checkPageExist(flowid, SiteDefaultValue.constantConfName);
                    if (confExist)
                    {
                        FlowControler flwObj = new FlowControler(fn, flowid, currentstage);
                        string page = flwObj.CurrIndexModule;

                        if (Session["Regno"] != null)
                        {
                            string regno = Session["Regno"].ToString();

                            InvoiceControler invControler = new InvoiceControler(fn);
                            StatusSettings stuSettings = new StatusSettings(fn);

                            string invStatus = invControler.getInvoiceStatus(regno);
                            if (invStatus == stuSettings.Pending.ToString()
                                || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                            {
                                page = ReLoginStaticValueClass.regConfirmationPage;
                                currentstage = Flw.GetConirmationStage(flowid, SiteDefaultValue.constantConfirmationPage);

                                string route = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage, regno, regType);
                                hpLink.NavigateUrl = route;

                                //***added on 9-10-2018 (set link in message template)
                                if (divReloginIndexMsg.Visible == true)
                                {
                                    HtmlAnchor aModal = divReloginIndexMsg.FindControl("aLink") as HtmlAnchor;
                                    if (aModal != null)
                                    {
                                        aModal.Attributes.Remove("href");
                                        aModal.Attributes.Add("href", route);
                                    }
                                }
                                //***added on 9-10-2018 (set link in message template)
                            }
                            else
                            {
                                string route = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage, regno, regType);
                                hpLink.NavigateUrl = route;

                                //***added on 9-10-2018 (set link in message template)
                                if (divReloginIndexMsg.Visible == true)
                                {
                                    HtmlAnchor aModal = divReloginIndexMsg.FindControl("aLink") as HtmlAnchor;
                                    if (aModal != null)
                                    {
                                        aModal.Attributes.Remove("href");
                                        aModal.Attributes.Add("href", route);
                                    }
                                    aLink.NavigateUrl = route;
                                }
                                //***added on 9-10-2018 (set link in message template)
                            }
                        }
                    }
                    else
                    {
                        FlowControler flwObj = new FlowControler(fn, flowid, currentstage);
                        string page = flwObj.CurrIndexModule;

                        FlowMaster flwConfig = Flw.GetFlowMasterConfig(cFun.DecryptValue(flowid));
                        if (flwConfig.FlowLimitDelegateQty == YesOrNo.Yes)
                        {
                            string route = flwObj.MakeFullURL(page, flowid, showid, groupid, currentstage, "", regType);
                            hpLink.NavigateUrl = route;

                            //***added on 9-10-2018 (set link in message template)
                            if (divReloginIndexMsg.Visible == true)
                            {
                                HtmlAnchor aModal = divReloginIndexMsg.FindControl("aLink") as HtmlAnchor;
                                if (aModal != null)
                                {
                                    aModal.Attributes.Remove("href");
                                    aModal.Attributes.Add("href", route);
                                }
                                aLink.NavigateUrl = route;
                            }
                            //***added on 9-10-2018 (set link in message template)
                        }
                        else
                        {
                            if (Session["Regno"] != null)
                            {
                                string regno = Session["Regno"].ToString();
                                string route = flwObj.MakeFullURL(page, flowid, showid, groupid, currentstage, regno, regType);
                                hpLink.NavigateUrl = route;

                                //***added on 9-10-2018 (set link in message template)
                                if (divReloginIndexMsg.Visible == true)
                                {
                                    HtmlAnchor aModal = divReloginIndexMsg.FindControl("aLink") as HtmlAnchor;
                                    if (aModal != null)
                                    {
                                        aModal.Attributes.Remove("href");
                                        aModal.Attributes.Add("href", route);
                                    }
                                    aLink.NavigateUrl = route;
                                }
                                //***added on 9-10-2018 (set link in message template)
                            }
                        }
                    }
                }
            }
        }
    }

    #region bindMsg
    private void bindMsg(string flowid)
    {
        FlowControler flwControl = new FlowControler(fn);
        FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
        if (!string.IsNullOrEmpty(flwMasterConfig.FlowReloginIndexIncompletePageMsg))
        {
            divMsg.Visible = false;
            divReloginIndexMsg.Visible = true;
            lblTemplate.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowReloginIndexIncompletePageMsg) ? flwMasterConfig.FlowReloginIndexIncompletePageMsg : "");
        }
        else
        {
            divMsg.Visible = true;
            divReloginIndexMsg.Visible = false;
        }
    }
    #endregion
}