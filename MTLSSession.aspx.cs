﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
public partial class MTLSSession : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Session"] != null)
            {
                LoadSpeakerList(Request.QueryString["Session"].ToString());
            }
        }

    }
    protected void btnBook_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["Session"] != null)
        {
            Dictionary<string, string> dr = GetShowIDByEventName("registerSharingSessions");
            if (dr.Count > 0)
                RouteTOSession(dr["ShowID"].ToString(), dr["FlowID"].ToString(), Request.QueryString["Session"].ToString());
        }
    }

    protected void btnHome_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("https://www.mtls.sg/");
    }
    protected void btnBack_Click(object sender, ImageClickEventArgs e)
    {
        if (Request.QueryString["Session"] != null)
        {
            string currLang = GetCurrentLang(Request.QueryString["Session"].ToString());
            Response.Redirect("https://www.mtls.sg" + currLang);
        }
    }
    private void LoadSpeakerList(string ID)
    {
        string sql = "select * from tb_Proj_MTLS_Session where sn=@SID";
        List<SqlParameter> spList = new List<SqlParameter>();
        SqlParameter sqID = new SqlParameter();
        sqID.SqlDbType = SqlDbType.NVarChar;
        sqID.Value = ID;
        sqID.ParameterName = "SID";

        spList.Add(sqID);
        Functionality fn = new Functionality();
        DataTable dt = fn.GetDatasetByCommand(sql, "DT", spList).Tables["DT"];
        if (dt.Rows.Count > 0)
        {

            RptSpeaker.DataSource = dt;
            RptSpeaker.DataBind();
            PanelSess.Visible = true;
            lblSynosis_EL.Text = dt.Rows[0]["Synopsis_EL"].ToString();
            lblSynosis_OL.Text = dt.Rows[0]["Synopsis_OL"].ToString();
            lblTitle_EL.Text = dt.Rows[0]["Title_EL"].ToString();
            lblTitle_OL.Text = dt.Rows[0]["Title_OL"].ToString();
            tblFormat.Text = dt.Rows[0]["Format"].ToString();
        }
    }


    #region RptVale
    public string GetImgUrl(string imgFile)
    {
        string rtnVal = "";
        if (!string.IsNullOrEmpty(imgFile))
            rtnVal = "https://event-reg.biz/DefaultBanner/images/MTLS2019/Speaker/" + imgFile;
        return rtnVal;
    }
    public string GetFullName(string engName, string oName)
    {
        string rtnVal = "";
        rtnVal = engName;
        if (!string.IsNullOrEmpty(oName))
            rtnVal = rtnVal + string.Format(" ({0})", oName);
        return rtnVal;
    }
    #endregion

    #region Route
    private void RouteTOSession(string showID, string FlowID, string sessionID)
    {

        Functionality fn = new Functionality();
        CommonFuns cFun = new CommonFuns();
        SiteSettings st = new SiteSettings(fn, showID);
        string Type = ""; // Future Reg Close date can b vary depend on Delegate Type
        if (!st.IsRegClosed(Type))
        {
            string page = "";
            string step = "";
            FlowControler Flw = new FlowControler(fn);

            string grpNum = string.Empty;
            RegGroupObj rgp = new RegGroupObj(fn);
            grpNum = rgp.GenGroupNumber(showID);// GetRunNUmber("GRP_KEY");

            Dictionary<string, string> nValues = Flw.GetNextRoute(FlowID);
            if (nValues.Count > 0)
            {
                page = nValues["nURL"].ToString();
                step = nValues["nStep"].ToString();
                FlowID = nValues["FlowID"].ToString();
            }

            grpNum = cFun.EncryptValue(grpNum);
            string route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step);
            CommonFuns cFuz = new CommonFuns();
            route = route + "&MSession=" + cFuz.EncryptValue(sessionID);
            Response.Redirect(route);
        }

    }

    private Dictionary<string, string> GetShowIDByEventName(string eventName)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        try
        {

            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);
            Functionality fn = new Functionality();
            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                dic.Add("ShowID", dList.Rows[0]["showID"].ToString());
                dic.Add("FlowID", dList.Rows[0]["FLW_ID"].ToString());

            }

        }
        catch { }
        return dic;
    }



    #endregion

    private string GetCurrentLang(string session)
    {
        string rntval = "/sharesessions/";
        string currlan = session.Substring(0, 1);
        switch (currlan.ToUpper())
        {
            case "E": { rntval = rntval + "english/"; break; }
            case "C": { rntval = rntval + "chinese/"; break; }
            case "M": { rntval = rntval + "malay/"; break; }
            case "T": { rntval = rntval + "tamil/"; break; }
        }

        return rntval;
    }

}