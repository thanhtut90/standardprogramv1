﻿using Corpit.BackendMaster;
using Corpit.Registration;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegCompany_Edit : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    static string _Name = "Name";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _City = "City";
    static string _State = "State";
    static string _ZipCode = "Zip Code";
    static string _Country = "Country";
    static string _TelCC = "Telcc";
    static string _TelAC = "Telac";
    static string _Tel = "Tel";
    static string _FaxCC = "Faxcc";
    static string _FaxAC = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Website = "Website";
    static string _Additional1 = "Additional1";
    static string _Additional2 = "Additional2";
    static string _Additional3 = "Additional3";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                if (Request.Params["groupid"] != null)
                {
                    string groupid = cFun.DecryptValue(Request.QueryString["groupid"]);

                    if (!string.IsNullOrEmpty(flowid) && flowid != "0")
                    {
                        if(!setDynamicForm(flowid, cFun.DecryptValue(showid)))
                        {
                            Response.Redirect("MasterRegistrationList_Company.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("MasterRegistrationList_Company.aspx");
                    }
                }
                else
                {
                    Response.Redirect("MasterRegistrationList_Company.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = Request.QueryString["SHW"].ToString();

                if (Request.Params["groupid"] != null)
                {
                    string groupid = cFun.DecryptValue(Request.QueryString["groupid"]);

                    RegCompanyObj rcom = new RegCompanyObj(fn);
                    DataTable dt = rcom.getRegCompanyByID(groupid, cFun.DecryptValue(showid));

                    if (dt.Rows.Count > 0)
                    {
                        bindDropdown();
                        populateUserDetails(dt);
                    }
                    else
                    {
                        Response.Redirect("MasterRegistrationList_Company.aspx");
                    }
                }
                else
                {
                    Response.Redirect("MasterRegistrationList_Company.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region populateUserDetails
    private void populateUserDetails(DataTable dtCom)
    {
        if (dtCom.Rows.Count > 0)
        {
            hfRegGroupID.Value = dtCom.Rows[0]["RegGroupID"].ToString();

            string reggroupid = dtCom.Rows[0]["RegGroupID"].ToString();
            string rc_name = dtCom.Rows[0]["RC_Name"].ToString();
            string rc_address1 = dtCom.Rows[0]["RC_Address1"].ToString();
            string rc_address2 = dtCom.Rows[0]["RC_Address2"].ToString();
            string rc_address3 = dtCom.Rows[0]["RC_Address3"].ToString();
            string rc_city = dtCom.Rows[0]["RC_City"].ToString();
            string rc_state = dtCom.Rows[0]["RC_State"].ToString();
            string rc_zipcode = dtCom.Rows[0]["RC_ZipCode"].ToString();
            string rc_country = dtCom.Rows[0]["RC_Country"].ToString();
            string rc_telcc = dtCom.Rows[0]["RC_Telcc"].ToString();
            string rc_telac = dtCom.Rows[0]["RC_Telac"].ToString();
            string rc_tel = dtCom.Rows[0]["RC_Tel"].ToString();
            string rc_faxcc = dtCom.Rows[0]["RC_Faxcc"].ToString();
            string rc_faxac = dtCom.Rows[0]["RC_Faxac"].ToString();
            string rc_fax = dtCom.Rows[0]["RC_Fax"].ToString();
            string rc_email = dtCom.Rows[0]["RC_Email"].ToString();
            string rc_website = dtCom.Rows[0]["RC_Website"].ToString();
            string rc_createddate = dtCom.Rows[0]["RC_CreatedDate"].ToString();
            string recycle = dtCom.Rows[0]["recycle"].ToString();
            string rc_stage = dtCom.Rows[0]["RC_Stage"].ToString();

            string rc_additional1 = dtCom.Rows[0]["RC_Additional1"].ToString();
            string rc_additional2 = dtCom.Rows[0]["RC_Additional2"].ToString();
            string rc_additional3 = dtCom.Rows[0]["RC_Additional3"].ToString();
            string rc_additional4 = dtCom.Rows[0]["RC_Additional4"].ToString();
            string rc_additional5 = dtCom.Rows[0]["RC_Additional5"].ToString();

            txtName.Text = rc_name;
            txtAddress1.Text = rc_address1;
            txtAddress2.Text = rc_address2;
            txtAddress3.Text = rc_address3;
            txtCity.Text = rc_city;
            txtState.Text = rc_state;
            txtZipcode.Text = rc_zipcode;

            try
            {
                if (!String.IsNullOrEmpty(rc_country))
                {
                    ListItem listItem = ddlCountry.Items.FindByValue(rc_country);
                    if (listItem != null)
                    {
                        ddlCountry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtTelcc.Text = rc_telcc;
            txtTelac.Text = rc_telac;
            txtTel.Text = rc_tel;
            txtFaxcc.Text = rc_faxcc;
            txtFaxac.Text = rc_faxac;
            txtFax.Text = rc_fax;
            txtEmail.Text = rc_email;
            txtWebsite.Text = rc_website;

            txtAdditional1.Text = rc_additional1;
            txtAdditional2.Text = rc_additional2;
            txtAdditional3.Text = rc_additional3;
            txtAdditional4.Text = rc_additional4;
            txtAdditional5.Text = rc_additional5;
        }
    }
    #endregion

    #region btnupdate_Click (Update the respective data into "tb_RegGroup" table)
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        lbls.Text = "";
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            if (!string.IsNullOrEmpty(hfRegGroupID.Value))
            {
                UpdateDataCompany(cFun.DecryptValue(showid));
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region UpdateDataCompany & Load update the related data into tb_RegCompany table
    private void UpdateDataCompany(string showid)
    {
        if (Page.IsValid)
        {
            if (!cFun.validatePhoneCode(txtTelcc.Text.ToString())
                //|| !cFun.validatePhoneCode(txtTelac.Text.ToString())
                || !cFun.validatePhoneCode(txtFaxcc.Text.ToString())
                //|| !cFun.validatePhoneCode(txtFaxac.Text.ToString())
                )
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Phone code is not valid.');", true);
                return;
            }
            RegCompanyObj rgc = new RegCompanyObj(fn);

            string groupid = string.Empty;
            string name = string.Empty;
            string address1 = string.Empty;
            string address2 = string.Empty;
            string address3 = string.Empty;
            string city = string.Empty;
            string state = string.Empty;
            string zipcode = string.Empty;
            string country = string.Empty;
            string telcc = string.Empty;
            string telac = string.Empty;
            string tel = string.Empty;
            string faxcc = string.Empty;
            string faxac = string.Empty;
            string fax = string.Empty;
            string email = string.Empty;
            string website = string.Empty;
            //string createdate = "getdate()";
            string createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            int recycle = 0;
            string stage = string.Empty;

            string additional1 = string.Empty;
            string additional2 = string.Empty;
            string additional3 = string.Empty;
            string additional4 = string.Empty;
            string additional5 = string.Empty;

            groupid = hfRegGroupID.Value;

            name = cFun.solveSQL(txtName.Text.Trim());
            address1 = cFun.solveSQL(txtAddress1.Text.Trim());
            address2 = cFun.solveSQL(txtAddress2.Text.Trim());
            address3 = cFun.solveSQL(txtAddress3.Text.Trim());
            city = cFun.solveSQL(txtCity.Text.Trim());
            zipcode = cFun.solveSQL(txtZipcode.Text.Trim());
            state = cFun.solveSQL(txtState.Text.Trim());
            country = ddlCountry.SelectedItem.Text.ToString();
            telcc = txtTelcc.Text.ToString();
            telac = txtTelac.Text.ToString();
            tel = txtTel.Text.ToString();
            faxcc = txtFaxcc.Text.ToString();
            faxac = txtFaxac.Text.ToString();
            fax = txtFax.Text.ToString();
            email = cFun.solveSQL(txtEmail.Text.Trim());
            website = cFun.solveSQL(txtWebsite.Text.Trim());

            additional1 = cFun.solveSQL(txtAdditional1.Text.Trim());
            additional2 = cFun.solveSQL(txtAdditional2.Text.Trim());
            additional3 = cFun.solveSQL(txtAdditional3.Text.Trim());
            additional4 = cFun.solveSQL(txtAdditional4.Text.Trim());
            additional5 = cFun.solveSQL(txtAdditional5.Text.Trim());

            rgc.groupid = groupid;
            rgc.name = name;
            rgc.address1 = address1;
            rgc.address2 = address2;
            rgc.address3 = address3;
            rgc.city = city;
            rgc.state = state;
            rgc.zipcode = zipcode;
            rgc.country = country;
            rgc.telcc = telcc;
            rgc.telac = telac;
            rgc.tel = tel;
            rgc.faxcc = faxcc;
            rgc.faxac = faxac;
            rgc.fax = fax;
            rgc.email = email;
            rgc.website = website;
            rgc.createdate = createdate;
            rgc.recycle = recycle;
            rgc.stage = stage;
            rgc.additional1 = additional1;
            rgc.additional2 = additional2;
            rgc.additional3 = additional3;
            rgc.additional4 = additional4;
            rgc.additional5 = additional5;

            rgc.showID = showid;

            //*Update
            /*int isSuccess = 0;
            bool isAlreadyExist = rgc.checkUpdateExist();
            if (isAlreadyExist == true)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This user already exists!');", true);
                return;
            }
            else
            {
                isSuccess = rgc.updateRegCompany();
            }*/
            int isSuccess = 0;
            isSuccess = rgc.updateRegCompany();
            if (isSuccess > 0)
            {
                ClearForm();
                lbls.Text = "Success";

                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success.');window.location='MasterRegistrationList_Company.aspx';", true);
                return;
            }
            else
            {
                lbls.Text = "Something is wrong.";

                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fail.');window.location='MasterRegistrationList_Company.aspx';", true);
                return;
            }
        }
    }
    #endregion

    #region PanelKeyDetial (form controls portion) & set dynamic form controls visibility or invisibility and so on

    #region setDynamicForm (Generate Dynamic Form and use tb_Form)
    protected bool setDynamicForm(string flowid, string showid)
    {
        bool isValidShow = false;

        DataSet ds = new DataSet();
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForCompany();

        string formtype = FormType.TypeCompany;

        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
        {
            isValidShow = true;

            #region set divName visibility is true or false if form_input_name is Name according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Name)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divName.Visible = true;
                }
                else
                {
                    divName.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Name, formtype);
                if (isrequired == 1)
                {
                    lblName.Text = labelname + "<span class=\"red\">*</span>";
                    //txtName.Attributes.Add("required", "");
                    vcName.Enabled = true;
                }
                else
                {
                    lblName.Text = labelname;
                    //txtName.Attributes.Remove("required");
                    vcName.Enabled = false;
                }
            }
            #endregion

            #region set divAddress1 visibility is true or false if form_input_name is Address1 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress1.Visible = true;
                }
                else
                {
                    divAddress1.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Address1, formtype);
                if (isrequired == 1)
                {
                    lblAddress1.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAddress1.Attributes.Add("required", "");
                    vcAddress1.Enabled = true;
                }
                else
                {
                    lblAddress1.Text = labelname;
                    //txtAddress1.Attributes.Remove("required");
                    vcAddress1.Enabled = false;
                }
            }
            #endregion

            #region set divAddress2 visibility is true or false if form_input_name is Address2 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress2.Visible = true;
                }
                else
                {
                    divAddress2.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : "";// frmObj.getDefaultLableNameByInputNameType(_Address2, formtype);
                if (isrequired == 1)
                {
                    lblAddress2.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAddress2.Attributes.Add("required", "");
                    vcAddress2.Enabled = true;
                }
                else
                {
                    lblAddress2.Text = labelname;
                    //txtAddress2.Attributes.Remove("required");
                    vcAddress2.Enabled = false;
                }
            }
            #endregion

            #region set divAddress3 visibility is true or false if form_input_name is Address3 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress3.Visible = true;
                }
                else
                {
                    divAddress3.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : "";// frmObj.getDefaultLableNameByInputNameType(_Address3, formtype);
                if (isrequired == 1)
                {
                    lblAddress3.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAddress3.Attributes.Add("required", "");
                    vcAddress3.Enabled = true;
                }
                else
                {
                    lblAddress3.Text = labelname;
                    //txtAddress3.Attributes.Remove("required");
                    vcAddress3.Enabled = false;
                }
            }
            #endregion

            #region set divCity visibility is true or false if form_input_name is City according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCity.Visible = true;
                }
                else
                {
                    divCity.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_City, formtype);
                if (isrequired == 1)
                {
                    lblCity.Text = labelname + "<span class=\"red\">*</span>";
                    //txtCity.Attributes.Add("required", "");
                    vcCity.Enabled = true;
                }
                else
                {
                    lblCity.Text = labelname;
                    //txtCity.Attributes.Remove("required");
                    vcCity.Enabled = false;
                }
            }
            #endregion

            #region set divState visibility is true or false if form_input_name is State according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divState.Visible = true;
                }
                else
                {
                    divState.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_State, formtype);
                if (isrequired == 1)
                {
                    lblState.Text = labelname + "<span class=\"red\">*</span>";
                    //txtState.Attributes.Add("required", "");
                    vcState.Enabled = true;
                }
                else
                {
                    lblState.Text = labelname;
                    //txtState.Attributes.Remove("required");
                    vcState.Enabled = false;
                }
            }
            #endregion

            #region set divZipcode visibility is true or false if form_input_name is Zip Code according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _ZipCode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divZipcode.Visible = true;
                }
                else
                {
                    divZipcode.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_ZipCode, formtype);
                if (isrequired == 1)
                {
                    lblZipcode.Text = labelname + "<span class=\"red\">*</span>";
                    //txtZipcode.Attributes.Add("required", "");
                    vcZipcode.Enabled = true;
                }
                else
                {
                    lblZipcode.Text = labelname;
                    //txtZipcode.Attributes.Remove("required");
                    vcZipcode.Enabled = false;
                }
            }
            #endregion

            #region set divCountry visibility is true or false if form_input_name is Country according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCountry.Visible = true;
                }
                else
                {
                    divCountry.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Country, formtype);
                if (isrequired == 1)
                {
                    lblCountry.Text = labelname + "<span class=\"red\">*</span>";
                    //ddlCountry.Attributes.Add("required", "");
                    vcCountry.Enabled = true;
                }
                else
                {
                    lblCountry.Text = labelname;
                    //ddlCountry.Attributes.Remove("required");
                    vcCountry.Enabled = false;
                }
            }
            #endregion

            #region set divTelcc visibility is true or false if form_input_name is Telcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _TelCC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelcc.Visible = true;
                }
                else
                {
                    divTelcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtTelcc.Attributes.Add("required", "");
                    vcTelcc.Enabled = true;
                }
                else
                {
                    //txtTelcc.Attributes.Remove("required");
                    vcTelcc.Enabled = false;
                }
            }
            #endregion

            #region set divTelac visibility is true or false if form_input_name is Telac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _TelAC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelac.Visible = true;
                }
                else
                {
                    divTelac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtTelac.Attributes.Add("required", "");
                    vcTelac.Enabled = true;
                }
                else
                {
                    //txtTelac.Attributes.Remove("required");
                    vcTelac.Enabled = false;
                }
            }
            #endregion

            #region set divTel visibility is true or false if form_input_name is Tel according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTel.Visible = true;
                    divTelNo.Visible = true;
                }
                else
                {
                    divTel.Visible = false;
                    divTelNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Tel, formtype);
                if (isrequired == 1)
                {
                    lblTel.Text = labelname + "<span class=\"red\">*</span>";
                    //txtTel.Attributes.Add("required", "");
                    vcTel.Enabled = true;
                }
                else
                {
                    lblTel.Text = labelname;
                    //txtTel.Attributes.Remove("required");
                    vcTel.Enabled = false;
                }
            }
            #endregion

            #region set divFaxcc visibility is true or false if form_input_name is Faxcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _FaxCC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxcc.Visible = true;
                }
                else
                {
                    divFaxcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtFaxcc.Attributes.Add("required", "");
                    vcFaxcc.Enabled = true;
                }
                else
                {
                    //txtFaxcc.Attributes.Remove("required");
                    vcFaxcc.Enabled = false;
                }
            }
            #endregion

            #region set divFaxac visibility is true or false if form_input_name is Faxac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _FaxAC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxac.Visible = true;
                }
                else
                {
                    divFaxac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtFaxac.Attributes.Add("required", "");
                    vcFaxac.Enabled = true;
                }
                else
                {
                    //txtFaxac.Attributes.Remove("required");
                    vcFaxac.Enabled = false;
                }
            }
            #endregion

            #region set divFax visibility is true or false if form_input_name is Fax according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFax.Visible = true;
                    divFaxNo.Visible = true;
                }
                else
                {
                    divFax.Visible = false;
                    divFaxNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Fax, formtype);
                if (isrequired == 1)
                {
                    lblFax.Text = labelname + "<span class=\"red\">*</span>";
                    //txtFax.Attributes.Add("required", "");
                    vcFax.Enabled = true;
                }
                else
                {
                    lblFax.Text = labelname;
                    //txtFax.Attributes.Remove("required");
                    vcFax.Enabled = false;
                }
            }
            #endregion

            #region set divEmail visibility is true or false if form_input_name is Email according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmail.Visible = true;
                }
                else
                {
                    divEmail.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Email, formtype);
                if (isrequired == 1)
                {
                    lblEmail.Text = labelname + "<span class=\"red\">*</span>";
                    //txtEmail.Attributes.Add("required", "");
                    vcEmail.Enabled = true;
                }
                else
                {
                    lblEmail.Text = labelname;
                    //txtEmail.Attributes.Remove("required");
                    vcEmail.Enabled = false;
                }
            }
            #endregion

            #region set divEmailConfirmation visibility is true or false if form_input_name is ConfirmEmail according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _EmailConfirmation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmailConfirmation.Visible = true;
                    //txtEmailConfirmation.Attributes.Add("required", "");
                }
                else
                {
                    divEmailConfirmation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_EmailConfirmation, formtype);
                if (isrequired == 1)
                {
                    lblEmailConfirmation.Text = labelname + "<span class=\"red\">*</span>";
                    //txtEmailConfirmation.Attributes.Add("required", "");
                    vcEConfirm.Enabled = true;
                }
                else
                {
                    lblEmailConfirmation.Text = labelname;
                    //txtEmailConfirmation.Attributes.Remove("required");
                    vcEConfirm.Enabled = false;
                }
            }
            #endregion

            #region set divWebsite visibility is true or false if form_input_name is Website according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Website)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divWebsite.Visible = true;
                }
                else
                {
                    divWebsite.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Website, formtype);
                if (isrequired == 1)
                {
                    lblWebsite.Text = labelname + "<span class=\"red\">*</span>";
                    //txtWebsite.Attributes.Add("required", "");
                    vcWeb.Enabled = true;
                }
                else
                {
                    lblWebsite.Text = labelname;
                    //txtWebsite.Attributes.Remove("required");
                    vcWeb.Enabled = false;
                }
            }
            #endregion            

            #region set divAdditional1 visibility is true or false if form_input_name is Additional1 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional1.Visible = true;
                }
                else
                {
                    divAdditional1.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Additional1, formtype);
                if (isrequired == 1)
                {
                    lblAdditional1.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAdditional1.Attributes.Add("required", "");
                    vcAdditional1.Enabled = true;
                }
                else
                {
                    lblAdditional1.Text = labelname;
                    //txtAdditional1.Attributes.Remove("required");
                    vcAdditional1.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional2 visibility is true or false if form_input_name is Additional2 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional2.Visible = true;
                }
                else
                {
                    divAdditional2.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Additional2, formtype);
                if (isrequired == 1)
                {
                    lblAdditional2.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAdditional2.Attributes.Add("required", "");
                    vcAdditional2.Enabled = true;
                }
                else
                {
                    lblAdditional2.Text = labelname;
                    //txtAdditional2.Attributes.Remove("required");
                    vcAdditional2.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional3 visibility is true or false if form_input_name is Additional3 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional3.Visible = true;
                }
                else
                {
                    divAdditional3.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Additional3, formtype);
                if (isrequired == 1)
                {
                    lblAdditional3.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAdditional3.Attributes.Add("required", "");
                    vcAdditional3.Enabled = true;
                }
                else
                {
                    lblAdditional3.Text = labelname;
                    //txtAdditional3.Attributes.Remove("required");
                    vcAdditional3.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional4 visibility is true or false if form_input_name is Additional4 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional4.Visible = true;
                }
                else
                {
                    divAdditional4.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Additional4, formtype);
                if (isrequired == 1)
                {
                    lblAdditional4.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAdditional4.Attributes.Add("required", "");
                    vcAdditional4.Enabled = true;
                }
                else
                {
                    lblAdditional4.Text = labelname;
                    //txtAdditional4.Attributes.Remove("required");
                    vcAdditional4.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional5 visibility is true or false if form_input_name is Additional5 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional5.Visible = true;
                }
                else
                {
                    divAdditional5.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Additional5, formtype);
                if (isrequired == 1)
                {
                    lblAdditional5.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAdditional5.Attributes.Add("required", "");
                    vcAdditional5.Enabled = true;
                }
                else
                {
                    lblAdditional5.Text = labelname;
                    //txtAdditional5.Attributes.Remove("required");
                    vcAdditional5.Enabled = false;
                }
            }
            #endregion
        }

        setDivCss_TelMobFax(divTelcc.Visible, divTelac.Visible, divTelNo.Visible, "Tel");//*
        setDivCss_TelMobFax(divFaxcc.Visible, divFaxac.Visible, divFaxNo.Visible, "Fax");//*

        return isValidShow;
    }
    #endregion

    #region bindDropdown & bind ddlCountry (countries)
    protected void bindDropdown()
    {
        DataSet dsCountry = new DataSet();
        CountryObj couObj = new CountryObj(fn);
        dsCountry = couObj.getAllCountry();

        if (dsCountry.Tables[0].Rows.Count != 0)
        {
            for (int x = 0; x < dsCountry.Tables[0].Rows.Count; x++)
            {
                ddlCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();
            }
        }
    }
    #endregion

    #region ClearForm
    private void ClearForm()
    {
        hfRegGroupID.Value = "";
        txtName.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtCity.Text = "";
        txtState.Text = "";
        txtZipcode.Text = "";
        ddlCountry.SelectedIndex = 0;
        txtTelcc.Text = "";
        txtTelac.Text = "";
        txtTel.Text = "";
        txtFaxcc.Text = "";
        txtFaxac.Text = "";
        txtFax.Text = "";
        txtEmail.Text = "";
        txtWebsite.Text = "";
        txtAdditional1.Text = "";
        txtAdditional2.Text = "";
        txtAdditional3.Text = "";
        txtAdditional4.Text = "";
        txtAdditional5.Text = "";
    }
    #endregion

    #region ddlCountry_SelectedIndexChanged
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        string countryid = ddlCountry.SelectedValue;

        if (ddlCountry.SelectedIndex == 0)
        {
            countryid = "0";
        }

        string sql = "Select countrycode,countryen From ref_country Where Cty_GUID='" + countryid + "'";
        DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
        if (dt.Rows.Count > 0)
        {
            string code = dt.Rows[0]["countryen"].ToString();

            txtTelcc.Text = code;
            txtFaxcc.Text = code;
        }
    }
    #endregion

    #endregion

    #region setDivCss_TelMobFax
    public void setDivCss_TelMobFax(bool isShowCC, bool isShowAC, bool isShowPhoneNo, string type)
    {
        string name = string.Empty;
        try
        {
            if (type == "Tel")
            {
                #region type="Tel"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");
                    divTelcc.Attributes.Add("class", "col-xs-6");

                    divTelac.Attributes.Remove("class");
                    divTelac.Attributes.Add("class", "col-xs-6");

                    divTelNo.Attributes.Remove("class");
                }
                #endregion
            }
            else if (type == "Fax")
            {
                #region Type="Fax"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");
                    divFaxcc.Attributes.Add("class", "col-xs-6");

                    divFaxac.Attributes.Remove("class");
                    divFaxac.Attributes.Add("class", "col-xs-6");

                    divFaxNo.Attributes.Remove("class");
                }
                #endregion
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

}