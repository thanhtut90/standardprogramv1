﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ArchAdmin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() == null)
                Response.Redirect("LoginArch.aspx");
        }
        catch (Exception ex)
        {
            Response.Redirect("LoginArch.aspx");
        }
    }
}
