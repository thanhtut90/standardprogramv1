﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageDietary.aspx.cs" Inherits="Admin_ManageDietary" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
         <div class="row" style="padding-bottom:20px;">
            <div class="col-lg-7 content-header">
                <h1>Manage Dietary </h1>
            </div>
        </div><!--page header -->
        <div id="contentwrapper" class="contentwrapper">
            <div id="index" class="subcontent">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h4>Add New Dietary</h4>

                        <asp:Label ID="lblDietName" runat="server" Text="Name" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtDietName" runat="server" Width="350px"></asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtDietName" runat="server"
                            ErrorMessage="Please insert the dietary name" ForeColor="Red"></asp:RequiredFieldValidator>
                        <br />

                        <asp:Label ID="lblSortOrder" runat="server" Text="Sort Order" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtSortorder" runat="server" Width="350px"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbSortorder" runat="server" TargetControlID="txtSortorder" FilterType="Numbers" ValidChars="0123456789" />
                        <br />

                        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                        <asp:Label ID="lblShowId" runat="server" Visible="false"></asp:Label>
                        <br /><br />

                        <asp:Panel ID="pnllist" runat="server" Visible="false">
                            <asp:GridView ID="gvList" runat="server" DataKeyNames="diet_id" 
                            OnRowDeleting="GridView1_RowDeleting"
                            OnRowEditing="GridView1_RowEditing"
                            OnRowUpdating="GridView1_RowUpdating"
                            OnRowCancelingEdit="GridView1_RowCancelingEdit" AutoGenerateColumns="false"
                            CssClass="table">
                                <HeaderStyle CssClass="theadstyle" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <%# Eval("diet_id")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dietary Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("diet_name") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("diet_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Order">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtOrder" runat="server" Text='<%# Eval("diet_order") %>' Width="50px"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="ftbOrder" runat="server" TargetControlID="txtOrder" FilterType="Numbers" ValidChars="0123456789" />
                                            <asp:Label ID="lblno" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrder" runat="server" Text='<%# Eval("diet_order") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowEditButton="True" CausesValidation="false" />
                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" OnClientClick="return confirm ('Are you sure you want to delete this record?')" Text="Delete" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div style="color:Red"><asp:Label ID="lblMsg" runat="server"></asp:Label></div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div><!--page index -->
        </div><!--contentwrapper -->
    </div><!--centercontent -->
</asp:Content>