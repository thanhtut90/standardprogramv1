﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using Corpit.Utilities;
using Corpit.BackendMaster;
using System.Reflection;
using Corpit.Site.Utilities;
using System.Data.SqlClient;
using System.Globalization;
using Corpit.Registration;
public partial class DashboardOSEA : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFuz = new CommonFuns();
    CultureInfo cul = new CultureInfo("en-GB");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string[] showList = new string[2];

            try { 
                if (Session["roleid"].ToString() == "1")
                {
                    showList = getAllShowList();
                }
                else
                {
                    showList = getShowList();
                }
                int res = CheckGroupReg(Session["userid"].ToString());
                if (res == 0)
                {
                    btnChartGroupRegDate.Visible = false;
                    ChartGroupRegDate.Visible = false;
                    btnChartGroupRegion.Visible = false;
                    ChartGroupRegion.Visible = false;
                    btnChartGroupCountry.Visible = false;
                    ChartGroupCountry.Visible = false;
                    ddlGroupCountry.Visible = false;
                    ChartGroupCategory.Visible = false;
                }
            }
            catch(Exception ex)
            {
                Response.Redirect("Login.aspx");
            }

            if (!string.IsNullOrEmpty(showList[0]))
            {
                if (showList[0].Contains(","))
                {
                    Panel1.Visible = true;
                    string[] showLists = showList[0].Split(',');
                    loadShowList(showList);
                    lbl_show.Text = showLists[0];
                    bindCountryList(showLists[0]);
                    loadChart(showLists[0]);
                }
                else
                {
                    Panel1.Visible = false;
                    lbl_show.Text = showList[0];
                    bindCountryList(showList[0]);
                    loadChart(showList[0]);
                }
            }
            else
            {
                Response.Redirect("Event_Config.aspx");
            }
        }
    }

    protected void loadShowList(string[] showList)
    {
        ddl_showList.Items.Clear();
        string[] showIDList = showList[0].Split(',');
        string[] showNameList = showList[1].Split(',');
        

        for (int i=0; i< showIDList.Length; i++)
        {

            ListItem items = new ListItem(showNameList[i], showIDList[i]);

            ddl_showList.Items.Insert(i,items);
        }
    }

    protected void loadChart(string showid)
    {
        Tuple<string, string> getType = checkFlowTypeGI(showid);
        if (getType != null)
        {
            ChartCategory.DataSource = ByType(showid);
            ChartCategory.DataBind();
            #region By Date
            if (!string.IsNullOrEmpty(getType.Item1) || !string.IsNullOrEmpty(getType.Item2))
            {
                bindChartByDate(showid);
            }
            #endregion
            #region By Country
            if (!string.IsNullOrEmpty(getType.Item1))
            {
                DataTable dtGroupCountry = GrpByCountry(showid);
                ChartGroupCountry.DataSource = dtGroupCountry;
                ChartGroupCountry.DataBind();
                int rcHeight = dtGroupCountry.Rows.Count * 80;
                ChartGroupCountry.Height = rcHeight > 300 ? rcHeight : 300;
            }
            if (!string.IsNullOrEmpty(getType.Item2))
            {
                DataTable dtIndivCountry = IndivByCountry(showid);
                ChartIndivCountry.DataSource = dtIndivCountry;
                ChartIndivCountry.DataBind();
                int rcHeight = dtIndivCountry.Rows.Count * 80;
                ChartIndivCountry.Height = rcHeight > 300 ? rcHeight : 300;
            }
            #endregion
            #region By Region
            if (!string.IsNullOrEmpty(getType.Item1))
            {
                DataTable dtGroupRegion = GrpByRegion(showid);
                ChartGroupRegion.DataSource = dtGroupRegion;
                ChartGroupRegion.DataBind();
                int rcHeight = dtGroupRegion.Rows.Count * 80;
                ChartGroupRegion.Height = rcHeight > 300 ? rcHeight : 300;
            }
            if (!string.IsNullOrEmpty(getType.Item2))
            {
                DataTable dtIndivRegion = IndivByRegion(showid);
                ChartIndivRegion.DataSource = dtIndivRegion;
                ChartIndivRegion.DataBind();
                int rcHeight = dtIndivRegion.Rows.Count * 80;
                ChartIndivRegion.Height = rcHeight > 300 ? rcHeight : 300;
            }
            #endregion

            if (!string.IsNullOrEmpty(getType.Item1) && !string.IsNullOrEmpty(getType.Item2))
            {
                string country = ddlIndividualCountry.SelectedValue;
                ChartIndividualCategory.DataSource = GrpByIndivCategory(showid, country);
                ChartIndividualCategory.DataBind();
            }

            if (!string.IsNullOrEmpty(getType.Item1) && !string.IsNullOrEmpty(getType.Item2))
            {
                string country = ddlGroupCountry.SelectedValue;
                ChartGroupCategory.DataSource = GrpByGrpCategory(showid, country);
                ChartGroupCategory.DataBind();
            }
        }
    }

    protected string[] getAllShowList()
    {
        string query = "Select * from tb_Show Order By SHW_ID";
        string showList = string.Empty;
        string showName = string.Empty;

        DataTable dt = fn.GetDatasetByCommand(query, "dsShow").Tables[0];
        for (int i = 0; i <dt.Rows.Count; i++)
        {
            if (showList == string.Empty)
            {
                showList = dt.Rows[i]["SHW_ID"].ToString();
                showName = dt.Rows[i]["SHW_Name"].ToString();
            }
            else
            {
                showList += "," + dt.Rows[i]["SHW_ID"].ToString();
                showName += "," + dt.Rows[i]["SHW_Name"].ToString();
            }
        }
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        return list;
    }

    protected string[] getShowList()
    {
        string showList = string.Empty;
        string showName = string.Empty;
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = Session["userid"].ToString();
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                query = "Select * from tb_Show WHERE SHW_ID=@showID";
                spar = new SqlParameter("showID", SqlDbType.NVarChar);

                string[] showLists = dt.Rows[0]["us_showid"].ToString().Split(',');
                for (int i = 0; i<showLists.Length; i++)
                {
                    pList.Clear();
                    spar.Value = showLists[i];
                    pList.Add(spar);

                    DataTable dtShow = fn.GetDatasetByCommand(query, "dsShow",pList).Tables[0];

                    if (showName == string.Empty)
                    {
                        showName = dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                    else
                    {
                        showName += "," + dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                }
                list[0] = dt.Rows[0]["us_showid"].ToString();
                list[1] = showName;
                return list;
            }
            return list;
        }
        catch (Exception ex)
        {
            return list;
        }
    }

    protected void bindCountryList(string showid)
    {
        MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        msregIndiv.ShowID = showid;

        ddlIndividualCountry.Items.Clear();
        ddlIndividualCountry.DataSource = msregIndiv.getRegChartIndivByCountry();
        ddlIndividualCountry.DataTextField = "countryname";
        ddlIndividualCountry.DataValueField = "countryname";
        ddlIndividualCountry.DataBind();

        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;

        ddlGroupCountry.Items.Clear();
        ddlGroupCountry.DataSource = msregGroup.getRegChartGroupByCountry();
        ddlGroupCountry.DataTextField = "countryname";
        ddlGroupCountry.DataValueField = "countryname";
        ddlGroupCountry.DataBind();
    }

    #region ByGroupIndiv
    private DataTable ByGroupIndiv(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Total");
        dt.Columns.Add("Desc");
        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        int masCount = 0;

        DataTable Cat = GetRegByCategory(showid);  
         Double catCount;
        Double TotalCount = masCount;
        
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["Type"].ToString();
            drow["Total"] = caldbscan.ToString();
            dt.Rows.Add(drow);
        }

        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["Total"] = "100";
        }

        return dt;
    }
    #endregion
    #region ByGroupIndiv
    private DataTable ByType(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Total");
        dt.Columns.Add("Desc");
        dt.Columns.Add("TotalPercentage");
        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        //DataTable dtmas = msregGroup.getRegByGroupIndivList();
        //int masCount = dtmas.Rows.Count;
        //Double catCount;
        //Double TotalCount = masCount;
        DataTable normalList = GetRegByCategory(showid);
        DataTable preList = GetPreRegByCategory(showid);
        Double TotalCount = 0;// masCount;
        if (normalList.Rows.Count > 0)
        {
            int totalcount = cFuz.ParseInt(normalList.Compute("SUM(SubCount)", string.Empty).ToString());
            TotalCount = totalcount;
        }
        if (preList.Rows.Count > 0)
        {
            int totalcount = cFuz.ParseInt(preList.Compute("SUM(SubCount)", string.Empty).ToString());
            TotalCount += totalcount;
        }

        for (int i = 0; i < normalList.Rows.Count; i++)
        {
            Double catCount = cFuz.ParseInt(normalList.Rows[i]["SubCount"].ToString());
            Double catPercentage = Math.Round((catCount / TotalCount) * 100, 2);//***changed by th on 12-7-2017, the client want to show total count
            DataRow drow = dt.NewRow();
            drow["Desc"] = GetCategoryDescription( normalList.Rows[i]["Type"].ToString());
            drow["Total"] = catCount;
            drow["TotalPercentage"] = catPercentage.ToString();
            dt.Rows.Add(drow);
        }

        for (int i = 0; i < preList.Rows.Count; i++)
        {
            Double catCount = cFuz.ParseInt(preList.Rows[i]["SubCount"].ToString());
            Double catPercentage = Math.Round((catCount / TotalCount) * 100, 2);//***changed by th on 12-7-2017, the client want to show total count
            DataRow drow = dt.NewRow();
            drow["Desc"] ="Pre-populated (" + GetCategoryDescription(preList.Rows[i]["Type"].ToString()) +")";
            drow["Total"] = catCount;
            drow["TotalPercentage"] = catPercentage.ToString();
            dt.Rows.Add(drow);
        }


        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["Total"] = "100";
            drow["TotalPercentage"] = "100";
        }

        return dt;
    }
    private string GetCategoryDescription(string cType)
    {
        string rtn = "";
        int categoryType =  cFuz.ParseInt(cType);
        if (categoryType == (int) RegApproveRejectStatus.VIP)
            rtn="VIP";
        //else if (categoryType == (int)RegApproveRejectStatus.Approved)
        //    rtn = "Confirmed Normal Visitor";
        else if (categoryType == (int)RegApproveRejectStatus.Confirmed || categoryType == (int)RegApproveRejectStatus.Approved)
            rtn = "Normal Visitor";
        else if (categoryType == (int)RegApproveRejectStatus.Rejected)
            rtn = "Auto Rejected";
        else if (categoryType == (int)RegApproveRejectStatus.Declined)
            rtn = "Declined";
        else if (categoryType == cFuz.ParseInt("555"))
            rtn = "Upgrade to VIP";
        else if (categoryType == 0)
            rtn = "Incomplete";

        return rtn;
    }
    #endregion
    private DataTable GrpByIndivCategory(string showid, string country)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Total");
        dt.Columns.Add("Desc");

        MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        msregIndiv.ShowID = showid;
        DataTable dtmas = msregIndiv.getDataAllRegList(country);
        int masCount = dtmas.Rows.Count;

        DataTable Cat = msregIndiv.getRegChartIndivByCategory(country);

        Double catCount;
        Double TotalCount = masCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["reg_CategoryName"].ToString();
            drow["Total"] = caldbscan.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["Total"] = "100";
        }

        return dt;
    }

    private DataTable GrpByGrpCategory(string showid, string country)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Total");
        dt.Columns.Add("Desc");

        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        DataTable dtmas = msregGroup.getDataAllRegGroupList(country);
        int masCount =  dtmas.Rows.Count;

        DataTable Cat = msregGroup.getRegChartGroupByCategory(country);
        Double catCount;
        Double TotalCount = masCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["reg_CategoryName"].ToString();
            drow["Total"] = caldbscan.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["Total"] = "100";
        }

        return dt;
    }

    #region byDate
    #region GrpByGroupRegDate
    private DataTable GrpByGroupRegDate(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("TotalPercentage");
        dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Desc");
        dt.Columns.Add("RegDate", typeof(DateTime));

        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        DataTable dtmas = msregGroup.getDataAllRegGroupMemberList();
        int masCount = dtmas.Rows.Count;

        DataTable Cat = msregGroup.getRegChartGroupByRegDate();

        Double catCount;
        Double TotalCount = masCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["dateCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["RegDate"] = Convert.ToDateTime(Cat.Rows[i]["reg_datecreated"].ToString(), cul);
            drow["Desc"] = Convert.ToDateTime(Cat.Rows[i]["reg_datecreated"].ToString()).ToString("dd/MM/yyyy");
            drow["TotalPercentage"] = caldbscan.ToString();
            drow["TotalNumber"] = catCount.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["TotalPercentage"] = "100";
            drow["TotalNumber"] = "0";
        }

        return dt;
    }
    #endregion
    #region GrpByIndivRegDate
    private DataTable GrpByIndivRegDate(string showid)
    {
        DataTable dt = new DataTable();
        //dt.Columns.Add("TotalPercentage");
        //dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Approved");
        dt.Columns.Add("TotalApprovedPercentage");
        dt.Columns.Add("Rejected");
        dt.Columns.Add("TotalRejectedPercentage");
        dt.Columns.Add("Desc");
        dt.Columns.Add("RegDate", typeof(DateTime));

        #region Old
        //DataTable Cat = msregIndiv.getRegChartIndivByRegDate();
        //Double catCount;
        //Double TotalCount = masCount;
        //for (int i = 0; i < Cat.Rows.Count; i++)
        //{
        //    catCount = cFuz.ParseInt(Cat.Rows[i]["dateCount"].ToString());
        //    Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
        //    DataRow drow = dt.NewRow();
        //    drow["RegDate"] = Convert.ToDateTime(Cat.Rows[i]["reg_datecreated"].ToString(), cul);
        //    drow["Desc"] = Convert.ToDateTime(Cat.Rows[i]["reg_datecreated"].ToString()).ToString("dd/MM/yyyy");
        //    drow["TotalPercentage"] = caldbscan.ToString();
        //    drow["TotalNumber"] = catCount.ToString();
        //    dt.Rows.Add(drow);
        //}
        //if (dt.Rows.Count == 0)
        //{
        //    DataRow drow = dt.NewRow();
        //    drow["Desc"] = "No Record";
        //    drow["TotalPercentage"] = "100";
        //    drow["TotalNumber"] = "0";
        //}
        #endregion

        //MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //msregIndiv.ShowID = showid;
        //DataTable dtmas = msregIndiv.getDataAllRegList();
        //int masCount = dtmas.Rows.Count;
        Double TotalCount = 0;// masCount;
        DataTable Cat = GetDailyByDate(showid);
        if (Cat.Rows.Count > 0)
        {
            int totalapp = cFuz.ParseInt(Cat.Compute("SUM(appRegStatus)", string.Empty).ToString());
            int totalrej = cFuz.ParseInt(Cat.Compute("SUM(rejRegStatus)", string.Empty).ToString());
            TotalCount = totalapp + totalrej;
        }
        Double approvedCount;
        Double rejectedCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            DataRow drow = dt.NewRow();
            drow["RegDate"] = Convert.ToDateTime(Cat.Rows[i]["reg_datecreated"].ToString(), cul);
            drow["Desc"] = Convert.ToDateTime(Cat.Rows[i]["reg_datecreated"].ToString()).ToString("dd/MM/yyyy");

            drow["Approved"] = Cat.Rows[i]["appRegStatus"].ToString();

            approvedCount = cFuz.ParseInt(Cat.Rows[i]["appRegStatus"].ToString());
            Double approvedCountPercentage = Math.Round((approvedCount / TotalCount) * 100, 2);
            drow["TotalApprovedPercentage"] = approvedCountPercentage.ToString();

            drow["Rejected"] = Cat.Rows[i]["rejRegStatus"].ToString();

            rejectedCount = cFuz.ParseInt(Cat.Rows[i]["rejRegStatus"].ToString());
            Double rejectedCountPercentage = Math.Round((rejectedCount / TotalCount) * 100, 2);
            drow["TotalRejectedPercentage"] = rejectedCountPercentage.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["RegDate"] = DateTime.Now;
            drow["Desc"] = "No Record";
            drow["Approved"] = "0";
            drow["TotalApprovedPercentage"] = "0";
            drow["Rejected"] = "0";
            drow["TotalRejectedPercentage"] = "0";
        }

        return dt;
    }
    #endregion
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string showid = string.Empty;
            if (Session["roleid"].ToString() != "1")
            {
                showid = lbl_show.Text.Trim();
            }
            else
            {
                showid = ddl_showList.SelectedValue;
            }

            loadChart(showid);
        }
        catch (Exception ex)
        { }
    }
    private void bindChartByDate(string showid)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()) && !string.IsNullOrEmpty(txtToDate.Text.Trim()))
            {
                string sdate = txtFromDate.Text.Trim();
                string edate = txtToDate.Text.Trim();
                string[] startdate1 = sdate.Split('/');
                string startdate = startdate1[1] + "/" + startdate1[0] + "/" + startdate1[2];
                DateTime dtSDate = Convert.ToDateTime(startdate, cul);
                string ssdate = dtSDate.ToString("MM/dd/yyyy HH:mm:ss");
                string[] enddate1 = edate.Split('/');
                string enddate = enddate1[1] + "/" + enddate1[0] + "/" + enddate1[2];
                DateTime dtEDate = Convert.ToDateTime(enddate, cul);
                string eedate = dtEDate.ToString("MM/dd/yyyy HH:mm:ss");
                #region Group
                DataTable dtGroupDate = GrpByGroupRegDate(showid);
                DataView dv = new DataView();
                dv = dtGroupDate.DefaultView;
                dv.RowFilter = "RegDate>=#" + ssdate + "# And RegDate<=#" + eedate + "#";
                DataTable dt = dv.ToTable();
                ChartGroupRegDate.DataSource = dt;
                ChartGroupRegDate.DataBind();
                int rcHeight = dt.Rows.Count * 80;
                ChartGroupRegDate.Height = rcHeight > 300 ? rcHeight : 300;
                #endregion

                #region Individual
                DataTable dtIndivDate = GrpByIndivRegDate(showid);
                DataView dvIndiv = new DataView();
                dvIndiv = dtIndivDate.DefaultView;
                dvIndiv.RowFilter = "RegDate>=#" + ssdate + "# And RegDate<=#" + eedate + "#";
                DataTable dtIndiv = dvIndiv.ToTable();
                ChartIndivRegDate.DataSource = dtIndiv;
                ChartIndivRegDate.DataBind();
                int rcHeightIndiv = dtIndiv.Rows.Count * 80;
                ChartIndivRegDate.Height = rcHeightIndiv > 300 ? rcHeightIndiv : 300;
                #endregion
            }
            else
            {
                DataTable dtGroupDate = GrpByGroupRegDate(showid);
                ChartGroupRegDate.DataSource = dtGroupDate;
                ChartGroupRegDate.DataBind();
                int rcHeight = dtGroupDate.Rows.Count * 80;
                ChartGroupRegDate.Height = rcHeight > 300 ? rcHeight : 300;

                DataTable dtIndivDate = GrpByIndivRegDate(showid);
                ChartIndivRegDate.DataSource = dtIndivDate;
                ChartIndivRegDate.DataBind();
                int rcHeightIndiv = dtIndivDate.Rows.Count * 80;
                ChartIndivRegDate.Height = rcHeightIndiv > 300 ? rcHeightIndiv : 300;
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region byCountry
    #region GrpByCountry (get data from tb_RegDelegate, tb_RegGroup, tb_Invoice tables according to the RG_IsMultiple Is Not Null or Like 'G')
    private DataTable GrpByCountry(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("TotalPercentage");
        dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Desc");

        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        DataTable dtmas = msregGroup.getDataAllRegGroupMemberList();
        int masCount = dtmas.Rows.Count;

        DataTable Cat = msregGroup.getRegChartGroupByCountry();
        Double catCount;
        Double TotalCount = masCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["countryname"].ToString();
            drow["TotalPercentage"] = caldbscan.ToString();
            drow["TotalNumber"] = catCount.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["TotalPercentage"] = "100";
            drow["TotalNumber"] = "0";
        }

        return dt;
    }
    #endregion
    #region IndivByCountry (get data from tb_RegDelegate, tb_RegGroup, tb_Invoice tables according to the RG_IsMultiple Is Null or Like 'I')
    private DataTable IndivByCountry(string showid)
    {
        DataTable dt = new DataTable();
        //dt.Columns.Add("TotalPercentage");
        //dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Approved");
        dt.Columns.Add("TotalApprovedPercentage");
        dt.Columns.Add("Rejected");
        dt.Columns.Add("TotalRejectedPercentage");
        dt.Columns.Add("Desc");

        #region Old
        //MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //msregIndiv.ShowID = showid;
        //DataTable dtmas = msregIndiv.getDataAllRegList();
        //int masCount = dtmas.Rows.Count;

        //DataTable Cat = msregIndiv.getRegChartIndivByCountry();
        //Double catCount;
        //Double TotalCount = masCount;
        //for (int i = 0; i < Cat.Rows.Count; i++)
        //{
        //    catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
        //    Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
        //    DataRow drow = dt.NewRow();
        //    drow["Desc"] = Cat.Rows[i]["countryname"].ToString();
        //    drow["TotalPercentage"] = caldbscan.ToString();
        //    drow["TotalNumber"] = catCount.ToString();
        //    dt.Rows.Add(drow);
        //}
        //if (dt.Rows.Count == 0)
        //{
        //    DataRow drow = dt.NewRow();
        //    drow["Desc"] = "No Record";
        //    drow["TotalPercentage"] = "100";
        //    drow["TotalNumber"] = "0";
        //}
        #endregion

        //MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //msregIndiv.ShowID = showid;
        //DataTable dtmas = msregIndiv.getDataAllRegList();
        //int masCount = dtmas.Rows.Count;
        Double TotalCount = 0;// masCount;
        DataTable Cat = GetReportByCountry(showid);
        if(Cat.Rows.Count > 0)
        {
            int totalapp = cFuz.ParseInt(Cat.Compute("SUM(appRegStatus)", string.Empty).ToString());
            int totalrej = cFuz.ParseInt(Cat.Compute("SUM(rejRegStatus)", string.Empty).ToString());
            TotalCount = totalapp + totalrej;
        }
        Double approvedCount;
        Double rejectedCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["countryname"].ToString();

            drow["Approved"] = Cat.Rows[i]["appRegStatus"].ToString();

            approvedCount = cFuz.ParseInt(Cat.Rows[i]["appRegStatus"].ToString());
            Double approvedCountPercentage = Math.Round((approvedCount / TotalCount) * 100, 2);
            drow["TotalApprovedPercentage"] = approvedCountPercentage.ToString();

            drow["Rejected"] = Cat.Rows[i]["rejRegStatus"].ToString();

            rejectedCount = cFuz.ParseInt(Cat.Rows[i]["rejRegStatus"].ToString());
            Double rejectedCountPercentage = Math.Round((rejectedCount / TotalCount) * 100, 2);
            drow["TotalRejectedPercentage"] = rejectedCountPercentage.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["Approved"] = "0";
            drow["TotalApprovedPercentage"] = "0";
            drow["Rejected"] = "0";
            drow["TotalRejectedPercentage"] = "0";
        }

        return dt;
    }
    #endregion
    #endregion

    #region byRegion
    #region GrpByRegion
    private DataTable GrpByRegion(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("TotalPercentage");
        dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Desc");

        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        DataTable dtmas = msregGroup.getDataAllRegGroupMemberList();
        int masCount = dtmas.Rows.Count;

        DataTable Cat = msregGroup.getRegChartGroupByRegion();
        Double catCount;
        Double TotalCount = masCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["regionname"].ToString();
            drow["TotalPercentage"] = caldbscan.ToString();
            drow["TotalNumber"] = catCount.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["TotalPercentage"] = "100";
            drow["TotalNumber"] = "0";
        }

        return dt;
    }
    #endregion
    #region IndivByRegion
    private DataTable IndivByRegion(string showid)
    {
        DataTable dt = new DataTable();
        //dt.Columns.Add("TotalPercentage");
        //dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Approved");
        dt.Columns.Add("TotalApprovedPercentage");
        dt.Columns.Add("Rejected");
        dt.Columns.Add("TotalRejectedPercentage");
        dt.Columns.Add("Desc");

        #region Old
        //DataTable Cat = msregIndiv.getRegChartIndivByRegion();
        //Double catCount;
        //Double TotalCount = masCount;
        //for (int i = 0; i < Cat.Rows.Count; i++)
        //{
        //    catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
        //    Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
        //    DataRow drow = dt.NewRow();
        //    drow["Desc"] = Cat.Rows[i]["regionname"].ToString();
        //    drow["TotalPercentage"] = caldbscan.ToString();
        //    drow["TotalNumber"] = catCount.ToString();
        //    dt.Rows.Add(drow);
        //}
        //if (dt.Rows.Count == 0)
        //{
        //    DataRow drow = dt.NewRow();
        //    drow["Desc"] = "No Record";
        //    drow["TotalPercentage"] = "100";
        //    drow["TotalNumber"] = "0";
        //}
        #endregion

        //MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //msregIndiv.ShowID = showid;
        //DataTable dtmas = msregIndiv.getDataAllRegList();
        //int masCount = dtmas.Rows.Count;
        Double TotalCount = 0;// masCount;
        DataTable Cat = GetReportByRegion(showid);
        if (Cat.Rows.Count > 0)
        {
            int totalapp = cFuz.ParseInt(Cat.Compute("SUM(appRegStatus)", string.Empty).ToString());
            int totalrej = cFuz.ParseInt(Cat.Compute("SUM(rejRegStatus)", string.Empty).ToString());
            TotalCount = totalapp + totalrej;
        }
        Double approvedCount;
        Double rejectedCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["regionname"].ToString();
            drow["Approved"] = Cat.Rows[i]["appRegStatus"].ToString();

            approvedCount = cFuz.ParseInt(Cat.Rows[i]["appRegStatus"].ToString());
            Double approvedCountPercentage = Math.Round((approvedCount / TotalCount) * 100, 2);
            drow["TotalApprovedPercentage"] = approvedCountPercentage.ToString();

            drow["Rejected"] = Cat.Rows[i]["rejRegStatus"].ToString();

            rejectedCount = cFuz.ParseInt(Cat.Rows[i]["rejRegStatus"].ToString());
            Double rejectedCountPercentage = Math.Round((rejectedCount / TotalCount) * 100, 2);
            drow["TotalRejectedPercentage"] = rejectedCountPercentage.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["Approved"] = "0";
            drow["TotalApprovedPercentage"] = "0";
            drow["Rejected"] = "0";
            drow["TotalRejectedPercentage"] = "0";
        }

        return dt;
    }
    #endregion
    #endregion

    #region checkFlowTypeGI (get Group or Individual from tb_site_flow_master table)
    private Tuple<string, string> checkFlowTypeGI(string showid)
    {
        Tuple<string, string> tplType;
        GeneralObj grnObj = new GeneralObj(fn);
        DataTable dtFlowMas = grnObj.getAllSiteFlowMaster();

        string grp = string.Empty;
        string inv = string.Empty;
        if(dtFlowMas.Rows.Count > 0)
        {
            foreach(DataRow dr in dtFlowMas.Rows)
            {
                if (dr["ShowID"].ToString() == showid)
                {
                    string flowtype = dr["FLW_Type"].ToString();
                    if (flowtype == SiteFlowType.FLOW_GROUP)
                    {
                        grp = SiteFlowType.FLOW_GROUP;
                    }
                    else
                    {
                        inv = SiteFlowType.FLOW_INDIVIDUAL;
                    }
                }
            }
        }

        tplType = new Tuple<string, string>(grp, inv);

        return tplType;
    }
    #endregion


    protected void ddlIndividualCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        string country = ddlIndividualCountry.SelectedValue;
        ChartIndividualCategory.DataSource = GrpByIndivCategory(lbl_show.Text, country);
        ChartIndividualCategory.DataBind();
    }

    protected void ddlGroupCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        string country = ddlGroupCountry.SelectedValue;
        ChartGroupCategory.DataSource = GrpByGrpCategory(lbl_show.Text, country);
        ChartGroupCategory.DataBind();
    }

    protected void ddl_showList_SelectedIndexChanged(object sender, EventArgs e)
    {
        lbl_show.Text = ddl_showList.SelectedValue;
        bindCountryList(ddl_showList.SelectedValue);
        loadChart(ddl_showList.SelectedValue);
    }
    private int CheckGroupReg(string userid)
    {
        int res = 0;
        try
        {
            string sql = "select * from tb_site_flow_master where ShowID in (select us_showid from tb_Admin_Show where us_userid='" + userid + "') and Status='Active' and FLW_Type='G'";
            DataSet ds = new DataSet();
            ds = fn.GetDatasetByCommand(sql, "sqlCheckGroup");

            res = ds.Tables[0].Rows.Count;
        }
        catch (Exception ex) { }
        return res;

    }

     

    public DataTable GetPreRegByCategory(string ShowID)
    {
        DataTable dt = new DataTable();

        try
        {
            //            string query = @" 
            //select reg_approveStatus Type, Count(*) SubCount from tb_RegDelegate where 
            //  regno    in ( select isnull(RefNo ,0) from tb_RegPrePopulate where ShowID=@SHWID )
            // and  ShowID=@SHWID and recycle=0 And reg_approveStatus Not In ('" + (int)RegApproveRejectStatus.Rejected + "')" +
            // " group by reg_approveStatus";//***removed Rejected records by th on 12-7-2017, the client want to remove Rejected Data
            string query = @"
            select Type As Type, Count(*) SubCount from (select 
            CASE WHEN reg_approveStatus = 22 Or reg_approveStatus = 11 THEN 11 
            When reg_approveStatus=33 And (reg_Status Is Null OR reg_Status=0) Then 0
            When reg_approveStatus=33 And (reg_Status=1) Then 33
            Else reg_approveStatus END AS Type,* from tb_RegDelegate where 
            regno  in ( select isnull(RefNo ,0) from tb_RegPrePopulate where ShowID=@SHWID )
            and  ShowID=@SHWID and recycle=0 ) as aa group by Type";//And reg_approveStatus Not In ('" + (int)RegApproveRejectStatus.Rejected + "')
            List <SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = ShowID;
            pList.Add(spar);
             
            dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    public DataTable GetRegByCategory(string ShowID)
    {
        DataTable dt = new DataTable();

        try
        {
            //            string query = @" 
            //select reg_approveStatus Type, Count(*) SubCount from tb_RegDelegate where 
            //  regno  not  in ( select isnull(RefNo ,0) from tb_RegPrePopulate where ShowID=@SHWID )
            // and  ShowID=@SHWID and recycle=0 And reg_approveStatus Not In ('" + (int)RegApproveRejectStatus.Rejected + "')" +
            // " group by reg_approveStatus";//***removed Rejected records by th on 12-7-2017, the client want to remove Rejected Data
            string query = @"
            select Type As Type, Count(*) SubCount from (select 
            CASE WHEN reg_approveStatus = 22 Or reg_approveStatus = 11 THEN 11 
            When reg_approveStatus=33 And (reg_Status Is Null OR reg_Status=0) Then 0
            When reg_approveStatus=33 And (reg_Additional5 Is Null Or reg_Additional5='') Then 555
            When reg_approveStatus=33 And (reg_Status=1) Then 33
            Else reg_approveStatus END AS Type ,* 
            from tb_RegDelegate where
             regno  not  in ( select isnull(RefNo ,0) from tb_RegPrePopulate where ShowID=@SHWID )
             and  ShowID=@SHWID and recycle=0 ) as aa group by Type";//And reg_approveStatus Not In ('" + (int)RegApproveRejectStatus.Rejected + "')

            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = ShowID;
            pList.Add(spar);

            dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }

    public DataTable GetDailyByDate(string showID)
    {
        DataTable dt = new DataTable();
        try
        {
            string sql = "SELECT CAST(reg_datecreated AS DATE) AS reg_datecreated,Sum(appRegStatus) As appRegStatus,Sum(rejRegStatus) As rejRegStatus, ShowID"
                            + " FROM (SELECT CAST(reg_datecreated AS DATE) AS reg_datecreated,   COUNT(RegStatus) As appRegStatus,0 As rejRegStatus, 'Approved' AS RegStatus, ShowID"
                            + " From (Select reg_datecreated,'Approved' AS RegStatus,ShowID FROM dbo.tb_RegDelegate AS r WHERE (recycle = 0) AND (RegGroupID IN"
                            + " (SELECT RegGroupID FROM dbo.tb_RegGroup WHERE (recycle = 0) AND (RG_IsMultiple IS NULL OR RG_IsMultiple LIKE 'I'))) AND (RegGroupID NOT IN"
                            + " (SELECT RegGroupId FROM dbo.tb_Invoice WHERE (RegGroupId IS NOT NULL))) And reg_approveStatus In ('33','22','11') And reg_Status=1) As aa"
                            + " GROUP BY CAST(reg_datecreated AS DATE), RegStatus, ShowID"
                            + " UNION SELECT CAST(reg_datecreated AS DATE) AS reg_datecreated,   0 As appRegStatus, COUNT(reg_approveStatus) As rejRegStatus,  'Reject' AS RegStatus, ShowID"
                            + " FROM  dbo.tb_RegDelegate AS r WHERE (recycle = 0) AND (RegGroupID IN (SELECT RegGroupID"
                            + " FROM dbo.tb_RegGroup WHERE (recycle = 0) AND (RG_IsMultiple IS NULL OR RG_IsMultiple LIKE 'I'))) AND (RegGroupID NOT IN"
                            + " (SELECT RegGroupId FROM dbo.tb_Invoice WHERE (RegGroupId IS NOT NULL)))"
                            + " And reg_approveStatus In ('66') And reg_Status=1 GROUP BY CAST(reg_datecreated AS DATE), reg_approveStatus, ShowID"
                            + " ) AS r_1 Where ShowID='" + showID + "' GROUP BY CAST(reg_datecreated AS DATE),  ShowID";

            dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        }
        catch(Exception ex)
        { }

        return dt;
    }
    public DataTable GetReportByRegion(string showID)
    {
        DataTable dt = new DataTable();
        try
        {
            string sql = "SELECT regionname AS regionname,Sum(appRegStatus) As appRegStatus,Sum(rejRegStatus) As rejRegStatus, ShowID"
                            + " FROM (SELECT regionname AS regionname,   COUNT(RegStatus) As appRegStatus,0 As rejRegStatus, 'Approved' AS RegStatus, ShowID"
                            + " From (Select cc.Region AS regionname,'Approved' AS RegStatus,ShowID FROM dbo.tb_RegDelegate AS r "
                            + " LEFT OUTER JOIN dbo.ref_country AS cc ON r.reg_Country = CONVERT(nvarchar, cc.Cty_GUID)"
                            + " WHERE (recycle = 0) AND (RegGroupID IN"
                            + " (SELECT RegGroupID FROM dbo.tb_RegGroup WHERE (recycle = 0) AND (RG_IsMultiple IS NULL OR RG_IsMultiple LIKE 'I'))) AND (RegGroupID NOT IN"
                            + " (SELECT RegGroupId FROM dbo.tb_Invoice WHERE (RegGroupId IS NOT NULL))) And reg_approveStatus In ('33','22','11') And reg_Status=1) As aa"
                            + " GROUP BY regionname, RegStatus, ShowID UNION Select regionname,0 As appRegStatus, COUNT(RegStatus) As rejRegStatus, RegStatus, ShowID"
                            + " From(SELECT cc.Region AS regionname,   'Reject' AS RegStatus, ShowID FROM  dbo.tb_RegDelegate AS r "
                            + " LEFT OUTER JOIN dbo.ref_country AS cc ON r.reg_Country = CONVERT(nvarchar, cc.Cty_GUID)"
                            + " WHERE (recycle = 0) AND (RegGroupID IN (SELECT RegGroupID"
                            + " FROM dbo.tb_RegGroup WHERE (recycle = 0) AND (RG_IsMultiple IS NULL OR RG_IsMultiple LIKE 'I'))) AND (RegGroupID NOT IN"
                            + " (SELECT RegGroupId FROM dbo.tb_Invoice WHERE (RegGroupId IS NOT NULL)))"
                            + " And reg_approveStatus In ('66') And reg_Status=1) as bb GROUP BY regionname, RegStatus, ShowID"
                            + " ) AS r_1 Where ShowID='" + showID + "' GROUP BY regionname,  ShowID";

            dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    public DataTable GetReportByCountry(string showID)
    {
        DataTable dt = new DataTable();
        try
        {
            string sql = "SELECT countryname AS countryname,Sum(appRegStatus) As appRegStatus,Sum(rejRegStatus) As rejRegStatus, ShowID"
                            + " FROM (SELECT reg_Country AS countryname,   COUNT(RegStatus) As appRegStatus,0 As rejRegStatus, 'Approved' AS RegStatus, ShowID"
                            + " From (Select CASE WHEN ISNUMERIC(r.reg_Country) > 0 THEN cc.Country ELSE r.reg_Country END AS reg_Country,'Approved' AS RegStatus,ShowID FROM dbo.tb_RegDelegate AS r"
                            + " LEFT OUTER JOIN dbo.ref_country AS cc ON r.reg_Country = CONVERT(nvarchar, cc.Cty_GUID)"
                            + " WHERE (recycle = 0) AND (RegGroupID IN"
                            + " (SELECT RegGroupID FROM dbo.tb_RegGroup WHERE (recycle = 0) AND (RG_IsMultiple IS NULL OR RG_IsMultiple LIKE 'I'))) AND (RegGroupID NOT IN"
                            + " (SELECT RegGroupId FROM dbo.tb_Invoice WHERE (RegGroupId IS NOT NULL))) And reg_approveStatus In ('33','22','11') And reg_Status=1) As aa"
                            + " GROUP BY reg_Country, RegStatus, ShowID UNION Select reg_Country AS countryname,0 As appRegStatus, COUNT(RegStatus) As rejRegStatus, RegStatus, ShowID"
                            + " From(SELECT CASE WHEN ISNUMERIC(r.reg_Country) > 0 THEN cc.Country ELSE r.reg_Country END AS reg_Country,   'Reject' AS RegStatus, ShowID FROM  dbo.tb_RegDelegate AS r"
                            + " LEFT OUTER JOIN dbo.ref_country AS cc ON r.reg_Country = CONVERT(nvarchar, cc.Cty_GUID)"
                            + " WHERE (recycle = 0) AND (RegGroupID IN (SELECT RegGroupID"
                            + " FROM dbo.tb_RegGroup WHERE (recycle = 0) AND (RG_IsMultiple IS NULL OR RG_IsMultiple LIKE 'I'))) AND (RegGroupID NOT IN"
                            + " (SELECT RegGroupId FROM dbo.tb_Invoice WHERE (RegGroupId IS NOT NULL)))"
                            + " And reg_approveStatus In ('66') And reg_Status=1) as bb GROUP BY reg_Country, RegStatus, ShowID"
                            + " ) AS r_1 Where ShowID='" + showID + "' GROUP BY countryname,  ShowID";

            dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }

}
