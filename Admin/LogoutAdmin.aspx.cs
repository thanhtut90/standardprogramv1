﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Registration;
public partial class Admin_LogoutAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string cUser = "";
            if (Session["username"] != null)
                cUser = Session["username"].ToString();

            Session.Clear();
            Session.Abandon();
            removeCookie();

            Functionality fn = new Functionality();
            if (!string.IsNullOrEmpty(cUser))
            {
                LoginControler loginCtr = new LoginControler(fn);
                loginCtr.AdminUserSingOut(cUser);
            }

            string param = HttpContext.Current.Request.Url.AbsoluteUri;

            string url = param.Substring(0, param.LastIndexOf('/')) + "/" + "Login.aspx";

            Response.Clear();
            var sb = new System.Text.StringBuilder();
            sb.Append("<html>");
            sb.AppendFormat("<body onload='document.forms[0].submit()'>");
            sb.AppendFormat("<form action='{0}' method='post'>", url);
            sb.Append("</form>");
            sb.Append("</body>");
            sb.Append("</html>");
            Response.Write(sb.ToString());
            Response.End();
        }
    }
    public void removeCookie()
    {
        HttpCookie aCookie;
        string cookieName;
        int limit = Request.Cookies.Count;
        for (int i = 0; i < limit; i++)
        {
            cookieName = Request.Cookies[i].Name;
            aCookie = new HttpCookie(cookieName);
            aCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(aCookie);
        }
    }
}