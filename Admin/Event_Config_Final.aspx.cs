﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Event_Config_Final : System.Web.UI.Page
{
    CommonFuns cFun = new CommonFuns();
    Functionality fn = new Functionality();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            hplLink.NavigateUrl = string.Format("http://203.125.155.188/StandardRegWeb_2016/Welcome?SHW={0}&FLW={1}", Request.Params["SHW"].ToString(), Request.Params["FLW"].ToString());

            //if (Request.QueryString["STP"] != null)
            //{

            //}

            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            CategoryClass catClass = new CategoryClass();
            string curStep = catClass.defaultID;

            FlowMaster fMaster = new FlowMaster();
            fMaster.FlowID = flowid;
            fMaster.RecordStatus = RecordStatus.ACTIVE;
            fMaster.FlowStatus = SiteFlowType.DEFAULT_FLOW_TERMINAL;
            FlowControler fControler = new FlowControler(fn);
            fControler.UpdateFlowMasterStatus(fMaster);
            fControler.UpdateFlowMasterRecordStatus(fMaster);
        }
        else
        {
            hplLink.Visible = false;
        }
    }
}