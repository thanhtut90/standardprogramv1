﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="SummaryByUser_SGCCSurvey.aspx.cs" Inherits="Admin_SummaryByUser_SGCCSurvey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" href="../Content/dist/css/skins/_all-skins.min.css">
    <%--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script> <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>--%>
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }

        $(function () {
            $.fn.datepicker.defaults.format = "dd/mm/yyyy";
            //     $("#txtFromDate").datepicker({}).val()
            //    $("#txtToDate").datepicker({}).val()
        });
    </script>
    <style type="text/css">
        .tdstyle1 {
            width: 170px;
        }

        .tdstyle2 {
            width: 300px;
        }

        form input[type="text"] {
            width: 39% !important;
        }

        .ajax__calendar_container {
            width: 320px !important;
            height: 280px !important;
        }

        .ajax__calendar_body {
            width: 100% !important;
            height: 100% !important;
        }

        td {
            vertical-align: middle;
        }
        /*#ctl00_ContentPlaceHolder1_GKeyMaster_GridData
        {
           height:650px !important;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    &nbsp;&nbsp;&nbsp;
    <asp:LinkButton ID="btnAddNew" runat="server" CssClass="btn btn-success" OnClick="lnkExcel_Clicked">
            <span aria-hidden="true" class="glyphicon glyphicon-export"></span> EXPORT EXCEL
    </asp:LinkButton>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Label ID="lblUser" runat="server" Visible="false"></asp:Label>
            <h3 class="box-title">Summary By Data Entry User (SGCC Survey)</h3>
            <div id="divsearch" runat="server" class="row" style="margin-bottom: 10PX;" visible="true">
                <div class="form-group col-sm-12">
                    <label for="inputEmail3" class="col-sm-1 control-label">Show List: &nbsp;</label>
                    <div class="col-sm-2">
                        <asp:Panel ID="showlist" runat="server">
                            <asp:DropDownList ID="ddl_showList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_showList_SelectedIndexChanged" CssClass="form-control">
                            </asp:DropDownList>
                        </asp:Panel>
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label for="inputEmail3" class="col-sm-1 control-label"> Flow List: &nbsp;</label>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="ddl_flowList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_flowList_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <br />
            <div style="overflow-x: scroll;">
                 <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" 
                     Font-Size="Small" OnItemCommand="GKeyMaster_ItemCommand"
                        PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="GKeyMaster_NeedDataSource" AllowPaging="true" PageSize="50" 
                     OnPageIndexChanged="GKeyMaster_PageIndexChanged">
                    <ClientSettings AllowKeyboardNavigation="true">
                        <Selecting AllowRowSelect="true"></Selecting>
                    </ClientSettings>
                    <MasterTableView AutoGenerateColumns="true" CommandItemDisplay="TopAndBottom" HeaderStyle-BackColor="#d9d9d9"
                        HeaderStyle-Font-Bold="true" >
                    <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

