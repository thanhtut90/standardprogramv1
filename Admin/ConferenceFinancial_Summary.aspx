﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ConferenceFinancial_Summary.aspx.cs" Inherits="Admin_ConferenceFinancial_Summary" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../Content/dist/css/skins/_all-skins.min.css">
    <%--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script> <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>--%>
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }

        $(function () {
            $.fn.datepicker.defaults.format = "dd/mm/yyyy";
            //     $("#txtFromDate").datepicker({}).val()
            //    $("#txtToDate").datepicker({}).val()
        });
    </script>
    <style type="text/css">
        .tdstyle1 {
            width: 170px;
        }

        .tdstyle2 {
            width: 300px;
        }

        form input[type="text"] {
            width: 39% !important;
        }

        .ajax__calendar_container {
            width: 320px !important;
            height: 280px !important;
        }

        .ajax__calendar_body {
            width: 100% !important;
            height: 100% !important;
        }

        td {
            vertical-align: middle;
        }
    </style>
 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    &nbsp;&nbsp;&nbsp;
    <asp:LinkButton ID="btnAddNew" runat="server" CssClass="btn btn-success" OnClick="lnkExcel_Clicked">
            <span aria-hidden="true" class="glyphicon glyphicon-export"></span> EXPORT EXCEL
    </asp:LinkButton>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="" Transparency="30"><%-- BackgroundTransparency="50"--%>
                    <asp:Label ID="Label2" runat="server" Text="Loading..">
                    </asp:Label>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/loading-icon.gif" />
                </telerik:RadAjaxLoadingPanel>
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true" LoadingPanelID="RadAjaxLoadingPanel1">

            <asp:Label ID="lblUser" runat="server" Visible="false"></asp:Label>
            <h3 class="box-title">Conference Financial Summary</h3>
            <br />
            <br />
            <div style="overflow-x: scroll;">
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientEvents-OnRequestStart="onRequestStart">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <%-- <telerik:AjaxSetting AjaxControlID="btnupdate">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                            <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial"></telerik:AjaxUpdatedControl>
                        </UpdatedControls>
                    </telerik:AjaxSetting>--%>
                    </AjaxSettings>
                </telerik:RadAjaxManager>

                <%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">--%>
                    <asp:Panel runat="server" ID="PanelKeyList">
                        <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true" 
                            EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                            OnNeedDataSource="GKeyMaster_NeedDataSource" PageSize="10"    >
                            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                            </ClientSettings>
                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="SHW_ID" AllowFilteringByColumn="True" ShowFooter="false">
                                <CommandItemSettings ShowExportToExcelButton="False" ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Show ID" SortExpression="SHW_ID" UniqueName="SHW_ID"
                                        FilterControlAltText="Filter Show ID" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("SHW_ID").ToString()%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Show Name" SortExpression="SHW_Name" UniqueName="SHW_NAME"
                                        FilterControlAltText="Filter Show Name" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("SHW_Name").ToString()%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Item Name" SortExpression="con_ItemName" UniqueName="con_ItemName"
                                        FilterControlAltText="Filter con_ItemName" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("con_ItemName").ToString()%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn DataField="Profession" HeaderText="Profession" UniqueName="Profession"
                                        FilterControlAltText="Filter Profession" SortExpression="Profession" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("Profession").ToString()%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="PriceType" HeaderText="Price Type" UniqueName="PriceType"
                                        FilterControlAltText="Filter PriceType" SortExpression="PriceType" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("PriceType").ToString()%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn DataField="Qty" HeaderText="Qty" UniqueName="Qty"
                                        FilterControlAltText="Filter Qty" SortExpression="Qty" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("Qty").ToString()%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                                                        
                                    <telerik:GridTemplateColumn HeaderText="Price" SortExpression="Item Price" UniqueName="Price"
                                        FilterControlAltText="Filter Price" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("ItemPrice")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>                                    

                                    <telerik:GridTemplateColumn HeaderText="Amount" SortExpression="Amount" UniqueName="Amount"
                                        FilterControlAltText="Filter Amount" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("Amount")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>                                    
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </asp:Panel>
                </telerik:RadAjaxPanel>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


