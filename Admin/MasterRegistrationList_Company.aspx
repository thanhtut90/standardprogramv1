﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="MasterRegistrationList_Company.aspx.cs" Inherits="Admin_MasterRegistrationList_Company" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }
    </script>
    <style type="text/css">
        .tdstyle1
        {
            width:170px;
        }

        .tdstyle2
        {
            width:300px;
        }

        form input[type="text"]
        {
            width:39% !important;
        }

        .ajax__calendar_container
        {
            width: 320px !important;
            height:280px !important;
        }

        .ajax__calendar_body
        {
            width: 100% !important;
            height:100% !important;
        }

        td
        {
            vertical-align:middle;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
    <h3 class="box-title">Master Registration List (Company)</h3>
    <asp:Panel ID="showlist" runat="server">Show List : <asp:DropDownList ID="ddl_showList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_showList_SelectedIndexChanged"></asp:DropDownList></asp:Panel>
    
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientEvents-OnRequestStart="onRequestStart">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
        <asp:Panel runat="server" ID="PanelKeyList">
            <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                onneeddatasource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="5" OnItemDataBound="grid_ItemDataBound">
                <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true"></Selecting>
                </ClientSettings>
                <MasterTableView  CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="RegGroupID">
                    <CommandItemSettings ShowExportToExcelButton="true"  ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                    <Columns>

                        <telerik:GridButtonColumn ConfirmTextFormatString="Do you want to Delete Company of {0}?" ConfirmTextFields="RegGroupID" CommandArgument="RegGroupID"
                            ConfirmDialogType="RadWindow" CommandName="Delete" ButtonType="ImageButton" ImageUrl="images/delete.jpg" UniqueName="Delete"
                            HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                        </telerik:GridButtonColumn>

                        <telerik:GridButtonColumn CommandName="Edit" ButtonType="ImageButton" ImageUrl="images/edit.jpg" UniqueName="Edit"
                            HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                        </telerik:GridButtonColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RegGroupID" FilterControlAltText="Filter RegGroupID"
                            HeaderText="RegGroupID" SortExpression="RegGroupID" UniqueName="RegGroupID">
                        </telerik:GridBoundColumn>
                        
                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_urlFlowID" FilterControlAltText="Filter RC_urlFlowID"
                            HeaderText="" SortExpression="RC_urlFlowID" UniqueName="FlowID"  Display="false" >
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Name" FilterControlAltText="Filter RC_Name"
                            HeaderText="" SortExpression="RC_Name" UniqueName="RC_Name">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Address1" FilterControlAltText="Filter RC_Address1"
                            HeaderText="" SortExpression="RC_Address1" UniqueName="RC_Address1">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Address2" FilterControlAltText="Filter RC_Address2"
                            HeaderText="" SortExpression="RC_Address2" UniqueName="RC_Address2">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Address3" FilterControlAltText="Filter RC_Address3"
                            HeaderText="" SortExpression="RC_Address3" UniqueName="RC_Address3">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_City" FilterControlAltText="Filter RC_City"
                            HeaderText="" SortExpression="RC_City" UniqueName="RC_City">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_State" FilterControlAltText="Filter RC_State"
                            HeaderText="" SortExpression="RC_State" UniqueName="RC_State">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_ZipCode" FilterControlAltText="Filter RC_ZipCode"
                            HeaderText="" SortExpression="RC_ZipCode" UniqueName="RC_ZipCode">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Country" FilterControlAltText="Filter RC_Country"
                            HeaderText="" SortExpression="RC_Country" UniqueName="RC_Country">
                        </telerik:GridBoundColumn>

                        <telerik:GridTemplateColumn HeaderText="" SortExpression="RC_Tel" UniqueName="RC_Tel"
                            FilterControlAltText="Filter RC_Tel">
                            <ItemTemplate>
                                <asp:Label ID="lbl_tel" runat="server"><%# Eval("RC_Telcc")%><%#Eval("RC_Telac")%><%#Eval("RC_Tel")%></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="" SortExpression="RC_Fax" UniqueName="RC_Fax"
                            FilterControlAltText="Filter RC_Fax">
                            <ItemTemplate>
                                <asp:Label ID="lbl_fax" runat="server"><%# Eval("RC_Faxcc")%><%# Eval("RC_Faxac")%><%#Eval("RC_Fax")%></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Email" FilterControlAltText="Filter RC_Email"
                            HeaderText="" SortExpression="RC_Email" UniqueName="RC_Email">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Website" FilterControlAltText="Filter RC_Website"
                            HeaderText="" SortExpression="RC_Website" UniqueName="RC_Website">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Additional1" FilterControlAltText="Filter RC_Additional1"
                            HeaderText="" SortExpression="RC_Additional1" UniqueName="RC_Additional1">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Additional2" FilterControlAltText="Filter RC_Additional2"
                            HeaderText="" SortExpression="RC_Additional2" UniqueName="RC_Additional2">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Additional3" FilterControlAltText="Filter RC_Additional3"
                            HeaderText="" SortExpression="RC_Additional3" UniqueName="RC_Additional3">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Additional4" FilterControlAltText="Filter RC_Additional4"
                            HeaderText="" SortExpression="RC_Additional4" UniqueName="RC_Additional4">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Additional5" FilterControlAltText="Filter RC_Additional5"
                            HeaderText="" SortExpression="RC_Additional5" UniqueName="RC_Additional5">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_CreatedDate" FilterControlAltText="Filter RC_CreatedDate"
                            HeaderText="Created Date" SortExpression="RC_CreatedDate" UniqueName="RC_CreatedDate">
                        </telerik:GridBoundColumn>
                   
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </asp:Panel>
    </telerik:RadAjaxPanel>
            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>

