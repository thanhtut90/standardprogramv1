﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Conference_Controler.aspx.cs" Inherits="Event_Conference_Controler" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Dashboard | Manage Flow</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle"> Manage Conference List</h1>
        </div>

        <div id="contentwrapper" class="contentwrapper" style="padding-top: 50px;">
            
          

            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                            <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>

            <div style="padding-bottom:30px;">
                <asp:LinkButton ID="btnAddDefault" runat="server" OnClick="btnCreateFlow_Onclick" CssClass="btn btn-danger">
                    <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Add Flow's Default Confenence List
                </asp:LinkButton>
                <asp:LinkButton ID="btnPromo" runat="server" OnClick="btnRefFlow_Onclick" CssClass="btn btn-danger">
                    <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Add Additional Confenence List By Ref Code
                </asp:LinkButton>
            </div>
     
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
                <asp:Panel runat="server" ID="PanelKeyList">
                    <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                        EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                        OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="10" Skin="Bootstrap">
                        <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="reg_CategoryId,CType" ShowHeader="false">
                            <Columns>
                                <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_CategoryId" FilterControlAltText="Filter reg_CategoryId" Visible="false"
                                    HeaderText="Flow ID" SortExpression="reg_CategoryId" UniqueName="reg_CategoryId" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_CategoryName" FilterControlAltText="Filter reg_CategoryName"
                                    HeaderText="Detail Description" SortExpression="reg_CategoryName" UniqueName="reg_CategoryName" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>

                                <telerik:GridButtonColumn CommandArgument="id" Text="Manage Conference"
                                    ConfirmDialogType="RadWindow" CommandName="EditConference" ButtonType="LinkButton" UniqueName="EditConference"
                                    HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                </telerik:GridButtonColumn>

                                <%--<telerik:GridButtonColumn CommandArgument="reg_order" Text="View Reference" Visible="true"
                                    ConfirmDialogType="RadWindow" CommandName="ViewRef" ButtonType="LinkButton" UniqueName="ViewRef"
                                    HeaderText="View" ItemStyle-HorizontalAlign="Center">
                                </telerik:GridButtonColumn>--%>

                                <telerik:GridTemplateColumn HeaderText="View">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbViewRef" runat="server" Visible='<%# setViewRefButtonVisibility(Eval("reg_order").ToString())%>' 
                                            OnClick="lbViewRef_Click" CommandArgument='<%# Eval("reg_CategoryId")%>' Text="View Reference"></asp:LinkButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </asp:Panel>
                <br />
                <br />

                <asp:Panel runat="server" ID="PanelKeyDetail">
                    <div class='page-header'>
                        <h2>Flow Steps</h2>
                        <div style="padding-top: 40px;">
                        </div>
                        <div style="padding-left: 30%;">
                            <asp:LinkButton ID="btnCreateFlow" runat="server" OnClick="btnCreateFlow_Onclick" CssClass="btn btn-danger">
                                <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Edit
                            </asp:LinkButton>

                            <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
                             <asp:TextBox runat="server" ID="txtFlow" Visible="false" Text=""></asp:TextBox>
                        </div>
                    </div>
                </asp:Panel>
            </telerik:RadAjaxPanel>
        </div>

        <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" CausesValidation="false" CssClass="btn btn-success pull-right" />
    </div>
</asp:Content>

