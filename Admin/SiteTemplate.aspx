﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="SiteTemplate.aspx.cs" Inherits="Admin_SiteTemplate" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Dashboard | Site Template</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <%--<telerik:AjaxSetting AjaxControlID="GKeyMaster">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="panelsitetemplate" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnadd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="panelsitetemplate"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnsave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="panelsitetemplate"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
           <%-- <telerik:AjaxSetting AjaxControlID="tempRadGrid">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tempRadGrid"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tempRadGrid">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tempRadGrid"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtvalue">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtvalue"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Site Template</h1>
            <span class="pagedesc">Page to manage Template Style.</span>

            <ul class="hornav">
                <li class="current"><a href="#index"><asp:Label runat="server" ID="lblheader"></asp:Label></a></li>
            </ul>
        </div>
        <br />
        <br />

            <%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">--%>
        <div>
            <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                OnNeedDataSource="GKeyMaster_NeedDataSource1" OnItemCommand="GKeyMaster_ItemCommand" PageSize="15" Skin="Bootstrap">
                <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true"></Selecting>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" DataKeyNames="temp_id">
                    <Columns>
                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="temp_id" FilterControlAltText="Filter temp_id"
                            HeaderText="temp_id" Visible="true" SortExpression="temp_id" UniqueName="temp_id" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                        </telerik:GridBoundColumn>

                        <telerik:GridTemplateColumn HeaderText="Template Name" ItemStyle-Width="240px">
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "temp_name")%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Template Base DataSource" ItemStyle-Width="240px">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# getdatasourcename(Eval("temp_BaseDSource").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Template Subject" ItemStyle-Width="240px">
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "temp_Subject")%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </div>

        <%--<div id="contentwrapper" class="contentwrapper">
            <div id="index" class="subcontent">--%>
                <br />
                <asp:Button runat="server" ID="btnaddnewrecord" Text="Add New Template" OnClick="btnaddnewrecord_Click"  CssClass="btn btn-primary"/> &nbsp;
              
         <asp:Button runat="server" ID="Button1" Text="Back" OnClick="btnback_Click" CssClass="btn btn-primary" CausesValidation="false"/>
                <asp:Label runat="server" id="lbls" ForeColor="Red"></asp:Label>
                <br />
                <br />
                <asp:Panel runat="server" ID="panelsitetemplate" Visible="false">
                    <table>
                        <tr>
                            <td>
                                <table class="stdtable">
                                    <tr>
                                        <td>Template ID</td>
                                        <td>
                                            <telerik:RadTextBox RenderMode="Lightweight" ID="txttempid" runat="server"></telerik:RadTextBox>
                                            <asp:RequiredFieldValidator ID="rfvtempid" runat="server" Display="Dynamic"
                                                ControlToValidate="txttempid" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Template Title</td>
                                        <td>
                                            <telerik:RadTextBox RenderMode="Lightweight" ID="txttemptitle" runat="server"></telerik:RadTextBox>
                                            <asp:RequiredFieldValidator ID="rfvtemptitle" runat="server" Display="Dynamic"
                                                ControlToValidate="txttemptitle" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Template DataSource</td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddltempdatasource" AutoPostBack="true" OnSelectedIndexChanged="ddltempdatasource_SelectedIndexChanged"></asp:DropDownList>
                                            <table runat="server">
                                                <tr>
                                                    <td>Table_FilterKey : </td>
                                                    <td><asp:Label runat="server" ID="lbltblfk"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td>Table_CotactEmailColumn : </td>
                                                    <td><asp:Label runat="server" ID="lbltblcec"></asp:Label></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Template Subject</td>
                                        <td>
                                            <%--<asp:TextBox ID="txtsubject" runat="server"></asp:TextBox>--%>
                                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtsubject" runat="server"></telerik:RadTextBox>
                                            <asp:RequiredFieldValidator ID="rfvsubject" runat="server" Display="Dynamic"
                                                ControlToValidate="txtsubject" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Template Value</td>
                                        <td>
                                          <CKEditor:CKEditorControl ID="txtvalue" runat="server"  Height="457px" Width="98%" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <div>
                                    <ul class="hornav"><li>Edit</li></ul>
                                    <asp:LinkButton runat="server" ID="linkbtndefaultbanner" OnClick="linkbtndefaultbanner_Click" Text="Default Banner"></asp:LinkButton>
                                    <br />
                                    <asp:ImageButton runat="server" ID="imgdefaultbanner" OnClick="imgdefaultbanner_Click" Width="141px" Height="48px" />
                                    <asp:Repeater runat="server" ID="repeater1" DataSourceID="tempdatasource">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" Text='<%# Eval("Column_name") %>' CommandArgument='<%# Eval("Column_name") %>'
                                                 ID="lbtntemp" OnClick="lbtntemp_Click"></asp:LinkButton>
                                            <br />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <asp:Button runat="server" ID="btnsave" Text ="Save" OnClick="btnsave_Click" />
                    <asp:Button runat="server" ID="btnadd" Text ="Add" onclick="btnadd_Click" />&nbsp;&nbsp;
                    <asp:Button runat="server" ID="btnpreview" Text="Preview" OnClick="btnpreview_Click" />
                </asp:Panel>
           <%-- </telerik:RadAjaxPanel>--%>
           <%-- </div>
        </div>--%>
        <asp:Panel runat="server" ID="panelpreview" Visible="false">
            <asp:Label runat="server" ID="lblpreview"></asp:Label>
            <br />
            <asp:Button runat="server" ID="btnback" Text="Back" OnClick="btnback_Click" />
        </asp:Panel>
    </div>

    <asp:SqlDataSource ID="tempdatasource" runat="server" connectionString="<%$ ConnectionStrings:CMSConnString %>"
        SelectCommand="select Column_name from Information_schema.columns where Table_name =@tablename">
        <SelectParameters>
            <asp:Parameter Name="tablename" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

