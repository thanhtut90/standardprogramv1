﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Admin_ManageCountry : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PanelKeyDetail.Visible = false;
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From ref_country table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            string query = string.Empty;
            query = "Select * From ref_country Where Countryen != 'Countrycode' Order By Country";
            DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];

            if (dt.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dt;
            }
            else
            {
                lbls.Text = "There is no record.";
            }
        }
        catch (Exception)
        {
            lbls.Text = "There is no record.";
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string curid = "";
        foreach (GridDataItem item in GKeyMaster.SelectedItems)
        {
            curid = item["Cty_GUID"].Text;
        }
        if (e.CommandName == "Delete")
        {
            GridDataItem item = (GridDataItem)e.Item;

            string curID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Cty_GUID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Cty_GUID"].ToString() : "";
            if (!string.IsNullOrEmpty(curID))
            {
                int isDeleted = fn.ExecuteSQL(string.Format("Delete From ref_country Where Cty_GUID='{0}'", curID));
                if (isDeleted > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already deleted.');", true);
                }
                else
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Deleting error.');", true);
                }

                ResetControls();
                lbls.Text = "";
                PanelKeyDetail.Visible = false;
                btnAdd.Visible = false;
                btnUpdateRecord.Visible = false;
            }
        }
        else
        {
            if (curid != "" && curid != null)
            {
                btnUpdateRecord.Visible = true;
                btnAdd.Visible = false;
                PanelKeyDetail.Visible = true;

                loadvalue(curid);
            }
        }
    }
    #endregion

    #region btnAddNew_Click (Load when click Add New button)
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        ResetControls();
        lbls.Text = "";
        btnUpdateRecord.Visible = false;
        btnAdd.Visible = true;
        PanelKeyDetail.Visible = true;
    }
    #endregion

    #region btnAdd_Click (Insert the respective data into ref_country table)
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lbls.Text = "";
            string crycode = cFun.solveSQL(txtCountryCode.Text.Trim());
            string crycn = cFun.solveSQL(txtCountryCn.Text.Trim());
            string cryeng = cFun.solveSQL(txtCountryEng.Text.Trim());

            try
            {
                DataTable dtmaster = fn.GetDatasetByCommand("Select * From ref_country Where Country='" + cryeng + "'", "ds").Tables[0];

                if (dtmaster.Rows.Count > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This country already exist.');", true);
                    return;
                }
                else
                {
                    string query = string.Format("Insert Into ref_country (Countryen, Countrycn, Country)"
                        + " Values ('{0}', N'{1}', '{2}')"
                        , crycode, crycn, cryeng);

                    int rowInserted = fn.ExecuteSQL(query);
                    if (rowInserted == 1)
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Saving error, try again.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                lbls.Text = ex.Message;
            }
        }
    }
    #endregion

    #region loadvalue (Load RadGrid selected row data from ref_country table to bind the respective controls while edit)
    private void loadvalue(string moduleid)
    {
        string query = "Select * From ref_country Where Cty_GUID='" + moduleid + "'";

        DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
        if(dt.Rows.Count > 0)
        {
            hfID.Value = moduleid;
            txtCountryCode.Text = dt.Rows[0]["Countryen"].ToString();
            txtCountryCn.Text = dt.Rows[0]["Countrycn"].ToString();
            txtCountryEng.Text = dt.Rows[0]["Country"].ToString();
        }
    }
    #endregion

    #region btnUpdateRecord_Click (Update the respective data into "ref_country" table)
    protected void btnUpdateRecord_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lbls.Text = "";
            string curid = hfID.Value;
            string crycode = cFun.solveSQL(txtCountryCode.Text.Trim());
            string crycn = cFun.solveSQL(txtCountryCn.Text.Trim());
            string cryeng = cFun.solveSQL(txtCountryEng.Text.Trim());

            try
            {
                DataTable dtmaster = fn.GetDatasetByCommand("Select * From ref_country Where Country='" + cryeng + "' And Cty_GUID!=" + curid, "ds").Tables[0];

                if (dtmaster.Rows.Count > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This country already exist.');", true);
                    return;
                }
                else
                {
                    string query = string.Format("Update ref_country Set Countryen='{0}', Countrycn=N'{1}', Country='{2}'"
                    + " Where Cty_GUID={3}", crycode, crycn, cryeng, curid);

                    int rowUpdated = fn.ExecuteSQL(query);
                    if (rowUpdated == 1)
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updating error, try again.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                lbls.Text += ex.Message;
            }
        }
    }
    #endregion

    #region ResetControls
    private void ResetControls()
    {
        hfID.Value = "";
        txtCountryCode.Text = "";
        txtCountryCn.Text = "";
        txtCountryEng.Text = "";
    }
    #endregion

    #region lnkBack_Click (redirect to EventSettings.aspx)
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            //Response.Redirect("Event_Settings_Master.aspx?SHW=" + Request.QueryString["SHW"].ToString());
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
}