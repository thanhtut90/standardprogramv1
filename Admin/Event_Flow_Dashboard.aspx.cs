﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Event_Flow_Dashboard : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                txtShowID.Text = cFun.DecryptValue(Request.Params["SHW"].ToString());
                string flw = cFun.DecryptValue(Request.Params["FLW"].ToString());
                txtFlowID.Text = flw;
                string sql = string.Format("select f.step_seq,m.script_desc,m.backendref_scriptID,* from tb_site_flow f, tb_site_module m where m.script_id= f.script_id and FLoW_ID='{0}' order by f.step_seq", flw);

                DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
                RptSteps.DataSource = dt;
                RptSteps.DataBind();

                FlowControler flwControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flw);
                if (!string.IsNullOrEmpty(flwMasterConfig.FlowName))
                {
                    txtFlowName.Text = flwMasterConfig.FlowName;
                }
                if (!string.IsNullOrEmpty(flwMasterConfig.FlowWelcomePageMsg))
                {
                    txtWelcomeMsg.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowWelcomePageMsg) ? flwMasterConfig.FlowWelcomePageMsg : "");
                }
                txtWelcomeMsgTmpID.Text = flwMasterConfig.FlowWelcomePageTmpID.Trim();

                /*added on 30-11-2018*/
                if (!string.IsNullOrEmpty(flwMasterConfig.FlowWelcomePageMsgUnderRegButton))
                {
                    txtWelcomeMsgUnderRegButton.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowWelcomePageMsgUnderRegButton) ? flwMasterConfig.FlowWelcomePageMsgUnderRegButton : "");
                }
                txtWelcomeMsgUnderRegButtonTmpID.Text = flwMasterConfig.FlowWelcomePageMsgUnderRegButtonTmpID.Trim();
                /*added on 30-11-2018*/

                //***added on 9-10-2018
                if (!string.IsNullOrEmpty(flwMasterConfig.FlowReloginIndexIncompletePageMsg))
                {
                    txtReloginIndexIncompleteMsg.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowReloginIndexIncompletePageMsg) ? flwMasterConfig.FlowReloginIndexIncompletePageMsg : "");
                }
                txtReloginIndexIncompleteMsgTmpID.Text = flwMasterConfig.FlowReloginIndexIncompletePageTmpID.Trim();

                if (!string.IsNullOrEmpty(flwMasterConfig.FlowReloginHomeCompletePageMsg))
                {
                    txtReloginHomeCompleteMsg.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowReloginHomeCompletePageMsg) ? flwMasterConfig.FlowReloginHomeCompletePageMsg : "");
                }
                txtReloginHomeCompleteMsgTmpID.Text = flwMasterConfig.FlowReloginHomeCompletePageTmpID.Trim();
                //***added on 9-10-2018

                Flow flow = new Flow();
                flow.FlowID = flw;
                if (flwControl.CheckFlowConfigComplete(flow) && flwMasterConfig.RecordStatus== RecordStatus.ACTIVE)
                {
                    btnFinalizeFlow.Visible = false;
                    btnUpdateFlowInfo.Visible = true;
                }
                else
                {
                    btnFinalizeFlow.Visible = true;
                    btnUpdateFlowInfo.Visible = false;

                }

            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region LoadDetails
    protected void btnEdit_Onclick(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        string[] arrArguments = btn.CommandArgument.Split(';');
        string browsePage = arrArguments[0];
        string step = arrArguments[1];
        string stepDesc = arrArguments[2];
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && !string.IsNullOrEmpty(browsePage) && !string.IsNullOrEmpty(step))
        {
            string shw = Request.Params["SHW"].ToString();
         
            string flw = cFun.DecryptValue(Request.Params["FLW"].ToString());
            if (btn.CommandName == "editPage")
            {
                string moduledesc = arrArguments[2];
                FlowControler fControl = new FlowControler(fn);
                string url = string.Empty;

                string sqlModuleType = "select t.ModuleType from tb_site_flow_template t left join tb_site_module m on m.module_id=t.Module_Id"
                                        + " where Route_ID in (select flW_REF_TEMPLATE from tb_site_flow_master where FLW_ID='"+ flw + "') And t.Module_Title='" + stepDesc + "' order by RouteOrder asc";

                string moduleType = fn.GetDataByCommand(sqlModuleType, "ModuleType");
                if (!string.IsNullOrEmpty(moduleType) && moduleType != "0")
                {
                    url = fControl.MakeFullURL(browsePage, flw, shw, "", step, null, moduleType);
                }
                else
                {
                    url = fControl.MakeFullURL(browsePage, flw, shw, "", step);
                }

                url = url + "&a=" + cFun.EncryptValue(BackendStaticValueClass.isFlowEdit);
                Response.Redirect(url);


            }
            else if (btn.CommandName == "previewPage")
            {
                showPreviewPage(browsePage, step, flw, shw);
            }
        }
    }

    public bool isEditable(string backScript)
    {
        bool isCanEdit = true;
        if (string.IsNullOrEmpty(backScript)) isCanEdit = false;
        return isCanEdit;
    }
    public bool isPreviewable(string backScript)
    {
        bool isCanEdit = true;
        if (string.IsNullOrEmpty(backScript)) isCanEdit = false;
        return isCanEdit;
    }
    public bool isFlowNoteAddable(string isAddFlowNote)
    {
        bool isCanEdit = true;
        if (string.IsNullOrEmpty(isAddFlowNote)) isCanEdit = false;
        return isCanEdit;
    }
    #endregion

    #region PreviewPage
    private void showPreviewPage(string page, string step, string flowid, string showid)
    {
        FlowControler flwObj = new FlowControler(fn);
        string route = flwObj.MakeFullURL(page, flowid, showid, null, step);
        route = "../" + route + "&t=" + cFun.EncryptValue(BackendStaticValueClass.isAdmin);

        System.Drawing.Rectangle resolution =
        System.Windows.Forms.Screen.PrimaryScreen.Bounds;
        int width = resolution.Width;
        int height = resolution.Height;

        if (width <= 1014)
        {
            width = width - 100;
        }
        else
        {
            width = 1000;
        }

        RadWindow newWindow = new RadWindow();
        newWindow.NavigateUrl = route;
        newWindow.Height = 800;
        newWindow.Width = width;
        newWindow.VisibleOnPageLoad = true;
        newWindow.KeepInScreenBounds = true;
        newWindow.VisibleOnPageLoad = true;
        newWindow.Visible = true;
        newWindow.VisibleStatusbar = false;
        //RadWindowManager1.ReloadOnShow = true;
        RadWindowManager1.EnableViewState = false;
        RadWindowManager1.Windows.Add(newWindow);
    }
    #endregion

    #region btnUpdateFlowInfo_Click
    protected void btnUpdateFlowInfo_Click(object sender, EventArgs e)
    {
        try
        {
            bool rowUpdated = false;
            FlowMaster fMaster = new FlowMaster();
            fMaster.FlowID = txtFlowID.Text;
            fMaster.FlowName = txtFlowName.Text.Trim();
            TemplateControler tmpControler = new TemplateControler(fn);
            #region Create WelcomePageTemplate
            string tmpid = txtWelcomeMsgTmpID.Text.Trim();
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            TemplateObj tmpObj = new TemplateObj();
            tmpObj.tempid = tmpid;
            tmpObj.tempname = "";
            tmpObj.tempvalue = Server.HtmlEncode(txtWelcomeMsg.Text);
            tmpObj.tempDBSource = "";
            tmpObj.tempsubject = "";
            tmpObj.temptype = 3;
            tmpObj.showid = showid;

            bool rowUpdated1 = false;
            if (!string.IsNullOrEmpty(tmpid) && tmpid != "0")
            {
                rowUpdated1 = tmpControler.UpdateTemplateByID(tmpObj);
            }
            else
            {
                tmpid = "0";
                rowUpdated1 = tmpControler.CreateNewTemplate(tmpObj, ref tmpid);
            }

            if (rowUpdated1 && !string.IsNullOrEmpty(tmpid) && tmpid != "0")
            {
                fMaster.FlowWelcomePageTmpID = tmpid;
                FlowControler fControler = new FlowControler(fn);
                rowUpdated = fControler.UpdateFlowMasterWelcomPageMsg(fMaster);//.UpdateFlowMasterNameSucMsg(fMaster);
            }
            #endregion

            #region Create WelcomePageTemplate Under Reg Button [changed on 30-11-2018]
            string tmpUnderRegButtonid = txtWelcomeMsgUnderRegButtonTmpID.Text.Trim();
            tmpObj = new TemplateObj();
            tmpObj.tempid = tmpUnderRegButtonid;
            tmpObj.tempname = "";
            tmpObj.tempvalue = Server.HtmlEncode(txtWelcomeMsgUnderRegButton.Text);
            tmpObj.tempDBSource = "";
            tmpObj.tempsubject = "";
            tmpObj.temptype = 3;
            tmpObj.showid = showid;

            bool rowUpdated2 = false;
            if (!string.IsNullOrEmpty(tmpUnderRegButtonid) && tmpUnderRegButtonid != "0")
            {
                rowUpdated2 = tmpControler.UpdateTemplateByID(tmpObj);
            }
            else
            {
                tmpUnderRegButtonid = "0";
                rowUpdated2 = tmpControler.CreateNewTemplate(tmpObj, ref tmpUnderRegButtonid);
            }

            if (rowUpdated2 && !string.IsNullOrEmpty(tmpUnderRegButtonid) && tmpUnderRegButtonid != "0")
            {
                fMaster.FlowWelcomePageMsgUnderRegButtonTmpID = tmpUnderRegButtonid;
                FlowControler fControler = new FlowControler(fn);
                rowUpdated = fControler.UpdateFlowMasterWelcomPageMsgUnderRegButton(fMaster);
            }
            #endregion

            #region Create ReloginIndexIncompletePageTemplate
            string tmpidReloginIndex = txtReloginIndexIncompleteMsgTmpID.Text.Trim();
            tmpObj = new TemplateObj();
            tmpObj.tempid = tmpidReloginIndex;
            tmpObj.tempname = "";
            tmpObj.tempvalue = Server.HtmlEncode(txtReloginIndexIncompleteMsg.Text);
            tmpObj.tempDBSource = "";
            tmpObj.tempsubject = "";
            tmpObj.temptype = 3;
            tmpObj.showid = showid;

            rowUpdated1 = false;
            if (!string.IsNullOrEmpty(tmpidReloginIndex) && tmpidReloginIndex != "0")
            {
                rowUpdated1 = tmpControler.UpdateTemplateByID(tmpObj);
            }
            else
            {
                tmpidReloginIndex = "0";
                rowUpdated1 = tmpControler.CreateNewTemplate(tmpObj, ref tmpidReloginIndex);
            }

            if (rowUpdated1 && !string.IsNullOrEmpty(tmpidReloginIndex) && tmpidReloginIndex != "0")
            {
                fMaster.FlowReloginIndexIncompletePageTmpID = tmpidReloginIndex;
                FlowControler fControler = new FlowControler(fn);
                rowUpdated = fControler.UpdateFlowMasterReloginIndexPageMsg(fMaster);
            }
            #endregion

            #region Create ReloginHomeCompletePageTemplate
            string tmpidReloginHome = txtReloginHomeCompleteMsgTmpID.Text.Trim();
            tmpObj = new TemplateObj();
            tmpObj.tempid = tmpidReloginHome;
            tmpObj.tempname = "";
            tmpObj.tempvalue = Server.HtmlEncode(txtReloginHomeCompleteMsg.Text);
            tmpObj.tempDBSource = "";
            tmpObj.tempsubject = "";
            tmpObj.temptype = 3;
            tmpObj.showid = showid;

            rowUpdated1 = false;
            if (!string.IsNullOrEmpty(tmpidReloginHome) && tmpidReloginHome != "0")
            {
                rowUpdated1 = tmpControler.UpdateTemplateByID(tmpObj);
            }
            else
            {
                tmpidReloginHome = "0";
                rowUpdated1 = tmpControler.CreateNewTemplate(tmpObj, ref tmpidReloginHome);
            }

            if (rowUpdated1 && !string.IsNullOrEmpty(tmpidReloginHome) && tmpidReloginHome != "0")
            {
                fMaster.FlowReloginHomeCompletePageTmpID = tmpidReloginHome;
                FlowControler fControler = new FlowControler(fn);
                rowUpdated = fControler.UpdateFlowMasterReloginHomePageMsg(fMaster);
            }
            #endregion

            if (rowUpdated)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success.');", true);
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fail');", true);
                return;
            }
        }
        catch (Exception ex)
        { }
    }

    protected void btnFinalizeFlow_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            Flow flow = new Flow();
            flow.FlowID = txtFlowID.Text;
            FlowControler flwControl = new FlowControler(fn);
            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flow.FlowID);
            if (flwControl.CheckFlowConfigComplete(flow) && flwMasterConfig.RecordStatus==RecordStatus.PENDING)
            {
                string url = string.Format("{0}?FLW={1}&SHW={2} ", "Event_Config_Final", Request.Params["FLW"].ToString(), Request.Params["SHW"].ToString());
                Response.Redirect(url);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Complete Configuration!!');", true);
                return;
            }
        }
    }
    #endregion


    #region Repeater
    protected void RptSteps_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            Label lblConfigStatus = (Label)e.Item.FindControl("lblConfigStatus");
            Label lblConfigFinished = (Label)e.Item.FindControl("lblConfigFinished");
            Label lblConfigPending = (Label)e.Item.FindControl("lblConfigPending");
            LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");
            bool isFinished = false;
            if (btnEdit.Visible)
            {
                if (lblConfigStatus.Text == YesOrNo.Yes.ToString()) isFinished = true;
            }
            else
            {
                isFinished = true;
            }

            lblConfigFinished.Visible = isFinished;
            lblConfigPending.Visible = !isFinished;
        }
        catch { }
    }
    #endregion

    #region Page Header Changing
    protected void btnUpdatePageName_Click(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                string pageIndexID = hfPageIndexID.Value;
                if (!string.IsNullOrEmpty(pageIndexID))
                {
                    try
                    {
                        string query = "Select * From tb_site_flow Where flow_id=@FlwID And flow_step_index=@stepIndexID";
                        List<SqlParameter> pList = new List<SqlParameter>();
                        SqlParameter spar = new SqlParameter("stepIndexID", SqlDbType.NVarChar);
                        SqlParameter spar1 = new SqlParameter("FlwID", SqlDbType.NVarChar);
                        spar.Value = pageIndexID;
                        spar1.Value = flowid;
                        pList.Add(spar);
                        pList.Add(spar1);
                        DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            string updateSql = "Update tb_site_flow Set step_label=@step_label, lupdate_date=@lupdate_date, lupdate_by=@lupdate_by"
                                + " Where flow_id=@FlwID And flow_step_index=@stepIndexID";
                            SqlParameter spar2 = new SqlParameter("step_label", SqlDbType.NVarChar);
                            SqlParameter spar3 = new SqlParameter("lupdate_date", SqlDbType.DateTime);
                            SqlParameter spar4 = new SqlParameter("lupdate_by", SqlDbType.NVarChar);
                            spar2.Value = txtPageHeaderName.Text.Trim();
                            spar3.Value = DateTime.Now;
                            spar4.Value = Session["userid"].ToString();
                            pList.Add(spar2);
                            pList.Add(spar3);
                            pList.Add(spar4);
                            int success = fn.ExecuteSQLWithParameters(updateSql, pList);
                            hfPageIndexID.Value = "";
                            txtPageHeaderName.Text = "";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePop", "alert('Updated!');closeModal();", true);
                        }
                    }
                    catch (Exception ex)
                    { }
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        hfPageIndexID.Value = "";
        txtPageHeaderName.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePop", "closeModal();", true);
    }
    #endregion

    #region btnChangePageHeader_Click
    protected void btnChangePageHeader_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        try
        {
            string[] arrArguments = btn.CommandArgument.Split(';');
            string step = arrArguments[0];
            string stepIndexID = arrArguments[1];
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && !string.IsNullOrEmpty(step) && !string.IsNullOrEmpty(stepIndexID))
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                hfPageIndexID.Value = stepIndexID.Trim();
                string query = "Select * From tb_site_flow Where flow_id=@FlwID And flow_step_index=@stepIndexID";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("stepIndexID", SqlDbType.NVarChar);
                SqlParameter spar1 = new SqlParameter("FlwID", SqlDbType.NVarChar);
                spar.Value = stepIndexID.Trim();
                spar1.Value = flowid;
                pList.Add(spar);
                pList.Add(spar1);
                DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtPageHeaderName.Text = dt.Rows[0]["step_label"] != DBNull.Value ? dt.Rows[0]["step_label"].ToString() : "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region lblAddFlowNote_Click
    protected void lblAddFlowNote_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        string[] arrArguments = btn.CommandArgument.Split(';');
        string flowIndex = arrArguments[0];
        string step = arrArguments[1];
        string stepDesc = arrArguments[2];
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && !string.IsNullOrEmpty(flowIndex) && !string.IsNullOrEmpty(step))
        {
            string shw = cFun.DecryptValue(Request.Params["SHW"].ToString());
            string flw = cFun.DecryptValue(Request.Params["FLW"].ToString());
            if (btn.CommandName == "addFlowNote")
            {
                string browsePage = "Event_Flow_NoteTemplate.aspx";
                FlowControler fControl = new FlowControler(fn);
                string url = string.Empty;
                url = fControl.MakeFullURL(browsePage, flw, shw, "", step);
                url = url + "&a=" + cFun.EncryptValue(BackendStaticValueClass.isFlowEdit) + "&STPINDEX=" + cFun.DecryptValue(flowIndex);
                Response.Redirect(url);
            }
        }
    }
    #endregion
}