﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuestionnaireAdd : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showID = cFun.DecryptValue(Request.Params["SHW"]);
                string qaID = string.Empty;

                qaID = cFun.DecryptValue(Request.Params["QAID"]);

                if (!String.IsNullOrWhiteSpace(qaID) && checkInsertExist(qaID))
                {
                    bindData(qaID);
                    txtQAID.Text = qaID;
                }
                else
                {
                    string query = "Select SQ_QAID from tb_site_QA_Master order by SQ_QAID DESC";
                    qaID = fn.GetDataByCommand(query, "SQ_QAID");
                    int qid = Convert.ToInt32(Regex.Match(qaID, @"\d+").Value);
                    int newQID = (qid);
                    qaID = generateQAID(newQID);
                    txtQAID.Text = qaID;
                }
            }
            else {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected string generateQAID(int QID)
    {
        CommonFuns cFun = new CommonFuns();
        QAHelper qHelp = new QAHelper();
        int newQID = (QID + 1);
        string qaNewID = "QLST" + (newQID);
        bool OK = qHelp.checkExistQA("qnaire_id=" + cFun.EncryptValue(qaNewID));
        if (OK)
        {
            qaNewID = generateQAID(newQID);
        }

        return qaNewID;
    }

    protected void bindData(string qaID)
    {
        string sql = string.Format("Select * from tb_site_QA_Master where SQ_QAID = @QAID");
        string title = string.Empty;
        string desc = string.Empty;

        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("QAID", SqlDbType.NVarChar);
        spar.Value = qaID;
        pList.Add(spar);

        DataTable dt = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            title = dt.Rows[0]["SQ_QATitle"].ToString();
            desc = dt.Rows[0]["SQ_Desc"].ToString();
            qaID = dt.Rows[0]["SQ_QAID"].ToString();
        }

        txtqatitle.Text = title;
        txtqadesc.Text = desc;
    }

    /*protected void bindFlow()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            string query = "select FLW_ID,(flw_id +' - ' +flw_desc) As fullflw from tb_site_flow_master as sfm FULL OUTER JOIN tb_site_QA_Master as sqm ON sfm.FLW_ID = sqm.SQ_FLW_ID where sfm.ShowID = '" + cFun.DecryptValue(Request.Params["SHW"]) + "'";

            using (SqlCommand cmd = new SqlCommand(query))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_qaFlow.Items.Clear();
                ddl_qaFlow.DataSource = cmd.ExecuteReader();
                ddl_qaFlow.DataTextField = "fullflw";
                ddl_qaFlow.DataValueField = "FLW_ID";
                ddl_qaFlow.DataBind();
                con.Close();
            }
        }
    }*/

    /*protected void AddRemoveFlow(object sender, EventArgs e)
    {
        string flowID = ddl_qaFlow.SelectedValue;
        string allFlowID = lbl_flow.Text;

        if (!string.IsNullOrEmpty(flowID))
        {
            if (allFlowID.Contains(flowID))
            {
                if (allFlowID.StartsWith(flowID))
                {
                    lbl_flow.Text = allFlowID.Replace(flowID + "," , "");
                    lbl_flow.Text = allFlowID.Replace(flowID, "");
                }
                else
                {
                    lbl_flow.Text = allFlowID.Replace("," + flowID, "");
                }
            }
            else
            {
                if (allFlowID == string.Empty)
                {
                    lbl_flow.Text = flowID;
                }
                else
                {
                    lbl_flow.Text = allFlowID + "," + flowID;
                }
            }
        }
    }*/

    public bool checkInsertExist(string qaID)
    {
        bool isExist = false;

        string sql = string.Format("Select * from tb_site_QA_Master where SQ_QAID = @QAID");

        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("QAID", SqlDbType.NVarChar);
        spar.Value = qaID;
        pList.Add(spar);

        DataTable dt = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
        if (dt.Rows.Count > 0)
        {
            isExist = true;
        }

        return isExist;
    }

    protected void SaveForm(object sender, EventArgs e)
    {
        string showID = string.Empty;
        string title = string.Empty;
        string desc = string.Empty;
        string query = string.Empty;
        string qaID = string.Empty;

        title = txtqatitle.Text;
        desc = txtqadesc.Text;
        showID = cFun.DecryptValue(Request.Params["SHW"]);
        qaID = txtQAID.Text;

        if (title == string.Empty)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter title!');", true);
            return;
        }
        else
        {
            if (!checkInsertExist(qaID))
            {
                    query = "Insert into tb_site_QA_Master (SQ_ID,SQ_SHWID,SQ_FLW_ID,SQ_FLOW_SETP_REF_ID,SQ_QAID,SQ_QATitle,SQ_Desc,SQ_Category,CreateDate)"
                                + " Values(@SQID,@SQ_SHWID,@SQ_FLW_ID,@SQ_FLOW_SETP_REF_ID,@SQ_QAID,@SQ_QATitle,@SQ_Desc,@SQ_Category,@CreateDate)";

                    using (SqlConnection con = new SqlConnection(fn.ConnString))
                    {
                        using (SqlCommand command = new SqlCommand(query))
                        {
                            command.Connection = con;
                            command.Parameters.AddWithValue("@SQID", qaID);
                            command.Parameters.AddWithValue("@SQ_SHWID", showID);
                            command.Parameters.AddWithValue("@SQ_FLW_ID", 0);
                            command.Parameters.AddWithValue("@SQ_FLOW_SETP_REF_ID", "");
                            command.Parameters.AddWithValue("@SQ_QAID", qaID);
                            command.Parameters.AddWithValue("@SQ_QATitle", title);
                            command.Parameters.AddWithValue("@SQ_Desc", desc);
                            command.Parameters.AddWithValue("@SQ_Category", "");
                            command.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture));

                            con.Open();
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                }

                if (Request.Params["a"] != null)
                {
                    string admintype = cFun.DecryptValue(Request.QueryString["a"].ToString());
                    if (admintype == BackendStaticValueClass.isFlowEdit)
                    {
                        Response.Redirect("QuestionnaireAddItem?SHW=" + Request.Params["SHW"] + "&QAID=" + cFun.EncryptValue(qaID) + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["TYPE"] + "&a=" + cFun.EncryptValue(admintype));
                    }
                    else
                    {
                        Response.Redirect("QuestionnaireAddItem?SHW=" + Request.Params["SHW"] + "&QAID=" + cFun.EncryptValue(qaID) + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["TYPE"]);
                    }
                }
                else
                {
                    Response.Redirect("QuestionnaireAddItem?SHW=" + Request.Params["SHW"] + "&QAID=" + cFun.EncryptValue(qaID) + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["TYPE"]);
                }
            }

            else
            {
                query = string.Format("Select * from tb_site_QA_Master where SQ_QAID = @QAID");
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("QAID", SqlDbType.NVarChar);
                spar.Value = qaID;
                pList.Add(spar);

                DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
                query = "Update tb_site_QA_Master SET SQ_QATitle=@SQ_QATitle, SQ_Desc=@SQ_Desc where SQ_QAID = @SQ_QAID";
                using (SqlConnection con = new SqlConnection(fn.ConnString))
                {
                    using (SqlCommand command = new SqlCommand(query))
                    {
                        command.Connection = con;
                        command.Parameters.AddWithValue("@SQ_QAID", qaID);
                        command.Parameters.AddWithValue("@SQ_QATitle", title);
                        command.Parameters.AddWithValue("@SQ_Desc", desc);

                        con.Open();
                        command.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }

            if (Request.Params["a"] != null)
            {
                string admintype = cFun.DecryptValue(Request.QueryString["a"].ToString());
                if (admintype == BackendStaticValueClass.isFlowEdit)
                {
                    Response.Redirect("QuestionnaireAddItem?SHW=" + Request.Params["SHW"] + "&QAID=" + Request.Params["QAID"] + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["TYPE"] + "&a=" + cFun.EncryptValue(admintype));
                }
                else
                {
                    Response.Redirect("QuestionnaireAddItem?SHW=" + Request.Params["SHW"] + "&QAID=" + Request.Params["QAID"] + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["TYPE"]);
                }
            }
            else
            {
                Response.Redirect("QuestionnaireAddItem?SHW=" + Request.Params["SHW"] + "&QAID=" + Request.Params["QAID"] + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["TYPE"]);
            }
        }
    }

}