﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Admin_ManageSiteReference : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    static string _typemas = "mas";
    static string _typecss = "css";
    static string _typerelogin = "rel";
    static string _sitemaster = "Site_master";
    static string _sitecssbandle = "Site_CSSBandle";
    static string _sitemasterrelogin = "Site_master_relogin";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PanelKeyDetail.Visible = false;
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_site_reference table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            string query = string.Empty;
            if(Request.Params["t"] != null)
            {
                string type = Request.QueryString["t"];
                if (type == _typemas)
                {
                    query = "Select * From tb_site_reference Where list_name='" + _sitemaster + "' Order By id";
                }
                else if(type == _typecss)
                {
                    query = "Select * From tb_site_reference Where list_name='" + _sitecssbandle + "' Order By id";
                }
                else if (type == _typerelogin)
                {
                    query = "Select * From tb_site_reference Where list_name='" + _sitemasterrelogin + "' Order By id";
                }
            }
            else
            {
                query = "Select * From tb_site_reference Order By id";
            }
            DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dt;
            }
            else
            {
                lbls.Text = "There is no record.";
            }
        }
        catch (Exception)
        {
            lbls.Text = "There is no record.";
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string refid = "";
        foreach (GridDataItem item in GKeyMaster.SelectedItems)
        {
            refid = item["id"].Text;
        }
        if (e.CommandName == "InsertRef")
        {
            GridDataItem item = (GridDataItem)e.Item;

            string name = item.OwnerTableView.DataKeyValues[item.ItemIndex]["list_name"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["list_name"].ToString() : "";
            if (!string.IsNullOrEmpty(name))
            {
                btnUpdateRecord.Visible = false;
                btnAdd.Visible = true;
                PanelKeyDetail.Visible = true;

                ResetControls();
                lbls.Text = "";
                txtName.Text = name;
                txtName.Enabled = false;
            }
        }
        else if (e.CommandName == "UpdateRef")
        {
            GridDataItem item = (GridDataItem)e.Item;

            string refID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["id"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["id"].ToString() : "";
            if (!string.IsNullOrEmpty(refID))
            {
                btnUpdateRecord.Visible = true;
                btnAdd.Visible = false;
                PanelKeyDetail.Visible = true;

                loadvalue(refID);
            }
        }
        else if (e.CommandName == "Delete")
        {
            GridDataItem item = (GridDataItem)e.Item;

            string refID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["id"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["id"].ToString() : "";
            if (!string.IsNullOrEmpty(refID))
            {
                int isDeleted = fn.ExecuteSQL(string.Format("Delete From tb_site_reference Where id='{0}'", refID));
                if (isDeleted > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already deleted.');", true);
                }
                else
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Deleting error.');", true);
                }
                ResetControls();
                lbls.Text = "";
                PanelKeyDetail.Visible = false;
                btnAdd.Visible = false;
                btnUpdateRecord.Visible = false;
            }
        }
    }
    #endregion

    #region btnAddNew_Click (Load when click Add New Method button)
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        ResetControls();
        lbls.Text = "";
        btnUpdateRecord.Visible = false;
        btnAdd.Visible = true;
        PanelKeyDetail.Visible = true;
    }
    #endregion

    #region btnAdd_Click (Insert the respective data into tb_site_reference table)
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CommonFuns cFun = new CommonFuns();
            lbls.Text = "";
            string name = cFun.solveSQL(txtName.Text.Trim());
            string value = cFun.solveSQL(txtValue.Text.Trim());

            try
            {
                DataTable dtmaster = fn.GetDatasetByCommand("Select * From tb_site_reference Where list_name='" + name + "' And list_value='" + value + "'", "ds").Tables[0];

                if (dtmaster.Rows.Count > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This site reference already exist.');", true);
                    return;
                }
                else
                {
                    string query = string.Format("Insert Into tb_site_reference (list_name, list_value)"
                        + " Values ('{0}', '{1}')"
                        , name, value);

                    int rowInserted = fn.ExecuteSQL(query);
                    if (rowInserted == 1)
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Saving error, try again.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                lbls.Text = ex.Message;
            }
        }
    }
    #endregion

    #region loadvalue (Load RadGrid selected row data from tb_site_reference table to bind the respective controls while edit)
    private void loadvalue(string moduleid)
    {
        string query = "Select * From tb_site_reference Where id='" + moduleid + "'";

        DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
        if(dt.Rows.Count > 0)
        {
            hfID.Value = moduleid;
            txtName.Text = dt.Rows[0]["list_name"].ToString();
            txtName.Enabled = false;
            txtValue.Text = dt.Rows[0]["list_value"].ToString();
        }
    }
    #endregion

    #region btnUpdateRecord_Click (Update the respective data into "tb_site_reference" table)
    protected void btnUpdateRecord_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CommonFuns cFun = new CommonFuns();
            lbls.Text = "";
            string refid = hfID.Value;
            string name = cFun.solveSQL(txtName.Text.Trim());
            string value = cFun.solveSQL(txtValue.Text.Trim());

            try
            {
                string query = string.Format("Update tb_site_reference Set list_name='{0}', list_value='{1}'"
                    + " Where id='{2}'", name, value, refid);

                int rowUpdated = fn.ExecuteSQL(query);
                if (rowUpdated == 1)
                {
                    GKeyMaster.Rebind();
                    btnUpdateRecord.Visible = false;
                    btnAdd.Visible = false;
                    PanelKeyDetail.Visible = false;
                    GKeyMaster.Rebind();
                    ResetControls();
                    lbls.Text = "";
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                }
                else
                {
                    GKeyMaster.Rebind();
                    btnUpdateRecord.Visible = false;
                    btnAdd.Visible = false;
                    PanelKeyDetail.Visible = false;
                    GKeyMaster.Rebind();
                    ResetControls();
                    lbls.Text = "";
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updating error, try again.');", true);
                }
            }
            catch (Exception ex)
            {
                lbls.Text += ex.Message;
            }
        }
    }
    #endregion

    #region ResetControls
    private void ResetControls()
    {
        hfID.Value = "";
        txtName.Text = "";
        txtName.Enabled = true;
        txtValue.Text = "";
    }
    #endregion

    #region lnkBack_Click (redirect to SiteSettings.aspx)
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            Response.Redirect("Event_Settings_WebSettings.aspx?SHW=" + Request.QueryString["SHW"].ToString());
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
}