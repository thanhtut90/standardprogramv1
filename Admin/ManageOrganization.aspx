﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageOrganization.aspx.cs" Inherits="Admin_ManageOrganization" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="row" style="padding-bottom:20px;">
            <div class="col-lg-7 content-header">
                <h1>Manage Organisation</h1>
            </div>
        </div><!--page header -->
        <div id="contentwrapper" class="contentwrapper">
            <div id="index" class="subcontent">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h4>Add New Organization</h4>

                        <asp:Label ID="lblOrgName" runat="server" Text="Name" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtOrgName" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtOrgName" runat="server"
                            ErrorMessage="Please insert the organization name" ForeColor="Red"></asp:RequiredFieldValidator>
                        <br />

                        <asp:Label ID="lblSortOrder" runat="server" Text="Sort Order" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtSortorder" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbSortorder" runat="server" TargetControlID="txtSortorder" FilterType="Numbers" ValidChars="0123456789" />
                        <br />

                        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" class="btn btn-lg" />
                        <asp:Label ID="lblShowid" runat="server" Visible="false"></asp:Label>
                        <br /><br />

                        <asp:Panel ID="pnllist" runat="server" Visible="false">
                            <h3>Organizations</h3>
                            <asp:GridView ID="gvList" runat="server" DataKeyNames="ID" 
                            OnRowDeleting="GridView1_RowDeleting"
                            OnRowEditing="GridView1_RowEditing"
                            OnRowUpdating="GridView1_RowUpdating"
                            OnRowCancelingEdit="GridView1_RowCancelingEdit" AutoGenerateColumns="false"
                            CssClass="table">
                                <HeaderStyle CssClass="theadstyle" />
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="ID" />
                                    <asp:TemplateField HeaderText="Organization Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("Organisation") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Organisation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Order">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtOrder" runat="server" Text='<%# Eval("Sorting") %>' Width="50px"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="ftbOrder" runat="server" TargetControlID="txtOrder" FilterType="Numbers" ValidChars="0123456789" />
                                            <asp:Label ID="lblno" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrder" runat="server" Text='<%# Eval("Sorting") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IsDeleted">
                                        <ItemTemplate>
                                            <%# Eval("isDeleted").ToString() == "0" || string.IsNullOrEmpty(Eval("isDeleted").ToString()) ? "Active" : "Deleted" %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowEditButton="True" CausesValidation="false" />
                                    
                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument='<%# Eval("ID")%>'
                                                OnClientClick="return confirm ('Are you sure you want to delete this record?')" Text="Delete" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Un-Delelete" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkUnDelete" runat="server" CausesValidation="False"
                                                OnClick="lnkUnDelete_Click" Text="Un-Delelete" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div style="color:Red"><asp:Label ID="lblMsg" runat="server"></asp:Label></div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div><!--page index -->
        </div><!--contentwrapper -->
    </div><!--centercontent -->
</asp:Content>