﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Admin_ManageAdminRegistration : System.Web.UI.Page
{
    Functionality fn = new Functionality();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        //    Master.setsettingnotopen();
            PanelKeyDetail.Visible = false;
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_site_Email table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            string query = "Select * From tb_Admin as a Inner Join tb_admin_role as r On a.admin_roleid= r.roleid Order By a.admin_id";
            DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dt;
            }
            else
            {
                lbls.Text = "There is no record.";
            }
        }
        catch (Exception)
        {
            lbls.Text = "There is no record.";
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string userid = "";
        foreach (GridDataItem item in GKeyMaster.SelectedItems)
        {
            userid = item["admin_id"].Text;
        }

        if (e.CommandName == "Delete")
        {
            GridDataItem item = (GridDataItem)e.Item;

            string userID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"].ToString() : "";
            if (!string.IsNullOrEmpty(userID))
            {
                int isDeleted = fn.ExecuteSQL(string.Format("Update tb_Admin Set admin_isdeleted=1 Where admin_id='{0}'", userID));
                if (isDeleted > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already deleted.');", true);
                }
                else
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Deleting error.');", true);
                }

                ResetControls();
                lbls.Text = "";
                PanelKeyDetail.Visible = false;
                btnAdd.Visible = false;
                btnUpdateRecord.Visible = false;
            }
        }
        else if(e.CommandName == "UnDelete")
        {
            GridDataItem item = (GridDataItem)e.Item;

            string userID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"].ToString() : "";
            if (!string.IsNullOrEmpty(userID))
            {
                int isDeleted = fn.ExecuteSQL(string.Format("Update tb_Admin Set admin_isdeleted=0 Where admin_id='{0}'", userID));
                if (isDeleted > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updated to un-delete.');", true);
                }
                else
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Error.');", true);
                }

                ResetControls();
                lbls.Text = "";
                PanelKeyDetail.Visible = false;
                btnAdd.Visible = false;
                btnUpdateRecord.Visible = false;
            }
        }
        else
        {
            if (userid != "" && userid != null)
            {
                btnUpdateRecord.Visible = true;
                btnAdd.Visible = false;
                PanelKeyDetail.Visible = true;

                LoadList();
                loadvalue(userid);
            }
        }
    }
    #endregion

    #region btnAddNew_Click (Load when click Add New Method button)
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        LoadList();
        ResetControls();
        lbls.Text = "";
        btnUpdateRecord.Visible = false;
        btnAdd.Visible = true;
        PanelKeyDetail.Visible = true;
    }
    #endregion

    #region btnAdd_Click (Insert the respective data into ref_PaymentMethod table)
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CommonFuns cFun = new CommonFuns();
            lbls.Text = "";
            string username = cFun.solveSQL(txtUserName.Text.Trim());
            string password = cFun.solveSQL(txtPassword.Text.Trim());
            string roleid = cFun.solveSQL(ddlRole.SelectedValue.ToString());

            try
            {
                DataTable dtadmin = fn.GetDatasetByCommand("Select * From tb_Admin Where admin_username='" + username + "' And admin_password='"
                    + password + "' And admin_roleid=" + roleid, "ds").Tables[0];

                if (dtadmin.Rows.Count > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This user already exist.');", true);
                    return;
                }
                else
                {
                    string query = string.Format("Insert Into tb_Admin (admin_username, admin_password, admin_roleid)"
                        + " Values ('{0}', '{1}', {2})"
                        , username, password, roleid);

                    int rowInserted = fn.ExecuteSQL(query);
                    if (rowInserted == 1)
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Saving error, try again.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                lbls.Text = ex.Message;
            }
        }
    }
    #endregion

    #region LoadList (bind admin role data to ddlRole dropdownlist control)
    protected void LoadList()
    {
        DataTable dt = new DataTable();
        dt = fn.GetDatasetByCommand("Select * From tb_admin_role", "ds").Tables[0];
        if (dt.Rows.Count != 0)
        {
            for (int x = 0; x < dt.Rows.Count; x++)
            {
                ddlRole.Items.Add(dt.Rows[x]["role_name"].ToString());
                ddlRole.Items[x + 1].Value = dt.Rows[x]["roleid"].ToString();
            }
        }
    }
    #endregion

    #region loadvalue (Load RadGrid selected row data from tb_site_Email table to bind the respective controls while edit)
    private void loadvalue(string userid)
    {
        string query = "Select * From tb_Admin Where admin_id='" + userid + "'";

        DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
        if(dt.Rows.Count > 0)
        {
            hfID.Value = userid;
            txtUserName.Text = dt.Rows[0]["admin_username"].ToString();
            txtPassword.Text = dt.Rows[0]["admin_password"].ToString();
            lblOldPassword.Text = "Old Password : " + dt.Rows[0]["admin_password"].ToString();
            string roleid = dt.Rows[0]["admin_roleid"].ToString();
            try
            {
                if (!String.IsNullOrEmpty(roleid))
                {
                    RadComboBoxItem listItem = ddlRole.FindItemByValue(roleid);
                    if (listItem != null)
                    {
                        ddlRole.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
    #endregion

    #region btnUpdateRecord_Click (Update the respective data into "ref_PaymentMethod" table)
    protected void btnUpdateRecord_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CommonFuns cFun = new CommonFuns();
            lbls.Text = "";
            string userid = hfID.Value;
            string username = cFun.solveSQL(txtUserName.Text.Trim());
            string password = cFun.solveSQL(txtPassword.Text.Trim());
            string roleid = cFun.solveSQL(ddlRole.SelectedValue.ToString());

            try
            {
                DataTable dtadmin = fn.GetDatasetByCommand("Select * From tb_Admin Where admin_username='" + username + "' And admin_password='"
                   + password + "' And admin_roleid=" + roleid + " And admin_id!=" + userid, "ds").Tables[0];
                if (dtadmin.Rows.Count > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This user already exist.');", true);
                    return;
                }
                else
                {
                    string query = string.Format("Update tb_Admin Set admin_username='{0}', admin_password='{1}', admin_roleid={2}"
                    + " Where admin_id={3}", username, password, roleid, userid);

                    int rowUpdated = fn.ExecuteSQL(query);
                    if (rowUpdated == 1)
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updating error, try again.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                lbls.Text += ex.Message;
            }
        }
    }
    #endregion

    #region ResetControls
    private void ResetControls()
    {
        hfID.Value = "";
        txtUserName.Text = "";
        txtPassword.Text = "";
        lblOldPassword.Text = "";
        txtConfirmPassword.Text = "";
        ddlRole.SelectedIndex = 0;
    }
    #endregion
}