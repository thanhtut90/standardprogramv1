﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using Corpit.Utilities;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using System.Configuration;
using Corpit.Registration;

public partial class Admin_Event_Settings_Master : System.Web.UI.Page
{
    #region Declaration
    CultureInfo provider = CultureInfo.GetCultureInfo("en-GB"); //.InvariantCulture;
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    static string _pagebanner = "page_banner";
    static string _pagetitle = "pagetitle";
    static string _indexcontent = "indexcontent";
    static string _Sitemaster = "Site_master";
    static string _SiteCSSBandle = "Site_CSSBandle";
    static string _copyrigtht = "copyrigtht";
    static string _SMTPUsername = "SMTPUsernname";
    static string _SMTPPassword = "SMTPPassword";
    static string _SMTPClient = "SMTPClient";
    static string _UseDefaultCredentials = "UseDefaultCredentials";
    static string _EnableSsl = "EnableSsl";
    static string _FromEmailAddress = "FromEmailAddress";
    static string _FromEmailDesc = "FromEmailDesc";
    static string _Sitereloginmaster = "Site_master_relogin";

    static string _eventname = "eventname";
    static string _prefix = "prefix";
    static string _currency = "Currency";
    static string _regclosing = "RegClosedDate";
    static string _regclosingmessage = "RegClosedMessage";
    static string _adminfee = "adminfee";
    static string _gstfee = "gstfee";
    static string _earlybirdclose = "EarlyBirdEndDate";
    static string _hastimer = "hastimer";
    static string _hastimertype = "hastimer_type";
    static string _hasQ = "hasQ";
    static string _hasterm = "hasterm";
    static string _allowpartialpayment = "AllowPartialPayment";
    static string _masterconfselectiontype = "MasterConfSelectionType";
    static string _maxpromousableitempertran = "maxpromousableitempertran";
    static string _hasVisa = "hasVisa";
    static string _chequetext = "chequetext";
    static string _cctext = "creditcardtext";

    static string _questionprefix = "QuestionnairePrefix";
    #endregion

    private string ConnectionString = ConfigurationManager.ConnectionStrings["CMSConnString"].ConnectionString;
    private int mint_CommandTimeout = 30;
    private SqlConnection mobj_SqlConnection;
    private SqlCommand mobj_SqlCommand;
    private string mstr_ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = cFun.DecryptValue( Request.QueryString["SHW"].ToString());
                bool isCreated = checkCreated(showid);
                if (!isCreated)
                {
                    InsertNewSiteSettings(showid);
                }

                bool isCreatedPayMethod = checkCreatedPaymentMethod(showid);
                if (!isCreatedPayMethod)
                {
                    InsertNewPaymentMethod(showid);
                }

                setBanner(showid);

                bindShowName(showid);
                bindcurrency();
                bindPaymentMethod(showid);
                binddata(showid);
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region bindShowName
    private void bindShowName(string showid)
    {
        string query = "Select SHW_Name From tb_Show Where SHW_ID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        string showName = fn.GetDataByCommand(query, "SHW_Name", pList);
        
        txtEventName.Text = showName == "0" ? string.Empty : showName;
    }
    #endregion

    #region checkCreated (check the record count of tmp_tbSiteSettings table is equal to the record counts of tb_site_settings table)
    private bool checkCreated(string showid)
    {
        bool isCreated = false;
        DataTable dt = fn.GetDatasetByCommand("Select * From tmp_tbSiteSettings", "ds").Tables[0];

        string query = "Select * From tb_site_settings Where ShowID=@SHWID And settings_name In (Select settings_name From tmp_tbSiteSettings)";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dtSettings = fn.GetDatasetByCommand(query, "dsSettings", pList).Tables[0];
        if (dt.Rows.Count == dtSettings.Rows.Count)
        {
            isCreated = true;
        }

        return isCreated;
    }
    #endregion

    #region checkCreatedPaymentMethod (check the record count of tmp_refPaymentMethod table is equal to the record counts of ref_PaymentMethod table)
    private bool checkCreatedPaymentMethod(string showid)
    {
        bool isCreated = false;
        DataTable dt = fn.GetDatasetByCommand("Select * From tmp_refPaymentMethod", "ds").Tables[0];

        string query = "Select * From ref_PaymentMethod Where ShowID=@SHWID And method_usedid In (Select method_usedid From tmp_refPaymentMethod)";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dtPayMethod = fn.GetDatasetByCommand(query, "dsPayMethod", pList).Tables[0];
        if (dt.Rows.Count == dtPayMethod.Rows.Count)
        {
            isCreated = true;
        }

        return isCreated;
    }
    #endregion

    #region bindcurrency(bind currency data from ref_Currency table to ddlCurrency dropdownlist)
    protected void bindcurrency()
    {
        DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand("Select * From ref_Currency", "ds");
        if (ds.Tables[0].Rows.Count != 0)
        {
            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
            {
                ddlCurrency.Items.Add(ds.Tables[0].Rows[x]["currency_name"].ToString());
                ddlCurrency.Items[x].Value = ds.Tables[0].Rows[x]["currency_display"].ToString();
            }
        }
    }
    #endregion

    /// <summary>
    /// if the payment method table change something, need to edit the PaymentType class in Corpit.Registration project
    /// ***Note "method_usedid" shouldn't null or empty because this id is really used by Comfirmation page
    /// </summary>
    ///
    #region bindPaymentMethod (bind Payment Methods from ref_PaymentMethod table to Payment Method Checkboxlist(chkPaymentMethod))
    private void bindPaymentMethod(string showid)
    {
        string query = "Select * From ref_PaymentMethod Where ShowID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dt = new DataTable();
        dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                //ListItem item = new ListItem();
                //item.Text = dr["method_name"].ToString();
                //item.Value = dr["method_usedid"].ToString();
                //item.Selected = Convert.ToBoolean(dr["method_active"]);
                //this.chkPaymentMethod.Items.Add(item);

                string usedid = dr["method_usedid"].ToString();
                string strIsActive = dr["method_active"] != null ? dr["method_active"].ToString() : YesOrNo.No;
                string templateID = dr["method_tmpid"] != null ? dr["method_tmpid"].ToString() : "";
                bool isActive = false;
                bool.TryParse(strIsActive, out isActive);

                if(usedid == PaymentType.CreditCard.ToString())
                {
                    chkCreditCard.Checked = isActive;
                    hfCreditCard.Value = templateID;
                }
                else if (usedid == PaymentType.TT.ToString())
                {
                    chkTT.Checked = isActive;
                    hfTT.Value = templateID;
                }
                else if (usedid == PaymentType.Waved.ToString())
                {
                    chkWaived.Checked = isActive;
                    hfWaived.Value = templateID;
                }
                else if (usedid == PaymentType.Cheque.ToString())
                {
                    chkCheque.Checked = isActive;
                    hfCheque.Value = templateID;
                }
            }
        }
    }
    #endregion

    #region setBanner (set imgbanner image url with the settings_value of settings_name=_pagebanner from tb_site_settings table)
    protected void setBanner(string showid)
    {
        string strimage = string.Empty;

        string query = "Select settings_value From tb_site_settings Where settings_name='" + _pagebanner + "' And ShowID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        strimage = fn.GetDataByCommand(query, "settings_value", pList);
        if (!string.IsNullOrEmpty(strimage))
        {
            strimage = strimage.Replace("../", "");
            string already_File = Server.MapPath("~/" + strimage);
            if (File.Exists(already_File))
            {
                imgbanner.Visible = true;
                imgbanner.ImageUrl = "../" + strimage;
            }
            else
            {
                imgbanner.Visible = false;
                imgbanner.ImageUrl = "";
            }
        }
        else
        {
            imgbanner.Visible = false;
            imgbanner.ImageUrl = "";
        }
    }
    #endregion

    #region binddata (bind data from tb_site_settings to related controls)
    protected void binddata(string showid)
    {
        #region Site Settings (for the whole event)
        string pagetitle = string.Empty;
        string indexcontent = string.Empty;
        string copyright = string.Empty;
        string img = string.Empty;
        string smtpuser = string.Empty;
        string smtppswd = string.Empty;
        string smtpclient = string.Empty;
        string fea = string.Empty;
        string fed = string.Empty;

        string query = string.Empty;

        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _pagetitle + "' And ShowID=@SHWID";
        pagetitle = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _indexcontent + "' And ShowID=@SHWID";
        indexcontent = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _copyrigtht + "' And ShowID=@SHWID";
        copyright = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _SMTPUsername + "' And ShowID=@SHWID";
        smtpuser = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _SMTPPassword + "' And ShowID=@SHWID";
        smtppswd = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _SMTPClient + "' And ShowID=@SHWID";
        smtpclient = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _FromEmailAddress + "' And ShowID=@SHWID";
        fea = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _FromEmailDesc + "' And ShowID=@SHWID";
        fed = fn.GetDataByCommand(query, "settings_value", pList);

        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select list_value From tb_site_reference Where list_name ='Site_master'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddlsitemaster.Items.Clear();
                ddlsitemaster.DataSource = cmd.ExecuteReader();
                ddlsitemaster.DataTextField = "list_value";
                ddlsitemaster.DataValueField = "list_value";
                ddlsitemaster.DataBind();
                con.Close();
            }
            using (SqlCommand cmd = new SqlCommand("Select list_value From tb_site_reference Where list_name ='Site_CSSBandle'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddlcssbandle.Items.Clear();
                ddlcssbandle.DataSource = cmd.ExecuteReader();
                ddlcssbandle.DataTextField = "list_value";
                ddlcssbandle.DataValueField = "list_value";
                ddlcssbandle.DataBind();
                con.Close();
            }
            using (SqlCommand cmd = new SqlCommand("Select list_value From tb_site_reference Where list_name ='true/false'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddludc.Items.Clear();
                ddludc.DataSource = cmd.ExecuteReader();
                ddludc.DataTextField = "list_value";
                ddludc.DataValueField = "list_value";
                ddludc.DataBind();
                con.Close();

                con.Open();
                ddlenablessl.Items.Clear();
                ddlenablessl.DataSource = cmd.ExecuteReader();
                ddlenablessl.DataTextField = "list_value";
                ddlenablessl.DataValueField = "list_value";
                ddlenablessl.DataBind();
                con.Close();
            }
            using (SqlCommand cmd = new SqlCommand("Select list_value From tb_site_reference Where list_name ='Site_master_relogin'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddlsitereloginmaster.Items.Clear();
                ddlsitereloginmaster.DataSource = cmd.ExecuteReader();
                ddlsitereloginmaster.DataTextField = "list_value";
                ddlsitereloginmaster.DataValueField = "list_value";
                ddlsitereloginmaster.DataBind();
                con.Close();
            }
        }

        query = "Select settings_value From tb_site_settings Where settings_name='" + _Sitemaster + "' And ShowID=@SHWID";
        ddlsitemaster.SelectedValue = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _SiteCSSBandle + "' And ShowID=@SHWID";
        ddlcssbandle.SelectedValue = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _UseDefaultCredentials + "' And ShowID=@SHWID";
        ddludc.SelectedValue = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _EnableSsl + "' And ShowID=@SHWID";
        ddlenablessl.SelectedValue = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _Sitereloginmaster + "' And ShowID=@SHWID";
        ddlsitereloginmaster.SelectedValue = fn.GetDataByCommand(query, "settings_value", pList);

        txt_title.Text = pagetitle;
        txtsmtpuser.Text = smtpuser;
        txtsmtppswd.Text = smtppswd;
        txtsmtpclient.Text = smtpclient;
        txtfea.Text = fea;
        txtfed.Text = fed;
        txtCopyright.Text = Server.HtmlDecode(copyright);
        #endregion

        #region Event
        string eventname = string.Empty;
        string prefix = string.Empty;
        string currency = string.Empty;
        string regclosing = string.Empty;
        string regclosingmsg = string.Empty;
        string adminfee = string.Empty;
        string gstfee = string.Empty;
        string earlybirdclosing = string.Empty;
        string hasTimer = string.Empty;
        string timerType = string.Empty;
        int hasQ = 0;
        int hasterm = 0;
        int allowPartialPayment = 0;
        string masConfSelectionType = string.Empty;
        string maxitempertrans = string.Empty;
        int hasVisa = 0;
        string chequetext = string.Empty;
        string cctext = string.Empty;

        query = "Select * From tb_site_settings Where ShowID=@SHWID";

        DataTable dt = new DataTable();
        dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

        for (int x = 0; x < dt.Rows.Count; x++)
        {
            string settingname = dt.Rows[x]["settings_name"].ToString();

            //if (settingname == _eventname)
            //{
            //    eventname = dt.Rows[x]["settings_value"].ToString();
            //    txtEventName.Text = eventname;
            //}
            if (settingname == _prefix)
            {
                prefix = dt.Rows[x]["settings_value"].ToString();
                txtPrefix.Text = prefix;
            }
            if (settingname == _currency)
            {
                currency = dt.Rows[x]["settings_value"].ToString();
                try
                {
                    if (!String.IsNullOrEmpty(currency))
                    {
                        ListItem listItem = ddlCurrency.Items.FindByValue(currency);
                        if (listItem != null)
                        {
                            ddlCurrency.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
            if (settingname == _regclosing)
            {
                regclosing = !string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                txtRegClosingDate.Text = regclosing;
            }
            if (settingname == _regclosingmessage)
            {
                regclosingmsg = dt.Rows[x]["settings_value"].ToString();
                txtRegcloseMsg.Text = Server.HtmlDecode(regclosingmsg);
            }
            if (settingname == _adminfee)
            {
                adminfee = !string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? (Convert.ToDouble(dt.Rows[x]["settings_value"].ToString()) * 100).ToString() : adminfee;
                txtAdminFee.Text = adminfee;
            }
            if (settingname == _gstfee)
            {
                gstfee = !string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? (Convert.ToDouble(dt.Rows[x]["settings_value"].ToString()) * 100).ToString() : gstfee;
                txtGSTFee.Text = gstfee;
            }
            if (settingname == _earlybirdclose)
            {
                earlybirdclosing = !string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                txtEarlyClosingDate.Text = earlybirdclosing;
            }
            if (settingname == _hastimer)
            {
                hasTimer = dt.Rows[x]["settings_value"].ToString();
                if (!String.IsNullOrEmpty(hasTimer))
                {
                    ListItem listItem = rbTimer.Items.FindByValue(hasTimer);
                    if (listItem != null)
                    {
                        rbTimer.ClearSelection();
                        listItem.Selected = true;

                        rbTimer_SelectedIndexChanged(this, null);
                    }
                }
            }
            if (settingname == _hastimertype)
            {
                timerType = dt.Rows[x]["settings_value"].ToString();
                if (!String.IsNullOrEmpty(timerType) && rbTimer.SelectedValue == "1")
                {
                    ListItem listItem = ddlTimerType.Items.FindByValue(timerType);
                    if (listItem != null)
                    {
                        ddlTimerType.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            if (settingname == _hasQ)
            {
                hasQ = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : "0");
                if (hasQ == 1)
                {
                    rbQues1.Checked = false;
                    rbQues2.Checked = true;
                }
                else
                {
                    rbQues1.Checked = true;
                    rbQues2.Checked = false;
                }
            }
            if (settingname == _hasterm)
            {
                hasterm = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : "0");
                if (hasterm == 1)
                {
                    rbTerms1.Checked = false;
                    rbTerms2.Checked = true;
                }
                else
                {
                    rbTerms1.Checked = true;
                    rbTerms2.Checked = false;
                }
            }
            if (settingname == _allowpartialpayment)
            {
                allowPartialPayment = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : "0");
                if (allowPartialPayment == 1)
                {
                    rbPartial1.Checked = true;
                    rbPartial2.Checked = false;
                }
                else
                {
                    rbPartial1.Checked = true;
                    rbPartial2.Checked = false;
                }
            }
            if (settingname == _masterconfselectiontype)
            {
                masConfSelectionType = dt.Rows[x]["settings_value"].ToString();
                if (!String.IsNullOrEmpty(masConfSelectionType))
                {
                    ListItem listItem = ddlMasterConferenceType.Items.FindByValue(masConfSelectionType);
                    if (listItem != null)
                    {
                        ddlMasterConferenceType.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            if (settingname == _maxpromousableitempertran)
            {
                maxitempertrans = dt.Rows[x]["settings_value"].ToString();
                txtMaxItemPerTrans.Text = maxitempertrans;
            }
            if (settingname == _hasVisa)
            {
                hasVisa = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : "0");
                if (hasVisa == 1)
                {
                    rbVisa1.Checked = false;
                    rbVisa2.Checked = true;
                }
                else
                {
                    rbVisa1.Checked = true;
                    rbVisa2.Checked = false;
                }
            }
            if (settingname == _chequetext)
            {
                chequetext = dt.Rows[x]["settings_value"].ToString();
                txtChequeText.Text = Server.HtmlDecode(chequetext);
            }
            if (settingname == _cctext)
            {
                cctext = dt.Rows[x]["settings_value"].ToString();
                txtCreditText.Text = Server.HtmlDecode(cctext);
            }
        }
        #endregion
    }
    #endregion

    #region rbTimer_SelectedIndexChanged (set TimerType portion (trTimerType) visibility true or false according to the rbTimer selection)
    protected void rbTimer_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbTimer.SelectedItem.Value == "1")
        {
            trTimerType.Visible = true;
            vcTimerType.Enabled = true;
        }
        else
        {
            trTimerType.Visible = false;
            vcTimerType.Enabled = false;
        }
    }
    #endregion

    #region upload (save and get fupload controls selected file)
    private void upload()
    {
        if (fupload.HasFile)
        {
            string filePath = "../images/Site/";
            string chkpath = Server.MapPath(filePath);
            var directory = new DirectoryInfo(chkpath);
            if (directory.Exists == false)
            {
                directory.Create();
            }

            string filepath = Server.MapPath(filePath);
            String fileExtension = System.IO.Path.GetExtension(fupload.FileName).ToLower();
            String[] allowedExtensions = { ".gif", ".png", ".jpeg", ".jpg" };
            for (int i = 0; i < allowedExtensions.Length; i++)
            {
                if (fileExtension == allowedExtensions[i])
                {
                    try
                    {
                        string showid = "";
                        if (Request.Params["SHW"] != null)
                        {
                            showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                        }
                        else
                            showid = "Default0009";

                        string s_newfilename = showid + DateTime.Now.Year.ToString() +
                        DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() +
                        DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + fileExtension;
                        fupload.PostedFile.SaveAs(filepath + s_newfilename);

                      

                        Session["imgBanner"] = "images/site/" + s_newfilename;
                    }
                    catch (Exception ex)
                    {
                        Response.Write("File could not be uploaded.");
                    }

                }


            }
        }
        }
    #endregion

    #region SaveForm (Save data into related tb_site_settings table and ref_PaymentMethod table)
    protected void SaveForm(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            try
            {
                #region Site Settings(for the whole event)
                string sql = string.Empty;
                string imgBanner = string.Empty;
                string title = string.Empty;
                string indexcontent = string.Empty;
                string copyright = string.Empty;
                string sitemaster = string.Empty;
                string sitecssbandle = string.Empty;
                string smtpuser = string.Empty;
                string smtppswd = string.Empty;
                string smtpclient = string.Empty;
                string fea = string.Empty;
                string fed = string.Empty;
                string udc = string.Empty;
                string enablessl = string.Empty;
                string sitereloginmaster = string.Empty;

                title = txt_title.Text;
                smtpuser = txtsmtpuser.Text;
                smtppswd = txtsmtppswd.Text;
                smtpclient = txtsmtpclient.Text;
                fea = txtfea.Text;
                fed = txtfed.Text;
                copyright = Server.HtmlEncode(txtCopyright.Text.ToString());
                sitemaster = Server.HtmlEncode(ddlsitemaster.SelectedValue.ToString());
                sitecssbandle = Server.HtmlEncode(ddlcssbandle.SelectedValue.ToString());
                udc = Server.HtmlEncode(ddludc.SelectedValue.ToString());
                enablessl = Server.HtmlEncode(ddlenablessl.SelectedValue.ToString());
                sitereloginmaster = Server.HtmlEncode(ddlsitereloginmaster.SelectedValue.ToString());

                upload();

                if (Session["imgBanner"] != null)
                {
                    imgBanner = Session["imgBanner"].ToString();
                    sql = "Update tb_site_settings Set settings_value='" + imgBanner + "' Where settings_name='" + _pagebanner + "' And ShowID='" + showid + "';";
                }

                sql += "Update tb_site_settings Set settings_value='" + title + "' Where settings_name='" + _pagetitle + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + indexcontent + "' Where settings_name='" + _indexcontent + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + copyright + "' Where settings_name='" + _copyrigtht + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + sitemaster + "' Where settings_name='" + _Sitemaster + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + sitecssbandle + "' Where settings_name='" + _SiteCSSBandle + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + smtpuser + "' Where settings_name='" + _SMTPUsername + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + smtppswd + "' Where settings_name='" + _SMTPPassword + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + smtpclient + "' Where settings_name='" + _SMTPClient + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + udc + "' Where settings_name='" + _UseDefaultCredentials + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + enablessl + "' Where settings_name='" + _EnableSsl + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + fea + "' Where settings_name='" + _FromEmailAddress + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + fed + "' Where settings_name='" + _FromEmailDesc + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + sitereloginmaster + "' Where settings_name='" + _Sitereloginmaster + "' And ShowID='" + showid + "';";
                fn.ExecuteSQL(sql);
                #endregion

                #region Event
                string eventname = string.Empty;
                string prefix = string.Empty;
                string currency = string.Empty;
                string regclosing = string.Empty;
                string regclosingmsg = string.Empty;
                Double adminfee;
                Double gstfee;
                string earlybirdclosing = string.Empty;
                string hasTimer = string.Empty;
                string timerType = string.Empty;
                int hasQ = 0;
                int hasterm = 0;
                int allowPartialPayment = 0;
                string masConfSelectionType = string.Empty;
                string maxitempertrans = string.Empty;
                int hasVisa = 0;
                string chequetext = string.Empty;
                string cctext = string.Empty;
                string questionprefix = string.Empty;

                eventname = cFun.solveSQL(txtEventName.Text.ToString());
                prefix = cFun.solveSQL(txtPrefix.Text.ToString());
                currency = ddlCurrency.SelectedItem.Value.ToString();
                regclosing = txtRegClosingDate.Text.Trim();//Convert.ToDateTime(txtRegClosingDate.Text.Trim(), provider);
                regclosingmsg = Server.HtmlEncode(txtRegcloseMsg.Text);
                adminfee = !string.IsNullOrEmpty(txtAdminFee.Text) && !string.IsNullOrWhiteSpace(txtAdminFee.Text) ? Convert.ToDouble(txtAdminFee.Text) / 100 : 0;
                gstfee = !string.IsNullOrEmpty(txtGSTFee.Text) && !string.IsNullOrWhiteSpace(txtGSTFee.Text) ? Convert.ToDouble(txtGSTFee.Text) / 100 : 0;
                earlybirdclosing = txtEarlyClosingDate.Text.Trim();
                hasTimer = rbTimer.SelectedItem.Value;
                timerType = ddlTimerType.SelectedItem.Value;

                if (rbQues1.Checked == true)
                {
                    hasQ = 0;
                }
                else if (rbQues1.Checked == true)
                {
                    hasQ = 1;
                }

                if (rbTerms1.Checked == true)
                {
                    hasterm = 0;
                }
                else if (rbTerms2.Checked == true)
                {
                    hasterm = 1;
                }

                if (rbPartial1.Checked == true)
                {
                    allowPartialPayment = 0;
                }
                else if (rbPartial2.Checked == true)
                {
                    allowPartialPayment = 1;
                }

                masConfSelectionType = ddlMasterConferenceType.SelectedItem.Value;
                maxitempertrans = txtMaxItemPerTrans.Text.Trim();

                if (rbVisa1.Checked == true)
                {
                    hasVisa = 0;
                }
                else if (rbVisa2.Checked == true)
                {
                    hasVisa = 1;
                }

                chequetext = Server.HtmlEncode(txtChequeText.Text);
                cctext = Server.HtmlEncode(txtCreditText.Text);

                questionprefix = txtQuestionPrefix.Text.Trim();

                #region Inserting data into tb_Show
                RunNumber run = new RunNumber(fn);
                string runNumber = run.GetRunNUmber("SHW_KEY");
                string randomPrefix = fn.CreateRandomCodeCharacter(3);
                string showID = randomPrefix + runNumber;
                string showimage = string.Empty;
                string editdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);

                string query = string.Format("Update tb_Show Set SHW_Name='{0}', SHW_EditDate='{1}'"
                        + " Where SHW_ID='{2}'", eventname, editdate, showid);

                int rowInserted = fn.ExecuteSQL(query);
                #endregion


                sql = "Update tb_site_settings Set settings_value='" + eventname + "' Where settings_name='" + _eventname + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + prefix + "' Where settings_name='" + _prefix + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + currency + "' Where settings_name='" + _currency + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + regclosing + "' Where settings_name='" + _regclosing + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + regclosingmsg + "' Where settings_name='" + _regclosingmessage + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + adminfee + "' Where settings_name='" + _adminfee + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + gstfee + "' Where settings_name='" + _gstfee + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + earlybirdclosing + "' Where settings_name='" + _earlybirdclose + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + hasTimer + "' Where settings_name='" + _hastimer + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + timerType + "' Where settings_name='" + _hastimertype + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + hasQ + "' Where settings_name='" + _hasQ + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + hasterm + "' Where settings_name='" + _hasterm + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + allowPartialPayment + "' Where settings_name='" + _allowpartialpayment + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + masConfSelectionType + "' Where settings_name='" + _masterconfselectiontype + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + maxitempertrans + "' Where settings_name='" + _maxpromousableitempertran + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + hasVisa + "' Where settings_name='" + _hasVisa + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + chequetext + "' Where settings_name='" + _chequetext + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + cctext + "' Where settings_name='" + _cctext + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + questionprefix + "' Where settings_name='" + _questionprefix + "' And ShowID='" + showid + "';";
                fn.ExecuteSQL(sql);

                //if (chkPaymentMethod.Items.Count > 0)
                //{
                //    foreach (ListItem item in chkPaymentMethod.Items)
                //    {
                //        int isActive = item.Selected == true ? 1 : 0;
                //        string updatePaymentMethod = "Update ref_PaymentMethod Set method_active=" + isActive + " Where method_usedid=" + item.Value + " And ShowID='" + showid + "'";
                //        fn.ExecuteSQL(updatePaymentMethod);
                //    }
                //}

                for(int i=1; i<=4; i++)
                {
                    int usedid = 0;
                    int isActive = 0;
                    string templateid = string.Empty;
                    string templatename = string.Empty;
                    string templatevalue = string.Empty;
                    if (i == 1)
                    {
                        usedid = (int)PaymentType.CreditCard;
                        isActive = chkCreditCard.Checked == true ? 1 : 0;
                        templateid = hfCreditCard.Value;
                        templatename = RegClass.strCreditCard;
                        templatevalue = Server.HtmlEncode(txtCreditCard.Text);
                    }
                    if (i == 2)
                    {
                        usedid = (int)PaymentType.TT;
                        isActive = chkTT.Checked == true ? 1 : 0;
                        templateid = hfCreditCard.Value;
                        templatename = RegClass.strTT;
                        templatevalue = Server.HtmlEncode(txtTT.Text);
                    }
                    if (i == 3)
                    {
                        usedid = (int)PaymentType.Waved;
                        isActive = chkWaived.Checked == true ? 1 : 0;
                        templateid = hfCreditCard.Value;
                        templatename = RegClass.strWaived;
                        templatevalue = Server.HtmlEncode(txtWaived.Text);
                    }
                    if (i == 4)
                    {
                        usedid = (int)PaymentType.Cheque;
                        isActive = chkCheque.Checked == true ? 1 : 0;
                        templateid = hfCreditCard.Value;
                        templatename = RegClass.strCheque;
                        templatevalue = Server.HtmlEncode(txtCheque.Text);
                    }

                    if (!string.IsNullOrEmpty(templatevalue))
                    {
                        if (string.IsNullOrEmpty(templateid))
                        {
                            RunNumber rn = new RunNumber(fn);
                            templateid = rn.GetRunNUmber("SITMP_KEY");
                            string insertTempate = string.Format("Insert Into tb_site_Template (temp_id,temp_name,temp_value) Values ('{0}', '{1}', '{2}')", templateid, templatename, templatevalue);
                            fn.ExecuteSQL(insertTempate);
                        }
                        else
                        {
                            string updateTempate = string.Format("Update tb_site_Template Set temp_name='{1}', temp_value='{2}' Where temp_id='{0}'", templateid, templatename, templatevalue);
                            fn.ExecuteSQL(updateTempate);
                        }
                    }

                    string updatePaymentMethod = "Update ref_PaymentMethod Set method_active=" + isActive + ", method_tmpid='" + templateid + "' Where method_usedid=" + usedid + " And ShowID='" + showid + "'";
                    fn.ExecuteSQL(updatePaymentMethod);
                }
                #endregion

                binddata(showid);

                pnlmessagebar.Visible = true;
                lblemsg.Text = "Updated Successfully";
                showid = cFun.EncryptValue(showid);
                Response.Redirect("Event_Flow_Mangaement?SHW=" + showid);
            }
            catch (Exception ex)
            {
                pnlmessagebar.Visible = true;
                lblemsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region InsertNewSiteSettings and using tb_site_settings table & Insert new blank records but settings_name, ShowID into tb_site_settings table
    protected void InsertNewSiteSettings(string showid)
    {
        try
        {
            string settingValue = string.Empty;

            DataTable dt = fn.GetDatasetByCommand("Select * From tmp_tbSiteSettings", "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string settingName = dr["settings_name"].ToString();

                    string query = "Select * From tb_site_settings Where ShowID=@SHWID And settings_name='" + settingName + "'";
                    List<SqlParameter> pList = new List<SqlParameter>();
                    SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    spar.Value = showid;
                    pList.Add(spar);

                    DataTable dtSettings = fn.GetDatasetByCommand(query, "dsSettings", pList).Tables[0];
                    if (dtSettings.Rows.Count == 0)
                    {
                        string sql = string.Format("Insert Into tb_site_settings (settings_name,settings_value,ShowID)"
                                    + " Values('{0}', '{1}', '{2}')", settingName, settingValue, showid);

                        fn.ExecuteSQL(sql);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            pnlmessagebar.Visible = true;
            lblemsg.Text = "Error Occur :" + ex.Message;
        }
    }
    #endregion

    #region InsertNewPaymentMethod and using ref_PaymentMethod table & Insert new blank records(method_name,method_usedid,method_sortorder) into ref_PaymentMethod table
    protected void InsertNewPaymentMethod(string showid)
    {
        try
        {
            string description = string.Empty;
            int isactive = 0;

            DataTable dt = fn.GetDatasetByCommand("Select * From tmp_refPaymentMethod", "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string methodName = dr["method_name"].ToString();
                    string methodUsedID = dr["method_usedid"].ToString();
                    string methodSortOrder = dr["method_sortorder"].ToString();

                    string query = "Select * From ref_PaymentMethod Where ShowID=@SHWID And method_usedid='" + methodUsedID + "'";
                    List<SqlParameter> pList = new List<SqlParameter>();
                    SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    spar.Value = showid;
                    pList.Add(spar);

                    DataTable dtPayMethod = fn.GetDatasetByCommand(query, "dsPayMethod", pList).Tables[0];
                    if (dtPayMethod.Rows.Count == 0)
                    {
                        string sql = string.Format("Insert Into ref_PaymentMethod (method_name,method_usedid,method_description,method_active,method_sortorder,ShowID)"
                                    + " Values('{0}', {1}, '{2}', {3}, {4}, '{5}')", methodName, methodUsedID, description, isactive, methodSortOrder, showid);

                        fn.ExecuteSQL(sql);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            pnlmessagebar.Visible = true;
            lblemsg.Text = "Error Occur :" + ex.Message;
        }
    }
    #endregion

    #region lnkCurrency_Click (redirect to ManageCurrency.aspx)
    protected void lnkCurrency_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            Response.Redirect("ManageCurrency.aspx?SHW=" + Request.QueryString["SHW"].ToString());
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region lnksitemaster_Click (redirect to ManageSiteReference.aspx)
    protected void lnksitemaster_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            Response.Redirect("ManageSiteReference.aspx?SHW=" + Request.QueryString["SHW"].ToString() + "&t=mas");
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region lnksitemaster_Click (redirect to ManageSiteReference.aspx)
    protected void lnkcssbandle_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            Response.Redirect("ManageSiteReference.aspx?SHW=" + Request.QueryString["SHW"].ToString() + "&t=css");
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region lnksitemaster_Click (redirect to ManageSiteReference.aspx)
    protected void lnksitereloginmaster_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            Response.Redirect("ManageSiteReference.aspx?SHW=" + Request.QueryString["SHW"].ToString() + "&t=rel");
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
}