﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Promo_List.aspx.cs" Inherits="Admin_Promo_List" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .rgOptions
        {
            display:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3 class="box-title">Manage Conference</h3>

    <asp:HyperLink ID="hplBack" runat="server" CssClass="btn btn-info"><i class="fa fa-arrow-left"></i> Back</asp:HyperLink>
    <br />
    <br />
    <div class="box-body table-responsive no-padding">
        <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" 
            EnableHeaderContextMenu="false" Skin="Bootstrap"
            EnableHeaderContextFilterMenu="false" AllowPaging="false" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="false"
            PageSize="20" >
            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                <Selecting AllowRowSelect="false"></Selecting>
            </ClientSettings>
            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="PromoCode" HeaderStyle-Font-Bold="true">
                <CommandItemSettings ShowExportToExcelButton="true"  ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                <Columns>
                    <telerik:GridTemplateColumn HeaderText="No." >
                        <ItemTemplate>
                            <%#Container.ItemIndex+1%>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridBoundColumn DataField="PromoCode" FilterControlAltText="Filter PromoCode"
                        HeaderText="PromoCode" SortExpression="PromoCode" UniqueName="PromoCode" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                    </telerik:GridBoundColumn>

                    <telerik:GridTemplateColumn HeaderText="Status" >
                        <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# setStatus(Eval("PromoCode").ToString())%>'
                            ForeColor='<%# setColor(Eval("PromoCode").ToString())%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Regno" >
                        <ItemTemplate>
                            <%#setRegno(Eval("PromoCode").ToString())%>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </div>
</asp:Content>

