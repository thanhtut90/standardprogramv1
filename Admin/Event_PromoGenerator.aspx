﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_PromoGenerator.aspx.cs" Inherits="Admin_Event_PromoGenerator" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        input[type="checkbox"], input[type="radio"]
        {
            margin : 4px !important;
        }
        .chkList label
        {
            font-weight:unset !important;
            /*padding-left:5px !important;
            vertical-align:top !important;*/
        }
        span
        {
            font-weight:700;
        }
    </style>
    
    <script src="js/plugins/jquery-1.9.1.min.js"></script>
    <script src="Content/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">

        function openModal() {
            $('#myModal').modal('show');
        }

        function closeModal() {
            $('#myModal').modal('hide');
        }
    </script>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <h3 class="box-title">Manage Reference Code</h3>

        <br />
        <div id="divPromoCode" runat="server">
            <h4><asp:Label ID="lblPromoHeader" runat="server" Text="Reference Code List" Visible="false"></asp:Label></h4>
            <asp:HyperLink ID="hplBack" runat="server" CssClass="btn btn-info" Visible="false"><i class="fa fa-arrow-left"></i> Back</asp:HyperLink>
            <asp:Button ID="btnAddNewConfItem" runat="server" Text="New Code" OnClick="btnAddNewConfItem_Click" CausesValidation="false" CssClass="btn btn-primary" />
            <asp:Button ID="btnExportAllPromoCode" runat="server" Text="Export All Promo Codes" OnClick="btnExportAllPromoCode_Click" CausesValidation="false" CssClass="btn btn-primary" />
            <br />
            <br />
            <div class="box-body table-responsive no-padding" id="divList" runat="server">
                <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" OnRowCommand="gvList_RowCommand">
                    <HeaderStyle />
                    <Columns>
                        <asp:TemplateField HeaderText="No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <%# Eval("Promo_Name")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="No of Code">
                            <ItemTemplate>
                                <%# Eval("Promo_Count")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="View">
                            <ItemTemplate>
                                <a href='<%# getUrl(Eval("Promo_Id").ToString(), Eval("Promo_CategoryID").ToString()) %>'>View Code</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Creating Additional Code">
                            <ItemTemplate>
                                <asp:Button class="btn btn-primary" ID="btnLaunchModal" runat="server" CausesValidation="false"
                                     CommandArgument='<%# Eval("Promo_Id") %>' CommandName="LaunchModal" Text="Create Additional Code" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                <%--<div style="display:none;">
                    <asp:GridView ID="grdExportAll" runat="server" DataKeyNames="PromoID" AutoGenerateColumns="true" CssClass="stdtable" AllowPaging="false"
                        GridLines="Both">
                        <HeaderStyle CssClass="theadstyle" />
                    </asp:GridView>
                </div>--%>
            </div>

            <%--<!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="btnHFcreate" style="display: none;" >
              Launch demo modal
            </button>
            <!-- Modal -->--%>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Additional Code</h4>
                        </div>
                        <div class="modal-body">
                            Enter Code Quantity
                            <asp:TextBox ID="txtNoofCode" runat="server"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbNoofCode" runat="server" TargetControlID="txtNoofCode"
                                    FilterType="Numbers" ValidChars="0123456789" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnClose" runat="server" class="btn btn-default" Text="Close" OnClick="btnClose_Click" CausesValidation="false"/>
                            <asp:Button ID="btnCreate" runat="server" class="btn btn-primary" Text="Create" OnClick="btnCreate_Click"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-horizontal" id="divPromoCodeInsert" runat="server" visible="false">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Promo Code Title</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txtPTitle" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <asp:RequiredFieldValidator ID="rfvPTitle" runat="server" 
                                ControlToValidate="txtPTitle" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group" style="display:none;">
                        <label class="col-md-2 control-label">Single/Multiple Usage</label>
                        <div class="col-md-6">
                            <asp:DropDownList ID="ddlPUsage" runat="server" AutoPostBack="true" onselectedindexchanged="ddlPUsage_SelectedIndexChanged" CssClass="form-control">
                                <asp:ListItem Value="1" Enabled="false">Single Usage</asp:ListItem>
                                <asp:ListItem Value="2" Selected="True">Multiple Usage</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="trMax" runat="server" visible="false" class="form-group">
                        <label class="col-md-2 control-label">Quota</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txtPMax" runat="server" Font-Size="Medium" MaxLength="6" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <asp:RequiredFieldValidator ID="rfvPMax" runat="server" 
                                ControlToValidate="txtPMax" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            &nbsp;<asp:RegularExpressionValidator ID="revPMax" 
                                runat="server" ControlToValidate="txtPMax" Display="Dynamic" 
                                ErrorMessage="Only numbers are allowed" ForeColor="Red" 
                                ValidationExpression="\d+"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Number of Code</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txtPNoofCode" runat="server" Font-Size="Medium" MaxLength="4" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <asp:RequiredFieldValidator ID="rfvPNoofCode" ControlToValidate="txtPNoofCode" runat="server" Enabled="true"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:FilteredTextBoxExtender ID="ftbPNoofCode" runat="server" TargetControlID="txtPNoofCode"
                                FilterType="Numbers" ValidChars="0123456789" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Prefix</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txtPPrefix" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <asp:RequiredFieldValidator ID="rfvPPrefix" ControlToValidate="txtPPrefix" runat="server" Enabled="false"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div id="divCondition" runat="server">
                        <div class="form-group" id="divFromTo" runat="server" visible="true">
                            <label class="col-md-2 control-label">Valid Date</label>
                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dateFrom" Skin="Bootstrap" runat="server" DateInput-Label="From: ">
                                    </telerik:RadDatePicker>
                                    <asp:RequiredFieldValidator ID="rfvFrom" ControlToValidate="dateFrom" runat="server" Enabled="true"
                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-md-6">
                                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dateTo" Skin="Bootstrap" runat="server" DateInput-Label="To: ">
                                    </telerik:RadDatePicker>
                                    <asp:RequiredFieldValidator ID="rfvTo" ControlToValidate="dateTo" runat="server" Enabled="true"
                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="col-md-4">
                                &nbsp;
                            </div>
                        </div>

                        <div class="form-group" id="divDiscountType" runat="server" visible="true">
                            <label class="col-md-2 control-label">Select Discount Type</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlDiscountType" runat="server" CssClass="form-control"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlDiscountType_SelectedIndexChanged">
                                    <asp:ListItem Text="Select Discount Type" Value="Select Discount Type"></asp:ListItem>
                                    <asp:ListItem Text="Percentage" Value="PCT"></asp:ListItem>
                                    <asp:ListItem Text="Amount" Value="SUBTRACT"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                <asp:CompareValidator ID="cvDiscountType" runat="server" 
                                    ControlToValidate="ddlDiscountType" ValueToCompare="Select Discount Type" Operator="NotEqual" Type="String" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            </div>
                        </div>

                        <div class="form-group" id="divPercentage" runat="server" visible="false">
                            <label class="col-md-2 control-label">Select Percentage</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlPercentage" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="Select Percentage" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                <asp:CompareValidator ID="cvPercentage" runat="server" Enabled="false"
                                    ControlToValidate="ddlPercentage" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            </div>
                        </div>

                        <div class="form-group" id="divAmount" runat="server" visible="false">
                            <label class="col-md-2 control-label">Amount</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtAmount" runat="server" Font-Size="Medium" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <asp:RequiredFieldValidator ID="rfvAmount" ControlToValidate="txtAmount" runat="server" Enabled="false"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:FilteredTextBoxExtender ID="ftbAmount" runat="server" 
                                    TargetControlID="txtAmount" FilterType="Numbers, Custom" ValidChars="." />
                            </div>
                        </div>

                        <div class="form-group" id="divOnRate" runat="server" visible="true">
                            <label class="col-md-2 control-label">Select Rate to apply promotion</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlOnRate" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="Select Rate to apply promotion" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="On Earlybird Rate" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="On Regular Rate" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                <asp:CompareValidator ID="cvOnRate" runat="server" 
                                    ControlToValidate="ddlOnRate" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            </div>
                        </div>

                        <div class="form-group" id="divProfession" runat="server" visible="false">
                            <asp:Label class="col-md-2 control-label" ID="lblProfession" runat="server" Text="Profession"></asp:Label>
                            <div class="col-md-10">
                                <asp:CheckBoxList ID="chklstProfession" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" CssClass="chkList"
                                    DataValueField="RefID" DataTextField="Name"></asp:CheckBoxList>
                                <asp:TextBox ID="txtTableFieldProfession" runat="server" Text="" Visible="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group" id="divCategory" runat="server" visible="false">
                            <asp:Label class="col-md-2 control-label" ID="lblCategory" runat="server" Text="Category"></asp:Label>
                            <div class="col-md-10">
                                <asp:CheckBoxList ID="chklstCategory" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" CssClass="chkList"
                                    DataValueField="RefID" DataTextField="Name"></asp:CheckBoxList>
                                <asp:TextBox ID="txtTableFieldCategory" runat="server" Text="" Visible="false"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group" id="divMainConferenceItem" runat="server" visible="false">
                            <asp:Label class="col-md-2 control-label" ID="lblMainConferenceItem" runat="server" Text="Main Conference Item"></asp:Label>
                            <div class="col-md-10">
                                <asp:CheckBoxList ID="chklstMainConfItem" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" CssClass="chkList"
                                    DataValueField="ConfID" DataTextField="ConfName"></asp:CheckBoxList>
                            </div>
                        </div>
                        <div class="form-group" id="divDependentConferenceItem" runat="server" visible="false">
                            <asp:Label class="col-md-2 control-label" ID="lblDependentConferenceItem" runat="server" Text="Dependent Conference Item"></asp:Label>
                            <div class="col-md-10">
                                <asp:CheckBoxList ID="chklstDependentConfItem" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" CssClass="chkList"
                                    DataValueField="ConfID" DataTextField="ConfName"></asp:CheckBoxList>
                            </div>
                        </div>

                        <div class="form-group" id="divCountry" runat="server" visible="false">
                            <asp:Label class="col-md-2 control-label" ID="lblCountry" runat="server" Text="Country"></asp:Label>
                            <div class="col-md-10">
                                <asp:CheckBoxList ID="chklstCountry" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" CssClass="chkList"
                                    DataValueField="RefID" DataTextField="Name"></asp:CheckBoxList>
                                <asp:TextBox ID="txtTableFieldCountry" runat="server" Text="" Visible="false"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            <asp:Button ID="btnSave" runat="server" Text="Save Code" CssClass="btn btn-primary" OnClick="SavePromoCode" />
                            <asp:HiddenField ID="hfPromoCodeID" runat="server" Value="" />
                        </div>
                        <div class="col-md-6">
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="btnCancel_Click"
                                CausesValidation="false" />
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                </div>
            </div>
        </div>

</asp:Content>