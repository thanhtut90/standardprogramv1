﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Corpit.BackendMaster;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System.Data.SqlClient;
using Telerik.Web.UI;

public partial class Admin_FormManagement_Group : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFUnz = new CommonFuns();
    protected string shwID;
    #region Form Parameters
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _Designation = "Designation";
    static string _Department = "Department";
    static string _Company = "Company";
    static string _Industry = "Industry";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _VisitDate = "Visit Date";
    static string _VisitTime = "Visit Time";
    static string _Password = "Password";
    static string _OtherSalutation = "Other Salutation";
    static string _OtherDesignation = "Other Designation";
    static string _OtherIndustry = "Other Industry";

    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Age = "Age";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string showid = cFUnz.DecryptValue(urlQuery.CurrShowID);
                string flowid = cFUnz.DecryptValue(urlQuery.FlowID);

                shwID = showid;

                bool isCreated = checkCreated(flowid, showid);
                if (!isCreated)
                {
                    InsertNewForm(flowid, showid);
                }

                setDynamicForm(flowid, showid);

                if (Request.QueryString["STP"] != null && Request.QueryString["a"] == null)
                {
                    string curStep = cFUnz.DecryptValue(urlQuery.CurrIndex);

                    FlowMaster fMaster = new FlowMaster();
                    fMaster.FlowID = flowid;
                    fMaster.FlowStatus = curStep;
                    FlowControler fControler = new FlowControler(fn);
                    fControler.UpdateFlowMasterStatus(fMaster);
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        else
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string showid = cFUnz.DecryptValue(urlQuery.CurrShowID);
                string flowid = cFUnz.DecryptValue(urlQuery.FlowID);

                shwID = showid;
            }
        }
    }

    #region checkCreated (check the record count of tmp_Form table is equal to the record counts of tb_Form table)
    private bool checkCreated(string flowid, string showid)
    {
        bool isCreated = false;
        DataTable dt = fn.GetDatasetByCommand("Select * From tmp_Form Where form_type='" + FormType.TypeGroup + "'", "ds").Tables[0];

        string query = "Select * From tb_Form Where form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID=@SHWID And form_input_name In (Select form_input_name From tmp_Form Where form_type='" + FormType.TypeGroup + "')";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dtForm = fn.GetDatasetByCommand(query, "dsForm", pList).Tables[0];
        if (dt.Rows.Count == dtForm.Rows.Count)
        {
            isCreated = true;
        }

        return isCreated;
    }
    #endregion

    #region Generate Dynamic Form and use table tb_Form & bind data to related controls
    protected void setDynamicForm(string flowid, string showid)
    {
        DataSet ds = new DataSet();
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForGroup();

        StringBuilder sb = new StringBuilder();
        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
        {
            #region form_input_name=Salutation & set chkshowSal and chkcompSal checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtSal textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowSal.Checked = true;
                    string jscript = string.Empty;

                    jscript = "document.getElementById('dvsal').style.display = '';";
                    jscript += "document.getElementById('dvsumsal').style.display = '';";
                    jscript += "document.getElementById('" + txtSal.ClientID + "').style.display = '';";

                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else 
                {
                    chkshowSal.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompSal.Checked = true;
                }
                else
                {
                    chkcompSal.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumSal.Checked = true;
                }
                else
                {
                    chksumSal.Checked = false;
                }

                txtSal.Text = inputtext;
            }
            #endregion

            #region form_input_name=FName & set chkshowFname and chkcompFname checked properties and txtFname display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtFname textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowFname.Checked = true;
                    string jscript = string.Empty;

                    jscript = "document.getElementById('dvfname').style.display = '';";
                    jscript += "document.getElementById('dvsumfname').style.display = '';";
                    jscript += "document.getElementById('" + txtFname.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    txtFname.Attributes.Add("display", "");
                }
                else
                {
                    chkshowFname.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompFname.Checked = true;
                }
                else
                {
                    chkcompFname.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumFname.Checked = true;
                }
                else
                {
                    chksumFname.Checked = false;
                }

                txtFname.Text = inputtext;
            }
            #endregion

            #region form_input_name=LName & set chkshowLname and chkcompLname checked properties and txtLname display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtLname textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowLname.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvlname').style.display = '';";
                    jscript += "document.getElementById('dvsumlname').style.display = '';";
                    jscript += "document.getElementById('" + txtLname.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtLname.Attributes.Add("display", "");
                }
                else
                {
                    chkshowLname.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompLname.Checked = true;
                }
                else
                {
                    chkcompLname.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumLname.Checked = true;
                }
                else
                {
                    chksumLname.Checked = false;
                }

                txtLname.Text = inputtext;
            }
            #endregion

            #region form_input_name=Designation & set chkshowDesignation and chkcompDesignation checked properties and txtDesignation display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtDesignation textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowDesignation.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvdesignation').style.display = '';";
                    jscript += "document.getElementById('dvsumdesignation').style.display = '';";
                    jscript += "document.getElementById('" + txtDesignation.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtDesignation.Attributes.Add("display", "");
                }
                else
                {
                    chkshowDesignation.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompDesignation.Checked = true;
                }
                else
                {
                    chkcompDesignation.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumDesignation.Checked = true;
                }
                else
                {
                    chksumDesignation.Checked = false;
                }

                txtDesignation.Text = inputtext;
            }
            #endregion

            #region form_input_name=Department & set chkshowDepartment and chkcompDepartment checked properties and txtDepartment display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtDepartment textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowDepartment.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvdepartment').style.display = '';";
                    jscript += "document.getElementById('dvsumdepartment').style.display = '';";
                    jscript += "document.getElementById('" + txtDepartment.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtDepartment.Attributes.Add("display", "");
                }
                else
                {
                    chkshowDepartment.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompDepartment.Checked = true;
                }
                else
                {
                    chkcompDepartment.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumDepartment.Checked = true;
                }
                else
                {
                    chksumDepartment.Checked = false;
                }

                txtDepartment.Text = inputtext;
            }
            #endregion

            #region form_input_name=Company & set chkshowCompany and chkcompCompany checked properties and txtCompany display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtCompany textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Company)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowCompany.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvcompany').style.display = '';";
                    jscript += "document.getElementById('dvsumcompany').style.display = '';";
                    jscript += "document.getElementById('" + txtCompany.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtCompany.Attributes.Add("display", "");
                }
                else
                {
                    chkshowCompany.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompCompany.Checked = true;
                }
                else
                {
                    chkcompCompany.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumCompany.Checked = true;
                }
                else
                {
                    chksumCompany.Checked = false;
                }

                txtCompany.Text = inputtext;
            }
            #endregion

            #region form_input_name=Industry & set chkshowIndustry and chkcompIndustry checked properties and txtIndustry display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtIndustry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Industry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowIndustry.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvindustry').style.display = '';";
                    jscript += "document.getElementById('dvsumindustry').style.display = '';";
                    jscript += "document.getElementById('" + txtIndustry.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtIndustry.Attributes.Add("display", "");
                }
                else
                {
                    chkshowIndustry.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompIndustry.Checked = true;
                }
                else
                {
                    chkcompIndustry.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumIndustry.Checked = true;
                }
                else
                {
                    chksumIndustry.Checked = false;
                }

                txtIndustry.Text = inputtext;
            }
            #endregion

            #region form_input_name=Address1 & set chkshowAddress1 and chkcompAddress1 checked properties and txtAddress1 display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAddress1 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAddress1.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaddress1').style.display = '';";
                    jscript += "document.getElementById('dvsumaddress1').style.display = '';";
                    jscript += "document.getElementById('" + txtAddress1.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtAddress1.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAddress1.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAddress1.Checked = true;
                }
                else
                {
                    chkcompAddress1.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumAddress1.Checked = true;
                }
                else
                {
                    chksumAddress1.Checked = false;
                }

                txtAddress1.Text = inputtext;
            }
            #endregion

            #region form_input_name=Address2 & set chkshowAddress2 and chkcompAddress2 checked properties and txtAddress2 display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAddress2 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAddress2.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaddress2').style.display = '';";
                    jscript += "document.getElementById('dvsumaddress2').style.display = '';";
                    jscript += "document.getElementById('" + txtAddress2.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtAddress2.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAddress2.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAddress2.Checked = true;
                }
                else
                {
                    chkcompAddress2.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumAddress2.Checked = true;
                }
                else
                {
                    chksumAddress2.Checked = false;
                }

                txtAddress2.Text = inputtext;
            }
            #endregion

            #region form_input_name=Address3 & set chkshowAddress3 and chkcompAddress3 checked properties and txtAddress3 display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAddress3 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAddress3.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaddress3').style.display = '';";
                    jscript += "document.getElementById('dvsumaddress3').style.display = '';";
                    jscript += "document.getElementById('" + txtAddress3.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtAddress3.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAddress3.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAddress3.Checked = true;
                }
                else
                {
                    chkcompAddress3.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumAddress3.Checked = true;
                }
                else
                {
                    chksumAddress3.Checked = false;
                }

                txtAddress3.Text = inputtext;
            }
            #endregion

            #region form_input_name=City & set chkshowCity and chkcompCity checked properties and txtCity display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtCity textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowCity.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvcity').style.display = '';";
                    jscript += "document.getElementById('dvsumcity').style.display = '';";
                    jscript += "document.getElementById('" + txtCity.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtCity.Attributes.Add("display", "");
                }
                else
                {
                    chkshowCity.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompCity.Checked = true;
                }
                else
                {
                    chkcompCity.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumCity.Checked = true;
                }
                else
                {
                    chksumCity.Checked = false;
                }

                txtCity.Text = inputtext;
            }
            #endregion

            #region form_input_name=State & set chkshowState and chkcompState checked properties and txtState display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtState textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowState.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvstate').style.display = '';";
                    jscript += "document.getElementById('dvsumstate').style.display = '';";
                    jscript += "document.getElementById('" + txtState.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtState.Attributes.Add("display", "");
                }
                else
                {
                    chkshowState.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompState.Checked = true;
                }
                else
                {
                    chkcompState.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumState.Checked = true;
                }
                else
                {
                    chksumState.Checked = false;
                }

                txtState.Text = inputtext;
            }
            #endregion

            #region form_input_name=PostalCode & set chkshowPostalCode and chkcompPostalCode checked properties and txtPostalCode display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtPostalCode textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowPostalCode.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvpostalcode').style.display = '';";
                    jscript += "document.getElementById('dvsumpostalcode').style.display = '';";
                    jscript += "document.getElementById('" + txtPostalCode.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtPostalCode.Attributes.Add("display", "");
                }
                else
                {
                    chkshowPostalCode.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompPostalCode.Checked = true;
                }
                else
                {
                    chkcompPostalCode.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumPostalCode.Checked = true;
                }
                else
                {
                    chksumPostalCode.Checked = false;
                }

                txtPostalCode.Text = inputtext;
            }
            #endregion

            #region form_input_name=Country & set chkshowCountry and chkcompCountry checked properties and txtCountry display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtCountry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowCountry.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvcountry').style.display = '';";
                    jscript += "document.getElementById('dvsumcountry').style.display = '';";
                    jscript += "document.getElementById('" + txtCountry.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtCountry.Attributes.Add("display", "");
                }
                else
                {
                    chkshowCountry.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompCountry.Checked = true;
                }
                else
                {
                    chkcompCountry.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumCountry.Checked = true;
                }
                else
                {
                    chksumCountry.Checked = false;
                }

                txtCountry.Text = inputtext;
            }
            #endregion

            #region form_input_name=RCountry & set chkshowRCountry and chkcompRCountry checked properties and txtRCountry display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtRCountry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowRCountry.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvrcountry').style.display = '';";
                    jscript += "document.getElementById('dvsumrcountry').style.display = '';";
                    jscript += "document.getElementById('" + txtRCountry.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtRCountry.Attributes.Add("display", "");
                }
                else
                {
                    chkshowRCountry.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompRCountry.Checked = true;
                }
                else
                {
                    chkcompRCountry.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumRCountry.Checked = true;
                }
                else
                {
                    chksumRCountry.Checked = false;
                }

                txtRCountry.Text = inputtext;
            }
            #endregion

            #region form_input_name=TelCC & set chkshowTelCC and chkcompTelCC checked properties and txtTelCC display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtTelCC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowTelCC.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvtelcc').style.display = '';";
                    jscript += "document.getElementById('dvsumtelcc').style.display = '';";
                    jscript += "document.getElementById('" + txtTelCC.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtTelCC.Attributes.Add("display", "");
                }
                else
                {
                    chkshowTelCC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompTelCC.Checked = true;
                }
                else
                {
                    chkcompTelCC.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumTelCC.Checked = true;
                }
                else
                {
                    chksumTelCC.Checked = false;
                }

                txtTelCC.Text = inputtext;
            }
            #endregion

            #region form_input_name=TelAC & set chkshowTelAC and chkcompTelAC checked properties and txtTelAC display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtTelAC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowTelAC.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvtelac').style.display = '';";
                    jscript += "document.getElementById('dvsumtelac').style.display = '';";
                    jscript += "document.getElementById('" + txtTelAC.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtTelAC.Attributes.Add("display", "");
                }
                else
                {
                    chkshowTelAC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompTelAC.Checked = true;
                }
                else
                {
                    chkcompTelAC.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumTelAC.Checked = true;
                }
                else
                {
                    chksumTelAC.Checked = false;
                }

                txtTelAC.Text = inputtext;
            }
            #endregion

            #region form_input_name=Tel & set chkshowTel and chkcompTel checked properties and txtTel display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtTel textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowTel.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvtel').style.display = '';";
                    jscript += "document.getElementById('dvsumtel').style.display = '';";
                    jscript += "document.getElementById('" + txtTel.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtTel.Attributes.Add("display", "");
                }
                else
                {
                    chkshowTel.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompTel.Checked = true;
                }
                else
                {
                    chkcompTel.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumTel.Checked = true;
                }
                else
                {
                    chksumTel.Checked = false;
                }

                txtTel.Text = inputtext;
            }
            #endregion

            #region form_input_name=MobileCC & set chkshowMobileCC and chkcompMobileCC checked properties and txtMobileCC display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtMobileCC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowMobileCC.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvmobilecc').style.display = '';";
                    jscript += "document.getElementById('dvsummobilecc').style.display = '';";
                    jscript += "document.getElementById('" + txtMobileCC.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtMobileCC.Attributes.Add("display", "");
                }
                else
                {
                    chkshowMobileCC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompMobileCC.Checked = true;
                }
                else
                {
                    chkcompMobileCC.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumMobileCC.Checked = true;
                }
                else
                {
                    chksumMobileCC.Checked = false;
                }

                txtMobileCC.Text = inputtext;
            }
            #endregion

            #region form_input_name=Mobileac & set chkshowMobileAC and chkcompMobileAC checked properties and txtMobileAC display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtMobileAC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowMobileAC.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvmobileac').style.display = '';";
                    jscript += "document.getElementById('dvsummobileac').style.display = '';";
                    jscript += "document.getElementById('" + txtMobileAC.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtMobileAC.Attributes.Add("display", "");
                }
                else
                {
                    chkshowMobileAC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompMobileAC.Checked = true;
                }
                else
                {
                    chkcompMobileAC.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumMobileAC.Checked = true;
                }
                else
                {
                    chksumMobileAC.Checked = false;
                }

                txtMobileAC.Text = inputtext;
            }
            #endregion

            #region form_input_name=Mobile & set chkshowMobile and chkcompMobile checked properties and txtMobile display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtMobile textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowMobile.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvmobile').style.display = '';";
                    jscript += "document.getElementById('dvsummobile').style.display = '';";
                    jscript += "document.getElementById('" + txtMobile.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtCity.Attributes.Add("display", "");
                }
                else
                {
                    chkshowMobile.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompMobile.Checked = true;
                }
                else
                {
                    chkcompMobile.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumMobile.Checked = true;
                }
                else
                {
                    chksumMobile.Checked = false;
                }

                txtMobile.Text = inputtext;
            }
            #endregion

            #region form_input_name=FaxCC & set chkshowFaxCC and chkcompFaxCC checked properties and txtFaxCC display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtFaxCC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowFaxCC.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvfaxcc').style.display = '';";
                    jscript += "document.getElementById('dvsumfaxcc').style.display = '';";
                    jscript += "document.getElementById('" + txtFaxCC.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtFaxCC.Attributes.Add("display", "");
                }
                else
                {
                    chkshowFaxCC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompFaxCC.Checked = true;
                }
                else
                {
                    chkcompFaxCC.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumFaxCC.Checked = true;
                }
                else
                {
                    chksumFaxCC.Checked = false;
                }

                txtFaxCC.Text = inputtext;
            }
            #endregion

            #region form_input_name=FaxAC & set chkshowFaxAC and chkcompFaxAC checked properties and txtFaxAC display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtFaxAC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowFaxAC.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvfaxac').style.display = '';";
                    jscript += "document.getElementById('dvsumfaxac').style.display = '';";
                    jscript += "document.getElementById('" + txtFaxAC.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    txtFaxAC.Attributes.Add("display", "");
                }
                else
                {
                    chkshowFaxAC.Checked = false;
                }
                if (isrequired == 1)
                {
                    chkcompFaxAC.Checked = true;
                }
                else
                {
                    chkcompFaxAC.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumFaxAC.Checked = true;
                }
                else
                {
                    chksumFaxAC.Checked = false;
                }

                txtFaxAC.Text = inputtext;
            }
            #endregion

            #region form_input_name=Fax & set chkshowFax and chkcompFax checked properties and txtFax display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtFax textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowFax.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvfax').style.display = '';";
                    jscript += "document.getElementById('dvsumfax').style.display = '';";
                    jscript += "document.getElementById('" + txtFax.ClientID + "').style.display = '';";
                    sb.Append(jscript);
                    txtFax.Attributes.Add("display", "");
                }
                else
                {
                    chkshowFax.Checked = false;
                }
                if (isrequired == 1)
                {
                    chkcompFax.Checked = true;
                }
                else
                {
                    chkcompFax.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumFax.Checked = true;
                }
                else
                {
                    chksumFax.Checked = false;
                }

                txtFax.Text = inputtext;
            }
            #endregion

            #region form_input_name=Email & set chkshowEmail and chkcompEmail checked properties and txtEmail display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtEmail textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowEmail.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvemail').style.display = '';";
                    jscript += "document.getElementById('dvsumemail').style.display = '';";
                    jscript += "document.getElementById('" + txtEmail.ClientID + "').style.display = '';";
                    sb.Append(jscript);
                    txtEmail.Attributes.Add("display", "");
                }
                else
                {
                    chkshowEmail.Checked = false;
                }
                if (isrequired == 1)
                {
                    chkcompEmail.Checked = true;
                }
                else
                {
                    chkcompEmail.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumEmail.Checked = true;
                }
                else
                {
                    chksumEmail.Checked = false;
                }

                txtEmail.Text = inputtext;
            }
            #endregion

            #region form_input_name=EmailConfirmation & set chkshowEmailConfirmation and chkcompEmailConfirmation checked properties and txtEmailConfirmation display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtEmailConfirmation textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _EmailConfirmation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowEmailConfirmation.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvemailconfirmation').style.display = '';";
                    jscript += "document.getElementById('dvsumemailconfirmation').style.display = '';";
                    jscript += "document.getElementById('" + txtEmailConfirmation.ClientID + "').style.display = '';";
                    sb.Append(jscript);
                    txtEmailConfirmation.Attributes.Add("display", "");
                }
                else
                {
                    chkshowEmailConfirmation.Checked = false;
                }
                if (isrequired == 1)
                {
                    chkcompEmailConfirmation.Checked = true;
                }
                else
                {
                    chkcompEmailConfirmation.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumEmailConfirmation.Checked = true;
                }
                else
                {
                    chksumEmailConfirmation.Checked = false;
                }

                txtEmailConfirmation.Text = inputtext;
            }
            #endregion

            #region form_input_name=VisitDate & set chkshowVisitDate and chkcompVisitDate checked properties and txtVisitDate display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtVisitDate textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VisitDate)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowVisitDate.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvvisitdate').style.display = '';";
                    jscript += "document.getElementById('dvsumvisitdate').style.display = '';";
                    jscript += "document.getElementById('" + txtVisitDate.ClientID + "').style.display = '';";
                    sb.Append(jscript);
                    txtVisitDate.Attributes.Add("display", "");
                }
                else
                {
                    chkshowVisitDate.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompVisitDate.Checked = true;
                }
                else
                {
                    chkcompVisitDate.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumVisitDate.Checked = true;
                }
                else
                {
                    chksumVisitDate.Checked = false;
                }

                txtVisitDate.Text = inputtext;
            }
            #endregion

            #region form_input_name=VisitTime & set chkshowVisitTime and chkcompVisitTime checked properties and txtVisitTime display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtVisitTime textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VisitTime)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowVisitTime.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvvisittime').style.display = '';";
                    jscript += "document.getElementById('dvsumvisittime').style.display = '';";
                    jscript += "document.getElementById('" + txtVisitTime.ClientID + "').style.display = '';";
                    sb.Append(jscript);
                    txtVisitTime.Attributes.Add("display", "");
                }
                else
                {
                    chkshowVisitTime.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompVisitTime.Checked = true;
                }
                else
                {
                    chkcompVisitTime.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumVisitTime.Checked = true;
                }
                else
                {
                    chksumVisitTime.Checked = false;
                }

                txtVisitTime.Text = inputtext;
            }
            #endregion
            
            #region form_input_name=Password & set chkshowPassword and chkcompPassword checked properties and txtPassword display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtPassword textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Password)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowPassword.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvpassword').style.display = '';";
                    jscript += "document.getElementById('dvsumpassword').style.display = '';";
                    jscript += "document.getElementById('" + txtPassword.ClientID + "').style.display = '';";
                    sb.Append(jscript);
                    txtPassword.Attributes.Add("display", "");
                }
                else
                {
                    chkshowPassword.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompPassword.Checked = true;
                }
                else
                {
                    chkcompPassword.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumPassword.Checked = true;
                }
                else
                {
                    chksumPassword.Checked = false;
                }

                txtPassword.Text = inputtext;
            }
            #endregion

            #region form_input_name=Gender & set chkshowGender and chkcompGender checked properties and txtGender display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional1 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowGender.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvgender').style.display = '';";
                    jscript += "document.getElementById('dvsumgender').style.display = '';";
                    jscript += "document.getElementById('" + txtGender.ClientID + "').style.display = '';";
                    sb.Append(jscript);
                    txtGender.Attributes.Add("display", "");
                }
                else
                {
                    chkshowGender.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompGender.Checked = true;
                }
                else
                {
                    chkcompGender.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumGender.Checked = true;
                }
                else
                {
                    chksumGender.Checked = false;
                }

                txtGender.Text = inputtext;
            }
            #endregion

            #region form_input_name=DOB & set chkshowAdditional2 and chkcompDOB checked properties and txtDOB display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional2 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowDOB.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvDOB').style.display = '';";
                    jscript += "document.getElementById('dvsumDOB').style.display = '';";
                    jscript += "document.getElementById('" + txtDOB.ClientID + "').style.display = '';";
                    sb.Append(jscript);
                    txtDOB.Attributes.Add("display", "");
                }
                else
                {
                    chkshowDOB.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompDOB.Checked = true;
                }
                else
                {
                    chkcompDOB.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumDOB.Checked = true;
                }
                else
                {
                    chksumDOB.Checked = false;
                }

                txtDOB.Text = inputtext;
            }
            #endregion

            #region form_input_name=Age & set chkshowAdditional3 and chkcompAge checked properties and txtAge display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional3 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAge.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvage').style.display = '';";
                    jscript += "document.getElementById('dvsumage').style.display = '';";
                    jscript += "document.getElementById('" + txtAge.ClientID + "').style.display = '';";
                    sb.Append(jscript);
                    txtAge.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAge.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAge.Checked = true;
                }
                else
                {
                    chkcompAge.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumAge.Checked = true;
                }
                else
                {
                    chksumAge.Checked = false;
                }

                txtAge.Text = inputtext;
            }
            #endregion

            #region form_input_name=Additional4 & set chkshowAdditional4 and chkcompAdditional4 checked properties and txtAdditional4 display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional4 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAdditional4.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvadditional4').style.display = '';";
                    jscript += "document.getElementById('dvsumadditional4').style.display = '';";
                    jscript += "document.getElementById('" + txtAdditional4.ClientID + "').style.display = '';";
                    sb.Append(jscript);
                    txtAdditional4.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAdditional4.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAdditional4.Checked = true;
                }
                else
                {
                    chkcompAdditional4.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumAdditional4.Checked = true;
                }
                else
                {
                    chksumAdditional4.Checked = false;
                }

                txtAdditional4.Text = inputtext;
            }
            #endregion

            #region form_input_name=Additional5 & set chkshowAdditional5 and chkcompAdditional5 checked properties and txtAdditional5 display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional5 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAdditional5.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvadditional5').style.display = '';";
                    jscript += "document.getElementById('dvsumadditional5').style.display = '';";
                    jscript += "document.getElementById('" + txtAdditional5.ClientID + "').style.display = '';";
                    sb.Append(jscript);
                    txtAdditional5.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAdditional5.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAdditional5.Checked = true;
                }
                else
                {
                    chkcompAdditional5.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumAdditional5.Checked = true;
                }
                else
                {
                    chksumAdditional5.Checked = false;
                }

                txtAdditional5.Text = inputtext;
            }
            #endregion
            
            #region form_input_name=OtherSalutation & set chkshowOtherSalutation and chkcompOtherSalutation checked properties and txtOtherSalutation display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtOtherSalutation textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OtherSalutation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowOtherSalutation.Checked = true;
                    string jscript = string.Empty;

                    jscript = "document.getElementById('dvothersalutation').style.display = '';";
                    jscript += "document.getElementById('dvsumothersalutation').style.display = '';";
                    jscript += "document.getElementById('" + txtOtherSalutation.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    txtOtherSalutation.Attributes.Add("display", "");
                }
                else
                {
                    chkshowOtherSalutation.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompOtherSalutation.Checked = true;
                }
                else
                {
                    chkcompOtherSalutation.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumOtherSalutation.Checked = true;
                }
                else
                {
                    chksumOtherSalutation.Checked = false;
                }

                txtOtherSalutation.Text = inputtext;
            }
            #endregion

            #region form_input_name=OtherDesignation & set chkOtherDesignation and chkcompOtherDesignation checked properties and txtOtherDesignation display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtOtherDesignation textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OtherDesignation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowOtherDesignation.Checked = true;
                    string jscript = string.Empty;

                    jscript = "document.getElementById('dvotherdesignation').style.display = '';";
                    jscript += "document.getElementById('dvsumotherdesignation').style.display = '';";
                    jscript += "document.getElementById('" + txtOtherDesignation.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    txtOtherDesignation.Attributes.Add("display", "");
                }
                else
                {
                    chkshowOtherDesignation.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompOtherDesignation.Checked = true;
                }
                else
                {
                    chkcompOtherDesignation.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumOtherDesignation.Checked = true;
                }
                else
                {
                    chksumOtherDesignation.Checked = false;
                }

                txtOtherDesignation.Text = inputtext;
            }
            #endregion

            #region form_input_name=OtherIndustry & set chkOtherIndustry and chkcompOtherIndustry checked properties and txtOtherIndustry display property true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtOtherIndustry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OtherIndustry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowOtherIndustry.Checked = true;
                    string jscript = string.Empty;

                    jscript = "document.getElementById('dvotherindustry').style.display = '';";
                    jscript += "document.getElementById('dvsumotherindustry').style.display = '';";
                    jscript += "document.getElementById('" + txtOtherIndustry.ClientID + "').style.display = '';";

                    sb.Append(jscript);
                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    txtOtherIndustry.Attributes.Add("display", "");
                }
                else
                {
                    chkshowOtherIndustry.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompOtherIndustry.Checked = true;
                }
                else
                {
                    chkcompOtherIndustry.Checked = false;
                }

                if (issummary == 1)
                {
                    chksumOtherIndustry.Checked = true;
                }
                else
                {
                    chksumOtherIndustry.Checked = false;
                }

                txtOtherIndustry.Text = inputtext;
            }
            #endregion


        }
        ClientScript.RegisterStartupScript(this.GetType(), "show", sb.ToString(), true);
    }
    #endregion

    # region SaveForm and using tb_Form table & Update tb_Form table set form_input_isshow, form_input_isrequired and form_input_text fields according to form_input_name
    protected void SaveForm(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = cFUnz.DecryptValue(Request.QueryString["SHW"].ToString());
            string flowid = cFUnz.DecryptValue(Request.QueryString["FLW"].ToString());

            try
            {
                #region Declaration
                string strshowSal = "0";
                string strcompSal = "0";
                string strSal = string.Empty;
                string strsumSal = "0";

                string strshowFname = "0";
                string strcompFname = "0";
                string strFname = string.Empty;
                string strsumFname = "0";

                string strshowLname = "0";
                string strcompLname = "0";
                string strLname = string.Empty;
                string strsumLname = "0";

                string strshowDesignation = "0";
                string strcompDesignation = "0";
                string strDesignation = string.Empty;
                string strsumDesignation = "0";

                string strshowDepartment = "0";
                string strcompDepartment = "0";
                string strDepartment = string.Empty;
                string strsumDepartment = "0";

                string strshowCompany = "0";
                string strcompCompany = "0";
                string strCompany = string.Empty;
                string strsumCompany = "0";

                string strshowIndustry = "0";
                string strcompIndustry = "0";
                string strIndustry = string.Empty;
                string strsumIndustry = "0";

                string strshowAddress1 = "0";
                string strcompAddress1 = "0";
                string strAddress1 = string.Empty;
                string strsumAddress1 = "0";

                string strshowAddress2 = "0";
                string strcompAddress2 = "0";
                string strAddress2 = string.Empty;
                string strsumAddress2 = "0";

                string strshowAddress3 = "0";
                string strcompAddress3 = "0";
                string strAddress3 = string.Empty;
                string strsumAddress3 = "0";

                string strshowCity = "0";
                string strcompCity = "0";
                string strCity = string.Empty;
                string strsumCity = "0";

                string strshowState = "0";
                string strcompState = "0";
                string strState = string.Empty;
                string strsumState = "0";

                string strshowPostalCode = "0";
                string strcompPostalCode = "0";
                string strPostalCode = string.Empty;
                string strsumPostalCode = "0";

                string strshowCountry = "0";
                string strcompCountry = "0";
                string strCountry = string.Empty;
                string strsumCountry = "0";

                string strshowRCountry = "0";
                string strcompRCountry = "0";
                string strRCountry = string.Empty;
                string strsumRCountry = "0";

                string strshowTelCC = "0";
                string strcompTelCC = "0";
                string strTelCC = string.Empty;
                string strsumTelCC = "0";

                string strshowTelAC = "0";
                string strcompTelAC = "0";
                string strTelAC = string.Empty;
                string strsumTelAC = "0";

                string strshowTel = "0";
                string strcompTel = "0";
                string strTel = string.Empty;
                string strsumTel = "0";

                string strshowMobileCC = "0";
                string strcompMobileCC = "0";
                string strMobileCC = string.Empty;
                string strsumMobileCC = "0";

                string strshowMobileAC = "0";
                string strcompMobileAC = "0";
                string strMobileAC = string.Empty;
                string strsumMobileAC = "0";

                string strshowMobile = "0";
                string strcompMobile = "0";
                string strMobile = string.Empty;
                string strsumMobile = "0";

                string strshowFaxCC = "0";
                string strcompFaxCC = "0";
                string strFaxCC = string.Empty;
                string strsumFaxCC = "0";

                string strshowFaxAC = "0";
                string strcompFaxAC = "0";
                string strFaxAC = string.Empty;
                string strsumFaxAC = "0";

                string strshowFax = "0";
                string strcompFax = "0";
                string strFax = string.Empty;
                string strsumFax = "0";

                string strshowEmail = "0";
                string strcompEmail = "0";
                string strEmail = string.Empty;
                string strsumEmail = "0";

                string strshowEmailConfirmation = "0";
                string strcompEmailConfirmation = "0";
                string strEmailConfirmation = string.Empty;
                string strsumEmailConfirmation = "0";

                string strshowVisitDate = "0";
                string strcompVisitDate = "0";
                string strVisitDate = string.Empty;
                string strsumVisitDate = "0";

                string strshowVisitTime = "0";
                string strcompVisitTime = "0";
                string strVisitTime = string.Empty;
                string strsumVisitTime = "0";

                string strshowPassword = "0";
                string strcompPassword = "0";
                string strPassword = string.Empty;
                string strsumPassword = "0";

                string strshowGender = "0";
                string strcompGender = "0";
                string strGender = string.Empty;
                string strsumGender = "0";

                string strshowDOB = "0";
                string strcompDOB = "0";
                string strDOB = string.Empty;
                string strsumDOB = "0";

                string strshowAge = "0";
                string strcompAge = "0";
                string strAge = string.Empty;
                string strsumAge = "0";

                string strshowAdditional4 = "0";
                string strcompAdditional4 = "0";
                string strAdditional4 = string.Empty;
                string strsumAdditional4 = "0";

                string strshowAdditional5 = "0";
                string strcompAdditional5 = "0";
                string strAdditional5 = string.Empty;
                string strsumAdditional5 = "0";

                string strshowOtherSalutation = "0";
                string strcompOtherSalutation = "0";
                string strOtherSalutation = string.Empty;
                string strsumOtherSalutation = "0";

                string strshowOtherDesignation = "0";
                string strcompOtherDesignation = "0";
                string strOtherDesignation = string.Empty;
                string strsumOtherDesignation = "0";

                string strshowOtherIndustry = "0";
                string strcompOtherIndustry = "0";
                string strOtherIndustry = string.Empty;
                string strsumOtherIndustry = "0";
                #endregion

                #region set variable values
                if (chkshowSal.Checked == true)
                {
                    strshowSal = "1";
                    strSal = txtSal.Text.ToString();
                    if (chkcompSal.Checked == true)
                    {
                        strcompSal = "1";
                    }
                    else
                    {
                        strcompSal = "0";
                    }

                    if (chksumSal.Checked == true)
                    {
                        strsumSal = "1";
                    }
                    else
                    {
                        strsumSal = "0";
                    }
                }
                if (chkshowFname.Checked == true)
                {
                    strshowFname = "1";
                    strFname = txtFname.Text.ToString();
                    if (chkcompFname.Checked == true)
                    {
                        strcompFname = "1";
                    }
                    else
                    {
                        strcompFname = "0";
                    }

                    if (chksumFname.Checked == true)
                    {
                        strsumFname = "1";
                    }
                    else
                    {
                        strsumFname = "0";
                    }
                }
                if (chkshowLname.Checked == true)
                {
                    strshowLname = "1";
                    strLname = txtLname.Text.ToString();
                    if (chkcompLname.Checked == true)
                    {
                        strcompLname = "1";
                    }
                    else
                    {
                        strcompLname = "0";
                    }

                    if (chksumLname.Checked == true)
                    {
                        strsumLname = "1";
                    }
                    else
                    {
                        strsumLname = "0";
                    }
                }

                if (chkshowDesignation.Checked == true)
                {
                    strshowDesignation = "1";
                    strDesignation = txtDesignation.Text.ToString();
                    if (chkcompDesignation.Checked == true)
                    {
                        strcompDesignation = "1";
                    }
                    else
                    {
                        strcompDesignation = "0";
                    }

                    if (chksumDesignation.Checked == true)
                    {
                        strsumDesignation = "1";
                    }
                    else
                    {
                        strsumDesignation = "0";
                    }
                }

                if (chkshowDepartment.Checked == true)
                {
                    strshowDepartment = "1";
                    strDepartment = txtDepartment.Text.ToString();
                    if (chkcompDepartment.Checked == true)
                    {
                        strcompDepartment = "1";
                    }
                    else
                    {
                        strcompDepartment = "0";
                    }

                    if (chksumDepartment.Checked == true)
                    {
                        strsumDepartment = "1";
                    }
                    else
                    {
                        strsumDepartment = "0";
                    }
                }

                if (chkshowCompany.Checked == true)
                {
                    strshowCompany = "1";
                    strCompany = txtCompany.Text.ToString();
                    if (chkcompCompany.Checked == true)
                    {
                        strcompCompany = "1";
                    }
                    else
                    {
                        strcompCompany = "0";
                    }

                    if (chksumCompany.Checked == true)
                    {
                        strsumCompany = "1";
                    }
                    else
                    {
                        strsumCompany = "0";
                    }
                }

                if (chkshowIndustry.Checked == true)
                {
                    strshowIndustry = "1";
                    strIndustry = txtIndustry.Text.ToString();
                    if (chkcompIndustry.Checked == true)
                    {
                        strcompIndustry = "1";
                    }
                    else
                    {
                        strcompIndustry = "0";
                    }

                    if (chksumIndustry.Checked == true)
                    {
                        strsumIndustry = "1";
                    }
                    else
                    {
                        strsumIndustry = "0";
                    }
                }

                if (chkshowAddress1.Checked == true)
                {
                    strshowAddress1 = "1";
                    strAddress1 = txtAddress1.Text.ToString();
                    if (chkcompAddress1.Checked == true)
                    {
                        strcompAddress1 = "1";
                    }
                    else
                    {
                        strcompAddress1 = "0";
                    }

                    if (chksumAddress1.Checked == true)
                    {
                        strsumAddress1 = "1";
                    }
                    else
                    {
                        strsumAddress1 = "0";
                    }
                }

                if (chkshowAddress2.Checked == true)
                {
                    strshowAddress2 = "1";
                    strAddress2 = txtAddress2.Text.ToString();
                    if (chkcompAddress2.Checked == true)
                    {
                        strcompAddress2 = "1";
                    }
                    else
                    {
                        strcompAddress2 = "0";
                    }

                    if (chksumAddress2.Checked == true)
                    {
                        strsumAddress2 = "1";
                    }
                    else
                    {
                        strsumAddress2 = "0";
                    }
                }

                if (chkshowAddress3.Checked == true)
                {
                    strshowAddress3 = "1";
                    strAddress3 = txtAddress3.Text.ToString();
                    if (chkcompAddress3.Checked == true)
                    {
                        strcompAddress3 = "1";
                    }
                    else
                    {
                        strcompAddress3 = "0";
                    }

                    if (chksumAddress3.Checked == true)
                    {
                        strsumAddress3 = "1";
                    }
                    else
                    {
                        strsumAddress3 = "0";
                    }
                }

                if (chkshowCity.Checked == true)
                {
                    strshowCity = "1";
                    strCity = txtCity.Text.ToString();
                    if (chkcompCity.Checked == true)
                    {
                        strcompCity = "1";
                    }
                    else
                    {
                        strcompCity = "0";
                    }

                    if (chksumCity.Checked == true)
                    {
                        strsumCity = "1";
                    }
                    else
                    {
                        strsumCity = "0";
                    }
                }
                if (chkshowState.Checked == true)
                {
                    strshowState = "1";
                    strState = txtState.Text.ToString();
                    if (chkcompState.Checked == true)
                    {
                        strcompState = "1";
                    }
                    else
                    {
                        strcompState = "0";
                    }

                    if (chksumState.Checked == true)
                    {
                        strsumState = "1";
                    }
                    else
                    {
                        strsumState = "0";
                    }
                }
                if (chkshowPostalCode.Checked == true)
                {
                    strshowPostalCode = "1";
                    strPostalCode = txtPostalCode.Text.ToString();
                    if (chkcompPostalCode.Checked == true)
                    {
                        strcompPostalCode = "1";
                    }
                    else
                    {
                        strcompPostalCode = "0";
                    }

                    if (chksumPostalCode.Checked == true)
                    {
                        strsumPostalCode = "1";
                    }
                    else
                    {
                        strsumPostalCode = "0";
                    }
                }

                if (chkshowCountry.Checked == true)
                {
                    strshowCountry = "1";
                    strCountry = txtCountry.Text.ToString();
                    if (chkcompCountry.Checked == true)
                    {
                        strcompCountry = "1";
                    }
                    else
                    {
                        strcompCountry = "0";
                    }

                    if (chksumCountry.Checked == true)
                    {
                        strsumCountry = "1";
                    }
                    else
                    {
                        strsumCountry = "0";
                    }
                }

                if (chkshowRCountry.Checked == true)
                {
                    strshowRCountry = "1";
                    strRCountry = txtRCountry.Text.ToString();
                    if (chkcompRCountry.Checked == true)
                    {
                        strcompRCountry = "1";
                    }
                    else
                    {
                        strcompRCountry = "0";
                    }

                    if (chksumRCountry.Checked == true)
                    {
                        strsumRCountry = "1";
                    }
                    else
                    {
                        strsumRCountry = "0";
                    }
                }

                if (chkshowTelCC.Checked == true)
                {
                    strshowTelCC = "1";
                    strTelCC = txtTelCC.Text.ToString();
                    if (chkcompTelCC.Checked == true)
                    {
                        strcompTelCC = "1";
                    }
                    else
                    {
                        strcompTelCC = "0";
                    }

                    if (chksumTelCC.Checked == true)
                    {
                        strsumTelCC = "1";
                    }
                    else
                    {
                        strsumTelCC = "0";
                    }
                }

                if (chkshowTelAC.Checked == true)
                {
                    strshowTelAC = "1";
                    strTelAC = txtTelAC.Text.ToString();
                    if (chkcompTelAC.Checked == true)
                    {
                        strcompTelAC = "1";
                    }
                    else
                    {
                        strcompTelAC = "0";
                    }

                    if (chksumTelAC.Checked == true)
                    {
                        strsumTelAC = "1";
                    }
                    else
                    {
                        strsumTelAC = "0";
                    }
                }

                if (chkshowTel.Checked == true)
                {
                    strshowTel = "1";
                    strTel = txtTel.Text.ToString();
                    if (chkcompTel.Checked == true)
                    {
                        strcompTel = "1";
                    }
                    else
                    {
                        strcompTel = "0";
                    }

                    if (chksumTel.Checked == true)
                    {
                        strsumTel = "1";
                    }
                    else
                    {
                        strsumTel = "0";
                    }
                }

                if (chkshowMobileCC.Checked == true)
                {
                    strshowMobileCC = "1";
                    strMobileCC = txtMobileCC.Text.ToString();
                    if (chkcompMobileCC.Checked == true)
                    {
                        strcompMobileCC = "1";
                    }
                    else
                    {
                        strcompMobileCC = "0";
                    }

                    if (chksumMobileCC.Checked == true)
                    {
                        strsumMobileCC = "1";
                    }
                    else
                    {
                        strsumMobileCC = "0";
                    }
                }

                if (chkshowMobileAC.Checked == true)
                {
                    strshowMobileAC = "1";
                    strMobileAC = txtMobileAC.Text.ToString();
                    if (chkcompMobileAC.Checked == true)
                    {
                        strcompMobileAC = "1";
                    }
                    else
                    {
                        strcompMobileAC = "0";
                    }

                    if (chksumMobileAC.Checked == true)
                    {
                        strsumMobileAC = "1";
                    }
                    else
                    {
                        strsumMobileAC = "0";
                    }
                }

                if (chkshowMobile.Checked == true)
                {
                    strshowMobile = "1";
                    strMobile = txtMobile.Text.ToString();
                    if (chkcompMobile.Checked == true)
                    {
                        strcompMobile = "1";
                    }
                    else
                    {
                        strcompMobile = "0";
                    }

                    if (chksumMobile.Checked == true)
                    {
                        strsumMobile = "1";
                    }
                    else
                    {
                        strsumMobile = "0";
                    }
                }
                if (chkshowFaxCC.Checked == true)
                {
                    strshowFaxCC = "1";
                    strFaxCC = txtFaxCC.Text.ToString();
                    if (chkcompFaxCC.Checked == true)
                    {
                        strcompFaxCC = "1";
                    }
                    else
                    {
                        strcompFaxCC = "0";
                    }

                    if (chksumFaxCC.Checked == true)
                    {
                        strsumFaxCC = "1";
                    }
                    else
                    {
                        strsumFaxCC = "0";
                    }
                }

                if (chkshowFaxAC.Checked == true)
                {
                    strshowFaxAC = "1";
                    strFaxAC = txtFaxAC.Text.ToString();
                    if (chkcompFaxAC.Checked == true)
                    {
                        strcompFaxAC = "1";
                    }
                    else
                    {
                        strcompFaxAC = "0";
                    }

                    if (chksumFaxAC.Checked == true)
                    {
                        strsumFaxAC = "1";
                    }
                    else
                    {
                        strsumFaxAC = "0";
                    }
                }

                if (chkshowFax.Checked == true)
                {
                    strshowFax = "1";
                    strFax = txtFax.Text.ToString();
                    if (chkcompFax.Checked == true)
                    {
                        strcompFax = "1";
                    }
                    else
                    {
                        strcompFax = "0";
                    }

                    if (chksumFax.Checked == true)
                    {
                        strsumFax = "1";
                    }
                    else
                    {
                        strsumFax = "0";
                    }
                }

                if (chkshowEmail.Checked == true)
                {
                    strshowEmail = "1";
                    strEmail = txtEmail.Text.ToString();
                    if (chkcompEmail.Checked == true)
                    {
                        strcompEmail = "1";
                    }
                    else
                    {
                        strcompEmail = "0";
                    }

                    if (chksumEmail.Checked == true)
                    {
                        strsumEmail = "1";
                    }
                    else
                    {
                        strsumEmail = "0";
                    }
                }

                if (chkshowEmailConfirmation.Checked == true)
                {
                    strshowEmailConfirmation = "1";
                    strEmailConfirmation = txtEmailConfirmation.Text.ToString();
                    if (chkcompEmailConfirmation.Checked == true)
                    {
                        strcompEmailConfirmation = "1";
                    }
                    else
                    {
                        strcompEmailConfirmation = "0";
                    }

                    if (chksumEmailConfirmation.Checked == true)
                    {
                        strsumEmailConfirmation = "1";
                    }
                    else
                    {
                        strsumEmailConfirmation = "0";
                    }
                }
                if (chkshowVisitDate.Checked == true)
                {
                    strshowVisitDate = "1";
                    strVisitDate = txtVisitDate.Text.ToString();
                    if (chkcompVisitDate.Checked == true)
                    {
                        strcompVisitDate = "1";
                    }
                    else
                    {
                        strcompVisitDate = "0";
                    }

                    if (chksumVisitDate.Checked == true)
                    {
                        strsumVisitDate = "1";
                    }
                    else
                    {
                        strsumVisitDate = "0";
                    }
                }
                if (chkshowVisitTime.Checked == true)
                {
                    strshowVisitTime = "1";
                    strVisitTime = txtVisitTime.Text.ToString();
                    if (chkcompVisitTime.Checked == true)
                    {
                        strcompVisitTime = "1";
                    }
                    else
                    {
                        strcompVisitTime = "0";
                    }

                    if (chksumVisitTime.Checked == true)
                    {
                        strsumVisitTime = "1";
                    }
                    else
                    {
                        strsumVisitTime = "0";
                    }
                }
                if (chkshowPassword.Checked == true)
                {
                    strshowPassword = "1";
                    strPassword = txtPassword.Text.ToString();
                    if (chkcompPassword.Checked == true)
                    {
                        strcompPassword = "1";
                    }
                    else
                    {
                        strcompPassword = "0";
                    }

                    if (chksumPassword.Checked == true)
                    {
                        strsumPassword = "1";
                    }
                    else
                    {
                        strsumPassword = "0";
                    }
                }
                if (chkshowGender.Checked == true)
                {
                    strshowGender = "1";
                    strGender = txtGender.Text.ToString();
                    if (chkcompGender.Checked == true)
                    {
                        strcompGender = "1";
                    }
                    else
                    {
                        strcompGender = "0";
                    }

                    if (chksumGender.Checked == true)
                    {
                        strsumGender = "1";
                    }
                    else
                    {
                        strsumGender = "0";
                    }
                }
                if (chkshowDOB.Checked == true)
                {
                    strshowDOB = "1";
                    strDOB = txtDOB.Text.ToString();
                    if (chkcompDOB.Checked == true)
                    {
                        strcompDOB = "1";
                    }
                    else
                    {
                        strcompDOB = "0";
                    }

                    if (chksumDOB.Checked == true)
                    {
                        strsumDOB = "1";
                    }
                    else
                    {
                        strsumDOB = "0";
                    }
                }
                if (chkshowAge.Checked == true)
                {
                    strshowAge = "1";
                    strAge = txtAge.Text.ToString();
                    if (chkcompAge.Checked == true)
                    {
                        strcompAge = "1";
                    }
                    else
                    {
                        strcompAge = "0";
                    }

                    if (chksumAge.Checked == true)
                    {
                        strsumAge = "1";
                    }
                    else
                    {
                        strsumAge = "0";
                    }
                }
                if (chkshowAdditional4.Checked == true)
                {
                    strshowAdditional4 = "1";
                    strAdditional4 = txtAdditional4.Text.ToString();
                    if (chkcompAdditional4.Checked == true)
                    {
                        strcompAdditional4 = "1";
                    }
                    else
                    {
                        strcompAdditional4 = "0";
                    }

                    if (chksumAdditional4.Checked == true)
                    {
                        strsumAdditional4 = "1";
                    }
                    else
                    {
                        strsumAdditional4 = "0";
                    }
                }
                if (chkshowAdditional5.Checked == true)
                {
                    strshowAdditional5 = "1";
                    strAdditional5 = txtAdditional5.Text.ToString();
                    if (chkcompAdditional5.Checked == true)
                    {
                        strcompAdditional5 = "1";
                    }
                    else
                    {
                        strcompAdditional5 = "0";
                    }

                    if (chksumAdditional5.Checked == true)
                    {
                        strsumAdditional5 = "1";
                    }
                    else
                    {
                        strsumAdditional5 = "0";
                    }
                }
                if (chkshowOtherSalutation.Checked == true)
                {
                    strshowOtherSalutation = "1";
                    strOtherSalutation = txtOtherSalutation.Text.ToString();
                    if (chkcompOtherSalutation.Checked == true)
                    {
                        strcompOtherSalutation = "1";
                    }
                    else
                    {
                        strcompOtherSalutation = "0";
                    }

                    if (chksumOtherSalutation.Checked == true)
                    {
                        strsumOtherSalutation = "1";
                    }
                    else
                    {
                        strsumOtherSalutation = "0";
                    }
                }
                if (chkshowOtherDesignation.Checked == true)
                {
                    strshowOtherDesignation = "1";
                    strOtherDesignation = txtOtherDesignation.Text.ToString();
                    if (chkcompOtherDesignation.Checked == true)
                    {
                        strcompOtherDesignation = "1";
                    }
                    else
                    {
                        strcompOtherDesignation = "0";
                    }

                    if (chksumOtherDesignation.Checked == true)
                    {
                        strsumOtherDesignation = "1";
                    }
                    else
                    {
                        strsumOtherDesignation = "0";
                    }
                }
                if (chkshowOtherIndustry.Checked == true)
                {
                    strshowOtherIndustry = "1";
                    strOtherIndustry = txtOtherIndustry.Text.ToString();
                    if (chkcompOtherIndustry.Checked == true)
                    {
                        strcompOtherIndustry = "1";
                    }
                    else
                    {
                        strcompOtherIndustry = "0";
                    }

                    if (chksumOtherIndustry.Checked == true)
                    {
                        strsumOtherIndustry = "1";
                    }
                    else
                    {
                        strsumOtherIndustry = "0";
                    }
                }
                #endregion

                string sql1 = "Update tb_Form Set form_input_isshow=" + strshowSal + ",form_input_isrequired=" + strcompSal + ",form_input_text=N'" + strSal + "',form_input_active=" + strsumSal + " Where form_input_name='" + _Salutation + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFname + ",form_input_isrequired=" + strcompFname + ",form_input_text=N'" + strFname + "',form_input_active=" + strsumFname + " Where form_input_name='" + _Fname + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowLname + ",form_input_isrequired=" + strcompLname + ",form_input_text=N'" + strLname + "',form_input_active=" + strsumLname + " Where form_input_name='" + _Lname + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDesignation + ",form_input_isrequired=" + strcompDesignation + ",form_input_text=N'" + strDesignation + "',form_input_active=" + strsumDesignation + " Where form_input_name='" + _Designation + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDepartment + ",form_input_isrequired=" + strcompDepartment + ",form_input_text=N'" + strDepartment + "',form_input_active=" + strsumDepartment + " Where form_input_name='" + _Department + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCompany + ",form_input_isrequired=" + strcompCompany + ",form_input_text=N'" + strCompany + "',form_input_active=" + strsumCompany + " Where form_input_name='" + _Company + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowIndustry + ",form_input_isrequired=" + strcompIndustry + ",form_input_text=N'" + strIndustry + "',form_input_active=" + strsumIndustry + " Where form_input_name='" + _Industry + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress1 + ",form_input_isrequired=" + strcompAddress1 + ",form_input_text=N'" + strAddress1 + "',form_input_active=" + strsumAddress1 + " Where form_input_name='" + _Address1 + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress2 + ",form_input_isrequired=" + strcompAddress2 + ",form_input_text=N'" + strAddress2 + "',form_input_active=" + strsumAddress2 + " Where form_input_name='" + _Address2 + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress3 + ",form_input_isrequired=" + strcompAddress3 + ",form_input_text=N'" + strAddress3 + "',form_input_active=" + strsumAddress3 + " Where form_input_name='" + _Address3 + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCity + ",form_input_isrequired=" + strcompCity + ",form_input_text=N'" + strCity + "',form_input_active=" + strsumCity + " Where form_input_name='" + _City + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowState + ",form_input_isrequired=" + strcompState + ",form_input_text=N'" + strState + "',form_input_active=" + strsumState + " Where form_input_name='" + _State + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowPostalCode + ",form_input_isrequired=" + strcompPostalCode + ",form_input_text=N'" + strPostalCode + "',form_input_active=" + strsumPostalCode + " Where form_input_name='" + _PostalCode + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCountry + ",form_input_isrequired=" + strcompCountry + ",form_input_text=N'" + strCountry + "',form_input_active=" + strsumCountry + " Where form_input_name='" + _Country + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowRCountry + ",form_input_isrequired=" + strcompRCountry + ",form_input_text=N'" + strRCountry + "',form_input_active=" + strsumRCountry + " Where form_input_name='" + _RCountry + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelCC + ",form_input_isrequired=" + strcompTelCC + ",form_input_text=N'" + strTelCC + "',form_input_active=" + strsumTelCC + " Where form_input_name='" + _Telcc + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelAC + ",form_input_isrequired=" + strcompTelAC + ",form_input_text=N'" + strTelAC + "',form_input_active=" + strsumTelAC + " Where form_input_name='" + _Telac + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTel + ",form_input_isrequired=" + strcompTel + ",form_input_text=N'" + strTel + "',form_input_active=" + strsumTel + " Where form_input_name='" + _Tel + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobileCC + ",form_input_isrequired=" + strcompMobileCC + ",form_input_text=N'" + strMobileCC + "',form_input_active=" + strsumMobileCC + " Where form_input_name='" + _Mobilecc + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobileAC + ",form_input_isrequired=" + strcompMobileAC + ",form_input_text=N'" + strMobileAC + "',form_input_active=" + strsumMobileAC + " Where form_input_name='" + _Mobileac + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobile + ",form_input_isrequired=" + strcompMobile + ",form_input_text=N'" + strMobile + "',form_input_active=" + strsumMobile + " Where form_input_name='" + _Mobile + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxCC + ",form_input_isrequired=" + strcompFaxCC + ",form_input_text=N'" + strFaxCC + "',form_input_active=" + strsumFaxCC + " Where form_input_name='" + _Faxcc + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxAC + ",form_input_isrequired=" + strcompFaxAC + ",form_input_text=N'" + strFaxAC + "',form_input_active=" + strsumFaxAC + " Where form_input_name='" + _Faxac + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFax + ",form_input_isrequired=" + strcompFax + ",form_input_text=N'" + strFax + "',form_input_active=" + strsumFax + " Where form_input_name='" + _Fax + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmail + ",form_input_isrequired=" + strcompEmail + ",form_input_text=N'" + strEmail + "',form_input_active=" + strsumEmail + " Where form_input_name='" + _Email + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmailConfirmation + ",form_input_isrequired=" + strcompEmailConfirmation + ",form_input_text=N'" + strEmailConfirmation + "',form_input_active=" + strsumEmailConfirmation + " Where form_input_name='" + _EmailConfirmation + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVisitDate + ",form_input_isrequired=" + strcompVisitDate + ",form_input_text=N'" + strVisitDate + "',form_input_active=" + strsumVisitDate + " Where form_input_name='" + _VisitDate + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVisitTime + ",form_input_isrequired=" + strcompVisitTime + ",form_input_text=N'" + strVisitTime + "',form_input_active=" + strsumVisitTime + " Where form_input_name='" + _VisitTime + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowPassword + ",form_input_isrequired=" + strcompPassword + ",form_input_text=N'" + strPassword + "',form_input_active=" + strsumPassword + " Where form_input_name='" + _Password + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowGender + ",form_input_isrequired=" + strcompGender + ",form_input_text=N'" + strGender + "',form_input_active=" + strsumGender + " Where form_input_name='" + _Gender + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDOB + ",form_input_isrequired=" + strcompDOB + ",form_input_text=N'" + strDOB + "',form_input_active=" + strsumDOB + " Where form_input_name='" + _DOB + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAge + ",form_input_isrequired=" + strcompAge + ",form_input_text=N'" + strAge + "',form_input_active=" + strsumAge + " Where form_input_name='" + _Age + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional4 + ",form_input_isrequired=" + strcompAdditional4 + ",form_input_text=N'" + strAdditional4 + "',form_input_active=" + strsumAdditional4 + " Where form_input_name='" + _Additional4 + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional5 + ",form_input_isrequired=" + strcompAdditional5 + ",form_input_text=N'" + strAdditional5 + "',form_input_active=" + strsumAdditional5 + " Where form_input_name='" + _Additional5 + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherSalutation + ",form_input_isrequired=" + strcompOtherSalutation + ",form_input_text=N'" + strOtherSalutation + "',form_input_active=" + strsumOtherSalutation + " Where form_input_name='" + _OtherSalutation + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherDesignation + ",form_input_isrequired=" + strcompOtherDesignation + ",form_input_text=N'" + strOtherDesignation + "',form_input_active=" + strsumOtherDesignation + " Where form_input_name='" + _OtherDesignation + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherIndustry + ",form_input_isrequired=" + strcompOtherIndustry + ",form_input_text=N'" + strOtherIndustry + "',form_input_active=" + strsumOtherIndustry + " Where form_input_name='" + _OtherIndustry + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";

                fn.ExecuteSQL(sql1);

                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-success alert-dismissible");
                lblMsg.Text = "Success.";

                setDynamicForm(flowid, showid);

                Dictionary<string, string> nValues = new Dictionary<string, string>();
                FlowControler fControl = new FlowControler(fn);
                string curstep = cFUnz.DecryptValue(Request.QueryString["STP"].ToString());
                nValues = fControl.GetAdminNextRoute(flowid, curstep);
                if (nValues.Count > 0)
                {
                    //*Update Currenct Flow Step Status
                    Flow flw = new Flow();
                    flw.FlowID = flowid;
                    flw.FlowConfigStatus = YesOrNo.Yes;
                    flw.FlowStep = curstep;
                    fControl.UpdateFlowIndivConfigStatus(flw);
                    //*

                    string path = string.Empty;
                    if (Request.Params["a"] != null)
                    {
                        string admintype = cFUnz.DecryptValue(Request.QueryString["a"].ToString());
                        if (admintype == BackendStaticValueClass.isFlowEdit)
                        {
                            string page = "Event_Flow_Dashboard.aspx";

                            path = fControl.MakeFullURL(page, flowid, showid);
                        }
                    }
                    else
                    {
                        string page = nValues["nURL"].ToString();
                        string step = nValues["nStep"].ToString();
                        string FlowID = nValues["FlowID"].ToString();

                        if (page == "") page = "Event_Config_Final";

                        path = fControl.MakeFullURL(page, FlowID, showid, "", step, null, "G");
                    }

                    if (path == "") path = "404.aspx";
                    Response.Redirect(path);
                }
            }
            catch (Exception ex)
            {
                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-danger alert-dismissible");
                lblMsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region InsertNewForm and using tb_Form & Insert new blank records but form_input_name, form_control_id, formType, ShowID into tb_Form table
    protected void InsertNewForm(string flowid, string showid)
    {
        try
        {
            int isShow = 0;
            int isRequired = 0;
            int isActive = 0;
            string inputText = string.Empty;
            string formType = "G";

            DataTable dt = fn.GetDatasetByCommand("Select * From tmp_Form Where form_type='" + FormType.TypeGroup + "'", "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string formname = dr["form_input_name"].ToString();
                    string controlid = dr["form_control_id"].ToString();
                    string orderno = dr["form_input_order"].ToString();

                    string query = "Select * From tb_Form Where FlowID='" + flowid + "' And ShowID=@SHWID And form_input_name='" + formname + "' And form_type='" + FormType.TypeGroup + "'";
                    List<SqlParameter> pList = new List<SqlParameter>();
                    SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    spar.Value = showid;
                    pList.Add(spar);

                    DataTable dtForm = fn.GetDatasetByCommand(query, "dsForm", pList).Tables[0];
                    if (dtForm.Rows.Count == 0)
                    {
                        string sql = string.Format("Insert Into tb_Form (form_input_name,form_control_id,form_input_isshow,form_input_isrequired,form_input_active,"
                                    + "form_input_order,form_input_text,form_type,ShowID,FlowID)"
                                    + " Values('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', '{9}')", formname, controlid, isShow, isRequired, isActive,
                                        orderno, inputText, formType, showid, flowid);

                        fn.ExecuteSQL(sql);
                    }
                }
            }
            setDynamicForm(flowid, showid);
        }
        catch (Exception ex)
        {
            divMsg.Visible = true;
            divMsg.Attributes.Remove("class");
            divMsg.Attributes.Add("class", "alert alert-danger alert-dismissible");
            lblMsg.Text = "Error occur: " + ex.Message;
        }
    }
    #endregion

    #region btnSavePreview_Click
    protected void btnSavePreview_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = cFUnz.DecryptValue(Request.QueryString["SHW"].ToString());
            string flowid = cFUnz.DecryptValue(Request.QueryString["FLW"].ToString());

            try
            {
                #region Declaration
                string strshowSal = "0";
                string strcompSal = "0";
                string strSal = string.Empty;
                string strsumSal = "0";

                string strshowFname = "0";
                string strcompFname = "0";
                string strFname = string.Empty;
                string strsumFname = "0";

                string strshowLname = "0";
                string strcompLname = "0";
                string strLname = string.Empty;
                string strsumLname = "0";

                string strshowDesignation = "0";
                string strcompDesignation = "0";
                string strDesignation = string.Empty;
                string strsumDesignation = "0";

                string strshowDepartment = "0";
                string strcompDepartment = "0";
                string strDepartment = string.Empty;
                string strsumDepartment = "0";

                string strshowCompany = "0";
                string strcompCompany = "0";
                string strCompany = string.Empty;
                string strsumCompany = "0";

                string strshowIndustry = "0";
                string strcompIndustry = "0";
                string strIndustry = string.Empty;
                string strsumIndustry = "0";

                string strshowAddress1 = "0";
                string strcompAddress1 = "0";
                string strAddress1 = string.Empty;
                string strsumAddress1 = "0";

                string strshowAddress2 = "0";
                string strcompAddress2 = "0";
                string strAddress2 = string.Empty;
                string strsumAddress2 = "0";

                string strshowAddress3 = "0";
                string strcompAddress3 = "0";
                string strAddress3 = string.Empty;
                string strsumAddress3 = "0";

                string strshowCity = "0";
                string strcompCity = "0";
                string strCity = string.Empty;
                string strsumCity = "0";

                string strshowState = "0";
                string strcompState = "0";
                string strState = string.Empty;
                string strsumState = "0";

                string strshowPostalCode = "0";
                string strcompPostalCode = "0";
                string strPostalCode = string.Empty;
                string strsumPostalCode = "0";

                string strshowCountry = "0";
                string strcompCountry = "0";
                string strCountry = string.Empty;
                string strsumCountry = "0";

                string strshowRCountry = "0";
                string strcompRCountry = "0";
                string strRCountry = string.Empty;
                string strsumRCountry = "0";

                string strshowTelCC = "0";
                string strcompTelCC = "0";
                string strTelCC = string.Empty;
                string strsumTelCC = "0";

                string strshowTelAC = "0";
                string strcompTelAC = "0";
                string strTelAC = string.Empty;
                string strsumTelAC = "0";

                string strshowTel = "0";
                string strcompTel = "0";
                string strTel = string.Empty;
                string strsumTel = "0";

                string strshowMobileCC = "0";
                string strcompMobileCC = "0";
                string strMobileCC = string.Empty;
                string strsumMobileCC = "0";

                string strshowMobileAC = "0";
                string strcompMobileAC = "0";
                string strMobileAC = string.Empty;
                string strsumMobileAC = "0";

                string strshowMobile = "0";
                string strcompMobile = "0";
                string strMobile = string.Empty;
                string strsumMobile = "0";

                string strshowFaxCC = "0";
                string strcompFaxCC = "0";
                string strFaxCC = string.Empty;
                string strsumFaxCC = "0";

                string strshowFaxAC = "0";
                string strcompFaxAC = "0";
                string strFaxAC = string.Empty;
                string strsumFaxAC = "0";

                string strshowFax = "0";
                string strcompFax = "0";
                string strFax = string.Empty;
                string strsumFax = "0";

                string strshowEmail = "0";
                string strcompEmail = "0";
                string strEmail = string.Empty;
                string strsumEmail = "0";

                string strshowEmailConfirmation = "0";
                string strcompEmailConfirmation = "0";
                string strEmailConfirmation = string.Empty;
                string strsumEmailConfirmation = "0";

                string strshowVisitDate = "0";
                string strcompVisitDate = "0";
                string strVisitDate = string.Empty;
                string strsumVisitDate = "0";

                string strshowVisitTime = "0";
                string strcompVisitTime = "0";
                string strVisitTime = string.Empty;
                string strsumVisitTime = "0";

                string strshowPassword = "0";
                string strcompPassword = "0";
                string strPassword = string.Empty;
                string strsumPassword = "0";

                string strshowGender = "0";
                string strcompGender = "0";
                string strGender = string.Empty;
                string strsumGender = "0";

                string strshowDOB = "0";
                string strcompDOB = "0";
                string strDOB = string.Empty;
                string strsumDOB = "0";

                string strshowAge = "0";
                string strcompAge = "0";
                string strAge = string.Empty;
                string strsumAge = "0";

                string strshowAdditional4 = "0";
                string strcompAdditional4 = "0";
                string strAdditional4 = string.Empty;
                string strsumAdditional4 = "0";

                string strshowAdditional5 = "0";
                string strcompAdditional5 = "0";
                string strAdditional5 = string.Empty;
                string strsumAdditional5 = "0";

                string strshowOtherSalutation = "0";
                string strcompOtherSalutation = "0";
                string strOtherSalutation = string.Empty;
                string strsumOtherSalutation = "0";

                string strshowOtherDesignation = "0";
                string strcompOtherDesignation = "0";
                string strOtherDesignation = string.Empty;
                string strsumOtherDesignation = "0";

                string strshowOtherIndustry = "0";
                string strcompOtherIndustry = "0";
                string strOtherIndustry = string.Empty;
                string strsumOtherIndustry = "0";
                #endregion

                #region set variable values
                if (chkshowSal.Checked == true)
                {
                    strshowSal = "1";
                    strSal = txtSal.Text.ToString();
                    if (chkcompSal.Checked == true)
                    {
                        strcompSal = "1";
                    }
                    else
                    {
                        strcompSal = "0";
                    }

                    if (chksumSal.Checked == true)
                    {
                        strsumSal = "1";
                    }
                    else
                    {
                        strsumSal = "0";
                    }
                }
                if (chkshowFname.Checked == true)
                {
                    strshowFname = "1";
                    strFname = txtFname.Text.ToString();
                    if (chkcompFname.Checked == true)
                    {
                        strcompFname = "1";
                    }
                    else
                    {
                        strcompFname = "0";
                    }

                    if (chksumFname.Checked == true)
                    {
                        strsumFname = "1";
                    }
                    else
                    {
                        strsumFname = "0";
                    }
                }
                if (chkshowLname.Checked == true)
                {
                    strshowLname = "1";
                    strLname = txtLname.Text.ToString();
                    if (chkcompLname.Checked == true)
                    {
                        strcompLname = "1";
                    }
                    else
                    {
                        strcompLname = "0";
                    }

                    if (chksumLname.Checked == true)
                    {
                        strsumLname = "1";
                    }
                    else
                    {
                        strsumLname = "0";
                    }
                }

                if (chkshowDesignation.Checked == true)
                {
                    strshowDesignation = "1";
                    strDesignation = txtDesignation.Text.ToString();
                    if (chkcompDesignation.Checked == true)
                    {
                        strcompDesignation = "1";
                    }
                    else
                    {
                        strcompDesignation = "0";
                    }

                    if (chksumDesignation.Checked == true)
                    {
                        strsumDesignation = "1";
                    }
                    else
                    {
                        strsumDesignation = "0";
                    }
                }

                if (chkshowDepartment.Checked == true)
                {
                    strshowDepartment = "1";
                    strDepartment = txtDepartment.Text.ToString();
                    if (chkcompDepartment.Checked == true)
                    {
                        strcompDepartment = "1";
                    }
                    else
                    {
                        strcompDepartment = "0";
                    }

                    if (chksumDepartment.Checked == true)
                    {
                        strsumDepartment = "1";
                    }
                    else
                    {
                        strsumDepartment = "0";
                    }
                }

                if (chkshowCompany.Checked == true)
                {
                    strshowCompany = "1";
                    strCompany = txtCompany.Text.ToString();
                    if (chkcompCompany.Checked == true)
                    {
                        strcompCompany = "1";
                    }
                    else
                    {
                        strcompCompany = "0";
                    }

                    if (chksumCompany.Checked == true)
                    {
                        strsumCompany = "1";
                    }
                    else
                    {
                        strsumCompany = "0";
                    }
                }

                if (chkshowIndustry.Checked == true)
                {
                    strshowIndustry = "1";
                    strIndustry = txtIndustry.Text.ToString();
                    if (chkcompIndustry.Checked == true)
                    {
                        strcompIndustry = "1";
                    }
                    else
                    {
                        strcompIndustry = "0";
                    }

                    if (chksumIndustry.Checked == true)
                    {
                        strsumIndustry = "1";
                    }
                    else
                    {
                        strsumIndustry = "0";
                    }
                }

                if (chkshowAddress1.Checked == true)
                {
                    strshowAddress1 = "1";
                    strAddress1 = txtAddress1.Text.ToString();
                    if (chkcompAddress1.Checked == true)
                    {
                        strcompAddress1 = "1";
                    }
                    else
                    {
                        strcompAddress1 = "0";
                    }

                    if (chksumAddress1.Checked == true)
                    {
                        strsumAddress1 = "1";
                    }
                    else
                    {
                        strsumAddress1 = "0";
                    }
                }

                if (chkshowAddress2.Checked == true)
                {
                    strshowAddress2 = "1";
                    strAddress2 = txtAddress2.Text.ToString();
                    if (chkcompAddress2.Checked == true)
                    {
                        strcompAddress2 = "1";
                    }
                    else
                    {
                        strcompAddress2 = "0";
                    }

                    if (chksumAddress2.Checked == true)
                    {
                        strsumAddress2 = "1";
                    }
                    else
                    {
                        strsumAddress2 = "0";
                    }
                }

                if (chkshowAddress3.Checked == true)
                {
                    strshowAddress3 = "1";
                    strAddress3 = txtAddress3.Text.ToString();
                    if (chkcompAddress3.Checked == true)
                    {
                        strcompAddress3 = "1";
                    }
                    else
                    {
                        strcompAddress3 = "0";
                    }

                    if (chksumAddress3.Checked == true)
                    {
                        strsumAddress3 = "1";
                    }
                    else
                    {
                        strsumAddress3 = "0";
                    }
                }

                if (chkshowCity.Checked == true)
                {
                    strshowCity = "1";
                    strCity = txtCity.Text.ToString();
                    if (chkcompCity.Checked == true)
                    {
                        strcompCity = "1";
                    }
                    else
                    {
                        strcompCity = "0";
                    }

                    if (chksumCity.Checked == true)
                    {
                        strsumCity = "1";
                    }
                    else
                    {
                        strsumCity = "0";
                    }
                }
                if (chkshowState.Checked == true)
                {
                    strshowState = "1";
                    strState = txtState.Text.ToString();
                    if (chkcompState.Checked == true)
                    {
                        strcompState = "1";
                    }
                    else
                    {
                        strcompState = "0";
                    }

                    if (chksumState.Checked == true)
                    {
                        strsumState = "1";
                    }
                    else
                    {
                        strsumState = "0";
                    }
                }
                if (chkshowPostalCode.Checked == true)
                {
                    strshowPostalCode = "1";
                    strPostalCode = txtPostalCode.Text.ToString();
                    if (chkcompPostalCode.Checked == true)
                    {
                        strcompPostalCode = "1";
                    }
                    else
                    {
                        strcompPostalCode = "0";
                    }

                    if (chksumPostalCode.Checked == true)
                    {
                        strsumPostalCode = "1";
                    }
                    else
                    {
                        strsumPostalCode = "0";
                    }
                }

                if (chkshowCountry.Checked == true)
                {
                    strshowCountry = "1";
                    strCountry = txtCountry.Text.ToString();
                    if (chkcompCountry.Checked == true)
                    {
                        strcompCountry = "1";
                    }
                    else
                    {
                        strcompCountry = "0";
                    }

                    if (chksumCountry.Checked == true)
                    {
                        strsumCountry = "1";
                    }
                    else
                    {
                        strsumCountry = "0";
                    }
                }

                if (chkshowRCountry.Checked == true)
                {
                    strshowRCountry = "1";
                    strRCountry = txtRCountry.Text.ToString();
                    if (chkcompRCountry.Checked == true)
                    {
                        strcompRCountry = "1";
                    }
                    else
                    {
                        strcompRCountry = "0";
                    }

                    if (chksumRCountry.Checked == true)
                    {
                        strsumRCountry = "1";
                    }
                    else
                    {
                        strsumRCountry = "0";
                    }
                }

                if (chkshowTelCC.Checked == true)
                {
                    strshowTelCC = "1";
                    strTelCC = txtTelCC.Text.ToString();
                    if (chkcompTelCC.Checked == true)
                    {
                        strcompTelCC = "1";
                    }
                    else
                    {
                        strcompTelCC = "0";
                    }

                    if (chksumTelCC.Checked == true)
                    {
                        strsumTelCC = "1";
                    }
                    else
                    {
                        strsumTelCC = "0";
                    }
                }

                if (chkshowTelAC.Checked == true)
                {
                    strshowTelAC = "1";
                    strTelAC = txtTelAC.Text.ToString();
                    if (chkcompTelAC.Checked == true)
                    {
                        strcompTelAC = "1";
                    }
                    else
                    {
                        strcompTelAC = "0";
                    }

                    if (chksumTelAC.Checked == true)
                    {
                        strsumTelAC = "1";
                    }
                    else
                    {
                        strsumTelAC = "0";
                    }
                }

                if (chkshowTel.Checked == true)
                {
                    strshowTel = "1";
                    strTel = txtTel.Text.ToString();
                    if (chkcompTel.Checked == true)
                    {
                        strcompTel = "1";
                    }
                    else
                    {
                        strcompTel = "0";
                    }

                    if (chksumTel.Checked == true)
                    {
                        strsumTel = "1";
                    }
                    else
                    {
                        strsumTel = "0";
                    }
                }

                if (chkshowMobileCC.Checked == true)
                {
                    strshowMobileCC = "1";
                    strMobileCC = txtMobileCC.Text.ToString();
                    if (chkcompMobileCC.Checked == true)
                    {
                        strcompMobileCC = "1";
                    }
                    else
                    {
                        strcompMobileCC = "0";
                    }

                    if (chksumMobileCC.Checked == true)
                    {
                        strsumMobileCC = "1";
                    }
                    else
                    {
                        strsumMobileCC = "0";
                    }
                }

                if (chkshowMobileAC.Checked == true)
                {
                    strshowMobileAC = "1";
                    strMobileAC = txtMobileAC.Text.ToString();
                    if (chkcompMobileAC.Checked == true)
                    {
                        strcompMobileAC = "1";
                    }
                    else
                    {
                        strcompMobileAC = "0";
                    }

                    if (chksumMobileAC.Checked == true)
                    {
                        strsumMobileAC = "1";
                    }
                    else
                    {
                        strsumMobileAC = "0";
                    }
                }

                if (chkshowMobile.Checked == true)
                {
                    strshowMobile = "1";
                    strMobile = txtMobile.Text.ToString();
                    if (chkcompMobile.Checked == true)
                    {
                        strcompMobile = "1";
                    }
                    else
                    {
                        strcompMobile = "0";
                    }

                    if (chksumMobile.Checked == true)
                    {
                        strsumMobile = "1";
                    }
                    else
                    {
                        strsumMobile = "0";
                    }
                }
                if (chkshowFaxCC.Checked == true)
                {
                    strshowFaxCC = "1";
                    strFaxCC = txtFaxCC.Text.ToString();
                    if (chkcompFaxCC.Checked == true)
                    {
                        strcompFaxCC = "1";
                    }
                    else
                    {
                        strcompFaxCC = "0";
                    }

                    if (chksumFaxCC.Checked == true)
                    {
                        strsumFaxCC = "1";
                    }
                    else
                    {
                        strsumFaxCC = "0";
                    }
                }

                if (chkshowFaxAC.Checked == true)
                {
                    strshowFaxAC = "1";
                    strFaxAC = txtFaxAC.Text.ToString();
                    if (chkcompFaxAC.Checked == true)
                    {
                        strcompFaxAC = "1";
                    }
                    else
                    {
                        strcompFaxAC = "0";
                    }

                    if (chksumFaxAC.Checked == true)
                    {
                        strsumFaxAC = "1";
                    }
                    else
                    {
                        strsumFaxAC = "0";
                    }
                }

                if (chkshowFax.Checked == true)
                {
                    strshowFax = "1";
                    strFax = txtFax.Text.ToString();
                    if (chkcompFax.Checked == true)
                    {
                        strcompFax = "1";
                    }
                    else
                    {
                        strcompFax = "0";
                    }

                    if (chksumFax.Checked == true)
                    {
                        strsumFax = "1";
                    }
                    else
                    {
                        strsumFax = "0";
                    }
                }

                if (chkshowEmail.Checked == true)
                {
                    strshowEmail = "1";
                    strEmail = txtEmail.Text.ToString();
                    if (chkcompEmail.Checked == true)
                    {
                        strcompEmail = "1";
                    }
                    else
                    {
                        strcompEmail = "0";
                    }

                    if (chksumEmail.Checked == true)
                    {
                        strsumEmail = "1";
                    }
                    else
                    {
                        strsumEmail = "0";
                    }
                }

                if (chkshowEmailConfirmation.Checked == true)
                {
                    strshowEmailConfirmation = "1";
                    strEmailConfirmation = txtEmailConfirmation.Text.ToString();
                    if (chkcompEmailConfirmation.Checked == true)
                    {
                        strcompEmailConfirmation = "1";
                    }
                    else
                    {
                        strcompEmailConfirmation = "0";
                    }

                    if (chksumEmailConfirmation.Checked == true)
                    {
                        strsumEmailConfirmation = "1";
                    }
                    else
                    {
                        strsumEmailConfirmation = "0";
                    }
                }
                if (chkshowVisitDate.Checked == true)
                {
                    strshowVisitDate = "1";
                    strVisitDate = txtVisitDate.Text.ToString();
                    if (chkcompVisitDate.Checked == true)
                    {
                        strcompVisitDate = "1";
                    }
                    else
                    {
                        strcompVisitDate = "0";
                    }

                    if (chksumVisitDate.Checked == true)
                    {
                        strsumVisitDate = "1";
                    }
                    else
                    {
                        strsumVisitDate = "0";
                    }
                }
                if (chkshowVisitTime.Checked == true)
                {
                    strshowVisitTime = "1";
                    strVisitTime = txtVisitTime.Text.ToString();
                    if (chkcompVisitTime.Checked == true)
                    {
                        strcompVisitTime = "1";
                    }
                    else
                    {
                        strcompVisitTime = "0";
                    }

                    if (chksumVisitTime.Checked == true)
                    {
                        strsumVisitTime = "1";
                    }
                    else
                    {
                        strsumVisitTime = "0";
                    }
                }
                if (chkshowPassword.Checked == true)
                {
                    strshowPassword = "1";
                    strPassword = txtPassword.Text.ToString();
                    if (chkcompPassword.Checked == true)
                    {
                        strcompPassword = "1";
                    }
                    else
                    {
                        strcompPassword = "0";
                    }

                    if (chksumPassword.Checked == true)
                    {
                        strsumPassword = "1";
                    }
                    else
                    {
                        strsumPassword = "0";
                    }
                }
                if (chkshowGender.Checked == true)
                {
                    strshowGender = "1";
                    strGender = txtGender.Text.ToString();
                    if (chkcompGender.Checked == true)
                    {
                        strcompGender = "1";
                    }
                    else
                    {
                        strcompGender = "0";
                    }

                    if (chksumGender.Checked == true)
                    {
                        strsumGender = "1";
                    }
                    else
                    {
                        strsumGender = "0";
                    }
                }
                if (chkshowDOB.Checked == true)
                {
                    strshowDOB = "1";
                    strDOB = txtDOB.Text.ToString();
                    if (chkcompDOB.Checked == true)
                    {
                        strcompDOB = "1";
                    }
                    else
                    {
                        strcompDOB = "0";
                    }

                    if (chksumDOB.Checked == true)
                    {
                        strsumDOB = "1";
                    }
                    else
                    {
                        strsumDOB = "0";
                    }
                }
                if (chkshowAge.Checked == true)
                {
                    strshowAge = "1";
                    strAge = txtAge.Text.ToString();
                    if (chkcompAge.Checked == true)
                    {
                        strcompAge = "1";
                    }
                    else
                    {
                        strcompAge = "0";
                    }

                    if (chksumAge.Checked == true)
                    {
                        strsumAge = "1";
                    }
                    else
                    {
                        strsumAge = "0";
                    }
                }
                if (chkshowAdditional4.Checked == true)
                {
                    strshowAdditional4 = "1";
                    strAdditional4 = txtAdditional4.Text.ToString();
                    if (chkcompAdditional4.Checked == true)
                    {
                        strcompAdditional4 = "1";
                    }
                    else
                    {
                        strcompAdditional4 = "0";
                    }

                    if (chksumAdditional4.Checked == true)
                    {
                        strsumAdditional4 = "1";
                    }
                    else
                    {
                        strsumAdditional4 = "0";
                    }
                }
                if (chkshowAdditional5.Checked == true)
                {
                    strshowAdditional5 = "1";
                    strAdditional5 = txtAdditional5.Text.ToString();
                    if (chkcompAdditional5.Checked == true)
                    {
                        strcompAdditional5 = "1";
                    }
                    else
                    {
                        strcompAdditional5 = "0";
                    }

                    if (chksumAdditional5.Checked == true)
                    {
                        strsumAdditional5 = "1";
                    }
                    else
                    {
                        strsumAdditional5 = "0";
                    }
                }
                if (chkshowOtherSalutation.Checked == true)
                {
                    strshowOtherSalutation = "1";
                    strOtherSalutation = txtOtherSalutation.Text.ToString();
                    if (chkcompOtherSalutation.Checked == true)
                    {
                        strcompOtherSalutation = "1";
                    }
                    else
                    {
                        strcompOtherSalutation = "0";
                    }

                    if (chksumOtherSalutation.Checked == true)
                    {
                        strsumOtherSalutation = "1";
                    }
                    else
                    {
                        strsumOtherSalutation = "0";
                    }
                }
                if (chkshowOtherDesignation.Checked == true)
                {
                    strshowOtherDesignation = "1";
                    strOtherDesignation = txtOtherDesignation.Text.ToString();
                    if (chkcompOtherDesignation.Checked == true)
                    {
                        strcompOtherDesignation = "1";
                    }
                    else
                    {
                        strcompOtherDesignation = "0";
                    }

                    if (chksumOtherDesignation.Checked == true)
                    {
                        strsumOtherDesignation = "1";
                    }
                    else
                    {
                        strsumOtherDesignation = "0";
                    }
                }
                if (chkshowOtherIndustry.Checked == true)
                {
                    strshowOtherIndustry = "1";
                    strOtherIndustry = txtOtherIndustry.Text.ToString();
                    if (chkcompOtherIndustry.Checked == true)
                    {
                        strcompOtherIndustry = "1";
                    }
                    else
                    {
                        strcompOtherIndustry = "0";
                    }

                    if (chksumOtherIndustry.Checked == true)
                    {
                        strsumOtherIndustry = "1";
                    }
                    else
                    {
                        strsumOtherIndustry = "0";
                    }
                }
                #endregion

                string sql1 = "Update tb_Form Set form_input_isshow=" + strshowSal + ",form_input_isrequired=" + strcompSal + ",form_input_text=N'" + strSal + "',form_input_active=" + strsumSal + " Where form_input_name='" + _Salutation + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFname + ",form_input_isrequired=" + strcompFname + ",form_input_text=N'" + strFname + "',form_input_active=" + strsumFname + " Where form_input_name='" + _Fname + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowLname + ",form_input_isrequired=" + strcompLname + ",form_input_text=N'" + strLname + "',form_input_active=" + strsumLname + " Where form_input_name='" + _Lname + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDesignation + ",form_input_isrequired=" + strcompDesignation + ",form_input_text=N'" + strDesignation + "',form_input_active=" + strsumDesignation + " Where form_input_name='" + _Designation + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDepartment + ",form_input_isrequired=" + strcompDepartment + ",form_input_text=N'" + strDepartment + "',form_input_active=" + strsumDepartment + " Where form_input_name='" + _Department + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCompany + ",form_input_isrequired=" + strcompCompany + ",form_input_text=N'" + strCompany + "',form_input_active=" + strsumCompany + " Where form_input_name='" + _Company + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowIndustry + ",form_input_isrequired=" + strcompIndustry + ",form_input_text=N'" + strIndustry + "',form_input_active=" + strsumIndustry + " Where form_input_name='" + _Industry + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress1 + ",form_input_isrequired=" + strcompAddress1 + ",form_input_text=N'" + strAddress1 + "',form_input_active=" + strsumAddress1 + " Where form_input_name='" + _Address1 + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress2 + ",form_input_isrequired=" + strcompAddress2 + ",form_input_text=N'" + strAddress2 + "',form_input_active=" + strsumAddress2 + " Where form_input_name='" + _Address2 + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress3 + ",form_input_isrequired=" + strcompAddress3 + ",form_input_text=N'" + strAddress3 + "',form_input_active=" + strsumAddress3 + " Where form_input_name='" + _Address3 + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCity + ",form_input_isrequired=" + strcompCity + ",form_input_text=N'" + strCity + "',form_input_active=" + strsumCity + " Where form_input_name='" + _City + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowState + ",form_input_isrequired=" + strcompState + ",form_input_text=N'" + strState + "',form_input_active=" + strsumState + " Where form_input_name='" + _State + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowPostalCode + ",form_input_isrequired=" + strcompPostalCode + ",form_input_text=N'" + strPostalCode + "',form_input_active=" + strsumPostalCode + " Where form_input_name='" + _PostalCode + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCountry + ",form_input_isrequired=" + strcompCountry + ",form_input_text=N'" + strCountry + "',form_input_active=" + strsumCountry + " Where form_input_name='" + _Country + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowRCountry + ",form_input_isrequired=" + strcompRCountry + ",form_input_text=N'" + strRCountry + "',form_input_active=" + strsumRCountry + " Where form_input_name='" + _RCountry + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelCC + ",form_input_isrequired=" + strcompTelCC + ",form_input_text=N'" + strTelCC + "',form_input_active=" + strsumTelCC + " Where form_input_name='" + _Telcc + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelAC + ",form_input_isrequired=" + strcompTelAC + ",form_input_text=N'" + strTelAC + "',form_input_active=" + strsumTelAC + " Where form_input_name='" + _Telac + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTel + ",form_input_isrequired=" + strcompTel + ",form_input_text=N'" + strTel + "',form_input_active=" + strsumTel + " Where form_input_name='" + _Tel + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobileCC + ",form_input_isrequired=" + strcompMobileCC + ",form_input_text=N'" + strMobileCC + "',form_input_active=" + strsumMobileCC + " Where form_input_name='" + _Mobilecc + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobileAC + ",form_input_isrequired=" + strcompMobileAC + ",form_input_text=N'" + strMobileAC + "',form_input_active=" + strsumMobileAC + " Where form_input_name='" + _Mobileac + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobile + ",form_input_isrequired=" + strcompMobile + ",form_input_text=N'" + strMobile + "',form_input_active=" + strsumMobile + " Where form_input_name='" + _Mobile + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxCC + ",form_input_isrequired=" + strcompFaxCC + ",form_input_text=N'" + strFaxCC + "',form_input_active=" + strsumFaxCC + " Where form_input_name='" + _Faxcc + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxAC + ",form_input_isrequired=" + strcompFaxAC + ",form_input_text=N'" + strFaxAC + "',form_input_active=" + strsumFaxAC + " Where form_input_name='" + _Faxac + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFax + ",form_input_isrequired=" + strcompFax + ",form_input_text=N'" + strFax + "',form_input_active=" + strsumFax + " Where form_input_name='" + _Fax + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmail + ",form_input_isrequired=" + strcompEmail + ",form_input_text=N'" + strEmail + "',form_input_active=" + strsumEmail + " Where form_input_name='" + _Email + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmailConfirmation + ",form_input_isrequired=" + strcompEmailConfirmation + ",form_input_text=N'" + strEmailConfirmation + "',form_input_active=" + strsumEmailConfirmation + " Where form_input_name='" + _EmailConfirmation + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVisitDate + ",form_input_isrequired=" + strcompVisitDate + ",form_input_text=N'" + strVisitDate + "',form_input_active=" + strsumVisitDate + " Where form_input_name='" + _VisitDate + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVisitTime + ",form_input_isrequired=" + strcompVisitTime + ",form_input_text=N'" + strVisitTime + "',form_input_active=" + strsumVisitTime + " Where form_input_name='" + _VisitTime + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowPassword + ",form_input_isrequired=" + strcompPassword + ",form_input_text=N'" + strPassword + "',form_input_active=" + strsumPassword + " Where form_input_name='" + _Password + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowGender + ",form_input_isrequired=" + strcompGender + ",form_input_text=N'" + strGender + "',form_input_active=" + strsumGender + " Where form_input_name='" + _Gender + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDOB + ",form_input_isrequired=" + strcompDOB + ",form_input_text=N'" + strDOB + "',form_input_active=" + strsumDOB + " Where form_input_name='" + _DOB + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAge + ",form_input_isrequired=" + strcompAge + ",form_input_text=N'" + strAge + "',form_input_active=" + strsumAge + " Where form_input_name='" + _Age + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional4 + ",form_input_isrequired=" + strcompAdditional4 + ",form_input_text=N'" + strAdditional4 + "',form_input_active=" + strsumAdditional4 + " Where form_input_name='" + _Additional4 + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional5 + ",form_input_isrequired=" + strcompAdditional5 + ",form_input_text=N'" + strAdditional5 + "',form_input_active=" + strsumAdditional5 + " Where form_input_name='" + _Additional5 + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherSalutation + ",form_input_isrequired=" + strcompOtherSalutation + ",form_input_text=N'" + strOtherSalutation + "',form_input_active=" + strsumOtherSalutation + " Where form_input_name='" + _OtherSalutation + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherDesignation + ",form_input_isrequired=" + strcompOtherDesignation + ",form_input_text=N'" + strOtherDesignation + "',form_input_active=" + strsumOtherDesignation + " Where form_input_name='" + _OtherDesignation + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherIndustry + ",form_input_isrequired=" + strcompOtherIndustry + ",form_input_text=N'" + strOtherIndustry + "',form_input_active=" + strsumOtherIndustry + " Where form_input_name='" + _OtherIndustry + "' And form_type='" + FormType.TypeGroup + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";

                fn.ExecuteSQL(sql1);

                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-success alert-dismissible");
                lblMsg.Text = "Success.";

                setDynamicForm(flowid, showid);

                FlowControler flwObj = new FlowControler(fn);
                Dictionary<string, string> nValues = new Dictionary<string, string>();
                nValues = flwObj.GetFrontendFormByBckForm(flowid, System.IO.Path.GetFileName(Request.Url.AbsolutePath));
                if (nValues.Count > 0)
                {
                    string page = nValues["page"].ToString();
                    string step = nValues["step"].ToString();
                    string route = flwObj.MakeFullURL(page, flowid, showid, null, step);
                    route = route + "&t=" + cFUnz.EncryptValue(BackendStaticValueClass.isAdmin);

                    System.Drawing.Rectangle resolution =
                    System.Windows.Forms.Screen.PrimaryScreen.Bounds;
                    int width = resolution.Width;
                    int height = resolution.Height;

                    if (width <= 1014)
                    {
                        width = width - 100;
                    }
                    else
                    {
                        width = 1000;
                    }

                    RadWindow newWindow = new RadWindow();
                    newWindow.NavigateUrl = route;
                    newWindow.Height = 800;
                    newWindow.Width = width;
                    newWindow.VisibleOnPageLoad = true;
                    newWindow.KeepInScreenBounds = true;
                    newWindow.VisibleOnPageLoad = true;
                    newWindow.Visible = true;
                    newWindow.VisibleStatusbar = false;
                    RadWindowManager1.Windows.Add(newWindow);
                }
            }
            catch (Exception ex)
            {
                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-danger alert-dismissible");
                lblMsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    protected void btnSetupEmail_Onclick(object sender, EventArgs e)
    {
        string url = "Event_Flow_EmailConfig?" + Request.QueryString;
        Response.Redirect(url);
    }

}