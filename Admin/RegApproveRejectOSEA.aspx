﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="RegApproveRejectOSEA.aspx.cs" Inherits="Admin_RegApproveRejectOSEA" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .red {
            color: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h3 class="box-title">Manage Individual (Delegate) Info</h3>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="form-horizontal">
                <div class="box-body">
                  <div class="form-group" runat="server" id="divRegno" Visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblRegno" runat="server" CssClass="form-control-label" Text="Registration No."></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:Label ID="txtRegno" runat="server" CssClass="form-control" Visible="false"></asp:Label>
                        </div>

                    </div>

                    <div class="form-group" runat="server" id="divFName">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblFName" runat="server" CssClass="form-control-label" Text="First Name"></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:Label ID="txtFName" runat="server" CssClass="form-control"></asp:Label>
                        </div>

                    </div>

                    <div class="form-group" runat="server" id="divLName">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblLName" runat="server" CssClass="form-control-label" Text="Family Name "></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:Label ID="txtLName" runat="server" CssClass="form-control"></asp:Label>
                        </div>

                    </div>
                    <div class="form-group" runat="server" id="divEmail">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text="Email Address"></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:Label ID="txtEmail" runat="server" CssClass="form-control"></asp:Label>
                        </div>

                    </div>
                   <div class="form-group" runat="server" id="div3">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="Label1" runat="server" CssClass="form-control-label" Text="Status"></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                <asp:ListItem Text="Please Select" Value="0"></asp:ListItem>
                                <%--<asp:ListItem Text="Rejected" Value="66"></asp:ListItem>--%>
                                <asp:ListItem Text="Declined" Value="88"></asp:ListItem>
                                <asp:ListItem Text="Confirmed" Value="11"></asp:ListItem>
                                <asp:ListItem Text="Approved" Value="22"></asp:ListItem>
                                <asp:ListItem Text="VIP" Value="33"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcStatus" runat="server" 
                            ControlToValidate="ddlStatus" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4"></div>
                        <div class="col-md-6">
                            <asp:Button runat="server" Text="Update" ID="btnupdate" OnClick="btnupdate_Click" />
                            <asp:HiddenField runat="server" ID="hfRegGroupID" Value=""></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfRegno" Value=""></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfcategoryID" Value=""></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfStage" Value=""></asp:HiddenField>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <br />
    <asp:Label CssClass="col-md-2 control-label" runat="server" ID="lbls"></asp:Label>
</asp:Content>

