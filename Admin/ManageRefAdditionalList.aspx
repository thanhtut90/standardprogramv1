﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageRefAdditionalList.aspx.cs" Inherits="Admin_ManageRefAdditionalList" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Manage Ref Additional List</h1>
            <span class="pagedesc">Manage the list of Ref Additional List.</span>
            <ul class="hornav">
                <li class="current"><a href="#index">Index</a></li>
            </ul>
        </div><!--page header -->
        <div id="contentwrapper" class="contentwrapper">
            <div id="index" class="subcontent">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h4>Add New Ref Additional List</h4>
                        <asp:Label ID="lblRefAddID" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblShowid" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblRefAdditionalFieldType" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblRefAddName" runat="server" Text="Name" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtRefAddName" runat="server" Width="350px"></asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtRefAddName" runat="server"
                            ErrorMessage="Please insert the name" ForeColor="Red"></asp:RequiredFieldValidator>
                        <br />

                        <asp:Label ID="lblSortOrder" runat="server" Text="Sort Order" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtSortorder" runat="server" Width="350px"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbSortorder" runat="server" TargetControlID="txtSortorder" FilterType="Numbers" ValidChars="0123456789" />
                        <br />

                        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                        <br /><br />

                        <asp:Panel ID="pnllist" runat="server" Visible="false">
                            <h3>Ref Additional List</h3>
                            <asp:GridView ID="gvList" runat="server" DataKeyNames="refAdd_id" 
                            OnRowDeleting="GridView1_RowDeleting"
                            OnRowEditing="GridView1_RowEditing"
                            OnRowUpdating="GridView1_RowUpdating"
                            OnRowCancelingEdit="GridView1_RowCancelingEdit" AutoGenerateColumns="false"
                            CssClass="table">
                                <HeaderStyle CssClass="theadstyle" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <%# Eval("refAdd_id")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("refAdd_name") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("refAdd_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Order">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtOrder" runat="server" Text='<%# Eval("refAdd_SortOrder") %>' Width="50px"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="ftbOrder" runat="server" TargetControlID="txtOrder" FilterType="Numbers" ValidChars="0123456789" />
                                            <asp:Label ID="lblno" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrder" runat="server" Text='<%# Eval("refAdd_SortOrder") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowEditButton="True" CausesValidation="false" />
                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" OnClientClick="return confirm ('Are you sure you want to delete this record?')" Text="Delete" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div style="color:Red"><asp:Label ID="lblMsg" runat="server"></asp:Label></div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div><!--page index -->
        </div><!--contentwrapper -->
    </div><!--centercontent -->
</asp:Content>