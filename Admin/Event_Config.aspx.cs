﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Utilities;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.BackendMaster;

public partial class Admin_Event_Config : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    private static string eventname = "eventname";
    private static string pagetitle = "pagetitle";
    private static string copyrigtht = "copyrigtht";
    private static string RegClosedDate = "RegClosedDate";
    private static string FromEmailAddress = "FromEmailAddress";
    private static string Currency = "Currency";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                ShowPanel();
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_Show table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        string extraQuery = string.Empty;
        string query = string.Empty;
        DataTable dt;

        try
        {
            ShowController shwCtr = new ShowController(fn);
            if (Session["roleid"].ToString() != Number.One)
            {
                dt = shwCtr.getActiveShowByUser(Session["userid"].ToString());
            }
            else
            {
                dt = shwCtr.getAllShows();
            }
            if (dt.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dt;
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string curid = "";
        string showName = "";
        try
        {
            foreach (GridDataItem item in GKeyMaster.SelectedItems)
            {
                curid = item.OwnerTableView.DataKeyValues[item.ItemIndex]["SHW_ID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["SHW_ID"].ToString() : "";
                showName = item.OwnerTableView.DataKeyValues[item.ItemIndex]["SHW_Name"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["SHW_Name"].ToString() : "";
            }
            if (e.CommandName == "Delete")
            {
                GridDataItem item = (GridDataItem)e.Item;

                ShowController shwCtr = new ShowController(fn);
                string curID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["SHW_ID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["SHW_ID"].ToString() : "";
                if (!string.IsNullOrEmpty(curID))
                {
                    ShowObj sObj = new ShowObj();
                    sObj.showid = curID;
                    bool isDeleted = shwCtr.DeleteShow(sObj);
                    if (isDeleted)
                    {
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already deleted.');", true);
                    }
                    else
                    {
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Deleting error.');", true);
                    }

                    ResetControls();
                    lbls.Text = "";
                    PanelKeyDetail.Visible = false;
                    btnAdd.Visible = false;
                    btnUpdateRecord.Visible = false;
                }
            }
            else
            {
                if (curid != "" && curid != null)
                {
                    btnUpdateRecord.Visible = false;
                    btnAdd.Visible = false;
                    PanelKeyDetail.Visible = false;

                    RadAjaxPanel1.Visible = true;
                    txtShowID.Text = curid;

                    try
                    {
                        GKeyMaster1.Rebind();
                        if (GKeyMaster1.DataSource != null && Session["roleid"].ToString() == Number.One)
                        {
                            divFlow.Visible = true;

                            LoadShowInfo(curid);
                        }
                        else if (GKeyMaster1.DataSource != null && Session["roleid"].ToString() == Number.Two)
                        {
                            divFlow.Visible = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Response.Redirect("Login.aspx");
                    }
                }
            }
        }
        catch { }
    }
    #endregion

    #region btnAddNew_Click (Load when click Add New button)
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        ResetControls();
        lbls.Text = "";
        btnUpdateRecord.Visible = false;
        btnAdd.Visible = true;
        PanelKeyDetail.Visible = true;

        RadAjaxPanel1.Visible = false;

        PanelKeyList.Visible = false;
        PanelCreate.Visible = false;
    }
    #endregion

    #region btnCancel_Click (Load when click Cancel button)
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ResetControls();
        lbls.Text = "";
        btnUpdateRecord.Visible = false;
        btnAdd.Visible = false;
        PanelKeyDetail.Visible = false;
        PanelCreate.Visible = true;
        PanelKeyList.Visible = true;
    }
    #endregion

    #region btnAdd_Click (Insert the respective data into tb_Show table)
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lbls.Text = "";
            string name = cFun.solveSQL(txtName.Text.Trim());
            string prefix = string.Empty;// cFun.solveSQL(txtPrefix.Text.Trim());
            string description = cFun.solveSQL(txtDescription.Text.Trim());
            string showimage = string.Empty;

            PanelKeyList.Visible = true;
            //if (!fupImage.HasFile)
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select the file.');", true);
            //    return;
            //}
            //else
            {
                //string filePath = "../Admin/images/Logo/";
                //string chkpath = Server.MapPath(filePath);
                //var directory = new DirectoryInfo(chkpath);
                //if (directory.Exists == false)
                //{
                //    directory.Create();
                //}
                //String fileExtension = System.IO.Path.GetExtension(fupImage.FileName).ToLower();
                //string filename = txtPrefix.Text.ToString().Trim() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() +
                //          DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + fileExtension;
                //fupImage.SaveAs(chkpath + filename);
                //showimage = filePath + filename;


                ShowController shwCtr = new ShowController(fn);
                try
                {
                    DataTable dtmaster = shwCtr.getShowByNamePrefix(name, prefix);

                    if (dtmaster.Rows.Count > 0)
                    {
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This show already exist.');", true);
                        return;
                    }
                    else
                    {
                        RunNumber run = new RunNumber(fn);
                        string runNumber = run.GetRunNUmber("SHW_KEY");
                        string showID = fn.CreateRandomCodeCharacter(3) + runNumber;

                        ShowObj sObj = new ShowObj();
                        sObj.showid = showID;
                        sObj.showname = name;
                        sObj.prefix = prefix;
                        sObj.logo = showimage;
                        sObj.description = description;
                        sObj.activeornot = RecordStatus.ACTIVE;
                        sObj.shwRefID = runNumber;
                        bool rowInserted = shwCtr.insertShow(sObj);
                        if (rowInserted)
                        {
                            GKeyMaster.Rebind();
                            btnUpdateRecord.Visible = false;
                            btnAdd.Visible = false;
                            PanelKeyDetail.Visible = false;
                            GKeyMaster.Rebind();
                            ResetControls();
                            lbls.Text = "";
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);

                            InsertNewSiteSettings(showID, name);

                            showID = cFun.EncryptValue(showID);
                            Response.Redirect("Event_Settings_MasterPage?SHW=" + showID);//Instead of Event_Settings_Master for now
                        }
                        else
                        {
                            GKeyMaster.Rebind();
                            btnUpdateRecord.Visible = false;
                            btnAdd.Visible = false;
                            PanelKeyDetail.Visible = false;
                            GKeyMaster.Rebind();
                            ResetControls();
                            lbls.Text = "";
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Saving error, try again.');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    lbls.Text = ex.Message;
                }
            }
        }
    }
    #endregion

    protected void btnAddFlow_Click(object sender, EventArgs e)
    {
        Response.Redirect("EVent_Flow_Template?SHW=" + cFun.EncryptValue(txtShowID.Text));
    }

    protected void btnShowInfo_Click(object sender, EventArgs e)
    {
        string showID = txtShowID.Text;
        if (!string.IsNullOrEmpty(showID))
        {
            Response.Redirect("Event_Settings_MasterPage?SHW=" + cFun.EncryptValue(showID));
        }
    }

    #region InsertNewSiteSettings and using tb_site_settings table & Insert new blank records but settings_name, ShowID into tb_site_settings table
    protected void InsertNewSiteSettings(string showid, string name)
    {
        try
        {
            string settingValue = string.Empty;
            string sql = string.Empty;

            DataTable dt = fn.GetDatasetByCommand("Select * From tmp_tbSiteSettings", "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string settingName = dr["settings_name"].ToString();

                    string query = "Select * From tb_site_settings Where ShowID=@SHWID And settings_name='" + settingName + "'";
                    List<SqlParameter> pList = new List<SqlParameter>();
                    SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    spar.Value = showid;
                    pList.Add(spar);

                    DataTable dtSettings = fn.GetDatasetByCommand(query, "dsSettings", pList).Tables[0];
                    if (dtSettings.Rows.Count == 0)
                    {
                        sql = string.Format("Insert Into tb_site_settings (settings_name,settings_value,ShowID)"
                                    + " Values('{0}', '{1}', '{2}')", settingName, settingValue, showid);

                        fn.ExecuteSQL(sql);
                    }
                }

                sql = "Update tb_site_settings Set settings_value='" + name + "' Where settings_name='" + eventname + "' And ShowID='" + showid + "';";
                fn.ExecuteSQL(sql);
            }
        }
        catch (Exception ex)
        {
            //pnlmessagebar.Visible = true;
            //lblemsg.Text = "Error Occur :" + ex.Message;
        }
    }
    #endregion

    #region loadvalue (Load RadGrid selected row data from tb_Show table to bind the respective controls while edit)
    private void loadvalue(string showid)
    {
        ShowController shwCtr = new ShowController(fn);
        DataTable dt = shwCtr.getShowByID(showid);
        if (dt.Rows.Count > 0)
        {
            hfID.Value = showid;
            txtName.Text = dt.Rows[0]["SHW_Name"].ToString();
            //txtPrefix.Enabled = false;
            //txtPrefix.Text = dt.Rows[0]["SHW_Prefix"].ToString();
            txtDescription.Text = dt.Rows[0]["SHW_Desc"].ToString();


            string imgname = dt.Rows[0]["SHW_Logo"].ToString();
            //if (!string.IsNullOrEmpty(imgname))
            //{
            //    string already_File = Server.MapPath(imgname);
            //    if (File.Exists(already_File))
            //    {
            //        imgImage.Visible = true;
            //        imgImage.ImageUrl = imgname;
            //    }
            //    else
            //    {
            //        imgImage.Visible = false;
            //        imgImage.ImageUrl = "";
            //    }
            //}
        }
    }
    #endregion

    #region btnUpdateRecord_Click (Update the respective data into "tb_Show" table)
    protected void btnUpdateRecord_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lbls.Text = "";
            string curid = hfID.Value;
            string name = cFun.solveSQL(txtName.Text.Trim());
            string prefix = string.Empty;// cFun.solveSQL(txtPrefix.Text.Trim());
            string description = cFun.solveSQL(txtDescription.Text.Trim());
            string showimage = string.Empty;

            //if(fupImage.HasFile)
            //{
            //    string filePath = "../Admin/images/Logo/";
            //    string chkpath = Server.MapPath(filePath);
            //    var directory = new DirectoryInfo(chkpath);
            //    if (directory.Exists == false)
            //    {
            //        directory.Create();
            //    }
            //    String fileExtension = System.IO.Path.GetExtension(fupImage.FileName).ToLower();
            // string filename= txtPrefix.Text.ToString().Trim()+  DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() +
            //           DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + fileExtension;
            //    fupImage.SaveAs(chkpath +filename);
            //    showimage = filePath + filename;
            //}

            ShowController shwCtr = new ShowController(fn);
            try
            {
                ShowObj sObj = new ShowObj();
                sObj.showid = curid;
                sObj.showname = name;
                sObj.prefix = prefix;
                bool isExist = shwCtr.checkShowUpdateExist(sObj);
                if (isExist)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This show already exist.');", true);
                    return;
                }
                else
                {
                    string query = string.Empty;
                    string editdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);

                    bool rowUpdated = false;
                    if (!string.IsNullOrEmpty(showimage))
                    {
                        ShowObj shwObj = new ShowObj();
                        shwObj.showid = curid;
                        shwObj.showname = name;
                        shwObj.logo = showimage;
                        shwObj.description = description;
                        rowUpdated = shwCtr.UpdateShow(shwObj);
                    }
                    else
                    {
                        ShowObj shwObj = new ShowObj();
                        shwObj.showid = curid;
                        shwObj.showname = name;
                        shwObj.description = description;
                        rowUpdated = shwCtr.UpdateShow(shwObj);
                    }

                    if (rowUpdated)
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        lbls.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updating error, try again.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                lbls.Text += ex.Message;
            }
        }
    }
    #endregion

    #region ResetControls
    private void ResetControls()
    {
        hfID.Value = "";
        txtName.Text = "";
        txtName.Focus();
        //txtPrefix.Enabled = true;
        //txtPrefix.Text = "";
        //imgImage.Visible = false;
        //imgImage.ImageUrl = "";
        txtDescription.Text = "";
    }
    #endregion

    #region ShowDetails

    #region EventSettings

    #region GKeyMasterEventSettings_NeedDataSource (Binding data into RadGrid & get DataSource From tb_Show table for RadGrid)
    protected void GKeyMasterEventSettings_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        string showID = txtShowID.Text;
        if (!string.IsNullOrEmpty(showID))
        {
            string query = "Select * From tb_site_settings Where showID=@showID And settings_name In ('eventname','pagetitle','copyrigtht','RegClosedDate','FromEmailAddress','Currency')";
            SqlParameter spar = new SqlParameter("showID", SqlDbType.NVarChar);
            spar.Value = showID;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar);

            DataTable dt = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                DataTable dtEventSettings = new DataTable();
                dtEventSettings.Columns.Add("ShowID", typeof(string));
                dtEventSettings.Columns.Add(eventname, typeof(string));
                dtEventSettings.Columns.Add(pagetitle, typeof(string));
                dtEventSettings.Columns.Add(copyrigtht, typeof(string));
                dtEventSettings.Columns.Add(RegClosedDate, typeof(string));
                dtEventSettings.Columns.Add(FromEmailAddress, typeof(string));
                dtEventSettings.Columns.Add(Currency, typeof(string));

                DataRow newdr = dtEventSettings.NewRow();
                newdr["ShowID"] = showID;
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["settings_name"].ToString() == eventname)
                    {
                        newdr[eventname] = string.IsNullOrEmpty(dr["settings_value"].ToString()) ? "-" : dr["settings_value"];
                    }
                    else if (dr["settings_name"].ToString() == pagetitle)
                    {
                        newdr[pagetitle] = string.IsNullOrEmpty(dr["settings_value"].ToString()) ? "-" : dr["settings_value"];
                    }
                    else if (dr["settings_name"].ToString() == copyrigtht)
                    {
                        newdr[copyrigtht] = string.IsNullOrEmpty(dr["settings_value"].ToString()) ? "-" : dr["settings_value"];
                    }
                    else if (dr["settings_name"].ToString() == RegClosedDate)
                    {
                        newdr[RegClosedDate] = string.IsNullOrEmpty(dr["settings_value"].ToString()) ? "-" : dr["settings_value"];
                    }
                    else if (dr["settings_name"].ToString() == FromEmailAddress)
                    {
                        newdr[FromEmailAddress] = string.IsNullOrEmpty(dr["settings_value"].ToString()) ? "-" : dr["settings_value"];
                    }
                    else if (dr["settings_name"].ToString() == Currency)
                    {
                        newdr[Currency] = string.IsNullOrEmpty(dr["settings_value"].ToString()) ? "-" : dr["settings_value"];
                    }
                }
                dtEventSettings.Rows.Add(newdr);

                //GKeyMasterEventSettings.DataSource = dtEventSettings;
            }
        }
    }
    #endregion

    #region GKeyMasterEventSettings_ItemCommand (Load when RadGrid row selected to edit)
    protected void GKeyMasterEventSettings_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string curid = "";
        //foreach (GridDataItem item in GKeyMasterEventSettings.SelectedItems)
        //{
        //    curid = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ShowID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["ShowID"].ToString() : "";
        //}
        if (e.CommandName == "EditSettings")
        {
            GridDataItem item = (GridDataItem)e.Item;

            curid = item.OwnerTableView.DataKeyValues[item.ItemIndex]["ShowID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["ShowID"].ToString() : "";
            if (!string.IsNullOrEmpty(curid))
            {
                Response.Redirect("Event_Settings_MasterPage?SHW=" + cFun.EncryptValue(curid));
            }
        }
    }
    #endregion
    #endregion

    #region Flow
    #region GKeyMaster1_NeedDataSource (Binding data into RadGrid & get DataSource From tb_site_flow_master table for RadGrid)
    protected void GKeyMaster1_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            string showID = txtShowID.Text;
            if (!string.IsNullOrEmpty(showID))
            {
                DataTable dt = GetFlowList(showID);

                if (dt.Rows.Count > 0)
                {
                    GKeyMaster1.DataSource = dt;
                }
            }
        }
        catch { }
    }
    #endregion

    #region GKeyMaster1_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string curid = "";
        foreach (GridDataItem item in GKeyMaster1.SelectedItems)
        {
            curid = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Flw_ID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Flw_ID"].ToString() : "";
        }
        if (e.CommandName == "EditFlow")
        {
            GridDataItem item = (GridDataItem)e.Item;

            curid = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Flw_ID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Flw_ID"].ToString() : "";
            if (!string.IsNullOrEmpty(curid))
            {
                string FlowID = curid;
                CommonFuns cFun = new CommonFuns();
                FlowID = cFun.EncryptValue(FlowID);
                string showID = cFun.EncryptValue(txtShowID.Text);
                string url = string.Format("{0}?FLW={1}&SHW={2} ", "Event_Flow_Dashboard", FlowID, showID);

                Response.Redirect(url);

                //FlowControler fControl = new FlowControler(fn);

                //Dictionary<string, string> nValues = new Dictionary<string, string>();
                //nValues = fControl.GetAdminNextRoute(curid, Number.Zero);
                //if (nValues.Count > 0)
                //{
                //    string page = nValues["nURL"].ToString();
                //    string step = nValues["nStep"].ToString();
                //    string FlowID = nValues["FlowID"].ToString();

                //    if (page == "") page = "Event_Config_Final";

                //    string path = fControl.MakeFullURL(page, FlowID, txtShowID.Text, "", step);

                //    Response.Redirect(path);
                //}
            }
        }

        else if (e.CommandName == "Browse")
        {
            GridDataItem item = (GridDataItem)e.Item;

            curid = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Flw_ID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Flw_ID"].ToString() : "";

            //Response.Redirect("http://203.125.155.188/StandardRegWeb_2016/Welcome?SHW=" + cFun.EncryptValue(txtShowID.Text) + "&FLW=" + cFun.EncryptValue(curid));
            string url = "http://203.125.155.188/StandardRegWeb_2016/Welcome?SHW=" + cFun.EncryptValue(txtShowID.Text) + "&FLW=" + cFun.EncryptValue(curid);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + "');", true);
        }
    }

    protected void GKeyMaster1_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = (GridDataItem)e.Item;
                if (item["StatusDesc"].Text == "Pending")//your condition
                {
                    LinkButton btn = (LinkButton)item["Browse"].Controls[0];
                    btn.Visible = false;
                }
            }
        }
        catch { }
    }
    #endregion

    #region LoadDetails
    protected void btnCreateFlow_Onclick(object sender, EventArgs e)
    {
        string showID = txtShowID.Text;
        if (!string.IsNullOrEmpty(showID))
        {
            showID = cFun.EncryptValue(showID);
            Response.Redirect("EVent_Flow_Template?SHW=" + showID);
        }
    }
    #endregion
    #endregion

    private void ShowPanel()
    {
        PanelKeyDetail.Visible = false;
        RadAjaxPanel1.Visible = false;
        PanelKeyDetail1.Visible = false;
    

        ShowController shwCtr = new ShowController(fn);
        DataTable dt=shwCtr.getShowByUser(Session["userid"].ToString());
        if (dt.Rows.Count > 1 || Session["roleid"].ToString() == Number.One)
        {
            PanelKeyList.Visible = true;
        }
        else
        {
            if (dt.Rows.Count > 0)
            {
                txtShowID.Text = dt.Rows[0]["SHW_ID"].ToString();
            }
            PanelKeyList.Visible = false;
            RadAjaxPanel1.Visible = true;
            if (Session["roleid"].ToString() == Number.One || Session["roleid"].ToString() == Number.Two)
            {
                divFlow.Visible = true;
                LoadShowInfo(txtShowID.Text);
            }
        }
        if (Session["roleid"].ToString() == Number.Two || Session["roleid"].ToString() == Number.Three)
        {
            PanelCreate.Visible = false;
            RadAjaxPanel1.Visible = false;
        }
    }
    private void LoadShowInfo(string ShowID)
    {
        SiteSettings sSettting = new SiteSettings(fn, ShowID);

        lblShowClosedDate.Text = sSettting.RegCloseDate.ToString();
        DataTable dList = GetFlowList(ShowID);

        lblShowFlowTotal.Text = dList.Rows.Count.ToString();
    }

    private DataTable GetFlowList(string ShowID)
    {
        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(ShowID))
        {
            string query = "select *,case when status= '{0}' Then 'Pending' else status End StatusDesc from tb_site_flow_master where showID=@showID and Status<>@stus";

            query = string.Format(query, RecordStatus.PENDING);
            SqlParameter spar = new SqlParameter("showID", SqlDbType.NVarChar);
            spar.Value = ShowID;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar);
            SqlParameter stus = new SqlParameter("stus", SqlDbType.NVarChar);
            stus.Value = RecordStatus.OFF;
            pList.Add(stus);
            dt = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];
        }
        return dt;
    }
    #endregion
}