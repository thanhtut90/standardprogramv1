﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Site.Utilities;
using System.IO;
using System.Data.SqlClient;
using Corpit.Utilities;

public partial class Admin_Dyn_ItemList : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.Attributes.Add("enctype", "multipart/form-data");
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["ITM"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

                bindlist("", showid);

                string itemid = cFun.DecryptValue(Request.QueryString["ITM"].ToString());

                bindSubItems(itemid);
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    /// <summary>
    /// ViewState["con_itemId"]= con_ItemId From tb_ConfItem
    /// </summary>
    /// 

    #region bindSubItems
    private void bindSubItems(string itemid)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            pnl_SubItems.Visible = true;
            ViewState["con_itemId"] = itemid;

            string query = "Select * From tb_ConfDependentItem Where con_itemId=" + ViewState["con_itemId"] + " And ShowID=@SHWID And isDeleted=0 Order By con_OrderSeq";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showid;
            pList.Add(spar);

            DataSet ds = new DataSet();
            ds = fn.GetDatasetByCommand(query, "ds", pList);
            if (ds.Tables[0].Rows.Count != 0)
            {
                rptSubItems.DataSource = ds;
                rptSubItems.DataBind();
            }
            else
            {
                rptSubItems.DataSource = null;
                rptSubItems.DataBind();

                pnl_Sub.Visible = false;
                divSub.Visible = false;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region rptcommand (Get Dependent Items according to Conference Item & use tb_ConfDependentItem table)
    protected void rptcommand(object sender, RepeaterCommandEventArgs e)
    {
        //if (e.CommandName == "sub-items")
        //{
        //    if (Request.Params["SHW"] != null)
        //    {
        //        string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

        //        pnl_SubItems.Visible = true;
        //        ViewState["con_itemId"] = e.CommandArgument.ToString();

        //        string query = "Select * From tb_ConfDependentItem Where con_itemId=" + ViewState["con_itemId"] + " And ShowID=@SHWID And isDeleted=0 Order By con_OrderSeq";
        //        List<SqlParameter> pList = new List<SqlParameter>();
        //        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        //        spar.Value = showid;
        //        pList.Add(spar);

        //        DataSet ds = new DataSet();
        //        ds = fn.GetDatasetByCommand(query, "ds", pList);
        //        if (ds.Tables[0].Rows.Count != 0)
        //        {
        //            rptSubItems.DataSource = ds;
        //            rptSubItems.DataBind();
        //        }
        //        else
        //        {
        //            rptSubItems.DataSource = null;
        //            rptSubItems.DataBind();

        //            pnl_Sub.Visible = false;
        //            divSub.Visible = false;
        //        }
        //    }
        //    else
        //    {
        //        Response.Redirect("Login.aspx");
        //    }
        //}
    }
    #endregion

    #region rptSubcommand (Bind the respective data from tb_ConfDependentItem table to the related controls according to selected item)
    protected void rptSubcommand(object sender, RepeaterCommandEventArgs e)
    {
        #region delete (Comment)
        //if (e.CommandName == "delete")
        //{
        //    string filter = string.Empty;
        //    if (ViewState["filter"] != null)
        //    {
        //        filter = ViewState["filter"].ToString();
        //    }
        //    string itemid = e.CommandArgument.ToString();
        //    string sql = "update tb_ConfItem set isdeleted = 1 where con_itemId=" + itemid;
        //    fn.ExecuteSQL(sql);
        //    bindlist(filter);
        //}
        #endregion

        if (e.CommandName == "sub")
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

                pnl_Sub.Visible = true;
                divSub.Visible = true;
                ViewState["Id"] = e.CommandArgument.ToString();

                string query = "Select * From tb_ConfDependentItem Where con_DependentID=" + ViewState["Id"] + " And ShowID=@SHWID And isDeleted=0 Order By con_OrderSeq";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);

                DataSet ds = new DataSet();
                ds = fn.GetDatasetByCommand(query, "ds", pList);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    string itemname = ds.Tables[0].Rows[0]["con_ItemName"].ToString();
                    string isearlybird = ds.Tables[0].Rows[0]["con_isEarlyBird"].ToString();
                    string ebprice = ds.Tables[0].Rows[0]["con_EBPrice"].ToString();
                    string rprice = ds.Tables[0].Rows[0]["con_RegPrice"].ToString();
                    string maxreg = ds.Tables[0].Rows[0]["con_MaxReg"].ToString();
                    string message = ds.Tables[0].Rows[0]["con_MaxMessage"].ToString();
                    string imgname = ds.Tables[0].Rows[0]["con_ItemImage"].ToString();
                    string Desc = ds.Tables[0].Rows[0]["con_ItemDesc"].ToString();
                    string isShow = ds.Tables[0].Rows[0]["con_IsShow"].ToString();
                    string orderSequence = ds.Tables[0].Rows[0]["con_OrderSeq"].ToString();
                    string maxBuyableTime = ds.Tables[0].Rows[0]["maxBuyableTime"] != DBNull.Value ? ds.Tables[0].Rows[0]["maxBuyableTime"].ToString() : "1";
                    string disabledItems = ds.Tables[0].Rows[0]["disabledItems"] != DBNull.Value ? ds.Tables[0].Rows[0]["disabledItems"].ToString() : "";
                    string con_ItemDesc_ShowedInTemplate = ds.Tables[0].Rows[0]["con_ItemDesc_ShowedInTemplate"] != DBNull.Value ? ds.Tables[0].Rows[0]["con_ItemDesc_ShowedInTemplate"].ToString() : "";
                    string ReportDisplayName = ds.Tables[0].Rows[0]["ReportDisplayName"] != DBNull.Value ? ds.Tables[0].Rows[0]["ReportDisplayName"].ToString() : "";

                    txtName.Text = itemname;
                    if (imgname == "")
                    {
                        Image1.Visible = false;
                    }
                    else
                    {
                        Image1.Visible = true;
                    }
                    Image1.ImageUrl = imgname;
                    if (isearlybird == "1")
                    {
                        chkisEarlyBird.Checked = true;
                    }
                    else
                    {
                        chkisEarlyBird.Checked = false;
                    }

                    if (isShow == "1")
                    {
                        chkIsShow.Checked = true;
                    }
                    else
                    {
                        chkIsShow.Checked = false;
                    }

                    txtEarlyBirdPrice.Text = ebprice;
                    txtRegularPrice.Text = rprice;
                    txtmax.Text = !string.IsNullOrEmpty(maxreg) ? maxreg : "0";
                    txtDesc.Text = Desc;
                    txtDescShowInEmailTemplate.Text = Server.HtmlDecode(con_ItemDesc_ShowedInTemplate);
                    txtDescShowInReportItemBreakdown.Text = ReportDisplayName;
                    txtMessage.Text = Server.HtmlDecode(message);
                    txtSequence.Text = orderSequence;

                    txtMaxBuyableTime.Text = maxBuyableTime;
                    string queryDep = "Select * From tb_ConfDependentItem Where con_itemId=" + ViewState["con_itemId"] + " And ShowID=@SHWID And isDeleted=0"
                                        + " And con_DependentID<>" + ViewState["Id"] + " Order By con_OrderSeq";
                    DataSet dsDep = fn.GetDatasetByCommand(queryDep, "ds", pList);
                    if (dsDep.Tables[0].Rows.Count > 0)
                    {
                        chkDisabledItems.DataSource = dsDep;
                        chkDisabledItems.DataTextField = "con_ItemName";
                        chkDisabledItems.DataValueField = "con_DependentID";
                        chkDisabledItems.DataBind();

                        try
                        {
                            if (!String.IsNullOrEmpty(disabledItems))
                            {
                                string[] strDisabledItems = disabledItems.Split(',');
                                if (strDisabledItems.Length > 0)
                                {
                                    chkDisabledItems.ClearSelection();
                                    foreach (var item in strDisabledItems)
                                    {
                                        ListItem listItem = chkDisabledItems.Items.FindByValue(item);
                                        if (listItem != null)
                                        {
                                            listItem.Selected = true;
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }
    #endregion

    #region ajaxUpload1_UploadComplete
    protected void ajaxUpload1_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
    {
        string filePath = "~/Admin/Site/" + e.FileName;
        fupImg.SaveAs(MapPath(filePath));
        Session["imgBanner"] = "../Admin/site/" + e.FileName;
    }
    #endregion

    #region UpSystemLogo_Click
    protected void UpSystemLogo_Click(object sender, EventArgs e)
    {
        try
        {
            if (fupImg.PostedFile.FileName != "")
            {
                string postedfilename = Path.GetFileNameWithoutExtension(fupImg.PostedFile.FileName);
                string postedfileext = Path.GetExtension(fupImg.PostedFile.FileName);
                string datetick = "_" + DateTime.Now.ToString("ddmmyyyyhhmm");

                string filePath = "~/Admin/Site/" + postedfilename + datetick + postedfileext;
                fupImg.SaveAs(MapPath(filePath));
                Session["imgBanner"] = "../Admin/site/" + postedfilename + datetick + postedfileext;
                Image1.Visible = true;
                Image1.ImageUrl = "../Admin/site/" + postedfilename + datetick + postedfileext;
            }
            else
            {
                Image1.Visible = false;
                Image1.ImageUrl = "";
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region btnSave_Click
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            try
            {
                RunNumber run = new RunNumber(fn);
                string depID = run.GetRunNUmber("DEPENITEM_KEY");
                int con_isEarlyBird = 0;
                string con_EBPrice = "0.00";
                string con_RegPrice = "0.00";
                int con_itemId = 0;
                int con_MaxReg = 0;
                string con_MaxMessage = string.Empty;
                string imgname = string.Empty;
                string contype = string.Empty;
                string Desc = string.Empty;
                string con_ItemDesc_ShowedInTemplate = string.Empty;
                string ReportDisplayName = string.Empty;
                int con_IsShow = 0;
                int sequence = 0;

                int maxBuyableTime = 0;//***added on 6-8-2018
                string disabledItems = "";//***added on 6-8-2018

                if (Session["imgBanner"] != null)
                {
                    imgname = Session["imgBanner"].ToString();
                }
                else if (!string.IsNullOrEmpty(Image1.ImageUrl))
                {
                    imgname = Image1.ImageUrl;
                }
                con_itemId = Convert.ToInt32(ViewState["con_itemId"]);
                con_EBPrice = txtEarlyBirdPrice.Text.ToString();// Convert.ToInt32(txtEarlyBirdPrice.Text.ToString());
                con_RegPrice = txtRegularPrice.Text.ToString();
                if (chkisEarlyBird.Checked == true)
                {
                    con_isEarlyBird = 1;
                }
                else
                {
                    con_isEarlyBird = 0;
                }

                con_IsShow = chkIsShow.Checked == true ? 1 : 0;

                con_MaxReg = Convert.ToInt32(!string.IsNullOrWhiteSpace(txtmax.Text) && !string.IsNullOrEmpty(txtmax.Text) ? txtmax.Text.ToString() : "0");
                con_MaxMessage = Server.HtmlEncode(txtMessage.Text);
                Desc = txtDesc.Text;
                con_ItemDesc_ShowedInTemplate = Server.HtmlEncode(txtDescShowInEmailTemplate.Text);
                ReportDisplayName = txtDescShowInReportItemBreakdown.Text;

                sequence = !string.IsNullOrWhiteSpace(txtSequence.Text) ? Convert.ToInt32(txtSequence.Text.Trim()) : 0;

                //***added on 6-8-2018
                maxBuyableTime = Convert.ToInt32(!string.IsNullOrWhiteSpace(txtMaxBuyableTime.Text) && !string.IsNullOrEmpty(txtMaxBuyableTime.Text) ? txtMaxBuyableTime.Text.ToString() : "0");
                for (int i = 0; i < chkDisabledItems.Items.Count; i++)
                {
                    if (chkDisabledItems.Items[i].Selected == true)
                    {
                        disabledItems += chkDisabledItems.Items[i].Value + ",";
                    }
                }
                disabledItems = disabledItems.TrimEnd(',');
                //***added on 6-8-2018

                string sql = "";
                if (ViewState["Id"] != null)
                {
                    sql = "Update tb_ConfDependentItem Set con_ItemName = '" + txtName.Text + "',con_ItemDesc='" + Desc
                        + "', con_isEarlyBird = " + con_isEarlyBird + " ,con_EBPrice= " + con_EBPrice + " ,con_RegPrice= " + con_RegPrice
                        + " ,con_ItemImage='" + imgname + "' ,con_MaxReg=" + con_MaxReg + ", con_MaxMessage='" + con_MaxMessage
                        + "', con_IsShow=" + con_IsShow + ",con_OrderSeq=" + sequence + ",maxBuyableTime=" + maxBuyableTime + ",disabledItems='" + disabledItems
                        + "',con_ItemDesc_ShowedInTemplate='" + con_ItemDesc_ShowedInTemplate + "',ReportDisplayName='" + ReportDisplayName
                        + "' Where con_DependentID = " + ViewState["Id"] + " And ShowID='" + showid + "'";
                }
                else
                {
                    sql = "Insert Into tb_ConfDependentItem (con_DependentID,con_ItemName,con_ItemDesc,con_isEarlyBird,con_EBPrice,con_RegPrice"
                        + ",con_itemId,con_ItemImage,con_MaxReg,con_MaxMessage,con_IsShow,con_OrderSeq,ShowID,maxBuyableTime,disabledItems,con_ItemDesc_ShowedInTemplate,ReportDisplayName) Values ";
                    sql += "(" + depID + ",'" + txtName.Text + "','" + Desc + "'," + con_isEarlyBird + "," + con_EBPrice + "," + con_RegPrice
                        + "," + con_itemId + ",'" + imgname + "'," + con_MaxReg + ", '" + con_MaxMessage + "'," + con_IsShow + "," + sequence + ",'" + showid + "',"
                        + maxBuyableTime + ",'" + disabledItems + "','" + con_ItemDesc_ShowedInTemplate + "','" + ReportDisplayName + "')";
                }
                fn.ExecuteSQL(sql);

                string query = "Select * From tb_ConfDependentItem Where con_itemId=" + con_itemId + " And ShowID=@SHWID And isDeleted=0 Order By con_OrderSeq";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);

                DataSet ds = new DataSet();
                ds = fn.GetDatasetByCommand(query, "ds", pList);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    rptSubItems.DataSource = ds;
                    rptSubItems.DataBind();
                    pnl_SubItems.Visible = true;
                    pnl_Sub.Visible = false;
                    divSub.Visible = false;
                }
                else
                {
                    pnl_SubItems.Visible = false;
                    pnl_Sub.Visible = false;
                    divSub.Visible = false;
                }

                txtName.Text = "";
                txtEarlyBirdPrice.Text = "";
                txtRegularPrice.Text = "";
                txtDesc.Text = "";
                txtDescShowInEmailTemplate.Text = "";
                txtDescShowInReportItemBreakdown.Text = "";
                txtmax.Text = "";
                txtMessage.Text = "";
                txtSequence.Text = "";
                Image1.Visible = false;
                Image1.ImageUrl = "";
                ViewState["Id"] = null;
                Session["imgBanner"] = null;
            }
            catch (Exception ex)
            {
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region btnDelete_Click
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            int con_itemId = 0;
            if (ViewState["Id"] != null && ViewState["Id"] != "")
            {
                con_itemId = Convert.ToInt32(ViewState["con_itemId"]);

                string str = "Update tb_ConfDependentItem Set isDeleted=1 Where con_DependentID = " + ViewState["Id"] + " And ShowID='" + showid + "'";
                fn.ExecuteSQL(str);

                DataSet ds = new DataSet();
                ds = fn.GetDatasetByCommand("Select * From tb_ConfDependentItem Where con_itemId = " + con_itemId + " And isDeleted=0 Order By con_OrderSeq", "ds");
                if (ds.Tables[0].Rows.Count != 0)
                {
                    rptSubItems.DataSource = ds;
                    rptSubItems.DataBind();
                    pnl_SubItems.Visible = true;
                    pnl_Sub.Visible = false;
                    divSub.Visible = false;
                }
                else
                {
                    pnl_SubItems.Visible = false;
                    pnl_Sub.Visible = false;
                    divSub.Visible = false;
                }

                txtName.Text = "";
                txtEarlyBirdPrice.Text = "";
                txtRegularPrice.Text = "";
                txtDesc.Text = "";
                txtDescShowInEmailTemplate.Text = "";
                txtDescShowInReportItemBreakdown.Text = "";
                txtmax.Text = "";
                txtMessage.Text = "";
                txtSequence.Text = "";
                Image1.Visible = false;
                Image1.ImageUrl = "";
                ViewState["Id"] = null;
                Session["imgBanner"] = null;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region btnReset_Click
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtName.Text = "";
        txtEarlyBirdPrice.Text = "";
        txtRegularPrice.Text = "";
        txtDesc.Text = "";
        txtDescShowInEmailTemplate.Text = "";
        txtDescShowInReportItemBreakdown.Text = "";
        txtmax.Text = "";
        txtMessage.Text = "";
        txtSequence.Text = "";
        Image1.Visible = false;
        Image1.ImageUrl = "";
        ViewState["Id"] = null;
        Session["imgBanner"] = null;
    }
    #endregion

    #region setGroupName
    protected string setGroupName(string groupid)
    {
        string groupname = fn.GetDataByCommand("Select con_GroupName From tb_Conf_Group Where con_GroupId=" + groupid, "con_GroupName");

        return groupname;
    }
    protected string setCategory(string catid)
    {
        string categoryname = string.Empty;

        if (catid != "0")
        {

            categoryname = fn.GetDataByCommand("Select reg_CategoryName From ref_reg_Category Where reg_CategoryId=" + catid, "reg_CategoryName");
        }
        else
        {
            categoryname = "All";
        }
        return categoryname;
    }
    #endregion

    #region bindlist
    protected void bindlist(string filter, string showid)
    {
        //string query = "Select * From tb_ConfItem Where isDeleted=0 And ShowID=@SHWID";
        //List<SqlParameter> pList = new List<SqlParameter>();
        //SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        //spar.Value = showid;
        //pList.Add(spar);

        //DataSet ds = new DataSet();
        //ds = fn.GetDatasetByCommand(query, "ds", pList);
        //if (ds.Tables[0].Rows.Count != 0)
        //{
        //    rptItem.DataSource = ds;
        //    rptItem.DataBind();
        //    pnllist.Visible = true;
        //}
        //else
        //{
        //    pnllist.Visible = false;
        //}
    }
    #endregion

    #region bindSubItemlist
    protected void bindSubItemlist(string filter, string showid)
    {
        string query = "Select * From tb_ConfDependentItem Where ShowID=@SHWID And isDeleted=0 Order By con_OrderSeq";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand(query, "ds", pList);
        if (ds.Tables[0].Rows.Count != 0)
        {
            rptSubItems.DataSource = ds;
            rptSubItems.DataBind();
            pnl_SubItems.Visible = true;
        }
        else
        {
            pnl_SubItems.Visible = false;
        }
    }
    #endregion

    #region bindCategory & bindGroup (Comment)
    //protected void bindCategory()
    //{
    //    int lastindex = 1;
    //    DataSet ds = new DataSet();
    //    ds = fn.GetDatasetByCommand("Select * from ref_reg_Category Order By reg_order ASC", "ds");
    //    if (ds.Tables[0].Rows.Count != 0)
    //    {
    //        ddlCategory.Items.Add("Show All");
    //        ddlCategory.Items[0].Value = "-1";
    //        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
    //        {
    //            lastindex += 1;
    //            ddlCategory.Items.Add(ds.Tables[0].Rows[x]["reg_CategoryName"].ToString());
    //            ddlCategory.Items[x + 1].Value = ds.Tables[0].Rows[x]["reg_CategoryId"].ToString();
    //        }
    //    }
    //}

    //protected void bindGroup()
    //{
    //    DataSet ds = new DataSet();
    //    ds = fn.GetDatasetByCommand("Select * from tb_ItemGroup Order By con_Orderno ASC", "ds");
    //    if (ds.Tables[0].Rows.Count != 0)
    //    {
    //        ddlGroup.Items.Add("Show All");
    //        ddlGroup.Items[0].Value = "0";
    //        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
    //        {
    //            ddlGroup.Items.Add(ds.Tables[0].Rows[x]["con_GroupName"].ToString());
    //            ddlGroup.Items[x + 1].Value = ds.Tables[0].Rows[x]["con_GroupId"].ToString();
    //        }
    //    }
    //}
    #endregion

    #region btnAdd_Click
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        pnl_Sub.Visible = true;
        divSub.Visible = true;
        txtName.Text = "";
        txtEarlyBirdPrice.Text = "";
        txtRegularPrice.Text = "";
        txtDesc.Text = "";
        txtDescShowInEmailTemplate.Text = "";
        txtDescShowInReportItemBreakdown.Text = "";
        txtmax.Text = "";
        txtMessage.Text = "";
        txtSequence.Text = "";
        Image1.Visible = false;
        Image1.ImageUrl = "";
        ViewState["Id"] = null;
        Session["imgBanner"] = null;
        txtMaxBuyableTime.Text = "";

        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showid;
            pList.Add(spar);
            string queryDep = "Select * From tb_ConfDependentItem Where con_itemId=" + ViewState["con_itemId"] + " And ShowID=@SHWID And isDeleted=0 Order By con_OrderSeq";
            DataSet dsDep = fn.GetDatasetByCommand(queryDep, "ds", pList);
            if (dsDep.Tables[0].Rows.Count > 0)
            {
                chkDisabledItems.DataSource = dsDep;
                chkDisabledItems.DataTextField = "con_ItemName";
                chkDisabledItems.DataValueField = "con_DependentID";
                chkDisabledItems.DataBind();
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region setIsShowValue
    protected string setIsShowValue(string status)
    {
        return status == "1" ? "Yes" : "No";
    }
    #endregion

    #region setImgVisibility
    protected bool setImgVisibility(string imgPath)
    {
        bool isVisible = false;
        if (!String.IsNullOrEmpty(imgPath))
        {
            string already_File = Server.MapPath(imgPath);
            if (File.Exists(already_File))
            {
                isVisible = true;
            }
        }

        return isVisible;
    }
    #endregion

    #region btnBack_Click
    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["FLW"] != null && Request.QueryString["SHW"] != null && Request.QueryString["STP"] != null && Request.QueryString["CAT"] != null)
        {
            string url = string.Format("Event_Conference_Items?FLW={0}&SHW={1}&STP={2}&CAT={3}",
                Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString(), Request.QueryString["CAT"].ToString());
            Response.Redirect(url);
        }
    }
    #endregion

}