﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="AdminRegister.aspx.cs" Inherits="Admin_AdminRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Dashboard | Admin Registration</title>
    <style type="text/css">
        .style1
        {
            width:150px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Admin Registration</h1>
            <!--<span class="pagedesc">Page to Register for Login.</span>
            
            <ul class="hornav">
                <li class="current"><a href="#index">Admin Registration</a></li>
            </ul>-->
        </div>
        <div id="contentwrapper" class="contentwrapper">
            <div class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Username</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtusername"  CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvusername" runat="server" ControlToValidate="txtusername"
                                    ErrorMessage="* Required" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-2 control-label">Password</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtpassword"  CssClass="form-control" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvpassword" runat="server" ControlToValidate="txtpassword"
                                    ErrorMessage="* Required" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-2 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtconfirmpswd" TextMode="Password"  CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvconfirmpswd" runat="server" ControlToValidate="txtconfirmpswd"
                                ErrorMessage="* Required" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvconfirmpswd" runat="server" ControlToValidate="txtconfirmpswd" ControlToCompare="txtpassword"
                                ErrorMessage="Not match." Display="Dynamic" ForeColor="Red"></asp:CompareValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Show Name</label>
                            <div class="col-md-6">
                                <asp:DropDownList runat="server" ID="ddlShowName" AutoPostBack="false"  CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Role Name</label>
                            <div class="col-md-6">
                                <asp:DropDownList runat="server" ID="ddlrolename" AutoPostBack="false"  CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="form-group">
                            <div class="col-md-2 control-label"></div>
                            <div class="col-md-6">
                                <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnsubmit_Click" CssClass="btn btn-primary" />
                                &nbsp;
                                <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" CssClass="btn btn-primary" />
                            </div>
                        </div>
            <asp:Label runat="server" ID="lbls"></asp:Label>
        </div>
    </div>

</asp:Content>

