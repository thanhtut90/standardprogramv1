﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="EVent_Flow_Template.aspx.cs" Inherits="Admin_EVent_Flow_Template" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        input[type="checkbox"], input[type="radio"]
        {
            margin : 4px !important;
        }
        .page-header
        {
            font-size: 18px !important;
            border-bottom: 0 !important;
        }
        .box-header .box-title
        {
            margin: 10px 0 20px 0;
            font-size: 22px !important;
        }
        .box-header
        {
            padding:0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="centercontent">
        <div class="content-header">
            <h1 >3. Manage Registraion Steps</h1>
        </div>
        <div id="contentwrapper" class="contentwrapper" style="padding-top:25px;">     

            <h4 class="pagetitle" style="padding-bottom:20px;text-decoration:underline;font-weight:bold;">Please Select Template</h4>
            <%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">--%>
                <asp:Panel runat="server" ID="PanelKeyList">
                    <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                        EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                        OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="10" Skin="Silk">
                        <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="Route_ID" ShowHeader="false">
                            <Columns>
                                <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Route_ID" FilterControlAltText="Filter Route_ID"
                                    HeaderText="Flow Template ID" SortExpression="Route_ID" UniqueName="Route_ID" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Desc" FilterControlAltText="Filter Route_ID"
                                    HeaderText="Detail Description" SortExpression="Desc" UniqueName="Desc" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </asp:Panel>
                <br />
                <br />
                <asp:Panel runat="server" ID="PanelKeyDetail">
                        <div style="padding-top: 10px;">
                                    <div class="form-group">
                                                    <asp:Label ID="lblFlowName" runat="server" CssClass="col-md-2 control-label">Registration Name </asp:Label>
                                                   
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="txtFlowName" runat="server"></asp:TextBox>
                                                         <span style="font-size:11px;"><br />( Use When browse Registration)</span>
                                                        <asp:RequiredFieldValidator ID="rfvFlowName" runat="server" ControlToValidate="txtFlowName" ForeColor="Red"
                                                            ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                       
                                  <div class='page-header'>
                                     
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Step</th>
                                                    <th>Description</th>
                                                    <th>Remove Step from Flow</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater runat="server" ID="RptSteps" OnItemDataBound="RptSteps_DataBond">
                                                    <ItemTemplate>
                                                        <asp:Panel runat="server" ID="PanelRow">
                                                            <tr style="padding:4px;">
                                                                <td><span class="fa-stack fa-1">
                                                                    <i class="fa fa-bookmark fa-stack-2x"></i>
                                                                    <i class="fa fa-inverse fa-stack-1x"><%#Eval("RouteOrder")%></i>
                                                                </span></td>
                                                                <td><%#Eval("script_desc")%></td>
                                                                <td>
                                                                    <asp:CheckBox runat="server" ID="chckRemoveFromFlow" />
                                                                    <asp:TextBox runat="server" ID="txtTemplateID" Visible="false" Text='<%#Eval("ID")%>'></asp:TextBox>
                                                                    <asp:TextBox runat="server" ID="txtRouteOrder" Visible="false" Text='<%#Eval("RouteOrder")%>'></asp:TextBox>
                                                                    <asp:TextBox runat="server" ID="ChkOutModule" Visible="false" Text='<%#Eval("ChkOutModule")%>'></asp:TextBox>
                                                                    <asp:TextBox runat="server" ID="txtModuleID" Visible="false" Text='<%#Eval("Module_Id")%>'></asp:TextBox>
                                                                    <asp:TextBox runat="server" ID="txtTitle" Visible="false" Text='<%#Eval("Module_title")%>'></asp:TextBox>
                                                                    <asp:TextBox runat="server" ID="isShowRow" Visible="false" Text='<%#Eval("IsVisible")%>'></asp:TextBox>
                                                                    <asp:TextBox runat="server" ID="isAllowSkip" Visible="false" Text='<%#Eval("IsSkip")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </asp:Panel>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                        <!-- /.box-body -->
                                    </div>
                        </div>
                        <div style="padding-left: 30%;">
                            <asp:LinkButton ID="btnCreateFlow" runat="server" OnClick="btnCreateFlow_Onclick" CssClass="btn btn-danger">
                                <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Create
                            </asp:LinkButton>

                            <asp:TextBox runat="server" ID="txtSelectedRoute" Visible="false"></asp:TextBox>
                            <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
                        </div>
                </asp:Panel>
            <%--</telerik:RadAjaxPanel>--%>
        </div>
    </div>
</asp:Content>

