﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;
public partial class Admin_SiteTemplate_Receipt : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    SqlConnection con;
    SqlCommand cmd;
    SqlDataReader dr;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        //    Master.setopenemail();
            lblheader.Text = "Site Template (Receipt)";
            panelsitetemplate.Visible = false;
        }
        else
        {
          //  Master.setopenemail();
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_site_Template table for RadGrid)
    protected void GKeyMaster_NeedDataSource1(object sender, GridNeedDataSourceEventArgs e)
    {
        try
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            string query = string.Format("select * from tb_site_Template where SHowID='{0}'", showid) ;
            DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dt;
            }
            else
            {
                lbls.Text = "There is no record.";
            }
        }
        catch (Exception)
        {
            lbls.Text = "There is no record.";
        }
    }
    #endregion

    #region LoadList (bind data from tb_site_Template_DataSource table to ddltempdatasource dropdownlist control)
    private void loadlist()
    {
        string constr = fn.ConnString;
        using (con = new SqlConnection(constr))
        {
            using (cmd = new SqlCommand("Select * From tb_site_Template_DataSource"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddltempdatasource.DataSource = cmd.ExecuteReader();
                ddltempdatasource.DataTextField = "Table_Name";
                ddltempdatasource.DataValueField = "DataSourceID";
                ddltempdatasource.DataBind();
                con.Close();
            }
        }
    }
    #endregion

    #region bindRightSight
    private void bindRightSight()
    {
        string tbname = ddltempdatasource.SelectedItem.Text;
        tempdatasource.SelectParameters["tablename"].DefaultValue = tbname;
    }
    #endregion

    #region loadvalue (Load RadGrid selected row data from tb_site_Template and tb_site_Template_DataSource tables to bind the respective controls while edit)
    private void loadvalue(string tempid)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            string connStr = fn.ConnString;
            string query = "Select * From tb_site_Template Where temp_id='" + tempid + "'";
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();

                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            txttempid.Text = dataReader["temp_id"].ToString();
                            txttemptitle.Text = dataReader["temp_name"].ToString();
                            string tempbdid = dataReader["temp_BaseDSource"].ToString();

                            try
                            {
                                if (!String.IsNullOrEmpty(tempbdid))
                                {
                                    ListItem listItem = ddltempdatasource.Items.FindByValue(tempbdid);
                                    if (listItem != null)
                                    {
                                        ddltempdatasource.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                            }

                            txtsubject.Text = dataReader["temp_Subject"].ToString();
                            string tempvalue = dataReader["temp_value"].ToString();
                            txtvalue.Text = Server.HtmlDecode(tempvalue);
                        }
                        dataReader.Close();
                        dataReader.Dispose();
                    }

                    connection.Close();
                }
            }

            string tbname = ddltempdatasource.SelectedItem.Text;
            bindRightSight();
            lbltblfk.Text = fn.GetDataByCommand("Select Table_FilterKey From tb_site_Template_DataSource Where Table_Name = '" + tbname + "'", "Table_FilterKey");
            lbltblcec.Text = fn.GetDataByCommand("Select Table_CotactEmailColumn From tb_site_Template_DataSource Where Table_Name = '" + tbname + "'", "Table_CotactEmailColumn");
            if (lbltblfk.Text == "" || lbltblfk.Text == null || lbltblfk.Text == "0")
            {
                lbltblfk.Text = "nil";
            }
            if (lbltblcec.Text == "" || lbltblcec.Text == null || lbltblcec.Text == "0")
            {
                lbltblcec.Text = "nil";
            }

            //EmailHelpers eh = new EmailHelpers(fn);
            //string image = SiteDefaultValue.DefaultEmailBindDelimiter + SiteDefaultValue.DefaultSiteBanner + SiteDefaultValue.DefaultEmailBindDelimiter;
            //string imageurl = eh.BindTemplateWithStandardSetting(image, showid);
            //imgdefaultbanner.ImageUrl = imageurl;
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region ddltempdatasource_SelectedIndexChanged (get related data from tb_site_Template_DataSource table and bind to lbltblfk and lbltblces controls)
    protected void ddltempdatasource_SelectedIndexChanged(object sender, EventArgs e)
    {
        string conString = fn.ConnString;
        string tbname = ddltempdatasource.SelectedItem.Text;
        bindRightSight();
        lbltblfk.Text = fn.GetDataByCommand("Select Table_FilterKey From tb_site_Template_DataSource Where Table_Name = '" + tbname + "'", "Table_FilterKey");
        lbltblcec.Text = fn.GetDataByCommand("Select Table_CotactEmailColumn From tb_site_Template_DataSource Where Table_Name = '" + tbname + "'", "Table_CotactEmailColumn");
        if (lbltblfk.Text == "" || lbltblfk.Text == null || lbltblfk.Text == "0")
        {
            lbltblfk.Text = "nil";
        }
        if (lbltblcec.Text == "" || lbltblcec.Text == null || lbltblcec.Text == "0")
        {
            lbltblcec.Text = "nil";
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, GridCommandEventArgs e)
    {
        string tempid = string.Empty;
        foreach (GridDataItem item in GKeyMaster.SelectedItems)
        {
            tempid = item["temp_id"].Text;
        }

        loadlist();
        loadvalue(tempid);
        panelsitetemplate.Visible = true;
        btnsave.Visible = true;
        btnadd.Visible = false;
        txttempid.Enabled = false;
    }
    #endregion

    #region getdatasourcename (Get Table Name from tb_site_Template_DataSource table according to DataSourceID)
    public string getdatasourcename(string datasourceid)
    {
        return fn.GetDataByCommand("Select Table_Name From tb_site_Template_DataSource Where DataSourceID ='" + datasourceid + "'", "Table_Name");
    }
    #endregion

    #region btnaddnewrecord_Click ("Add New Template" button click to add new template)
    protected void btnaddnewrecord_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            panelsitetemplate.Visible = true;
            ResetControls();
            btnadd.Visible = true;
            btnsave.Visible = false;
            txttempid.ReadOnly = false;

            loadlist();
            bindRightSight();

            //EmailHelpers eh = new EmailHelpers(fn);
            //string image = SiteDefaultValue.DefaultEmailBindDelimiter + SiteDefaultValue.DefaultSiteBanner + SiteDefaultValue.DefaultEmailBindDelimiter;
            //string imageurl = eh.BindTemplateWithStandardSetting(image, showid);
            //imgdefaultbanner.ImageUrl = imageurl;

            ddltempdatasource_SelectedIndexChanged(this, null);
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region btnadd_Click ("Add" button click & Insert data into tb_site_Template table)
    protected void btnadd_Click(object sender, EventArgs e)
    {
        string conString = fn.ConnString;
        string tempid = txttempid.Text;
        string tempname = txttemptitle.Text;
        string tempvalue = Server.HtmlEncode(txtvalue.Text);
        string tempbasedsource = ddltempdatasource.SelectedValue;

        string tempsubject = txtsubject.Text;
        if (tempname == "" || tempvalue == "" || tempsubject == "" || tempid == "")
        {
            lbls.Text = "Please fill up the forms.";
        }
        else
        {
            try
            {
                DataTable dttemplate = fn.GetDatasetByCommand("Select * From tb_site_Template Where temp_id='" + tempid + "'", "ds").Tables[0];
                if (dttemplate.Rows.Count > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Template ID already exist.');", true);
                    return;
                }
                else
                {
                    con = new SqlConnection(conString);
                    con.Open();
                    string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                    cmd = new SqlCommand("Insert into tb_site_Template (temp_id, temp_name, temp_value, temp_BaseDSource, temp_Subject,SHowID,temp_type)" +
                                         " Values (@temp_id,@temp_name, @temp_value, @temp_BaseDSource, @temp_Subject,@SHowID,1)");
                    cmd.Parameters.AddWithValue("@temp_id", tempid);
                    cmd.Parameters.AddWithValue("@temp_name", tempname);
                    cmd.Parameters.AddWithValue("@temp_value", tempvalue);
                    cmd.Parameters.AddWithValue("@temp_BaseDSource", tempbasedsource);
                    cmd.Parameters.AddWithValue("@temp_Subject", tempsubject);
                    cmd.Parameters.AddWithValue("@SHowID", showid);
                    cmd.Connection = con;

                    int rowUpdated1 = cmd.ExecuteNonQuery();
                    if (rowUpdated1 == 1)
                    {
                        panelsitetemplate.Visible = false;
                        ResetControls();
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        panelsitetemplate.Visible = false;
                        ResetControls();
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Saving error, try again.');", true);
                    }
                    con.Close();
                }
            }
            catch (Exception ea)
            {
                lbls.Text += ea.ToString();
            }
        }
    }
    #endregion

    #region btnsave_Click (Update data to tb_site_Template table)
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string conString = fn.ConnString;
            string tempid = txttempid.Text;
            string tempname = txttemptitle.Text;
            string tempvalue = Server.HtmlEncode(txtvalue.Text);
            string tempbasedsource = ddltempdatasource.SelectedValue;

            string tempsubject = txtsubject.Text;
            if (tempname == "" || tempvalue == "" || tempsubject == "" || tempid == "")
            {
                lbls.Text = "Please fill up the forms.";
            }
            else
            {
                try
                {
                    con = new SqlConnection(conString);
                    con.Open();
                    cmd = new SqlCommand("Update tb_site_Template Set temp_name = @temp_name, temp_value = @temp_value," +
                    "temp_BaseDSource = @temp_BaseDSource, temp_Subject = @temp_Subject WHERE temp_id='" + tempid + "'");

                    cmd.Parameters.AddWithValue("@temp_name", tempname);
                    cmd.Parameters.AddWithValue("@temp_value", tempvalue);
                    cmd.Parameters.AddWithValue("@temp_BaseDSource", tempbasedsource);
                    cmd.Parameters.AddWithValue("@temp_Subject", tempsubject);

                    cmd.Connection = con;

                    int rowUpdated1 = cmd.ExecuteNonQuery();
                    if (rowUpdated1 == 1)
                    {
                        panelsitetemplate.Visible = false;
                        ResetControls();
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        panelsitetemplate.Visible = false;
                        ResetControls();
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updating error, try again.');", true);
                    }
                    con.Close();
                }
                catch (Exception ea)
                {
                    lbls.Text += ea.ToString();
                }
            }
        }
    }
    #endregion

    #region btnpreview_Click (Show the template in formated html)
    protected void btnpreview_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            //   EmailHelpers eh = new EmailHelpers(fn);
            string template = "";// eh.BindTemplateWithStandardSetting(txtvalue.Text, showid);
            lblheader.Text = "Preview";
            panelpreview.Visible = true;
            panelsitetemplate.Visible = false;
            btnback.Visible = true;
            lblpreview.Text = template;
            btnaddnewrecord.Visible = false;
            GKeyMaster.Visible = false;
            ResetControls();
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region btnback_Click
    protected void btnback_Click(object sender, EventArgs e)
    {
        //lblheader.Text = "Site Template";
        //panelpreview.Visible = false;
        //panelsitetemplate.Visible = true;
        //btnback.Visible = false;
        //btnaddnewrecord.Visible = true;
        //GKeyMaster.Visible = true;
        Response.Redirect("Event_Settings_MasterPage?SHW=" + Request.QueryString["SHW"].ToString());
         
    }
    #endregion

    #region linkbtndefaultbanner_Click (right-sight "Default Banner" link button click event & bind data to "Template Value" textbox (txtvalue))
    protected void linkbtndefaultbanner_Click(object sender, EventArgs e)
    {
        //EmailHelpers eh = new EmailHelpers(fn);
        //string image = SiteDefaultValue.DefaultEmailBindDelimiter + SiteDefaultValue.DefaultSiteBanner + SiteDefaultValue.DefaultEmailBindDelimiter;
        //string imagetemplate = "<img alt='banner' src='image' width='1000px' />";
        //txtvalue.Text += imagetemplate;
    }
    #endregion

    #region imgdefaultbanner_Click (right-sight image(banner) click event & bind data to "Template Value" textbox (txtvalue))
    protected void imgdefaultbanner_Click(object sender, ImageClickEventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            //EmailHelpers eh = new EmailHelpers(fn);
            //string image = SiteDefaultValue.DefaultEmailBindDelimiter + SiteDefaultValue.DefaultSiteBanner + SiteDefaultValue.DefaultEmailBindDelimiter;
            //string imageurl = eh.BindTemplateWithStandardSetting(image, showid);
            //string imagetemplate = "<img alt='banner' src='" + imageurl + "' width='1000px' />";
            //txtvalue.Text += imagetemplate;
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region lbtntemp_Click (right-sight data field link button click event & bind data to "Template Value" textbox (txtvalue))
    protected void lbtntemp_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)(sender);
        string text = lb.Text;
        txtvalue.Text += SiteDefaultValue.DefaultEmailBindDelimiter + text + SiteDefaultValue.DefaultEmailBindDelimiter;
    }
    #endregion

    #region ResetControls (Clear data from controls)
    private void ResetControls()
    {
        txttempid.Text = "";
        txttempid.Enabled = true;
        txttemptitle.Text = "";
        txtsubject.Text = "";
        txtvalue.Text = "";
        lbls.Text = "";
    }
    #endregion
}