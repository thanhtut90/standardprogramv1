﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="PaymentUpdate.aspx.cs" Inherits="Admin_PaymentUpdate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

    function enablebutton() {
        var y = document.getElementById("ContentPlaceHolder1_txtamount").value;
        if (isNaN(y)) {
            alert("Please enter only number into the Payable Amount");
        }
        var z = document.getElementById("ContentPlaceHolder1_txtpaid").value;
        if (isNaN(z)) {
            alert("Please enter only number into the Paid Amount");
        }

        var paidamt = 0;
        var currentPaidAmount = document.getElementById("ContentPlaceHolder1_lblCurrentPaidAmount").innerText;
        if (!isNaN(currentPaidAmount)) {
            paidamt = parseFloat(Math.round(currentPaidAmount * 100) / 100).toFixed(2);
        }

        var a = y - (+z + +paidamt);
        document.getElementById("ContentPlaceHolder1_lbloutstanding").innerHTML = a;
    }
    function toggle() {
        var waived = document.getElementById("ContentPlaceHolder1_rbwaived");
        var trmethod = document.getElementById("trmethod");
        if (waived.checked == true) {
            trmethod.style.display = 'none';
            document.getElementById("ContentPlaceHolder1_lbloutstanding").innerHTML = '0';
//            rb1.disabled = true;
//            rb2.disabled = true;
//            rb3.disabled = true;
//            rb1.checked = false;
//            rb2.checked = false;
//            rb3.checked = false;
        }
        else {
            trmethod.style.display = '';
//            rb1.disabled = false;
//            rb2.disabled = false;
//            rb3.disabled = false;
        }
    }
    function toggleCreditFields1()
    {
        var rb1 = document.getElementById("ContentPlaceHolder1_rbpayment1");
        var rb2 = document.getElementById("ContentPlaceHolder1_rbpayment2");
        var rb3 = document.getElementById("ContentPlaceHolder1_rbpayment3");
        if (rb3.checked == true) {
            $("#ContentPlaceHolder1_txtCharges").removeAttr("readonly");
            $("#ContentPlaceHolder1_txtOthCharges").removeAttr("readonly");
            $("#ContentPlaceHolder1_txtCollectGST").removeAttr("readonly");
            $("#ContentPlaceHolder1_txtRecAmount").removeAttr("readonly");
        }
        else {
            $("#ContentPlaceHolder1_txtCharges").attr("readonly", true);
            $("#ContentPlaceHolder1_txtOthCharges").attr("readonly", true);
            $("#ContentPlaceHolder1_txtCollectGST").attr("readonly", true);
            $("#ContentPlaceHolder1_txtRecAmount").attr("readonly", true);
        }
        
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3 class="box-title">Update Payment</h3>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblRegnoLabel" runat="server" Text="RegID"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <asp:Label ID="lblRegno" runat="server" Text="Regno"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblPaymentStatusLabel" runat="server" Text="Payment Status"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <asp:Panel ID="Panel1" runat="server">
                                <input id="rbpaid" type="radio" name="status" runat="server" onchange="toggle();"/> Paid
                                &nbsp;&nbsp;<input id="rbunpaid" type="radio" name="status" runat="server" onchange="toggle();"/> Unpaid
                                <%--&nbsp;&nbsp;<input id="rbwaived" type="radio" name="status" runat="server" onchange="toggle();"/> Waived--%>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblPaymentMethodLabel" runat="server" Text="Payment Method"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <asp:Panel ID="Panel2" runat="server">
                                <asp:RadioButton ID="rbCreditCard" runat="server" GroupName="Method" Text="Credit Card"
                                     OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" />
                                <asp:RadioButton ID="rbTT" runat="server" GroupName="Method" Text="Telegraphic Transfer"
                                     OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" />
                                <asp:RadioButton ID="rbWaivedMethod" runat="server" GroupName="Method" Text="Waived"
                                     OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" />
                                <asp:RadioButton ID="rbCheque" runat="server" GroupName="Method" Text="Local Cheque"
                                     OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" />
                                <%--<input id="rbpayment3" type="radio" name="payment" runat="server"  /> Cheque
                                &nbsp;&nbsp;<input id="rbpayment2" type="radio" name="payment" runat="server"  /> Telegraphic Transfer
                                &nbsp;&nbsp;<input id="rbpayment1" type="radio" name="payment" runat="server"  /> Credit Card--%>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblSubTotalLabel" runat="server" Text="Sub-Total Amount"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <%=currency %> &nbsp;<asp:TextBox ID="txtSubTotal" runat="server" CssClass="form-control" Enabled="false" Text="0"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblTotalLabel" runat="server" Text="Total Amount"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <%=currency %>&nbsp;<input id="txtamount" runat="server" maxlength="50" type="text" onkeyup="enablebutton();" class="form-control" 
                                disabled="disabled"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblPayableAmountLabel" runat="server" Text="Payable Amount"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <%=currency %> &nbsp;<asp:TextBox ID="txtPayableAmount" runat="server" CssClass="form-control" Enabled="false" Text="0"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblPaidAmountLabel" runat="server" Text="Paid Amount"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <%--<%=currency %>&nbsp;<input id="txtpaid" runat="server" maxlength="50" type="text" onkeyup="enablebutton();" class="form-control"/>--%>
                            <%=currency %>&nbsp;<asp:TextBox ID="txtpaid" runat="server" MaxLength="50" CssClass="form-control" onkeyup="enablebutton();"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbpaid" runat="server" TargetControlID="txtpaid" FilterType="Numbers, Custom" ValidChars="." />
                        </div>
                        <div class="col-md-4">
                            <br />
                            <i>Current Paid Amount:<strong><%=currency %>&nbsp;<asp:Label ID="lblCurrentPaidAmount" runat="server" Text="0"></asp:Label></strong></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblReferencenoLabel" runat="server" Text="Reference no"></asp:Label><span style="color:red;">*</span>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtRefNo" maxlength="50" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvRefNo" runat="server"
                            ControlToValidate="txtRefNo" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblInvoicenoLabel" runat="server" Text="Invoice no"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtinvoice" maxlength="50" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblChargesLabel" runat="server" Text="Admin Charges"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <%=currency %>&nbsp; <asp:TextBox ID="txtAdminCharges" maxlength="50" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblOtherChargesLabel" runat="server" Text="Other Charges"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <%=currency %>&nbsp; <asp:TextBox ID="txtOthCharges" maxlength="50" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblCollectionGSTLabel" runat="server" Text="Collection GST"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <%=currency %>&nbsp; <asp:TextBox ID="txtCollectGST" maxlength="50" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <%--<div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblReceivingAmountLabel" runat="server" Text="Receiving Amount"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <%=currency %>&nbsp;<asp:TextBox ID="txtRecAmount" maxlength="50" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>--%>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblOutstandingAmountLabel" runat="server" Text="Outstanding Amount"></asp:Label>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-4">
                            <%=currency %>&nbsp;<asp:Label ID="lbloutstanding" runat="server" Text="0"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:Label ID="lblRemarksLabel" runat="server" Text="Remarks"></asp:Label><span style="color:red;">*</span>
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-6">
                            <asp:Label ID="lblemail" runat="server" Text="Label" Visible="False"></asp:Label>
                            <asp:TextBox ID="txtremarks" runat="server" TextMode="MultiLine" Columns="20" Rows="5" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvremarks" runat="server"
                            ControlToValidate="txtremarks" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <asp:Button ID="btnback" runat="server" Text="Back" onclick="btnback_Click" Visible="false" CssClass="btn btn-default" />
                            <asp:Button ID="btnsubmit" runat="server" Text="Update" onclick="btnsubmit_Click" CssClass="btn btn-warning" />
                            <asp:TextBox runat="server" Visible="false" Text="0" ID="regCountrySameWithShowCountry" ></asp:TextBox>
                            <asp:TextBox runat="server" Visible="false" Text="0" ID="txtDiscount" ></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

