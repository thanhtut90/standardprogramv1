﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Event_Flow_Success : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                txtShowID.Text = cFun.DecryptValue(Request.Params["SHW"].ToString());
                string flw = cFun.DecryptValue(Request.Params["FLW"].ToString());
                txtFlowID.Text = flw;

                FlowControler flwControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flw);
                if (!string.IsNullOrEmpty(flwMasterConfig.FlowSuccessMsg))
                {
                    txtSuccessMsg.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowSuccessMsg) ? flwMasterConfig.FlowSuccessMsg : "");
                }
                txtSuccessMsgTmpID.Text = flwMasterConfig.FlowSuccessTmpID.Trim();
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region btnUpdateFlowInfo_Click
    protected void btnUpdateFlowInfo_Click(object sender, EventArgs e)
    {
        try
        {
            bool rowUpdated = false;
            string tmpid = txtSuccessMsgTmpID.Text.Trim();
            FlowMaster fMaster = new FlowMaster();
            fMaster.FlowID = txtFlowID.Text;

            #region Create SuccessTemplate
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            TemplateControler tmpControler = new TemplateControler(fn);
            TemplateObj tmpObj = new TemplateObj();
            tmpObj.tempid = tmpid;
            tmpObj.tempname = "";
            tmpObj.tempvalue = Server.HtmlEncode(txtSuccessMsg.Text);
            tmpObj.tempDBSource = "";
            tmpObj.tempsubject = "";
            tmpObj.temptype = 3;
            tmpObj.showid = showid;

            bool rowUpdated1 = false;
            if (!string.IsNullOrEmpty(tmpid) && tmpid != "0")
            {
                rowUpdated1 = tmpControler.UpdateTemplateByID(tmpObj);
            }
            else
            {
                tmpid = "0";
                rowUpdated1 = tmpControler.CreateNewTemplate(tmpObj, ref tmpid);
            }

            if (rowUpdated1 && !string.IsNullOrEmpty(tmpid) && tmpid != "0")
            {
                fMaster.FlowSuccessTmpID = tmpid;
                FlowControler fControler = new FlowControler(fn);
                rowUpdated = fControler.UpdateFlowMasterSucMsg(fMaster);
            }
            #endregion

            if (rowUpdated)
            {
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
                FlowControler fControl = new FlowControler(fn);
                string curstep = cFun.DecryptValue(Request.QueryString["STP"].ToString());
                Flow flw = new Flow();
                flw.FlowID = flowid;
                flw.FlowConfigStatus = YesOrNo.Yes;
                flw.FlowStep = curstep;
                fControl.UpdateFlowIndivConfigStatus(flw);

                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success.');", true);
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fail');", true);
                return;
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region btnNext_Click
    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (Request.Params["FLW"] != null)
            {
                string flowid = cFun.DecryptValue(urlQuery.FlowID);
                string curstep = cFun.DecryptValue(urlQuery.CurrIndex);
                Dictionary<string, string> nValues = new Dictionary<string, string>();
                FlowControler fControl = new FlowControler(fn);

                if (Request.QueryString["a"] == null)
                {
                    string stp = cFun.DecryptValue(Request.QueryString["STP"].ToString());
                    nValues = fControl.GetAdminNextRoute(flowid, stp);
                    if (nValues.Count > 0)
                    {
                        //*Update Currenct Flow Step Status
                        Flow flw = new Flow();
                        flw.FlowID = flowid;
                        flw.FlowConfigStatus = YesOrNo.Yes;
                        flw.FlowStep = curstep;
                        fControl.UpdateFlowIndivConfigStatus(flw);
                        //*

                        string page = nValues["nURL"].ToString();
                        string step = nValues["nStep"].ToString();
                        string FlowID = nValues["FlowID"].ToString();

                        if (page == "") page = "Event_Config_Final";

                        string path = fControl.MakeFullURL(page, FlowID, showid, "", step);

                        Response.Redirect(path);
                    }
                }
                else
                {
                    string page = "Event_Flow_Dashboard";
                    string path = fControl.MakeFullURL(page, flowid, showid);
                    Response.Redirect(path);
                }
            }
        }
    }
    #endregion


    protected void btnSetupEmail_Onclick(object sender, EventArgs e)
    {
        string url = "Event_Flow_EmailConfig?" +Request.QueryString;
        Response.Redirect(url);
    }
}