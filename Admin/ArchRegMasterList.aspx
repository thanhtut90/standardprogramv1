﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/ArchAdmin.master" AutoEventWireup="true" CodeFile="ArchRegMasterList.aspx.cs" Inherits="ArchRegMasterList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../Content/Default/bootstrap.css" rel="stylesheet" />
    <link href="../Content/Default/Site.css" rel="stylesheet" />
    <script src="js/plugins/jquery-1.9.1.min.js"></script>
    <script src="../Content/bootstrap/js/bootstrap.min.js"></script>

    <style>
        table,
        table tr,
        table td,
        table th {
            border-color: black !important;
        }

        body {
            font-family: Tahoma, sans-serif !important;
            font-weight: normal !important;
            font-size: 11pt !important;
        }

        .CenterHeader {
            text-align: center !important;
            font-size: 12pt !important;
            vertical-align: central !important;
        }

        td {
            vertical-align: central;
        }

        tr:first-child {
            background-color: gray;
            border: solid 1px #4f4f4f !important;
        }

        tr:nth-child(even) {
            background-color: #DBDBDB;
            border: solid 1px #4f4f4f !important;
        }

        tr:nth-child(odd) {
            background-color: #EDEDED;
            border: solid 1px #4f4f4f !important;
        }

        .btn-grey {
            background: linear-gradient(to bottom, #CCCCCC, #C1C1C1);
            border-color: #A5A5A5 !important;
            border-radius: 0 !important;
        }

        .grey-header {
            background-color: #808080 !important;
            border: solid 1px #4f4f4f !important;
            color: black !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <div class="container-fluid">
                    <img src="http://203.125.155.188/StandardRegWeb_2016/images/site/WVF342201821415403.png" width="350" height="76" />
                    <h3 style="text-align: center;" runat="server" id="lblReportTitle"><strong>Architect Expo 2018 Registration List</strong><asp:Button runat="server" ID="btnLogOut" OnClick="btnLogOut_Click" CssClass="btn btn-default btn-grey btn-sm pull-right" Style="margin-right: 15px;" Text="Log Out" /></h3>
                    <div class="row col-md-12" style="padding-right: 0; min-height: 132px;">
                        <div class="col-md-9" style="min-width: 100px; min-height: 120px;">
                            <ul style="position: absolute; right: 0; bottom: 0; list-style: none; margin-right: 15px;">
                                <li style="display: inline-block;">
                                    <asp:Button runat="server" ID="btnReset" Text="Reset" CssClass="btn btn-default btn-grey" OnClick="btnReset_Click" Visible="false" /></li>
                                <li style="display: inline-block;">
                                    <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="btn btn-default btn-grey" OnClick="btnSearch_Click" /></li>
                                <li style="display: inline-block;">
                                    <asp:TextBox runat="server" ID="tbFilter" CssClass="form-control" placeholder="Search..." Width="200px" Height="34px"></asp:TextBox></li>
                            </ul>
                        </div>
                        <div class="col-md-3" style="padding-right: 0;">
                            <table runat="server" id="tblRegInfo" class="table table-bordered tableFig" style="border-color: black !important;">
                                <tr>
                                    <th runat="server" id="thThaiRegCount" style="text-align: right; vertical-align: central;">Thailand Register</th>
                                    <th runat="server" id="tdThaiRegCount" style="text-align: center; vertical-align: central; width: 100px;">1,000</th>
                                </tr>
                                <tr>
                                    <th runat="server" id="thOverseaRegCount" style="text-align: right; vertical-align: central;">Foreigner Register</th>
                                    <th runat="server" id="tdOverseaRegCount" style="text-align: center; vertical-align: central; width: 100px;">1</th>
                                </tr>
                                <tr>
                                    <th runat="server" id="thRegTotalCount" style="text-align: right; vertical-align: central;">Total</th>
                                    <th runat="server" id="tdRegTotalCount" style="text-align: center; vertical-align: central; width: 100px;">1,001</th>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div class="row col-md-12" style="padding-right: 0;">
                        <div class="col-md-9">
                            <ul style="float: right; list-style: none;">
                                <li runat="server" style="margin-bottom: 5px;" visible="false">
                                    <asp:Button runat="server" ID="btnExportMasterList" Text="Export All Visitor" CssClass="btn btn-default btn-grey" OnClick="btnExportMasterList_Click" Width="174px" Height="34px" /></li>
                                <li style="margin-bottom: 5px;">
                                    <asp:Button runat="server" ID="btnExportVisitorIndv" Text="Export Individual Visitor" CssClass="btn btn-default btn-grey" OnClick="btnExportVisitorIndv_Click" Width="174px" Height="34px" /></li>
                                <li style="margin-top: 5px; margin-bottom: 5px;">
                                    <asp:Button runat="server" ID="btnExportVisitorGroup" Text="Export Group Visitor" CssClass="btn btn-default btn-grey" OnClick="btnExportVisitorGroup_Click" Width="174px" Height="34px" /></li>
                            </ul>
                        </div>

                        <div class="col-md-3" style="padding-right: 0;">
                            <table runat="server" id="tblVisitorsInfo" class="table table-bordered tableFig" style="border-color: black !important;">
                                <tr>
                                    <th runat="server" id="thIndVisitorCount" style="text-align: right; vertical-align: central;">Individual Visitors</th>
                                    <th runat="server" id="tdIndVisitorCount" style="text-align: center; vertical-align: central; width: 100px;">1,234</th>
                                </tr>
                                <tr>
                                    <th runat="server" id="thOverseaVisitorCount" style="text-align: right; vertical-align: central;">Group Visitors</th>
                                    <th runat="server" id="tdOverseaVisitorCount" style="text-align: center; vertical-align: central; width: 100px;">0</th>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <asp:Panel ID="pnllist" CssClass="table table-responsive" runat="server" Visible="true" Style="padding: 15px;">
                        <asp:HiddenField runat="server" ID="hfQuery" Visible="false" />
                        <asp:GridView runat="server" ID="gvRegReport" AllowPaging="true" DataKeyNames="RegNo" AutoGenerateColumns="false" PageSize="50" Width="100%"
                            OnRowCommand="gvRegReport_RowCommand"
                            OnDataBound="gvRegReport_DataBound"
                            OnPageIndexChanging="gvRegReport_PageIndexChanging" ShowHeaderWhenEmpty="true">
                            <PagerStyle BorderStyle="None" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />
                            <HeaderStyle CssClass="grey-header" />
                            <Columns>
                                <asp:BoundField DataField="sn" HeaderText="No." HeaderStyle-CssClass="CenterHeader" />
                                <asp:BoundField DataField="reg_datecreated" HeaderText="Date" HeaderStyle-CssClass="CenterHeader" />
                                <asp:BoundField DataField="refID" HeaderText="Reference ID" HeaderStyle-CssClass="CenterHeader" />
                                <asp:BoundField DataField="salutation" HeaderText="Title" HeaderStyle-CssClass="CenterHeader" />
                                <asp:BoundField DataField="reg_FName" HeaderText="First Name" HeaderStyle-CssClass="CenterHeader" />
                                <asp:BoundField DataField="reg_LName" HeaderText="Last Name" HeaderStyle-CssClass="CenterHeader" />
                                <asp:BoundField DataField="reg_OName" HeaderText="Company" HeaderStyle-CssClass="CenterHeader" />
                                <asp:TemplateField HeaderText="Email" HeaderStyle-Width="22%" HeaderStyle-CssClass="CenterHeader">
                                    <ItemTemplate>
                                        <div class="col-md-9" style="padding: 0;">
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("reg_Email") %>'></asp:Label>
                                        </div>
                                        <asp:Button ID="btnReSendEmail" runat="server" CssClass="btn btn-default btn-sm btn-grey" Text="Resend" OnClick="btnReSendEmail_Click" CommandName="ReSend" Visible="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="reg_Tel" HeaderText="Tel." HeaderStyle-CssClass="CenterHeader" />
                                <asp:BoundField DataField="reg_Mobile" HeaderText="Mobile" HeaderStyle-CssClass="CenterHeader" />
                                <asp:BoundField DataField="country" HeaderText="Country" HeaderStyle-Width="100px" HeaderStyle-CssClass="CenterHeader" />
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </div>

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSearch" />
                <asp:PostBackTrigger ControlID="btnReset" />
                <asp:PostBackTrigger ControlID="btnExportVisitorIndv" />
                <asp:PostBackTrigger ControlID="btnExportVisitorGroup" />
                <asp:PostBackTrigger ControlID="btnExportMasterList" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

