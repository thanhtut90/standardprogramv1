﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Flow_Dashboard.aspx.cs" Inherits="Event_Flow_Dashboard" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .page-header {
            font-size: 18px !important;
            border-bottom: 0 !important;
        }

        .box-header .box-title {
            margin: 10px 0 20px 0;
            font-size: 22px !important;
        }

        .box-header {
            padding: 0px !important;
        }
        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
          padding:1px !important;
        }
    </style>

    <script src="js/plugins/jquery-1.9.1.min.js"></script>
    <script src="Content/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }

        function closeModal() {
            $('#myModal').modal('hide');
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="centercontent">
        <div class="row" >
            <div class="col-lg-7 content-header">
                <h1>Manage Registration Steps</h1>
            </div>
        </div>
        <div id="contentwrapper" class="contentwrapper" style="padding-top: 20px;">
            <div>
                <asp:HiddenField ID="hfPageIndexID" runat="server" Value="" />
                <div class='page-header' style="padding-bottom:1px;">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Step</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Preview</th>
                                <th>Edit</th>
                                <th>Edit Page Header</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater runat="server" ID="RptSteps" OnItemDataBound="RptSteps_ItemDataBound">
                                <ItemTemplate>
                                    <asp:Panel runat="server" ID="PanelRow">
                                        <tr>
                                            <td><span class="fa-stack fa-lg">
                                                <i class="fa fa-bookmark fa-stack-1x"></i>
                                                <i class="fa fa-inverse fa-stack-1x"><%#Eval("step_seq")%></i>
                                            </span></td>
                                            <td><%#Eval("script_desc")%></td>
                                            <td>&nbsp&nbsp&nbsp
                                                <asp:Label runat="server" Visible="false" ID="lblConfigPending"><span class="fa  fa-battery-quarter "> </span> </asp:Label>
                                                <asp:Label runat="server" ID="lblConfigFinished" Visible="true"><span class="glyphicon glyphicon-ok"> </span> </asp:Label>
                                                <asp:Label runat="server" Visible="false" ID="lblConfigStatus" Text='<%#Eval("flow_isConfigured")%>'></asp:Label>

                                            </td>

                                            <td>
                                                <asp:LinkButton runat="server" CommandArgument='<%#Eval("backendpreview_scriptID") + ";" + Eval("step_seq") + ";" + Eval("step_desc")%>'
                                                    CommandName="previewPage"
                                                    CssClass="btn btn-primary"
                                                    OnClick="btnEdit_Onclick" Visible='<%#isPreviewable(Eval("backendpreview_scriptID").ToString())%>'>
                                                    <span aria-hidden="true" class="glyphicon glyphicon-eye-open"></span> Preview
                                                </asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%#Eval("backendref_scriptID") + ";" + Eval("step_seq") + ";" + Eval("step_desc")%>'
                                                    CommandName="editPage"
                                                    CssClass="btn btn-success"
                                                    OnClick="btnEdit_Onclick" Visible='<%#isEditable(Eval("backendref_scriptID").ToString())%>'>
                                                    <span aria-hidden="true" class="glyphicon glyphicon-edit"></span> Edit
                                                </asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="btnChangePageHeader" runat="server" CommandArgument='<%#Eval("step_seq") + ";" + Eval("flow_step_index")%>'
                                                    CommandName="editPageHeader"
                                                    CssClass="btn btn-warning"
                                                    OnClick="btnChangePageHeader_Click">
                                                    <span aria-hidden="true" class="glyphicon glyphicon-edit"></span> Edit Page Header
                                                </asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lblAddFlowNote" runat="server" CommandArgument='<%#Eval("flow_step_index") + ";" + Eval("step_seq") + ";" + Eval("flowNote_scriptID")%>'
                                                    CommandName="addFlowNote"
                                                    CssClass="btn btn-success"
                                                    OnClick="lblAddFlowNote_Click" Visible='<%#isFlowNoteAddable(Eval("flowNote_scriptID").ToString())%>'>
                                                    <span aria-hidden="true" class="glyphicon glyphicon-edit"></span> Add Flow Note
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                    <!-- /.box-body -->
                </div>


                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Page Header Changing</h4>
                            </div>
                            <div class="modal-body">
                                Enter Page Header Name
                                <asp:TextBox ID="txtPageHeaderName" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="btnClose" runat="server" class="btn btn-default" Text="Close" OnClick="btnClose_Click" CausesValidation="false"/>
                                <asp:Button ID="btnUpdatePageName" runat="server" class="btn btn-primary" Text="Update" OnClick="btnUpdatePageName_Click"/>
                            </div>
                        </div>
                    </div>
                </div>

      
                <!-- Default box -->
                <div class="box">
                          <div class="box-body">
                        <div class='page-header'>
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <asp:Label ID="lblFlowName" runat="server" CssClass="col-md-2 control-label">Flow Name</asp:Label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtFlowName" runat="server" Enabled="false"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvFlowName" runat="server" ControlToValidate="txtFlowName" ForeColor="Red"
                                                ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                        </div> <asp:LinkButton ID="btnFinalizeFlow" runat="server" OnClick="btnFinalizeFlow_Click" CssClass="btn btn-danger">
                                                <span aria-hidden="true" class="glyphicon glyphicon-ok"></span> Finish Setup
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnUpdateFlowInfo" runat="server" OnClick="btnUpdateFlowInfo_Click" CssClass="btn btn-success btn-lg">
                                                <span aria-hidden="true" class="glyphicon glyphicon-edit"></span> Update
                                            </asp:LinkButton>


                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lblWelcomeMsg" runat="server" CssClass="col-md-2 control-label">Welcome Page Message</asp:Label>
                                        <div class="col-md-8">
                                            <CKEditor:CKEditorControl ID="txtWelcomeMsg" runat="server" Height="300px" Width="98%"
                                                BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" runat="server" CssClass="col-md-2 control-label">Welcome Page Message Under Registration Button</asp:Label>
                                        <div class="col-md-8">
                                            <CKEditor:CKEditorControl ID="txtWelcomeMsgUnderRegButton" runat="server" Height="300px" Width="98%"
                                                BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lblReloginIndexIncompleteMsg" runat="server" CssClass="col-md-2 control-label">Relogin Index Incomplete Page Message</asp:Label>
                                        <div class="col-md-8">
                                            <CKEditor:CKEditorControl ID="txtReloginIndexIncompleteMsg" runat="server" Height="300px" Width="98%"
                                                BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lblReloginHomeCompleteMsg" runat="server" CssClass="col-md-2 control-label">Relogin Home Complete Page Message</asp:Label>
                                        <div class="col-md-8">
                                            <CKEditor:CKEditorControl ID="txtReloginHomeCompleteMsg" runat="server" Height="300px" Width="98%"
                                                BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2">&nbsp;</div>
                                        <div class="col-md-8">
                                        
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
           
                </div>
                <!-- /.box -->
                <!-- Default box -->
            </div>
            <div style="padding-left: 30%;">
                <asp:TextBox runat="server" ID="txtSelectedRoute" Visible="false"></asp:TextBox>
                <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
                <asp:TextBox runat="server" ID="txtFlowID" Visible="false" Text=""></asp:TextBox>
                <asp:TextBox runat="server" ID="txtWelcomeMsgTmpID" Visible="false" Text=""></asp:TextBox>
                <asp:TextBox runat="server" ID="txtWelcomeMsgUnderRegButtonTmpID" Visible="false" Text=""></asp:TextBox>
                <asp:TextBox runat="server" ID="txtReloginIndexIncompleteMsgTmpID" Visible="false" Text=""></asp:TextBox>
                <asp:TextBox runat="server" ID="txtReloginHomeCompleteMsgTmpID" Visible="false" Text=""></asp:TextBox>
                <telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>
            </div>

        </div>
    </div>
</asp:Content>

