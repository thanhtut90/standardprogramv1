﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Utilities;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.BackendMaster;

public partial class Admin_Event_Settings_PaymentSettings : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    static string _currency = "Currency";
    static string _adminfee = "adminfee";
    static string _gstfee = "gstfee";
    static string _allowpartialpayment = "AllowPartialPayment";
    static string _maxpromousableitempertran = "maxpromousableitempertran";
    static string _hasVisa = "hasVisa";
    static string _cctext = "creditcardtext";
    static string _ttext = "tttext";
    static string _waivedtext = "waivedtext";
    static string _chequetext = "chequetext";
    static string _ttadminfee = "ttadminfee";
    static string _ProjectID = "ProjectID";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                if (!checkCreated(showid))
                {
                    Response.Redirect("Event_Config");
                }

                bool isCreatedPayMethod = checkCreatedPaymentMethod(showid);
                if (!isCreatedPayMethod)
                {
                    InsertNewPaymentMethod(showid);
                }

                bindcurrency(showid);
                bindPaymentMethod(showid);
                bindShowName(showid);
                binddata(showid);
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void btnCreateNewPPortal_Onclick(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string url = string.Format("Event_Settings_PaymentPortalConfig?SHW={0}", Request.Params["SHW"].ToString());
            Response.Redirect(url);
        }
    }

    #region binddata (bind data from tb_site_settings to related controls)
    protected void binddata(string showid)
    {
        string query = string.Empty;
        string currency = string.Empty;
        string adminfee = string.Empty;
        string gstfee = string.Empty;
        int allowPartialPayment = 0;
        string maxitempertrans = string.Empty;
        int hasVisa = 0;
        string chequetext = string.Empty;
        string cctext = string.Empty;
        string ttadminfee = string.Empty;
        string projectID = string.Empty;

        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        query = "Select * From tb_site_settings Where ShowID=@SHWID";

        DataTable dt = new DataTable();
        dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

        for (int x = 0; x < dt.Rows.Count; x++)
        {
            string settingname = dt.Rows[x]["settings_name"].ToString();

            if (settingname == _currency)
            {
                currency = dt.Rows[x]["settings_value"].ToString();
                try
                {
                    if (!String.IsNullOrEmpty(currency))
                    {
                        //ListItem listItem = ddlCurrency.Items.FindByValue(currency);
                        //if (listItem != null)
                        //{
                        //    ddlCurrency.ClearSelection();
                        //    listItem.Selected = true;
                        //}
                        txtCurrency.Text = currency;
                    }
                }
                catch (Exception ex)
                {
                }
            }

            if (settingname == _ProjectID)
            {
                projectID = !string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : "";
                //  txtProjectID.Text = projectID;
                ListItem li = txtProjectID.Items.FindByValue(projectID);
                if (li != null) li.Selected = true;
            }

            if (settingname == _adminfee)
            {
                adminfee = !string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? (Convert.ToDouble(dt.Rows[x]["settings_value"].ToString()) * 100).ToString() : adminfee;
                txtAdminFee.Text = adminfee;
            }
            if (settingname == _gstfee)
            {
                gstfee = !string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? (Convert.ToDouble(dt.Rows[x]["settings_value"].ToString()) * 100).ToString() : gstfee;
                txtGSTFee.Text = gstfee;
            }
            if (settingname == _ttadminfee)
            {
                ttadminfee = !string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? (Convert.ToDouble(dt.Rows[x]["settings_value"].ToString())).ToString() : gstfee;
                txtTTAdminFee.Text = ttadminfee;
            }
            if (settingname == _allowpartialpayment)
            {
                allowPartialPayment = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : "0");
                if (allowPartialPayment == 1)
                {
                    rbPartial1.Checked = false;
                    rbPartial2.Checked = true;
                }
                else
                {
                    rbPartial1.Checked = true;
                    rbPartial2.Checked = false;
                }
            }
            if (settingname == _maxpromousableitempertran)
            {
                maxitempertrans = dt.Rows[x]["settings_value"].ToString();
                txtMaxItemPerTrans.Text = maxitempertrans;
            }
            //if (settingname == _hasVisa)
            //{
            //    hasVisa = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : "0");
            //}
            if (settingname == _cctext)
            {
                cctext = dt.Rows[x]["settings_value"].ToString();
                txtCreditCard.Text = Server.HtmlDecode(cctext);
            }
            if (settingname == _ttext)
            {
                cctext = dt.Rows[x]["settings_value"].ToString();
                txtTT.Text = Server.HtmlDecode(cctext);
            }
            if (settingname == _waivedtext)
            {
                cctext = dt.Rows[x]["settings_value"].ToString();
                txtWaived.Text = Server.HtmlDecode(cctext);
            }
            if (settingname == _chequetext)
            {
                chequetext = dt.Rows[x]["settings_value"].ToString();
                txtCheque.Text = Server.HtmlDecode(chequetext);
            }
        }
    }
    #endregion

    #region checkCreatedPaymentMethod (check the record count of tmp_refPaymentMethod table is equal to the record counts of ref_PaymentMethod table)
    private bool checkCreatedPaymentMethod(string showid)
    {
        bool isCreated = false;

        CommonDataObj comObj = new CommonDataObj(fn);
        DataTable dt = comObj.getTmpPaymentMethod();

        SetUpController setupCtr = new SetUpController(fn);
        DataTable dtPayMethod = setupCtr.checkPaymentMethodCreated(showid);
        if (dtPayMethod.Rows.Count >= dt.Rows.Count)
        {
            isCreated = true;
        }

        return isCreated;
    }
    #endregion

    #region bindcurrency(bind currency data from ref_Currency table to ddlCurrency dropdownlist)
    protected void bindcurrency(string showid)
    {
        //DataSet ds = new DataSet();
        //ds = fn.GetDatasetByCommand("Select * From ref_Currency", "ds");
        //if (ds.Tables[0].Rows.Count != 0)
        //{
        //    for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
        //    {
        //        ddlCurrency.Items.Add(ds.Tables[0].Rows[x]["currency_name"].ToString());
        //        ddlCurrency.Items[x].Value = ds.Tables[0].Rows[x]["currency_display"].ToString();
        //    }
        //}

        string query = "select PaymentID,PaymentDesc from tb_site_PaymentConfig where SHOWID in ('DEFAULT',@SHWID) Order By ShowID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);
        DataTable dtList = fn.GetDatasetByCommand(query, "DT", pList).Tables["DT"];
        for (int x = 0; x < dtList.Rows.Count; x++)
        {
            txtProjectID.Items.Add(dtList.Rows[x]["PaymentDesc"].ToString());
            txtProjectID.Items[x].Value = dtList.Rows[x]["PaymentID"].ToString();
        }
    }

    protected void PaymentPortal_OnChange(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            string query = "select * from tb_site_PaymentConfig where SHOWID=@SHWID And PaymentID=@PaymentID Order By ShowID";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            SqlParameter spar1 = new SqlParameter("PaymentID", SqlDbType.NVarChar);
            spar.Value = showid;
            spar1.Value = txtProjectID.SelectedItem.Value;
            pList.Add(spar);
            pList.Add(spar1);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
            string currency = "SGD";
            if (dt.Rows.Count > 0)
            {
                currency = dt.Rows[0]["Currency"].ToString();
            }
            txtCurrency.Text = currency;
        }
    }
    #endregion

    /// <summary>
    /// if the payment method table change something, need to edit the PaymentType class in Corpit.Registration project
    /// ***Note "method_usedid" shouldn't null or empty because this id is really used by Comfirmation page
    /// </summary>
    ///
    #region bindPaymentMethod (bind Payment Methods from ref_PaymentMethod table to Payment Method Checkboxlist(chkPaymentMethod))
    private void bindPaymentMethod(string showid)
    {
        SetUpController setupCtr = new SetUpController(fn);
        DataTable dt = setupCtr.getPaymentMethodByShowID(showid);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                //ListItem item = new ListItem();
                //item.Text = dr["method_name"].ToString();
                //item.Value = dr["method_usedid"].ToString();
                //item.Selected = Convert.ToBoolean(dr["method_active"]);
                //this.chkPaymentMethod.Items.Add(item);

                string usedid = dr["method_usedid"].ToString();
                string strIsActive = dr["method_active"] != null ? dr["method_active"].ToString() : YesOrNo.No;
                bool isActive;
                isActive = CommonFuns.BoolParser.GetValue(strIsActive);

                if (usedid == ((int)PaymentType.CreditCard).ToString())
                {
                    chkCreditCard.Checked = isActive;
                }
                else if (usedid == ((int)PaymentType.TT).ToString())
                {
                    chkTT.Checked = isActive;
                }
                else if (usedid == ((int)PaymentType.Waved).ToString())
                {
                    chkWaived.Checked = isActive;
                }
                else if (usedid == ((int)PaymentType.Cheque).ToString())
                {
                    chkCheque.Checked = isActive;
                }
            }
        }
    }
    #endregion

    #region InsertNewPaymentMethod and using ref_PaymentMethod table & Insert new blank records(method_name,method_usedid,method_sortorder) into ref_PaymentMethod table
    protected void InsertNewPaymentMethod(string showid)
    {
        try
        {
            string description = string.Empty;
            int isactive = 0;

            CommonDataObj comObj = new CommonDataObj(fn);
            DataTable dt = comObj.getTmpPaymentMethod();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string methodName = dr["method_name"].ToString();
                    string methodUsedID = dr["method_usedid"].ToString();
                    string methodSortOrder = dr["method_sortorder"].ToString();

                    SetUpController setupCtr = new SetUpController(fn);
                    DataTable dtPayMethod = setupCtr.getPaymentMethodByShowIDUsedID(methodUsedID, showid);
                    if (dtPayMethod.Rows.Count == 0)
                    {
                        PaymentMethodObj payObj = new PaymentMethodObj();
                        payObj.method_name = methodName;
                        payObj.method_usedid = Convert.ToInt32(methodUsedID);
                        payObj.method_description = description;
                        payObj.method_active = isactive;
                        payObj.method_sortorder = Convert.ToInt32(methodSortOrder);
                        payObj.ShowID = showid;
                        int rowInserted = setupCtr.insertPaymentMethod(payObj);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            pnlmessagebar.Visible = true;
            lblemsg.Text = "Error Occur :" + ex.Message;
        }
    }
    #endregion

    #region checkCreated (check the record count of tmp_tbSiteSettings table is equal to the record counts of tb_site_settings table)
    private bool checkCreated(string showid)
    {
        bool isCreated = false;
        DataTable dt = fn.GetDatasetByCommand("Select * From tmp_tbSiteSettings", "ds").Tables[0];

        string query = "Select * From tb_site_settings Where ShowID=@SHWID And settings_name In (Select settings_name From tmp_tbSiteSettings)";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dtSettings = fn.GetDatasetByCommand(query, "dsSettings", pList).Tables[0];
        if (dt.Rows.Count == dtSettings.Rows.Count)
        {
            isCreated = true;
        }

        return isCreated;
    }
    #endregion

    #region bindShowName
    private void bindShowName(string showid)
    {
        try
        {
            ShowController shwCtr = new ShowController(fn);
            DataTable dt = shwCtr.getShowByID(showid);
            if (dt.Rows.Count > 0)
            {
                string showName = dt.Rows[0]["SHW_Name"].ToString();
                txtEventName.Text = showName == "0" ? string.Empty : showName;
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region SaveForm (Save data into related tb_site_settings table and ref_PaymentMethod table)
    protected void SaveForm(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            try
            {
                string sql = string.Empty;
                string eventname = string.Empty;
                string currency = string.Empty;
                Double adminfee;
                Double gstfee;
                Double ttadminfee;
                string maxitempertrans = string.Empty;
                string cctext = string.Empty;
                string tttext = string.Empty;
                string waivedtext = string.Empty;
                string chequetext = string.Empty;
                int allowPartialPayment = 0;
                string projectID = string.Empty;

                eventname = cFun.solveSQL(txtEventName.Text.ToString());
                currency= txtCurrency.Text.ToString();
                adminfee = !string.IsNullOrEmpty(txtAdminFee.Text) && !string.IsNullOrWhiteSpace(txtAdminFee.Text) ? Convert.ToDouble(txtAdminFee.Text) / 100 : 0;
                gstfee = !string.IsNullOrEmpty(txtGSTFee.Text) && !string.IsNullOrWhiteSpace(txtGSTFee.Text) ? Convert.ToDouble(txtGSTFee.Text) / 100 : 0;
                ttadminfee = !string.IsNullOrEmpty(txtTTAdminFee.Text) && !string.IsNullOrWhiteSpace(txtTTAdminFee.Text) ? Convert.ToDouble(txtTTAdminFee.Text) : 0;
                maxitempertrans = txtMaxItemPerTrans.Text.Trim();

                if (rbPartial1.Checked == true)
                {
                    allowPartialPayment = 0;
                }
                else if (rbPartial2.Checked == true)
                {
                    allowPartialPayment = 1;
                }

                cctext = Server.HtmlEncode(txtCreditCard.Text);
                tttext = Server.HtmlEncode(txtTT.Text);
                waivedtext = Server.HtmlEncode(txtWaived.Text);
                chequetext = Server.HtmlEncode(txtCheque.Text);

                projectID = txtProjectID.Text.Trim();

                #region Inserting data into tb_Show
                ShowController shwCtr = new ShowController(fn);
                ShowObj sObj = new ShowObj();
                sObj.showid = showid;
                sObj.showname = eventname;
                bool rowInserted = shwCtr.UpdateShowName(sObj);
                #endregion

                sql += "Update tb_site_settings Set settings_value='" + currency + "' Where settings_name='" + _currency + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + adminfee + "' Where settings_name='" + _adminfee + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + gstfee + "' Where settings_name='" + _gstfee + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + allowPartialPayment + "' Where settings_name='" + _allowpartialpayment + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + maxitempertrans + "' Where settings_name='" + _maxpromousableitempertran + "' And ShowID='" + showid + "';";
                //sql += "Update tb_site_settings Set settings_value='" + hasVisa + "' Where settings_name='" + _hasVisa + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + cctext + "' Where settings_name='" + _cctext + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + tttext + "' Where settings_name='" + _ttext + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + waivedtext + "' Where settings_name='" + _waivedtext + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + chequetext + "' Where settings_name='" + _chequetext + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + ttadminfee + "' Where settings_name='" + _ttadminfee + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + projectID + "' Where settings_name='" + _ProjectID + "' And ShowID='" + showid + "';";
                fn.ExecuteSQL(sql);

                for (int i = 1; i <= 4; i++)
                {
                    int usedid = 0;
                    int isActive = 0;
                    if (i == (int)PaymentType.CreditCard)
                    {
                        usedid = (int)PaymentType.CreditCard;
                        isActive = chkCreditCard.Checked == true ? 1 : 0;
                    }
                    if (i == (int)PaymentType.TT)
                    {
                        usedid = (int)PaymentType.TT;
                        isActive = chkTT.Checked == true ? 1 : 0;
                    }
                    if (i == (int)PaymentType.Waved)
                    {
                        usedid = (int)PaymentType.Waved;
                        isActive = chkWaived.Checked == true ? 1 : 0;
                    }
                    if (i == (int)PaymentType.Cheque)
                    {
                        usedid = (int)PaymentType.Cheque;
                        isActive = chkCheque.Checked == true ? 1 : 0;
                    }

                    SetUpController setupCtr = new SetUpController(fn);
                    PaymentMethodObj payObj = new PaymentMethodObj();
                    payObj.method_active = isActive;
                    payObj.method_usedid = usedid;
                    payObj.ShowID = showid;
                    int rowUpdated = setupCtr.updatePaymentMethodActive(payObj);
                }

                pnlmessagebar.Visible = true;
                lblemsg.Text = "Updated Successfully";
                showid = cFun.EncryptValue(showid);
                Response.Redirect("Event_Settings_MasterPage?SHW=" + showid);
            }
            catch (Exception ex)
            {
                pnlmessagebar.Visible = true;
                lblemsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region lnkCurrency_Click (redirect to ManageCurrency.aspx)
    protected void lnkCurrency_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            Response.Redirect("ManageCurrency.aspx?SHW=" + Request.QueryString["SHW"].ToString());
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region Back
    protected void Back(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            Response.Redirect("Event_Settings_MasterPage?SHW=" + showid);
        }
        else
        {
            Response.Redirect("Login");
        }
    }
    #endregion
}