﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using Telerik.Web.UI;
using System.Text;
using Corpit.Site.Utilities;
using Corpit.Payment;

public partial class Admin_MasterRegistrationList_Member : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    QuesFunctionality qfn = new QuesFunctionality();
    CommonFuns cFun = new CommonFuns();

    protected string htmltb;
    static StringBuilder StrBuilder = new StringBuilder();
    static string _Company = "Company";
    static string _CreateDate = "CreateDate";
    static string _Industry = "Industry";
    static string _StateProvince = "StateProvince";

    static string _VisitDate = "Visit Date";
    static string _VisitTime = "Visit Time";

    string showID = string.Empty;
    string flowID = string.Empty;
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _Designation = "Designation";
    static string _Department = "Department";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _City = "City";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    static string noInvoice = "-10";
    private static string[] checkingShowIDNotToShowOtherButton = new string[] { "IDEM Online Registration" };//***17-6-2019
    private static string VIPNomationFlowsMDA = "'F470'";
    #endregion

    #region  Company Declaration

    static string _Name = "Name";
    static string _CAddress1 = "Address1";
    static string _CAddress2 = "Address2";
    static string _CAddress3 = "Address3";
    static string _CCity = "City";
    static string _State = "State";
    static string _ZipCode = "Zip Code";
    static string _CCountry = "Country";
    static string _TelCC = "Telcc";
    static string _TelAC = "Telac";
    static string _CTel = "Tel";
    static string _FaxCC = "Faxcc";
    static string _FaxAC = "Faxac";
    static string _CFax = "Fax";
    static string _CEmail = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Website = "Website";
    static string _Additional1 = "Additional1";
    static string _Additional2 = "Additional2";
    static string _Additional3 = "Additional3";
    static string _CAdditional4 = "Additional4";
    static string _CAdditional5 = "Additional5";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                string userID = Session["userid"].ToString();
                if (!string.IsNullOrEmpty(userID))
                {
                    binddata();
                    if (Session["roleid"].ToString() != "1")
                    {
                        lblUser.Text = userID;
                        getShowID(userID);
                        showList.Visible = false;
                        string constr = fn.ConnString;
                        using (SqlConnection con = new SqlConnection(constr))
                        {
                            int Fcount = CheckGFlowExist(getShowID(userID));
                            if (Fcount > 0)
                            {
                                using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Name From tb_site_flow_master where ShowID = '" + showID + "' and FLW_Type='G' and Status='Active' And FLW_ID Not In (" + VIPNomationFlowsMDA + ")"))
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.Connection = con;
                                    con.Open();
                                    ddl_flowList.Items.Clear();
                                    ddl_flowList.DataSource = cmd.ExecuteReader();
                                    ddl_flowList.DataTextField = "FLW_Name";
                                    ddl_flowList.DataValueField = "FLW_ID";
                                    ddl_flowList.DataBind();
                                    con.Close();
                                }
                            }
                            else
                            {
                                Response.Write(@"<script language='javascript'>alert('There is no group registration for this event.');</script>");
                                Response.End();
                            }
                        }
                    }
                    else
                    {
                        showID = ddl_showList.SelectedValue;
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected string getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }
        return showID;
    }

    protected void binddata()
    {
        string constr = fn.ConnString;
        string statusQuery = "Select status_name,status_usedid From ref_Status";
        FlowControler Flw = new FlowControler(fn);
        bool confExist = Flw.checkPageExist(flowID, SiteDefaultValue.constantConfName);
        if (!confExist)
        {
            statusQuery = "Select status_name,status_usedid From ref_Status Where status_name Not In ('TTPending','Waived','ChequePending','Cancel','Complimentary')";
        }
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand(statusQuery))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_paymentStatus.Items.Clear();
                ddl_paymentStatus.DataSource = cmd.ExecuteReader();
                ddl_paymentStatus.DataTextField = "status_name";
                ddl_paymentStatus.DataValueField = "status_usedid";
                ddl_paymentStatus.DataBind();
                con.Close();

                ddl_paymentStatus.Items.Insert(0, new ListItem("All", "-1"));
            }

            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_showList.Items.Clear();
                ddl_showList.DataSource = cmd.ExecuteReader();
                ddl_showList.DataTextField = "SHW_Name";
                ddl_showList.DataValueField = "SHW_ID";
                ddl_showList.DataBind();
                con.Close();
            }

            using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Name From tb_site_flow_master where ShowID = '" + ddl_showList.SelectedValue + "' and FLW_Type='G' and Status='Active' And FLW_ID Not In (" + VIPNomationFlowsMDA + ")"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_flowList.Items.Clear();
                ddl_flowList.DataSource = cmd.ExecuteReader();
                ddl_flowList.DataTextField = "FLW_Name";
                ddl_flowList.DataValueField = "FLW_ID";
                ddl_flowList.DataBind();
                con.Close();
            }
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_RegDelegate and tb_Invoice tables for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID))
        {
            try
            {
                int status = Convert.ToInt16(ddl_paymentStatus.SelectedValue);

                MasterRegGroup msregGroup = new MasterRegGroup(fn);
                msregGroup.ShowID = showID;
                msregGroup.FlowID = flowID;
                if (status == -1)
                    GKeyMaster.DataSource = msregGroup.getGroupDataByShowID(showID);//.getDataByGroupID(showID);
                else
                    GKeyMaster.DataSource = msregGroup.getGroupDataByShowIDStatus(showID, status);//.getDataByGroupID(showID, status);

                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showID);
                FlowControler Flw = new FlowControler(fn);
                bool confExist = Flw.checkPageExist(flowID, SiteDefaultValue.constantConfName);
                if (!confExist)
                {
                    this.GKeyMaster.MasterTableView.GetColumn("CongressSelection").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("PaymentMethod").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("Invoice_grandtotal").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("PaidPrice").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("Outstanding").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("Invoice_discount").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("InvoiceStatus").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("remarks").Display = false;
                    //this.GKeyMaster.MasterTableView.GetColumn("UpdatePayment").Display = false;
                    //this.GKeyMaster.MasterTableView.GetColumn("DownloadInvoice").Display = false;
                    //this.GKeyMaster.MasterTableView.GetColumn("DownloadReceipt").Display = false;
                    //this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmail").Display = false;
                    //this.GKeyMaster.MasterTableView.GetColumn("DownloadBadge").Display = false;
                }
                else
                {
                    this.GKeyMaster.MasterTableView.GetColumn("CongressSelection").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("PaymentMethod").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("Invoice_grandtotal").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("PaidPrice").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("Outstanding").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("Invoice_discount").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("InvoiceStatus").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("remarks").Display = true;
                    //this.GKeyMaster.MasterTableView.GetColumn("UpdatePayment").Display = checkPaymentMode(showID);//true;
                    //this.GKeyMaster.MasterTableView.GetColumn("DownloadInvoice").Display = checkPaymentMode(showID);//true;
                    //this.GKeyMaster.MasterTableView.GetColumn("DownloadReceipt").Display = checkReceiptInVisible(showID);//true;
                    //this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmail").Display = true;
                    //this.GKeyMaster.MasterTableView.GetColumn("DownloadBadge").Display = checkBadge(showID);//true;
                }
            }
            catch (Exception)
            {
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region Page_PreRender
    protected void Page_PreRender(object o, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
        {
            string userID = lblUser.Text.Trim();
            getShowID(userID);
        }
        else
            showID = ddl_showList.SelectedValue;

        if (!string.IsNullOrEmpty(showID) && !string.IsNullOrEmpty(flowID))
        {
            #region Group Contact-Person
            DataTable dtfrm = new DataTable();
            FormManageObj frmObj = new FormManageObj(fn);
            frmObj.showID = showID;
            frmObj.flowID = flowID;

            dtfrm = frmObj.getDynFormForGroup().Tables[0];

            #region Declaration
            int vis_Salutation = 0;
            int vis_Fname = 0;
            int vis_Lname = 0;
            int vis_Designation = 0;
            int vis_Department = 0;
            int vis_Company = 0;
            int vis_Industry = 0;
            int vis_Address1 = 0;
            int vis_Address2 = 0;
            int vis_Address3 = 0;

            int vis_City = 0;
            int vis_StateProvince = 0;
            int vis_PostalCode = 0;
            int vis_Country = 0;
            int vis_RCountry = 0;
            int vis_Tel = 0;
            int vis_Mobile = 0;
            int vis_Fax = 0;
            int vis_Email = 0;

            int vis_VisitDate = 0;
            int vis_VisitTime = 0;
            int vis_CreateDate = 0;

            int vis_Age = 0;
            int vis_Gender = 0;
            int vis_DOB = 0;
            int vis_Additional4 = 0;
            int vis_Additional5 = 0;
            #endregion

            if (dtfrm.Rows.Count > 0)
            {
                for (int x = 0; x < dtfrm.Rows.Count; x++)
                {
                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Salutation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Salutation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Salutation);

                                GKeyMaster.MasterTableView.GetColumn("RG_Salutation").HeaderText = labelname;

                                vis_Salutation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Salutation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Fname == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_ContactFName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Fname);
                                GKeyMaster.MasterTableView.GetColumn("RG_ContactFName").HeaderText = labelname;

                                vis_Fname++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_ContactFName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Lname == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_ContactLName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Lname);
                                GKeyMaster.MasterTableView.GetColumn("RG_ContactLName").HeaderText = labelname;

                                vis_Lname++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_ContactLName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Designation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Designation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Designation);
                                GKeyMaster.MasterTableView.GetColumn("RG_Designation").HeaderText = labelname;
                                vis_Designation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Designation").Display = false;
                        }
                    }

                    //if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _OtherDes)
                    //{
                    //    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    //    if (isshow == 1)
                    //    {
                    //        if (vis_SupDesignation == 0)
                    //        {
                    //            GKeyMaster.MasterTableView.GetColumn("RG_DesignationOther").Display = true;
                    //            string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_OtherDes);
                    //            GKeyMaster.MasterTableView.GetColumn("RG_DesignationOther").HeaderText = labelname;

                    //            vis_SupDesignation++;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        GKeyMaster.MasterTableView.GetColumn("RG_DesignationOther").Display = false;
                    //    }
                    //}

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Department)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Department == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Department").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Department);
                                GKeyMaster.MasterTableView.GetColumn("RG_Department").HeaderText = labelname;

                                vis_Department++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Department").Display = false;
                        }
                    }


                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Company)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Company == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Company").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Company);
                                GKeyMaster.MasterTableView.GetColumn("RG_Company").HeaderText = labelname;

                                vis_Company++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Company").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Industry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Industry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Industry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Industry);
                                GKeyMaster.MasterTableView.GetColumn("RG_Industry").HeaderText = labelname;

                                vis_Industry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Industry").Display = false;
                        }
                    }


                    //if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _OtherIndustry)
                    //{
                    //    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    //    if (isshow == 1)
                    //    {
                    //        if (vis_SupIndustry == 0)
                    //        {
                    //            GKeyMaster.MasterTableView.GetColumn("RG_IndustryOthers").Display = true;
                    //            string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_OtherIndustry);
                    //            GKeyMaster.MasterTableView.GetColumn("RG_IndustryOthers").HeaderText = labelname;

                    //            vis_SupIndustry++;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        GKeyMaster.MasterTableView.GetColumn("RG_IndustryOthers").Display = false;
                    //    }
                    //}

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address1 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Address1").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Address1);
                                GKeyMaster.MasterTableView.GetColumn("RG_Address1").HeaderText = labelname;
                                vis_Address1++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Address1").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address2 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Address2").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Address2);
                                GKeyMaster.MasterTableView.GetColumn("RG_Address2").HeaderText = labelname;
                                vis_Address2++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Address2").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address3 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Address3").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Address3);
                                GKeyMaster.MasterTableView.GetColumn("RG_Address3").HeaderText = labelname;

                                vis_Address3++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Address3").Display = false;
                        }
                    }



                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _City)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_City == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_City").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_City);
                                GKeyMaster.MasterTableView.GetColumn("RG_City").HeaderText = labelname;

                                vis_City++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_City").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _StateProvince)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_StateProvince == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_StateProvince").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_StateProvince);
                                GKeyMaster.MasterTableView.GetColumn("RG_StateProvince").HeaderText = labelname;

                                vis_StateProvince++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_StateProvince").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_PostalCode == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_PostalCode").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_PostalCode);
                                GKeyMaster.MasterTableView.GetColumn("RG_PostalCode").HeaderText = labelname;

                                vis_PostalCode++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_PostalCode").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Country)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Country == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Country").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Country);
                                GKeyMaster.MasterTableView.GetColumn("RG_Country").HeaderText = labelname;

                                vis_Country++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Country").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_RCountry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_RCountry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_RCountry);
                                GKeyMaster.MasterTableView.GetColumn("RG_RCountry").HeaderText = labelname;
                                vis_RCountry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_RCountry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Tel == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Tel").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Tel);
                                GKeyMaster.MasterTableView.GetColumn("RG_Tel").HeaderText = labelname;

                                vis_Tel++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Tel").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Mobile == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Mobile").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Mobile);
                                GKeyMaster.MasterTableView.GetColumn("RG_Mobile").HeaderText = labelname;

                                vis_Mobile++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Mobile").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Fax == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Fax").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Fax);
                                GKeyMaster.MasterTableView.GetColumn("RG_Fax").HeaderText = labelname;
                                vis_Fax++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Fax").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Email == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_ContactEmail").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Email);
                                GKeyMaster.MasterTableView.GetColumn("RG_ContactEmail").HeaderText = labelname;
                                vis_Email++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_ContactEmail").Display = false;
                        }
                    }

                    //if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Remark)
                    //{
                    //    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    //    if (isshow == 1)
                    //    {
                    //        if (vis_Remark == 0)
                    //        {
                    //            GKeyMaster.MasterTableView.GetColumn("RG_Remark").Display = true;
                    //            string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Remark);
                    //            GKeyMaster.MasterTableView.GetColumn("RG_Remark").HeaderText = labelname;

                    //            vis_Remark++;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        GKeyMaster.MasterTableView.GetColumn("RG_Remark").Display = false;
                    //    }
                    //}

                    //if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Type)
                    //{
                    //    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    //    if (isshow == 1)
                    //    {
                    //        if (vis_Type == 0)
                    //        {
                    //            GKeyMaster.MasterTableView.GetColumn("RG_Type").Display = true;
                    //            string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Type);
                    //            GKeyMaster.MasterTableView.GetColumn("RG_Type").HeaderText = labelname;

                    //            vis_Type++;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        GKeyMaster.MasterTableView.GetColumn("RG_Type").Display = false;
                    //    }
                    //}

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VisitDate)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VisitDate == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_VisitDate").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_VisitDate);
                                GKeyMaster.MasterTableView.GetColumn("RG_VisitDate").HeaderText = labelname;

                                vis_VisitDate++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_VisitDate").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VisitTime)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VisitTime == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_VisitTime").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_VisitTime);
                                GKeyMaster.MasterTableView.GetColumn("RG_VisitTime").HeaderText = labelname;

                                vis_VisitTime++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_VisitTime").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CreateDate)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_CreateDate == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_CreateDate").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_CreateDate);
                                GKeyMaster.MasterTableView.GetColumn("RG_CreateDate").HeaderText = labelname;
                                vis_CreateDate++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_CreateDate").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Age)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Age == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Age").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Age);
                                GKeyMaster.MasterTableView.GetColumn("RG_Age").HeaderText = labelname;

                                vis_Age++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Age").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_DOB == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_DOB").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_DOB);
                                GKeyMaster.MasterTableView.GetColumn("RG_DOB").HeaderText = labelname;

                                vis_DOB++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_DOB").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Gender == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Gender").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Gender);
                                GKeyMaster.MasterTableView.GetColumn("RG_Gender").HeaderText = labelname;
                                vis_Gender++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Gender").Display = false;
                        }
                    }


                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Additional4 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Additional4").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Additional4);
                                GKeyMaster.MasterTableView.GetColumn("RG_Additional4").HeaderText = labelname;

                                vis_Additional4++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Additional4").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Additional5 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RG_Additional5").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Additional5);
                                GKeyMaster.MasterTableView.GetColumn("RG_Additional5").HeaderText = labelname;
                                vis_Additional5++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RG_Additional5").Display = false;
                        }
                    }
                }
            }
            ddl_paymentStatus_SelectedIndexChanged(this, EventArgs.Empty);
            #endregion

            //PreRender
            #region Company



            dtfrm = frmObj.getDynFormForCompany().Tables[0];

            #region Declaration
            int vis_Name = 0;
            int vis_CAddress1 = 0;
            int vis_CAddress2 = 0;
            int vis_CAddress3 = 0;
            int vis_CCity = 0;
            int vis_State = 0;
            int vis_ZipCode = 0;
            int vis_CCountry = 0;
            int vis_CTel = 0;
            int vis_CFax = 0;
            int vis_CEmail = 0;
            int vis_Website = 0;
            int vis_Additional1 = 0;
            int vis_Additional2 = 0;
            int vis_Additional3 = 0;
            int vis_CAdditional4 = 0;
            int vis_CAdditional5 = 0;
            #endregion

            #region no company
            if (dtfrm.Rows.Count == 0)
            {
                GKeyMaster.MasterTableView.GetColumn("RC_Name").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Address1").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Address2").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Address3").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_City").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_State").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_ZipCode").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Country").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Tel").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Fax").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Email").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Website").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Additional1").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Additional2").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Additional3").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Additional4").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_Additional5").Display = false;
                GKeyMaster.MasterTableView.GetColumn("RC_CreatedDate").Display = false;
            }
            #endregion
            else
            {
                for (int x = 0; x < dtfrm.Rows.Count; x++)
                {
                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Name)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Name == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Name").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Name").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();
                                vis_Name++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Name").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CAddress1)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_CAddress1 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Address1").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Address1").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_CAddress1++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Address1").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CAddress2)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_CAddress2 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Address2").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Address2").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_CAddress2++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Address2").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CAddress3)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_CAddress3 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Address3").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Address3").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_CAddress3++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Address3").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CCity)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_CCity == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_City").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_City").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_CCity++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_City").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _State)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_State == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_State").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_State").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_State++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_State").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _ZipCode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_ZipCode == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_ZipCode").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_ZipCode").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_ZipCode++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_ZipCode").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_CCountry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Country").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Country").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_CCountry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Country").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CTel)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_CTel == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Tel").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Tel").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_CTel++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Tel").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CFax)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_CFax == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Fax").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Fax").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_CFax++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Fax").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CEmail)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_CEmail == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Email").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Email").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_CEmail++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Email").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Website)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Website == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Website").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Website").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_Website++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Website").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional1)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Additional1 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Additional1").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Additional1").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_Additional1++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional1").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional2)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Additional2 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Additional2").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Additional2").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_Additional2++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional2").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional3)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Additional3 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Additional3").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Additional3").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_Additional3++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional3").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CAdditional4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_CAdditional4 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Additional4").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Additional4").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_CAdditional4++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional4").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CAdditional5)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_CAdditional5 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("RC_Additional5").Display = true;
                                GKeyMaster.MasterTableView.GetColumn("RC_Additional5").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                                vis_CAdditional5++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional5").Display = false;
                        }
                    }
                }
            }

            #endregion




        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID) && !string.IsNullOrEmpty(flowID))
        {
            string groupid = "";

            foreach (GridDataItem item in GKeyMaster.SelectedItems)
            {

                groupid = item["RegGroupID"].Text;
            }


            if (e.CommandName == "Delete")
            {
                GridDataItem item = (GridDataItem)e.Item;
                // string del_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
                string del_RegGroupID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
                if (!string.IsNullOrEmpty(del_RegGroupID))
                {
                    int isDeleted = delRecord(del_RegGroupID);
                    if (isDeleted > 0)
                    {
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + del_RegGroupID + " is already deleted.');", true);
                        return;
                    }
                }
            }
            else if (e.CommandName == "Edit")
            {
                GridDataItem item = (GridDataItem)e.Item;
                //string edit_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
                string edit_RegGroupID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
                if (!string.IsNullOrEmpty(edit_RegGroupID))
                {
                    Response.Redirect("RegGroup_Edit.aspx?groupid=" + cFun.EncryptValue(edit_RegGroupID) + "&SHW=" + cFun.EncryptValue(showID) + "&FLW=" + cFun.EncryptValue(flowID));
                }
            }
            else if (e.CommandName == "View")
            {
                if (e.Item is GridDataItem)
                {

                    GridDataItem item = (GridDataItem)e.Item;
                    // string edit_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
                    string edit_RegGroupID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
                    if (!string.IsNullOrEmpty(edit_RegGroupID))
                    {



                        // Response.Redirect("RegIndiv_Edit.aspx?t=m&regno=" + cFun.EncryptValue(edit_RegGroupID) + "&SHW=" + cFun.EncryptValue(showID));
                    }
                }
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                GKeyMaster.ExportSettings.FileName = "ContactPersonList";
                GKeyMaster.ExportSettings.IgnorePaging = false;
                GKeyMaster.ExportSettings.ExportOnlyData = true;
                GKeyMaster.ExportSettings.OpenInNewWindow = true;
                GKeyMaster.MasterTableView.ExportToExcel();
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    /// <summary>
    /// Item databound for the radgrid, set the header text here based on the form management ***Item[UniqueName]
    /// </summary>
    protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
        {
            string userID = lblUser.Text.Trim();
            getShowID(userID);
        }
        else
            showID = ddl_showList.SelectedValue;

        if (!string.IsNullOrEmpty(showID) && !string.IsNullOrEmpty(flowID))
        {

            if (e.Item is GridHeaderItem)
            {
                GridHeaderItem item = e.Item as GridHeaderItem;

                #region Group Contact-Person
                DataTable dtfrm = new DataTable();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showID;
                frmObj.flowID = flowID;
                dtfrm = frmObj.getDynFormForDelegate().Tables[0];

                if (dtfrm.Rows.Count > 0)
                {
                    for (int x = 0; x < dtfrm.Rows.Count; x++)
                    {
                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Salutation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Salutation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Salutation);
                                }
                            }
                            else
                            {
                                (item["RG_Salutation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_ContactFName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_ContactFName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Fname);
                                }
                            }
                            else
                            {
                                (item["RG_ContactFName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_ContactLName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_ContactLName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Lname);
                                }
                            }
                            else
                            {
                                (item["RG_ContactLName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Designation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Designation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Designation);
                                }
                            }
                            else
                            {
                                (item["RG_Designation"].Controls[0] as LinkButton).Text = "";
                            }
                        }




                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Company)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Company"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Company"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Company);
                                }
                            }
                            else
                            {
                                (item["RG_Company"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Industry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Industry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Industry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Industry);
                                }
                            }
                            else
                            {
                                (item["RG_Industry"].Controls[0] as LinkButton).Text = "";
                            }
                        }


                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Address1"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Address1"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Address1);
                                }
                            }
                            else
                            {
                                (item["RG_Address1"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Address2"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Address2"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Address2);
                                }
                            }
                            else
                            {
                                (item["RG_Address2"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Address3"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Address3"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Address3);
                                }
                            }
                            else
                            {
                                (item["RG_Address3"].Controls[0] as LinkButton).Text = "";
                            }
                        }


                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _City)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_City"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_City"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_City);
                                }
                            }
                            else
                            {
                                (item["RG_City"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _StateProvince)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_StateProvince"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_StateProvince"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_StateProvince);
                                }
                            }
                            else
                            {
                                (item["RG_StateProvince"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_PostalCode"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_PostalCode"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_PostalCode);
                                }
                            }
                            else
                            {
                                (item["RG_PostalCode"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Country)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Country"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Country"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Country);
                                }
                            }
                            else
                            {
                                (item["RG_Country"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_RCountry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_RCountry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_RCountry);
                                }
                            }
                            else
                            {
                                (item["RG_RCountry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Tel"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Tel"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Tel);
                                }
                            }
                            else
                            {
                                (item["RG_Tel"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Mobile"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Mobile"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Mobile);
                                }
                            }
                            else
                            {
                                (item["RG_Mobile"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Fax"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Fax"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Fax);
                                }
                            }
                            else
                            {
                                (item["RG_Fax"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_ContactEmail"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_ContactEmail"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Email);
                                }
                            }
                            else
                            {
                                (item["RG_ContactEmail"].Controls[0] as LinkButton).Text = "";
                            }
                        }


                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CreateDate)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_CreatedDate"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_CreatedDate"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Email);
                                }
                            }
                            else
                            {
                                (item["RG_CreatedDate"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        //if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Remark)
                        //{
                        //    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        //    if (isshow == 1)
                        //    {
                        //        if (string.IsNullOrEmpty((item["RG_Remark"].Controls[0] as LinkButton).Text))
                        //        {
                        //            (item["RG_Remark"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Remark);
                        //        }
                        //    }
                        //    else
                        //    {
                        //        (item["RG_Remark"].Controls[0] as LinkButton).Text = "";
                        //    }
                        //}

                        //if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Type)
                        //{
                        //    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        //    if (isshow == 1)
                        //    {
                        //        if (string.IsNullOrEmpty((item["RG_Type"].Controls[0] as LinkButton).Text))
                        //        {
                        //            (item["RG_Type"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Type);
                        //        }
                        //    }
                        //    else
                        //    {
                        //        (item["RG_Type"].Controls[0] as LinkButton).Text = "";
                        //    }
                        //}

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VisitDate)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_VisitDate"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_VisitDate"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_VisitDate);
                                }
                            }
                            else
                            {
                                (item["RG_VisitDate"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VisitTime)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_VisitTime"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_VisitTime"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_VisitTime);
                                }
                            }
                            else
                            {
                                (item["RG_VisitTime"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Age)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Age"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Age"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Age);
                                }
                            }
                            else
                            {
                                (item["RG_Age"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_DOB"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_DOB"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_DOB);
                                }
                            }
                            else
                            {
                                (item["RG_DOB"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Gender"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Gender"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Gender);
                                }
                            }
                            else
                            {
                                (item["RG_Gender"].Controls[0] as LinkButton).Text = "";
                            }
                        }



                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Additional4"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Additional4"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Additional4);
                                }
                            }
                            else
                            {
                                (item["RG_Additional4"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["RG_Additional5"].Controls[0] as LinkButton).Text))
                                {
                                    (item["RG_Additional5"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForGroup(_Additional5);
                                }
                            }
                            else
                            {
                                (item["RG_Additional5"].Controls[0] as LinkButton).Text = "";
                            }
                        }
                    }
                }
                #endregion


                #region Company
                dtfrm = frmObj.getDynFormForCompany().Tables[0];
                for (int x = 0; x < dtfrm.Rows.Count; x++)
                {
                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Name)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Name"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Name"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Name"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CAddress1)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Address1"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Address1"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Address1"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CAddress2)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Address2"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Address2"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Address2"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CAddress3)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Address3"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Address3"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Address3"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CCity)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_City"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_City"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_City"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _State)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_State"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_State"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_State"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _ZipCode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_ZipCode"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_ZipCode"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_ZipCode"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Country"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Country"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Country"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CTel)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Tel"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Tel"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Tel"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CFax)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Fax"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Fax"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Fax"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CEmail)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Email"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Email"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Email"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Website)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Website"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Website"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Website"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional1)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Additional1"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Additional1"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Additional1"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional2)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Additional2"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Additional2"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Additional2"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional3)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Additional3"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Additional3"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Additional3"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CAdditional4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Additional4"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Additional4"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Additional4"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _CAdditional5)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Additional5"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Additional5"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Additional5"].Controls[0] as LinkButton).Text = "";
                        }
                    }
                }

                #endregion

            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        
        }
    }
    #region delRecord
    private int delRecord(string groupid)
    {
        int isDeleted = 0;
        if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
        {
            string userID = lblUser.Text.Trim();
            getShowID(userID);
        }
        else
            showID = ddl_showList.SelectedValue;

        if (showID != null)
        {
            try
            {
                isDeleted = fn.ExecuteSQL(string.Format("Update tb_RegGroup Set recycle=1 Where RegGroupID={0} And ShowID='{1}'", groupid, showID));
                isDeleted += fn.ExecuteSQL(string.Format("Update tb_RegDelegate Set recycle=1 Where RegGroupID={0} And ShowID='{1}'", groupid, showID));
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

        return isDeleted;
    }
    #endregion

    protected void ddl_paymentStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
        {
            string userID = lblUser.Text.Trim();
            getShowID(userID);
        }
        else
            showID = ddl_showList.SelectedValue;
        GKeyMaster.Rebind();
    }

    protected void ddl_showList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;
        string constr = fn.ConnString;
        try
        {
            int groupflowcount = 0;
           groupflowcount=GetGroupFlowCount(ddl_showList.SelectedValue);
            if (groupflowcount > 0)
            {

                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Name From tb_site_flow_master where ShowID = '" + showID + "' and FLW_Type='G' and Status='Active' And FLW_ID Not In (" + VIPNomationFlowsMDA + ")"))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        con.Open();
                        ddl_flowList.Items.Clear();
                        ddl_flowList.DataSource = cmd.ExecuteReader();
                        ddl_flowList.DataTextField = "FLW_Name";
                        ddl_flowList.DataValueField = "FLW_ID";
                        ddl_flowList.DataBind();
                        con.Close();
                    }
                }
                flowID = ddl_flowList.SelectedValue;

                GKeyMaster.Rebind();
            }
            //else
            //{
            //    Response.Write(@"<script language='javascript'>alert('There is no group registration for this event.');</script>");
            //   // Response.End();
            //}
          
        }
        catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);

        }

    }

    protected void ddl_flowList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
        {
            string userID = lblUser.Text.Trim();
            getShowID(userID);
        }
        else
            showID = ddl_showList.SelectedValue;
        flowID = ddl_flowList.SelectedValue;
        GKeyMaster.Rebind();
    }


    protected void RadGd1_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem)e.Item;
            string edit_RegGroupID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
            if (!string.IsNullOrEmpty(edit_RegGroupID))
            {
                if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
                {
                    string userID = lblUser.Text.Trim();
                    getShowID(userID);
                }
                else
                    showID = ddl_showList.SelectedValue;

                ImageButton memberlink = (ImageButton)item["ViewMember"].Controls[0];
                memberlink.Attributes.Add("onclick", "centeredPopup('Group_MemberList.aspx?groupid=" + cFun.EncryptValue(edit_RegGroupID) + "&SHW=" + cFun.EncryptValue(showID) + "&FLW=" + cFun.EncryptValue(flowID) + "', 'myWindow', '1000', '500', 'yes'); return false");

                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showID);
                if (checkingShowIDNotToShowOtherButton.Contains(shw.SHW_Name))
                {
                    GKeyMaster.MasterTableView.GetColumn("Download").Display = false;
                    GKeyMaster.MasterTableView.GetColumn("ViewMember").Display = false;
                    GKeyMaster.MasterTableView.GetColumn("Edit").Display = false;
                }
                else
                {
                    ImageButton downloadlink = (ImageButton)item["Download"].Controls[0];
                    downloadlink.Attributes.Add("onclick", "window.open('../AcknowledgeLetter.aspx?DID=" + cFun.EncryptValue(edit_RegGroupID) + "&SHW=" + cFun.EncryptValue(showID) + "'); return false");
                }
            }
        }
    }


    public void GetDetailReport(string qnaireID)
    {
        if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
        {
            string userID = lblUser.Text.Trim();
            getShowID(userID);
        }
        else
            showID = ddl_showList.SelectedValue;

        string logid = string.Empty;
        string item = string.Empty;
        StringBuilder str = new StringBuilder();

        string sqlQ = "select * from gen_Question where status='Active' and quest_class <> 'Header' and quest_id in (select top 500 quest_id from gen_QnaireQuest where qnaire_id='" + qnaireID + "' And status='Active' order by qnaire_seq)";
        DataTable dtQ = qfn.GetDatasetByCommand(sqlQ, "sdtQ").Tables[0];
        //Regno	Salutation	First Name	Last Name	Company	Job Title	Email	Mobile	Address	Country	Type	Category

        flowID = ddl_flowList.SelectedValue;
        FlowControler Flw = new FlowControler(fn);
        bool companyExist = Flw.checkPageExist(flowID, SiteDefaultValue.constantRegCompany);
        if (companyExist)
        {
            #region with Company Page
            str.Append("<tr>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "No" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Registration ID" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Created Date" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Update Date" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Registration Status" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Salutation" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "FirstName" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Last Name" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Job Title" + "</td>");
            //str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Department" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Name" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Address Line1" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Address Line2" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Address Line3" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Postcode" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "City" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "State/Province" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Country" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Tel No Code" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Tel No" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Mobile Code" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Mobile No" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Fax Code" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Faxno" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Email" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Email Alternate" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Website" + "</td>");


            if (dtQ.Rows.Count > 0)
            {
                for (int q = 0; q < dtQ.Rows.Count; q++)
                {
                    int span = GetQuestCount(dtQ.Rows[q]["quest_id"].ToString());
                    str.Append("<td colspan='" + span + "' style='background-color:#b3b3b3'>" + dtQ.Rows[q]["quest_desc"].ToString() + "</td>");
                }
            }
            str.Append("</tr>");
            str.Append("<tr>");
            if (dtQ.Rows.Count > 0)
            {
                for (int q = 0; q < dtQ.Rows.Count; q++)
                {
                    string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
                    DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
                    if (dtI.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtI.Rows.Count; i++)
                        {
                            str.Append("<td style='background-color:#b3b3b3'>" + dtI.Rows[i]["qitem_desc"].ToString() + "</td>");
                        }
                    }

                }
            }
            str.Append("</tr>");
            string sqlU = "select * from tbl" + qnaireID + " order by user_reference";
            DataTable dtU = qfn.GetDatasetByCommand(sqlU, "sdtU").Tables[0];

            string sqlDelegateAll = "select * from  tb_RegGroup  full outer join ref_Salutation on RG_Salutation=Sal_ID full outer join ref_country on RG_Country=Cty_GUID where RG_urlFlowID='" + ddl_flowList.SelectedValue + "' and recycle='0' order by RegGroupID";// and RG_Status='1' 
            DataTable dtDelegateAll = fn.GetDatasetByCommand(sqlDelegateAll, "sdtDelegateAll").Tables[0];


            string sqlA1 = "select * from gen_QuestItem  where  status='Active' and quest_id in (select quest_id from gen_QnaireQuest where qnaire_id='" + qnaireID + "' and status='Active' and quest_id in(select quest_id from gen_Question where quest_class<>'Header'))";
            DataTable dtA1 = qfn.GetDatasetByCommand(sqlA1, "sdtA1").Tables[0];
            if (dtDelegateAll.Rows.Count > 0)
            {
                for (int da = 0; da < dtDelegateAll.Rows.Count; da++)
                {
                    if (dtU.Rows.Count > 0)
                    {
                        string daregno = dtDelegateAll.Rows[da]["RegGroupID"].ToString();
                        string sqlDelegate = "select * from  tb_RegGroup as g full outer join ref_Salutation on g.RG_Salutation=Sal_ID full outer join ref_country on g.RG_Country = Cty_GUID full outer join tb_RegCompany as c on g.RegGroupID = c.RegGroupID where g.RegGroupID in ('" + daregno + "') ";
                        DataTable dtDelegate = fn.GetDatasetByCommand(sqlDelegate, "sdtDelegate").Tables[0];
                        if (dtDelegate.Rows.Count > 0)
                        {
                            for (int d = 0; d < dtDelegate.Rows.Count; d++)
                            {
                                str.Append("<tr>");
                                str.Append("<td>" + (da + 1).ToString() + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RegGroupID"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RG_CreatedDate"] + "</td>");
                                str.Append("<td> </td>");
                                str.Append("<td>" + getStatus(dtDelegate.Rows[d]["RG_Status"] != DBNull.Value ? dtDelegate.Rows[d]["RG_Status"].ToString() : "0") + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["Sal_Name"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RG_ContactFName"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RG_ContactLName"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RG_Designation"] + "</td>");
                                //str.Append("<td>" + dtDelegate.Rows[d]["RG_Department"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RC_Name"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RC_Address1"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RC_Address2"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RC_Address3"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RC_ZipCode"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RC_City"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RC_State"] + "</td>");

                                string grpCountry = dtDelegate.Rows[d]["RG_Country"] != null ? dtDelegate.Rows[d]["RG_Country"].ToString() : "";
                                string comCountry = dtDelegate.Rows[d]["RC_Country"] != null ? dtDelegate.Rows[d]["RC_Country"].ToString() : "";
                                string countryName = bindCountry(!string.IsNullOrEmpty(grpCountry) && grpCountry != "0" ? grpCountry : comCountry);
                                str.Append("<td>" + countryName + "</td>");

                                str.Append("<td>" + dtDelegate.Rows[d]["RG_Telcc"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RG_Telac"] + "-" + dtDelegate.Rows[d]["RG_Tel"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RG_Mobilecc"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RG_Mobileac"] + "-" + dtDelegate.Rows[d]["RG_Mobile"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RG_Faxcc"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RG_Faxac"] + "-" + dtDelegate.Rows[d]["RG_Fax"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RG_ContactEmail"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RG_Additional5"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["RC_Website"] + "</td>");
                            }
                        }
                        #region Old (Comment)
                        //if (dtA1.Rows.Count > 0)
                        //{
                        //    for (int a = 0; a < dtA1.Rows.Count; a++)
                        //    {
                        //        #region old
                        //        //if (logid != dtA1.Rows[a]["qitem_id"].ToString() && item != dtU.Rows[u]["qnaire_log_id"].ToString() && !string.IsNullOrEmpty(dtA1.Rows[a]["qitem_class"].ToString()))
                        //        //{
                        //        //    var res = fn.GetDataByCommand("select  count(qnaire_log_id) as R from gen_QnaireResult where qitem_id='" + dtA1.Rows[a]["qitem_id"] + "' and qnaire_log_id='" + dtU.Rows[u]["qnaire_log_id"] + "'", "R");
                        //        //    if (res == "0")
                        //        //    {
                        //        //        res = "";
                        //        //    }
                        //        //    str.Append("<td>" + res + "</td>");
                        //        //    item = dtA1.Rows[a]["qitem_id"].ToString();
                        //        //    logid = dtU.Rows[u]["qnaire_log_id"].ToString();
                        //        //}
                        //        #endregion
                        //        #region new from bex
                        //        if (logid != dtA1.Rows[a]["qitem_id"].ToString() && item != dtU.Rows[0]["qnaire_log_id"].ToString())
                        //        {
                        //            string sql = "select qitem_input_num as R from gen_QnaireResult where qitem_id='" + dtA1.Rows[a]["qitem_id"] + "' and qnaire_log_id='" + dtU.Rows[u]["qnaire_log_id"] + "' and Status='Active'";
                        //            var res = qfn.GetDataByCommand(sql, "R");
                        //            if (res == "0")
                        //            {
                        //                string sql2 = "select  qitem_input_txt as R from gen_QnaireResult where qitem_id='" + dtA1.Rows[a]["qitem_id"] + "' and qnaire_log_id='" + dtU.Rows[u]["qnaire_log_id"] + "'";
                        //                res = qfn.GetDataByCommand(sql2, "R");
                        //                if (res == "0")
                        //                {
                        //                    res = "";
                        //                }
                        //            }
                        //            str.Append("<td>" + res + "</td>");
                        //            item = dtA1.Rows[a]["qitem_id"].ToString();
                        //            logid = dtU.Rows[u]["qnaire_log_id"].ToString();

                        //        }

                        //    }
                        //}
                        #endregion
                        string qnaireid = (from DataRow dr in dtU.Rows
                                           where (string)dr["user_reference"] == daregno
                                           select (string)dr["qnaire_log_id"]).FirstOrDefault();
                        if (!string.IsNullOrEmpty(qnaireid))
                        {
                            string sqlResult = "SELECT * INTO #TempTable FROM tbl" + qnaireID + " where user_reference='" + daregno + "' "
                                            + "ALTER TABLE #TempTable "
                                            + "DROP COLUMN qnaire_log_id,user_reference,status,create_date,update_date "
                                            + "SELECT * FROM #TempTable";
                            DataTable dtResut = qfn.GetDatasetByCommand(sqlResult, "sqResult").Tables[0];
                            if (dtResut.Rows.Count > 0)
                            {
                                foreach (DataRow drR in dtResut.Rows)
                                {
                                    foreach (DataColumn dc in dtResut.Columns)
                                    {
                                        string hutt = drR[dc].ToString();
                                        if (hutt == "0")
                                        {
                                            str.Append("<td></td>");
                                        }
                                        else
                                        {
                                            str.Append("<td>" + hutt + "</td>");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (dtQ.Rows.Count > 0)
                            {
                                for (int q = 0; q < dtQ.Rows.Count; q++)
                                {
                                    string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
                                    DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
                                    if (dtI.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtI.Rows.Count; i++)
                                        {
                                            str.Append("<td></td>");
                                        }
                                    }

                                }
                            }
                        }
                        str.Append("</tr>");
                    }
                    #region testNodata
                    //str.Append("<tr>");
                    //str.Append("<td>" + (da + 1).ToString() + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RegGroupID"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_CreatedDate"] + "</td>");
                    //str.Append("<td> </td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["Sal_Name"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_ContactFName"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_ContactLName"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Designation"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Department"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Company"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Address1"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Address2"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_PostalCode"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_City"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_StateProvince"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["Country"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Telcc"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Telac"] + "-" + dtDelegateAll.Rows[da]["RG_Tel"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Mobilecc"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Mobileac"] + "-" + dtDelegateAll.Rows[da]["RG_Mobile"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Faxcc"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Faxac"] + "-" + dtDelegateAll.Rows[da]["RG_Fax"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_ContactEmail"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Additional5"] + "</td>");
                    //str.Append("<td>" + dtDelegateAll.Rows[da]["RG_Additional4"] + "</td>");
                    #endregion
                }
            }
            htmltb += str.ToString();
            #endregion
        }
        else
        {
            #region No Company Page
            int status = Convert.ToInt16(ddl_paymentStatus.SelectedValue);

            MasterRegGroup msregGroup = new MasterRegGroup(fn);
            msregGroup.ShowID = showID;
            msregGroup.FlowID = flowID;
            DataTable dtReg = new DataTable();
            if (status == -1)
            {
                dtReg = msregGroup.getGroupDataByShowID(showID);//.getDataByGroupID(showID);
            }
            else
            {
                dtReg = msregGroup.getGroupDataByShowIDStatus(showID, status);//.getDataByGroupID(showID, status);
            }
            if (dtReg.Rows.Count > 0)
            {
                #region header
                this.GKeyMaster.MasterTableView.GetColumn("Download").Display = false;
                this.GKeyMaster.MasterTableView.GetColumn("ViewMember").Display = false;
                this.GKeyMaster.MasterTableView.GetColumn("Delete").Display = false;
                this.GKeyMaster.MasterTableView.GetColumn("Edit").Display = false;
                this.GKeyMaster.ExportSettings.ExportOnlyData = true;
                this.GKeyMaster.AllowPaging = false;
                this.GKeyMaster.Rebind();
                str.Append("<tr>");
                str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "No" + "</td>");
                //foreach (GridDataItem row in GKeyMaster.Items) // loops through each rows in RadGrid
                {
                    foreach (GridColumn col in GKeyMaster.Columns) //loops through each column in RadGrid
                    {
                        if (col.Visible == true && col.Display == true)
                        {
                            if (col.HeaderText != "")
                            {
                                if (col.UniqueName != "Download" && col.UniqueName != "ViewMember"
                                    && col.UniqueName != "Delete" && col.UniqueName != "Edit")
                                {
                                    str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + col.HeaderText + "</td>");
                                }
                            }
                        }
                    }
                }

                #region QA header
                if (dtQ.Rows.Count > 0)
                {
                    for (int q = 0; q < dtQ.Rows.Count; q++)
                    {
                        int span = GetQuestCount(dtQ.Rows[q]["quest_id"].ToString());
                        //NewDt.Columns.Add(dtQ.Rows[q]["quest_desc"].ToString(), typeof(string));
                        str.Append("<td colspan='" + span + "' style='background-color:#b3b3b3'>" + dtQ.Rows[q]["quest_desc"].ToString() + "</td>");
                    }
                }
                str.Append("</tr>");
                str.Append("<tr>");
                if (dtQ.Rows.Count > 0)
                {
                    for (int q = 0; q < dtQ.Rows.Count; q++)
                    {
                        string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
                        DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
                        if (dtI.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtI.Rows.Count; i++)
                            {
                                //NewDt.Columns.Add(dtI.Rows[i]["qitem_desc"].ToString(), typeof(string));
                                str.Append("<td style='background-color:#b3b3b3'>" + dtI.Rows[i]["qitem_desc"].ToString() + "</td>");
                            }
                        }
                    }
                }
                #endregion
                str.Append("</tr>");
                #endregion

                try
                {
                    int count = 0;
                    foreach (GridDataItem row in GKeyMaster.Items) // loops through each rows in RadGrid
                    {
                        str.Append("<tr>");
                        str.Append("<td>" + (count + 1).ToString() + "</td>");

                        string groupid = string.Empty;

                        foreach (GridColumn col in GKeyMaster.Columns) //loops through each column in RadGrid
                        {
                            if (col.Visible == true && col.Display == true)
                            {
                                if (col.UniqueName != "Download" && col.UniqueName != "ViewMember"
                                    && col.UniqueName != "Delete" && col.UniqueName != "Edit")
                                {
                                    //var test = DataBinder.Eval(row.DataItem, col.UniqueName).ToString();
                                    string dataValue = row[col.UniqueName].Text;
                                    if (col.UniqueName == "RegGroupID")
                                    {
                                        groupid = dataValue;
                                    }

                                    if (col.UniqueName == "RG_Salutation")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = bindSalutation(drReg[0]["RG_Salutation"].ToString(), drReg[0]["RG_SalOther"].ToString());
                                    }
                                    else if (col.UniqueName == "RG_Designation")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = !string.IsNullOrEmpty(drReg[0]["RG_DesignationOther"].ToString()) ? drReg[0]["RG_Designation"].ToString() + ": " + drReg[0]["RG_DesignationOther"].ToString() : drReg[0]["RG_Designation"].ToString();
                                    }
                                    else if (col.UniqueName == "RG_Industry")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = bindIndustry(drReg[0]["RG_Industry"].ToString(), drReg[0]["RG_IndustryOthers"].ToString());
                                    }
                                    else if (col.UniqueName == "RG_Country")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = bindCountry(drReg[0]["RG_Country"].ToString());
                                    }
                                    if (col.UniqueName == "RG_RCountry")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = bindCountry(drReg[0]["RG_RCountry"].ToString());
                                    }
                                    if (col.UniqueName == "RG_Tel")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = drReg[0]["RG_Telcc"].ToString() + drReg[0]["RG_Telac"].ToString() + drReg[0]["RG_Tel"].ToString();
                                    }
                                    else if (col.UniqueName == "RG_Mobile")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = drReg[0]["RG_Mobilecc"].ToString() + drReg[0]["RG_Mobileac"].ToString() + drReg[0]["RG_Mobile"].ToString();
                                    }
                                    else if (col.UniqueName == "RG_Fax")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = drReg[0]["RG_Faxcc"].ToString() + drReg[0]["RG_Faxac"].ToString() + drReg[0]["RG_Fax"].ToString();
                                    }
                                    else if (col.UniqueName == "status_name")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = !string.IsNullOrEmpty(drReg[0]["status_name"].ToString()) ? drReg[0]["status_name"].ToString() : "Pending";
                                    }
                                    else if (col.UniqueName == "RC_Country")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = bindCountry(drReg[0]["RC_Country"].ToString());
                                    }
                                    else if (col.UniqueName == "RC_Tel")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = drReg[0]["RC_Telcc"].ToString() + drReg[0]["RC_Telac"].ToString() + drReg[0]["RC_Tel"].ToString();
                                    }
                                    else if (col.UniqueName == "RC_Fax")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = drReg[0]["RC_Faxcc"].ToString() + drReg[0]["RC_Faxac"].ToString() + drReg[0]["RC_Fax"].ToString();
                                    }
                                    else if (col.UniqueName == "CongressSelection")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = getCongressSelection("", drReg[0]["RegGroupID"].ToString(), drReg[0]["Invoice_status"].ToString(), drReg[0]["InvoiceID"].ToString());
                                    }
                                    else if (col.UniqueName == "PaymentMethod")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = getPaymentMethod(drReg[0]["PaymentMethod"].ToString(), drReg[0]["showid"].ToString());
                                    }
                                    else if (col.UniqueName == "PaidPrice")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = getPaidPrice(drReg[0]["InvoiceID"].ToString());
                                    }
                                    else if (col.UniqueName == "Outstanding")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = calculateOutstanding(drReg[0]["Invoice_grandtotal"].ToString(), drReg[0]["InvoiceID"].ToString());
                                    }
                                    else if (col.UniqueName == "InvoiceStatus")
                                    {
                                        DataRow[] drReg = dtReg.Select("RegGroupID='" + groupid + "'");
                                        dataValue = getInvoiceStatus(drReg[0]["Invoice_status"].ToString());
                                    }
                                    else
                                    {
                                        if (row[col.UniqueName].Text == "&nbsp;")
                                        {
                                            if (row.DataItem == null)
                                            { }
                                            else
                                            {
                                                dataValue = DataBinder.Eval(row.DataItem, col.UniqueName).ToString();
                                            }
                                        }
                                    }

                                    str.Append("<td>" + dataValue + "</td>");
                                }
                            }
                        }

                        #region QA
                        string daregno = row["RegGroupID"].Text;
                        if (!string.IsNullOrEmpty(qnaireID) && qnaireID != "0")
                        {
                            string sqlU = "select * from tbl" + qnaireID + " order by user_reference";
                            DataTable dtU = qfn.GetDatasetByCommand(sqlU, "sdtU").Tables[0];
                            string qnaireid = (from DataRow dr in dtU.Rows
                                               where (string)dr["user_reference"] == daregno
                                               select (string)dr["qnaire_log_id"]).FirstOrDefault();
                            if (!string.IsNullOrEmpty(qnaireid))
                            {
                                string sqlResult = "SELECT * INTO #TempTable FROM tbl" + qnaireID + " where user_reference='" + daregno + "' "
                                                        + "ALTER TABLE #TempTable "
                                                        + "DROP COLUMN qnaire_log_id,user_reference,status,create_date,update_date "
                                                        + "SELECT * FROM #TempTable";
                                DataTable dtResut = qfn.GetDatasetByCommand(sqlResult, "sqResult").Tables[0];
                                if (dtResut.Rows.Count > 0)
                                {
                                    foreach (DataRow drR in dtResut.Rows)
                                    {
                                        foreach (DataColumn dc in dtResut.Columns)
                                        {
                                            string hutt = drR[dc].ToString();
                                            if (hutt == "0")
                                            {
                                                str.Append("<td></td>");
                                            }
                                            else
                                            {
                                                str.Append("<td>" + hutt + "</td>");
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (dtQ.Rows.Count > 0)
                                {
                                    for (int q = 0; q < dtQ.Rows.Count; q++)
                                    {
                                        string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
                                        DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
                                        if (dtI.Rows.Count > 0)
                                        {
                                            for (int i = 0; i < dtI.Rows.Count; i++)
                                            {
                                                str.Append("<td></td>");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        str.Append("</tr>");
                        count++;
                    }
                    htmltb += str.ToString();
                }
                catch(Exception ex)
                {

                }
            }
            #endregion
        }
    }

    protected void lnkExcel_Clicked(object sender, EventArgs e)
    {

        try
        {

            flowID = ddl_flowList.SelectedValue;
            string QnaireID = GetQIDFromFLW(flowID);
            ExportExcel(QnaireID);

        }
        catch (Exception ex) { }
    }

    public void ExportExcel(string QnaireID)
    {
        try
        {
            string flowName = ddl_flowList.SelectedItem.Text;
            string name = flowName.Replace(" ", "");
            StringBuilder StrBuilder = new StringBuilder();
            htmltb = string.Empty;
            GetDetailReport(QnaireID);
            StrBuilder.Append("<table cellpadding='0' cellspacing='0' border='1' class='Messages' id='idTbl' runat='server'>");
            StrBuilder.Append(htmltb.ToString());
            StrBuilder.Append("</table>");
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/msexcel";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode;
            HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            HttpContext.Current.Response.AddHeader("Content-Disposition", "filename=ContactPerson-Detailed-Report-"+name+".xls");
            HttpContext.Current.Response.Write(StrBuilder);
            HttpContext.Current.Response.End();

            HttpContext.Current.Response.Flush();
        }
        catch (System.Threading.ThreadAbortException exf)
        {

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);

        }
    }



    public string GetQIDFromFLW(string flowID)
    {
    
        string result = string.Empty;
        string sql = string.Empty;
            string query = "select SQ_QAID from tb_site_QA_Master where SQ_FLW_ID='" + flowID + "' and SQ_Category='G'";
          DataTable dtFlow = fn.GetDatasetByCommand(query, "dtFlow").Tables[0];
        if (dtFlow.Rows.Count > 0)
        {
             sql = "select SQ_QAID from tb_site_QA_Master where SQ_FLW_ID='" + flowID + "' and SQ_Category='G'";
        }else
        {
             sql = "select SQ_QAID from tb_site_QA_Master where SQ_FLW_ID='" + flowID + "' and SQ_Category='C'";
        }

        result = fn.GetDataByCommand(sql, "SQ_QAID");
        return result;
    }

    public int GetQuestCount(string questID)
    {
        int Qcount = 0;
        string sql = "select * from gen_QuestItem where status='Active' and quest_id='" + questID + "'";
        DataSet ds = new DataSet();
        ds = qfn.GetDatasetByCommand(sql, "dsds");

        Qcount = ds.Tables[0].Rows.Count;

        return Qcount;

    }

    public int GetGroupFlowCount(string showid)
    {
        int Qcount = 0;
        string sql = "Select * From tb_site_flow_master where ShowID = '"+ showid + "' and FLW_Type = 'G' and Status = 'Active' And FLW_ID Not In (" + VIPNomationFlowsMDA + ")";
        DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand(sql, "dsds");

        Qcount = ds.Tables[0].Rows.Count;

        return Qcount;
    }

    public int CheckGFlowExist(string showID)
    {
        if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
        {
            string userID = lblUser.Text.Trim();
            getShowID(userID);
        }
        else
            showID = ddl_showList.SelectedValue;

        int Fcount = 0;
        string sql = "Select FLW_ID,FLW_Name From tb_site_flow_master where ShowID = '" + showID + "' and FLW_Type='G' and Status='Active' And FLW_ID Not In (" + VIPNomationFlowsMDA + ")";
       DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand(sql, "dsfs");

        Fcount = ds.Tables[0].Rows.Count;


        return Fcount;
    }

    #region bindName
    public string bindSalutation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
                {
                    string userID = lblUser.Text.Trim();
                    getShowID(userID);
                }
                else
                    showID = ddl_showList.SelectedValue;

                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getSalutationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindProfession(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
                {
                    string userID = lblUser.Text.Trim();
                    getShowID(userID);
                }
                else
                    showID = ddl_showList.SelectedValue;

                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getProfessionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDepartment(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
                {
                    string userID = lblUser.Text.Trim();
                    getShowID(userID);
                }
                else
                    showID = ddl_showList.SelectedValue;

                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDepartmentNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindOrganisation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
                {
                    string userID = lblUser.Text.Trim();
                    getShowID(userID);
                }
                else
                    showID = ddl_showList.SelectedValue;

                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getOrganisationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindInstitution(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
                {
                    string userID = lblUser.Text.Trim();
                    getShowID(userID);
                }
                else
                    showID = ddl_showList.SelectedValue;

                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getInstitutionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindCountry(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
                {
                    string userID = lblUser.Text.Trim();
                    getShowID(userID);
                }
                else
                    showID = ddl_showList.SelectedValue;

                if (!string.IsNullOrEmpty(id))
                {
                    CountryObj conCtr = new CountryObj(fn);
                    name = conCtr.getCountryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindAffiliation(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
                {
                    string userID = lblUser.Text.Trim();
                    getShowID(userID);
                }
                else
                    showID = ddl_showList.SelectedValue;

                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getAffiliationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDietary(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
                {
                    string userID = lblUser.Text.Trim();
                    getShowID(userID);
                }
                else
                    showID = ddl_showList.SelectedValue;

                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDietaryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindIndustry(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
                {
                    string userID = lblUser.Text.Trim();
                    getShowID(userID);
                }
                else
                    showID = ddl_showList.SelectedValue;

                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getIndustryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    #region bindPhoneNo
    public string bindPhoneNo(string cc, string ac, string phoneno, string type)
    {
        string name = string.Empty;
        bool isShowCC = false, isShowAC = false, isShowPhoneNo = false;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
                {
                    string userID = lblUser.Text.Trim();
                    getShowID(userID);
                }
                else
                    showID = ddl_showList.SelectedValue;

                string flowid = ddl_flowList.SelectedItem.Value;
                if (!string.IsNullOrEmpty(showID))
                {
                    DataSet ds = new DataSet();
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showID;
                    frmObj.flowID = flowid;
                    ds = frmObj.getDynFormForDelegate();

                    for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                    {
                        if (type == "Tel")
                        {
                            #region type="Tel"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                        else if (type == "Mob")
                        {
                            #region type="Mob"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                        else if (type == "Fax")
                        {
                            #region Type="Fax"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                    }

                    if (isShowCC)
                    {
                        name = "+" + cc;
                    }
                    if (isShowAC)
                    {
                        name += " " + ac;
                    }
                    if (isShowPhoneNo)
                    {
                        name += " " + phoneno;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion
    #endregion

    public string getStatus(string invStatus)
    {
        string result = string.Empty;
        try
        {
            //InvoiceControler invControler = new InvoiceControler(fn);
            //StatusSettings stuSettings = new StatusSettings(fn);
            //string invStatus = invControler.getInvoiceStatus(regno);
            string sqlStatus = "select status_name from ref_Status Where status_usedid='" + invStatus + "'";
            result = fn.GetDataByCommand(sqlStatus, "status_name");
            if (result == "0" || string.IsNullOrEmpty(result))
            {
                result = "Pending";
            }
        }
        catch (Exception ex)
        { }

        return result;
    }

    #region Invoice
    public string getInvoiceStatus(string invStatus)
    {
        string result = string.Empty;
        try
        {
            //InvoiceControler invControler = new InvoiceControler(fn);
            //StatusSettings stuSettings = new StatusSettings(fn);
            //string invStatus = invControler.getInvoiceStatus(regno);
            string sqlStatus = "select status_name from ref_Status Where status_usedid='" + invStatus + "'";
            result = fn.GetDataByCommand(sqlStatus, "status_name");
            if (result == "0" || string.IsNullOrEmpty(result))
            {
                result = "Pending";
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getPaymentMethod(string paymentMethod, string showid)
    {
        string result = string.Empty;
        try
        {
            string sqlStatus = "Select method_name From ref_PaymentMethod Where method_usedid='" + paymentMethod + "' And ShowID='" + showid + "'";//tmp_refPaymentMethod [changed on 27-3-2019 th]
            result = fn.GetDataByCommand(sqlStatus, "method_name");
            if (result == "0" || string.IsNullOrEmpty(result))
            {
                result = "N/A";
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getPaidPrice(string InvoiceID)
    {
        string result = string.Empty;
        try
        {
            PaymentControler pControl = new PaymentControler(fn);
            decimal totalPaidAmt = pControl.GetPaymentTotalPaidAmtByInvNo(InvoiceID);
            result = String.Format("{0:f2}", totalPaidAmt);
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string calculateOutstanding(string grandTotal, string InvoiceID)
    {
        string result = string.Empty;
        try
        {
            Double grandtotalAmount = 0;
            if (!string.IsNullOrEmpty(grandTotal))
            {
                Double.TryParse(grandTotal, out grandtotalAmount);
            }
            string paidprice = getPaidPrice(InvoiceID);
            Double paidAmount = 0;
            if (!string.IsNullOrEmpty(paidprice))
            {
                Double.TryParse(paidprice, out paidAmount);
            }
            result = String.Format("{0:f2}", (grandtotalAmount - paidAmount));
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getCongressSelection(string regno, string groupid, string invStatus, string invoiceID)
    {
        string result = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                OrderControler oControl = new OrderControler(fn);
                //string currenturl= Request.Url.AbsoluteUri;
                string fullUrl = "GRP=" + cFun.EncryptValue(groupid) + "&INV=" + cFun.EncryptValue(regno) + "&SHW=" + cFun.EncryptValue(showID)
                    + "&FLW=" + cFun.EncryptValue(ddl_flowList.SelectedItem.Value);
                FlowURLQuery urlQuery = new FlowURLQuery(fullUrl);

                StatusSettings stuSettings = new StatusSettings(fn);
                if (invStatus == stuSettings.Pending.ToString()
                    || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString()
                    || invStatus == noInvoice)
                {
                    OrderItemList oList = new OrderItemList();
                    oList = oControl.GetPendingOrderList(urlQuery);//***Pending Order
                    result = getCongressSelectionString(oList);
                }
                else
                {
                    List<OrderItemList> oSucList = getPaidOrder(urlQuery, invoiceID);//***Paid Order
                    if (oSucList.Count > 0)
                    {
                        foreach (OrderItemList ordList in oSucList)
                        {
                            result += getCongressSelectionString(ordList) + ",";
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return result.TrimEnd(',');
    }
    private List<OrderItemList> getPaidOrder(FlowURLQuery urlQuery, string invoiceID)
    {
        List<OrderItemList> oPaidOrderItemList = new List<OrderItemList>();
        try
        {
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cFun.DecryptValue(urlQuery.DelegateID);
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj != null && invListObj.Count > 0)
            {
                foreach (Invoice invObj in invListObj)
                {
                    if (invObj.InvoiceID == invoiceID)
                    {
                        OrderControler oControl = new OrderControler(fn);
                        oPaidOrderItemList = oControl.GetAllOrderedListByInvoiceID(urlQuery, invObj.InvoiceID);
                    }
                }
            }
        }
        catch (Exception ex)
        { oPaidOrderItemList = new List<OrderItemList>(); }

        return oPaidOrderItemList;
    }
    private string getCongressSelectionString(OrderItemList OList)
    {
        string selectedCongress = string.Empty;
        try
        {
            foreach (OrderItem oItem in OList.OrderList)
            {
                selectedCongress += HttpUtility.HtmlDecode(oItem.ItemDescription) + (!string.IsNullOrEmpty(oItem.ItemDescription_ShowedInTemplate) ? "[" + HttpUtility.HtmlDecode(oItem.ItemDescription_ShowedInTemplate) + "]" : "") + ",";
            }
            selectedCongress = selectedCongress.TrimEnd(',');
        }
        catch (Exception ex)
        { }

        return selectedCongress;
    }
    public bool isPaymentVisible(string invStatus)
    {
        bool isVisible = false;
        try
        {
            StatusSettings stuSettings = new StatusSettings(fn);
            //invStatus = getInvoiceStatus(invStatus);
            if (invStatus == stuSettings.Pending.ToString() || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
            {
                isVisible = true;
            }
        }
        catch (Exception ex)
        { }

        return isVisible;
    }
    public bool isDownloadReceiptVisible(string invStatus)
    {
        bool isVisible = false;
        try
        {
            StatusSettings stuSettings = new StatusSettings(fn);
            //invStatus = getInvoiceStatus(invStatus);
            if (invStatus == stuSettings.Success.ToString())
            {
                isVisible = true;
            }
        }
        catch (Exception ex)
        { }

        return isVisible;
    }
    #region checkPaymentMode
    private bool checkPaymentMode(string showID)
    {
        bool isUsedChequeTT = false;
        try
        {
            CommonDataObj cmdObj = new CommonDataObj(fn);
            DataTable dt = cmdObj.getPaymentMethods(showID);
            if (dt.Rows.Count > 0)
            {
                SiteSettings st = new SiteSettings(fn, showID);
                foreach (DataRow dr in dt.Rows)
                {
                    string usedid = dr["method_usedid"].ToString();
                    if (usedid == ((int)PaymentType.TT).ToString())
                    {
                        isUsedChequeTT = true;
                    }
                    if (usedid == ((int)PaymentType.Cheque).ToString())
                    {
                        isUsedChequeTT = true;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return isUsedChequeTT;
    }
    #endregion
    #endregion

}