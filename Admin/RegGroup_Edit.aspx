﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="RegGroup_Edit.aspx.cs" Inherits="Admin_RegGroup_Edit" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
    .red
    {
        color:red;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h3 class="box-title">Manage Group (Contact Person) Info</h3>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="form-horizontal">
                <div class="box-body">
                    <div class="form-group" runat="server" id="divSalutation">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblSalutation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlSalutation" runat="server" CssClass="form-control"
                                OnSelectedIndexChanged="ddlSalutation_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcSal" runat="server" 
                            ControlToValidate="ddlSalutation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divSalOther" visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            &nbsp;
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtSalOther" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcSalOther" runat="server"
                            ControlToValidate="txtSalOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divFName">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblFName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcFName" runat="server"
                            ControlToValidate="txtFName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divLName">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblLName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtLName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcLName" runat="server"
                            ControlToValidate="txtLName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divDesignation">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcDesig" runat="server"
                            ControlToValidate="txtDesignation" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divVisitDate">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblVisitDate" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtVisitDate" runat="server" CssClass="form-control"></asp:TextBox><%-- onblur="validateDate(this);"--%>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcVisitDate" runat="server"
                            ControlToValidate="txtVisitDate" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator></div>
                    </div>

                    <div class="form-group" runat="server" id="divVisitTime">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblVisitTime" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtVisitTime" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcVisitTime" runat="server"
                            ControlToValidate="txtVisitTime" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divOtherDesignation">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblOtherDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtOtherDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcOtherDesignation" runat="server"
                            ControlToValidate="txtOtherDesignation" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divDepartment">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblDepartment" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcDeptm" runat="server"
                            ControlToValidate="txtDepartment" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divCompany">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblCompany" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtCompany" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcCom" runat="server"
                            ControlToValidate="txtCompany" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divIndustry">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblIndustry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                           <asp:DropDownList ID="ddlIndustry" runat="server" CssClass="form-control"
                                OnSelectedIndexChanged="ddlIndustry_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcIndus" runat="server" 
                            ControlToValidate="ddlIndustry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divIndusOther" visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            &nbsp;
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtIndusOther" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcIndusOther" runat="server"
                            ControlToValidate="txtIndusOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAddress1">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAddress1" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAdd1" runat="server"
                            ControlToValidate="txtAddress1" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAddress2">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAddress2" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAdd2" runat="server"
                            ControlToValidate="txtAddress2" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAddress3">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAddress3" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>            
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAdd3" runat="server"
                            ControlToValidate="txtAddress3" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divCountry">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcCountry" runat="server" 
                                ControlToValidate="ddlCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divRCountry">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblRCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlRCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlRCountry_SelectedIndexChanged">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcRCountry" runat="server" 
                                ControlToValidate="ddlRCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divCity">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblCity" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                          <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcCity" runat="server"
                            ControlToValidate="txtCity" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divState">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblState" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcState" runat="server"
                            ControlToValidate="txtState" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divPostalcode">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblPostalcode" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtPostalcode" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcPCode" runat="server"
                            ControlToValidate="txtPostalcode" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divTel">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblTel" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5" runat="server">
                            <div class="col-xs-3" runat="server" id="divTelcc" style="padding-left:0px;" >
                                <asp:TextBox ID="txtTelcc" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTelcc" runat="server"
                                    TargetControlID="txtTelcc"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-3" runat="server" id="divTelac" style="padding-left:0px;" >
                                <asp:TextBox ID="txtTelac" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTelac" runat="server"
                                    TargetControlID="txtTelac"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-6" runat="server" id="divTelNo" style="padding-right:0px;padding-left:0px;">
                                <asp:TextBox ID="txtTel" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTel" runat="server"
                                    TargetControlID="txtTel"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcTel" runat="server"
                            ControlToValidate="txtTel" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcTelcc" runat="server"
                            ControlToValidate="txtTelcc" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcTelac" runat="server"
                            ControlToValidate="txtTelac" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divMobile">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblMobile" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5" runat="server" >
                            <div class="col-xs-3" runat="server" id="divMobcc" style="padding-left:0px;" >
                                <asp:TextBox ID="txtMobcc" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbMobcc" runat="server"
                                    TargetControlID="txtMobcc"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-3" runat="server" id="divMobac" style="padding-left:0px;" >
                                <asp:TextBox ID="txtMobac" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbMobac" runat="server"
                                    TargetControlID="txtMobac"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-6" runat="server" id="divMobileNo" style="padding-right:0px;padding-left:0px;">
                                <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbMob" runat="server"
                                    TargetControlID="txtMobile"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcMob" runat="server"
                            ControlToValidate="txtMobile" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcMobcc" runat="server"
                            ControlToValidate="txtMobcc" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcMobac" runat="server"
                            ControlToValidate="txtMobac" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divFax">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblFax" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5" runat="server" >
                            <div class="col-xs-3" runat="server" id="divFaxcc" style="padding-left:0px;" >
                                <asp:TextBox ID="txtFaxcc" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbFaxcc" runat="server"
                                    TargetControlID="txtFaxcc"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-3" runat="server" id="divFaxac" style="padding-left:0px;" >
                                <asp:TextBox ID="txtFaxac" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbFaxac" runat="server"
                                    TargetControlID="txtFaxac"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-6" runat="server" id="divFaxNo" style="padding-right:0px;padding-left:0px;">
                                <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbFax" runat="server"
                                    TargetControlID="txtFax"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcFax" runat="server"
                            ControlToValidate="txtFax" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcFaxcc" runat="server"
                            ControlToValidate="txtFaxcc" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcFaxac" runat="server"
                            ControlToValidate="txtFaxac" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divEmail">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:RequiredFieldValidator ID="vcEmail" runat="server"
                            ControlToValidate="txtEmail" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="validateEmail"
                            runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                            ControlToValidate="txtEmail"
                            ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divEmailConfirmation">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblEmailConfirmation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtEmailConfirmation" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:RequiredFieldValidator ID="vcEConfirm" runat="server"
                            ControlToValidate="txtEmailConfirmation" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cmpEmail" runat="server" ControlToValidate="txtEmailConfirmation"
                            ControlToCompare="txtEmail" ErrorMessage="Not match." ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAge">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAge" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAge" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbAge" runat="server"
                                TargetControlID="txtAge"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAge" runat="server"
                            ControlToValidate="txtAge" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divDOB">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblDOB" runat="server" CssClass="form-control-label" Text="Date of Birth"></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox><%-- onblur="validateDate(this);"--%>
                            <asp:CalendarExtender ID="calendarDOB" TargetControlID="txtDOB"
                                    runat="server" PopupButtonID="txtDOB" Format="dd/MM/yyyy" ></asp:CalendarExtender>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcDOB" runat="server" Enabled="false"
                            ControlToValidate="txtDOB" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                     <div class="form-group" runat="server" id="divGender">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblGender" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                                <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <%--<asp:CompareValidator ID="vcGender" runat="server" 
                                ControlToValidate="ddlGender" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>--%>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divPassword">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblPassword" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcPassword" runat="server"
                            ControlToValidate="txtPassword" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAdditional4">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAdditional4" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAdditional4" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAdditional4" runat="server"
                            ControlToValidate="txtAdditional4" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAdditional5">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAdditional5" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAdditional5" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAdditional5" runat="server"
                            ControlToValidate="txtAdditional5" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-6">
                            <asp:Button runat="server" Text="Update" id="btnupdate" OnClick="btnupdate_Click" CssClass="btn btn-primary"/>
                            <asp:HiddenField runat="server" ID="hfRegGroupID" Value=""></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfStage" Value=""></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfIsMultiple" Value=""></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfReferralCode" Value=""></asp:HiddenField>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <br />
    <asp:Label runat="server" ID="lbls"></asp:Label>

</asp:Content>

