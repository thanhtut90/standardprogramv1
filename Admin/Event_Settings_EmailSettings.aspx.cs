﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Utilities;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using Corpit.Site.Email;
public partial class Admin_Event_Settings_EmailSettings : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    static string _SMTPUsername = "SMTPUsernname";
    static string _SMTPPassword = "SMTPPassword";
    static string _SMTPClient = "SMTPClient";
    static string _UseDefaultCredentials = "UseDefaultCredentials";
    static string _EnableSsl = "EnableSsl";
    static string _FromEmailAddress = "FromEmailAddress";
    static string _FromEmailDesc = "FromEmailDesc";

    protected void btnSetupEmail_Onclick(object sender, EventArgs e)
    {
        Response.Redirect("SiteTemplate?SHW=" + Request.QueryString["SHW"].ToString());
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                if (!checkCreated(showid))
                {
                    Response.Redirect("Event_Config");
                }

                bindShowName(showid);
                LoadSiteEmailConfig(showid);
                LoadEmailConfigSetting(txtPortalRefID.Text);
            }

            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }
    protected void SaveForm(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            try
            {
                string eventname = string.Empty;
                string smtpuser = string.Empty;
                string smtppswd = string.Empty;
                string smtpclient = string.Empty;
                string fea = string.Empty;
                string fed = string.Empty;
                string udc = string.Empty;
                string enablessl = string.Empty;

                eventname = cFun.solveSQL(txtEventName.Text.ToString());
                smtpuser = txtsmtpuser.Text;
                smtppswd = txtsmtppswd.Text;
                smtpclient = txtsmtpclient.Text;
                fea = txtfea.Text;
                fed = txtfed.Text;
                udc = Server.HtmlEncode(ddludc.SelectedValue.ToString());
                enablessl = Server.HtmlEncode(ddlenablessl.SelectedValue.ToString());
                string message = ValidateCredentials(smtpuser, smtppswd, fea, smtpclient, Boolean.Parse(udc), Boolean.Parse(enablessl));

                if (message == "")
                {
                    ePoratalShowConfigJsonData showConfig = new ePoratalShowConfigJsonData();
                    showConfig.ShowID = txtPortalRefID.Text;
                    showConfig.SMTPUsername = txtsmtpuser.Text;
                    showConfig.SMTPPassword = txtsmtppswd.Text;
                    showConfig.SMTPClient = txtsmtpclient.Text;
                    showConfig.UseDefaultCredentials = ddludc.SelectedValue;
                    showConfig.EnableSsl = ddlenablessl.SelectedValue;
                    showConfig.FromEmailAddress = txtfea.Text;
                    showConfig.FromEmailDesc = txtfed.Text;
                    showConfig.EmailBanner = "";

                    EmailHelper sHelper = new EmailHelper();
                   bool OK= sHelper.SetConfigSettings(showConfig);
                    if (OK)
                    {
                        pnlmessagebar.Visible = true;
                        lblemsg.Text = "Updated Successfully";
                        showid = cFun.EncryptValue(showid);
                        Response.Redirect("Event_Settings_MasterPage?SHW=" + showid);
                    }
                }
                else
                {
                    pnlmessagebar.Visible = true;
                    lblemsg.Text = message;
                }
            }
            catch (Exception ex)
            {
                pnlmessagebar.Visible = true;
                lblemsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

    }
    #region ValidateCredentials (check whether the smtp details given is valid by sending an email)
    public static string ValidateCredentials(string login, string password, string fea, string server, bool defaultCred, bool enableSsl)
    {
        SmtpClient smtpClient = new SmtpClient(server);
        NetworkCredential credentials = new NetworkCredential(login, password);
        smtpClient.UseDefaultCredentials = defaultCred;
        smtpClient.Credentials = credentials;
        smtpClient.EnableSsl = enableSsl;
        try
        {
            //  smtpClient.Send(new MailMessage(fea, "tanyongkuan@corpit.com.sg", "test", "This is a testing email."));
            return string.Empty;
        }
        catch (SmtpFailedRecipientException)
        {
            return string.Empty;
        }
        catch (Exception ex)
        {
            return string.Format("SMTP server connection test failed: {0}", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
        }
    }
    #endregion

    #region checkCreated (check the record count of tmp_tbSiteSettings table is equal to the record counts of tb_site_settings table)
    private bool checkCreated(string showid)
    {
        bool isCreated = false;
        DataTable dt = fn.GetDatasetByCommand("Select * From tmp_tbSiteSettings", "ds").Tables[0];

        string query = "Select * From tb_site_settings Where ShowID=@SHWID And settings_name In (Select settings_name From tmp_tbSiteSettings)";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dtSettings = fn.GetDatasetByCommand(query, "dsSettings", pList).Tables[0];
        if (dt.Rows.Count == dtSettings.Rows.Count)
        {
            isCreated = true;
        }

        return isCreated;
    }
    #endregion

    #region bindShowName
    private void bindShowName(string showid)
    {
        string query = "Select SHW_Name From tb_Show Where SHW_ID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        string showName = fn.GetDataByCommand(query, "SHW_Name", pList);

        txtEventName.Text = showName == "0" ? string.Empty : showName;
    }

    private void LoadSiteEmailConfig(string showID)
    {
        SiteEmailConfigHelper eConHelper = new SiteEmailConfigHelper(fn);
        SiteEmailConfig eConfig = eConHelper.GetSiteEmailConfig(showID);
        if (string.IsNullOrEmpty(eConfig.RefEmailProjID))
        {
            EmailHelper sEmailHepler = new EmailHelper();
            string refShowID = sEmailHepler.CreateShowMasterInPortal(txtEventName.Text, "Admin");
            if (!string.IsNullOrEmpty(refShowID) && refShowID.ToUpper() != "ERROR")
            {
                eConfig.ShowID = showID;
                eConfig.RefEmailProjID = refShowID;
                eConfig.EmailDesc = showID + " Show Config";
                eConfig.BannerURL = "";
                bool isOK = eConHelper.SetSiteEmailConfig(eConfig);
                if (!isOK)
                    Response.Redirect("Login");
                else
                    txtPortalRefID.Text = refShowID;
            }
            // Create New

        }
        {
            //Update
            txtPortalRefID.Text = eConfig.RefEmailProjID;
        }
    }


    private void LoadEmailConfigSetting(string refPortalID)
    {
        //ePoratalShowConfigJsonData showConfig = new ePoratalShowConfigJsonData();
        //showConfig.ShowID = txtPortalRefID.Text;
        //showConfig.SMTPUsername = txtsmtpuser.Text;
        //showConfig.SMTPPassword = txtsmtppswd.Text;
        //showConfig.SMTPClient = txtsmtpclient.Text;
        //showConfig.FromEmailAddress = txtfea.Text;
        //showConfig.FromEmailDesc = txtfed.Text;
        // = "";

        ePoratalShowConfigJsonData showConfig = new ePoratalShowConfigJsonData();
        showConfig.ShowID = txtPortalRefID.Text;
        EmailHelper sHelper = new EmailHelper();
        showConfig = sHelper.GetShowConfigSettings(showConfig);

        if (showConfig != null && !string.IsNullOrEmpty(showConfig.SMTPUsername))
        {
            txtsmtpuser.Text = showConfig.SMTPUsername;
            txtsmtppswd.Text = showConfig.SMTPPassword;
            txtsmtpclient.Text = showConfig.SMTPClient;
            txtfea.Text = showConfig.FromEmailAddress;
            txtfed.Text = showConfig.FromEmailDesc;
            // showConfig.EmailBanner;
            ListItem li = ddlenablessl.Items.FindByValue(showConfig.EnableSsl);
            if (li != null) li.Selected = true;
            ListItem li2 = ddludc.Items.FindByValue(showConfig.UseDefaultCredentials);
            if (li2 != null) li2.Selected = true;
        }

    }

    #endregion

    #region Back
    protected void Back(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            Response.Redirect("Event_Settings_MasterPage?SHW=" + showid);
        }
        else
        {
            Response.Redirect("Login");
        }
    }
    #endregion
}