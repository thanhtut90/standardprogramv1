﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using Corpit.Site.Utilities;
using ClosedXML.Excel;
using System.IO;

public partial class Admin_DoorScanningReport : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    string showID = string.Empty;
    static string _Name = "Name";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _City = "City";
    static string _State = "State";
    static string _ZipCode = "Zip Code";
    static string _Country = "Country";
    static string _TelCC = "Telcc";
    static string _TelAC = "Telac";
    static string _Tel = "Tel";
    static string _FaxCC = "Faxcc";
    static string _FaxAC = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Website = "Website";
    static string _Additional1 = "Additional1";
    static string _Additional2 = "Additional2";
    static string _Additional3 = "Additional3";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string[] showList = new string[2];

            try
            {
                if (Session["userid"] != null)
                {
                    string userID = Session["userid"].ToString();

                    binddata();

                    if (Session["roleid"].ToString() == "1")
                    {
                        //showList = getAllShowList();
                        showID = ddl_showList.SelectedValue;
                    }
                    else
                    {
                        showlist.Visible = false;
                        //showList = getShowList();
                        getShowID(userID);
                    }

                    bindDoorScanningReport(showID);
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void binddata()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_showList.Items.Clear();
                ddl_showList.DataSource = cmd.ExecuteReader();
                ddl_showList.DataTextField = "SHW_Name";
                ddl_showList.DataValueField = "SHW_ID";
                ddl_showList.DataBind();
                con.Close();
            }
        }
    }
    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }
    protected string[] getAllShowList()
    {
        string query = "Select * from tb_Show Order By SHW_ID";
        string showList = string.Empty;
        string showName = string.Empty;

        DataTable dt = fn.GetDatasetByCommand(query, "dsShow").Tables[0];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (showList == string.Empty)
            {
                showList = dt.Rows[i]["SHW_ID"].ToString();
                showName = dt.Rows[i]["SHW_Name"].ToString();
            }
            else
            {
                showList += "," + dt.Rows[i]["SHW_ID"].ToString();
                showName += "," + dt.Rows[i]["SHW_Name"].ToString();
            }
        }
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        return list;
    }
    protected string[] getShowList()
    {
        string showList = string.Empty;
        string showName = string.Empty;
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = Session["userid"].ToString();
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                query = "Select * from tb_Show WHERE SHW_ID=@showID";
                spar = new SqlParameter("showID", SqlDbType.NVarChar);

                string[] showLists = dt.Rows[0]["us_showid"].ToString().Split(',');
                for (int i = 0; i < showLists.Length; i++)
                {
                    pList.Clear();
                    spar.Value = showLists[i];
                    pList.Add(spar);

                    DataTable dtShow = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];

                    if (showName == string.Empty)
                    {
                        showName = dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                    else
                    {
                        showName += "," + dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                }
                list[0] = dt.Rows[0]["us_showid"].ToString();
                list[1] = showName;
                return list;
            }
            return list;
        }
        catch (Exception ex)
        {
            return list;
        }
    }
    protected void bindDoorScanningReport(string showid)
    {
        DataTable dt = DoorScanningReport(showid);
        gvDoorScanningReport.DataSource = dt;
        gvDoorScanningReport.DataBind();
    }

    protected void ddl_showList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;
        bindDoorScanningReport(showID);
    }

    #region DoorScanningReport
    private DataTable DoorScanningReport(string showid)
    {
        DataTable dt = new DataTable();
        string query = "Select t.Regno,t.TurnUpDate as TurnUpDateTime,t.Category From tb_Onsite_TurnUpLog as t Where t.ShowID='" + showid + "' Order By TurnUpDate";
        dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
        return dt;
    }
    #endregion

    protected void genDoorScanningReportExcel(object sender, EventArgs e)
    {
        ExportToExcel(gvDoorScanningReport, "DoorScanningReport");
    }

    #region ExportToExcel
    private void ExportToExcel(GridView gv, string filename)//, DataTable dt)
    {
        DataTable dt = new DataTable();

        //*convert Dynamic Gridview to DataTable
        if (gv.Rows.Count > 0)
        {
            // add the columns to the datatable
            if (gv.HeaderRow != null)
            {
                for (int i = 0; i < gv.HeaderRow.Cells.Count; i++)
                {
                    dt.Columns.Add(System.Net.WebUtility.HtmlDecode(gv.HeaderRow.Cells[i].Text.Replace("&nbsp;", "")));
                }
            }

            //  add each of the data rows to the table
            foreach (GridViewRow row in gv.Rows)
            {
                DataRow dr;
                dr = dt.NewRow();

                for (int i = 0; i < row.Cells.Count; i++)
                {
                    dr[i] = System.Net.WebUtility.HtmlDecode(row.Cells[i].Text.Replace("&nbsp;", ""));
                }
                dt.Rows.Add(dr);
            }
        }
        //*

        XLWorkbook wb = new XLWorkbook();
        var ws = wb.Worksheets.Add("List");
        ws.Cell(1, 1).InsertTable(dt);
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format(@"attachment;filename={0}.xlsx", filename));
        ws.FirstRow().AddConditionalFormat().WhenGreaterThan(20000).Fill.SetBackgroundColor(XLColor.OrangeRed);//add bg color at first row(header)
        ws.Tables.FirstOrDefault().ShowAutoFilter = false;//remove first blank column and filter

        using (MemoryStream memoryStream = new MemoryStream())
        {
            wb.SaveAs(memoryStream);
            memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
            memoryStream.Close();
        }

        HttpContext.Current.Response.End();
    }
    #endregion
}