﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="VendorPhotoList.aspx.cs" Inherits="Admin_VendorPhotoList" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../Content/dist/css/skins/_all-skins.min.css">
    <%--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>--%> <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }

        $(function () {
            $.fn.datepicker.defaults.format = "dd/mm/yyyy";
            //     $("#txtFromDate").datepicker({}).val()
            //    $("#txtToDate").datepicker({}).val()
        });
    </script>
    <style type="text/css">
        .tdstyle1 {
            width: 170px;
        }

        .tdstyle2 {
            width: 300px;
        }

        .searchTxtStyle {
            width: 39% !important;
        }

        .selectedRcdStyle {
            width: 100% !important;
        }

        .ajax__calendar_container {
            width: 320px !important;
            height: 280px !important;
        }

        .ajax__calendar_body {
            width: 100% !important;
            height: 100% !important;
        }

        td {
            vertical-align: middle;
        }
    </style>
    <script type="text/javascript">

        function chkClick(obj, id) {
            AddorRemoveIdInHiddenField($("#ContentPlaceHolder1_txtSelectedRegno").get(0), id, obj.checked);
        }

        function AddorRemoveIdInHiddenField(hf, id, IsAdd) {
            if (hf.value == "") {
                //hf.value = ",";
            }

            if (IsAdd == true) {
                if (hf.value.indexOf("," + id + ",") == -1) {
                    hf.value = hf.value + id + ",";
                }
            }
            else if (IsAdd == false) {
                if (hf.value.indexOf("," + id + ",") >= 0) {
                    hf.value = hf.value.replace("," + id + ",", ",");
                }
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    &nbsp;&nbsp;&nbsp;
    <asp:LinkButton ID="btnAddNew" runat="server" CssClass="btn btn-success" OnClick="lnkExcel_Clicked" Visible="false">
            <span aria-hidden="true" class="glyphicon glyphicon-export"></span> EXPORT EXCEL
    </asp:LinkButton>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="" Transparency="30">
                    <asp:Label ID="Label2" runat="server" Text="Loading..">
                    </asp:Label>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/loading-icon.gif" />
                </telerik:RadAjaxLoadingPanel><%-- BackgroundTransparency="50"--%>
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true" LoadingPanelID="RadAjaxLoadingPanel1">

            <asp:Label ID="lblUser" runat="server" Visible="false"></asp:Label>
            <h3 class="box-title">Master Registration List (Individual)</h3>
            <div id="divsearch" runat="server" class="row" style="margin-bottom: 10PX;" visible="true">
                <div class="form-group col-sm-12">
                    <label for="inputEmail3" class="col-sm-1 control-label" style="display:none;">Show List: &nbsp;</label>
                    <div class="col-sm-2">
                        <asp:Panel ID="showlist" runat="server">
                            <asp:DropDownList ID="ddl_showList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_showList_SelectedIndexChanged" CssClass="form-control">
                            </asp:DropDownList>
                        </asp:Panel>
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label for="inputEmail3" class="col-sm-1 control-label"> Flow List: &nbsp;</label>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="ddl_flowList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_flowList_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div style="display:none;">
                        <label for="inputEmail3" class="col-sm-1 control-label">   Payment Status: &nbsp;</label>
                        <div class="col-sm-2">
                            <asp:DropDownList ID="ddl_paymentStatus" runat="server" OnSelectedIndexChanged="ddl_paymentStatus_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-group col-sm-10" style="display:none;">
                    <asp:TextBox ID="txtFromDate" placeholder="From" runat="server" ClientIDMode="Static" onclick="$(this).datepicker().datepicker('show')" CssClass="form-control col-sm-1"></asp:TextBox>
                    <%-- <label for="inputEmail3" class="col-sm-1 control-label">  To </label>--%>
                    <div class="col-sm-1">
                    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" Enabled="false">To</asp:LinkButton>
                    </div>
                    <asp:TextBox ID="txtToDate" placeholder="To " runat="server" onclick="$(this).datepicker().datepicker('show')" CssClass="form-control col-sm-1"></asp:TextBox>
                    <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-success" runat="server" ><i class="fa fa-search"></i> </asp:LinkButton>
                </div>
                <div class="form-group col-sm-10">
                    <asp:TextBox ID="txtKey" placeholder="Enter Key Word (Registration Number, Name, Passport No)" runat="server" ClientIDMode="Static" CssClass="form-control  col-sm-1 searchTxtStyle"></asp:TextBox>
                    <div class="col-sm-1">
                        <asp:LinkButton ID="btnKeysearch" OnClick="btnKeysearch_Click" class="btn btn-success" runat="server" ><i class="fa fa-search"></i> &nbsp;Search </asp:LinkButton>
                    </div>
                </div>
            </div>

            <br /><br />
            <div class="row">
                <div class="form-group col-sm-12">
                    <label for="inputEmail3" class="col-sm-1 control-label">Selected Record(s)</label>
                    <div class="form-group col-sm-7">
                        <asp:TextBox ID="txtSelectedRegno" runat="server" Visible="true" CssClass="form-control selectedRcdStyle" Enabled="false" ></asp:TextBox>
                    </div>
                    <div class="form-group col-sm-2">
                        <asp:Button ID="btnApprovedPhoto" runat="server" Text="Approve Select Photo(s)" OnClick="btnApprovedPhoto_Click"
                            CssClass="btn btn-warning"/>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div style="overflow-x: scroll;">
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientEvents-OnRequestStart="onRequestStart"><%--UpdateInitiatorPanelsOnly="true">--%>
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial" />
                                <%--<telerik:AjaxUpdatedControl ControlID="txtSelectedRegno"></telerik:AjaxUpdatedControl>--%>
                                <telerik:AjaxUpdatedControl ControlID="btnApprovedPhoto"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <%-- <telerik:AjaxSetting AjaxControlID="btnupdate">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                            <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial"></telerik:AjaxUpdatedControl>
                        </UpdatedControls>
                    </telerik:AjaxSetting>--%>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">--%>
                    <asp:Panel runat="server" ID="PanelKeyList">
                        <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true" 
                            EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                            OnNeedDataSource="GKeyMaster_NeedDataSource" PageSize="30" OnItemCreated="RadGrid1_ItemDataBound"
                            OnPageIndexChanged="GKeyMaster_PageIndexChanged"> <%-- OnItemCommand="GKeyMaster_ItemCommand" OnItemDataBound="grid_ItemDataBound"    --%>
                            <ClientSettings AllowKeyboardNavigation="false" EnablePostBackOnRowClick="false">
                                <Selecting AllowRowSelect="false"></Selecting>
                            </ClientSettings>
                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="Regno,RegGroupID" AllowFilteringByColumn="True" ShowFooter="false">
                                <CommandItemSettings ShowExportToExcelButton="false" ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="" UniqueName="PhotoApprove">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="MyCheckBox" runat="server" Text='<%#Eval("Regno") %>' 
                                                 />
                                            <%--AutoPostBack="true" OnCheckedChanged="MyCheckBox_CheckedChanged" --%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <%--<telerik:GridTemplateColumn HeaderText="Photo Approved" UniqueName="PhotoApproved">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnPhotoApproved" runat="server" OnCommand="btnPhotoApproved_Command"
                                                Text="Approve Photo" ForeColor="DarkBlue" CssClass="btn btn-success"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID")%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RegGroupID" FilterControlAltText="Filter RegGroupID"
                                        HeaderText="RegGroupID" SortExpression="RegGroupID" UniqueName="RegGroupID" Visible="false">
                                    </telerik:GridBoundColumn>
                                    <%-- **Added Image 03-Oct-2018 --%>
                                    <telerik:GridTemplateColumn 
                                        HeaderText="Serial No" UniqueName="serial_No" Exportable="true">
                                        <ItemTemplate>
                                            <%# Container.DataSetIndex + 1 %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Regno" FilterControlAltText="Filter Regno"
                                        HeaderText="Regno" SortExpression="Regno" UniqueName="Regno">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_FName" FilterControlAltText="Filter reg_FName"
                                        HeaderText="Applicant's Name (In full)" SortExpression="reg_FName" UniqueName="reg_FName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_PassNo" FilterControlAltText="Filter reg_PassNo"
                                        HeaderText="NRIC / FIN / ID" SortExpression="reg_PassNo" UniqueName="reg_PassNo">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="ProfessionName" HeaderText="Nationality" UniqueName="ProfessionName"
                                        FilterControlAltText="Filter ProfessionName" SortExpression="ProfessionName" Exportable="true">
                                        <ItemTemplate>
                                             <%# Eval("ProfessionName") %>
                                            <%--<%# bindProfession(Eval("reg_Profession").ToString(), Eval("reg_otherProfession").ToString())%>--%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_datecreated" FilterControlAltText="Filter reg_datecreated"
                                        HeaderText="Created Date" SortExpression="reg_datecreated" UniqueName="reg_datecreated">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="imageFileName" FilterControlAltText="Filter imageFileName"
                                        HeaderText="Image File Name" SortExpression="imageFileName" UniqueName="imageFileName">
                                    </telerik:GridBoundColumn>

                                    <%-- **Added Image 03-Oct-2018 --%>
                                    <%--<telerik:GridTemplateColumn 
                                        HeaderText="Photo" UniqueName="imageURL" Exportable="true">
                                        <ItemTemplate>
                                            <asp:Image ID="imgPhoto" runat="server" 
                                                ImageUrl='<%#getImgPath(Eval("Regno").ToString())%>' Visible='<%#getImgVisible(Eval("Regno").ToString())%>' 
                                                Width="400" Height="515"/>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                                    <telerik:GridTemplateColumn 
                                        HeaderText="Photo" UniqueName="imageURL" Exportable="true">
                                        <ItemTemplate>
                                            <asp:Image ID="imgPhoto" runat="server" 
                                                ImageUrl='<%#Eval("imagePath")%>' 
                                                Width="35mm" Height="45mm"/>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </asp:Panel>
                </telerik:RadAjaxPanel>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


