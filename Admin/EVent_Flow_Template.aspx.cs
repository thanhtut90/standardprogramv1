﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Admin_EVent_Flow_Template : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                PanelKeyDetail.Visible = false;
                txtShowID.Text = cFun.DecryptValue(Request.Params["SHW"].ToString());

                if(Request.Params["FLW"] != null)
                {
                    string flowid = cFun.DecryptValue(Request.Params["FLW"].ToString());

                    //get route id (group, indiv, indiv without conf,..) from tb_site_flow_template_master table
                    string selectedRouteID = fn.GetDataByCommand("Select tm.Route_ID From tb_site_flow_master as fm Right Join tb_site_Flow_Template_Master as tm On fm.FLW_Desc=tm.[Desc] Where fm.FLW_ID='" + flowid + "'", "Route_ID");
                    if (!string.IsNullOrEmpty(selectedRouteID))
                    {
                        txtSelectedRoute.Text = selectedRouteID;
                        bool isHasTemplate = LoadFlowDetails(selectedRouteID);
                        if (isHasTemplate)
                        {
                            PanelKeyDetail.Visible = true;
                        }
                        else
                        {
                            Response.Redirect("Event_Flow_Management.aspx?SHW=" + cFun.EncryptValue(txtShowID.Text));
                        }
                    }
                    else
                    {
                        //PanelKeyDetail.Visible = false;
                        Response.Redirect("Event_Flow_Management.aspx?SHW=" + cFun.EncryptValue(txtShowID.Text));
                    }
                    //

                    PanelKeyList.Visible = false;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_Show table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        string query = "Select * From tb_site_Flow_Template_Master";
        DataTable dt = fn.GetDatasetByCommand(query, "dsShow").Tables[0];

        if (dt.Rows.Count > 0)
        {
            GKeyMaster.DataSource = dt;
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string curid = "";
        foreach (GridDataItem item in GKeyMaster.SelectedItems)
        {
            curid = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Route_ID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Route_ID"].ToString() : "";
        }
        if (e.CommandName == "Delete")
        {
        }
        else
        {
            if (curid != "" && curid != null)
            {
                txtSelectedRoute.Text = curid;
                bool isHasTemplate = LoadFlowDetails(curid);
                if (isHasTemplate) PanelKeyDetail.Visible = true;
                else PanelKeyDetail.Visible = false;
            }
        }
    }
    #endregion


    #region LoadDetails

    private bool LoadFlowDetails(string routeID)
    {

        string query = "select t.*,m.script_desc from tb_site_flow_template t left join tb_site_module m on m.module_id=t.Module_Id  where Route_ID=@RID order by RouteOrder asc";
        SqlParameter spar = new SqlParameter("RID", SqlDbType.NVarChar);
        spar.Value = routeID;
        List<SqlParameter> pList = new List<SqlParameter>();
        pList.Add(spar);

        DataTable dt = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];
        bool hasData = false;
        if (dt.Rows.Count > 0)
        {
            RptSteps.DataSource = dt;
            RptSteps.DataBind();
            hasData = true;

        }
        else
            RptSteps.DataSource = null;

        return hasData;
    }

    protected void RptSteps_DataBond(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox isAllowSkip = (TextBox)e.Item.FindControl("isAllowSkip");
            TextBox isShowRow = (TextBox)e.Item.FindControl("isShowRow");
            CheckBox chckRemoveFromFlow = (CheckBox)e.Item.FindControl("chckRemoveFromFlow");

            Panel Row = (Panel)e.Item.FindControl("PanelRow");
            if (isAllowSkip.Text == "1")
            {
                chckRemoveFromFlow.Enabled = true;
            }
            else
            {
                chckRemoveFromFlow.Enabled = false;
            }
            if (isShowRow.Text == "1") Row.Visible = true;
            else Row.Visible = false;
        }
    }

    protected void btnCreateFlow_Onclick(object sender, EventArgs e)
    {
        List<Flow> fList = new List<Flow>();
        FlowMaster fMaster = new FlowMaster();
        string RouteID = txtSelectedRoute.Text;
        string ShowID = txtShowID.Text;
        string flowname = txtFlowName.Text.Trim();
        if (!string.IsNullOrEmpty(ShowID) && !string.IsNullOrEmpty(RouteID))
        {
            string sql = "select * from tb_site_Flow_Template_Master where Route_ID=@RID";
            SqlParameter spar = new SqlParameter("RID", SqlDbType.NVarChar);
            spar.Value = RouteID;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar);

            DataTable dt = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];
            if (dt.Rows.Count > 0)
            {
                fMaster.ShowID = ShowID;
                fMaster.FlowName = flowname;
                fMaster.FlowDesc = dt.Rows[0]["Desc"].ToString();
                fMaster.FlowInvGenereateType = dt.Rows[0]["InvType"].ToString();
                fMaster.FlowType = dt.Rows[0]["FlowType"].ToString();
                fMaster.FlowRefTemplate = RouteID;
                CategoryClass catClass = new CategoryClass();
                fMaster.FlowCategoryConfigType = catClass.flowcat_ByFlow.ToString();

                //
                int canSkipConf = 0;
                if(RouteID == RouteTemplateType.Route_WithoutConference)
                {
                    canSkipConf = 1;
                }
                else
                {
                    //if(chkCanSkipConf.Checked)
                    //{
                    //    canSkipConf = 1;
                    //}
                }
                //

                fMaster.FlowConfSkip = canSkipConf.ToString();

                FlowControler fControl = new FlowControler(fn);

                List<TemplateRouteDetail> SelectedRouteList = GetSelectedRouteList(RouteID);
                if (SelectedRouteList.Count > 0)
                {
                    string tmpid = "0";
                    #region Create SuccessTemplate
                    {
                        //string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                        //TemplateControler tmpControler = new TemplateControler(fn);
                        //TemplateObj tmpObj = new TemplateObj();
                        //tmpObj.tempname = "";
                        //tmpObj.tempvalue = Server.HtmlEncode(txtWelcomeMsg.Text);
                        //tmpObj.tempDBSource = "";
                        //tmpObj.tempsubject = "";
                        //tmpObj.temptype = 3;
                        //tmpObj.showid = showid;
                        //bool rowUpdated1 = tmpControler.CreateNewTemplate(tmpObj, ref tmpid);
                    }
                    #endregion

                    List<Flow> FlowList = GetFlowList(SelectedRouteList);
                    fMaster.FlowSuccessTmpID = "";
                    fMaster.FlowWelcomePageTmpID = tmpid;
                    bool isOK =fControl.CreateNewFlowFromTemplate(fMaster, FlowList);

                    if (isOK)
                    {
                        string FlowID = fMaster.FlowID;
                        string showID = Request.Params["SHW"].ToString();
                        string url = string.Format("{0}?FLW={1}&SHW={2} ", "Event_Flow_Dashboard", FlowID, showID);
                        Response.Redirect(url);
                        //Dictionary<string, string> nValues = new Dictionary<string, string>();
                        //nValues = fControl.GetAdminNextRoute(fMaster.FlowID, "0");
                        //if (nValues.Count > 0)
                        //{
                        //    string page = nValues["nURL"].ToString();
                        //    string step = nValues["nStep"].ToString();
                        //    string FlowID = nValues["FlowID"].ToString();

                        //    if (page == "") page = "Event_Config_Final";

                        //    string path = fControl.MakeFullURL(page, FlowID, ShowID, "", step);

                        //    Response.Redirect(path);
                        //}
                    }
                }
            }
        }
    }

    private List<TemplateRouteDetail> GetSelectedRouteList(string RouteID)
    {

        List<TemplateRouteDetail> SelectedRouteList = new List<TemplateRouteDetail>();
        List<TemplateRouteDetail> FinalList = new List<TemplateRouteDetail>();
        int indx = 1;
        foreach (RepeaterItem item in RptSteps.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chckRemoveFromFlow = (CheckBox)item.FindControl("chckRemoveFromFlow");
                TextBox templateID = (TextBox)item.FindControl("txtTemplateID");
                TextBox txtRouteOrder = (TextBox)item.FindControl("txtRouteOrder");
                TextBox ChkOutModule = (TextBox)item.FindControl("ChkOutModule");
                TextBox txtModuleID = (TextBox)item.FindControl("txtModuleID");
                TextBox txtTitle = (TextBox)item.FindControl("txtTitle");

                if (!chckRemoveFromFlow.Checked)
                {
                    //Flow fDetail = new Flow();
                    //sql = "select t.*,m.script_desc,m.script_id from tb_site_flow_template t left join tb_site_module m on m.module_id=t.Module_Id  where Route_ID='{0}'  and t.ID='{1}'";
                    //sql = string.Format(sql, RouteID, templateID);
                    //DataTable dTemp = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
                    //if (dTemp.Rows.Count > 0)
                    //{
                    //    fDetail.ShowID = ShowID;
                    //    fDetail.FlowID = flowID;
                    // //   fDetail.ScriptID=
                    //        }


                    TemplateRouteDetail rDeetail = new TemplateRouteDetail();
                    rDeetail.RouteID = RouteID;
                    rDeetail.ID = templateID.Text;
                    rDeetail.RouteOrder = indx.ToString();
                    rDeetail.ModuleID = txtModuleID.Text;
                    rDeetail.Title = txtTitle.Text;
                    rDeetail.CheckOut = ChkOutModule.Text;

                    SelectedRouteList.Add(rDeetail);

                    indx += indx; // Route Order According to Selected from template
                }
            }
        }
        string LoopStart = "";
        string LoopStartModule = "";
        string CheoutMoudule = "";
        string CheckoutIndx = "";

        indx = 0;
        for (int i = 0; i < SelectedRouteList.Count; i++)
        {
            TemplateRouteDetail rd = SelectedRouteList[i];

            TemplateRouteDetail New = new TemplateRouteDetail();

            New.RouteID = rd.RouteID;
            New.ID = rd.ID;
            New.ModuleID = rd.ModuleID;
            New.Title = rd.Title;
            if (rd.ModuleID == SiteFlowType.DEFAULT_MODULE_END)
            {
                if (!string.IsNullOrEmpty(LoopStart))
                    New.RouteOrder = SiteFlowType.DEFAULT_LOOP_END; //66 As Loop End

            }
            else
            {

                indx += 1;
                New.RouteOrder = indx.ToString();

                if (!string.IsNullOrEmpty(rd.CheckOut) && string.IsNullOrEmpty(LoopStart))
                {
                    LoopStart = New.RouteOrder;
                    LoopStartModule = New.ModuleID;

                    CheoutMoudule = rd.CheckOut;
                }

                if (CheoutMoudule == rd.ModuleID)
                    CheckoutIndx = New.RouteOrder;
            }

            if (FinalList.Count > 0)
            {
                FinalList[FinalList.Count - 1].NextSetp = New.RouteOrder;
                FinalList[FinalList.Count - 1].NextModule = New.ModuleID;
            }
            FinalList.Add(New);
        }

        if (FinalList.Count > 0)
        {
            for (int j = 0; j < FinalList.Count; j++)
            {

                if (FinalList[j].NextSetp == SiteFlowType.DEFAULT_LOOP_END)
                {
                    FinalList[j].NextSetp = LoopStart;
                    FinalList[j].NextModule = LoopStartModule;
                }
            }

            FinalList[FinalList.Count - 1].NextSetp = SiteFlowType.DEFAULT_FLOW_TERMINAL;
            FinalList[FinalList.Count - 1].NextModule = "";

            int loopStartInt = cFun.ParseInt(LoopStart);
            loopStartInt= loopStartInt -1;
            if (FinalList.Count >= loopStartInt && loopStartInt >=0)
            {
                FinalList[loopStartInt].CheckOut = CheckoutIndx;
                FinalList[loopStartInt].CheckOutModule = CheoutMoudule;
            }
        }
        return FinalList;
    }

    private List<Flow> GetFlowList(List<TemplateRouteDetail> templist)
    {
        List<Flow> fList = new List<Flow>();
        string moduleID = "";
        string scriptID = "";
         
        for (int i = 0; i < templist.Count; i++)
        {
       
            moduleID = templist[i].ModuleID;
            if (moduleID != SiteFlowType.DEFAULT_MODULE_END)
            {
                scriptID = GetScriptNamefromModuleID(moduleID);

                Flow ftemp = new Flow();
                ftemp.ScriptID = scriptID;
                ftemp.FlowStep = templist[i].RouteOrder;
                ftemp.FlowIsRequired = "OFF";
                ftemp.FlowStepDesc = templist[i].Title;
                moduleID = templist[i].NextModule;
                scriptID = GetScriptNamefromModuleID(moduleID);
                ftemp.FlowNScriptID = scriptID;
                ftemp.FlowNStep = templist[i].NextSetp;
                ftemp.FlowCheckoutStep = templist[i].CheckOut;
                moduleID = templist[i].CheckOutModule;
                scriptID = GetScriptNamefromModuleID(moduleID);
                ftemp.FlowCheckoutScriptID = scriptID;
                ftemp.FlowRootPath = "";
                ftemp.FlowLabel = templist[i].Title;
                ftemp.Status = SiteFlowType.DEFAULT_FLOW_ACTIVE;
                ftemp.Remark = "";
                ftemp.CreateBy = "User";

                fList.Add(ftemp);
            }
        }

        return fList;

    }

    private string GetScriptNamefromModuleID(string ModuleID)
    {
        string scriptID = "";

        if (!string.IsNullOrEmpty(ModuleID))
        {
            string sql = "select * from tb_site_module where module_id=@MDL";
            SqlParameter spar = new SqlParameter("MDL", SqlDbType.NVarChar);
            spar.Value = ModuleID;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar);

            DataTable dt = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];
            if (dt.Rows.Count > 0)
            {
                scriptID = dt.Rows[0]["script_Id"].ToString();
            }
        }
        return scriptID;
    }

    #endregion

}