﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Admin_AdminLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../Content/Default/bootstrap.css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.10.2.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <title></title>
    <style>
        @import "bourbon";

        body {
            background: #eee !important;
        }

        .wrapper {
            margin-top: 80px;
            margin-bottom: 80px;
        }

        .form-signin {
            max-width: 380px;
            padding: 15px 35px 45px;
            margin: 0 auto;
            background-color: #fff;
            border: 1px solid rgba(0,0,0,0.1);
        }



        .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
        }
    </style>
</head>
<body style="background :url(../images/BKimages/AdminBgroundImg.jpg) no-repeat !important;  ">
    <div class="wrapper">
        <form id="form1" runat="server" class="form-signin" autocomplete="off">
             <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
            <h3 class="form-signin-heading"></h3>
            <asp:TextBox runat="server" ID="txtUsername" CssClass="form-control" placeholder="User Name" autocomplete="off"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                ControlToValidate="txtUsername" ErrorMessage="required" CssClass="text-danger" Text="*"
                Display="Dynamic"></asp:RequiredFieldValidator><br />
            <asp:TextBox runat="server" ID="txtPassword" CssClass="form-control" placeholder="Password" TextMode="Password" autocomplete="off"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*"
                ControlToValidate="txtPassword" ErrorMessage="required" CssClass="text-danger"
                Display="Dynamic"></asp:RequiredFieldValidator><br />
             
             <div>
                 <asp:Label ID="lblpasswerror" runat="server" Text="" Visible="false" CssClass="tex-danger" Font-Bold="true" ForeColor="Red"></asp:Label>
                 <telerik:RadCaptcha ID="RadCaptcha1" runat="server" ErrorMessage="The code you entered is not valid."
                                ValidationGroup="Group">
                                <CaptchaImage ImageCssClass="imageClass" />
              </telerik:RadCaptcha>
            </div> 
           <asp:Button runat="server" Text="Log In" ID="btnSubmit" OnClick="btnSubmit_Onclick" class="btn btn-lg btn-block btn-success"  ValidationGroup="Group"/>
        </form>

    </div>
</body>
</html>
