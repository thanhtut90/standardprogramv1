﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Config.aspx.cs" Inherits="Admin_Event_Config" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Dashboard | Manage Show</title>
    <style type="text/css">
        .RadInput .riTextBox {
            width: auto !important;
        }

        .info-box {
            background-color: #f5f5f5;
        }

        .bg-flow {
            background-image: url("images/wave2.jpg");
        }

        .fa-arrow-circle-o-right {
            color: darkblue !important;
        }
    </style>
    <link rel="stylesheet" href="css/style.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="centercontent">

        <div id="contentwrapper" class="contentwrapper">
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <AjaxSettings>
                </AjaxSettings>
            </telerik:RadAjaxManager>

            <asp:Panel runat="server" ID="PanelCreate">
                <div class="row" style="padding-bottom:20px;">
                    <div class="col-lg-7 content-header">
                        <h1>Manage Event</h1>
                    </div>
                    <div class="col-lg-4 content-header">
                        <asp:LinkButton ID="btnAddNew" runat="server" CssClass="btn btn-success" OnClick="btnAddNew_Click">
                        <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Create Event
                        </asp:LinkButton>
                    </div>
                </div>

            </asp:Panel>
            <%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">--%>
            <asp:Panel runat="server" ID="PanelKeyList">
                <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                    EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                    OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="10" Skin="Silk">
                    <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true"></Selecting>
                    </ClientSettings>
                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="SHW_ID,SHW_Name" ShowHeader="true">
                        <Columns>

                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="SHW_Name" FilterControlAltText="Filter SHW_Name"
                                HeaderText="Event Name" SortExpression="SHW_Name" UniqueName="SHW_Name" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SHW_Desc" FilterControlAltText="Filter SHW_Name"
                                HeaderText="Descriptions" SortExpression="SHW_Desc" UniqueName="SHW_Desc" AutoPostBackOnFilter="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="SHW_ID" FilterControlAltText="Filter SHW_ID" Visible="false"
                                HeaderText="ID" SortExpression="SHW_ID" UniqueName="SHW_ID">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </asp:Panel>
             
            <asp:Panel runat="server" ID="PanelKeyDetail">
                <div class="row" style="line-height: 30px; text-align: center;">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-box">
                        <div class="f1-steps">
                            <div class="f1-progress">
                                <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 15.66%;"></div>
                            </div>
                            <div class="f1-step active">
                                <div class="f1-step-icon"><i class="fa fa-plus"></i></div>
                                <p style="text-align: center">Create Event</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-gear"></i></div>
                                <p style="text-align: center">Event Configuration</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                                <p style="text-align: center">Registration SetUp</p>
                            </div>
                            <div class="f1-step">
                                <div class="f1-step-icon"><i class="fa fa-check-circle-o"></i></div>
                                <p style="text-align: center">Finished</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row" >           
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Event Name</label>
                            <div class="col-sm-8">
                                <telerik:RadTextBox RenderMode="Lightweight" ID="txtName" runat="server" Skin="Bootstrap" CssClass="form-control"></telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" Display="Dynamic"
                                    ControlToValidate="txtName" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-8">
                                <telerik:RadTextBox RenderMode="Lightweight" ID="txtDescription" runat="server" TextMode="MultiLine" Rows="5" Columns="100" Skin="Bootstrap"></telerik:RadTextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-8">
                                <asp:Button ID="btnAdd" runat="server" Text="Create" OnClick="btnAdd_Click" CssClass="btn btn-danger" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn btn-default" CausesValidation="false" />
                                <asp:Button ID="btnUpdateRecord" runat="server" Text="Update Record" OnClick="btnUpdateRecord_Click" UseSubmitBehavior="true"></asp:Button>
                            </div>
                        </div>
                    </div>
                    <asp:Label runat="server" ID="lbls" ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="hfID" runat="server" Value="" />
                </div>
            </asp:Panel>

            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true" Visible="false">
                        <div class="content-header" style="padding-bottom:10px;padding-top:15px;">          
                                          <h3 style="font-weight:bold;color:#c63a07"><asp:Label runat="server" ID="lblShowTitle" Text="Show Title"></asp:Label></h3>
                            </div>
                  
                    <div class="containter">
                        <div id="headerInfo">
                      
                            <div class="row">
                                   
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h3>
                                                <asp:Label runat="server" ID="lblShowFlowTotal" Text="0"></asp:Label></h3>

                                            <p>Registration Route(s)</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-ios-pulse-strong"></i>
                                        </div>
                                        <asp:LinkButton runat="server" ID="btnAddNewFlow" OnClick="btnAddFlow_Click" CssClass="small-box-footer">
                                             Create New Registration <i class="fa fa-arrow-circle-right"></i> 
                                        </asp:LinkButton>
                                    </div>
                                </div>
                                <!-- ./col -->

                                <!-- ./col -->
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-red">
                                        <div class="inner">
                                            <h3>
                                                <asp:Label runat="server" ID="lblShowClosedDate" Text="12-Jul-2017"></asp:Label></h3>

                                            <p>Reg Close Date</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-ios-alarm-outline"></i>
                                        </div>
                                        <asp:LinkButton runat="server" ID="btnShowInfo2" OnClick="btnShowInfo_Click" CssClass="small-box-footer">
                                              Edit Event Config <i class="fa fa-arrow-circle-right"></i> 
                                        </asp:LinkButton>
                                    </div>
                                </div>
                                <!-- ./col -->
                            </div>
                        </div>

                    </div>
 
                <div id="divFlow" runat="server" visible="false">
                    <h3 class="pagetitle">Existing Registration(s) </h3>

                    <asp:Panel runat="server" ID="Panel2">
                        <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster1" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                            EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                            OnNeedDataSource="GKeyMaster1_NeedDataSource" OnItemCommand="GKeyMaster1_ItemCommand" PageSize="10" Skin="Silk" OnItemDataBound="GKeyMaster1_ItemDataBound">
                            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                            </ClientSettings>
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="Flw_ID" ShowHeader="true">
                                <PagerStyle PageSizeControlType="RadDropDownList" AlwaysVisible="true" />
                                <Columns>
                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Flw_ID" FilterControlAltText="Filter Flw_ID"
                                        HeaderText="Flow  ID" SortExpression="Flw_ID" UniqueName="Flw_ID" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" Visible="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="FlW_Name" FilterControlAltText="Filter FlW_Name"
                                        HeaderText="Name" SortExpression="FlW_Name" UniqueName="FlW_Name" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="FLW_Desc" FilterControlAltText="Filter FLW_Desc"
                                        HeaderText="Detail Description" SortExpression="FLW_Desc" UniqueName="FLW_Desc" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="StatusDesc" FilterControlAltText="Filter StatusDesc"
                                        HeaderText="Status" SortExpression="StatusDesc" UniqueName="StatusDesc" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridButtonColumn CommandArgument="id" Text="Edit"
                                        ConfirmDialogType="RadWindow" CommandName="EditFlow" ButtonType="LinkButton" UniqueName="EditFlow"
                                        HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>
                                    <telerik:GridButtonColumn CommandArgument="id" Text="Browse"
                                        ConfirmDialogType="RadWindow" CommandName="Browse" ButtonType="LinkButton" UniqueName="Browse"
                                        HeaderText="Browse" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </asp:Panel>
                    <br />
                    <br />

                    <asp:Panel runat="server" ID="PanelKeyDetail1">
                        <div class='page-header'>
                            <h2>Flow Steps</h2>
                            <div style="padding-top: 40px;"></div>
                            <div style="padding-left: 30%;">
                                <asp:LinkButton ID="btnCreateFlow" runat="server" OnClick="btnCreateFlow_Onclick" CssClass="btn btn-danger">
                                    <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Edit
                                </asp:LinkButton>

                                <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
                            </div>
                        </div>
                    </asp:Panel>
                </div>

            </telerik:RadAjaxPanel>

        </div>
    </div>
</asp:Content>

