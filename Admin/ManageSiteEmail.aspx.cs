﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;

public partial class Admin_ManageSiteEmail : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    SqlConnection con;
    SqlCommand cmd;
    SqlDataReader dr;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Master.setopensettings();

            PanelKeyDetial.Visible = false;
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_site_Email table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        string conString = fn.ConnString;
        string query = "Select * From tb_site_Email Order By EMAIL_TblKey";
        using (SqlConnection con = new SqlConnection(conString))
        {
            using (SqlCommand cmd = new SqlCommand(query))
            {
                SqlDataAdapter dt = new SqlDataAdapter();
                try
                {
                    cmd.Connection = con;
                    con.Open();
                    dt.SelectCommand = cmd;

                    DataTable dTable = new DataTable();
                    dt.Fill(dTable);

                    GKeyMaster.DataSource = dTable;
                }
                catch (Exception)
                {
                    lbls.Text = "record not found";
                }
            }
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string EMAIL_TblKey = "";
        foreach (GridDataItem item in GKeyMaster.SelectedItems)
        {
            EMAIL_TblKey = item["EMAIL_TblKey"].Text;
        }
        btnupdate.Visible = true;
        btnadd.Visible = false;
        PanelKeyDetial.Visible = true;
        txtemailid.Enabled = false;

        if (EMAIL_TblKey != "" && EMAIL_TblKey != null)
        {
            loadlist();
            loadvalue(EMAIL_TblKey);
        }
    }
    #endregion

    #region loadlist (Binding related template status from tb_site_reference table into ddlstatus & Binding all template data from tb_site_Template into ddltempName)
    private void loadlist()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select list_value From tb_site_reference Where list_name = 'status'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddlstatus.DataSource = cmd.ExecuteReader();
                ddlstatus.DataTextField = "list_value";
                ddlstatus.DataValueField = "list_value";
                ddlstatus.DataBind();
                con.Close();
            }
            using (SqlCommand cmd = new SqlCommand("Select temp_id, temp_name From tb_site_Template"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddltempName.DataSource = cmd.ExecuteReader();
                ddltempName.DataTextField = "temp_name";
                ddltempName.DataValueField = "temp_id";
                ddltempName.DataBind();
                con.Close();
            }
        }
    }
    #endregion

    #region loadvalue (Load RadGrid selected row data from tb_site_Email table to bind the respective controls while edit)
    private void loadvalue(string EMAIL_TblKey)
    {
        string connStr = fn.ConnString;
        string query = "Select * From tb_site_Email Where EMAIL_TblKey='" + EMAIL_TblKey + "'";
        using (SqlConnection connection = new SqlConnection(connStr))
        {
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                connection.Open();

                using (SqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        hfTblKey.Value = EMAIL_TblKey;

                        txtemailid.Text = dataReader["EMAIL_ID"].ToString();
                        txtemailid.Enabled = false;
                        txtdesc.Text = dataReader["Description"].ToString();

                        string tempid = dataReader["Template_ID"].ToString();
                        try
                        {
                            if (!String.IsNullOrEmpty(tempid))
                            {
                                ListItem listItem = ddltempName.Items.FindByValue(tempid);
                                if (listItem != null)
                                {
                                    ddltempName.ClearSelection();
                                    listItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        calevsd.SelectedDate = !string.IsNullOrEmpty(dataReader["Email_Vaild_StartDate"].ToString()) ? Convert.ToDateTime(dataReader["Email_Vaild_StartDate"].ToString()) : DateTime.Now;
                        calevsd.FocusedDate = calevsd.SelectedDate;
                        caleved.SelectedDate = !string.IsNullOrEmpty(dataReader["Email_Vaild_EndDate"].ToString()) ? Convert.ToDateTime(dataReader["Email_Vaild_EndDate"].ToString()) : DateTime.Now;
                        caleved.FocusedDate = caleved.SelectedDate;
                        ddlstatus.SelectedValue = dataReader["Status"].ToString();
                    }

                    dataReader.Close();
                    dataReader.Dispose();
                }

                connection.Close();
            }
        }
    }
    #endregion

    #region btnaddnew_Click (Load when click Add New Email button)
    protected void btnaddnew_Click(object sender, EventArgs e)
    {
        PanelKeyDetial.Visible = true;
        loadlist();
        ResetControls();
        btnupdate.Visible = false;
        btnadd.Visible = true;
    }
    #endregion

    #region btnadd_Click (Insert the respective data into tb_site_Email table)
    protected void btnadd_Click(object sender, EventArgs e)
    {
        lbls.Text = "";
        string emailid1 = txtemailid.Text;
        string desc = txtdesc.Text;
        string tempid = ddltempName.SelectedValue;
        string status = ddlstatus.SelectedValue;
        DateTime evsd1 = calevsd.SelectedDate;
        DateTime eved1 = caleved.SelectedDate;
        string connStr = fn.ConnString;
        try
        {
            int emailid = int.Parse(emailid1);
            try
            {
                DataTable dtemail = fn.GetDatasetByCommand("Select * From tb_site_Email Where EMAIL_ID='" + emailid + "'", "ds").Tables[0];
                if (dtemail.Rows.Count > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Email ID already exist.');", true);
                    return;
                }
                else
                {
                    con = new SqlConnection(connStr);
                    con.Open();
                    cmd = new SqlCommand("Insert Into tb_site_Email(EMAIL_ID, Description, Template_ID, Email_Vaild_StartDate, Email_Vaild_EndDate, Status)" +
                                         " Values (@eid,@desc, @tid,@evs,@eve,@status)");

                    cmd.Parameters.AddWithValue("@eid", emailid);
                    cmd.Parameters.AddWithValue("@desc", desc);
                    cmd.Parameters.AddWithValue("@tid", tempid);
                    cmd.Parameters.AddWithValue("@evs", evsd1);
                    cmd.Parameters.AddWithValue("@eve", eved1);
                    cmd.Parameters.AddWithValue("@status", status);

                    cmd.Connection = con;
                    int rowUpdated = cmd.ExecuteNonQuery();
                    if (rowUpdated == 1)
                    {
                        GKeyMaster.Rebind();
                        PanelKeyDetial.Visible = false;
                        ResetControls();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        GKeyMaster.Rebind();
                        PanelKeyDetial.Visible = false;
                        ResetControls();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Saving error, try again.');", true);
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                lbls.Text += ex.ToString();
            }
        }
        catch (Exception ex)
        {
            lbls.Text = ex.ToString();
        }
    }
    #endregion

    #region btnupdate_Click (Update the respective data into "tb_site_Email" table)
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lbls.Text = "";
            string emailtblkey = hfTblKey.Value;
            string emailid1 = txtemailid.Text;
            string desc = txtdesc.Text;
            string tempid = ddltempName.SelectedValue;
            DateTime evsd1 = calevsd.SelectedDate;
            DateTime eved1 = caleved.SelectedDate;
            string status = ddlstatus.SelectedValue;
            string connStr = fn.ConnString;

            try
            {
                int emailid = int.Parse(emailid1);
                try
                {
                    try
                    {
                        con = new SqlConnection(connStr);
                        con.Open();
                        cmd = new SqlCommand("Update tb_site_Email Set EMAIL_ID = @eid, Description = @desc, Template_ID = @tid, " +
                                             "Email_Vaild_StartDate =@evs, Email_Vaild_EndDate = @eve, Status = @status Where EMAIL_TblKey = @etk");

                        cmd.Parameters.AddWithValue("@etk", emailtblkey);
                        cmd.Parameters.AddWithValue("@eid", emailid);
                        cmd.Parameters.AddWithValue("@desc", desc);
                        cmd.Parameters.AddWithValue("@tid", tempid);
                        cmd.Parameters.AddWithValue("@evs", evsd1);
                        cmd.Parameters.AddWithValue("@eve", eved1);
                        cmd.Parameters.AddWithValue("@status", status);

                        cmd.Connection = con;
                        int rowUpdated = cmd.ExecuteNonQuery();
                        if (rowUpdated == 1)
                        {
                            GKeyMaster.Rebind();
                            PanelKeyDetial.Visible = false;
                            ResetControls();
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                        }
                        else
                        {
                            GKeyMaster.Rebind();
                            PanelKeyDetial.Visible = false;
                            ResetControls();
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updating error, try again.');", true);
                        }
                        con.Close();

                    }
                    catch (Exception ex)
                    {
                        lbls.Text += ex.ToString();
                    }

                }
                catch (Exception ex)
                {
                    lbls.Text += ex.ToString();
                }
            }
            catch (Exception ex)
            {
                lbls.Text = ex.ToString();
            }
        }
    }
    #endregion

    #region gettempname (Get Template Name from tb_site_Template table according to temp_id and call from design page)
    public string gettempname(string id)
    {
        return fn.GetDataByCommand("Select temp_name From tb_site_Template Where temp_id ='" + id + "'", "temp_name");
    }
    #endregion

    #region ResetControls (Clear data from controls)
    private void ResetControls()
    {
        hfTblKey.Value = "";

        txtemailid.Text = "";
        txtemailid.Enabled = true;
        txtdesc.Text = "";
        ddltempName.SelectedIndex = 0;

        calevsd.SelectedDate = DateTime.Now;
        calevsd.FocusedDate = calevsd.SelectedDate;
        caleved.SelectedDate = DateTime.Now;
        caleved.FocusedDate = caleved.SelectedDate;

        lbls.Text = "";
    }
    #endregion
}