﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Corpit.Utilities;
using System.IO;
using System.Globalization;
using Corpit.Site.Utilities;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.BackendMaster;

public partial class Admin_Event_Settings_EventSettings : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    static string _eventname = "eventname";
    static string _regclosing = "RegClosedDate";
    static string _regclosingmessage = "RegClosedMessage";
    static string _earlybirdclose = "EarlyBirdEndDate";
    static string _hastimer = "hastimer";
    static string _hastimertype = "hastimer_type";
    static string _hasterm = "hasterm";
    static string _termstemplate = "termstemplate";
    static string _prefix = "prefix";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

                if (!checkCreated(showid))
                {
                    Response.Redirect("Event_Config");
                }

                bindShowName(showid);
                binddata(showid);
            }

            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region bindShowName
    private void bindShowName(string showid)
    {
        try
        {
            ShowController shwCtr = new ShowController(fn);
            DataTable dt = shwCtr.getShowByID(showid);
            if (dt.Rows.Count > 0)
            {
                string showName = dt.Rows[0]["SHW_Name"].ToString();
                txtEventName.Text = showName == "0" ? string.Empty : showName;
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region binddata (bind data from tb_site_settings to related controls)
    protected void binddata(string showid)
    {
        string regclosing = string.Empty;
        string regclosingmsg = string.Empty;
        string earlybirdclosing = string.Empty;
        string hasTimer = string.Empty;
        string timerType = string.Empty;
        string masConfSelectionType = string.Empty;
        int hasQ = 0;
        int hasterm = 0;
        string termtemplate = string.Empty;
        string query = string.Empty;

        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        query = "Select * From tb_site_settings Where ShowID=@SHWID";

        DataTable dt = new DataTable();
        dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

        for (int x = 0; x < dt.Rows.Count; x++)
        {
            string settingname = dt.Rows[x]["settings_name"].ToString();

            if (settingname == _regclosing)
            {
                regclosing = !string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                txtRegClosingDate.Text = regclosing;
            }
            if (settingname == _regclosingmessage)
            {
                regclosingmsg = dt.Rows[x]["settings_value"].ToString();
                txtRegcloseMsg.Text = Server.HtmlDecode(regclosingmsg);
            }
            if (settingname == _earlybirdclose)
            {
                earlybirdclosing = !string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : DateTime.Now.ToString("dd/MM/yyyy hh:mm");
                txtEarlyClosingDate.Text = earlybirdclosing;
            }
            if (settingname == _hastimer)
            {
                hasTimer = dt.Rows[x]["settings_value"].ToString();
                if (!String.IsNullOrEmpty(hasTimer))
                {
                    ListItem listItem = rbTimer.Items.FindByValue(hasTimer);
                    if (listItem != null)
                    {
                        rbTimer.ClearSelection();
                        listItem.Selected = true;

                        rbTimer_SelectedIndexChanged(this, null);
                    }
                }
            }
            if (settingname == _hastimertype)
            {
                timerType = dt.Rows[x]["settings_value"].ToString();
                if (!String.IsNullOrEmpty(timerType) && rbTimer.SelectedValue == Number.One)
                {
                    ListItem listItem = ddlTimerType.Items.FindByValue(timerType);
                    if (listItem != null)
                    {
                        ddlTimerType.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            if (settingname == _hasterm)
            {
                hasterm = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : Number.Zero);
                if (hasterm == 1)
                {
                    rbTerms1.Checked = false;
                    rbTerms2.Checked = true;
                }
                else
                {
                    rbTerms1.Checked = true;
                    rbTerms2.Checked = false;
                }
            }
            if (settingname == _termstemplate)
            {
                termtemplate = !string.IsNullOrEmpty(dt.Rows[x]["settings_value"].ToString()) ? dt.Rows[x]["settings_value"].ToString() : "";
                if(!string.IsNullOrEmpty(termtemplate))
                {
                    if(rbTerms2.Checked == true)
                    {
                        txtTerms.Visible = true;
                        txtTerms.Text = Server.HtmlDecode(termtemplate);
                    }
                }
            }
            if (settingname == _prefix)
            {
                string prefix = dt.Rows[x]["settings_value"].ToString();
                txtPrefix.Text = Server.HtmlDecode(prefix);
            }
        }
    }
    #endregion

    #region rbTimer_SelectedIndexChanged (set TimerType portion (trTimerType) visibility true or false according to the rbTimer selection)
    protected void rbTimer_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbTimer.SelectedItem.Value == Number.One)
        {
            trTimerType.Visible = true;
            vcTimerType.Enabled = true;
        }
        else
        {
            trTimerType.Visible = false;
            vcTimerType.Enabled = false;
        }
    }
    #endregion

    #region checkCreated (check the record count of tmp_tbSiteSettings table is equal to the record counts of tb_site_settings table)
    private bool checkCreated(string showid)
    {
        bool isCreated = false;
        DataTable dt = fn.GetDatasetByCommand("Select * From tmp_tbSiteSettings", "ds").Tables[0];

        string query = "Select * From tb_site_settings Where ShowID=@SHWID And settings_name In (Select settings_name From tmp_tbSiteSettings)";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dtSettings = fn.GetDatasetByCommand(query, "dsSettings", pList).Tables[0];
        if (dt.Rows.Count == dtSettings.Rows.Count)
        {
            isCreated = true;
        }

        return isCreated;
    }
    #endregion

    #region SaveForm (Save data into related tb_site_settings table and ref_PaymentMethod table)
    protected void SaveForm(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            try
            {
                string sql = string.Empty;
                string eventname = string.Empty;
                string regclosing = string.Empty;
                string regclosingmsg = string.Empty;
                string earlybirdclosing = string.Empty;
                string hasTimer = string.Empty;
                string timerType = string.Empty;
                int hasterm = 0;
                string termtemplate = string.Empty;

                eventname = cFun.solveSQL(txtEventName.Text.ToString());
                regclosing = txtRegClosingDate.Text.Trim();//Convert.ToDateTime(txtRegClosingDate.Text.Trim(), provider);
                regclosingmsg = Server.HtmlEncode(txtRegcloseMsg.Text);
                earlybirdclosing = txtEarlyClosingDate.Text.Trim();
                hasTimer = rbTimer.SelectedItem.Value;
                timerType = ddlTimerType.SelectedItem.Value;
              string  prefix = Server.HtmlEncode(txtPrefix.Text.ToString());
                if (rbTerms1.Checked == true)
                {
                    hasterm = 0;
                }
                else if (rbTerms2.Checked == true)
                {
                    hasterm = 1;
                }

                termtemplate = Server.HtmlEncode(txtTerms.Text);

                #region Inserting data into tb_Show
                ShowController shwCtr = new ShowController(fn);
                ShowObj sObj = new ShowObj();
                sObj.showid = showid;
                sObj.showname = eventname;
                bool rowInserted = shwCtr.UpdateShowName(sObj);
                #endregion

                sql = "Update tb_site_settings Set settings_value='" + eventname + "' Where settings_name='" + _eventname + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + regclosing + "' Where settings_name='" + _regclosing + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + regclosingmsg + "' Where settings_name='" + _regclosingmessage + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + earlybirdclosing + "' Where settings_name='" + _earlybirdclose + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + hasTimer + "' Where settings_name='" + _hastimer + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + timerType + "' Where settings_name='" + _hastimertype + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + hasterm + "' Where settings_name='" + _hasterm + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + termtemplate + "' Where settings_name='" + _termstemplate + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + prefix + "' Where settings_name='" + _prefix + "' And ShowID='" + showid + "';";

                fn.ExecuteSQL(sql);

                pnlmessagebar.Visible = true;
                lblemsg.Text = "Updated Successfully";
                showid = cFun.EncryptValue(showid);
                Response.Redirect("Event_Settings_MasterPage?SHW=" + showid);
            }
            catch (Exception ex) {
                pnlmessagebar.Visible = true;
                lblemsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region Back
    protected void Back(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            Response.Redirect("Event_Settings_MasterPage?SHW=" + showid);
        }
        else
        {
            Response.Redirect("Login");
        }
    }
    #endregion

    #region rbTerms_CheckedChanged
    protected void rbTerms_CheckedChanged(object sender, EventArgs e)
    {
        if (rbTerms2.Checked == true)
        {
            txtTerms.Visible = true;
        }
        else
        {
            txtTerms.Visible = false;
        }
    }
    #endregion

    #region btnSavePreview_Click
    protected void btnSavePreview_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            try
            {
                string sql = string.Empty;
                string eventname = string.Empty;
                string regclosing = string.Empty;
                string regclosingmsg = string.Empty;
                string earlybirdclosing = string.Empty;
                string hasTimer = string.Empty;
                string timerType = string.Empty;
                int hasterm = 0;
                string termtemplate = string.Empty;

                eventname = cFun.solveSQL(txtEventName.Text.ToString());
                regclosing = txtRegClosingDate.Text.Trim();//Convert.ToDateTime(txtRegClosingDate.Text.Trim(), provider);
                regclosingmsg = Server.HtmlEncode(txtRegcloseMsg.Text);
                earlybirdclosing = txtEarlyClosingDate.Text.Trim();
                hasTimer = rbTimer.SelectedItem.Value;
                timerType = ddlTimerType.SelectedItem.Value;

                if (rbTerms1.Checked == true)
                {
                    hasterm = 0;
                }
                else if (rbTerms2.Checked == true)
                {
                    hasterm = 1;
                }

                termtemplate = Server.HtmlEncode(txtTerms.Text);

                #region Inserting data into tb_Show
                ShowController shwCtr = new ShowController(fn);
                ShowObj sObj = new ShowObj();
                sObj.showid = showid;
                sObj.showname = eventname;
                bool rowInserted = shwCtr.UpdateShowName(sObj);
                #endregion

                sql = "Update tb_site_settings Set settings_value='" + eventname + "' Where settings_name='" + _eventname + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + regclosing + "' Where settings_name='" + _regclosing + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + regclosingmsg + "' Where settings_name='" + _regclosingmessage + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + earlybirdclosing + "' Where settings_name='" + _earlybirdclose + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + hasTimer + "' Where settings_name='" + _hastimer + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + timerType + "' Where settings_name='" + _hastimertype + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + hasterm + "' Where settings_name='" + _hasterm + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + termtemplate + "' Where settings_name='" + _termstemplate + "' And ShowID='" + showid + "';";

                fn.ExecuteSQL(sql);

                pnlmessagebar.Visible = true;
                lblemsg.Text = "Updated Successfully";

                FlowControler flwObj = new FlowControler(fn);
                string page = "../WebsiteSettings.aspx";
                string route = flwObj.MakeFullURL(page, Number.Zero, showid);
                route = route + "&t=" + cFun.EncryptValue(BackendStaticValueClass.isEventSettings);

                System.Drawing.Rectangle resolution =
                System.Windows.Forms.Screen.PrimaryScreen.Bounds;
                int width = resolution.Width;
                int height = resolution.Height;

                if (width <= 1014)
                {
                    width = width - 100;
                }
                else
                {
                    width = 1000;
                }

                RadWindow newWindow = new RadWindow();
                newWindow.NavigateUrl = route;
                newWindow.Height = 800;
                newWindow.Width = width;
                newWindow.VisibleOnPageLoad = true;
                newWindow.KeepInScreenBounds = true;
                newWindow.VisibleOnPageLoad = true;
                newWindow.Visible = true;
                newWindow.VisibleStatusbar = false;
                RadWindowManager1.Windows.Add(newWindow);
            }
            catch (Exception ex)
            {
                pnlmessagebar.Visible = true;
                lblemsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

}