﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data;
using Corpit.Utilities;
using Corpit.BackendMaster;
using System.Reflection;
using Corpit.Site.Utilities;
using System.Data.SqlClient;
using System.Globalization;
using Corpit.Registration;

public partial class DashboardFJ : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFuz = new CommonFuns();
    CultureInfo cul = new CultureInfo("en-GB");
    QuesFunctionality qfn = new QuesFunctionality();
    private static string[] checkingMDAShowName = new string[] { "MFA 2018", "MMA 2018", "OSHA 2018" };
    private static string FJIndivTradeFlowID = "F376";
    private static string FJIndivPublicFlowID = "F378";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string[] showList = new string[2];

            try { 
                if (Session["roleid"].ToString() == "1")
                {
                    showList = getAllShowList();
                }
                else
                {
                    showList = getShowList();
                }
                int res = CheckGroupReg(Session["userid"].ToString());
                if (res == 0)
                {
                    btnChartGroupRegDate.Visible = false;
                    ChartGroupRegDate.Visible = false;
                    btnChartGroupRegion.Visible = false;
                    ChartGroupRegion.Visible = false;
                    btnChartGroupCountry.Visible = false;
                    ChartGroupCountry.Visible = false;
                    ddlGroupCountry.Visible = false;
                    ChartGroupCategory.Visible = false;
                }
            }
            catch(Exception ex)
            {
                Response.Redirect("Login.aspx");
            }

            if (!string.IsNullOrEmpty(showList[0]))
            {
                if (showList[0].Contains(","))
                {
                    Panel1.Visible = true;
                    string[] showLists = showList[0].Split(',');
                    loadShowList(showList);
                    lbl_show.Text = showLists[0];
                    bindCountryList(showLists[0]);
                    loadChart(showLists[0]);
                }
                else
                {
                    Panel1.Visible = false;
                    lbl_show.Text = showList[0];
                    bindCountryList(showList[0]);
                    loadChart(showList[0]);
                }
            }
            else
            {
                Response.Redirect("Event_Config.aspx");
            }
        }
    }

    protected void loadShowList(string[] showList)
    {
        ddl_showList.Items.Clear();
        string[] showIDList = showList[0].Split(',');
        string[] showNameList = showList[1].Split(',');
        

        for (int i=0; i< showIDList.Length; i++)
        {

            ListItem items = new ListItem(showNameList[i], showIDList[i]);

            ddl_showList.Items.Insert(i,items);
        }
    }

    protected void loadChart(string showid)
    {
        Tuple<string, string> getType = checkFlowTypeGI(showid);
        if (getType != null)
        {
            if (!string.IsNullOrEmpty(getType.Item1) && !string.IsNullOrEmpty(getType.Item2))
            {
                ChartCategory.DataSource = ByGroupIndiv(showid);
                ChartCategory.DataBind();
            }else
            {
                ChartCategory.DataSource = ByGroupIndiv(showid);
                ChartCategory.DataBind();
            }

            #region MDA New Visitor or Repeat Visitor (Comment)
            //ShowControler shwCtr = new ShowControler(fn);
            //Show shw = shwCtr.GetShow(showid);
            //if (checkingMDAShowName.Contains(shw.SHW_Name))
            //{
            //    divMDA.Visible = true;
            //    if (!string.IsNullOrEmpty(getType.Item1) && !string.IsNullOrEmpty(getType.Item2))
            //    {
            //        ChartMDA.DataSource = ByGroupIndivMDAChart(showid);
            //        ChartMDA.DataBind();
            //    }
            //    else
            //    {
            //        ChartMDA.DataSource = ByGroupIndivMDAChart(showid);
            //        ChartMDA.DataBind();
            //    }
            //}
            #endregion

            #region By Date
            if (!string.IsNullOrEmpty(getType.Item1) || !string.IsNullOrEmpty(getType.Item2))
            {
                bindChartByDate(showid);
            }
            #endregion
            #region By Country
            if (!string.IsNullOrEmpty(getType.Item1))
            {
                DataTable dtGroupCountry = GrpByCountry(showid);
                ChartGroupCountry.DataSource = dtGroupCountry;
                ChartGroupCountry.DataBind();
                int rcHeight = dtGroupCountry.Rows.Count * 40;
                ChartGroupCountry.Height = rcHeight > 300 ? rcHeight : 300;
            }
            if (!string.IsNullOrEmpty(getType.Item2))
            {
                #region Individual
                DataTable dtIndivCountry = IndivByCountry(showid, false);
                ChartIndivCountry.DataSource = dtIndivCountry;
                ChartIndivCountry.DataBind();
                int rcHeight = dtIndivCountry.Rows.Count * 40;
                ChartIndivCountry.Height = rcHeight > 300 ? rcHeight : 300;
                #endregion

                #region Public
                DataTable dtIndivPublicCountry = IndivByCountry(showid, true);
                ChartIndivPublicCountry.DataSource = dtIndivPublicCountry;
                ChartIndivPublicCountry.DataBind();
                int rcHeightPublic = dtIndivPublicCountry.Rows.Count * 40;
                ChartIndivPublicCountry.Height = rcHeightPublic > 300 ? rcHeightPublic : 300;
                #endregion
            }
            #endregion
            #region By Region
            if (!string.IsNullOrEmpty(getType.Item1))
            {
                DataTable dtGroupRegion = GrpByRegion(showid);
                ChartGroupRegion.DataSource = dtGroupRegion;
                ChartGroupRegion.DataBind();
                int rcHeight = dtGroupRegion.Rows.Count * 40;
                ChartGroupRegion.Height = rcHeight > 300 ? rcHeight : 300;
            }
            if (!string.IsNullOrEmpty(getType.Item2))
            {
                #region Individual
                DataTable dtIndivRegion = IndivByRegion(showid, false);
                ChartIndivRegion.DataSource = dtIndivRegion;
                ChartIndivRegion.DataBind();
                int rcHeight = dtIndivRegion.Rows.Count * 40;
                ChartIndivRegion.Height = rcHeight > 300 ? rcHeight : 300;
                #endregion

                #region Public
                DataTable dtIndivPublicRegion = IndivByRegion(showid, true);
                ChartIndivPublicRegion.DataSource = dtIndivPublicRegion;
                ChartIndivPublicRegion.DataBind();
                int rcHeightPublic = dtIndivPublicRegion.Rows.Count * 40;
                ChartIndivPublicRegion.Height = rcHeightPublic > 300 ? rcHeightPublic : 300;
                #endregion
            }
            #endregion

            if (!string.IsNullOrEmpty(getType.Item1) && !string.IsNullOrEmpty(getType.Item2))
            {
                string country = ddlIndividualCountry.SelectedValue;
                ChartIndividualCategory.DataSource = GrpByIndivCategory(showid, country);
                ChartIndividualCategory.DataBind();
            }

            if (!string.IsNullOrEmpty(getType.Item1) && !string.IsNullOrEmpty(getType.Item2))
            {
                string country = ddlGroupCountry.SelectedValue;
                ChartGroupCategory.DataSource = GrpByGrpCategory(showid, country);
                ChartGroupCategory.DataBind();
            }
        }
    }

    protected string[] getAllShowList()
    {
        string query = "Select * from tb_Show Order By SHW_ID";
        string showList = string.Empty;
        string showName = string.Empty;

        DataTable dt = fn.GetDatasetByCommand(query, "dsShow").Tables[0];
        for (int i = 0; i <dt.Rows.Count; i++)
        {
            if (showList == string.Empty)
            {
                showList = dt.Rows[i]["SHW_ID"].ToString();
                showName = dt.Rows[i]["SHW_Name"].ToString();
            }
            else
            {
                showList += "," + dt.Rows[i]["SHW_ID"].ToString();
                showName += "," + dt.Rows[i]["SHW_Name"].ToString();
            }
        }
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        return list;
    }

    protected string[] getShowList()
    {
        string showList = string.Empty;
        string showName = string.Empty;
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = Session["userid"].ToString();
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                query = "Select * from tb_Show WHERE SHW_ID=@showID";
                spar = new SqlParameter("showID", SqlDbType.NVarChar);

                string[] showLists = dt.Rows[0]["us_showid"].ToString().Split(',');
                for (int i = 0; i<showLists.Length; i++)
                {
                    pList.Clear();
                    spar.Value = showLists[i];
                    pList.Add(spar);

                    DataTable dtShow = fn.GetDatasetByCommand(query, "dsShow",pList).Tables[0];

                    if (showName == string.Empty)
                    {
                        showName = dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                    else
                    {
                        showName += "," + dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                }
                list[0] = dt.Rows[0]["us_showid"].ToString();
                list[1] = showName;
                return list;
            }
            return list;
        }
        catch (Exception ex)
        {
            return list;
        }
    }

    protected void bindCountryList(string showid)
    {
        MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        msregIndiv.ShowID = showid;

        ddlIndividualCountry.Items.Clear();
        ddlIndividualCountry.DataSource = msregIndiv.getRegChartIndivByCountry();
        ddlIndividualCountry.DataTextField = "countryname";
        ddlIndividualCountry.DataValueField = "countryname";
        ddlIndividualCountry.DataBind();

        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;

        ddlGroupCountry.Items.Clear();
        ddlGroupCountry.DataSource = msregGroup.getRegChartGroupByCountry();
        ddlGroupCountry.DataTextField = "countryname";
        ddlGroupCountry.DataValueField = "countryname";
        ddlGroupCountry.DataBind();
    }

    #region ByGroupIndiv
    private DataTable ByGroupIndiv(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Total");
        dt.Columns.Add("TotalPercentage");
        dt.Columns.Add("Desc");
        //MasterRegGroup msregGroup = new MasterRegGroup(fn);
        //msregGroup.ShowID = showid;
        //DataTable dtmas = msregGroup.getRegByGroupIndivList();
        //int masCount = dtmas.Rows.Count;

        DataTable Cat = ByGroupIndivFlow(showid);//msregGroup.getRegChartByGroupIndiv();
        Double catCount;
        //Double TotalCount = 0;// masCount;
        //if (Cat.Rows.Count > 0)
        //{
        //    TotalCount = cFuz.ParseInt(Cat.Compute("SUM(SubCount)", string.Empty).ToString());
        //}
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            //Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Total"] = catCount.ToString();
            drow["TotalPercentage"] = Cat.Rows[i]["PercentageValue"].ToString();
            drow["Desc"] = Cat.Rows[i]["Type"].ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Total"] = "0";
            drow["TotalPercentage"] = "0";
            drow["Desc"] = "No Record";
            dt.Rows.Add(drow);
        }

        return dt;
    }
    private DataTable ByGroupIndivFlow(string showid)
    {
        DataTable dt = new DataTable();
        try
        {
            //string sql = string.Format("Select * From RegChartByGroupIndivFlow Where ShowID='{0}'", showid);//*View(Old)
            string sql = string.Format("Select * From GetRegChartByGroupIndivFlow('{0}')", showid);//*Table-valued functions[29-8-2018]
            dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    #endregion

    private DataTable GrpByIndivCategory(string showid, string country)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Total");
        dt.Columns.Add("Desc");

        MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        msregIndiv.ShowID = showid;
        //DataTable dtmas = msregIndiv.getDataAllRegList(country);
        //int masCount = dtmas.Rows.Count;

        DataTable Cat = msregIndiv.getRegChartIndivByCategory(country);
        Double catCount;
        Double TotalCount = 0;// masCount;
        if (Cat.Rows.Count > 0)
        {
            TotalCount = cFuz.ParseInt(Cat.Compute("SUM(SubCount)", string.Empty).ToString());
        }
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["reg_CategoryName"].ToString();
            drow["Total"] = caldbscan.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["Total"] = "0";
        }

        return dt;
    }

    private DataTable GrpByGrpCategory(string showid, string country)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Total");
        dt.Columns.Add("Desc");

        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        DataTable dtmas = msregGroup.getDataAllRegGroupList(country);
        int masCount =  dtmas.Rows.Count;

        DataTable Cat = msregGroup.getRegChartGroupByCategory(country);
        Double catCount;
        Double TotalCount = masCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["reg_CategoryName"].ToString();
            drow["Total"] = caldbscan.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["Total"] = "0";
        }

        return dt;
    }

    #region byDate
    #region GrpByGroupRegDate
    private DataTable GrpByGroupRegDate(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("TotalPercentage");
        dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Desc");
        dt.Columns.Add("RegDate", typeof(DateTime));

        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        DataTable dtmas = msregGroup.getDataAllRegGroupMemberList();
        int masCount = dtmas.Rows.Count;

        DataTable Cat = msregGroup.getRegChartGroupByRegDate();

        Double catCount;
        Double TotalCount = masCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["dateCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["RegDate"] = Convert.ToDateTime(Cat.Rows[i]["reg_datecreated"].ToString(), cul);
            drow["Desc"] = Convert.ToDateTime(Cat.Rows[i]["reg_datecreated"].ToString()).ToString("dd/MM/yyyy");
            drow["TotalPercentage"] = caldbscan.ToString();
            drow["TotalNumber"] = catCount.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["TotalPercentage"] = "0";
            drow["TotalNumber"] = "0";
        }

        return dt;
    }
    #endregion
    #region GrpByIndivRegDate
    private DataTable GrpByIndivRegDate(string showid, bool isPublic)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("TotalPercentage");
        dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Desc");
        dt.Columns.Add("RegDate", typeof(DateTime));

        //MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //msregIndiv.ShowID = showid;
        ////DataTable dtmas = msregIndiv.getDataAllRegList();
        ////DataView dv = new DataView();
        ////dv = dtmas.DefaultView;
        ////dv.RowFilter = "reg_status=1";
        ////dtmas = dv.ToTable();
        ////int masCount = dtmas.Rows.Count;
        DataTable Cat = ByIndivRegDateFlow(showid, isPublic == false ? FJIndivTradeFlowID : FJIndivPublicFlowID); //msregIndiv.getRegChartIndivByRegDate();
        Double catCount;
        Double TotalCount = 0;// masCount;
        if (Cat.Rows.Count > 0)
        {
            TotalCount = cFuz.ParseInt(Cat.Compute("SUM(dateCount)", string.Empty).ToString());
        }
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["dateCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["RegDate"] = Convert.ToDateTime(Cat.Rows[i]["reg_datecreated"].ToString(), cul);
            drow["Desc"] = Convert.ToDateTime(Cat.Rows[i]["reg_datecreated"].ToString()).ToString("dd/MM/yyyy");
            drow["TotalPercentage"] = caldbscan.ToString();
            drow["TotalNumber"] = catCount.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["TotalPercentage"] = "0";
            drow["TotalNumber"] = "0";
        }

        return dt;
    }
    private DataTable ByIndivRegDateFlow(string showid, string flowid)
    {
        DataTable dt = new DataTable();
        try
        {
            string sql = string.Format("Select * From RegChartIndivByDateFlow Where ShowID='{0}' And reg_urlFlowID='{1}'", showid, flowid);
            dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    #endregion
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string showid = string.Empty;
            if (Session["roleid"].ToString() != "1")
            {
                showid = lbl_show.Text.Trim();
            }
            else
            {
                showid = ddl_showList.SelectedValue;
            }

            loadChart(showid);
        }
        catch (Exception ex)
        { }
    }
    private void bindChartByDate(string showid)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()) && !string.IsNullOrEmpty(txtToDate.Text.Trim()))
            {
                string sdate = txtFromDate.Text.Trim();
                string edate = txtToDate.Text.Trim();
                string[] startdate1 = sdate.Split('/');
                string startdate = startdate1[1] + "/" + startdate1[0] + "/" + startdate1[2];
                DateTime dtSDate = Convert.ToDateTime(startdate, cul);
                string ssdate = dtSDate.ToString("MM/dd/yyyy HH:mm:ss");
                string[] enddate1 = edate.Split('/');
                string enddate = enddate1[1] + "/" + enddate1[0] + "/" + enddate1[2];
                DateTime dtEDate = Convert.ToDateTime(enddate, cul);
                string eedate = dtEDate.ToString("MM/dd/yyyy HH:mm:ss");
                #region Group
                DataTable dtGroupDate = GrpByGroupRegDate(showid);
                DataView dv = new DataView();
                dv = dtGroupDate.DefaultView;
                dv.RowFilter = "RegDate>=#" + ssdate + "# And RegDate<=#" + eedate + "#";
                DataTable dt = dv.ToTable();
                ChartGroupRegDate.DataSource = dt;
                ChartGroupRegDate.DataBind();
                int rcHeight = dt.Rows.Count * 40;
                ChartGroupRegDate.Height = rcHeight > 300 ? rcHeight : 300;
                #endregion

                #region Individual
                DataTable dtIndivDate = GrpByIndivRegDate(showid, false);
                DataView dvIndiv = new DataView();
                dvIndiv = dtIndivDate.DefaultView;
                dvIndiv.RowFilter = "RegDate>=#" + ssdate + "# And RegDate<=#" + eedate + "#";
                DataTable dtIndiv = dvIndiv.ToTable();
                ChartIndivRegDate.DataSource = dtIndiv;
                ChartIndivRegDate.DataBind();
                int rcHeightIndiv = dtIndiv.Rows.Count * 40;
                ChartIndivRegDate.Height = rcHeightIndiv > 300 ? rcHeightIndiv : 300;
                #endregion

                #region Public
                DataTable dtIndivPublicDate = GrpByIndivRegDate(showid, true);
                DataView dvIndivPublic = new DataView();
                dvIndivPublic = dtIndivPublicDate.DefaultView;
                dvIndivPublic.RowFilter = "RegDate>=#" + ssdate + "# And RegDate<=#" + eedate + "#";
                DataTable dtIndivPublic = dvIndivPublic.ToTable();
                ChartIndivPublicRegDate.DataSource = dtIndivPublic;
                ChartIndivPublicRegDate.DataBind();
                int rcHeightIndivPublic = dtIndivPublic.Rows.Count * 40;
                ChartIndivPublicRegDate.Height = rcHeightIndivPublic > 300 ? rcHeightIndivPublic : 300;
                #endregion
            }
            else
            {
                DataTable dtGroupDate = GrpByGroupRegDate(showid);
                ChartGroupRegDate.DataSource = dtGroupDate;
                ChartGroupRegDate.DataBind();
                int rcHeight = dtGroupDate.Rows.Count * 40;
                ChartGroupRegDate.Height = rcHeight > 300 ? rcHeight : 300;

                #region Individual
                DataTable dtIndivDate = GrpByIndivRegDate(showid, false);
                ChartIndivRegDate.DataSource = dtIndivDate;
                ChartIndivRegDate.DataBind();
                int rcHeightIndiv = dtIndivDate.Rows.Count * 40;
                ChartIndivRegDate.Height = rcHeightIndiv > 300 ? rcHeightIndiv : 300;
                #endregion

                #region Public
                DataTable dtIndivPublicDate = GrpByIndivRegDate(showid, true);
                ChartIndivPublicRegDate.DataSource = dtIndivPublicDate;
                ChartIndivPublicRegDate.DataBind();
                int rcHeightIndivPublic = dtIndivDate.Rows.Count * 40;
                ChartIndivPublicRegDate.Height = rcHeightIndivPublic > 300 ? rcHeightIndivPublic : 300;
                #endregion
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region byCountry
    #region GrpByCountry (get data from tb_RegDelegate, tb_RegGroup, tb_Invoice tables according to the RG_IsMultiple Is Not Null or Like 'G')
    private DataTable GrpByCountry(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("TotalPercentage");
        dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Desc");

        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        DataTable dtmas = msregGroup.getDataAllRegGroupMemberList();
        int masCount = dtmas.Rows.Count;

        DataTable Cat = msregGroup.getRegChartGroupByCountry();
        Double catCount;
        Double TotalCount = masCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["countryname"].ToString();
            drow["TotalPercentage"] = caldbscan.ToString();
            drow["TotalNumber"] = catCount.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["TotalPercentage"] = "0";
            drow["TotalNumber"] = "0";
        }

        return dt;
    }
    #endregion
    #region IndivByCountry (get data from tb_RegDelegate, tb_RegGroup, tb_Invoice tables according to the RG_IsMultiple Is Null or Like 'I')
    private DataTable IndivByCountry(string showid, bool isPublic)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("TotalPercentage");
        dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Desc");

        //MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //msregIndiv.ShowID = showid;
        ////DataTable dtmas = msregIndiv.getDataAllRegList();
        ////DataView dv = new DataView();
        ////dv = dtmas.DefaultView;
        ////dv.RowFilter = "reg_status=1";
        ////dtmas = dv.ToTable();
        ////int masCount = dtmas.Rows.Count;
        DataTable Cat = ByIndivRegCountryFlow(showid, isPublic == false ? FJIndivTradeFlowID : FJIndivPublicFlowID);// msregIndiv.getRegChartIndivByCountry();
        Double catCount;
        Double TotalCount = 0;// masCount;
        if (Cat.Rows.Count > 0)
        {
            TotalCount = cFuz.ParseInt(Cat.Compute("SUM(SubCount)", string.Empty).ToString());
        }
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["countryname"].ToString();
            drow["TotalPercentage"] = caldbscan.ToString();
            drow["TotalNumber"] = catCount.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["TotalPercentage"] = "0";
            drow["TotalNumber"] = "0";
        }

        return dt;
    }
    private DataTable ByIndivRegCountryFlow(string showid, string flowid)
    {
        DataTable dt = new DataTable();
        try
        {
            string sql = string.Format("Select * From RegChartIndivByCountryFlow Where ShowID='{0}' And reg_urlFlowID='{1}'", showid, flowid);
            dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    #endregion
    #endregion

    #region byRegion
    #region GrpByRegion
    private DataTable GrpByRegion(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("TotalPercentage");
        dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Desc");

        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        DataTable dtmas = msregGroup.getDataAllRegGroupMemberList();
        int masCount = dtmas.Rows.Count;

        DataTable Cat = msregGroup.getRegChartGroupByRegion();
        Double catCount;
        Double TotalCount = masCount;
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["regionname"].ToString();
            drow["TotalPercentage"] = caldbscan.ToString();
            drow["TotalNumber"] = catCount.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["TotalPercentage"] = "0";
            drow["TotalNumber"] = "0";
        }

        return dt;
    }
    #endregion
    #region IndivByRegion
    private DataTable IndivByRegion(string showid, bool isPublic)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("TotalPercentage");
        dt.Columns.Add("TotalNumber");
        dt.Columns.Add("Desc");

        //MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //msregIndiv.ShowID = showid;
        ////DataTable dtmas = msregIndiv.getDataAllRegList();
        ////DataView dv = new DataView();
        ////dv = dtmas.DefaultView;
        ////dv.RowFilter = "reg_status=1";
        ////dtmas = dv.ToTable();
        ////int masCount = dtmas.Rows.Count;
        DataTable Cat = ByIndivRegRegionFlow(showid, isPublic == false ? FJIndivTradeFlowID : FJIndivPublicFlowID);// msregIndiv.getRegChartIndivByRegion();
        Double catCount;
        Double TotalCount = 0;// masCount;
        if (Cat.Rows.Count > 0)
        {
            TotalCount = cFuz.ParseInt(Cat.Compute("SUM(SubCount)", string.Empty).ToString());
        }
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFuz.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Desc"] = Cat.Rows[i]["regionname"].ToString();
            drow["TotalPercentage"] = caldbscan.ToString();
            drow["TotalNumber"] = catCount.ToString();
            dt.Rows.Add(drow);
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["TotalPercentage"] = "0";
            drow["TotalNumber"] = "0";
        }

        return dt;
    }
    private DataTable ByIndivRegRegionFlow(string showid, string flowid)
    {
        DataTable dt = new DataTable();
        try
        {
            string sql = string.Format("Select * From RegChartIndivByRegionFlow Where ShowID='{0}' And reg_urlFlowID='{1}'", showid, flowid);
            dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    #endregion
    #endregion

    #region checkFlowTypeGI (get Group or Individual from tb_site_flow_master table)
    private Tuple<string, string> checkFlowTypeGI(string showid)
    {
        Tuple<string, string> tplType;
        GeneralObj grnObj = new GeneralObj(fn);
        DataTable dtFlowMas = grnObj.getAllSiteFlowMaster();

        string grp = string.Empty;
        string inv = string.Empty;
        if(dtFlowMas.Rows.Count > 0)
        {
            foreach(DataRow dr in dtFlowMas.Rows)
            {
                if (dr["ShowID"].ToString() == showid)
                {
                    string flowtype = dr["FLW_Type"].ToString();
                    if (flowtype == SiteFlowType.FLOW_GROUP)
                    {
                        grp = SiteFlowType.FLOW_GROUP;
                    }
                    else
                    {
                        inv = SiteFlowType.FLOW_INDIVIDUAL;
                    }
                }
            }
        }

        tplType = new Tuple<string, string>(grp, inv);

        return tplType;
    }
    #endregion


    protected void ddlIndividualCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        string country = ddlIndividualCountry.SelectedValue;
        ChartIndividualCategory.DataSource = GrpByIndivCategory(lbl_show.Text, country);
        ChartIndividualCategory.DataBind();
    }

    protected void ddlGroupCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        string country = ddlGroupCountry.SelectedValue;
        ChartGroupCategory.DataSource = GrpByGrpCategory(lbl_show.Text, country);
        ChartGroupCategory.DataBind();
    }

    protected void ddl_showList_SelectedIndexChanged(object sender, EventArgs e)
    {
        lbl_show.Text = ddl_showList.SelectedValue;
        bindCountryList(ddl_showList.SelectedValue);
        loadChart(ddl_showList.SelectedValue);
    }
    private int CheckGroupReg(string userid)
    {
        int res = 0;
        try
        {
            string sql = "select * from tb_site_flow_master where ShowID in (select us_showid from tb_Admin_Show where us_userid='" + userid + "') and Status='Active' and FLW_Type='G'";
            DataSet ds = new DataSet();
            ds = fn.GetDatasetByCommand(sql, "sqlCheckGroup");

            res = ds.Tables[0].Rows.Count;
        }
        catch (Exception ex) { }
        return res;

    }

    #region checkNewVisitorMDA
    #region ByGroupIndivMDAChart
    private DataTable ByGroupIndivMDAChart(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Total");
        dt.Columns.Add("Desc");
        dt.Columns.Add("TotalPercentage");
        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        DataTable dtmas = msregGroup.getRegByGroupIndivList();
        int masCount = dtmas.Rows.Count;
        Double catCount;
        Double TotalCount = masCount;


        DataTable dtSub = new DataTable();
        dtSub.Columns.Add("Total", typeof(int));
        dtSub.Columns.Add("Desc");
        dtSub.Columns.Add("TotalPercentage");
        int countMas = 0;
        if (dtmas.Rows.Count > 0)
        {
            foreach(DataRow drmas in dtmas.Rows)
            {
                string regno = drmas["Regno"].ToString();
                string flowid = drmas["reg_urlFlowID"] != DBNull.Value ? drmas["reg_urlFlowID"].ToString() : "";
                string NewRepeatVisitor = checkNewVisitorMDA(regno, flowid,showid);
                DataRow drow = dtSub.NewRow();
                drow["Desc"] = NewRepeatVisitor;
                drow["Total"] = countMas ++;
                drow["TotalPercentage"] = "0";
                dtSub.Rows.Add(drow);
            }
        }

        if (dtSub.Rows.Count > 0)
        {
            DataTable dtTotal = dtSub.AsEnumerable()
                .GroupBy(r => new { Col1 = r["Desc"] })
                .Select(g =>
                {
                    var row = dtSub.NewRow();

                    row["Total"] = g.Count();// g.Min(r => r.Field<int>("Total"));
                    row["Desc"] = g.Key.Col1;
                    row["TotalPercentage"] = "0";

                    return row;

                })
                .CopyToDataTable();

            for (int i = 0; i < dtTotal.Rows.Count; i++)
            {
                catCount = cFuz.ParseInt(dtTotal.Rows[i]["Total"].ToString());
                Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
                DataRow drow = dt.NewRow();
                drow["Desc"] = dtTotal.Rows[i]["Desc"].ToString();
                drow["Total"] = catCount;
                drow["TotalPercentage"] = caldbscan.ToString();
                dt.Rows.Add(drow);
            }
        }
        else
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["Total"] = "0";
            drow["TotalPercentage"] = "0";
        }

        return dt;
    }
 //   public DataTable GetPreRegByCategory(string ShowID)
 //   {
 //       DataTable dt = new DataTable();

 //       try
 //       {
 //           string query = @" 
 //           select reg_approveStatus Type, Count(*) SubCount from tb_RegDelegate where 
 //             regno    in ( select isnull(RefNo ,0) from tb_RegPrePopulate where ShowID=@SHWID )
 //            and  ShowID=@SHWID and recycle=0 And reg_approveStatus Not In ('" + (int)RegApproveRejectStatus.Rejected + "')" +
 //" group by reg_approveStatus";//***removed Rejected records by th on 12-7-2017, the client want to remove Rejected Data
 //           string query = @"
 //           select Type As Type, Count(*) SubCount from (select 
 //           CASE WHEN reg_approveStatus = 22 Or reg_approveStatus = 11 THEN 11 
 //           When reg_approveStatus=33 And (reg_Status Is Null OR reg_Status=0) Then 0
 //           When reg_approveStatus=33 And (reg_Status=1) Then 33
 //           Else reg_approveStatus END AS Type,* from tb_RegDelegate where 
 //           regno  in ( select isnull(RefNo ,0) from tb_RegPrePopulate where ShowID=@SHWID )
 //           and  ShowID=@SHWID and recycle=0 ) as aa group by Type";//And reg_approveStatus Not In ('" + (int)RegApproveRejectStatus.Rejected + "')
 //           List<SqlParameter> pList = new List<SqlParameter>();
 //           SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
 //           spar.Value = ShowID;
 //           pList.Add(spar);

 //           dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
 //       }
 //       catch (Exception ex)
 //       { }

 //       return dt;
 //   }
 //   public DataTable GetRegByCategory(string ShowID)
 //   {
 //       DataTable dt = new DataTable();

 //       try
 //       {
 //           //            string query = @" 
 //           //select reg_approveStatus Type, Count(*) SubCount from tb_RegDelegate where 
 //           //  regno  not  in ( select isnull(RefNo ,0) from tb_RegPrePopulate where ShowID=@SHWID )
 //           // and  ShowID=@SHWID and recycle=0 And reg_approveStatus Not In ('" + (int)RegApproveRejectStatus.Rejected + "')" +
 //           // " group by reg_approveStatus";//***removed Rejected records by th on 12-7-2017, the client want to remove Rejected Data
 //           string query = @"
 //           select Type As Type, Count(*) SubCount from (select 
 //           CASE WHEN reg_approveStatus = 22 Or reg_approveStatus = 11 THEN 11 
 //           When reg_approveStatus=33 And (reg_Status Is Null OR reg_Status=0) Then 0
 //           When reg_approveStatus=33 And (reg_Additional5 Is Null Or reg_Additional5='') Then 555
 //           When reg_approveStatus=33 And (reg_Status=1) Then 33
 //           Else reg_approveStatus END AS Type ,* 
 //           from tb_RegDelegate where
 //            regno  not  in ( select isnull(RefNo ,0) from tb_RegPrePopulate where ShowID=@SHWID )
 //            and  ShowID=@SHWID and recycle=0 ) as aa group by Type";//And reg_approveStatus Not In ('" + (int)RegApproveRejectStatus.Rejected + "')

 //           List<SqlParameter> pList = new List<SqlParameter>();
 //           SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
 //           spar.Value = ShowID;
 //           pList.Add(spar);

 //           dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
 //       }
 //       catch (Exception ex)
 //       { }

 //       return dt;
 //   }
    #endregion
    private static string _newVisitorMDA = "New Visitor";
    private static string _repeatVisitorMDA = "Repeat Visitor";
    private static string _checkQuestionMDA = "'Did you visit MEDICAL FAIR ASIA 2016?', 'Did you visit MEDICAL MANUFACTURING ASIA in 2016?', 'Did you visit OS+H Asia in 2016'";

    public string checkNewVisitorMDA(string regno, string flowid, string showid)
    {
        string result = string.Empty;
        try
        {
            result = _newVisitorMDA;
            if (!string.IsNullOrEmpty(regno) && !string.IsNullOrWhiteSpace(regno))
            {
                string qnaireID = getQnaireID(flowid, showid);

                string sqlResult = "SELECT * INTO #TempTable FROM tbl" + qnaireID + " where user_reference='" + regno + "' "
                                               + "ALTER TABLE #TempTable "
                                               + "DROP COLUMN qnaire_log_id,user_reference,status,create_date,update_date "
                                               + "SELECT * FROM #TempTable";
                DataTable dtResut = qfn.GetDatasetByCommand(sqlResult, "sqResult").Tables[0];

                //    string sql = "Select * From gen_QnaireLog Where qnaire_log_id In("
                //            + " Select qnaire_log_id From gen_QnaireResult Where qnaire_log_id In"
                //            + " (Select qnaire_log_id From gen_QnaireLog Where qnaire_id='" + qnaireID + "' And status='Active')"
                //            + " And qitem_id In (Select qitem_id From gen_QuestItem Where"
                //            + " quest_id In (Select quest_id From gen_Question Where quest_id In"
                //            + " (Select quest_id From gen_QnaireQuest Where qnaire_id='" + qnaireID + "' And status='Active')"
                //            + " And quest_desc In (" + _checkQuestionMDA + ") And status='Active')"
                //            + " And qitem_desc Like 'Yes' And status='Active')"
                //            + " And status='Active'"
                //            + " )"
                //            + " And user_reference='" + regno + "'";
                //DataTable dt = qfn.GetDatasetByCommand(sql, "ds").Tables[0];
                //if (dt.Rows.Count > 0)
                //{
                if (dtResut.Rows.Count > 0)
                {
                    ShowControler shwCtr = new ShowControler(fn);
                    Show shw = shwCtr.GetShow(showid);
                    if (shw.SHW_Name == checkingMDAShowName[0])
                    {
                        if (dtResut.Rows[0]["QITM5656"].ToString() == "1")
                        {
                            result = _repeatVisitorMDA;
                        }
                    }
                    else if (shw.SHW_Name == checkingMDAShowName[1])
                    {
                        if (dtResut.Rows[0]["QITM5764"].ToString() == "1")
                        {
                            result = _repeatVisitorMDA;
                        }
                    }
                    else if (shw.SHW_Name == checkingMDAShowName[2])
                    {
                        if (dtResut.Rows[0]["QITM5535"].ToString() == "1")
                        {
                            result = _repeatVisitorMDA;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getQnaireID(string flowid, string showid)
    {
        string result = string.Empty;
        if (Session["userid"] != null)
        {
            try
            {
                FlowControler flwControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
                string flowType = flwMasterConfig.FlowType;
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                {
                    flowType = BackendRegType.backendRegType_Delegate;
                }
                result = getQID(showid, flowid, flowType);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

        return result;
    }
    private string getQID(string showid, string flowid, string type)
    {
        QuestionnaireControler qCtr = new QuestionnaireControler(fn);
        DataTable dt = qCtr.getQID(type, flowid, showid);
        if (dt.Rows.Count > 0)
            return dt.Rows[0]["SQ_QAID"].ToString();
        else
            return "QLST";
    }
    #endregion
}
