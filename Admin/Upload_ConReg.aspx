﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Upload_ConReg.aspx.cs" Inherits="Admin_Upload_ConReg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="tableoptions" id="divUpload" runat="server" style="padding-top:40px;">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-info-circle"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">Note</span>
                <span class="info-box-number" style="font-size:small;">
                    Please download the template first.<br />
                    Please fill the data in all columns :<br />
                    For Personal Particular, if don't have data, please put " <span style="color:red;">.</span>" <span style="color:red;">(dot)</span>)<br />
                    For Conference/Program Selection, please put 1 OR 0 [1 (one) for selected and 0 (zero) for unselected]<br />
                </span>
                <br />
                <br />
            </div>
            <!-- /.info-box-content -->
            <br />
            <asp:Label ID="lblIDEMNote" runat="server" Visible="false">
                For <strong style="font-size:15px;font-weight:bold;color:#DD4B39;"><i>Would you like to share your email address with sponsors/exhibitors?</i></strong>, please put <br />
                <strong style="font-size:15px;font-weight:bold;color:#DD4B39;"><i>Yes, I agree to share my email address with sponsors/exhibitors.</i></strong> OR 
                <strong style="font-size:15px;font-weight:bold;color:#DD4B39;"><i>No, I do not agree to share my email address with sponsors/exhibitors</i></strong><br /><br />
            </asp:Label>
        </div>
        <div class="row">
            <div class="col-md-2">
                Registration :
            </div>
            <div class="col-md-4">
                <asp:DropDownList runat="server" ID="ddlFlow" CssClass="form-control"></asp:DropDownList>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-2">
                Download Template :
            </div>
            <div class="col-md-4">
                <asp:LinkButton ID="lnkDownloadTemplate" runat="server" Text="Download Template" 
                    OnClick="lnkDownloadTemplate_Click" Font-Underline="true"></asp:LinkButton>
            </div>
            <div class="col-md-6">
                &nbsp;
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-2">
                Upload File :
            </div>
            <div class="col-md-4">
                <asp:FileUpload ID="excelUpload" runat="server" CssClass="bodypar" />
            </div>
            <div class="col-md-6">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                    ErrorMessage="Select the file to upload" ForeColor="Red"
                    ControlToValidate="excelUpload" ValidationGroup="Validate"></asp:RequiredFieldValidator>
            </div>
        </div>
        <br />
        <br />
        <div class="row">
            <div class="col-md-2">
                &nbsp;
            </div>
            <div class="col-md-6">
                <asp:Button ID="btnUpload" runat="server" Text="Upload file" ValidationGroup="Validate" OnClick="btnUpload_Click" CssClass="btn-success" />
            </div>
        </div>
        <br />
        <br />
    </div>
    <br />
    <br />
    <br />

    <div class="row">
        <div class="col-md-12">
            <div class="tableoptions" id="divSaveData" runat="server" visible="false">
                <table width="600" align="center" style="border: 1px" class="table">
                    <tr>
                        <td align="left" width="250"><b>Step 2</b></td>
                        <td align="left" width="5"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left" width="250">Please check your details and click Save</td>
                        <td align="left" width="5">:</td>
                        <td align="left">
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn-warning" />
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn-default" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                </table>

                <div style="overflow-x: scroll">
                    <asp:GridView runat="server" ID="gvList" AutoGenerateColumns="true" Width="100%" CssClass="table table-bordered"></asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <div class="tableoptions" id="divUnSuccessList" runat="server" visible="false">
        <h2>Un-Success List</h2>
        <i style="color:red;">The below record(s) already exist.</i>
        <div style="overflow-x: scroll">
            <asp:GridView runat="server" ID="gvUnSuccessList" AutoGenerateColumns="true" Width="100%" CssClass="table table-bordered" 
                HeaderStyle-BackColor="LightGray" RowStyle-BackColor="LightPink"></asp:GridView>
        </div>
    </div>

    <div class="tableoptions" id="PanelSuccess" runat="server" visible="false">
        <h2>Success List</h2>
        <div style="overflow-x: scroll">
            <asp:GridView runat="server" ID="gSuccess" AutoGenerateColumns="true" Width="100%" CssClass="gridstyle"></asp:GridView>
        </div>
    </div>


    <asp:TextBox runat="server" ID="txtShow" Visible="false"></asp:TextBox>
    <asp:TextBox runat="server" ID="txtUserID" Visible="false"></asp:TextBox>
</asp:Content>

