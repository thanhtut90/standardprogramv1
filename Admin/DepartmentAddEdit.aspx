﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="DepartmentAddEdit.aspx.cs" Inherits="Admin_DepartmentAddEdit" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Department Add/Edit</h1>
            <span class="pagedesc"></span>
            
            <ul class="hornav">
                <li class="current"><a href="#index">Department</a></li>
            </ul>
        </div><!--page header -->
        
        <div id="contentwrapper" class="contentwrapper">
            <div id="index" class="subcontent">
                <h4>Institution</h4>
                <asp:Label ID="lblShowID" runat="server" Visible="false"></asp:Label>
                <asp:Panel ID="pnlmessagebar" runat="server" CssClass="notibar announcement" Visible="false">
                    <p class="txtgreen">
                    <asp:Label ID="lblemsg" runat="server" Text="Label"></asp:Label> </p>
                </asp:Panel>
                <table class="stdtable">
                    <tbody>
                        <tr>
                            <td colspan="2"> <strong><asp:Label ID="lblmsg" runat="server" Text="" ForeColor="Green"></asp:Label></strong></td>
                        </tr>
                        <tr>
                            <td width="150px">Select Institution</td>
                            <td>
                                <asp:DropDownList ID="ddlInstitution" runat="server" Width="250px"></asp:DropDownList>
                                <asp:CompareValidator ID="cvInstitution" runat="server" 
                                ControlToValidate="ddlInstitution" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Please select" ForeColor="Red"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="150px">Deparment Name</td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" Width="460px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="*Required" ForeColor="Red" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="150px">Sorting</td>
                            <td>
                                <asp:TextBox ID="txtSorting" runat="server" Width="250px" Text="0"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbSorting" runat="server" TargetControlID="txtSorting" FilterType="Numbers" ValidChars="0123456789" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="elecenter"><asp:Button ID="btnSave" runat="server" Text="Save" OnClick="SaveForm" /></div>
            </div>
        </div>
    </div>
</asp:Content>