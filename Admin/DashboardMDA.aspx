﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master"  Debug="true" AutoEventWireup="true" CodeFile="DashboardMDA.aspx.cs" Inherits="DashboardMDA" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function exportChartToImageGRegion() {

            var $ = $telerik.$;
            //get reference to the ClientExportManager object
            var exportManager = $find('<%=RadClientExportManager1.ClientID%>');

            var filename = 'Group Registration By Region' + '.png';

            //specify the image settings fileName, proxy, width, height
            var imageSettings = {
                fileName: filename
             }
            //set the image settings
            exportManager.set_imageSettings(imageSettings);
            //export the element/container
            //exportManager.exportImage($(".RadHtmlChart"));
            exportManager.exportImage($("#ctl00_ContentPlaceHolder1_ChartGroupRegion"));
        }
        function exportChartToImageIRegion() {

            var $ = $telerik.$;
            //get reference to the ClientExportManager object
            var exportManager = $find('<%=RadClientExportManager1.ClientID%>');

            var filename = 'Individual Registration By Region' + '.png';

            //specify the image settings fileName, proxy, width, height
            var imageSettings = {
                fileName: filename
             }
            //set the image settings
            exportManager.set_imageSettings(imageSettings);
            //export the element/container
            //exportManager.exportImage($(".RadHtmlChart"));
            exportManager.exportImage($("#ctl00_ContentPlaceHolder1_ChartIndivRegion"));
        }
        function exportChartToImageGDate() {

            var $ = $telerik.$;
            //get reference to the ClientExportManager object
            var exportManager = $find('<%=RadClientExportManager1.ClientID%>');

            var filename = 'Group Registration By Registration Date' + '.png';

            //specify the image settings fileName, proxy, width, height
            var imageSettings = {
                fileName: filename
             }
            //set the image settings
            exportManager.set_imageSettings(imageSettings);
            //export the element/container
            //exportManager.exportImage($(".RadHtmlChart"));
            exportManager.exportImage($("#ctl00_ContentPlaceHolder1_ChartGroupRegDate"));
        }
        function exportChartToImageIDate() {

            var $ = $telerik.$;
            //get reference to the ClientExportManager object
            var exportManager = $find('<%=RadClientExportManager1.ClientID%>');

            var filename = 'Individual Registration By Registration Date' + '.png';

            //specify the image settings fileName, proxy, width, height
            var imageSettings = {
                fileName: filename
             }
            //set the image settings
            exportManager.set_imageSettings(imageSettings);
            //export the element/container
            //exportManager.exportImage($(".RadHtmlChart"));
            exportManager.exportImage($("#ctl00_ContentPlaceHolder1_ChartIndivRegDate"));
        }
    </script>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"></telerik:RadAjaxManager>
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Dashboard</h1>
        </div>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div id="contentwrapper" class="contentwrapper">
                    <asp:Panel ID="Panel1" runat="server" Visible="false">
                        Show List : <asp:DropDownList ID="ddl_showList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_showList_SelectedIndexChanged"></asp:DropDownList>
                        <asp:Label ID="lbl_show" runat="server" Text="" Visible="false"></asp:Label>
                    </asp:Panel>
                    <br />
                    <div class="col-lg-12">
                        <div style="width:50%;float:left;">
                            <telerik:RadHtmlChart runat="server" ID="ChartCategory"  CssClass="fb-sized">
                                <PlotArea>
                                    <Series>
                                        <telerik:PieSeries StartAngle="45" DataFieldY="SubCount" ExplodeField="IsExploded" Name="PieSeriesName"
                                            NameField="Type">
                                            <LabelsAppearance><%-- DataFormatString="{0}%"--%>
                                                <ClientTemplate>
                                                    #=dataItem.SubCount# (#=dataItem.Percentage# %)
                                                </ClientTemplate>
                                            </LabelsAppearance>
                                            <TooltipsAppearance Color="White" ClientTemplate="#=dataItem.Type# #=dataItem.SubCount# (#=dataItem.Percentage# %)"></TooltipsAppearance><%--DataFormatString="{0}%"--%>
                                        </telerik:PieSeries>
                                    </Series>
                                    <YAxis>
                                    </YAxis>
                                </PlotArea>
                                <ChartTitle Text="By Type"></ChartTitle>
                            </telerik:RadHtmlChart>
                        </div>
                        <div style="width:50%;float:right;">
                            <telerik:RadHtmlChart runat="server" ID="ChartLocalInternational"  CssClass="fb-sized">
                                <PlotArea>
                                    <Series>
                                        <telerik:PieSeries StartAngle="45" DataFieldY="SubCount" ExplodeField="IsExploded" Name="PieSeriesName"
                                            NameField="countryname">
                                            <LabelsAppearance><%-- DataFormatString="{0}%"--%>
                                                <ClientTemplate>
                                                    #=dataItem.SubCount# (#=dataItem.Percentage# %)
                                                </ClientTemplate>
                                            </LabelsAppearance>
                                            <TooltipsAppearance Color="White" ClientTemplate="#=dataItem.countryname# #=dataItem.SubCount# (#=dataItem.Percentage# %)"></TooltipsAppearance><%--DataFormatString="{0}%"--%>
                                        </telerik:PieSeries>
                                    </Series>
                                    <YAxis>
                                    </YAxis>
                                </PlotArea>
                                <ChartTitle Text="By Thailand vs International"></ChartTitle>
                            </telerik:RadHtmlChart>
                        </div>
                    </div>
                    <br /><br />

                    <%--By Type (Website or Landing Page [By No. of  Individual Registrations]) created on 30-5-2019--%>
                    <div class="col-lg-12">
                        <div style="width:50%;float:left;">
                            <telerik:RadHtmlChart runat="server" ID="ChartWebsiteLanding"  CssClass="fb-sized">
                                <PlotArea>
                                    <Series>
                                        <telerik:PieSeries StartAngle="45" DataFieldY="SubCount" ExplodeField="IsExploded" Name="PieSeriesName"
                                            NameField="TrackCode">
                                            <LabelsAppearance><%-- DataFormatString="{0}%"--%>
                                                <ClientTemplate>
                                                    #=dataItem.SubCount# (#=dataItem.Percentage# %)
                                                </ClientTemplate>
                                            </LabelsAppearance>
                                            <TooltipsAppearance Color="White" ClientTemplate="#=dataItem.TrackCode# #=dataItem.SubCount# (#=dataItem.Percentage# %)"></TooltipsAppearance><%--DataFormatString="{0}%"--%>
                                        </telerik:PieSeries>
                                    </Series>
                                    <YAxis>
                                    </YAxis>
                                </PlotArea>
                                <ChartTitle Text="By Website vs Landing"></ChartTitle>
                            </telerik:RadHtmlChart>
                        </div>
                    </div>
                    <br /><br />

                    <%--<div class="col-lg-12" id="divMDA" runat="server" visible="false">
                        <div style="width:50%;float:left;">
                            <telerik:RadHtmlChart runat="server" ID="ChartMDA"  CssClass="fb-sized">
                                <PlotArea>
                                    <Series>
                                        <telerik:PieSeries StartAngle="45" DataFieldY="Total" ExplodeField="IsExploded" Name="PieSeriesName"
                                            NameField="Desc">
                                            <LabelsAppearance>
                                                <ClientTemplate>
                                                    #=dataItem.Total# (#=dataItem.TotalPercentage# %)
                                                </ClientTemplate>
                                            </LabelsAppearance>
                                            <TooltipsAppearance Color="White" DataFormatString="#=dataItem.Total# (#=dataItem.TotalPercentage# %)"></TooltipsAppearance>
                                        </telerik:PieSeries>
                                    </Series>
                                    <YAxis>
                                    </YAxis>
                                </PlotArea>
                                <ChartTitle Text="By New/Repeat Type"></ChartTitle>
                            </telerik:RadHtmlChart>
                        </div>
                    </div>--%>

                    <telerik:RadClientExportManager runat="server" ID="RadClientExportManager1"></telerik:RadClientExportManager>

                    <div class="col-lg-12" id="divDate" runat="server">
                        <div class="box" style="width:100%;">
                            <div class="box-header with-border">
                                <h1 class="box-title" style="font-size:25px;">By Date</h1>
                                <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="col-sm-12" style="padding-left:0px;">
                                    <div class="col-sm-2" style="padding-left:0px;">
                                        <asp:TextBox ID="txtFromDate" placeholder="From Date" runat="server" ClientIDMode="Static" onclick="$(this).datepicker().datepicker('show')" CssClass="form-control col-sm-1"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="txtToDate" placeholder="To Date" runat="server" onclick="$(this).datepicker().datepicker('show')" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-1">
                                        <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-success" runat="server" ><i class="fa fa-search"></i> </asp:LinkButton>
                                    </div>
                                </div>
                                <br /><br /><br />
                                <div style="width:100%;float:left;">
                                    <%--<telerik:RadHtmlChart runat="server" ID="ChartGroupRegDate"  CssClass="fb-sized">
                                        <PlotArea>
                                            <Series>
                                                <telerik:PieSeries StartAngle="45" DataFieldY="TotalPercentage" ExplodeField="IsExploded" Name="PieSeriesName"
                                                    NameField="Desc">
                                                    <LabelsAppearance DataFormatString="{0}%">
                                                    </LabelsAppearance>
                                                    <TooltipsAppearance Color="White" DataFormatString="{0}%"></TooltipsAppearance>
                                                </telerik:PieSeries>
                                            </Series>
                                            <YAxis>
                                            </YAxis>
                                        </PlotArea>
                                        <ChartTitle Text="Group Registration By Registration Date"></ChartTitle>
                                    </telerik:RadHtmlChart>--%>
                                    <telerik:RadButton RenderMode="Lightweight" runat="server" OnClientClicked="exportChartToImageGDate" 
                                    Text="Export Group Registration By Date to Image" AutoPostBack="false" Skin="Office2007" ID="btnChartGroupRegDate" />
                                    <telerik:RadHtmlChart runat="server" ID="ChartGroupRegDate" Width="100%" Transitions="true" Skin="Silk">
                                        <%--<Zoom Enabled="true">
                                            <MouseWheel Enabled="true" />
                                            <Selection Enabled="true" ModifierKey="Shift" />
                                        </Zoom>--%>
                                        <PlotArea>
                                            <Series>
                                                <telerik:BarSeries Name="Registration" Stacked="false" Spacing="0.4" DataFieldY="dateCount" Gap="1.5">
                                                    <Appearance>
                                                        <FillStyle BackgroundColor="#4572A7"></FillStyle>
                                                    </Appearance>
                                                    <LabelsAppearance>
                                                        <TextStyle Color="#4572A7" FontSize="13" Bold="true"/>
                                                        <ClientTemplate>
                                                            #=dataItem.dateCount# (#=dataItem.Percentage# %)
                                                        </ClientTemplate>
                                                    </LabelsAppearance>
                                                    <TooltipsAppearance ClientTemplate="#=dataItem.Desc# <br /> <strong> Registration </strong> : #=dataItem.dateCount# (#=dataItem.Percentage# %)"></TooltipsAppearance>
                                                </telerik:BarSeries>
                                            </Series>
                                            <Appearance>
                                                <FillStyle BackgroundColor="Transparent"></FillStyle>
                                            </Appearance>
                                            <XAxis AxisCrossingValue="0" Color="black" MajorTickType="Outside" MinorTickType="None" DataLabelsField="Desc"
                                                Reversed="false">
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="360" Skip="0" Step="1"><TextStyle Color="Black" Bold="true" /></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="Date">
                                                    <TextStyle Color="#666" Bold="true" />
                                                </TitleAppearance>
                                            </XAxis>
                                            <YAxis AxisCrossingValue="0" Color="black" Reversed="false">
                                                <LabelsAppearance RotationAngle="0" Skip="0" Step="1"><TextStyle Color="#666666" /></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="Registrants">
                                                    <TextStyle Color="#6D869F" Bold="true" />
                                                </TitleAppearance>
                                            </YAxis>
                                        </PlotArea>
                                        <Appearance>
                                            <FillStyle BackgroundColor="Transparent"></FillStyle>
                                        </Appearance>
                                        <ChartTitle Text="Group Registration By Registration Date">
                                            <Appearance Align="Center" BackgroundColor="Transparent" Position="Top">
                                                <TextStyle Color="#3E576F" FontSize="20" />
                                            </Appearance>
                                        </ChartTitle>
                                        <Legend>
                                            <Appearance BackgroundColor="Transparent" Position="Bottom">
                                            </Appearance>
                                        </Legend>
                                    </telerik:RadHtmlChart>
                                </div>
                                <br /><br />
                                <div style="width:100%;float:right;">
                                    <%--<telerik:RadHtmlChart runat="server" ID="ChartIndivRegDate"  CssClass="fb-sized">
                                        <PlotArea>
                                            <Series>
                                                <telerik:PieSeries StartAngle="45" DataFieldY="TotalPercentage" ExplodeField="IsExploded" Name="PieSeriesName"
                                                    NameField="Desc">
                                                    <LabelsAppearance DataFormatString="{0}%">
                                                    </LabelsAppearance>
                                                    <TooltipsAppearance Color="White" DataFormatString="{0}%"></TooltipsAppearance>
                                                </telerik:PieSeries>
                                            </Series>
                                            <YAxis>
                                            </YAxis>
                                        </PlotArea>
                                        <ChartTitle Text="Individual Registration By Registration Date"></ChartTitle>
                                    </telerik:RadHtmlChart>--%>
                                    <telerik:RadButton RenderMode="Lightweight" runat="server" OnClientClicked="exportChartToImageIDate" 
                                    Text="Export Individual Registration By Date to Image" AutoPostBack="false" Skin="Office2007" />
                                    <telerik:RadHtmlChart runat="server" ID="ChartIndivRegDate" Width="100%" Transitions="true" Skin="Silk">
                                        <%--<Zoom Enabled="true">
                                            <MouseWheel Enabled="true" />
                                            <Selection Enabled="true" ModifierKey="Shift" />
                                        </Zoom>--%>
                                        <PlotArea>
                                            <Series>
                                                <telerik:BarSeries Name="Registration" Stacked="false" Spacing="0.4" DataFieldY="dateCount" Gap="1.5">
                                                    <Appearance>
                                                        <FillStyle BackgroundColor="#4572A7"></FillStyle>
                                                    </Appearance>
                                                    <LabelsAppearance>
                                                        <TextStyle Color="#4572A7" FontSize="13" Bold="true"/>
                                                        <ClientTemplate>
                                                            #=dataItem.dateCount# (#=dataItem.Percentage# %)
                                                        </ClientTemplate>
                                                    </LabelsAppearance>
                                                    <TooltipsAppearance ClientTemplate="#=dataItem.Desc# <br /> <strong> Registration </strong> : #=dataItem.dateCount# (#=dataItem.Percentage# %)"></TooltipsAppearance>
                                                </telerik:BarSeries>
                                            </Series>
                                            <Appearance>
                                                <FillStyle BackgroundColor="Transparent"></FillStyle>
                                            </Appearance>
                                            <XAxis AxisCrossingValue="0" Color="black" MajorTickType="Outside" MinorTickType="None" DataLabelsField="Desc"
                                                Reversed="false">
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="360" Skip="0" Step="1"><TextStyle Color="Black" Bold="true" /></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="Date">
                                                    <TextStyle Color="#666" Bold="true" />
                                                </TitleAppearance>
                                            </XAxis>
                                            <YAxis AxisCrossingValue="0" Color="black" Reversed="false">
                                                <LabelsAppearance RotationAngle="0" Skip="0" Step="1"><TextStyle Color="#666666" /></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="Registrants">
                                                    <TextStyle Color="#6D869F" Bold="true" />
                                                </TitleAppearance>
                                            </YAxis>
                                        </PlotArea>
                                        <Appearance>
                                            <FillStyle BackgroundColor="Transparent"></FillStyle>
                                        </Appearance>
                                        <ChartTitle Text="Individual Registration By Registration Date">
                                            <Appearance Align="Center" BackgroundColor="Transparent" Position="Top">
                                                <TextStyle Color="#3E576F" FontSize="20" />
                                            </Appearance>
                                        </ChartTitle>
                                        <Legend>
                                            <Appearance BackgroundColor="Transparent" Position="Bottom">
                                            </Appearance>
                                        </Legend>
                                    </telerik:RadHtmlChart>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!-- /.box -->
                    </div>

                    <div class="col-lg-12" id="divRegion" runat="server">
                        <!-- Default box -->
                        <div class="box" style="width:100%;">
                            <div class="box-header with-border">
                                <h1 class="box-title" style="font-size:25px;">By Region</h1>
                                <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div style="width:100%;">
                                    <telerik:RadButton RenderMode="Lightweight" runat="server" OnClientClicked="exportChartToImageGRegion" 
                                    Text="Export Group Registration By Region to Image" AutoPostBack="false" Skin="Office2007" ID="btnChartGroupRegion" />
                                    <telerik:RadHtmlChart runat="server" ID="ChartGroupRegion" Width="100%" Transitions="true" Skin="Silk">
                                        <%--<Zoom Enabled="true">
                                            <MouseWheel Enabled="true" />
                                            <Selection Enabled="true" ModifierKey="Shift" />
                                        </Zoom>--%>
                                        <PlotArea>
                                            <Series>
                                                <telerik:BarSeries Name="Registration" Stacked="false" Spacing="0.4" DataFieldY="SubCount" Gap="1.5">
                                                    <Appearance>
                                                        <FillStyle BackgroundColor="#4572A7"></FillStyle>
                                                    </Appearance>
                                                    <LabelsAppearance>
                                                        <TextStyle Color="#4572A7" FontSize="13" Bold="true"/>
                                                        <ClientTemplate>
                                                            #=dataItem.SubCount# (#=dataItem.Percentage# %)
                                                        </ClientTemplate>
                                                    </LabelsAppearance>
                                                    <TooltipsAppearance ClientTemplate="#=dataItem.regionname# <br /> <strong> Registration </strong> : #=dataItem.SubCount# (#=dataItem.Percentage# %)"></TooltipsAppearance>
                                                </telerik:BarSeries>
                                            </Series>
                                            <Appearance>
                                                <FillStyle BackgroundColor="Transparent"></FillStyle>
                                            </Appearance>
                                            <XAxis AxisCrossingValue="0" Color="black" MajorTickType="Outside" MinorTickType="None" DataLabelsField="regionname"
                                                Reversed="false">
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="360" Skip="0" Step="1"><TextStyle Color="Black" Bold="true" /></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="Region">
                                                    <TextStyle Color="#666" Bold="true" />
                                                </TitleAppearance>
                                            </XAxis>
                                            <YAxis AxisCrossingValue="0" Color="black" Reversed="false">
                                                <LabelsAppearance RotationAngle="0" Skip="0" Step="1"><TextStyle Color="#666666" /></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="Registrants">
                                                    <TextStyle Color="#6D869F" Bold="true" />
                                                </TitleAppearance>
                                            </YAxis>
                                        </PlotArea>
                                        <Appearance>
                                            <FillStyle BackgroundColor="Transparent"></FillStyle>
                                        </Appearance>
                                        <ChartTitle Text="Group Registration By Region">
                                            <Appearance Align="Center" BackgroundColor="Transparent" Position="Top">
                                                <TextStyle Color="#3E576F" FontSize="20" />
                                            </Appearance>
                                        </ChartTitle>
                                        <Legend>
                                            <Appearance BackgroundColor="Transparent" Position="Bottom">
                                            </Appearance>
                                        </Legend>
                                    </telerik:RadHtmlChart>
                                </div>
                                <br /><br />
                                <div style="width:100%;">
                                    <telerik:RadButton RenderMode="Lightweight" runat="server" OnClientClicked="exportChartToImageIRegion" 
                                    Text="Export Individual Registration By Region to Image" AutoPostBack="false" Skin="Office2007" />
                                    <telerik:RadHtmlChart runat="server" ID="ChartIndivRegion" Width="100%" Transitions="true" Skin="Silk">
                                        <%-- <Zoom Enabled="true">
                                            <MouseWheel Enabled="true" />
                                            <Selection Enabled="true" ModifierKey="Shift" />
                                        </Zoom>--%>
                                        <PlotArea>
                                            <Series>
                                                <telerik:BarSeries Name="Registration" Stacked="false" Spacing="0.4" DataFieldY="SubCount" Gap="1.5">
                                                    <Appearance>
                                                        <FillStyle BackgroundColor="#4572A7"></FillStyle>
                                                    </Appearance>
                                                    <LabelsAppearance>
                                                        <TextStyle Color="#4572A7" FontSize="13" Bold="true"/>
                                                        <ClientTemplate>
                                                            #=dataItem.SubCount# (#=dataItem.Percentage# %)
                                                        </ClientTemplate>
                                                    </LabelsAppearance>
                                                    <TooltipsAppearance ClientTemplate="#=dataItem.regionname# <br /> <strong> Registration </strong> : #=dataItem.SubCount# (#=dataItem.Percentage# %)"></TooltipsAppearance>
                                                </telerik:BarSeries>
                                            </Series>
                                            <Appearance>
                                                <FillStyle BackgroundColor="Transparent"></FillStyle>
                                            </Appearance>
                                            <XAxis AxisCrossingValue="0" Color="black" MajorTickType="Outside" MinorTickType="None" DataLabelsField="regionname"
                                                Reversed="false">
                                                <LabelsAppearance DataFormatString="{0}" RotationAngle="360" Skip="0" Step="1"><TextStyle Color="Black" Bold="true" /></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="Region">
                                                    <TextStyle Color="#666" Bold="true" />
                                                </TitleAppearance>
                                            </XAxis>
                                            <YAxis AxisCrossingValue="0" Color="black" Reversed="false">
                                                <LabelsAppearance RotationAngle="0" Skip="0" Step="1"><TextStyle Color="#666666" /></LabelsAppearance>
                                                <TitleAppearance Position="Center" RotationAngle="0" Text="Registrants">
                                                    <TextStyle Color="#6D869F" Bold="true" />
                                                </TitleAppearance>
                                            </YAxis>
                                        </PlotArea>
                                        <Appearance>
                                            <FillStyle BackgroundColor="Transparent"></FillStyle>
                                        </Appearance>
                                        <ChartTitle Text="Individual Registration By Region">
                                            <Appearance Align="Center" BackgroundColor="Transparent" Position="Top">
                                                <TextStyle Color="#3E576F" FontSize="20" />
                                            </Appearance>
                                        </ChartTitle>
                                        <Legend>
                                            <Appearance BackgroundColor="Transparent" Position="Bottom">
                                            </Appearance>
                                        </Legend>
                                    </telerik:RadHtmlChart>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!-- /.box -->
                    </div>

                    <%--<div class="col-lg-12" id="divCategory" runat="server" visible="false">
                        <div class="box" style="width:100%;">
                            <div class="box-header with-border">
                                <h1 class="box-title" style="font-size:25px;">By Category</h1>
                                <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                    <i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div style="width:50%;float:left;">
                                            Country : <asp:DropDownList ID="ddlGroupCountry" runat="server" OnSelectedIndexChanged="ddlGroupCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            <telerik:RadHtmlChart runat="server" ID="ChartGroupCategory"  CssClass="fb-sized">
                                                <PlotArea>
                                                    <Series>
                                                        <telerik:PieSeries StartAngle="45" DataFieldY="Total" ExplodeField="IsExploded" Name="PieSeriesName"
                                                            NameField="Desc">
                                                            <LabelsAppearance DataFormatString="{0}%">
                                                            </LabelsAppearance>
                                                            <TooltipsAppearance Color="White" DataFormatString="{0}%"></TooltipsAppearance>
                                                        </telerik:PieSeries>
                                                    </Series>
                                                    <YAxis>
                                                    </YAxis>
                                                </PlotArea>
                                                <ChartTitle Text="Group Registration By Category"></ChartTitle>
                                            </telerik:RadHtmlChart>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div style="width:50%;float:right;">
                                            Country : <asp:DropDownList ID="ddlIndividualCountry" runat="server" OnSelectedIndexChanged="ddlIndividualCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            <telerik:RadHtmlChart runat="server" ID="ChartIndividualCategory"  CssClass="fb-sized">
                                                <PlotArea>
                                                    <Series>
                                                        <telerik:PieSeries StartAngle="45" DataFieldY="Total" ExplodeField="IsExploded" Name="PieSeriesName"
                                                            NameField="Desc">
                                                            <LabelsAppearance DataFormatString="{0}%">
                                                            </LabelsAppearance>
                                                            <TooltipsAppearance Color="White" DataFormatString="{0}%"></TooltipsAppearance>
                                                        </telerik:PieSeries>
                                                    </Series>
                                                    <YAxis>
                                                    </YAxis>
                                                </PlotArea>
                                                <ChartTitle Text="Individual Registration By Category"></ChartTitle>
                                            </telerik:RadHtmlChart>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"></div>
                            <!-- /.box-footer-->
                        </div>
                        <!-- /.box -->
                    </div>--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

