﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Conference_Promo.aspx.cs" Inherits="Admin_Event_Conference_Promo" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        input[type="checkbox"], input[type="radio"]
        {
            margin : 4px !important;
        }
    </style>
    
    <script src="js/plugins/jquery-1.9.1.min.js"></script>
    <script src="Content/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">

        function openModal() {
            $('#myModal').modal('show');
        }

        function closeModal() {
            $('#myModal').modal('hide');
        }
    </script>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <h3 class="box-title">Manage Conference Visibility Reference Code</h3>

        <br />
        <div id="divPromoCode" runat="server">
            <h4><asp:Label ID="lblPromoHeader" runat="server" Text="Reference Code List" Visible="false"></asp:Label></h4>
            <asp:HyperLink ID="hplBack" runat="server" CssClass="btn btn-info"><i class="fa fa-arrow-left"></i> Back</asp:HyperLink>
            <asp:Button ID="btnAddNewConfItem" runat="server" Text="New Code" OnClick="btnAddNewConfItem_Click" CausesValidation="false" CssClass="btn btn-primary" />
            <br />
            <br />
            <div class="box-body table-responsive no-padding">
                <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="false" CssClass="table table-hover" GridLines="None" OnRowCommand="gvList_RowCommand">
                    <HeaderStyle />
                    <Columns>
                        <asp:TemplateField HeaderText="No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reference Name">
                            <ItemTemplate>
                                <%# Eval("Promo_Name")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="No of Code">
                            <ItemTemplate>
                                <%# Eval("Promo_Count")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="View">
                            <ItemTemplate>
                                <a href='<%# getUrl(Eval("Promo_Id").ToString(), Eval("Promo_CategoryID").ToString()) %>'>View Reference Code</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Creating Conference">
                            <ItemTemplate>
                                <a href='<%# getConfItemUrlWithCatID(Eval("Promo_CategoryID").ToString()) %>'>Create Conference</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Creating Additional Code">
                            <ItemTemplate>
                                <asp:Button class="btn btn-primary" ID="btnLaunchModal" runat="server" CausesValidation="false"
                                     CommandArgument='<%# Eval("Promo_Id") %>' CommandName="LaunchModal" Text="Create Additional Code" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

            <%--<!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" id="btnHFcreate" style="display: none;" >
              Launch demo modal
            </button>
            <!-- Modal -->--%>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Additional Reference Code</h4>
                        </div>
                        <div class="modal-body">
                            Enter Code Quantity
                            <asp:TextBox ID="txtNoofCode" runat="server"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbNoofCode" runat="server" TargetControlID="txtNoofCode"
                                    FilterType="Numbers" ValidChars="0123456789" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnClose" runat="server" class="btn btn-default" Text="Close" OnClick="btnClose_Click" CausesValidation="false"/>
                            <asp:Button ID="btnCreate" runat="server" class="btn btn-primary" Text="Create" OnClick="btnCreate_Click"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-horizontal" id="divPromoCodeInsert" runat="server" visible="false">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Reference Title</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txtPTitle" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Single/Multiple Usage</label>
                        <div class="col-md-6">
                            <asp:DropDownList ID="ddlPUsage" runat="server" AutoPostBack="true" onselectedindexchanged="ddlPUsage_SelectedIndexChanged" CssClass="form-control">
                                <asp:ListItem Value="1">Single Usage</asp:ListItem>
                                <asp:ListItem Value="2">Multiple Usage</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="trMax" runat="server" visible="false" class="form-group">
                        <label class="col-md-2 control-label">Max usage of each code</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txtPMax" runat="server" Font-Size="Medium" MaxLength="6" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <asp:RequiredFieldValidator ID="rfvPMax" runat="server" 
                                ControlToValidate="txtPMax" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            &nbsp;<asp:RegularExpressionValidator ID="revPMax" 
                                runat="server" ControlToValidate="txtPMax" Display="Dynamic" 
                                ErrorMessage="Only numbers are allowed" ForeColor="Red" 
                                ValidationExpression="\d+"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Number of Code</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txtPNoofCode" runat="server" Font-Size="Medium" MaxLength="4" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <asp:RequiredFieldValidator ID="rfvPNoofCode" ControlToValidate="txtPNoofCode" runat="server" Enabled="false"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:FilteredTextBoxExtender ID="ftbPNoofCode" runat="server" TargetControlID="txtPNoofCode"
                                FilterType="Numbers" ValidChars="0123456789" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Reference Prefix</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txtPPrefix" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-4">
                            <asp:RequiredFieldValidator ID="rfvPPrefix" ControlToValidate="txtPPrefix" runat="server" Enabled="false"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-6">
                            <asp:Button ID="btnSave" runat="server" Text="Save Reference Code" CssClass="btn btn-primary" OnClick="SavePromoCode" />
                            <asp:HiddenField ID="hfPromoCodeID" runat="server" Value="" />
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                </div>
            </div>
        </div>

</asp:Content>