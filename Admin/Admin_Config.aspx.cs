﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Admin_Admin_Config : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region GKeyMaster1_NeedDataSource (Binding data into RadGrid & get DataSource From tb_site_flow_master table for RadGrid)
    protected void GKeyMaster1_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        string query = "Select a.admin_id,a.admin_username,ar.role_name,s.SHW_Name from tb_admin as a INNER JOIN tb_Admin_Role as ar ON a.admin_roleid = ar.roleid ";
        query += "FULL OUTER JOIN tb_Admin_Show as ash on ash.us_userid = a.admin_id ";
        query += "FULL OUTER JOIN tb_show as s ON ash.us_showid = s.SHW_ID ";
        query += "WHERE a.admin_isdeleted=0 group by a.admin_id,ar.role_name,a.admin_username,s.SHW_Name";

        DataTable dt = fn.GetDatasetByCommand(query, "dsShow").Tables[0];

        if (dt.Rows.Count > 0)
        {
            GKeyMaster1.DataSource = dt;
        }

    }
    #endregion

    #region GKeyMaster1_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster1_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string adminID = "";
        foreach (GridDataItem item in GKeyMaster1.SelectedItems)
        {
            adminID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"].ToString() : "";
        }
        if (e.CommandName == "EditAdmin")
        {
            GridDataItem item = (GridDataItem)e.Item;

            adminID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"].ToString() : "";
            if (!string.IsNullOrEmpty(adminID))
            {
                adminID = cFun.EncryptValue(adminID);
                Response.Redirect("AdminRegister?adminID=" + adminID);


                /*FlowControler fControl = new FlowControler(fn);

                Dictionary<string, string> nValues = new Dictionary<string, string>();
                nValues = fControl.GetAdminNextRoute(adminID, "0");
                if (nValues.Count > 0)
                {
                    /*string page = nValues["nURL"].ToString();
                    string step = nValues["nStep"].ToString();
                    string FlowID = nValues["FlowID"].ToString();

                    if (page == "") page = "Event_Config_Final";

                    string path = fControl.MakeFullURL(page, FlowID, txtShowID.Text, "", step);

                    Response.Redirect(path);
                }*/
            }
        }
    }
    #endregion

    protected void CreateAdmin(object sender, EventArgs e)
    {
        Response.Redirect("AdminRegister");
    }
}