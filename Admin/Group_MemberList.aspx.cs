﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using System.Text;

public partial class Admin_Group_MemberList : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    QuesFunctionality qfn = new QuesFunctionality();
    CommonFuns cFun = new CommonFuns();

    protected string htmltb;
    static StringBuilder StrBuilder = new StringBuilder();
    string showID = string.Empty;
    string flowID = string.Empty;
    string groupID = string.Empty;
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string other_value = "Others";
    static string prostudent = "Student";
    static string proalliedhealth = "Allied Health";
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                showID = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                flowID = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                if (Request.Params["groupid"] != null)
                {
                    groupID = cFun.DecryptValue(Request.QueryString["groupid"]);

                    GKeyMaster.ExportSettings.ExportOnlyData = true;
                    GKeyMaster.ExportSettings.IgnorePaging = true;
                    GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                    GKeyMaster.ExportSettings.FileName = groupID+ "MemberList_" + DateTime.Now.ToString("ddMMyyyy");
                }
                else
                {
                    Response.Redirect("MasterRegistrationList_Group.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }




    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_RegDelegate and tb_Invoice tables for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            showID = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            groupID = cFun.DecryptValue(Request.QueryString["groupid"]);
            if (!string.IsNullOrEmpty(groupID))
            {
                try
                {


                    //MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
                    //msregIndiv.ShowID = showID;

                    GKeyMaster.DataSource = getDataByGroupID(groupID, showID); //msregIndiv.getDataByGroupID(groupID, showID);
                }

                catch (Exception)
                {
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    public DataTable getDataByGroupID(string groupid, string showID)
    {
        DataTable dt = new DataTable();

        try
        {
            string query = string.Format("Select * From DelegateWithRefValue Where recycle=0 And RegGroupID={0} And ShowID=@SHWID", groupid);
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);

            dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    #endregion

    #region Page_PreRender
    protected void Page_PreRender(object o, EventArgs e)
    {
        showID = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
        groupID = cFun.DecryptValue(Request.QueryString["groupid"].ToString());
        if (!string.IsNullOrEmpty(showID) && !string.IsNullOrEmpty(groupID))
        {
            #region Delegate
            DataTable dtfrm = new DataTable();
            FormManageObj frmObj = new FormManageObj(fn);
            frmObj.showID = showID;
            dtfrm = frmObj.getDynFormForDelegate().Tables[0];

            #region Declaration
            int vis_Salutation = 0;
            int vis_Fname = 0;
            int vis_Lname = 0;
            int vis_OName = 0;
            int vis_PassNo = 0;
            int vis_isReg = 0;
            int vis_regSpecific = 0;
            int vis_IDNo = 0;
            int vis_Designation = 0;
            int vis_Profession = 0;
            int vis_Department = 0;
            int vis_Organization = 0;
            int vis_Institution = 0;
            int vis_Address1 = 0;
            int vis_Address2 = 0;
            int vis_Address3 = 0;
            int vis_Address4 = 0;
            int vis_City = 0;
            int vis_State = 0;
            int vis_PostalCode = 0;
            int vis_Country = 0;
            int vis_RCountry = 0;
            int vis_Tel = 0;
            int vis_Mobile = 0;
            int vis_Fax = 0;
            int vis_Email = 0;
            int vis_Affiliation = 0;
            int vis_Dietary = 0;
            int vis_Nationality = 0;
            int vis_MembershipNo = 0;

            int vis_VName = 0;
            int vis_VDOB = 0;
            int vis_VPassNo = 0;
            int vis_VPassExpiry = 0;
            int vis_VPassIssueDate = 0;
            int vis_VEmbarkation = 0;
            int vis_VArrivalDate = 0;
            int vis_VCountry = 0;

            int vis_UDF_CName = 0;
            int vis_UDF_DelegateType = 0;
            int vis_UDF_ProfCategory = 0;
            int vis_UDF_CPcode = 0;
            int vis_UDF_CLDepartment = 0;
            int vis_UDF_CAddress = 0;
            int vis_UDF_CLCompany = 0;
            int vis_UDF_CCountry = 0;
            int vis_UDF_ProfCategroyOther = 0;
            int vis_UDF_CLCompanyOther = 0;

            int vis_SupName = 0;
            int vis_SupDesignation = 0;
            int vis_SupContact = 0;
            int vis_SupEmail = 0;

            int vis_Age = 0;
            int vis_Gender = 0;
            int vis_DOB = 0;
            int vis_Additional4 = 0;
            int vis_Additional5 = 0;
            #endregion

            if (dtfrm.Rows.Count > 0)
            {
                for (int x = 0; x < dtfrm.Rows.Count; x++)
                {
                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Salutation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Salutation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Salutation);

                                GKeyMaster.MasterTableView.GetColumn("reg_Salutation").HeaderText = labelname;

                                vis_Salutation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Salutation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Fname == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_FName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fname);
                                GKeyMaster.MasterTableView.GetColumn("reg_FName").HeaderText = labelname;

                                vis_Fname++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_FName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Lname == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_LName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Lname);
                                GKeyMaster.MasterTableView.GetColumn("reg_LName").HeaderText = labelname;

                                vis_Lname++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_LName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _OName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_OName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_OName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_OName);
                                GKeyMaster.MasterTableView.GetColumn("reg_OName").HeaderText = labelname;
                                vis_OName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_OName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_PassNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_PassNo").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PassNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_PassNo").HeaderText = labelname;

                                vis_PassNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_PassNo").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _isReg)//Is registered MOH/Resident?
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_isReg == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_isReg").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_isReg);
                                GKeyMaster.MasterTableView.GetColumn("reg_isReg").HeaderText = labelname;

                                vis_isReg++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_isReg").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)//MCR/SNB/PRN
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_regSpecific == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_sgregistered").Display = true;

                                vis_regSpecific++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_sgregistered").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _IDNo)//MCR/SNB/PRN No.
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_IDNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_IDno").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_IDNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_IDno").HeaderText = labelname;

                                vis_IDNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_IDno").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Designation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Designation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                                GKeyMaster.MasterTableView.GetColumn("reg_Designation").HeaderText = labelname;

                                vis_Designation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Designation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Profession == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Profession").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                                GKeyMaster.MasterTableView.GetColumn("reg_Profession").HeaderText = labelname;

                                vis_Profession++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Profession").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Department)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Department == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Department").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Department);
                                GKeyMaster.MasterTableView.GetColumn("reg_Department").HeaderText = labelname;

                                vis_Department++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Department").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Organization == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Organization").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Organization);
                                GKeyMaster.MasterTableView.GetColumn("reg_Organization").HeaderText = labelname;

                                vis_Organization++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Organization").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Institution == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Institution").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Institution);
                                GKeyMaster.MasterTableView.GetColumn("reg_Institution").HeaderText = labelname;

                                vis_Institution++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Institution").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address1 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address1").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address1);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address1").HeaderText = labelname;
                                vis_Address1++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address1").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address2 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address2").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address2);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address2").HeaderText = labelname;
                                vis_Address2++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address2").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address3 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address3").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address3);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address3").HeaderText = labelname;

                                vis_Address3++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address3").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address4 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address4").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address4);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address4").HeaderText = labelname;

                                vis_Address4++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address4").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _City)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_City == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_City").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_City);
                                GKeyMaster.MasterTableView.GetColumn("reg_City").HeaderText = labelname;

                                vis_City++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_City").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _State)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_State == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_State").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_State);
                                GKeyMaster.MasterTableView.GetColumn("reg_State").HeaderText = labelname;

                                vis_State++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_State").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_PostalCode == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_PostalCode").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PostalCode);
                                GKeyMaster.MasterTableView.GetColumn("reg_PostalCode").HeaderText = labelname;

                                vis_PostalCode++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_PostalCode").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Country)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Country == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Country").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Country);
                                GKeyMaster.MasterTableView.GetColumn("reg_Country").HeaderText = labelname;

                                vis_Country++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Country").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_RCountry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_RCountry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_RCountry);
                                GKeyMaster.MasterTableView.GetColumn("reg_RCountry").HeaderText = labelname;
                                vis_RCountry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_RCountry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Tel == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Tel").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Tel);
                                GKeyMaster.MasterTableView.GetColumn("reg_Tel").HeaderText = labelname;

                                vis_Tel++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Tel").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Mobile == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Mobile").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Mobile);
                                GKeyMaster.MasterTableView.GetColumn("reg_Mobile").HeaderText = labelname;

                                vis_Mobile++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Mobile").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Fax == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Fax").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fax);
                                GKeyMaster.MasterTableView.GetColumn("reg_Fax").HeaderText = labelname;
                                vis_Fax++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Fax").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Email == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Email").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Email);
                                GKeyMaster.MasterTableView.GetColumn("reg_Email").HeaderText = labelname;
                                vis_Email++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Email").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Affiliation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Affiliation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Affiliation);
                                GKeyMaster.MasterTableView.GetColumn("reg_Affiliation").HeaderText = labelname;

                                vis_Affiliation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Affiliation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Dietary == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Dietary").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Dietary);
                                GKeyMaster.MasterTableView.GetColumn("reg_Dietary").HeaderText = labelname;

                                vis_Dietary++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Dietary").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Nationality == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Nationality").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Nationality);
                                GKeyMaster.MasterTableView.GetColumn("reg_Nationality").HeaderText = labelname;

                                vis_Nationality++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Nationality").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Age)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Age == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Age").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Age);
                                GKeyMaster.MasterTableView.GetColumn("reg_Age").HeaderText = labelname;

                                vis_Age++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Age").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_DOB == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_DOB").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_DOB);
                                GKeyMaster.MasterTableView.GetColumn("reg_DOB").HeaderText = labelname;

                                vis_DOB++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_DOB").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Gender == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Gender").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Gender);
                                GKeyMaster.MasterTableView.GetColumn("reg_Gender").HeaderText = labelname;
                                vis_Gender++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Gender").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_MembershipNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_MembershipNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").HeaderText = labelname;
                                vis_MembershipNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VName);
                                GKeyMaster.MasterTableView.GetColumn("reg_vName").HeaderText = labelname;
                                vis_VName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VDOB == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vDOB").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VDOB);
                                GKeyMaster.MasterTableView.GetColumn("reg_vDOB").HeaderText = labelname;
                                vis_VDOB++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vDOB").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VPassNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassno").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassno").HeaderText = labelname;
                                vis_VPassNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vPassno").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VPassExpiry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassexpiry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassExpiry);
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassexpiry").HeaderText = labelname;
                                vis_VPassExpiry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vPassexpiry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VPassIssueDate == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vIssueDate").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassIssueDate);
                                GKeyMaster.MasterTableView.GetColumn("reg_vIssueDate").HeaderText = labelname;
                                vis_VPassIssueDate++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vIssueDate").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VEmbarkation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vEmbarkation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VEmbarkation);
                                GKeyMaster.MasterTableView.GetColumn("reg_vEmbarkation").HeaderText = labelname;
                                vis_VEmbarkation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vEmbarkation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VArrivalDate == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vArrivalDate").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VArrivalDate);
                                GKeyMaster.MasterTableView.GetColumn("reg_vArrivalDate").HeaderText = labelname;
                                vis_VArrivalDate++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vArrivalDate").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VCountry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vCountry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VCountry);
                                GKeyMaster.MasterTableView.GetColumn("reg_vCountry").HeaderText = labelname;
                                vis_VCountry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vCountry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CName);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CName").HeaderText = labelname;
                                vis_UDF_CName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_DelegateType == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_DelegateType").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_DelegateType);
                                GKeyMaster.MasterTableView.GetColumn("UDF_DelegateType").HeaderText = labelname;
                                vis_UDF_DelegateType++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_DelegateType").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_ProfCategory == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_ProfCategory").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_ProfCategory);
                                GKeyMaster.MasterTableView.GetColumn("UDF_ProfCategory").HeaderText = labelname;
                                vis_UDF_ProfCategory++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_ProfCategory").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CPcode == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CPcode").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CPcode);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CPcode").HeaderText = labelname;
                                vis_UDF_CPcode++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CPcode").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CLDepartment == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLDepartment").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLDepartment);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLDepartment").HeaderText = labelname;
                                vis_UDF_CLDepartment++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CLDepartment").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CAddress == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CAddress").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CAddress);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CAddress").HeaderText = labelname;
                                vis_UDF_CAddress++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CAddress").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CLCompany == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLCompany").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLCompany);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLCompany").HeaderText = labelname;
                                vis_UDF_CLCompany++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CLCompany").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CCountry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CCountry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CCountry);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CCountry").HeaderText = labelname;
                                vis_UDF_CCountry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CCountry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupName);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorName").HeaderText = labelname;
                                vis_SupName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupDesignation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorDesignation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupDesignation);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorDesignation").HeaderText = labelname;
                                vis_SupDesignation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorDesignation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupContact == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorContact").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupContact);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorContact").HeaderText = labelname;
                                vis_SupContact++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorContact").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupEmail == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorEmail").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupEmail);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorEmail").HeaderText = labelname;
                                vis_SupEmail++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorEmail").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Additional4 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional4").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional4);
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional4").HeaderText = labelname;

                                vis_Additional4++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Additional4").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Additional5 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional5").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional5);
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional5").HeaderText = labelname;
                                vis_Additional5++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Additional5").Display = false;
                        }
                    }
                }
            }

            #endregion
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        showID = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

        if (!string.IsNullOrEmpty(showID))
        {
            string regno = "", groupid = "";

            foreach (GridDataItem item in GKeyMaster.SelectedItems)
            {
                regno = item["Regno"].Text;
                groupid = item["RegGroupID"].Text;
            }

            //if (e.CommandName == "Delete")
            //{
            //    GridDataItem item = (GridDataItem)e.Item;
            //    string del_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
            //    string del_RegGroupID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
            //    if (!string.IsNullOrEmpty(del_Regno))
            //    {
            //        int isDeleted = delRecord(del_Regno, del_RegGroupID);
            //        if (isDeleted > 0)
            //        {
            //            GKeyMaster.Rebind();
            //            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + del_Regno + " is already deleted.');", true);
            //            return;
            //        }
            //    }
            //}
            //else if (e.CommandName == "View")
            //{
            //    GridDataItem item = (GridDataItem)e.Item;
            //    string edit_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
            //    string edit_RegGroupID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
            //    if (!string.IsNullOrEmpty(edit_Regno))
            //    {
            //        Response.Redirect("RegGroup_Edit.aspx?groupid=" + cFun.EncryptValue(edit_RegGroupID) + "&SHW=" + cFun.EncryptValue(showID));
            //    }
            //}
            //else if (e.CommandName == "Edit")
            //{
            //    GridDataItem item = (GridDataItem)e.Item;
            //    string edit_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
            //    string edit_RegGroupID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
            //    if (!string.IsNullOrEmpty(edit_Regno))
            //    {
            //        Response.Redirect("RegIndiv_Edit.aspx?t=m&regno=" + cFun.EncryptValue(edit_Regno) + "&SHW=" + cFun.EncryptValue(showID));
            //    }
            //}
            //else 
            if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                GKeyMaster.ExportSettings.FileName = "GropMemberList";
                GKeyMaster.ExportSettings.IgnorePaging = false;
                GKeyMaster.ExportSettings.ExportOnlyData = true;
                GKeyMaster.ExportSettings.OpenInNewWindow = true;
                GKeyMaster.MasterTableView.ExportToExcel();

            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    /// <summary>
    /// Item databound for the radgrid, set the header text here based on the form management ***Item[UniqueName]
    /// </summary>
    protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (!string.IsNullOrEmpty(showID) && !string.IsNullOrEmpty(groupID))
        {

            if (e.Item is GridHeaderItem)
            {
                GridHeaderItem item = e.Item as GridHeaderItem;

                #region Delegate
                DataTable dtfrm = new DataTable();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showID;
                dtfrm = frmObj.getDynFormForDelegate().Tables[0];

                if (dtfrm.Rows.Count > 0)
                {
                    for (int x = 0; x < dtfrm.Rows.Count; x++)
                    {
                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Salutation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Salutation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Salutation);
                                }
                            }
                            else
                            {
                                (item["reg_Salutation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_FName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_FName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fname);
                                }
                            }
                            else
                            {
                                (item["reg_FName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_LName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_LName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Lname);
                                }
                            }
                            else
                            {
                                (item["reg_LName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _OName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_OName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_OName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_OName);
                                }
                            }
                            else
                            {
                                (item["reg_OName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_PassNo"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_PassNo"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PassNo);
                                }
                            }
                            else
                            {
                                (item["reg_PassNo"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_isReg"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_isReg"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_isReg);
                                }
                            }
                            else
                            {
                                (item["reg_isReg"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_sgregistered"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_sgregistered"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_regSpecific);
                                }
                            }
                            else
                            {
                                (item["reg_sgregistered"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_IDno"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_IDno"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_IDNo);
                                }
                            }
                            else
                            {
                                (item["reg_IDno"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Designation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Designation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                                }
                            }
                            else
                            {
                                (item["reg_Designation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Profession"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Profession"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Profession);
                                }
                            }
                            else
                            {
                                (item["reg_Profession"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Department)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Department"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Department"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Department);
                                }
                            }
                            else
                            {
                                (item["reg_Department"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Organization"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Organization"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Organization);
                                }
                            }
                            else
                            {
                                (item["reg_Organization"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Institution"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Institution"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Institution);
                                }
                            }
                            else
                            {
                                (item["reg_Institution"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address1"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address1"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address1);
                                }
                            }
                            else
                            {
                                (item["reg_Address1"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address2"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address2"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address2);
                                }
                            }
                            else
                            {
                                (item["reg_Address2"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address3"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address3"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address3);
                                }
                            }
                            else
                            {
                                (item["reg_Address3"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address4"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address4"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address4);
                                }
                            }
                            else
                            {
                                (item["reg_Address4"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _City)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_City"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_City"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_City);
                                }
                            }
                            else
                            {
                                (item["reg_City"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _State)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_State"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_State"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_State);
                                }
                            }
                            else
                            {
                                (item["reg_State"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_PostalCode"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_PostalCode"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PostalCode);
                                }
                            }
                            else
                            {
                                (item["reg_PostalCode"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Country)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Country"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Country"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Country);
                                }
                            }
                            else
                            {
                                (item["reg_Country"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_RCountry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_RCountry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_RCountry);
                                }
                            }
                            else
                            {
                                (item["reg_RCountry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Tel"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Tel"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Tel);
                                }
                            }
                            else
                            {
                                (item["reg_Tel"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Mobile"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Mobile"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Mobile);
                                }
                            }
                            else
                            {
                                (item["reg_Mobile"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Fax"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Fax"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fax);
                                }
                            }
                            else
                            {
                                (item["reg_Fax"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Email"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Email"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Email);
                                }
                            }
                            else
                            {
                                (item["reg_Email"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Affiliation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Affiliation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Affiliation);
                                }
                            }
                            else
                            {
                                (item["reg_Affiliation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Dietary"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Dietary"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Dietary);
                                }
                            }
                            else
                            {
                                (item["reg_Dietary"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Nationality"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Nationality"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Nationality);
                                }
                            }
                            else
                            {
                                (item["reg_Nationality"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Age)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Age"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Age"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Age);
                                }
                            }
                            else
                            {
                                (item["reg_Age"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_DOB"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_DOB"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_DOB);
                                }
                            }
                            else
                            {
                                (item["reg_DOB"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Gender"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Gender"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Gender);
                                }
                            }
                            else
                            {
                                (item["reg_Gender"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Membershipno"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Membershipno"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_MembershipNo);
                                }
                            }
                            else
                            {
                                (item["reg_Membershipno"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VName);
                                }
                            }
                            else
                            {
                                (item["reg_vName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vDOB"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vDOB"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VDOB);
                                }
                            }
                            else
                            {
                                (item["reg_vDOB"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vPassno"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vPassno"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassNo);
                                }
                            }
                            else
                            {
                                (item["reg_vPassno"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vPassexpiry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vPassexpiry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassExpiry);
                                }
                            }
                            else
                            {
                                (item["reg_vPassexpiry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vIssueDate"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vIssueDate"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassIssueDate);
                                }
                            }
                            else
                            {
                                (item["reg_vIssueDate"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vEmbarkation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vEmbarkation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VEmbarkation);
                                }
                            }
                            else
                            {
                                (item["reg_vEmbarkation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vArrivalDate"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vArrivalDate"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VArrivalDate);
                                }
                            }
                            else
                            {
                                (item["reg_vArrivalDate"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vCountry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vCountry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VCountry);
                                }
                            }
                            else
                            {
                                (item["reg_vCountry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CName);
                                }
                            }
                            else
                            {
                                (item["UDF_CName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_DelegateType"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_DelegateType"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_DelegateType);
                                }
                            }
                            else
                            {
                                (item["UDF_DelegateType"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_ProfCategory"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_ProfCategory"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_ProfCategory);
                                }
                            }
                            else
                            {
                                (item["UDF_ProfCategory"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CPcode"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CPcode"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CPcode);
                                }
                            }
                            else
                            {
                                (item["UDF_CPcode"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CLDepartment"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CLDepartment"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLDepartment);
                                }
                            }
                            else
                            {
                                (item["UDF_CLDepartment"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CAddress"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CAddress"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CAddress);
                                }
                            }
                            else
                            {
                                (item["UDF_CAddress"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CLCompany"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CLCompany"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLCompany);
                                }
                            }
                            else
                            {
                                (item["UDF_CLCompany"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CCountry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CCountry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CCountry);
                                }
                            }
                            else
                            {
                                (item["UDF_CCountry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupName);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorDesignation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorDesignation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupDesignation);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorDesignation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorContact"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorContact"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupContact);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorContact"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorEmail"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorEmail"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupEmail);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorEmail"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Additional4"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Additional4"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional4);
                                }
                            }
                            else
                            {
                                (item["reg_Additional4"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Additional5"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Additional5"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional5);
                                }
                            }
                            else
                            {
                                (item["reg_Additional5"].Controls[0] as LinkButton).Text = "";
                            }
                        }
                    }
                }
                #endregion
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    public void GetDetailReport(string qnaireID)
    {
        string logid = string.Empty;
        string item = string.Empty;
        StringBuilder str = new StringBuilder();
        string sqlQ = "select * from gen_Question where status='Active' and quest_class <> 'Header' and quest_id in (select top 500 quest_id from gen_QnaireQuest where qnaire_id='" + qnaireID + "' order by qnaire_seq)";
        DataTable dtQ = qfn.GetDatasetByCommand(sqlQ, "sdtQ").Tables[0];
        //Regno	Salutation	First Name	Last Name	Company	Job Title	Email	Mobile	Address	Country	Type	Category
        str.Append("<tr>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "No" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Registration ID" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Created Date" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Update Date" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Salutation" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "FirstName" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Last Name" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Job Title" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Department" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Name" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Address Line1" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Address Line2" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Postcode" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "City" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "State/Province" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Country" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Tel No Code" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Tel No" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Mobile Code" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Mobile No" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Fax Code" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Faxno" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Email" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Email Alternate" + "</td>");
        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Website" + "</td>");


        if (dtQ.Rows.Count > 0)
        {
            for (int q = 0; q < dtQ.Rows.Count; q++)
            {
                int span = GetQuestCount(dtQ.Rows[q]["quest_id"].ToString());
                str.Append("<td colspan='" + span + "' style='background-color:#b3b3b3'>" + dtQ.Rows[q]["quest_desc"].ToString() + "</td>");
            }

        }
        str.Append("</tr>");
        str.Append("<tr>");
        if (dtQ.Rows.Count > 0)
        {
            for (int q = 0; q < dtQ.Rows.Count; q++)
            {
                string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
                DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
                if (dtI.Rows.Count > 0)
                {
                    for (int i = 0; i < dtI.Rows.Count; i++)
                    {
                        str.Append("<td style='background-color:#b3b3b3'>" + dtI.Rows[i]["qitem_desc"].ToString() + "</td>");
                    }
                }

            }
        }
        str.Append("</tr>");
        string sqlU = "select * from tbl" + qnaireID + " order by user_reference";
        DataTable dtU = qfn.GetDatasetByCommand(sqlU, "sdtU").Tables[0];

        flowID = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
        groupID = cFun.DecryptValue(Request.QueryString["groupid"]);

        string sqlDelegateAll = "select * from  tb_RegDelegate  full outer join ref_Salutation on reg_Salutation=Sal_ID full outer join ref_country on reg_Country=Cty_GUID where reg_urlFlowID='" + flowID + "' and recycle='0' and RegGroupID='"+groupID+ "' and reg_Status='1' order by Regno";
        DataTable dtDelegateAll = fn.GetDatasetByCommand(sqlDelegateAll, "sdtDelegateAll").Tables[0];

        string sqlA1 = "select * from gen_QuestItem  where  status='Active' and quest_id in (select quest_id from gen_QnaireQuest where qnaire_id='" + qnaireID + "' and status='Active' and quest_id in(select quest_id from gen_Question where quest_class<>'Header'))";
        DataTable dtA1 = qfn.GetDatasetByCommand(sqlA1, "sdtA1").Tables[0];
        if (dtDelegateAll.Rows.Count > 0)
        {
            for (int da = 0; da < dtDelegateAll.Rows.Count; da++)
            {

                if (dtU.Rows.Count > 0)
                {
                    for (int u = 0; u < dtU.Rows.Count; u++)
                    {
                        string daregno = dtDelegateAll.Rows[da]["Regno"].ToString();
                        string uf = dtU.Rows[u]["user_reference"].ToString();
                        if (daregno == uf)
                        {

                            string sqlDelegate = "select * from  tb_RegDelegate as d full outer join ref_Salutation on d.reg_Salutation=Sal_ID full outer join ref_country on d.reg_Country=Cty_GUID full outer join tb_RegCompany as c on d.RegGroupID=c.RegGroupID where Regno in ('" + uf + "') ";
                            DataTable dtDelegate = fn.GetDatasetByCommand(sqlDelegate, "sdtDelegate").Tables[0];
                            if (dtDelegate.Rows.Count > 0)
                            {
                                for (int d = 0; d < dtDelegate.Rows.Count; d++)
                                {

                                    str.Append("<tr>");
                                    str.Append("<td>" + (da + 1).ToString() + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["Regno"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_datecreated"] + "</td>");
                                    str.Append("<td> </td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["Sal_Name"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_FName"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_LName"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Designation"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Department"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Name"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Address1"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Address2"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_PostalCode"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_City"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_State"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["Country"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Telcc"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Telac"] + "-" + dtDelegate.Rows[d]["reg_Tel"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Mobcc"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Mobac"] + "-" + dtDelegate.Rows[d]["reg_Mobile"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Faxcc"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Faxac"] + "-" + dtDelegate.Rows[d]["reg_Fax"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Email"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Additional5"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Website"] + "</td>");

                                }
                            }
                            string sqlResult = "SELECT * INTO #TempTable FROM tbl" + qnaireID + " where user_reference='" + uf + "' "
                                                   + "ALTER TABLE #TempTable "
                                                   + "DROP COLUMN qnaire_log_id,user_reference,status,create_date,update_date "
                                                   + "SELECT * FROM #TempTable";
                            DataTable dtResut = qfn.GetDatasetByCommand(sqlResult, "sqResult").Tables[0];
                            if (dtResut.Rows.Count > 0)
                            {
                                foreach (DataRow drR in dtResut.Rows)
                                {
                                    foreach (DataColumn dc in dtResut.Columns)
                                    {
                                        string hutt = drR[dc].ToString();
                                        if (hutt == "0")
                                        {
                                            str.Append("<td></td>");
                                        }
                                        else
                                        {
                                            str.Append("<td>" + hutt + "</td>");
                                        }
                                    }
                                }
                            }
                            str.Append("</tr>");
                        }

                    }


                }

            }

        }
      //  #endregion
        htmltb += str.ToString();



    }

    protected void lnkExcel_Clicked(object sender, EventArgs e)
    {
        try
        {
            flowID = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
            string QnaireID = GetQIDFromFLW(flowID);
            //ExportExcel(QnaireID);
            if (string.IsNullOrEmpty(QnaireID) || QnaireID == "0")
            {
                GKeyMaster.AllowPaging = false;
                GKeyMaster.Rebind();
                GKeyMaster.ExportSettings.ExportOnlyData = true;
                GKeyMaster.ExportSettings.IgnorePaging = true;
                GKeyMaster.ExportSettings.OpenInNewWindow = true;
                GKeyMaster.MasterTableView.ExportToExcel();
                //GKeyMaster.AllowPaging = true;
                //GKeyMaster.Rebind();
            }
            else
            {
                ExportExcel(QnaireID);
            }
        }
        catch (Exception ex) { }
    }

    public void ExportExcel(string QnaireID)
    {
        try
        {
            StringBuilder StrBuilder = new StringBuilder();

            htmltb = string.Empty;
            GetDetailReport(QnaireID);
            StrBuilder.Append("<table cellpadding='0' cellspacing='0' border='1' class='Messages' id='idTbl' runat='server'>");
            StrBuilder.Append(htmltb.ToString());
            StrBuilder.Append("</table>");
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/msexcel";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "filename=GroupMember-Questionnaire-Detailed-Report.xls");
            HttpContext.Current.Response.Write(StrBuilder);
            HttpContext.Current.Response.End();

            HttpContext.Current.Response.Flush();
        }
        catch (System.Threading.ThreadAbortException exf)
        {

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);

        }
    }



    public string GetQIDFromFLW(string flowID)
    {
        string result = string.Empty;

        string query = "select SQ_QAID from tb_site_QA_Master where SQ_FLW_ID='" + flowID + "' and SQ_Category='D'";
        result = fn.GetDataByCommand(query, "SQ_QAID");

        return result;
    }


    public int GetQuestCount(string questID)
    {
        int Qcount = 0;
        string sql = "select * from gen_QuestItem where status='Active' and quest_id='" + questID + "'";
        DataSet ds = new DataSet();
        ds = qfn.GetDatasetByCommand(sql, "dsds");

        Qcount = ds.Tables[0].Rows.Count;

        return Qcount;

    }


}