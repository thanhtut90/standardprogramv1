﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Flow_NoteTemplate.aspx.cs" Inherits="Admin_Event_Flow_NoteTemplate" %>
<%@ MasterType VirtualPath="~/Admin/Master.master" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="row" >
            <div class="col-lg-7 content-header">
                <h1>Manage Registration Steps</h1>
            </div>
        </div>
        <div id="contentwrapper" class="contentwrapper" style="padding-top: 20px;">
            <div>
                <%--<div class="box">
                    <div class="box-body">
                        <div class='page-header'>
                            <div class="form-horizontal">
                                <div class="box-body">--%>
                <div class="row">
                                    <div class="form-group">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Step</th>
                                                    <th>Message</th>
                                                    <th>isSkip</th>
                                                    <th>SortOrder</th>
                                                    <th>isAutoCheck</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater runat="server" ID="rptNotes">
                                                    <ItemTemplate>
                                                        <asp:Panel runat="server" ID="PanelRow">
                                                            <tr>
                                                                <td>
                                                                    <span class="fa-stack fa-lg">
                                                                        <i class="fa fa-bookmark fa-stack-1x"></i>
                                                                        <i class="fa fa-inverse fa-stack-1x"><%#Eval("note_FlowStep")%></i>
                                                                    </span>
                                                                </td>
                                                                <td><%#Eval("note_TemplateID")%></td>
                                                                <td>
                                                                    <%#Eval("note_isSkip")%>
                                                                </td>
                                                                <td>
                                                                    <%#Eval("note_SortOrder")%>
                                                                </td>
                                                                <td>
                                                                    <%#Eval("note_isAutoCheck")%>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="btnEditNote" runat="server" CommandArgument='<%#Eval("note_ID")%>'
                                                                        CommandName="editNote"
                                                                        CssClass="btn btn-warning"
                                                                        OnClick="btnEditNote_Click">
                                                                        <span aria-hidden="true" class="glyphicon glyphicon-edit"></span> Edit
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%#Eval("note_ID")%>'
                                                                        CommandName="deleteNote"
                                                                        CssClass="btn btn-warning"
                                                                        OnClick="btnDelete_Click">
                                                                        <span aria-hidden="true" class="glyphicon glyphicon-edit"></span> Delete
                                                                    </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </asp:Panel>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lblNoteType" runat="server" CssClass="col-md-2 control-label">Note Type</asp:Label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ddlNoteType" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                </div>
                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="lblSortOrder" runat="server" CssClass="col-md-2 control-label">Sort Order</asp:Label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtSortOrder" runat="server" CssClass="form-control"></asp:TextBox>
                                            <%--<asp:FilteredTextBoxExtender ID="ftbeSortOrder" runat="server" TargetControlID="txtSortOrder" FilterType="Numbers" />--%>
                                        </div>
                                    </div>
                </div>
                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Can skip?</label>
                                        <div class="col-md-8">
                                            <asp:CheckBox ID="chkIsSkip" runat="server" Text="Yes" />
                                        </div>
                                    </div>
                </div>
                <div class="row">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Auto Check?</label>
                                        <div class="col-md-8">
                                            <asp:CheckBox ID="chkIsAutoCheck" runat="server" Text="Yes" />
                                        </div>
                                    </div>
                </div>
                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-2 control-label">Message</asp:Label>
                                        <div class="col-md-8">
                                            <CKEditor:CKEditorControl ID="txtMsg" runat="server" Height="300px" Width="98%"
                                                BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                        </div>
                                    </div>
                </div>
                <div class="row">
                                    <div class="form-group">
                                        <asp:LinkButton ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" CssClass="btn btn-success btn-lg">
                                            <span aria-hidden="true" class="glyphicon glyphicon-edit"></span> Submit
                                        </asp:LinkButton>
                                    </div>
                </div>
                                <%--</div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
                <!-- Default box -->--%>
            </div>
            <div style="padding-left: 30%;">
                <asp:TextBox runat="server" ID="txtStepIndex" Visible="false"></asp:TextBox>
                <asp:TextBox runat="server" ID="txtStep" Visible="false"></asp:TextBox>
                <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
                <asp:TextBox runat="server" ID="txtFlowID" Visible="false" Text=""></asp:TextBox>
                <asp:TextBox runat="server" ID="txtMsgTmpID" Visible="false" Text=""></asp:TextBox>
                <asp:TextBox runat="server" ID="txtNoteID" Visible="false" Text=""></asp:TextBox>
                <%--<telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>--%>
            </div>
        </div>
    </div>
</asp:Content>

