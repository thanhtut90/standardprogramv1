﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using NPOI.HSSF.UserModel;
using Corpit.Utilities;
public partial class Admin_VendorSPF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Session["DbList"] = null;

            if (Session["userid"] != null)
            {
                string userID = Session["userid"].ToString();

                txtShow.Text = getShowID(userID);

            }
            else
            {

            }
        }

    }

    protected void btnShowDownloadList_OnClick(object sender, EventArgs e)
    {
        Functionality vFun = new Functionality();

        string sql = "";
        sql = @"select regno SN ,row_number() OVER (ORDER BY regno) OrderID, reg_PassNo Passport,reg_FName FullName,reg_OName PreferName,
(select Profession from ref_Profession where ShowID=@ShowID and ID=reg_Profession) Nationality,
(select Organisation from ref_Organisation  where ShowID=@ShowID and ID=reg_Organization) ContryOfBirth, CONVERT(VARCHAR(10),reg_DOB, 112) DOB,reg_Gender Gender,reg_Address1 Religion,
reg_Address2 Organisation,reg_Address3 Department,reg_Designation Designation,reg_Address4 DivisionStatus,reg_City Remark
 from tb_RegDelegate where ShowID= @ShowID
and recycle = 0 ";
        if (!chkInclude.Checked)
        {
            sql += " and regno  not in (select SN from tb_SPFDownload) ";
        }

        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("ShowID", SqlDbType.NVarChar);
        spar.Value = txtShow.Text;
        pList.Add(spar);
        DataTable dtList = vFun.GetDatasetByCommand(sql, "ds", pList).Tables[0];
        if (dtList.Rows.Count > 0)
        {

            gvList.DataSource = dtList;
            gvList.DataBind();
            PanelShowList.Visible = true;
            PanelStep1.Visible = false;
            Session["DbList"] = dtList;
        }
        else
            lblMsg.Visible = true;
    }

    protected void btnDownload_Onclick(object sender, EventArgs e)
    {

        if (Session["DbList"] != null)
        {
            string templateFile = Server.MapPath(@"~\Template\\Background Screening template.xls");
            if (templateFile.EndsWith(".xls"))
            {
                DataTable dList = (DataTable)Session["DbList"];
                AddSPFDownloadLog(dList);
                bool isOK = Download(templateFile, dList);

            }
        }

    }

    #region Download
    public bool Download(string filename, DataTable dList)
    {
        bool isOK = false;
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

        HSSFWorkbook templateWorkbook = new HSSFWorkbook(fs, true);

        HSSFSheet sheet = templateWorkbook.GetSheet("Screening Request");
        DateTime dtime = DateTime.Now;
        //  sheet.GetRow(0).GetCell(3).SetCellValue(projectNo);
        int startRow = 11;
        String cellText = "";
        int dob = 0;
        CommonFuns cFun = new CommonFuns();
        for (int i = 0; i < dList.Rows.Count; i++)
        {
            for (int j = 1; j < dList.Columns.Count; j++) // Without Include Regno
            {
                cellText = dList.Rows[i][j].ToString();
                if (dList.Columns[j].ColumnName == "DOB")
                {
                    dob = cFun.ParseInt(cellText);
                    sheet.GetRow(startRow).GetCell(j - 1).SetCellValue(dob);
                }
                else
                    sheet.GetRow(startRow).GetCell(j - 1).SetCellValue(cellText);

            }
            startRow = startRow + 1;
        }
        sheet.ForceFormulaRecalculation = true;


        MemoryStream ms = new MemoryStream();

        templateWorkbook.Write(ms);
        string fileName = "Background Screening template" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
        ExportDataTableToExcel(ms, fileName);
        isOK = true;

        return isOK;
    }
    public static void ExportDataTableToExcel(MemoryStream memoryStream, string fileName)
    {
        HttpResponse response = HttpContext.Current.Response;
        response.ContentType = "application/vnd.ms-excel";
        response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
        response.Clear();

        response.BinaryWrite(memoryStream.GetBuffer());
        response.End();
    }

    #endregion

    #region SPFLog
    private void AddSPFDownloadLog(DataTable dList)
    {
        try
        {
            Functionality fn = new Functionality();
            string connectionString = fn.ConnString;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    //    foreach (DataColumn c in myDataTable.Columns)
                    //        bulkCopy.ColumnMappings.Add(c.ColumnName, c.ColumnName);
                    bulkCopy.ColumnMappings.Add(dList.Columns["SN"].ColumnName, dList.Columns["SN"].ColumnName);
                    bulkCopy.ColumnMappings.Add(dList.Columns["Passport"].ColumnName, dList.Columns["Passport"].ColumnName);
                    bulkCopy.DestinationTableName = "tb_SPFDownload";
                    try
                    {
                        bulkCopy.WriteToServer(dList);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }
        catch { }
    }
    #endregion



    protected string getShowID(string userID)
    {
        string showID = "";
        try
        {
            Functionality fn = new Functionality();
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);

        }

        return showID;
    }

}