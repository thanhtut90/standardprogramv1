﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Promo_Table.aspx.cs" Inherits="Admin_Promo_Table" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div id="contentwrapper" class="contentwrapper">
            <div class="contenttitle2">
                <h3><asp:Label ID="lblTitle" runat="server" Text="Promo Code Table"></asp:Label></h3>
            </div><!--contenttitle-->
                
            <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="false" CssClass="stdtable">
                <HeaderStyle CssClass="theadstyle" />
                <Columns>
                    <asp:TemplateField HeaderText="No">
                        <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Promo Name">
                        <ItemTemplate>
                            <%# Eval("Promo_Name")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="No of Code">
                        <ItemTemplate>
                            <%# Eval("Promo_Count")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="View">
                        <ItemTemplate>
                            <a href='Promo_List.aspx?promoid=<%# Eval("Promo_Id") %>'>View</a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>

