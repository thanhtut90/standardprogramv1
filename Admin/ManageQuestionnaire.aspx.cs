﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_ManageQuestionnaire : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["TYPE"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
                
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        FlowControler fControl = new FlowControler(fn);

        if (Request.Params["a"] != null)
        {
            string admintype = cFun.DecryptValue(Request.QueryString["a"].ToString());
            if (admintype == BackendStaticValueClass.isFlowEdit)
            {
                string link = fControl.MakeFullURL("QuestionnaireAdd", Request.Params["FLW"], Request.Params["SHW"], null, Request.Params["STP"], null, Request.Params["type"]);
                link += "&a=" + cFun.EncryptValue(admintype);
                Response.Redirect(link);
                //Response.Redirect("QuestionnaireAdd?SHW="+ Request.QueryString["SHW"].ToString() + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["type"]);
            }
            else
            {
                string link = fControl.MakeFullURL("QuestionnaireAdd", Request.Params["FLW"], Request.Params["SHW"], null, Request.Params["STP"], null, Request.Params["type"]);
                Response.Redirect(link);
                //Response.Redirect("QuestionnaireAdd?SHW="+ Request.QueryString["SHW"].ToString() + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["type"]);
            }
        }
        else
        {
            string link = fControl.MakeFullURL("QuestionnaireAdd", Request.Params["FLW"], Request.Params["SHW"], null, Request.Params["STP"], null, Request.Params["type"]);
            Response.Redirect(link);
            //Response.Redirect("QuestionnaireAdd?SHW="+ Request.QueryString["SHW"].ToString() + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["type"]);
        }
    }

    protected void NextForm(object sender, EventArgs e)
    {
        string sql = string.Empty;
        string selectedQAID = string.Empty;

        string flowID = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
        string type = Request.QueryString["TYPE"].ToString();

        foreach (GridDataItem item in GKeyMaster2.MasterTableView.Items)
        {
            if (item.Selected) {
                selectedQAID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["SQ_QAID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["SQ_QAID"].ToString() : "";                
            }
        }

        if (string.IsNullOrEmpty(selectedQAID))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select a questionnaire!');", true);
            return;
        }
        else 
        {
            sql = string.Format("Select * from tb_site_QA_Master where SQ_QAID = @SQ_QAID");
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SQ_QAID", SqlDbType.NVarChar);
            spar.Value = selectedQAID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
            
            sql = string.Format("Select * from tb_site_QA_Master where SQ_FLW_ID = @SQ_FLW_ID and SQ_Category=@SQ_Category");
            pList.Clear();
            spar = new SqlParameter("SQ_FLW_ID", SqlDbType.NVarChar);
            spar.Value = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
            pList.Add(spar);
            spar = new SqlParameter("SQ_Category", SqlDbType.NVarChar);
            spar.Value = cFun.DecryptValue(Request.QueryString["TYPE"].ToString());
            pList.Add(spar);
            DataTable dt2 = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];

            if (dt2.Rows.Count == 0)
            {
                sql = "Insert into tb_site_QA_Master (SQ_ID,SQ_SHWID,SQ_FLW_ID,SQ_FLOW_SETP_REF_ID,SQ_QAID,SQ_QATitle,SQ_Desc,SQ_Category,CreateDate) Values(@SQID,@SQ_SHWID,@SQ_FLW_ID,@SQ_FLOW_SETP_REF_ID,@SQ_QAID,@SQ_QATitle,@SQ_Desc,@SQ_Category,@CreateDate)";

                using (SqlConnection con = new SqlConnection(fn.ConnString))
                {
                    using (SqlCommand command = new SqlCommand(sql))
                    {
                        command.Connection = con;
                        command.Parameters.AddWithValue("@SQID", selectedQAID);
                        command.Parameters.AddWithValue("@SQ_SHWID", dt.Rows[0]["SQ_SHWID"].ToString());
                        command.Parameters.AddWithValue("@SQ_FLW_ID", flowID);
                        command.Parameters.AddWithValue("@SQ_FLOW_SETP_REF_ID", "");
                        command.Parameters.AddWithValue("@SQ_QAID", selectedQAID);
                        command.Parameters.AddWithValue("@SQ_QATitle", dt.Rows[0]["SQ_QATitle"].ToString());
                        command.Parameters.AddWithValue("@SQ_Desc", dt.Rows[0]["SQ_Desc"].ToString());
                        command.Parameters.AddWithValue("@SQ_Category", type);
                        command.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture));

                        con.Open();
                        command.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            else
            {
                sql = "Update tb_site_QA_Master SET SQ_QAID=@SQ_QAID,SQ_QATitle=@SQ_QATitle,SQ_Desc=@SQ_Desc where SQ_FLW_ID=@SQ_FLW_ID and SQ_Category=@SQ_Category";
                using (SqlConnection con = new SqlConnection(fn.ConnString))
                {
                    using (SqlCommand command = new SqlCommand(sql))
                    {
                        command.Connection = con;
                        command.Parameters.AddWithValue("@SQ_QAID", selectedQAID);
                        command.Parameters.AddWithValue("@SQ_QATitle", dt.Rows[0]["SQ_QATitle"].ToString());
                        command.Parameters.AddWithValue("@SQ_Desc", dt.Rows[0]["SQ_Desc"].ToString());
                        command.Parameters.AddWithValue("@SQ_Category", type);
                        command.Parameters.AddWithValue("@SQ_FLW_ID", flowID);

                        con.Open();
                        command.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }

            Dictionary<string, string> nValues = new Dictionary<string, string>();
            FlowControler fControl = new FlowControler(fn);
            string stp = cFun.DecryptValue(Request.QueryString["STP"].ToString());
            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            nValues = fControl.GetAdminNextRoute(flowid, stp);

            if (nValues.Count > 0)
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string curstep = cFun.DecryptValue(urlQuery.CurrIndex);
                string path = string.Empty; 
                FlowControler fCtrl = new FlowControler(fn);
                Flow flw = new Flow();
                flw.FlowID = flowid;
                flw.FlowConfigStatus = YesOrNo.Yes;
                flw.FlowStep = curstep;
                fCtrl.UpdateFlowIndivConfigStatus(flw);
                if (Request.Params["a"] != null)
                {
                    string admintype = cFun.DecryptValue(Request.QueryString["a"].ToString());
                    if (admintype == BackendStaticValueClass.isFlowEdit)
                    {
                        string page = "Event_Flow_Dashboard.aspx";

                        path = fControl.MakeFullURL(page, flowid, showid);
                    }
                    else
                    {
                        string page = nValues["nURL"].ToString();
                        string step = nValues["nStep"].ToString();
                        string FlowID = nValues["FlowID"].ToString();

                        if (page == "") page = "Event_Config_Final";

                        path = fControl.MakeFullURL(page, FlowID, showid, "", step);
                    }
                }
                else
                {
                    string page = nValues["nURL"].ToString();
                    string step = nValues["nStep"].ToString();
                    string FlowID = nValues["FlowID"].ToString();

                    if (page == "") page = "Event_Config_Final";


                    path = fControl.MakeFullURL(page, FlowID, showid, "", step);
                }

                Response.Redirect(path);
            }
        }

       
    }

    #region GKeyMaster2_NeedDataSource (Binding data into RadGrid & get DataSource From tb_site_flow_master table for RadGrid)
    protected void GKeyMaster2_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        string showID = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

        string query = "Select DISTINCT SQ_QAID, SQ_QATitle from tb_site_QA_Master where SQ_SHWID = @SHWID GROUP BY SQ_QAID,SQ_QATitle";

        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showID;
        pList.Add(spar);

        DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        GKeyMaster2.DataSource = dt;
    }
    #endregion

    #region GKeyMaster2_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster2_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string qaID = "";
        foreach (GridDataItem item in GKeyMaster2.SelectedItems)
        {
            qaID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["SQ_QAID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["SQ_QAID"].ToString() : "";
        }

        if(e.CommandName == "EditQA")
        {
            GridDataItem item = (GridDataItem)e.Item;
            qaID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["SQ_QAID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["SQ_QAID"].ToString() : "";

            if (!string.IsNullOrEmpty(qaID))
            {
                if (Request.Params["a"] != null)
                {
                    string admintype = cFun.DecryptValue(Request.QueryString["a"].ToString());
                    if (admintype == BackendStaticValueClass.isFlowEdit)
                    {
                        Response.Redirect("QuestionnaireAdd?SHW=" + Request.QueryString["SHW"].ToString() + "&QAID=" + cFun.EncryptValue(qaID) + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&type=" + Request.Params["type"] + "&a=" + cFun.EncryptValue(admintype));
                    }
                    else
                    {
                        Response.Redirect("QuestionnaireAdd?SHW=" + Request.QueryString["SHW"].ToString() + "&QAID=" + cFun.EncryptValue(qaID) + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&type=" + Request.Params["type"]);
                    }
                }
                else
                {
                    Response.Redirect("QuestionnaireAdd?SHW=" + Request.QueryString["SHW"].ToString() + "&QAID=" + cFun.EncryptValue(qaID) + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&type=" + Request.Params["type"]);
                }
            }
        }
        /*string adminID = "";
        foreach (GridDataItem item in GKeyMaster2.SelectedItems)
        {
            adminID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"].ToString() : "";
        }
        if (e.CommandName == "RemoveAdmin")
        {
            GridDataItem item = (GridDataItem)e.Item;

            adminID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["admin_id"].ToString() : "";

            if (!string.IsNullOrEmpty(adminID))
            {
                string query = string.Empty;
                string shows = string.Empty;

                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("us_userid", SqlDbType.Int);
                spar.Value = Convert.ToInt32(adminID);
                pList.Add(spar);
                query = "Select us_showid From tb_Admin_Show Where us_userid=@us_userid";
                shows = fn.GetDatasetByCommand(query, "adminshow_value", pList).Tables[0].Rows[0]["us_showid"].ToString();

                string[] showList = shows.Split(',');
                showList = showList.Except(new string[] { txtShowID.Text }).ToArray();
                shows = string.Join(",", showList);

                query = "Update tb_Admin_Show Set us_showid='" + shows + "' where us_userid='" + adminID + "'";
                fn.ExecuteSQL(query);

                bindAdminList();
                GKeyMaster2.Rebind();
            }
        }*/
    }
    #endregion
}