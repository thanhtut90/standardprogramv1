﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Config_Final.aspx.cs" Inherits="Admin_Event_Config_Final" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" href="css/style.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="row" style="line-height:30px; text-align:center;">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-box">
            <div class="f1-steps">
                <div class="f1-progress">
                    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 100%;"></div>
                </div>
                <div class="f1-step activated">
                    <div class="f1-step-icon"><i class="fa fa-plus"></i></div>
                    <p style="text-align:center">Create Event</p>
                </div>
                <div class="f1-step activated">
                    <div class="f1-step-icon"><i class="fa fa-gear"></i></div>
                    <p style="text-align:center">Event Configuration</p>
                </div>
                <div class="f1-step activated">
                    <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                    <p style="text-align:center">Registration SetUp</p>
                </div>
                <div class="f1-step active">
                    <div class="f1-step-icon"><i class="fa fa-check-circle-o"></i></div>
                    <p style="text-align:center">Finished</p>
                </div>
            </div>
        </div>
    </div>

    <h1>Success!!!!</h1>

    <div style="font-weight:bold;padding-top:40px;">
        Registration Link <asp:HyperLink Text="Click here"  runat="server" ID="hplLink" Target="_blank"></asp:HyperLink>
    </div>
</asp:Content>

