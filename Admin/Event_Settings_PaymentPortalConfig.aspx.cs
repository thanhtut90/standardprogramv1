﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Utilities;
using System.Data;
using System.Data.SqlClient;
public partial class Admin_Event_Setttings_PaymentPortalConfig : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                LoadPaymentPortalList();
            }
            else
            {
                Response.Redirect("Login.aspx");
            }


        }
    }

    #region ServerFunz

    protected void Back(object sender, EventArgs e)
    {
        string url = string.Format("Event_Settings_PaymentSettings?SHW={0}", Request.Params["SHW"].ToString());
        Response.Redirect(url);
    }

    protected void SaveForm(object sender, EventArgs e)
    {
        if (IsValid() && Request.Params["SHW"] != null)
        {
            bool isOK = SavePaymentPortalData();
            if (isOK)
            {
                CommonFuns cFun = new CommonFuns();
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                SaveConfigTable(showid);
            }

        }
    }

    #endregion

    #region HelperFuz
    private void LoadPaymentPortalList()
    {

        string query = "select* from Ref_Collection where Type = 'PayPortal'";

        DataTable dtList = fn.GetDatasetByCommand(query, "DT").Tables["DT"];
        for (int x = 0; x < dtList.Rows.Count; x++)
        {
            ddlPaymentPotal.Items.Add(dtList.Rows[x]["RefDesc"].ToString());
            ddlPaymentPotal.Items[x].Value = dtList.Rows[x]["RefID"].ToString();
        }
    }
    #endregion

    #region Validata
    private bool IsValid()
    {
        bool isOK = false;
        try
        {
            PaymentFunctionality pFn = new PaymentFunctionality();
            string sql = "select 1 from tb_PaymentConfig where PROJID=@PROJ";


            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("PROJ", SqlDbType.NVarChar);
            spar.Value = txtProjectID.Text.ToString().Trim();
            pList.Add(spar);
            DataTable dtList = pFn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];
            if (dtList.Rows.Count == 0)

            {
                isOK = true;
                lblProjMsg.Visible = false;
            }
            else
            {
                lblProjMsg.Visible = true;
            }
        }
        catch (Exception ex)
        {

            isOK = false;
        }
        return isOK;

    }
    #endregion

    #region Save

    private bool SavePaymentPortalData()

    {
        bool isOK = false;
        try
        {
            PaymentFunctionality pFn = new PaymentFunctionality();
            string sql = "Insert into tb_PaymentConfig (PROJID,MERCHANT_ACC_NO,TXN_PASSWORD,TXN_CURRENCY,RTN_URL,TXN_SECRETKEY,STR_URL,PORTAL_TYPE ) Values (";
            sql += string.Format("'{0}',", txtProjectID.Text.Trim());
            sql += string.Format("'{0}',", txtMarchentACC.Text.Trim());
            sql += string.Format("'{0}',", txtMPassord.Text.Trim());
            sql += string.Format("'{0}',", txtCurrency.Text.Trim());
            sql += string.Format("'{0}',", txtReturnURL.Text.Trim());
            sql += string.Format("'{0}',", txtSecretKey.Text.Trim());
            sql += string.Format("'{0}',", txtPaymentPortalURL.Text.Trim());
            sql += string.Format("'{0}')", ddlPaymentPotal.SelectedValue.Trim());
            pFn.ExecuteSQL(sql);
            isOK = true;
        }
        catch (Exception)
        {

            throw;
        }

        return isOK;
    }

    private bool SaveConfigTable(string showID)
    {
        bool isOK = false;
        try
        {
            Functionality pFn = new Functionality();
            string sql = "Insert into tb_site_PaymentConfig (ShowID,PaymentID,PaymentDesc,Currency) Values (";
            sql += string.Format("'{0}',", showID.Trim());
            sql += string.Format("'{0}',", txtProjectID.Text.Trim());
            sql += string.Format("'{0}',", txtDesc.Text.Trim()); 
            sql += string.Format("'{0}')", txtCurrency.Text.Trim()); 
            pFn.ExecuteSQL(sql);
            isOK = true;
        }
        catch (Exception)
        {

            throw;
        }

        return isOK;
    }
    #endregion
}