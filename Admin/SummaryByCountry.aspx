﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="SummaryByCountry.aspx.cs" Inherits="Admin_SummaryByCountry" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }
    </script>
    <style type="text/css">
        .tdstyle1
        {
            width:170px;
        }

        .tdstyle2
        {
            width:300px;
        }

        form input[type="text"]
        {
            width:39% !important;
        }

        .ajax__calendar_container
        {
            width: 320px !important;
            height:280px !important;
        }

        .ajax__calendar_body
        {
            width: 100% !important;
            height:100% !important;
        }

        td
        {
            vertical-align:middle;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <h3 class="box-title">Summary (By Country)</h3>
            <asp:Panel ID="showlist" runat="server">
                Show List : <asp:DropDownList ID="ddl_showList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_showList_SelectedIndexChanged"></asp:DropDownList>
            </asp:Panel>
            <div class="table-responsive">
                <asp:Button ID="Button1" runat="server" Text="Export to Excel Group Report By Country" OnClick="genCountryGExcel" />
                <asp:GridView ID="gvSummaryByCountryG" runat="server" DataKeyNames="Country" AutoGenerateColumns="true" CssClass="table" AllowPaging="false"
                    GridLines="None">
                    <HeaderStyle CssClass="theadstyle" />
                </asp:GridView>
                <br />
                <br />
                <asp:Button ID="Button2" runat="server" Text="Export to Excel Individual Report By Country" OnClick="genCountryIndivExcel" />
                <asp:GridView ID="gvSummaryByCountry" runat="server" DataKeyNames="Country" AutoGenerateColumns="true" CssClass="table" AllowPaging="false"
                    GridLines="None">
                    <HeaderStyle CssClass="theadstyle" />
                </asp:GridView>
                <br />
                <br />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Button1" />
            <asp:PostBackTrigger ControlID="Button2" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

