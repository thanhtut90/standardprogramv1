﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Corpit.Utilities;
using Corpit.Site.Utilities;
using System.Data.SqlClient;

public partial class Admin_Event_Conference_Items : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    CategoryClass catClass = new CategoryClass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["CAT"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                string Cateogory = cFun.DecryptValue(Request.QueryString["CAT"].ToString());
                txtCategory.Text = Cateogory;
                // bindConfConfig(flowid, showid);
                bindGroup(showid, "", flowid);
                string filter = " And (reg_categoryId=" + txtCategory.Text + ")";
                bindlist(filter, showid);

                if (rptItem.Items.Count == 0)
                {
                    btnAddNewConfItem.Visible = false;
                    ResetControls();
                    divConfInsertUpdate.Visible = true;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region Listing
    #region rptcommand (Delete)
    protected void rptcommand(object sender, RepeaterCommandEventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            string itemid = e.CommandArgument.ToString();

            if (e.CommandName == "delete")
            {
                string sql = "Update tb_ConfItem Set isdeleted = 1 Where con_itemId=" + itemid + " And ShowID='" + showid + "'";
                fn.ExecuteSQL(sql);
                string filter = " And (reg_categoryId=" + txtCategory.Text + ")";
                bindlist(filter, showid);
            }

            if (e.CommandName == "edit")
            {
                bindData(itemid, showid);
            }

            if (e.CommandName == "addsubitem")
            {
                if (Request.QueryString["FLW"] != null && Request.QueryString["SHW"] != null && Request.QueryString["STP"] != null && Request.QueryString["CAT"] != null)
                {
                    string paraA = "";
                    if(Request.Params["a"] != null) paraA="&a="+Request.Params["a"].ToString();
                        string url = string.Format("Dyn_ItemList.aspx?FLW={0}&SHW={1}&STP={2}&CAT={3}&ITM={4}{5}",
                        Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString(), Request.QueryString["CAT"].ToString(), cFun.EncryptValue(itemid),paraA);
                    Response.Redirect(url);
                }
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region setName
    protected string setName(string groupitemid)
    {
        string name = fn.GetDataByCommand("Select GI_Name From tb_Conf_GroupItem Where (isDeleted=0 Or isDeleted Is Null) And GI_GUID=" + groupitemid, "GI_Name");

        return name;
    }
    #endregion

    #region setGroupName
    protected string setGroupName(string groupid)
    {
        string groupname = fn.GetDataByCommand("Select con_GroupName From tb_Conf_Group Where (isDeleted=0 Or isDeleted Is Null) And con_GroupId=" + groupid, "con_GroupName");

        return groupname;
    }
    #endregion

    #region setCategory
    protected string setCategory(string catid)
    {
        string categoryname = string.Empty;

        if (catid != "0")
        {

            categoryname = fn.GetDataByCommand("Select reg_CategoryName From ref_reg_Category Where reg_CategoryId=" + catid, "reg_CategoryName");
        }
        else
        {
            categoryname = "All";
        }
        return categoryname;
    }
    #endregion

    #region bindlist
    protected void bindlist(string filter, string showid)
    {
        string query = "Select * From tb_ConfItem Where isdeleted=0" + filter + " And ShowID=@SHWID Order By con_OrderSeq Asc";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand(query, "ds", pList);
        if (ds.Tables[0].Rows.Count > 0)
        {
            rptItem.DataSource = ds;
            rptItem.DataBind();
            pnllist.Visible = true;
        }
        else
        {
            pnllist.Visible = false;
        }
    }
    #endregion

    #region setImgVisibility
    protected bool setImgVisibility(string imgPath)
    {
        bool isVisible = false;
        if (!String.IsNullOrEmpty(imgPath))
        {
            string already_File = Server.MapPath(imgPath);
            if (File.Exists(already_File))
            {
                isVisible = true;
            }
        }

        return isVisible;
    }
    #endregion

    #region setIsShowValue
    protected string setIsShowValue(string status)
    {
        return status == "1" ? "Yes" : "No";
    }
    #endregion

    #region btnAddNewConfItem_Click
    protected void btnAddNewConfItem_Click(object sender, EventArgs e)
    {
        ResetControls();
        divConfInsertUpdate.Visible = true;
        btnAddNewConfItem.Visible = false;
        pnllist.Visible = false;

        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["CAT"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
            string Cateogory = cFun.DecryptValue(Request.QueryString["CAT"].ToString());
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("SHWID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("reg_categoryId", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = Cateogory;
            pList.Add(spar1);
            pList.Add(spar2);
            string queryDep = "Select * From tb_ConfItem Where reg_categoryId=@reg_categoryId And ShowID=@SHWID And isDeleted=0";
            DataSet dsDep = fn.GetDatasetByCommand(queryDep, "ds", pList);
            if (dsDep.Tables[0].Rows.Count > 0)
            {
                chkDisabledItems.DataSource = dsDep;
                chkDisabledItems.DataTextField = "con_ItemName";
                chkDisabledItems.DataValueField = "con_itemId";
                chkDisabledItems.DataBind();
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
    #endregion

    #region Save/Update Conference Item
    #region bindData
    protected void bindData(string itemid, string showid)
    {
        string query = "Select * From tb_ConfItem Where con_itemId=" + itemid + " And ShowID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dt = new DataTable();
        dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        if (dt.Rows.Count > 0)
        {
            divConfInsertUpdate.Visible = true;

            hfConfID.Value = itemid;
            string categoryid = dt.Rows[0]["reg_categoryId"].ToString();
            string groupid = dt.Rows[0]["con_GroupId"].ToString();
            string gitemid = dt.Rows[0]["con_GroupItemID"].ToString();
            string name = dt.Rows[0]["con_ItemName"].ToString();
            string Desc = dt.Rows[0]["con_ItemDesc"].ToString();
            string imgname = dt.Rows[0]["con_ItemImage"].ToString();
            string isearlybird = dt.Rows[0]["con_isEarlyBird"].ToString();
            string ebprice = dt.Rows[0]["con_EBPrice"].ToString();
            string rprice = dt.Rows[0]["con_RegPrice"].ToString();
            string maxreg = dt.Rows[0]["con_MaxReg"].ToString();
            string message = dt.Rows[0]["con_MaxMessage"].ToString();
            string isShow = dt.Rows[0]["con_IsShow"].ToString();
            string sortorder = dt.Rows[0]["con_OrderSeq"].ToString();
            string disabledItems = dt.Rows[0]["disabledItems"] != DBNull.Value ? dt.Rows[0]["disabledItems"].ToString() : "";
            string Desc2AboveName = dt.Rows[0]["con_ItemDesc2_AboveName"].ToString();
            string con_ItemDesc_ShowedInTemplate = dt.Rows[0]["con_ItemDesc_ShowedInTemplate"] != DBNull.Value ? dt.Rows[0]["con_ItemDesc_ShowedInTemplate"].ToString() : "";
            string ReportDisplayName = dt.Rows[0]["ReportDisplayName"] != DBNull.Value ? dt.Rows[0]["ReportDisplayName"].ToString() : "";

            try
            {
                if (!String.IsNullOrEmpty(categoryid))
                {
                    txtCategory.Text = categoryid;
                }
            }
            catch (Exception ex)
            {
            }

            ////*add by th ((29 - 3 - 2018)
            try
            {
                if (!String.IsNullOrEmpty(groupid))
                {
                    ListItem listItem = ddlGroup.Items.FindByValue(groupid);
                    if (listItem != null)
                    {
                        ddlGroup.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            ddlGroup_SelectedIndexChanged(this, null);
            ////*add by th ((29 - 3 - 2018)

            //try
            //{
            //    if (!String.IsNullOrEmpty(gitemid))
            //    {
            //        ListItem listItem = ddlGroupItem.Items.FindByValue(gitemid);
            //        if (listItem != null)
            //        {
            //            ddlGroupItem.ClearSelection();
            //            listItem.Selected = true;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //}

            txtName.Text = name;
            txtDesc.Text = Desc;
            txtDescShowInEmailTemplate.Text = Server.HtmlDecode(con_ItemDesc_ShowedInTemplate);
            txtDescShowInReportItemBreakdown.Text = ReportDisplayName;
            txtDesc2.Text = Desc2AboveName;
            if (!string.IsNullOrEmpty(imgname))
            {
                string already_File = Server.MapPath(imgname);
                if (File.Exists(already_File))
                {
                    imgPhoto.Visible = true;
                    imgPhoto.ImageUrl = imgname;
                }
                else
                {
                    imgPhoto.Visible = false;
                    imgPhoto.ImageUrl = "";
                }
            }
            if (isearlybird == "1")
            {
                chkIsEarlyBird.Checked = true;
            }
            else
            {
                chkIsEarlyBird.Checked = false;
            }

            if (isShow == "1")
            {
                chkIsShow.Checked = true;
            }
            else
            {
                chkIsShow.Checked = false;
            }

            txtEPrice.Text = ebprice;
            txtRPrice.Text = rprice;
            txtMax.Text = !string.IsNullOrEmpty(maxreg) ? maxreg : "0";
            txtMessage.Text = Server.HtmlDecode(message);
            txtSequence.Text = sortorder;


            SqlParameter spar2 = new SqlParameter("reg_categoryId", SqlDbType.NVarChar);
            spar2.Value = categoryid;
            pList.Add(spar2);
            string queryDep = "Select * From tb_ConfItem Where reg_categoryId=@reg_categoryId And ShowID=@SHWID And isDeleted=0"
                                        + " And con_itemId<>" + itemid;
            DataSet dsDep = fn.GetDatasetByCommand(queryDep, "ds", pList);
            if (dsDep.Tables[0].Rows.Count > 0)
            {
                chkDisabledItems.DataSource = dsDep;
                chkDisabledItems.DataTextField = "con_ItemName";
                chkDisabledItems.DataValueField = "con_itemId";
                chkDisabledItems.DataBind();

                try
                {
                    if (!String.IsNullOrEmpty(disabledItems))
                    {
                        string[] strDisabledItems = disabledItems.Split(',');
                        if (strDisabledItems.Length > 0)
                        {
                            chkDisabledItems.ClearSelection();
                            foreach (var item in strDisabledItems)
                            {
                                ListItem listItem = chkDisabledItems.Items.FindByValue(item);
                                if (listItem != null)
                                {
                                    listItem.Selected = true;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }
    }
    #endregion


    #region Category (Comment) (Add, Save, Cancel Click)
    #region AddCatClick
    //protected void AddCatClick(object sender, EventArgs e)
    //{
    //    txtNewCat.Visible = true;
    //    btnAddCat.Visible = true;
    //    btnCancelCat.Visible = true;
    //}
    #endregion

    #region SaveCatClick
    //protected void SaveCatClick(object sender, EventArgs e)
    //{
    //    if (Request.Params["SHW"] != null)
    //    {
    //        string showid = Request.QueryString["SHW"].ToString();

    //        try
    //        {
    //            RunNumber run = new RunNumber(fn);
    //            string catID = run.GetRunNUmber("REFCAT_KEY");
    //            string catname = cFun.solveSQL(txtNewCat.Text.ToString());
    //            string orderno = ddlCategory.Items.Count.ToString();
    //            if (!string.IsNullOrEmpty(catname))
    //            {
    //                string sql = "Insert Into ref_reg_Category (reg_CategoryId,reg_CategoryName, reg_order, ShowID) Values (" + catID + ",'" + catname + "'," + orderno + ",'" + showid + "')";

    //                fn.ExecuteSQL(sql);
    //                txtNewCat.Visible = false;
    //                btnAddCat.Visible = false;
    //                btnCancelCat.Visible = false;
    //                ddlCategory.Items.Clear();
    //                bindCategory(showid);

    //                //ddlCategory.Items.FindByText(catname).Selected = true;
    //                txtNewCat.BackColor = System.Drawing.Color.White;
    //                txtNewCat.Text = string.Empty;
    //            }
    //            else
    //            {
    //                txtNewCat.BackColor = System.Drawing.Color.LightPink;
    //            }
    //        }
    //        catch(Exception ex)
    //        { }
    //    }
    //    else
    //    {
    //        Response.Redirect("Login.aspx");
    //    }
    //}
    #endregion

    #region CancelCatClick
    //protected void CancelCatClick(object sender, EventArgs e)
    //{
    //    txtNewCat.Visible = false;
    //    btnAddCat.Visible = false;
    //    btnCancelCat.Visible = false;
    //    txtNewCat.BackColor = System.Drawing.Color.White;
    //    txtNewCat.Text = string.Empty;
    //}
    #endregion
    #endregion

    #region validateform
    protected Boolean validateform()
    {
        Boolean isvalid = true;

        //if (ddlCategory.SelectedIndex == 0)
        //{
        //    isvalid = false;
        //    lblvalcategory.Visible = true;
        //}
        //else
        //{
        //    lblvalcategory.Visible = false;
        //}

        //if (ddlGroup.SelectedIndex == 0)
        //{
        //    isvalid = false;
        //    lblvalgroup.Visible = true;
        //}
        //else
        //{
        //    lblvalgroup.Visible = false;
        //}

        //if (ddlGroupItem.SelectedIndex == 0)
        //{
        //    isvalid = false;
        //    lblvalgroupitem.Visible = true;
        //}
        //else
        //{
        //    lblvalgroupitem.Visible = false;
        //}

        //if (txtEPrice.Text == "")
        //{
        //    isvalid = false;
        //    lblvaleprice.Visible = true;
        //}
        //else
        //{
        //    lblvaleprice.Visible = false;
        //}

        //if (txtRPrice.Text == "")
        //{
        //    isvalid = false;
        //    lblvalrprice.Visible = true;
        //}
        //else
        //{
        //    lblvalrprice.Visible = false;
        //}

        return isvalid;
    }
    #endregion

    #region SaveUpdateItem
    protected void SaveUpdateItem(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            if (Request.Params["FLW"] != null)
            {
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                try
                {
                    string itemid = string.Empty;
                    ////*Comment by th ((29-3-2018)
                    //if (!HasExistingGroupItem(showid, flowid))
                    //    CreateNewGroupItem(showid, flowid);
                    int categoryID = 0;
                    int groupID = 0;
                    string itemName = string.Empty;
                    int isEarlyBird = 0;
                    string ebPrice = "0.00";
                    string rPrice = "0.00";
                    int maxReg = 0;
                    string maxMessage = string.Empty;
                    string groupitemID = string.Empty;
                    string imgname = string.Empty;
                    string contype = string.Empty;
                    string desc = string.Empty;
                    string con_ItemDesc_ShowedInTemplate = string.Empty;
                    string ReportDisplayName = string.Empty;
                    string desc2AboveName = string.Empty;
                    int isShow = 0;
                    int sequence = 0;

                    if (Session["imgBanner"] != null)
                    {
                        imgname = Session["imgBanner"].ToString();
                    }
                    categoryID = int.Parse(txtCategory.Text);
                    groupID = int.Parse(ddlGroup.SelectedItem.Value);// int.Parse(txtGroupID.Text);
                    itemName = cFun.solveSQL(txtName.Text.Trim());
                    groupitemID = ddlGroupItem.SelectedItem.Value; //txtGroupItem.Text.ToString();
                    if (chkIsEarlyBird.Checked == true)
                    {
                        isEarlyBird = 1;
                    }
                    else
                    {
                        isEarlyBird = 0;
                    }

                    isShow = chkIsShow.Checked == true ? 1 : 0;

                    ebPrice = txtEPrice.Text.Trim().ToString();//Convert.ToInt32(txtEPrice.Text.ToString());
                    rPrice = txtRPrice.Text.Trim().ToString();
                    maxReg = Convert.ToInt32(!string.IsNullOrWhiteSpace(txtMax.Text) && !string.IsNullOrEmpty(txtMax.Text) ? txtMax.Text.ToString() : "0");
                    maxMessage = Server.HtmlEncode(txtMessage.Text);
                    desc = txtDesc.Text;
                    con_ItemDesc_ShowedInTemplate = Server.HtmlEncode(txtDescShowInEmailTemplate.Text);
                    ReportDisplayName = txtDescShowInReportItemBreakdown.Text;
                    desc2AboveName = txtDesc2.Text;

                    sequence = !string.IsNullOrWhiteSpace(txtSequence.Text) ? Convert.ToInt32(txtSequence.Text.Trim()) : 0;

                    //***added on 30-11-2018
                    string disabledItems = "";
                    for (int i = 0; i < chkDisabledItems.Items.Count; i++)
                    {
                        if (chkDisabledItems.Items[i].Selected == true)
                        {
                            disabledItems += chkDisabledItems.Items[i].Value + ",";
                        }
                    }
                    disabledItems = disabledItems.TrimEnd(',');
                    //***added on 30-11-2018

                    string sql = string.Empty;
                    if (!string.IsNullOrEmpty(hfConfID.Value))//*Update
                    {
                        itemid = hfConfID.Value;

                        if (!string.IsNullOrEmpty(imgname))
                        {
                            sql = "Update tb_ConfItem Set con_ItemName = '" + itemName + "',reg_categoryId=" + categoryID + ",con_GroupId=" + groupID
                                + ",con_isEarlyBird=" + isEarlyBird + ",con_EBPrice=" + ebPrice + ",con_RegPrice=" + rPrice
                                + ",con_MaxReg=" + maxReg + ",con_MaxMessage='" + maxMessage + "',con_GroupItemID='" + groupitemID
                                + "',con_ItemImage='" + imgname + "',con_ItemDesc='" + desc + "',con_IsShow=" + isShow + ",con_OrderSeq=" + sequence + ",disabledItems='" + disabledItems + "'"
                                + ",con_ItemDesc2_AboveName='" + desc2AboveName + "' "
                                + ",con_ItemDesc_ShowedInTemplate='" + con_ItemDesc_ShowedInTemplate + "',ReportDisplayName='" + ReportDisplayName + "' "
                                + " Where con_itemId=" + itemid;
                        }
                        else
                        {
                            sql = "Update tb_ConfItem Set con_ItemName = '" + itemName + "',reg_categoryId=" + categoryID + ",con_GroupId=" + groupID
                                + ",con_isEarlyBird=" + isEarlyBird + ",con_EBPrice=" + ebPrice + ",con_RegPrice=" + rPrice
                                + ",con_MaxReg=" + maxReg + ",con_MaxMessage='" + maxMessage + "',con_GroupItemID='" + groupitemID
                                + "',con_ItemDesc='" + desc + "',con_IsShow=" + isShow + ",con_OrderSeq=" + sequence + ",disabledItems='" + disabledItems + "'"
                                + ",con_ItemDesc2_AboveName='" + desc2AboveName + "' "
                                + ",con_ItemDesc_ShowedInTemplate='" + con_ItemDesc_ShowedInTemplate + "',ReportDisplayName='" + ReportDisplayName + "' "
                                + " Where con_itemId=" + itemid;
                        }

                        int rowInserted = fn.ExecuteSQL(sql);
                        if (rowInserted == 1)
                        {
                            string paraA = "";
                            if (Request.Params["a"] != null) paraA = "&a=" + Request.Params["a"].ToString();
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Updated.');window.location='Event_Conference_Items?SHW=" + Request.Params["SHW"].ToString() + "&FLW=" + Request.Params["FLW"].ToString() + "&STP=" + Request.Params["STP"].ToString() + "&CAT=" + Request.Params["CAT"].ToString() + paraA+ "';", true);
                        }
                    }
                    else//*Insert
                    {
                        RunNumber run = new RunNumber(fn);
                        string confItemID = run.GetRunNUmber("CONITEM_KEY");

                        string query = string.Format("Insert Into tb_ConfItem (con_itemId, con_ItemName, con_ItemDesc, con_ItemImage, reg_categoryId, con_GroupId, con_GroupItemID"
                                + ",con_isEarlyBird, con_EBPrice, con_RegPrice, con_MaxReg, con_MaxMessage, con_IsShow, con_OrderSeq, ShowID, disabledItems,con_ItemDesc2_AboveName,con_ItemDesc_ShowedInTemplate,ReportDisplayName)"
                                + " Values ({0}, '{1}', '{2}', '{3}', {4}, {5}, {6}"
                                + ",{7}, {8}, {9}, {10}, '{11}', {12}, {13}, '{14}', '{15}', '{16}', '{17}', '{18}')"
                                , confItemID, itemName, desc, imgname, categoryID, groupID, groupitemID
                                , isEarlyBird, ebPrice, rPrice, maxReg, maxMessage, isShow, sequence, showid, disabledItems, desc2AboveName
                                ,con_ItemDesc_ShowedInTemplate, ReportDisplayName);

                        int rowInserted = fn.ExecuteSQL(query);
                        if (rowInserted == 1)
                        {
                            string paraA = "";
                            if (Request.Params["a"] != null) paraA = "&a=" + Request.Params["a"].ToString();
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Item Inserted.');window.location='Event_Conference_Items?SHW=" + Request.Params["SHW"].ToString() + "&FLW=" + Request.Params["FLW"].ToString() + "&STP=" + Request.Params["STP"].ToString() + "&CAT=" + Request.Params["CAT"].ToString() + paraA + "';", true);
                        }
                    }

                    string Cateogory = cFun.DecryptValue(Request.QueryString["CAT"].ToString());
                    txtCategory.Text = Cateogory;
                    // bindConfConfig(flowid, showid);
                    bindGroup(showid, "", flowid);
                    string filter = " And (reg_categoryId=" + txtCategory.Text + ")";
                    bindlist(filter, showid);
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Saving error, try again.');", true);
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region UpSystemLogo_Click
    protected void UpSystemLogo_Click(object sender, EventArgs e)
    {
        try
        {
            if (fupImg.PostedFile.FileName != "")
            {
                string postedfilename = Path.GetFileNameWithoutExtension(fupImg.PostedFile.FileName);
                string postedfileext = Path.GetExtension(fupImg.PostedFile.FileName);
                string datetick = "_" + DateTime.Now.ToString("ddmmyyyyhhmm");

                string filePath = "~/Admin/Site/" + postedfilename + datetick + postedfileext;
                fupImg.SaveAs(MapPath(filePath));
                Session["imgBanner"] = "../Admin/site/" + postedfilename + datetick + postedfileext;
                imgPhoto.Visible = true;
                imgPhoto.ImageUrl = "../Admin/site/" + postedfilename + datetick + postedfileext;
            }
            else
            {
                imgPhoto.Visible = false;
                imgPhoto.ImageUrl = "";
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region btnReset_Click
    protected void btnReset_Click(object sender, EventArgs e)
    {
        ResetControls();

        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            if (Request.Params["FLW"] != null)
            {
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                string Cateogory = cFun.DecryptValue(Request.QueryString["CAT"].ToString());
                txtCategory.Text = Cateogory;
                // bindConfConfig(flowid, showid);
                bindGroup(showid, "", flowid);
                string filter = " And (reg_categoryId=" + txtCategory.Text + ")";
                bindlist(filter, showid);
            }
        }
    }
    #endregion

    #region ResetControls (clear data from controls)
    private void ResetControls()
    {
        hfConfID.Value = "";
        // ddlCategory.SelectedIndex = 0;
        //  ddlGroup.SelectedIndex = 0;

        //  ddlGroup_SelectedIndexChanged(this, null);

        Session["imgBanner"] = null;
        imgPhoto.Visible = false;
        imgPhoto.ImageUrl = "";
        txtName.Text = "";
        txtDesc.Text = "";
        txtDescShowInEmailTemplate.Text = "";
        txtDescShowInReportItemBreakdown.Text = "";
        txtDesc2.Text = "";
        txtSequence.Text = "";
        chkIsShow.Checked = true;
        chkIsEarlyBird.Checked = false;
        txtEPrice.Text = "";
        txtRPrice.Text = "";
        txtMax.Text = "0";
        txtMessage.Text = "";
        ViewState["Id"] = null;
    }
    #endregion
    #endregion

    #region btnNext_Click
    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["STP"] != null)
        {
            string showID = Request.Params["SHW"].ToString();
            string flowID = Request.Params["FLW"].ToString();
            string step = Request.Params["STP"].ToString();

            if (Request.Params["a"] == null)
            {
                string url = string.Format("Event_Conference_Controler?FLW={0}&SHW={1}&STP={2}", flowID, showID, step);
                Response.Redirect(url);
            }
            else
            {
                string actionType = Request.QueryString["a"].ToString();
                string url = string.Format("Event_Conference_Controler?FLW={0}&SHW={1}&STP={2}&a={3}", flowID, showID, step, actionType);
                Response.Redirect(url);
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region updateSiteFlowMaster
    private void updateSiteFlowMaster(string flowid, string configType, string showid)
    {
        try
        {
            string updateConfigType = "Update tb_site_flow_master Set FLW_Cat_Config_Type=" + configType + " Where FLW_ID='" + flowid + "' And ShowID='" + showid + "'";
            fn.ExecuteSQL(updateConfigType);
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region btnCancel_Click
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ResetControls();
        divConfInsertUpdate.Visible = false;
        btnAddNewConfItem.Visible = true;

        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["CAT"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

            string Cateogory = cFun.DecryptValue(Request.QueryString["CAT"].ToString());
            txtCategory.Text = Cateogory;
            //       bindGroup(showid, "");
            string filter = " And (reg_categoryId=" + txtCategory.Text + ")";
            bindlist(filter, showid);
        }
    }


    #endregion

    #region GroupITem
    private bool HasExistingGroupItem(string shwID, string flowID)
    {
        bool hasExist = false;
        try
        {
            bool isGroup = false;
            bool isItem = false;
            
            string sql = string.Format("select * from tb_conf_group where showID='{0}' and FlowID='{1}'", shwID, flowID);
            DataTable dtGrp = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dtGrp.Rows.Count > 0)
            {
                txtGroupID.Text = dtGrp.Rows[0]["con_groupID"].ToString();
                isGroup = true;
            }

            sql = string.Format("select * from tb_Conf_GroupItem where showID='{0}' and FlowID='{1}'", shwID, flowID);
            DataTable dtItem = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dtItem.Rows.Count > 0)
            {
                txtGroupItem.Text = dtItem.Rows[0]["GI_GUID"].ToString();
                isItem = true;
            }
            if (isGroup && isItem) hasExist = true;
        }
        catch { }

        return hasExist;
    }
    private void CreateNewGroupItem(string shwID, string flowID, string grpID, string groupname, string orderno)
    {
        try
        {
            RunNumber run = new RunNumber(fn);
            string grpitemID = run.GetRunNUmber("CONGRPITEM_KEY");
            ////*Comment by th ((29-3-2018)
            //string groupname = "Flow - " + flowID;
            //string orderno = "0";

            string sql = "Insert Into tb_Conf_Group (con_GroupId, con_GroupName, con_Orderno, ShowID,FlowID) Values (" + grpID + ",'" + groupname + "'," + orderno + ",'" + shwID + "','" + flowID + "')";
            fn.ExecuteSQL(sql);

            //string groupitemname = "Flow - " + flowID;
            string groupitemname = groupname;


            if (groupitemname != "" || groupitemname != string.Empty)
            {
                sql = "Insert Into tb_Conf_GroupItem (GI_GUID, GI_Name, GroupID, GI_OrderNo, ShowID,FlowID) Values ("
                   + grpitemID + ",'" + groupitemname + "'," + grpID + "," + orderno + ",'" + shwID + "','" + flowID + "')";

                fn.ExecuteSQL(sql);
                txtGroupID.Text = grpID;
                txtGroupItem.Text = grpitemID;
            }
        }
        catch { }
    }
    #region *add by th ((29-3-2018)
    protected void AddGroupClick(object sender, EventArgs e)
    {
        txtNewGroup.Visible = true;
        btnAddGroup.Visible = true;
        btnCancelGroup.Visible = true;
    }
    protected void SaveGroupClick(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

            try
            {
                RunNumber run = new RunNumber(fn);
                string grpID = run.GetRunNUmber("CONGRP_KEY");
                string groupname = cFun.solveSQL(txtNewGroup.Text.ToString());
                string orderno = ddlGroup.Items.Count.ToString();
                if (groupname != "" || groupname != string.Empty)
                {
                    CreateNewGroupItem(showid, flowid, grpID, groupname, orderno);//***

                    txtNewGroup.Visible = false;
                    btnAddGroup.Visible = false;
                    btnCancelGroup.Visible = false;
                    ddlGroup.Items.Clear();
                    bindGroup(showid, grpID, flowid);

                    //ddlGroup.Items.FindByText(groupname).Selected = true;
                    txtNewGroup.BackColor = System.Drawing.Color.White;
                    txtNewGroup.Text = string.Empty;
                }
                else
                {
                    txtNewGroup.BackColor = System.Drawing.Color.LightPink;
                }
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    protected void CancelGroupClick(object sender, EventArgs e)
    {
        txtNewGroup.Visible = false;
        btnAddGroup.Visible = false;
        btnCancelGroup.Visible = false;
        txtNewGroup.BackColor = System.Drawing.Color.White;
        txtNewGroup.Text = string.Empty;
    }
    protected void bindGroup(string showid, string selectedgroupid, string flowid)
    {
        try
        {
            string query = "Select * From tb_Conf_Group Where (isDeleted=0 Or isDeleted Is Null) And ShowID=@SHWID And FlowID=@FlowID Order By con_Orderno";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            SqlParameter spar1 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            spar.Value = showid;
            spar1.Value = flowid;
            pList.Add(spar);
            pList.Add(spar1);

            DataSet ds = new DataSet();
            ddlGroup.Items.Clear();
            ds = fn.GetDatasetByCommand(query, "ds", pList);
            if (ds.Tables[0].Rows.Count != 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlGroup.Items.Add(ds.Tables[0].Rows[i]["con_GroupName"].ToString());
                    ddlGroup.Items[i].Value = ds.Tables[0].Rows[i]["con_GroupId"].ToString();

                    if (ds.Tables[0].Rows[i]["con_GroupId"].ToString() == selectedgroupid)
                    {
                        ddlGroup.ClearSelection();
                        ddlGroup.Items[i].Selected = true;
                    }
                    else
                    {
                        ddlGroup.SelectedIndex = 0;
                    }
                }

                ddlGroup_SelectedIndexChanged(this, null);
            }
        }
        catch (Exception ex)
        { }
    }
    protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

            bindGroupItem(ddlGroup.SelectedValue, showid, "", flowid);
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    protected void bindGroupItem(string filter, string showid, string selectedgroupitemid, string flowid)
    {
        try
        {
            ddlGroupItem.Items.Clear();
            if (filter == "Select" || filter == "")
            {
                filter = "0";
            }
            string query = "Select * From tb_Conf_GroupItem Where GroupID=" + filter + " And ShowID=@SHWID And FlowID=@FlowID";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            SqlParameter spar1 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            spar.Value = showid;
            spar1.Value = flowid;
            pList.Add(spar);
            pList.Add(spar1);

            DataSet ds = fn.GetDatasetByCommand(query, "ds", pList);
            if (ds.Tables[0].Rows.Count != 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlGroupItem.Items.Add(ds.Tables[0].Rows[i]["GI_Name"].ToString());
                    ddlGroupItem.Items[i].Value = ds.Tables[0].Rows[i]["GI_GUID"].ToString();

                    if (ds.Tables[0].Rows[i]["GI_GUID"].ToString() == selectedgroupitemid)
                    {
                        ddlGroupItem.ClearSelection();
                        ddlGroupItem.Items[i].Selected = true;
                    }
                    else
                    {
                        ddlGroupItem.SelectedIndex = 0;
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #endregion
}