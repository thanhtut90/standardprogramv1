﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="RegIndiv_Edit.aspx.cs" Inherits="Admin_RegIndiv_Edit" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">
    .red
    {
        color:red;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h3 class="box-title">Manage Individual (Delegate) Info</h3>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="form-horizontal">
                <div class="box-body">
                    <div class="form-group" runat="server" id="divSalutation">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblSalutation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlSalutation" runat="server" CssClass="form-control"
                                OnSelectedIndexChanged="ddlSalutation_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcSal" runat="server" 
                            ControlToValidate="ddlSalutation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divSalOther" visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            &nbsp;
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtSalOther" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcSalOther" runat="server"
                            ControlToValidate="txtSalOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divFName">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblFName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcFName" runat="server"
                            ControlToValidate="txtFName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divLName">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblLName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtLName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcLName" runat="server"
                            ControlToValidate="txtLName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divOName">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblOName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtOName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcOName" runat="server"
                            ControlToValidate="txtOName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divPassNo">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblPassNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtPassNo" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcPassNo" runat="server"
                            ControlToValidate="txtPassNo" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divIsReg">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblIsReg" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:RadioButtonList ID="rbreg" runat="server" Width="100px" RepeatDirection="Horizontal" TextAlign="Right" AutoPostBack="true" OnSelectedIndexChanged="rbreg_SelectedIndexChanged">
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                            </asp:RadioButtonList>
                            <div runat="server" id="divRegSpecific" visible="false">
                                <asp:RadioButtonList ID="rbregspecific" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="150px">
                                    <asp:ListItem Value="MCR" Text="MCR" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="SNB" Text="SNB"></asp:ListItem>
                                    <asp:ListItem Value="PRN" Text="PRN"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divIDNo">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblIDNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtIDNo" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcIDNo" runat="server"
                            ControlToValidate="txtIDNo" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divDesignation">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcDesig" runat="server"
                            ControlToValidate="txtDesignation" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <!-- Job Title for Studnet & Allied Health -->
                    <div class="form-group" runat="server" id="divJobtitle">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblJobtitle" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtJobtitle" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcJobtitle" runat="server"
                            ControlToValidate="txtJobtitle" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divProfession">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblProfession" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList runat="server" ID="ddlProfession" AutoPostBack="true" CssClass="form-control"
                                OnSelectedIndexChanged="ddlProfession_SelectedIndexChanged">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcProfession" runat="server" 
                            ControlToValidate="ddlProfession" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divProOther" visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            &nbsp;
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtProOther" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcProOther" runat="server"
                            ControlToValidate="txtProOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divDoctor" visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblDoctor" runat="server" CssClass="form-control-label" Text="Are you a MOHOS/Resident?"></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:RadioButtonList ID="rbDoctor" runat="server" Width="100px" RepeatDirection="Horizontal" TextAlign="Right">
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divStudentUpload">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblStudentUpload" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:FileUpload runat="server" ID="fupStudentUpload" CssClass="style4" 
                                style="font-family: DINPro-Regular" />
                            <br />
                            <asp:Image runat="server" ID="imgStudentUpload" Width="280px" Height="150px" Visible="false" ImageUrl="#" />
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                            ControlToValidate="ddlStudentType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divStudentType">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblStudentType" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList runat="server" ID="ddlStudentType" AutoPostBack="true" CssClass="form-control"
                                OnSelectedIndexChanged="ddlStudentType_SelectedIndexChanged">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcStudentType" runat="server" 
                            ControlToValidate="ddlStudentType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divStudentOther" visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            &nbsp;
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtStudentOther" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcStudentOther" runat="server"
                            ControlToValidate="txtStudentOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divOrganization">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblOrganization" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlOrganization" runat="server" CssClass="form-control"
                                OnSelectedIndexChanged="ddlOrganization_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcOrg" runat="server" 
                            ControlToValidate="ddlOrganization" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divOrgOther" visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            &nbsp;
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtOrgOther" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcOrgOther" runat="server"
                            ControlToValidate="txtOrgOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divInstitution">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblInstitution" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlInstitution" runat="server" CssClass="form-control"
                                OnSelectedIndexChanged="ddlInstitution_SelectedIndexChanged" AutoPostBack="true">
                                <%--<asp:ListItem Value="0">Please Select</asp:ListItem>--%>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcInsti" runat="server" 
                            ControlToValidate="ddlInstitution" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divInstiOther" visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            &nbsp;
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtInstiOther" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcInstiOther" runat="server"
                            ControlToValidate="txtInstiOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divDepartment">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblDepartment" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList runat="server" ID="ddlDepartment" AutoPostBack="true" CssClass="form-control"
                                OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                <%--<asp:ListItem Value="0">Please Select</asp:ListItem>--%>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcDeptm" runat="server" 
                            ControlToValidate="ddlDepartment" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divDepartmentOther" visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            &nbsp;
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtDepartmentOther" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcDeptmOther" runat="server"
                            ControlToValidate="txtDepartmentOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAddress1">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAddress1" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAddress1" runat="server"
                            ControlToValidate="txtAddress1" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAddress2">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAddress2" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAddress2" runat="server"
                            ControlToValidate="txtAddress2" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAddress3">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAddress3" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAddress3" runat="server"
                            ControlToValidate="txtAddress3" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAddress4">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAddress4" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAddress4" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAddress4" runat="server"
                            ControlToValidate="txtAddress4" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divRCountry">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblRCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlRCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlRCountry_SelectedIndexChanged">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcRCountry" runat="server" 
                                ControlToValidate="ddlRCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divCity">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblCity" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcCity" runat="server"
                            ControlToValidate="txtCity" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divState">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblState" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcState" runat="server"
                            ControlToValidate="txtState" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divCountry">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcCountry" runat="server" 
                                ControlToValidate="ddlCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divPostalcode">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblPostalcode" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtPostalcode" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcPostalcode" runat="server"
                            ControlToValidate="txtPostalcode" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divTel">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblTel" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5" runat="server" >
                            <div class="col-xs-3" runat="server" id="divTelcc" style="padding-left:0px;" >
                                <asp:TextBox ID="txtTelcc" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTelcc" runat="server"
                                    TargetControlID="txtTelcc"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-3" runat="server" id="divTelac" style="padding-left:0px;" >
                                <asp:TextBox ID="txtTelac" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTelac" runat="server"
                                    TargetControlID="txtTelac"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-6" runat="server" id="divTelNo" style="padding-right:0px;padding-left:0px;">
                                <asp:TextBox ID="txtTel" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTel" runat="server"
                                    TargetControlID="txtTel"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcTel" runat="server"
                            ControlToValidate="txtTel" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcTelcc" runat="server"
                            ControlToValidate="txtTelcc" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcTelac" runat="server"
                            ControlToValidate="txtTelac" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divMobile">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblMobile" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5" runat="server" >
                            <div class="col-xs-3" runat="server" id="divMobcc" style="padding-left:0px;" >
                                <asp:TextBox ID="txtMobcc" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbMobcc" runat="server"
                                    TargetControlID="txtMobcc"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-3" runat="server" id="divMobac" style="padding-left:0px;" >
                                <asp:TextBox ID="txtMobac" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbMobac" runat="server"
                                    TargetControlID="txtMobac"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-6" runat="server" id="divMobileNo" style="padding-right:0px;padding-left:0px;">
                                <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbMobile" runat="server"
                                    TargetControlID="txtMobile"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcMob" runat="server"
                            ControlToValidate="txtMobile" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcMobcc" runat="server"
                            ControlToValidate="txtMobcc" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcMobac" runat="server"
                            ControlToValidate="txtMobac" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divFax">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblFax" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5" runat="server" >
                            <div class="col-xs-3" runat="server" id="divFaxcc" style="padding-left:0px;" >
                                <asp:TextBox ID="txtFaxcc" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbFaxcc" runat="server"
                                    TargetControlID="txtFaxcc"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-3" runat="server" id="divFaxac" style="padding-left:0px;" >
                                <asp:TextBox ID="txtFaxac" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbFaxac" runat="server"
                                    TargetControlID="txtFaxac"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                            <div class="col-xs-6" runat="server" id="divFaxNo" style="padding-right:0px;padding-left:0px;">
                                <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbFax" runat="server"
                                    TargetControlID="txtFax"
                                    FilterType="Numbers"
                                    ValidChars="+0123456789" />
                            </div>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcFax" runat="server"
                            ControlToValidate="txtFax" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcFaxcc" runat="server"
                            ControlToValidate="txtFaxcc" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-1">
                            <asp:RequiredFieldValidator ID="vcFaxac" runat="server"
                            ControlToValidate="txtFaxac" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divEmail">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:RequiredFieldValidator ID="vcEmail" runat="server"
                            ControlToValidate="txtEmail" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="validateEmail"
                            runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                            ControlToValidate="txtEmail"
                            ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divEmailConfirmation">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblEmailConfirmation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtEmailConfirmation" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:RequiredFieldValidator ID="vcEConfirm" runat="server"
                            ControlToValidate="txtEmailConfirmation" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cmpEmail" runat="server" ControlToValidate="txtEmailConfirmation"
                            ControlToCompare="txtEmail" ErrorMessage="Not match." ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAffiliation">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAffiliation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlAffiliation" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcAffil" runat="server" 
                                ControlToValidate="ddlAffiliation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divDietary">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblDietary" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlDietary" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcDietary" runat="server" 
                                ControlToValidate="ddlDietary" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divNationality">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblNationality" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtNationality" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcNation" runat="server"
                            ControlToValidate="txtNationality" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAge">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAge" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAge" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbAge" runat="server"
                                TargetControlID="txtAge"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAge" runat="server"
                            ControlToValidate="txtAge" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divDOB">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblDOB" runat="server" CssClass="form-control-label" Text="Date of Birth"></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox><%-- onblur="validateDate(this);"--%>
                            <asp:CalendarExtender ID="calendarDOB" TargetControlID="txtDOB"
                                    runat="server" PopupButtonID="txtDOB" Format="dd/MM/yyyy" ></asp:CalendarExtender>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcDOB" runat="server" Enabled="false"
                            ControlToValidate="txtDOB" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <%--<asp:RegularExpressionValidator ID="regexpName" runat="server"
                                    ErrorMessage="This expression does not validate." 
                                    ControlToValidate="txtDOB"
                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$" />--%>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divGender">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblGender" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                                <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <%--<asp:CompareValidator ID="vcGender" runat="server" 
                                ControlToValidate="ddlGender" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>--%>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divMemberNo">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblMemberNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtMemberNo" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcMember" runat="server"
                            ControlToValidate="txtMemberNo" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divAdditional4">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAdditional4" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAdditional4" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAdditional4" runat="server"
                            ControlToValidate="txtAdditional4" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
        
                    <div class="form-group" runat="server" id="divAdditional5">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblAdditional5" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtAdditional5" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcAdditional5" runat="server"
                            ControlToValidate="txtAdditional5" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>

                <div class="form-group" runat="server" id="divVName">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblVName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVName" runat="server"
                        ControlToValidate="txtVName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group" runat="server" id="divVDOB">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblVDOB" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVDOB" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVDOB" runat="server"
                        ControlToValidate="txtVDOB" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group" runat="server" id="divVPassNo">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblVPassNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVPassNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVPassNo" runat="server"
                        ControlToValidate="txtVPassNo" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group" runat="server" id="divVPassIssueDate">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblVPassIssueDate" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVPassIssueDate" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVPassIssueDate" runat="server" Enabled="false"
                        ControlToValidate="txtVPassIssueDate" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group" runat="server" id="divVPassExpiry">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblVPassExpiry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVPassExpiry" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVPExpiry" runat="server"
                        ControlToValidate="txtVPassExpiry" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group" runat="server" id="divVEmbarkation">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblVEmbarkation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVEmbarkation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVEmbarkation" runat="server" Enabled="false"
                        ControlToValidate="txtVEmbarkation" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group" runat="server" id="divVArrivalDate">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblVArrivalDate" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVArrivalDate" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVArrivalDate" runat="server"
                        ControlToValidate="txtVArrivalDate" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group" runat="server" id="divVCountry">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblVCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlVCountry" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcVCountry" runat="server" 
                            ControlToValidate="ddlVCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                    <div class="form-group" runat="server" id="divUDFCName">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblUDFCName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtUDFCName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcUDFCName" runat="server"
                            ControlToValidate="txtUDFCName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divUDFDelType">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblUDFDelType" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtUDFDelType" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcUDFDelType" runat="server"
                            ControlToValidate="txtUDFDelType" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divUDFProCategory">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblUDFProCategory" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlUDFProCategory" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUDFProCategory_SelectedIndexChanged">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcUDFProCat" runat="server"
                                ControlToValidate="ddlUDFProCategory" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>
                
                    <div class="form-group" runat="server" id="divUDFProCatOther" visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            &nbsp;
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtUDFProCatOther" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcProCatOther" runat="server"
                            ControlToValidate="txtUDFProCatOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divUDFCpostalcode">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblUDFCpostalcode" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtUDFCpostalcode" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcUDFCpcode" runat="server"
                            ControlToValidate="txtUDFCpostalcode" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divUDFCLDept">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblUDFCLDept" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtUDFCLDept" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcUDFCLDept" runat="server"
                            ControlToValidate="txtUDFCLDept" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divUDFAddress">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblUDFAddress" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtUDFAddress" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcUDFAddress" runat="server"
                            ControlToValidate="txtUDFAddress" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divUDFCLCom">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblUDFCLCom" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlUDFCLCom" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUDFCLCom_SelectedIndexChanged">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcUDFCLCom" runat="server" 
                                ControlToValidate="ddlUDFCLCom" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divUDFCLComOther" visible="false">
                        <div class="col-md-3 col-md-offset-1">
                            &nbsp;
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtUDFCLComOther" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcUDFCLComOther" runat="server"
                            ControlToValidate="txtUDFCLComOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divUDFCCountry">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblUDFCCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:DropDownList ID="ddlUDFCCountry" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2">
                            <asp:CompareValidator ID="vcUDFCCountry" runat="server" 
                                ControlToValidate="ddlUDFCCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divSupName">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblSupName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtSupName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcSupName" runat="server"
                            ControlToValidate="txtSupName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divSupDesignation">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblSupDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtSupDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcSupDes" runat="server"
                            ControlToValidate="txtSupDesignation" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group" runat="server" id="divSupContact">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblSupContact" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtSupContact" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:RequiredFieldValidator ID="vcSupContact" runat="server"
                            ControlToValidate="txtSupContact" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                
                    <div class="form-group" runat="server" id="divSupEmail">
                        <div class="col-md-3 col-md-offset-1">
                            <asp:Label ID="lblSupEmail" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                        </div>
                        <div class="col-md-5">
                            <asp:TextBox ID="txtSupEmail" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:RequiredFieldValidator ID="vcSupEmail" runat="server"
                            ControlToValidate="txtSupEmail" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="validateSupEmail"
                            runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                            ControlToValidate="txtSupEmail"
                            ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-6">
                            <asp:Button runat="server" Text="Update" ID="btnupdate" OnClick="btnupdate_Click"/>
                            <asp:HiddenField runat="server" ID="hfRegGroupID" Value=""></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfRegno" Value=""></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfcategoryID" Value=""></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfStage" Value=""></asp:HiddenField>
                            <asp:HiddenField runat="server" ID="hfApproveStatus" Value=""></asp:HiddenField>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <br />
    <asp:Label CssClass="col-md-2 control-label" runat="server" ID="lbls"></asp:Label>
</asp:Content>

