﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;

public partial class Admin_ManageSalutation : System.Web.UI.Page
{
    /// <summary>
    /// If you update or delete already inserted records, need to check and update in the tb_RegGroup and tb_RegDelegate tables
    /// because the salutation name may be used by those table.
    /// </summary>


    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                lblShowid.Text = Request.QueryString["SHW"].ToString();

                BindGrid(lblShowid.Text);
                //Master.setgenopen();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    #region BindGrid and use table ref_salutation & bind gvList & set pnllist visibility true or false
    protected void BindGrid(string showid)
    {
        DataSet dsSalutation = new DataSet();

        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsSalutation = cmdObj.getSalutation(showid);
        if (dsSalutation.Tables[0].Rows.Count > 0)
        {
            gvList.DataSource = dsSalutation;
            gvList.DataBind();
            pnllist.Visible = true;
        }
        else
        {
            pnllist.Visible = false;
        }
    }
    #endregion

    #region GridView1_RowDeleting and OnRowDeleting event of gvList & use table ref_salutation & delete from ref_salutation according to Sal_ID
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;

        SalutationObj objSal = new SalutationObj();
        objSal.Sal_ID = ID;
        objSal.ShowID = lblShowid.Text;

        SetUpController setupCtrlr = new SetUpController(fn);
        int isSuccess = setupCtrlr.deleteSalutation(objSal);

        BindGrid(lblShowid.Text);
    }
    #endregion

    #region GridView1_RowEditing and OnRowEditing event of gvList
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvList.EditIndex = e.NewEditIndex;
        BindGrid(lblShowid.Text);
    }
    #endregion

    #region GridView1_RowUpdating and OnRowUpdating event of gvList & use table ref_salutation & update ref_salutation according to Sal_ID
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;
        // Retrieve the row being edited.
        int index = gvList.EditIndex;
        GridViewRow row = gvList.Rows[index];
        TextBox t1 = row.FindControl("txtName") as TextBox;
        TextBox t2 = row.FindControl("txtOrder") as TextBox;
        Label lblno = row.FindControl("lblno") as Label;
        string t3 = gvList.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            lblno.Visible = false;

            SalutationObj objSal = new SalutationObj();
            objSal.Sal_ID = ID;
            objSal.Sal_Name = cFun.solveSQL(t1.Text.ToString());
            objSal.Sal_order = Convert.ToInt32(t2.Text.ToString());
            objSal.ShowID = lblShowid.Text;

            SetUpController setupCtrlr = new SetUpController(fn);
            int isSuccess = setupCtrlr.updateSalutation(objSal);

            gvList.EditIndex = -1;
            BindGrid(lblShowid.Text);
        }
        catch (Exception ex)
        {
            lblno.Visible = true;
            lblno.Text = ex.Message;
        }
    }
    #endregion

    #region GridView1_RowCancelingEdit and OnRowCancelingEdit event of gvList
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvList.EditIndex = -1;
        BindGrid(lblShowid.Text);
    }
    #endregion

    #region btnAdd_Click and use table ref_salutation & insert into ref_salutation table
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        RunNumber run = new RunNumber(fn);
        string salID = run.GetRunNUmber("SAL_KEY");
        string salName = cFun.solveSQL(txtSalName.Text.ToString());
        string sortorder = cFun.solveSQL(txtSortorder.Text.ToString());
        string showid = lblShowid.Text;

        try
        {
            SalutationObj objSal = new SalutationObj();
            objSal.Sal_ID = Convert.ToInt32(salID);
            objSal.Sal_Name = salName;
            objSal.Sal_order = Convert.ToInt32(sortorder);
            objSal.ShowID = showid;

            SetUpController setupCtrlr = new SetUpController(fn);
            int isSuccess = setupCtrlr.insertSalutation(objSal);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }

        BindGrid(lblShowid.Text);
        txtSalName.Text = "";
        txtSortorder.Text = "";
    }
    #endregion
}