﻿using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AdminLoginLock : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session.Clear();
            Session.Abandon();
            removeCookie();//***added on 5-11-2018

            bool isUnlock = isRegLoginUnLock();
            if (isUnlock)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }
    public void removeCookie()
    {
        HttpCookie aCookie;
        string cookieName;
        int limit = Request.Cookies.Count;
        for (int i = 0; i < limit; i++)
        {
            cookieName = Request.Cookies[i].Name;
            aCookie = new HttpCookie(cookieName);
            aCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(aCookie);
        }
    }
    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
    private bool isRegLoginUnLock()
    {
        bool isUnlock = false;
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogAdminLoginLocking Where login_machineip='" + machineIP + "'";
            DataTable dtlogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtlogin.Rows.Count > 0)
            {
                string sqlLoginUpdate = string.Empty;
                string AttemptTime = fn.GetDataByCommand("Select login_lockdatetime From tb_LogAdminLoginLocking Where login_machineip='" + machineIP + "'", "login_lockdatetime");
                DateTime attempedDateTime = cFun.ParseDateTime(AttemptTime);
                DateTime datetimenow = DateTime.Now;
                TimeSpan duration = datetimenow - attempedDateTime;
                double durationTime = duration.TotalMinutes;
                if (durationTime >= 20)//***
                {
                    sqlLoginUpdate = "Update tb_LogAdminLoginLocking Set login_lockdatetime=Null,login_islock=0,login_attemptTimes=0 Where login_machineip='" + machineIP + "'";
                    fn.ExecuteSQL(sqlLoginUpdate);
                    isUnlock = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isUnlock;
    }
}