﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Registration;

public partial class Admin_ManageDepartment : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    public string shwID;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["SHW"] != null)
            {
                shwID = Request.QueryString["SHW"].ToString();
                lblShowID.Text = shwID;
                binddropdown(shwID);
                bindlist(shwID);
                //Master.setgenopen();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }

        if (Request.QueryString["SHW"] != null)
        {
            shwID = Request.QueryString["SHW"].ToString();
        }
    }

    #region binddropdown (bind data from ref_Institution table to ddlInstitution)
    public void binddropdown(string showId)
    {
        try
        {
            DataSet ds = new DataSet();
            CommonDataObj commonObj = new CommonDataObj(fn);
            ds = commonObj.getInstitutionList(showId);
            if (ds.Tables[0].Rows.Count != 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlInstitution.Items.Add(ds.Tables[0].Rows[i]["Institution"].ToString());
                    ddlInstitution.Items[i].Value = ds.Tables[0].Rows[i]["ID"].ToString();
                }
            }
        }
        catch (Exception)
        {
        }
    }
    #endregion

    #region bindlist (bind data from ref_Department, ref_Institution tables to GridView(gvList)
    protected void bindlist(string showId)
    {
        try
        {
            DataSet ds = new DataSet();
            string instiID = ddlInstitution.SelectedValue.ToString();
            ds = fn.GetDatasetByCommand("Select * From ref_Department as d Inner Join ref_Institution as i On d.InstitutionID=i.ID Where d.InstitutionID='" + instiID + "' and d.ShowID='" + showId + "' Order By d.Sorting Asc", "ds");
            if (ds.Tables[0].Rows.Count != 0)
            {
                gvList.DataSource = ds;
                gvList.DataBind();
                pnllist.Visible = true;
            }
            else
            {
                pnllist.Visible = false;
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region gvcommand (delete data from ref_Department table according to ID)
    protected void gvcommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "deleterow")
        {
            string deptid = e.CommandArgument.ToString();
            SetUpController controller = new SetUpController(fn);
            DepartmentObj departmentObj = new DepartmentObj();
            departmentObj.department_id = Convert.ToInt32(deptid);
            departmentObj.ShowID = lblShowID.Text;

            int success = controller.deleteDepartment(departmentObj);

            bindlist(lblShowID.Text);
        }
    }
    #endregion

    #region ddlInstitution_SelectedIndexChanged (bind department data according to the selected institution to the gridview (gvList))
    protected void ddlInstitution_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindlist(lblShowID.Text);
    }
    #endregion
}