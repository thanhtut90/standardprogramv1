﻿using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
//using App.DAL;


public partial class SiteMaster : MasterPage
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    protected string profileimg;
    public void setopenreg()
    { }
    public void setopensettings()
    { }
    public void setopen()
    { }
    public void setgenopen()
    { }

    protected void Page_Init(object sender, EventArgs e)
    {
        // The code below helps to protect against XSRF attacks
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;
        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            // Use the Anti-XSRF token from the cookie
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            // Generate a new Anti-XSRF token and save to the cookie
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;

            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
            {
                responseCookie.Secure = true;
            }
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;
    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Set Anti-XSRF token
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            // Validate the Anti-XSRF token
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
            }
        }
    }

    private static string checkingMFAShowName = "MFA 2020";
    private static string checkingMMAShowName = "MMA 2020";
    private static string checkingOSHAShowName = "OSHA 2020";
    private static string checkingVendorShowName = "Vendor Registration";
    private static string checkingFoodJapanShowName = "Food Japan";
    private static string checkingANZIC = "SG-ANZICS 2020";
    private static string checkingANZICWorkshopOnly = "SG-ANZICS 2020 workshop registration only";
    private static string checkingAPCCMI = "APCCMI Singapore 2020";
    private static string checkingiSRRS = "iSRRS 2019";
    private static string checkingMFT = "MFT 2019";
    private static string checkingPPI = "PPI 2019";
    private static string checkingTPlas = "TPlas 2019";
    private static string checkingWireTube = "Wire and Tube 2019";
    private static string checkingECGShowName = "ECG Symposium 2019";
    private static string checkingDoorScanningShowName1 = "SG-ANZICS 2019";//**14-4-2019
    private static string checkingDoorScanningShowName2 = "iSRRS 2019";//**14-4-2019
    private static string checkingDoorScanningShowName3 = "SG-ANZICS 2019 Workshop";//**14-4-2019
    private static string ReportSummaryByCountry = "Report Customized Summary By Country";//**31-5-2019
    private static string checkingMDAThailandMFTShowName = "MFT 2019";//***MDA Thailand
    private static string checkingMDAThailandPPIShowName = "PPI 2019";//***MDA Thailand
    private static string checkingMDAThailandTPlasShowName = "TPlas 2019";//***MDA Thailand
    private static string checkingMDAThailandWireTubeShowName = "Wire and Tube 2019";//***MDA Thailand
    private static string checkingMTLS = "MTLS";//***MDA Thailand
    private static string checkingIDEMShowName = "IDEM Online Registration";
    protected void Page_Load(object sender, EventArgs e)
    {
        //if(Session["id"] != null && Session["name"] != null && Session["role"] != null && Session["img"] != null)
        //{
        //    string id = Session["id"].ToString();
        //    string name = Session["name"].ToString();
        //    string role= Session["role"].ToString();
        //    lblusername1.Text = name;
        //    lblusername2.Text = name;
        //    lblusername3.Text = name;
        profileimg = "images/noimage.png";//Session["img"].ToString();
        //    if (role =="1")
        //    {
        //        #region Super Administrator
        //        lnkdashboard.Visible = true;
        //        lnkExhibitor.Visible = false;
        //        lnkVisitor.Visible = false;
        //        #endregion
        //    }
        //    else if(role =="2")//Exhibitor
        //    {
        //        #region Exhibitor
        //        lnkdashboard.Visible = false;
        //        lnkExhibitor.Visible = true;
        //        lnkVisitor.Visible = false;

        //        #endregion
        //    }
        //    else if (role == "3")//Visitor
        //    {
        //        #region Visitor
        //        lnkdashboard.Visible = false;
        //        lnkExhibitor.Visible = false;
        //        lnkVisitor.Visible = true;
        //        #endregion
        //    }
        //}
        //else
        //{
        //    Response.Redirect("../Login.aspx");
        //}
        try
        {
            if (Session["roleid"].ToString() != Number.One)
                lnkAdmin.Visible = false;
            if (Session["roleid"].ToString() == Number.Three)
            {
                lnkExhibitor.Visible = false;
                lnkRDelegateC.Visible = false;
                lnkRGroupC.Visible = false;
                if (Session["userid"] != null)
                {
                    int res = CheckGroupReg(Session["userid"].ToString());
                    if (res == 0)
                    {
                        lnkRGroup.Visible = false;
                        lnkRCompany.Visible = false;
                    }
                    else
                    {
                        if (hasCompanyReg(Session["userid"].ToString()))
                        {
                            lnkRCompany.Visible = true;
                        }
                    }
                }

                string[] showList = new string[2];
                showList = getShowList();
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showList[0]);

                lblShowName.Text = shw.SHW_Name;/*added on 24-4-2019 by th - display show name*/

                if (shw.SHW_Name == checkingMFAShowName || shw.SHW_Name == checkingMMAShowName || shw.SHW_Name == checkingOSHAShowName
                    || shw.SHW_Name == checkingMFT || shw.SHW_Name == checkingPPI || shw.SHW_Name == checkingTPlas || shw.SHW_Name == checkingWireTube)//MDA Thai shows
                {
                    lnkdashboardMDANewRepeat.Visible = true;
                    lnkTrackingReport.Visible = true;
                    if (shw.SHW_Name == checkingMFT || shw.SHW_Name == checkingPPI || shw.SHW_Name == checkingTPlas || shw.SHW_Name == checkingWireTube)
                    {
                        lnkMDAVisitorBMReport.Visible = true;
                    }
                }

                if (shw.SHW_Name == checkingVendorShowName)
                {
                    lnkVendorSPF.Visible = true;
                    lnkVendorPhotoList.Visible = true;
                    lnkVendorSummaryByCompany.Visible = true;
                }
                if (shw.SHW_Name == checkingANZIC || shw.SHW_Name== checkingiSRRS || shw.SHW_Name == checkingAPCCMI || shw.SHW_Name == checkingANZICWorkshopOnly)
                {
                    ConferenceFinancialSummary.Visible = true;
                    lnkSummaryByPackage.Visible = true;
                }

                #region set visibility of Visitor Upload Page or not added on 10-12-2019 th
                if (checkIsShowVisitorUploadPage(Session["userid"].ToString()))
                {
                    lnkUpload.Visible = true;
                }
                #endregion
                #region set visibility of Conference Upload Page or not added on 10-12-2019 th
                if (checkIsShowConferenceUploadPage(Session["userid"].ToString()))
                {
                    lnkUploadConference.Visible = true;
                }
                #endregion

                /*ECG*/
                if (shw.SHW_Name == checkingECGShowName)
                {
                    lnkRDelegateBreakDown.Visible = true;
                }

                if (checkingDoorScanningShowName1 == shw.SHW_Name || checkingDoorScanningShowName2 == shw.SHW_Name || checkingDoorScanningShowName3 == shw.SHW_Name)
                {
                    lnkDoorScanningReport.Visible = true;
                }

                /*added on 23-4-2019 by than - checking to show Items Breakdown Report*/
                bool isShowItemBreakdown = checkIsShowItemBreakdownMasterList(Session["userid"].ToString());
                if (isShowItemBreakdown)
                {
                    lnkRDelegateBreakDown.Visible = true;
                }
                /*added on 23-4-2019 by than - checking to show Items Breakdown report or not*/

                /*added on 17-7-2019 by than - checking to show Items Breakdown Report*/
                bool isShowItemBreakdownPriceQty = checkIsShowItemBreakdownPriceQtyMasterList(Session["userid"].ToString());
                if (isShowItemBreakdownPriceQty)
                {
                    lnkRItemBreakdown.Visible = true;
                }
                /*added on 17-7-2019 by than - checking to show Items Breakdown report or not*/

                /*added on 31-5-2019 by than - for MDA Thailand*/
                if (shw.SHW_Name == checkingMDAThailandMFTShowName || shw.SHW_Name == checkingMDAThailandPPIShowName
                    || shw.SHW_Name == checkingMDAThailandTPlasShowName || shw.SHW_Name == checkingMDAThailandWireTubeShowName)
                {
                    lnkSummaryByType.Visible = true;
                    lnkSummaryByOutcome.Visible = true;
                    //lnkUpload.Visible = true;
                }
                else
                {
                    lnkSummaryByType.Visible = false;
                    lnkSummaryByOutcome.Visible = false;
                }
                /*added on 31-5-2019 by than - for MDA Thailand*/

                if (shw.SHW_Name == checkingMTLS)
                {
                    SummaryByConf.Visible = true;
                }
            }

            if (Session["userid"] != null)
            {
                string dashboardUrl = getCustomizedMasterPage("", Session["userid"].ToString());
                if (!string.IsNullOrEmpty(dashboardUrl))
                {
                    aDashboard.HRef = dashboardUrl;
                    lnkdashboard.HRef = dashboardUrl;
                }

                /*added on 31-5-2019 by than - get customized page for Summary By Country Report (List and Pie Chart)*/
                string summarybycountryUrl = getCustomizedMasterPage(ReportSummaryByCountry, Session["userid"].ToString());
                if (!string.IsNullOrEmpty(summarybycountryUrl))
                {
                    lnkSummaryByCountry.HRef = summarybycountryUrl;
                }
                /*added on 31-5-2019 by than - get customized page for Summary By Country Report (List and Pie Chart)*/

                string delegateMasterList = getCustomizedMasterPage(BackendRegType.backendRegType_Delegate, Session["userid"].ToString());
                if (!string.IsNullOrEmpty(delegateMasterList) && !string.IsNullOrWhiteSpace(delegateMasterList))
                {
                    lnkRDelegate.HRef = delegateMasterList;
                }

                string[] showList = new string[2];
                showList = getShowList();
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showList[0]);
                string groupMasterList = getCustomizedMasterPage(BackendRegType.backendRegType_Group, Session["userid"].ToString());
                if (!string.IsNullOrEmpty(groupMasterList) && !string.IsNullOrWhiteSpace(groupMasterList))
                {
                    lnkRGroup.HRef = groupMasterList;
                }

                setPromoCodeVisible(shw.SHW_ID);/*(added on 11-7-2019 by th)*/

                if(checkingIDEMShowName.Contains(shw.SHW_Name))
                {
                    //lnkUploadConference.Visible = true;
                    lnkAccompanyingPersonList.Visible = true;
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
    }

    protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        Context.GetOwinContext().Authentication.SignOut();
    }

    private string getCustomizedMasterPage(string type, string userid)
    {
        string result = string.Empty;
        try
        {
            if (string.IsNullOrEmpty(type))
            {
                string sqlDashbord = "Select us_Dashboard_Cus From tb_Admin_Show Where us_userid='" + userid + "'";
                result = fn.GetDataByCommand(sqlDashbord, "us_Dashboard_Cus");
                if (result == "0")
                {
                    result = "";
                }
            }

            /*added on 31-5-2019 by than - get customized page for Summary By Country Report (List and Pie Chart)*/
            if (type == ReportSummaryByCountry)
            {
                string sqlSummaryByCountry = "Select us_SummaryByCountry_Cus From tb_Admin_Show Where us_userid='" + userid + "'";
                result = fn.GetDataByCommand(sqlSummaryByCountry, "us_SummaryByCountry_Cus");
                if (result == "0")
                {
                    result = "";
                }
            }
            /*added on 31-5-2019 by than - get customized page for Summary By Country Report (List and Pie Chart)*/

            if (type == BackendRegType.backendRegType_Delegate)
            {
                string sql = "Select us_DelegateBackendMaster_Cus From tb_Admin_Show Where us_userid='" + userid + "'";
                result = fn.GetDataByCommand(sql, "us_DelegateBackendMaster_Cus");
                if (result == "0")
                {
                    result = "";
                }
            }
            else if (type == BackendRegType.backendRegType_Group)
            {
                string sql = "Select us_GroupBackendMaster_Cus From tb_Admin_Show Where us_userid='" + userid + "'";
                result = fn.GetDataByCommand(sql, "us_GroupBackendMaster_Cus");
                if (result == "0")
                {
                    result = "";
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }

    private bool checkIsShowItemBreakdownMasterList(string userid)/*added on 23-4-2019 by than - checking in tb_Admin_Show table to show Items Breakdown Report or not*/
    {
        bool isShow = false;
        try
        {
            if (!string.IsNullOrEmpty(userid))
            {
                string sqlDashbord = "Select us_isShowItemBreakdownMasterList From tb_Admin_Show Where us_userid='" + userid + "'";
                string result = fn.GetDataByCommand(sqlDashbord, "us_isShowItemBreakdownMasterList");
                if (result == "1")
                {
                    isShow = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isShow;
    }
    private bool checkIsShowItemBreakdownPriceQtyMasterList(string userid)/*added on 23-4-2019 by than - checking in tb_Admin_Show table to show Items Breakdown Report or not*/
    {
        bool isShow = false;
        try
        {
            if (!string.IsNullOrEmpty(userid))
            {
                string sqlDashbord = "Select us_isShowItemBreakdownMasterList_PriceQty From tb_Admin_Show Where us_userid='" + userid + "'";
                string result = fn.GetDataByCommand(sqlDashbord, "us_isShowItemBreakdownMasterList_PriceQty");
                if (result == "1")
                {
                    isShow = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isShow;
    }

    private bool checkIsShowVisitorUploadPage(string userid)/*added on 10-12-2019 by than - checking in tb_Admin_Show table to show Visitor Upload Page or not*/
    {
        bool isShow = false;
        try
        {
            if (!string.IsNullOrEmpty(userid))
            {
                string sqlDashbord = "Select us_isShowBackendVisitorUploadPage From tb_Admin_Show Where us_userid='" + userid + "'";
                string result = fn.GetDataByCommand(sqlDashbord, "us_isShowBackendVisitorUploadPage");
                if (result == "1")
                {
                    isShow = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isShow;
    }
    private bool checkIsShowConferenceUploadPage(string userid)/*added on 10-12-2019 by than - checking in tb_Admin_Show table to show Conference Upload Page or not*/
    {
        bool isShow = false;
        try
        {
            if (!string.IsNullOrEmpty(userid))
            {
                string sqlDashbord = "Select us_isShowBackendConferenceUploadPage From tb_Admin_Show Where us_userid='" + userid + "'";
                string result = fn.GetDataByCommand(sqlDashbord, "us_isShowBackendConferenceUploadPage");
                if (result == "1")
                {
                    isShow = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isShow;
    }

    private int CheckGroupReg(string userid)
    {
        int res = 0;
        try
        {
            string sql = "select * from tb_site_flow_master where ShowID in (select us_showid from tb_Admin_Show where us_userid='" + userid + "') and Status='Active' and FLW_Type='G'";
            DataSet ds = new DataSet();
            ds = fn.GetDatasetByCommand(sql, "sqlCheckGroup");

            res = ds.Tables[0].Rows.Count;
        }
        catch (Exception ex) { }
        return res;

    }
    private bool hasCompanyReg(string userid)
    {
        bool res = false;
        try
        {
            string sql = "select * from tb_site_flow_master where ShowID in (select us_showid from tb_Admin_Show where us_userid='" + userid + "') and Status='Active' and FLW_Type='G'";
            DataSet ds = new DataSet();
            ds = fn.GetDatasetByCommand(sql, "sqlCheckGroup");
            if (ds.Tables[0].Rows.Count > 0)
            {
                FlowControler Flw = new FlowControler(fn);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string flowid = dr["FLW_ID"].ToString();
                    bool comExist = Flw.checkPageExist(flowid, SiteDefaultValue.constantRegCompany);
                    if (comExist)
                    {
                        res = true;
                    }
                }
            }
        }
        catch (Exception ex) { }
        return res;

    }

    protected string[] getShowList()
    {
        string showList = string.Empty;
        string showName = string.Empty;
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid='" + Session["userid"].ToString() + "'";
            DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];

            if (dt.Rows.Count > 0)
            {
                string[] showLists = dt.Rows[0]["us_showid"].ToString().Split(',');
                for (int i = 0; i < showLists.Length; i++)
                {
                    query = "Select * from tb_Show WHERE SHW_ID='" + showLists[i] + "'";
                    DataTable dtShow = fn.GetDatasetByCommand(query, "dsShow").Tables[0];

                    if (showName == string.Empty)
                    {
                        showName = dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                    else
                    {
                        showName += "," + dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                }
                list[0] = dt.Rows[0]["us_showid"].ToString();
                list[1] = showName;
                return list;
            }
            return list;
        }
        catch (Exception ex)
        {
            return list;
        }
    }

    #region setPromoCodeVisible (added on 11-7-2019 by th)
    private void setPromoCodeVisible(string showid)
    {
        string flowid = getPromoFlowID(showid);
        if (!string.IsNullOrEmpty(flowid))
        {
            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
            if (fmaster != null)
            {
                if (fmaster.usePromoCodeApplyInSummaryPage == Number.One)/*"1" means this flow is to show Promo Code in Summary Page*/
                {
                    lnkPromoCodeGenerator.Visible = true;
                    string step = fCtrl.GetConirmationStage(flowid, "Conference");
                    lnkPromoCodeGenerator.HRef = "Event_PromoGenerator?FLW=" + cFun.EncryptValue(flowid) + "&SHW=" + cFun.EncryptValue(showid) + "&STP=" + cFun.EncryptValue(step);
                }
            }
        }
    }
    private string getPromoFlowID(string showid)
    {
        string rtnFlowID = "";
        try
        {
            string sql = "Select usePromoCodeApplyInSummaryPage,* From tb_site_flow_master Where ShowID='" + showid + "' And usePromoCodeApplyInSummaryPage=1";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                rtnFlowID = dt.Rows[0]["FLW_ID"].ToString();
            }
        }
        catch (Exception ex)
        { }

        return rtnFlowID;
    }
    #endregion
}