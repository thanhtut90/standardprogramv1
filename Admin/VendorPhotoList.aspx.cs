﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using System.Text;
using Corpit.Site.Utilities;
using Corpit.Payment;
using System.IO;
using ClosedXML.Excel;
using Corpit.Site.Email;
using System.Text.RegularExpressions;
using Corpit.Logging;
using System.Globalization;
using System.Net;

public partial class Admin_VendorPhotoList : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    QuesFunctionality qfn = new QuesFunctionality();
    CommonFuns cFun = new CommonFuns();

    protected int count = 0;
    protected string htmltb;
    static StringBuilder StrBuilder = new StringBuilder();
    string showID = string.Empty;
    string flowID = string.Empty;
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string other_value = "Others";
    static string prostudent = "Student";
    static string proalliedhealth = "Allied Health";
    static string noInvoice = "-10";
    private static string[] checkingmdaFlowName = new string[] { "MFA2018iReg", "MFA2018GroupReg", "MMA2018iReg", "MMA2018GroupReg", "OSHA2018iReg", "OSHA2018GroupReg" };//***MDA
    private static string[] checkingBadgeShowName = new string[] { "VNU_AGRI", "VNU_HORTI" };
    private static string[] checkingReceiptInVisibleShowName = new string[] { "VNU_AGRI", "VNU_HORTI" };
    private static string[] checkingSARCShowName = new string[] { "SARC" };
    private static string[] checkingAcknowledgementMDAShowName = new string[] { "MFA 2018", "MMA 2018", "OSHA 2018" };//***MDA 31-7-2018
    private static string[] checkingFJShowName = new string[] { "Food Japan" };//***MDA 31-7-2018
    private static string[] checkingVendorRegistrationShowName = new string[] { "Vendor Registration" };//***Vendor Registration 17-9-2018
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (Session["userid"] != null)
                {
                    string userID = Session["userid"].ToString();

                    if (!string.IsNullOrEmpty(userID))
                    {
                        binddata();

                        if (Session["roleid"].ToString() != "1")
                        {
                            lblUser.Text = userID;
                            getShowID(userID);
                            showlist.Visible = false;


                            GKeyMaster.ExportSettings.ExportOnlyData = true;
                            GKeyMaster.ExportSettings.IgnorePaging = true;
                            GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                            GKeyMaster.ExportSettings.FileName = "IndividualReg_" + DateTime.Now.ToString("ddMMyyyy");


                            string constr = fn.ConnString;
                            using (SqlConnection con = new SqlConnection(constr))
                            {
                                using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID = '" + showID + "' and Status='Active'"))
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.Connection = con;
                                    con.Open();
                                    ddl_flowList.Items.Clear();
                                    ddl_flowList.DataSource = cmd.ExecuteReader();
                                    ddl_flowList.DataTextField = "FLW_Desc";
                                    ddl_flowList.DataValueField = "FLW_ID";
                                    ddl_flowList.DataBind();
                                    con.Close();
                                }
                            }
                        }
                        else
                            showID = ddl_showList.SelectedValue;
                    }
                    else
                    {
                        Response.Redirect("Event_Config");
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }

    protected void binddata()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select status_name,status_usedid From ref_Status"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_paymentStatus.Items.Clear();
                ddl_paymentStatus.DataSource = cmd.ExecuteReader();
                ddl_paymentStatus.DataTextField = "status_name";
                ddl_paymentStatus.DataValueField = "status_usedid";
                ddl_paymentStatus.DataBind();
                con.Close();

                ddl_paymentStatus.Items.Insert(0, new ListItem("All", "-1"));
            }

            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show where Status='Active'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_showList.Items.Clear();
                ddl_showList.DataSource = cmd.ExecuteReader();
                ddl_showList.DataTextField = "SHW_Name";
                ddl_showList.DataValueField = "SHW_ID";
                ddl_showList.DataBind();
                con.Close();
            }

            using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID = '" + ddl_showList.SelectedValue + "' and Status='Active'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_flowList.Items.Clear();
                ddl_flowList.DataSource = cmd.ExecuteReader();
                ddl_flowList.DataTextField = "FLW_Desc";
                ddl_flowList.DataValueField = "FLW_ID";
                ddl_flowList.DataBind();
                con.Close();
            }
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_RegDelegate and tb_Invoice tables for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID))
        {
            try
            {
                int status = Convert.ToInt16(ddl_paymentStatus.SelectedValue);

                MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
                msregIndiv.ShowID = showID;
                msregIndiv.FlowID = flowID;
                if (status == -1)
                {
                    //string sqlVendorPhotoList = "Select * From VendorPhotoList Where ShowID='" + showID + "' And PhotoApprovalStatus='Pending' Order By reg_datecreated Desc";
                    string sqlVendorPhotoList = @"SELECT        rd.Regno, rd.RegGroupID, rd.reg_FName, rd.reg_Email, rd.reg_PassNo, rd.reg_Profession, rd.Profession AS ProfessionName, rd.ShowID, rd.reg_urlFlowID, 
                                                CASE WHEN (reg_Status IS NULL OR
                                                reg_Status = 0) OR
                                                reg_approveStatus = 0 THEN 'Incomplete' WHEN reg_Status = 1 AND 
                                                reg_approveStatus = 33 THEN 'VIP' WHEN reg_approveStatus = 22 THEN 'Approved' WHEN reg_approveStatus = 11 THEN 'Approved' WHEN reg_approveStatus = 66
                                                THEN 'Reject' WHEN reg_approveStatus = 88 THEN 'Declined' END AS RegStatus, 
                                                CASE WHEN reg_isSMS = 0 THEN 'Pending' WHEN reg_isSMS = 1 THEN 'Approved' END AS PhotoApprovalStatus, rd.FileName, rd.reg_datecreated
                                                ,'https://www.event-reg.biz/registration/FileUpload/' + rd.ShowID + '/' + rd.FileName As imagePath
                                                ,rd.FileName As imageFileName
                                                From
                                                (Select r.* ,fu.FileName,p.Profession
                                                FROM            dbo.tb_RegDelegate AS r FULL OUTER JOIN
                                                dbo.ref_Status AS rs ON r.reg_Status = rs.status_id
                                                INNER JOIN
                                                dbo.ref_FileUpload AS fu ON r.Regno = fu.Regno AND r.ShowID = fu.ShowID LEFT OUTER JOIN
                                                dbo.ref_Profession AS p ON r.reg_Profession = p.ID
                                                WHERE     (r.recycle = 0) And  (fu.Type = 2) AND (fu.isDeleted = 0) AND (r.ShowID = p.ShowID)
                                                And r.ShowID='" + showID + "' And r.reg_isSMS = 0"
                                                + " Union "
                                                + " Select r.* ,fu.FileName,p.Profession "
                                                + " FROM            dbo.tb_RegDelegate AS r FULL OUTER JOIN"
                                                + " dbo.ref_Status AS rs ON r.reg_Status = rs.status_id"
                                                + " INNER JOIN"
                                                + " dbo.ref_FileUpload AS fu ON r.Regno = fu.Regno AND r.ShowID = fu.ShowID LEFT OUTER JOIN"
                                                + " dbo.ref_Profession AS p ON r.reg_Profession = p.ID"
                                                + " WHERE     (r.recycle = 0) And  (fu.Type = 2) AND (fu.isDeleted = 0) AND (r.ShowID = p.ShowID)"
                                                + " And r.ShowID= '" + showID + "' And r.reg_isSMS = 0"
                                                + " And fu.CreatedDate>(Select Top 1 ap_CreatedDate From tb_LogRegAction Where ap_Regno=fu.Regno Order By ap_CreatedDate Desc)) as rd"
                                                + " Order By reg_datecreated Desc";
                    //,'~/FileUpload/' + r.ShowID + '/' + fu.FileName As imagePath
                    DataTable dt = fn.GetDatasetByCommand(sqlVendorPhotoList, "ds").Tables[0];
                    GKeyMaster.DataSource = dt;
                }
                else
                {
                    DataTable dt = msregIndiv.getDataByPaymentStatus(status);
                    DataView dv = new DataView();
                    dv = dt.DefaultView;
                    dv.Sort = "reg_datecreated DESC";
                    dt = dv.ToTable();
                    GKeyMaster.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region checkPaymentMode
    private bool checkPaymentMode(string showID)
    {
        bool isUsedChequeTT = false;
        try
        {
            CommonDataObj cmdObj = new CommonDataObj(fn);
            DataTable dt = cmdObj.getPaymentMethods(showID);
            if (dt.Rows.Count > 0)
            {
                SiteSettings st = new SiteSettings(fn, showID);
                foreach (DataRow dr in dt.Rows)
                {
                    string usedid = dr["method_usedid"].ToString();
                    if (usedid == ((int)PaymentType.TT).ToString())
                    {
                        isUsedChequeTT = true;
                    }
                    if (usedid == ((int)PaymentType.Cheque).ToString())
                    {
                        isUsedChequeTT = true;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return isUsedChequeTT;
    }
    #endregion

    #region checkBadge
    private bool checkBadge(string showid)
    {
        bool isBadgeExist = false;
        try
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            if (checkingBadgeShowName.Contains(shw.SHW_Name))
            {
                isBadgeExist = true;
            }
        }
        catch (Exception ex)
        { }

        return isBadgeExist;
    }
    #endregion

    #region checkReceiptInVisible
    private bool checkReceiptInVisible(string showid)
    {
        bool isBadgeExist = true;
        try
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            if (checkingReceiptInVisibleShowName.Contains(shw.SHW_Name))
            {
                isBadgeExist = false;
            }
        }
        catch (Exception ex)
        { }

        return isBadgeExist;
    }
    #endregion

    #region Page_PreRender (Comment)
    protected void Page_PreRender(object o, EventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
            {
                getShowID(Session["userid"].ToString());
            }
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
        if (!string.IsNullOrEmpty(showID))
        {
            #region Delegate
            DataTable dtfrm = new DataTable();
            FormManageObj frmObj = new FormManageObj(fn);
            frmObj.showID = showID;
            frmObj.flowID = flowID;
            dtfrm = frmObj.getDynFormForDelegate().Tables[0];

            #region Declaration
            int vis_Fname = 0;
            int vis_PassNo = 0;
            int vis_Profession = 0;
            #endregion

            if (dtfrm.Rows.Count > 0)
            {
                for (int x = 0; x < dtfrm.Rows.Count; x++)
                {
                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Fname == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_FName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fname);
                                GKeyMaster.MasterTableView.GetColumn("reg_FName").HeaderText = labelname;

                                vis_Fname++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_FName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_PassNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_PassNo").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PassNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_PassNo").HeaderText = labelname;

                                vis_PassNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_PassNo").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Profession == 0)
                            {
                                //Show Nationality
                                GKeyMaster.MasterTableView.GetColumn("ProfessionName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                                GKeyMaster.MasterTableView.GetColumn("ProfessionName").HeaderText = labelname;

                                vis_Profession++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("ProfessionName").Display = false;
                        }
                    }
                }
            }

            ddl_paymentStatus_SelectedIndexChanged(this, EventArgs.Empty);
            #endregion
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;
            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
        if (!string.IsNullOrEmpty(showID) && !string.IsNullOrEmpty(flowID))
        {
            string regno = "";

            foreach (GridDataItem item in GKeyMaster.SelectedItems)
            {
                regno = item["Regno"].Text;
            }
            if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                //GridBoundColumn boundColumn;

                ////Important: first Add column to the collection 
                //boundColumn = new GridBoundColumn();
                //this.GKeyMaster.MasterTableView.Columns.Add(boundColumn);

                ////Then set properties 
                //boundColumn.DataField = "CustomerID";
                //boundColumn.HeaderText = "CustomerID";
                GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                GKeyMaster.ExportSettings.FileName = "DelegateList";
                GKeyMaster.ExportSettings.IgnorePaging = false;
                GKeyMaster.ExportSettings.ExportOnlyData = true;
                GKeyMaster.ExportSettings.OpenInNewWindow = true;
                GKeyMaster.MasterTableView.ExportToExcel();
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    /// <summary> 
    /// Item databound for the radgrid, set the header text here based on the form management ***Item[UniqueName]
    /// </summary>
    protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;
            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
        if (!string.IsNullOrEmpty(showID))
        {
            if (e.Item is GridHeaderItem)
            {
                GridHeaderItem item = e.Item as GridHeaderItem;

                #region Delegate
                DataTable dtfrm = new DataTable();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showID;
                frmObj.flowID = flowID;
                dtfrm = frmObj.getDynFormForDelegate().Tables[0];

                if (dtfrm.Rows.Count > 0)
                {
                    for (int x = 0; x < dtfrm.Rows.Count; x++)
                    {
                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_FName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_FName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fname);
                                }
                            }
                            else
                            {
                                (item["reg_FName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_PassNo"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_PassNo"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PassNo);
                                }
                            }
                            else
                            {
                                (item["reg_PassNo"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["ProfessionName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["ProfessionName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Profession);
                                }
                            }
                            else
                            {
                                (item["ProfessionName"].Controls[0] as LinkButton).Text = "";
                            }
                        }
                    }
                }
                #endregion
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    protected void GKeyMaster_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
            {
                getShowID(Session["userid"].ToString());
            }
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
        if (!string.IsNullOrEmpty(showID))
        {
            Session["pgno"] = GKeyMaster.CurrentPageIndex + 1;

            if (!string.IsNullOrEmpty(txtKey.Text) && !string.IsNullOrWhiteSpace(txtKey.Text))
            {
                btnKeysearch_Click(this, null);
            }
            else
            {
                //RememberSelected();
                if (string.IsNullOrEmpty(txtFromDate.Text.Trim()) || string.IsNullOrEmpty(txtToDate.Text.Trim()))
                {
                    //string sqlVendorPhotoList = "Select * From VendorPhotoList Where ShowID='" + showID + "' And PhotoApprovalStatus='Pending' Order By reg_datecreated Desc";
                    string sqlVendorPhotoList = @"SELECT        rd.Regno, rd.RegGroupID, rd.reg_FName, rd.reg_Email, rd.reg_PassNo, rd.reg_Profession, rd.Profession AS ProfessionName, rd.ShowID, rd.reg_urlFlowID, 
                                                CASE WHEN (reg_Status IS NULL OR
                                                reg_Status = 0) OR
                                                reg_approveStatus = 0 THEN 'Incomplete' WHEN reg_Status = 1 AND 
                                                reg_approveStatus = 33 THEN 'VIP' WHEN reg_approveStatus = 22 THEN 'Approved' WHEN reg_approveStatus = 11 THEN 'Approved' WHEN reg_approveStatus = 66
                                                THEN 'Reject' WHEN reg_approveStatus = 88 THEN 'Declined' END AS RegStatus, 
                                                CASE WHEN reg_isSMS = 0 THEN 'Pending' WHEN reg_isSMS = 1 THEN 'Approved' END AS PhotoApprovalStatus, rd.FileName, rd.reg_datecreated
                                                ,'https://www.event-reg.biz/registration/FileUpload/' + rd.ShowID + '/' + rd.FileName As imagePath
                                                ,rd.FileName As imageFileName
                                                From
                                                (Select r.* ,fu.FileName,p.Profession
                                                FROM            dbo.tb_RegDelegate AS r FULL OUTER JOIN
                                                dbo.ref_Status AS rs ON r.reg_Status = rs.status_id
                                                INNER JOIN
                                                dbo.ref_FileUpload AS fu ON r.Regno = fu.Regno AND r.ShowID = fu.ShowID LEFT OUTER JOIN
                                                dbo.ref_Profession AS p ON r.reg_Profession = p.ID
                                                WHERE     (r.recycle = 0) And  (fu.Type = 2) AND (fu.isDeleted = 0) AND (r.ShowID = p.ShowID)
                                                And r.ShowID='" + showID + "' And r.reg_isSMS = 0"
                                                + " Union "
                                                + " Select r.* ,fu.FileName,p.Profession "
                                                + " FROM            dbo.tb_RegDelegate AS r FULL OUTER JOIN"
                                                + " dbo.ref_Status AS rs ON r.reg_Status = rs.status_id"
                                                + " INNER JOIN"
                                                + " dbo.ref_FileUpload AS fu ON r.Regno = fu.Regno AND r.ShowID = fu.ShowID LEFT OUTER JOIN"
                                                + " dbo.ref_Profession AS p ON r.reg_Profession = p.ID"
                                                + " WHERE     (r.recycle = 0) And  (fu.Type = 2) AND (fu.isDeleted = 0) AND (r.ShowID = p.ShowID)"
                                                + " And r.ShowID= '" + showID + "' And r.reg_isSMS = 0"
                                                + " And fu.CreatedDate>(Select Top 1 ap_CreatedDate From tb_LogRegAction Where ap_Regno=fu.Regno Order By ap_CreatedDate Desc)) as rd"
                                                + " Order By reg_datecreated Desc";
                    //,'~/FileUpload/' + r.ShowID + '/' + fu.FileName As imagePath

                    DataTable dt = fn.GetDatasetByCommand(sqlVendorPhotoList, "ds").Tables[0];//msregIndiv.getDataAllRegList();
                    GKeyMaster.DataSource = dt;
                }
                else
                {
                    GKeyMaster.DataSource = getDataAllRegList(txtFromDate.Text.Trim(), txtToDate.Text.Trim());
                }
            }
        }
    }
    private void RememberSelected()
    {
        string strIds = ",";

        if (Session["selectedID"] != null)
        {
            strIds = Convert.ToString(Session["selectedID"]);
        }

        foreach (GridDataItem item in GKeyMaster.MasterTableView.Items)
        {
            CheckBox chkbx = (CheckBox)item["PhotoApprove"].FindControl("MyCheckBox");
            string regId = item.GetDataKeyValue("Regno").ToString();
            if (chkbx != null && chkbx.Checked)
            {
                string regNumber = ExtractNumber(regId);
                int regInt = 0;
                try
                {
                    int.TryParse(regNumber, out regInt);
                }
                catch (Exception ex)
                { }

                if (regInt > 0)
                {
                    if (txtSelectedRegno.Text.Contains(regInt.ToString()))
                    {
                        txtSelectedRegno.Text = txtSelectedRegno.Text.Replace(regInt + ",", "");
                    }
                    else
                    {
                        txtSelectedRegno.Text += regInt + ",";
                    }
                }
            }
            else
            {
                strIds = strIds.Replace("," + regId.ToString() + ",", ",");
            }
        }

        Session["selectedID"] = strIds;
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = e.Item as GridDataItem;
            CheckBox CheckBox1 = item.FindControl("MyCheckBox") as CheckBox;
            CheckBox1.Attributes.Add("onclick", "chkClick(this,'" + item.GetDataKeyValue("Regno").ToString() + "');");
        }
    }

    #region delRecord
    private int delRecord(string regno)
    {
        int isDeleted = 0;
        if (!string.IsNullOrEmpty(showID))
        {
            try
            {
                //isDeleted = fn.ExecuteSQL(string.Format("Update tb_RegGroup Set recycle=1 Where RegGroupID={0} And ShowID='{1}'", groupid, showid));
                isDeleted += fn.ExecuteSQL(string.Format("Update tb_RegDelegate Set recycle=1 Where Regno={0} And ShowID='{1}'", regno, showID));
            }
            catch (Exception ex)
            {

            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

        return isDeleted;
    }
    #endregion

    protected void ddl_paymentStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        //txtFromDate.Text = "";
        //txtToDate.Text = "";
        GKeyMaster.Rebind();
    }

    protected void ddl_showList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;

        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID = '" + showID + "' and Status='Active'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_flowList.Items.Clear();
                ddl_flowList.DataSource = cmd.ExecuteReader();
                ddl_flowList.DataTextField = "FLW_Desc";
                ddl_flowList.DataValueField = "FLW_ID";
                ddl_flowList.DataBind();
                con.Close();
            }
        }
        flowID = ddl_flowList.SelectedValue;

        FlowControler Flw = new FlowControler(fn);//***MDA
        FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowID);
        if (checkingmdaFlowName.Contains(flwMasterConfig.FlowName))
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") == null)
            {
                ddl_paymentStatus.Items.Add("Pre-populate");
                ddl_paymentStatus.Items[ddl_paymentStatus.Items.Count - 1].Value = "-20";
            }
            else
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = true;
            }
        }
        else
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") != null)
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = false;

                ddl_paymentStatus.SelectedIndex = 0;
            }
        }//***MDA

        GKeyMaster.Rebind();
    }

    protected void ddl_flowList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;
        flowID = ddl_flowList.SelectedValue;
        txtFromDate.Text = "";
        txtToDate.Text = "";
        FlowControler Flw = new FlowControler(fn);//***MDA
        FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowID);
        if (checkingmdaFlowName.Contains(flwMasterConfig.FlowName))
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") == null)
            {
                ddl_paymentStatus.Items.Add("Pre-populate");
                ddl_paymentStatus.Items[ddl_paymentStatus.Items.Count - 1].Value = "-20";
            }
            else
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = true;
            }
        }
        else
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") != null)
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = false;

                ddl_paymentStatus.SelectedIndex = 0;
            }
        }//***MDA

        GKeyMaster.Rebind();
    }

    protected void lnkExcel_Clicked(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                FlowControler Flw = new FlowControler(fn);
                flowID = ddl_flowList.SelectedValue;
                string QnaireID = GetQIDFromFLW(flowID);
                bool confExist = Flw.checkPageExist(flowID, SiteDefaultValue.constantConfName);
                if (!confExist)
                //if (showID != "EAD351")
                {
                    if (string.IsNullOrEmpty(QnaireID) || QnaireID == "0")
                    {
                        GKeyMaster.AllowPaging = false;
                        GKeyMaster.Rebind();
                        GKeyMaster.ExportSettings.ExportOnlyData = true;
                        GKeyMaster.ExportSettings.IgnorePaging = true;
                        GKeyMaster.ExportSettings.OpenInNewWindow = true;
                        GKeyMaster.MasterTableView.ExportToExcel();
                        //GKeyMaster.AllowPaging = true;
                        //GKeyMaster.Rebind();
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(QnaireID) || QnaireID == "0")
                    {
                        GKeyMaster.AllowPaging = false;
                        GKeyMaster.Rebind();
                        GKeyMaster.ExportSettings.ExportOnlyData = true;
                        GKeyMaster.ExportSettings.IgnorePaging = true;
                        GKeyMaster.ExportSettings.OpenInNewWindow = true;
                        //string flowName = ddl_flowList.SelectedItem.Text;
                        //string name = flowName.Replace(" ", "");
                        //GKeyMaster.ExportSettings.FileName = "Delegate-Detailed-Report-" + name;// + DateTime.Now.ToString("ddMMyyyy");
                        GKeyMaster.MasterTableView.ExportToExcel();
                        ////GKeyMaster.AllowPaging = true;
                        ////GKeyMaster.Rebind();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }
    }

    public string GetQIDFromFLW(string flowID)
    {
        string result = string.Empty;

        string query = "select SQ_QAID from tb_site_QA_Master where SQ_FLW_ID='" + flowID + "' and SQ_Category='D'";
        result = fn.GetDataByCommand(query, "SQ_QAID");

        return result;
    }

    public int GetQuestCount(string questID)
    {
        int Qcount = 0;
        string sql = "select * from gen_QuestItem where status='Active' and quest_id='" + questID + "'";
        DataSet ds = new DataSet();
        ds = qfn.GetDatasetByCommand(sql, "dsds");

        Qcount = ds.Tables[0].Rows.Count;

        return Qcount;

    }

    #region bindName
    public string bindSalutation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getSalutationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindProfession(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getProfessionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDepartment(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDepartmentNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindOrganisation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getOrganisationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindInstitution(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getInstitutionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindCountry(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    CountryObj conCtr = new CountryObj(fn);
                    name = conCtr.getCountryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindAffiliation(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getAffiliationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDietary(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDietaryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindIndustry(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getIndustryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    #region bindPhoneNo
    public string bindPhoneNo(string cc, string ac, string phoneno, string type)
    {
        string name = string.Empty;
        bool isShowCC = false, isShowAC = false, isShowPhoneNo = false;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string flowid = ddl_flowList.SelectedItem.Value;
                if (!string.IsNullOrEmpty(showID))
                {
                    DataSet ds = new DataSet();
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showID;
                    frmObj.flowID = flowid;
                    ds = frmObj.getDynFormForDelegate();

                    for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                    {
                        if (type == "Tel")
                        {
                            #region type="Tel"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                        else if (type == "Mob")
                        {
                            #region type="Mob"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                        else if (type == "Fax")
                        {
                            #region Type="Fax"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                    }

                    if (isShowCC)
                    {
                        name = "+" + cc;
                    }
                    if (isShowAC)
                    {
                        name += " " + ac;
                    }
                    if (isShowPhoneNo)
                    {
                        name += " " + phoneno;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion

    #region Invoice
    public string getInvoiceStatus(string invStatus)
    {
        string result = string.Empty;
        try
        {
            //InvoiceControler invControler = new InvoiceControler(fn);
            //StatusSettings stuSettings = new StatusSettings(fn);
            //string invStatus = invControler.getInvoiceStatus(regno);
            string sqlStatus = "select status_name from ref_Status Where status_usedid='" + invStatus + "'";
            result = fn.GetDataByCommand(sqlStatus, "status_name");
            if (result == "0" || string.IsNullOrEmpty(result))
            {
                result = "Pending";
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getPaymentMethod(string paymentMethod)
    {
        string result = string.Empty;
        try
        {
            string sqlStatus = "Select method_name From tmp_refPaymentMethod Where method_usedid='" + paymentMethod + "'";
            result = fn.GetDataByCommand(sqlStatus, "method_name");
            if (result == "0" || string.IsNullOrEmpty(result))
            {
                result = "N/A";
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getPaidPrice(string InvoiceID)
    {
        string result = string.Empty;
        try
        {
            PaymentControler pControl = new PaymentControler(fn);
            decimal totalPaidAmt = pControl.GetPaymentTotalPaidAmtByInvNo(InvoiceID);
            result = String.Format("{0:f2}", totalPaidAmt);
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string calculateOutstanding(string grandTotal, string InvoiceID)
    {
        string result = string.Empty;
        try
        {
            Double grandtotalAmount = 0;
            if (!string.IsNullOrEmpty(grandTotal))
            {
                Double.TryParse(grandTotal, out grandtotalAmount);
            }
            string paidprice = getPaidPrice(InvoiceID);
            Double paidAmount = 0;
            if (!string.IsNullOrEmpty(paidprice))
            {
                Double.TryParse(paidprice, out paidAmount);
            }
            result = String.Format("{0:f2}", (grandtotalAmount - paidAmount));
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getCongressSelection(string regno, string groupid, string invStatus)
    {
        string result = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                OrderControler oControl = new OrderControler(fn);
                //string currenturl= Request.Url.AbsoluteUri;
                string fullUrl = "GRP=" + cFun.EncryptValue(groupid) + "&INV=" + cFun.EncryptValue(regno) + "&SHW=" + cFun.EncryptValue(showID)
                    + "&FLW=" + cFun.EncryptValue(ddl_flowList.SelectedItem.Value);
                FlowURLQuery urlQuery = new FlowURLQuery(fullUrl);

                StatusSettings stuSettings = new StatusSettings(fn);
                if (invStatus == stuSettings.Pending.ToString()
                    || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString()
                    || invStatus == noInvoice)
                {
                    OrderItemList oList = new OrderItemList();
                    oList = oControl.GetPendingOrderList(urlQuery);//***Pending Order
                    result = getCongressSelectionString(oList);
                }
                else
                {
                    List<OrderItemList> oSucList = getPaidOrder(urlQuery);//***Paid Order
                    if (oSucList.Count > 0)
                    {
                        foreach (OrderItemList ordList in oSucList)
                        {
                            result = getCongressSelectionString(ordList);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    private List<OrderItemList> getPaidOrder(FlowURLQuery urlQuery)
    {
        List<OrderItemList> oPaidOrderItemList = new List<OrderItemList>();
        try
        {
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cFun.DecryptValue(urlQuery.DelegateID);
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj != null && invListObj.Count > 0)
            {
                foreach (Invoice invObj in invListObj)
                {
                    OrderControler oControl = new OrderControler(fn);
                    oPaidOrderItemList = oControl.GetAllOrderedListByInvoiceID(urlQuery, invObj.InvoiceID);
                }
            }
        }
        catch (Exception ex)
        { oPaidOrderItemList = new List<OrderItemList>(); }

        return oPaidOrderItemList;
    }
    private string getCongressSelectionString(OrderItemList OList)
    {
        string selectedCongress = string.Empty;
        try
        {
            foreach (OrderItem oItem in OList.OrderList)
            {
                selectedCongress += oItem.ItemDescription + ",";
            }
            selectedCongress = selectedCongress.TrimEnd(',');
        }
        catch (Exception ex)
        { }

        return selectedCongress;
    }
    public bool isPaymentVisible(string invStatus)
    {
        bool isVisible = false;
        try
        {
            StatusSettings stuSettings = new StatusSettings(fn);
            //invStatus = getInvoiceStatus(invStatus);
            if (invStatus == stuSettings.Pending.ToString() || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
            {
                isVisible = true;
            }
        }
        catch (Exception ex)
        { }

        return isVisible;
    }
    public bool isDownloadReceiptVisible(string invStatus)
    {
        bool isVisible = false;
        try
        {
            StatusSettings stuSettings = new StatusSettings(fn);
            //invStatus = getInvoiceStatus(invStatus);
            if (invStatus == stuSettings.Success.ToString())
            {
                isVisible = true;
            }
        }
        catch (Exception ex)
        { }

        return isVisible;
    }
    #endregion
    #endregion

    #region getPrepopulateDataByShowFlow
    private DataTable getPrepopulateDataByShowFlow(string showid, string flowid)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = fn.GetDatasetByCommand(string.Format("Select * From tb_RegPrePopulate Where (recycle=0 Or recycle Is Null) And ShowID='{0}' And reg_urlFlowID='{1}'"
                + " And (RefNo Is Not Null And RefNo<>'')"
                , showid, flowid), "ds").Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    #endregion

    #region Search
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;
            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()) && !string.IsNullOrEmpty(txtToDate.Text.Trim()))
            {
                string fromD = txtFromDate.Text.ToString();
                string toD = txtToDate.Text.ToString();
                int status = Convert.ToInt16(ddl_paymentStatus.SelectedValue);

                if (status == -1)
                {
                    GKeyMaster.DataSource = getDataAllRegList(fromD, toD);
                }
                else if (status == -20)//***MDA
                {
                    DataTable dtRegList = getDataAllRegList(fromD, toD);
                    DataTable dtPreRegList = getPrepopulateDataByShowFlow(showID, flowID);

                    DataTable TableC = dtRegList.AsEnumerable()
                            .Where(ra => dtPreRegList.AsEnumerable()
                                                .Any(rb => rb.Field<int>("RefNo") == ra.Field<int>("Regno")))
                            .CopyToDataTable();
                    GKeyMaster.DataSource = TableC;
                }//***MDA
                else
                    GKeyMaster.DataSource = getDataByPaymentStatus(status, fromD, toD);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please fill Date');", true);
                return;
            }
        }
        catch (Exception ex)
        { }
    }
    public DataTable getDataAllRegList(string sdate, string edate)
    {
        DataTable dt = new DataTable();

        try
        {
            string[] startdate1 = sdate.Split('/');
            //string startdate = startdate1[2] + "-" + startdate1[0] + "-" + startdate1[1];
            string startdate = startdate1[2] + "/" + startdate1[0] + "/" + startdate1[1];
            string[] enddate1 = edate.Split('/');
            //string enddate = enddate1[2] + "-" + enddate1[0] + "-" + enddate1[1];
            string enddate = enddate1[2] + "/" + enddate1[0] + "/" + enddate1[1];
            string query = "Select * From RegIndiv_All Where ShowID=@SHWID";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);

            if (!string.IsNullOrEmpty(flowID))
            {
                //query += " and reg_urlFlowID=@reg_urlFlowID and reg_datecreated  BETWEEN '" + startdate + "' and '" + enddate + "'";
                query += " and reg_urlFlowID=@reg_urlFlowID and reg_datecreated  BETWEEN '" + startdate + "' and DATEADD(s,-1,DATEADD(d,1,'" + enddate + "'))";
                spar = new SqlParameter("reg_urlFlowID", SqlDbType.NVarChar);
                spar.Value = flowID;
                pList.Add(spar);
            }

            dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    public DataTable getDataByPaymentStatus(int status, string sdate, string edate)
    {
        DataTable dt = new DataTable();

        try
        {
            string[] startdate1 = sdate.Split('/');
            string startdate = startdate1[2] + "-" + startdate1[0] + "-" + startdate1[1];
            string[] enddate1 = edate.Split('/');
            string enddate = enddate1[2] + "-" + enddate1[0] + "-" + enddate1[1];
            string query = "Select * From RegIndiv_All Where ShowID=@SHWID and Invoice_status=@Status";

            if (status == 3)
                query = "Select * From RegIndiv_All Where ShowID=@SHWID and (Invoice_status=@Status or (Invoice_status=-10 and reg_Status=1))";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);

            if (!string.IsNullOrEmpty(flowID))
            {
                query += " and reg_urlFlowID=@reg_urlFlowID and reg_datecreated  BETWEEN '" + startdate + "' and '" + enddate + "'";
                spar = new SqlParameter("reg_urlFlowID", SqlDbType.NVarChar);
                spar.Value = flowID;
                pList.Add(spar);
            }

            spar = new SqlParameter("Status", SqlDbType.Int);
            spar.Value = status;
            pList.Add(spar);

            dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    public string checkDateForExcel()
    {
        string start = txtFromDate.Text.Trim();
        string end = txtToDate.Text.Trim();
        string res = string.Empty;
        if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
        {
            res = "and reg_datecreated  BETWEEN '" + start + "' and '" + end + "'";
        }
        else
        {
            res = "";
        }
        return res;
    }
    #endregion

    /// <summary>
    /// added on 13-7-2018 th
    /// Keyword Search
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnKeysearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;

            MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
            msregIndiv.ShowID = showID;
            msregIndiv.FlowID = flowID;
            int status = Convert.ToInt16(ddl_paymentStatus.SelectedValue);
            if (!string.IsNullOrEmpty(txtKey.Text) && !string.IsNullOrWhiteSpace(txtKey.Text))
            {
                string key = txtKey.Text.Trim();
                DataTable dtIndiv = new DataTable();
                if (status == -1)
                {
                    if (Regex.IsMatch(key, @"^\d+$"))
                    {
                        //string sqlVendorPhotoList = "Select * From VendorPhotoList Where ShowID='" + showID + "' And PhotoApprovalStatus='Pending'"
                        //       + " And (Regno='" + key + "') Order By reg_datecreated Desc";
                        string sqlVendorPhotoList = @"SELECT        rd.Regno, rd.RegGroupID, rd.reg_FName, rd.reg_Email, rd.reg_PassNo, rd.reg_Profession, rd.Profession AS ProfessionName, rd.ShowID, rd.reg_urlFlowID, 
                                                CASE WHEN (reg_Status IS NULL OR
                                                reg_Status = 0) OR
                                                reg_approveStatus = 0 THEN 'Incomplete' WHEN reg_Status = 1 AND 
                                                reg_approveStatus = 33 THEN 'VIP' WHEN reg_approveStatus = 22 THEN 'Approved' WHEN reg_approveStatus = 11 THEN 'Approved' WHEN reg_approveStatus = 66
                                                THEN 'Reject' WHEN reg_approveStatus = 88 THEN 'Declined' END AS RegStatus, 
                                                CASE WHEN reg_isSMS = 0 THEN 'Pending' WHEN reg_isSMS = 1 THEN 'Approved' END AS PhotoApprovalStatus, rd.FileName, rd.reg_datecreated
                                                ,'https://www.event-reg.biz/registration/FileUpload/' + rd.ShowID + '/' + rd.FileName As imagePath
                                                ,rd.FileName As imageFileName
                                                From
                                                (Select r.* ,fu.FileName,p.Profession
                                                FROM            dbo.tb_RegDelegate AS r FULL OUTER JOIN
                                                dbo.ref_Status AS rs ON r.reg_Status = rs.status_id
                                                INNER JOIN
                                                dbo.ref_FileUpload AS fu ON r.Regno = fu.Regno AND r.ShowID = fu.ShowID LEFT OUTER JOIN
                                                dbo.ref_Profession AS p ON r.reg_Profession = p.ID
                                                WHERE     (r.recycle = 0) And  (fu.Type = 2) AND (fu.isDeleted = 0) AND (r.ShowID = p.ShowID)
                                                And r.ShowID='" + showID + "' And r.reg_isSMS = 0"
                                                + " Union "
                                                + " Select r.* ,fu.FileName,p.Profession "
                                                + " FROM            dbo.tb_RegDelegate AS r FULL OUTER JOIN"
                                                + " dbo.ref_Status AS rs ON r.reg_Status = rs.status_id"
                                                + " INNER JOIN"
                                                + " dbo.ref_FileUpload AS fu ON r.Regno = fu.Regno AND r.ShowID = fu.ShowID LEFT OUTER JOIN"
                                                + " dbo.ref_Profession AS p ON r.reg_Profession = p.ID"
                                                + " WHERE     (r.recycle = 0) And  (fu.Type = 2) AND (fu.isDeleted = 0) AND (r.ShowID = p.ShowID)"
                                                + " And r.ShowID= '" + showID + "' And r.reg_isSMS = 0"
                                                + " And fu.CreatedDate>(Select Top 1 ap_CreatedDate From tb_LogRegAction Where ap_Regno=fu.Regno Order By ap_CreatedDate Desc)) as rd"
                                                + " Where (Regno='" + key + "') Order By reg_datecreated Desc";
                        //,'~/FileUpload/' + r.ShowID + '/' + fu.FileName As imagePath

                        dtIndiv = fn.GetDatasetByCommand(sqlVendorPhotoList, "ds").Tables[0];//msregIndiv.getDataAllRegList();
                        GKeyMaster.DataSource = dtIndiv;
                        GKeyMaster.DataBind();
                    }
                    else
                    {
                        //string sqlVendorPhotoList = "Select * From VendorPhotoList Where ShowID='" + showID + "' And PhotoApprovalStatus='Pending'"
                        //    + " And (reg_FName like '%" + key + "%' Or reg_PassNo like '%" + key + "%') Order By reg_datecreated Desc";
                        string sqlVendorPhotoList = @"SELECT        rd.Regno, rd.RegGroupID, rd.reg_FName, rd.reg_Email, rd.reg_PassNo, rd.reg_Profession, rd.Profession AS ProfessionName, rd.ShowID, rd.reg_urlFlowID, 
                                                CASE WHEN (reg_Status IS NULL OR
                                                reg_Status = 0) OR
                                                reg_approveStatus = 0 THEN 'Incomplete' WHEN reg_Status = 1 AND 
                                                reg_approveStatus = 33 THEN 'VIP' WHEN reg_approveStatus = 22 THEN 'Approved' WHEN reg_approveStatus = 11 THEN 'Approved' WHEN reg_approveStatus = 66
                                                THEN 'Reject' WHEN reg_approveStatus = 88 THEN 'Declined' END AS RegStatus, 
                                                CASE WHEN reg_isSMS = 0 THEN 'Pending' WHEN reg_isSMS = 1 THEN 'Approved' END AS PhotoApprovalStatus, rd.FileName, rd.reg_datecreated
                                                ,'https://www.event-reg.biz/registration/FileUpload/' + rd.ShowID + '/' + rd.FileName As imagePath
                                                ,rd.FileName As imageFileName
                                                From
                                                (Select r.* ,fu.FileName,p.Profession
                                                FROM            dbo.tb_RegDelegate AS r FULL OUTER JOIN
                                                dbo.ref_Status AS rs ON r.reg_Status = rs.status_id
                                                INNER JOIN
                                                dbo.ref_FileUpload AS fu ON r.Regno = fu.Regno AND r.ShowID = fu.ShowID LEFT OUTER JOIN
                                                dbo.ref_Profession AS p ON r.reg_Profession = p.ID
                                                WHERE     (r.recycle = 0) And  (fu.Type = 2) AND (fu.isDeleted = 0) AND (r.ShowID = p.ShowID)
                                                And r.ShowID='" + showID + "' And r.reg_isSMS = 0"
                                                + " Union "
                                                + " Select r.* ,fu.FileName,p.Profession "
                                                + " FROM            dbo.tb_RegDelegate AS r FULL OUTER JOIN"
                                                + " dbo.ref_Status AS rs ON r.reg_Status = rs.status_id"
                                                + " INNER JOIN"
                                                + " dbo.ref_FileUpload AS fu ON r.Regno = fu.Regno AND r.ShowID = fu.ShowID LEFT OUTER JOIN"
                                                + " dbo.ref_Profession AS p ON r.reg_Profession = p.ID"
                                                + " WHERE     (r.recycle = 0) And  (fu.Type = 2) AND (fu.isDeleted = 0) AND (r.ShowID = p.ShowID)"
                                                + " And r.ShowID= '" + showID + "' And r.reg_isSMS = 0"
                                                + " And fu.CreatedDate>(Select Top 1 ap_CreatedDate From tb_LogRegAction Where ap_Regno=fu.Regno Order By ap_CreatedDate Desc)) as rd"
                                                + " Where (reg_FName like '%" + key + "%' Or reg_PassNo like '%" + key + "%') Order By reg_datecreated Desc";
                        //,'~/FileUpload/' + r.ShowID + '/' + fu.FileName As imagePath

                        dtIndiv = fn.GetDatasetByCommand(sqlVendorPhotoList, "ds").Tables[0];//msregIndiv.getDataAllRegList();
                        GKeyMaster.DataSource = dtIndiv;
                        GKeyMaster.DataBind();
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    private string getCountriesByName(string countryName)
    {
        string result = string.Empty;
        try
        {
            string sql = "SELECT TOP 1 Substring ((SELECT ', ' + Convert(nvarchar,Cty_GUID) + '' FROM ref_country"
                        + " WHERE Country Like '%" + countryName + "%' And Cty_GUID = dbo.ref_country.Cty_GUID FOR XML PATH('')), 2, 200000) AS countryIDs";
            result = fn.GetDataByCommand(sql, "countryIDs");
            if(string.IsNullOrEmpty(result))
            {
                result = "0";
            }
        }
        catch(Exception ex)
        { }

        return result;
    }

    protected void ddlSortBy_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    #region Vendor
    //**Added Function for Image 03-Oct-2018
    public string getImgPath(string regno)
    {
        string name = string.Empty;
        SetUpController setupCtr = new SetUpController(fn);
        DataTable dtUpload = setupCtr.getFileUploadByID(regno, showID, FileUploadType.delegatePhotoImg);
        try
        {
            if (dtUpload.Rows.Count > 0)
            {
                string imgfilename = dtUpload.Rows[0]["FileName"].ToString();

                if (!string.IsNullOrEmpty(imgfilename) && imgfilename != Number.Zero)
                {
                    //string urihost = HttpContext.Current.Request.Url.Authority;
                    //string path = urihost + "/FileUpload/" + showID;
                    string path = "~/FileUpload/" + showID;
                    name = path + "/" + imgfilename;
                }
            }
        }
        catch (Exception ex) { }
        return name;
    }
    public bool getImgVisible(string regno)
    {
        bool isVisible = false;
        SetUpController setupCtr = new SetUpController(fn);
        DataTable dtUpload = setupCtr.getFileUploadByID(regno, showID, FileUploadType.delegatePhotoImg);
        try
        {
            if (dtUpload.Rows.Count > 0)
            {
                string imgfilename = dtUpload.Rows[0]["FileName"].ToString();

                if (!string.IsNullOrEmpty(imgfilename) && imgfilename != Number.Zero)
                {
                    string path = "~/FileUpload/" + showID;
                    string imgPath = path + "/" + imgfilename;
                    if (File.Exists(Server.MapPath(imgPath)))
                    {
                        isVisible = true;
                    }
                }
            }
        }
        catch (Exception ex) { }
        return isVisible;
    }
    private void updatePhotoApproveStatus(string regno, string groupid, string flowid, string showid)
    {
        try
        {
            string status = "1";
            string updateApprove = "Update tb_RegDelegate Set reg_isSMS=" + status + " Where Regno='" + regno + (!string.IsNullOrEmpty(groupid) ? ("' And RegGroupID='" + groupid) : "")
                                    + "' And reg_urlFlowID='" + flowid + "' And ShowID='" + showid + "'";
            fn.ExecuteSQL(updateApprove);

            //////Log Update Status
            string sqlInsert = "Insert Into tb_LogRegAction (ap_Regno,ShowID,ap_ActionType,ap_Action) Values (@ap_Regno,@ShowID,@ap_ActionType,@ap_Action)";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ap_Regno", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("ap_ActionType", SqlDbType.NVarChar);
            SqlParameter spar4 = new SqlParameter("ap_Action", SqlDbType.NVarChar);
            spar1.Value = regno;
            spar2.Value = showid;
            spar3.Value = "Photochk";
            spar4.Value = status;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);
            pList.Add(spar4);
            fn.ExecuteSQLWithParameters(sqlInsert, pList);
            //////
        }
        catch(Exception ex)
        { }
    }
    protected void btnPhotoApproved_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;

                flowID = ddl_flowList.SelectedValue;

                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                if (!string.IsNullOrEmpty(regno) && !string.IsNullOrEmpty(groupid))
                {
                    updatePhotoApproveStatus(regno, groupid, flowID, showID);
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Approved.');window.location.href='VendorPhotoList.aspx';", true);
                //return;
            }
            catch(Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    protected void MyCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox CheckBox1 = sender as CheckBox;

        string regId = CheckBox1.Text;
        string regNumber = ExtractNumber(regId);
        int regInt = 0;
        try
        {
            int.TryParse(regNumber, out regInt);
        }
        catch (Exception ex)
        { }

        if (regInt > 0)
        {
            if (txtSelectedRegno.Text.Contains(regInt.ToString()))
            {
                txtSelectedRegno.Text = txtSelectedRegno.Text.Replace(regInt + ",", "");
            }
            else
            {
                txtSelectedRegno.Text += regInt + ",";
            }
        }

        if (!string.IsNullOrEmpty(txtSelectedRegno.Text) && !string.IsNullOrWhiteSpace(txtSelectedRegno.Text) && txtSelectedRegno.Text != ",")
        {
            txtSelectedRegno.Visible = true;
        }
        else
        {
            txtSelectedRegno.Visible = false;
        }
    }
    public string ExtractNumber(string original)
    {
        return new string(original.Where(c => Char.IsDigit(c)).ToArray());
    }
    protected void btnApprovedPhoto_Click(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;

                flowID = ddl_flowList.SelectedValue;

                #region loop Grid (Comment)
                //foreach (GridDataItem item in GKeyMaster.MasterTableView.Items)
                //{
                //    CheckBox chkbx = (CheckBox)item["PhotoApprove"].FindControl("MyCheckBox");
                //    if (chkbx.Checked)
                //    {
                //        string regId = chkbx.Text;
                //        Response.End();
                //        string groupid = "";
                //        if (!string.IsNullOrEmpty(regId))
                //        {
                //            updatePhotoApproveStatus(regId, groupid, flowID, showID);
                //        }
                //    }
                //}
                #endregion

                #region with selected in text box
                if (!string.IsNullOrEmpty(txtSelectedRegno.Text) && !string.IsNullOrWhiteSpace(txtSelectedRegno.Text) && txtSelectedRegno.Text != ",")
                {
                    string selectedIDs = txtSelectedRegno.Text.Trim(',');

                    string[] arrRegistrations = selectedIDs.Split(',');

                    if (arrRegistrations.Length < 1)
                    {
                        string strMessage = "No Registrations Detected.";
                        string strScript = "<script language='javascript'>";
                        strScript += "alert('" + strMessage + "');";
                        strScript += "</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "clientScript", strScript, false);
                    }
                    else
                    {
                        int[] arrIntRegistrations = new int[arrRegistrations.Length];
                        int i = 0;
                        foreach (String regId in arrRegistrations)
                        {
                            int.TryParse(regId, out arrIntRegistrations[i]);
                            ++i;
                            //Response.End();
                            string groupid = "";
                            if (!string.IsNullOrEmpty(regId))
                            {
                                updatePhotoApproveStatus(regId, groupid, flowID, showID);
                            }
                        }
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Approved selected records.');window.location.href='VendorPhotoList.aspx';", true);
                    }
                }
                else
                {
                    string strMessage = "No Registrations Detected.";
                    string strScript = "<script language='javascript'>";
                    strScript += "alert('" + strMessage + "');";
                    strScript += "</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "clientScript", strScript, false);
                }
                #endregion
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
}
