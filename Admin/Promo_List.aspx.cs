﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using Corpit.Utilities;

public partial class Admin_Promo_List : System.Web.UI.Page
{
    #region Declaration
    static string _promoid;
    public static string promoid
    {
        get
        {
            return _promoid;
        }
        set
        {
            _promoid = value;
        }
    }

    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

                if (Request.Params["promoid"] != null)
                {
                    promoid = Request.QueryString["promoid"];
                }

                bindPromo(showid);

                string flowid = string.Empty;
                if (Request.Params["FLW"] != null)
                {
                    flowid = Request.QueryString["FLW"].ToString();
                }

                if (Request.Params["FLW"] != null && Request.Params["STP"] != null && Request.Params["CAT"] != null)
                {
                    string url = string.Empty;

                    if(Request.Params["a"] == null)
                    {
                        url = "Event_Conference_Promo?SHW=" + Request.Params["SHW"].ToString() + "&FLW=" + Request.Params["FLW"].ToString() + "&STP=" + Request.Params["STP"].ToString() + "&CAT=" + Request.Params["CAT"].ToString();
                    }
                    else
                    {
                        url = "Event_Conference_Promo?SHW=" + Request.Params["SHW"].ToString() + "&FLW=" + Request.Params["FLW"].ToString() + "&STP=" + Request.Params["STP"].ToString() + "&CAT=" + Request.Params["CAT"].ToString() + "&a=" + Request.QueryString["a"].ToString();
                    }
                    hplBack.NavigateUrl = url;
                }
                else
                {
                    hplBack.NavigateUrl = "#";
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region bindPromo
    protected void bindPromo(string showid)
    {
        string query = "Select * From tb_PromoList Where PromocodeID=" + promoid + " And ShowID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand(query, "Ds", pList);
        if (ds.Tables[0].Rows.Count != 0)
        {
            GKeyMaster.DataSource = ds;
            GKeyMaster.DataBind();
        }
    }
    #endregion

    #region setStatus
    public string setStatus(string promocode)
    {
        int count = Convert.ToInt16(fn.GetDataByCommand("Select Count(*) as cnt From tb_User_Promo Where Promo_Code='" + promocode + "'","cnt"));
        string status = string.Empty;
        if (count != 0)
        {
            status = "Used";
        }
        else
        {
            status = "Available";
        }
        return status;
    }
    #endregion

    #region setColor
    public Color setColor(string promocode)
    {
        int count = Convert.ToInt16(fn.GetDataByCommand("Select count (*) as cnt From tb_User_Promo Where Promo_Code='" + promocode + "'", "cnt"));
        System.Drawing.Color status = System.Drawing.Color.Green;
        if (count != 0)
        {
            status = System.Drawing.Color.Red;
        }
        else
        {
        }
        return status;
    }
    #endregion

    #region setRegno
    public string setRegno(string promocode)
    {
        string regno = fn.GetDataByCommand("Select Regno From tb_User_Promo Where Promo_Code='" + promocode + "'", "regno");
        if (regno == "0")
        {
            regno = "N/A";
        }
        return regno;
    }
    #endregion
}