﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Settings_PaymentSettings.aspx.cs" Inherits="Admin_Event_Settings_PaymentSettings" %>
<%@ MasterType VirtualPath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <h1 class="box-title">2-3. Manage Payment Settings</h1>

    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="Event" class="subcontent">
                <asp:Panel ID="pnlmessagebar" runat="server" CssClass="notibar announcement" Visible="false">
                    <p class="txtgreen">
                        <asp:Label ID="lblemsg" runat="server" Text="Label"></asp:Label>
                    </p>
                </asp:Panel>

                <div class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group" style="display:none;">
                            <label class="col-md-2 control-label">Show Name</label> 
                            <div class="col-md-6">
                                <asp:TextBox ID="txtEventName" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                              
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Payment Portal Ref ID</label>
                            <div class="col-md-6">
                               <asp:DropDownList runat="server" ID="txtProjectID" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="PaymentPortal_OnChange"></asp:DropDownList> <asp:LinkButton runat="server" ID="btnCreateNewPPortal" OnClick="btnCreateNewPPortal_Onclick" Text="Create New Payment Ref" ></asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-md-2 control-label">Event Currency</label>
                            <div class="col-md-6"> 
                                 <asp:TextBox ID="txtCurrency" runat="server" CssClass="form-control" ReadOnly="true" Text="SGD"></asp:TextBox>
                               
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label class="col-md-2 control-label">Admin Fee</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtAdminFee" runat="server" CssClass="form-control" MaxLength="3"></asp:TextBox>
                                %
                                <asp:FilteredTextBoxExtender ID="ftbAdminFee" runat="server" TargetControlID="txtAdminFee" FilterType="Numbers, Custom" ValidChars="." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">GST Fee</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtGSTFee" runat="server" CssClass="form-control" MaxLength="3"></asp:TextBox>%
                                <asp:FilteredTextBoxExtender ID="ftb" runat="server" TargetControlID="txtGSTFee" FilterType="Numbers, Custom" ValidChars="." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Telegraphic Admin Fee</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtTTAdminFee" runat="server" CssClass="form-control" MaxLength="3"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTTAdminFee" runat="server" TargetControlID="txtTTAdminFee" FilterType="Numbers, Custom" ValidChars="." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Payment Method</label>
                            <div class="col-md-6">
                                <%--<asp:CheckBoxList ID="chkPaymentMethod" runat="server" RepeatDirection="Vertical"></asp:CheckBoxList>--%>
                                <asp:CheckBox ID="chkCreditCard" runat="server" Text="Credit Card" />
                                <br />
                                <CKEditor:CKEditorControl ID="txtCreditCard" runat="server" Height="200px" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                <br />
                                <asp:CheckBox ID="chkTT" runat="server" Text="Telegraphic Transfer" />
                                <br />
                                <CKEditor:CKEditorControl ID="txtTT" runat="server" Height="200px" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                <br />
                                <asp:CheckBox ID="chkWaived" runat="server" Text="Waived" />
                                <br />
                                <CKEditor:CKEditorControl ID="txtWaived" runat="server" Height="200px" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                <br />
                                <asp:CheckBox ID="chkCheque" runat="server" Text="Cheque" />
                                <br />
                                <CKEditor:CKEditorControl ID="txtCheque" runat="server" Height="200px" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Allow Partial Payment?</label>
                            <div class="col-md-6">
                                <asp:RadioButton ID="rbPartial1" runat="server" GroupName="rbpartial" Text="No" />
                                <asp:RadioButton ID="rbPartial2" runat="server" GroupName="rbpartial" Text="Yes" />
                            </div>
                        </div>
                        <div class="form-group" runat="server" visible="false">
                            <label class="col-md-2 control-label">Max Promo Usable Item Per Transaction for Promo Code Calculation</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtMaxItemPerTrans" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbMaxItemPerTrans" runat="server" TargetControlID="txtMaxItemPerTrans" FilterType="Numbers" ValidChars="0123456789" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2 control-label"></div>
                            <div class="col-md-6">
                                <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back" CssClass="btn btn-primary" />
                                &nbsp;
                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="SaveForm" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

