﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Flow_Mangaement.aspx.cs" Inherits="Admin_EVent_Flow_Manage" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="css/style.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="row" style="line-height:30px; text-align:center;">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-box">
            <div class="f1-steps">
                <div class="f1-progress">
                    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 64.99%;"></div>
                </div>
                <div class="f1-step activated">
                    <div class="f1-step-icon"><i class="fa fa-plus"></i></div>
                    <p style="text-align:center">Create Event</p>
                </div>
                <div class="f1-step activated">
                    <div class="f1-step-icon"><i class="fa fa-gear"></i></div>
                    <p style="text-align:center">Event Configuration</p>
                </div>
                <div class="f1-step active">
                    <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                    <p style="text-align:center">Registration SetUp</p>
                </div>
                <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-check-circle-o"></i></div>
                    <p style="text-align:center">Finished</p>
                </div>
            </div>
        </div>
    </div>

    <div class="centercontent">    

        <div id="contentwrapper" class="contentwrapper" style="padding-top: 50px;">
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                            <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>

            <div style="padding-bottom:10px;" class="row">
              <div class="col-lg-2 col-lg-offset-9">
                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="btnCreateFlow_Onclick" CssClass="btn btn-danger btn-block btn-lg">
                    <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Add New Registration
                </asp:LinkButton>
                  </div>
            </div>

            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
                <asp:Panel runat="server" ID="PanelKeyList">
                      <h4 class="pagetitle" style="padding-bottom: 20px; text-decoration: underline; font-weight: bold;">Existing Registration(s)</h4>
                    <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                        EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                        OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="10" Skin="Bootstrap">
                        <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="Flw_ID" ShowHeader="false">
                            <Columns>
                                <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Flw_ID" FilterControlAltText="Filter Flw_ID"
                                    HeaderText="Flow  ID" SortExpression="Flw_ID" UniqueName="Flw_ID" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="FLW_Desc" FilterControlAltText="Filter FLW_Desc"
                                    HeaderText="Detail Description" SortExpression="FLW_Desc" UniqueName="FLW_Desc" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>

                                <telerik:GridButtonColumn CommandArgument="id" Text="Edit"
                                    ConfirmDialogType="RadWindow" CommandName="EditFlow" ButtonType="LinkButton" UniqueName="EditFlow"
                                    HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                </telerik:GridButtonColumn>

                                <telerik:GridTemplateColumn ItemStyle-HorizontalAlign="Center" HeaderText="Browse">
                                    <ItemTemplate>
                                        <a href='http://203.125.155.188/StandardRegWeb_2016/Welcome?SHW=<%#Eval("ShowID")%>&FLW=<%#Eval("Flw_ID")%>' target="_blank"> Browse..</a>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </asp:Panel>
                <br />
                <br />

                <asp:Panel runat="server" ID="PanelKeyDetail">
                    <div class='page-header'>
                        <h2>Flow Steps</h2>
                        <div style="padding-top: 40px;"></div>
                        <div style="padding-left: 30%;">
                            <asp:LinkButton ID="btnCreateFlow" runat="server" OnClick="btnCreateFlow_Onclick" CssClass="btn btn-danger">
                                <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Edit
                            </asp:LinkButton>

                            <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
                        </div>
                    </div>
                </asp:Panel>
            </telerik:RadAjaxPanel>
        </div>
    </div>
</asp:Content>

