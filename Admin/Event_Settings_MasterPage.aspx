﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Settings_MasterPage.aspx.cs" Inherits="Admin_Event_Settings_MasterPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="css/style.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="row" style="line-height: 30px; text-align: center;">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-box">
            <div class="f1-steps">
                <div class="f1-progress">
                    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 40.99%;"></div>
                </div>
                <div class="f1-step activated">
                    <div class="f1-step-icon"><i class="fa fa-plus"></i></div>
                    <p style="text-align: center">Create Event</p>
                </div>
                <div class="f1-step active">
                    <div class="f1-step-icon"><i class="fa fa-gear"></i></div>
                    <p style="text-align: center">Event Configuration</p>
                </div>
                <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-key"></i></div>
                    <p style="text-align: center">Registration SetUp</p>
                </div>
                <div class="f1-step">
                    <div class="f1-step-icon"><i class="fa fa-check-circle-o"></i></div>
                    <p style="text-align: center">Finished</p>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel runat="server" ID="Panel1">
        <br />
        <h4 class="box-title SubTitle">Event Name :
            <asp:Label ID="lbl_eventName" runat="server" Text=""></asp:Label></h4>
        <br />

        <table class="table table-hover" style="font-size: 15px;">
            <tbody>
                <tr>
                    <td>Site Template Setup</td>
                    <td>
                        <asp:LinkButton ID="btn_editWebSetting" runat="server" OnClick="btn_editWebSetting_Click">Edit..</asp:LinkButton>
                    </td>
                    <td>
                        <asp:Image ID="img_webSetting" runat="server" Width="25" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 37px">Events Settings</td>

                    <td style="height: 37px">
                        <asp:LinkButton ID="btn_editEventSetting" runat="server" OnClick="btn_editEventSetting_Click">Edit..</asp:LinkButton>
                    </td>
                    <td>
                        <asp:Image ID="img_eventSetting" runat="server" Width="25" />
                    </td>
                </tr>
                <tr>
                    <td>Payment Settings</td>
                    <td>
                        <asp:LinkButton ID="btn_editPaymentSetting" runat="server" OnClick="btn_editPaymentSetting_Click">Edit..</asp:LinkButton>
                    </td>

                    <td>
                        <asp:Image ID="img_paymentSetting" runat="server" Width="25" />
                    </td>
                </tr>
                <tr>
                    <td>Email Settings</td>
                    <td>
                        <asp:LinkButton ID="btn_editEmailSetting" runat="server" OnClick="btn_editEmailSetting_Click">Edit..</asp:LinkButton>
                    </td>

                    <td>
                        <asp:Image ID="img_emailSetting" runat="server" Width="25" />
                    </td>
                </tr>
            </tbody>
        </table>
        <br />
       
        <div class="row">
            <div class="col-lg-2 col-lg-offset-9">
                <asp:Button ID="btn_navToEventFlow" runat="server" Text="Next" OnClick="NavEventFlowForm" CssClass="btn btn-block btn-primary btn-lg" />
            </div>
        </div>  
    </asp:Panel>
</asp:Content>

