﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Group_MemberList.aspx.cs" Inherits="Admin_Group_MemberList" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <link rel="stylesheet" href="Content/bootstrap/css/bootstrap.min.css" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"/>
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"/>
  <!-- daterange picker -->
  <link rel="stylesheet" href="Content/plugins/daterangepicker/daterangepicker-bs3.css"/>
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="Content/plugins/datepicker/datepicker3.css"/>
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="Content/plugins/iCheck/all.css"/>
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="Content/plugins/colorpicker/bootstrap-colorpicker.min.css"/>
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="Content/plugins/timepicker/bootstrap-timepicker.min.css"/>
  <!-- Select2 -->
  <link rel="stylesheet" href="Content/plugins/select2/select2.min.css"/>
  <!-- Theme style -->
  <link rel="stylesheet" href="Content/dist/css/AdminLTE.min.css"/>
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="Content/dist/css/skins/_all-skins.min.css"/>  
    <title></title>
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }
    </script>
    <style type="text/css">
        
        .tdstyle1
        {
            width:170px;
        }

        .tdstyle2
        {
            width:300px;
        }

        form input[type="text"]
        {
            width:39% !important;
        }

        .ajax__calendar_container
        {
            width: 320px !important;
            height:280px !important;
        }

        .ajax__calendar_body
        {
            width: 100% !important;
            height:100% !important;
        }

        td
        {
            vertical-align:middle;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
         <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
         <br />
&nbsp;&nbsp;&nbsp;
       <asp:LinkButton ID="btnAddNew" runat="server" CssClass="btn btn-success" OnClick="lnkExcel_Clicked">
                        <span aria-hidden="true" class="glyphicon glyphicon-export"></span> EXPORT EXCEL
                        </asp:LinkButton>
        <ContentTemplate>
    <h3 class="box-title">Member List of Group Registration<asp:Label ID="lblGroupID" runat="server" Text=""></asp:Label>.</h3>
    <br />
           
  
    <div style="overflow-x:scroll;">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientEvents-OnRequestStart="onRequestStart">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnupdate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
        <asp:Panel runat="server" ID="PanelKeyList">
            <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                EnableHeaderContextFilterMenu="true" AllowPaging="true" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                onneeddatasource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="5" OnItemDataBound="grid_ItemDataBound">
                <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true"></Selecting>
                </ClientSettings>
                <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="Regno,RegGroupID">
                    <CommandItemSettings ShowExportToExcelButton="False"  ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                    <Columns>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RegGroupID" FilterControlAltText="Filter RegGroupID"
                            HeaderText="RegGroupID" SortExpression="RegGroupID" UniqueName="RegGroupID">
                        </telerik:GridBoundColumn>

                       <%-- <telerik:GridButtonColumn CommandName="View" ButtonType="ImageButton" ImageUrl="images/member.png" UniqueName="ViewContactPerson"
                            HeaderText="View Contact Person" ItemStyle-HorizontalAlign="Center">
                        </telerik:GridButtonColumn>

                        <telerik:GridButtonColumn CommandName="Edit" ButtonType="ImageButton" ImageUrl="images/edit.jpg" UniqueName="Edit"
                            HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                        </telerik:GridButtonColumn>--%>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Regno" FilterControlAltText="Filter Regno"
                            HeaderText="Regno" SortExpression="Regno" UniqueName="Regno">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Salutation" FilterControlAltText="Filter Salutation"
                            HeaderText="" SortExpression="Salutation" UniqueName="reg_Salutation">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_FName" FilterControlAltText="Filter reg_FName"
                            HeaderText="" SortExpression="reg_FName" UniqueName="reg_FName">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_LName" FilterControlAltText="Filter reg_LName"
                            HeaderText="" SortExpression="reg_LName" UniqueName="reg_LName">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_OName" FilterControlAltText="Filter reg_OName"
                            HeaderText="" SortExpression="reg_OName" UniqueName="reg_OName">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_PassNo" FilterControlAltText="Filter reg_PassNo"
                            HeaderText="" SortExpression="reg_PassNo" UniqueName="reg_PassNo">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_isReg" FilterControlAltText="Filter reg_isReg"
                            HeaderText="" SortExpression="reg_isReg" UniqueName="reg_isReg">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_sgregistered" FilterControlAltText="Filter reg_sgregistered"
                            HeaderText="" SortExpression="reg_sgregistered" UniqueName="reg_sgregistered">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_IDno" FilterControlAltText="Filter reg_IDno"
                            HeaderText="" SortExpression="reg_IDno" UniqueName="reg_IDno">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Designation" FilterControlAltText="Filter reg_Designation"
                            HeaderText="" SortExpression="reg_Designation" UniqueName="reg_Designation">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="ProfessionName" FilterControlAltText="Filter ProfessionName"
                            HeaderText="" SortExpression="ProfessionName" UniqueName="reg_Profession">
                        </telerik:GridBoundColumn>

                        <%--<telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Profession" UniqueName="reg_Profession"
                            FilterControlAltText="Filter reg_Profession">
                            <ItemTemplate>
                                <asp:Label ID="lbl_profession" runat="server">
                                    <%#!string.IsNullOrEmpty(Eval("reg_otherProfession").ToString()) ? Eval("reg_Profession").ToString() + ": " + Eval("reg_otherProfession").ToString() : Eval("reg_Profession").ToString()%></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>--%>

                        <%--<telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Jobtitle_alliedstu" FilterControlAltText="Filter reg_Jobtitle_alliedstu"
                            HeaderText="Allied Health JobTitle" SortExpression="reg_Jobtitle_alliedstu" UniqueName="reg_Jobtitle_alliedstu">
                        </telerik:GridBoundColumn>--%>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="DepartmentName" FilterControlAltText="Filter DepartmentName"
                            HeaderText="" SortExpression="DepartmentName" UniqueName="reg_Department">
                        </telerik:GridBoundColumn>

                        <%--<telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Department" UniqueName="reg_Department"
                            FilterControlAltText="Filter reg_Department">
                            <ItemTemplate>
                                <asp:Label ID="lbl_department" runat="server">
                                    <%#!string.IsNullOrEmpty(Eval("reg_otherDepartment").ToString()) ? Eval("reg_Department").ToString() + ": " + Eval("reg_otherDepartment").ToString() : Eval("reg_Department").ToString()%></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>--%>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="OrganizationName" FilterControlAltText="Filter OrganizationName"
                            HeaderText="" SortExpression="OrganizationName" UniqueName="reg_Organization">
                        </telerik:GridBoundColumn>

                        <%--<telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Organization" UniqueName="reg_Organization"
                            FilterControlAltText="Filter reg_Organization">
                            <ItemTemplate>
                                <asp:Label ID="lbl_organization" runat="server">
                                    <%#!string.IsNullOrEmpty(Eval("reg_otherOrganization").ToString()) ? Eval("reg_Organization").ToString() + ": " + Eval("reg_otherOrganization").ToString() : Eval("reg_Organization").ToString()%></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>--%>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="InstitutionName" FilterControlAltText="Filter InstitutionName"
                            HeaderText="" SortExpression="InstitutionName" UniqueName="reg_Institution">
                        </telerik:GridBoundColumn>

                        <%--<telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Institution" UniqueName="reg_Institution"
                            FilterControlAltText="Filter reg_Institution">
                            <ItemTemplate>
                                <asp:Label ID="lbl_institution" runat="server">
                                    <%#!string.IsNullOrEmpty(Eval("reg_otherInstitution").ToString()) ? Eval("reg_Institution").ToString() + ": " + Eval("reg_otherInstitution").ToString() : Eval("reg_Institution").ToString()%></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>--%>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address1" FilterControlAltText="Filter reg_Address1"
                            HeaderText="" SortExpression="reg_Address1" UniqueName="reg_Address1">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address2" FilterControlAltText="Filter reg_Address2"
                            HeaderText="" SortExpression="reg_Address2" UniqueName="reg_Address2">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address3" FilterControlAltText="Filter reg_Address3"
                            HeaderText="" SortExpression="reg_Address3" UniqueName="reg_Address3">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address4" FilterControlAltText="Filter reg_Address4"
                            HeaderText="" SortExpression="reg_Address4" UniqueName="reg_Address4">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_City" FilterControlAltText="Filter reg_City"
                            HeaderText="" SortExpression="reg_City" UniqueName="reg_City">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_State" FilterControlAltText="Filter reg_State"
                            HeaderText="" SortExpression="reg_State" UniqueName="reg_State">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_PostalCode" FilterControlAltText="Filter reg_PostalCode"
                            HeaderText="" SortExpression="reg_PostalCode" UniqueName="reg_PostalCode">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RegCountry" FilterControlAltText="Filter RegCountry"
                            HeaderText="" SortExpression="RegCountry" UniqueName="reg_Country">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_RCountry" FilterControlAltText="Filter reg_RCountry"
                            HeaderText="" SortExpression="reg_RCountry" UniqueName="reg_RCountry">
                        </telerik:GridBoundColumn>

                        <telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Tel" UniqueName="reg_Tel"
                            FilterControlAltText="Filter reg_Tel">
                            <ItemTemplate>
                                <asp:Label ID="lbl_tel" runat="server"><%# Eval("reg_Telcc")%><%#Eval("reg_Telac")%><%#Eval("reg_Tel")%></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Mobile" UniqueName="reg_Mobile"
                            FilterControlAltText="Filter reg_Mobile">
                            <ItemTemplate>
                                <asp:Label ID="lbl_mobile" runat="server"><%# Eval("reg_Mobcc")%><%#Eval("reg_Mobac")%><%#Eval("reg_Mobile")%></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Fax" UniqueName="reg_Fax"
                            FilterControlAltText="Filter reg_Fax">
                            <ItemTemplate>
                                <asp:Label ID="lbl_fax" runat="server"><%# Eval("reg_Faxcc")%><%# Eval("reg_Faxac")%><%#Eval("reg_Fax")%></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Email" FilterControlAltText="Filter reg_Email"
                            HeaderText="" SortExpression="reg_Email" UniqueName="reg_Email">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="AffiliationName" FilterControlAltText="Filter AffiliationName"
                            HeaderText="" SortExpression="AffiliationName" UniqueName="reg_Affiliation">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="DietaryName" FilterControlAltText="Filter DietaryName"
                            HeaderText="" SortExpression="DietaryName" UniqueName="reg_Dietary">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Nationality" FilterControlAltText="Filter reg_Nationality"
                            HeaderText="" SortExpression="reg_Nationality" UniqueName="reg_Nationality">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Age" FilterControlAltText="Filter reg_Age"
                            HeaderText="" SortExpression="reg_Age" UniqueName="reg_Age">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_DOB" FilterControlAltText="Filter reg_DOB"
                            HeaderText="" SortExpression="reg_DOB" UniqueName="reg_DOB">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Gender" FilterControlAltText="Filter reg_Gender"
                            HeaderText="" SortExpression="reg_Gender" UniqueName="reg_Gender">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Membershipno" FilterControlAltText="Filter reg_Membershipno"
                            HeaderText="" SortExpression="reg_Membershipno" UniqueName="reg_Membershipno">
                        </telerik:GridBoundColumn>


                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vName" FilterControlAltText="Filter reg_vName"
                            HeaderText="" SortExpression="reg_vName" UniqueName="reg_vName">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vDOB" FilterControlAltText="Filter reg_vDOB"
                            HeaderText="" SortExpression="reg_vDOB" UniqueName="reg_vDOB">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vPassno" FilterControlAltText="Filter reg_vPassno"
                            HeaderText="" SortExpression="reg_vPassno" UniqueName="reg_vPassno">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vIssueDate" FilterControlAltText="Filter reg_vIssueDate"
                            HeaderText="" SortExpression="reg_vIssueDate" UniqueName="reg_vIssueDate">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vPassexpiry" FilterControlAltText="Filter reg_vPassexpiry"
                            HeaderText="" SortExpression="reg_vPassexpiry" UniqueName="reg_vPassexpiry">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vEmbarkation" FilterControlAltText="Filter reg_vEmbarkation"
                            HeaderText="" SortExpression="reg_vEmbarkation" UniqueName="reg_vEmbarkation">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vArrivalDate" FilterControlAltText="Filter reg_vArrivalDate"
                            HeaderText="" SortExpression="reg_vArrivalDate" UniqueName="reg_vArrivalDate">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vCountry" FilterControlAltText="Filter reg_vCountry"
                            HeaderText="" SortExpression="reg_vCountry" UniqueName="reg_vCountry">
                        </telerik:GridBoundColumn>


                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CName" FilterControlAltText="Filter UDF_CName"
                            HeaderText="" SortExpression="UDF_CName" UniqueName="UDF_CName">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_DelegateType" FilterControlAltText="Filter UDF_DelegateType"
                            HeaderText="" SortExpression="UDF_DelegateType" UniqueName="UDF_DelegateType">
                        </telerik:GridBoundColumn>

                        <telerik:GridTemplateColumn HeaderText="" SortExpression="UDF_ProfCategory" UniqueName="UDF_ProfCategory"
                            FilterControlAltText="Filter UDF_ProfCategory">
                            <ItemTemplate>
                                <asp:Label ID="lbl_udfprocat" runat="server"><%# Eval("UDF_ProfCategory")%><%#Eval("UDF_ProfCategoryOther")%></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CPcode" FilterControlAltText="Filter UDF_CPcode"
                            HeaderText="" SortExpression="UDF_CPcode" UniqueName="UDF_CPcode">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CLDepartment" FilterControlAltText="Filter UDF_CLDepartment"
                            HeaderText="" SortExpression="UDF_CLDepartment" UniqueName="UDF_CLDepartment">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CAddress" FilterControlAltText="Filter UDF_CAddress"
                            HeaderText="" SortExpression="UDF_CAddress" UniqueName="UDF_CAddress">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CLCompany" FilterControlAltText="Filter UDF_CLCompany"
                            HeaderText="" SortExpression="UDF_CLCompany" UniqueName="UDF_CLCompany">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CCountry" FilterControlAltText="Filter UDF_CCountry"
                            HeaderText="" SortExpression="UDF_CCountry" UniqueName="UDF_CCountry">
                        </telerik:GridBoundColumn>


                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorName" FilterControlAltText="Filter reg_SupervisorName"
                            HeaderText="" SortExpression="reg_SupervisorName" UniqueName="reg_SupervisorName">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorDesignation" FilterControlAltText="Filter reg_SupervisorDesignation"
                            HeaderText="" SortExpression="reg_SupervisorDesignation" UniqueName="reg_SupervisorDesignation">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorContact" FilterControlAltText="Filter reg_SupervisorContact"
                            HeaderText="" SortExpression="reg_SupervisorContact" UniqueName="reg_SupervisorContact">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorEmail" FilterControlAltText="Filter reg_SupervisorEmail"
                            HeaderText="" SortExpression="reg_SupervisorEmail" UniqueName="reg_SupervisorEmail">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Additional4" FilterControlAltText="Filter reg_Additional4"
                            HeaderText="" SortExpression="reg_Additional4" UniqueName="reg_Additional4">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Additional5" FilterControlAltText="Filter reg_Additional5"
                            HeaderText="" SortExpression="reg_Additional5" UniqueName="reg_Additional5">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_datecreated" FilterControlAltText="Filter reg_datecreated"
                            HeaderText="Created Date" SortExpression="reg_datecreated" UniqueName="reg_datecreated">
                        </telerik:GridBoundColumn>

                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </asp:Panel>
    </telerik:RadAjaxPanel>
    </div>
        </ContentTemplate>
        </asp:UpdatePanel>

    </form>
    </body></html>

