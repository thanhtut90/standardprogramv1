﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="FormManagement_Company.aspx.cs" Inherits="Admin_FormManagement_Company" ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        input[type="checkbox"], input[type="radio"]
        {
            margin : 4px !important;
        }
        label {
    font-weight: 400 !important;
}
    </style>

    <script type="text/javascript">
        function showField(type) {
            if (type == 'name') {
                var cbox = document.getElementById('<%=chkshowName.ClientID %>');
                var content1 = document.getElementById('dvname');
                var content2 = document.getElementById('<%=txtName.ClientID %>');
                var content3 = document.getElementById('dvsumname');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'address1') {
                var cbox = document.getElementById('<%=chkshowAddress1.ClientID %>');
                var content1 = document.getElementById('dvaddress1');
                var content2 = document.getElementById('<%=txtAddress1.ClientID %>');
                var content3 = document.getElementById('dvsumaddress1');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'address2') {
                var cbox = document.getElementById('<%=chkshowAddress2.ClientID %>');
                var content1 = document.getElementById('dvaddress2');
                var content2 = document.getElementById('<%=txtAddress2.ClientID %>');
                var content3 = document.getElementById('dvsumaddress2');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'address3') {
                var cbox = document.getElementById('<%=chkshowAddress3.ClientID %>');
                var content1 = document.getElementById('dvaddress3');
                var content2 = document.getElementById('<%=txtAddress3.ClientID %>');
                var content1 = document.getElementById('dvsumaddress3');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'city') {
                var cbox = document.getElementById('<%=chkshowCity.ClientID %>');
                var content1 = document.getElementById('dvcity');
                var content2 = document.getElementById('<%=txtCity.ClientID %>');
                var content3 = document.getElementById('dvsumcity');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'state') {
                var cbox = document.getElementById('<%=chkshowState.ClientID %>');
                var content1 = document.getElementById('dvstate');
                var content2 = document.getElementById('<%=txtState.ClientID %>');
                var content3 = document.getElementById('dvsumstate');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'zipcode') {
                var cbox = document.getElementById('<%=chkshowZipCode.ClientID %>');
                var content1 = document.getElementById('dvzipcode');
                var content2 = document.getElementById('<%=txtZipCode.ClientID %>');
                var content3 = document.getElementById('dvsumzipcode');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'country') {
                var cbox = document.getElementById('<%=chkshowCountry.ClientID %>');
                var content1 = document.getElementById('dvcountry');
                var content2 = document.getElementById('<%=txtCountry.ClientID %>');
                var content3 = document.getElementById('dvsumcountry');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'telcc') {
                var cbox = document.getElementById('<%=chkshowTelCC.ClientID %>');
                var content1 = document.getElementById('dvtelcc');
                var content2 = document.getElementById('<%=txtTelCC.ClientID %>');
                var content3 = document.getElementById('dvsumtelcc');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'telac') {
                var cbox = document.getElementById('<%=chkshowTelAC.ClientID %>');
                var content1 = document.getElementById('dvtelac');
                var content2 = document.getElementById('<%=txtTelAC.ClientID %>');
                var content3 = document.getElementById('dvsumtelac');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'tel') {
                var cbox = document.getElementById('<%=chkshowTel.ClientID %>');
                var content1 = document.getElementById('dvtel');
                var content2 = document.getElementById('<%=txtTel.ClientID %>');
                var content3 = document.getElementById('dvsumtel');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'faxcc') {
                var cbox = document.getElementById('<%=chkshowFaxCC.ClientID %>');
                var content1 = document.getElementById('dvfaxcc');
                var content2 = document.getElementById('<%=txtFaxCC.ClientID %>');
                var content3 = document.getElementById('dvsumfaxcc');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
           
            else if (type == 'faxac') {
                var cbox = document.getElementById('<%=chkshowFaxAC.ClientID %>');
                var content1 = document.getElementById('dvfaxac');
                var content2 = document.getElementById('<%=txtFaxAC.ClientID %>');
                var content3 = document.getElementById('dvsumfaxac');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'fax') {
                var cbox = document.getElementById('<%=chkshowFax.ClientID %>');
                var content1 = document.getElementById('dvfax');
                var content2 = document.getElementById('<%=txtFax.ClientID %>');
                var content3 = document.getElementById('dvsumfax');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'email') {
                var cbox = document.getElementById('<%=chkshowEmail.ClientID %>');
                var content1 = document.getElementById('dvemail');
                var content2 = document.getElementById('<%=txtEmail.ClientID %>');
                var content3 = document.getElementById('dvsumemail');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'emailconfirmation') {
                var cbox = document.getElementById('<%=chkshowEmailConfirmation.ClientID %>');
                var content1 = document.getElementById('dvemailconfirmation');
                var content2 = document.getElementById('<%=txtEmailConfirmation.ClientID %>');
                var content3 = document.getElementById('dvsumemailconfirmation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'website') {
                var cbox = document.getElementById('<%=chkshowWebsite.ClientID %>');
                var content1 = document.getElementById('dvwebsite');
                var content2 = document.getElementById('<%=txtWebsite.ClientID %>');
                var content3 = document.getElementById('dvsumwebsite');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'additional1') {
                var cbox = document.getElementById('<%=chkshowAdditional1.ClientID %>');
                var content1 = document.getElementById('dvadditional1');
                var content2 = document.getElementById('<%=txtAdditional1.ClientID %>');
                var content3 = document.getElementById('dvsumadditional1');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'additional2') {
                var cbox = document.getElementById('<%=chkshowAdditional2.ClientID %>');
                var content1 = document.getElementById('dvadditional2');
                var content2 = document.getElementById('<%=txtAdditional2.ClientID %>');
                var content3 = document.getElementById('dvsumadditional2');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'additional3') {
                var cbox = document.getElementById('<%=chkshowAdditional3.ClientID %>');
                var content1 = document.getElementById('dvadditional3');
                var content2 = document.getElementById('<%=txtAdditional3.ClientID %>');
                var content3 = document.getElementById('dvsumadditional3');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'additional4') {
                var cbox = document.getElementById('<%=chkshowAdditional4.ClientID %>');
                var content1 = document.getElementById('dvadditional4');
                var content2 = document.getElementById('<%=txtAdditional4.ClientID %>');
                var content3 = document.getElementById('dvsumadditional4');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'additional5') {
                var cbox = document.getElementById('<%=chkshowAdditional5.ClientID %>');
                var content1 = document.getElementById('dvadditional5');
                var content2 = document.getElementById('<%=txtAdditional5.ClientID %>');
                var content3 = document.getElementById('dvsumadditional5');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h3 class="box-title">Manage Form (Company)</h3>

    Select the field to be used for the registration form.
    <br />
    <br />

    <div class="row">
        <div class="col-md-6">
            <div class="box-body">
                <div class="alert alert-success alert-dismissible" id="divMsg" runat="server" visible="false">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <asp:Label ID="lblMsg" runat="server" ></asp:Label>
                </div>
            </div>
        </div>
    </div>

    <%--<p style="text-align:right;"><a class="ds_next" href='<%= ResolveClientUrl("~/Register.aspx") %>' target="_blank">Go To Registration Form</a></p>--%>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Field Name</th>
                <th>Compulsory Field</th>
                <th>Field Display Text</th>
                <th>Display in Summary & Report</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><asp:CheckBox ID="chkshowName" runat="server" Text=" Name" onclick="showField('name');" /></td>
                <td><div id="dvname" style="display:none;"><asp:CheckBox ID="chkcompName" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtName" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumname" style="display:none;"><asp:CheckBox ID="chksumName" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAddress1" runat="server" Text=" Address1" onclick="showField('address1');" /></td>
                <td><div id="dvaddress1" style="display:none;"><asp:CheckBox ID="chkcompAddress1" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAddress1" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaddress1" style="display:none;"><asp:CheckBox ID="chksumAddress1" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAddress2" runat="server" Text=" Address2" onclick="showField('address2');" /></td>
                <td><div id="dvaddress2" style="display:none;"><asp:CheckBox ID="chkcompAddress2" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAddress2" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaddress2" style="display:none;"><asp:CheckBox ID="chksumAddress2" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAddress3" runat="server" Text=" Address3" onclick="showField('address3');" /></td>
                <td><div id="dvaddress3" style="display:none;"><asp:CheckBox ID="chkcompAddress3" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAddress3" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaddress3" style="display:none;"><asp:CheckBox ID="chksumAddress3" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowCity" runat="server" Text=" City" onclick="showField('city');" /></td>
                <td><div id="dvcity" style="display:none;"><asp:CheckBox ID="chkcompCity" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtCity" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumcity" style="display:none;"><asp:CheckBox ID="chksumCity" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowState" runat="server" Text=" State" onclick="showField('state');" /></td>
                <td><div id="dvstate" style="display:none;"><asp:CheckBox ID="chkcompState" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtState" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumstate" style="display:none;"><asp:CheckBox ID="chksumState" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowZipCode" runat="server" Text=" Zip Code" onclick="showField('zipcode');" /></td>
                <td><div id="dvzipcode" style="display:none;"><asp:CheckBox ID="chkcompZipCode" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtZipCode" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumzipcode" style="display:none;"><asp:CheckBox ID="chksumZipCode" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowCountry" runat="server" Text=" Country" onclick="showField('country');" />
                      (<a href="ManageCountry.aspx" class="text-danger" target="_blank" >Manage Country</a>)
                </td>
                <td><div id="dvcountry" style="display:none;"><asp:CheckBox ID="chkcompCountry" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtCountry" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumcountry" style="display:none;"><asp:CheckBox ID="chksumCountry" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowTelCC" runat="server" Text=" TelCC" onclick="showField('telcc');" /></td>
                <td><div id="dvtelcc" style="display:none;"><asp:CheckBox ID="chkcompTelCC" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtTelCC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumtelcc" style="display:none;"><asp:CheckBox ID="chksumTelCC" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowTelAC" runat="server" Text=" TelAC" onclick="showField('telac');" /></td>
                <td><div id="dvtelac" style="display:none;"><asp:CheckBox ID="chkcompTelAC" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtTelAC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumtelac" style="display:none;"><asp:CheckBox ID="chksumTelAC" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowTel" runat="server" Text=" Tel" onclick="showField('tel');" /></td>
                <td><div id="dvtel" style="display:none;"><asp:CheckBox ID="chkcompTel" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtTel" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumtel" style="display:none;"><asp:CheckBox ID="chksumTel" runat="server" Text="  Display in Summary & Report" /></div></td>

            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowFaxCC" runat="server" Text=" FaxCC" onclick="showField('faxcc');" /></td>
                <td><div id="dvfaxcc" style="display:none;"><asp:CheckBox ID="chkcompFaxCC" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtFaxCC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumfaxcc" style="display:none;"><asp:CheckBox ID="chksumFaxCC" runat="server" Text="  Display in Summary & Report" /></div></td>

            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowFaxAC" runat="server" Text=" FaxAC" onclick="showField('faxac');" /></td>
                <td><div id="dvfaxac" style="display:none;"><asp:CheckBox ID="chkcompFaxAC" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtFaxAC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumfaxac" style="display:none;"><asp:CheckBox ID="chksumFaxAC" runat="server" Text="  Display in Summary & Report" /></div></td>

            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowFax" runat="server" Text=" Fax" onclick="showField('fax');" /></td>
                <td><div id="dvfax" style="display:none;"><asp:CheckBox ID="chkcompFax" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtFax" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumfax" style="display:none;"><asp:CheckBox ID="chksumFax" runat="server" Text="  Display in Summary & Report" /></div></td>

            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowEmail" runat="server" Text=" Email" onclick="showField('email');" /></td>
                <td><div id="dvemail" style="display:none;"><asp:CheckBox ID="chkcompEmail" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtEmail" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumemail" style="display:none;"><asp:CheckBox ID="chksumEmail" runat="server" Text="  Display in Summary & Report" /></div></td>

            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowEmailConfirmation" runat="server" Text=" Email Confirmation" onclick="showField('emailconfirmation');" /></td>
                <td><div id="dvemailconfirmation" style="display:none;"><asp:CheckBox ID="chkcompEmailConfirmation" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtEmailConfirmation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumemailconfirmation" style="display:none;"><asp:CheckBox ID="chksumEmailConfirmation" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowWebsite" runat="server" Text=" Website" onclick="showField('website');" /></td>
                <td><div id="dvwebsite" style="display:none;"><asp:CheckBox ID="chkcompWebsite" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtWebsite" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumwebsite" style="display:none;"><asp:CheckBox ID="chksumWebsite" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAdditional1" runat="server" Text=" Additional1" onclick="showField('additional1');" /></td>
                <td><div id="dvadditional1" style="display:none;"><asp:CheckBox ID="chkcompAdditional1" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAdditional1" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumadditional1" style="display:none;"><asp:CheckBox ID="chksumAdditional1" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAdditional2" runat="server" Text=" Additional2" onclick="showField('additional2');" /></td>
                <td><div id="dvadditional2" style="display:none;"><asp:CheckBox ID="chkcompAdditional2" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAdditional2" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumadditional2" style="display:none;"><asp:CheckBox ID="chksumAdditional2" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAdditional3" runat="server" Text=" Additional3" onclick="showField('additional3');" /></td>
                <td><div id="dvadditional3" style="display:none;"><asp:CheckBox ID="chkcompAdditional3" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAdditional3" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumadditional3" style="display:none;"><asp:CheckBox ID="chksumAdditional3" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAdditional4" runat="server" Text=" Additional4" onclick="showField('additional4');" /></td>
                <td><div id="dvadditional4" style="display:none;"><asp:CheckBox ID="chkcompAdditional4" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAdditional4" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumadditional4" style="display:none;"><asp:CheckBox ID="chksumAdditional4" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAdditional5" runat="server" Text=" Additional5" onclick="showField('additional5');" /></td>
                <td><div id="dvadditional5" style="display:none;"><asp:CheckBox ID="chkcompAdditional5" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAdditional5" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumadditional5" style="display:none;"><asp:CheckBox ID="chksumAdditional5" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
        </tbody>
    </table>
    <asp:Button ID="btnSave" runat="server" Text="Save and Next" OnClick="SaveForm" CssClass="btn btn-primary"/>
    &nbsp;
    <asp:Button ID="btnSavePreview" runat="server" Text="Save And Preview" OnClick="btnSavePreview_Click" CssClass="btn btn-primary" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>
</asp:Content>

