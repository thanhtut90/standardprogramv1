﻿using Corpit.BackendMaster;
using Corpit.Logging;
using Corpit.Payment;
using Corpit.Registration;
using Corpit.Site.Email;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_PaymentUpdate : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected static string _paymentLogRemark = " Payment Status Updating";
    protected static string _backendPayment = "Backend Payment";
    protected string currency;

    protected void Page_Load(object sender, EventArgs e)
    {
        string InvoiceId = "";

        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string flwID = cFun.DecryptValue(urlQuery.FlowID);
            string GroupRegID = cFun.DecryptValue(urlQuery.GoupRegID);
            string DelegateID = cFun.DecryptValue(urlQuery.DelegateID);
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            if (!string.IsNullOrEmpty(GroupRegID) && !string.IsNullOrEmpty(showid) && !string.IsNullOrEmpty(flwID) && Request.Params["INVID"] != null)
            {
                ShowController shwCtrl = new ShowController(fn);
                if (shwCtrl.checkValidShow(showid))
                {
                    SiteSettings sSetting = new SiteSettings(fn, showid);
                    sSetting.LoadBaseSiteProperties(showid);
                    currency = sSetting.SiteCurrency;

                    ViewState["Regno"] = DelegateID;
                    InvoiceId = cFun.DecryptValue(Request.QueryString["INVID"].ToString());
                    ViewState["InvoiceId"] = InvoiceId;

                    checkCountry(showid, flwID, GroupRegID, DelegateID);//***Added by th on 2-7-2018

                    InvoiceControler invControler = new InvoiceControler(fn);
                    Invoice inv = invControler.GetInvoiceByID(InvoiceId);
                    if (inv != null)
                    {
                        lblRegno.Text = DelegateID;
                        int paystatus = Convert.ToInt32(inv.Status);
                        //1=Credit Card ; 2=TT; 3=Cheque
                        int paymethod = Convert.ToInt32(inv.PaymentMethod);
                        string invoiceno = inv.InvoiceNo;
                        string subtotalamount = convertstr(inv.SubTotal.ToString());
                        decimal subtotal = cFun.ParseDecimal(Number.Zero);
                        decimal.TryParse(subtotalamount, out subtotal);
                        string discount = convertstr(inv.Discount.ToString());

                        string payamount = convertstr(inv.GrandTotal.ToString());

                        string refno = inv.InvoiceNo;

                        string remark = inv.Remarks;
                        if (paymethod == (int)PaymentType.CreditCard)
                        {
                            rbCheque.Checked = false;
                            rbTT.Checked = false;
                            rbCreditCard.Checked = true;
                            rbWaivedMethod.Checked = false;
                        }
                        else if (paymethod == (int)PaymentType.TT)
                        {
                            rbCreditCard.Checked = false;
                            rbTT.Checked = true;
                            rbCheque.Checked = false;
                            rbWaivedMethod.Checked = false;
                        }
                        else if (paymethod == (int)PaymentType.Cheque)
                        {
                            rbCheque.Checked = true;
                            rbTT.Checked = false;
                            rbCreditCard.Checked = false;
                            rbWaivedMethod.Checked = false;
                        }
                        else if (paymethod == (int)PaymentType.Waved)
                        {
                            rbCheque.Checked = false;
                            rbTT.Checked = false;
                            rbCreditCard.Checked = false;
                            rbWaivedMethod.Checked = true;
                        }

                        StatusSettings statusSet = new StatusSettings(fn);
                        if (paystatus == statusSet.Pending || paystatus == statusSet.TTPending || paystatus == statusSet.ChequePending)
                        {
                            rbunpaid.Checked = true;
                            rbpaid.Checked = false;
                            //rbwaived.Checked = false;
                        }
                        else
                        {
                            rbunpaid.Checked = false;
                            rbpaid.Checked = true;
                            //rbwaived.Checked = false;
                        }
                        //else if (paystatus == 2)
                        //{
                        //    rbunpaid.Checked = false;
                        //    rbpaid.Checked = false;
                        //    rbwaived.Checked = true;
                        //}

                        if (!string.IsNullOrEmpty(remark))
                        {
                            string[] remark_refno = remark.Split('^');
                            if (remark_refno.Length > 0)
                            {
                                txtremarks.Text = remark_refno[0];
                                if (remark_refno.Length > 1)
                                {
                                    txtRefNo.Text = remark_refno[1];
                                }
                            }
                        }

                        ////***change by th on 3-7-2018
                        SiteSettings st = new SiteSettings(fn, showid);
                        string charges = convertstr(inv.AdminFee.ToString());

                        string ocharges = convertstr(inv.OtherFee.ToString());
                        decimal ttadminfee = cFun.ParseDecimal(Number.Zero);
                        if (rbTT.Checked == true)
                        {
                            ttadminfee = cFun.ParseDecimal(st.ttAdminFee);
                        }
                        //decimal otherFee = cFun.ParseDecimal(ocharges);//*** [than,2018-Jul-2] TT admin chareg=0 ,if Host Country and Delegate Country Same
                        if (isSameWithHostCountry(st.ShowHostCountry))
                            ttadminfee = Number.zeroDecimal;

                        string colGst = convertstr(inv.GSTAmount.ToString());
                        decimal gstfee = cFun.ParseDecimal(Number.Zero);
                        gstfee = cFun.ParseDecimal(st.gstfee);
                        //decimal.TryParse(colGst, out gstfee);
                        decimal realGstFee = cFun.ParseDecimal(Number.Zero);
                        decimal adminfee = cFun.ParseDecimal(Number.Zero);

                        bool showAdminBeforeGST = true;
                        if (string.IsNullOrEmpty(st.displayAdminFeeAfterGSTFee) || st.displayAdminFeeAfterGSTFee == "0")
                        {
                            if (rbCreditCard.Checked == true)
                            {
                                adminfee = cFun.ParseDecimal(st.adminfee);
                                adminfee = Math.Round((subtotal + realGstFee) * adminfee, 2);
                            }
                        }
                        else
                        {
                            showAdminBeforeGST = false;
                        }

                        if (showAdminBeforeGST)
                        {
                            realGstFee = Math.Round((subtotal + ttadminfee + adminfee) * gstfee, 2);
                        }
                        else
                        {
                            realGstFee = Math.Round((subtotal + adminfee) * gstfee, 2);
                        }

                        if (!string.IsNullOrEmpty(st.ShowHostCountry) && st.ShowHostCountry != "0" &&  !isSameWithHostCountry(st.ShowHostCountry))
                        {
                            realGstFee = Number.zeroDecimal;
                        }

                        if (st.displayAdminFeeAfterGSTFee == "1")
                        {
                            if (rbCreditCard.Checked == true)
                            {
                                adminfee = cFun.ParseDecimal(st.adminfee);
                                adminfee = Math.Round((subtotal + realGstFee) * adminfee, 2);
                            }
                            //decimal.TryParse(charges, out adminfee);
                        }

                        PaymentControler pControl = new PaymentControler(fn);
                        decimal totalPaidAmt = pControl.GetPaymentTotalPaidAmtByInvNo(InvoiceId);
                        string paidamount = convertstr(totalPaidAmt.ToString());

                        if (subtotal > 0)
                        {
                            payamount = (subtotal + adminfee + ttadminfee + realGstFee).ToString();
                        }
                        //decimal grandtotal = 0;
                        //decimal.TryParse(payamount, out grandtotal);
                        //decimal outstandingAmt = grandtotal - totalPaidAmt;
                        //string outstanding = convertstr(outstandingAmt.ToString());

                        txtamount.Value = payamount;
                        decimal payableamount = 0;
                        decimal.TryParse(payamount, out payableamount);
                        txtPayableAmount.Text = (payableamount - totalPaidAmt).ToString();
                        txtSubTotal.Text = subtotalamount;
                        //txtpaid.Text = paidamount;/*(comment on 8-10-2019 th)*/
                        lblCurrentPaidAmount.Text = paidamount;
                        txtinvoice.Text = invoiceno;
                        txtAdminCharges.Text = adminfee.ToString();// charges;
                        txtOthCharges.Text = ttadminfee.ToString();// ocharges;

                        txtCollectGST.Text = realGstFee.ToString();// colGst;
                        //txtRecAmount.Text = recAmount;

                        txtDiscount.Text = discount;

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "enablebutton();", true);
                        //lbloutstanding.Text = outstanding;
                        ////***change by th on 3 - 7 - 2018
                    }
                    else
                    {
                        Panel1.Enabled = false;
                        Panel2.Enabled = false;
                        txtRefNo.Enabled = false;
                        txtinvoice.Enabled = false;
                        txtAdminCharges.Enabled = false;
                        txtOthCharges.Enabled = false;
                        txtCollectGST.Enabled = false;
                        //txtRecAmount.Enabled = false;
                        lbloutstanding.Enabled = false;
                        txtremarks.Enabled = false;
                        btnsubmit.Enabled = false;
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string flwID = cFun.DecryptValue(urlQuery.FlowID);
            string GroupRegID = cFun.DecryptValue(urlQuery.GoupRegID);
            string DelegateID = cFun.DecryptValue(urlQuery.DelegateID);
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            if (!string.IsNullOrEmpty(GroupRegID) && !string.IsNullOrEmpty(showid) && !string.IsNullOrEmpty(flwID) && Request.Params["INVID"] != null)
            {
                FlowControler flwControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flwID);
                string flowType = flwMasterConfig.FlowType;

                string invoiceID = cFun.DecryptValue(Request.QueryString["INVID"].ToString());
                string RefNumber = txtinvoice.Text.Trim();
                if (validation())
                {
                    if (txtpaid.Text == "")
                    {
                        txtpaid.Text = "0";
                    }

                    int payType = 0;
                    payType = Convert.ToInt32(Number.Zero);
                    if (rbCreditCard.Checked == true)
                    {
                        payType = (int)PaymentType.CreditCard;
                    }
                    else if (rbTT.Checked == true)
                    {
                        payType = (int)PaymentType.TT;
                    }
                    else if (rbWaivedMethod.Checked == true)
                    {
                        payType = (int)PaymentType.Waved;
                    }
                    else if (rbCheque.Checked == true)
                    {
                        payType = (int)PaymentType.Cheque;
                    }

                    string amount = txtamount.Value;
                    string pAmount = txtpaid.Text;
                    //string cheque = replace(txtRefNo.Text);
                    string invoice = replace(txtinvoice.Text);
                    string refNo = replace(txtRefNo.Text);
                    decimal pay = 0;
                    decimal amt = 0;
                    if (!string.IsNullOrEmpty(amount)) amt = decimal.Parse(amount);
                    if (!string.IsNullOrEmpty(pAmount)) pay = decimal.Parse(pAmount);

                    //decimal outstand = 0;
                    //if (payType != (int)PaymentType.TT)
                    //{
                    //    outstand = amt - pay;
                    //}

                    InvoiceControler iControl = new InvoiceControler(fn);
                    bool isOK = false;
                    //string outstanding = lbloutstanding.Text.Replace("(", "").Replace(")", "");/*(comment on 8-10-2019 th)*/
                    string remarks = replace(txtremarks.Text);
                    string admincharges = txtAdminCharges.Text.ToString();
                    string ocharges = txtOthCharges.Text.ToString();
                    string colgst = txtCollectGST.Text.ToString();
                    string subtotal = txtSubTotal.Text.ToString();
                    //string recamount = txtRecAmount.Text.ToString();

                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    StatusSettings stuSettings = new StatusSettings(fn);

                    Invoice inv = new Invoice();
                    inv.InvoiceID = invoiceID;
                    inv.GSTAmount = cFun.ParseDecimal(colgst);
                    inv.AdminFee = cFun.ParseDecimal(admincharges);
                    inv.OtherFee = cFun.ParseDecimal(ocharges);
                    inv.SubTotal = cFun.ParseDecimal(subtotal);
                    inv.Discount = !string.IsNullOrEmpty(txtDiscount.Text) && !string.IsNullOrWhiteSpace(txtDiscount.Text) ? cFun.ParseDecimal(txtDiscount.Text) : 0;
                    inv.PaymentMethod = payType.ToString();
                    inv.GrandTotal = amt;
                    inv.Remarks = txtremarks.Text.ToString() + "^" + refNo;
                    isOK = iControl.UpdateInvoiceBackend(invoiceID, inv);

                    if (isOK)
                    {
                        OrderControler oControler = new OrderControler(fn);
                        PaymentControler pControl = new PaymentControler(fn);
                        PaymentLog pLog = new PaymentLog();
                        if (rbpaid.Checked == true && (payType == (int)PaymentType.CreditCard || payType == (int)PaymentType.TT || payType == (int)PaymentType.Cheque))
                        {
                            PaymentControler pCtrl = new PaymentControler(fn);
                            SiteSettings sSetting = new SiteSettings(fn, showid);
                            sSetting.LoadBaseSiteProperties(showid);

                            pLog.PayRefNo = RefNumber;// pCtrl.GenerateRefNumber(sSetting.SiteProjectID);
                            pLog.InvoiceID = invoiceID;
                            pLog.Amount = amt;
                            pLog.RegGroupId = GroupRegID;
                            pLog.RegID = DelegateID;
                            pLog.UrlFlowID = flwID;
                            pLog.UrlStep = _backendPayment;
                            pLog.UrlShowID = showid;
                            bool sOK = pCtrl.LogPayment(pLog);

                            //pLog = pControl.GetPaymentLogObjByRef(RefNumber);
                            decimal grandAmount = pLog.Amount;
                            PaymentObj payment = new PaymentObj();
                            payment.InvNo = pLog.InvoiceID;
                            payment.PaidAmount = pay;
                            payment.PaidRefNo = refNo;// pLog.PayRefNo;
                            payment.RegGroupId = pLog.RegGroupId;
                            payment.RegId = pLog.RegID;
                            payment.PaidBy = RefNumber;

                            payment.PaidType = payType.ToString(); //((int)PaymentType.CreditCard).ToString();

                            isOK = pControl.AddCreditCardPayment(payment); //Add Paid

                            if (isOK)
                            {
                                List<Order> oList = oControler.GetAllPendingOrder(GroupRegID, DelegateID);
                                oControler.UpdateInvoiceNuber(oList, pLog.InvoiceID);

                                decimal totalPaidAmt = pControl.GetPaymentTotalPaidAmtByInvNo(pLog.InvoiceID);
                                isOK = iControl.UpdateInvoiceStatus(pLog.InvoiceID, (int)PaymentType.CreditCard, totalPaidAmt);

                                iControl.UpdateInvoiceLock(pLog.InvoiceID, (int)InvoiceLock.Locaked); //Lock Invoice Frist ; For Multiple Payment

                                oControler.UpdateOrderStatusSuccess(pLog.InvoiceID);
                                rgd.updateStatus(DelegateID, stuSettings.Success, showid);//***added on 27-5-2019 (update reg status as Success)
                                string latestStep = flwControl.GetLatestStep(flwID);//***added on 27-5-2019 (get latest flow step)
                                if (!string.IsNullOrEmpty(latestStep) && latestStep != "0")
                                {
                                    rgd.updateStep(DelegateID, flwID, latestStep, showid);//***added on 27-5-2019 (update reg stage as latest step)
                                }
                            }
                        }
                        else if (payType == (int)PaymentType.Waved)
                        {
                            iControl.UpdateInvoiceStatus(invoiceID, payType);
                            oControler.UpdateOrderStatusSuccess(invoiceID);
                            rgd.updateStatus(DelegateID, stuSettings.Success, showid);//***added on 27-5-2019 (update reg status as Success)
                            string latestStep = flwControl.GetLatestStep(flwID);//***added on 27-5-2019 (get latest flow step)
                            if (!string.IsNullOrEmpty(latestStep) && latestStep != "0")
                            {
                                rgd.updateStep(DelegateID, flwID, latestStep, showid);//***added on 27-5-2019 (update reg stage as latest step)
                            }
                        }

                        string updatedUser = string.Empty;
                        if (Session["userid"] != null)
                        {
                            updatedUser = Session["userid"].ToString();
                        }

                        bool paidFullAmt = false;
                        decimal totalPaidAmtFinal = pControl.GetPaymentTotalPaidAmtByInvNo(pLog.InvoiceID);
                        //send receipt and confirmation letter
                        if (rbpaid.Checked == true || payType == (int)PaymentType.Waved)//|| rbwaived.Checked == true 
                        {
                            if (totalPaidAmtFinal >= amt)
                            {
                                paidFullAmt = true;
                                ////Send Email
                                //FlowControler flwObj = new FlowControler(fn, urlQuery);
                                EmailHelper eHelper = new EmailHelper();
                                ////string path = Path.GetFileName(Request.Url.AbsolutePath);
                                ////string currentpage = path.Substring(path.LastIndexOf("/")).ToString();
                                //string fullUrl = "";//"FLW=" + cFun.DecryptValue(flwObj.FlowID) + "&STP=" + cFun.DecryptValue(urlQuery.CurrIndex)
                                //                    //+ "&GRP=" + cFun.DecryptValue(groupid) + "&INV=" + cFun.DecryptValue(regno) + "&SHW=" + cFun.DecryptValue(urlQuery.CurrShowID);
                                //FlowURLQuery reLoginFlow = new FlowURLQuery(fullUrl);
                                ////eHelper.SendCurrentFlowStepEmail(reLoginFlow);

                                string eType = EmailHTMLTemlateType.Confirmation;
                                bool isExistEmailType = checkEmailTypeForConfirmation(showid, EmailHTMLTemlateType.Confirmation);
                                if (!isExistEmailType)
                                {
                                    isExistEmailType = checkEmailTypeForConfirmation(showid, EmailHTMLTemlateType.RECEIPT);
                                    if (isExistEmailType)
                                    {
                                        eType = EmailHTMLTemlateType.RECEIPT;
                                    }
                                }
                                string emailRegType = (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL ? BackendRegType.backendRegType_Delegate : "");// "D";
                                EmailDSourceKey sourcKey = new EmailDSourceKey();
                                string showID = showid;// "MBF242";
                                string flowID = flwID;// "F322";
                                string groupID = GroupRegID;// "2421113";
                                string delegateID = (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL ? DelegateID : "");// "24210092";//Blank

                                sourcKey.AddKeyToList("ShowID", showID);
                                sourcKey.AddKeyToList("RegGroupID", groupID);
                                sourcKey.AddKeyToList("Regno", delegateID);
                                sourcKey.AddKeyToList("FlowID", flowID);
                                sourcKey.AddKeyToList("INVID", invoiceID);
                                eHelper.SendEmailFromBackend(eType, sourcKey, emailRegType, updatedUser);
                            }
                            else
                            {
                                paidFullAmt = false;
                            }
                            string invoiceNo = txtinvoice.Text.Trim();
                        }

                        #region Logging
                        LogGenEmail lggenemail = new LogGenEmail(fn);
                        LogActionObj lactObj = new LogActionObj();
                        lggenemail.type = lactObj.actupdate;// GenLogDefaultValue.errorUpdate;
                        lggenemail.RefNumber = GroupRegID + "," + DelegateID;
                        lggenemail.description = lactObj.actupdate + _paymentLogRemark;
                        lggenemail.remark = RegClass.typeCmf + "(ActionUserID:" + updatedUser + "; MachineIP: " + GetUserIP() + ")";
                        lggenemail.step = cFun.DecryptValue("");//urlQuery.CurrIndex
                        lggenemail.ShowID = showid;
                        lggenemail.writeLog();
                        #endregion

                        if (paidFullAmt)
                        {
                            string currentUrl = Request.Url.AbsoluteUri;
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Payment Updated!');window.location.href='" + currentUrl + "';", true);
                            //ClientScript.RegisterStartupScript(typeof(Page), "myscript", "<script>alert('Payment Updated!');</script>");
                            return;
                            //window.location='MasterRegistrationList_Indiv.aspx?regno=" + ViewState["Regno"] + "';
                        }
                        else
                        {
                            string currentUrl = Request.Url.AbsoluteUri;
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please take note the outstanding amount.');window.location.href='" + currentUrl + "';", true);
                            //ClientScript.RegisterStartupScript(typeof(Page), "myscript", "<script>alert('Payment Updated!');</script>");
                            return;
                            //window.location='MasterRegistrationList_Indiv.aspx?regno=" + ViewState["Regno"] + "';
                        }
                    }
                    else
                    {
                        string currentUrl = Request.Url.AbsoluteUri;
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fail!');window.location.href='" + currentUrl + "';", true);
                        //ClientScript.RegisterStartupScript(typeof(Page), "myscript", "<script>alert('Fail!');</script>");
                        return;
                    }
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }
    #region PaymentMethodChange
    protected void radioPaymentsChange(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            showFeeByPaymentMethod(showid);

            bindConferenceCalPrice(showid, urlQuery);
        }
    }
    private void showFeeByPaymentMethod(string showid)
    {
        SiteSettings st = new SiteSettings(fn, showid);
        if (rbCreditCard.Checked == true)
        {
            decimal adminfee = cFun.ParseDecimal(st.adminfee);
        }
        else if (rbTT.Checked == true)
        {
            decimal ttadminfee = cFun.ParseDecimal(st.ttAdminFee);
        }
        decimal gstfee = cFun.ParseDecimal(st.gstfee);
    }
    private void bindConferenceCalPrice(string showid, FlowURLQuery urlQuery)
    {
        txtpaid.Enabled = true;
        txtpaid.Text = "";//***
        SiteSettings st = new SiteSettings(fn, showid);
        currency = st.SiteCurrency;
        #region Conference
        string subTotal = Number.zeroDecimal.ToString();
        //lblSubTotalCurr.Text = st.SiteCurrency;
        subTotal = cFun.FormatCurrency(txtSubTotal.Text.ToString());
        //lblGrandTotal.Text = subTotal;
        decimal adminfee = cFun.ParseDecimal(Number.Zero);
        decimal gstfee = cFun.ParseDecimal(Number.Zero);

        gstfee = cFun.ParseDecimal(st.gstfee);
        if (rbCreditCard.Checked == true)
        {
            adminfee = cFun.ParseDecimal(st.adminfee);
        }

        decimal ttadminfee = cFun.ParseDecimal(Number.Zero);
        if (rbTT.Checked == true)
        {
            ttadminfee = cFun.ParseDecimal(st.ttAdminFee);
        }

        decimal subTotalPrice = 0;
        decimal.TryParse(subTotal, out subTotalPrice);
        decimal TotalPrice = subTotalPrice;

        SiteSettings sSetting = new SiteSettings(fn, showid);
        bool showAdminBeforeGST = true;
        if (sSetting.displayAdminFeeAfterGSTFee == "1")
        {
            showAdminBeforeGST = false;
        }

        decimal realAdminFee = Number.zeroDecimal;
        decimal realTTAdminFee = Number.zeroDecimal;
        if (showAdminBeforeGST)
        {
            realAdminFee = Math.Round(subTotalPrice * adminfee, 2);
            //lblAdminFeeText.Text = (adminfee * 100).ToString();
            TotalPrice += realAdminFee;

            realTTAdminFee = subTotalPrice == Number.zeroDecimal ? Number.zeroDecimal : ttadminfee;//Math.Round(TotalPrice * ttadminfee, 2);
            //lblTTAdminFeeText.Text = (ttadminfee).ToString();
            if (isSameWithHostCountry(sSetting.ShowHostCountry))//*** [than,2018-Jul-2] TT admin chareg=0 ,if Host Country and Delegate Country Same
                realTTAdminFee = Number.zeroDecimal;
            TotalPrice += realTTAdminFee;
        }

        decimal realGstFee = Math.Round(TotalPrice * gstfee, 2);
        if (isSameWithHostCountry(sSetting.ShowHostCountry))
        {
            realGstFee = Number.zeroDecimal;
        }
        //lblGSTFeeText.Text = (gstfee * 100).ToString();
        txtCollectGST.Text = realGstFee.ToString();
        TotalPrice += realGstFee;

        if (!showAdminBeforeGST)
        {
            realAdminFee = Math.Round(subTotalPrice * adminfee, 2);
            //lblAdminFeeText.Text = (adminfee * 100).ToString();
            TotalPrice += realAdminFee;

            realTTAdminFee = subTotalPrice == Number.zeroDecimal ? Number.zeroDecimal : ttadminfee;//Math.Round(TotalPrice * ttadminfee, 2);
            //lblTTAdminFeeText.Text = (ttadminfee).ToString();
            if (isSameWithHostCountry(sSetting.ShowHostCountry))//*** [than,2018-Jul-2] TT admin chareg=0 ,if Host Country and Delegate Country Same
                realTTAdminFee = Number.zeroDecimal;
            TotalPrice += realTTAdminFee;
        }

        txtAdminCharges.Text = realAdminFee.ToString();
        txtOthCharges.Text = realTTAdminFee.ToString();
        txtamount.Value = TotalPrice.ToString();

        PaymentControler pControl = new PaymentControler(fn);
        decimal totalPaidAmt = pControl.GetPaymentTotalPaidAmtByInvNo(txtinvoice.Text.Trim());
        string paidamount = convertstr(totalPaidAmt.ToString());
        decimal payableamount = TotalPrice;
        txtPayableAmount.Text = (payableamount - totalPaidAmt).ToString();
        lblCurrentPaidAmount.Text = paidamount;

        if (rbWaivedMethod.Checked == true)
        {
            txtpaid.Text = TotalPrice.ToString();
            txtpaid.Enabled = false;
        }

        showFeeByPaymentMethod(showid);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "enablebutton();", true);
        #endregion
    }
    #endregion
    private bool validation()
    {
        decimal amount;
        decimal paid;
        string cheque = txtRefNo.Text;
        if (rbpaid.Checked == true || rbunpaid.Checked == true)// || rbwaived.Checked == true)
        {
            Panel1.BackColor = Color.White;
            if (rbpaid.Checked == true || rbunpaid.Checked == true)
            {
                if (rbCreditCard.Checked == true || rbTT.Checked == true || rbCheque.Checked == true || rbWaivedMethod.Checked == true)//***added || rbWaivedMethod.Checked == true on 27-5-2019
                {
                    Panel2.BackColor = Color.White;
                }
                else
                {
                    Panel2.Focus();
                    Panel2.BackColor = Color.Pink;
                    ClientScript.RegisterStartupScript(typeof(Page), "myscript", "<script>alert('Please select Payment Method.');</script>");
                    return false;
                }
            }
        }
        else
        {
            Panel1.Focus();
            Panel1.BackColor = Color.Pink;
            ClientScript.RegisterStartupScript(typeof(Page), "myscript", "<script>alert('Please select Payment Status.');</script>");
            return false;
        }
        if (txtamount.Value == null)
        {
            txtamount.Focus();
            txtamount.Attributes.Add("style", "background-color:pink;");
            ClientScript.RegisterStartupScript(typeof(Page), "myscript", "<script>alert('Please enter Payable Amount.');</script>");
            return false;
        }
        else if (!decimal.TryParse(txtamount.Value, out amount))
        {
            txtamount.Focus();
            txtamount.Attributes.Add("style", "background-color:pink;");
            ClientScript.RegisterStartupScript(typeof(Page), "myscript", "<script>alert('Invalid Payable Amount format.');</script>");
            return false;
        }
        else
        {
            txtamount.Attributes.Add("style", "background-color:white;");
        }
        if (txtpaid.Text == null)
        {
            txtpaid.Focus();
            txtpaid.Attributes.Add("style", "background-color:pink;");
            ClientScript.RegisterStartupScript(typeof(Page), "myscript", "<script>alert('Please enter Paid Amount.');</script>");
            return false;
        }
        else if (!decimal.TryParse(txtpaid.Text, out paid))
        {
            txtpaid.Focus();
            txtpaid.Attributes.Add("style", "background-color:pink;");
            ClientScript.RegisterStartupScript(typeof(Page), "myscript", "<script>alert('Invalid Paid Amount format.');</script>");
            return false;
        }
        else
        {
            txtamount.Attributes.Add("style", "background-color:white;");
        }
        if (rbpaid.Checked == true)
        {
            if (txtRefNo.Text == "")
            {
                txtRefNo.Focus();
                txtRefNo.Attributes.Add("style", "background-color:pink;");
                ClientScript.RegisterStartupScript(typeof(Page), "myscript", "<script>alert('Please enter Reference No.');</script>");
                return false;
            }
        }
        return true;
    }
    private string replace(string str)
    {
        return str.Replace("'", "''");
    }
    protected void btnback_Click(object sender, EventArgs e)
    {
        //Response.Redirect("MasterRegistrationList.aspx?regno=" + regno);
    }
    protected string convertstr(string val)
    {
        string rtr = string.Empty;
        if (val == string.Empty || val == "")
        {
            rtr = "0";
        }
        else
        {
            rtr = val;
        }
        return rtr;
    }

    #region checkCountry//*** [than,2018-Jul-2] TT admin chareg=0 ,if Host Country and Delegate Country Same
    private void checkCountry(string showid, string flowid, string groupid, string delegateid)
    {
        string currentCountry = "";
        SiteSettings sSetting = new SiteSettings(fn, showid);
        if (sSetting.ShowHostCountry == currentCountry)
            regCountrySameWithShowCountry.Text = "1";
        else
            regCountrySameWithShowCountry.Text = "0";

        FlowControler fCtrl = new FlowControler(fn);
        FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
        if (fmaster.FlowType == SiteFlowType.FLOW_GROUP || string.IsNullOrEmpty(delegateid))
        {
            RegGroupObj rgg = new RegGroupObj(fn);
            DataTable dtGrp = rgg.getRegGroupByID(groupid, showid);
            if (dtGrp.Rows.Count > 0)
            {
                string rg_country = dtGrp.Rows[0]["RG_Country"].ToString();
                currentCountry = rg_country;
            }
        }
        else
        {
            RegDelegateObj rgd = new RegDelegateObj(fn);
            DataTable dtDelegate = rgd.getDataByGroupIDRegno(groupid, delegateid, showid);
            if (dtDelegate.Rows.Count > 0)
            {
                string del_country = dtDelegate.Rows[0]["reg_Country"].ToString();
                currentCountry = del_country;
            }
        }
        if (sSetting.ShowHostCountry == currentCountry)
            regCountrySameWithShowCountry.Text = "1";
        else
            regCountrySameWithShowCountry.Text = "0";
    }
    private bool isSameWithHostCountry(string isSettingSameHostCountry)
    {
        bool isSame = false;

        if (!string.IsNullOrEmpty(isSettingSameHostCountry) && isSettingSameHostCountry != "0")
        {
            if (regCountrySameWithShowCountry.Text == "1")
                isSame = true;
        }

        return isSame;
    }
    #endregion

    private bool checkEmailTypeForConfirmation(string showid, string emailType)
    {
        bool isExist = false;
        string sql = "Select * From tb_site_flow_Email Where showid='" + showid + "' And EmailType='" + emailType + "'";
        DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        if (dt.Rows.Count > 0)
        {
            isExist = true;
        }
        return isExist;
    }

    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
}