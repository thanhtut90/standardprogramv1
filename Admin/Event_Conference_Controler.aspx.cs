﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Event_Conference_Controler : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    static string _masterconfselectiontype = "MasterConfSelectionType";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null & Request.Params["FLW"] != null)
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());

                PanelKeyDetail.Visible = false;
                txtShowID.Text = cFun.DecryptValue(urlQuery.CurrShowID);
                txtFlow.Text= cFun.DecryptValue(urlQuery.FlowID);
                ShowHideButtons(txtShowID.Text,txtFlow.Text);

                if (Request.QueryString["STP"] != null && Request.QueryString["a"] == null)
                {
                    string curStep = cFun.DecryptValue(urlQuery.CurrIndex);

                    FlowMaster fMaster = new FlowMaster();
                    fMaster.FlowID = cFun.DecryptValue(urlQuery.FlowID);
                    fMaster.FlowStatus = curStep;
                    FlowControler fControler = new FlowControler(fn);
                    fControler.UpdateFlowMasterStatus(fMaster);
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region binddata (bind data from tb_site_settings to related controls)
    protected void binddata(string showid)
    {
        string masConfSelectionType = string.Empty;

        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        //string query = "Select * From tb_site_settings Where ShowID=@SHWID";

        //DataTable dt = new DataTable();
        //dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

        //for (int x = 0; x < dt.Rows.Count; x++)
        //{
        //    string settingname = dt.Rows[x]["settings_name"].ToString();
        //    if (settingname == _masterconfselectiontype)
        //    {
        //        masConfSelectionType = dt.Rows[x]["settings_value"].ToString();
        //        if (!String.IsNullOrEmpty(masConfSelectionType))
        //        {
        //            ListItem listItem = ddlMasterConferenceType.Items.FindByValue(masConfSelectionType);
        //            if (listItem != null)
        //            {
        //                ddlMasterConferenceType.ClearSelection();
        //                listItem.Selected = true;
        //            }
        //        }
        //    }
        //}
    }
    #endregion

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_Show table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        string showID = txtShowID.Text;
        string flwID = txtFlow.Text;
        if (!string.IsNullOrEmpty(showID) && !string.IsNullOrEmpty(flwID))
        {
            //string query = "select r.reg_CategoryId, reg_CategoryName from ref_reg_Category r ,tb_ConfItem c where c.reg_categoryId=r.reg_CategoryId and  r.showID=@showID  and RefValue =@Flow Group By r.reg_CategoryId,r.reg_CategoryName";
            string query = "select r.reg_CategoryId, r.reg_CategoryName, r.CType, r.reg_order from ref_reg_Category r where r.showID=@showID and RefValue=@Flow Group By r.reg_CategoryId,r.reg_CategoryName,r.CType, r.reg_order";
            SqlParameter spar = new SqlParameter("showID", SqlDbType.NVarChar);
            spar.Value = showID;
            SqlParameter spar2 = new SqlParameter("Flow", SqlDbType.NVarChar);
            spar2.Value = flwID;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar);
            pList.Add(spar2);
            DataTable dt = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dt;
            }
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string actionType = string.Empty;
        if (Request.Params["a"] != null)
        {
            actionType = Request.QueryString["a"].ToString();
        }

        if (e.CommandName == "EditConference")
        {
            GridDataItem item = (GridDataItem)e.Item;

            string curID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["reg_CategoryId"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["reg_CategoryId"].ToString() : "";
            if (!string.IsNullOrEmpty(curID))
            {
                if(string.IsNullOrEmpty(actionType))
                {
                    string url = string.Format("Event_Conference_Items?FLW={0}&SHW={1}&STP={2}&CAT={3}",
                        Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString(), cFun.EncryptValue(curID));
                    Response.Redirect(url);
                }
                else
                {
                    string url = string.Format("Event_Conference_Items?FLW={0}&SHW={1}&STP={2}&CAT={3}&a={4}",
                        Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString(), cFun.EncryptValue(curID), actionType);
                    Response.Redirect(url);
                }
            }
        }
        else if(e.CommandName == "ViewRef")
        {
            GridDataItem item = (GridDataItem)e.Item;

            string curID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["reg_CategoryId"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["reg_CategoryId"].ToString() : "";
            if (!string.IsNullOrEmpty(curID))
            {
                if (string.IsNullOrEmpty(actionType))
                {
                    string url = string.Format("Event_Conference_Promo?FLW={0}&SHW={1}&STP={2}",
                    Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString());
                    Response.Redirect(url);
                }
                else
                {
                    string url = string.Format("Event_Conference_Promo?FLW={0}&SHW={1}&STP={2}&a={3}",
                    Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString(), actionType);
                    Response.Redirect(url);
                }
            }
        }
    }
    #endregion


    #region LoadDetails
    protected void btnCreateFlow_Onclick(object sender, EventArgs e)
    {
        string showID = txtShowID.Text;
        string flowID = "";
        if (Request.Params["FLW"] != null && Request.Params["STP"] != null && !string.IsNullOrEmpty(showID))
        {
            flowID = Request.Params["FLW"].ToString();
            string step = Request.Params["STP"].ToString();
            flowID = cFun.DecryptValue(flowID);
            step = cFun.DecryptValue(step);
            string sql = "select * from ref_reg_Category r where r.showID=@Show and RefValue=@flow and CType='F'";
            SqlParameter spar = new SqlParameter("Show", SqlDbType.NVarChar);
            spar.Value = showID;
            SqlParameter spar2 = new SqlParameter("flow", SqlDbType.NVarChar);
            spar2.Value = flowID;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar);
            pList.Add(spar2);

            DataTable dt = fn.GetDatasetByCommand(sql, "dsShow", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                string categoryID = dt.Rows[0]["reg_CategoryId"].ToString();
                if (!string.IsNullOrEmpty(categoryID))
                {
                    showID = cFun.EncryptValue(showID);
                    flowID = cFun.EncryptValue(flowID);
                    step = cFun.EncryptValue(step);
                    categoryID = cFun.EncryptValue(categoryID);
                    string url = "";
                    url = string.Format("Event_Conference_Items?FLW={0}&SHW={1}&STP={2}&CAT={3}", flowID, showID, step, categoryID);
                    Response.Redirect(url);
                }
            }
        }
    }

    protected void btnRefFlow_Onclick(object sender, EventArgs e)
    {
        string showID = txtShowID.Text;
        string flowID = "";
        if (Request.Params["FLW"] != null && Request.Params["STP"] != null && !string.IsNullOrEmpty(showID))
        {
            flowID = Request.Params["FLW"].ToString();
            string step = Request.Params["STP"].ToString();

            string url = "";

            if (Request.Params["a"] == null)
            {
                url = string.Format("Event_Conference_Promo?FLW={0}&SHW={1}&STP={2}", flowID, showID, step);
                Response.Redirect(url);
            }
            else
            {
                string actionType = Request.QueryString["a"].ToString();
                url = string.Format("Event_Conference_Promo?FLW={0}&SHW={1}&STP={2}&a={3}", flowID, showID, step, actionType);
                Response.Redirect(url);
            }
        }
    }

    private void ShowHideButtons(string showID,string flowID)
    {
        if (string.IsNullOrEmpty(showID))
        {
            btnAddDefault.Visible = false;
            btnPromo.Visible = false;
            btnNext.Visible = false;
        }
        else
        {
            //if (HasSetupDefaultConference(showID, flowID))
            {
                btnAddDefault.Visible = false;
                btnPromo.Visible = true;
                btnNext.Visible = true;
            }
            //else
            //{
            //    btnAddDefault.Visible = true;
            //    btnPromo.Visible = false;
            //    btnNext.Visible = false;
            //}
        }
    }

    private bool HasSetupDefaultConference(string showID,string FlowID)
    {
        bool isOK = false;
        string query = " select * from tb_ConfItem where reg_categoryId in (select reg_categoryId  from ref_reg_Category where showID=@showID and RefValue=@flow and Ctype='F')";
        SqlParameter spar = new SqlParameter("showID", SqlDbType.NVarChar);
        spar.Value = showID;
        SqlParameter spar2 = new SqlParameter("flow", SqlDbType.NVarChar);
        spar2.Value = FlowID;
        List<SqlParameter> pList = new List<SqlParameter>();
        pList.Add(spar);
        pList.Add(spar2);

        DataTable dt = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];

        if (dt.Rows.Count > 0)
        {
            isOK = true;
        }
        return isOK;
    }
    #endregion

    #region btnNext_Click
    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (Request.Params["FLW"] != null)
            {
                string flowid = cFun.DecryptValue(urlQuery.FlowID);
                string curstep = cFun.DecryptValue(urlQuery.CurrIndex);

                #region Update CoferenceSelectionType in tb_site_flow_master
                //string masConfSelectionType = ddlMasterConferenceType.SelectedItem.Value;
                string masConfSelectionType = "1";

                //updateSiteFlowMaster(masConfSelectionType, showid, flowid);
                #endregion

                Dictionary<string, string> nValues = new Dictionary<string, string>();
                FlowControler fControl = new FlowControler(fn);
                Flow flw = new Flow();
                flw.FlowID = flowid;
                flw.FlowConfigStatus = YesOrNo.Yes;
                flw.FlowStep = curstep;
                if (Request.QueryString["a"] == null)
                {
                    string stp = cFun.DecryptValue(Request.QueryString["STP"].ToString());
                    nValues = fControl.GetAdminNextRoute(flowid, stp);
                    if (nValues.Count > 0)
                    {
                        //*Update Currenct Flow Step Status
               
                        fControl.UpdateFlowIndivConfigStatus(flw);
                        //*

                        string page = nValues["nURL"].ToString();
                        string step = nValues["nStep"].ToString();
                        string FlowID = nValues["FlowID"].ToString();

                        if (page == "") page = "Event_Config_Final";

                        string path = fControl.MakeFullURL(page, FlowID, showid, "", step);

                        Response.Redirect(path);
                    }
                }
                else
                {
                    fControl.UpdateFlowIndivConfigStatus(flw);
                    string page = "Event_Flow_Dashboard";
                    string path = fControl.MakeFullURL(page, flowid, showid);
                    Response.Redirect(path);
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region updateSiteFlowMaster
    private void updateSiteFlowMaster(string confSelectionType, string showid, string flowid)
    {
        try
        {
            string updateConfigType = "Update tb_site_flow_master Set FLW_ConfSelectionType=@FLW_ConfSelectionType Where ShowID=@SHWID And Flw_id=@Flw_id";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Flw_id", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("FLW_ConfSelectionType", SqlDbType.Int);
            spar.Value = showid;
            spar2.Value = flowid;
            spar3.Value = !string.IsNullOrEmpty(confSelectionType) ? Convert.ToInt32(confSelectionType) : 0;
            pList.Add(spar);
            pList.Add(spar2);
            pList.Add(spar3);
            fn.ExecuteSQLWithParameters(updateConfigType, pList);
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region setViewRefButtonVisibility
    public bool setViewRefButtonVisibility(string order)
    {
        bool isVisible = false;
        {
            if(!string.IsNullOrEmpty(order))
            {
                CategoryClass catClass = new CategoryClass();
                if(order != catClass.defaultID)
                {
                    isVisible = true;
                }
            }
        }

        return isVisible;
    }
    #endregion

    #region lbViewRef_Click
    protected void lbViewRef_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        string catID = btn.CommandArgument;

        if (Request.Params["a"] == null)
        {
            string url = string.Format("Event_Conference_Promo?FLW={0}&SHW={1}&STP={2}&CAT={3}",
            Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString(), cFun.EncryptValue(catID));
            Response.Redirect(url);
        }
        else
        {
            string actionType = Request.QueryString["a"].ToString();
            string url = string.Format("Event_Conference_Promo?FLW={0}&SHW={1}&STP={2}&CAT={3}&a={4}",
            Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString(), cFun.EncryptValue(catID), actionType);
            Response.Redirect(url);
        }
    }
    #endregion

}