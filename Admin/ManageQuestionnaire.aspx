﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageQuestionnaire.aspx.cs" Inherits="Admin_ManageQuestionnaire" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Manage Questionnaire</h1>

        </div>
        <div id="contentwrapper" class="contentwrapper">
            <asp:Panel runat="server" ID="Panel1">
                <div class="mybuttoncss" style="padding-bottom:10px;">
                    <asp:LinkButton ID="btnUpload" runat="server" CssClass="btn btn-success" OnClick="btnAddNew_Click">
                        <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Add
                    </asp:LinkButton>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnl_questionnaireTable">
                <h4 class="pagetitle" style="padding-bottom:20px;text-decoration:underline;font-weight:bold;">Choose Questionnaire Template</h4>
                        <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster2" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                            EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                            OnNeedDataSource="GKeyMaster2_NeedDataSource" OnItemCommand="GKeyMaster2_ItemCommand" PageSize="10" Skin="Bootstrap">
                            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                            </ClientSettings>
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="SQ_QAID" HeaderStyle-Font-Bold="true">
                                <Columns>
                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="SQ_QAID" FilterControlAltText="Filter Flw_ID"
                                        HeaderText="Questionnaire ID" SortExpression="SQ_QAID" UniqueName="SQ_QAID" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="SQ_QATitle" FilterControlAltText="Filter Flw_ID"
                                        HeaderText="Title" SortExpression="SQ_QATitle" UniqueName="SQ_QATitle" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridButtonColumn CommandArgument="id" Text="Edit"
                                        ConfirmDialogType="RadWindow" CommandName="EditQA" ButtonType="LinkButton" UniqueName="RemoveAdmin"
                                        HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </asp:Panel>
            <br /><br />
            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnClick="NextForm">
                        <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Next
                    </asp:LinkButton>
        </div>
    </div>

</asp:Content>

