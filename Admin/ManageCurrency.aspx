﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageCurrency.aspx.cs" Inherits="Admin_ManageCurrency" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h3 class="box-title">Manage Currency</h3>

    <br />
    <br />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateRecord">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CssClass="btn btn-info"><i class="fa fa-arrow-left"></i> Back</asp:LinkButton>

    <br />
    <br />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
        <asp:Panel runat="server" ID="PanelKeyList">
            <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="20" Skin="Bootstrap">
                <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true"></Selecting>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" DataKeyNames="currency_id">
                    <Columns>

                        <telerik:GridButtonColumn ConfirmTextFormatString="Do you want to Delete Currency of {0}?" ConfirmTextFields="currency_id" CommandArgument="currency_id"
                            ConfirmDialogType="RadWindow" CommandName="Delete" ButtonType="ImageButton" ImageUrl="images/delete.jpg" UniqueName="Delete"
                            HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                        </telerik:GridButtonColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="currency_id" FilterControlAltText="Filter currency_id"
                            HeaderText="ID" SortExpression="currency_id" UniqueName="currency_id">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="currency_name" FilterControlAltText="Filter currency_name"
                            HeaderText="Currency Name" SortExpression="currency_name" UniqueName="currency_name">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="currency_display" FilterControlAltText="Filter currency_display"
                            HeaderText="Keyword" SortExpression="currency_display" UniqueName="currency_display">
                        </telerik:GridBoundColumn>

                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </asp:Panel>

        <br />
        <br />
        <div class="mybuttoncss">
            <asp:Button runat="server" Text ="Add New" ID="btnAddNew" OnClick="btnAddNew_Click" CssClass="btn btn-primary"/>
        </div>
        <br />
        <br />
        <asp:Panel runat="server" ID="PanelKeyDetail">
            <h4>Detail</h4>

            <div class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Currency Name</label>
                        <div class="col-md-2">
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtName" runat="server" CssClass="form-control"></telerik:RadTextBox>
                        </div>
                        <div class="col-md-8">
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" Display="Dynamic"
                                ControlToValidate="txtName" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Keyword</label>
                        <div class="col-md-2">
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtKeyword" runat="server" CssClass="form-control"></telerik:RadTextBox>
                        </div>
                        <div class="col-md-8">
                            <asp:RequiredFieldValidator ID="rfvKeyword" runat="server" Display="Dynamic"
                                ControlToValidate="txtKeyword" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtKeyword"
                                ID="RegularExpressionValidator1" ValidationExpression = "^[A-Z]{0,3}$" runat="server"
                            ErrorMessage="Maximum 3 characters(A-Z) allowed and all must be capital letters." ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2 control-label"></div>
                        <div class="col-md-4">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" CssClass="btn btn-primary"/>
                            <asp:Button ID="btnUpdateRecord" runat="server" Text="Update Record" OnClick="btnUpdateRecord_Click" UseSubmitBehavior="true" CssClass="btn btn-primary"></asp:Button>
                            <asp:Label runat="server" ID="lbls" ForeColor="Red"></asp:Label>
                            <asp:HiddenField ID="hfID" runat="server" Value="" />
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </telerik:RadAjaxPanel>
</asp:Content>

