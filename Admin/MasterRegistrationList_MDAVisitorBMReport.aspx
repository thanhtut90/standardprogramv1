﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="MasterRegistrationList_MDAVisitorBMReport.aspx.cs" Inherits="Admin_MasterRegistrationList_MDAVisitorBMReport" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../Content/dist/css/skins/_all-skins.min.css">
    <%--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script> <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>--%>
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }

        $(function () {
            $.fn.datepicker.defaults.format = "dd/mm/yyyy";
            //     $("#txtFromDate").datepicker({}).val()
            //    $("#txtToDate").datepicker({}).val()
        });
    </script>
    <style type="text/css">
        .tdstyle1 {
            width: 170px;
        }

        .tdstyle2 {
            width: 300px;
        }

        form input[type="text"] {
            width: 39% !important;
        }

        .ajax__calendar_container {
            width: 320px !important;
            height: 280px !important;
        }

        .ajax__calendar_body {
            width: 100% !important;
            height: 100% !important;
        }

        td {
            vertical-align: middle;
        }
        /*#ctl00_ContentPlaceHolder1_GKeyMaster_GridData
        {
           height:650px !important;
        }*/
    </style>
 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    &nbsp;&nbsp;&nbsp;
    <asp:LinkButton ID="btnAddNew" runat="server" CssClass="btn btn-success" OnClick="lnkExcel_Clicked">
            <span aria-hidden="true" class="glyphicon glyphicon-export"></span> EXPORT EXCEL
    </asp:LinkButton>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="" Transparency="30"><%-- BackgroundTransparency="50"--%>
                    <asp:Label ID="Label2" runat="server" Text="Loading..">
                    </asp:Label>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/loading-icon.gif" />
                </telerik:RadAjaxLoadingPanel>
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true" LoadingPanelID="RadAjaxLoadingPanel1">
                <asp:Label ID="lblUser" runat="server" Visible="false"></asp:Label>
                <h3 class="box-title">Visitor Business Matching Report</h3>
                <div id="divsearch" runat="server" class="row" style="margin-bottom: 10PX;" visible="false">
                    <div class="form-group col-sm-12">
                        <label for="inputEmail3" class="col-sm-1 control-label" style="display:none;">Show List: &nbsp;</label>
                        <div class="col-sm-2">
                            <asp:Panel ID="showlist" runat="server">
                                <asp:DropDownList ID="ddl_showList" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="form-group col-sm-10">
                        <asp:TextBox ID="txtFromDate" placeholder="From" runat="server" ClientIDMode="Static" onclick="$(this).datepicker().datepicker('show')" CssClass="form-control col-sm-1"></asp:TextBox>
                        <%-- <label for="inputEmail3" class="col-sm-1 control-label">  To </label>--%>
                        <div class="col-sm-1">
                        <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" Enabled="false">To</asp:LinkButton>
                        </div>
                        <asp:TextBox ID="txtToDate" placeholder="To " runat="server" onclick="$(this).datepicker().datepicker('show')" CssClass="form-control col-sm-1"></asp:TextBox>
                        <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-success" runat="server" ><i class="fa fa-search"></i> </asp:LinkButton>
                    </div>
                    <div class="form-group col-sm-10">
                        <asp:TextBox ID="txtKey" placeholder="Enter Key Word (Registration Number, First Name, Surname, Email, Country)" runat="server" ClientIDMode="Static" CssClass="form-control  col-sm-1"></asp:TextBox>
                        <div class="col-sm-1">
                            <asp:LinkButton ID="btnKeysearch" OnClick="btnKeysearch_Click" class="btn btn-success" runat="server" ><i class="fa fa-search"></i> &nbsp;Search </asp:LinkButton>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <div style="overflow-x: scroll;">
                    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientEvents-OnRequestStart="onRequestStart">
                        <AjaxSettings>
                            <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                                <UpdatedControls>
                                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial" />
                                </UpdatedControls>
                            </telerik:AjaxSetting>
                            <%-- <telerik:AjaxSetting AjaxControlID="btnupdate">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>--%>
                        </AjaxSettings>
                    </telerik:RadAjaxManager>

                    <%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">--%>
                        <asp:Panel runat="server" ID="PanelKeyList">
                            <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="false" runat="server" FilterType="HeaderContext" 
                                EnableHeaderContextMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="false" GroupingEnabled="false"
                                OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="20" 
                                OnPageIndexChanged="GKeyMaster_PageIndexChanged">
                                <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="false">
                                    <Selecting AllowRowSelect="false"></Selecting>
                                    <%--<Scrolling AllowScroll="True" EnableVirtualScrollPaging="True" UseStaticHeaders="True"
                                            SaveScrollPosition="True"></Scrolling>--%>
                                </ClientSettings>
                                <%--<HeaderStyle Width="90px" />--%>
                                <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="true" DataKeyNames="RegID">
                                    <CommandItemSettings ShowExportToExcelButton="False" ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                                </MasterTableView>
                            </telerik:RadGrid>
                        </asp:Panel>
                </div>
            </telerik:RadAjaxPanel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


