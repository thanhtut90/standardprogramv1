﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Utilities;
using System.Data.SqlClient;

public partial class Admin_Promo_Add : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = Request.QueryString["SHW"].ToString();

                bindDropdownCommon(ddlGroup, "con_GroupId", "con_GroupName", "tb_Conf_Group", "con_Orderno", " Where isdeleted=0");
                bindDropdownCommon(ddlcategory, "reg_CategoryId", "reg_CategoryName", "ref_reg_Category", "reg_order", "");
                bindDropdownCommon(ddlconitem, "GI_GUID", "GI_Name", "tb_Conf_GroupItem", "GI_OrderNo", " Where isDeleted=0");

                chkChange1(this, null);
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region bindDropdownCommon
    public void bindDropdownCommon(DropDownList ddl, string dataKeyValue, string dataKeyText, string dstable, string ordercolumn, string whclause)
    {
        DataSet ds = new DataSet();
        try
        {
            ds = fn.GetDatasetByCommand("Select * From " + dstable + whclause + " Order By " + ordercolumn, "ds");
            if (ds.Tables[0].Rows.Count != 0)
            {
                ddl.Items.Add("--Please Select--");
                ddl.Items[0].Value = "0";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddl.Items.Add(ds.Tables[0].Rows[i][dataKeyText].ToString());
                    ddl.Items[i + 1].Value = ds.Tables[0].Rows[i][dataKeyValue].ToString();
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region SaveForm
    protected void SaveForm(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();

            string title = string.Empty;
            int isitem = 0;
            string groupid = "0";
            string promotype = "0";
            string value = "0";
            int pselect = 0;
            string timeused = string.Empty;
            string itemid = string.Empty;

            int promo_type1 = 0;

            int noofcode = 0;
            string prefix = string.Empty;
            int regid = 0;
            int confitemid = 0;

            int usagepertran = 0;

            title = cFun.solveSQL(txttitle.Text);

            try
            {
                noofcode = Convert.ToInt32(txtnoofcode.Text);
                prefix = cFun.solveSQL(txtprefix.Text);

                if (rbpertranOne.Checked == true)
                {
                    usagepertran = 1;
                }
                else if (rbpertranWhole.Checked == true)
                {
                    usagepertran = 2;
                }

                int maxusage = 1;

                if (rbbtn1.Checked == true)
                {
                    promo_type1 = 1;
                    promotype = ddlpromotype.SelectedValue.ToString();

                    if (rbprice1.Checked == true)
                    {
                        pselect = 1;
                    }
                    else if (rbprice2.Checked == true)
                    {
                        pselect = 2;
                    }

                    value = txtvalue.Text;

                    if (ddlGroup.SelectedIndex != 0)
                    {
                        groupid = ddlGroup.SelectedValue.ToString();
                    }

                    timeused = ddlusage.SelectedValue.ToString();

                    if (ddlusage.SelectedValue == "2")
                    {
                        maxusage = Convert.ToInt32(txtmax.Text);
                    }

                    string sql = "Insert Into tb_PromoCode "
                       + "(Promo_Type1,Promo_Type,Promo_Value,Promo_Added,Promo_Name,Promo_Prefix,Promo_Count"
                       + ",Promo_Timeused,Promo_PriceSelect,con_GroupId,Promo_isitem,Promo_ConfItemId,Promo_CategoryID,Promo_isEarlyBird,ShowID)"
                       + " Values (" + promo_type1 + ", " + promotype + ", " + value + ", getdate(), '" + title + "', '" + prefix + "', '" + noofcode
                       + "', " + timeused + ", " + pselect + ", '" + groupid + "',0,0,0,0,'" + showid + "')";
                    fn.ExecuteSQL(sql);

                    string promocodeid = fn.GetDataByCommand("Select Top(1)Promo_Id From tb_PromoCode Order By Promo_Id Desc", "Promo_Id");

                    SavePCode(noofcode, prefix, promocodeid, title, maxusage, usagepertran, showid);
                }
                else if (RadioButton1.Checked == true)
                {
                    promo_type1 = 2;
                    promotype = ddlpromotype.SelectedValue.ToString();

                    if (rbprice1.Checked == true)
                    {
                        pselect = 1;
                    }
                    else if (rbprice2.Checked == true)
                    {
                        pselect = 2;
                    }

                    value = txtvalue.Text;

                    timeused = ddlusage.SelectedValue.ToString();

                    if (ddlusage.SelectedValue == "2")
                    {
                        maxusage = Convert.ToInt32(txtmax.Text);
                    }

                    string sql = "Insert Into tb_PromoCode "
                       + "(Promo_Type1,Promo_Type,Promo_Value,Promo_Added,Promo_Name,Promo_Prefix,Promo_Count"
                       + ",Promo_Timeused,Promo_PriceSelect,con_GroupId,Promo_isitem,Promo_ConfItemId,Promo_CategoryID,Promo_isEarlyBird,ShowID)"
                       + " Values (" + promo_type1 + ", " + promotype + ", " + value + ", getdate(), '" + title + "', '" + prefix + "', '" + noofcode
                       + "', " + timeused + ", " + pselect + ", 0,0,0,0,0,'" + showid + "')";
                    fn.ExecuteSQL(sql);

                    string promocodeid = fn.GetDataByCommand("Select Top(1)Promo_Id From tb_PromoCode Order By Promo_Id Desc", "Promo_Id");

                    SavePCode(noofcode, prefix, promocodeid, title, maxusage, usagepertran, showid);
                }
                else if (rbcat.Checked == true)
                {
                    promo_type1 = 3;
                    promotype = ddlpromotype.SelectedValue.ToString();

                    if (rbprice1.Checked == true)
                    {
                        pselect = 1;
                    }
                    else if (rbprice2.Checked == true)
                    {
                        pselect = 2;
                    }

                    value = txtvalue.Text;

                    if (ddlcategory.SelectedIndex != 0)
                    {
                        regid = Convert.ToInt32(ddlcategory.SelectedValue.ToString());
                    }

                    timeused = ddlusage.SelectedValue.ToString();

                    if (ddlusage.SelectedValue == "2")
                    {
                        maxusage = Convert.ToInt32(txtmax.Text);
                    }

                    string sql = "Insert Into tb_PromoCode "
                       + "(Promo_Type1,Promo_Type,Promo_Value,Promo_Added,Promo_Name,Promo_Prefix,Promo_Count"
                       + ",Promo_Timeused,Promo_PriceSelect,con_GroupId,Promo_isitem,Promo_ConfItemId,Promo_CategoryID,Promo_isEarlyBird,ShowID)"
                       + " Values (" + promo_type1 + ", " + promotype + ", " + value + ", getdate(), '" + title + "', '" + prefix + "', '" + noofcode
                       + "', " + timeused + ", " + pselect + ", " + "0,0,0," + regid + ",0,'" + showid + "')";
                    fn.ExecuteSQL(sql);

                    string promocodeid = fn.GetDataByCommand("Select Top(1)Promo_Id From tb_PromoCode Order By Promo_Id Desc", "Promo_Id");

                    SavePCode(noofcode, prefix, promocodeid, title, maxusage, usagepertran, showid);
                }
                else if (rbcon1.Checked == true)
                {
                    promo_type1 = 4;
                    promotype = ddltype1.SelectedValue.ToString();

                    if (rbeb1.Checked == true)
                    {
                        pselect = 1;
                    }
                    else if (rbrp1.Checked == true)
                    {
                        pselect = 2;
                    }

                    value = txtvalue1.Text;

                    timeused = ddlusage.SelectedValue.ToString();

                    if (ddlusage.SelectedValue == "2")
                    {
                        maxusage = Convert.ToInt32(txtmax.Text);
                    }

                    isitem = 1;

                    if (ddlconitem.SelectedIndex != 0)
                    {
                        confitemid = Convert.ToInt32(ddlconitem.SelectedValue.ToString());
                    }

                    string sql = "Insert Into tb_PromoCode "
                        + "(Promo_Type1,Promo_Type,Promo_Value,Promo_Added,Promo_Name,Promo_Prefix,Promo_Count"
                        + ",Promo_Timeused,Promo_PriceSelect,con_GroupId,Promo_isitem,Promo_ConfItemId,Promo_CategoryID,Promo_isEarlyBird,ShowID)"
                        + " Values (" + promo_type1 + ", " + promotype + ", " + value + ", getdate(), '" + title + "', '" + prefix + "', '" + noofcode
                        + "', " + timeused + ", " + pselect + ",0," + isitem + "," + confitemid + ",0,0,'" + showid + "')";
                    fn.ExecuteSQL(sql);

                    string promocodeid = fn.GetDataByCommand("Select Top(1)Promo_Id From tb_PromoCode Order By Promo_Id Desc", "Promo_Id");

                    SavePCode(noofcode, prefix, promocodeid, title, maxusage, usagepertran, showid);
                }
                else if (rbeebird.Checked == true)
                {
                    promo_type1 = 5;
                    promotype = "0";
                    pselect = 0;
                    value = "0";

                    string sql = "Insert Into tb_PromoCode "
                       + "(Promo_Type1,Promo_Type,Promo_Value,Promo_Added,Promo_Name,Promo_Prefix,Promo_Count"
                       + ",Promo_Timeused,Promo_PriceSelect,con_GroupId,Promo_isitem,Promo_ConfItemId,Promo_CategoryID,Promo_isEarlyBird,ShowID)"
                       + " Values (" + promo_type1 + ", " + promotype + ", " + value + ", getdate(), '" + title + "', '" + prefix + "', '" + noofcode
                       + "', 1, " + pselect + ", " + "0,0,0,0,1,'" + showid + "')";
                    fn.ExecuteSQL(sql);

                    string promocodeid = fn.GetDataByCommand("Select Top(1)Promo_Id From tb_PromoCode Order By Promo_Id Desc", "Promo_Id");

                    SavePCode(noofcode, prefix, promocodeid, title, maxusage, usagepertran, showid);
                }
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region chkChange1
    protected void chkChange1(object sender, EventArgs e)
    {
        trgroup.Visible = true;
        trtype.Visible = true;
        trcount.Visible = true;
        trvalue.Visible = true;
        trcat.Visible = false;
        tb1.Visible = true;
        trcon2.Visible = false;
        trusagepertran.Visible = true;
    }
    #endregion

    #region chkChange2
    protected void chkChange2(object sender, EventArgs e)
    {
        trgroup.Visible = false;
        trtype.Visible = true;
        trcount.Visible = true;
        trvalue.Visible = true;
        trcat.Visible = false;
        tb1.Visible = true;
        trcon2.Visible = false;
        trusagepertran.Visible = true;
    }
    #endregion

    #region chkChange3
    protected void chkChange3(object sender, EventArgs e)
    {
        trgroup.Visible = false;
        trtype.Visible = true;
        trcount.Visible = true;
        trvalue.Visible = true;
        trcat.Visible = true;
        tb1.Visible = true;
        trcon2.Visible = false;
        trusagepertran.Visible = true;
    }
    #endregion

    #region chkChange4
    protected void chkChange4(object sender, EventArgs e)
    {
        trgroup.Visible = false;
        trtype.Visible = false;
        trcount.Visible = true;
        trvalue.Visible = false;
        trcat.Visible = true;
        tb1.Visible = false;
        trcon2.Visible = true;
        trusagepertran.Visible = true;
    }
    #endregion

    #region chkChange5
    protected void chkChange5(object sender, EventArgs e)
    {
        trgroup.Visible = false;
        trtype.Visible = false;
        trcount.Visible = false;
        trvalue.Visible = false;
        trcat.Visible = false;
        tb1.Visible = false;
        trcon2.Visible = false;
        trusagepertran.Visible = true;
    }
    #endregion

    #region GeneratePromoCode
    protected string GeneratePromoCode(string prefix)
    {
        string ncode = fn.CreateRandomCode(8);
        string pcode = prefix + ncode;

        return pcode;
    }
    #endregion

    #region SavePCode
    protected void SavePCode(int count, string prefix, string promocodeid, string promotitle, int maxusage, int usagepertran, string showid)
    {
        DataTable NewDt = new DataTable();
        NewDt.Columns.Add("No", typeof(string));
        NewDt.Columns.Add("Promo Code", typeof(string));
        NewDt.Columns.Add("Promo Title", typeof(string));

        for (int x = 0; x < count; x++)
        {
            string pcode = string.Empty;
            pcode = GeneratePromoCode(prefix);

            try
            {
                string query = "Select Count(*) as ctcount From tb_PromoList Where PromoCode='" + pcode + "' And ShowID=@SHWID";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);

                int countcode = Convert.ToInt32(fn.GetDataByCommand(query, "ctcount", pList));
                if (countcode > 0)
                {
                    pcode = GeneratePromoCode(prefix);
                }
                string sqlinsert = "Insert Into tb_PromoList (PromocodeID,PromoCode,DateCreated, MaxUsage,NoOfUsagePerTransaction,ShowID) Values (" 
                    + promocodeid + ",'" + pcode + "',getdate(),'" + maxusage + "'," + usagepertran + ",'" + showid + "')";
                fn.ExecuteSQL(sqlinsert);
            }
            catch (Exception ex)
            { }

            DataRow newdr = NewDt.NewRow();
            newdr[0] = (x + 1).ToString();
            newdr[1] = pcode;
            newdr[2] = promotitle;
            NewDt.Rows.Add(newdr);
            System.Threading.Thread.Sleep(50);
        }

        divMsg.Visible = true;
        divMsg.Attributes.Remove("class");
        divMsg.Attributes.Add("class", "alert alert-success alert-dismissible");
        lblMsg.Text = "Promo codes has been successfully created.";
        ExportToExcel.DataTable3Excel(NewDt, "Promo_Code_" + txttitle.Text);
    }
    #endregion

    #region ddlusage_SelectedIndexChanged
    protected void ddlusage_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlusage.SelectedValue == "2")
        {
            trMax.Visible = true;
        }
        else
        {
            trMax.Visible = false;
        }
    }
    #endregion
}
