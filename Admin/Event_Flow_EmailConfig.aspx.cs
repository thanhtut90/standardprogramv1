﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System.Text.RegularExpressions;
using Corpit.Site.Email;
using Corpit.Email;
public partial class Event_Flow_EmailConfig : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    SqlConnection con;
    SqlCommand cmd;
    SqlDataReader dr;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //    Master.setopenemail();
            bool isOK = false;
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["STP"] != null)
            {
                //string emalID = Request.Params["EMLID"];
                string showID = cFun.DecryptValue(Request.Params["SHW"].ToString());
                SiteEmailConfigHelper siteEConfigHelper = new SiteEmailConfigHelper(fn);
                SiteEmailConfig siteEConfig = siteEConfigHelper.GetSiteEmailConfig(showID);
                if (siteEConfig != null && !string.IsNullOrEmpty(siteEConfig.ShowID))
                {
                    txtPortalRefShowID.Text = siteEConfig.RefEmailProjID;

                    loadlist();
                    string flw = cFun.DecryptValue(Request.Params["FLW"].ToString());
                    string step = cFun.DecryptValue(Request.Params["STP"].ToString());
                    FlowControler flowControler = new FlowControler(fn);
                    flowControler.FillCurrentStepInfo(flw, step);

                    txtFlowKey.Text = flowControler.FlowKey;
                    bool isEmailSetUpFinished = false;
                    if (siteEConfig != null && !string.IsNullOrEmpty(siteEConfig.RefEmailProjID))
                    {
                        ePoratalShowConfigJsonData showConfig = new ePoratalShowConfigJsonData();
                        showConfig.ShowID = siteEConfig.RefEmailProjID;
                        EmailHelper sHelper = new EmailHelper();
                        showConfig = sHelper.GetShowConfigSettings(showConfig);
                        if (showConfig != null && !string.IsNullOrEmpty(showConfig.SMTPUsername))
                            isEmailSetUpFinished = true;
                    }

                    if (!string.IsNullOrEmpty(txtFlowKey.Text) && isEmailSetUpFinished)
                        isOK = true;
                }

            }
            else
                Response.Redirect("login.aspx");

            if (isOK)
            {

                PanelDisplay.Visible = true;
                PanelEmpty.Visible = false;
                // Redirect To setup
            }
            else
            {

                PanelDisplay.Visible = false;
                PanelEmpty.Visible = true;
            }

        }
    }


    #region LoadList (bind data from tb_site_Template_DataSource table to ddltempdatasource dropdownlist control)
    private void loadlist()
    {
        string constr = fn.ConnString;
        using (con = new SqlConnection(constr))
        {
            using (cmd = new SqlCommand("Select * From tb_site_flow_Email_DataSource"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddltempdatasource.DataSource = cmd.ExecuteReader();
                ddltempdatasource.DataTextField = "Table_Name";
                ddltempdatasource.DataValueField = "DataSourceID";
                ddltempdatasource.DataBind();
                con.Close();
            }
        }
    }
    #endregion

    #region bindRightSight
    private void bindRightSight()
    {
        string tbname = ddltempdatasource.SelectedItem.Text;
        tempdatasource.SelectParameters["tablename"].DefaultValue = tbname;
    }
    #endregion

    #region loadvalue (Load RadGrid selected row data from tb_site_Template and tb_site_Template_DataSource tables to bind the respective controls while edit)
    private void loadvalue(string tempid)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            string connStr = fn.ConnString;
            string query = "Select * From tb_site_Template Where temp_id='" + tempid + "'";
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();

                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            //txttempid.Text = dataReader["temp_id"].ToString();
                            //txttemptitle.Text = dataReader["temp_name"].ToString();
                            string tempbdid = dataReader["temp_BaseDSource"].ToString();

                            try
                            {
                                if (!String.IsNullOrEmpty(tempbdid))
                                {
                                    ListItem listItem = ddltempdatasource.Items.FindByValue(tempbdid);
                                    if (listItem != null)
                                    {
                                        ddltempdatasource.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                            }

                            txtsubject.Text = dataReader["temp_Subject"].ToString();
                            string tempvalue = dataReader["temp_value"].ToString();
                            txtvalue.Text = Server.HtmlDecode(tempvalue);
                        }
                        dataReader.Close();
                        dataReader.Dispose();
                    }

                    connection.Close();
                }
            }

            string tbname = ddltempdatasource.SelectedItem.Text;
            bindRightSight();
            lbltblfk.Text = fn.GetDataByCommand("Select Table_FilterKey From tb_site_Template_DataSource Where Table_Name = '" + tbname + "'", "Table_FilterKey");
            lbltblcec.Text = fn.GetDataByCommand("Select Table_CotactEmailColumn From tb_site_Template_DataSource Where Table_Name = '" + tbname + "'", "Table_CotactEmailColumn");
            if (lbltblfk.Text == "" || lbltblfk.Text == null || lbltblfk.Text == "0")
            {
                lbltblfk.Text = "nil";
            }
            if (lbltblcec.Text == "" || lbltblcec.Text == null || lbltblcec.Text == "0")
            {
                lbltblcec.Text = "nil";
            }

            //EmailHelpers eh = new EmailHelpers(fn);
            //string image = SiteDefaultValue.DefaultEmailBindDelimiter + SiteDefaultValue.DefaultSiteBanner + SiteDefaultValue.DefaultEmailBindDelimiter;
            //string imageurl = eh.BindTemplateWithStandardSetting(image, showid);
            //imgdefaultbanner.ImageUrl = imageurl;
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_site_Template table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        try
        {
            string showID = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            string step = cFun.DecryptValue(Request.QueryString["STP"].ToString());
            string flowID = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

            FlowControler fControler = new FlowControler(fn);
            fControler.FillCurrentStepInfo(flowID, step);
            SiteEmailConfigHelper siteConfig = new SiteEmailConfigHelper(fn);
            List<FlowEmail> fEmailList = siteConfig.GetFlowStepIndxEmailList(fControler.FlowKey, showID);
            GKeyMaster.DataSource = fEmailList;

            if (fEmailList.Count > 0)
            {
                panelsitetemplate.Visible = false;
                EmailList.Visible = true;
            }
            else
            {
                panelsitetemplate.Visible = true;
                EmailList.Visible = false;
            }
        }
        catch
        {
        }
    }
    #endregion
    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, GridCommandEventArgs e)
    {
        string emailID = string.Empty;
        string refKEy = "";
        string baseSource = "";
        foreach (GridDataItem item in GKeyMaster.SelectedItems)
        {
            emailID = item["EmailID"].Text;
            refKEy = item["EmailPortalRefKey"].Text;
            baseSource = item["Email_BaseDSource"].Text;
        }
        if (!string.IsNullOrEmpty(emailID) && !string.IsNullOrEmpty(refKEy) && !string.IsNullOrEmpty(baseSource))
        {
          bool isOK=   LoadEmailTemplate(emailID, refKEy, baseSource);
            if (isOK)
            {
                panelsitetemplate.Visible = true;
                ddltempdatasource_SelectedIndexChanged(this, null);
            }
            else
                panelsitetemplate.Visible = false;
        }

    }

    private bool LoadEmailTemplate(string emailID, string refKey, string baseSource)
    {
        bool isOK = false;

        try
        {
            ddltempdatasource.ClearSelection();
            txtEmailID.Text = emailID;
            txtEmailRefKey.Text = refKey;
            ListItem li = ddltempdatasource.Items.FindByValue(baseSource);
            if (li != null) li.Selected = true;

            EmailHelper eHelp = new EmailHelper();
            ePortalEmailMaster eMaster = new ePortalEmailMaster();
            eMaster.EmailAccessKey = txtEmailRefKey.Text;
            eMaster.ShowID = txtPortalRefShowID.Text;
            List<ePortalEmailTemplateDetail> tempList = eHelp.GetEmailTemplateList(eMaster);
            foreach (ePortalEmailTemplateDetail tmp in tempList)
            {
                txtvalue.Text =Server.HtmlDecode( tmp.TemplateContent);
                txtsubject.Text = tmp.TemplateTitle;
                txtEmailDesc.Text = tmp.TemplateName;
                txtPortalTemplateID.Text = tmp.TemplateID;
                txtPortalEmailID.Text = tmp.EmailID;
                dateFrom.SelectedDate = tmp.ValidFrom;
                dateTo.SelectedDate = tmp.ValidTo;
            }
            SiteEmailConfigHelper siteEConfigHelper = new SiteEmailConfigHelper(fn);
            string showID = cFun.DecryptValue(Request.Params["SHW"].ToString());
           FlowEmail femail= siteEConfigHelper.GetFlowEmail(showID, emailID);
            if (!string.IsNullOrEmpty(femail.ShowID))
            {
                ddlIsAcknowledgement.ClearSelection();
                ddlSendToAllDelegate.ClearSelection();
                ddlSendCondition.ClearSelection();
                ddlEmailType.ClearSelection();
                ListItem liAc = ddlIsAcknowledgement.Items.FindByValue(femail.IsAcknowledgement);
                if (liAc != null) liAc.Selected = true;

                ListItem liSendAll = ddlSendToAllDelegate.Items.FindByValue(femail.SendToAllDelegate);
                if (liSendAll != null) liSendAll.Selected = true;

                ListItem liEType = ddlEmailType.Items.FindByValue(femail.EmailType);
                if (liEType != null) liEType.Selected = true;

                ListItem liCondition = ddlSendCondition.Items.FindByValue(femail.EmailSendCondition);
                if (liCondition != null) liCondition.Selected = true;
            }
            isOK = true;
        }
        catch { }

        return isOK;
    }
    #endregion
    #region ddltempdatasource_SelectedIndexChanged (get related data from tb_site_Template_DataSource table and bind to lbltblfk and lbltblces controls)
    protected void ddltempdatasource_SelectedIndexChanged(object sender, EventArgs e)
    {
        string conString = fn.ConnString;
        string tbname = ddltempdatasource.SelectedItem.Text;
        bindRightSight();
        lbltblfk.Text = fn.GetDataByCommand("Select Table_FilterKey From tb_site_flow_Email_DataSource Where Table_Name = '" + tbname + "'", "Table_FilterKey");
        lbltblcec.Text = fn.GetDataByCommand("Select Table_CotactEmailColumn From tb_site_flow_Email_DataSource Where Table_Name = '" + tbname + "'", "Table_CotactEmailColumn");
        if (lbltblfk.Text == "" || lbltblfk.Text == null || lbltblfk.Text == "0")
        {
            lbltblfk.Text = "nil";
        }
        if (lbltblcec.Text == "" || lbltblcec.Text == null || lbltblcec.Text == "0")
        {
            lbltblcec.Text = "nil";
        }
    }
    #endregion


    #region getdatasourcename (Get Table Name from tb_site_Template_DataSource table according to DataSourceID)
    public string getdatasourcename(string datasourceid)
    {
        return fn.GetDataByCommand("Select Table_Name From tb_site_Template_DataSource Where DataSourceID ='" + datasourceid + "'", "Table_Name");
    }
    #endregion

    #region btnaddnewrecord_Click ("Add New Template" button click to add new template)
    protected void btnaddnewrecord_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            panelsitetemplate.Visible = true;
            ResetControls(); 
            //   txttempid.ReadOnly = false;

            loadlist();
            bindRightSight();

            //EmailHelpers eh = new EmailHelpers(fn);
            //string image = SiteDefaultValue.DefaultEmailBindDelimiter + SiteDefaultValue.DefaultSiteBanner + SiteDefaultValue.DefaultEmailBindDelimiter;
            //string imageurl = eh.BindTemplateWithStandardSetting(image, showid);
            //imgdefaultbanner.ImageUrl = imageurl;

            ddltempdatasource_SelectedIndexChanged(this, null);
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region btnadd_Click ("Add" button click & Insert data into tb_site_Template table)
    protected void btnadd_Click(object sender, EventArgs e)
    {
        string conString = fn.ConnString;
        //string tempid = txttempid.Text;
        //string tempname = txttemptitle.Text;
        string tempid = "";
        string tempname = "";
        string tempvalue = Server.HtmlEncode(txtvalue.Text);
        string tempbasedsource = ddltempdatasource.SelectedValue;

        string tempsubject = txtsubject.Text;
        if (tempname == "" || tempvalue == "" || tempsubject == "" || tempid == "")
        {
            //lbls.Text = "Please fill up the forms.";
        }
        else
        {
            try
            {
                DataTable dttemplate = fn.GetDatasetByCommand("Select * From tb_site_Template Where temp_id='" + tempid + "'", "ds").Tables[0];
                if (dttemplate.Rows.Count > 0)
                {
                    // GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This Template ID already exist.');", true);
                    return;
                }
                else
                {
                    con = new SqlConnection(conString);
                    con.Open();
                    string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                    cmd = new SqlCommand("Insert into tb_site_Template (temp_id, temp_name, temp_value, temp_BaseDSource, temp_Subject,SHowID)" +
                                         " Values (@temp_id,@temp_name, @temp_value, @temp_BaseDSource, @temp_Subject,@SHowID)");
                    cmd.Parameters.AddWithValue("@temp_id", tempid);
                    cmd.Parameters.AddWithValue("@temp_name", tempname);
                    cmd.Parameters.AddWithValue("@temp_value", tempvalue);
                    cmd.Parameters.AddWithValue("@temp_BaseDSource", tempbasedsource);
                    cmd.Parameters.AddWithValue("@temp_Subject", tempsubject);
                    cmd.Parameters.AddWithValue("@SHowID", showid);
                    cmd.Connection = con;

                    int rowUpdated1 = cmd.ExecuteNonQuery();
                    if (rowUpdated1 == 1)
                    {
                        panelsitetemplate.Visible = false;
                        ResetControls();
                        //  GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        panelsitetemplate.Visible = false;
                        ResetControls();
                        //   GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Saving error, try again.');", true);
                    }
                    con.Close();
                }
            }
            catch (Exception ea)
            {
                // lbls.Text += ea.ToString();
            }
        }
    }
    #endregion

    #region btnsave_Click (Update data to tb_site_Template table)
    protected void btnsave_Click(object sender, EventArgs e)
    {

        // Creating New
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["STP"] != null)
        {
            try
            {
                string showID = cFun.DecryptValue(Request.Params["SHW"].ToString());
                SiteSettings sr = new SiteSettings(fn, showID);
                string showPrefix = sr.SitePrefix;
                string PortalShowID = txtPortalRefShowID.Text;
                string siteShowID = showID;
                string FlowKey = txtFlowKey.Text;
                string createBy = "Admin";

                string siteEmailID = txtEmailID.Text;
                string emailRefKey = txtEmailRefKey.Text;
                string portalTemplateID = txtPortalTemplateID.Text;
                string eDataSource = ddltempdatasource.SelectedValue;
                string emailSubject = txtsubject.Text;
                string emailDesc = txtEmailDesc.Text;
                string emailContent = Server.HtmlEncode(txtvalue.Text);
                string validFrom = dateFrom.SelectedDate.ToString();
                string validTO = dateTo.SelectedDate.ToString();

                EmailHelper eHelp = new EmailHelper();
                ePorataSetuplEmailJsonData eJson = new ePorataSetuplEmailJsonData();
                SiteEmailConfigHelper siteConfig = new SiteEmailConfigHelper(fn);
                eJson.PortalShowID = PortalShowID;
                eJson.ShowPrefix = showPrefix;
                eJson.EmailDesc = emailDesc;
                eJson.EmailAccessKey = emailRefKey;  // set Empty if New
                Template tem = new Template(); //Tempalte
                tem.ShowiD = eJson.PortalShowID;
                tem.ID = portalTemplateID;// TemplateID
                tem.TemplateContent = emailContent;
                tem.TemplateTitle = emailSubject;
                tem.TemplateName = emailDesc;
                eJson.eTempalate = tem;
                eJson.ValidFrom = DateTime.Parse(validFrom);
                eJson.ValidTo = DateTime.Parse(validTO);
                eJson.Status = RecordStatus.ACTIVE;
                eJson.CreateBy = createBy;
                List<EmailParam> eParam = siteConfig.ExtractPamraListfromStr(emailContent); // extract Param from emailBody;
                string refKey = eHelp.SetupEmailConfig(eJson); // Send Email to Portal DB
                if (!string.IsNullOrEmpty(refKey))
                {
                    FlowEmail fEmail = new FlowEmail();
                    //GEnereate new EMailID 
                    fEmail.EmailID = siteEmailID; // set Empty if New
                    fEmail.ShowID = siteShowID;
                    fEmail.EmailPortalRefKey = refKey;
                    fEmail.EmailPortalRefShow = PortalShowID;
                    fEmail.Email_BaseDSource = eDataSource;
                    fEmail.FlowKey = FlowKey;
                    fEmail.Status = RecordStatus.ACTIVE;
                    fEmail.Email_Desc = emailDesc;
                    fEmail.SendToAllDelegate = ddlSendToAllDelegate.SelectedValue;
                    fEmail.IsAcknowledgement = ddlIsAcknowledgement.SelectedValue;
                    fEmail.EmailType = ddlEmailType.SelectedValue;
                    fEmail.EmailSendCondition = ddlSendCondition.SelectedValue;
                    if (ddltempdatasource.SelectedItem.Text == "tb_RegGroup")
                        fEmail.EmailRegType = "G";
                    if (ddltempdatasource.SelectedItem.Text == "tb_RegDelegate" || ddltempdatasource.SelectedItem.Text == "DelegateWithGroupCompanyInfo" || ddltempdatasource.SelectedItem.Text == "DelegateWithRefValue")
                        fEmail.EmailRegType = "D";

                    siteConfig.SetFlowEmail(fEmail, eParam);
                }
            }
            catch { }
        }
    }
    protected void btnsave_Click2(object sender, EventArgs e)
    {
        string text = Server.HtmlEncode(txtvalue.Text);
        if (Page.IsValid)
        {
            string conString = fn.ConnString;
            //string tempid = txttempid.Text;
            //string tempname = txttemptitle.Text;
            string tempid = "";
            string tempname = "";
            string tempvalue = Server.HtmlEncode(txtvalue.Text);
            string tempbasedsource = ddltempdatasource.SelectedValue;

            string tempsubject = txtsubject.Text;
            if (tempname == "" || tempvalue == "" || tempsubject == "" || tempid == "")
            {
                // lbls.Text = "Please fill up the forms.";
            }
            else
            {
                try
                {
                    con = new SqlConnection(conString);
                    con.Open();
                    cmd = new SqlCommand("Update tb_site_Template Set temp_name = @temp_name, temp_value = @temp_value," +
                    "temp_BaseDSource = @temp_BaseDSource, temp_Subject = @temp_Subject WHERE temp_id='" + tempid + "'");

                    cmd.Parameters.AddWithValue("@temp_name", tempname);
                    cmd.Parameters.AddWithValue("@temp_value", tempvalue);
                    cmd.Parameters.AddWithValue("@temp_BaseDSource", tempbasedsource);
                    cmd.Parameters.AddWithValue("@temp_Subject", tempsubject);

                    cmd.Connection = con;

                    int rowUpdated1 = cmd.ExecuteNonQuery();
                    if (rowUpdated1 == 1)
                    {
                        panelsitetemplate.Visible = false;
                        ResetControls();
                        //     GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        panelsitetemplate.Visible = false;
                        ResetControls();
                        //    GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updating error, try again.');", true);
                    }
                    con.Close();
                }
                catch (Exception ea)
                {
                    //   lbls.Text += ea.ToString();
                }
            }
        }
    }
    protected void btnTemplateConfig_Click(object sender, EventArgs e)
    {

        //  Response.Redirect();
        string url = "Event_Flow_EmailHTMLConfig.aspx" + Request.Url.Query.ToString();
        string script = String.Format("window.open('{0}','_blank');", url);
        ClientScript.RegisterStartupScript(GetType(), "scr", script, true);
    }
    #endregion

    #region btnpreview_Click (Show the template in formated html)
    protected void btnpreview_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            //   EmailHelpers eh = new EmailHelpers(fn);
            string template = "";// eh.BindTemplateWithStandardSetting(txtvalue.Text, showid);
                                 //    lblheader.Text = "Preview";
            panelpreview.Visible = true;
            panelsitetemplate.Visible = false;
            btnback.Visible = true;
            lblpreview.Text = template;
            //    btnaddnewrecord.Visible = false;
            //      GKeyMaster.Visible = false;
            ResetControls();
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region btnback_Click
    protected void btnback_Click(object sender, EventArgs e)
    {
        //lblheader.Text = "Site Template";
        //panelpreview.Visible = false;
        //panelsitetemplate.Visible = true;
        //btnback.Visible = false;
        //btnaddnewrecord.Visible = true;
        //GKeyMaster.Visible = true;
        Response.Redirect("Event_Settings_EmailSettings?SHW=" + Request.QueryString["SHW"].ToString());

    }
    #endregion

    #region linkbtndefaultbanner_Click (right-sight "Default Banner" link button click event & bind data to "Template Value" textbox (txtvalue))
    protected void linkbtndefaultbanner_Click(object sender, EventArgs e)
    {
        //EmailHelpers eh = new EmailHelpers(fn);
        //string image = SiteDefaultValue.DefaultEmailBindDelimiter + SiteDefaultValue.DefaultSiteBanner + SiteDefaultValue.DefaultEmailBindDelimiter;
        //string imagetemplate = "<img alt='banner' src='image' width='1000px' />";
        //txtvalue.Text += imagetemplate;
    }
    #endregion
    #region Encryted Regno
    protected void btnEncryRegno_Click(object sender, EventArgs e)
    {
        //EmailHelpers eh = new EmailHelpers(fn);
        //string image = SiteDefaultValue.DefaultEmailBindDelimiter + SiteDefaultValue.DefaultSiteBanner + SiteDefaultValue.DefaultEmailBindDelimiter;
        //string imagetemplate = "<img alt='banner' src='image' width='1000px' />";
        //txtvalue.Text += imagetemplate;
    }
    #endregion

    #region imgdefaultbanner_Click (right-sight image(banner) click event & bind data to "Template Value" textbox (txtvalue))
    protected void imgdefaultbanner_Click(object sender, ImageClickEventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            //EmailHelpers eh = new EmailHelpers(fn);
            //string image = SiteDefaultValue.DefaultEmailBindDelimiter + SiteDefaultValue.DefaultSiteBanner + SiteDefaultValue.DefaultEmailBindDelimiter;
            //string imageurl = eh.BindTemplateWithStandardSetting(image, showid);
            //string imagetemplate = "<img alt='banner' src='" + imageurl + "' width='1000px' />";
            //txtvalue.Text += imagetemplate;
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region lbtntemp_Click (right-sight data field link button click event & bind data to "Template Value" textbox (txtvalue))
    protected void lbtntemp_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)(sender);
        string text = lb.CommandArgument.ToString() ;
        txtvalue.Text += SiteDefaultValue.DefaultEmailBindDelimiter + text + SiteDefaultValue.DefaultEmailBindDelimiter;
    }
    #endregion

    protected void btnLinkToConfig_Onclick(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string url = "Event_Settings_MasterPage?SHW=" + Request.Params["SHW"].ToString();
            Response.Redirect(url);
        }
    }
        
    #region ResetControls (Clear data from controls)
    private void ResetControls()
    {
        //txttempid.Text = "";
        //txttempid.Enabled = true;
        //txttemptitle.Text = "";
        txtsubject.Text = "";
        txtvalue.Text = "";
        txtEmailDesc.Text = "";
        txtEmailID.Text="";
        txtEmailRefKey.Text = "";
        txtPortalTemplateID.Text = "";
        //     lbls.Text = "";
    }
    #endregion
}