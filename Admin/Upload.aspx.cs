﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using Corpit.Upload;
using System.Data;
using System.Data.SqlClient;
using Corpit.Registration;
using Corpit.Site.Utilities;
using System.Text;
using ClosedXML.Excel;
using Corpit.Site.Email;
using Corpit.Logging;
using Corpit.Utilities;

public partial class Admin_Upload : System.Web.UI.Page
{
    private static string[] checkingNotBindDDLShowName = new string[] { "Food Japan" };
    private static string[] checkingForGenerateReLoginPasswordShowNames = new string[] { "IDEM Online Registration" };

    #region ServerFuz
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["userid"] != null)
            {
                string userID = Session["userid"].ToString();

                txtShow.Text = getShowID(userID);
                txtUserID.Text = userID;
                LoadUploadList(txtShow.Text);
            }
            else
            {
                divUpload.Visible = false;
            }
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        string filename = "";
        string showID = txtShow.Text;
        string flowID = ddlFlow.SelectedValue;
        bool isOk = false;
        if (excelUpload.HasFile == false)
        {
            ClientScript.RegisterStartupScript(typeof(Page), "myscript", "<script>alert('Please choose the excel file before you upload');</script>");
            return;
        }
        if (excelUpload.HasFile == true)
        {
            string filePath = SaveFile(showID);
            if (!string.IsNullOrEmpty(filePath))
            {
                DataTable dtList = ExcelToDataList(showID, flowID, filePath);
                if (dtList.Rows.Count > 0)
                {
                    divSaveData.Visible = true;
                    divUpload.Visible = false;

                    gvList.DataSource = dtList;
                    gvList.DataBind();
                    Session["dt"] = dtList;
                }
            }
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)Session["dt"];

        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                string showID = txtShow.Text;
                string flowID = ddlFlow.SelectedValue;
                divSaveData.Visible = false;

                DataTable dtUnSuccessList = new DataTable();
                DataTable successList = InputToDB(dt, showID, flowID, ref dtUnSuccessList);
                if (successList.Rows.Count > 0)
                {

                    gSuccess.DataSource = successList;
                    gSuccess.DataBind();
                    PanelSuccess.Visible = true;
                }

                divUnSuccessList.Visible = false;
                if (dtUnSuccessList.Rows.Count > 0)
                {
                    gvUnSuccessList.DataSource = dtUnSuccessList;
                    gvUnSuccessList.DataBind();
                    divUnSuccessList.Visible = true;
                }
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session["dt"] = null;
        Session.Remove("dt");
        divUpload.Visible = true;
        divSaveData.Visible = false;
    }

    #endregion

    #region ExcelToDBList
    private string SaveFile(string showID)
    {
        string rtnFildPath = "";
        try
        {
            string datepor = DateTime.Now.ToString("ddMMyyyy");
            string Timepor = DateTime.Now.ToString("hhmmss");
            string ext = System.IO.Path.GetExtension(excelUpload.FileName);
            string filename = showID + datepor + Timepor + ext;

            string folder = GetUploadFolderPath(showID);
            if (!string.IsNullOrEmpty(folder)) // file Folder is not Emapty
            {
                string filePath = folder + "\\" + filename;
                excelUpload.SaveAs(filePath);
                rtnFildPath = filePath;
            }
        }
        catch (Exception ex) { rtnFildPath = ""; }
        return rtnFildPath;
    }

    private DataTable ExcelToDataList(string showID, string flowID, string filePath)
    {
        DataTable dtList = new DataTable();
        try
        {

            Functionality fn = new Functionality();
            UploadHelper uHelp = new UploadHelper(fn);
            UploadConfig uConfig = uHelp.GetUploadConfig(showID, flowID);
            if (!string.IsNullOrEmpty(uConfig.ShowID)) // Upload Config  Exist
            {
                List<UploadConfigField> fieldList = uHelp.GetUploadConfigFields(showID, flowID, uConfig.UNo);
                if (fieldList.Count > 0) // List is Not Empty
                {
                    dtList = uHelp.ExcelToDataList(showID, flowID, filePath, fieldList);

                }
            }

        }
        catch (Exception ex) { }
        return dtList;
    }
    private string GetUploadFolderPath(string showID)
    {
        string folder = "";
        try
        {
            if (ConfigurationManager.AppSettings["UploadExcelFilePath"] != null)
            {
                folder = ConfigurationManager.AppSettings["UploadExcelFilePath"].Trim('\\') + "\\" + showID;
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                    if (!Directory.Exists(folder))
                        folder = "";
                }
            }
        }
        catch (Exception ex) { }
        return folder;
    }

    #endregion

    #region InputToDB
    private DataTable InputToDB(DataTable dList, string showID, string flowID, ref DataTable unsuccessList)
    {
        Functionality fn = new Functionality();
        UploadHelper uHelp = new UploadHelper(fn);
        UploadConfig uConfig = uHelp.GetUploadConfig(showID, flowID);
        DataTable successList = dList.Clone();
        if (successList.Rows.Count > 0)
            successList.Rows.Clear();

        unsuccessList = new DataTable();
        unsuccessList = dList.Clone();
        if (unsuccessList.Rows.Count > 0)
            unsuccessList.Rows.Clear();

        if (!string.IsNullOrEmpty(uConfig.ShowID)) // Upload Config  Exist
        {
            List<UploadConfigField> fieldList = uHelp.GetUploadConfigFields(showID, flowID, uConfig.UNo);
            if (fieldList.Count > 0) // List is Not Empty
            {
                if (dList.Columns.Count == fieldList.Count)
                {
                    UploadInputHelper inputHelp = new UploadInputHelper(fn);
                    List<UploadFormTypeRange> rangeList = uHelp.ListSegment(fieldList);
                    RegGroupObj rgp = new RegGroupObj(fn);
                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    string grpNum = "";
                    UploadGroupData groupData = new UploadGroupData();
                    string GroupIndex = "";
                    string currentGroupIndex = "";
                    bool isCreatNewGroup = false;
                    bool isOKgroup = false;
                    bool hasCompany = true;

                    CategoryObj catObj = new CategoryObj(fn);
                    FlowURLQuery urlQuery = new FlowURLQuery("");
                    urlQuery.CurrShowID = showID;
                    urlQuery.FlowID = flowID;

                    UploadFormTypeRange groupRange = rangeList.FirstOrDefault(x => x.FormType == UploadFormType.Group);
                    UploadFormTypeRange gIndx = rangeList.FirstOrDefault(x => x.FormType == UploadFormType.GroupIndex);
                    UploadFormTypeRange delRange = rangeList.FirstOrDefault(x => x.FormType == UploadFormType.Delegate);
                    bool isFirstTime = true;
                    int con_categoryID = 0;
                    bool isGroup = false;
                    int dListCount = dList.Rows.Count;

                    List<UploadedRegIDsParam> lstUploadedRegIDs = new List<UploadedRegIDsParam>();
                    FlowControler flwObj = new FlowControler(fn);
                    FlowMaster flwMaster = flwObj.GetFlowMasterConfig(flowID);

                    for (int i = 0; i < dList.Rows.Count; i++)
                    {
                        //Loop Rows

                        if (uConfig.UType == UploadTypes.Group)
                        {
                            //Group

                            if (gIndx != null)
                            {
                                List<UploadDataParam> pList = MakeDBParmaList(ref dList, ref fieldList, i, gIndx, showID);
                                if (pList.Count > 0)
                                    currentGroupIndex = pList[0].ParamValue;

                                if (GroupIndex != currentGroupIndex) // if Same Keep Same one
                                {
                                    GroupIndex = currentGroupIndex;
                                    isCreatNewGroup = true;
                                }
                                else
                                    isCreatNewGroup = false;
                            }
                            isGroup = true;
                        }
                        else if (uConfig.UType == UploadTypes.Individual)
                        {
                            //Individual
                            isCreatNewGroup = true;
                            isGroup = false;
                        }

                        if (isCreatNewGroup)
                        {
                            groupData = new UploadGroupData();
                            grpNum = rgp.GenGroupNumber(showID);
                            groupData.ShowID = showID;
                            groupData.RG_urlFlowID = flowID;
                            groupData.recycle = "0";
                            groupData.Stage = flwObj.GetLatestStep(flowID);
                            groupData.Status = "1";
                            groupData.GroupID = grpNum;
                            if (isGroup)
                                groupData.IsMultiple = UploadTypes.Group;
                            if (groupRange != null)
                            {
                                groupData.ParamList = MakeDBParmaList(ref dList, ref fieldList, i, groupRange, showID);
                            }
                            //Save Group

                            isOKgroup = inputHelp.GroupDataInput(groupData);

                            #region list down Regno to send confirmation email
                            if (isGroup)
                            {
                                /**/
                                FlowURLQuery urlQueryUploaded = new FlowURLQuery("");
                                urlQueryUploaded.CurrShowID = showID;
                                urlQueryUploaded.FlowID = flowID;
                                urlQueryUploaded.GoupRegID = groupData.GroupID;
                                urlQueryUploaded.DelegateID = "";
                                urlQueryUploaded.CurrIndex = flwObj.GetLatestStep(flowID);
                                UploadedRegIDsParam uploadedIDObj = new UploadedRegIDsParam();
                                uploadedIDObj.RegType = UploadTypes.Group;
                                uploadedIDObj.RegGroupID = groupData.GroupID;
                                uploadedIDObj.urlQuery = urlQueryUploaded;
                                lstUploadedRegIDs.Add(uploadedIDObj);
                                /**/
                            }
                            #endregion
                        }

                        if (isOKgroup)
                        {
                            LogUpload(showID, flowID, groupData.GroupID, "");/*group data insert log*/
                            if (hasCompany)
                            {
                                UploadFormTypeRange comRange = rangeList.FirstOrDefault(x => x.FormType == UploadFormType.Company);
                                if (comRange != null)
                                { //input group
                                    UploadCompanyData uCom = new UploadCompanyData();
                                    uCom.ShowID = showID;
                                    uCom.RC_urlFlowID = flowID;
                                    uCom.recycle = "0";
                                    uCom.Stage = flwObj.GetLatestStep(flowID);
                                    uCom.GroupID = groupData.GroupID;

                                    uCom.ParamList = MakeDBParmaList(ref dList, ref fieldList, i, comRange, showID);
                                    inputHelp.CompanyDataInput(uCom);
                                    LogUpload(showID, flowID, groupData.GroupID, "");/*company data insert log*/
                                }
                                else
                                    hasCompany = false;
                            }
                            //Delegate Input
                            UploadDelegateData uDelegate = new UploadDelegateData();
                            uDelegate.ShowID = showID;
                            uDelegate.reg_urlflowID = flowID;
                            uDelegate.recycle = "0";
                            uDelegate.Stage = flwObj.GetLatestStep(flowID);
                            uDelegate.Status = "1";
                            uDelegate.GroupID = groupData.GroupID;
                            uDelegate.Regno = rgd.GenDelegateNumber(showID);
                            if (delRange != null)
                            {
                                uDelegate.ParamList = MakeDBParmaList(ref dList, ref fieldList, i, delRange, showID);
                            }

                            bool isAlreadyExist = false;
                            #region checking duplicate data (added on 9-7-2019 - than)
                            rgd.showID = showID;
                            rgd.fname = uDelegate.ParamList.Where(a=>a.ParamKey == "Reg_fName").Select(a => a.ParamValue).FirstOrDefault();
                            rgd.fname = (!string.IsNullOrEmpty(rgd.fname) ? rgd.fname : "");
                            rgd.lname = uDelegate.ParamList.Where(a => a.ParamKey == "reg_LName").Select(a => a.ParamValue).FirstOrDefault();
                            rgd.lname = (!string.IsNullOrEmpty(rgd.lname) ? rgd.lname : "");
                            rgd.email = uDelegate.ParamList.Where(a => a.ParamKey == "reg_Email").Select(a => a.ParamValue).FirstOrDefault();
                            rgd.email = (!string.IsNullOrEmpty(rgd.email) ? rgd.email : "");
                            if (flwMaster.isDuplicateChecking == 1)//***check in complete registration status
                            {
                                isAlreadyExist = checkInsertExist(showID, rgd.fname, rgd.lname, rgd.email);
                            }
                            else if (flwMaster.isDuplicateChecking == 2)//***check in all(complete,pending) registration status
                            {
                                isAlreadyExist = checkInsertExistWithoutRegStatus(showID, rgd.fname, rgd.lname, rgd.email);
                            }
                            else if (flwMaster.isDuplicateChecking == 3)//***check in all(complete,pending) flowid
                            {
                                isAlreadyExist = checkInsertExistWithFLowID(showID, rgd.fname, rgd.lname, rgd.email, flowID);
                            }
                            #endregion

                            if (isAlreadyExist)
                            {
                                DataRow dRow = dList.Rows[i];
                                unsuccessList.Rows.Add(dRow.ItemArray);
                            }
                            else
                            {
                                bool isOK = inputHelp.DelegateDataInput(uDelegate);
                                if (isOK)
                                {
                                    LogUpload(showID, flowID, groupData.GroupID, uDelegate.Regno);/*delegate data insert log*/
                                    saveRegLoginPassword(uDelegate.Regno, showID, flowID);/*generate re-login username and password (IDEM) added on 10-12-2019 th*/
                                    if (isFirstTime)
                                    {
                                        urlQuery.GoupRegID = uDelegate.GroupID;
                                        urlQuery.DelegateID = uDelegate.Regno;
                                        int.TryParse(catObj.checkCategory(urlQuery, uDelegate.Regno, ""), out con_categoryID);
                                        isFirstTime = false;
                                    }
                                    rgd.updateCategoryID(uDelegate.Regno, con_categoryID, uDelegate.ShowID);

                                    #region list down Regno to send confirmation email
                                    if (!isGroup)
                                    {
                                        /**/
                                        FlowURLQuery urlQueryUploaded = new FlowURLQuery("");
                                        urlQueryUploaded.CurrShowID = showID;
                                        urlQueryUploaded.FlowID = flowID;
                                        urlQueryUploaded.GoupRegID = uDelegate.GroupID;
                                        urlQueryUploaded.DelegateID = uDelegate.Regno;
                                        urlQueryUploaded.CurrIndex = flwObj.GetLatestStep(flowID);
                                        UploadedRegIDsParam uploadedIDObj = new UploadedRegIDsParam();
                                        uploadedIDObj.RegType = UploadTypes.Individual;
                                        uploadedIDObj.RegGroupID = uDelegate.GroupID;
                                        uploadedIDObj.Regno = uDelegate.Regno;
                                        uploadedIDObj.urlQuery = urlQueryUploaded;
                                        lstUploadedRegIDs.Add(uploadedIDObj);
                                        /**/
                                    }
                                    #endregion

                                    DataRow dRow = dList.Rows[i];
                                    successList.Rows.Add(dRow.ItemArray);
                                }
                            }
                        }
                    }

                    #region send confirmation email
                    if (lstUploadedRegIDs != null && lstUploadedRegIDs.Count > 0)
                    {
                        foreach (UploadedRegIDsParam upID in lstUploadedRegIDs)
                        {
                            if (upID.RegType == UploadTypes.Group)
                            {
                                /*send confirmation email for Group/Visitor Reg [Regno(Delegate Regno) is empty as if this is group registration*/
                                btnSendVisitorConfirmationEmail("", upID.RegGroupID, flowID, showID, upID.urlQuery);
                            }
                            else if (upID.RegType == UploadTypes.Individual)
                            {
                                /*send confirmation email for Individual/Visitor Registration*/
                                btnSendVisitorConfirmationEmail(upID.Regno, upID.RegGroupID, flowID, showID, upID.urlQuery);
                            }
                        }
                    }
                    #endregion
                }
            }
        }

        return successList;
    }

    private List<UploadDataParam> MakeDBParmaList(ref DataTable dList, ref List<UploadConfigField> fieldList, int currentRow, UploadFormTypeRange range, string showID)
    {
        List<UploadDataParam> uList = new List<UploadDataParam>();
        try
        {
            CommonFuns cFun = new CommonFuns();
            if (dList.Columns.Count > range.StartPoint && dList.Columns.Count > range.EndtPoint && fieldList.Count > range.StartPoint && fieldList.Count > range.EndtPoint)
            {
                string pVal = "";
                string passShowID = "";
                Functionality fn = new Functionality();
                UploadHelper uHelp = new UploadHelper(fn);
                for (int i = range.StartPoint; i <= range.EndtPoint; i++)
                {
                    pVal = "";
                    UploadDataParam dataParm = new UploadDataParam();
                    dataParm.ParamKey = fieldList[i].FieldName;
                    pVal = dList.Rows[currentRow][i].ToString();
                    if (!string.IsNullOrEmpty(fieldList[i].RefTableName))
                    {
                        passShowID = "";
                        if (fieldList[i].WithShowID == "1")
                            passShowID = showID;
                        pVal = uHelp.UploadColRefValByName(fieldList[i].RefTableName, fieldList[i].RefTableValueCol, fieldList[i].RefTableDescCol, pVal, passShowID);
                    }


                    dataParm.ParamValue = cFun.solveSQL(pVal);
                    uList.Add(dataParm);
                }
            }
        }
        catch (Exception ex) { }
        return uList;
    }

    #endregion

    protected string getShowID(string userID)
    {
        string showID = "";
        try
        {
            Functionality fn = new Functionality();
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);

        }

        return showID;
    }

    private void LoadUploadList(string showiD)
    {
        Functionality fn = new Functionality();
        string query = "select * from tb_site_Flow_Upload_Config where ShowID=@showiD And RegistrationType='VISITOR'";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("showiD", SqlDbType.NVarChar);
        spar.Value = showiD;
        pList.Add(spar);
        ddlFlow.Items.Clear();
        DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ListItem li = new ListItem();
            li.Value = dt.Rows[i]["flowID"].ToString();
            li.Text = dt.Rows[i]["UploadName"].ToString();
            ddlFlow.Items.Add(li);
        }

    }

    #region lnkDownloadTemplate_Click
    static string _excelFileType = ".xlsx";
    static int _rowno = 2;
    protected void lnkDownloadTemplate_Click(object sender, EventArgs e)
    {
        try
        {
            string showID = txtShow.Text;
            string flowID = ddlFlow.SelectedValue;
            Functionality fn = new Functionality();
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showID);
            UploadHelper uHelp = new UploadHelper(fn);
            UploadConfig uConfig = uHelp.GetUploadConfig(showID, flowID);
            if (!string.IsNullOrEmpty(uConfig.ShowID)) // Upload Config  Exist
            {
                DataTable NewDt = new DataTable();
                List<UploadConfigField> fieldList = uHelp.GetUploadConfigFields(showID, flowID, uConfig.UNo);
                List<UploadConfigBindingField> bindingFieldList = new List<UploadConfigBindingField>();
                if (fieldList.Count > 0) // List is Not Empty
                {
                    int columnCount = 1;
                    foreach (UploadConfigField upf in fieldList)
                    {
                        NewDt.Columns.Add(upf.FieldDesc, typeof(string));
                        if (!string.IsNullOrEmpty(upf.RefTable))
                        {
                            bool notBindDDLAnotherCondition = false;
                            if(checkingNotBindDDLShowName.Contains(shw.SHW_Name) && (upf.RefTable == "4434" || upf.RefTable == "4437" || upf.RefTable == "4438"))
                            {
                                notBindDDLAnotherCondition = true;
                            }

                            if (upf.RefTable != "4432" && (!notBindDDLAnotherCondition))
                            {
                                UploadConfigBindingField bindingFieldObj = new UploadConfigBindingField();
                                bindingFieldObj.ColumnName = upf.FieldDesc;
                                bindingFieldObj.CoulumnIndexAlpha = UploadHelper.getAlphabet(columnCount) + _rowno;
                                bindingFieldObj.RefFieldTableName = upf.RefTableName;
                                bindingFieldObj.RefFieldColDesc = upf.RefTableDescCol;
                                bindingFieldObj.RefFieldColRecycle = upf.RefColRecycle;
                                bindingFieldObj.RefFieldColSorting = upf.RefColSorting;
                                bindingFieldList.Add(bindingFieldObj);
                            }
                        }

                        columnCount++;
                    }

                    columnCount = columnCount - 1;

                    #region bind congress header
                    string paymentMethodColumnIndex = "";
                    FlowControler Flw = new FlowControler(fn);
                    bool confExist = Flw.checkPageExist(flowID, SiteDefaultValue.constantConfName);
                    if (confExist)
                    {
                        //////*main congress selection
                        uHelp.getMainCongressHeaders(showID, flowID, ref NewDt, ref columnCount);
                        //////*congress selection
                        //////*dependent congress selection
                        uHelp.getDependentCongressHeaders(showID, flowID, ref NewDt, ref columnCount);
                        //////*dependent congress selection
                        NewDt.Columns.Add(InvoiceRefValues.PaymentMethod);
                        columnCount++;
                        paymentMethodColumnIndex = UploadHelper.getAlphabet(columnCount) + _rowno;
                        NewDt.Columns.Add(InvoiceRefValues.InvoiceStatus);
                        columnCount++;
                    }
                    #endregion

                    string sheetName = "Sheet1";
                    string path = "~/DownloadExhExcels/";
                    string uploadFlowName = ddlFlow.SelectedItem.Text;
                    string downloadTmpName = "_ImportTemplate_";
                    downloadTmpName = uploadFlowName + downloadTmpName;
                    string filename = downloadTmpName.Replace(" ", "") + DateTime.Now.Ticks;
                    string exportedPath = ExportToExcel(NewDt, filename, sheetName, path);

                    if (!string.IsNullOrEmpty(exportedPath))
                    {
                        OpenExcelWorkBook(exportedPath, sheetName, bindingFieldList, showID, paymentMethodColumnIndex);

                        if (exportedPath.LastIndexOf("/") > 0)
                        {
                            //divDownloadTemplate.Visible = false;
                            //divBrowse.Visible = true;
                            //divList.Visible = false;

                            DownLoadFile(exportedPath);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #region BindExcel Helper
    // Creates a new Excel Application
    private void OpenExcelWorkBook(string filename, string sheetName, List<UploadConfigBindingField> lstBindingField, string showid, string paymentMethodColIndexAlpha)
    {
        try
        {
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            app.Visible = true;  // Makes Excel visible to the user.

            string sWbFileName = AppDomain.CurrentDomain.BaseDirectory.Replace("bin\\Debug\\", "") + filename.Replace("~/", "").Replace("/", "\\");
            Microsoft.Office.Interop.Excel.Workbook xWb = null;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            try
            {
                xWb = app.Workbooks.Open(
                                sWbFileName,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing,
                                Type.Missing);
            }
            catch
            {
                //Create a new workbook if the existing workbook failed to open.
                xWb = app.Workbooks.Add();
            }

            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xWb.Sheets[sheetName];

            // Call Populate DropDown sheet
            PopulateDropdown(xlWorkSheet, lstBindingField, showid, paymentMethodColIndexAlpha);

            xWb.Save();
            xWb.Close(true);
            app.Quit();
            //app = null;
            //xWb = null;
            System.Runtime.InteropServices.Marshal.ReleaseComObject(app.Workbooks);
        }
        catch (Exception ex)
        { }
    }

    // Populate Dropdown
    private void PopulateDropdown(Microsoft.Office.Interop.Excel.Worksheet oSheet, List<UploadConfigBindingField> lstBindingField, string showid, string paymentMethodColIndexAlpha)
    {
        try
        {
            if (lstBindingField != null && lstBindingField.Count > 0)
            {
                foreach (UploadConfigBindingField bindFieldObj in lstBindingField)
                {
                    oSheet.Range[bindFieldObj.CoulumnIndexAlpha].Validation.Add(Microsoft.Office.Interop.Excel.XlDVType.xlValidateList, Type.Missing,
                        Microsoft.Office.Interop.Excel.XlFormatConditionOperator.xlBetween,
                        getValuesFromXML(bindFieldObj.RefFieldTableName, bindFieldObj.RefFieldColDesc, bindFieldObj.RefFieldColRecycle, bindFieldObj.RefFieldColSorting, showid));
                }

                try
                {
                    if (!string.IsNullOrEmpty(paymentMethodColIndexAlpha))
                    {
                        oSheet.Range[paymentMethodColIndexAlpha].Validation.Add(Microsoft.Office.Interop.Excel.XlDVType.xlValidateList, Type.Missing,
                               Microsoft.Office.Interop.Excel.XlFormatConditionOperator.xlBetween,
                               getValuesFromXMLPaymentMethod(showid));
                    }
                }
                catch (Exception ex)
                { }
            }
        }
        catch (Exception ex)
        { }
    }

    // Get values from XML
    private string getValuesFromXML(string tblName, string colNameOfRetunVal, string colRecycle, string colSorting, string showid)
    {
        StringBuilder strBuilder = new StringBuilder();
        try
        {
            Functionality fn = new Functionality();
            UploadHelper uHelp = new UploadHelper(fn);
            strBuilder.Append(uHelp.getUploadColRefValNameAsCommaSeparatedString(tblName, colNameOfRetunVal, colRecycle, colSorting, showid));
        }
        catch (Exception ex)
        {
        }
        return strBuilder.ToString();
    }
    private string getValuesFromXMLPaymentMethod(string showid)
    {
        StringBuilder strBuilder = new StringBuilder();
        try
        {
            Functionality fn = new Functionality();
            UploadHelper uHelp = new UploadHelper(fn);
            strBuilder.Append(uHelp.getPaymentMethodAsCommaSeparatedString(showid));
        }
        catch (Exception ex)
        {
        }
        return strBuilder.ToString();
    }
    #endregion
    #region ExportToExcel (Generate Excel in directory)
    public string ExportToExcel(DataTable dt, string FileName, string sheetName, string path)
    {
        string savePath = string.Empty;
        string filepath = Server.MapPath(path) + FileName + _excelFileType;
        try
        {
            XLWorkbook wb = new XLWorkbook();
            var ws = wb.Worksheets.Add(sheetName);
            ws.Cell(2, 1).InsertTable(dt);
            ws.Row(1).Delete();
            ws.Tables.FirstOrDefault().ShowAutoFilter = false;//remove first blank column and filter
            ws.FirstRow().AddConditionalFormat().WhenGreaterThan(20000).Fill.SetBackgroundColor(XLColor.OrangeRed);//add bg color at first row(header)
            wb.SaveAs(filepath);
            wb.Dispose();

            savePath = path + FileName + _excelFileType;
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return savePath;
    }
    #endregion
    #region DownLoad Excel File
    protected void DownLoadFile(string filepath)
    {
        try
        {
            String sPath = Server.MapPath(filepath);

            int index = sPath.LastIndexOf("\\");
            String fileName = sPath.Substring(index + 1);

            Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.TransmitFile(filepath);
            Response.End();
        }
        catch (Exception ex) { }
    }
    #endregion
    #endregion

    #region SendConfirmationEmail
    protected void btnSendVisitorConfirmationEmail(string regno, string groupid, string flowID, string showID, FlowURLQuery urlQuery)
    {
        try
        {
            bool isSuccess = true;
            string updatedUser = txtUserID.Text.Trim() + " sent from Upload.aspx";
            Functionality fn = new Functionality();
            FlowControler flwObj = new FlowControler(fn, urlQuery);
            EmailHelper eHelper = new EmailHelper();
            eHelper.SendCurrentFlowStepEmail(urlQuery);

            #region use SendEmailFromBackend
            //string eType = EmailHTMLTemlateType.Confirmation;
            //string emailRegType = BackendRegType.backendRegType_Delegate;// "D";
            //EmailDSourceKey sourcKey = new EmailDSourceKey();
            //string groupID = groupid;//eg. "2421113";
            //string delegateID = regno;//eg. "24210092";//Blank
            //if(string.IsNullOrEmpty(regno))
            //{
            //    emailRegType = BackendRegType.backendRegType_Group;
            //}
            //sourcKey.AddKeyToList("ShowID", showID);
            //sourcKey.AddKeyToList("RegGroupID", groupID);
            //sourcKey.AddKeyToList("Regno", delegateID);
            //sourcKey.AddKeyToList("FlowID", flowID);
            //sourcKey.AddKeyToList("INVID", "");
            //isSuccess = eHelper.SendEmailFromBackend(eType, sourcKey, emailRegType, updatedUser);///////*
            #endregion

            insertLogGen(groupid, regno, updatedUser + " -Status:" + isSuccess, flowID, showID);
        }
        catch (Exception ex)
        { }
    }
    private void insertLogGen(string groupid, string delegateid, string action, string flowid, string showid)
    {
        try
        {
            Functionality fn = new Functionality();
            LogGenEmail lggenemail = new LogGenEmail(fn);
            lggenemail.type = action;
            lggenemail.RefNumber = groupid = "-" + delegateid;
            lggenemail.description = action;
            lggenemail.remark = action;
            lggenemail.step = GetUserIP();
            lggenemail.writeLog();
        }
        catch (Exception ex)
        { }
    }
    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
    #endregion

    #region Log Upload
    private void LogUpload(string showid, string flowid, string reggroupid, string regno)
    {
        try
        {
            Functionality fn = new Functionality();
            string userid = txtUserID.Text.Trim();
            string remark = "Backend Upload Page";
            string sqlInsert = "Insert Into tb_LogUpload (ShowID, FlowID, RegGroupID, Regno, UserID, MachineIP, Remark) Values (@ShowID,@FlowID,@RegGroupID,@Regno,@UserID,@MachineIP,@Remark)";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("RegGroupID", SqlDbType.NVarChar);
            SqlParameter spar4 = new SqlParameter("Regno", SqlDbType.NVarChar);
            SqlParameter spar5 = new SqlParameter("UserID", SqlDbType.NVarChar);
            SqlParameter spar6 = new SqlParameter("MachineIP", SqlDbType.NVarChar);
            SqlParameter spar7 = new SqlParameter("Remark", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            spar3.Value = reggroupid;
            spar4.Value = regno;
            spar5.Value = userid;
            spar6.Value = GetUserIP();
            spar7.Value = remark;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);
            pList.Add(spar4);
            pList.Add(spar5);
            pList.Add(spar6);
            pList.Add(spar7);
            fn.ExecuteSQLWithParameters(sqlInsert, pList);
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region checkInsertExist
    public bool checkInsertExist(string showID, string fname, string lname, string email)
    {
        bool isExist = false;

        try
        {
            CommonFuns cFun = new CommonFuns();
            fname = cFun.solveSQL(fname);
            lname = cFun.solveSQL(lname);

            string fnameFilter = string.Format(" reg_FName='{0}'", fname);
            if (string.IsNullOrEmpty(fname))
            {
                fnameFilter = string.Format(" (reg_FName='{0}' Or reg_FName Is NULL)", fname);
            }

            string lnameFilter = string.Format(" reg_LName='{0}'", lname);
            if(string.IsNullOrEmpty(lname))
            {
                lnameFilter = string.Format(" (reg_LName='{0}' Or reg_LName Is NULL)", lname);
            }
            Functionality fn = new Functionality();
            string sql = string.Format("Select * From tb_RegDelegate Where {0} And {1} And reg_Email='{2}' And ShowID=@SHWID And recycle=0 And reg_Status='1'", fnameFilter, lnameFilter, email);
            //RegGroupID={0} And groupid, 
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);

            DataTable dt = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                isExist = true;
            }
        }
        catch (Exception ex) { }

        return isExist;
    }
    public bool checkInsertExistWithoutRegStatus(string showID, string fname, string lname, string email)
    {
        bool isExist = false;

        try
        {
            CommonFuns cFun = new CommonFuns();
            fname = cFun.solveSQL(fname);
            lname = cFun.solveSQL(lname);

            string fnameFilter = string.Format(" reg_FName='{0}'", fname);
            if (string.IsNullOrEmpty(fname))
            {
                fnameFilter = string.Format(" (reg_FName='{0}' Or reg_FName Is NULL)", fname);
            }

            string lnameFilter = string.Format(" reg_LName='{0}'", lname);
            if (string.IsNullOrEmpty(lname))
            {
                lnameFilter = string.Format(" (reg_LName='{0}' Or reg_LName Is NULL)", lname);
            }
            Functionality fn = new Functionality();
            string sql = string.Format("Select * From tb_RegDelegate Where {0} And {1} And reg_Email='{2}' And ShowID=@SHWID And recycle=0", fnameFilter, lnameFilter, email);
            //RegGroupID={0} And groupid, 
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);

            DataTable dt = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                isExist = true;
            }
        }
        catch (Exception ex) { }

        return isExist;
    }
    public bool checkInsertExistWithFLowID(string showID, string fname, string lname, string email, string flowID)
    {
        bool isExist = false;

        try
        {
            CommonFuns cFun = new CommonFuns();
            fname = cFun.solveSQL(fname);
            lname = cFun.solveSQL(lname);

            string fnameFilter = string.Format(" reg_FName='{0}'", fname);
            if (string.IsNullOrEmpty(fname))
            {
                fnameFilter = string.Format(" (reg_FName='{0}' Or reg_FName Is NULL)", fname);
            }

            string lnameFilter = string.Format(" reg_LName='{0}'", lname);
            if (string.IsNullOrEmpty(lname))
            {
                lnameFilter = string.Format(" (reg_LName='{0}' Or reg_LName Is NULL)", lname);
            }
            Functionality fn = new Functionality();
            string sql = string.Format("Select * From tb_RegDelegate Where {0} And {1} And reg_Email='{2}' And ShowID=@SHWID And recycle=0 And reg_urlFlowID=@FlowID", fnameFilter, lnameFilter, email);
            //RegGroupID={0} And groupid, 
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            spar.Value = showID;
            spar2.Value = flowID;
            pList.Add(spar);
            pList.Add(spar2);

            DataTable dt = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                isExist = true;
            }
        }
        catch (Exception ex) { }

        return isExist;
    }
    #endregion

    #region saveRegLoginPassword added on 10-12-2019
    private string saveRegLoginPassword(string regno, string showid, string flowid)
    {
        string refNo = "";
        try
        {
            Functionality fn = new Functionality();
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            if (checkingForGenerateReLoginPasswordShowNames.Contains(shw.SHW_Name))
            {
                RegLoginPassword regLP = new RegLoginPassword(fn);
                string password = regLP.getPasswordByOwnerID(regno, showid);
                if (string.IsNullOrEmpty(password) || string.IsNullOrWhiteSpace(password))
                {
                    SetUpController setObj = new SetUpController(fn);
                    string reandomCode = setObj.generateReferenceNo(showid, flowid, regno, 7);
                    password = reandomCode;
                    regLP.createRegLoginPassword(password, regno, showid);
                }
            }
        }
        catch (Exception ex)
        { }

        return refNo;
    }
    #endregion
}