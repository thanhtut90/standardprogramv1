﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="FormManagement_Delegate.aspx.cs" Inherits="Admin_FormManagement_Delegate" ValidateRequest="false" EnableEventValidation="false"%>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        input[type="checkbox"], input[type="radio"]
        {
            margin : 4px !important;
        }
                label {
    font-weight: 400 !important;
}
    </style>

    <script type="text/javascript">
        function showField(type)
        {
            if (type == 'salutation')
            {
                var cbox = document.getElementById('<%=chkshowSalutation.ClientID %>');
                var content1 = document.getElementById('dvsalutation');
                var content2 = document.getElementById('<%=txtSalutation.ClientID %>');
                var content3 = document.getElementById('dvsumsalutation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else
                {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'fname') {

                var cbox = document.getElementById('<%=chkshowFname.ClientID %>');
                var content1 = document.getElementById('dvfname');
                var content2 = document.getElementById('<%=txtFname.ClientID %>');
                var content3 = document.getElementById('dvsumfname');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'lname') {

                var cbox = document.getElementById('<%=chkshowLname.ClientID %>');
                var content1 = document.getElementById('dvlname');
                var content2 = document.getElementById('<%=txtLname.ClientID %>');
                var content3 = document.getElementById('dvsumlname');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';
                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'oname') {

                var cbox = document.getElementById('<%=chkshowOname.ClientID %>');
                var content1 = document.getElementById('dvoname');
                var content2 = document.getElementById('<%=txtOname.ClientID %>');
                var content3 = document.getElementById('dvsumoname');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'passportno') {

                var cbox = document.getElementById('<%=chkshowPassportNo.ClientID %>');
                var content1 = document.getElementById('dvpassportno');
                var content2 = document.getElementById('<%=txtPassportNo.ClientID %>');
                var content3 = document.getElementById('dvsumpassportno');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'isregistered') {

                var cbox = document.getElementById('<%=chkshowIsRegistered.ClientID %>');
                var content1 = document.getElementById('dvisregistered');
                var content2 = document.getElementById('<%=txtIsRegistered.ClientID %>');
                var content3 = document.getElementById('dvsumisregistered');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'regspecific') {

                var cbox = document.getElementById('<%=chkshowRegSpecific.ClientID %>');
                var content1 = document.getElementById('dvregspecific');
                var content2 = document.getElementById('<%=txtRegSpecific.ClientID %>');
                var content3 = document.getElementById('dvsumregspecific');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'idno') {

                var cbox = document.getElementById('<%=chkshowIDNo.ClientID %>');
                var content1 = document.getElementById('dvidno');
                var content2 = document.getElementById('<%=txtIDNo.ClientID %>');
                var content3 = document.getElementById('dvsumidno');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'designation') {

                var cbox = document.getElementById('<%=chkshowDesignation.ClientID %>');
                var content1 = document.getElementById('dvdesignation');
                var content2 = document.getElementById('<%=txtDesignation.ClientID %>');
                var content3 = document.getElementById('dvsumdesignation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'profession') {

                var cbox = document.getElementById('<%=chkshowProfession.ClientID %>');
                var content1 = document.getElementById('dvprofession');
                var content2 = document.getElementById('<%=txtProfession.ClientID %>');
                var content3 = document.getElementById('dvsumprofession');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'department') {

                var cbox = document.getElementById('<%=chkshowDepartment.ClientID %>');
                var content1 = document.getElementById('dvdepartment');
                var content2 = document.getElementById('<%=txtDepartment.ClientID %>');
                var content3 = document.getElementById('dvsumdepartment');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'organization') {

                var cbox = document.getElementById('<%=chkshowOrganization.ClientID %>');
                var content1 = document.getElementById('dvorganization');
                var content2 = document.getElementById('<%=txtOrganization.ClientID %>');
                var content3 = document.getElementById('dvsumorganization');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'institution') {

                var cbox = document.getElementById('<%=chkshowInstitution.ClientID %>');
                var content1 = document.getElementById('dvinstitution');
                var content2 = document.getElementById('<%=txtInstitution.ClientID %>');
                var content3 = document.getElementById('dvsuminstitution');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'address1') {

                var cbox = document.getElementById('<%=chkshowAddress1.ClientID %>');
                var content1 = document.getElementById('dvaddress1');
                var content2 = document.getElementById('<%=txtAddress1.ClientID %>');
                var content3 = document.getElementById('dvsumaddress1');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'address2') {

                var cbox = document.getElementById('<%=chkshowAddress2.ClientID %>');
                var content1 = document.getElementById('dvaddress2');
                var content2 = document.getElementById('<%=txtAddress2.ClientID %>');
                var content3 = document.getElementById('dvsumaddress2');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'address3') {

                var cbox = document.getElementById('<%=chkshowAddress3.ClientID %>');
                var content1 = document.getElementById('dvaddress3');
                var content2 = document.getElementById('<%=txtAddress3.ClientID %>');
                var content3 = document.getElementById('dvsumaddress3');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'address4') {

                var cbox = document.getElementById('<%=chkshowAddress4.ClientID %>');
                var content1 = document.getElementById('dvaddress4');
                var content2 = document.getElementById('<%=txtAddress4.ClientID %>');
                var content3 = document.getElementById('dvsumaddress4');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'city') {

                var cbox = document.getElementById('<%=chkshowCity.ClientID %>');
                var content1 = document.getElementById('dvcity');
                var content2 = document.getElementById('<%=txtCity.ClientID %>');
                var content3 = document.getElementById('dvsumcity');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'state') {

                var cbox = document.getElementById('<%=chkshowState.ClientID %>');
                var content1 = document.getElementById('dvstate');
                var content2 = document.getElementById('<%=txtState.ClientID %>');
                var content3 = document.getElementById('dvsumstate');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'postalcode') {

                var cbox = document.getElementById('<%=chkshowPostalCode.ClientID %>');
                var content1 = document.getElementById('dvpostalcode');
                var content2 = document.getElementById('<%=txtPostalCode.ClientID %>');
                var content3 = document.getElementById('dvsumpostalcode');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'country') {

                var cbox = document.getElementById('<%=chkshowCountry.ClientID %>');
                var content1 = document.getElementById('dvcountry');
                var content2 = document.getElementById('<%=txtCountry.ClientID %>');
                var content3 = document.getElementById('dvsumcountry');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'rcountry') {

                var cbox = document.getElementById('<%=chkshowRCountry.ClientID %>');
                var content1 = document.getElementById('dvrcountry');
                var content2 = document.getElementById('<%=txtRCountry.ClientID %>');
                var content3 = document.getElementById('dvsumrcountry');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'telcc') {

                var cbox = document.getElementById('<%=chkshowTelCC.ClientID %>');
                var content1 = document.getElementById('dvtelcc');
                var content2 = document.getElementById('<%=txtTelCC.ClientID %>');
                var content3 = document.getElementById('dvsumtelcc');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'telac') {

                var cbox = document.getElementById('<%=chkshowTelAC.ClientID %>');
                var content1 = document.getElementById('dvtelac');
                var content2 = document.getElementById('<%=txtTelAC.ClientID %>');
                var content3 = document.getElementById('dvsumtelac');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'tel') {

                var cbox = document.getElementById('<%=chkshowTel.ClientID %>');
                var content1 = document.getElementById('dvtel');
                var content2 = document.getElementById('<%=txtTel.ClientID %>');
                var content3 = document.getElementById('dvsumtel');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'mobilecc') {

                var cbox = document.getElementById('<%=chkshowMobileCC.ClientID %>');
                var content1 = document.getElementById('dvmobilecc');
                var content2 = document.getElementById('<%=txtMobileCC.ClientID %>');
                var content3 = document.getElementById('dvsummobilecc');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'mobileac') {

                var cbox = document.getElementById('<%=chkshowMobileAC.ClientID %>');
                var content1 = document.getElementById('dvmobileac');
                var content2 = document.getElementById('<%=txtMobileAC.ClientID %>');
                var content3 = document.getElementById('dvsummobileac');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'mobile') {

                var cbox = document.getElementById('<%=chkshowMobile.ClientID %>');
                var content1 = document.getElementById('dvmobile');
                var content2 = document.getElementById('<%=txtMobile.ClientID %>');
                var content3 = document.getElementById('dvsummobile');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'faxcc') {

                var cbox = document.getElementById('<%=chkshowFaxCC.ClientID %>');
                var content1 = document.getElementById('dvfaxcc');
                var content2 = document.getElementById('<%=txtFaxCC.ClientID %>');
                var content3 = document.getElementById('dvsumfaxcc');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'faxac') {

                var cbox = document.getElementById('<%=chkshowFaxAC.ClientID %>');
                var content1 = document.getElementById('dvfaxac');
                var content2 = document.getElementById('<%=txtFaxAC.ClientID %>');
                var content3 = document.getElementById('dvsumfaxac');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'fax') {

                var cbox = document.getElementById('<%=chkshowFax.ClientID %>');
                var content1 = document.getElementById('dvfax');
                var content2 = document.getElementById('<%=txtFax.ClientID %>');
                var content3 = document.getElementById('dvsumfax');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'email') {

                var cbox = document.getElementById('<%=chkshowEmail.ClientID %>');
                var content1 = document.getElementById('dvemail');
                var content2 = document.getElementById('<%=txtEmail.ClientID %>');
                var content3 = document.getElementById('dvsumemail');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'emailconfirmation') {

                var cbox = document.getElementById('<%=chkshowEmailConfirmation.ClientID %>');
                var content1 = document.getElementById('dvemailconfirmation');
                var content2 = document.getElementById('<%=txtEmailConfirmation.ClientID %>');
                var content3 = document.getElementById('dvsumemailconfirmation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'affiliation') {

                var cbox = document.getElementById('<%=chkshowAffiliation.ClientID %>');
                var content1 = document.getElementById('dvaffiliation');
                var content2 = document.getElementById('<%=txtAffiliation.ClientID %>');
                var content3 = document.getElementById('dvsumaffiliation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'dietary') {

                var cbox = document.getElementById('<%=chkshowDietary.ClientID %>');
                var content1 = document.getElementById('dvdietary');
                var content2 = document.getElementById('<%=txtDietary.ClientID %>');
                var content3 = document.getElementById('dvsumdietary');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'nationality') {

                var cbox = document.getElementById('<%=chkshowNationality.ClientID %>');
                var content1 = document.getElementById('dvnationality');
                var content2 = document.getElementById('<%=txtNationality.ClientID %>');
                var content3 = document.getElementById('dvsumnationality');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'membershipno') {

                var cbox = document.getElementById('<%=chkshowMembershipNo.ClientID %>');
                var content1 = document.getElementById('dvmembershipno');
                var content2 = document.getElementById('<%=txtMembershipNo.ClientID %>');
                var content3 = document.getElementById('dvsummembershipno');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                }
            }
            else if (type == 'vname') {

                var cbox = document.getElementById('<%=chkshowVname.ClientID %>');
                var content1 = document.getElementById('dvvname');
                var content2 = document.getElementById('<%=txtVname.ClientID %>');
                var content3 = document.getElementById('dvsumvname');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'vdob') {

                var cbox = document.getElementById('<%=chkshowVDOB.ClientID %>');
                var content1 = document.getElementById('dvvdob');
                var content2 = document.getElementById('<%=txtVDOB.ClientID %>');
                var content3 = document.getElementById('dvsumvdob');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'vpassno') {

                var cbox = document.getElementById('<%=chkshowVPassNo.ClientID %>');
                var content1 = document.getElementById('dvvpassno');
                var content2 = document.getElementById('<%=txtVPassNo.ClientID %>');
                var content3 = document.getElementById('dvsumvpassno');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'vpassexpiry') {

                var cbox = document.getElementById('<%=chkshowVPassExpiry.ClientID %>');
                var content1 = document.getElementById('dvvpassexpiry');
                var content2 = document.getElementById('<%=txtVPassExpiry.ClientID %>');
                var content3 = document.getElementById('dvsumvpassexpiry');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'vpassissuedate') {

                var cbox = document.getElementById('<%=chkshowVPassIssueDate.ClientID %>');
                var content1 = document.getElementById('dvvpassissuedate');
                var content2 = document.getElementById('<%=txtVPassIssueDate.ClientID %>');
                var content3 = document.getElementById('dvsumvpassissuedate');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'vembarkation') {

                var cbox = document.getElementById('<%=chkshowVEmbarkation.ClientID %>');
                var content1 = document.getElementById('dvvembarkation');
                var content2 = document.getElementById('<%=txtVEmbarkation.ClientID %>');
                var content3 = document.getElementById('dvsumvembarkation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'varrivaldate') {

                var cbox = document.getElementById('<%=chkshowVArrivalDate.ClientID %>');
                var content1 = document.getElementById('dvvarrivaldate');
                var content2 = document.getElementById('<%=txtVArrivalDate.ClientID %>');
                var content3 = document.getElementById('dvsumvarrivaldate');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'vcountry') {

                var cbox = document.getElementById('<%=chkshowVCountry.ClientID %>');
                var content1 = document.getElementById('dvvcountry');
                var content2 = document.getElementById('<%=txtVCountry.ClientID %>');
                var content3 = document.getElementById('dvsumvcountry');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'udf_cname') {

                var cbox = document.getElementById('<%=chkshowUDF_CName.ClientID %>');
                var content1 = document.getElementById('dvudf_cname');
                var content2 = document.getElementById('<%=txtUDF_CName.ClientID %>');
                var content3 = document.getElementById('dvsumudf_cname');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'udf_delegatetype') {

                var cbox = document.getElementById('<%=chkshowUDF_DelegateType.ClientID %>');
                var content1 = document.getElementById('dvudf_delegatetype');
                var content2 = document.getElementById('<%=txtUDF_DelegateType.ClientID %>');
                var content3 = document.getElementById('dvsumudf_delegatetype');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'udf_profcategory') {

                var cbox = document.getElementById('<%=chkshowUDF_ProfCategory.ClientID %>');
                var content1 = document.getElementById('dvudf_profcategory');
                var content2 = document.getElementById('<%=txtUDF_ProfCategory.ClientID %>');
                var content3 = document.getElementById('dvsumudf_profcategory');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'udf_profcategoryother') {

                var cbox = document.getElementById('<%=chkshowUDF_ProfCategoryOther.ClientID %>');
                var content1 = document.getElementById('dvudf_profcategoryother');
                var content2 = document.getElementById('<%=txtUDF_ProfCategoryOther.ClientID %>');
                var content3 = document.getElementById('dvsumudf_profcategoryother');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'udf_cpcode') {

                var cbox = document.getElementById('<%=chkshowUDF_CPcode.ClientID %>');
                var content1 = document.getElementById('dvudf_cpcode');
                var content2 = document.getElementById('<%=txtUDF_CPcode.ClientID %>');
                var content3 = document.getElementById('dvsumudf_cpcode');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'udf_cldepartment') {

                var cbox = document.getElementById('<%=chkshowUDF_CLDepartment.ClientID %>');
                var content1 = document.getElementById('dvudf_cldepartment');
                var content2 = document.getElementById('<%=txtUDF_CLDepartment.ClientID %>');
                var content3 = document.getElementById('dvsumudf_cldepartment');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'udf_caddress') {

                var cbox = document.getElementById('<%=chkshowUDF_CAddress.ClientID %>');
                var content1 = document.getElementById('dvudf_caddress');
                var content2 = document.getElementById('<%=txtUDF_CAddress.ClientID %>');
                var content3 = document.getElementById('dvsumudf_caddress');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'udf_clcompany') {

                var cbox = document.getElementById('<%=chkshowUDF_CLCompany.ClientID %>');
                var content1 = document.getElementById('dvudf_clcompany');
                var content2 = document.getElementById('<%=txtUDF_CLCompany.ClientID %>');
                var content3 = document.getElementById('dvsumudf_clcompany');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'udf_clcompanyother') {

                var cbox = document.getElementById('<%=chkshowUDF_CLCompanyOther.ClientID %>');
                var content1 = document.getElementById('dvudf_clcompanyother');
                var content2 = document.getElementById('<%=txtUDF_CLCompanyOther.ClientID %>');
                var content3 = document.getElementById('dvsumudf_clcompanyother');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'udf_ccountry') {

                var cbox = document.getElementById('<%=chkshowUDF_CCountry.ClientID %>');
                var content1 = document.getElementById('dvudf_ccountry');
                var content2 = document.getElementById('<%=txtUDF_CCountry.ClientID %>');
                var content3 = document.getElementById('dvsumudf_ccountry');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'supervisorname') {

                var cbox = document.getElementById('<%=chkshowSupervisorName.ClientID %>');
                var content1 = document.getElementById('dvsupervisorname');
                var content2 = document.getElementById('<%=txtSupervisorName.ClientID %>');
                var content3 = document.getElementById('dvsumsupervisorname');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'supervisordesignation') {

                var cbox = document.getElementById('<%=chkshowSupervisorDesignation.ClientID %>');
                var content1 = document.getElementById('dvsupervisordesignation');
                var content2 = document.getElementById('<%=txtSupervisorDesignation.ClientID %>');
                var content3 = document.getElementById('dvsumsupervisordesignation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'supervisorcontact') {

                var cbox = document.getElementById('<%=chkshowSupervisorContact.ClientID %>');
                var content1 = document.getElementById('dvsupervisorcontact');
                var content2 = document.getElementById('<%=txtSupervisorContact.ClientID %>');
                var content3 = document.getElementById('dvsumsupervisorcontact');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'supervisoremail') {

                var cbox = document.getElementById('<%=chkshowSupervisorEmail.ClientID %>');
                var content1 = document.getElementById('dvsupervisoremail');
                var content2 = document.getElementById('<%=txtSupervisorEmail.ClientID %>');
                var content3 = document.getElementById('dvsumsupervisoremail');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'othersalutation') {

                var cbox = document.getElementById('<%=chkshowOtherSalutation.ClientID %>');
                var content1 = document.getElementById('dvothersalutation');
                var content2 = document.getElementById('<%=txtOtherSalutation.ClientID %>');
                var content3 = document.getElementById('dvsumothersalutation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'otherprofession') {

                var cbox = document.getElementById('<%=chkshowOtherProfession.ClientID %>');
                var content1 = document.getElementById('dvotherprofession');
                var content2 = document.getElementById('<%=txtOtherProfession.ClientID %>');
                var content3 = document.getElementById('dvsumotherprofession');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'otherdepartment') {

                var cbox = document.getElementById('<%=chkshowOtherDepartment.ClientID %>');
                var content1 = document.getElementById('dvotherdepartment');
                var content2 = document.getElementById('<%=txtOtherDepartment.ClientID %>');
                var content3 = document.getElementById('dvsumotherdepartment');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'otherorganization') {

                var cbox = document.getElementById('<%=chkshowOtherOrganization.ClientID %>');
                var content1 = document.getElementById('dvotherorganization');
                var content2 = document.getElementById('<%=txtOtherOrganization.ClientID %>');
                var content3 = document.getElementById('dvsumotherorganization');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'otherinstitution') {

                var cbox = document.getElementById('<%=chkshowOtherInstitution.ClientID %>');
                var content1 = document.getElementById('dvotherinstitution');
                var content2 = document.getElementById('<%=txtOtherInstitution.ClientID %>');
                var content3 = document.getElementById('dvsumotherinstitution');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'aemail') {

                var cbox = document.getElementById('<%=chkshowAEmail.ClientID %>');
                var content1 = document.getElementById('dvaemail');
                var content2 = document.getElementById('<%=txtAEmail.ClientID %>');
                var content3 = document.getElementById('dvsumaemail');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'issms') {

                var cbox = document.getElementById('<%=chkshowIsSMS.ClientID %>');
                var content1 = document.getElementById('dvissms');
                var content2 = document.getElementById('<%=txtIsSMS.ClientID %>');
                var content3 = document.getElementById('dvsumissms');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'age') {

                var cbox = document.getElementById('<%=chkshowAge.ClientID %>');
                var content1 = document.getElementById('dvage');
                var content2 = document.getElementById('<%=txtAge.ClientID %>');
                var content3 = document.getElementById('dvsumage');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'gender') {

                var cbox = document.getElementById('<%=chkshowGender.ClientID %>');
                var content1 = document.getElementById('dvgender');
                var content2 = document.getElementById('<%=txtGender.ClientID %>');
                var content3 = document.getElementById('dvsumgender');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'dob') {

                var cbox = document.getElementById('<%=chkshowDOB.ClientID %>');
                var content1 = document.getElementById('dvdob');
                var content2 = document.getElementById('<%=txtDOB.ClientID %>');
                var content3 = document.getElementById('dvsumdob');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'additional4') {

                var cbox = document.getElementById('<%=chkshowAdditional4.ClientID %>');
                var content1 = document.getElementById('dvadditional4');
                var content2 = document.getElementById('<%=txtAdditional4.ClientID %>');
                var content3 = document.getElementById('dvsumadditional4');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'additional5') {

                var cbox = document.getElementById('<%=chkshowAdditional5.ClientID %>');
                var content1 = document.getElementById('dvadditional5');
                var content2 = document.getElementById('<%=txtAdditional5.ClientID %>');
                var content3 = document.getElementById('dvsumadditional5');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h3 class="box-title">Manage Form (Delegate)</h3>

    Select the field to be used for the registration form.
    <br />
    <br />

    <div class="row">
        <div class="col-md-6">
            <div class="box-body">
                <div class="alert alert-success alert-dismissible" id="divMsg" runat="server" visible="false">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <asp:Label ID="lblMsg" runat="server" ></asp:Label>
                </div>
            </div>
        </div>
    </div>

    <%--<p style="text-align:right;"><a class="ds_next" href='<%= ResolveClientUrl("~/Register.aspx") %>' target="_blank">Go To Registration Form</a></p>--%>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Field Name</th>
                <th>Compulsory Field</th>
                <th>Field Display Text</th>
                <th>Display in Summary & Report</th>
                <th>Use in QR Code (Enter Sequence No.)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <asp:CheckBox ID="chkshowSalutation" runat="server" Text=" Salutation" onclick="showField('salutation');" /> &nbsp&nbsp&nbsp
              ( <a href="ManageSalutation.aspx?SHW=<%=shwID %>" class="text-danger" target="_blank" >Manage Salutation</a>)
                </td>
                <td><div id="dvsalutation" style="display:none;"><asp:CheckBox ID="chkcompSalutation" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtSalutation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumsalutation" style="display:none;"><asp:CheckBox ID="chksumSalutation" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrsalutation" style="display:block;"><asp:TextBox ID="txtqrSalutation" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtqrSalutation" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowFname" runat="server" Text=" Fname" onclick="showField('fname');" /></td>
                <td><div id="dvfname" style="display:none;"><asp:CheckBox ID="chkcompFname" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtFname" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumfname" style="display:none;"><asp:CheckBox ID="chksumFname" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrfname" style="display:block;"><asp:TextBox ID="txtqrFname" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtqrFname" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowLname" runat="server" Text=" Lname" onclick="showField('lname');" /></td>
                <td><div id="dvlname" style="display:none;"><asp:CheckBox ID="chkcompLname" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtLname" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumlname" style="display:none;"><asp:CheckBox ID="chksumLname" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrlname" style="display:block;"><asp:TextBox ID="txtqrLname" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtqrLname" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowOname" runat="server" Text=" Oname" onclick="showField('oname');" /></td>
                <td><div id="dvoname" style="display:none;"><asp:CheckBox ID="chkcompOname" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtOname" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumoname" style="display:none;"><asp:CheckBox ID="chksumOname" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqroname" style="display:block;"><asp:TextBox ID="txtqrOname" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtqrOname" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowPassportNo" runat="server" Text=" Passport No." onclick="showField('passportno');" /></td>
                <td><div id="dvpassportno" style="display:none;"><asp:CheckBox ID="chkcompPassportNo" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtPassportNo" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumpassportno" style="display:none;"><asp:CheckBox ID="chksumPassportNo" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrpassportno" style="display:block;"><asp:TextBox ID="txtqrPassportNo" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtqrPassportNo" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowIsRegistered" runat="server" Text=" IsRegistered" onclick="showField('isregistered');" /></td>
                <td><div id="dvisregistered" style="display:none;"><asp:CheckBox ID="chkcompIsRegistered" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtIsRegistered" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumisregistered" style="display:none;"><asp:CheckBox ID="chksumIsRegistered" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrisregistered" style="display:block;"><asp:TextBox ID="txtqrIsRegistered" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtqrIsRegistered" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowRegSpecific" runat="server" Text=" RegSpecific" onclick="showField('regspecific');" /></td>
                <td><div id="dvregspecific" style="display:none;"><asp:CheckBox ID="chkcompRegSpecific" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td> <asp:TextBox CssClass="form-control" ID="txtRegSpecific" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumregspecific" style="display:none;"><asp:CheckBox ID="chksumRegSpecific" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrregspecific" style="display:block;"><asp:TextBox ID="txtqrRegSpecific" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtqrRegSpecific" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowIDNo" runat="server" Text=" IDNo" onclick="showField('idno');" /></td>
                <td><div id="dvidno" style="display:none;"><asp:CheckBox ID="chkcompIDNo" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtIDNo" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumidno" style="display:none;"><asp:CheckBox ID="chksumIDNo" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqridno" style="display:block;"><asp:TextBox ID="txtqrIDNo" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtqrIDNo" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowDesignation" runat="server" Text=" Designation" onclick="showField('designation');" /></td>
                <td><div id="dvdesignation" style="display:none;"><asp:CheckBox ID="chkcompDesignation" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtDesignation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumdesignation" style="display:none;"><asp:CheckBox ID="chksumDesignation" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrdesignation" style="display:block;"><asp:TextBox ID="txtqrDesignation" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtqrDesignation" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowProfession" runat="server" Text=" Profession" onclick="showField('profession');" />
                    
                      ( <a href="ManageProfession.aspx?SHW=<%=shwID %>" class="text-danger" target="_blank" >Manage Profession</a>)
                </td>
                <td><div id="dvprofession" style="display:none;"><asp:CheckBox ID="chkcompProfession" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtProfession" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumprofession" style="display:none;"><asp:CheckBox ID="chksumProfession" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrprofession" style="display:block;"><asp:TextBox ID="txtqrProfession" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtqrProfession" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowDepartment" runat="server" Text=" Department" onclick="showField('department');" />( <a href="ManageDepartment.aspx?SHW=<%=shwID %>" class="text-danger" target="_blank" >Manage Department</a>)</td>
                <td><div id="dvdepartment" style="display:none;"><asp:CheckBox ID="chkcompDepartment" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtDepartment" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumdepartment" style="display:none;"><asp:CheckBox ID="chksumDepartment" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrdepartment" style="display:block;"><asp:TextBox ID="txtqrDepartment" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtqrDepartment" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowOrganization" runat="server" Text=" Organization" onclick="showField('organization');" />
                    
                      ( <a href="ManageOrganization.aspx?SHW=<%=shwID %>" class="text-danger" target="_blank" >Manage Organisation</a>)
                </td>
                <td><div id="dvorganization" style="display:none;"><asp:CheckBox ID="chkcompOrganization" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtOrganization" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumorganization" style="display:none;"><asp:CheckBox ID="chksumOrganization" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrorganization" style="display:block;"><asp:TextBox ID="txtqrOrganization" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtqrOrganization" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowInstitution" runat="server" Text=" Institution" onclick="showField('institution');" />
                      ( <a href="ManageInstitution.aspx?SHW=<%=shwID %>" class="text-danger" target="_blank" >Manage Institution</a>)
                </td>
                <td><div id="dvinstitution" style="display:none;"><asp:CheckBox ID="chkcompInstitution" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtInstitution" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsuminstitution" style="display:none;"><asp:CheckBox ID="chksumInstitution" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrinstitution" style="display:block;"><asp:TextBox ID="txtqrInstitution" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtqrInstitution" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAddress1" runat="server" Text=" Address1" onclick="showField('address1');" /></td>
                <td><div id="dvaddress1" style="display:none;"><asp:CheckBox ID="chkcompAddress1" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAddress1" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaddress1" style="display:none;"><asp:CheckBox ID="chksumAddress1" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqraddress1" style="display:block;"><asp:TextBox ID="txtqrAddress1" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="txtqrAddress1" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAddress2" runat="server" Text=" Address2" onclick="showField('address2');" /></td>
                <td><div id="dvaddress2" style="display:none;"><asp:CheckBox ID="chkcompAddress2" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAddress2" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaddress2" style="display:none;"><asp:CheckBox ID="chksumAddress2" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqraddress2" style="display:block;"><asp:TextBox ID="txtqrAddress2" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="txtqrAddress2" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAddress3" runat="server" Text=" Address3" onclick="showField('address3');" /></td>
                <td><div id="dvaddress3" style="display:none;"><asp:CheckBox ID="chkcompAddress3" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAddress3" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaddress3" style="display:none;"><asp:CheckBox ID="chksumAddress3" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqraddress3" style="display:block;"><asp:TextBox ID="txtqrAddress3" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="txtqrAddress3" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAddress4" runat="server" Text=" Address4" onclick="showField('address4');" /></td>
                <td><div id="dvaddress4" style="display:none;"><asp:CheckBox ID="chkcompAddress4" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAddress4" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaddress4" style="display:none;"><asp:CheckBox ID="chksumAddress4" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqraddress4" style="display:block;"><asp:TextBox ID="txtqrAddress4" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="txtqrAddress4" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowCity" runat="server" Text=" City" onclick="showField('city');" /></td>
                <td><div id="dvcity" style="display:none;"><asp:CheckBox ID="chkcompCity" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtCity" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumcity" style="display:none;"><asp:CheckBox ID="chksumCity" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrcity" style="display:block;"><asp:TextBox ID="txtqrCity" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="txtqrCity" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowState" runat="server" Text=" State" onclick="showField('state');" /></td>
                <td><div id="dvstate" style="display:none;"><asp:CheckBox ID="chkcompState" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtState" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumstate" style="display:none;"><asp:CheckBox ID="chksumState" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrstate" style="display:block;"><asp:TextBox ID="txtqrState" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="txtqrState" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowPostalCode" runat="server" Text=" Postal Code" onclick="showField('postalcode');" /></td>
                <td><div id="dvpostalcode" style="display:none;"><asp:CheckBox ID="chkcompPostalCode" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtPostalCode" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumpostalcode" style="display:none;"><asp:CheckBox ID="chksumPostalCode" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrpostalcode" style="display:block;"><asp:TextBox ID="txtqrPostalCode" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" TargetControlID="txtqrPostalCode" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowCountry" runat="server" Text=" Country" onclick="showField('country');" />
                        (<a href="ManageCountry.aspx" class="text-danger" target="_blank" >Manage Country</a>)
                </td>
                <td><div id="dvcountry" style="display:none;"><asp:CheckBox ID="chkcompCountry" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtCountry" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumcountry" style="display:none;"><asp:CheckBox ID="chksumCountry" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrcountry" style="display:block;"><asp:TextBox ID="txtqrCountry" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" TargetControlID="txtqrCountry" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowRCountry" runat="server" Text=" RCountry" onclick="showField('rcountry');" /></td>
                <td><div id="dvrcountry" style="display:none;"><asp:CheckBox ID="chkcompRCountry" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtRCountry" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumrcountry" style="display:none;"><asp:CheckBox ID="chksumRCountry" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrrcountry" style="display:block;"><asp:TextBox ID="txtqrRCountry" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" TargetControlID="txtqrRCountry" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowTelCC" runat="server" Text=" TelCC" onclick="showField('telcc');" /></td>
                <td><div id="dvtelcc" style="display:none;"><asp:CheckBox ID="chkcompTelCC" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtTelCC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumtelcc" style="display:none;"><asp:CheckBox ID="chksumTelCC" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrtelcc" style="display:block;"><asp:TextBox ID="txtqrTelCC" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" TargetControlID="txtqrTelCC" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowTelAC" runat="server" Text=" TelAC" onclick="showField('telac');" /></td>
                <td><div id="dvtelac" style="display:none;"><asp:CheckBox ID="chkcompTelAC" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtTelAC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumtelac" style="display:none;"><asp:CheckBox ID="chksumTelAC" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrtelac" style="display:block;"><asp:TextBox ID="txtqrTelAC" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" TargetControlID="txtqrTelAC" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowTel" runat="server" Text=" Tel" onclick="showField('tel');" /></td>
                <td><div id="dvtel" style="display:none;"><asp:CheckBox ID="chkcompTel" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtTel" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumtel" style="display:none;"><asp:CheckBox ID="chksumTel" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrtel" style="display:block;"><asp:TextBox ID="txtqrTel" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" TargetControlID="txtqrTel" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowMobileCC" runat="server" Text=" MobileCC" onclick="showField('mobilecc');" /></td>
                <td><div id="dvmobilecc" style="display:none;"><asp:CheckBox ID="chkcompMobileCC" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtMobileCC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsummobilecc" style="display:none;"><asp:CheckBox ID="chksumMobileCC" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrmobilecc" style="display:block;"><asp:TextBox ID="txtqrMobileCC" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" TargetControlID="txtqrMobileCC" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowMobileAC" runat="server" Text=" MobileAC" onclick="showField('mobileac');" /></td>
                <td><div id="dvmobileac" style="display:none;"><asp:CheckBox ID="chkcompMobileAC" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtMobileAC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsummobileac" style="display:none;"><asp:CheckBox ID="chksumMobileAC" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrmobileac" style="display:block;"><asp:TextBox ID="txtqrMobileAC" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" TargetControlID="txtqrMobileAC" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowMobile" runat="server" Text=" Mobile" onclick="showField('mobile');" /></td>
                <td><div id="dvmobile" style="display:none;"><asp:CheckBox ID="chkcompMobile" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtMobile" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsummobile" style="display:none;"><asp:CheckBox ID="chksumMobile" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrmobile" style="display:block;"><asp:TextBox ID="txtqrMobile" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" TargetControlID="txtqrMobile" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowFaxCC" runat="server" Text=" FaxCC" onclick="showField('faxcc');" /></td>
                <td><div id="dvfaxcc" style="display:none;"><asp:CheckBox ID="chkcompFaxCC" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtFaxCC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumfaxcc" style="display:none;"><asp:CheckBox ID="chksumFaxCC" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrfaxcc" style="display:block;"><asp:TextBox ID="txtqrFaxCC" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" TargetControlID="txtqrFaxCC" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowFaxAC" runat="server" Text=" FaxAC" onclick="showField('faxac');" /></td>
                <td><div id="dvfaxac" style="display:none;"><asp:CheckBox ID="chkcompFaxAC" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtFaxAC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumfaxac" style="display:none;"><asp:CheckBox ID="chksumFaxAC" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrfaxac" style="display:block;"><asp:TextBox ID="txtqrFaxAC" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" TargetControlID="txtqrFaxAC" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowFax" runat="server" Text=" Fax" onclick="showField('fax');" /></td>
                <td><div id="dvfax" style="display:none;"><asp:CheckBox ID="chkcompFax" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtFax" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumfax" style="display:none;"><asp:CheckBox ID="chksumFax" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrfax" style="display:block;"><asp:TextBox ID="txtqrFax" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" TargetControlID="txtqrFax" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowEmail" runat="server" Text=" Email" onclick="showField('email');" /></td>
                <td><div id="dvemail" style="display:none;"><asp:CheckBox ID="chkcompEmail" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtEmail" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumemail" style="display:none;"><asp:CheckBox ID="chksumEmail" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqremail" style="display:block;"><asp:TextBox ID="txtqrEmail" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" TargetControlID="txtqrEmail" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowEmailConfirmation" runat="server" Text=" Email Confirmation" onclick="showField('emailconfirmation');" /></td>
                <td><div id="dvemailconfirmation" style="display:none;"><asp:CheckBox ID="chkcompEmailConfirmation" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtEmailConfirmation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumemailconfirmation" style="display:none;"><asp:CheckBox ID="chksumEmailConfirmation" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAffiliation" runat="server" Text=" Affiliation" onclick="showField('affiliation');" />&nbsp&nbsp&nbsp
              ( <a href="ManageAffiliation.aspx?SHW=<%=shwID %>" class="text-danger" target="_blank" >Manage Affiliation</a>)</td>
                <td><div id="dvaffiliation" style="display:none;"><asp:CheckBox ID="chkcompAffiliation" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAffiliation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaffiliation" style="display:none;"><asp:CheckBox ID="chksumAffiliation" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqraffiliation" style="display:block;"><asp:TextBox ID="txtqrAffiliation" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="txtqrAffiliation" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowDietary" runat="server" Text=" Dietary" onclick="showField('dietary');" />&nbsp&nbsp&nbsp
              ( <a href="ManageDietary.aspx?SHW=<%=shwID %>" class="text-danger" target="_blank" >Manage Dietary</a>)</td>
                <td><div id="dvdietary" style="display:none;"><asp:CheckBox ID="chkcompDietary" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtDietary" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumdietary" style="display:none;"><asp:CheckBox ID="chksumDietary" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrdietary" style="display:block;"><asp:TextBox ID="txtqrDietary" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="txtqrDietary" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowNationality" runat="server" Text=" Nationality" onclick="showField('nationality');" /></td>
                <td><div id="dvnationality" style="display:none;"><asp:CheckBox ID="chkcompNationality" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtNationality" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumnationality" style="display:none;"><asp:CheckBox ID="chksumNationality" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrnationality" style="display:block;"><asp:TextBox ID="txtqrNationality" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" TargetControlID="txtqrNationality" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowMembershipNo" runat="server" Text=" Membership No" onclick="showField('membershipno');" /></td>
                <td><div id="dvmembershipno" style="display:none;"><asp:CheckBox ID="chkcompMembershipNo" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtMembershipNo" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsummembershipno" style="display:none;"><asp:CheckBox ID="chksumMembershipNo" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrmembershipno" style="display:block;"><asp:TextBox ID="txtqrMembershipNo" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" TargetControlID="txtqrMembershipNo" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowVname" runat="server" Text=" Vname" onclick="showField('vname');" /></td>
                <td><div id="dvvname" style="display:none;"><asp:CheckBox ID="chkcompVname" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtVname" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumvname" style="display:none;"><asp:CheckBox ID="chksumVname" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrvname" style="display:block;"><asp:TextBox ID="txtqrVname" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" TargetControlID="txtqrVname" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowVDOB" runat="server" Text=" VDOB" onclick="showField('vdob');" /></td>
                <td><div id="dvvdob" style="display:none;"><asp:CheckBox ID="chkcompVDOB" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtVDOB" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumvdob" style="display:none;"><asp:CheckBox ID="chksumVDOB" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrvdob" style="display:block;"><asp:TextBox ID="txtqrVDOB" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" TargetControlID="txtqrVDOB" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowVPassNo" runat="server" Text=" VPassNo" onclick="showField('vpassno');" /></td>
                <td><div id="dvvpassno" style="display:none;"><asp:CheckBox ID="chkcompVPassNo" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtVPassNo" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumvpassno" style="display:none;"><asp:CheckBox ID="chksumVPassNo" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrvpassno" style="display:block;"><asp:TextBox ID="txtqrVPassNo" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" TargetControlID="txtqrVPassNo" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowVPassIssueDate" runat="server" Text=" VPassIssueDate" onclick="showField('vpassissuedate');" /></td>
                <td><div id="dvvpassissuedate" style="display:none;"><asp:CheckBox ID="chkcompVPassIssueDate" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtVPassIssueDate" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumvpassissuedate" style="display:none;"><asp:CheckBox ID="chksumVPassIssueDate" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrvpassissuedate" style="display:block;"><asp:TextBox ID="txtqrVPassIssueDate" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" TargetControlID="txtqrVPassIssueDate" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowVPassExpiry" runat="server" Text=" VPassExpiry" onclick="showField('vpassexpiry');" /></td>
                <td><div id="dvvpassexpiry" style="display:none;"><asp:CheckBox ID="chkcompVPassExpiry" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtVPassExpiry" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumvpassexpiry" style="display:none;"><asp:CheckBox ID="chksumVPassExpiry" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrvpassexpiry" style="display:block;"><asp:TextBox ID="txtqrVPassExpiry" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server" TargetControlID="txtqrVPassExpiry" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowVEmbarkation" runat="server" Text=" VEmbarkation" onclick="showField('vembarkation');" /></td>
                <td><div id="dvvembarkation" style="display:none;"><asp:CheckBox ID="chkcompVEmbarkation" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtVEmbarkation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumvembarkation" style="display:none;"><asp:CheckBox ID="chksumVEmbarkation" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrvembarkation" style="display:block;"><asp:TextBox ID="txtqrVEmbarkation" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender42" runat="server" TargetControlID="txtqrVEmbarkation" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowVArrivalDate" runat="server" Text=" VArrivalDate" onclick="showField('varrivaldate');" /></td>
                <td><div id="dvvarrivaldate" style="display:none;"><asp:CheckBox ID="chkcompVArrivalDate" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtVArrivalDate" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumvarrivaldate" style="display:none;"><asp:CheckBox ID="chksumVArrivalDate" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrvarrivaldate" style="display:block;"><asp:TextBox ID="txtqrVArrivalDate" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender43" runat="server" TargetControlID="txtqrVArrivalDate" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowVCountry" runat="server" Text=" VCountry" onclick="showField('vcountry');" /></td>
                <td><div id="dvvcountry" style="display:none;"><asp:CheckBox ID="chkcompVCountry" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtVCountry" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumvcountry" style="display:none;"><asp:CheckBox ID="chksumVCountry" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrvcountry" style="display:block;"><asp:TextBox ID="txtqrVCountry" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender44" runat="server" TargetControlID="txtqrVCountry" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowUDF_CName" runat="server" Text=" UDF_CName" onclick="showField('udf_cname');" /></td>
                <td><div id="dvudf_cname" style="display:none;"><asp:CheckBox ID="chkcompUDF_CName" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtUDF_CName" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumudf_cname" style="display:none;"><asp:CheckBox ID="chksumUDF_CName" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrudf_cname" style="display:block;"><asp:TextBox ID="txtqrUDF_CName" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender45" runat="server" TargetControlID="txtqrUDF_CName" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowUDF_DelegateType" runat="server" Text=" UDF_DelegateType" onclick="showField('udf_delegatetype');" /></td>
                <td><div id="dvudf_delegatetype" style="display:none;"><asp:CheckBox ID="chkcompUDF_DelegateType" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtUDF_DelegateType" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumudf_delegatetype" style="display:none;"><asp:CheckBox ID="chksumUDF_DelegateType" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrudf_delegatetype" style="display:block;"><asp:TextBox ID="txtqrUDF_DelegateType" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender46" runat="server" TargetControlID="txtqrUDF_DelegateType" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowUDF_ProfCategory" runat="server" Text=" UDF_ProfCategory" onclick="showField('udf_profcategory');" /></td>
                <td><div id="dvudf_profcategory" style="display:none;"><asp:CheckBox ID="chkcompUDF_ProfCategory" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtUDF_ProfCategory" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumudf_profcategory" style="display:none;"><asp:CheckBox ID="chksumUDF_ProfCategory" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrudf_profcategory" style="display:block;"><asp:TextBox ID="txtqrUDF_ProfCategory" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender47" runat="server" TargetControlID="txtqrUDF_ProfCategory" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowUDF_ProfCategoryOther" runat="server" Text=" UDF_ProfCategoryOther" onclick="showField('udf_profcategoryother');" /></td>
                <td><div id="dvudf_profcategoryother" style="display:none;"><asp:CheckBox ID="chkcompUDF_ProfCategoryOther" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtUDF_ProfCategoryOther" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumudf_profcategoryother" style="display:none;"><asp:CheckBox ID="chksumUDF_ProfCategoryOther" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrudf_profcategoryother" style="display:block;"><asp:TextBox ID="txtqrUDF_ProfCategoryOther" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender48" runat="server" TargetControlID="txtqrUDF_ProfCategoryOther" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowUDF_CPcode" runat="server" Text=" UDF_CPcode" onclick="showField('udf_cpcode');" /></td>
                <td><div id="dvudf_cpcode" style="display:none;"><asp:CheckBox ID="chkcompUDF_CPcode" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtUDF_CPcode" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumudf_cpcode" style="display:none;"><asp:CheckBox ID="chksumUDF_CPcode" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrudf_cpcode" style="display:block;"><asp:TextBox ID="txtqrUDF_CPcode" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender49" runat="server" TargetControlID="txtqrUDF_CPcode" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowUDF_CLDepartment" runat="server" Text=" UDF_CLDepartment" onclick="showField('udf_cldepartment');" /></td>
                <td><div id="dvudf_cldepartment" style="display:none;"><asp:CheckBox ID="chkcompUDF_CLDepartment" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtUDF_CLDepartment" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumudf_cldepartment" style="display:none;"><asp:CheckBox ID="chksumUDF_CLDepartment" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrudf_cldepartment" style="display:block;"><asp:TextBox ID="txtqrUDF_CLDepartment" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender50" runat="server" TargetControlID="txtqrUDF_CLDepartment" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowUDF_CAddress" runat="server" Text=" UDF_CAddress" onclick="showField('udf_caddress');" /></td>
                <td><div id="dvudf_caddress" style="display:none;"><asp:CheckBox ID="chkcompUDF_CAddress" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtUDF_CAddress" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumudf_caddress" style="display:none;"><asp:CheckBox ID="chksumUDF_CAddress" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrudf_caddress" style="display:block;"><asp:TextBox ID="txtqrUDF_CAddress" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender51" runat="server" TargetControlID="txtqrUDF_CAddress" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowUDF_CLCompany" runat="server" Text=" UDF_CLCompany" onclick="showField('udf_clcompany');" /></td>
                <td><div id="dvudf_clcompany" style="display:none;"><asp:CheckBox ID="chkcompUDF_CLCompany" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtUDF_CLCompany" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumudf_clcompany" style="display:none;"><asp:CheckBox ID="chksumUDF_CLCompany" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrudf_clcompany" style="display:block;"><asp:TextBox ID="txtqrUDF_CLCompany" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender52" runat="server" TargetControlID="txtqrUDF_CLCompany" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowUDF_CLCompanyOther" runat="server" Text=" UDF_CLCompanyOther" onclick="showField('udf_clcompanyother');" /></td>
                <td><div id="dvudf_clcompanyother" style="display:none;"><asp:CheckBox ID="chkcompUDF_CLCompanyOther" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtUDF_CLCompanyOther" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumudf_clcompanyother" style="display:none;"><asp:CheckBox ID="chksumUDF_CLCompanyOther" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrudf_clcompanyother" style="display:block;"><asp:TextBox ID="txtqrUDF_CLCompanyOther" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender53" runat="server" TargetControlID="txtqrUDF_CLCompanyOther" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowUDF_CCountry" runat="server" Text=" UDF_CCountry" onclick="showField('udf_ccountry');" /></td>
                <td><div id="dvudf_ccountry" style="display:none;"><asp:CheckBox ID="chkcompUDF_CCountry" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtUDF_CCountry" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumudf_ccountry" style="display:none;"><asp:CheckBox ID="chksumUDF_CCountry" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrudf_ccountry" style="display:block;"><asp:TextBox ID="txtqrUDF_CCountry" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender54" runat="server" TargetControlID="txtqrUDF_CCountry" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowSupervisorName" runat="server" Text=" Supervisor Name" onclick="showField('supervisorname');" /></td>
                <td><div id="dvsupervisorname" style="display:none;"><asp:CheckBox ID="chkcompSupervisorName" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtSupervisorName" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumsupervisorname" style="display:none;"><asp:CheckBox ID="chksumSupervisorName" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrsupervisorname" style="display:block;"><asp:TextBox ID="txtqrSupervisorName" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender55" runat="server" TargetControlID="txtqrSupervisorName" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowSupervisorDesignation" runat="server" Text=" Supervisor Designation" onclick="showField('supervisordesignation');" /></td>
                <td><div id="dvsupervisordesignation" style="display:none;"><asp:CheckBox ID="chkcompSupervisorDesignation" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtSupervisorDesignation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumsupervisordesignation" style="display:none;"><asp:CheckBox ID="chksumSupervisorDesignation" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrsupervisordesignation" style="display:block;"><asp:TextBox ID="txtqrSupervisorDesignation" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender56" runat="server" TargetControlID="txtqrSupervisorDesignation" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowSupervisorContact" runat="server" Text=" Supervisor Contact" onclick="showField('supervisorcontact');" /></td>
                <td><div id="dvsupervisorcontact" style="display:none;"><asp:CheckBox ID="chkcompSupervisorContact" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtSupervisorContact" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumsupervisorcontact" style="display:none;"><asp:CheckBox ID="chksumSupervisorContact" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrsupervisorcontact" style="display:block;"><asp:TextBox ID="txtqrSupervisorContact" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender57" runat="server" TargetControlID="txtqrSupervisorContact" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowSupervisorEmail" runat="server" Text=" Supervisor Email" onclick="showField('supervisoremail');" /></td>
                <td><div id="dvsupervisoremail" style="display:none;"><asp:CheckBox ID="chkcompSupervisorEmail" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtSupervisorEmail" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumsupervisoremail" style="display:none;"><asp:CheckBox ID="chksumSupervisorEmail" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrsupervisoremail" style="display:block;"><asp:TextBox ID="txtqrSupervisorEmail" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender58" runat="server" TargetControlID="txtqrSupervisorEmail" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowOtherSalutation" runat="server" Text=" Other Salutation" onclick="showField('othersalutation');" /></td>
                <td><div id="dvothersalutation" style="display:none;"><asp:CheckBox ID="chkcompOtherSalutation" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtOtherSalutation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumothersalutation" style="display:none;"><asp:CheckBox ID="chksumOtherSalutation" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrothersalutation" style="display:block;"><asp:TextBox ID="txtqrOtherSalutation" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender59" runat="server" TargetControlID="txtqrOtherSalutation" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowOtherProfession" runat="server" Text=" Other Profession" onclick="showField('otherprofession');" /></td>
                <td><div id="dvotherprofession" style="display:none;"><asp:CheckBox ID="chkcompOtherProfession" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtOtherProfession" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumotherprofession" style="display:none;"><asp:CheckBox ID="chksumOtherProfession" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrotherprofession" style="display:block;"><asp:TextBox ID="txtqrOtherProfession" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender60" runat="server" TargetControlID="txtqrOtherProfession" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowOtherDepartment" runat="server" Text=" Other Department" onclick="showField('otherdepartment');" /></td>
                <td><div id="dvotherdepartment" style="display:none;"><asp:CheckBox ID="chkcompOtherDepartment" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtOtherDepartment" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumotherdepartment" style="display:none;"><asp:CheckBox ID="chksumOtherDepartment" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrotherdepartment" style="display:block;"><asp:TextBox ID="txtqrOtherDepartment" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender61" runat="server" TargetControlID="txtqrOtherDepartment" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowOtherOrganization" runat="server" Text=" Other Organization" onclick="showField('otherorganization');" /></td>
                <td><div id="dvotherorganization" style="display:none;"><asp:CheckBox ID="chkcompOtherOrganization" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtOtherOrganization" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumotherorganization" style="display:none;"><asp:CheckBox ID="chksumOtherOrganization" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrotherorganization" style="display:block;"><asp:TextBox ID="txtqrOtherOrganization" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender62" runat="server" TargetControlID="txtqrOtherOrganization" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowOtherInstitution" runat="server" Text=" Other Institution" onclick="showField('otherinstitution');" /></td>
                <td><div id="dvotherinstitution" style="display:none;"><asp:CheckBox ID="chkcompOtherInstitution" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtOtherInstitution" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumotherinstitution" style="display:none;"><asp:CheckBox ID="chksumOtherInstitution" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrotherinstitution" style="display:block;"><asp:TextBox ID="txtqrOtherInstitution" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender63" runat="server" TargetControlID="txtqrOtherInstitution" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAEmail" runat="server" Text=" AEmail" onclick="showField('aemail');" /></td>
                <td><div id="dvaemail" style="display:none;"><asp:CheckBox ID="chkcompAEmail" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAEmail" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaemail" style="display:none;"><asp:CheckBox ID="chksumAEmail" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqraemail" style="display:block;"><asp:TextBox ID="txtqrAEmail" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender64" runat="server" TargetControlID="txtqrAEmail" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowIsSMS" runat="server" Text=" IsSMS" onclick="showField('issms');" /></td>
                <td><div id="dvissms" style="display:none;"><asp:CheckBox ID="chkcompIsSMS" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtIsSMS" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumissms" style="display:none;"><asp:CheckBox ID="chksumIsSMS" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrissms" style="display:block;"><asp:TextBox ID="txtqrIsSMS" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender65" runat="server" TargetControlID="txtqrIsSMS" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAge" runat="server" Text=" Age" onclick="showField('age');" /></td>
                <td><div id="dvage" style="display:none;"><asp:CheckBox ID="chkcompAge" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAge" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumage" style="display:none;"><asp:CheckBox ID="chksumAge" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrage" style="display:block;"><asp:TextBox ID="txtqrAge" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender66" runat="server" TargetControlID="txtqrAge" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowGender" runat="server" Text=" Gender" onclick="showField('gender');" /></td>
                <td><div id="dvgender" style="display:none;"><asp:CheckBox ID="chkcompGender" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtGender" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumgender" style="display:none;"><asp:CheckBox ID="chksumGender" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrgender" style="display:block;"><asp:TextBox ID="txtqrGender" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender67" runat="server" TargetControlID="txtqrGender" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowDOB" runat="server" Text=" Date of Birth" onclick="showField('dob');" /></td>
                <td><div id="dvdob" style="display:none;"><asp:CheckBox ID="chkcompDOB" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtDOB" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumdob" style="display:none;"><asp:CheckBox ID="chksumDOB" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqrdob" style="display:block;"><asp:TextBox ID="txtqrDOB" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender68" runat="server" TargetControlID="txtqrDOB" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAdditional4" runat="server" Text=" Additional4" onclick="showField('additional4');" /></td>
                <td><div id="dvadditional4" style="display:none;"><asp:CheckBox ID="chkcompAdditional4" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAdditional4" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumadditional4" style="display:none;"><asp:CheckBox ID="chksumAdditional4" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqradditional4" style="display:block;"><asp:TextBox ID="txtqrAdditional4" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender69" runat="server" TargetControlID="txtqrAdditional4" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAdditional5" runat="server" Text=" Additional5" onclick="showField('additional5');" /></td>
                <td><div id="dvadditional5" style="display:none;"><asp:CheckBox ID="chkcompAdditional5" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAdditional5" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumadditional5" style="display:none;"><asp:CheckBox ID="chksumAdditional5" runat="server" Text="  Display in Summary & Report" /></div></td>
                <td><div id="dvqradditional5" style="display:block;"><asp:TextBox ID="txtqrAdditional5" runat="server"></asp:TextBox></div>
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender70" runat="server" TargetControlID="txtqrAdditional5" FilterType="Numbers" ValidChars="0123456789" /></td>
            </tr>
        </tbody>
    </table>
    <asp:Button ID="btnSave" runat="server" Text="Save and Next" OnClick="SaveForm" CssClass="btn btn-primary"/>
    &nbsp;
    <asp:Button ID="btnSavePreview" runat="server" Text="Save And Preview" OnClick="btnSavePreview_Click" CssClass="btn btn-primary" /> 
    &nbsp;    <asp:Button ID="btnSetupEmail" runat="server" Text="Set up Email" OnClick="btnSetupEmail_Onclick" CssClass="btn btn-success"  />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>
</asp:Content>
