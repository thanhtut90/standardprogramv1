﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using Corpit.Site.Utilities;
using ClosedXML.Excel;
using System.IO;
using System.Collections;
using System.Reflection;

public partial class Admin_SummaryByPackage : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    string showID = string.Empty;
    static string _Name = "Name";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _City = "City";
    static string _State = "State";
    static string _ZipCode = "Zip Code";
    static string _Country = "Country";
    static string _TelCC = "Telcc";
    static string _TelAC = "Telac";
    static string _Tel = "Tel";
    static string _FaxCC = "Faxcc";
    static string _FaxAC = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Website = "Website";
    static string _Additional1 = "Additional1";
    static string _Additional2 = "Additional2";
    static string _Additional3 = "Additional3";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    private static string[] checkingWorkshopShowIDs = new string[] { "BFZ364", "WBW392" };
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string[] showList = new string[2];

            try
            {
                if (Session["userid"] != null)
                {
                    string userID = Session["userid"].ToString();

                    binddata();

                    if (Session["roleid"].ToString() == "1")
                    {
                        //showList = getAllShowList();
                        showID = ddl_showList.SelectedValue;
                    }
                    else
                    {
                        showlist.Visible = false;
                        //showList = getShowList();
                        getShowID(userID);
                    }

                    bindByItemsSelection(showID);
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region bind dropdown
    protected void binddata()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_showList.Items.Clear();
                ddl_showList.DataSource = cmd.ExecuteReader();
                ddl_showList.DataTextField = "SHW_Name";
                ddl_showList.DataValueField = "SHW_ID";
                ddl_showList.DataBind();
                con.Close();
            }
        }
    }
    #endregion
    #region Show
    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }
    protected string[] getAllShowList()
    {
        string query = "Select * from tb_Show Order By SHW_ID";
        string showList = string.Empty;
        string showName = string.Empty;

        DataTable dt = fn.GetDatasetByCommand(query, "dsShow").Tables[0];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (showList == string.Empty)
            {
                showList = dt.Rows[i]["SHW_ID"].ToString();
                showName = dt.Rows[i]["SHW_Name"].ToString();
            }
            else
            {
                showList += "," + dt.Rows[i]["SHW_ID"].ToString();
                showName += "," + dt.Rows[i]["SHW_Name"].ToString();
            }
        }
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        return list;
    }
    protected string[] getShowList()
    {
        string showList = string.Empty;
        string showName = string.Empty;
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = Session["userid"].ToString();
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                query = "Select * from tb_Show WHERE SHW_ID=@showID";
                spar = new SqlParameter("showID", SqlDbType.NVarChar);

                string[] showLists = dt.Rows[0]["us_showid"].ToString().Split(',');
                for (int i = 0; i < showLists.Length; i++)
                {
                    pList.Clear();
                    spar.Value = showLists[i];
                    pList.Add(spar);

                    DataTable dtShow = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];

                    if (showName == string.Empty)
                    {
                        showName = dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                    else
                    {
                        showName += "," + dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                }
                list[0] = dt.Rows[0]["us_showid"].ToString();
                list[1] = showName;
                return list;
            }
            return list;
        }
        catch (Exception ex)
        {
            return list;
        }
    }
    protected void ddl_showList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;
        bindByItemsSelection(showID);
    }
    #endregion
    #region CheckGroupReg
    private int CheckGroupReg(string userid, string showid)
    {
        int res = 0;
        try
        {
            if (string.IsNullOrEmpty(showid))
            {
                string sql = "select * from tb_site_flow_master where ShowID in (select us_showid from tb_Admin_Show where us_userid='" + userid + "') and Status='Active' and FLW_Type='G'";
                DataSet ds = new DataSet();
                ds = fn.GetDatasetByCommand(sql, "sqlCheckGroup");

                res = ds.Tables[0].Rows.Count;
            }
            else
            {
                string sql = "select * from tb_site_flow_master where ShowID='" + showid + "' and Status='Active' and FLW_Type='G'";
                DataSet ds = new DataSet();
                ds = fn.GetDatasetByCommand(sql, "sqlCheckGroup");

                res = ds.Tables[0].Rows.Count;
            }
        }
        catch (Exception ex) { }
        return res;

    }
    #endregion
    #region bindByItemsSelection
    protected void bindByItemsSelection(string showid)
    {
        if (checkingWorkshopShowIDs.Contains(showid))
        {
            Button2.Text = "Export to Excel Summary By Workshop";
        }
        bindReportData(showid);
    }
    private void bindReportData(string showid)
    {
        DataTable NewDt = new DataTable();
        Tuple<string, string> FlowTypeIDsGroup = getFlowTypeIDsGI(showid, true);/*mainConferenceFlowIDs,dependentConferenceFlowIDs*/
        Tuple<string, string> FlowTypeIDsIndividual = getFlowTypeIDsGI(showid, false);/*mainConferenceFlowIDs,dependentConferenceFlowIDs*/
        if (!string.IsNullOrEmpty(FlowTypeIDsGroup.Item1) || !string.IsNullOrEmpty(FlowTypeIDsIndividual.Item1))
        {
            DataTable dtMainConferenceSelection = ByPackageSelection(showid, FlowTypeIDsGroup.Item1, FlowTypeIDsIndividual.Item1, ConfDefaultValue.conf_MainItem);
            gvSummaryByPackageSelection.DataSource = dtMainConferenceSelection;
            gvSummaryByPackageSelection.DataBind();
        }
        else
        {
            Button1.Visible = false;
        }

        if (!string.IsNullOrEmpty(FlowTypeIDsGroup.Item2) || !string.IsNullOrEmpty(FlowTypeIDsIndividual.Item2))
        {
            DataTable dtDependentConferenceSelection = ByPackageSelection(showid, FlowTypeIDsGroup.Item1, FlowTypeIDsIndividual.Item1, ConfDefaultValue.conf_DependentItem);
            gvSummaryByIndependentItems.DataSource = dtDependentConferenceSelection;
            gvSummaryByIndependentItems.DataBind();
        }
        else
        {
            Button2.Visible = false;
        }
    }
    #region ByPackageSelection (get main conference items selection data from tb_RegDelegate, tb_RegGroup, tb_Invoice, tb_Order, tb_OrderDetails, tb_ConfItem tables, (if conferenceType is M (main conference items), then get from tb_ConfItem/if conferenceType is D (dependent conference items), then get from tb_ConfDependentItem))
    private DataTable ByPackageSelection(string showid, string flowidsGroup, string flowidsIndiv, string conferenceType)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Package");
        dt.Columns.Add("Count");
        dt.Columns.Add("Percentage");
        try
        {
            DataTable Cat = new DataTable();
            Cat.Columns.Add("Package");
            Cat.Columns.Add("SubCount");

            string query = "Select Package, Sum(SubCount) as SubCount From (";

            string dataQuery = "";
            if (!string.IsNullOrEmpty(flowidsIndiv))
            {
                dataQuery += " Select * From GetSummaryByConferenceSelection('" + showid + "','" + flowidsIndiv + "','" + SiteFlowType.FLOW_INDIVIDUAL + "','" + conferenceType + "')";
            }
            if (!string.IsNullOrEmpty(flowidsGroup))
            {
                dataQuery += (!string.IsNullOrEmpty(query) ? "Union " : " ") + " Select * From GetSummaryByConferenceSelection('" + showid + "','" + flowidsGroup + "','" + SiteFlowType.FLOW_GROUP + "','" + conferenceType + "')";
            }

            if (!string.IsNullOrEmpty(dataQuery))
            {
                DataTable dtResult = fn.GetDatasetByCommand(query + dataQuery + ") as aa Group By Package Order By Package", "ds").Tables[0];
                Cat = dtResult;
            }

            Double catCount;
            Double TotalCount = 0;
            Double totalRegCount = 0;
            Double totalRegPercentageCount = 0;

            if (Cat.Rows.Count > 0)
            {
                TotalCount = cFun.ParseInt(Cat.Compute("SUM(SubCount)", string.Empty).ToString());
            }
            for (int i = 0; i < Cat.Rows.Count; i++)
            {
                catCount = cFun.ParseInt(Cat.Rows[i]["SubCount"].ToString());
                Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
                DataRow drow = dt.NewRow();
                drow["Package"] = Cat.Rows[i]["Package"].ToString();
                drow["Count"] = catCount.ToString();
                drow["Percentage"] = caldbscan.ToString() + "%";
                dt.Rows.Add(drow);

                totalRegCount += catCount;
                totalRegPercentageCount += caldbscan;
            }
            if (dt.Rows.Count == 0)
            {
                DataRow drow = dt.NewRow();
                drow["Package"] = "No Record";
                drow["Count"] = "0";
                drow["Percentage"] = "0" + "%";
                dt.Rows.Add(drow);
            }
            else
            {
                DataRow drow = dt.NewRow();
                drow["Package"] = "Total";
                drow["Count"] = totalRegCount;
                drow["Percentage"] = Math.Round(totalRegPercentageCount, 0) + "%";
                dt.Rows.Add(drow);
            }
        }
        catch(Exception ex)
        { }

        return dt;
    }
    #endregion
    #endregion

    #region checkFlowTypeGI (get Group or Individual from tb_site_flow_master table)
    private Tuple<string, string> getFlowTypeIDsGI(string showid, bool isGroup)
    {
        Tuple<string, string> result = new Tuple<string, string>("", "");
        try
        {
            GeneralObj grnObj = new GeneralObj(fn);
            DataTable dtFlowMas = grnObj.getAllSiteFlowMaster();
            DataView dv = new DataView();
            dv = dtFlowMas.DefaultView;
            dv.RowFilter = "ShowID='" + showid + "' And FLW_Desc Not Like '%No Use%' And FLW_Desc Not Like '%Not Use%'";
            dtFlowMas = dv.ToTable();
            if (dtFlowMas.Rows.Count > 0)
            {
                string flowidIDsMainConference = string.Empty;
                string flowidIDsDependentConference = string.Empty;
                foreach (DataRow dr in dtFlowMas.Rows)
                {
                    if (dr["ShowID"].ToString() == showid)
                    {
                        string flowtype = dr["FLW_Type"].ToString();
                        string flowid = dr["FLW_ID"].ToString();
                        if (isGroup && flowtype == SiteFlowType.FLOW_GROUP)
                        {
                            bool confExist = checkConferencePageExist(showid, flowid, SiteDefaultValue.constantConfName);
                            if (confExist)
                            {
                                bool hasMainConference = checkConferenceMainOrDependent(flowid, showid, ConfDefaultValue.conf_MainItem);
                                if (hasMainConference)
                                {
                                    flowidIDsMainConference += "" + flowid + ",";
                                }
                                bool hasDependentConference = checkConferenceMainOrDependent(flowid, showid, ConfDefaultValue.conf_DependentItem);
                                if (hasDependentConference)
                                {
                                    flowidIDsDependentConference += "" + flowid + ",";
                                }
                            }
                        }
                        else if(!isGroup && flowtype == SiteFlowType.FLOW_INDIVIDUAL)
                        {
                            bool confExist = checkConferencePageExist(showid, flowid, SiteDefaultValue.constantConfName);
                            if (confExist)
                            {
                                bool hasMainConference = checkConferenceMainOrDependent(flowid, showid, ConfDefaultValue.conf_MainItem);
                                if (hasMainConference)
                                {
                                    flowidIDsMainConference += "" + flowid + ",";
                                }
                                bool hasDependentConference = checkConferenceMainOrDependent(flowid, showid, ConfDefaultValue.conf_DependentItem);
                                if (hasDependentConference)
                                {
                                    flowidIDsDependentConference += "" + flowid + ",";
                                }
                            }
                        }
                    }
                }
                result = new Tuple<string, string>(flowidIDsMainConference.TrimEnd(','), flowidIDsDependentConference.TrimEnd(','));
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    private bool checkConferenceMainOrDependent(string flowid, string showid, string type)
    {
        bool isMainOrDependent = false;
        try
        {
            if (type == ConfDefaultValue.conf_MainItem)
            {
                string sql = "Select * From tb_ConfItem Where ShowID='" + showid + "' And isDeleted=0 And tb_ConfItem.reg_categoryId In "
                                + " (Select ref_reg_Category.reg_CategoryId From ref_reg_Category Where ref_reg_Category.ShowID='" + showid + "' And ref_reg_Category.RefValue='" + flowid + "')";
                DataTable dtResult = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dtResult.Rows.Count > 0)
                {
                    isMainOrDependent = true;
                }
            }
            else if (type == ConfDefaultValue.conf_DependentItem)
            {
                string sql = "Select * From tb_ConfDependentItem Where ShowID='" + showid + "' And isDeleted=0 And tb_ConfDependentItem.con_itemId In "
                                + " (Select tb_ConfItem.con_itemId From tb_ConfItem Where tb_ConfItem.ShowID='" + showid + "' And tb_ConfItem.isDeleted=0 And tb_ConfItem.reg_categoryId In "
                                + " (Select ref_reg_Category.reg_CategoryId From ref_reg_Category Where ref_reg_Category.ShowID='" + showid + "' And ref_reg_Category.RefValue='" + flowid + "'))";
                DataTable dtResult = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dtResult.Rows.Count > 0)
                {
                    isMainOrDependent = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isMainOrDependent;
    }
    #endregion

    #region genReport
    protected void genReportPackageSelection(object sender, EventArgs e)
    {
        ExportToExcel(gvSummaryByPackageSelection, "ReportSummaryByPackageSelection");
    }
    protected void genReportIndependentItems(object sender, EventArgs e)
    {
        string fileName = "ReportSummaryByDependentItemsSelection";
        try
        {
            if (Session["userid"] != null)
            {
                string userID = Session["userid"].ToString();
                if (Session["roleid"].ToString() == "1")
                {
                    showID = ddl_showList.SelectedValue;
                }
                else
                {
                    getShowID(userID);
                }
            }
            if (checkingWorkshopShowIDs.Contains(showID))
            {
                fileName = "ReportSummaryByWorkshop";
            }

            ExportToExcel(gvSummaryByIndependentItems, fileName);
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ExportToExcel
    private void ExportToExcel(GridView gv, string filename)//, DataTable dt)
    {
        DataTable dt = new DataTable();

        //*convert Dynamic Gridview to DataTable
        if (gv.Rows.Count > 0)
        {
            // add the columns to the datatable
            if (gv.HeaderRow != null)
            {
                for (int i = 0; i < gv.HeaderRow.Cells.Count; i++)
                {
                    dt.Columns.Add(System.Net.WebUtility.HtmlDecode(gv.HeaderRow.Cells[i].Text.Replace("&nbsp;", "")));
                }
            }

            //  add each of the data rows to the table
            foreach (GridViewRow row in gv.Rows)
            {
                DataRow dr;
                dr = dt.NewRow();

                for (int i = 0; i < row.Cells.Count; i++)
                {
                    dr[i] = System.Net.WebUtility.HtmlDecode(row.Cells[i].Text.Replace("&nbsp;", ""));
                }
                dt.Rows.Add(dr);
            }
        }
        //*

        XLWorkbook wb = new XLWorkbook();
        var ws = wb.Worksheets.Add("List");
        ws.Cell(1, 1).InsertTable(dt);
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format(@"attachment;filename={0}.xlsx", filename));
        ws.FirstRow().AddConditionalFormat().WhenGreaterThan(20000).Fill.SetBackgroundColor(XLColor.OrangeRed);//add bg color at first row(header)
        ws.Tables.FirstOrDefault().ShowAutoFilter = false;//remove first blank column and filter

        using (MemoryStream memoryStream = new MemoryStream())
        {
            wb.SaveAs(memoryStream);
            memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
            memoryStream.Close();
        }

        HttpContext.Current.Response.End();
    }
    #endregion

    #region checkConferencePageExist
    public bool checkConferencePageExist(string showid, string flowid, string scriptid)
    {
        bool isExist = false;
        string sql = "select * from tb_site_flow ";
        string FlwFilter = "";
        Dictionary<string, string> rtnValue = new Dictionary<string, string>();
        try
        {
            if (!string.IsNullOrEmpty(showid))
            {
                List<SqlParameter> pList = new List<SqlParameter>();
                FlwFilter = " where flow_Id=@flow_Id And flow_Id In (Select FLW_ID From tb_site_flow_master Where ShowID=@ShowID) And script_id Like '%'+@ScriptID+'%'";
                FlwFilter += " and Status = 'Active'";

                sql += FlwFilter + " order by flow_ID,step_seq";

                SqlParameter pSHW = new SqlParameter("ShowID", SqlDbType.NVarChar);
                pSHW.Value = showid;
                pList.Add(pSHW);
                SqlParameter pScriptID = new SqlParameter("ScriptID", SqlDbType.NVarChar);
                pScriptID.Value = scriptid;
                pList.Add(pScriptID);
                SqlParameter pFlw = new SqlParameter("flow_Id", SqlDbType.NVarChar);
                pFlw.Value = flowid;
                pList.Add(pFlw);

                DataTable dt = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];
                if (dt.Rows.Count > 0)
                {
                    isExist = true;
                }
            }
        }
        catch (Exception Ex) { isExist = false; }

        return isExist;
    }
    #endregion
}