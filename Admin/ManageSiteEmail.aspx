﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageSiteEmail.aspx.cs" Inherits="Admin_ManageSiteEmail" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Dashboard | Site Email</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="centercontent">
         <div class="pageheader">
            <h1 class="pagetitle">Site Email</h1>
            <span class="pagedesc">Page For Email.</span>
            <ul class="hornav">
                <li class="current"><a href="#index">Site Email</a></li>
            </ul>
        </div>
        <div id="contentwrapper" class="contentwrapper">
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                            <telerik:AjaxUpdatedControl ControlID="GKeySegment" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                            <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial" />
                            <telerik:AjaxUpdatedControl ControlID="PanelSegmentDeail" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                    <telerik:AjaxSetting AjaxControlID="btnadd">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                            <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial"></telerik:AjaxUpdatedControl>
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                    <telerik:AjaxSetting AjaxControlID="btnupdate">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                            <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial"></telerik:AjaxUpdatedControl>
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
            
            <div style="padding-left:25px;">
                <asp:LinkButton ID="lnkBack" runat="server" Text="Back" PostBackUrl="ManageSiteFlowStep.aspx?stp=2"></asp:LinkButton>
            </div>
            <br />

            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
                <asp:Panel runat="server" ID="PanelKeyList">
                    <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                        EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                        onneeddatasource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="20" Skin="Bootstrap">
                        <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="EMAIL_TblKey">
                            <Columns>

                                <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="EMAIL_TblKey" FilterControlAltText="Filter EMAIL_TblKey"
                                    HeaderText="EMAIL_TblKey" SortExpression="EMAIL_TblKey" UniqueName="EMAIL_TblKey">
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="EMAIL_ID" FilterControlAltText="Filter EMAIL_ID"
                                    HeaderText="EMAIL ID" SortExpression="EMAIL_ID" UniqueName="EMAIL_ID">
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Description" FilterControlAltText="Filter Description"
                                    HeaderText="Description" SortExpression="Description" UniqueName="Description">
                                </telerik:GridBoundColumn>

                                <telerik:GridTemplateColumn HeaderText="Template ID" ItemStyle-Width="240px">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# gettempname(Eval("Template_ID").ToString()) %>'></asp:Label>
                                </ItemTemplate>
                                     </telerik:GridTemplateColumn>
                        
                                <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Email_Vaild_StartDate" FilterControlAltText="Filter Email_Vaild_StartDate"
                                    HeaderText="Email Vaild StartDate" SortExpression="Email_Vaild_StartDate" UniqueName="Email_Vaild_StartDate">
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Email_Vaild_EndDate" FilterControlAltText="Filter Email_Vaild_EndDate"
                                    HeaderText="Email Vaild EndDate" SortExpression="Email_Vaild_EndDate" UniqueName="Email_Vaild_EndDate">
                                </telerik:GridBoundColumn>

                               <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Status" FilterControlAltText="Filter Status"
                                    HeaderText="Status" SortExpression="Status" UniqueName="Status">
                                </telerik:GridBoundColumn>

                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </asp:Panel>

                <br />
                <br />
                <div class="mybuttoncss">
                    <asp:Button runat="server" Text ="Add New Email" ID="btnaddnew" OnClick="btnaddnew_Click" />
                </div>
                <br />
                <asp:Label runat="server" ID="lbls" ForeColor="Red"></asp:Label>
                <br />
                <br />
                <asp:Panel runat="server" ID="PanelKeyDetial">
                    <div class='page-header'>
                        <h2>Detail </h2>
                    </div>
                    <table id="tblShow" class="stdtable">
                        <tr class="EditFormHeader">
                            <td style="width: 170px;">Email ID</td>
                            <td>
                                <telerik:RadTextBox RenderMode="Lightweight" ID="txtemailid" runat="server"></telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="rfvemailid" runat="server" Display="Dynamic"
                                    ControlToValidate="txtemailid" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="EditFormHeader">
                            <td style="width: 170px;">Description</td>
                            <td>
                                <asp:TextBox runat="server" ID="txtdesc"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="EditFormHeader">
                            <td style="width: 170px;">Template Name</td>
                            <td>
                            <asp:DropDownList runat="server" ID="ddltempName"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="EditFormHeader">
                            <td style="width: 170px;">Email Valid Start Date</td>
                            <td>
                                <telerik:RadCalendar runat="server" ID="calevsd" EnableMultiSelect="false"></telerik:RadCalendar>
                            </td>
                        </tr>
                        <tr class="EditFormHeader">
                            <td style="width: 170px;">Email Valid End Date</td>
                            <td>
                                <telerik:RadCalendar runat="server" ID="caleved" EnableMultiSelect="false"></telerik:RadCalendar>
                            </td>
                        </tr>
                        <tr class="EditFormHeader">
                            <td style="width: 170px;"><b>Status</b></td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlstatus">
                                    <asp:ListItem Text ="Active" Value="Active"></asp:ListItem>
                                    <asp:ListItem Text ="Inactive" Value="Inactive"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <asp:Button runat="server" Text="Update" id="btnupdate" OnClick="btnupdate_Click" />
                    <asp:Button runat="server" Text="Add" ID="btnadd" OnClick="btnadd_Click" />
                    <asp:HiddenField ID="hfTblKey" runat="server" Value="" />
                </asp:Panel>
            </telerik:RadAjaxPanel>
        </div>
    </div>
</asp:Content>

