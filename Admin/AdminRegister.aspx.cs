﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using Corpit.Utilities;
using System.Globalization;

public partial class Admin_AdminRegister : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string constr = fn.ConnString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("Select * From tb_admin_role where roleid != 1"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    ddlrolename.DataSource = cmd.ExecuteReader();
                    ddlrolename.DataTextField = "role_name";
                    ddlrolename.DataValueField = "roleid";
                    ddlrolename.DataBind();
                    con.Close();
                }

                using (SqlCommand cmd = new SqlCommand("Select * From tb_show"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    ddlShowName.DataSource = cmd.ExecuteReader();
                    ddlShowName.DataTextField = "SHW_Name";
                    ddlShowName.DataValueField = "SHW_ID";
                    ddlShowName.DataBind();
                    con.Close();
                }
            }

            if (Request.Params["AdminID"] != null)
            {
                string adminID = cFun.DecryptValue(Request.QueryString["AdminID"].ToString());
                bindData(adminID);
            }
        }
    }

    #region binddata (bind data from tb_admin to related controls)
    protected void bindData(string adminID) {
        string query = string.Empty;

        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("adminID", SqlDbType.NVarChar);
        spar.Value = adminID;
        pList.Add(spar);
        query = "Select * From tb_admin as a INNER JOIN tb_Admin_Show as s ON a.admin_id = s.us_userid  Where admin_id=@adminID";

        DataTable dt = new DataTable();
        dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

        if(dt.Rows.Count > 0) { 
            txtusername.Text = dt.Rows[0]["admin_username"].ToString();
            txtusername.Enabled = false;
            txtpassword.Attributes.Add("value", dt.Rows[0]["admin_password"].ToString());
            ddlrolename.SelectedValue = dt.Rows[0]["admin_roleid"].ToString();
            ddlShowName.SelectedValue = dt.Rows[0]["us_showid"].ToString();
        }
    }
    #endregion

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        string username = txtusername.Text;
        string password = txtpassword.Text;
        string cfmpswd = txtconfirmpswd.Text;
        string roleid = ddlrolename.SelectedValue;
        string connStr = fn.ConnString;
        string Usernamecheck = fn.GetDataByCommand("Select admin_username From tb_admin Where admin_username ='" + username + "'", "admin_username");
        string error = string.Empty;
        if (username != "")
        {
            if (password != "")
            {
                if (password == cfmpswd)
                {
                    if (Request.Params["AdminID"] == null)
                    {
                        if (Usernamecheck == "0")
                        {
                            try
                            {
                                SqlConnection con = new SqlConnection(connStr);
                                con.Open();
                                SqlCommand cmd = new SqlCommand("Insert Into tb_admin(admin_username, admin_password, admin_roleid, admin_isdeleted)" +
                                                     " values (@adminusername, @adminpassword, @adminroleid, @adminisdeleted)");

                                cmd.Parameters.AddWithValue("@adminusername", username);
                                cmd.Parameters.AddWithValue("@adminpassword", password);
                                cmd.Parameters.AddWithValue("@adminroleid", roleid);
                                cmd.Parameters.AddWithValue("@adminisdeleted", 0);

                                cmd.Connection = con;
                                int rowUpdated = cmd.ExecuteNonQuery();

                                if (rowUpdated == 1)
                                {
                                    DataTable dt = fn.GetDatasetByCommand("Select admin_id From tb_admin Where admin_username ='" + username + "'", "admin_username").Tables[0];
                                    //string userID = fn.GetDataSetByCommand("Select admin_id From tb_admin Where admin_username ='" + username + "'", "admin_username");

                                    SqlCommand cmd2 = new SqlCommand("Insert Into tb_admin_Show(us_userid, us_showid, us_datecreated)" +
                                                         " values (@us_userid, @us_showid, @us_datecreated)");

                                    cmd2.Parameters.AddWithValue("@us_userid", dt.Rows[0]["admin_id"].ToString());
                                    cmd2.Parameters.AddWithValue("@us_showid", ddlShowName.SelectedValue);
                                    cmd2.Parameters.AddWithValue("@us_datecreated", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture));

                                    cmd2.Connection = con;
                                    int rowUpdated2 = cmd2.ExecuteNonQuery();

                                    if (rowUpdated2 == 1)
                                    {
                                        lbls.Text = "success";
                                        Response.Redirect("Admin_Config");
                                    }
                                }
                                else
                                {
                                    lbls.Text += "error";
                                }
                                con.Close();
                            }
                            catch (Exception eo)
                            {
                                lbls.Text += eo.ToString();
                            }
                        }

                        else
                        {
                            lbls.Text = "The username is already existed";
                        }
                    }
                    else
                    {
                        string sql = string.Empty;
                        sql = "Update tb_admin Set admin_username = '" + username + "',admin_password= '" + password + "', admin_roleid = '" + roleid;
                        sql += "' where admin_id='" + cFun.DecryptValue(Request.QueryString["AdminID"].ToString()) + "'";
                        fn.ExecuteSQL(sql);

                        sql = "Update tb_admin_show Set us_showid= @us_showid where us_userid=@us_userid";
                        using (SqlConnection con = new SqlConnection(fn.ConnString))
                        {
                            using (SqlCommand command = new SqlCommand(sql))
                            {
                                command.Connection = con;
                                command.Parameters.AddWithValue("@us_showid", ddlShowName.SelectedValue);
                                command.Parameters.AddWithValue("@us_userid", cFun.DecryptValue(Request.QueryString["AdminID"].ToString()));
                                con.Open();
                                command.ExecuteNonQuery();
                                con.Close();
                            }
                        }

                        Response.Redirect("Admin_Config");
                    }
                }
                else
                {
                    lbls.Text = "Passwords are not in match";
                }
            }
            else
            {
                lbls.Text = "Please enter your password";
            }
        }
        else
        {
            lbls.Text = "Please enter your username";
        }

    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        txtusername.Text = "";
        txtpassword.Text = "";
        txtconfirmpswd.Text = "";
    }
}