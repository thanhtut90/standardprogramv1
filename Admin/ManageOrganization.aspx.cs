﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;

public partial class Admin_ManageOrganization : System.Web.UI.Page
{
    /// <summary>
    /// If you update or delete already inserted records, need to check and update in the tb_RegDelegate table
    /// because the organization name may be used by those table.
    /// </summary>


    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                lblShowid.Text = Request.QueryString["SHW"].ToString();

                BindGrid(lblShowid.Text);
                //Master.setgenopen();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    #region BindGrid and use table ref_Organisation & bind data to GridView(gvList)
    protected void BindGrid(string showid)
    {
        DataSet dsOrganisation = new DataSet();
        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsOrganisation = cmdObj.getBackendOrganizationList(showid);
        if (dsOrganisation.Tables[0].Rows.Count > 0)
        {
            gvList.DataSource = dsOrganisation;
            gvList.DataBind();
            pnllist.Visible = true;
        }
        else
        {
            pnllist.Visible = false;
        }
    }
    #endregion

    #region GridView1_RowDeleting and OnRowDeleting event of gvList & use table ref_Organisation & delete from ref_Organisation according to ID
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;

        OrganisationObj objOrg = new OrganisationObj();
        objOrg.ID = ID;
        objOrg.ShowID = lblShowid.Text;

        SetUpController setupCtrlr = new SetUpController(fn);
        int isSuccess = setupCtrlr.deleteOrganisation(objOrg);

        BindGrid(lblShowid.Text);
    }
    #endregion

    #region GridView1_RowEditing and OnRowEditing event of gvList
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvList.EditIndex = e.NewEditIndex;
        BindGrid(lblShowid.Text);
    }
    #endregion

    #region GridView1_RowUpdating and OnRowUpdating event of gvList & use table ref_Organisation & update ref_Organisation according to ID
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;
        // Retrieve the row being edited.
        int index = gvList.EditIndex;
        GridViewRow row = gvList.Rows[index];
        TextBox t1 = row.FindControl("txtName") as TextBox;
        TextBox t2 = row.FindControl("txtOrder") as TextBox;
        Label lblno = row.FindControl("lblno") as Label;
        string t3 = gvList.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            lblno.Visible = false;

            OrganisationObj objOrg = new OrganisationObj();
            objOrg.ID = ID;
            objOrg.Organisation = cFun.solveSQL(t1.Text.ToString());
            objOrg.Sorting = Convert.ToInt32(t2.Text.ToString());
            objOrg.ShowID = lblShowid.Text;

            SetUpController setupCtrlr = new SetUpController(fn);
            int isSuccess = setupCtrlr.updateOrganisation(objOrg);

            gvList.EditIndex = -1;
            BindGrid(lblShowid.Text);
        }
        catch (Exception ex)
        {
            lblno.Visible = true;
            lblno.Text = ex.Message;
        }
    }
    #endregion

    #region GridView1_RowCancelingEdit and OnRowCancelingEdit event of gvList
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvList.EditIndex = -1;
        BindGrid(lblShowid.Text);
    }
    #endregion

    #region btnAdd_Click and use table ref_Organisation & insert into ref_Organisation table
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string orgName = cFun.solveSQL(txtOrgName.Text.ToString());
        string sortorder = txtSortorder.Text.ToString();
        string showid = lblShowid.Text;

        try
        {
            OrganisationObj objOrg = new OrganisationObj();
            objOrg.Organisation = orgName;
            objOrg.Sorting = Convert.ToInt32(sortorder);
            objOrg.isDeleted = 0;
            objOrg.ShowID = showid;

            SetUpController setupCtrlr = new SetUpController(fn);
            int isSuccess = setupCtrlr.insertOrganisation(objOrg);

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }

        BindGrid(lblShowid.Text);
        txtOrgName.Text = "";
        txtSortorder.Text = "";
    }
    #endregion

    #region lnkUnDelete_Click (load when Un-Delete button click and update isDeleted=0 to tb_Terms table)
    protected void lnkUnDelete_Click(object sender, EventArgs e)
    {
        GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
        int id = Convert.ToInt32(grdrow.Cells[0].Text);

        OrganisationObj objOrg = new OrganisationObj();
        objOrg.ID = id;
        objOrg.ShowID = lblShowid.Text;

        SetUpController setupCtrlr = new SetUpController(fn);
        int isSuccess = setupCtrlr.undeleteOrganisation(objOrg);

        BindGrid(lblShowid.Text);

        txtOrgName.Text = "";
        txtSortorder.Text = "";
    }
    #endregion
}