﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Admin_ManageTerms : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
            Master.setgenopen();
        }
    }

    #region BindGrid and use table tb_Terms & bind data to GridView(gvList)
    protected void BindGrid()
    {
        DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand("Select * From tb_Terms Order By terms_Order Asc", "ds");
        if (ds.Tables[0].Rows.Count != 0)
        {
            gvList.DataSource = ds;
            gvList.DataBind();
            pnllist.Visible = true;
        }
        else
        {
            pnllist.Visible = false;
        }
    }
    #endregion

    #region gvList_RowDeleting (load when Delete button click and update terms_isDeleted=1 to tb_Terms table)
    protected void gvList_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int termid = (int)gvList.DataKeys[e.RowIndex].Value;
        string query = string.Format("Update tb_Terms Set terms_isDeleted=1 Where terms_ID={0}", termid);
        fn.ExecuteSQL(query);
        DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand("Select * From tb_Terms Order By terms_Order Asc", "ds");
        if (ds.Tables[0].Rows.Count != 0)
        {
            gvList.DataSource = null;
            gvList.DataSource = ds;
            gvList.DataBind();
        }

        txtName.Text = "";
        txtDesc.Text = "";
    }
    #endregion

    #region GridView1_RowEditing and OnRowEditing event of gvList
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvList.EditIndex = e.NewEditIndex;
        BindGrid();
        txtName.Text = "";
        txtDesc.Text = "";
    }
    #endregion

    #region GridView1_RowUpdating and OnRowUpdating event of gvList & use table tb_Terms & update tb_Terms according to terms_ID
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int termid = (int)gvList.DataKeys[e.RowIndex].Value;
        int index = gvList.EditIndex;
        GridViewRow row = gvList.Rows[index];
        TextBox t1 = row.FindControl("txtName") as TextBox;
        TextBox tdesc = row.FindControl("txtDescription") as TextBox;
        TextBox t2 = row.FindControl("txtOrder") as TextBox;
        Label lblno = row.FindControl("lblno") as Label;
        string t3 = gvList.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            int i = Convert.ToInt16(t2.Text.ToString());
            lblno.Visible = false;
            string query = string.Format("Update tb_Terms Set terms_Name='{0}',terms_Desc='{1}',terms_Order={2} Where terms_ID={3}", cFun.solveSQL(t1.Text.ToString()), cFun.solveSQL(tdesc.Text.ToString()), t2.Text.ToString(), termid);
            
            fn.ExecuteSQL(query);
            gvList.EditIndex = -1;
            BindGrid();
        }
        catch (Exception ex)
        {
            lblno.Visible = true;
            lblno.Text = "Please insert number only.";
        }
    }
    #endregion

    #region GridView1_RowCancelingEdit and OnRowCancelingEdit event of gvList
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvList.EditIndex = -1;
        BindGrid();

        txtName.Text = "";
        txtDesc.Text = "";
    }
    #endregion

    #region addTerms and use table tb_Terms & insert into tb_Terms table
    protected void addTerms(object sender, EventArgs e)
    {
        string Name = cFun.solveSQL(txtName.Text.ToString());
        string Desc = cFun.solveSQL(txtDesc.Text.ToString());
        string query = string.Format("Insert Into tb_Terms(terms_Name,terms_Desc,terms_Order) Values('{0}','{1}',0)", Name, Desc);
        fn.ExecuteSQL(query);
        BindGrid();

        txtName.Text = "";
        txtDesc.Text = "";
    }
    #endregion

    #region lnkUnDelete_Click (load when Un-Delete button click and update terms_isDeleted=0 to tb_Terms table)
    protected void lnkUnDelete_Click(object sender, EventArgs e)
    {
        GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
        string termid = grdrow.Cells[0].Text;

        string query = string.Format("Update tb_Terms Set terms_isDeleted=0 Where terms_ID={0}", termid);
        fn.ExecuteSQL(query);
        BindGrid();

        txtName.Text = "";
        txtDesc.Text = "";
    }
    #endregion
}