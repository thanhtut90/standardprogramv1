﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="MasterRegistrationList_IndivFJ.aspx.cs" Inherits="Admin_MasterRegistrationList_IndivFJ" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../Content/dist/css/skins/_all-skins.min.css">
    <%--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script> <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>--%>
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }

        $(function () {
            $.fn.datepicker.defaults.format = "dd/mm/yyyy";
            //     $("#txtFromDate").datepicker({}).val()
            //    $("#txtToDate").datepicker({}).val()
        });
    </script>
    <style type="text/css">
        .tdstyle1 {
            width: 170px;
        }

        .tdstyle2 {
            width: 300px;
        }

        form input[type="text"] {
            width: 39% !important;
        }

        .ajax__calendar_container {
            width: 320px !important;
            height: 280px !important;
        }

        .ajax__calendar_body {
            width: 100% !important;
            height: 100% !important;
        }

        td {
            vertical-align: middle;
        }
    </style>
 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    &nbsp;&nbsp;&nbsp;
    <asp:LinkButton ID="btnAddNew" runat="server" CssClass="btn btn-success" OnClick="lnkExcel_Clicked">
            <span aria-hidden="true" class="glyphicon glyphicon-export"></span> EXPORT EXCEL
    </asp:LinkButton>
    <asp:LinkButton ID="btnExportAllTrade" runat="server" CssClass="btn btn-success" OnClick="btnExportAllTrade_Click">
            <span aria-hidden="true" class="glyphicon glyphicon-export"></span> EXPORT ALL TRADE DATA TO EXCEL
    </asp:LinkButton>
    <asp:LinkButton ID="btnExportTrade" runat="server" CssClass="btn btn-success" OnClick="btnExportTrade_Click">
            <span aria-hidden="true" class="glyphicon glyphicon-export"></span> EXPORT ALL TRADE DATA WITHOUT QUESTIONNAIRE TO EXCEL 
    </asp:LinkButton>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="" Transparency="30"><%-- BackgroundTransparency="50"--%>
                <asp:Label ID="Label2" runat="server" Text="Loading..">
                </asp:Label>
                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/loading-icon.gif" />
            </telerik:RadAjaxLoadingPanel>
            <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" EnableAJAX="true" LoadingPanelID="RadAjaxLoadingPanel1">

            <asp:Label ID="lblUser" runat="server" Visible="false"></asp:Label>
            <h3 class="box-title">Master Registration List</h3>
            <div id="divsearch" runat="server" class="row" style="margin-bottom: 10PX;" visible="true">
                <div class="form-group col-sm-12">
                    <label for="inputEmail3" class="col-sm-1 control-label">Show List: &nbsp;</label>
                    <div class="col-sm-2">
                        <asp:Panel ID="showlist" runat="server">
                            <asp:DropDownList ID="ddl_showList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_showList_SelectedIndexChanged" CssClass="form-control">
                            </asp:DropDownList>
                        </asp:Panel>
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label for="inputEmail3" class="col-sm-1 control-label"> Flow List: &nbsp;</label>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="ddl_flowList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_flowList_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <label for="inputEmail3" class="col-sm-1 control-label">   Payment Status: &nbsp;</label>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="ddl_paymentStatus" runat="server" OnSelectedIndexChanged="ddl_paymentStatus_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-sm-10">
                    <asp:TextBox ID="txtFromDate" placeholder="From" runat="server" ClientIDMode="Static" onclick="$(this).datepicker().datepicker('show')" CssClass="form-control col-sm-1"></asp:TextBox>
                    <%-- <label for="inputEmail3" class="col-sm-1 control-label">  To </label>--%>
                    <div class="col-sm-1">
                    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" Enabled="false">To</asp:LinkButton>
                    </div>
                    <asp:TextBox ID="txtToDate" placeholder="To " runat="server" onclick="$(this).datepicker().datepicker('show')" CssClass="form-control col-sm-1" ></asp:TextBox>
                    <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-success" runat="server" ><i class="fa fa-search"></i> </asp:LinkButton>
                </div>
                <div class="form-group col-sm-10">
                    <asp:TextBox ID="txtKey" placeholder="Enter Key Word (Registration Number, First Name, Surname, Email, Country)" runat="server" ClientIDMode="Static" CssClass="form-control  col-sm-1"></asp:TextBox>
                    <div class="col-sm-1">
                        <asp:LinkButton ID="btnKeysearch" OnClick="btnKeysearch_Click" class="btn btn-success" runat="server" ><i class="fa fa-search"></i> &nbsp;Search </asp:LinkButton>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div style="overflow-x: scroll;">
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientEvents-OnRequestStart="onRequestStart">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <%-- <telerik:AjaxSetting AjaxControlID="btnupdate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
                    </AjaxSettings>
                </telerik:RadAjaxManager>

                <%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">--%>
                    <asp:Panel runat="server" ID="PanelKeyList">
                        <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true" 
                            EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                            OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="10" OnPageIndexChanged="GKeyMaster_PageIndexChanged">
                            <%--OnItemDataBound="grid_ItemDataBound" OnItemCreated="RadGd1_ItemCreated"  --%>
                            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="false">
                                <Selecting AllowRowSelect="false"></Selecting>
                            </ClientSettings>
                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="Regno,RegGroupID" AllowFilteringByColumn="True" ShowFooter="false">
                                <CommandItemSettings ShowExportToExcelButton="False" ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                                <Columns>
                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RegGroupID" FilterControlAltText="Filter RegGroupID"
                                        HeaderText="RegGroupID" SortExpression="RegGroupID" UniqueName="RegGroupID" Visible="false">
                                    </telerik:GridBoundColumn>

                                    <%--<telerik:GridButtonColumn CommandName="Download" ButtonType="ImageButton" ImageUrl="images/download.png" UniqueName="Download"
                                        HeaderText="Acknowledge Letter" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>--%>

                                    <telerik:GridButtonColumn ConfirmTextFormatString="Do you want to Delete {0}?" ConfirmTextFields="Regno"
                                        ConfirmDialogType="RadWindow" CommandName="Delete" ButtonType="ImageButton" ImageUrl="images/delete.jpg" UniqueName="Delete"
                                        HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>

                                    <telerik:GridButtonColumn CommandName="Edit" ButtonType="ImageButton" ImageUrl="images/edit.jpg" UniqueName="Edit"
                                        HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Regno" FilterControlAltText="Filter Regno"
                                        HeaderText="Regno" SortExpression="Regno" UniqueName="Regno">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Salutation" FilterControlAltText="Filter Salutation"
                                        HeaderText="" SortExpression="Salutation" UniqueName="Salutation">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridTemplateColumn DataField="reg_Salutation" HeaderText="" UniqueName="reg_Salutation"
                                        FilterControlAltText="Filter reg_Salutation" SortExpression="reg_Salutation" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindSalutation(Eval("reg_Salutation").ToString(), Eval("reg_SalutationOthers").ToString()) %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_FName" FilterControlAltText="Filter reg_FName"
                                        HeaderText="" SortExpression="reg_FName" UniqueName="FName" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_OName" FilterControlAltText="Filter reg_OName"
                                        HeaderText="" SortExpression="reg_OName" UniqueName="Oname" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="ProfessionName" FilterControlAltText="Filter ProfessionName"
                                        HeaderText="" SortExpression="ProfessionName" UniqueName="Profession" Display="false">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridTemplateColumn DataField="reg_Profession" HeaderText="" UniqueName="reg_Profession"
                                        FilterControlAltText="Filter reg_Profession" SortExpression="reg_Profession" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindProfession(Eval("reg_Profession").ToString(), Eval("reg_otherProfession").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="OrganizationName" FilterControlAltText="Filter OrganizationName"
                                        HeaderText="" SortExpression="OrganizationName" UniqueName="Organization" Display="false">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridTemplateColumn DataField="reg_Organization" HeaderText="" UniqueName="reg_Organization"
                                        FilterControlAltText="Filter reg_Organization" SortExpression="reg_Organization" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindOrganisation(Eval("reg_Organization").ToString(), Eval("reg_otherOrganization").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address1" FilterControlAltText="Filter reg_Address1"
                                        HeaderText="" SortExpression="reg_Address1" UniqueName="Address1" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address2" FilterControlAltText="Filter reg_Address2"
                                        HeaderText="" SortExpression="reg_Address2" UniqueName="Address2" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address3" FilterControlAltText="Filter reg_Address3"
                                        HeaderText="" SortExpression="reg_Address3" UniqueName="Address3" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RegCountry" FilterControlAltText="Filter RegCountry"
                                        HeaderText="" SortExpression="RegCountry" UniqueName="Country" Display="false">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridTemplateColumn DataField="reg_Country" HeaderText="" UniqueName="reg_Country"
                                        FilterControlAltText="Filter reg_Country" SortExpression="reg_Country" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindCountry(Eval("reg_Country").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Telcc" FilterControlAltText="Filter reg_Telcc"
                                        HeaderText="Tel(Country Code)" SortExpression="reg_Telcc" UniqueName="Telcc" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Telac" FilterControlAltText="Filter reg_Telac"
                                        HeaderText="Tel(Area Code)" SortExpression="reg_Telac" UniqueName="Telac" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Tel" FilterControlAltText="Filter reg_Tel"
                                        HeaderText="Telepone" SortExpression="reg_Tel" UniqueName="Tel" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Mobcc" FilterControlAltText="Filter reg_Mobcc"
                                        HeaderText="Mobile(Country Code)" SortExpression="reg_Mobcc" UniqueName="Mobilecc" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Mobac" FilterControlAltText="Filter reg_Mobac"
                                        HeaderText="Mobile(Area Code)" SortExpression="reg_Mobac" UniqueName="Mobileac" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Mobile" FilterControlAltText="Filter reg_Mobile"
                                        HeaderText="Mobile" SortExpression="reg_Mobile" UniqueName="Mobile" Display="false">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Tel" UniqueName="reg_Tel"
                                        FilterControlAltText="Filter reg_Tel" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("reg_Telcc")%><%#Eval("reg_Telac")%><%#Eval("reg_Tel")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Mobile" UniqueName="reg_Mobile"
                                        FilterControlAltText="Filter reg_Mobile" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("reg_Mobcc")%><%#Eval("reg_Mobac")%><%#Eval("reg_Mobile")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Fax" UniqueName="reg_Fax"
                                        FilterControlAltText="Filter reg_Fax" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("reg_Faxcc")%><%# Eval("reg_Faxac")%><%#Eval("reg_Fax")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Email" FilterControlAltText="Filter reg_Email"
                                        HeaderText="" SortExpression="reg_Email" UniqueName="Email" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="DietaryName" FilterControlAltText="Filter DietaryName"
                                        HeaderText="" SortExpression="DietaryName" UniqueName="Dietary" Display="false">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridTemplateColumn DataField="reg_Dietary" HeaderText="" UniqueName="reg_Dietary"
                                        FilterControlAltText="Filter reg_Dietary" SortExpression="reg_Dietary" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindDietary(Eval("reg_Dietary").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Gender" FilterControlAltText="Filter reg_Gender"
                                        HeaderText="" SortExpression="reg_Gender" UniqueName="Gender" Display="false">
                                    </telerik:GridBoundColumn>

                                    <%--<telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Additional4" FilterControlAltText="Filter reg_Additional4"
                                        HeaderText="" SortExpression="reg_Additional4" UniqueName="reg_Additional4">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Additional5" FilterControlAltText="Filter reg_Additional5"
                                        HeaderText="" SortExpression="reg_Additional5" UniqueName="reg_Additional5">
                                    </telerik:GridBoundColumn>--%>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Promo_Code" FilterControlAltText="Filter Promo_Code"
                                        HeaderText="Promo Code" SortExpression="Promo_Code" UniqueName="Promo_Code" Display="true">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="Status" SortExpression="status_name" UniqueName="status_name"
                                        FilterControlAltText="Filter status_name" Exportable="true" Display="true">
                                        <ItemTemplate>
                                            <%#!string.IsNullOrEmpty(Eval("status_name").ToString()) ? Eval("status_name").ToString() : "Pending"%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <%--<telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_datecreated" FilterControlAltText="Filter reg_datecreated"
                                        HeaderText="Created Date" SortExpression="reg_datecreated" UniqueName="reg_datecreated">
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Created Date" UniqueName="reg_datecreated" Display="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCreatedDate" runat="server" Text='<%# Bind("reg_datecreated", "{0:G}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="BackendUpload" FilterControlAltText="Filter BackendUpload"
                                        HeaderText="Source" SortExpression="BackendUpload" UniqueName="BackendUpload" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="Re-send Confirmation Email" UniqueName="SendConfirmationEmailNormal">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnSendConfirmationEmailNormal" runat="server" OnCommand="btnSendConfirmationEmailNormal_Command" 
                                                Text="Send Confirmation Email" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID") + ";" + Eval("InvoiceID") + ";" + Eval("Invoice_status") + ";" + Eval("reg_Status")%>'
                                                Visible='<%#isResendConfirmationEmailVisible(Eval("reg_Status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>


                                    <telerik:GridTemplateColumn HeaderText="Congress Selection" UniqueName="CongressSelection" Exportable="true" Display="false">
                                        <ItemTemplate>
                                            <%#getCongressSelection(Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("Invoice_status").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Payment Method" UniqueName="PaymentMethod" Exportable="true" Display="false">
                                        <ItemTemplate>
                                            <%#getPaymentMethod(Eval("PaymentMethod").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Invoice_grandtotal" FilterControlAltText="Filter Invoice_grandtotal"
                                        HeaderText="Total Price" SortExpression="Invoice_grandtotal" UniqueName="Invoice_grandtotal" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="Paid Price" UniqueName="PaidPrice" Exportable="true" Display="false">
                                        <ItemTemplate>
                                            <%#getPaidPrice(Eval("InvoiceID").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Outstanding" UniqueName="Outstanding" Exportable="true" Display="false">
                                        <ItemTemplate>
                                            <%#calculateOutstanding(Eval("Invoice_grandtotal").ToString(), Eval("InvoiceID").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Invoice_discount" FilterControlAltText="Filter Invoice_discount"
                                        HeaderText="Discount Price" SortExpression="Invoice_discount" UniqueName="Invoice_discount" Display="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="Invoice Status" UniqueName="InvoiceStatus" Exportable="true" Display="false">
                                        <ItemTemplate>
                                            <%#getInvoiceStatus(Eval("Invoice_status").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="Regno" HeaderText="Print Date" UniqueName="PrintDateOnly" Exportable="true">
                                        <ItemTemplate>
                                            <%# getPrintedDate(Eval("Regno").ToString(),Eval("showid").ToString(), true)%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn DataField="Regno" HeaderText="Print Time" UniqueName="PrintTimeOnly" Exportable="true">
                                        <ItemTemplate>
                                            <%# getPrintedDate(Eval("Regno").ToString(),Eval("showid").ToString(), false, true)%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Update Payment" UniqueName="UpdatePayment" Display="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnUpdatePayment" runat="server" OnCommand="btnUpdatePayment_Command" Text="Update Payment" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID") + ";" +Eval("InvoiceID")%>'
                                                Visible='<%#isPaymentVisible(Eval("Invoice_status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Download Invoice" UniqueName="DownloadInvoice" Display="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDownloadInvoice" runat="server" OnCommand="btnDownloadInvoice_Command" Text="Download Invoice" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID") + ";" +Eval("InvoiceID")%>'
                                                Visible='<%#isPaymentVisible(Eval("Invoice_status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Download Receipt" UniqueName="DownloadReceipt" Display="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDownloadReceipt" runat="server" OnCommand="btnDownloadReceipt_Command" Text="Download Receipt" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID") + ";" +Eval("InvoiceID")%>'
                                                Visible='<%#isDownloadReceiptVisible(Eval("Invoice_status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Download Badge" UniqueName="DownloadBadge" Display="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDownloadBadge" runat="server" OnCommand="btnDownloadBadge_Command" Text="Download Badge" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID") + ";" +Eval("InvoiceID")%>'
                                                Visible='<%#isDownloadReceiptVisible(Eval("Invoice_status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Send Confirmation Email" UniqueName="SendConfirmationEmail" Display="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnSendConfirmationEmail" runat="server" OnCommand="btnSendConfirmationEmail_Command" Text="Send Confirmation Email" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID") + ";" +Eval("InvoiceID")%>'
                                                Visible='<%#isDownloadReceiptVisible(Eval("Invoice_status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </asp:Panel>
                </telerik:RadAjaxPanel>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


