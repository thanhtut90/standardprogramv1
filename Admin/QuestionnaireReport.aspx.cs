﻿using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_QuestionaireReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CommonFuns cFun = new CommonFuns();
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                this.questionnaireFrame.Attributes.Add("src", ConfigurationManager.AppSettings["QuestionnaireReportURL"] + "?ShowID=" + showid);

            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }
}