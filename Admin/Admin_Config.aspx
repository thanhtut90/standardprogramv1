﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Admin_Config.aspx.cs" Inherits="Admin_Admin_Config" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">1. Manage Admin</h1>

        </div>
        <div id="contentwrapper" class="contentwrapper">
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <AjaxSettings>

                </AjaxSettings>
            </telerik:RadAjaxManager>
            <asp:Panel runat="server" ID="Panel1">
                <div class="mybuttoncss" style="padding-bottom:10px;">
                    <asp:LinkButton ID="btnAddNew" runat="server" CssClass="btn btn-success" OnClick="CreateAdmin">
                        <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Create Admin
                    </asp:LinkButton>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="Panel2">
                        <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster1" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                            EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                            OnNeedDataSource="GKeyMaster1_NeedDataSource" OnItemCommand="GKeyMaster1_ItemCommand" PageSize="10" Skin="Bootstrap">
                            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                            </ClientSettings>
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="admin_id" HeaderStyle-Font-Bold="true">
                                <Columns>
                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="admin_id" FilterControlAltText="Filter admin_id"
                                        HeaderText="Admin ID" SortExpression="admin_id" UniqueName="admin_id" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="admin_username" FilterControlAltText="Filter admin_username"
                                        HeaderText="Admin Username" SortExpression="admin_username" UniqueName="admin_username" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="role_name" FilterControlAltText="Filter role_name"
                                        HeaderText="Admin Role" SortExpression="role_name" UniqueName="role_name" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="SHW_Name" FilterControlAltText="Filter SHW_Name"
                                        HeaderText="Show" SortExpression="SHW_Name" UniqueName="SHW_Name" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridButtonColumn CommandArgument="id" Text="Edit"
                                        ConfirmDialogType="RadWindow" CommandName="EditAdmin" ButtonType="LinkButton" UniqueName="EditAdmin"
                                        HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </asp:Panel>
            </div>
        </div>
</asp:Content>

