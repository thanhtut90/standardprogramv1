﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;

public partial class Admin_MasterRegistrationList_Company : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    string showID = string.Empty;
    static string _Name = "Name";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _City = "City";
    static string _State = "State";
    static string _ZipCode = "Zip Code";
    static string _Country = "Country";
    static string _TelCC = "Telcc";
    static string _TelAC = "Telac";
    static string _Tel = "Tel";
    static string _FaxCC = "Faxcc";
    static string _FaxAC = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Website = "Website";
    static string _Additional1 = "Additional1";
    static string _Additional2 = "Additional2";
    static string _Additional3 = "Additional3";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                string userID = Session["userid"].ToString();

                if (!string.IsNullOrEmpty(userID))
                {
                    binddata();

                    if (Session["roleid"].ToString() != "1")
                    {
                        getShowID(userID);
                        showlist.Visible = false;
                    }
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                {
                    Response.Redirect("Event_Config");
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
                return;
                //Response.Redirect("Login.aspx");
            }
        }
    }

    protected void binddata()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_showList.Items.Clear();
                ddl_showList.DataSource = cmd.ExecuteReader();
                ddl_showList.DataTextField = "SHW_Name";
                ddl_showList.DataValueField = "SHW_ID";
                ddl_showList.DataBind();
                con.Close();
            }
        }
    }

    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_RegGroup and tb_Invoice tables for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID))
        {
            try
            {
                MasterRegCompany msregCompany = new MasterRegCompany(fn);
                msregCompany.ShowID = showID;
                GKeyMaster.DataSource = msregCompany.getDataAllRegCompanyList();
            }
            catch (Exception)
            {
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region Page_PreRender
    protected void Page_PreRender(object o, EventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID))
        {
            #region Company
            DataTable dtfrm = new DataTable();
            FormManageObj frmObj = new FormManageObj(fn);
            frmObj.showID = showID;
            dtfrm = frmObj.getDynFormForCompany().Tables[0];

            #region Declaration
            int vis_Name = 0;
            int vis_Address1 = 0;
            int vis_Address2 = 0;
            int vis_Address3 = 0;
            int vis_City = 0;
            int vis_State = 0;
            int vis_ZipCode = 0;
            int vis_Country = 0;
            int vis_Tel = 0;
            int vis_Fax = 0;
            int vis_Email = 0;
            int vis_Website = 0;
            int vis_Additional1 = 0;
            int vis_Additional2 = 0;
            int vis_Additional3 = 0;
            int vis_Additional4 = 0;
            int vis_Additional5 = 0;
            #endregion

            for (int x = 0; x < dtfrm.Rows.Count; x++)
            {
                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Name)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Name == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Name").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Name").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();
                            vis_Name++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Name").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Address1 == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Address1").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Address1").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Address1++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Address1").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Address2 == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Address2").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Address2").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Address2++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Address2").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Address3 == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Address3").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Address3").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Address3++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Address3").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _City)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_City == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_City").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_City").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_City++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_City").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _State)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_State == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_State").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_State").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_State++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_State").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _ZipCode)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_ZipCode == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_ZipCode").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_ZipCode").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_ZipCode++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_ZipCode").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Country)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Country == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Country").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Country").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Country++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Country").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Tel == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Tel").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Tel").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Tel++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Tel").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Fax == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Fax").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Fax").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Fax++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Fax").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Email == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Email").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Email").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Email++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Email").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Website)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Website == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Website").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Website").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Website++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Website").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional1)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Additional1 == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional1").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional1").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Additional1++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Additional1").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional2)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Additional2 == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional2").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional2").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Additional2++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Additional2").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional3)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Additional3 == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional3").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional3").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Additional3++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Additional3").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Additional4 == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional4").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional4").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Additional4++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Additional4").Display = false;
                    }
                }

                if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                {
                    int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                    if (isshow == 1)
                    {
                        if (vis_Additional5 == 0)
                        {
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional5").Display = true;
                            GKeyMaster.MasterTableView.GetColumn("RC_Additional5").HeaderText = dtfrm.Rows[x]["form_input_text"].ToString();

                            vis_Additional5++;
                        }
                    }
                    else
                    {
                        GKeyMaster.MasterTableView.GetColumn("RC_Additional5").Display = false;
                    }
                }
            }
            ddl_showList_SelectedIndexChanged(this, EventArgs.Empty);
            #endregion
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID))
        {
            string groupid = "";
            string flowID = "";
            foreach (GridDataItem item in GKeyMaster.SelectedItems)
            {
                groupid = item["RegGroupID"].Text;
              
            }

            if (e.CommandName == "Delete")
            {
                GridDataItem item = (GridDataItem)e.Item;
                string del_groupid = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
                if (!string.IsNullOrEmpty(del_groupid))
                {
                    int isDeleted = delRecord(del_groupid);
                    if (isDeleted > 0)
                    {
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + del_groupid + " is already deleted.');", true);
                        return;
                    }
                }
            }
            else if (e.CommandName == "Edit")
            {
                GridDataItem item = (GridDataItem)e.Item;
                string edit_groupid = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
                flowID = item["FlowID"] != null ? item["FlowID"].Text: "";  
                if (!string.IsNullOrEmpty(edit_groupid))
                {
                    string ulr = string.Format("RegCompany_Edit.aspx?groupid={0}&SHW={1}&FLW={2}", cFun.EncryptValue(edit_groupid), cFun.EncryptValue(showID), cFun.EncryptValue(flowID));
                    Response.Redirect(ulr);
                }
            }

            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                GKeyMaster.ExportSettings.IgnorePaging = false;
                GKeyMaster.ExportSettings.ExportOnlyData = true;
                GKeyMaster.ExportSettings.OpenInNewWindow = true;
                GKeyMaster.MasterTableView.ExportToExcel();
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    /// <summary> 
    /// Item databound for the radgrid, set the header text here based on the form management ***Item[UniqueName]
    /// </summary> 
    protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
        if (!string.IsNullOrEmpty(showID))
        {
            if (e.Item is GridHeaderItem)
            {
                GridHeaderItem item = e.Item as GridHeaderItem;

                #region Company
                DataTable dtfrm = new DataTable();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showID;
                dtfrm = frmObj.getDynFormForCompany().Tables[0];

                for (int x = 0; x < dtfrm.Rows.Count; x++)
                {
                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Name)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Name"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Name"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Name"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Address1"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Address1"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Address1"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Address2"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Address2"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Address2"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Address3"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Address3"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Address3"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _City)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_City"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_City"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_City"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _State)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_State"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_State"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_State"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _ZipCode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_ZipCode"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_ZipCode"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_ZipCode"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Country)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Country"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Country"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Country"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Tel"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Tel"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Tel"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Fax"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Fax"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Fax"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Email"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Email"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Email"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Website)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Website"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Website"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Website"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional1)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Additional1"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Additional1"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Additional1"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional2)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Additional2"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Additional2"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Additional2"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional3)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Additional3"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Additional3"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Additional3"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Additional4"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Additional4"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Additional4"].Controls[0] as LinkButton).Text = "";
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (string.IsNullOrEmpty((item["RC_Additional5"].Controls[0] as LinkButton).Text))
                            {
                                (item["RC_Additional5"].Controls[0] as LinkButton).Text = dtfrm.Rows[x]["form_input_text"].ToString();
                            }
                        }
                        else
                        {
                            (item["RC_Additional5"].Controls[0] as LinkButton).Text = "";
                        }
                    }
                }
                #endregion
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    #region delRecord
    private int delRecord(string groupid)
    {
        int isDeleted = 0;

        if (!string.IsNullOrEmpty(showID))
        {
            try
            {
                isDeleted += fn.ExecuteSQL(string.Format("Update tb_RegCompany Set recycle=1 Where RegGroupID={0} And ShowID='{1}'", groupid, showID));
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

        return isDeleted;
    }
    #endregion

    protected void ddl_showList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;
        GKeyMaster.Rebind();
    }

}