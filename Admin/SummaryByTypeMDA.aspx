﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="SummaryByTypeMDA.aspx.cs" Inherits="Admin_SummaryByTypeMDA" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }
        function exportChartToImageIType1() {

            var $ = $telerik.$;
            //get reference to the ClientExportManager object
            var exportManager = $find('<%=RadClientExportManager1.ClientID%>');

            var filename = 'Indiv Registration By Type-Website' + '.png';

            //specify the image settings fileName, proxy, width, height
            var imageSettings = {
                fileName: filename
             }
            //set the image settings
            exportManager.set_imageSettings(imageSettings);
            //export the element/container
            //exportManager.exportImage($(".RadHtmlChart"));
            exportManager.exportImage($("#ctl00_ContentPlaceHolder1_ChartWebsiteLanding"));
        }
        function exportChartToImageIType2() {

            var $ = $telerik.$;
            //get reference to the ClientExportManager object
            var exportManager = $find('<%=RadClientExportManager1.ClientID%>');

            var filename = 'Indiv Registration By Type-Landing Page' + '.png';

            //specify the image settings fileName, proxy, width, height
            var imageSettings = {
                fileName: filename
             }
            //set the image settings
            exportManager.set_imageSettings(imageSettings);
            //export the element/container
            //exportManager.exportImage($(".RadHtmlChart"));
            exportManager.exportImage($("#ctl00_ContentPlaceHolder1_ChartWebsiteLanding2"));
        }
    </script>
    <style type="text/css">
        .tdstyle1
        {
            width:170px;
        }

        .tdstyle2
        {
            width:300px;
        }

        form input[type="text"]
        {
            width:39% !important;
        }

        .ajax__calendar_container
        {
            width: 320px !important;
            height:280px !important;
        }

        .ajax__calendar_body
        {
            width: 100% !important;
            height:100% !important;
        }

        td
        {
            vertical-align:middle;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"></telerik:RadAjaxManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <h3 class="box-title">Summary (By Type - Website or Landing Page)</h3>
            <asp:Panel ID="showlist" runat="server">
                Show List : <asp:DropDownList ID="ddl_showList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_showList_SelectedIndexChanged"></asp:DropDownList>
            </asp:Panel>
            <div class="table-responsive">

                <telerik:RadClientExportManager runat="server" ID="RadClientExportManager1"></telerik:RadClientExportManager>

                <div class="col-lg-12" id="divByType" runat="server">
                    <!-- Default box -->
                    <div class="box" style="width:100%;">
                        <div class="box-header with-border">
                        </div>
                        <div class="box-body">
                            <div style="width:50%;float:left;">
                                <telerik:RadButton RenderMode="Lightweight" runat="server" OnClientClicked="exportChartToImageIType1" 
                                Text="Export Indiv Registration Summary By Type-Website to Image" AutoPostBack="false" Skin="Office2007" ID="btnChartIWebsiteLanding1" />
                                <telerik:RadHtmlChart runat="server" ID="ChartWebsiteLanding"  CssClass="fb-sized">
                                    <PlotArea>
                                        <Series>
                                            <telerik:PieSeries StartAngle="45" DataFieldY="Total" ExplodeField="IsExploded" Name="PieSeriesName"
                                                NameField="Desc">
                                                <LabelsAppearance><%-- DataFormatString="{0}%"--%>
                                                    <ClientTemplate>
                                                        #=dataItem.Total# (#=dataItem.TotalPercentage# %)
                                                    </ClientTemplate>
                                                </LabelsAppearance>
                                                <TooltipsAppearance Color="White" ClientTemplate="#=dataItem.Desc# #=dataItem.Total# (#=dataItem.TotalPercentage# %)"></TooltipsAppearance><%--DataFormatString="{0}%"--%>
                                            </telerik:PieSeries>
                                        </Series>
                                        <YAxis>
                                        </YAxis>
                                    </PlotArea>
                                    <ChartTitle Text="By Type-Website"></ChartTitle>
                                </telerik:RadHtmlChart>
                            </div>
                            <div style="width:50%;float:right;">
                                <telerik:RadButton RenderMode="Lightweight" runat="server" OnClientClicked="exportChartToImageIType2" 
                                Text="Export Indiv Registration Summary By Type-Landing Page to Image" AutoPostBack="false" Skin="Office2007" ID="btnChartIWebsiteLanding2" />
                                <telerik:RadHtmlChart runat="server" ID="ChartWebsiteLanding2"  CssClass="fb-sized">
                                    <PlotArea>
                                        <Series>
                                            <telerik:PieSeries StartAngle="45" DataFieldY="Total" ExplodeField="IsExploded" Name="PieSeriesName"
                                                NameField="Desc">
                                                <LabelsAppearance><%-- DataFormatString="{0}%"--%>
                                                    <ClientTemplate>
                                                        #=dataItem.Total# (#=dataItem.TotalPercentage# %)
                                                    </ClientTemplate>
                                                </LabelsAppearance>
                                                <TooltipsAppearance Color="White" ClientTemplate="#=dataItem.Desc# #=dataItem.Total# (#=dataItem.TotalPercentage# %)"></TooltipsAppearance><%--DataFormatString="{0}%"--%>
                                            </telerik:PieSeries>
                                        </Series>
                                        <YAxis>
                                        </YAxis>
                                    </PlotArea>
                                    <ChartTitle Text="By Type-Landing Page"></ChartTitle>
                                </telerik:RadHtmlChart>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />

                <div class="col-lg-12" id="div1" runat="server">
                    <!-- Default box -->
                    <div class="box" style="width:100%;">
                        <div class="box-header with-border">
                        </div>
                        <div class="box-body">
                            <div style="width:50%;float:left;">
                                <asp:Button ID="Button1" runat="server" Text="Export to Excel Indiv Report By Type-Website" OnClick="genTypeIWebsiteExcel" />
                                <asp:GridView ID="gvSummaryByTypeIWebsite" runat="server" DataKeyNames="Type" AutoGenerateColumns="true" CssClass="table" AllowPaging="false"
                                    GridLines="None">
                                    <HeaderStyle CssClass="theadstyle" />
                                </asp:GridView>
                            </div>
                            <div style="width:50%;float:right;">
                                <asp:Button ID="Button2" runat="server" Text="Export to Excel Indiv Report By Type-Landing Page" OnClick="genTypeILandingExcel" />
                                <asp:GridView ID="gvSummaryByTypeILanding" runat="server" DataKeyNames="Type" AutoGenerateColumns="true" CssClass="table" AllowPaging="false"
                                    GridLines="None">
                                    <HeaderStyle CssClass="theadstyle" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Button1" />
            <asp:PostBackTrigger ControlID="Button2" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

