﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Settings_PaymentPortalConfig.aspx.cs" Inherits="Admin_Event_Setttings_PaymentPortalConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="box-title">Payment Portal Config </h1>
    <div class="form-horizontal">
        <div class="box-body">
              <div class="form-group">
                            <label class="col-md-2 control-label">Payment Portal </label>
                            <div class="col-md-6">
                               <asp:DropDownList runat="server" ID="ddlPaymentPotal" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Project ID<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <asp:TextBox ID="txtProjectID" runat="server" CssClass="form-control" Text=""></asp:TextBox>
                      <asp:Label runat="server" ID="lblProjMsg" CssClass="text-danger" Text="Duplicate ID" Visible="false"></asp:Label>
                      <asp:RequiredFieldValidator runat="server" ControlToValidate="txtProjectID" CssClass="text-danger" Text="Required" Display="Dynamic" ></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Project Description<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control" Text=""></asp:TextBox> 
                      <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDesc" CssClass="text-danger" Text="Required" Display="Dynamic" ></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">CURRENCY (eg.SGD)<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <asp:TextBox ID="txtCurrency" runat="server" CssClass="form-control" Text=""></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtCurrency" CssClass="text-danger" Text="Required" Display="Dynamic" ></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Merchant Account No.<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <asp:TextBox ID="txtMarchentACC" runat="server" CssClass="form-control" Text=""></asp:TextBox>
                     <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMarchentACC" CssClass="text-danger" Text="Required" Display="Dynamic" ></asp:RequiredFieldValidator>
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-2 control-label">Merchant Account Password</label>
                <div class="col-md-6">
                    <asp:TextBox ID="txtMPassord" runat="server" CssClass="form-control" Text=""></asp:TextBox>

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Secrect KEY </label>
                <div class="col-md-6">
                    <asp:TextBox ID="txtSecretKey" runat="server" CssClass="form-control" Text=""></asp:TextBox>

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Return URL<span class="text-danger">*</span></label>
                <div class="col-md-6">
                    <asp:TextBox ID="txtReturnURL" runat="server" CssClass="form-control" Text=""></asp:TextBox>
                     <asp:RequiredFieldValidator runat="server" ControlToValidate="txtReturnURL" CssClass="text-danger" Text="Required" Display="Dynamic" ></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Payment Portal URL</label>
                <div class="col-md-6">
                    <asp:TextBox ID="txtPaymentPortalURL" runat="server" CssClass="form-control" Text=""></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-2 control-label"></div>
                <div class="col-md-6">
                    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back" CssClass="btn btn-primary" CausesValidation="false" />
                    &nbsp;
                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="SaveForm" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

