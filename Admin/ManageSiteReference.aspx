﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageSiteReference.aspx.cs" Inherits="Admin_ManageSiteReference" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     
    <h3 class="box-title">Manage Site Reference</h3>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateRecord">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <br />
    <br />
    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CssClass="btn btn-info"><i class="fa fa-arrow-left"></i> Back</asp:LinkButton>

    <br />
    <br />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
        <asp:Panel runat="server" ID="PanelKeyList">
            <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="20" Skin="Bootstrap">
                <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true"></Selecting>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" DataKeyNames="id,list_name">
                    <Columns>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="id" FilterControlAltText="Filter id"
                            HeaderText="ID" SortExpression="id" UniqueName="id">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="list_name" FilterControlAltText="Filter list_name"
                            HeaderText="Name" SortExpression="list_name" UniqueName="list_name">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="list_value" FilterControlAltText="Filter list_value"
                            HeaderText="Value" SortExpression="list_value" UniqueName="list_value">
                        </telerik:GridBoundColumn>

                        <telerik:GridButtonColumn CommandArgument="id" Text="Insert"
                            ConfirmDialogType="RadWindow" CommandName="InsertRef" ButtonType="LinkButton" UniqueName="InsertRef"
                            HeaderText="Insert" ItemStyle-HorizontalAlign="Center">
                        </telerik:GridButtonColumn>

                        <telerik:GridButtonColumn CommandArgument="id" Text="Update"
                            ConfirmDialogType="RadWindow" CommandName="UpdateRef" ButtonType="LinkButton" UniqueName="UpdateRef"
                            HeaderText="Update" ItemStyle-HorizontalAlign="Center">
                        </telerik:GridButtonColumn>

                        <telerik:GridButtonColumn ConfirmTextFormatString="Do you want to Delete Script Module of {0}?" ConfirmTextFields="id" Text="Delete"
                            CommandArgument="id" ConfirmDialogType="RadWindow" CommandName="Delete" ButtonType="LinkButton" UniqueName="Delete"
                            HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                        </telerik:GridButtonColumn>

                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </asp:Panel>

        <br />
        <br />
        <asp:Panel runat="server" ID="PanelKeyDetail">
            <h4>Detail</h4>

            <div class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Name</label>
                        <div class="col-md-2">
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtName" runat="server" CssClass="form-control"></telerik:RadTextBox>
                        </div>
                        <div class="col-md-8">
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" Display="Dynamic"
                                ControlToValidate="txtName" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Value</label>
                        <div class="col-md-2">
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtValue" runat="server" CssClass="form-control"></telerik:RadTextBox>
                        </div>
                        <div class="col-md-8">
                            <asp:RequiredFieldValidator ID="rfvValue" runat="server" Display="Dynamic"
                                ControlToValidate="txtValue" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2 control-label"></div>
                        <div class="col-md-4">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" CssClass="btn btn-primary" />
                            <asp:Button ID="btnUpdateRecord" runat="server" Text="Update Record" OnClick="btnUpdateRecord_Click" UseSubmitBehavior="true" CssClass="btn btn-primary"></asp:Button>
                            <asp:Label runat="server" ID="lbls" ForeColor="Red"></asp:Label>
                            <asp:HiddenField ID="hfID" runat="server" Value="" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </telerik:RadAjaxPanel>
</asp:Content>

