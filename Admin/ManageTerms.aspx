﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageTerms.aspx.cs" Inherits="Admin_ManageTerms" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Terms and Conditions Management</h1>
            <span class="pagedesc">Manage the list of terms and conditions.</span>
            
            <ul class="hornav">
                <li class="current"><a href="#index">Index</a></li>
            </ul>
        </div><!--page header -->
        <div id="contentwrapper" class="contentwrapper">
            <div id="index" class="subcontent">
                
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h4>Add Terms and Conditions</h4>

                        <asp:Label ID="lblName" runat="server" Text="Name" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtName" runat="server" Width="350px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtName" runat="server" 
                            ErrorMessage="Please insert the name" ForeColor="Red"></asp:RequiredFieldValidator>
                        <br />

                        <asp:Label ID="lblDesc" runat="server" Text="Description" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtDesc" runat="server" Width="350px" TextMode="MultiLine"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtDesc" runat="server" 
                            ErrorMessage="Please insert the description" ForeColor="Red"></asp:RequiredFieldValidator>
                        <br />
                        <asp:Button ID="Button1" runat="server" Text="Add" OnClick="addTerms" />
                        <br /><br />

                        <asp:Panel ID="pnllist" runat="server" Visible="false">
                            <asp:GridView ID="gvList" runat="server" DataKeyNames="terms_ID" 
                             OnRowDeleting="gvList_RowDeleting"
                            OnRowEditing="GridView1_RowEditing"
                            OnRowUpdating="GridView1_RowUpdating"
                            OnRowCancelingEdit="GridView1_RowCancelingEdit" AutoGenerateColumns="false"
                            CssClass="stdtable"><%--OnRowCommand="gvList_RowCommand"--%>
                                <HeaderStyle CssClass="theadstyle" />
                                <Columns>
                                    <asp:BoundField DataField="terms_ID" HeaderText="Id" />
                                    <asp:TemplateField HeaderText="Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("terms_Name") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("terms_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDescription" runat="server" Text='<%# Eval("terms_Desc") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("terms_Desc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Order">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtOrder" runat="server" Text='<%# Eval("terms_Order") %>' Width="50px"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="ftbOrder" runat="server" TargetControlID="txtOrder" FilterType="Numbers" ValidChars="0123456789" />
                                            <asp:Label ID="lblno" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrder" runat="server" Text='<%# Eval("terms_Order") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="IsDeleted">
                                        <ItemTemplate>
                                            <%# Eval("terms_isDeleted") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowEditButton="True" CausesValidation="false" />
                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument='<%# Eval("terms_ID")%>'
                                                OnClientClick="return confirm ('Are you sure you want to delete this record?')" Text="Delete" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Un-Delelete" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkUnDelete" runat="server" CausesValidation="False"
                                                OnClick="lnkUnDelete_Click" Text="Un-Delelete" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div style="color:Red"><asp:Label ID="lblMsg" runat="server"></asp:Label></div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div><!--page index -->
        </div><!--contentwrapper -->
    </div><!--centercontent -->
</asp:Content>