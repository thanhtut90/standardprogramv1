﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class QuestionnaireAddItem : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["QAID"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                string qaID = cFun.DecryptValue(Request.QueryString["QAID"].ToString());
                loadQAFrame("edit");
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void loadQAFrame(string option)
    {
        if (option == "upload")
        {
            this.questionnaireFrame.Attributes.Remove("src");
            this.questionnaireFrame.Attributes.Add("src", ConfigurationManager.AppSettings["QuestionnaireUploadPortalURL"] +"?qnaire_id=" + Request.QueryString["QAID"].ToString());
        }

        else if (option == "edit")
        {
            this.questionnaireFrame.Attributes.Remove("src");
            this.questionnaireFrame.Attributes.Add("src", ConfigurationManager.AppSettings["QuestionnairePortalURL"] + "?qnaire_id=" + Request.QueryString["QAID"].ToString());
        }
    }

    protected void UploadQA(object sender, EventArgs e)
    {
        loadQAFrame("upload");
    }

    protected void EditQA(object sender, EventArgs e)
    {
        loadQAFrame("edit");
    }

    protected void EndQA(object sender, EventArgs e)
    {
        FlowControler fControl = new FlowControler(fn);

        if (Request.Params["a"] != null)
        {
            string admintype = cFun.DecryptValue(Request.QueryString["a"].ToString());
            if (admintype == BackendStaticValueClass.isFlowEdit)
            {
                string link = fControl.MakeFullURL("ManageQuestionnaire", Request.Params["FLW"], Request.Params["SHW"], null, Request.Params["STP"], null, Request.Params["type"]);
                link += "&a=" + cFun.EncryptValue(admintype);
                Response.Redirect(link);
                //Response.Redirect("ManageQuestionnaire?SHW=" + Request.QueryString["SHW"].ToString() + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["TYPE"]);
            }
            else
            {
                string link = fControl.MakeFullURL("ManageQuestionnaire", Request.Params["FLW"], Request.Params["SHW"], null, Request.Params["STP"], null, Request.Params["type"]);
                Response.Redirect(link);
                //Response.Redirect("ManageQuestionnaire?SHW=" + Request.QueryString["SHW"].ToString() + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["TYPE"]);
            }
        }
        else
        {
            string link = fControl.MakeFullURL("ManageQuestionnaire", Request.Params["FLW"], Request.Params["SHW"], null, Request.Params["STP"], null, Request.Params["type"]);
            Response.Redirect(link);
            //Response.Redirect("ManageQuestionnaire?SHW=" + Request.QueryString["SHW"].ToString() + "&FLW=" + Request.Params["FLW"] + "&STP=" + Request.Params["STP"] + "&TYPE=" + Request.Params["TYPE"]);
        }
    }
}