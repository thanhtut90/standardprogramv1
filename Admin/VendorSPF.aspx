﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="VendorSPF.aspx.cs" Inherits="Admin_VendorSPF" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script>
        function checkBOx()
        { 
            if ($('#chkCorrect').is(':checked')) {
                return true;
            }
            else {
                alert('Pls Click on Check');
                return false;
            }

        
        }
         
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
            <div>
            <h2>SPF Download</h2>
        </div>
        <asp:Panel runat="server" ID="PanelStep1">
            <div>
                <asp:DropDownList runat="server" ID="ddlClassification" CssClass="form-control">
                    <asp:ListItem Text="Vendor" Value="Vendor"></asp:ListItem>
                </asp:DropDownList>
                <br /><br />
                <asp:CheckBox runat="server" ID="chkInclude" Text="Include Sumitted Records" Visible="false"/>
            </div>
            <div style="padding-top: 20px;">
                <asp:Button runat="server" ID="btnShowSPFList" Text="Show Download List" CssClass="btn btn-primary" OnClick="btnShowDownloadList_OnClick"></asp:Button>
            </div>
              <asp:Label runat="server" ID="lblMsg" Visible="false" Text="No Record Found" CssClass="text-danger"></asp:Label>
        </asp:Panel>
        <asp:Panel runat="server" ID="PanelShowList" Visible="false">
                 <input id="chkCorrect" type="checkbox"  > <b>All Data are correct</b>
               
               <br /><br />
            <asp:Button runat="server" ID="btnDownload" Text="Download"  CssClass="btn btn-danger" OnClick=" btnDownload_Onclick" OnClientClick="return checkBOx();" />
            <br /><br />
                     <asp:GridView runat="server" ID="gvList" AutoGenerateColumns="true" Width="100%" CssClass="gridstyle"></asp:GridView>
        </asp:Panel>
        <asp:TextBox runat="server" ID="txtShow" Visible="false"></asp:TextBox>
    </div>
</asp:Content>

