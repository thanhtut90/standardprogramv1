﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;

public partial class Admin_ManageRefAdditionalList : System.Web.UI.Page
{
    /// <summary>
    /// If you update or delete already inserted records, need to check and update in the tb_RegDelegate, tb_RegGroup table
    /// because the additional name may be used by those table.
    /// </summary>


    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["SHW"] != null && Request.QueryString["TYP"] != null)
            {
                lblShowid.Text = Request.QueryString["SHW"].ToString();
                lblRefAdditionalFieldType.Text = Request.QueryString["TYP"].ToString();
                BindGrid(lblShowid.Text, lblRefAdditionalFieldType.Text);
                //Master.setgenopen();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    #region BindGrid and use table ref_AdditionalList & bind gvList & set pnllist visibility true or false
    protected void BindGrid(string showid, string fieldType)
    {
        DataSet dsRefAdditionalList = new DataSet();

        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsRefAdditionalList = cmdObj.getRefAdditionalList(showid, fieldType);
        if (dsRefAdditionalList.Tables[0].Rows.Count > 0)
        {
            gvList.DataSource = dsRefAdditionalList;
            gvList.DataBind();
            pnllist.Visible = true;
        }
        else
        {
            pnllist.Visible = false;
        }
    }
    #endregion

    #region GridView1_RowDeleting and OnRowDeleting event of gvList & use table ref_AdditionalList & delete from ref_AdditionalList according to RefAddid
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;
        SetUpController setupCtrlr = new SetUpController(fn);
        RefAdditionalListObj objRefAdd = new RefAdditionalListObj();
        objRefAdd.ShowID = lblShowid.Text;
        objRefAdd.refAdd_id = Convert.ToInt32(ID);
        setupCtrlr.deleteRefAdditionalList(objRefAdd);
        BindGrid(lblShowid.Text, lblRefAdditionalFieldType.Text);
    }
    #endregion

    #region GridView1_RowEditing and OnRowEditing event of gvList
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvList.EditIndex = e.NewEditIndex;
        BindGrid(lblShowid.Text, lblRefAdditionalFieldType.Text);
    }
    #endregion

    #region GridView1_RowUpdating and OnRowUpdating event of gvList & use table ref_AdditionalList & update ref_AdditionalList according to RefAddid
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;
        // Retrieve the row being edited.
        int index = gvList.EditIndex;
        GridViewRow row = gvList.Rows[index];
        TextBox t1 = row.FindControl("txtName") as TextBox;
        TextBox t2 = row.FindControl("txtOrder") as TextBox;
        Label lblno = row.FindControl("lblno") as Label;
        string t3 = gvList.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            lblno.Visible = false;
            SetUpController setupCtrlr = new SetUpController(fn);
            RefAdditionalListObj objRefAdd = new RefAdditionalListObj();
            objRefAdd.refAdd_name = cFun.solveSQL(t1.Text.ToString());
            objRefAdd.refAdd_SortOrder = Convert.ToInt32(t2.Text.ToString());
            objRefAdd.ShowID = lblShowid.Text;
            objRefAdd.refAdd_FieldType = lblRefAdditionalFieldType.Text;
            objRefAdd.refAdd_id = Convert.ToInt32(ID);
            setupCtrlr.updateRefAdditionalList(objRefAdd);

            gvList.EditIndex = -1;
            BindGrid(lblShowid.Text, lblRefAdditionalFieldType.Text);
        }
        catch (Exception ex)
        {
            lblno.Visible = true;
            lblno.Text = ex.Message;
        }
    }
    #endregion

    #region GridView1_RowCancelingEdit and OnRowCancelingEdit event of gvList
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvList.EditIndex = -1;
        BindGrid(lblShowid.Text, lblRefAdditionalFieldType.Text);
    }
    #endregion

    #region btnAdd_Click and use table ref_AdditionalList & insert into ref_AdditionalList table
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string RefAddID = lblRefAddID.Text;
        string RefAddName = cFun.solveSQL(txtRefAddName.Text.ToString());
        string sortorder = cFun.solveSQL(txtSortorder.Text.ToString());

        try
        {
            SetUpController setupCtrlr = new SetUpController(fn);
            RefAdditionalListObj objRefAdd = new RefAdditionalListObj();
            objRefAdd.refAdd_name = RefAddName;
            objRefAdd.refAdd_SortOrder = Convert.ToInt32(sortorder);
            objRefAdd.ShowID = lblShowid.Text;
            objRefAdd.refAdd_FieldType = lblRefAdditionalFieldType.Text;

            if (!string.IsNullOrEmpty(lblRefAddID.Text))
            {
                objRefAdd.refAdd_id = Convert.ToInt32(RefAddID);
                setupCtrlr.updateRefAdditionalList(objRefAdd);
            }
            else
            {
                int success = setupCtrlr.insertRefAdditionalList(objRefAdd);
            }
        }
        catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }

        BindGrid(lblShowid.Text, lblRefAdditionalFieldType.Text);
        txtRefAddName.Text = "";
        txtSortorder.Text = "";
    }
    #endregion
}