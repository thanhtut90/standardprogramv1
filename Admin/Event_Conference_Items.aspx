﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Conference_Items.aspx.cs" Inherits="Admin_Event_Conference_Items" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/jquery-ui.min.css" rel="stylesheet" />
    <link href="css/jquery-ui.theme.min.css" rel="stylesheet" />
    <link href="css/theme.css" rel="stylesheet" />
    <style type="text/css">
        input[type="checkbox"], input[type="radio"]
        {
            margin : 4px !important;
        }
    </style>

    <script src="js/plugins/jquery-1.7.min.js"></script>
    <script src="js/plugins/jquery-ui.min.js"></script>

    <script type="text/javascript">
        var _confirm = false;

        function confirmCheckIn(button) {
     
            if (_confirm == false) {
                $('<div>')
                    .html("<p>Do you want to Proceed?</p>")
                    .dialog({
                        autoOpen: true,
                        modal: true,
                        title: "Confirmation",
                        buttons: {
                            "No": function () {
                                $(this).dialog("close");
                            },
                            "Yes": function () {
                                $(this).dialog("close");
                                _confirm = true;
                                button.click();
                            }
                        },
                        close: function () {
                            $(this).remove();
                        }
                    });
            }
            return _confirm;
        }

        function radioMeShow(e) {
              <%--if (!e) e = window.event;
              var sender = e.target || e.srcElement;

              if (sender.nodeName != 'INPUT') return;
              var checker = sender;
              var chkBox = document.getElementById('<%= chkConfigType.ClientID %>');
              var chks = chkBox.getElementsByTagName('INPUT');
              for (i = 0; i < chks.length; i++) {
                  if (chks[i] != checker)
                  chks[i].checked = false;
              }--%>
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <h3 class="box-title">Manage Conference Items</h3>
 

        <br />
    <asp:Button ID="btnAddNewConfItem" runat="server" Text="Add Conference Item" OnClick="btnAddNewConfItem_Click"  CssClass="btn btn-primary" CausesValidation ="false"/>
        <div id="divConference" runat="server" visible="true">
    
            <asp:Panel ID="pnllist" runat="server" Visible="true">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Edit</th>
                                <th>Delete</th>
                                <th>Add Dependent Item(s)</th>
                                <th>Is Show</th>
                                <th>Order Sequence</th>
                                <th>Item Name</th>
                                <th>Item Description</th>
                                <th>Group</th>
                                <th>Category</th>
                                <th>Image</th>
                                <th>Early Bird Price</th>
                                <th>Regular Price</th>
                                <th>Maximum Registrant</th>
                                <th>Used Quantity</th>
                                <th>Limit Message</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rptItem" runat="server" OnItemCommand="rptcommand">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="images/default/edit-icon.png" CommandName="edit"
                                                CommandArgument='<%#Eval("con_itemId") %>' />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="images/default/searchcancel.png" CommandName="delete" 
                                                OnClientClick="return confirmCheckIn(this);" CommandArgument='<%#Eval("con_itemId") %>' />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="images/default/add.png" CommandName="addsubitem"
                                                CommandArgument='<%#Eval("con_itemId") %>' />
                                        </td>
                                        <td><%# setIsShowValue(Eval("con_IsShow").ToString())%></td>
                                        <td><%#Eval("con_OrderSeq")%></td>
                                        <td><%#Eval("con_ItemName") %><%--<%#setName(Eval("con_GroupItemID").ToString()) %>--%></td>
                                        <td>
                                            <asp:Label ID="lblDesc" runat="server" Text='<%# System.Web.HttpUtility.HtmlDecode(Eval("con_ItemDesc").ToString()) %>'></asp:Label>
                                        </td>
                                        <td><%#setGroupName(Eval("con_GroupId").ToString())%></td>
                                        <td><%#setCategory(Eval("reg_categoryId").ToString())%></td>
                                        <td style="width:15%"><img runat="server" id="imgPhoto" visible='<%#setImgVisibility(Eval("con_ItemImage").ToString())%>' src='<%#Eval("con_ItemImage")%>' width="100"/></td>
                                        <td><%#Eval("con_EBPrice")%></td>
                                        <td><%#Eval("con_RegPrice")%></td>
                                        <td><%#Eval("con_MaxReg")%></td>
                                        <td><%#Eval("con_Used")%></td>
                                        <td>
                                            <asp:Label ID="lblMaxMsg" runat="server" Text='<%# System.Web.HttpUtility.HtmlDecode(Eval("con_MaxMessage").ToString()) %>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </asp:Panel>

            <br />
            <div id="divConfInsertUpdate" runat="server" visible="false">
                <h5><b>Conference Item Management</b></h5>
                <div class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Image</label>
                            <div class="col-md-6">
                                <asp:FileUpload ID="fupImg" runat="server" CssClass="col-md-4" />
                                <asp:Button ID="Button3" runat="server" Text="Upload" OnClick="UpSystemLogo_Click" CssClass="btn btn-primary" CausesValidation="false"/>
                                <asp:Image ID="imgPhoto" runat="server"  width="25%" style="display:block;"/>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox runat="server" ID="txtGroupID" Visible="false" ></asp:TextBox>
                                <asp:TextBox runat="server" ID="txtGroupItem" Visible="false" ></asp:TextBox>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Conference Group</label>
                                    <div class="col-md-6">
                                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:LinkButton ID="lbAddGroup" runat="server" OnClick="AddGroupClick" CausesValidation="false">+ Add New Group</asp:LinkButton>
                                        <asp:TextBox ID="txtNewGroup" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
                                        <asp:Button ID="btnAddGroup" runat="server" Text="Add" Visible="false" OnClick="SaveGroupClick" CausesValidation="false" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnCancelGroup" runat="server" Text="Cancel" Visible="false" OnClick="CancelGroupClick" CausesValidation="false" CssClass="btn btn-default" />
                                        <asp:Label ID="lblvalgroup" runat="server" Text="* Required" ForeColor="Red" Visible="false"></asp:Label>
                                        <asp:DropDownList ID="ddlGroupItem" runat="server" Enabled="false" CssClass="form-control" Visible="false">
                                            <%--<asp:ListItem Value="Select">Select</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-4">
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtName"
                                        Display="Dynamic" ErrorMessage="* Required" CssClass="validator" ForeColor="Red"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Conference Item Name</label>
                                    <div class="col-md-6">
                                        <%--<asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                        <CKEditor:CKEditorControl ID="txtName" runat="server"  Height="300px" BasePath="~/Admin/ckeditor" EnterMode="BR" 
                                        ShiftEnterMode="P" FilebrowserUploadUrl="Upload.ashx"></CKEditor:CKEditorControl>
                                    </div>
                                    <div class="col-md-4">
                                        <asp:RequiredFieldValidator runat="server" ID="rfvName" ControlToValidate="txtName"
                                        Display="Dynamic" ErrorMessage="* Required" CssClass="validator" ForeColor="Red"/>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Description</label>
                            <div class="col-md-6">
                                <CKEditor:CKEditorControl ID="txtDesc" runat="server"  Height="300px" BasePath="~/Admin/ckeditor" EnterMode="BR" 
                                    ShiftEnterMode="P" FilebrowserUploadUrl="Upload.ashx"></CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Description (Show In Email Template)</label>
                            <div class="col-md-6">
                                <CKEditor:CKEditorControl ID="txtDescShowInEmailTemplate" runat="server"  Height="300px" BasePath="~/Admin/ckeditor" EnterMode="BR" 
                                    ShiftEnterMode="P" FilebrowserUploadUrl="Upload.ashx"></CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Description (Show In Item Breakdown Report)</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtDescShowInReportItemBreakdown" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Description(to show above name)</label>
                            <div class="col-md-6">
                                <CKEditor:CKEditorControl ID="txtDesc2" runat="server"  Height="300px" BasePath="~/Admin/ckeditor" EnterMode="BR" 
                                    ShiftEnterMode="P" FilebrowserUploadUrl="Upload.ashx"></CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-6">
                                        <asp:CheckBox ID="chkIsEarlyBird" runat="server" Text="&nbsp;Has Early Bird Price" Checked="true" onclick="checkbx();"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-6">
                                        <asp:CheckBox ID="chkIsShow" runat="server" Text="&nbsp;Is Show" Checked="true"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Order Sequence</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtSequence" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtSequence" FilterType="Numbers" ValidChars="0123456789" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Early Bird Price</label>
                                    <div class="col-md-6">
                                        <%=Global.Currency%> <asp:TextBox ID="txtEPrice" runat="server" Text="0" CssClass="form-control"></asp:TextBox>
                                        <asp:Label ID="lblvaleprice" runat="server" Text="* Required" ForeColor="Red"  Visible="false"></asp:Label>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtEPrice" FilterType="Numbers, Custom" ValidChars="." />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Regular Price</label>
                                    <div class="col-md-6">
                                        <%=Global.Currency%> <asp:TextBox ID="txtRPrice" runat="server" Text="0" CssClass="form-control"></asp:TextBox>
                                        <asp:Label ID="lblvalrprice" runat="server" Text="* Required" ForeColor="Red"  Visible="false"></asp:Label>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtRPrice" FilterType="Numbers, Custom" ValidChars="." />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Maximum Registrant</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtMax" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtMax" FilterType="Numbers" ValidChars="0123456789" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Limit Message</label>
                                    <div class="col-md-6">
                                        <CKEditor:CKEditorControl ID="txtMessage" runat="server" Height="300px" BasePath="~/Admin/ckeditor" EnterMode="BR" 
                                        ShiftEnterMode="P" FilebrowserUploadUrl="Upload.ashx"></CKEditor:CKEditorControl>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Disabled Items</label>
                                    <div class="col-md-6">
                                        <asp:CheckBoxList ID="chkDisabledItems" runat="server"></asp:CheckBoxList>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-6">
                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="SaveUpdateItem" CssClass="btn btn-primary" />&nbsp;&nbsp;
                                <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" CausesValidation="false" CssClass="btn btn-default" />&nbsp;&nbsp;
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-info" />
                                <asp:HiddenField ID="hfConfID" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" CausesValidation="false" CssClass="btn btn-success pull-right" />

            <asp:TextBox runat="server" ID="txtCategory" Text="" Visible="false"></asp:TextBox>
        </div>
</asp:Content>