﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Corpit.Utilities;
using System.IO;
using System.Globalization;
using Telerik.Web.UI;
using Corpit.Site.Utilities;
using Corpit.BackendMaster;

public partial class Admin_Event_Settings_WebSettings : System.Web.UI.Page
{

    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    static string _pagebanner = "page_banner";
    static string _pagetitle = "pagetitle";
    static string _Sitemaster = "Site_master";
    static string _SiteCSSBandle = "Site_CSSBandle";
    static string _copyrigtht = "copyrigtht";
    static string _Sitereloginmaster = "Site_master_relogin";
    static string _prefix = "prefix";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

                //If show is not created, redirect to the create event page
                if (!checkCreated(showid))
                {
                    Response.Redirect("Event_Config");
                }

                setBanner(showid);
                bindShowName(showid);
                binddata(showid);
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region binddata (bind data from tb_site_settings to related controls)
    protected void binddata(string showid)
    {
        string query = string.Empty;
        string pagetitle = string.Empty;
        string copyright = string.Empty;
        string img = string.Empty;
        string prefix = string.Empty;

        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select list_value From tb_site_reference Where list_name ='Site_master'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddlsitemaster.Items.Clear();
                ddlsitemaster.DataSource = cmd.ExecuteReader();
                ddlsitemaster.DataTextField = "list_value";
                ddlsitemaster.DataValueField = "list_value";
                ddlsitemaster.DataBind();
                con.Close();
            }
            using (SqlCommand cmd = new SqlCommand("Select list_value From tb_site_reference Where list_name ='Site_CSSBandle'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddlcssbandle.Items.Clear();
                ddlcssbandle.DataSource = cmd.ExecuteReader();
                ddlcssbandle.DataTextField = "list_value";
                ddlcssbandle.DataValueField = "list_value";
                ddlcssbandle.DataBind();
                con.Close();
            }
            using (SqlCommand cmd = new SqlCommand("Select list_value From tb_site_reference Where list_name ='Site_master_relogin'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddlsitereloginmaster.Items.Clear();
                ddlsitereloginmaster.DataSource = cmd.ExecuteReader();
                ddlsitereloginmaster.DataTextField = "list_value";
                ddlsitereloginmaster.DataValueField = "list_value";
                ddlsitereloginmaster.DataBind();
                con.Close();
            }
        }


        query = "Select settings_value From tb_site_settings Where settings_name='" + _pagetitle + "' And ShowID=@SHWID";
        pagetitle = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _copyrigtht + "' And ShowID=@SHWID";
        copyright = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _Sitemaster + "' And ShowID=@SHWID";
        ddlsitemaster.SelectedValue = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _SiteCSSBandle + "' And ShowID=@SHWID";
        ddlcssbandle.SelectedValue = fn.GetDataByCommand(query, "settings_value", pList);

        query = "Select settings_value From tb_site_settings Where settings_name='" + _Sitereloginmaster + "' And ShowID=@SHWID";
        ddlsitereloginmaster.SelectedValue = fn.GetDataByCommand(query, "settings_value", pList);

        //query = "Select settings_value From tb_site_settings Where settings_name='" + _prefix + "' And ShowID=@SHWID";
        //prefix = fn.GetDataByCommand(query, "settings_value", pList);

        txt_title.Text = pagetitle;
        txtCopyright.Text = Server.HtmlDecode(copyright);
       // txtPrefix.Text = prefix;
    }
    #endregion

    #region upload (save and get fupload controls selected file)
    private void upload()
    {
        if (fupload.HasFile)
        {
            string filePath = "../images/Site/";
            string chkpath = Server.MapPath(filePath);
            var directory = new DirectoryInfo(chkpath);
            if (directory.Exists == false)
            {
                directory.Create();
            }

            string filepath = Server.MapPath(filePath);
            String fileExtension = System.IO.Path.GetExtension(fupload.FileName).ToLower();
            String[] allowedExtensions = { ".gif", ".png", ".jpeg", ".jpg" };
            for (int i = 0; i < allowedExtensions.Length; i++)
            {
                if (fileExtension == allowedExtensions[i])
                {
                    try
                    {
                        string showid = "";
                        if (Request.Params["SHW"] != null)
                        {
                            showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                        }
                        else
                            showid = "Default0009";

                        string s_newfilename = showid + DateTime.Now.Year.ToString() +
                        DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() +
                        DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + fileExtension;
                        fupload.PostedFile.SaveAs(filepath + s_newfilename);



                        Session["imgBanner"] = "images/site/" + s_newfilename;
                    }
                    catch (Exception ex)
                    {
                        Response.Write("File could not be uploaded.");
                    }

                }


            }
        }
    }
    #endregion

    #region SaveForm (Save data into related tb_site_settings table)
    protected void SaveForm(object sender, EventArgs e)
    {
        upload();
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            try
            {
                string sql = string.Empty;
                string imgBanner = string.Empty;
                string title = string.Empty;
                string copyright = string.Empty;
                string sitemaster = string.Empty;
                string sitecssbandle = string.Empty;
                string sitereloginmaster = string.Empty;
                string prefix = string.Empty;
                string eventname = string.Empty;

                eventname = cFun.solveSQL(txtEventName.Text.ToString());
                title = txt_title.Text;
                copyright = Server.HtmlEncode(txtCopyright.Text.ToString());
                sitemaster = Server.HtmlEncode(ddlsitemaster.SelectedValue.ToString());
                sitecssbandle = Server.HtmlEncode(ddlcssbandle.SelectedValue.ToString());
                sitereloginmaster = Server.HtmlEncode(ddlsitereloginmaster.SelectedValue.ToString());

                #region Inserting data into tb_Show
                ShowController shwCtr = new ShowController(fn);
                ShowObj sObj = new ShowObj();
                sObj.showid = showid;
                sObj.showname = eventname;
                bool rowInserted = shwCtr.UpdateShowName(sObj);
                #endregion

                upload();

                if (Session["imgBanner"] != null)
                {
                    imgBanner = Session["imgBanner"].ToString();
                    sql = "Update tb_site_settings Set settings_value='" + imgBanner + "' Where settings_name='" + _pagebanner + "' And ShowID='" + showid + "';";
                }

                sql += "Update tb_site_settings Set settings_value='" + title + "' Where settings_name='" + _pagetitle + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + copyright + "' Where settings_name='" + _copyrigtht + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + sitemaster + "' Where settings_name='" + _Sitemaster + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + sitecssbandle + "' Where settings_name='" + _SiteCSSBandle + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + sitereloginmaster + "' Where settings_name='" + _Sitereloginmaster + "' And ShowID='" + showid + "';";
               // sql += "Update tb_site_settings Set settings_value='" + prefix + "' Where settings_name='" + _prefix + "' And ShowID='" + showid + "';";
                fn.ExecuteSQL(sql);

                pnlmessagebar.Visible = true;
                lblemsg.Text = "Updated Successfully";
                showid = cFun.EncryptValue(showid);
                Response.Redirect("Event_Settings_MasterPage?SHW=" + showid);
            }
            catch (Exception ex)
            {
                pnlmessagebar.Visible = true;
                lblemsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region bindShowName
    private void bindShowName(string showid)
    {
        try
        {
            ShowController shwCtr = new ShowController(fn);
            DataTable dt = shwCtr.getShowByID(showid);
            if (dt.Rows.Count > 0)
            {
                string showName = dt.Rows[0]["SHW_Name"].ToString();
                txtEventName.Text = showName == "0" ? string.Empty : showName;
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region checkCreated (check the record count of tmp_tbSiteSettings table is equal to the record counts of tb_site_settings table)
    private bool checkCreated(string showid)
    {
        bool isCreated = false;
        DataTable dt = fn.GetDatasetByCommand("Select * From tmp_tbSiteSettings", "ds").Tables[0];

        string query = "Select * From tb_site_settings Where ShowID=@SHWID And settings_name In (Select settings_name From tmp_tbSiteSettings)";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dtSettings = fn.GetDatasetByCommand(query, "dsSettings", pList).Tables[0];
        if (dt.Rows.Count == dtSettings.Rows.Count)
        {
            isCreated = true;
        }

        return isCreated;
    }
    #endregion

    #region setBanner (set imgbanner image url with the settings_value of settings_name=_pagebanner from tb_site_settings table)
    protected void setBanner(string showid)
    {
        string strimage = string.Empty;

        string query = "Select settings_value From tb_site_settings Where settings_name='" + _pagebanner + "' And ShowID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        strimage = fn.GetDataByCommand(query, "settings_value", pList);
        if (!string.IsNullOrEmpty(strimage))
        {
            strimage = strimage.Replace("../", "");
            string already_File = Server.MapPath("~/" + strimage);
            if (File.Exists(already_File))
            {
                imgbanner.Visible = true;
                imgbanner.ImageUrl = "../" + strimage;
            }
            else
            {
                imgbanner.Visible = false;
                imgbanner.ImageUrl = "";
            }
        }
        else
        {
            imgbanner.Visible = false;
            imgbanner.ImageUrl = "";
        }
    }
    #endregion

    #region lnksitemaster_Click (redirect to ManageSiteReference.aspx)
    protected void lnksitemaster_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            Response.Redirect("ManageSiteReference.aspx?SHW=" + Request.QueryString["SHW"].ToString() + "&t=mas");
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region lnksitereloginmaster_Click (redirect to ManageSiteReference.aspx)
    protected void lnksitereloginmaster_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            Response.Redirect("ManageSiteReference.aspx?SHW=" + Request.QueryString["SHW"].ToString() + "&t=rel");
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region lnkcssbandle_Click (redirect to ManageSiteReference.aspx)
    protected void lnkcssbandle_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            Response.Redirect("ManageSiteReference.aspx?SHW=" + Request.QueryString["SHW"].ToString() + "&t=css");
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region Back
    protected void Back(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            Response.Redirect("Event_Settings_MasterPage?SHW=" + showid);
        }
        else
        {
            Response.Redirect("Login");
        }
    }
    #endregion

    #region btnSavePreview_Click
    protected void btnSavePreview_Click(object sender, EventArgs e)
    {
        upload();
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            try
            {
                string sql = string.Empty;
                string imgBanner = string.Empty;
                string title = string.Empty;
                string copyright = string.Empty;
                string sitemaster = string.Empty;
                string sitecssbandle = string.Empty;
                string sitereloginmaster = string.Empty;
                string prefix = string.Empty;
                string eventname = string.Empty;

                eventname = cFun.solveSQL(txtEventName.Text.ToString());
                title = txt_title.Text;
                copyright = Server.HtmlEncode(txtCopyright.Text.ToString());
                sitemaster = Server.HtmlEncode(ddlsitemaster.SelectedValue.ToString());
                sitecssbandle = Server.HtmlEncode(ddlcssbandle.SelectedValue.ToString());
                sitereloginmaster = Server.HtmlEncode(ddlsitereloginmaster.SelectedValue.ToString());

                #region Inserting data into tb_Show
                ShowController shwCtr = new ShowController(fn);
                ShowObj sObj = new ShowObj();
                sObj.showid = showid;
                sObj.showname = eventname;
                bool rowInserted = shwCtr.UpdateShowName(sObj);
                #endregion

                upload();

                if (Session["imgBanner"] != null)
                {
                    imgBanner = Session["imgBanner"].ToString();
                    sql = "Update tb_site_settings Set settings_value='" + imgBanner + "' Where settings_name='" + _pagebanner + "' And ShowID='" + showid + "';";
                }

                sql += "Update tb_site_settings Set settings_value='" + title + "' Where settings_name='" + _pagetitle + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + copyright + "' Where settings_name='" + _copyrigtht + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + sitemaster + "' Where settings_name='" + _Sitemaster + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + sitecssbandle + "' Where settings_name='" + _SiteCSSBandle + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + sitereloginmaster + "' Where settings_name='" + _Sitereloginmaster + "' And ShowID='" + showid + "';";
                sql += "Update tb_site_settings Set settings_value='" + prefix + "' Where settings_name='" + _prefix + "' And ShowID='" + showid + "';";
                fn.ExecuteSQL(sql);

                pnlmessagebar.Visible = true;
                lblemsg.Text = "Updated Successfully";

                FlowControler flwObj = new FlowControler(fn);
                string page = "../WebsiteSettings.aspx";
                string route = flwObj.MakeFullURL(page, "0", showid);
                route = route + "&t=" + cFun.EncryptValue(BackendStaticValueClass.isWebSettings);

                System.Drawing.Rectangle resolution =
                System.Windows.Forms.Screen.PrimaryScreen.Bounds;
                int width = resolution.Width;
                int height = resolution.Height;

                if (width <= 1014)
                {
                    width = width - 100;
                }
                else
                {
                    width = 1000;
                }

                RadWindow newWindow = new RadWindow();
                newWindow.NavigateUrl = route;
                newWindow.Height = 800;
                newWindow.Width = width;
                newWindow.VisibleOnPageLoad = true;
                newWindow.KeepInScreenBounds = true;
                newWindow.VisibleOnPageLoad = true;
                newWindow.Visible = true;
                newWindow.VisibleStatusbar = false;
                RadWindowManager1.Windows.Add(newWindow);
            }
            catch (Exception ex)
            {
                pnlmessagebar.Visible = true;
                lblemsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

}