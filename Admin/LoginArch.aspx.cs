﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Utilities;
using System.Configuration;
using System.Data.SqlClient;
using Corpit.Registration;
using Corpit.Logging;
using System.Data;
public partial class Admin_LoginArch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Onclick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            {
                Functionality fn = new Functionality();
                CommonFuns cFun = new CommonFuns();
                Boolean isvalid = isValid();
                if (isvalid)
                {
                    string username = cFun.solveSQL(txtUsername.Text.ToString().Trim());
                    string password = cFun.solveSQL(txtPassword.Text.ToString().Trim());


                    LoginControler loginCtr = new LoginControler(fn);
                    DataTable dt = loginCtr.authenticateLogin(username, password);
                    if (dt.Rows.Count > 0)
                    {
                        string roleid = dt.Rows[0]["admin_roleid"].ToString();
                        string sesUser = dt.Rows[0]["admin_username"].ToString();
                        string sesUserID = dt.Rows[0]["admin_id"].ToString();
                        Session["roleid"] = roleid;
                        Session["username"] = sesUser;
                        Session["userid"] = sesUserID;

                        lblpasswerror.Visible = false;

                        LoginLog lg = new LoginLog(fn);
                        string userip = GetUserIP();
                        lg.loginid = sesUser;
                        lg.loginip = userip;
                        lg.logintype = LoginLogType.adminLogin;
                        lg.saveLoginLog();
                        //if (roleid == "1")
                        //{
                        Response.Redirect("ArchRegMasterList.aspx");
                        //}
                        //else
                        //{
                        //Response.Redirect("MasterRegistrationList_Indiv.aspx");
                        //Response.Redirect("Dashboard");
                        // }
                    }
                    else
                    {
                        lblpasswerror.Text = "Invalid username or password";
                        lblpasswerror.Visible = true;
                    }
                }
            }
        }
    }
    protected Boolean isValid()
    {
        Boolean isvalid = true;

        if (txtPassword.Text.ToString().Trim() == "" || txtPassword.Text.ToString().Trim() == string.Empty)
        {
            lblpasswerror.Text = "Please insert your password";
            lblpasswerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblpasswerror.Visible = false;
        }
        if (txtUsername.Text.ToString().Trim() == "" || txtUsername.Text.ToString().Trim() == string.Empty)
        {
            lblpasswerror.Text = "Please insert your username";
            lblpasswerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblpasswerror.Visible = false;
        }
        return isvalid;
    }

    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
}