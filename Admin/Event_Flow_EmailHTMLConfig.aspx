﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Flow_EmailHTMLConfig.aspx.cs" Inherits="Event_Flow_EmailHTMLConfig" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>
    <div class="centercontent">
        <asp:Panel runat="server" Visible="false">
            <asp:TextBox runat="server" ID="txtFlowKey"></asp:TextBox>

            <asp:TextBox runat="server" ID="txtTemplateID"></asp:TextBox> 

        </asp:Panel> 
        <asp:Panel ID="PanelDisplay" runat="server">
           
            <asp:Panel runat="server" ID="panelsitetemplate" Visible="true">
                <%--<asp:UpdatePanel runat="server">
                    <ContentTemplate>--%>

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-horizontal">
                                    <div class="box-body">
                                         <div class="form-group">
                                            <label class="col-md-2 control-label">Email Type</label>
                                            <div class="col-md-6">
                                                <asp:DropDownList runat="server" ID="ddlEmailType"  CssClass="form-control"  OnSelectedIndexChanged="ddltempdtype_SelectedIndexChanged" AutoPostBack="true" >
                                                    <asp:ListItem Text="Please Select" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Invoice" Value="INV"></asp:ListItem>
                                                    <asp:ListItem Text="Receipt" Value="RECEIPT"></asp:ListItem>
                                                    <asp:ListItem Text="Visa" Value="VISA"></asp:ListItem>
                                                    <asp:ListItem Text="E-Badge" Value="BADGE"></asp:ListItem>
                                                    <asp:ListItem Text="Confirmation Letter" Value="Confirmation"></asp:ListItem>
                                                    <asp:ListItem Text="Group Member Confirmation Letter" Value="GMemberConfirmation"></asp:ListItem>
                                                </asp:DropDownList>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Email Description</label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="txtEmailDesc" runat="server" CssClass="form-control" Visible="true"></asp:TextBox>
                                                <asp:TextBox ID="txtPortalRefID" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Email Data Source</label>
                                            <div class="col-md-6">
                                                <asp:DropDownList runat="server" ID="ddltempdatasource" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddltempdatasource_SelectedIndexChanged"></asp:DropDownList>
                                                <span class="text-bold">Filter Key:   
                                        <asp:Label runat="server" ID="lbltblfk" CssClass="text-danger"></asp:Label>
                                                    <br />
                                                </span>
                                                <span class="text-bold">Email Address Column:    
                                        <asp:Label runat="server" ID="lbltblcec" CssClass="text-danger"></asp:Label>
                                                </span>
                                            </div>
                                        </div>
                                           
                                       
                                         
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Email Body</label>
                                            <div class="col-md-10">
                                                <CKEditor:CKEditorControl ID="txtvalue" runat="server" Height="457px" Width="98%" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Button runat="server" ID="btnsave" Text="Save" OnClick="btnsave_Click" CssClass="btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            
                            <div class="col-md-3">
                                <div>
                                    <ul class="hornav">
                                        <li>Edit</li>
                                    </ul>
                                     <asp:LinkButton runat="server" ID="linkbtndefaultbanner" OnClick="linkbtndefaultbanner_Click" Text="Default Banner"></asp:LinkButton>
                                      <br />
                                       <asp:LinkButton runat="server" ID="btnEncryShow" OnClick="lbtntemp_Click" Text="Encryted ShowID" CommandArgument="EncryptedShowID"></asp:LinkButton><br />
                                      <asp:LinkButton runat="server" ID="btnEncryRegno" OnClick="lbtntemp_Click" Text="Encryted Regno" CommandArgument="EncryptedRegno"></asp:LinkButton><br />
                                      <asp:LinkButton runat="server" ID="btnBarCode" OnClick="lbtntemp_Click" Text="Barcode URL" CommandArgument="BarcodeURL"></asp:LinkButton><br />
                                      <asp:LinkButton runat="server" ID="LinkButton2" OnClick="lbtntemp_Click" Text="Group QRCode URL" CommandArgument="QRcodeGroupURL"></asp:LinkButton><br />
                                      <asp:LinkButton runat="server" ID="LinkButton3" OnClick="lbtntemp_Click" Text="Delegate QRCode URL" CommandArgument="QRcodeDelegateURL"></asp:LinkButton><br />

                                      <asp:LinkButton runat="server" ID="LinkButton1" OnClick="lbtntemp_Click" Text="Acknowledgement URL" CommandArgument="AcknowledgeURL"></asp:LinkButton><br />
                                     <asp:LinkButton runat="server" ID="LinkButton5" OnClick="lbtntemp_Click" Text="Invoice URL" CommandArgument="InvoiceURL"></asp:LinkButton><br />
                                      <asp:LinkButton runat="server" ID="LinkButton6" OnClick="lbtntemp_Click" Text="Receipt URL" CommandArgument="ReceiptURL"></asp:LinkButton><br />
                                    <asp:LinkButton runat="server" ID="LinkButton7" OnClick="lbtntemp_Click" Text="Visa URL" CommandArgument="VisaURL"></asp:LinkButton><br />
                                    <asp:LinkButton runat="server" ID="LinkButton4" OnClick="lbtntemp_Click" Text="Delegate List" CommandArgument="DelegateList"></asp:LinkButton><br />
                                     <asp:LinkButton runat="server" ID="LinkButton8" OnClick="lbtntemp_Click" Text="Conference List" CommandArgument="ConferenceList"></asp:LinkButton><br />
                                        <asp:LinkButton runat="server" ID="LinkButton9" OnClick="lbtntemp_Click" Text="TodayDate" CommandArgument="TodayDate"></asp:LinkButton><br />


                                    <asp:Repeater runat="server" ID="repeater1" DataSourceID="tempdatasource">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" Text='<%# Eval("Column_name") %>' CommandArgument='<%# Eval("Column_name") %>'
                                                ID="lbtntemp" OnClick="lbtntemp_Click" CausesValidation="false"></asp:LinkButton>
                                            <br />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
<%--

                    </ContentTemplate>
                </asp:UpdatePanel>--%>


                <asp:Button runat="server" ID="btnadd" Text="Add" OnClick="btnadd_Click" Visible="false" />&nbsp;&nbsp;
                    <asp:Button runat="server" ID="btnpreview" Text="Preview" OnClick="btnpreview_Click" CssClass="btn btn-primary" Visible="false" />
            </asp:Panel>

        </asp:Panel>
        <%-- </telerik:RadAjaxPanel>--%>
        <%-- </div>
        </div>--%>
        <asp:Panel runat="server" ID="panelpreview" Visible="false">
            <asp:Label runat="server" ID="lblpreview"></asp:Label>
            <br />
            <asp:Button runat="server" ID="btnback" Text="Back" OnClick="btnback_Click" />
        </asp:Panel>
    </div>

    <asp:SqlDataSource ID="tempdatasource" runat="server" ConnectionString="<%$ ConnectionStrings:CMSConnString %>"
        SelectCommand="select Column_name from Information_schema.columns where Table_name =@tablename">
        <SelectParameters>
            <asp:Parameter Name="tablename" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

