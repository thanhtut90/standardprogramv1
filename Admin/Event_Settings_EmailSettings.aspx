﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Settings_EmailSettings.aspx.cs" Inherits="Admin_Event_Settings_EmailSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="box-title">2-4. Manage Email Settings</h1>

    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="Event" class="subcontent">
                <asp:Panel ID="pnlmessagebar" runat="server" CssClass="notibar announcement" Visible="false">
                    <p class="txtgreen">
                        <asp:Label ID="lblemsg" runat="server" Text="Label"></asp:Label>
                    </p>
                </asp:Panel>

                <div class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group" style="display:none;">
                            <label class="col-md-2 control-label">Show Name</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtEventName" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtPortalRefID" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">SMTP Username</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtsmtpuser" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">SMTP Password</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtsmtppswd" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">SMTP Client</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtsmtpclient" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Use Default Credentials</label>
                            <div class="col-md-6">
                                <asp:DropDownList runat="server" ID="ddludc" CssClass="form-control">
                                               <asp:ListItem Text="Yes" Value="TRUE"></asp:ListItem>
                        <asp:ListItem Text="No" Value="FALSE"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Enable Ssl</label>
                            <div class="col-md-6">
                                <asp:DropDownList runat="server" ID="ddlenablessl" CssClass="form-control">
                                               <asp:ListItem Text="Yes" Value="TRUE"></asp:ListItem>
                        <asp:ListItem Text="No" Value="FALSE"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">From Email Address</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtfea" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">From Email Description</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtfed" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2 control-label"></div>
                            <div class="col-md-6">
                                <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back" CssClass="btn btn-primary" />
                                &nbsp;
                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="SaveForm" CssClass="btn btn-primary" />
                                 &nbsp;
                                <asp:Button ID="btnSetupEmail" runat="server" Text="Set up Email" OnClick="btnSetupEmail_Onclick" CssClass="btn btn-primary" Visible="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

