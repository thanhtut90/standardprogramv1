﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Admin_ManageCategory : System.Web.UI.Page
{
    Functionality fn = new Functionality();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                PanelKeyDetail.Visible = false;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From ref_reg_Category table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            try
            {
                string showid = Request.QueryString["SHW"].ToString();

                string query = "Select * From ref_reg_Category Where ShowID=@SHWID Order By reg_order";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);

                DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

                if (dt.Rows.Count > 0)
                {
                    GKeyMaster.DataSource = dt;
                }
                else
                {
                    lbls.Text = "There is no record.";
                }
            }
            catch (Exception)
            {
                lbls.Text = "There is no record.";
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            string categoryid = "";

            foreach (GridDataItem item in GKeyMaster.SelectedItems)
            {
                categoryid = item["reg_CategoryId"].Text;
            }
            if (e.CommandName == "Delete")
            {
                GridDataItem item = (GridDataItem)e.Item;

                string curID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["reg_CategoryId"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["reg_CategoryId"].ToString() : "";
                if (!string.IsNullOrEmpty(curID))
                {
                    int isDeleted = fn.ExecuteSQL(string.Format("Delete From ref_reg_Category Where reg_CategoryId='{0}' And ShowID='" + showid + "'", curID));
                    if (isDeleted > 0)
                    {
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already deleted.');", true);
                    }
                    else
                    {
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Deleting error.');", true);
                    }

                    ResetControls();
                    lbls.Text = "";
                    PanelKeyDetail.Visible = false;
                    btnAdd.Visible = false;
                    btnUpdateRecord.Visible = false;
                }
            }
            else
            {
                if (categoryid != "" && categoryid != null)
                {
                    btnUpdateRecord.Visible = true;
                    btnAdd.Visible = false;
                    PanelKeyDetail.Visible = true;

                    loadvalue(categoryid, showid);
                }
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region btnAddNew_Click (Load when click Add New button)
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        ResetControls();
        lbls.Text = "";
        btnUpdateRecord.Visible = false;
        btnAdd.Visible = true;
        PanelKeyDetail.Visible = true;
    }
    #endregion

    #region btnAdd_Click (Insert the respective data into ref_reg_Category table)
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            if (Page.IsValid)
            {
                lbls.Text = "";
                CommonFuns cFun = new CommonFuns();
                RunNumber run = new RunNumber(fn);
                string catID = run.GetRunNUmber("REFCAT_KEY");
                string name = cFun.solveSQL(txtName.Text.Trim());
                string sortorder = cFun.solveSQL(txtOrder.Text.Trim());

                try
                {
                    string query = "Select * From ref_reg_Category Where reg_order=" + sortorder + " And reg_CategoryName='" + name + "' And ShowID=@SHWID";
                    List<SqlParameter> pList = new List<SqlParameter>();
                    SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    spar.Value = showid;
                    pList.Add(spar);

                    DataTable dtmaster = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

                    if (dtmaster.Rows.Count > 0)
                    {
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This category already exist.');", true);
                        return;
                    }
                    else
                    {
                        string queryInsert = string.Format("Insert Into ref_reg_Category (reg_CategoryId, reg_CategoryName, reg_order, ShowID)"
                            + " Values ({0}, '{1}', {2}, '{3}')"
                            , catID, name, sortorder, showid);

                        int rowInserted = fn.ExecuteSQL(queryInsert);
                        if (rowInserted == 1)
                        {
                            GKeyMaster.Rebind();
                            btnUpdateRecord.Visible = false;
                            btnAdd.Visible = false;
                            PanelKeyDetail.Visible = false;
                            GKeyMaster.Rebind();
                            ResetControls();
                            lbls.Text = "";
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                        }
                        else
                        {
                            GKeyMaster.Rebind();
                            btnUpdateRecord.Visible = false;
                            btnAdd.Visible = false;
                            PanelKeyDetail.Visible = false;
                            GKeyMaster.Rebind();
                            ResetControls();
                            lbls.Text = "";
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Saving error, try again.');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    lbls.Text = ex.Message;
                }
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region loadvalue (Load RadGrid selected row data from ref_reg_Category table to bind the respective controls while edit)
    private void loadvalue(string categoryid, string showid)
    {

        string query = "Select * From ref_reg_Category Where reg_CategoryId='" + categoryid + "' And ShowID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        if(dt.Rows.Count > 0)
        {
            hfID.Value = categoryid;
            txtName.Text = dt.Rows[0]["reg_CategoryName"].ToString();
            txtOrder.Text = dt.Rows[0]["reg_order"].ToString();
        }
    }
    #endregion

    #region btnUpdateRecord_Click (Update the respective data into "ref_reg_Category" table)
    protected void btnUpdateRecord_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            if (Page.IsValid)
            {
                lbls.Text = "";
                CommonFuns cFun = new CommonFuns();
                string categoryid = hfID.Value;
                string name = cFun.solveSQL(txtName.Text.Trim());
                string sortorder = cFun.solveSQL(txtOrder.Text.Trim());

                try
                {
                    string query = "Select * From ref_reg_Category Where reg_order=" + sortorder + " And reg_CategoryName='" + name
                        + "' And reg_CategoryId!=" + categoryid + " And ShowID=@SHWID";
                    List<SqlParameter> pList = new List<SqlParameter>();
                    SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    spar.Value = showid;
                    pList.Add(spar);

                    DataTable dtmaster = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

                    if (dtmaster.Rows.Count > 0)
                    {
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This currency already exist.');", true);
                        return;
                    }
                    else
                    {
                        string queryUpdate = string.Format("Update ref_reg_Category Set reg_CategoryName='{0}', reg_order={1}"
                        + " Where reg_CategoryId={2} And ShowID='{3}'", name, sortorder, categoryid, showid);

                        int rowUpdated = fn.ExecuteSQL(queryUpdate);
                        if (rowUpdated == 1)
                        {
                            GKeyMaster.Rebind();
                            btnUpdateRecord.Visible = false;
                            btnAdd.Visible = false;
                            PanelKeyDetail.Visible = false;
                            GKeyMaster.Rebind();
                            ResetControls();
                            lbls.Text = "";
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                        }
                        else
                        {
                            GKeyMaster.Rebind();
                            btnUpdateRecord.Visible = false;
                            btnAdd.Visible = false;
                            PanelKeyDetail.Visible = false;
                            GKeyMaster.Rebind();
                            ResetControls();
                            lbls.Text = "";
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updating error, try again.');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    lbls.Text += ex.Message;
                }
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region ResetControls
    private void ResetControls()
    {
        hfID.Value = "";
        txtName.Text = "";
        txtOrder.Text = "";
    }
    #endregion
}