﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageDepartment.aspx.cs" Inherits="Admin_ManageDepartment" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Department Management</h1>
            <span class="pagedesc">Manage the department.</span>
            
            <ul class="hornav">
                <li class="current"><a href="#index">Index</a></li>
            </ul>
        </div><!--page header -->
        <div id="contentwrapper" class="contentwrapper">
            <div id="index" class="subcontent">
                <h4>Department</h4>

                <label>Institution:</label>
                <asp:DropDownList ID="ddlInstitution" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlInstitution_SelectedIndexChanged"></asp:DropDownList>
                <br />

                <p style="text-align:right;"><a class="ds_next" href="DepartmentAddEdit.aspx?SHW=<%=shwID %>">Add New Item</a></p>
                <asp:Label ID="lblShowID" runat="server" Visible="false"></asp:Label>
                <asp:Panel ID="pnllist" runat="server" Visible="false">
                    <h3>Departments</h3>
                    <asp:GridView ID="gvList" runat="server" DataKeyNames="ID" AutoGenerateColumns="false"
                    CssClass="table" OnRowCommand="gvcommand">
                    <HeaderStyle CssClass="theadstyle" />
                        <Columns>
                            <asp:TemplateField HeaderText="Serial No.">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Id">
                                <ItemTemplate>
                                    <%# Eval("ID")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Department") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Institution Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblInstitution" runat="server" Text='<%# Eval("Institution") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Order">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrder" runat="server" Text='<%# Eval("Sorting") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <a href='DepartmentAddEdit.aspx?SHW=<%=shwID%>&dept=<%# Eval("ID") %>'><img src="images/default/edit-icon.png" alt="edit" /></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="images/default/searchcancel.png" CommandName="deleterow" 
                                        CommandArgument='<%#Eval("ID") %>' OnClientClick="return confirm ('Are you sure you want to delete this record?')" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div style="color:Red">
                        <asp:Label ID="lblMsg" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
            </div><!--page index -->
        </div><!--contentwrapper -->
    </div><!--centercontent -->
</asp:Content>