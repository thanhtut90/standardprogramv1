﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Corpit.Utilities;
using Corpit.Site.Utilities;
using System.Data.SqlClient;

public partial class Admin_Event_Conference_Promo : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    public static int promoType = 6;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["STP"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                string catID = string.Empty;
                if (Request.Params["CAT"] != null)
                {
                    catID = cFun.DecryptValue(Request.QueryString["CAT"].ToString());
                    //btnAddNewConfItem.Visible = false;
                }

                bindPromo(showid, flowid, catID);

                string url = string.Empty;

                if (Request.Params["a"] == null)
                {
                    url = string.Format("Event_Conference_Controler?FLW={0}&SHW={1}&STP={2}", Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString());
                }
                else
                {
                    string actionType = Request.QueryString["a"].ToString();
                    url = string.Format("Event_Conference_Controler?FLW={0}&SHW={1}&STP={2}&a={3}", Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString(), actionType);
                }

                hplBack.NavigateUrl = url;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region bindPromo (bind data from tb_PromoCode table to GridView(gvList))
    protected void bindPromo(string showid, string flowid, string catID)
    {
        try
        {
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            SqlParameter spar1 = new SqlParameter("FlwID", SqlDbType.NVarChar);
            spar.Value = showid;
            spar1.Value = flowid;
            pList.Add(spar);
            pList.Add(spar1);

            string query = string.Empty;
            if (!string.IsNullOrEmpty(catID))
            {
                query = "Select * From tb_PromoCode Where Promo_Type1=" + promoType + " And ShowID=@SHWID And FlwID=@FlwID And Promo_CategoryID=@Promo_CategoryID";
                SqlParameter spar2 = new SqlParameter("Promo_CategoryID", SqlDbType.NVarChar);
                pList.Add(spar2);
                spar2.Value = catID;
            }
            else
            {
                query = "Select * From tb_PromoCode Where Promo_Type1=" + promoType + " And ShowID=@SHWID And FlwID=@FlwID";
            }

            DataSet ds = new DataSet();
            ds = fn.GetDatasetByCommand(query, "Ds", pList);
            if (ds.Tables[0].Rows.Count != 0)
            {
                lblPromoHeader.Visible = true;
                gvList.DataSource = ds;
                gvList.DataBind();
            }
            else
            {
                lblPromoHeader.Visible = false;
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region getUrl
    public string getUrl(string promoid, string catid)
    {
        string routeUrl = "#";

        if (Request.Params["SHW"] != null)
        {
            routeUrl = "Promo_List.aspx?promoid=" + promoid;

            string stepid = Request.QueryString["STP"].ToString();
            routeUrl += "&STP=" + stepid;

            routeUrl += "&CAT=" + cFun.EncryptValue(catid);

            string showid = Request.QueryString["SHW"].ToString();
            routeUrl += "&SHW=" + showid;

            if (Request.Params["FLW"] != null)
            {
                string flowid = Request.QueryString["FLW"].ToString();
                routeUrl += "&FLW=" + flowid;
            }

            if (Request.Params["a"] != null)
            {
                string actionType = Request.QueryString["a"].ToString();
                routeUrl += "&a=" + actionType;
            }
        }

        return routeUrl;
    }
    #endregion

    #region getConfItemUrlWithCatID
    public string getConfItemUrlWithCatID(string catid)
    {
        string routeUrl = "#";

        if (Request.Params["SHW"] != null)
        {
            routeUrl = "Event_Conference_Items.aspx?CAT=" + cFun.EncryptValue(catid);

            string stepid = Request.QueryString["STP"].ToString();
            routeUrl += "&STP=" + stepid;

            string showid = Request.QueryString["SHW"].ToString();
            routeUrl += "&SHW=" + showid;

            if (Request.Params["FLW"] != null)
            {
                string flowid = Request.QueryString["FLW"].ToString();
                routeUrl += "&FLW=" + flowid;
            }
        }

        return routeUrl;
    }
    #endregion

    #region gvList_RowCommand (to show modal popup)
    protected void gvList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "LaunchModal")
        {
            string promocodeID = e.CommandArgument.ToString();
            hfPromoCodeID.Value = promocodeID;

            txtNoofCode.Text = "";
            txtPTitle.Text = "";
            ddlPUsage.SelectedIndex = 0;
            txtPNoofCode.Text = "";
            txtPMax.Text = "";
            txtPPrefix.Text = "";

            divPromoCodeInsert.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }
    }
    #endregion

    #region btnAddNewConfItem_Click
    protected void btnAddNewConfItem_Click(object sender, EventArgs e)
    {
        divPromoCodeInsert.Visible = true;
    }
    #endregion

    #region ddlPUsage_SelectedIndexChanged
    protected void ddlPUsage_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPUsage.SelectedValue == "2")
        {
            trMax.Visible = true;
        }
        else
        {
            trMax.Visible = false;
        }
    }
    #endregion

    #region SavePromoCode (create new record into ref_reg_Category table according to Reference Title & insert data into tb_PromoCode and tb_PromoList tables)
    protected void SavePromoCode(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

            string title = string.Empty;
            string promotype = "0";
            string value = "0";
            int pselect = 0;
            string timeused = string.Empty;
            string itemid = string.Empty;

            int promo_type1 = 0;

            int noofcode = cFun.ParseInt(txtPNoofCode.Text.Trim());// 0;
            string prefix = string.Empty;
            int regid = 0;

            int usagepertran = 0;

            title = cFun.solveSQL(txtPTitle.Text);

            try
            {
                int maxusage = 1;

                //*Conference visibility using promo code accoding to Category
                promo_type1 = promoType;
                promotype = "0";
                pselect = 0;
                value = "0";

                //***Create New Category
                RunNumber run = new RunNumber(fn);
                string catID = run.GetRunNUmber("REFCAT_KEY");

                int latestsortOrder = 0;
                //int.TryParse(fn.GetDataByCommand("Select reg_order From ref_reg_Category Where ShowID='" + showid + "' Order By reg_order Desc", "reg_order"), out latestsortOrder);

                string sqlCatInsert = string.Format("Insert Into ref_reg_Category (reg_CategoryId, reg_CategoryName, reg_order, ShowID,CType,RefValue)"
                            + " Values ({0}, '{1}', {2}, '{3}','P','{4}')"
                            , catID, title, latestsortOrder, showid, flowid);
                int isSuccess = fn.ExecuteSQL(sqlCatInsert);
                if (isSuccess > 0)
                {
                    //*
                    CategoryClass catClass = new CategoryClass();
                    string configType = catClass.flowcat_ByBusinessLogic.ToString();
                    //updateSiteFlowMaster(configType, showid, flowid);
                    //*

                    if (noofcode > 0)
                    {
                        //*
                        configType = catClass.flowcat_ByPromoCode.ToString();
                        //updateSiteFlowMaster(configType, showid, flowid);
                        //*

                        noofcode = Convert.ToInt32(txtPNoofCode.Text);
                        prefix = cFun.solveSQL(txtPPrefix.Text);

                        regid = Convert.ToInt32(catID);

                        timeused = ddlPUsage.SelectedValue.ToString();

                        if (ddlPUsage.SelectedValue == "2")
                        {
                            maxusage = Convert.ToInt32(txtPMax.Text);
                        }

                        string sql = "Insert Into tb_PromoCode "
                            + "(Promo_Type1,Promo_Type,Promo_Value,Promo_Added,Promo_Name,Promo_Prefix,Promo_Count"
                            + ",Promo_Timeused,Promo_PriceSelect,con_GroupId,Promo_isitem,Promo_ConfItemId,Promo_CategoryID,Promo_isEarlyBird,ShowID,FlwID)"
                            + " Values (" + promo_type1 + ", " + promotype + ", " + value + ", getdate(), '" + title + "', '" + prefix + "', '" + noofcode
                            + "', " + timeused + ", " + pselect + ", " + "0,0,0," + regid + ",0,'" + showid + "','" + flowid + "')";
                        fn.ExecuteSQL(sql);

                        string query = "Select Top(1)Promo_Id From tb_PromoCode Where ShowID=@SHWID Order By Promo_Id Desc";
                        List<SqlParameter> pList = new List<SqlParameter>();
                        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                        spar.Value = showid;
                        pList.Add(spar);

                        string promocodeid = fn.GetDataByCommand(query, "Promo_Id", pList);

                        divPromoCode.Visible = true;
                        bindPromo(showid, flowid, string.Empty);

                        SavePCode(noofcode, prefix, promocodeid, title, maxusage, usagepertran, showid);
                    }
                    ResetControls();
                }
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region updateSiteFlowMaster
    private void updateSiteFlowMaster(string catConfigType, string showid, string flowid)
    {
        try
        {
            string updateConfigType = "Update tb_site_flow_master Set FLW_Cat_Config_Type=@FLW_Cat_Config_Type Where ShowID=@SHWID And Flw_id=@Flw_id";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Flw_id", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("FLW_Cat_Config_Type", SqlDbType.Int);
            spar.Value = showid;
            spar2.Value = flowid;
            spar3.Value = !string.IsNullOrEmpty(catConfigType) ? Convert.ToInt32(catConfigType) : 0;
            pList.Add(spar);
            pList.Add(spar2);
            pList.Add(spar3);
            fn.ExecuteSQLWithParameters(updateConfigType, pList);
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region ResetControls (clear data from controls)
    private void ResetControls()
    {
        hfPromoCodeID.Value = "";
        txtNoofCode.Text = "";
        txtPTitle.Text = "";
        ddlPUsage.SelectedIndex = 0;
        txtPNoofCode.Text = "";
        txtPMax.Text = "";
        txtPPrefix.Text = "";

        divPromoCodeInsert.Visible = false;
    }
    #endregion

    #region SavePCode (insert data into tb_PromoList table)
    protected void SavePCode(int count, string prefix, string promocodeid, string promotitle, int maxusage, int usagepertran, string showid)
    {
        DataTable NewDt = new DataTable();
        NewDt.Columns.Add("No", typeof(string));
        NewDt.Columns.Add("Promo Code", typeof(string));
        NewDt.Columns.Add("Promo Title", typeof(string));

        for (int x = 0; x < count; x++)
        {
            string pcode = string.Empty;
            pcode = GeneratePromoCode(prefix);

            try
            {
                string query = "Select Count(*) as ctcount From tb_PromoList Where PromoCode='" + pcode + "' And ShowID=@SHWID";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);

                int countcode = Convert.ToInt32(fn.GetDataByCommand(query, "ctcount", pList));
                if (countcode > 0)
                {
                    pcode = GeneratePromoCode(prefix);
                }
                string sqlinsert = "Insert Into tb_PromoList (PromocodeID,PromoCode,DateCreated, MaxUsage,NoOfUsagePerTransaction,ShowID) Values ("
                    + promocodeid + ",'" + pcode + "',getdate(),'" + maxusage + "'," + usagepertran + ",'" + showid + "')";
                fn.ExecuteSQL(sqlinsert);
            }
            catch (Exception ex)
            { }

            DataRow newdr = NewDt.NewRow();
            newdr[0] = (x + 1).ToString();
            newdr[1] = pcode;
            newdr[2] = promotitle;
            NewDt.Rows.Add(newdr);
            System.Threading.Thread.Sleep(50);
        }

        //ExportToExcel.DataTable3Excel(NewDt, "Promo_Code_" + txtPTitle.Text);
    }
    #endregion

    #region btnClose_Click (to close modal popup [Close Button Click])
    protected void btnClose_Click(object sender, EventArgs e)
    {
        ResetControls();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePop", "closeModal();", true);
    }
    #endregion

    #region btnCreate_Click (update Promo_Count of tb_PromoList & create additional Reference Cod, modal popup [Create Button Click])
    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

            string catID = string.Empty;
            if (Request.Params["CAT"] != null)
            {
                catID = cFun.DecryptValue(Request.QueryString["CAT"].ToString());
            }

            string promocodeID = hfPromoCodeID.Value;
            if (!string.IsNullOrEmpty(promocodeID))
            {
                try
                {
                    string query = "Select * From tb_PromoCode Where Promo_Id=" + promocodeID + " And ShowID=@SHWID And FlwID=@FlwID";
                    List<SqlParameter> pList = new List<SqlParameter>();
                    SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    SqlParameter spar1 = new SqlParameter("FlwID", SqlDbType.NVarChar);
                    spar.Value = showid;
                    spar1.Value = flowid;
                    pList.Add(spar);
                    pList.Add(spar1);

                    DataTable dtPromoCode = fn.GetDatasetByCommand(query, "dsPromoCode", pList).Tables[0];
                    if (dtPromoCode.Rows.Count > 0)
                    {
                        int noofcode = Convert.ToInt32(txtNoofCode.Text);
                        string prefix = dtPromoCode.Rows[0]["Promo_Prefix"].ToString();
                        string title = dtPromoCode.Rows[0]["Promo_Name"].ToString();

                        string queryPromoList = "Select * From tb_PromoList Where PromocodeID=" + promocodeID + " And ShowID=@SHWID";

                        DataTable dtPromo = fn.GetDatasetByCommand(queryPromoList, "dsPromo", pList).Tables[0];

                        int maxusage = dtPromo.Rows[0]["MaxUsage"] != null ? Convert.ToInt32(dtPromo.Rows[0]["MaxUsage"].ToString()) : 1;
                        int usagepertran = 0;

                        string updatePromoCount = "Update tb_PromoCode Set Promo_Count=Promo_Count+" + noofcode + " Where Promo_Id=" + promocodeID;
                        fn.ExecuteSQL(updatePromoCount);

                        SavePCode(noofcode, prefix, promocodeID, title, maxusage, usagepertran, showid);

                        bindPromo(showid, flowid, catID);

                        ResetControls();
                    }
                }
                catch (Exception ex)
                { }
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region GeneratePromoCode
    protected string GeneratePromoCode(string prefix)
    {
        string ncode = fn.CreateRandomCode(8);
        string pcode = prefix + ncode;

        return pcode;
    }
    #endregion
}