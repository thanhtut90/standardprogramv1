﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using System.Text;
using Corpit.Site.Utilities;
using Corpit.Payment;
using System.IO;
using ClosedXML.Excel;
using Corpit.Site.Email;
using System.Text.RegularExpressions;
using Corpit.Logging;
using System.Globalization;

public partial class Admin_ConferenceFinancial_Summary : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    QuesFunctionality qfn = new QuesFunctionality();
    CommonFuns cFun = new CommonFuns();

    protected int count = 0;
    protected string htmltb;
    static StringBuilder StrBuilder = new StringBuilder();
    string showID = string.Empty;
 
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (Session["userid"] != null)
                {
                    string userID = Session["userid"].ToString();

                    if (!string.IsNullOrEmpty(userID))
                    {
                        
                        lblUser.Text = userID;
                        getShowID(userID);
                     
                        GKeyMaster.ExportSettings.ExportOnlyData = true;
                        GKeyMaster.ExportSettings.IgnorePaging = true;
                        GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                        GKeyMaster.ExportSettings.FileName = "ConferenceSummary_" + DateTime.Now.ToString("ddMMyyyy");
   
                       // else
                         //   showID = getShowID(userID);
                    }
                    else
                    {
                        Response.Redirect("Event_Config");
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }

    

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_RegDelegate and tb_Invoice tables for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            getShowID(Session["userid"].ToString());
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID))
        {
            try
            {
                DataTable dt = getRegSummary();
                DataView dv = new DataView();
                dv = dt.DefaultView;
                dv.Sort = "SHW_ID ASC";
                dt = dv.ToTable();
                GKeyMaster.DataSource = dt;
            }
            catch (Exception ex)
            {
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    

    #region Page_PreRender
    protected void Page_PreRender(object o, EventArgs e)
    {
        try
        {       
           getShowID(Session["userid"].ToString());   
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

   

    /// <summary> 
    /// Item databound for the radgrid, set the header text here based on the form management ***Item[UniqueName]
    /// </summary>
   

    
    protected void RadGd1_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem)e.Item;
            string edit_RegGroupID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
            string edit_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
            if (!string.IsNullOrEmpty(edit_Regno))
            {
                ImageButton downloadlink = (ImageButton)item["Download"].Controls[0];
                downloadlink.Attributes.Add
               ("onclick", "window.open('../AcknowledgeLetter.aspx?DID=" + cFun.EncryptValue(edit_Regno) +
               "&SHW=" + cFun.EncryptValue(showID) + "'); return false");
            }
        }
    }
     

    protected void lnkExcel_Clicked(object sender, EventArgs e)
    {
        try
        {
            GKeyMaster.AllowPaging = true;
            GKeyMaster.Rebind();
            GKeyMaster.ExportSettings.ExportOnlyData = true;
            GKeyMaster.ExportSettings.IgnorePaging = true;
            GKeyMaster.ExportSettings.OpenInNewWindow = true;
            GKeyMaster.MasterTableView.ExportToExcel();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }
    }

    public void ExportExcel()
    {
        try
        {
            string name = "RegSummary";
            StringBuilder StrBuilder = new StringBuilder();
            htmltb = string.Empty;
            StrBuilder.Append("<table cellpadding='0' cellspacing='0' border='1' class='Messages' id='idTbl' runat='server'>");
            StrBuilder.Append(htmltb.ToString());
            StrBuilder.Append("</table>");
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/msexcel";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode;
            HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            HttpContext.Current.Response.AddHeader("Content-Disposition", "filename=Delegate-Detailed-Report-" + name + ".xls");
            HttpContext.Current.Response.Write(StrBuilder);
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush();
        }
        catch (System.Threading.ThreadAbortException exf)
        {

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);

        }
    }
    
    #region Search
    
    public DataTable getRegSummary()
    {
        DataTable dt = new DataTable();

        try
        {
            string query = "Select * From vw_OrderSummary Where SHW_ID=@SHWID";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            //spar.Value = "ZMN363";
            pList.Add(spar);
            dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    #endregion
}
