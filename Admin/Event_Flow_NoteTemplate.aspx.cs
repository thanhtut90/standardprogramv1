﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Event_Flow_NoteTemplate : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null
                && Request.Params["STPINDEX"] != null && Request.Params["STP"] != null)
            {
                string showid= cFun.DecryptValue(Request.Params["SHW"].ToString());
                txtShowID.Text = showid;
                string flowid = cFun.DecryptValue(Request.Params["FLW"].ToString());
                txtFlowID.Text = flowid;

                string stepIndex = cFun.DecryptValue(Request.Params["STPINDEX"].ToString());
                txtStepIndex.Text = stepIndex;
                string step = cFun.DecryptValue(Request.Params["STP"].ToString());
                txtStep.Text = step;

                bindNoteType();
                bindStepNotes();
            }
        }
    }

    private void bindNoteType()
    {
        Items.Clear();
        ddlNoteType.Items.Add(new ListItem("Please Select", "0"));
        ddlNoteType.Items.Add(new ListItem("HDR1", "HDR1"));
        ddlNoteType.Items.Add(new ListItem("HDR2", "HDR2"));
        ddlNoteType.Items.Add(new ListItem("HDR3", "HDR3"));
        ddlNoteType.Items.Add(new ListItem("FTR1", "FTR1"));
        ddlNoteType.Items.Add(new ListItem("FTR2", "FTR2"));
        ddlNoteType.Items.Add(new ListItem("FTRCHK", "FTRCHK"));
        ddlNoteType.Items.Add(new ListItem("FTRCHK1", "FTRCHK1"));
    }
    private void bindStepNotes()
    {
        string showid= txtShowID.Text.Trim();
        string flowid = txtFlowID.Text.Trim();
        string stepindex = txtStepIndex.Text.Trim();
        string step = txtStep.Text.Trim();
        string sql = string.Format("Select * From tb_site_FlowTemplateNote Where note_ShowID='{0}' And note_FlowID='{1}' And note_FlowIndexID='{2}' And note_FlowStep='{3}' Order By note_Type,note_SortOrder"
                                    , showid, flowid, stepindex, step);
        DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
        rptNotes.DataSource = dt;
        rptNotes.DataBind();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string noteID = txtNoteID.Text.Trim();
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null
                   && Request.Params["STPINDEX"] != null && Request.Params["STP"] != null)
        {
            string showid = txtShowID.Text.Trim();
            string flowid = txtFlowID.Text.Trim();
            string stepindex = txtStepIndex.Text.Trim();
            string step = txtStep.Text.Trim();
            string notetype = ddlNoteType.SelectedItem.Value;
            string sortorder = !string.IsNullOrEmpty(txtSortOrder.Text.Trim()) ? txtSortOrder.Text.Trim() : "99";
            string isSkip = chkIsSkip.Checked == true ? "1" : "0";
            string isAutoCheck = chkIsAutoCheck.Checked == true ? "1" : "0";
            string tmpid = txtMsgTmpID.Text.Trim();
            #region Template
            TemplateControler tmpControler = new TemplateControler(fn);
            TemplateObj tmpObj = new TemplateObj();
            tmpObj.tempid = tmpid;
            tmpObj.tempname = "";
            tmpObj.tempvalue = Server.HtmlEncode(txtMsg.Text);
            tmpObj.tempDBSource = "";
            tmpObj.tempsubject = "";
            tmpObj.temptype = 3;
            tmpObj.showid = showid;
            bool rowUpdated1 = false;
            if (!string.IsNullOrEmpty(tmpid) && tmpid != "0")
            {
                rowUpdated1 = tmpControler.UpdateTemplateByID(tmpObj);
            }
            else
            {
                tmpid = "0";
                rowUpdated1 = tmpControler.CreateNewTemplate(tmpObj, ref tmpid);
            }
            #endregion
            if (rowUpdated1)
            {
                int isSuccess = 0;
                if (!string.IsNullOrEmpty(noteID))
                {
                    try
                    {
                        string sqlUpdate = string.Format("Update tb_site_FlowTemplateNote Set note_Type='{0}', note_TemplateID='{1}', note_SortOrder='{2}', note_UpdatedDate=getdate(), note_isSkip='{7}', note_isAutoCheck='{9}'"
                                            + " Where note_ID='{8}' And note_ShowID='{3}' And note_FlowID='{4}' And note_FlowIndexID='{5}' And note_FlowStep='{6}'"
                                            , notetype, tmpid, sortorder, showid, flowid, stepindex, step, isSkip, noteID, isAutoCheck);
                        isSuccess = fn.ExecuteSQL(sqlUpdate);
                    }
                    catch (Exception ex)
                    {
                        bindStepNotes();
                        ResetControls();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Failed.');", true);
                    }
                    bindStepNotes();
                    ResetControls();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Successfully updated.');", true);
                }
                else
                {
                    try
                    {
                        string sqlInsert = string.Format("Insert Into tb_site_FlowTemplateNote (note_Type, note_TemplateID, note_SortOrder, note_ShowID, note_FlowID, note_FlowIndexID, note_FlowStep, note_isSkip, note_isAutoCheck)"
                                            + " Values('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')"
                                            , notetype, tmpid, sortorder, showid, flowid, stepindex, step, isSkip, isAutoCheck);
                        isSuccess = fn.ExecuteSQL(sqlInsert);
                    }
                    catch (Exception ex)
                    {
                        bindStepNotes();
                        ResetControls();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Failed.');", true);
                    }
                    bindStepNotes();
                    ResetControls();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Successfully created.');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Failed.');", true);
            }
        }
    }
    protected void btnGetTemplate_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtMsgTmpID.Text) && !string.IsNullOrWhiteSpace(txtMsgTmpID.Text))
        {
            string sql = "Select * From tb_site_Template Where temp_id='" + txtMsgTmpID.Text.Trim() + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                txtMsg.Text = Server.HtmlDecode(dt.Rows[0]["temp_value"].ToString());
            }
        }
    }
    protected void btnEditNote_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        try
        {
            string noteID = btn.CommandArgument;

            if (!string.IsNullOrEmpty(noteID) && Request.Params["SHW"] != null && Request.Params["FLW"] != null
                && Request.Params["STPINDEX"] != null && Request.Params["STP"] != null)
            {
                string showid = txtShowID.Text.Trim();
                string flowid = txtFlowID.Text.Trim();
                string stepindex = txtStepIndex.Text.Trim();
                string step = txtStep.Text.Trim();

                txtNoteID.Text = noteID.Trim();
                string sql = string.Format("Select * From tb_site_FlowTemplateNote Where note_ShowID='{0}' And note_FlowID='{1}' And note_FlowIndexID='{2}' And note_FlowStep='{3}' And note_ID='{4}'"
                                    , showid, flowid, stepindex, step, noteID);
                DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
                if (dt.Rows.Count > 0)
                {
                    string noteType = dt.Rows[0]["note_Type"].ToString();
                    string templateID = dt.Rows[0]["note_TemplateID"].ToString();
                    txtMsgTmpID.Text = templateID;
                    string sortOrder = dt.Rows[0]["note_SortOrder"].ToString();
                    string templateMsg = fn.GetDataByCommand("Select temp_value From tb_site_template Where temp_id='" + templateID + "'", "temp_value");
                    if(templateMsg != "0")
                    {
                        txtMsg.Text = Server.HtmlDecode(!string.IsNullOrEmpty(templateMsg) ? templateMsg : "");
                    }
                    try
                    {
                        ListItem lst = ddlNoteType.Items.FindByValue(noteType);
                        if(lst != null)
                        {
                            ddlNoteType.ClearSelection();
                            lst.Selected = true;
                        }
                    }
                    catch(Exception ex)
                    { }
                    txtSortOrder.Text = sortOrder;
                    string isSkip = dt.Rows[0]["note_isSkip"].ToString();
                    chkIsSkip.Checked = isSkip == "1" ? true : false;
                    string isAutoCheck = dt.Rows[0]["note_isAutoCheck"].ToString();
                    chkIsAutoCheck.Checked = isAutoCheck == "1" ? true : false;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        try
        {
            string noteID = btn.CommandArgument;

            if (!string.IsNullOrEmpty(noteID) && Request.Params["SHW"] != null && Request.Params["FLW"] != null
                && Request.Params["STPINDEX"] != null && Request.Params["STP"] != null)
            {
                string showid = txtShowID.Text.Trim();
                string flowid = txtFlowID.Text.Trim();
                string stepindex = txtStepIndex.Text.Trim();
                string step = txtStep.Text.Trim();
                string sqlDelete = string.Format("Update tb_site_FlowTemplateNote Set note_DeleteFlag=1 Where note_ShowID='{0}' And note_FlowID='{1}' And note_FlowIndexID='{2}' And note_FlowStep='{3}' And note_ID='{4}'"
                                    , showid, flowid, stepindex, step, noteID);
                fn.ExecuteSQL(sqlDelete);
                bindStepNotes();
                ResetControls();
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Deleted.');", true);
            }
        }
        catch (Exception ex)
        {
            bindStepNotes();
            ResetControls();
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Failed.');", true);
        }
    }
    #region ResetControls (clear data from controls)
    private void ResetControls()
    {
        txtNoteID.Text = "";
        txtMsgTmpID.Text = "";
        ddlNoteType.SelectedIndex = 0;
        txtSortOrder.Text = "";
        txtMsg.Text = "";
        chkIsSkip.Checked = false;
        chkIsAutoCheck.Checked = false;
    }
    #endregion
}