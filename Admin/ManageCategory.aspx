﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageCategory.aspx.cs" Inherits="Admin_ManageCategory" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     
    <h3 class="box-title">Manage Category</h3>

    <br />
    <br />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateRecord">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <asp:Label runat="server" ID="lbls" ForeColor="Red"></asp:Label>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
        <asp:Panel runat="server" ID="PanelKeyList">
            <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="20" Skin="Bootstrap">
                <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true"></Selecting>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" DataKeyNames="reg_CategoryId">
                    <Columns>

                        <telerik:GridButtonColumn ConfirmTextFormatString="Do you want to Delete Category of {0}?" ConfirmTextFields="reg_CategoryId" CommandArgument="reg_CategoryId"
                            ConfirmDialogType="RadWindow" CommandName="Delete" ButtonType="ImageButton" ImageUrl="images/delete.jpg" UniqueName="Delete"
                            HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                        </telerik:GridButtonColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_CategoryId" FilterControlAltText="Filter reg_CategoryId"
                            HeaderText="ID" SortExpression="reg_CategoryId" UniqueName="reg_CategoryId">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_CategoryName" FilterControlAltText="Filter reg_CategoryName"
                            HeaderText="Category Name" SortExpression="reg_CategoryName" UniqueName="reg_CategoryName">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_order" FilterControlAltText="Filter reg_order"
                            HeaderText="Order" SortExpression="reg_order" UniqueName="reg_order">
                        </telerik:GridBoundColumn>

                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </asp:Panel>

        <br />
        <br />
        <div class="mybuttoncss">
            <asp:Button runat="server" Text ="Add New" ID="btnAddNew" OnClick="btnAddNew_Click" CausesValidation="false" CssClass="btn btn-primary" />
        </div>
        <br />
        <br />
        <asp:Panel runat="server" ID="PanelKeyDetail">
            <h4>Detail</h4>

            <div class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Category Name</label>
                        <div class="col-md-6">
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtName" runat="server" CssClass="form-control"></telerik:RadTextBox>
                        </div>
                        <div class="col-md-4">
                            <asp:RequiredFieldValidator ID="rfvName" runat="server" Display="Dynamic"
                                ControlToValidate="txtName" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Order</label>
                        <div class="col-md-6">
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtOrder" runat="server" CssClass="form-control"></telerik:RadTextBox>
                        </div>
                        <div class="col-md-4">
                            <asp:RequiredFieldValidator ID="rfvOrder" runat="server" Display="Dynamic"
                                ControlToValidate="txtOrder" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ID="cvOrder"
                                ControlToValidate="txtOrder" ErrorMessage="* Number Only" ForeColor="Red" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2 control-label"></div>
                        <div class="col-md-6">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" CssClass="btn btn-primary" />
                            <asp:Button ID="btnUpdateRecord" runat="server" Text="Update Record" OnClick="btnUpdateRecord_Click" UseSubmitBehavior="true" CssClass="btn btn-primary"></asp:Button>
                            <asp:HiddenField ID="hfID" runat="server" Value="" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </telerik:RadAjaxPanel>
</asp:Content>

