﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageInstitution.aspx.cs" Inherits="Admin_ManageInstitution" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="row" style="padding-bottom:20px;">
            <div class="col-lg-7 content-header">
                <h1>Manage Institution </h1>
            </div>
        </div><!--page header -->
        <div id="contentwrapper" class="contentwrapper">
            <div id="index" class="subcontent">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h4>Add New Institution</h4>

                        <asp:Label ID="lblOrganisation" runat="server" Text="Organisation" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:DropDownList ID="ddlOrganisation" runat="server" Width="350px" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:CompareValidator ID="vcOrganisation" runat="server" Enabled="false"
                        ControlToValidate="ddlOrganisation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <br />

                        <asp:Label ID="lblInstitutionName" runat="server" Text="Name" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtInstitutionName" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtInstitutionName" runat="server"
                            ErrorMessage="Please insert the institution name" ForeColor="Red"></asp:RequiredFieldValidator>
                        <br />

                        <asp:Label ID="lblAddress1" runat="server" Text="Address 1" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtAddress1" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        <br />

                        <asp:Label ID="lblAddress2" runat="server" Text="Address 2" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtAddress2" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        <br />

                        <asp:Label ID="lblZipCode" runat="server" Text="Zip Code" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtZipCode" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        <br />

                        <asp:Label ID="lblSortOrder" runat="server" Text="Sort Order" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtSortorder" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbSortorder" runat="server" TargetControlID="txtSortorder" FilterType="Numbers" ValidChars="0123456789" />
                        <br />

                        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" cssClass="btn btn-lg" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" cssClass="btn btn-lg" CausesValidation="false" />
                        <asp:Label ID="lblShowid" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblInstituionID" runat="server" Visible="false"></asp:Label>
                        <br /><br />

                        <asp:Panel ID="pnllist" runat="server" Visible="false">
                            <h3>Institutions</h3>
                            <asp:GridView ID="gvList" runat="server" DataKeyNames="ID" AutoGenerateColumns="false"
                            CssClass="table" OnRowCommand="gvcommand">
                            <HeaderStyle  />
                                <Columns>
                                    <asp:TemplateField HeaderText="Serial No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <%# Eval("ID")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Institution") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Organization Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrgName" runat="server" Text='<%# Eval("OrgID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Address 1">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAddress1" runat="server" Text='<%# Eval("Address1") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Address 2">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAddress2" runat="server" Text='<%# Eval("Address2") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Zip Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lblZipCode" runat="server" Text='<%# Eval("ZipCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Order">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrder" runat="server" Text='<%# Eval("Sorting") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="editrow" 
                                                CommandArgument='<%# Eval("ID")%>' Text="Edit" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="false" CommandName="deleterow" 
                                                CommandArgument='<%#Eval("ID") %>' Text="Delete" 
                                                OnClientClick="return confirm ('Are you sure you want to delete this record?')" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div style="color:Red">
                                <asp:Label ID="lblMsg" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div><!--page index -->
        </div><!--contentwrapper -->
    </div><!--centercontent -->
</asp:Content>