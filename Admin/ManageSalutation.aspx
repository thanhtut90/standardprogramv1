﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageSalutation.aspx.cs" Inherits="Admin_ManageSalutation" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="row" style="padding-bottom:20px;">
            <div class="col-lg-7 content-header">
                <h1>Manage Salutation</h1>
            </div>
        </div><!--page header -->
        <div id="contentwrapper" class="contentwrapper">
            <div id="index" class="subcontent">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <h4>Add New Salutation</h4>

                        <asp:Label ID="lblSalName" runat="server" Text="Name" Font-Bold="true" ></asp:Label>
                        <br />
                        <asp:TextBox ID="txtSalName" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtSalName" runat="server"
                            ErrorMessage="Please insert the salutation name" ForeColor="Red"></asp:RequiredFieldValidator>
                        <br />

                        <asp:Label ID="lblSortOrder" runat="server" Text="Sort Order" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtSortorder" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbSortorder" runat="server" TargetControlID="txtSortorder" FilterType="Numbers" ValidChars="0123456789" />
                        <br />

                        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" class="btn btn-lg" />
                        <asp:Label ID="lblShowid" runat="server" Visible="false"></asp:Label>
                        <br /><br />

                        <div>
                            
                        <h3>Salutations</h3>
                        <asp:Panel ID="pnllist" runat="server" Visible="false">
                            <asp:GridView ID="gvList" runat="server" DataKeyNames="Sal_ID" 
                            OnRowDeleting="GridView1_RowDeleting"
                            OnRowEditing="GridView1_RowEditing"
                            OnRowUpdating="GridView1_RowUpdating"
                            OnRowCancelingEdit="GridView1_RowCancelingEdit" AutoGenerateColumns="false"
                            CssClass="table">
                                <HeaderStyle CssClass="theadstyle" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <%# Eval("Sal_ID")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Salutation Name">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("Sal_Name") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Sal_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Order">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtOrder" runat="server" Text='<%# Eval("Sal_order") %>' Width="50px"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="ftbOrder" runat="server" TargetControlID="txtOrder" FilterType="Numbers" ValidChars="0123456789" />
                                            <asp:Label ID="lblno" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrder" runat="server" Text='<%# Eval("Sal_order") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowEditButton="True" CausesValidation="false" />
                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" OnClientClick="return confirm ('Are you sure you want to delete this record?')" Text="Delete" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div style="color:Red"><asp:Label ID="lblMsg" runat="server"></asp:Label></div>
                        </asp:Panel>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div><!--page index -->
        </div><!--contentwrapper -->
    </div><!--centercontent -->
</asp:Content>