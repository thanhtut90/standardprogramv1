﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;

public partial class Admin_ManageAffiliation : System.Web.UI.Page
{
    /// <summary>
    /// If you update or delete already inserted records, need to check and update in the tb_RegDelegate table
    /// because the affiliation name may be used by those table.
    /// </summary>


    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["SHW"] != null)
            {
                lblShowid.Text = Request.QueryString["SHW"].ToString();
                BindGrid(lblShowid.Text);
                //Master.setgenopen();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    #region BindGrid and use table ref_Affiliation & bind gvList & set pnllist visibility true or false
    protected void BindGrid(string showid)
    {
        DataSet dsAffiliation = new DataSet();

        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsAffiliation = cmdObj.getAffiliation(showid);
        if (dsAffiliation.Tables[0].Rows.Count > 0)
        {
            gvList.DataSource = dsAffiliation;
            gvList.DataBind();
            pnllist.Visible = true;
        }
        else
        {
            pnllist.Visible = false;
        }
    }
    #endregion

    #region GridView1_RowDeleting and OnRowDeleting event of gvList & use table ref_Affiliation & delete from ref_Affiliation according to affid
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;
        string query = string.Format("Delete From ref_Affiliation Where affid={0}", ID);
        fn.ExecuteSQL(query);
        BindGrid(lblShowid.Text);
    }
    #endregion

    #region GridView1_RowEditing and OnRowEditing event of gvList
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvList.EditIndex = e.NewEditIndex;
        BindGrid(lblShowid.Text);
    }
    #endregion

    #region GridView1_RowUpdating and OnRowUpdating event of gvList & use table ref_Affiliation & update ref_Affiliation according to affid
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;
        // Retrieve the row being edited.
        int index = gvList.EditIndex;
        GridViewRow row = gvList.Rows[index];
        TextBox t1 = row.FindControl("txtName") as TextBox;
        TextBox t2 = row.FindControl("txtOrder") as TextBox;
        Label lblno = row.FindControl("lblno") as Label;
        string t3 = gvList.DataKeys[e.RowIndex].Value.ToString();
        string showid = lblShowid.Text;
        try
        {
            lblno.Visible = false;

            AffiliationObj objAff = new AffiliationObj();
            objAff.affid = ID;
            objAff.aff_name = t1.Text.Trim();
            objAff.aff_order = Convert.ToInt32(t2.Text.Trim());
            objAff.ShowID = showid;

            SetUpController setupCtrlr = new SetUpController(fn);
            int isSuccess = setupCtrlr.updateAffiliation(objAff);
            gvList.EditIndex = -1;
            BindGrid(lblShowid.Text);
        }
        catch (Exception ex)
        {
            lblno.Visible = true;
            lblno.Text = ex.Message;
        }
    }
    #endregion

    #region GridView1_RowCancelingEdit and OnRowCancelingEdit event of gvList
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvList.EditIndex = -1;
        BindGrid(lblShowid.Text);
    }
    #endregion

    #region btnAdd_Click and use table ref_Affiliation & insert into ref_Affiliation table
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string affID = lblAffID.Text;
        string affName = cFun.solveSQL(txtAffName.Text.ToString());
        string sortorder = cFun.solveSQL(txtSortorder.Text.ToString());

        try
        {
            SetUpController setupCtrlr = new SetUpController(fn);

            AffiliationObj objAffiliation = new AffiliationObj();
            objAffiliation.aff_name = affName;
            objAffiliation.aff_order = Convert.ToInt32(sortorder);
            objAffiliation.ShowID = lblShowid.Text;

            if (!string.IsNullOrEmpty(lblAffID.Text))
            {
                objAffiliation.affid = Convert.ToInt32(affID);
                setupCtrlr.updateAffiliation(objAffiliation);
            }
            else
            {
                int success = setupCtrlr.insertAffiliation(objAffiliation);
            }
        }
        catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }

        BindGrid(lblShowid.Text);
        txtAffName.Text = "";
        txtSortorder.Text = "";
    }
    #endregion
}