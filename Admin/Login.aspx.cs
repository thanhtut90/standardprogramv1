﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Utilities;
using System.Configuration;
using System.Data.SqlClient;
using Corpit.Registration;
using Corpit.Logging;
using System.Data;
public partial class Admin_AdminLogin : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected static string adminloginLockPage = "AdminLoginLock.aspx";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //***added on 8-11-2018
            Session.Clear();
            Session.Abandon();
            removeCookie();
            //***added on 8-11-2018
        }
    }
    public void removeCookie()
    {
        HttpCookie aCookie;
        string cookieName;
        int limit = Request.Cookies.Count;
        for (int i = 0; i < limit; i++)
        {
            cookieName = Request.Cookies[i].Name;
            aCookie = new HttpCookie(cookieName);
            aCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(aCookie);
        }
    }
    protected void btnSubmit_Onclick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            {
                try
                {
                    logRegLogin();//***
                    Functionality fn = new Functionality();
                    CommonFuns cFun = new CommonFuns();
                    Boolean isvalid = isValid();
                    if (isvalid)
                    {
                        string username = cFun.solveSQL(txtUsername.Text.ToString().Trim());
                        string password = cFun.solveSQL(txtPassword.Text.ToString().Trim());


                        LoginControler loginCtr = new LoginControler(fn);
                        LoginUser lUser = loginCtr.CheckAdminLogin(username, password);
                        //   DataTable dt = loginCtr.authenticateLogin(username, password);
                        //if (dt.Rows.Count > 0)
                        if (!string.IsNullOrEmpty(lUser.UserID))
                        {
                          //  bool hasUSer = loginCtr.HasCurrentUser(username);
                            bool hasUSer =false;
                            if (hasUSer)
                            {
                                lblpasswerror.Text = "This Account is using by another Person.";
                                lblpasswerror.Visible = true;
                            }
                            else
                            {
                                string roleid = lUser.RoleID.ToString();
                                string sesUser = lUser.UserName.ToString();
                                string sesUserID = lUser.UserID.ToString();
                                Session["roleid"] = roleid;
                                Session["username"] = sesUser;
                                Session["userid"] = sesUserID;


                                LoginLog lg = new LoginLog(fn);
                                string userip = GetUserIP();
                                lg.loginid = sesUser;
                                lg.loginip = userip;
                                lg.logintype = LoginLogType.adminLogin;
                                lg.saveLoginLog();
                                //if (roleid == "1")
                                //{
                                //    Response.Redirect("Event_Config");
                                //}
                                //else
                                //{
                                //Response.Redirect("MasterRegistrationList_Indiv.aspx");
                                string url = "Welcome";
                                string dashboardUrl = getCustomizedMasterPage("", Session["userid"].ToString());
                                //if (!string.IsNullOrEmpty(dashboardUrl))
                                //{
                                //    url = dashboardUrl;
                                //}

                                bool isUnlock = isRegLoginUnLock();//**
                                if (isUnlock)
                                {
                                    Response.Redirect(url);
                                }
                                else
                                {
                                    Response.Redirect(adminloginLockPage);
                                }
                                //}
                            }
                        }
                        else
                        {
                            lblpasswerror.Text = "Invalid username or password";
                            lblpasswerror.Visible = true;
                            bool isLock = isRegLoginLock();
                            if (isLock)
                            {
                                Response.Redirect(adminloginLockPage);
                            }
                        }
                    }
                }
                catch(Exception ex) { }
            }
        }
    }
    protected Boolean isValid()
    {
        Boolean isvalid = true;

        if (txtPassword.Text.ToString().Trim() == "" || txtPassword.Text.ToString().Trim() == string.Empty)
        {
            lblpasswerror.Text = "Please insert your password";
            lblpasswerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblpasswerror.Visible = false;
        }
        if (txtUsername.Text.ToString().Trim() == "" || txtUsername.Text.ToString().Trim() == string.Empty)
        {
            lblpasswerror.Text = "Please insert your username";
            lblpasswerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblpasswerror.Visible = false;
        }
        return isvalid;
    }

    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }

    private string getCustomizedMasterPage(string type, string userid)
    {
        string result = string.Empty;
        try
        {
            if (string.IsNullOrEmpty(type))
            {
                string sqlDashbord = "Select us_Dashboard_Cus From tb_Admin_Show Where us_userid='" + userid + "'";
                result = fn.GetDataByCommand(sqlDashbord, "us_Dashboard_Cus");
                if (result == "0")
                {
                    result = "";
                }
            }

            if (type == BackendRegType.backendRegType_Delegate)
            {
                string sql = "Select us_DelegateBackendMaster_Cus From tb_Admin_Show Where us_userid='" + userid + "'";
                result = fn.GetDataByCommand(sql, "us_DelegateBackendMaster_Cus");
                if (result == "0")
                {
                    result = "";
                }
            }
            else if (type == BackendRegType.backendRegType_Group)
            {
                string sql = "Select us_GroupBackendMaster_Cus From tb_Admin_Show Where us_userid='" + userid + "'";
                result = fn.GetDataByCommand(sql, "us_GroupBackendMaster_Cus");
                if (result == "0")
                {
                    result = "";
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }

    #region lock account
    private void logRegLogin()
    {
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogAdminLoginLocking Where login_machineip='" + machineIP + "'";
            DataTable dtlogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtlogin.Rows.Count > 0)
            {
                string sqlLoginUpdate = "Update tb_LogAdminLoginLocking Set login_attemptTimes=login_attemptTimes+1 Where login_machineip='" + machineIP + "'";
                fn.ExecuteSQL(sqlLoginUpdate);
                string AttemptTime = fn.GetDataByCommand("Select login_attemptTimes From tb_LogAdminLoginLocking Where login_machineip='" + machineIP + "'", "login_attemptTimes");
                int attempedTime = 0;
                int.TryParse(AttemptTime, out attempedTime);
                if (attempedTime > 5)
                {
                    Response.Redirect(adminloginLockPage);
                }
            }
            else
            {
                string sqlLoginInsert = "Insert Into tb_LogAdminLoginLocking (login_machineip)"
                                + " Values ('" + machineIP + "')";
                fn.ExecuteSQL(sqlLoginInsert);
            }
        }
        catch (Exception ex)
        { }
    }
    private bool isRegLoginLock()
    {
        bool isLock = false;
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogAdminLoginLocking Where login_machineip='" + machineIP + "'";
            DataTable dtlogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtlogin.Rows.Count > 0)
            {
                string sqlLoginUpdate = string.Empty;
                string AttemptTime = fn.GetDataByCommand("Select login_attemptTimes From tb_LogAdminLoginLocking Where login_machineip='" + machineIP + "'", "login_attemptTimes");
                int attempedTime = 0;
                int.TryParse(AttemptTime, out attempedTime);
                if (attempedTime > 5)
                {
                    sqlLoginUpdate = "Update tb_LogAdminLoginLocking Set login_lockdatetime=getdate(),login_islock=1 Where login_machineip='" + machineIP + "'";
                    fn.ExecuteSQL(sqlLoginUpdate);
                    isLock = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isLock;
    }
    private bool isRegLoginUnLock()
    {
        bool isUnlock = false;
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogAdminLoginLocking Where login_machineip='" + machineIP + "'";
            DataTable dtlogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtlogin.Rows.Count > 0)
            {
                string sqlLoginUpdate = string.Empty;
                string AttemptTime = fn.GetDataByCommand("Select login_lockdatetime From tb_LogAdminLoginLocking Where login_machineip='" + machineIP + "'", "login_lockdatetime");
                DateTime attempedDateTime = cFun.ParseDateTime(AttemptTime);
                DateTime datetimenow = DateTime.Now;
                TimeSpan duration = datetimenow - attempedDateTime;
                double durationTime = duration.TotalMinutes;
                if (durationTime >= 20)//***
                {
                    sqlLoginUpdate = "Update tb_LogAdminLoginLocking Set login_lockdatetime=Null,login_islock=0,login_attemptTimes=0 Where login_machineip='" + machineIP + "'";
                    fn.ExecuteSQL(sqlLoginUpdate);
                    isUnlock = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isUnlock;
    }
    #endregion
}