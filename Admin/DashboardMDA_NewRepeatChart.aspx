﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="DashboardMDA_NewRepeatChart.aspx.cs" Inherits="Admin_DashboardMDA_NewRepeatChart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"></telerik:RadAjaxManager>
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Dashboard</h1>
        </div>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div id="contentwrapper" class="contentwrapper">
                    <div style="width:100%">
                        <div class="col-lg-12" id="divMDA" runat="server" visible="false">
                            <div style="width:50%; float:left;">
                                <div class="box-header with-border">
                                    <h1 class="box-title" style="font-size:25px;">By New/Repeat Type</h1>
                                    <%--<div class="box-tools pull-right">
                                        <asp:LinkButton ID="btnViewByNewRepeatType" runat="server" OnClick="btnViewByNewRepeatType_Click">
                                            <i class="fa fa-eye"></i> View Report</asp:LinkButton>
                                    </div>--%>
                                </div>
                                <div id="divViewChartMDA" runat="server">
                                    <telerik:RadHtmlChart runat="server" ID="ChartMDA" CssClass="fb-sized">
                                        <PlotArea>
                                            <Series>
                                                <telerik:PieSeries StartAngle="45" DataFieldY="Total" ExplodeField="IsExploded" Name="PieSeriesName"
                                                    NameField="Desc">
                                                    <LabelsAppearance>
                                                        <ClientTemplate>
                                                            #=dataItem.Total# (#=dataItem.TotalPercentage# %)
                                                        </ClientTemplate>
                                                    </LabelsAppearance>
                                                    <TooltipsAppearance Color="White" DataFormatString="#=dataItem.Total# (#=dataItem.TotalPercentage# %)"></TooltipsAppearance>
                                                </telerik:PieSeries>
                                            </Series>
                                            <YAxis>
                                            </YAxis>
                                        </PlotArea>
                                        <ChartTitle Text="By New/Repeat Type"></ChartTitle>
                                    </telerik:RadHtmlChart>
                             </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

