﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageCountry.aspx.cs" Inherits="Admin_ManageCountry" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h3 class="box-title">Manage Country</h3>

    <br />
    <br />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateRecord">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                    <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>

    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CssClass="btn btn-info"><i class="fa fa-arrow-left"></i> Back</asp:LinkButton>

    <br />
    <br />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
        <asp:Panel runat="server" ID="PanelKeyList">
            <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="50" Skin="Bootstrap">
                <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="true"></Selecting>
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" DataKeyNames="Cty_GUID">
                    <Columns>

                        <telerik:GridButtonColumn ConfirmTextFormatString="Do you want to Delete Currency of {0}?" ConfirmTextFields="Cty_GUID" CommandArgument="Cty_GUID"
                            ConfirmDialogType="RadWindow" CommandName="Delete" ButtonType="ImageButton" ImageUrl="images/delete.jpg" UniqueName="Delete"
                            HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                        </telerik:GridButtonColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Cty_GUID" FilterControlAltText="Filter Cty_GUID"
                            HeaderText="ID" SortExpression="Cty_GUID" UniqueName="Cty_GUID">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Countryen" FilterControlAltText="Filter Countryen"
                            HeaderText="Country Code" SortExpression="Countryen" UniqueName="Countryen">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Countrycn" FilterControlAltText="Filter Countrycn"
                            HeaderText="Country Name (Chinese)" SortExpression="Countrycn" UniqueName="Countrycn">
                        </telerik:GridBoundColumn>

                        <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Country" FilterControlAltText="Filter Country"
                            HeaderText="Country Name (English)" SortExpression="Country" UniqueName="Country">
                        </telerik:GridBoundColumn>

                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </asp:Panel>

        <br />
        <br />
        <div class="mybuttoncss">
            <asp:Button runat="server" Text ="Add New" ID="btnAddNew" OnClick="btnAddNew_Click" CssClass="btn btn-primary"/>
        </div>
        <br />
        <br />
        <asp:Panel runat="server" ID="PanelKeyDetail">
            <h4>Detail</h4>

            <div class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Country Code</label>
                        <div class="col-md-2">
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtCountryCode" runat="server" CssClass="form-control"></telerik:RadTextBox>
                        </div>
                        <div class="col-md-8">
                            <asp:RequiredFieldValidator ID="rfvCountryCode" runat="server" Display="Dynamic"
                                ControlToValidate="txtCountryCode" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ID="cvCountryCode"
                                ControlToValidate="txtCountryCode" ErrorMessage="* Number Only" ForeColor="Red" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Country Name (Chinese)</label>
                        <div class="col-md-2">
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtCountryCn" runat="server" CssClass="form-control"></telerik:RadTextBox>
                        </div>
                        <div class="col-md-8">
                            <asp:RequiredFieldValidator ID="rfvCountryCn" runat="server" Display="Dynamic"
                                ControlToValidate="txtCountryCn" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Country Name (English)</label>
                        <div class="col-md-2">
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtCountryEng" runat="server" CssClass="form-control"></telerik:RadTextBox>
                        </div>
                        <div class="col-md-8">
                            <asp:RequiredFieldValidator ID="rfvCountryEng" runat="server" Display="Dynamic"
                                ControlToValidate="txtCountryEng" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2 control-label"></div>
                        <div class="col-md-4">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" CssClass="btn btn-primary"/>
                            <asp:Button ID="btnUpdateRecord" runat="server" Text="Update Record" OnClick="btnUpdateRecord_Click" UseSubmitBehavior="true" CssClass="btn btn-primary"></asp:Button>
                            <asp:Label runat="server" ID="lbls" ForeColor="Red"></asp:Label>
                            <asp:HiddenField ID="hfID" runat="server" Value="" />
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </telerik:RadAjaxPanel>
</asp:Content>

