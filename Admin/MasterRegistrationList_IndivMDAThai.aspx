﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="MasterRegistrationList_IndivMDAThai.aspx.cs" Inherits="Admin_MasterRegistrationList_IndivMDAThai" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="../Content/dist/css/skins/_all-skins.min.css">
    <%--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script> <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>--%>
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }

        $(function () {
            $.fn.datepicker.defaults.format = "dd/mm/yyyy";
            //     $("#txtFromDate").datepicker({}).val()
            //    $("#txtToDate").datepicker({}).val()
        });
    </script>
    <style type="text/css">
        .tdstyle1 {
            width: 170px;
        }

        .tdstyle2 {
            width: 300px;
        }

        form input[type="text"] {
            width: 39% !important;
        }

        .ajax__calendar_container {
            width: 320px !important;
            height: 280px !important;
        }

        .ajax__calendar_body {
            width: 100% !important;
            height: 100% !important;
        }

        td {
            vertical-align: middle;
        }
        
        #ctl00_ContentPlaceHolder1_GKeyMaster_GridData
        {
           height:500px !important;
        }
    </style>
 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    &nbsp;&nbsp;&nbsp;
    <asp:LinkButton ID="btnAddNew" runat="server" CssClass="btn btn-success" OnClick="lnkExcel_Clicked">
            <span aria-hidden="true" class="glyphicon glyphicon-export"></span> EXPORT EXCEL
    </asp:LinkButton>
    <asp:LinkButton ID="btnExportAll" runat="server" CssClass="btn btn-success" OnClick="btnExportAll_Click">
            <span aria-hidden="true" class="glyphicon glyphicon-export"></span> EXPORT ALL DATA TO EXCEL
    </asp:LinkButton>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="" Transparency="30"><%-- BackgroundTransparency="50"--%>
                    <asp:Label ID="Label2" runat="server" Text="Loading..">
                    </asp:Label>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/loading-icon.gif" />
                </telerik:RadAjaxLoadingPanel>
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true" LoadingPanelID="RadAjaxLoadingPanel1">

            <asp:Label ID="lblUser" runat="server" Visible="false"></asp:Label>
            <h3 class="box-title">Master Registration List (Individual)</h3>
            <div id="divsearch" runat="server" class="row" style="margin-bottom: 10PX;" visible="true">
                <div class="form-group col-sm-12">
                    <label for="inputEmail3" class="col-sm-1 control-label">Show List: &nbsp;</label>
                    <div class="col-sm-2">
                        <asp:Panel ID="showlist" runat="server">
                            <asp:DropDownList ID="ddl_showList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_showList_SelectedIndexChanged" CssClass="form-control">
                            </asp:DropDownList>
                        </asp:Panel>
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label for="inputEmail3" class="col-sm-1 control-label"> Flow List: &nbsp;</label>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="ddl_flowList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_flowList_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <label for="inputEmail3" class="col-sm-1 control-label">   Payment Status: &nbsp;</label>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="ddl_paymentStatus" runat="server" OnSelectedIndexChanged="ddl_paymentStatus_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group col-sm-10">
                    <asp:TextBox ID="txtFromDate" placeholder="From" runat="server" ClientIDMode="Static" onclick="$(this).datepicker().datepicker('show')" CssClass="form-control col-sm-1"></asp:TextBox>
                    <%-- <label for="inputEmail3" class="col-sm-1 control-label">  To </label>--%>
                    <div class="col-sm-1">
                    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" Enabled="false">To</asp:LinkButton>
                    </div>
                    <asp:TextBox ID="txtToDate" placeholder="To " runat="server" onclick="$(this).datepicker().datepicker('show')" CssClass="form-control col-sm-1"></asp:TextBox>
                    <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-success" runat="server" ><i class="fa fa-search"></i> </asp:LinkButton>
                </div>
                <div class="form-group col-sm-10">
                    <asp:TextBox ID="txtKey" placeholder="Enter Key Word (Registration Number, First Name, Surname, Email, Country)" runat="server" ClientIDMode="Static" CssClass="form-control  col-sm-1"></asp:TextBox>
                    <div class="col-sm-1">
                        <asp:LinkButton ID="btnKeysearch" OnClick="btnKeysearch_Click" class="btn btn-success" runat="server" ><i class="fa fa-search"></i> &nbsp;Search </asp:LinkButton>
                    </div>
                </div>
                <%--<div class="form-group col-sm-10">
                    <label for="inputEmail3" class="col-sm-1 control-label">   Payment Status: &nbsp;</label>
                    <div class="form-group col-sm-8">
                    <asp:DropDownList ID="ddlSortBy" runat="server" OnSelectedIndexChanged="ddlSortBy_SelectedIndexChanged"
                         AutoPostBack="true" CssClass="form-control">
                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Registration Date" Value="reg_datecreated"></asp:ListItem>
                    </asp:DropDownList>
                    </div>
                </div>--%>
            </div>
            <br />
            <br />
            <div style="overflow-x: scroll;">
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientEvents-OnRequestStart="onRequestStart">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <%-- <telerik:AjaxSetting AjaxControlID="btnupdate">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                            <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial"></telerik:AjaxUpdatedControl>
                        </UpdatedControls>
                    </telerik:AjaxSetting>--%>
                    </AjaxSettings>
                </telerik:RadAjaxManager>

                <%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">--%>
                    <asp:Panel runat="server" ID="PanelKeyList">
                        <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true" 
                            EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                            OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="10" OnItemDataBound="grid_ItemDataBound" OnItemCreated="RadGd1_ItemCreated"  OnPageIndexChanged="GKeyMaster_PageIndexChanged">
                            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                                <Selecting AllowRowSelect="false"></Selecting>
                                <Scrolling AllowScroll="True" EnableVirtualScrollPaging="false" UseStaticHeaders="True"
                                        SaveScrollPosition="True"></Scrolling>
                            </ClientSettings>
                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="Regno,RegGroupID" AllowFilteringByColumn="True" ShowFooter="false">
                                <CommandItemSettings ShowExportToExcelButton="False" ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                                <Columns>
                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RegGroupID" FilterControlAltText="Filter RegGroupID"
                                        HeaderText="RegGroupID" SortExpression="RegGroupID" UniqueName="RegGroupID" Visible="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridButtonColumn CommandName="Download" ButtonType="ImageButton" ImageUrl="images/download.png" UniqueName="Download"
                                        HeaderText="Acknowledge Letter" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>

                                    <telerik:GridButtonColumn ConfirmTextFormatString="Do you want to Delete {0}?" ConfirmTextFields="Regno" 
                                        ConfirmDialogType="RadWindow" CommandName="Delete" ButtonType="ImageButton" ImageUrl="images/delete.jpg" UniqueName="Delete"
                                        HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>

                                    <telerik:GridButtonColumn CommandName="Edit" ButtonType="ImageButton" ImageUrl="images/edit.jpg" UniqueName="Edit"
                                        HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Regno" FilterControlAltText="Filter Regno"
                                        HeaderText="Regno" SortExpression="Regno" UniqueName="Regno">
                                    </telerik:GridBoundColumn>

                                    <%--<telerik:GridTemplateColumn DataField="Regno" HeaderText="New/Repeat Visitor" UniqueName="IsNewVisitorMDA"
                                        FilterControlAltText="Filter Regno" SortExpression="Regno" Exportable="true">
                                        <ItemTemplate>
                                            <%# checkNewVisitorMDA(Eval("Regno").ToString(), "") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>

                                    <telerik:GridBoundColumn DataField="Salutation" HeaderText="" UniqueName="reg_Salutation"
                                        FilterControlAltText="Filter Salutation" SortExpression="Salutation" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindSalutation(Eval("reg_Salutation").ToString(), Eval("reg_SalutationOthers").ToString()) %>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_FName" FilterControlAltText="Filter reg_FName"
                                        HeaderText="" SortExpression="reg_FName" UniqueName="reg_FName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_LName" FilterControlAltText="Filter reg_LName"
                                        HeaderText="" SortExpression="reg_LName" UniqueName="reg_LName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_OName" FilterControlAltText="Filter reg_OName"
                                        HeaderText="" SortExpression="reg_OName" UniqueName="reg_OName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_PassNo" FilterControlAltText="Filter reg_PassNo"
                                        HeaderText="" SortExpression="reg_PassNo" UniqueName="reg_PassNo">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="reg_isReg" HeaderText="" UniqueName="reg_isReg"
                                        FilterControlAltText="Filter reg_isReg" SortExpression="reg_isReg" Exportable="true">
                                        <ItemTemplate>
                                            <%#Eval("reg_isReg") != null ? (Eval("reg_isReg").ToString() == "1" ? "Yes" : "No") : "No"%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_sgregistered" FilterControlAltText="Filter reg_sgregistered"
                                        HeaderText="" SortExpression="reg_sgregistered" UniqueName="reg_sgregistered">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_IDno" FilterControlAltText="Filter reg_IDno"
                                        HeaderText="" SortExpression="reg_IDno" UniqueName="reg_IDno">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Designation" FilterControlAltText="Filter reg_Designation"
                                        HeaderText="" SortExpression="reg_Designation" UniqueName="reg_Designation">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Email" FilterControlAltText="Filter reg_Email"
                                        HeaderText="" SortExpression="reg_Email" UniqueName="reg_Email">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address2" FilterControlAltText="Filter reg_Address2"
                                        HeaderText="" SortExpression="reg_Address2" UniqueName="reg_Address2">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Tel" UniqueName="reg_Tel"
                                        FilterControlAltText="Filter reg_Tel" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("reg_Telcc")%><%#Eval("reg_Telac")%><%#Eval("reg_Tel")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Mobile" UniqueName="reg_Mobile"
                                        FilterControlAltText="Filter reg_Mobile" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("reg_Mobcc")%><%#Eval("reg_Mobac")%><%#Eval("reg_Mobile")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Fax" UniqueName="reg_Fax"
                                        FilterControlAltText="Filter reg_Fax" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("reg_Faxcc")%><%# Eval("reg_Faxac")%><%#Eval("reg_Fax")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn DataField="InstitutionName" HeaderText="" UniqueName="reg_Institution"
                                        FilterControlAltText="Filter InstitutionName" SortExpression="InstitutionName" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindInstitution(Eval("reg_Institution").ToString(), Eval("reg_otherInstitution").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address1" FilterControlAltText="Filter reg_Address1"
                                        HeaderText="" SortExpression="reg_Address1" UniqueName="reg_Address1">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address3" FilterControlAltText="Filter reg_Address3"
                                        HeaderText="" SortExpression="reg_Address3" UniqueName="reg_Address3">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address4" FilterControlAltText="Filter reg_Address4"
                                        HeaderText="" SortExpression="reg_Address4" UniqueName="reg_Address4">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_City" FilterControlAltText="Filter reg_City"
                                        HeaderText="" SortExpression="reg_City" UniqueName="reg_City">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_State" FilterControlAltText="Filter reg_State"
                                        HeaderText="" SortExpression="reg_State" UniqueName="reg_State">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_PostalCode" FilterControlAltText="Filter reg_PostalCode"
                                        HeaderText="" SortExpression="reg_PostalCode" UniqueName="reg_PostalCode">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="RegCountry" HeaderText="" UniqueName="reg_Country"
                                        FilterControlAltText="Filter RegCountry" SortExpression="RegCountry" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindCountry(Eval("reg_Country").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="reg_RCountry" HeaderText="" UniqueName="reg_RCountry"
                                        FilterControlAltText="Filter reg_RCountry" SortExpression="reg_RCountry" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindCountry(Eval("reg_RCountry").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="AffiliationName" HeaderText="" UniqueName="reg_Affiliation"
                                        FilterControlAltText="Filter AffiliationName" SortExpression="AffiliationName" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindAffiliation(Eval("reg_Affiliation").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="DietaryName" HeaderText="" UniqueName="reg_Dietary"
                                        FilterControlAltText="Filter DietaryName" SortExpression="DietaryName" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindDietary(Eval("reg_Dietary").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Nationality" FilterControlAltText="Filter reg_Nationality"
                                        HeaderText="" SortExpression="reg_Nationality" UniqueName="reg_Nationality">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Age" FilterControlAltText="Filter reg_Age"
                                        HeaderText="" SortExpression="reg_Age" UniqueName="reg_Age">
                                    </telerik:GridBoundColumn>

                                    <%--<telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_DOB" FilterControlAltText="Filter reg_DOB"
                                        HeaderText="" SortExpression="reg_DOB" UniqueName="reg_DOB">
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="reg_DOB" HeaderText="" UniqueName="reg_DOB"
                                        FilterControlAltText="Filter reg_DOB" SortExpression="reg_DOB" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindDOBVender(Eval("reg_DOB").ToString(),Eval("showid").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Gender" FilterControlAltText="Filter reg_Gender"
                                        HeaderText="" SortExpression="reg_Gender" UniqueName="reg_Gender">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Membershipno" FilterControlAltText="Filter reg_Membershipno"
                                        HeaderText="" SortExpression="reg_Membershipno" UniqueName="reg_Membershipno">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vName" FilterControlAltText="Filter reg_vName"
                                        HeaderText="" SortExpression="reg_vName" UniqueName="reg_vName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vDOB" FilterControlAltText="Filter reg_vDOB"
                                        HeaderText="" SortExpression="reg_vDOB" UniqueName="reg_vDOB">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vPassno" FilterControlAltText="Filter reg_vPassno"
                                        HeaderText="" SortExpression="reg_vPassno" UniqueName="reg_vPassno">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vIssueDate" FilterControlAltText="Filter reg_vIssueDate"
                                        HeaderText="" SortExpression="reg_vIssueDate" UniqueName="reg_vIssueDate">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vPassexpiry" FilterControlAltText="Filter reg_vPassexpiry"
                                        HeaderText="" SortExpression="reg_vPassexpiry" UniqueName="reg_vPassexpiry">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vEmbarkation" FilterControlAltText="Filter reg_vEmbarkation"
                                        HeaderText="" SortExpression="reg_vEmbarkation" UniqueName="reg_vEmbarkation">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vArrivalDate" FilterControlAltText="Filter reg_vArrivalDate"
                                        HeaderText="" SortExpression="reg_vArrivalDate" UniqueName="reg_vArrivalDate">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="reg_vCountry" HeaderText="" UniqueName="reg_vCountry"
                                        FilterControlAltText="Filter reg_vCountry" SortExpression="reg_vCountry" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindCountry(Eval("reg_vCountry").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>


                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CName" FilterControlAltText="Filter UDF_CName"
                                        HeaderText="" SortExpression="UDF_CName" UniqueName="UDF_CName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_DelegateType" FilterControlAltText="Filter UDF_DelegateType"
                                        HeaderText="" SortExpression="UDF_DelegateType" UniqueName="UDF_DelegateType">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn HeaderText="" SortExpression="UDF_ProfCategory" UniqueName="UDF_ProfCategory"
                                        FilterControlAltText="Filter UDF_ProfCategory" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# Eval("UDF_ProfCategory")%><%#Eval("UDF_ProfCategoryOther")%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CPcode" FilterControlAltText="Filter UDF_CPcode"
                                        HeaderText="" SortExpression="UDF_CPcode" UniqueName="UDF_CPcode">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CLDepartment" FilterControlAltText="Filter UDF_CLDepartment"
                                        HeaderText="" SortExpression="UDF_CLDepartment" UniqueName="UDF_CLDepartment">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CAddress" FilterControlAltText="Filter UDF_CAddress"
                                        HeaderText="" SortExpression="UDF_CAddress" UniqueName="UDF_CAddress">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CLCompany" FilterControlAltText="Filter UDF_CLCompany"
                                        HeaderText="" SortExpression="UDF_CLCompany" UniqueName="UDF_CLCompany">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="UDF_CCountry" HeaderText="" UniqueName="UDF_CCountry"
                                        FilterControlAltText="Filter UDF_CCountry" SortExpression="UDF_CCountry" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindCountry(Eval("UDF_CCountry").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>


                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorName" FilterControlAltText="Filter reg_SupervisorName"
                                        HeaderText="" SortExpression="reg_SupervisorName" UniqueName="reg_SupervisorName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorDesignation" FilterControlAltText="Filter reg_SupervisorDesignation"
                                        HeaderText="" SortExpression="reg_SupervisorDesignation" UniqueName="reg_SupervisorDesignation">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorContact" FilterControlAltText="Filter reg_SupervisorContact"
                                        HeaderText="" SortExpression="reg_SupervisorContact" UniqueName="reg_SupervisorContact">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorEmail" FilterControlAltText="Filter reg_SupervisorEmail"
                                        HeaderText="" SortExpression="reg_SupervisorEmail" UniqueName="reg_SupervisorEmail">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Additional4" FilterControlAltText="Filter reg_Additional4"
                                        HeaderText="" SortExpression="reg_Additional4" UniqueName="reg_Additional4">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Additional5" FilterControlAltText="Filter reg_Additional5"
                                        HeaderText="" SortExpression="reg_Additional5" UniqueName="reg_Additional5">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="Please provide vegetarian meals." SortExpression="reg_Additional5" UniqueName="VegetarianMeals"
                                        FilterControlAltText="Filter reg_Additional5" Exportable="true">
                                        <ItemTemplate>
                                            <%#Eval("reg_Additional5").ToString() == "1" ? "Yes" : "No"%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Promo_Code" FilterControlAltText="Filter Promo_Code"
                                        HeaderText="Promo Code" SortExpression="Promo_Code" UniqueName="Promo_Code" >
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="Status" SortExpression="status_name" UniqueName="status_name"
                                        FilterControlAltText="Filter status_name" Exportable="true">
                                        <ItemTemplate>
                                            <%#!string.IsNullOrEmpty(Eval("status_name").ToString()) ? Eval("status_name").ToString() : "Incomplete"%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    

                                    <%--Disclosure for MDA Thai--%>
                                    <telerik:GridTemplateColumn DataField="Regno" HeaderText="Registration Policy" UniqueName="Disclosure"
                                        FilterControlAltText="Filter Regno" SortExpression="Regno" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindDisclosure(Eval("Regno").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn DataField="ProfessionName" HeaderText="" UniqueName="reg_Profession"
                                        FilterControlAltText="Filter ProfessionName" SortExpression="ProfessionName" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindProfession(Eval("reg_Profession").ToString(), Eval("reg_otherProfession").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>

                                    <%--<telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Jobtitle_alliedstu" FilterControlAltText="Filter reg_Jobtitle_alliedstu"
                                            HeaderText="Allied Health JobTitle" SortExpression="reg_Jobtitle_alliedstu" UniqueName="reg_Jobtitle_alliedstu">
                                        </telerik:GridBoundColumn>--%>

                                    <telerik:GridBoundColumn DataField="DepartmentName" HeaderText="" UniqueName="reg_Department"
                                        FilterControlAltText="Filter DepartmentName" SortExpression="DepartmentName" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindDepartment(Eval("reg_Department").ToString(), Eval("reg_otherDepartment").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn DataField="OrganizationName" HeaderText="" UniqueName="reg_Organization"
                                        FilterControlAltText="Filter OrganizationName" SortExpression="OrganizationName" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%# bindOrganisation(Eval("reg_Organization").ToString(), Eval("reg_otherOrganization").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="SPF Status" UniqueName="SPFStatus" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%#SPFStatus(Eval("reg_approveStatus").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Photo Url" UniqueName="VendorPhotoUrl" Exportable="true">
                                        <%--GridTemplateColumn<ItemTemplate>
                                            <%#getImgPath(Eval("Regno").ToString())%>
                                        </ItemTemplate>--%>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Send Confirmation Email" UniqueName="SendVisitorConfirmationEmail">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnSendVisitorConfirmationEmail" runat="server" OnCommand="btnSendVisitorConfirmationEmail_Command" Text="Send Confirmation Email" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID")%>'
                                                Visible='<%#isSendVisitorConfirmationEmailVisible(Eval("reg_Status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_datecreated" FilterControlAltText="Filter reg_datecreated"
                                        HeaderText="Created Date" SortExpression="reg_datecreated" UniqueName="reg_datecreated">
                                    </telerik:GridBoundColumn>

                                    <%--<telerik:GridTemplateColumn HeaderText="Congress Selection" UniqueName="CongressSelection" Exportable="true">
                                        <ItemTemplate>
                                            <%#getCongressSelection(Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("Invoice_status").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Payment Method" UniqueName="PaymentMethod" Exportable="true">
                                        <ItemTemplate>
                                            <%#getPaymentMethod(Eval("PaymentMethod").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Invoice_grandtotal" FilterControlAltText="Filter Invoice_grandtotal"
                                        HeaderText="Total Price" SortExpression="Invoice_grandtotal" UniqueName="Invoice_grandtotal">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="Paid Price" UniqueName="PaidPrice" Exportable="true">
                                        <ItemTemplate>
                                            <%#getPaidPrice(Eval("InvoiceID").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Outstanding" UniqueName="Outstanding" Exportable="true">
                                        <ItemTemplate>
                                            <%#calculateOutstanding(Eval("Invoice_grandtotal").ToString(), Eval("InvoiceID").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Invoice_discount" FilterControlAltText="Filter Invoice_discount"
                                        HeaderText="Discount Price" SortExpression="Invoice_discount" UniqueName="Invoice_discount">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="Invoice Status" UniqueName="InvoiceStatus" Exportable="true">
                                        <ItemTemplate>
                                            <%#getInvoiceStatus(Eval("Invoice_status").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Update Payment" UniqueName="UpdatePayment">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnUpdatePayment" runat="server" OnCommand="btnUpdatePayment_Command" Text="Update Payment" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID") + ";" +Eval("InvoiceID")%>'
                                                Visible='<%#isPaymentVisible(Eval("Invoice_status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Download Invoice" UniqueName="DownloadInvoice">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDownloadInvoice" runat="server" OnCommand="btnDownloadInvoice_Command" Text="Download Invoice" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID") + ";" +Eval("InvoiceID")%>'
                                                Visible='<%#isPaymentVisible(Eval("Invoice_status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Download Receipt" UniqueName="DownloadReceipt">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDownloadReceipt" runat="server" OnCommand="btnDownloadReceipt_Command" Text="Download Receipt" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID") + ";" +Eval("InvoiceID")%>'
                                                Visible='<%#isDownloadReceiptVisible(Eval("Invoice_status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Download Badge" UniqueName="DownloadBadge">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDownloadBadge" runat="server" OnCommand="btnDownloadBadge_Command" Text="Download Badge" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID") + ";" +Eval("InvoiceID")%>'
                                                Visible='<%#isDownloadReceiptVisible(Eval("Invoice_status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Send Confirmation Email" UniqueName="SendConfirmationEmail">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnSendConfirmationEmail" runat="server" OnCommand="btnSendConfirmationEmail_Command" Text="Send Confirmation Email" ForeColor="DarkBlue"
                                                CommandArgument='<%#Eval("Regno") + ";" +Eval("RegGroupID") + ";" +Eval("InvoiceID")%>'
                                                Visible='<%#isDownloadReceiptVisible(Eval("Invoice_status").ToString())%>'>
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>

                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </asp:Panel>
                </telerik:RadAjaxPanel>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


