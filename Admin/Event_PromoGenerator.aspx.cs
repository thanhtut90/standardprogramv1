﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Corpit.Utilities;
using Corpit.Site.Utilities;
using System.Data.SqlClient;
using Corpit.PromoV2;
using Corpit.Registration;
using ClosedXML.Excel;

public partial class Admin_Event_PromoGenerator : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["STP"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                setupSetting(showid, flowid);//////***
                ddlPUsage_SelectedIndexChanged(this, null);//////***
                setDiscountType(showid);//////***

                string catID = string.Empty;
                if (Request.Params["CAT"] != null)
                {
                    catID = cFun.DecryptValue(Request.QueryString["CAT"].ToString());
                    //btnAddNewConfItem.Visible = false;
                }

                bindPromo(showid, flowid, catID);

                string url = string.Empty;

                if (Request.Params["a"] == null)
                {
                    url = string.Format("Event_Conference_Controler?FLW={0}&SHW={1}&STP={2}", Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString());
                }
                else
                {
                    string actionType = Request.QueryString["a"].ToString();
                    url = string.Format("Event_Conference_Controler?FLW={0}&SHW={1}&STP={2}&a={3}", Request.QueryString["FLW"].ToString(), Request.QueryString["SHW"].ToString(), Request.QueryString["STP"].ToString(), actionType);
                }

                hplBack.NavigateUrl = url;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region setupSetting
    private void setupSetting(string showid, string flowid)
    {
        PromoFormFieldController profrmCtrl = new PromoFormFieldController(fn);
        List<FormFieldObj> lstFormObj = new List<FormFieldObj>();
        lstFormObj = profrmCtrl.getRegDelegateTableFieldListForPromo(showid, flowid);
        if (lstFormObj.Count > 0)
        {
            foreach (FormFieldObj frmObj in lstFormObj)
            {
                if (frmObj != null)
                {
                    if (frmObj.FormInputName == RefDelegateFormInputName._Profession)
                    {
                        divProfession.Visible = true;
                        lblProfession.Text = frmObj.FormInputLabel;
                        chklstProfession.DataSource = frmObj.lstRefTblFormField;
                        chklstProfession.DataBind();
                        txtTableFieldProfession.Text = frmObj.FormTableField;
                    }
                    if (frmObj.FormInputName == RefDelegateFormInputName._CategoryID)
                    {
                        divCategory.Visible = true;
                        lblCategory.Text = frmObj.FormInputLabel;
                        chklstCategory.DataSource = frmObj.lstRefTblFormField;
                        chklstCategory.DataBind();
                        txtTableFieldCategory.Text = frmObj.FormTableField;
                    }
                    if (frmObj.FormInputName == RefDelegateFormInputName._Country)
                    {
                        divCountry.Visible = true;
                        lblCountry.Text = frmObj.FormInputLabel;
                        chklstCountry.DataSource = frmObj.lstRefTblFormField;
                        chklstCountry.DataBind();
                        txtTableFieldCountry.Text = frmObj.FormTableField;
                    }
                }
            }
        }

        #region Conference
        string labelText = "";
        bool isShowMainConfItem = profrmCtrl.checkIsShowConferenceByType(showid, RefPromoConfigConfType.MainConferenceItem, ref labelText);
        if(isShowMainConfItem)
        {
            divMainConferenceItem.Visible = true;
            lblMainConferenceItem.Text = labelText;
            List<ConferenceItemObj> lstConf = new List<ConferenceItemObj>();
            lstConf = profrmCtrl.getAllMainConferenceItem(showid, flowid);
            chklstMainConfItem.DataSource = lstConf;
            chklstMainConfItem.DataBind();
        }
        labelText = "";
        bool isShowDependentConfItem = profrmCtrl.checkIsShowConferenceByType(showid, RefPromoConfigConfType.DependentConferenceItem, ref labelText);
        if (isShowDependentConfItem)
        {
            divDependentConferenceItem.Visible = true;
            lblDependentConferenceItem.Text = labelText;
            List<ConferenceItemObj> lstDependentConf = new List<ConferenceItemObj>();
            lstDependentConf = profrmCtrl.getAllDependentConferenceItem(showid, flowid);
            chklstDependentConfItem.DataSource = lstDependentConf;
            chklstDependentConfItem.DataBind();
        }
        #endregion
    }
    #endregion

    #region bindPromo (bind data from tb_PromoCode table to GridView(gvList))
    protected void bindPromo(string showid, string flowid, string catID)
    {
        try
        {
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            //SqlParameter spar1 = new SqlParameter("FlwID", SqlDbType.NVarChar);
            spar.Value = showid;
            //spar1.Value = flowid;
            pList.Add(spar);
            //pList.Add(spar1);

            string query = string.Empty;
            if (!string.IsNullOrEmpty(catID))
            {
                query = "Select * From tb_PromoCode Where ShowID=@SHWID And Promo_CategoryID=@Promo_CategoryID";//Promo_Type1=" + promoType + " And  And FlwID=@FlwID
                SqlParameter spar2 = new SqlParameter("Promo_CategoryID", SqlDbType.NVarChar);
                pList.Add(spar2);
                spar2.Value = catID;
            }
            else
            {
                query = "Select * From tb_PromoCode Where ShowID=@SHWID";//Promo_Type1=" + promoType + " And  And FlwID=@FlwID
            }

            DataSet ds = new DataSet();
            ds = fn.GetDatasetByCommand(query, "Ds", pList);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblPromoHeader.Visible = true;
                gvList.DataSource = ds;
                gvList.DataBind();
                divList.Visible = true;
            }
            else
            {
                lblPromoHeader.Visible = false;
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region gvList_RowCommand (to show modal popup)
    protected void gvList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "LaunchModal")
        {
            string promocodeID = e.CommandArgument.ToString();
            hfPromoCodeID.Value = promocodeID;

            ResetControls();

            divList.Visible = true;
            divPromoCodeInsert.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }
    }
    #endregion

    #region ddlPUsage_SelectedIndexChanged
    protected void ddlPUsage_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPUsage.SelectedValue == "2")
        {
            trMax.Visible = true;
        }
        else
        {
            trMax.Visible = false;
        }
    }
    #endregion

    #region btnAddNewConfItem_Click
    protected void btnAddNewConfItem_Click(object sender, EventArgs e)
    {
        divList.Visible = false;
        divPromoCodeInsert.Visible = true;
    }
    #endregion

    #region SavePromoCode (insert data into tb_PromoCode and tb_PromoList tables)
    protected void SavePromoCode(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

            string title = cFun.solveSQL(txtPTitle.Text);
            string promotype = "0";
            string value = "0";
            int pselect = 0;
            string timeused = string.Empty;
            string itemid = string.Empty;
            string prefix = string.Empty;
            int promo_type1 = PromoConditionType.RegistrationOnly;
            int noofcode = cFun.ParseInt(txtPNoofCode.Text.Trim());
            int usagepertran = 0;
            int maxusage = 1;
            try
            {
                promotype = "0";
                pselect = 0;
                value = "0";

                if (noofcode > 0)
                {
                    noofcode = Convert.ToInt32(txtPNoofCode.Text);
                    prefix = cFun.solveSQL(txtPPrefix.Text);
                    timeused = ddlPUsage.SelectedValue.ToString();

                    if (ddlPUsage.SelectedValue == "2")
                    {
                        maxusage = Convert.ToInt32(txtPMax.Text);
                    }

                    string onRate = cFun.solveSQL(ddlOnRate.SelectedValue);
                    string discountType = cFun.solveSQL(ddlDiscountType.SelectedValue);
                    string discountValue = "0";
                    if (ddlDiscountType.SelectedItem.Value == PromoDiscountType.percentage)
                    {
                        discountValue = cFun.solveSQL(ddlPercentage.SelectedValue);
                    }
                    else if(ddlDiscountType.SelectedItem.Value == PromoDiscountType.subtraction)
                    {
                        discountValue = cFun.solveSQL(txtAmount.Text.Trim());
                    }

                    string sql = "Insert Into tb_PromoCode "
                    + "(Promo_Type1,Promo_Type,Promo_Value,Promo_Added,Promo_Name,Promo_Prefix,Promo_Count"
                    + ",Promo_Timeused,Promo_PriceSelect,con_GroupId,Promo_isitem,Promo_ConfItemId,Promo_isEarlyBird,ShowID,FlwID"
                    + " ,discountType,discountValue,OnRate)"
                    + " Values (" + promo_type1 + ", " + promotype + ", " + value + ", getdate(), '" + title + "', '" + prefix + "', '" + noofcode
                    + "', " + timeused + ", " + pselect + ", " + "0,0,0,0,'" + showid + "','" + flowid 
                    + "','" + discountType + "','" + discountValue + "','" + onRate + "');SELECT SCOPE_IDENTITY();";
                    //////fn.ExecuteSQLGetID(sql);
                    string promocodeid = "";
                    string constr = fn.ConnString;
                    using (SqlConnection con = new SqlConnection(constr))
                    {
                        using (SqlCommand cmd = new SqlCommand(sql))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            con.Open();
                            promocodeid = (cmd.ExecuteScalar()).ToString();
                            con.Close();
                        }
                    }
                    #region get ID (comment)
                    ////string query = "Select Top(1)Promo_Id From tb_PromoCode Where ShowID=@SHWID Order By Promo_Id Desc";
                    ////List<SqlParameter> pList = new List<SqlParameter>();
                    ////SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    ////spar.Value = showid;
                    ////pList.Add(spar);
                    ////string promocodeid = fn.GetDataByCommand(query, "Promo_Id", pList);
                    #endregion

                    divPromoCode.Visible = true;
                    bindPromo(showid, flowid, string.Empty);

                    SavePCode(noofcode, prefix, promocodeid, title, maxusage, usagepertran, showid);
                }
                hfPromoCodeID.Value = "";
                ResetControls();
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region SavePCode (insert data into tb_PromoList table)
    protected void SavePCode(int count, string prefix, string promocodeid, string promotitle, int maxusage, int usagepertran, string showid)
    {
        DateTime validFrom = cFun.ParseDateTime(dateFrom.SelectedDate.ToString());
        DateTime validTO = cFun.ParseDateTime(dateTo.SelectedDate.ToString());
        for (int x = 0; x < count; x++)
        {
            string pcode = string.Empty;
            pcode = GeneratePromoCode(prefix);

            try
            {
                string promoid = "";
                string query = "Select Count(*) as ctcount From tb_PromoList Where PromoCode='" + pcode + "' And ShowID=@SHWID";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);
                int countcode = Convert.ToInt32(fn.GetDataByCommand(query, "ctcount", pList));
                if (countcode > 0)
                {
                    pcode = GeneratePromoCode(prefix);
                }

                string strFromDate = validFrom.ToString("yyyy-MM-dd HH:mm:ss.fff");
                string strToDate = validTO.ToString("yyyy-MM-dd HH:mm:ss.fff");

                string sqlinsert = "Insert Into tb_PromoList (PromocodeID,PromoCode,DateCreated, MaxUsage,NoOfUsagePerTransaction,ShowID,Valid_FromDate,Valid_ToDate) Values ("
                    + promocodeid + ",'" + pcode + "',getdate(),'" + maxusage + "'," + usagepertran + ",'" + showid + "','" + strFromDate + "','" + strToDate + "');SELECT SCOPE_IDENTITY();";
                //////fn.ExecuteSQL(sqlinsert);
                string constr = fn.ConnString;
                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand(sqlinsert))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        con.Open();
                        promoid = (cmd.ExecuteScalar()).ToString();
                        con.Close();
                    }
                }
                SavePromoCondition(promoid, pcode, promocodeid, showid);
            }
            catch (Exception ex)
            { }
            System.Threading.Thread.Sleep(50);
        }

        //ExportToExcel.DataTable3Excel(NewDt, "Promo_Code_" + txtPTitle.Text);
    }
    protected void SavePCodeAdditional(int count, string prefix, string promocodeid, string promotitle, int maxusage, int usagepertran, string showid)
    {
        for (int x = 0; x < count; x++)
        {
            string pcode = string.Empty;
            pcode = GeneratePromoCode(prefix);

            try
            {
                string promoid = "";
                string query = "Select Count(*) as ctcount From tb_PromoList Where PromoCode='" + pcode + "' And ShowID=@SHWID";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);
                int countcode = Convert.ToInt32(fn.GetDataByCommand(query, "ctcount", pList));
                if (countcode > 0)
                {
                    pcode = GeneratePromoCode(prefix);
                }

                string queryPromoList = "Select Top 1 * From tb_PromoList Where PromocodeID=" + promocodeid + " And ShowID=@SHWID Order By DateCreated";
                DataTable dtPromo = fn.GetDatasetByCommand(queryPromoList, "dsPromo", pList).Tables[0];
                if (dtPromo.Rows.Count > 0)
                {
                    string strFromDate = cFun.ParseDateTime(dtPromo.Rows[0]["Valid_FromDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
                    string strToDate = cFun.ParseDateTime(dtPromo.Rows[0]["Valid_ToDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");

                    string sqlinsert = "Insert Into tb_PromoList (PromocodeID,PromoCode,DateCreated, MaxUsage,NoOfUsagePerTransaction,ShowID,Valid_FromDate,Valid_ToDate) Values ("
                        + promocodeid + ",'" + pcode + "',getdate(),'" + maxusage + "'," + usagepertran + ",'" + showid + "','" + strFromDate + "','" + strToDate + "');SELECT SCOPE_IDENTITY();";
                    //////fn.ExecuteSQL(sqlinsert);
                    string constr = fn.ConnString;
                    using (SqlConnection con = new SqlConnection(constr))
                    {
                        using (SqlCommand cmd = new SqlCommand(sqlinsert))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Connection = con;
                            con.Open();
                            promoid = (cmd.ExecuteScalar()).ToString();
                            con.Close();
                        }
                    }

                    string promocodeTblID = dtPromo.Rows[0]["PromoID"].ToString();
                    string sqlPromoCondition = "Select RefConditionFieldDisplayLabel, RefConditionValueDisplayLabel, * From tb_PromoCondition Where PromoCodeID='" + promocodeTblID + "' And ShowID='" + showid + "'";
                    DataTable dtPromoCondition = fn.GetDatasetByCommand(sqlPromoCondition, "ds").Tables[0];
                    if (dtPromoCondition.Rows.Count > 0)
                    {
                        string queryPLID = "Select Top(1) PromoID From tb_PromoList Where ShowID=@SHWID Order By PromoID Desc";
                        List<SqlParameter> pListPLID = new List<SqlParameter>();
                        SqlParameter sparPLID = new SqlParameter("SHWID", SqlDbType.NVarChar);
                        sparPLID.Value = showid;
                        pListPLID.Add(sparPLID);
                        string PLID = fn.GetDataByCommand(queryPLID, "PromoID", pListPLID);

                        string sqlPromoConditionDuplicate = "Insert Into tb_PromoCondition"
                                        + " (PromoCodeID, ShowID, Condition, ConditionValue, RefConditionType, TblField, RefConditionFieldDisplayLabel, RefConditionValueDisplayLabel) "
                                        + " Select '" + PLID + "', ShowID, Condition, ConditionValue, RefConditionType, TblField, RefConditionFieldDisplayLabel, RefConditionValueDisplayLabel "
                                        + " From tb_PromoCondition Where PromoCodeID='" + promocodeTblID + "' And ShowID='" + showid + "'";
                        fn.ExecuteSQL(sqlPromoConditionDuplicate);
                    }
                }
            }
            catch (Exception ex)
            { }
            System.Threading.Thread.Sleep(50);
        }

        //ExportToExcel.DataTable3Excel(NewDt, "Promo_Code_" + txtPTitle.Text);
    }
    #endregion

    #region PromoCondition (insert data into tb_PromoCondition table)
    private void SavePromoCondition(string promoid, string promocode, string promocodeid, string showid)
    {
        try
        {
            int promo_type1 = 0;
            PromoConditionObj prmObj = new PromoConditionObj();
            prmObj.ID = "";
            prmObj.PromoCodeID = promoid;
            prmObj.ShowID = showid;
            if (divProfession.Visible == true)
            {
                int selectedCount = chklstProfession.Items.Cast<ListItem>().Count(li => li.Selected);
                if (selectedCount > 0)
                {
                    prmObj.RefConditionFieldDisplayLabel = lblProfession.Text.Trim();
                    PromoConditionObj prmSaveObj = prmObj;
                    saveProfession(ref prmSaveObj);
                    promo_type1 = PromoConditionType.RegistrationOnly;
                }
            }
            if (divCategory.Visible == true)
            {
                int selectedCount = chklstCategory.Items.Cast<ListItem>().Count(li => li.Selected);
                if (selectedCount > 0)
                {
                    prmObj.RefConditionFieldDisplayLabel = lblCategory.Text.Trim();
                    PromoConditionObj prmSaveObj = prmObj;
                    saveCategory(ref prmSaveObj);
                    promo_type1 = PromoConditionType.RegistrationOnly;
                }
            }
            if (divCountry.Visible == true)
            {
                int selectedCount = chklstCountry.Items.Cast<ListItem>().Count(li => li.Selected);
                if (selectedCount > 0)
                {
                    prmObj.RefConditionFieldDisplayLabel = lblCountry.Text.Trim();
                    PromoConditionObj prmSaveObj = prmObj;
                    saveCountry(ref prmSaveObj);
                    promo_type1 = PromoConditionType.RegistrationOnly;
                }
            }
            if (divMainConferenceItem.Visible == true)
            {
                int selectedCount = chklstMainConfItem.Items.Cast<ListItem>().Count(li => li.Selected);
                if (selectedCount > 0)
                {
                    prmObj.RefConditionFieldDisplayLabel = lblMainConferenceItem.Text.Trim();
                    PromoConditionObj prmSaveObj = prmObj;
                    saveMainConferenceItem(ref prmSaveObj);
                    if (promo_type1 == PromoConditionType.RegistrationOnly)
                    {
                        promo_type1 = PromoConditionType.RegistrationAndItem;
                    }
                    else
                    {
                        promo_type1 = PromoConditionType.ItemOnly;
                    }
                }
            }
            if (divDependentConferenceItem.Visible == true)
            {
                int selectedCount = chklstDependentConfItem.Items.Cast<ListItem>().Count(li => li.Selected);
                if (selectedCount > 0)
                {
                    prmObj.RefConditionFieldDisplayLabel = lblDependentConferenceItem.Text.Trim();
                    PromoConditionObj prmSaveObj = prmObj;
                    saveDependentConferenceItem(ref prmSaveObj);
                    if (promo_type1 == PromoConditionType.RegistrationOnly || promo_type1 == PromoConditionType.RegistrationAndItem)
                    {
                        promo_type1 = PromoConditionType.RegistrationAndItem;
                    }
                    else
                    {
                        promo_type1 = PromoConditionType.ItemOnly;
                    }
                }
            }

            /*update promo type in tb_PromoCode*/
            promo_type1 = promo_type1 == 0 ? PromoConditionType.RegistrationOnly : promo_type1;
            updatePromoType(promo_type1.ToString(), promocodeid, showid);
            /*update promo type in tb_PromoCode*/
        }
        catch (Exception ex)
        { }
        System.Threading.Thread.Sleep(50);
    }
    #region RegDelegateFields
    private void saveProfession(ref PromoConditionObj prmObj)
    {
        try
        {
            string condition = RefDelegateFormInputName._Profession;
            string conditionValue = "";
            string conditionValueDisplayLabel = "";
            if (chklstProfession.Items.Count > 0)
            {
                foreach (ListItem item in chklstProfession.Items)
                {
                    if (item.Selected)
                    {
                        conditionValue += "'" + item.Value + "'" + ",";
                        conditionValueDisplayLabel += item.Text.Trim() + ",";
                    }
                }
            }
            conditionValue = conditionValue.TrimEnd(',');
            conditionValueDisplayLabel = conditionValueDisplayLabel.TrimEnd(',');

            string refConditionType = cFun.solveSQL(RefPromoRegType.DELEGATEReg);
            string tblField = cFun.solveSQL(txtTableFieldProfession.Text.Trim());

            prmObj.Condition = condition;
            prmObj.ConditionValue = conditionValue;
            prmObj.RefConditionType = refConditionType;
            prmObj.TblField = tblField;
            prmObj.RefConditionValueDisplayLabel = conditionValueDisplayLabel;
            CreatePromoCondition(prmObj);
        }
        catch (Exception ex)
        { }
    }
    private void saveCategory(ref PromoConditionObj prmObj)
    {
        try
        {
            string condition = RefDelegateFormInputName._CategoryID;
            string conditionValue = "";
            string conditionValueDisplayLabel = "";
            if (chklstCategory.Items.Count > 0)
            {
                foreach (ListItem item in chklstCategory.Items)
                {
                    if (item.Selected)
                    {
                        conditionValue += "'" + item.Value + "'" + ",";
                        conditionValueDisplayLabel += item.Text.Trim() + ",";
                    }
                }
            }
            conditionValue = conditionValue.TrimEnd(',');
            conditionValueDisplayLabel = conditionValueDisplayLabel.TrimEnd(',');

            string refConditionType = cFun.solveSQL(RefPromoRegType.DELEGATEReg);
            string tblField = cFun.solveSQL(txtTableFieldCategory.Text.Trim());

            prmObj.Condition = condition;
            prmObj.ConditionValue = conditionValue;
            prmObj.RefConditionType = refConditionType;
            prmObj.TblField = tblField;
            prmObj.RefConditionValueDisplayLabel = conditionValueDisplayLabel;
            CreatePromoCondition(prmObj);
        }
        catch (Exception ex)
        { }
    }
    private void saveCountry(ref PromoConditionObj prmObj)
    {
        try
        {
            string condition = RefDelegateFormInputName._Country;
            string conditionValue = "";
            string conditionValueDisplayLabel = "";
            if (chklstCountry.Items.Count > 0)
            {
                foreach (ListItem item in chklstCountry.Items)
                {
                    if (item.Selected)
                    {
                        conditionValue += "'" + item.Value + "'" + ",";
                        conditionValueDisplayLabel += item.Text.Trim() + ",";
                    }
                }
            }
            conditionValue = conditionValue.TrimEnd(',');
            conditionValueDisplayLabel = conditionValueDisplayLabel.TrimEnd(',');

            string refConditionType = cFun.solveSQL(RefPromoRegType.DELEGATEReg);
            string tblField = cFun.solveSQL(txtTableFieldCountry.Text.Trim());

            prmObj.Condition = condition;
            prmObj.ConditionValue = conditionValue;
            prmObj.RefConditionType = refConditionType;
            prmObj.TblField = tblField;
            prmObj.RefConditionValueDisplayLabel = conditionValueDisplayLabel;
            CreatePromoCondition(prmObj);
        }
        catch (Exception ex)
        { }
    }
    private void saveMainConferenceItem(ref PromoConditionObj prmObj)
    {
        try
        {
            string condition = RefPromoConfConditionType.MainConferenceItem;
            string conditionValue = "";
            string conditionValueDisplayLabel = "";
            if (chklstMainConfItem.Items.Count > 0)
            {
                foreach (ListItem item in chklstMainConfItem.Items)
                {
                    if (item.Selected)
                    {
                        //conditionValue += "'" + item.Value + "'" + ",";
                        conditionValue += item.Value + ",";
                        conditionValueDisplayLabel += item.Text.Trim() + ",";
                    }
                }
            }
            conditionValue = conditionValue.TrimEnd(',');
            conditionValueDisplayLabel = conditionValueDisplayLabel.TrimEnd(',');

            string refConditionType = cFun.solveSQL(RefPromoConfigConfType.MainConferenceItem);
            string tblField = ConfTableField.conf_MainItemTblField;

            prmObj.Condition = condition;
            prmObj.ConditionValue = conditionValue;
            prmObj.RefConditionType = refConditionType;
            prmObj.TblField = tblField;
            prmObj.RefConditionValueDisplayLabel = conditionValueDisplayLabel;
            CreatePromoCondition(prmObj);
        }
        catch (Exception ex)
        { }
    }
    private void saveDependentConferenceItem(ref PromoConditionObj prmObj)
    {
        try
        {
            string condition = RefPromoConfConditionType.DependentConferenceItem;
            string conditionValue = "";
            string conditionValueDisplayLabel = "";
            if (chklstDependentConfItem.Items.Count > 0)
            {
                foreach (ListItem item in chklstDependentConfItem.Items)
                {
                    if (item.Selected)
                    {
                        //conditionValue += "'" + item.Value + "'" + ",";
                        conditionValue += item.Value + ",";
                        conditionValueDisplayLabel += item.Text.Trim() + ",";
                    }
                }
            }
            conditionValue = conditionValue.TrimEnd(',');
            conditionValueDisplayLabel = conditionValueDisplayLabel.TrimEnd(',');

            string refConditionType = cFun.solveSQL(RefPromoConfigConfType.DependentConferenceItem);
            string tblField = ConfTableField.conf_DependentItemTblField;

            prmObj.Condition = condition;
            prmObj.ConditionValue = conditionValue;
            prmObj.RefConditionType = refConditionType;
            prmObj.TblField = tblField;
            prmObj.RefConditionValueDisplayLabel = conditionValueDisplayLabel;
            CreatePromoCondition(prmObj);
        }
        catch (Exception ex)
        { }
    }
    #endregion
    private string CreatePromoCondition(PromoConditionObj prmObj)
    {
        string rtnID = "";
        try
        {
            string sqlinsert = "Insert Into tb_PromoCondition (PromoCodeID, ShowID, Condition, ConditionValue, RefConditionType, TblField, RefConditionFieldDisplayLabel, RefConditionValueDisplayLabel) "
                            + " Values('" + prmObj.PromoCodeID + "','" + prmObj.ShowID + "','" + cFun.solveSQL(prmObj.Condition)
                            + "','" + cFun.solveSQL(prmObj.ConditionValue) + "','" + cFun.solveSQL(prmObj.RefConditionType) + "','" + cFun.solveSQL(prmObj.TblField) 
                            + "','" + cFun.solveSQL(prmObj.RefConditionFieldDisplayLabel) + "','" + cFun.solveSQL(prmObj.RefConditionValueDisplayLabel) + "');SELECT SCOPE_IDENTITY();";
            string constr = fn.ConnString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(sqlinsert))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    rtnID = (cmd.ExecuteScalar()).ToString();
                    con.Close();
                }
            }
        }
        catch (Exception ex)
        { }

        return rtnID;
    }
    private void updatePromoType(string promotype, string promocodeid, string showid)
    {
        try
        {
            string sql = "Update tb_PromoCode Set Promo_Type1='" + promotype + "' Where Promo_Id='" + promocodeid + "' And ShowID='" + showid + "'";
            fn.ExecuteSQL(sql);
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region GeneratePromoCode
    protected string GeneratePromoCode(string prefix)
    {
        string ncode = fn.CreateRandomCode(5);//8/*changed to 5 on 30-10-2019*/
        string pcode = prefix + ncode;

        return pcode;
    }
    #endregion

    #region getUrl
    public string getUrl(string promoid, string catid)
    {
        string routeUrl = "#";

        if (Request.Params["SHW"] != null)
        {
            routeUrl = "Promo_List_PromoCodeGenerator.aspx?promoid=" + promoid;

            string stepid = Request.QueryString["STP"].ToString();
            routeUrl += "&STP=" + stepid;

            routeUrl += "&CAT=" + cFun.EncryptValue(catid);

            string showid = Request.QueryString["SHW"].ToString();
            routeUrl += "&SHW=" + showid;

            if (Request.Params["FLW"] != null)
            {
                string flowid = Request.QueryString["FLW"].ToString();
                routeUrl += "&FLW=" + flowid;
            }

            if (Request.Params["a"] != null)
            {
                string actionType = Request.QueryString["a"].ToString();
                routeUrl += "&a=" + actionType;
            }
        }

        return routeUrl;
    }
    #endregion

    #region ResetControls (clear data from controls)
    private void ResetControls()
    {
        txtNoofCode.Text = "";
        txtPTitle.Text = "";
        ddlPUsage.SelectedIndex = 0;
        txtPNoofCode.Text = "";
        txtPMax.Text = "";
        txtPPrefix.Text = "";
        ddlOnRate.SelectedIndex = 0;
        if (chklstProfession.Items.Count > 0)
        {
            chklstProfession.ClearSelection();
        }
        if (chklstCategory.Items.Count > 0)
        {
            chklstCategory.ClearSelection();
        }
        if (chklstCountry.Items.Count > 0)
        {
            chklstCountry.ClearSelection();
        }
        if (chklstMainConfItem.Items.Count > 0)
        {
            chklstMainConfItem.ClearSelection();
        }
        if (chklstDependentConfItem.Items.Count > 0)
        {
            chklstDependentConfItem.ClearSelection();
        }
        dateFrom.SelectedDate = null;
        dateTo.SelectedDate = null;

        divList.Visible = true;
        divPromoCodeInsert.Visible = false;
    }
    #endregion

    #region Additional Code
    #region btnClose_Click (to close modal popup [Close Button Click])
    protected void btnClose_Click(object sender, EventArgs e)
    {
        hfPromoCodeID.Value = "";
        ResetControls();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePop", "closeModal();", true);
    }
    #endregion

    #region btnCreate_Click (update Promo_Count of tb_PromoList & create additional Reference Cod, modal popup [Create Button Click])
    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

            string catID = string.Empty;
            if (Request.Params["CAT"] != null)
            {
                catID = cFun.DecryptValue(Request.QueryString["CAT"].ToString());
            }

            string promocodeID = hfPromoCodeID.Value;
            if (!string.IsNullOrEmpty(promocodeID))
            {
                try
                {
                    string query = "Select * From tb_PromoCode Where Promo_Id=" + promocodeID + " And ShowID=@SHWID And FlwID=@FlwID";
                    List<SqlParameter> pList = new List<SqlParameter>();
                    SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    SqlParameter spar1 = new SqlParameter("FlwID", SqlDbType.NVarChar);
                    spar.Value = showid;
                    spar1.Value = flowid;
                    pList.Add(spar);
                    pList.Add(spar1);

                    DataTable dtPromoCode = fn.GetDatasetByCommand(query, "dsPromoCode", pList).Tables[0];
                    if (dtPromoCode.Rows.Count > 0)
                    {
                        int noofcode = Convert.ToInt32(txtNoofCode.Text);
                        string prefix = dtPromoCode.Rows[0]["Promo_Prefix"].ToString();
                        string title = dtPromoCode.Rows[0]["Promo_Name"].ToString();

                        string queryPromoList = "Select * From tb_PromoList Where PromocodeID=" + promocodeID + " And ShowID=@SHWID";

                        DataTable dtPromo = fn.GetDatasetByCommand(queryPromoList, "dsPromo", pList).Tables[0];
                        if (dtPromo.Rows.Count > 0)
                        {
                            int maxusage = dtPromo.Rows[0]["MaxUsage"] != null ? Convert.ToInt32(dtPromo.Rows[0]["MaxUsage"].ToString()) : 1;
                            int usagepertran = 0;

                            string updatePromoCount = "Update tb_PromoCode Set Promo_Count=Promo_Count+" + noofcode + " Where Promo_Id=" + promocodeID;
                            fn.ExecuteSQL(updatePromoCount);

                            SavePCodeAdditional(noofcode, prefix, promocodeID, title, maxusage, usagepertran, showid);
                        }
                        bindPromo(showid, flowid, catID);

                        hfPromoCodeID.Value = "";
                        ResetControls();
                    }
                }
                catch (Exception ex)
                { }
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
    #endregion

    #region Discount
    protected void ddlDiscountType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["STP"] != null)
        {
            string ShowID = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            if (ddlDiscountType.SelectedItem.Value == PromoDiscountType.percentage)
            {
                #region Percentage
                divPercentage.Visible = false;
                cvPercentage.Enabled = false;
                bool isShowPercentage = false;
                string value = getShowPromoDiscountTypeConfig(ShowID, PromoDiscountType.percentage, ref isShowPercentage);
                if (isShowPercentage)
                {
                    string[] strValue = value.Split(',');
                    if (strValue.Length > 0)
                    {
                        ddlDiscountType.Items[1].Enabled = true;
                        divPercentage.Visible = true;
                        cvPercentage.Enabled = true;
                        divAmount.Visible = false;
                        rfvAmount.Enabled = false;
                        txtAmount.Text = "";
                        for (int i = 0; i < strValue.Length; i++)
                        {
                            ddlPercentage.Items.Add(strValue[i].ToString() + "%");
                            ddlPercentage.Items[i + 1].Value = strValue[i].ToString();
                        }
                    }
                }
                #endregion
            }

            if (ddlDiscountType.SelectedItem.Value == PromoDiscountType.subtraction)
            {
                #region Amount
                divAmount.Visible = false;
                rfvAmount.Enabled = false;
                bool isShowAmount = false;
                string value = getShowPromoDiscountTypeConfig(ShowID, PromoDiscountType.subtraction, ref isShowAmount);
                if (isShowAmount)
                {
                    divAmount.Visible = true;
                    rfvAmount.Enabled = true;
                    divPercentage.Visible = false;
                    cvPercentage.Enabled = false;
                    ddlPercentage.SelectedIndex = 0;
                }
                #endregion
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    private void setDiscountType(string ShowID)
    {
        #region Percentage
        ddlDiscountType.Items[1].Enabled = false;
        bool isShowPercentage = false;
        string value = getShowPromoDiscountTypeConfig(ShowID, PromoDiscountType.percentage, ref isShowPercentage);
        if(isShowPercentage)
        {
            string[] strValue = value.Split(',');
            if (strValue.Length > 0)
            {
                ddlDiscountType.Items[1].Enabled = true;
            }
        }
        #endregion

        #region Amount
        ddlDiscountType.Items[2].Enabled = false;
        bool isShowAmount = false;
        value = getShowPromoDiscountTypeConfig(ShowID, PromoDiscountType.subtraction, ref isShowAmount);
        if (isShowAmount)
        {
            ddlDiscountType.Items[2].Enabled = true;
        }
        #endregion
    }

    private string getShowPromoDiscountTypeConfig(string showID, string type, ref bool isShow)
    {
        string result = "";
        try
        {
            string sql = string.Format("Select Value,* From tb_PromoShowDiscountTypeConfig Where ShowID='{0}' And Type='{1}'", showID, type);
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if(dt.Rows.Count > 0)
            {
                result = dt.Rows[0]["Value"].ToString();
                isShow = true;
            }
        }
        catch(Exception ex)
        { }

        return result;
    }
    #endregion

    #region btnExportAllPromoCode_Click
    protected void btnExportAllPromoCode_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["STP"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
                string catID = string.Empty;
                if (Request.Params["CAT"] != null)
                {
                    catID = cFun.DecryptValue(Request.QueryString["CAT"].ToString());
                }
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                //SqlParameter spar1 = new SqlParameter("FlwID", SqlDbType.NVarChar);
                spar.Value = showid;
                //spar1.Value = flowid;
                pList.Add(spar);
                //pList.Add(spar1);
                string query = string.Empty;
                if (!string.IsNullOrEmpty(catID))
                {
                    query = "Select * From tb_PromoCode as pc Inner Join tb_PromoList as pl On pc.Promo_Id=pl.PromocodeID Where pc.ShowID=@SHWID And pl.ShowID=@SHWID And Promo_CategoryID=@Promo_CategoryID";//Promo_Type1=" + promoType + " And  And FlwID=@FlwID
                    SqlParameter spar2 = new SqlParameter("Promo_CategoryID", SqlDbType.NVarChar);
                    pList.Add(spar2);
                    spar2.Value = catID;
                }
                else
                {
                    query = "Select * From tb_PromoCode as pc Inner Join tb_PromoList as pl On pc.Promo_Id=pl.PromocodeID Where pc.ShowID=@SHWID And pl.ShowID=@SHWID";//Promo_Type1=" + promoType + " And  And FlwID=@FlwID
                }

                DataTable dtExport = new DataTable();
                dtExport.Columns.Add("SrNo.");
                dtExport.Columns.Add("Name");
                dtExport.Columns.Add("PromoCode");
                dtExport.Columns.Add("Criteria");
                dtExport.Columns.Add("Valid From");
                dtExport.Columns.Add("Valid To");
                dtExport.Columns.Add("Status");

                DataTable dt = new DataTable();
                dt = fn.GetDatasetByCommand(query, "Ds", pList).Tables[0];
                if(dt.Rows.Count > 0)
                {
                    int count = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if(dr["PromoCode"].ToString() == "1GZZAM28S")
                        {

                        }
                        DataRow drExport = dtExport.NewRow();
                        drExport["SrNo."] = count;
                        drExport["Name"] = dr["Promo_Name"];
                        drExport["PromoCode"] = dr["PromoCode"];
                        drExport["Criteria"] = setCriteria(dr["PromoID"].ToString(), showid);
                        drExport["Valid From"] = dr["Valid_FromDate"];
                        drExport["Valid To"] = dr["Valid_ToDate"];
                        drExport["Status"] = setStatus(dr["PromoCode"].ToString());
                        dtExport.Rows.Add(drExport);

                        count++;
                    }
                }

                exportExcel(dtExport);
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch(Exception ex)
        { }
    }
    public string setCriteria(string promocodeID, string showid)
    {
        string result = "";
        try
        {
            string sql = "Select RefConditionFieldDisplayLabel, RefConditionValueDisplayLabel, * From tb_PromoCondition Where PromoCodeID='" + promocodeID + "' And ShowID='" + showid + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    result += dr["RefConditionFieldDisplayLabel"].ToString().Replace(":", "");
                    result += ":";
                    result += dr["RefConditionValueDisplayLabel"] + ";";
                }
                result = result.TrimEnd(';');
            }
        }
        catch (Exception ex)
        { }
        return result;
    }
    public string setStatus(string promocode)
    {
        string status = string.Empty;
        try
        {
            int count = Convert.ToInt16(fn.GetDataByCommand("Select Count(*) as cnt From tb_User_Promo Where Promo_Code='" + promocode + "'", "cnt"));
            if (count != 0)
            {
                status = "Used";
            }
            else
            {
                status = "Available";
            }
        }
        catch(Exception ex)
        { }
        return status;
    }
    private void exportExcel(DataTable dtExport)
    {
        string filename = "IDEMPromoCodesList";
        XLWorkbook wb = new XLWorkbook();
        var ws = wb.Worksheets.Add("List");
        ws.Cell(1, 1).InsertTable(dtExport);
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format(@"attachment;filename={0}.xlsx", filename));
        ws.FirstRow().AddConditionalFormat().WhenGreaterThan(20000).Fill.SetBackgroundColor(XLColor.OrangeRed);//add bg color at first row(header)
        ws.Tables.FirstOrDefault().ShowAutoFilter = false;//remove first blank column and filter

        using (MemoryStream memoryStream = new MemoryStream())
        {
            wb.SaveAs(memoryStream);
            memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
            memoryStream.Close();
        }

        HttpContext.Current.Response.End();
    }
    #endregion

    #region btnCancel_Click
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ResetControls();
    }
    #endregion
}