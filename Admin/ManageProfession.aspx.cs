﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;

public partial class Admin_ManageProfession : System.Web.UI.Page
{
    /// <summary>
    /// If you update or delete already inserted records, need to check and update in the tb_RegDelegate table
    /// because the profession name may be used by those table.
    /// </summary>


    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                lblShowid.Text = Request.QueryString["SHW"].ToString();

                BindGrid(lblShowid.Text);
                //Master.setgenopen();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    #region BindGrid and use table ref_Profession & bind gvList & set pnllist visibility true or false
    protected void BindGrid(string showid)
    {
        DataSet dsProfession = new DataSet();
        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsProfession = cmdObj.getProfession(showid);
        if (dsProfession.Tables[0].Rows.Count > 0)
        {
            gvList.DataSource = dsProfession;
            gvList.DataBind();
            pnllist.Visible = true;
        }
        else
        {
            pnllist.Visible = false;
        }
    }
    #endregion

    #region GridView1_RowDeleting and OnRowDeleting event of gvList & use table ref_Profession & delete from ref_Profession according to ID
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;

        ProfessionObj objProf = new ProfessionObj();
        objProf.ID = ID;
        objProf.ShowID = lblShowid.Text;

        SetUpController setupCtrlr = new SetUpController(fn);
        int isSuccess = setupCtrlr.deleteProfession(objProf);

        BindGrid(lblShowid.Text);
    }
    #endregion

    #region GridView1_RowEditing and OnRowEditing event of gvList
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvList.EditIndex = e.NewEditIndex;
        BindGrid(lblShowid.Text);
    }
    #endregion

    #region GridView1_RowUpdating and OnRowUpdating event of gvList & use table ref_Profession & update ref_Profession according to ID
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;
        // Retrieve the row being edited.
        int index = gvList.EditIndex;
        GridViewRow row = gvList.Rows[index];
        TextBox t1 = row.FindControl("txtName") as TextBox;
        TextBox t2 = row.FindControl("txtOrder") as TextBox;
        Label lblno = row.FindControl("lblno") as Label;
        string t3 = gvList.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            lblno.Visible = false;

            ProfessionObj objProf = new ProfessionObj();
            objProf.ID = ID;
            objProf.Profession = cFun.solveSQL(t1.Text.ToString());
            objProf.Sorting = Convert.ToInt32(t2.Text.ToString());
            objProf.ShowID = lblShowid.Text;

            SetUpController setupCtrlr = new SetUpController(fn);
            int isSuccess = setupCtrlr.updateProfession(objProf);

            gvList.EditIndex = -1;
            BindGrid(lblShowid.Text);
        }
        catch (Exception ex)
        {
            lblno.Visible = true;
            lblno.Text = ex.Message;
        }
    }
    #endregion

    #region GridView1_RowCancelingEdit and OnRowCancelingEdit event of gvList
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvList.EditIndex = -1;
        BindGrid(lblShowid.Text);
    }
    #endregion

    #region btnAdd_Click and use table ref_Profession & insert into ref_Profession table
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string profName = cFun.solveSQL(txtProfName.Text.ToString());
        string sortorder = cFun.solveSQL(txtSortorder.Text.ToString());
        string showid = lblShowid.Text;

        try
        {
            ProfessionObj objProf = new ProfessionObj();
            objProf.Profession = profName;
            objProf.Sorting = Convert.ToInt32(sortorder);
            objProf.ShowID = showid;

            SetUpController setupCtrlr = new SetUpController(fn);
            int isSuccess = setupCtrlr.insertProfession(objProf);
        }
        catch(Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }

        BindGrid(lblShowid.Text);
        txtProfName.Text = "";
        txtSortorder.Text = "";
    }
    #endregion
}