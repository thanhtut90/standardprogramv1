﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master.master" CodeFile="Dyn_ItemList.aspx.cs" Inherits="Admin_Dyn_ItemList" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <asp:Panel ID="pnl_SubItems" runat="server" Visible="false">
                    <asp:Button ID = "btnAdd" runat = "server" Text = "Add New Item" OnClick="btnAdd_Click" CssClass="btn btn-primary" />
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Is Show</th>
                                    <th>Order Sequence</th>
                                    <th>Item Name</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Early Bird Price</th>
                                    <th>Regular Price</th>
                                    <th>Maximum Registrant</th>
                                    <th>Used Quantity</th>
                                    <th>Limit Message</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptSubItems" runat="server" OnItemCommand="rptSubcommand">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# setIsShowValue(Eval("con_IsShow").ToString())%></td>
                                            <td><%#Eval("con_OrderSeq")%></td>
                                            <td><%#Eval("con_ItemName")%></td>
                                            <td><%#Eval("con_ItemDesc")%></td>
                                            <td style="width:15%"><img runat="server" id="imgPhoto" visible='<%#setImgVisibility(Eval("con_ItemImage").ToString())%>' src='<%#Eval("con_ItemImage")%>' width="100"/></td>
                                            <td><%#Eval("con_EBPrice")%></td>
                                            <td><%#Eval("con_RegPrice")%></td>
                                            <td><%#Eval("con_MaxReg")%></td>
                                            <td><%#Eval("con_Used")%></td>
                                            <td>
                                                <asp:Label ID="lblMaxMsg" runat="server" Text='<%# System.Web.HttpUtility.HtmlDecode(Eval("con_MaxMessage").ToString()) %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="images/default/edit-icon.png" Width="20px" 
                                                    Height = "20px" CommandName="sub" CommandArgument='<%#Eval("con_DependentID") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </asp:Panel>

                    <div id="divSub" runat="server" visible="false">
                        <div class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Image</label>
                                    <div class="col-md-6">
                                        <asp:FileUpload ID="fupImg" runat="server" /><asp:Button ID="Button3" runat="server" Text="Upload" OnClick="UpSystemLogo_Click"/>
                                        <asp:Image ID="Image1" runat="server"  width="25%" style="display:block;"/>
                                        <img src="" id="imgUpload" alt="" width="25%" style="display:none;"/>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <asp:Panel ID="pnl_Sub" runat="server" Visible="false">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Item Name</label>
                                    <div class="col-md-6">
                                        <%--<asp:TextBox ID="txtName" runat="server"></asp:TextBox>--%>
                                        <CKEditor:CKEditorControl ID="txtName" runat="server"  Height="300px" Width="80%" BasePath="~/Admin/ckeditor" EnterMode="BR" 
                                            ShiftEnterMode="P" FilebrowserUploadUrl="Upload.ashx"></CKEditor:CKEditorControl>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Description</label>
                                    <div class="col-md-6">
                                        <CKEditor:CKEditorControl ID="txtDesc" runat="server"  Height="300px" Width="80%" BasePath="~/Admin/ckeditor" EnterMode="BR" 
                                            ShiftEnterMode="P" FilebrowserUploadUrl="Upload.ashx"></CKEditor:CKEditorControl>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Description (Show In Email Template)</label>
                                    <div class="col-md-6">
                                        <CKEditor:CKEditorControl ID="txtDescShowInEmailTemplate" runat="server"  Height="300px" BasePath="~/Admin/ckeditor" EnterMode="BR" 
                                            ShiftEnterMode="P" FilebrowserUploadUrl="Upload.ashx"></CKEditor:CKEditorControl>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Description (Show In Item Breakdown Report)</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtDescShowInReportItemBreakdown" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-6">
                                        <asp:CheckBox ID="chkisEarlyBird" runat="server" Text="Has Early Bird Price" Checked="true" onclick="checkbx();"/>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-6">
                                        <asp:CheckBox ID="chkIsShow" runat="server" Text="&nbsp;Is Show" Checked="true"/>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Order Sequence</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtSequence" runat="server"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtSequence" FilterType="Numbers" ValidChars="0123456789" />
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Early Bird Price</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtEarlyBirdPrice" runat="server" Text="0"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="txtEarlyBirdPrice" FilterType="Numbers, Custom" ValidChars="." />
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Regular Price</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtRegularPrice" runat="server" Text="0"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtRegularPrice" FilterType="Numbers, Custom" ValidChars="." />
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Maximum Registrant</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtmax" runat="server" Width="70px" CssClass="txtbig" Text="0"></asp:TextBox> 
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtmax" FilterType="Numbers" ValidChars="0123456789" />
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Limit Message</label>
                                    <div class="col-md-6">
                                        <CKEditor:CKEditorControl ID="txtMessage" runat="server"  Height="300px" Width="80%" BasePath="~/Admin/ckeditor" EnterMode="BR" 
                                            ShiftEnterMode="P" FilebrowserUploadUrl="Upload.ashx"></CKEditor:CKEditorControl>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Max Buyable Time</label>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtMaxBuyableTime" runat="server" Width="70px" CssClass="txtbig" Text="0"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtMaxBuyableTime" FilterType="Numbers" ValidChars="0123456789" />
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Disabled Items</label>
                                    <div class="col-md-6">
                                        <asp:CheckBoxList ID="chkDisabledItems" runat="server"></asp:CheckBoxList>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" onclick="btnSave_Click" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-primary" onclick="btnDelete_Click" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnReset" runat="server" Text="Reset"  CssClass="btn btn-primary" onclick="btnReset_Click" />
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                            </div>
                        </div>
                    </div>


                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" onclick="btnBack_Click" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Button3" />
                </Triggers>
            </asp:UpdatePanel>
</asp:Content>
