﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="TestingAdminStages.aspx.cs" Inherits="Admin_TestingAdminStages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">3. Manage Event Flow</h1>

        </div>
        <div id="contentwrapper" class="contentwrapper" style="padding-top:50px;">
            <asp:Panel runat="server" ID="PanelKeyList">
                <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                    EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                    OnNeedDataSource="GKeyMaster_NeedDataSource" PageSize="10" Skin="Bootstrap">
                    <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="false"></Selecting>
                    </ClientSettings>
                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" ShowHeader="false">
                        <Columns>
                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Title" FilterControlAltText="Filter Title"
                                HeaderText="Title" SortExpression="Title" UniqueName="Title" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                            </telerik:GridBoundColumn>

                            <telerik:GridTemplateColumn HeaderText="" SortExpression="Link" UniqueName="Link"
                                FilterControlAltText="Filter Link">
                                <ItemTemplate>
                                    <a href="javascript:void(0);" onclick='openRadWindow("<%# Eval("Link")%>")' style="color:cornflowerblue">View</a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </asp:Panel>
            <telerik:RadWindowManager RenderMode="Lightweight" ID="RadWindowManager1" runat="server" Visible="true" VisibleStatusbar="false"></telerik:RadWindowManager>
        </div>
    </div>
    
    <script src="Scripts/jquery-1.10.2.min.js"></script>

    <script type="text/javascript">
        function openRadWindow(url) {

            var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            if (w <= 1014) {
                w = w - 100;
            }
            else {
                w = 1000;
            }

            var oWnd = radopen(url, "Info", w, "800px");

            oWnd.center();
            oWnd.set_modal(true);
        }
    </script>
</asp:Content>

