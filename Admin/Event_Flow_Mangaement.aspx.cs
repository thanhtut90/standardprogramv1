﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Admin_EVent_Flow_Manage : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                PanelKeyDetail.Visible = false;
                txtShowID.Text = cFun.DecryptValue(Request.Params["SHW"].ToString());
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_site_flow_master table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        string showID = txtShowID.Text;
        if (!string.IsNullOrEmpty(showID))
        {
            string query = "select Flw_ID,FLW_Desc,ShowID from tb_site_flow_master as m inner join tb_site_flow as f on m.FLW_ID=f.flow_id where showID=@showID group by Flw_ID,FLW_Desc,ShowID";
            SqlParameter spar = new SqlParameter("showID", SqlDbType.NVarChar);
            spar.Value = showID;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar);

            DataTable dt = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dt;
                PanelKeyList.Visible = true;
            }
            else
                PanelKeyList.Visible = false;
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string curid = "";
        foreach (GridDataItem item in GKeyMaster.SelectedItems)
        {
            curid = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Flw_ID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Flw_ID"].ToString() : "";
        }
        if (e.CommandName == "EditFlow")
        {
            GridDataItem item = (GridDataItem)e.Item;

            curid = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Flw_ID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Flw_ID"].ToString() : "";
            if (!string.IsNullOrEmpty(curid))
            {
                FlowControler fControl = new FlowControler(fn);

                Dictionary<string, string> nValues = new Dictionary<string, string>();
                nValues = fControl.GetAdminNextRoute(curid, "0");
                if (nValues.Count > 0)
                {
                    string page = nValues["nURL"].ToString();
                    string step = nValues["nStep"].ToString();
                    string FlowID = nValues["FlowID"].ToString();

                    if (page == "") page = "Event_Config_Final";

                    string path = fControl.MakeFullURL(page, FlowID, txtShowID.Text, "", step);

                    Response.Redirect(path);
                }
            }
        }
    }
    #endregion

    #region LoadDetails 
    protected void btnCreateFlow_Onclick(object sender, EventArgs e)
    {
        string showID = txtShowID.Text;
        if (!string.IsNullOrEmpty(showID))
        {
            showID = cFun.EncryptValue(showID);
            Response.Redirect("EVent_Flow_Template?SHW=" + showID);
        }
    }
    #endregion

}