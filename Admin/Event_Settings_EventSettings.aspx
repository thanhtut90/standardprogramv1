﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Settings_EventSettings.aspx.cs" Inherits="Admin_Event_Settings_EventSettings" %>
<%@ MasterType VirtualPath="~/Admin/Master.master" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3 class="box-title">2-2. Manage Event Settings</h3>

    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="Event" class="subcontent">
                <asp:Panel ID="pnlmessagebar" runat="server" CssClass="notibar announcement" Visible="false">
                    <p class="txtgreen">
                        <asp:Label ID="lblemsg" runat="server" Text="Label"></asp:Label>
                    </p>
                </asp:Panel>
                <div class="form-horizontal">
                    
                    <div class="form-group">
                            <label class="col-md-2 control-label">Show Name</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtEventName" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                     <div class="form-group">
                            <label class="col-md-2 control-label">Show Prefix</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtPrefix" runat="server" CssClass="form-control"></asp:TextBox>                            
                            </div>
                        </div>
                     <div class="form-group">
                            <label class="col-md-2 control-label">Early Bird Closing Date</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtEarlyClosingDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="images/icons/calendar.png" />
                                <asp:RegularExpressionValidator ID="revEarlyClosingDate" runat="server"
                                    ControlToValidate="txtEarlyClosingDate" ErrorMessage="Invalid, valid format is dd/MM/yyyy hh:mm" ForeColor="Red"
                                    ValidationExpression="(((((0[1-9]|[12][0-9]|3[01])/(0[13578]|1[02]))|((0[1-9]|[12][0-9]|30)/(0[469]|11))|((0[1-9]|[1][0-9]|2[0-8]))/02)/([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}) ((([0-1][0-9])|([2][0-3]))[:][0-5][0-9]$))|(29/02/(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)) ((([0-1][0-9])|([2][0-3]))[:][0-5][0-9]$)))"
                                    Display="Dynamic">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                     <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#ff6a00">Registration Closing Date</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtRegClosingDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="images/icons/calendar.png" />
                                <asp:RegularExpressionValidator ID="revRegClosingDate" runat="server"
                                    ControlToValidate="txtRegClosingDate" ErrorMessage="Invalid, valid format is dd/MM/yyyy hh:mm" ForeColor="Red"
                                    ValidationExpression="(((((0[1-9]|[12][0-9]|3[01])/(0[13578]|1[02]))|((0[1-9]|[12][0-9]|30)/(0[469]|11))|((0[1-9]|[1][0-9]|2[0-8]))/02)/([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}) ((([0-1][0-9])|([2][0-3]))[:][0-5][0-9]$))|(29/02/(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)) ((([0-1][0-9])|([2][0-3]))[:][0-5][0-9]$)))"
                                    Display="Dynamic">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Registration Closing Message</label>
                            <div class="col-md-6">
                                <CKEditor:CKEditorControl ID="txtRegcloseMsg" runat="server" Height="200px" Width="98%" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Enable Timer?</label>
                            <div class="col-md-6">
                                <asp:RadioButtonList ID="rbTimer" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                    AutoPostBack="true" OnSelectedIndexChanged="rbTimer_SelectedIndexChanged">
                                    <asp:ListItem Text="No" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="form-group" id="trTimerType" runat="server" visible="false">
                            <label class="col-md-2 control-label">Timer Type</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlTimerType" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="-----Select-----" Value="0"></asp:ListItem>
                                    <%--<asp:ListItem Text="Second" Value="1"></asp:ListItem>--%>
                                    <asp:ListItem Text=" 30 Minutes" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="vcTimerType" runat="server" Enabled="false"
                                    ControlToValidate="ddlTimerType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Enable Terms & Conditions?</label>
                            <div class="col-md-6">
                                <asp:RadioButton ID="rbTerms1" runat="server" GroupName="rbterm" Text="No" AutoPostBack="true" OnCheckedChanged="rbTerms_CheckedChanged" />
                                <asp:RadioButton ID="rbTerms2" runat="server" GroupName="rbterm" Text="Yes" AutoPostBack="true" OnCheckedChanged="rbTerms_CheckedChanged" />
                                <br />
                                <CKEditor:CKEditorControl ID="txtTerms" runat="server" Height="200px" BasePath="~/Admin/ckeditor" Visible="false"
                                    EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2 control-label"></div>
                            <div class="col-md-6">
                                <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back" CssClass="btn btn-primary" />
                                &nbsp;
                                <asp:Button ID="btnUpdate" runat="server" Text="Save" OnClick="SaveForm" CssClass="btn btn-primary" />
                                &nbsp;
                                <asp:Button ID="btnSavePreview" runat="server" Text="Save And Preview" OnClick="btnSavePreview_Click" CssClass="btn btn-primary" />
                            </div>
                        </div>

                        <telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>
                </div>
            </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    <script src="js/datetimepicker/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="js/datetimepicker/jquery.dynDateTime.min.js" type="text/javascript"></script>
    <script src="js/datetimepicker/calendar-en.min.js" type="text/javascript"></script>
    <link href="css/calendar-blue.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
            $("#<%=txtEarlyClosingDate.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%d/%m/%Y %H:%M",
                daFormat: "%l;%M %p, %e %m, %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
            $("#<%=txtRegClosingDate.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%d/%m/%Y %H:%M",
                daFormat: "%l;%M %p, %e %m, %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
    </script>
</asp:Content>

