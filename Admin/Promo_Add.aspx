﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Promo_Add.aspx.cs" Inherits="Admin_Promo_Add" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <h3 class="box-title">Manage Promo Code</h3>

    <br />
    <br />
    <div class="row">
        <div class="col-md-6">
            <div class="box-body">
                <div class="alert alert-success alert-dismissible" id="divMsg" runat="server" visible="false">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <asp:Label ID="lblMsg" runat="server" ></asp:Label>
                </div>
            </div>
        </div>
    </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <div class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Promo Title</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txttitle" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Promo Code Applicable</label>
                        <div class="col-md-10">
                            <asp:RadioButton ID="rbbtn1" runat="server" GroupName="rbCon" Text=" Apply for Individual Conference"
                                OnCheckedChanged="chkChange1" AutoPostBack="true" Checked="true"/>&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="RadioButton1" runat="server" GroupName="rbCon" Text=" Apply for Total Price"
                                OnCheckedChanged="chkChange2"  AutoPostBack="true"/> &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbcat" runat="server" GroupName="rbCon" Text=" Apply for Category"
                                OnCheckedChanged="chkChange3"  AutoPostBack="true"/>&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbcon1" runat="server" GroupName="rbCon" Text=" Apply for Conference Item"
                                OnCheckedChanged="chkChange4"  AutoPostBack="true"/>&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbeebird" runat="server" GroupName="rbCon" Text=" Extended Early Bird" Visible="false"
                                OnCheckedChanged="chkChange5" AutoPostBack="true" />
                        </div>
                    </div>

                    <div id="tb1" runat="server">
                        <div class="form-group" id="trgroup" runat="server" visible="false">
                            <label class="col-md-2 control-label">Select Group</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlGroup" runat="server" Width="250px" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group" id="trcat" runat="server" visible="false">
                            <label class="col-md-2 control-label">Select Category</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlcategory" runat="server" Width="250px" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group" id="trtype" runat="server">
                            <label class="col-md-2 control-label">Promotion Type</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlpromotype" runat="server">
                                    <asp:ListItem Value="0">--Please Select--</asp:ListItem>
                                    <asp:ListItem Value="1">Percentage</asp:ListItem>
                                    <asp:ListItem Value="2">Fixed Price</asp:ListItem>
                                </asp:DropDownList>
                                <br /><br />
                                <asp:RadioButton ID="rbprice1" runat="server" GroupName="Pricetype" Text=" On Early Bird Price" />
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:RadioButton ID="rbprice2" runat="server" GroupName="Pricetype" Text=" On Regular Price" />
                            </div>
                        </div>

                        <div class="form-group" id="trvalue" runat="server">
                            <label class="col-md-2 control-label">Promo Value</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtvalue" runat="server" Width="50px" Font-Size="Medium" MaxLength="5" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtvalue" runat="server"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtvalue" 
                                    FilterType="Numbers" ValidChars="0123456789" />
                            </div>
                        </div>
                    </div>

                    <div id="trcon2" runat="server" visible="false">
                        <div class="form-group" id="tr3" runat="server">
                            <label class="col-md-2 control-label">Conference Item</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlconitem" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group" id="tr1" runat="server">
                            <label class="col-md-2 control-label">Promotion Type</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddltype1" runat="server">
                                    <asp:ListItem Value="0">--Please Select--</asp:ListItem>
                                    <asp:ListItem Value="1">Percentage</asp:ListItem>
                                    <asp:ListItem Value="2">Fixed Price</asp:ListItem>
                                </asp:DropDownList>
                                <br /><br />
                                <asp:RadioButton ID="rbeb1" runat="server" GroupName="Pricetype" Text=" On Early Bird Price" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:RadioButton ID="rbrp1" runat="server" GroupName="Pricetype" Text=" On Regular Price" />
                            </div>
                        </div>

                        <div class="form-group" id="tr2" runat="server">
                            <label class="col-md-2 control-label">Promo Value</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtvalue1" runat="server" Width="50px" Font-Size="Medium" MaxLength="5" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtvalue" runat="server" 
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtvalue"
                                    FilterType="Numbers" ValidChars="0123456789" />
                            </div>
                        </div>
                    </div>

                    <div class="stdtable">
                        <div class="form-group" id="trcount" runat="server">
                            <label class="col-md-2 control-label">Single/Multiple Usage</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlusage" runat="server" AutoPostBack="true" onselectedindexchanged="ddlusage_SelectedIndexChanged">
                                    <asp:ListItem Value="0">--Please Select--</asp:ListItem>
                                    <asp:ListItem Value="1">Single Usage</asp:ListItem>
                                    <asp:ListItem Value="2">Multiple Usage</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="form-group" id="trMax" runat="server" visible="false">
                            <label class="col-md-2 control-label">Max usage of each code</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtmax" runat="server" Font-Size="Medium" MaxLength="3" Width="30px" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                    ControlToValidate="txtmax" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                                    runat="server" ControlToValidate="txtmax" Display="Dynamic" 
                                    ErrorMessage="Only numbers are allowed" ForeColor="Red" 
                                    ValidationExpression="\d+"></asp:RegularExpressionValidator>
                            </div>
                        </div>

                        <div class="form-group" id="trusagepertran" runat="server">
                            <label class="col-md-2 control-label">Affected Items per Transaction</label>
                            <div class="col-md-6">
                                <asp:RadioButton ID="rbpertranOne" runat="server" GroupName="UsagePerTran" Text=" Only One Item per Transaction" />
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:RadioButton ID="rbpertranWhole" runat="server" GroupName="UsagePerTran" Text=" All Items per Transaction" Checked="true"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Number of Code</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtnoofcode" runat="server" Width="50px" Font-Size="Medium" MaxLength="3" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtnoofcode" runat="server"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="txtnoofcode"
                                    FilterType="Numbers" ValidChars="0123456789" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Promo Prefix</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtprefix" runat="server" Width="50px" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2 control-label"></div>
                        <div class="col-md-6">
                            <asp:Button ID="Button1" runat="server" Text="Save" OnClick="SaveForm" CssClass="btn btn-primary" />
                        </div>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>

