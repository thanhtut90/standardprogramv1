﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageSiteScriptModule.aspx.cs" Inherits="Admin_ManageSiteScriptModule" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Dashboard | Manage Site Script Module</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="centercontent">
         <div class="pageheader">
            <h1 class="pagetitle">Manage Site Script Module</h1>
            <span class="pagedesc">Page For Managing Site Script Module.</span>

            <ul class="hornav">
                <li class="current"><a href="#Index">Manage Site Script Module</a></li>
            </ul>
        </div>
        <div id="contentwrapper" class="contentwrapper">
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnAdd">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnUpdateRecord">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <div style="padding-left:25px;">
            <asp:LinkButton ID="lnkBack" runat="server" Text="Back" PostBackUrl="ManageSiteFlowStep.aspx?stp=2"></asp:LinkButton>
        </div>
        <br />

        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
            <asp:Panel runat="server" ID="PanelKeyList">
                <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                    EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                    OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="20" Skin="Bootstrap">
                    <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true"></Selecting>
                    </ClientSettings>
                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="module_id">
                        <Columns>

                            <telerik:GridButtonColumn ConfirmTextFormatString="Do you want to Delete Script Module of {0}?" ConfirmTextFields="module_id" CommandArgument="module_id"
                                ConfirmDialogType="RadWindow" CommandName="Delete" ButtonType="ImageButton" ImageUrl="images/delete.jpg" UniqueName="Delete"
                                HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                            </telerik:GridButtonColumn>

                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="module_id" FilterControlAltText="Filter module_id"
                                HeaderText="Module ID" SortExpression="module_id" UniqueName="module_id">
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="script_id" FilterControlAltText="Filter script_id"
                                HeaderText="Script ID" SortExpression="script_id" UniqueName="script_id">
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="script_desc" FilterControlAltText="Filter script_desc"
                                HeaderText="Script Description" SortExpression="script_desc" UniqueName="script_desc">
                            </telerik:GridBoundColumn>

                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </asp:Panel>

            <br />
            <br />
            <div class="mybuttoncss">
                <asp:Button runat="server" Text ="Add New" ID="btnAddNew" OnClick="btnAddNew_Click"/>
            </div>
            <br />
            <br />
            <asp:Panel runat="server" ID="PanelKeyDetail">
                <div class='page-header'>
                    <h2>Detail</h2>
                </div>
                <table id="tblShow" class="stdtable">
                      <tr class="EditFormHeader">
                        <td style="width: 170px;">
                            Module ID
                        </td>
                        <td>
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtModuleID" runat="server"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfvModuleID" runat="server" Display="Dynamic"
                                ControlToValidate="txtModuleID" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="EditFormHeader">
                        <td style="width: 170px;">
                             Script ID
                        </td>
                        <td>
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtScriptID" runat="server"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfvScriptID" runat="server" Display="Dynamic"
                                ControlToValidate="txtScriptID" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="EditFormHeader">
                        <td style="width: 170px;">
                             Script Description
                        </td>
                        <td>
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtDescription" runat="server"></telerik:RadTextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                <%--<telerik:RadButton ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click"></telerik:RadButton>--%>
                <asp:Button ID="btnUpdateRecord" runat="server" Text="Update Record" OnClick="btnUpdateRecord_Click" UseSubmitBehavior="true"></asp:Button>
                <asp:Label runat="server" ID="lbls" ForeColor="Red"></asp:Label>
                <asp:HiddenField ID="hfID" runat="server" Value="" />
            </asp:Panel>
        </telerik:RadAjaxPanel>
    </div>
</div>
</asp:Content>

