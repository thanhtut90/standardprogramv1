﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;

public partial class Admin_ManageDietary : System.Web.UI.Page
{
    /// <summary>
    /// If you update or delete already inserted records, need to check and update in the tb_RegDelegate table
    /// because the Dietary name may be used by those table.
    /// </summary>


    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["SHW"] != null)
            {
                lblShowId.Text = Request.QueryString["SHW"].ToString();
                BindGrid(lblShowId.Text);
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
            //Master.setgenopen();
        }
    }

    #region BindGrid and use table ref_Diet & bind gvList & set pnllist visibility true or false
    protected void BindGrid(string showId)
    {
        CommonDataObj cmdObj = new CommonDataObj(fn);
        try
        {
            gvList.DataSource = cmdObj.getDietary(showId);
            gvList.DataBind();
            pnllist.Visible = true;
        }
        catch (Exception ex)
        {
        }
        //DataSet ds = new DataSet();
        //ds = fn.GetDatasetByCommand("Select * From ref_Diet Order By diet_order", "ds");
        //if (ds.Tables[0].Rows.Count != 0)
        //{
        //    gvList.DataSource = ds;
        //    gvList.DataBind();
        //    pnllist.Visible = true;
        //}
        //else
        //{
        //    pnllist.Visible = false;
        //}
    }
    #endregion

    #region GridView1_RowDeleting and OnRowDeleting event of gvList & use table ref_Diet & delete from ref_Diet according to diet_id
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;
        string query = string.Format("Delete From ref_Diet Where diet_id={0}", ID);
        fn.ExecuteSQL(query);
        BindGrid(lblShowId.Text);
    }
    #endregion

    #region GridView1_RowEditing and OnRowEditing event of gvList
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvList.EditIndex = e.NewEditIndex;
        BindGrid(lblShowId.Text);
    }
    #endregion

    #region GridView1_RowUpdating and OnRowUpdating event of gvList & use table ref_Diet & update ref_Diet according to diet_id
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.RowIndex].Value;
        // Retrieve the row being edited.
        int index = gvList.EditIndex;
        GridViewRow row = gvList.Rows[index];
        TextBox t1 = row.FindControl("txtName") as TextBox;
        TextBox t2 = row.FindControl("txtOrder") as TextBox;
        Label lblno = row.FindControl("lblno") as Label;
        string t3 = gvList.DataKeys[e.RowIndex].Value.ToString();
        string dietName = cFun.solveSQL(t1.Text.ToString());
        string sortorder = cFun.solveSQL(t2.Text.ToString());
        string showid = lblShowId.Text;

        try
        {
            DietaryObj objDiet = new DietaryObj();
            objDiet.diet_id = ID;
            objDiet.diet_name = dietName;
            objDiet.diet_order = Convert.ToInt32(sortorder);
            objDiet.ShowID = showid;

            SetUpController setupCtrlr = new SetUpController(fn);
            int isSuccess = setupCtrlr.updateDietary(objDiet);
        }
        catch (Exception ex)
        {
            lblno.Visible = true;
            lblno.Text = ex.Message;
        }
    }
    #endregion

    #region GridView1_RowCancelingEdit and OnRowCancelingEdit event of gvList
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvList.EditIndex = -1;
        BindGrid(lblShowId.Text);
    }
    #endregion

    #region btnAdd_Click and use table ref_Diet & insert into ref_Diet table
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string dietName = cFun.solveSQL(txtDietName.Text.ToString());
        string sortorder = cFun.solveSQL(txtSortorder.Text.ToString());
        string showid = lblShowId.Text;

        try
        {
            DietaryObj objDiet = new DietaryObj();
            objDiet.diet_name = dietName;
            objDiet.diet_order = Convert.ToInt32(sortorder);
            objDiet.ShowID = showid;

            SetUpController setupCtrlr = new SetUpController(fn);
            int isSuccess = setupCtrlr.insertDietary(objDiet);
        }
        catch (Exception ex)
        {
            lblMsg.Visible = true;
            lblMsg.Text = ex.Message;
        }

        BindGrid(lblShowId.Text);
        txtDietName.Text = "";
        txtSortorder.Text = "";
    }
    #endregion
}