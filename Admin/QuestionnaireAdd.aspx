﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="QuestionnaireAdd.aspx.cs" Inherits="Admin_QuestionnaireAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Add Questionnaire</h1>
        </div>
        <div id="contentwrapper" class="contentwrapper">
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <AjaxSettings>

                </AjaxSettings>
            </telerik:RadAjaxManager>

            <div class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group" style="display:none;">
                            <label class="col-md-2 control-label">QA ID</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtQAID" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">QA Title</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtqatitle" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">QA Description</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtqadesc" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8">
                                <asp:Button ID="btnAdd" runat="server" Text="Save" OnClick="SaveForm" CssClass="btn btn-danger" />
                            </div>
                        </div>                
                    </div>
            </div>
        </div>
    </div>
</asp:Content>

