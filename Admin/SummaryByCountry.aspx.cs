﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using Corpit.Site.Utilities;
using ClosedXML.Excel;
using System.IO;

public partial class Admin_SummaryByCountry : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    string showID = string.Empty;
    static string _Name = "Name";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _City = "City";
    static string _State = "State";
    static string _ZipCode = "Zip Code";
    static string _Country = "Country";
    static string _TelCC = "Telcc";
    static string _TelAC = "Telac";
    static string _Tel = "Tel";
    static string _FaxCC = "Faxcc";
    static string _FaxAC = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Website = "Website";
    static string _Additional1 = "Additional1";
    static string _Additional2 = "Additional2";
    static string _Additional3 = "Additional3";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string[] showList = new string[2];

            try
            {
                if (Session["userid"] != null)
                {
                    string userID = Session["userid"].ToString();

                    binddata();

                    if (Session["roleid"].ToString() == "1")
                    {
                        //showList = getAllShowList();
                        showID = ddl_showList.SelectedValue;

                        int res = CheckGroupReg(Session["userid"].ToString(), showID);
                        if (res == 0)
                        {
                            gvSummaryByCountryG.Visible = false;
                            Button1.Visible = false;
                        }
                    }
                    else
                    {
                        showlist.Visible = false;
                        //showList = getShowList();
                        getShowID(userID);
                        int res = CheckGroupReg(Session["userid"].ToString(), "");
                        if (res == 0)
                        {
                            gvSummaryByCountryG.Visible = false;
                            Button1.Visible = false;
                        }
                    }

                    bindByCountry(showID);
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }

            //if (!string.IsNullOrEmpty(showList[0]))
            //{
            //    if (showList[0].Contains(","))
            //    {
            //        string[] showLists = showList[0].Split(',');
            //        bindByCountry(showLists[0]);
            //    }
            //    else
            //    {
            //        bindByCountry(showList[0]);
            //    }
            //}
            //else
            //{
            //    Response.Redirect("Event_Config.aspx");
            //}
        }
    }

    protected void binddata()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_showList.Items.Clear();
                ddl_showList.DataSource = cmd.ExecuteReader();
                ddl_showList.DataTextField = "SHW_Name";
                ddl_showList.DataValueField = "SHW_ID";
                ddl_showList.DataBind();
                con.Close();
            }
        }
    }
    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }
    protected string[] getAllShowList()
    {
        string query = "Select * from tb_Show Order By SHW_ID";
        string showList = string.Empty;
        string showName = string.Empty;

        DataTable dt = fn.GetDatasetByCommand(query, "dsShow").Tables[0];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (showList == string.Empty)
            {
                showList = dt.Rows[i]["SHW_ID"].ToString();
                showName = dt.Rows[i]["SHW_Name"].ToString();
            }
            else
            {
                showList += "," + dt.Rows[i]["SHW_ID"].ToString();
                showName += "," + dt.Rows[i]["SHW_Name"].ToString();
            }
        }
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        return list;
    }
    private int CheckGroupReg(string userid, string showid)
    {
        int res = 0;
        try
        {
            if (string.IsNullOrEmpty(showid))
            {
                string sql = "select * from tb_site_flow_master where ShowID in (select us_showid from tb_Admin_Show where us_userid='" + userid + "') and Status='Active' and FLW_Type='G'";
                DataSet ds = new DataSet();
                ds = fn.GetDatasetByCommand(sql, "sqlCheckGroup");

                res = ds.Tables[0].Rows.Count;
            }
            else
            {
                string sql = "select * from tb_site_flow_master where ShowID='" + showid + "' and Status='Active' and FLW_Type='G'";
                DataSet ds = new DataSet();
                ds = fn.GetDatasetByCommand(sql, "sqlCheckGroup");

                res = ds.Tables[0].Rows.Count;
            }
        }
        catch (Exception ex) { }
        return res;

    }
    protected string[] getShowList()
    {
        string showList = string.Empty;
        string showName = string.Empty;
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = Session["userid"].ToString();
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                query = "Select * from tb_Show WHERE SHW_ID=@showID";
                spar = new SqlParameter("showID", SqlDbType.NVarChar);

                string[] showLists = dt.Rows[0]["us_showid"].ToString().Split(',');
                for (int i = 0; i < showLists.Length; i++)
                {
                    pList.Clear();
                    spar.Value = showLists[i];
                    pList.Add(spar);

                    DataTable dtShow = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];

                    if (showName == string.Empty)
                    {
                        showName = dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                    else
                    {
                        showName += "," + dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                }
                list[0] = dt.Rows[0]["us_showid"].ToString();
                list[1] = showName;
                return list;
            }
            return list;
        }
        catch (Exception ex)
        {
            return list;
        }
    }
    protected void bindByCountry(string showid)
    {
        DataTable NewDt = new DataTable();
        Tuple<string, string> getType = checkFlowTypeGI(showid);
        if (getType != null)
        {
            #region By Country
            if (!string.IsNullOrEmpty(getType.Item1))
            {
                DataTable dtGroupCountry = GrpByCountry(showid);
                gvSummaryByCountryG.DataSource = dtGroupCountry;
                gvSummaryByCountryG.DataBind();
            }
            if (!string.IsNullOrEmpty(getType.Item2))
            {
                DataTable dtIndivCountry = IndivByCountry(showid);
                gvSummaryByCountry.DataSource = dtIndivCountry;
                gvSummaryByCountry.DataBind();
            }
            #endregion
        }
    }

    protected void ddl_showList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;
        int res = CheckGroupReg(Session["userid"].ToString(), showID);
        if (res == 0)
        {
            gvSummaryByCountryG.Visible = false;
            Button1.Visible = false;
        }
        bindByCountry(showID);
    }

    #region checkFlowTypeGI (get Group or Individual from tb_site_flow_master table)
    private Tuple<string, string> checkFlowTypeGI(string showid)
    {
        Tuple<string, string> tplType;
        GeneralObj grnObj = new GeneralObj(fn);
        DataTable dtFlowMas = grnObj.getAllSiteFlowMaster();

        string grp = string.Empty;
        string inv = string.Empty;
        if (dtFlowMas.Rows.Count > 0)
        {
            foreach (DataRow dr in dtFlowMas.Rows)
            {
                if (dr["ShowID"].ToString() == showid)
                {
                    string flowtype = dr["FLW_Type"].ToString();
                    if (flowtype == SiteFlowType.FLOW_GROUP)
                    {
                        grp = SiteFlowType.FLOW_GROUP;
                    }
                    else
                    {
                        inv = SiteFlowType.FLOW_INDIVIDUAL;
                    }
                }
            }
        }

        tplType = new Tuple<string, string>(grp, inv);

        return tplType;
    }
    #endregion

    #region byCountry
    #region GrpByCountry (get data from tb_RegDelegate, tb_RegGroup, tb_Invoice tables according to the RG_IsMultiple Is Not Null or Like 'G')
    private DataTable GrpByCountry(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Country");
        dt.Columns.Add("Count");
        dt.Columns.Add("Percentage");

        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        //DataTable dtmas = msregGroup.getDataAllRegGroupMemberList();
        //int masCount = dtmas.Rows.Count;

        bool confExist = checkConferencePageExist(showid, SiteDefaultValue.constantConfName);
        string query = "Select * From " + (confExist == true ? "RegChartGroupByCountryCompleteInvoice" : "RegChartGroupByCountryComplete") + " Where ShowID='" + showid + "'";
        DataTable Cat = fn.GetDatasetByCommand(query, "ds").Tables[0];//msregGroup.getRegChartGroupByCountry();
        Double catCount;
        Double TotalCount = 0;
        Double totalRegCount = 0;
        Double totalRegPercentageCount = 0;

        if (Cat.Rows.Count > 0)
        {
            TotalCount = cFun.ParseInt(Cat.Compute("SUM(SubCount)", string.Empty).ToString());
        }
        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFun.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Country"] = Cat.Rows[i]["countryname"].ToString();
            drow["Count"] = catCount.ToString();
            drow["Percentage"] = caldbscan.ToString() + "%";
            dt.Rows.Add(drow);

            totalRegCount += catCount;
            totalRegPercentageCount += caldbscan;
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Country"] = "No Record";
            drow["Count"] = "0";
            drow["Percentage"] = "0" + "%";
            dt.Rows.Add(drow);
        }
        else
        {
            DataRow drow = dt.NewRow();
            drow["Country"] = "Total";
            drow["Count"] = totalRegCount;
            drow["Percentage"] = Math.Round(totalRegPercentageCount, 0) + "%";
            dt.Rows.Add(drow);
        }

        return dt;
    }
    #endregion
    #region IndivByCountry (get data from tb_RegDelegate, tb_RegGroup, tb_Invoice tables according to the RG_IsMultiple Is Null or Like 'I')
    private DataTable IndivByCountry(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Country");
        dt.Columns.Add("Count");
        dt.Columns.Add("Percentage");

        MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        msregIndiv.ShowID = showid;
        //DataTable dtmas = msregIndiv.getDataAllRegList();
        //int masCount = dtmas.Rows.Count;

        bool confExist = checkConferencePageExist(showid, SiteDefaultValue.constantConfName);
        string query = "Select * From " + (confExist == true ? "RegChartIndivByCountryCompleteInvoice" : "RegChartIndivByCountryComplete") + " Where ShowID='" + showid + "'";
        DataTable Cat = fn.GetDatasetByCommand(query, "ds").Tables[0];//msregIndiv.getRegChartIndivByCountry();
        Double catCount;
        Double TotalCount = 0;
        Double totalRegCount = 0;
        Double totalRegPercentageCount = 0;
        if (Cat.Rows.Count > 0)
        {
            TotalCount = cFun.ParseInt(Cat.Compute("SUM(SubCount)", string.Empty).ToString());
        }

        for (int i = 0; i < Cat.Rows.Count; i++)
        {
            catCount = cFun.ParseInt(Cat.Rows[i]["SubCount"].ToString());
            Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
            DataRow drow = dt.NewRow();
            drow["Country"] = Cat.Rows[i]["countryname"].ToString();
            drow["Count"] = catCount.ToString();
            drow["Percentage"] = caldbscan.ToString() + "%";
            dt.Rows.Add(drow);

            totalRegCount += catCount;
            totalRegPercentageCount += caldbscan;
        }
        if (dt.Rows.Count == 0)
        {
            DataRow drow = dt.NewRow();
            drow["Country"] = "No Record";
            drow["Count"] = "0";
            drow["Percentage"] = "0" + "%";
            dt.Rows.Add(drow);
        }
        else
        {
            DataRow drow = dt.NewRow();
            drow["Country"] = "Total";
            drow["Count"] = totalRegCount;
            drow["Percentage"] = Math.Round(totalRegPercentageCount,0) + "%";
            dt.Rows.Add(drow);
        }

        return dt;
    }
    #endregion
    #endregion

    protected void genCountryGExcel(object sender, EventArgs e)
    {
        ExportToExcel(gvSummaryByCountryG, "ReportSummaryByGRegCountry");
    }

    protected void genCountryIndivExcel(object sender, EventArgs e)
    {
        ExportToExcel(gvSummaryByCountry, "ReportSummaryByIndivRegCountry");
    }

    #region ExportToExcel
    private void ExportToExcel(GridView gv, string filename)//, DataTable dt)
    {
        DataTable dt = new DataTable();

        //*convert Dynamic Gridview to DataTable
        if (gv.Rows.Count > 0)
        {
            // add the columns to the datatable
            if (gv.HeaderRow != null)
            {
                for (int i = 0; i < gv.HeaderRow.Cells.Count; i++)
                {
                    dt.Columns.Add(System.Net.WebUtility.HtmlDecode(gv.HeaderRow.Cells[i].Text.Replace("&nbsp;", "")));
                }
            }

            //  add each of the data rows to the table
            foreach (GridViewRow row in gv.Rows)
            {
                DataRow dr;
                dr = dt.NewRow();

                for (int i = 0; i < row.Cells.Count; i++)
                {
                    dr[i] = System.Net.WebUtility.HtmlDecode(row.Cells[i].Text.Replace("&nbsp;", ""));
                }
                dt.Rows.Add(dr);
            }
        }
        //*

        XLWorkbook wb = new XLWorkbook();
        var ws = wb.Worksheets.Add("List");
        ws.Cell(1, 1).InsertTable(dt);
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format(@"attachment;filename={0}.xlsx", filename));
        ws.FirstRow().AddConditionalFormat().WhenGreaterThan(20000).Fill.SetBackgroundColor(XLColor.OrangeRed);//add bg color at first row(header)
        ws.Tables.FirstOrDefault().ShowAutoFilter = false;//remove first blank column and filter

        using (MemoryStream memoryStream = new MemoryStream())
        {
            wb.SaveAs(memoryStream);
            memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
            memoryStream.Close();
        }

        HttpContext.Current.Response.End();
    }
    #endregion

    #region checkConferencePageExist
    public bool checkConferencePageExist(string showid, string scriptid)
    {
        bool isExist = false;
        string sql = "select * from tb_site_flow ";
        string FlwFilter = "";
        Dictionary<string, string> rtnValue = new Dictionary<string, string>();
        try
        {
            if (!string.IsNullOrEmpty(showid))
            {
                List<SqlParameter> pList = new List<SqlParameter>();
                FlwFilter = " where flow_Id In (Select FLW_ID From tb_site_flow_master Where ShowID=@ShowID) And script_id Like '%'+@ScriptID+'%'";
                FlwFilter += " and Status = 'Active'";

                sql += FlwFilter + " order by flow_ID,step_seq";

                SqlParameter pFlw = new SqlParameter("ShowID", SqlDbType.NVarChar);
                pFlw.Value = showid;
                pList.Add(pFlw);
                SqlParameter pScriptID = new SqlParameter("ScriptID", SqlDbType.NVarChar);
                pScriptID.Value = scriptid;
                pList.Add(pScriptID);

                DataTable dt = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];
                if (dt.Rows.Count > 0)
                {
                    isExist = true;
                }
            }
        }
        catch (Exception Ex) { isExist = false; }

        return isExist;
    }
    #endregion
}