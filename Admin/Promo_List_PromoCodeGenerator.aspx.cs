﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using Corpit.Utilities;
using Telerik.Web.UI;

public partial class Admin_Promo_List_PromoCodeGenerator : System.Web.UI.Page
{
    #region Declaration
    static string _promoid;
    public static string promoid
    {
        get
        {
            return _promoid;
        }
        set
        {
            _promoid = value;
        }
    }

    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

                if (Request.Params["promoid"] != null)
                {
                    promoid = Request.QueryString["promoid"];
                }

                bindPromo(showid);
                bindPromoName(showid);

                string flowid = string.Empty;
                if (Request.Params["FLW"] != null)
                {
                    flowid = Request.QueryString["FLW"].ToString();
                }

                if (Request.Params["FLW"] != null && Request.Params["STP"] != null && Request.Params["CAT"] != null)
                {
                    string url = string.Empty;

                    if(Request.Params["a"] == null)
                    {
                        url = "Event_PromoGenerator?SHW=" + Request.Params["SHW"].ToString() + "&FLW=" + Request.Params["FLW"].ToString() + "&STP=" + Request.Params["STP"].ToString() + "&CAT=" + Request.Params["CAT"].ToString();
                    }
                    else
                    {
                        url = "Event_PromoGenerator?SHW=" + Request.Params["SHW"].ToString() + "&FLW=" + Request.Params["FLW"].ToString() + "&STP=" + Request.Params["STP"].ToString() + "&CAT=" + Request.Params["CAT"].ToString() + "&a=" + Request.QueryString["a"].ToString();
                    }
                    hplBack.NavigateUrl = url;
                }
                else
                {
                    hplBack.NavigateUrl = "#";
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region bindPromo
    protected void bindPromo(string showid)
    {
        string query = "Select * From tb_PromoList Where PromocodeID=" + promoid + " And ShowID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand(query, "Ds", pList);
        if (ds.Tables[0].Rows.Count > 0)
        {
            GKeyMaster.DataSource = ds;
            GKeyMaster.DataBind();
        }
    }
    protected void bindPromoName(string showid)
    {
        string query = "Select * From tb_PromoCode Where Promo_Id=" + promoid + " And ShowID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand(query, "Ds", pList);
        if (ds.Tables[0].Rows.Count > 0)
        {
            lblPromoName.Text = ds.Tables[0].Rows[0]["Promo_Name"].ToString();
        }
    }
    #endregion

    #region setStatus
    public string setStatus(string promocode, string promoid)
    {
        string status = string.Empty;
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            string validStatus = fn.GetDataByCommand("Select isInvalid From tb_PromoList Where ShowID='" + showid + "' And PromoID In (" + promoid + ")", "isInvalid");
            if (validStatus == "1")
            {
                status = "Invalid";
            }
            else
            {
                int count = Convert.ToInt16(fn.GetDataByCommand("Select Count(*) as cnt From tb_User_Promo Where Promo_Code='" + promocode + "'", "cnt"));
                if (count != 0)
                {
                    status = "Used";
                }
                else
                {
                    status = "Available";
                }
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
        return status;
    }
    #endregion

    #region setColor
    public Color setColor(string promocode, string promoid)
    {
        System.Drawing.Color status = System.Drawing.Color.Green;
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            string validStatus = fn.GetDataByCommand("Select isInvalid From tb_PromoList Where ShowID='" + showid + "' And PromoID In (" + promoid + ")", "isInvalid");
            if (validStatus == "1")
            {
                status = System.Drawing.Color.Red;
            }
            else
            {
                int count = Convert.ToInt16(fn.GetDataByCommand("Select count (*) as cnt From tb_User_Promo Where Promo_Code='" + promocode + "'", "cnt"));
                if (count != 0)
                {
                    status = System.Drawing.Color.Red;
                }
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
        return status;
    }
    #endregion

    #region setRegno
    public string setRegno(string promocode)
    {
        string regno = fn.GetDataByCommand("Select Regno From tb_User_Promo Where Promo_Code='" + promocode + "'", "regno");
        if (regno == "0")
        {
            regno = "N/A";
        }
        return regno;
    }
    #endregion

    #region setCriteria
    public string setCriteria(string promocodeID, string showid)
    {
        string result = "";
        try
        {
            string sql = "Select RefConditionFieldDisplayLabel, RefConditionValueDisplayLabel, * From tb_PromoCondition Where PromoCodeID='" + promocodeID + "' And ShowID='" + showid + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                result = "<table>";
                foreach(DataRow dr in dt.Rows)
                {
                    result += "<tr>";
                    result += "<td width='30%'>" + dr["RefConditionFieldDisplayLabel"].ToString().Replace(":","") + "</td>";
                    result += "<td width='5%'>:</td>";
                    result += "<td width='50%'>" + dr["RefConditionValueDisplayLabel"] + "</td>";
                    result += "</tr>";
                }

                #region bind promo value
                string sqlPromoDiscountValue = "Select discountValue,discountType,* From tb_PromoCode Where ShowID='" + showid + "' And Promo_Id In ( "
                            + " Select tb_PromoList.PromocodeID From tb_PromoList Where ShowID='" + showid + "' and PromoID=" + promocodeID + ")";
                DataTable dtPromoDiscountValue = fn.GetDatasetByCommand(sqlPromoDiscountValue, "ds").Tables[0];
                if (dtPromoDiscountValue.Rows.Count > 0)
                {
                    string discountType = dtPromoDiscountValue.Rows[0]["discountType"] != DBNull.Value ? 
                        (dtPromoDiscountValue.Rows[0]["discountType"].ToString() == "PCT" ? "Percentage" :
                        (dtPromoDiscountValue.Rows[0]["discountType"].ToString() == "SUBTRACT" ? "Amount" : "")) : "";
                    result += "<tr>";
                    result += "<td width='30%'>Discount amount</td>";
                    result += "<td width='5%'>:</td>";
                    result += "<td width='50%'>" + dtPromoDiscountValue.Rows[0]["discountValue"] + " " + discountType + "</td>";
                    result += "</tr>";
                }
                #endregion
                result += "</table>";
            }
        }
        catch(Exception ex)
        { }
        return result;
    }
    #endregion

    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "CancelPromoCode")
        {
            GridDataItem item = (GridDataItem)e.Item;
            string promoID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["PromoID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["PromoID"].ToString() : "";
            if (!string.IsNullOrEmpty(promoID))
            {
                try
                {
                    if (Request.Params["SHW"] != null)
                    {
                        string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                        string query = "Update tb_PromoList Set isInvalid=1 Where PromoID=" + promoID + " And ShowID=@SHWID";
                        List<SqlParameter> pList = new List<SqlParameter>();
                        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                        spar.Value = showid;
                        pList.Add(spar);
                        fn.ExecuteSQLWithParameters(query, pList);
                        bindPromo(showid);
                        bindPromoName(showid);
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updated.');", true);
                        return;
                    }
                    else
                    {
                        Response.Redirect("Login.aspx");
                    }
                }
                catch (Exception ex)
                { }
            }
        }
    }
}