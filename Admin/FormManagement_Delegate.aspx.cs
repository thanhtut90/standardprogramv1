﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Corpit.BackendMaster;
using Corpit.Utilities;
using Corpit.Site.Utilities;
using System.Data.SqlClient;
using Telerik.Web.UI;
using Corpit.Registration;

public partial class Admin_FormManagement_Delegate : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFUnz = new CommonFuns();
    protected string shwID;
    private static string datasource_delegate = "90003";//DelegateWithGroupCompanyInfo

    #region Form Parameters
    static string _Salutation = "Salutation";
    static string _FName = "FName";
    static string _LName = "LName";
    static string _Oname = "Oname";
    static string _PassportNo = "PassportNo";
    static string _IsRegistered = "isRegistered";
    static string _RegSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";
    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";
    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_ProfCategoryOther = "UDF_ProfCategoryOther";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _SupervisorName = "Supervisor Name";
    static string _SupervisorDesignation = "Supervisor Designation";
    static string _SupervisorContact = "Supervisor Contact";
    static string _SupervisorEmail = "Supervisor Email";
    static string _OtherSalutation = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDepartment = "Other Department";
    static string _OtherOrganization = "Other Organization";
    static string _OtherInstitution = "Other Institution";
    static string _AEmail = "aEmail";
    static string _IsSMS = "IsSMS";
    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string showid = cFUnz.DecryptValue(urlQuery.CurrShowID);
                string flowid = cFUnz.DecryptValue(urlQuery.FlowID);

                shwID = showid;

                bool isCreated = checkCreated(flowid, showid);
                if (!isCreated)
                {
                    InsertNewForm(flowid, showid);
                }

                setDynamicForm(flowid, showid);

                if (Request.QueryString["STP"] != null && Request.QueryString["a"] == null)
                {
                    string curStep = cFUnz.DecryptValue(urlQuery.CurrIndex);

                    FlowMaster fMaster = new FlowMaster();
                    fMaster.FlowID = flowid;
                    fMaster.FlowStatus = curStep;
                    FlowControler fControler = new FlowControler(fn);
                    fControler.UpdateFlowMasterStatus(fMaster);
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        else
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string showid = cFUnz.DecryptValue(urlQuery.CurrShowID);
                string flowid = cFUnz.DecryptValue(urlQuery.FlowID);

                shwID = showid;
            }
        }
    }

    #region checkCreated (check the record count of tmp_Form table is equal to the record counts of tb_Form table)
    private bool checkCreated(string flowid, string showid)
    {
        bool isCreated = false;
        DataTable dt = fn.GetDatasetByCommand("Select * From tmp_Form Where form_type='" + FormType.TypeDelegate + "'", "ds").Tables[0];

        string query = "Select * From tb_Form Where form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID=@SHWID And form_input_name In (Select form_input_name From tmp_Form Where form_type='" + FormType.TypeDelegate + "')";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dtForm = fn.GetDatasetByCommand(query, "dsForm", pList).Tables[0];
        if(dt.Rows.Count == dtForm.Rows.Count)
        {
            isCreated = true;
        }

        return isCreated;
    }
    #endregion

    #region Generate Dynamic Form and use table tb_Form & bind data to related controls
    protected void setDynamicForm(string flowid, string showid)
    {
        DataSet ds = new DataSet();
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForDelegate();

        string formtype = FormType.TypeDelegate;
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar1 = new SqlParameter("SHWID", SqlDbType.NVarChar);
        SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
        SqlParameter spar3 = new SqlParameter("form_type", SqlDbType.NVarChar);
        SqlParameter spar4 = new SqlParameter("FieldID", SqlDbType.NVarChar);
        spar1.Value = showid;
        spar2.Value = flowid;
        spar3.Value = formtype;
        spar4.Value = "";
        pList.Add(spar1);
        pList.Add(spar2);
        pList.Add(spar3);
        pList.Add(spar4);

        StringBuilder sb = new StringBuilder();
        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
        {
            #region form_input_name=Salutation & set chkshowSalutation and chkcompSalutation checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtSalutation textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowSalutation.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvsalutation').style.display = '';";
                    jscript += "document.getElementById('dvsumsalutation').style.display = '';";
                    jscript += "document.getElementById('" + txtSalutation.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowSalutation.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompSalutation.Checked = true;
                }
                else
                {
                    chkcompSalutation.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumSalutation.Checked = true;
                }
                else
                {
                    chksumSalutation.Checked = false;
                }

                txtSalutation.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Salutation;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrSalutation.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=FName & set chkshowFname and chkcompFname checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtFname textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _FName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowFname.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvfname').style.display = '';";
                    jscript += "document.getElementById('dvsumfname').style.display = '';";
                    jscript += "document.getElementById('" + txtFname.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowFname.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompFname.Checked = true;
                }
                else
                {
                    chkcompFname.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumFname.Checked = true;
                }
                else
                {
                    chksumFname.Checked = false;
                }

                txtFname.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_FName;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrFname.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=LName & set chkshowLname and chkcompLname checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtLname textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _LName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowLname.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvlname').style.display = '';";
                    jscript += "document.getElementById('dvsumlname').style.display = '';";
                    jscript += "document.getElementById('" + txtLname.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowLname.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompLname.Checked = true;
                }
                else
                {
                    chkcompLname.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumLname.Checked = true;
                }
                else
                {
                    chksumLname.Checked = false;
                }

                txtLname.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_LName;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrLname.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Oname & set chkshowOname and chkcompOname checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtOname textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Oname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowOname.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvoname').style.display = '';";
                    jscript += "document.getElementById('dvsumoname').style.display = '';";
                    jscript += "document.getElementById('" + txtOname.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowOname.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompOname.Checked = true;
                }
                else
                {
                    chkcompOname.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumOname.Checked = true;
                }
                else
                {
                    chksumOname.Checked = false;
                }

                txtOname.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_OName;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrOname.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=PassportNo & set chkshowPassportNo and chkcompPassportNo checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtPassportNo textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassportNo)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowPassportNo.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvpassportno').style.display = '';";
                    jscript += "document.getElementById('dvsumpassportno').style.display = '';";
                    jscript += "document.getElementById('" + txtPassportNo.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowPassportNo.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompPassportNo.Checked = true;
                }
                else
                {
                    chkcompPassportNo.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumPassportNo.Checked = true;
                }
                else
                {
                    chksumPassportNo.Checked = false;
                }

                txtPassportNo.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_PassNo;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrPassportNo.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=IsRegistered & set chkshowIsRegistered and chkcompIsRegistered checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtIsRegistered textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IsRegistered)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowIsRegistered.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvisregistered').style.display = '';";
                    jscript += "document.getElementById('dvsumisregistered').style.display = '';";
                    jscript += "document.getElementById('" + txtIsRegistered.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowIsRegistered.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompIsRegistered.Checked = true;
                }
                else
                {
                    chkcompIsRegistered.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumIsRegistered.Checked = true;
                }
                else
                {
                    chksumIsRegistered.Checked = false;
                }

                txtIsRegistered.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_isReg;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrIsRegistered.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=RegSpecific & set chkshowRegSpecific and chkcompRegSpecific checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtRegSpecific textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RegSpecific)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowRegSpecific.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvregspecific').style.display = '';";
                    jscript += "document.getElementById('dvsumregspecific').style.display = '';";
                    jscript += "document.getElementById('" + txtRegSpecific.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowRegSpecific.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompRegSpecific.Checked = true;
                }
                else
                {
                    chkcompRegSpecific.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumRegSpecific.Checked = true;
                }
                else
                {
                    chksumRegSpecific.Checked = false;
                }

                txtRegSpecific.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_sgregistered;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrRegSpecific.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=IDNo & set chkshowIDNo and chkcompIDNo checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtIDNo textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowIDNo.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvidno').style.display = '';";
                    jscript += "document.getElementById('dvsumidno').style.display = '';";
                    jscript += "document.getElementById('" + txtIDNo.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowIDNo.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompIDNo.Checked = true;
                }
                else
                {
                    chkcompIDNo.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumIDNo.Checked = true;
                }
                else
                {
                    chksumIDNo.Checked = false;
                }

                txtIDNo.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_IDno;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrIDNo.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Designation & set chkshowDesignation and chkcompDesignation checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtDesignation textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowDesignation.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvdesignation').style.display = '';";
                    jscript += "document.getElementById('dvsumdesignation').style.display = '';";
                    jscript += "document.getElementById('" + txtDesignation.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowDesignation.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompDesignation.Checked = true;
                }
                else
                {
                    chkcompDesignation.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumDesignation.Checked = true;
                }
                else
                {
                    chksumDesignation.Checked = false;
                }

                txtDesignation.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Designation;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrDesignation.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Profession & set chkshowProfession and chkcompProfession checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtProfession textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowProfession.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvprofession').style.display = '';";
                    jscript += "document.getElementById('dvsumprofession').style.display = '';";
                    jscript += "document.getElementById('" + txtProfession.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowProfession.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompProfession.Checked = true;
                }
                else
                {
                    chkcompProfession.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumProfession.Checked = true;
                }
                else
                {
                    chksumProfession.Checked = false;
                }

                txtProfession.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Profession;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrProfession.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Department & set chkshowDepartment and chkcompDepartment checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtDepartment textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowDepartment.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvdepartment').style.display = '';";
                    jscript += "document.getElementById('dvsumdepartment').style.display = '';";
                    jscript += "document.getElementById('" + txtDepartment.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowDepartment.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompDepartment.Checked = true;
                }
                else
                {
                    chkcompDepartment.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumDepartment.Checked = true;
                }
                else
                {
                    chksumDepartment.Checked = false;
                }

                txtDepartment.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Department;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrDepartment.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Organization & set chkshowOrganization and chkcompOrganization checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtOrganization textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowOrganization.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvorganization').style.display = '';";
                    jscript += "document.getElementById('dvsumorganization').style.display = '';";
                    jscript += "document.getElementById('" + txtOrganization.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowOrganization.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompOrganization.Checked = true;
                }
                else
                {
                    chkcompOrganization.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumOrganization.Checked = true;
                }
                else
                {
                    chksumOrganization.Checked = false;
                }

                txtOrganization.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Organization;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrOrganization.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Institution & set chkshowInstitution and chkcompInstitution checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtInstitution textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowInstitution.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvinstitution').style.display = '';";
                    jscript += "document.getElementById('dvsuminstitution').style.display = '';";
                    jscript += "document.getElementById('" + txtInstitution.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowInstitution.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompInstitution.Checked = true;
                }
                else
                {
                    chkcompInstitution.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumInstitution.Checked = true;
                }
                else
                {
                    chksumInstitution.Checked = false;
                }

                txtInstitution.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Institution;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrInstitution.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Address1 & set chkshowAddress1 and chkcompAddress1 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAddress1 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAddress1.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaddress1').style.display = '';";
                    jscript += "document.getElementById('dvsumaddress1').style.display = '';";
                    jscript += "document.getElementById('" + txtAddress1.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAddress1.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAddress1.Checked = true;
                }
                else
                {
                    chkcompAddress1.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAddress1.Checked = true;
                }
                else
                {
                    chksumAddress1.Checked = false;
                }

                txtAddress1.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Address1;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrAddress1.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Address2 & set chkshowAddress2 and chkcompAddress2 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAddress2 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAddress2.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaddress2').style.display = '';";
                    jscript += "document.getElementById('dvsumaddress2').style.display = '';";
                    jscript += "document.getElementById('" + txtAddress2.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAddress2.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAddress2.Checked = true;
                }
                else
                {
                    chkcompAddress2.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAddress2.Checked = true;
                }
                else
                {
                    chksumAddress2.Checked = false;
                }

                txtAddress2.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Address2;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrAddress2.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Address3 & set chkshowAddress3 and chkcompAddress3 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAddress3 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAddress3.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaddress3').style.display = '';";
                    jscript += "document.getElementById('dvsumaddress3').style.display = '';";
                    jscript += "document.getElementById('" + txtAddress3.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAddress3.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAddress3.Checked = true;
                }
                else
                {
                    chkcompAddress3.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAddress3.Checked = true;
                }
                else
                {
                    chksumAddress3.Checked = false;
                }

                txtAddress3.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Address3;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrAddress3.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Address4 & set chkshowAddress4 and chkcompAddress4 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAddress4 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAddress4.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaddress4').style.display = '';";
                    jscript += "document.getElementById('dvsumaddress4').style.display = '';";
                    jscript += "document.getElementById('" + txtAddress4.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAddress4.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAddress4.Checked = true;
                }
                else
                {
                    chkcompAddress4.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAddress4.Checked = true;
                }
                else
                {
                    chksumAddress4.Checked = false;
                }

                txtAddress4.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Address4;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrAddress4.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=City & set chkshowCity and chkcompCity checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtCity textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowCity.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvcity').style.display = '';";
                    jscript += "document.getElementById('dvsumcity').style.display = '';";
                    jscript += "document.getElementById('" + txtCity.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowCity.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompCity.Checked = true;
                }
                else
                {
                    chkcompCity.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumCity.Checked = true;
                }
                else
                {
                    chksumCity.Checked = false;
                }

                txtCity.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_City;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrCity.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=State & set chkshowState and chkcompState checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtState textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowState.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvstate').style.display = '';";
                    jscript += "document.getElementById('dvsumstate').style.display = '';";
                    jscript += "document.getElementById('" + txtState.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowState.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompState.Checked = true;
                }
                else
                {
                    chkcompState.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumState.Checked = true;
                }
                else
                {
                    chksumState.Checked = false;
                }

                txtState.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_State;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrState.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=PostalCode & set chkshowPostalCode and chkcompPostalCode checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtPostalCode textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowPostalCode.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvpostalcode').style.display = '';";
                    jscript += "document.getElementById('dvsumpostalcode').style.display = '';";
                    jscript += "document.getElementById('" + txtPostalCode.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowPostalCode.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompPostalCode.Checked = true;
                }
                else
                {
                    chkcompPostalCode.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumPostalCode.Checked = true;
                }
                else
                {
                    chksumPostalCode.Checked = false;
                }

                txtPostalCode.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_PostalCode;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrPostalCode.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Country & set chkshowCountry and chkcompCountry checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtCountry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowCountry.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvcountry').style.display = '';";
                    jscript += "document.getElementById('dvsumcountry').style.display = '';";
                    jscript += "document.getElementById('" + txtCountry.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowCountry.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompCountry.Checked = true;
                }
                else
                {
                    chkcompCountry.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumCountry.Checked = true;
                }
                else
                {
                    chksumCountry.Checked = false;
                }

                txtCountry.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Country;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrCountry.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=RCountry & set chkshowRCountry and chkcompRCountry checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtRCountry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowRCountry.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvrcountry').style.display = '';";
                    jscript += "document.getElementById('dvsumrcountry').style.display = '';";
                    jscript += "document.getElementById('" + txtRCountry.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowRCountry.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompRCountry.Checked = true;
                }
                else
                {
                    chkcompRCountry.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumRCountry.Checked = true;
                }
                else
                {
                    chksumRCountry.Checked = false;
                }

                txtRCountry.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_RCountry;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrRCountry.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Telcc & set chkshowTelCC and chkcompTelCC checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtTelCC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowTelCC.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvtelcc').style.display = '';";
                    jscript += "document.getElementById('dvsumtelcc').style.display = '';";
                    jscript += "document.getElementById('" + txtTelCC.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowTelCC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompTelCC.Checked = true;
                }
                else
                {
                    chkcompTelCC.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumTelCC.Checked = true;
                }
                else
                {
                    chksumTelCC.Checked = false;
                }

                txtTelCC.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Telcc;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrTelCC.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Telac & set chkshowTelAC and chkcompTelAC checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtTelAC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowTelAC.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvtelac').style.display = '';";
                    jscript += "document.getElementById('dvsumtelac').style.display = '';";
                    jscript += "document.getElementById('" + txtTelAC.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowTelAC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompTelAC.Checked = true;
                }
                else
                {
                    chkcompTelAC.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumTelAC.Checked = true;
                }
                else
                {
                    chksumTelAC.Checked = false;
                }

                txtTelAC.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Telac;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrTelAC.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Tel & set chkshowTel and chkcompTel checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtTel textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowTel.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvtel').style.display = '';";
                    jscript += "document.getElementById('dvsumtel').style.display = '';";
                    jscript += "document.getElementById('" + txtTel.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowTel.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompTel.Checked = true;
                }
                else
                {
                    chkcompTel.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumTel.Checked = true;
                }
                else
                {
                    chksumTel.Checked = false;
                }

                txtTel.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Tel;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrTel.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Mobilecc & set chkshowMobileCC and chkcompMobileCC checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtMobileCC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowMobileCC.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvmobilecc').style.display = '';";
                    jscript += "document.getElementById('dvsummobilecc').style.display = '';";
                    jscript += "document.getElementById('" + txtMobileCC.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowMobileCC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompMobileCC.Checked = true;
                }
                else
                {
                    chkcompMobileCC.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumMobileCC.Checked = true;
                }
                else
                {
                    chksumMobileCC.Checked = false;
                }

                txtMobileCC.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Mobcc;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrMobileCC.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Mobileac & set chkshowMobileAC and chkcompMobileAC checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtMobileAC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowMobileAC.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvmobileac').style.display = '';";
                    jscript += "document.getElementById('dvsummobileac').style.display = '';";
                    jscript += "document.getElementById('" + txtMobileAC.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowMobileAC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompMobileAC.Checked = true;
                }
                else
                {
                    chkcompMobileAC.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumMobileAC.Checked = true;
                }
                else
                {
                    chksumMobileAC.Checked = false;
                }

                txtMobileAC.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Mobac;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrMobileAC.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Mobile & set chkshowMobile and chkcompMobile checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtMobile textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowMobile.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvmobile').style.display = '';";
                    jscript += "document.getElementById('dvsummobile').style.display = '';";
                    jscript += "document.getElementById('" + txtMobile.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowMobile.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompMobile.Checked = true;
                }
                else
                {
                    chkcompMobile.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumMobile.Checked = true;
                }
                else
                {
                    chksumMobile.Checked = false;
                }

                txtMobile.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Mobile;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrMobile.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Faxcc & set chkshowFaxCC and chkcompFaxCC checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtFaxCC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowFaxCC.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvfaxcc').style.display = '';";
                    jscript += "document.getElementById('dvsumfaxcc').style.display = '';";
                    jscript += "document.getElementById('" + txtFaxCC.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowFaxCC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompFaxCC.Checked = true;
                }
                else
                {
                    chkcompFaxCC.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumFaxCC.Checked = true;
                }
                else
                {
                    chksumFaxCC.Checked = false;
                }

                txtFaxCC.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Faxcc;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrFaxCC.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Faxac & set chkshowFaxAC and chkcompFaxAC checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtFaxAC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowFaxAC.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvfaxac').style.display = '';";
                    jscript += "document.getElementById('dvsumfaxac').style.display = '';";
                    jscript += "document.getElementById('" + txtFaxAC.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowFaxAC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompFaxAC.Checked = true;
                }
                else
                {
                    chkcompFaxAC.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumFaxAC.Checked = true;
                }
                else
                {
                    chksumFaxAC.Checked = false;
                }

                txtFaxAC.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Faxac;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrFaxAC.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Fax & set chkshowFax and chkcompFax checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtFax textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowFax.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvfax').style.display = '';";
                    jscript += "document.getElementById('dvsumfax').style.display = '';";
                    jscript += "document.getElementById('" + txtFax.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowFax.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompFax.Checked = true;
                }
                else
                {
                    chkcompFax.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumFax.Checked = true;
                }
                else
                {
                    chksumFax.Checked = false;
                }

                txtFax.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Fax;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrFax.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Email & set chkshowEmail and chkcompEmail checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtEmail textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowEmail.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvemail').style.display = '';";
                    jscript += "document.getElementById('dvsumemail').style.display = '';";
                    jscript += "document.getElementById('" + txtEmail.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowEmail.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompEmail.Checked = true;
                }
                else
                {
                    chkcompEmail.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumEmail.Checked = true;
                }
                else
                {
                    chksumEmail.Checked = false;
                }

                txtEmail.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Email;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrEmail.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=EmailConfirmation & set chkshowEmailConfirmation and chkcompEmailConfirmation checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtEmailConfirmation textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _EmailConfirmation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowEmailConfirmation.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvemailconfirmation').style.display = '';";
                    jscript += "document.getElementById('dvsumemailconfirmation').style.display = '';";
                    jscript += "document.getElementById('" + txtEmailConfirmation.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowEmailConfirmation.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompEmailConfirmation.Checked = true;
                }
                else
                {
                    chkcompEmailConfirmation.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumEmailConfirmation.Checked = true;
                }
                else
                {
                    chksumEmailConfirmation.Checked = false;
                }

                txtEmailConfirmation.Text = inputtext;
            }
            #endregion

            #region form_input_name=Affiliation & set chkshowAffiliation and chkcompAffiliation checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAffiliation textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAffiliation.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaffiliation').style.display = '';";
                    jscript += "document.getElementById('dvsumaffiliation').style.display = '';";
                    jscript += "document.getElementById('" + txtAffiliation.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAffiliation.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAffiliation.Checked = true;
                }
                else
                {
                    chkcompAffiliation.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAffiliation.Checked = true;
                }
                else
                {
                    chksumAffiliation.Checked = false;
                }

                txtAffiliation.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Affiliation;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrAffiliation.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Dietary & set chkshowDietary and chkcompDietary checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtDietary textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowDietary.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvdietary').style.display = '';";
                    jscript += "document.getElementById('dvsumdietary').style.display = '';";
                    jscript += "document.getElementById('" + txtDietary.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowDietary.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompDietary.Checked = true;
                }
                else
                {
                    chkcompDietary.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumDietary.Checked = true;
                }
                else
                {
                    chksumDietary.Checked = false;
                }

                txtDietary.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Dietary;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrDietary.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Nationality & set chkshowNationality and chkcompNationality checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtNationality textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowNationality.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvnationality').style.display = '';";
                    jscript += "document.getElementById('dvsumnationality').style.display = '';";
                    jscript += "document.getElementById('" + txtNationality.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowNationality.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompNationality.Checked = true;
                }
                else
                {
                    chkcompNationality.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumNationality.Checked = true;
                }
                else
                {
                    chksumNationality.Checked = false;
                }

                txtNationality.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Nationality;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrNationality.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=MembershipNo & set chkshowMembershipNo and chkcompMembershipNo checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtMembershipNo textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowMembershipNo.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvmembershipno').style.display = '';";
                    jscript += "document.getElementById('dvsummembershipno').style.display = '';";
                    jscript += "document.getElementById('" + txtMembershipNo.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowMembershipNo.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompMembershipNo.Checked = true;
                }
                else
                {
                    chkcompMembershipNo.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumMembershipNo.Checked = true;
                }
                else
                {
                    chksumMembershipNo.Checked = false;
                }

                txtMembershipNo.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Membershipno;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrMembershipNo.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=VName & set chkshowVname and chkcompVname checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtVname textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowVname.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvvname').style.display = '';";
                    jscript += "document.getElementById('dvsumvname').style.display = '';";
                    jscript += "document.getElementById('" + txtVname.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowVname.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompVname.Checked = true;
                }
                else
                {
                    chkcompVname.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumVname.Checked = true;
                }
                else
                {
                    chksumVname.Checked = false;
                }

                txtVname.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_vName;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrVname.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=VDOB & set chkshowVDOB and chkcompVDOB checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtVDOB textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowVDOB.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvvdob').style.display = '';";
                    jscript += "document.getElementById('dvsumvdob').style.display = '';";
                    jscript += "document.getElementById('" + txtVDOB.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowVDOB.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompVDOB.Checked = true;
                }
                else
                {
                    chkcompVDOB.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumVDOB.Checked = true;
                }
                else
                {
                    chksumVDOB.Checked = false;
                }

                txtVDOB.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_vDOB;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrVDOB.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=VPassNo & set chkshowVPassNo and chkcompVPassNo checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtVPassNo textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowVPassNo.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvvpassno').style.display = '';";
                    jscript += "document.getElementById('dvsumvpassno').style.display = '';";
                    jscript += "document.getElementById('" + txtVPassNo.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowVPassNo.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompVPassNo.Checked = true;
                }
                else
                {
                    chkcompVPassNo.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumVPassNo.Checked = true;
                }
                else
                {
                    chksumVPassNo.Checked = false;
                }

                txtVPassNo.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_vPassno;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrVPassNo.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=VPassExpiry & set chkshowVPassExpiry and chkcompVPassExpiry checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtVPassExpiry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowVPassExpiry.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvvpassexpiry').style.display = '';";
                    jscript += "document.getElementById('dvsumvpassexpiry').style.display = '';";
                    jscript += "document.getElementById('" + txtVPassExpiry.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowVPassExpiry.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompVPassExpiry.Checked = true;
                }
                else
                {
                    chkcompVPassExpiry.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumVPassExpiry.Checked = true;
                }
                else
                {
                    chksumVPassExpiry.Checked = false;
                }

                txtVPassExpiry.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_vPassexpiry;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrVPassExpiry.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=VPassIssueDate & set chkshowVPassIssueDate and chkcompVPassIssueDate checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtVPassExpiry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowVPassIssueDate.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvvpassissuedate').style.display = '';";
                    jscript += "document.getElementById('dvsumvpassissuedate').style.display = '';";
                    jscript += "document.getElementById('" + txtVPassIssueDate.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowVPassIssueDate.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompVPassIssueDate.Checked = true;
                }
                else
                {
                    chkcompVPassIssueDate.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumVPassIssueDate.Checked = true;
                }
                else
                {
                    chksumVPassIssueDate.Checked = false;
                }

                txtVPassIssueDate.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_vIssueDate;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrVPassIssueDate.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=VEmbarkation & set chkshowVEmbarkation and chkcompVEmbarkation checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtVPassExpiry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowVEmbarkation.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvvembarkation').style.display = '';";
                    jscript += "document.getElementById('dvsumvembarkation').style.display = '';";
                    jscript += "document.getElementById('" + txtVEmbarkation.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowVEmbarkation.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompVEmbarkation.Checked = true;
                }
                else
                {
                    chkcompVEmbarkation.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumVEmbarkation.Checked = true;
                }
                else
                {
                    chksumVEmbarkation.Checked = false;
                }

                txtVEmbarkation.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_vEmbarkation;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrVEmbarkation.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion
            
            #region form_input_name=VArrivalDate & set chkshowVArrivalDate and chkcompVArrivalDate checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtVPassExpiry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowVArrivalDate.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvvarrivaldate').style.display = '';";
                    jscript += "document.getElementById('dvsumvarrivaldate').style.display = '';";
                    jscript += "document.getElementById('" + txtVArrivalDate.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowVArrivalDate.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompVArrivalDate.Checked = true;
                }
                else
                {
                    chkcompVArrivalDate.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumVArrivalDate.Checked = true;
                }
                else
                {
                    chksumVArrivalDate.Checked = false;
                }

                txtVArrivalDate.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_vArrivalDate;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrVArrivalDate.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=VCountry & set chkshowVCountry and chkcompVCountry checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtVCountry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowVCountry.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvvcountry').style.display = '';";
                    jscript += "document.getElementById('dvsumvcountry').style.display = '';";
                    jscript += "document.getElementById('" + txtVCountry.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowVCountry.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompVCountry.Checked = true;
                }
                else
                {
                    chkcompVCountry.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumVCountry.Checked = true;
                }
                else
                {
                    chksumVCountry.Checked = false;
                }

                txtVCountry.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_vCountry;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrVCountry.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=UDF_CName & set chkshowUDF_CName and chkcompUDF_CName checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtUDF_CName textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowUDF_CName.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvudf_cname').style.display = '';";
                    jscript += "document.getElementById('dvsumudf_cname').style.display = '';";
                    jscript += "document.getElementById('" + txtUDF_CName.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowUDF_CName.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompUDF_CName.Checked = true;
                }
                else
                {
                    chkcompUDF_CName.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumUDF_CName.Checked = true;
                }
                else
                {
                    chksumUDF_CName.Checked = false;
                }

                txtUDF_CName.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.UDF_CName;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrUDF_CName.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=UDF_ProfCategory & set chkshowUDF_ProfCategory and chkcompUDF_ProfCategory checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtUDF_ProfCategory textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowUDF_DelegateType.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvudf_delegatetype').style.display = '';";
                    jscript += "document.getElementById('dvsumudf_delegatetype').style.display = '';";
                    jscript += "document.getElementById('" + txtUDF_DelegateType.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowUDF_DelegateType.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompUDF_DelegateType.Checked = true;
                }
                else
                {
                    chkcompUDF_DelegateType.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumUDF_DelegateType.Checked = true;
                }
                else
                {
                    chksumUDF_DelegateType.Checked = false;
                }

                txtUDF_DelegateType.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.UDF_DelegateType;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrUDF_DelegateType.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=UDF_ProfCategory & set chkshowUDF_ProfCategory and chkcompUDF_ProfCategory checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtUDF_ProfCategory textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowUDF_ProfCategory.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvudf_profcategory').style.display = '';";
                    jscript += "document.getElementById('dvsumudf_profcategory').style.display = '';";
                    jscript += "document.getElementById('" + txtUDF_ProfCategory.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowUDF_ProfCategory.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompUDF_ProfCategory.Checked = true;
                }
                else
                {
                    chkcompUDF_ProfCategory.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumUDF_ProfCategory.Checked = true;
                }
                else
                {
                    chksumUDF_ProfCategory.Checked = false;
                }

                txtUDF_ProfCategory.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.UDF_ProfCategory;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrUDF_ProfCategory.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=UDF_ProfCategoryOther & set chkshowUDF_ProfCategoryOther and chkcompUDF_ProfCategoryOther checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtUDF_ProfCategoryOther textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategoryOther)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowUDF_ProfCategoryOther.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvudf_profcategoryother').style.display = '';";
                    jscript += "document.getElementById('dvsumudf_profcategoryother').style.display = '';";
                    jscript += "document.getElementById('" + txtUDF_ProfCategoryOther.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowUDF_ProfCategoryOther.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompUDF_ProfCategoryOther.Checked = true;
                }
                else
                {
                    chkcompUDF_ProfCategoryOther.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumUDF_ProfCategoryOther.Checked = true;
                }
                else
                {
                    chksumUDF_ProfCategoryOther.Checked = false;
                }

                txtUDF_ProfCategoryOther.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.UDF_ProfCategoryOther;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrUDF_ProfCategoryOther.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=UDF_CPcode & set chkshowUDF_CPcode and chkcompUDF_CPcode checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtUDF_CPcode textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowUDF_CPcode.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvudf_cpcode').style.display = '';";
                    jscript += "document.getElementById('dvsumudf_cpcode').style.display = '';";
                    jscript += "document.getElementById('" + txtUDF_CPcode.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowUDF_CPcode.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompUDF_CPcode.Checked = true;
                }
                else
                {
                    chkcompUDF_CPcode.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumUDF_CPcode.Checked = true;
                }
                else
                {
                    chksumUDF_CPcode.Checked = false;
                }

                txtUDF_CPcode.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.UDF_CPcode;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrUDF_CPcode.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=UDF_CLDepartment & set chkshowUDF_CLDepartment and chkcompUDF_CLDepartment checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtUDF_CLDepartment textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowUDF_CLDepartment.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvudf_cldepartment').style.display = '';";
                    jscript += "document.getElementById('dvsumudf_cldepartment').style.display = '';";
                    jscript += "document.getElementById('" + txtUDF_CLDepartment.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowUDF_CLDepartment.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompUDF_CLDepartment.Checked = true;
                }
                else
                {
                    chkcompUDF_CLDepartment.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumUDF_CLDepartment.Checked = true;
                }
                else
                {
                    chksumUDF_CLDepartment.Checked = false;
                }

                txtUDF_CLDepartment.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.UDF_CLDepartment;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrUDF_CLDepartment.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=UDF_CAddress & set chkshowUDF_CAddress and chkcompUDF_CAddress checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtUDF_CAddress textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowUDF_CAddress.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvudf_caddress').style.display = '';";
                    jscript += "document.getElementById('dvsumudf_caddress').style.display = '';";
                    jscript += "document.getElementById('" + txtUDF_CAddress.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowUDF_CAddress.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompUDF_CAddress.Checked = true;
                }
                else
                {
                    chkcompUDF_CAddress.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumUDF_CAddress.Checked = true;
                }
                else
                {
                    chksumUDF_CAddress.Checked = false;
                }

                txtUDF_CAddress.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.UDF_CAddress;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrUDF_CAddress.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=UDF_CLCompany& set chkshowUDF_CLCompany and chkcompUDF_CLCompany checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtUDF_CLCompany textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowUDF_CLCompany.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvudf_clcompany').style.display = '';";
                    jscript += "document.getElementById('dvsumudf_clcompany').style.display = '';";
                    jscript += "document.getElementById('" + txtUDF_CLCompany.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowUDF_CLCompany.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompUDF_CLCompany.Checked = true;
                }
                else
                {
                    chkcompUDF_CLCompany.Checked = false;
                }
                if (isrequired == 1)
                {
                    chksumUDF_CLCompany.Checked = true;
                }
                else
                {
                    chksumUDF_CLCompany.Checked = false;
                }

                txtUDF_CLCompany.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.UDF_CLCompany;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrUDF_CLCompany.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=UDF_CLCompanOther & set chkshowUDF_CLCompanyOther and chkcompUDF_CLCompanyOther checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtUDF_CLCompanyOther textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompanyOther)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowUDF_CLCompanyOther.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvudf_clcompanyother').style.display = '';";
                    jscript += "document.getElementById('dvsumudf_clcompanyother').style.display = '';";
                    jscript += "document.getElementById('" + txtUDF_CLCompanyOther.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowUDF_CLCompanyOther.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompUDF_CLCompanyOther.Checked = true;
                }
                else
                {
                    chkcompUDF_CLCompanyOther.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumUDF_CLCompanyOther.Checked = true;
                }
                else
                {
                    chksumUDF_CLCompanyOther.Checked = false;
                }

                txtUDF_CLCompanyOther.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.UDF_CLCompanyOther;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrUDF_CLCompanyOther.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=UDF_CCountry & set chkshowUDF_CCountry and chkcompUDF_CCountry checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtUDF_CCountry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowUDF_CCountry.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvudf_ccountry').style.display = '';";
                    jscript += "document.getElementById('dvsumudf_ccountry').style.display = '';";
                    jscript += "document.getElementById('" + txtUDF_CCountry.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowUDF_CCountry.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompUDF_CCountry.Checked = true;
                }
                else
                {
                    chkcompUDF_CCountry.Checked = false;
                }
                if (isrequired == 1)
                {
                    chksumUDF_CCountry.Checked = true;
                }
                else
                {
                    chksumUDF_CCountry.Checked = false;
                }

                txtUDF_CCountry.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.UDF_CCountry;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrUDF_CCountry.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=SupervisorName & set chkshowSupervisorName and chkcompSupervisorName checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtSupervisorName textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupervisorName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowSupervisorName.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvsupervisorname').style.display = '';";
                    jscript += "document.getElementById('dvsumsupervisorname').style.display = '';";
                    jscript += "document.getElementById('" + txtSupervisorName.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowSupervisorName.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompSupervisorName.Checked = true;
                }
                else
                {
                    chkcompSupervisorName.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumSupervisorName.Checked = true;
                }
                else
                {
                    chksumSupervisorName.Checked = false;
                }

                txtSupervisorName.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_SupervisorName;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrSupervisorName.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=SupervisorDesignation & set chkshowSupervisorDesignation and chkcompSupervisorDesignation checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtSupervisorDesignation textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupervisorDesignation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowSupervisorDesignation.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvsupervisordesignation').style.display = '';";
                    jscript += "document.getElementById('dvsumsupervisordesignation').style.display = '';";
                    jscript += "document.getElementById('" + txtSupervisorDesignation.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowSupervisorDesignation.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompSupervisorDesignation.Checked = true;
                }
                else
                {
                    chkcompSupervisorDesignation.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumSupervisorDesignation.Checked = true;
                }
                else
                {
                    chksumSupervisorDesignation.Checked = false;
                }

                txtSupervisorDesignation.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_SupervisorDesignation;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrSupervisorDesignation.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=SupervisorContact & set chkshowSupervisorContact and chkcompSupervisorContact checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtSupervisorContact textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupervisorContact)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowSupervisorContact.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvsupervisorcontact').style.display = '';";
                    jscript += "document.getElementById('dvsumsupervisorcontact').style.display = '';";
                    jscript += "document.getElementById('" + txtSupervisorContact.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowSupervisorContact.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompSupervisorContact.Checked = true;
                }
                else
                {
                    chkcompSupervisorContact.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumSupervisorContact.Checked = true;
                }
                else
                {
                    chksumSupervisorContact.Checked = false;
                }

                txtSupervisorContact.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_SupervisorContact;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrSupervisorContact.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=SupervisorEmail & set chkshowSupervisorEmail and chkcompSupervisorEmail checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtSupervisorEmail textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupervisorEmail)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowSupervisorEmail.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvsupervisoremail').style.display = '';";
                    jscript += "document.getElementById('dvsumsupervisoremail').style.display = '';";
                    jscript += "document.getElementById('" + txtSupervisorEmail.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowSupervisorEmail.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompSupervisorEmail.Checked = true;
                }
                else
                {
                    chkcompSupervisorEmail.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumSupervisorEmail.Checked = true;
                }
                else
                {
                    chksumSupervisorEmail.Checked = false;
                }

                txtSupervisorEmail.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_SupervisorEmail;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrSupervisorEmail.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=OtherSalutation & set chkshowOtherSalutation and chkcompOtherSalutation checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtOtherSalutation textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OtherSalutation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowOtherSalutation.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvothersalutation').style.display = '';";
                    jscript += "document.getElementById('dvsumothersalutation').style.display = '';";
                    jscript += "document.getElementById('" + txtOtherSalutation.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowOtherSalutation.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompOtherSalutation.Checked = true;
                }
                else
                {
                    chkcompOtherSalutation.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumOtherSalutation.Checked = true;
                }
                else
                {
                    chksumOtherSalutation.Checked = false;
                }

                txtOtherSalutation.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_SalutationOthers;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrOtherSalutation.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=OtherProfession & set chkshowOtherProfession and chkcompOtherProfession checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtOtherProfession textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OtherProfession)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowOtherProfession.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvotherprofession').style.display = '';";
                    jscript += "document.getElementById('dvsumotherprofession').style.display = '';";
                    jscript += "document.getElementById('" + txtOtherProfession.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowOtherProfession.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompOtherProfession.Checked = true;
                }
                else
                {
                    chkcompOtherProfession.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumOtherProfession.Checked = true;
                }
                else
                {
                    chksumOtherProfession.Checked = false;
                }

                txtOtherProfession.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_otherProfession;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrOtherProfession.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=OtherDepartment & set chkshowOtherDepartment and chkcompOtherDepartment checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtOtherDepartment textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OtherDepartment)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowOtherDepartment.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvotherdepartment').style.display = '';";
                    jscript += "document.getElementById('dvsumotherdepartment').style.display = '';";
                    jscript += "document.getElementById('" + txtOtherDepartment.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowOtherDepartment.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompOtherDepartment.Checked = true;
                }
                else
                {
                    chkcompOtherDepartment.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumOtherDepartment.Checked = true;
                }
                else
                {
                    chksumOtherDepartment.Checked = false;
                }

                txtOtherDepartment.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_otherDepartment;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrOtherDepartment.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=OtherOrganization & set chkshowOtherOrganization and chkcompOtherOrganization checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtOtherOrganization textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OtherOrganization)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowOtherOrganization.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvotherorganization').style.display = '';";
                    jscript += "document.getElementById('dvsumotherorganization').style.display = '';";
                    jscript += "document.getElementById('" + txtOtherOrganization.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowOtherOrganization.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompOtherOrganization.Checked = true;
                }
                else
                {
                    chkcompOtherOrganization.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumOtherOrganization.Checked = true;
                }
                else
                {
                    chksumOtherOrganization.Checked = false;
                }

                txtOtherOrganization.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_otherOrganization;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrOtherOrganization.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=OtherInstitution & set chkshowOtherInstitution and chkcompOtherInstitution checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtOtherInstitution textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OtherInstitution)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowOtherInstitution.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvotherinstitution').style.display = '';";
                    jscript += "document.getElementById('dvsumotherinstitution').style.display = '';";
                    jscript += "document.getElementById('" + txtOtherInstitution.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowOtherInstitution.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompOtherInstitution.Checked = true;
                }
                else
                {
                    chkcompOtherInstitution.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumOtherInstitution.Checked = true;
                }
                else
                {
                    chksumOtherInstitution.Checked = false;
                }

                txtOtherInstitution.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_otherInstitution;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrOtherInstitution.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=AEmail & set chkshowAEmail and chkcompAEmail checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAEmail textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _AEmail)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAEmail.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaemail').style.display = '';";
                    jscript += "document.getElementById('dvsumaemail').style.display = '';";
                    jscript += "document.getElementById('" + txtAEmail.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAEmail.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAEmail.Checked = true;
                }
                else
                {
                    chkcompAEmail.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAEmail.Checked = true;
                }
                else
                {
                    chksumAEmail.Checked = false;
                }

                txtAEmail.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_aemail;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrAEmail.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=IsSMS & set chkshowIsSMS and chkcompIsSMS checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtIsSMS textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IsSMS)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowIsSMS.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvissms').style.display = '';";
                    jscript += "document.getElementById('dvsumissms').style.display = '';";
                    jscript += "document.getElementById('" + txtIsSMS.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowIsSMS.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompIsSMS.Checked = true;
                }
                else
                {
                    chkcompIsSMS.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumIsSMS.Checked = true;
                }
                else
                {
                    chksumIsSMS.Checked = false;
                }

                txtIsSMS.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_isSMS;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrIsSMS.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Age & set chkshowAge and chkcompAge checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAge textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAge.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvage').style.display = '';";
                    jscript += "document.getElementById('dvsumage').style.display = '';";
                    jscript += "document.getElementById('" + txtAge.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAge.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAge.Checked = true;
                }
                else
                {
                    chkcompAge.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAge.Checked = true;
                }
                else
                {
                    chksumAge.Checked = false;
                }

                txtAge.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Age;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrAge.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Gender & set chkshowGender and chkcompGender checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtGender textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowGender.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvgender').style.display = '';";
                    jscript += "document.getElementById('dvsumgender').style.display = '';";
                    jscript += "document.getElementById('" + txtGender.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowGender.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompGender.Checked = true;
                }
                else
                {
                    chkcompGender.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumGender.Checked = true;
                }
                else
                {
                    chksumGender.Checked = false;
                }

                txtGender.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Gender;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrGender.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Additional3 & set chkshowAdditional3 and chkcompAdditional3 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional3 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowDOB.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvdob').style.display = '';";
                    jscript += "document.getElementById('dvsumdob').style.display = '';";
                    jscript += "document.getElementById('" + txtDOB.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowDOB.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompDOB.Checked = true;
                }
                else
                {
                    chkcompDOB.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumDOB.Checked = true;
                }
                else
                {
                    chksumDOB.Checked = false;
                }

                txtDOB.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_DOB;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrDOB.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Additional4 & set chkshowAdditional4 and chkcompAdditional4 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional4 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAdditional4.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvadditional4').style.display = '';";
                    jscript += "document.getElementById('dvsumadditional4').style.display = '';";
                    jscript += "document.getElementById('" + txtAdditional4.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAdditional4.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAdditional4.Checked = true;
                }
                else
                {
                    chkcompAdditional4.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAdditional4.Checked = true;
                }
                else
                {
                    chksumAdditional4.Checked = false;
                }

                txtAdditional4.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Additional4;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrAdditional4.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion

            #region form_input_name=Additional5 & set chkshowAdditional5 and chkcompAdditional5 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional5 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAdditional5.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvadditional5').style.display = '';";
                    jscript += "document.getElementById('dvsumadditional5').style.display = '';";
                    jscript += "document.getElementById('" + txtAdditional5.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAdditional5.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAdditional5.Checked = true;
                }
                else
                {
                    chkcompAdditional5.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAdditional5.Checked = true;
                }
                else
                {
                    chksumAdditional5.Checked = false;
                }

                txtAdditional5.Text = inputtext;

                
                spar4.Value = tbRegDelegateFields.reg_Additional5;
                
                DataTable dt = fn.GetDatasetByCommand("Select FieldOrder From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID And FieldID=@FieldID", "ds", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtqrAdditional5.Text = dt.Rows[0]["FieldOrder"].ToString();
                }
            }
            #endregion
        }
        ClientScript.RegisterStartupScript(this.GetType(), "show", sb.ToString(), true);
    }
    #endregion

    #region SaveForm and using tb_Form table & Update tb_Form table set form_input_isshow, form_input_isrequired and form_input_text fields according to form_input_name
    protected void SaveForm(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = cFUnz.DecryptValue(Request.QueryString["SHW"].ToString());
            string flowid = cFUnz.DecryptValue(Request.QueryString["FLW"].ToString());

            try
            {
                #region Declaration
                string strshowSalutation = "0";
                string strcompSalutation = "0";
                string strSalutation = string.Empty;
                string strsumSalutation = "0";

                string strshowFname = "0";
                string strcompFname = "0";
                string strFname = string.Empty;
                string strsumFname = "0";

                string strshowLname = "0";
                string strcompLname = "0";
                string strLname = string.Empty;
                string strsumLname = "0";

                string strshowOname = "0";
                string strcompOname = "0";
                string strOname = string.Empty;
                string strsumOname = "0";

                string strshowPassportNo = "0";
                string strcompPassportNo = "0";
                string strPassportNo = string.Empty;
                string strsumPassportNo = "0";

                string strshowIsRegistered = "0";
                string strcompIsRegistered = "0";
                string strIsRegistered = string.Empty;
                string strsumIsRegistered = "0";

                string strshowRegSpecific = "0";
                string strcompRegSpecific = "0";
                string strRegSpecific = string.Empty;
                string strsumRegSpecific = "0";

                string strshowIDNo = "0";
                string strcompIDNo = "0";
                string strIDNo = string.Empty;
                string strsumIDNo = "0";

                string strshowDesignation = "0";
                string strcompDesignation = "0";
                string strDesignation = string.Empty;
                string strsumDesignation = "0";

                string strshowProfession = "0";
                string strcompProfession = "0";
                string strProfession = string.Empty;
                string strsumProfession = "0";

                string strshowDepartment = "0";
                string strcompDepartment = "0";
                string strDepartment = string.Empty;
                string strsumDepartment = "0";

                string strshowOrganization = "0";
                string strcompOrganization = "0";
                string strOrganization = string.Empty;
                string strsumOrganization = "0";

                string strshowInstitution = "0";
                string strcompInstitution = "0";
                string strInstitution = string.Empty;
                string strsumInstitution = "0";

                string strshowAddress1 = "0";
                string strcompAddress1 = "0";
                string strAddress1 = string.Empty;
                string strsumAddress1 = "0";

                string strshowAddress2 = "0";
                string strcompAddress2 = "0";
                string strAddress2 = string.Empty;
                string strsumAddress2 = "0";

                string strshowAddress3 = "0";
                string strcompAddress3 = "0";
                string strAddress3 = string.Empty;
                string strsumAddress3 = "0";

                string strshowAddress4 = "0";
                string strcompAddress4 = "0";
                string strAddress4 = string.Empty;
                string strsumAddress4 = "0";

                string strshowCity = "0";
                string strcompCity = "0";
                string strCity = string.Empty;
                string strsumCity = "0";

                string strshowState = "0";
                string strcompState = "0";
                string strState = string.Empty;
                string strsumState = "0";

                string strshowPostalCode = "0";
                string strcompPostalCode = "0";
                string strPostalCode = string.Empty;
                string strsumPostalCode = "0";

                string strshowCountry = "0";
                string strcompCountry = "0";
                string strCountry = string.Empty;
                string strsumCountry = "0";

                string strshowRCountry = "0";
                string strcompRCountry = "0";
                string strRCountry = string.Empty;
                string strsumRCountry = "0";

                string strshowTelCC = "0";
                string strcompTelCC = "0";
                string strTelCC = string.Empty;
                string strsumTelCC = "0";

                string strshowTelAC = "0";
                string strcompTelAC = "0";
                string strTelAC = string.Empty;
                string strsumTelAC = "0";

                string strshowTel = "0";
                string strcompTel = "0";
                string strTel = string.Empty;
                string strsumTel = "0";

                string strshowMobileCC = "0";
                string strcompMobileCC = "0";
                string strMobileCC = string.Empty;
                string strsumMobileCC = "0";

                string strshowMobileAC = "0";
                string strcompMobileAC = "0";
                string strMobileAC = string.Empty;
                string strsumMobileAC = "0";

                string strshowMobile = "0";
                string strcompMobile = "0";
                string strMobile = string.Empty;
                string strsumMobile = "0";

                string strshowFaxCC = "0";
                string strcompFaxCC = "0";
                string strFaxCC = string.Empty;
                string strsumFaxCC = "0";

                string strshowFaxAC = "0";
                string strcompFaxAC = "0";
                string strFaxAC = string.Empty;
                string strsumFaxAC = "0";

                string strshowFax = "0";
                string strcompFax = "0";
                string strFax = string.Empty;
                string strsumFax = "0";

                string strshowEmail = "0";
                string strcompEmail = "0";
                string strEmail = string.Empty;
                string strsumEmail = "0";

                string strshowEmailConfirmation = "0";
                string strcompEmailConfirmation = "0";
                string strEmailConfirmation = string.Empty;
                string strsumEmailConfirmation = "0";

                string strshowAffiliation = "0";
                string strcompAffiliation = "0";
                string strAffiliation = string.Empty;
                string strsumAffiliation = "0";

                string strshowDietary = "0";
                string strcompDietary = "0";
                string strDietary = string.Empty;
                string strsumDietary = "0";

                string strshowNationality = "0";
                string strcompNationality = "0";
                string strNationality = string.Empty;
                string strsumNationality = "0";

                string strshowMembershipNo = "0";
                string strcompMembershipNo = "0";
                string strMembershipNo = string.Empty;
                string strsumMembershipNo = "0";

                string strshowVname = "0";
                string strcompVname = "0";
                string strVname = string.Empty;
                string strsumVname = "0";

                string strshowVDOB = "0";
                string strcompVDOB = "0";
                string strVDOB = string.Empty;
                string strsumVDOB = "0";

                string strshowVPassNo = "0";
                string strcompVPassNo = "0";
                string strVPassNo = string.Empty;
                string strsumVPassNo = "0";

                string strshowVPassExpiry = "0";
                string strcompVPassExpiry = "0";
                string strVPassExpiry = string.Empty;
                string strsumVPassExpiry = "0";

                string strshowVPassIssueDate = "0";
                string strcompVPassIssueDate = "0";
                string strVPassIssueDate = string.Empty;
                string strsumVPassIssueDate = "0";

                string strshowVEmbarkation = "0";
                string strcompVEmbarkation = "0";
                string strVEmbarkation = string.Empty;
                string strsumVEmbarkation = "0";

                string strshowVArrivalDate = "0";
                string strcompVArrivalDate = "0";
                string strVArrivalDate = string.Empty;
                string strsumVArrivalDate = "0";

                string strshowVCountry = "0";
                string strcompVCountry = "0";
                string strVCountry = string.Empty;
                string strsumVCountry = "0";

                string strshowUDF_CName = "0";
                string strcompUDF_CName = "0";
                string strUDF_CName = string.Empty;
                string strsumUDF_CName = "0";

                string strshowUDF_DelegateType = "0";
                string strcompUDF_DelegateType = "0";
                string strUDF_DelegateType = string.Empty;
                string strsumUDF_DelegateType = "0";

                string strshowUDF_ProfCategory = "0";
                string strcompUDF_ProfCategory = "0";
                string strUDF_ProfCategory = string.Empty;
                string strsumUDF_ProfCategory = "0";

                string strshowUDF_ProfCategoryOther = "0";
                string strcompUDF_ProfCategoryOther = "0";
                string strUDF_ProfCategoryOther = string.Empty;
                string strsumUDF_ProfCategoryOther = "0";

                string strshowUDF_CPcode = "0";
                string strcompUDF_CPcode = "0";
                string strUDF_CPcode = string.Empty;
                string strsumUDF_CPcode = "0";

                string strshowUDF_CLDepartment = "0";
                string strcompUDF_CLDepartment = "0";
                string strUDF_CLDepartment = string.Empty;
                string strsumUDF_CLDepartment = "0";

                string strshowUDF_CAddress = "0";
                string strcompUDF_CAddress = "0";
                string strUDF_CAddress = string.Empty;
                string strsumUDF_CAddress = "0";

                string strshowUDF_CLCompany = "0";
                string strcompUDF_CLCompany = "0";
                string strUDF_CLCompany = string.Empty;
                string strsumUDF_CLCompany = "0";

                string strshowUDF_CLCompanyOther = "0";
                string strcompUDF_CLCompanyOther = "0";
                string strUDF_CLCompanyOther = string.Empty;
                string strsumUDF_CLCompanyOther = "0";

                string strshowUDF_CCountry = "0";
                string strcompUDF_CCountry = "0";
                string strUDF_CCountry = string.Empty;
                string strsumUDF_CCountry = "0";

                string strshowSupervisorName = "0";
                string strcompSupervisorName = "0";
                string strSupervisorName = string.Empty;
                string strsumSupervisorName = "0";

                string strshowSupervisorDesignation = "0";
                string strcompSupervisorDesignation = "0";
                string strSupervisorDesignation = string.Empty;
                string strsumSupervisorDesignation = "0";

                string strshowSupervisorContact = "0";
                string strcompSupervisorContact = "0";
                string strSupervisorContact = string.Empty;
                string strsumSupervisorContact = "0";

                string strshowSupervisorEmail = "0";
                string strcompSupervisorEmail = "0";
                string strSupervisorEmail = string.Empty;
                string strsumSupervisorEmail = "0";

                string strshowOtherSalutation = "0";
                string strcompOtherSalutation = "0";
                string strOtherSalutation = string.Empty;
                string strsumOtherSalutation = "0";

                string strshowOtherProfession = "0";
                string strcompOtherProfession = "0";
                string strOtherProfession = string.Empty;
                string strsumOtherProfession = "0";

                string strshowOtherDepartment = "0";
                string strcompOtherDepartment = "0";
                string strOtherDepartment = string.Empty;
                string strsumOtherDepartment = "0";

                string strshowOtherOrganization = "0";
                string strcompOtherOrganization = "0";
                string strOtherOrganization = string.Empty;
                string strsumOtherOrganization = "0";

                string strshowOtherInstitution = "0";
                string strcompOtherInstitution = "0";
                string strOtherInstitution = string.Empty;
                string strsumOtherInstitution = "0";

                string strshowAEmail = "0";
                string strcompAEmail = "0";
                string strAEmail = string.Empty;
                string strsumAEmail = "0";

                string strshowIsSMS = "0";
                string strcompIsSMS = "0";
                string strIsSMS = string.Empty;
                string strsumIsSMS = "0";

                string strshowAge = "0";
                string strcompAge = "0";
                string strAge = string.Empty;
                string strsumAge = "0";

                string strshowGender = "0";
                string strcompGender = "0";
                string strGender = string.Empty;
                string strsumGender = "0";

                string strshowDOB = "0";
                string strcompDOB = "0";
                string strDOB = string.Empty;
                string strsumDOB = "0";

                string strshowAdditional4 = "0";
                string strcompAdditional4 = "0";
                string strAdditional4 = string.Empty;
                string strsumAdditional4 = "0";

                string strshowAdditional5 = "0";
                string strcompAdditional5 = "0";
                string strAdditional5 = string.Empty;
                string strsumAdditional5 = "0";
                #endregion

                #region set variable values
                if (chkshowSalutation.Checked == true)
                {
                    strshowSalutation = "1";
                    strSalutation = txtSalutation.Text.ToString();
                    if (chkcompSalutation.Checked == true)
                    {
                        strcompSalutation = "1";
                    }
                    else
                    {
                        strcompSalutation = "0";
                    }

                    if (chksumSalutation.Checked == true)
                    {
                        strsumSalutation = "1";
                    }
                    else
                    {
                        strsumSalutation = "0";
                    }
                }
                if (chkshowFname.Checked == true)
                {
                    strshowFname = "1";
                    strFname = txtFname.Text.ToString();
                    if (chkcompFname.Checked == true)
                    {
                        strcompFname = "1";
                    }
                    else
                    {
                        strcompFname = "0";
                    }

                    if (chksumFname.Checked == true)
                    {
                        strsumFname = "1";
                    }
                    else
                    {
                        strsumFname = "0";
                    }
                }
                if (chkshowLname.Checked == true)
                {
                    strshowLname = "1";
                    strLname = txtLname.Text.ToString();
                    if (chkcompLname.Checked == true)
                    {
                        strcompLname = "1";
                    }
                    else
                    {
                        strcompLname = "0";
                    }

                    if (chksumLname.Checked == true)
                    {
                        strsumLname = "1";
                    }
                    else
                    {
                        strsumLname = "0";
                    }
                }
                if (chkshowOname.Checked == true)
                {
                    strshowOname = "1";
                    strOname = txtOname.Text.ToString();
                    if (chkcompOname.Checked == true)
                    {
                        strcompOname = "1";
                    }
                    else
                    {
                        strcompOname = "0";
                    }

                    if (chksumOname.Checked == true)
                    {
                        strsumOname = "1";
                    }
                    else
                    {
                        strsumOname = "0";
                    }
                }
                if (chkshowPassportNo.Checked == true)
                {
                    strshowPassportNo = "1";
                    strPassportNo = txtPassportNo.Text.ToString();
                    if (chkcompPassportNo.Checked == true)
                    {
                        strcompPassportNo = "1";
                    }
                    else
                    {
                        strcompPassportNo = "0";
                    }

                    if (chksumPassportNo.Checked == true)
                    {
                        strsumPassportNo = "1";
                    }
                    else
                    {
                        strsumPassportNo = "0";
                    }
                }
                if (chkshowIsRegistered.Checked == true)
                {
                    strshowIsRegistered = "1";
                    strIsRegistered = txtIsRegistered.Text.ToString();
                    if (chkcompIsRegistered.Checked == true)
                    {
                        strcompIsRegistered = "1";
                    }
                    else
                    {
                        strcompIsRegistered = "0";
                    }

                    if (chksumIsRegistered.Checked == true)
                    {
                        strsumIsRegistered = "1";
                    }
                    else
                    {
                        strsumIsRegistered = "0";
                    }
                }
                if (chkshowRegSpecific.Checked == true)
                {
                    strshowRegSpecific = "1";
                    strRegSpecific = txtRegSpecific.Text.ToString();
                    if (chkcompRegSpecific.Checked == true)
                    {
                        strcompRegSpecific = "1";
                    }
                    else
                    {
                        strcompRegSpecific = "0";
                    }

                    if (chksumRegSpecific.Checked == true)
                    {
                        strsumRegSpecific = "1";
                    }
                    else
                    {
                        strsumRegSpecific = "0";
                    }
                }
                if (chkshowIDNo.Checked == true)
                {
                    strshowIDNo = "1";
                    strIDNo = txtIDNo.Text.ToString();
                    if (chkcompIDNo.Checked == true)
                    {
                        strcompIDNo = "1";
                    }
                    else
                    {
                        strcompIDNo = "0";
                    }

                    if (chksumIDNo.Checked == true)
                    {
                        strsumIDNo = "1";
                    }
                    else
                    {
                        strsumIDNo = "0";
                    }
                }
                if (chkshowDesignation.Checked == true)
                {
                    strshowDesignation = "1";
                    strDesignation = txtDesignation.Text.ToString();
                    if (chkcompDesignation.Checked == true)
                    {
                        strcompDesignation = "1";
                    }
                    else
                    {
                        strcompDesignation = "0";
                    }

                    if (chksumDesignation.Checked == true)
                    {
                        strsumDesignation = "1";
                    }
                    else
                    {
                        strsumDesignation = "0";
                    }
                }
                if (chkshowProfession.Checked == true)
                {
                    strshowProfession = "1";
                    strProfession = txtProfession.Text.ToString();
                    if (chkcompProfession.Checked == true)
                    {
                        strcompProfession = "1";
                    }
                    else
                    {
                        strcompProfession = "0";
                    }

                    if (chksumProfession.Checked == true)
                    {
                        strsumProfession = "1";
                    }
                    else
                    {
                        strsumProfession = "0";
                    }
                }
                if (chkshowDepartment.Checked == true)
                {
                    strshowDepartment = "1";
                    strDepartment = txtDepartment.Text.ToString();
                    if (chkcompDepartment.Checked == true)
                    {
                        strcompDepartment = "1";
                    }
                    else
                    {
                        strcompDepartment = "0";
                    }

                    if (chksumDepartment.Checked == true)
                    {
                        strsumDepartment = "1";
                    }
                    else
                    {
                        strsumDepartment = "0";
                    }
                }
                if (chkshowOrganization.Checked == true)
                {
                    strshowOrganization = "1";
                    strOrganization = txtOrganization.Text.ToString();
                    if (chkcompOrganization.Checked == true)
                    {
                        strcompOrganization = "1";
                    }
                    else
                    {
                        strcompOrganization = "0";
                    }

                    if (chksumOrganization.Checked == true)
                    {
                        strsumOrganization = "1";
                    }
                    else
                    {
                        strsumOrganization = "0";
                    }
                }
                if (chkshowInstitution.Checked == true)
                {
                    strshowInstitution = "1";
                    strInstitution = txtInstitution.Text.ToString();
                    if (chkcompInstitution.Checked == true)
                    {
                        strcompInstitution = "1";
                    }
                    else
                    {
                        strcompInstitution = "0";
                    }

                    if (chksumInstitution.Checked == true)
                    {
                        strsumInstitution = "1";
                    }
                    else
                    {
                        strsumInstitution = "0";
                    }
                }
                if (chkshowAddress1.Checked == true)
                {
                    strshowAddress1 = "1";
                    strAddress1 = txtAddress1.Text.ToString();
                    if (chkcompAddress1.Checked == true)
                    {
                        strcompAddress1 = "1";
                    }
                    else
                    {
                        strcompAddress1 = "0";
                    }

                    if (chksumAddress1.Checked == true)
                    {
                        strsumAddress1 = "1";
                    }
                    else
                    {
                        strsumAddress1 = "0";
                    }
                }
                if (chkshowAddress2.Checked == true)
                {
                    strshowAddress2 = "1";
                    strAddress2 = txtAddress2.Text.ToString();
                    if (chkcompAddress2.Checked == true)
                    {
                        strcompAddress2 = "1";
                    }
                    else
                    {
                        strcompAddress2 = "0";
                    }

                    if (chksumAddress2.Checked == true)
                    {
                        strsumAddress2 = "1";
                    }
                    else
                    {
                        strsumAddress2 = "0";
                    }
                }
                if (chkshowAddress3.Checked == true)
                {
                    strshowAddress3 = "1";
                    strAddress3 = txtAddress3.Text.ToString();
                    if (chkcompAddress3.Checked == true)
                    {
                        strcompAddress3 = "1";
                    }
                    else
                    {
                        strcompAddress3 = "0";
                    }

                    if (chksumAddress3.Checked == true)
                    {
                        strsumAddress3 = "1";
                    }
                    else
                    {
                        strsumAddress3 = "0";
                    }
                }
                if (chkshowAddress4.Checked == true)
                {
                    strshowAddress4 = "1";
                    strAddress4 = txtAddress4.Text.ToString();
                    if (chkcompAddress4.Checked == true)
                    {
                        strcompAddress4 = "1";
                    }
                    else
                    {
                        strcompAddress4 = "0";
                    }

                    if (chksumAddress4.Checked == true)
                    {
                        strsumAddress4 = "1";
                    }
                    else
                    {
                        strsumAddress4 = "0";
                    }
                }
                if (chkshowCity.Checked == true)
                {
                    strshowCity = "1";
                    strCity = txtCity.Text.ToString();
                    if (chkcompCity.Checked == true)
                    {
                        strcompCity = "1";
                    }
                    else
                    {
                        strcompCity = "0";
                    }

                    if (chksumCity.Checked == true)
                    {
                        strsumCity = "1";
                    }
                    else
                    {
                        strsumCity = "0";
                    }
                }
                if (chkshowState.Checked == true)
                {
                    strshowState = "1";
                    strState = txtState.Text.ToString();
                    if (chkcompState.Checked == true)
                    {
                        strcompState = "1";
                    }
                    else
                    {
                        strcompState = "0";
                    }

                    if (chksumState.Checked == true)
                    {
                        strsumState = "1";
                    }
                    else
                    {
                        strsumState = "0";
                    }
                }
                if (chkshowPostalCode.Checked == true)
                {
                    strshowPostalCode = "1";
                    strPostalCode = txtPostalCode.Text.ToString();
                    if (chkcompPostalCode.Checked == true)
                    {
                        strcompPostalCode = "1";
                    }
                    else
                    {
                        strcompPostalCode = "0";
                    }

                    if (chksumPostalCode.Checked == true)
                    {
                        strsumPostalCode = "1";
                    }
                    else
                    {
                        strsumPostalCode = "0";
                    }
                }
                if (chkshowCountry.Checked == true)
                {
                    strshowCountry = "1";
                    strCountry = txtCountry.Text.ToString();
                    if (chkcompCountry.Checked == true)
                    {
                        strcompCountry = "1";
                    }
                    else
                    {
                        strcompCountry = "0";
                    }

                    if (chksumCountry.Checked == true)
                    {
                        strsumCountry = "1";
                    }
                    else
                    {
                        strsumCountry = "0";
                    }
                }
                if (chkshowRCountry.Checked == true)
                {
                    strshowRCountry = "1";
                    strRCountry = txtRCountry.Text.ToString();
                    if (chkcompRCountry.Checked == true)
                    {
                        strcompRCountry = "1";
                    }
                    else
                    {
                        strcompRCountry = "0";
                    }

                    if (chksumRCountry.Checked == true)
                    {
                        strsumRCountry = "1";
                    }
                    else
                    {
                        strsumRCountry = "0";
                    }
                }
                if (chkshowTelCC.Checked == true)
                {
                    strshowTelCC = "1";
                    strTelCC = txtTelCC.Text.ToString();
                    if (chkcompTelCC.Checked == true)
                    {
                        strcompTelCC = "1";
                    }
                    else
                    {
                        strcompTelCC = "0";
                    }

                    if (chksumTelCC.Checked == true)
                    {
                        strsumTelCC = "1";
                    }
                    else
                    {
                        strsumTelCC = "0";
                    }
                }
                if (chkshowTelAC.Checked == true)
                {
                    strshowTelAC = "1";
                    strTelAC = txtTelAC.Text.ToString();
                    if (chkcompTelAC.Checked == true)
                    {
                        strcompTelAC = "1";
                    }
                    else
                    {
                        strcompTelAC = "0";
                    }

                    if (chksumTelAC.Checked == true)
                    {
                        strsumTelAC = "1";
                    }
                    else
                    {
                        strsumTelAC = "0";
                    }
                }
                if (chkshowTel.Checked == true)
                {
                    strshowTel = "1";
                    strTel = txtTel.Text.ToString();
                    if (chkcompTel.Checked == true)
                    {
                        strcompTel = "1";
                    }
                    else
                    {
                        strcompTel = "0";
                    }

                    if (chksumTel.Checked == true)
                    {
                        strsumTel = "1";
                    }
                    else
                    {
                        strsumTel = "0";
                    }
                }
                if (chkshowMobileCC.Checked == true)
                {
                    strshowMobileCC = "1";
                    strMobileCC = txtMobileCC.Text.ToString();
                    if (chkcompMobileCC.Checked == true)
                    {
                        strcompMobileCC = "1";
                    }
                    else
                    {
                        strcompMobileCC = "0";
                    }

                    if (chksumMobileCC.Checked == true)
                    {
                        strsumMobileCC = "1";
                    }
                    else
                    {
                        strsumMobileCC = "0";
                    }
                }
                if (chkshowMobileAC.Checked == true)
                {
                    strshowMobileAC = "1";
                    strMobileAC = txtMobileAC.Text.ToString();
                    if (chkcompMobileAC.Checked == true)
                    {
                        strcompMobileAC = "1";
                    }
                    else
                    {
                        strcompMobileAC = "0";
                    }

                    if (chksumMobileAC.Checked == true)
                    {
                        strsumMobileAC = "1";
                    }
                    else
                    {
                        strsumMobileAC = "0";
                    }
                }
                if (chkshowMobile.Checked == true)
                {
                    strshowMobile = "1";
                    strMobile = txtMobile.Text.ToString();
                    if (chkcompMobile.Checked == true)
                    {
                        strcompMobile = "1";
                    }
                    else
                    {
                        strcompMobile = "0";
                    }

                    if (chksumMobile.Checked == true)
                    {
                        strsumMobile = "1";
                    }
                    else
                    {
                        strsumMobile = "0";
                    }
                }
                if (chkshowFaxCC.Checked == true)
                {
                    strshowFaxCC = "1";
                    strFaxCC = txtFaxCC.Text.ToString();
                    if (chkcompFaxCC.Checked == true)
                    {
                        strcompFaxCC = "1";
                    }
                    else
                    {
                        strcompFaxCC = "0";
                    }

                    if (chksumFaxCC.Checked == true)
                    {
                        strsumFaxCC = "1";
                    }
                    else
                    {
                        strsumFaxCC = "0";
                    }
                }
                if (chkshowFaxAC.Checked == true)
                {
                    strshowFaxAC = "1";
                    strFaxAC = txtFaxAC.Text.ToString();
                    if (chkcompFaxAC.Checked == true)
                    {
                        strcompFaxAC = "1";
                    }
                    else
                    {
                        strcompFaxAC = "0";
                    }

                    if (chksumFaxAC.Checked == true)
                    {
                        strsumFaxAC = "1";
                    }
                    else
                    {
                        strsumFaxAC = "0";
                    }
                }
                if (chkshowFax.Checked == true)
                {
                    strshowFax = "1";
                    strFax = txtFax.Text.ToString();
                    if (chkcompFax.Checked == true)
                    {
                        strcompFax = "1";
                    }
                    else
                    {
                        strcompFax = "0";
                    }

                    if (chksumFax.Checked == true)
                    {
                        strsumFax = "1";
                    }
                    else
                    {
                        strsumFax = "0";
                    }
                }
                if (chkshowEmail.Checked == true)
                {
                    strshowEmail = "1";
                    strEmail = txtEmail.Text.ToString();
                    if (chkcompEmail.Checked == true)
                    {
                        strcompEmail = "1";
                    }
                    else
                    {
                        strcompEmail = "0";
                    }

                    if (chksumEmail.Checked == true)
                    {
                        strsumEmail = "1";
                    }
                    else
                    {
                        strsumEmail = "0";
                    }
                }
                if (chkshowEmailConfirmation.Checked == true)
                {
                    strshowEmailConfirmation = "1";
                    strEmailConfirmation = txtEmailConfirmation.Text.ToString();
                    if (chkcompEmailConfirmation.Checked == true)
                    {
                        strcompEmailConfirmation = "1";
                    }
                    else
                    {
                        strcompEmailConfirmation = "0";
                    }

                    if (chksumEmailConfirmation.Checked == true)
                    {
                        strsumEmailConfirmation = "1";
                    }
                    else
                    {
                        strsumEmailConfirmation = "0";
                    }
                }
                if (chkshowAffiliation.Checked == true)
                {
                    strshowAffiliation = "1";
                    strAffiliation = txtAffiliation.Text.ToString();
                    if (chkcompAffiliation.Checked == true)
                    {
                        strcompAffiliation = "1";
                    }
                    else
                    {
                        strcompAffiliation = "0";
                    }

                    if (chksumAffiliation.Checked == true)
                    {
                        strsumAffiliation = "1";
                    }
                    else
                    {
                        strsumAffiliation = "0";
                    }
                }
                if (chkshowDietary.Checked == true)
                {
                    strshowDietary = "1";
                    strDietary = txtDietary.Text.ToString();
                    if (chkcompDietary.Checked == true)
                    {
                        strcompDietary = "1";
                    }
                    else
                    {
                        strcompDietary = "0";
                    }

                    if (chksumDietary.Checked == true)
                    {
                        strsumDietary = "1";
                    }
                    else
                    {
                        strsumDietary = "0";
                    }
                }
                if (chkshowNationality.Checked == true)
                {
                    strshowNationality = "1";
                    strNationality = txtNationality.Text.ToString();
                    if (chkcompNationality.Checked == true)
                    {
                        strcompNationality = "1";
                    }
                    else
                    {
                        strcompNationality = "0";
                    }

                    if (chksumNationality.Checked == true)
                    {
                        strsumNationality = "1";
                    }
                    else
                    {
                        strsumNationality = "0";
                    }
                }
                if (chkshowMembershipNo.Checked == true)
                {
                    strshowMembershipNo = "1";
                    strMembershipNo = txtMembershipNo.Text.ToString();
                    if (chkcompMembershipNo.Checked == true)
                    {
                        strcompMembershipNo = "1";
                    }
                    else
                    {
                        strcompMembershipNo = "0";
                    }

                    if (chksumMembershipNo.Checked == true)
                    {
                        strsumMembershipNo = "1";
                    }
                    else
                    {
                        strsumMembershipNo = "0";
                    }
                }
                if (chkshowVname.Checked == true)
                {
                    strshowVname = "1";
                    strVname = txtVname.Text.ToString();
                    if (chkcompVname.Checked == true)
                    {
                        strcompVname = "1";
                    }
                    else
                    {
                        strcompVname = "0";
                    }

                    if (chksumVname.Checked == true)
                    {
                        strsumVname = "1";
                    }
                    else
                    {
                        strsumVname = "0";
                    }
                }
                if (chkshowVDOB.Checked == true)
                {
                    strshowVDOB = "1";
                    strVDOB = txtVDOB.Text.ToString();
                    if (chkcompVDOB.Checked == true)
                    {
                        strcompVDOB = "1";
                    }
                    else
                    {
                        strcompVDOB = "0";
                    }

                    if (chksumVDOB.Checked == true)
                    {
                        strsumVDOB = "1";
                    }
                    else
                    {
                        strsumVDOB = "0";
                    }
                }
                if (chkshowVPassNo.Checked == true)
                {
                    strshowVPassNo = "1";
                    strVPassNo = txtVPassNo.Text.ToString();
                    if (chkcompVPassNo.Checked == true)
                    {
                        strcompVPassNo = "1";
                    }
                    else
                    {
                        strcompVPassNo = "0";
                    }

                    if (chksumVPassNo.Checked == true)
                    {
                        strsumVPassNo = "1";
                    }
                    else
                    {
                        strsumVPassNo = "0";
                    }
                }
                if (chkshowVPassExpiry.Checked == true)
                {
                    strshowVPassExpiry = "1";
                    strVPassExpiry = txtVPassExpiry.Text.ToString();
                    if (chkcompVPassExpiry.Checked == true)
                    {
                        strcompVPassExpiry = "1";
                    }
                    else
                    {
                        strcompVPassExpiry = "0";
                    }

                    if (chksumVPassExpiry.Checked == true)
                    {
                        strsumVPassExpiry = "1";
                    }
                    else
                    {
                        strsumVPassExpiry = "0";
                    }
                }
                if (chkshowVPassIssueDate.Checked == true)
                {
                    strshowVPassIssueDate = "1";
                    strVPassIssueDate = txtVPassIssueDate.Text.ToString();
                    if (chkcompVPassIssueDate.Checked == true)
                    {
                        strcompVPassIssueDate = "1";
                    }
                    else
                    {
                        strcompVPassIssueDate = "0";
                    }

                    if (chksumVPassIssueDate.Checked == true)
                    {
                        strsumVPassIssueDate = "1";
                    }
                    else
                    {
                        strsumVPassIssueDate = "0";
                    }
                }
                if (chkshowVEmbarkation.Checked == true)
                {
                    strshowVEmbarkation = "1";
                    strVEmbarkation = txtVEmbarkation.Text.ToString();
                    if (chkcompVEmbarkation.Checked == true)
                    {
                        strcompVEmbarkation = "1";
                    }
                    else
                    {
                        strcompVEmbarkation = "0";
                    }

                    if (chksumVEmbarkation.Checked == true)
                    {
                        strsumVEmbarkation = "1";
                    }
                    else
                    {
                        strsumVEmbarkation = "0";
                    }
                }
                if (chkshowVArrivalDate.Checked == true)
                {
                    strshowVArrivalDate = "1";
                    strVArrivalDate = txtVArrivalDate.Text.ToString();
                    if (chkcompVArrivalDate.Checked == true)
                    {
                        strcompVArrivalDate = "1";
                    }
                    else
                    {
                        strcompVArrivalDate = "0";
                    }

                    if (chksumVArrivalDate.Checked == true)
                    {
                        strsumVArrivalDate = "1";
                    }
                    else
                    {
                        strsumVArrivalDate = "0";
                    }
                }
                if (chkshowVCountry.Checked == true)
                {
                    strshowVCountry = "1";
                    strVCountry = txtVCountry.Text.ToString();
                    if (chkcompVCountry.Checked == true)
                    {
                        strcompVCountry = "1";
                    }
                    else
                    {
                        strcompVCountry = "0";
                    }

                    if (chksumVCountry.Checked == true)
                    {
                        strsumVCountry = "1";
                    }
                    else
                    {
                        strsumVCountry = "0";
                    }
                }
                if (chkshowUDF_CName.Checked == true)
                {
                    strshowUDF_CName = "1";
                    strUDF_CName = txtUDF_CName.Text.ToString();
                    if (chkcompUDF_CName.Checked == true)
                    {
                        strcompUDF_CName = "1";
                    }
                    else
                    {
                        strcompUDF_CName = "0";
                    }

                    if (chksumUDF_CName.Checked == true)
                    {
                        strsumUDF_CName = "1";
                    }
                    else
                    {
                        strsumUDF_CName = "0";
                    }
                }
                if (chkshowUDF_DelegateType.Checked == true)
                {
                    strshowUDF_DelegateType = "1";
                    strUDF_DelegateType = txtUDF_DelegateType.Text.ToString();
                    if (chkcompUDF_DelegateType.Checked == true)
                    {
                        strcompUDF_DelegateType = "1";
                    }
                    else
                    {
                        strcompUDF_DelegateType = "0";
                    }

                    if (chksumUDF_DelegateType.Checked == true)
                    {
                        strsumUDF_DelegateType = "1";
                    }
                    else
                    {
                        strsumUDF_DelegateType = "0";
                    }
                }
                if (chkshowUDF_ProfCategory.Checked == true)
                {
                    strshowUDF_ProfCategory = "1";
                    strUDF_ProfCategory = txtUDF_ProfCategory.Text.ToString();
                    if (chkcompUDF_ProfCategory.Checked == true)
                    {
                        strcompUDF_ProfCategory = "1";
                    }
                    else
                    {
                        strcompUDF_ProfCategory = "0";
                    }

                    if (chksumUDF_ProfCategory.Checked == true)
                    {
                        strsumUDF_ProfCategory = "1";
                    }
                    else
                    {
                        strsumUDF_ProfCategory = "0";
                    }
                }
                if (chkshowUDF_ProfCategoryOther.Checked == true)
                {
                    strshowUDF_ProfCategoryOther = "1";
                    strUDF_ProfCategoryOther = txtUDF_ProfCategoryOther.Text.ToString();
                    if (chkcompUDF_ProfCategoryOther.Checked == true)
                    {
                        strcompUDF_ProfCategoryOther = "1";
                    }
                    else
                    {
                        strcompUDF_ProfCategoryOther = "0";
                    }

                    if (chksumUDF_ProfCategoryOther.Checked == true)
                    {
                        strsumUDF_ProfCategoryOther = "1";
                    }
                    else
                    {
                        strsumUDF_ProfCategoryOther = "0";
                    }
                }
                if (chkshowUDF_CPcode.Checked == true)
                {
                    strshowUDF_CPcode = "1";
                    strUDF_CPcode = txtUDF_CPcode.Text.ToString();
                    if (chkcompUDF_CPcode.Checked == true)
                    {
                        strcompUDF_CPcode = "1";
                    }
                    else
                    {
                        strcompUDF_CPcode = "0";
                    }

                    if (chksumUDF_CPcode.Checked == true)
                    {
                        strsumUDF_CPcode = "1";
                    }
                    else
                    {
                        strsumUDF_CPcode = "0";
                    }
                }
                if (chkshowUDF_CLDepartment.Checked == true)
                {
                    strshowUDF_CLDepartment = "1";
                    strUDF_CLDepartment = txtUDF_CLDepartment.Text.ToString();
                    if (chkcompUDF_CLDepartment.Checked == true)
                    {
                        strcompUDF_CLDepartment = "1";
                    }
                    else
                    {
                        strcompUDF_CLDepartment = "0";
                    }

                    if (chksumUDF_CLDepartment.Checked == true)
                    {
                        strsumUDF_CLDepartment = "1";
                    }
                    else
                    {
                        strsumUDF_CLDepartment = "0";
                    }
                }
                if (chkshowUDF_CAddress.Checked == true)
                {
                    strshowUDF_CAddress = "1";
                    strUDF_CAddress = txtUDF_CAddress.Text.ToString();
                    if (chkcompUDF_CAddress.Checked == true)
                    {
                        strcompUDF_CAddress = "1";
                    }
                    else
                    {
                        strcompUDF_CAddress = "0";
                    }

                    if (chksumUDF_CAddress.Checked == true)
                    {
                        strsumUDF_CAddress = "1";
                    }
                    else
                    {
                        strsumUDF_CAddress = "0";
                    }
                }
                if (chkshowUDF_CLCompany.Checked == true)
                {
                    strshowUDF_CLCompany = "1";
                    strUDF_CLCompany = txtUDF_CLCompany.Text.ToString();
                    if (chkcompUDF_CLCompany.Checked == true)
                    {
                        strcompUDF_CLCompany = "1";
                    }
                    else
                    {
                        strcompUDF_CLCompany = "0";
                    }

                    if (chksumUDF_CLCompany.Checked == true)
                    {
                        strsumUDF_CLCompany = "1";
                    }
                    else
                    {
                        strsumUDF_CLCompany = "0";
                    }
                }
                if (chkshowUDF_CLCompanyOther.Checked == true)
                {
                    strshowUDF_CLCompanyOther = "1";
                    strUDF_CLCompanyOther = txtUDF_CLCompanyOther.Text.ToString();
                    if (chkcompUDF_CLCompanyOther.Checked == true)
                    {
                        strcompUDF_CLCompanyOther = "1";
                    }
                    else
                    {
                        strcompUDF_CLCompanyOther = "0";
                    }

                    if (chksumUDF_CLCompanyOther.Checked == true)
                    {
                        strsumUDF_CLCompanyOther = "1";
                    }
                    else
                    {
                        strsumUDF_CLCompanyOther = "0";
                    }
                }
                if (chkshowUDF_CCountry.Checked == true)
                {
                    strshowUDF_CCountry = "1";
                    strUDF_CCountry = txtUDF_CCountry.Text.ToString();
                    if (chkcompUDF_CCountry.Checked == true)
                    {
                        strcompUDF_CCountry = "1";
                    }
                    else
                    {
                        strcompUDF_CCountry = "0";
                    }

                    if (chksumUDF_CCountry.Checked == true)
                    {
                        strsumUDF_CCountry = "1";
                    }
                    else
                    {
                        strsumUDF_CCountry = "0";
                    }
                }
                if (chkshowSupervisorName.Checked == true)
                {
                    strshowSupervisorName = "1";
                    strSupervisorName = txtSupervisorName.Text.ToString();
                    if (chkcompSupervisorName.Checked == true)
                    {
                        strcompSupervisorName = "1";
                    }
                    else
                    {
                        strcompSupervisorName = "0";
                    }

                    if (chksumSupervisorName.Checked == true)
                    {
                        strsumSupervisorName = "1";
                    }
                    else
                    {
                        strsumSupervisorName = "0";
                    }
                }
                if (chkshowSupervisorDesignation.Checked == true)
                {
                    strshowSupervisorDesignation = "1";
                    strSupervisorDesignation = txtSupervisorDesignation.Text.ToString();
                    if (chkcompSupervisorDesignation.Checked == true)
                    {
                        strcompSupervisorDesignation = "1";
                    }
                    else
                    {
                        strcompSupervisorDesignation = "0";
                    }

                    if (chksumSupervisorDesignation.Checked == true)
                    {
                        strsumSupervisorDesignation = "1";
                    }
                    else
                    {
                        strsumSupervisorDesignation = "0";
                    }
                }
                if (chkshowSupervisorContact.Checked == true)
                {
                    strshowSupervisorContact = "1";
                    strSupervisorContact = txtSupervisorContact.Text.ToString();
                    if (chkcompSupervisorContact.Checked == true)
                    {
                        strcompSupervisorContact = "1";
                    }
                    else
                    {
                        strcompSupervisorContact = "0";
                    }

                    if (chksumSupervisorContact.Checked == true)
                    {
                        strsumSupervisorContact = "1";
                    }
                    else
                    {
                        strsumSupervisorContact = "0";
                    }
                }
                if (chkshowSupervisorEmail.Checked == true)
                {
                    strshowSupervisorEmail = "1";
                    strSupervisorEmail = txtSupervisorEmail.Text.ToString();
                    if (chkcompSupervisorEmail.Checked == true)
                    {
                        strcompSupervisorEmail = "1";
                    }
                    else
                    {
                        strcompSupervisorEmail = "0";
                    }

                    if (chksumSupervisorEmail.Checked == true)
                    {
                        strsumSupervisorEmail = "1";
                    }
                    else
                    {
                        strsumSupervisorEmail = "0";
                    }
                }
                if (chkshowOtherSalutation.Checked == true)
                {
                    strshowOtherSalutation = "1";
                    strOtherSalutation = txtOtherSalutation.Text.ToString();
                    if (chkcompOtherSalutation.Checked == true)
                    {
                        strcompOtherSalutation = "1";
                    }
                    else
                    {
                        strcompOtherSalutation = "0";
                    }

                    if (chksumOtherSalutation.Checked == true)
                    {
                        strsumOtherSalutation = "1";
                    }
                    else
                    {
                        strsumOtherSalutation = "0";
                    }
                }
                if (chkshowOtherProfession.Checked == true)
                {
                    strshowOtherProfession = "1";
                    strOtherProfession = txtOtherProfession.Text.ToString();
                    if (chkcompOtherProfession.Checked == true)
                    {
                        strcompOtherProfession = "1";
                    }
                    else
                    {
                        strcompOtherProfession = "0";
                    }

                    if (chksumOtherProfession.Checked == true)
                    {
                        strsumOtherProfession = "1";
                    }
                    else
                    {
                        strsumOtherProfession = "0";
                    }
                }
                if (chkshowOtherDepartment.Checked == true)
                {
                    strshowOtherDepartment = "1";
                    strOtherDepartment = txtOtherDepartment.Text.ToString();
                    if (chkcompOtherDepartment.Checked == true)
                    {
                        strcompOtherDepartment = "1";
                    }
                    else
                    {
                        strcompOtherDepartment = "0";
                    }

                    if (chksumOtherDepartment.Checked == true)
                    {
                        strsumOtherDepartment = "1";
                    }
                    else
                    {
                        strsumOtherDepartment = "0";
                    }
                }
                if (chkshowOtherOrganization.Checked == true)
                {
                    strshowOtherOrganization = "1";
                    strOtherOrganization = txtOtherOrganization.Text.ToString();
                    if (chkcompOtherOrganization.Checked == true)
                    {
                        strcompOtherOrganization = "1";
                    }
                    else
                    {
                        strcompOtherOrganization = "0";
                    }

                    if (chksumOtherOrganization.Checked == true)
                    {
                        strsumOtherOrganization = "1";
                    }
                    else
                    {
                        strsumOtherOrganization = "0";
                    }
                }
                if (chkshowOtherInstitution.Checked == true)
                {
                    strshowOtherInstitution = "1";
                    strOtherInstitution = txtOtherInstitution.Text.ToString();
                    if (chkcompOtherInstitution.Checked == true)
                    {
                        strcompOtherInstitution = "1";
                    }
                    else
                    {
                        strcompOtherInstitution = "0";
                    }

                    if (chksumOtherInstitution.Checked == true)
                    {
                        strsumOtherInstitution = "1";
                    }
                    else
                    {
                        strsumOtherInstitution = "0";
                    }
                }
                if (chkshowAEmail.Checked == true)
                {
                    strshowAEmail = "1";
                    strAEmail = txtAEmail.Text.ToString();
                    if (chkcompAEmail.Checked == true)
                    {
                        strcompAEmail = "1";
                    }
                    else
                    {
                        strcompAEmail = "0";
                    }

                    if (chksumAEmail.Checked == true)
                    {
                        strsumAEmail = "1";
                    }
                    else
                    {
                        strsumAEmail = "0";
                    }
                }
                if (chkshowIsSMS.Checked == true)
                {
                    strshowIsSMS = "1";
                    strIsSMS = txtIsSMS.Text.ToString();
                    if (chkcompIsSMS.Checked == true)
                    {
                        strcompIsSMS = "1";
                    }
                    else
                    {
                        strcompIsSMS = "0";
                    }

                    if (chksumIsSMS.Checked == true)
                    {
                        strsumIsSMS = "1";
                    }
                    else
                    {
                        strsumIsSMS = "0";
                    }
                }
                if (chkshowAge.Checked == true)
                {
                    strshowAge = "1";
                    strAge = txtAge.Text.ToString();
                    if (chkcompAge.Checked == true)
                    {
                        strcompAge = "1";
                    }
                    else
                    {
                        strcompAge = "0";
                    }

                    if (chksumAge.Checked == true)
                    {
                        strsumAge = "1";
                    }
                    else
                    {
                        strsumAge = "0";
                    }
                }
                if (chkshowGender.Checked == true)
                {
                    strshowGender = "1";
                    strGender = txtGender.Text.ToString();
                    if (chkcompGender.Checked == true)
                    {
                        strcompGender = "1";
                    }
                    else
                    {
                        strcompGender = "0";
                    }

                    if (chksumGender.Checked == true)
                    {
                        strsumGender = "1";
                    }
                    else
                    {
                        strsumGender = "0";
                    }
                }
                if (chkshowDOB.Checked == true)
                {
                    strshowDOB = "1";
                    strDOB = txtDOB.Text.ToString();
                    if (chkcompDOB.Checked == true)
                    {
                        strcompDOB = "1";
                    }
                    else
                    {
                        strcompDOB = "0";
                    }

                    if (chksumDOB.Checked == true)
                    {
                        strsumDOB = "1";
                    }
                    else
                    {
                        strsumDOB = "0";
                    }
                }
                if (chkshowAdditional4.Checked == true)
                {
                    strshowAdditional4 = "1";
                    strAdditional4 = txtAdditional4.Text.ToString();
                    if (chkcompAdditional4.Checked == true)
                    {
                        strcompAdditional4 = "1";
                    }
                    else
                    {
                        strcompAdditional4 = "0";
                    }

                    if (chksumAdditional4.Checked == true)
                    {
                        strsumAdditional4 = "1";
                    }
                    else
                    {
                        strsumAdditional4 = "0";
                    }
                }
                if (chkshowAdditional5.Checked == true)
                {
                    strshowAdditional5 = "1";
                    strAdditional5 = txtAdditional5.Text.ToString();
                    if (chkcompAdditional5.Checked == true)
                    {
                        strcompAdditional5 = "1";
                    }
                    else
                    {
                        strcompAdditional5 = "0";
                    }

                    if (chksumAdditional5.Checked == true)
                    {
                        strsumAdditional5 = "1";
                    }
                    else
                    {
                        strsumAdditional5 = "0";
                    }
                }
                #endregion

                string sql1 = "Update tb_Form Set form_input_isshow=" + strshowSalutation + ",form_input_isrequired=" + strcompSalutation + ",form_input_text=N'" + strSalutation + "',form_input_active=" + strsumSalutation + " Where form_input_name='" + _Salutation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFname + ",form_input_isrequired=" + strcompFname + ",form_input_text=N'" + strFname + "',form_input_active=" + strsumFname + " Where form_input_name='" + _FName + "'  And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowLname + ",form_input_isrequired=" + strcompLname + ",form_input_text=N'" + strLname + "',form_input_active=" + strsumLname + " Where form_input_name='" + _LName + "'  And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOname + ",form_input_isrequired=" + strcompOname + ",form_input_text=N'" + strOname + "',form_input_active=" + strsumOname + " Where form_input_name='" + _Oname + "'  And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowPassportNo + ",form_input_isrequired=" + strcompPassportNo + ",form_input_text=N'" + strPassportNo + "',form_input_active=" + strsumPassportNo + " Where form_input_name='" + _PassportNo + "'  And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowIsRegistered + ",form_input_isrequired=" + strcompIsRegistered + ",form_input_text=N'" + strIsRegistered + "',form_input_active=" + strsumIsRegistered + " Where form_input_name='" + _IsRegistered + "'  And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowRegSpecific + ",form_input_isrequired=" + strcompRegSpecific + ",form_input_text=N'" + strRegSpecific + "',form_input_active=" + strsumRegSpecific + " Where form_input_name='" + _RegSpecific + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowIDNo + ",form_input_isrequired=" + strcompIDNo + ",form_input_text=N'" + strIDNo + "',form_input_active=" + strsumIDNo + " Where form_input_name='" + _IDNo + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDesignation + ",form_input_isrequired=" + strcompDesignation + ",form_input_text=N'" + strDesignation + "',form_input_active=" + strsumDesignation + " Where form_input_name='" + _Designation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowProfession + ",form_input_isrequired=" + strcompProfession + ",form_input_text=N'" + strProfession + "',form_input_active=" + strsumProfession + " Where form_input_name='" + _Profession + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDepartment + ",form_input_isrequired=" + strcompDepartment + ",form_input_text=N'" + strDepartment + "',form_input_active=" + strsumDepartment + " Where form_input_name='" + _Department + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOrganization + ",form_input_isrequired=" + strcompOrganization + ",form_input_text=N'" + strOrganization + "',form_input_active=" + strsumOrganization + " Where form_input_name='" + _Organization + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowInstitution + ",form_input_isrequired=" + strcompInstitution + ",form_input_text=N'" + strInstitution + "',form_input_active=" + strsumInstitution + " Where form_input_name='" + _Institution + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress1 + ",form_input_isrequired=" + strcompAddress1 + ",form_input_text=N'" + strAddress1 + "',form_input_active=" + strsumAddress1 + " Where form_input_name='" + _Address1 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress2 + ",form_input_isrequired=" + strcompAddress2 + ",form_input_text=N'" + strAddress2 + "',form_input_active=" + strsumAddress2 + " Where form_input_name='" + _Address2 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress3 + ",form_input_isrequired=" + strcompAddress3 + ",form_input_text=N'" + strAddress3 + "',form_input_active=" + strsumAddress3 + " Where form_input_name='" + _Address3 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress4 + ",form_input_isrequired=" + strcompAddress4 + ",form_input_text=N'" + strAddress4 + "',form_input_active=" + strsumAddress4 + " Where form_input_name='" + _Address4 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCity + ",form_input_isrequired=" + strcompCity + ",form_input_text=N'" + strCity + "',form_input_active=" + strsumCity + " Where form_input_name='" + _City + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowState + ",form_input_isrequired=" + strcompState + ",form_input_text=N'" + strState + "',form_input_active=" + strsumState + " Where form_input_name='" + _State + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowPostalCode + ",form_input_isrequired=" + strcompPostalCode + ",form_input_text=N'" + strPostalCode + "',form_input_active=" + strsumPostalCode + " Where form_input_name='" + _PostalCode + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCountry + ",form_input_isrequired=" + strcompCountry + ",form_input_text=N'" + strCountry + "',form_input_active=" + strsumCountry + " Where form_input_name='" + _Country + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowRCountry + ",form_input_isrequired=" + strcompRCountry + ",form_input_text=N'" + strRCountry + "',form_input_active=" + strsumRCountry + " Where form_input_name='" + _RCountry + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelCC + ",form_input_isrequired=" + strcompTelCC + ",form_input_text=N'" + strTelCC + "',form_input_active=" + strsumTelCC + " Where form_input_name='" + _Telcc + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelAC + ",form_input_isrequired=" + strcompTelAC + ",form_input_text=N'" + strTelAC + "',form_input_active=" + strsumTelAC + " Where form_input_name='" + _Telac + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTel + ",form_input_isrequired=" + strcompTel + ",form_input_text=N'" + strTel + "',form_input_active=" + strsumTel + " Where form_input_name='" + _Tel + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobileCC + ",form_input_isrequired=" + strcompMobileCC + ",form_input_text=N'" + strMobileCC + "',form_input_active=" + strsumMobileCC + " Where form_input_name='" + _Mobilecc + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobileAC + ",form_input_isrequired=" + strcompMobileAC + ",form_input_text=N'" + strMobileAC + "',form_input_active=" + strsumMobileAC + " Where form_input_name='" + _Mobileac + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobile + ",form_input_isrequired=" + strcompMobile + ",form_input_text=N'" + strMobile + "',form_input_active=" + strsumMobile + " Where form_input_name='" + _Mobile + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxCC + ",form_input_isrequired=" + strcompFaxCC + ",form_input_text=N'" + strFaxCC + "',form_input_active=" + strsumFaxCC + " Where form_input_name='" + _Faxcc + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxAC + ",form_input_isrequired=" + strcompFaxAC + ",form_input_text=N'" + strFaxAC + "',form_input_active=" + strsumFaxAC + " Where form_input_name='" + _Faxac + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFax + ",form_input_isrequired=" + strcompFax + ",form_input_text=N'" + strFax + "',form_input_active=" + strsumFax + " Where form_input_name='" + _Fax + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmail + ",form_input_isrequired=" + strcompEmail + ",form_input_text=N'" + strEmail + "',form_input_active=" + strsumEmail + " Where form_input_name='" + _Email + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmailConfirmation + ",form_input_isrequired=" + strcompEmailConfirmation + ",form_input_text=N'" + strEmailConfirmation + "',form_input_active=" + strsumEmailConfirmation + " Where form_input_name='" + _EmailConfirmation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAffiliation + ",form_input_isrequired=" + strcompAffiliation + ",form_input_text=N'" + strAffiliation + "',form_input_active=" + strsumAffiliation + " Where form_input_name='" + _Affiliation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDietary + ",form_input_isrequired=" + strcompDietary + ",form_input_text=N'" + strDietary + "',form_input_active=" + strsumDietary + " Where form_input_name='" + _Dietary + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowNationality + ",form_input_isrequired=" + strcompNationality + ",form_input_text=N'" + strNationality + "',form_input_active=" + strsumNationality + " Where form_input_name='" + _Nationality + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMembershipNo + ",form_input_isrequired=" + strcompMembershipNo + ",form_input_text=N'" + strMembershipNo + "',form_input_active=" + strsumMembershipNo + " Where form_input_name='" + _MembershipNo + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVname + ",form_input_isrequired=" + strcompVname + ",form_input_text=N'" + strVname + "',form_input_active=" + strsumVname + " Where form_input_name='" + _VName + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVDOB + ",form_input_isrequired=" + strcompVDOB + ",form_input_text=N'" + strVDOB + "',form_input_active=" + strsumVDOB + " Where form_input_name='" + _VDOB + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVPassNo + ",form_input_isrequired=" + strcompVPassNo + ",form_input_text=N'" + strVPassNo + "',form_input_active=" + strsumVPassNo + " Where form_input_name='" + _VPassNo + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVPassExpiry + ",form_input_isrequired=" + strcompVPassExpiry + ",form_input_text=N'" + strVPassExpiry + "',form_input_active=" + strsumVPassExpiry + " Where form_input_name='" + _VPassExpiry + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVPassIssueDate + ",form_input_isrequired=" + strcompVPassIssueDate + ",form_input_text=N'" + strVPassIssueDate + "',form_input_active=" + strsumVPassIssueDate + " Where form_input_name='" + _VPassIssueDate + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVEmbarkation + ",form_input_isrequired=" + strcompVEmbarkation + ",form_input_text=N'" + strVEmbarkation + "',form_input_active=" + strsumVEmbarkation + " Where form_input_name='" + _VEmbarkation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVArrivalDate + ",form_input_isrequired=" + strcompVArrivalDate + ",form_input_text=N'" + strVArrivalDate + "',form_input_active=" + strsumVArrivalDate + " Where form_input_name='" + _VArrivalDate + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVCountry + ",form_input_isrequired=" + strcompVCountry + ",form_input_text=N'" + strVCountry + "',form_input_active=" + strsumVCountry + " Where form_input_name='" + _VCountry + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CName + ",form_input_isrequired=" + strcompUDF_CName + ",form_input_text=N'" + strUDF_CName + "',form_input_active=" + strsumUDF_CName + " Where form_input_name='" + _UDF_CName + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_DelegateType + ",form_input_isrequired=" + strcompUDF_DelegateType + ",form_input_text=N'" + strUDF_DelegateType + "',form_input_active=" + strsumUDF_DelegateType + " Where form_input_name='" + _UDF_DelegateType + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_ProfCategory + ",form_input_isrequired=" + strcompUDF_ProfCategory + ",form_input_text=N'" + strUDF_ProfCategory + "',form_input_active=" + strsumUDF_ProfCategory + " Where form_input_name='" + _UDF_ProfCategory + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_ProfCategoryOther + ",form_input_isrequired=" + strcompUDF_ProfCategoryOther + ",form_input_text=N'" + strUDF_ProfCategoryOther + "',form_input_active=" + strsumUDF_ProfCategoryOther + " Where form_input_name='" + _UDF_ProfCategoryOther + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CPcode + ",form_input_isrequired=" + strcompUDF_CPcode + ",form_input_text=N'" + strUDF_CPcode + "',form_input_active=" + strsumUDF_CPcode + " Where form_input_name='" + _UDF_CPcode + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CLDepartment + ",form_input_isrequired=" + strcompUDF_CLDepartment + ",form_input_text=N'" + strUDF_CLDepartment + "',form_input_active=" + strsumUDF_CLDepartment + " Where form_input_name='" + _UDF_CLDepartment + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CAddress + ",form_input_isrequired=" + strcompUDF_CAddress + ",form_input_text=N'" + strUDF_CAddress + "',form_input_active=" + strsumUDF_CAddress + " Where form_input_name='" + _UDF_CAddress + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CLCompany + ",form_input_isrequired=" + strcompUDF_CLCompany + ",form_input_text=N'" + strUDF_CLCompany + "',form_input_active=" + strsumUDF_CLCompany + " Where form_input_name='" + _UDF_CLCompany + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CLCompanyOther + ",form_input_isrequired=" + strcompUDF_CLCompanyOther + ",form_input_text=N'" + strUDF_CLCompanyOther + "',form_input_active=" + strsumUDF_CLCompanyOther + " Where form_input_name='" + _UDF_CLCompanyOther + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CCountry + ",form_input_isrequired=" + strcompUDF_CCountry + ",form_input_text=N'" + strUDF_CCountry + "',form_input_active=" + strsumUDF_CCountry + " Where form_input_name='" + _UDF_CCountry + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowSupervisorName + ",form_input_isrequired=" + strcompSupervisorName + ",form_input_text=N'" + strSupervisorName + "',form_input_active=" + strsumSupervisorName + " Where form_input_name='" + _SupervisorName + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowSupervisorDesignation + ",form_input_isrequired=" + strcompSupervisorDesignation + ",form_input_text=N'" + strSupervisorDesignation + "',form_input_active=" + strsumSupervisorDesignation + " Where form_input_name='" + _SupervisorDesignation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowSupervisorContact + ",form_input_isrequired=" + strcompSupervisorContact + ",form_input_text=N'" + strSupervisorContact + "',form_input_active=" + strsumSupervisorContact + " Where form_input_name='" + _SupervisorContact + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowSupervisorEmail + ",form_input_isrequired=" + strcompSupervisorEmail + ",form_input_text=N'" + strSupervisorEmail + "',form_input_active=" + strsumSupervisorEmail + " Where form_input_name='" + _SupervisorEmail + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherSalutation + ",form_input_isrequired=" + strcompOtherSalutation + ",form_input_text=N'" + strOtherSalutation + "',form_input_active=" + strsumOtherSalutation + " Where form_input_name='" + _OtherSalutation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherProfession + ",form_input_isrequired=" + strcompOtherProfession + ",form_input_text=N'" + strOtherProfession + "',form_input_active=" + strsumOtherProfession + " Where form_input_name='" + _OtherProfession + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherDepartment + ",form_input_isrequired=" + strcompOtherDepartment + ",form_input_text=N'" + strOtherDepartment + "',form_input_active=" + strsumOtherDepartment + " Where form_input_name='" + _OtherDepartment + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherOrganization + ",form_input_isrequired=" + strcompOtherOrganization + ",form_input_text=N'" + strOtherOrganization + "',form_input_active=" + strsumOtherOrganization + " Where form_input_name='" + _OtherOrganization + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherInstitution + ",form_input_isrequired=" + strcompOtherInstitution + ",form_input_text=N'" + strOtherInstitution + "',form_input_active=" + strsumOtherInstitution + " Where form_input_name='" + _OtherInstitution + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAEmail + ",form_input_isrequired=" + strcompAEmail + ",form_input_text=N'" + strAEmail + "',form_input_active=" + strsumAEmail + " Where form_input_name='" + _AEmail + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowIsSMS + ",form_input_isrequired=" + strcompIsSMS + ",form_input_text=N'" + strIsSMS + "',form_input_active=" + strsumIsSMS + " Where form_input_name='" + _IsSMS + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAge + ",form_input_isrequired=" + strcompAge + ",form_input_text=N'" + strAge + "',form_input_active=" + strsumAge + " Where form_input_name='" + _Age + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowGender + ",form_input_isrequired=" + strcompGender + ",form_input_text=N'" + strGender + "',form_input_active=" + strsumGender + " Where form_input_name='" + _Gender + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDOB + ",form_input_isrequired=" + strcompDOB + ",form_input_text=N'" + strDOB + "',form_input_active=" + strsumDOB + " Where form_input_name='" + _DOB + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional4 + ",form_input_isrequired=" + strcompAdditional4 + ",form_input_text=N'" + strAdditional4 + "',form_input_active=" + strsumAdditional4 + " Where form_input_name='" + _Additional4 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional5 + ",form_input_isrequired=" + strcompAdditional5 + ",form_input_text=N'" + strAdditional5 + "',form_input_active=" + strsumAdditional5 + " Where form_input_name='" + _Additional5 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";

                fn.ExecuteSQL(sql1);

                InsertQRCodeFields(flowid,showid);//*

                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-success alert-dismissible");
                lblMsg.Text = "Success.";

                setDynamicForm(flowid, showid);

                Dictionary<string, string> nValues = new Dictionary<string, string>();
                FlowControler fControl = new FlowControler(fn);
                string curstep = cFUnz.DecryptValue(Request.QueryString["STP"].ToString());
                nValues = fControl.GetAdminNextRoute(flowid, curstep);
                if (nValues.Count > 0)
                {
                    //*Update Currenct Flow Step Status
                    Flow flw = new Flow();
                    flw.FlowID = flowid;
                    flw.FlowConfigStatus = YesOrNo.Yes;
                    flw.FlowStep = curstep;
                    fControl.UpdateFlowIndivConfigStatus(flw);
                    //*

                    string path = string.Empty;
                    if (Request.Params["a"] != null)
                    {
                        string admintype = cFUnz.DecryptValue(Request.QueryString["a"].ToString());
                        if (admintype == BackendStaticValueClass.isFlowEdit)
                        {
                            string page = "Event_Flow_Dashboard.aspx";

                            path = fControl.MakeFullURL(page, flowid, showid);
                        }
                    }
                    else
                    {
                        string page = nValues["nURL"].ToString();
                        string step = nValues["nStep"].ToString();
                        string FlowID = nValues["FlowID"].ToString();

                        if (page == "") page = "Event_Config_Final";

                        path = fControl.MakeFullURL(page, FlowID, showid, "", step, null, "D");
                    }

                    if (path == "") path = "404.aspx";
                    Response.Redirect(path);
                }
            }
            catch (Exception ex)
            {
                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-danger alert-dismissible");
                lblMsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region InsertNewForm and using tb_Form table & Insert new blank records but form_input_name, form_control_id, formType, ShowID into tb_Form table
    protected void InsertNewForm(string flowid, string showid)
    {
        try
        {
            int isShow = 0;
            int isRequired = 0;
            int isActive = 0;
            string inputText = string.Empty;
            string formType = "D";

            DataTable dt = fn.GetDatasetByCommand("Select * From tmp_Form Where form_type='" + FormType.TypeDelegate + "'", "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string formname = dr["form_input_name"].ToString();
                    string controlid = dr["form_control_id"].ToString();
                    string orderno = dr["form_input_order"].ToString();

                    string query = "Select * From tb_Form Where FlowID='" + flowid + "' And ShowID=@SHWID And form_input_name='" + formname + "' And form_type='" + FormType.TypeDelegate + "'";
                    List<SqlParameter> pList = new List<SqlParameter>();
                    SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    spar.Value = showid;
                    pList.Add(spar);

                    DataTable dtForm = fn.GetDatasetByCommand(query, "dsForm", pList).Tables[0];
                    if(dtForm.Rows.Count == 0)
                    {
                        string sql = string.Format("Insert Into tb_Form (form_input_name,form_control_id,form_input_isshow,form_input_isrequired,form_input_active,"
                                    + "form_input_order,form_input_text,form_type,ShowID,FlowID)"
                                    + " Values('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', '{9}')", formname, controlid, isShow, isRequired, isActive,
                                        orderno, inputText, formType, showid, flowid);

                        fn.ExecuteSQL(sql);
                    }
                }
            }
            setDynamicForm(flowid, showid);
        }
        catch (Exception ex)
        {
            divMsg.Visible = true;
            divMsg.Attributes.Remove("class");
            divMsg.Attributes.Add("class", "alert alert-danger alert-dismissible");
            lblMsg.Text = "Error occur: " + ex.Message;
        }
    }
    #endregion

    #region btnSavePreview_Click
    protected void btnSavePreview_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = cFUnz.DecryptValue(Request.QueryString["SHW"].ToString());
            string flowid = cFUnz.DecryptValue(Request.QueryString["FLW"].ToString());

            try
            {
                #region Declaration
                string strshowSalutation = "0";
                string strcompSalutation = "0";
                string strSalutation = string.Empty;
                string strsumSalutation = "0";

                string strshowFname = "0";
                string strcompFname = "0";
                string strFname = string.Empty;
                string strsumFname = "0";

                string strshowLname = "0";
                string strcompLname = "0";
                string strLname = string.Empty;
                string strsumLname = "0";

                string strshowOname = "0";
                string strcompOname = "0";
                string strOname = string.Empty;
                string strsumOname = "0";

                string strshowPassportNo = "0";
                string strcompPassportNo = "0";
                string strPassportNo = string.Empty;
                string strsumPassportNo = "0";

                string strshowIsRegistered = "0";
                string strcompIsRegistered = "0";
                string strIsRegistered = string.Empty;
                string strsumIsRegistered = "0";

                string strshowRegSpecific = "0";
                string strcompRegSpecific = "0";
                string strRegSpecific = string.Empty;
                string strsumRegSpecific = "0";

                string strshowIDNo = "0";
                string strcompIDNo = "0";
                string strIDNo = string.Empty;
                string strsumIDNo = "0";

                string strshowDesignation = "0";
                string strcompDesignation = "0";
                string strDesignation = string.Empty;
                string strsumDesignation = "0";

                string strshowProfession = "0";
                string strcompProfession = "0";
                string strProfession = string.Empty;
                string strsumProfession = "0";

                string strshowDepartment = "0";
                string strcompDepartment = "0";
                string strDepartment = string.Empty;
                string strsumDepartment = "0";

                string strshowOrganization = "0";
                string strcompOrganization = "0";
                string strOrganization = string.Empty;
                string strsumOrganization = "0";

                string strshowInstitution = "0";
                string strcompInstitution = "0";
                string strInstitution = string.Empty;
                string strsumInstitution = "0";

                string strshowAddress1 = "0";
                string strcompAddress1 = "0";
                string strAddress1 = string.Empty;
                string strsumAddress1 = "0";

                string strshowAddress2 = "0";
                string strcompAddress2 = "0";
                string strAddress2 = string.Empty;
                string strsumAddress2 = "0";

                string strshowAddress3 = "0";
                string strcompAddress3 = "0";
                string strAddress3 = string.Empty;
                string strsumAddress3 = "0";

                string strshowAddress4 = "0";
                string strcompAddress4 = "0";
                string strAddress4 = string.Empty;
                string strsumAddress4 = "0";

                string strshowCity = "0";
                string strcompCity = "0";
                string strCity = string.Empty;
                string strsumCity = "0";

                string strshowState = "0";
                string strcompState = "0";
                string strState = string.Empty;
                string strsumState = "0";

                string strshowPostalCode = "0";
                string strcompPostalCode = "0";
                string strPostalCode = string.Empty;
                string strsumPostalCode = "0";

                string strshowCountry = "0";
                string strcompCountry = "0";
                string strCountry = string.Empty;
                string strsumCountry = "0";

                string strshowRCountry = "0";
                string strcompRCountry = "0";
                string strRCountry = string.Empty;
                string strsumRCountry = "0";

                string strshowTelCC = "0";
                string strcompTelCC = "0";
                string strTelCC = string.Empty;
                string strsumTelCC = "0";

                string strshowTelAC = "0";
                string strcompTelAC = "0";
                string strTelAC = string.Empty;
                string strsumTelAC = "0";

                string strshowTel = "0";
                string strcompTel = "0";
                string strTel = string.Empty;
                string strsumTel = "0";

                string strshowMobileCC = "0";
                string strcompMobileCC = "0";
                string strMobileCC = string.Empty;
                string strsumMobileCC = "0";

                string strshowMobileAC = "0";
                string strcompMobileAC = "0";
                string strMobileAC = string.Empty;
                string strsumMobileAC = "0";

                string strshowMobile = "0";
                string strcompMobile = "0";
                string strMobile = string.Empty;
                string strsumMobile = "0";

                string strshowFaxCC = "0";
                string strcompFaxCC = "0";
                string strFaxCC = string.Empty;
                string strsumFaxCC = "0";

                string strshowFaxAC = "0";
                string strcompFaxAC = "0";
                string strFaxAC = string.Empty;
                string strsumFaxAC = "0";

                string strshowFax = "0";
                string strcompFax = "0";
                string strFax = string.Empty;
                string strsumFax = "0";

                string strshowEmail = "0";
                string strcompEmail = "0";
                string strEmail = string.Empty;
                string strsumEmail = "0";

                string strshowEmailConfirmation = "0";
                string strcompEmailConfirmation = "0";
                string strEmailConfirmation = string.Empty;
                string strsumEmailConfirmation = "0";

                string strshowAffiliation = "0";
                string strcompAffiliation = "0";
                string strAffiliation = string.Empty;
                string strsumAffiliation = "0";

                string strshowDietary = "0";
                string strcompDietary = "0";
                string strDietary = string.Empty;
                string strsumDietary = "0";

                string strshowNationality = "0";
                string strcompNationality = "0";
                string strNationality = string.Empty;
                string strsumNationality = "0";

                string strshowMembershipNo = "0";
                string strcompMembershipNo = "0";
                string strMembershipNo = string.Empty;
                string strsumMembershipNo = "0";

                string strshowVname = "0";
                string strcompVname = "0";
                string strVname = string.Empty;
                string strsumVname = "0";

                string strshowVDOB = "0";
                string strcompVDOB = "0";
                string strVDOB = string.Empty;
                string strsumVDOB = "0";

                string strshowVPassNo = "0";
                string strcompVPassNo = "0";
                string strVPassNo = string.Empty;
                string strsumVPassNo = "0";

                string strshowVPassExpiry = "0";
                string strcompVPassExpiry = "0";
                string strVPassExpiry = string.Empty;
                string strsumVPassExpiry = "0";

                string strshowVPassIssueDate = "0";
                string strcompVPassIssueDate = "0";
                string strVPassIssueDate = string.Empty;
                string strsumVPassIssueDate = "0";

                string strshowVEmbarkation = "0";
                string strcompVEmbarkation = "0";
                string strVEmbarkation = string.Empty;
                string strsumVEmbarkation = "0";

                string strshowVArrivalDate = "0";
                string strcompVArrivalDate = "0";
                string strVArrivalDate = string.Empty;
                string strsumVArrivalDate = "0";

                string strshowVCountry = "0";
                string strcompVCountry = "0";
                string strVCountry = string.Empty;
                string strsumVCountry = "0";

                string strshowUDF_CName = "0";
                string strcompUDF_CName = "0";
                string strUDF_CName = string.Empty;
                string strsumUDF_CName = "0";

                string strshowUDF_DelegateType = "0";
                string strcompUDF_DelegateType = "0";
                string strUDF_DelegateType = string.Empty;
                string strsumUDF_DelegateType = "0";

                string strshowUDF_ProfCategory = "0";
                string strcompUDF_ProfCategory = "0";
                string strUDF_ProfCategory = string.Empty;
                string strsumUDF_ProfCategory = "0";

                string strshowUDF_ProfCategoryOther = "0";
                string strcompUDF_ProfCategoryOther = "0";
                string strUDF_ProfCategoryOther = string.Empty;
                string strsumUDF_ProfCategoryOther = "0";

                string strshowUDF_CPcode = "0";
                string strcompUDF_CPcode = "0";
                string strUDF_CPcode = string.Empty;
                string strsumUDF_CPcode = "0";

                string strshowUDF_CLDepartment = "0";
                string strcompUDF_CLDepartment = "0";
                string strUDF_CLDepartment = string.Empty;
                string strsumUDF_CLDepartment = "0";

                string strshowUDF_CAddress = "0";
                string strcompUDF_CAddress = "0";
                string strUDF_CAddress = string.Empty;
                string strsumUDF_CAddress = "0";

                string strshowUDF_CLCompany = "0";
                string strcompUDF_CLCompany = "0";
                string strUDF_CLCompany = string.Empty;
                string strsumUDF_CLCompany = "0";

                string strshowUDF_CLCompanyOther = "0";
                string strcompUDF_CLCompanyOther = "0";
                string strUDF_CLCompanyOther = string.Empty;
                string strsumUDF_CLCompanyOther = "0";

                string strshowUDF_CCountry = "0";
                string strcompUDF_CCountry = "0";
                string strUDF_CCountry = string.Empty;
                string strsumUDF_CCountry = "0";

                string strshowSupervisorName = "0";
                string strcompSupervisorName = "0";
                string strSupervisorName = string.Empty;
                string strsumSupervisorName = "0";

                string strshowSupervisorDesignation = "0";
                string strcompSupervisorDesignation = "0";
                string strSupervisorDesignation = string.Empty;
                string strsumSupervisorDesignation = "0";

                string strshowSupervisorContact = "0";
                string strcompSupervisorContact = "0";
                string strSupervisorContact = string.Empty;
                string strsumSupervisorContact = "0";

                string strshowSupervisorEmail = "0";
                string strcompSupervisorEmail = "0";
                string strSupervisorEmail = string.Empty;
                string strsumSupervisorEmail = "0";

                string strshowOtherSalutation = "0";
                string strcompOtherSalutation = "0";
                string strOtherSalutation = string.Empty;
                string strsumOtherSalutation = "0";

                string strshowOtherProfession = "0";
                string strcompOtherProfession = "0";
                string strOtherProfession = string.Empty;
                string strsumOtherProfession = "0";

                string strshowOtherDepartment = "0";
                string strcompOtherDepartment = "0";
                string strOtherDepartment = string.Empty;
                string strsumOtherDepartment = "0";

                string strshowOtherOrganization = "0";
                string strcompOtherOrganization = "0";
                string strOtherOrganization = string.Empty;
                string strsumOtherOrganization = "0";

                string strshowOtherInstitution = "0";
                string strcompOtherInstitution = "0";
                string strOtherInstitution = string.Empty;
                string strsumOtherInstitution = "0";

                string strshowAEmail = "0";
                string strcompAEmail = "0";
                string strAEmail = string.Empty;
                string strsumAEmail = "0";

                string strshowIsSMS = "0";
                string strcompIsSMS = "0";
                string strIsSMS = string.Empty;
                string strsumIsSMS = "0";

                string strshowAge = "0";
                string strcompAge = "0";
                string strAge = string.Empty;
                string strsumAge = "0";

                string strshowGender = "0";
                string strcompGender = "0";
                string strGender = string.Empty;
                string strsumGender = "0";

                string strshowDOB = "0";
                string strcompDOB = "0";
                string strDOB = string.Empty;
                string strsumDOB = "0";

                string strshowAdditional4 = "0";
                string strcompAdditional4 = "0";
                string strAdditional4 = string.Empty;
                string strsumAdditional4 = "0";

                string strshowAdditional5 = "0";
                string strcompAdditional5 = "0";
                string strAdditional5 = string.Empty;
                string strsumAdditional5 = "0";
                #endregion

                #region set variable values
                if (chkshowSalutation.Checked == true)
                {
                    strshowSalutation = "1";
                    strSalutation = txtSalutation.Text.ToString();
                    if (chkcompSalutation.Checked == true)
                    {
                        strcompSalutation = "1";
                    }
                    else
                    {
                        strcompSalutation = "0";
                    }

                    if (chksumSalutation.Checked == true)
                    {
                        strsumSalutation = "1";
                    }
                    else
                    {
                        strsumSalutation = "0";
                    }
                }
                if (chkshowFname.Checked == true)
                {
                    strshowFname = "1";
                    strFname = txtFname.Text.ToString();
                    if (chkcompFname.Checked == true)
                    {
                        strcompFname = "1";
                    }
                    else
                    {
                        strcompFname = "0";
                    }

                    if (chksumFname.Checked == true)
                    {
                        strsumFname = "1";
                    }
                    else
                    {
                        strsumFname = "0";
                    }
                }
                if (chkshowLname.Checked == true)
                {
                    strshowLname = "1";
                    strLname = txtLname.Text.ToString();
                    if (chkcompLname.Checked == true)
                    {
                        strcompLname = "1";
                    }
                    else
                    {
                        strcompLname = "0";
                    }

                    if (chksumLname.Checked == true)
                    {
                        strsumLname = "1";
                    }
                    else
                    {
                        strsumLname = "0";
                    }
                }
                if (chkshowOname.Checked == true)
                {
                    strshowOname = "1";
                    strOname = txtOname.Text.ToString();
                    if (chkcompOname.Checked == true)
                    {
                        strcompOname = "1";
                    }
                    else
                    {
                        strcompOname = "0";
                    }

                    if (chksumOname.Checked == true)
                    {
                        strsumOname = "1";
                    }
                    else
                    {
                        strsumOname = "0";
                    }
                }
                if (chkshowPassportNo.Checked == true)
                {
                    strshowPassportNo = "1";
                    strPassportNo = txtPassportNo.Text.ToString();
                    if (chkcompPassportNo.Checked == true)
                    {
                        strcompPassportNo = "1";
                    }
                    else
                    {
                        strcompPassportNo = "0";
                    }

                    if (chksumPassportNo.Checked == true)
                    {
                        strsumPassportNo = "1";
                    }
                    else
                    {
                        strsumPassportNo = "0";
                    }
                }
                if (chkshowIsRegistered.Checked == true)
                {
                    strshowIsRegistered = "1";
                    strIsRegistered = txtIsRegistered.Text.ToString();
                    if (chkcompIsRegistered.Checked == true)
                    {
                        strcompIsRegistered = "1";
                    }
                    else
                    {
                        strcompIsRegistered = "0";
                    }

                    if (chksumIsRegistered.Checked == true)
                    {
                        strsumIsRegistered = "1";
                    }
                    else
                    {
                        strsumIsRegistered = "0";
                    }
                }
                if (chkshowRegSpecific.Checked == true)
                {
                    strshowRegSpecific = "1";
                    strRegSpecific = txtRegSpecific.Text.ToString();
                    if (chkcompRegSpecific.Checked == true)
                    {
                        strcompRegSpecific = "1";
                    }
                    else
                    {
                        strcompRegSpecific = "0";
                    }

                    if (chksumRegSpecific.Checked == true)
                    {
                        strsumRegSpecific = "1";
                    }
                    else
                    {
                        strsumRegSpecific = "0";
                    }
                }
                if (chkshowIDNo.Checked == true)
                {
                    strshowIDNo = "1";
                    strIDNo = txtIDNo.Text.ToString();
                    if (chkcompIDNo.Checked == true)
                    {
                        strcompIDNo = "1";
                    }
                    else
                    {
                        strcompIDNo = "0";
                    }

                    if (chksumIDNo.Checked == true)
                    {
                        strsumIDNo = "1";
                    }
                    else
                    {
                        strsumIDNo = "0";
                    }
                }
                if (chkshowDesignation.Checked == true)
                {
                    strshowDesignation = "1";
                    strDesignation = txtDesignation.Text.ToString();
                    if (chkcompDesignation.Checked == true)
                    {
                        strcompDesignation = "1";
                    }
                    else
                    {
                        strcompDesignation = "0";
                    }

                    if (chksumDesignation.Checked == true)
                    {
                        strsumDesignation = "1";
                    }
                    else
                    {
                        strsumDesignation = "0";
                    }
                }
                if (chkshowProfession.Checked == true)
                {
                    strshowProfession = "1";
                    strProfession = txtProfession.Text.ToString();
                    if (chkcompProfession.Checked == true)
                    {
                        strcompProfession = "1";
                    }
                    else
                    {
                        strcompProfession = "0";
                    }

                    if (chksumProfession.Checked == true)
                    {
                        strsumProfession = "1";
                    }
                    else
                    {
                        strsumProfession = "0";
                    }
                }
                if (chkshowDepartment.Checked == true)
                {
                    strshowDepartment = "1";
                    strDepartment = txtDepartment.Text.ToString();
                    if (chkcompDepartment.Checked == true)
                    {
                        strcompDepartment = "1";
                    }
                    else
                    {
                        strcompDepartment = "0";
                    }

                    if (chksumDepartment.Checked == true)
                    {
                        strsumDepartment = "1";
                    }
                    else
                    {
                        strsumDepartment = "0";
                    }
                }
                if (chkshowOrganization.Checked == true)
                {
                    strshowOrganization = "1";
                    strOrganization = txtOrganization.Text.ToString();
                    if (chkcompOrganization.Checked == true)
                    {
                        strcompOrganization = "1";
                    }
                    else
                    {
                        strcompOrganization = "0";
                    }

                    if (chksumOrganization.Checked == true)
                    {
                        strsumOrganization = "1";
                    }
                    else
                    {
                        strsumOrganization = "0";
                    }
                }
                if (chkshowInstitution.Checked == true)
                {
                    strshowInstitution = "1";
                    strInstitution = txtInstitution.Text.ToString();
                    if (chkcompInstitution.Checked == true)
                    {
                        strcompInstitution = "1";
                    }
                    else
                    {
                        strcompInstitution = "0";
                    }

                    if (chksumInstitution.Checked == true)
                    {
                        strsumInstitution = "1";
                    }
                    else
                    {
                        strsumInstitution = "0";
                    }
                }
                if (chkshowAddress1.Checked == true)
                {
                    strshowAddress1 = "1";
                    strAddress1 = txtAddress1.Text.ToString();
                    if (chkcompAddress1.Checked == true)
                    {
                        strcompAddress1 = "1";
                    }
                    else
                    {
                        strcompAddress1 = "0";
                    }

                    if (chksumAddress1.Checked == true)
                    {
                        strsumAddress1 = "1";
                    }
                    else
                    {
                        strsumAddress1 = "0";
                    }
                }
                if (chkshowAddress2.Checked == true)
                {
                    strshowAddress2 = "1";
                    strAddress2 = txtAddress2.Text.ToString();
                    if (chkcompAddress2.Checked == true)
                    {
                        strcompAddress2 = "1";
                    }
                    else
                    {
                        strcompAddress2 = "0";
                    }

                    if (chksumAddress2.Checked == true)
                    {
                        strsumAddress2 = "1";
                    }
                    else
                    {
                        strsumAddress2 = "0";
                    }
                }
                if (chkshowAddress3.Checked == true)
                {
                    strshowAddress3 = "1";
                    strAddress3 = txtAddress3.Text.ToString();
                    if (chkcompAddress3.Checked == true)
                    {
                        strcompAddress3 = "1";
                    }
                    else
                    {
                        strcompAddress3 = "0";
                    }

                    if (chksumAddress3.Checked == true)
                    {
                        strsumAddress3 = "1";
                    }
                    else
                    {
                        strsumAddress3 = "0";
                    }
                }
                if (chkshowAddress4.Checked == true)
                {
                    strshowAddress4 = "1";
                    strAddress4 = txtAddress4.Text.ToString();
                    if (chkcompAddress4.Checked == true)
                    {
                        strcompAddress4 = "1";
                    }
                    else
                    {
                        strcompAddress4 = "0";
                    }

                    if (chksumAddress4.Checked == true)
                    {
                        strsumAddress4 = "1";
                    }
                    else
                    {
                        strsumAddress4 = "0";
                    }
                }
                if (chkshowCity.Checked == true)
                {
                    strshowCity = "1";
                    strCity = txtCity.Text.ToString();
                    if (chkcompCity.Checked == true)
                    {
                        strcompCity = "1";
                    }
                    else
                    {
                        strcompCity = "0";
                    }

                    if (chksumCity.Checked == true)
                    {
                        strsumCity = "1";
                    }
                    else
                    {
                        strsumCity = "0";
                    }
                }
                if (chkshowState.Checked == true)
                {
                    strshowState = "1";
                    strState = txtState.Text.ToString();
                    if (chkcompState.Checked == true)
                    {
                        strcompState = "1";
                    }
                    else
                    {
                        strcompState = "0";
                    }

                    if (chksumState.Checked == true)
                    {
                        strsumState = "1";
                    }
                    else
                    {
                        strsumState = "0";
                    }
                }
                if (chkshowPostalCode.Checked == true)
                {
                    strshowPostalCode = "1";
                    strPostalCode = txtPostalCode.Text.ToString();
                    if (chkcompPostalCode.Checked == true)
                    {
                        strcompPostalCode = "1";
                    }
                    else
                    {
                        strcompPostalCode = "0";
                    }

                    if (chksumPostalCode.Checked == true)
                    {
                        strsumPostalCode = "1";
                    }
                    else
                    {
                        strsumPostalCode = "0";
                    }
                }
                if (chkshowCountry.Checked == true)
                {
                    strshowCountry = "1";
                    strCountry = txtCountry.Text.ToString();
                    if (chkcompCountry.Checked == true)
                    {
                        strcompCountry = "1";
                    }
                    else
                    {
                        strcompCountry = "0";
                    }

                    if (chksumCountry.Checked == true)
                    {
                        strsumCountry = "1";
                    }
                    else
                    {
                        strsumCountry = "0";
                    }
                }
                if (chkshowRCountry.Checked == true)
                {
                    strshowRCountry = "1";
                    strRCountry = txtRCountry.Text.ToString();
                    if (chkcompRCountry.Checked == true)
                    {
                        strcompRCountry = "1";
                    }
                    else
                    {
                        strcompRCountry = "0";
                    }

                    if (chksumRCountry.Checked == true)
                    {
                        strsumRCountry = "1";
                    }
                    else
                    {
                        strsumRCountry = "0";
                    }
                }
                if (chkshowTelCC.Checked == true)
                {
                    strshowTelCC = "1";
                    strTelCC = txtTelCC.Text.ToString();
                    if (chkcompTelCC.Checked == true)
                    {
                        strcompTelCC = "1";
                    }
                    else
                    {
                        strcompTelCC = "0";
                    }

                    if (chksumTelCC.Checked == true)
                    {
                        strsumTelCC = "1";
                    }
                    else
                    {
                        strsumTelCC = "0";
                    }
                }
                if (chkshowTelAC.Checked == true)
                {
                    strshowTelAC = "1";
                    strTelAC = txtTelAC.Text.ToString();
                    if (chkcompTelAC.Checked == true)
                    {
                        strcompTelAC = "1";
                    }
                    else
                    {
                        strcompTelAC = "0";
                    }

                    if (chksumTelAC.Checked == true)
                    {
                        strsumTelAC = "1";
                    }
                    else
                    {
                        strsumTelAC = "0";
                    }
                }
                if (chkshowTel.Checked == true)
                {
                    strshowTel = "1";
                    strTel = txtTel.Text.ToString();
                    if (chkcompTel.Checked == true)
                    {
                        strcompTel = "1";
                    }
                    else
                    {
                        strcompTel = "0";
                    }

                    if (chksumTel.Checked == true)
                    {
                        strsumTel = "1";
                    }
                    else
                    {
                        strsumTel = "0";
                    }
                }
                if (chkshowMobileCC.Checked == true)
                {
                    strshowMobileCC = "1";
                    strMobileCC = txtMobileCC.Text.ToString();
                    if (chkcompMobileCC.Checked == true)
                    {
                        strcompMobileCC = "1";
                    }
                    else
                    {
                        strcompMobileCC = "0";
                    }

                    if (chksumMobileCC.Checked == true)
                    {
                        strsumMobileCC = "1";
                    }
                    else
                    {
                        strsumMobileCC = "0";
                    }
                }
                if (chkshowMobileAC.Checked == true)
                {
                    strshowMobileAC = "1";
                    strMobileAC = txtMobileAC.Text.ToString();
                    if (chkcompMobileAC.Checked == true)
                    {
                        strcompMobileAC = "1";
                    }
                    else
                    {
                        strcompMobileAC = "0";
                    }

                    if (chksumMobileAC.Checked == true)
                    {
                        strsumMobileAC = "1";
                    }
                    else
                    {
                        strsumMobileAC = "0";
                    }
                }
                if (chkshowMobile.Checked == true)
                {
                    strshowMobile = "1";
                    strMobile = txtMobile.Text.ToString();
                    if (chkcompMobile.Checked == true)
                    {
                        strcompMobile = "1";
                    }
                    else
                    {
                        strcompMobile = "0";
                    }

                    if (chksumMobile.Checked == true)
                    {
                        strsumMobile = "1";
                    }
                    else
                    {
                        strsumMobile = "0";
                    }
                }
                if (chkshowFaxCC.Checked == true)
                {
                    strshowFaxCC = "1";
                    strFaxCC = txtFaxCC.Text.ToString();
                    if (chkcompFaxCC.Checked == true)
                    {
                        strcompFaxCC = "1";
                    }
                    else
                    {
                        strcompFaxCC = "0";
                    }

                    if (chksumFaxCC.Checked == true)
                    {
                        strsumFaxCC = "1";
                    }
                    else
                    {
                        strsumFaxCC = "0";
                    }
                }
                if (chkshowFaxAC.Checked == true)
                {
                    strshowFaxAC = "1";
                    strFaxAC = txtFaxAC.Text.ToString();
                    if (chkcompFaxAC.Checked == true)
                    {
                        strcompFaxAC = "1";
                    }
                    else
                    {
                        strcompFaxAC = "0";
                    }

                    if (chksumFaxAC.Checked == true)
                    {
                        strsumFaxAC = "1";
                    }
                    else
                    {
                        strsumFaxAC = "0";
                    }
                }
                if (chkshowFax.Checked == true)
                {
                    strshowFax = "1";
                    strFax = txtFax.Text.ToString();
                    if (chkcompFax.Checked == true)
                    {
                        strcompFax = "1";
                    }
                    else
                    {
                        strcompFax = "0";
                    }

                    if (chksumFax.Checked == true)
                    {
                        strsumFax = "1";
                    }
                    else
                    {
                        strsumFax = "0";
                    }
                }
                if (chkshowEmail.Checked == true)
                {
                    strshowEmail = "1";
                    strEmail = txtEmail.Text.ToString();
                    if (chkcompEmail.Checked == true)
                    {
                        strcompEmail = "1";
                    }
                    else
                    {
                        strcompEmail = "0";
                    }

                    if (chksumEmail.Checked == true)
                    {
                        strsumEmail = "1";
                    }
                    else
                    {
                        strsumEmail = "0";
                    }
                }
                if (chkshowEmailConfirmation.Checked == true)
                {
                    strshowEmailConfirmation = "1";
                    strEmailConfirmation = txtEmailConfirmation.Text.ToString();
                    if (chkcompEmailConfirmation.Checked == true)
                    {
                        strcompEmailConfirmation = "1";
                    }
                    else
                    {
                        strcompEmailConfirmation = "0";
                    }

                    if (chksumEmailConfirmation.Checked == true)
                    {
                        strsumEmailConfirmation = "1";
                    }
                    else
                    {
                        strsumEmailConfirmation = "0";
                    }
                }
                if (chkshowAffiliation.Checked == true)
                {
                    strshowAffiliation = "1";
                    strAffiliation = txtAffiliation.Text.ToString();
                    if (chkcompAffiliation.Checked == true)
                    {
                        strcompAffiliation = "1";
                    }
                    else
                    {
                        strcompAffiliation = "0";
                    }

                    if (chksumAffiliation.Checked == true)
                    {
                        strsumAffiliation = "1";
                    }
                    else
                    {
                        strsumAffiliation = "0";
                    }
                }
                if (chkshowDietary.Checked == true)
                {
                    strshowDietary = "1";
                    strDietary = txtDietary.Text.ToString();
                    if (chkcompDietary.Checked == true)
                    {
                        strcompDietary = "1";
                    }
                    else
                    {
                        strcompDietary = "0";
                    }

                    if (chksumDietary.Checked == true)
                    {
                        strsumDietary = "1";
                    }
                    else
                    {
                        strsumDietary = "0";
                    }
                }
                if (chkshowNationality.Checked == true)
                {
                    strshowNationality = "1";
                    strNationality = txtNationality.Text.ToString();
                    if (chkcompNationality.Checked == true)
                    {
                        strcompNationality = "1";
                    }
                    else
                    {
                        strcompNationality = "0";
                    }

                    if (chksumNationality.Checked == true)
                    {
                        strsumNationality = "1";
                    }
                    else
                    {
                        strsumNationality = "0";
                    }
                }
                if (chkshowMembershipNo.Checked == true)
                {
                    strshowMembershipNo = "1";
                    strMembershipNo = txtMembershipNo.Text.ToString();
                    if (chkcompMembershipNo.Checked == true)
                    {
                        strcompMembershipNo = "1";
                    }
                    else
                    {
                        strcompMembershipNo = "0";
                    }

                    if (chksumMembershipNo.Checked == true)
                    {
                        strsumMembershipNo = "1";
                    }
                    else
                    {
                        strsumMembershipNo = "0";
                    }
                }
                if (chkshowVname.Checked == true)
                {
                    strshowVname = "1";
                    strVname = txtVname.Text.ToString();
                    if (chkcompVname.Checked == true)
                    {
                        strcompVname = "1";
                    }
                    else
                    {
                        strcompVname = "0";
                    }

                    if (chksumVname.Checked == true)
                    {
                        strsumVname = "1";
                    }
                    else
                    {
                        strsumVname = "0";
                    }
                }
                if (chkshowVDOB.Checked == true)
                {
                    strshowVDOB = "1";
                    strVDOB = txtVDOB.Text.ToString();
                    if (chkcompVDOB.Checked == true)
                    {
                        strcompVDOB = "1";
                    }
                    else
                    {
                        strcompVDOB = "0";
                    }

                    if (chksumVDOB.Checked == true)
                    {
                        strsumVDOB = "1";
                    }
                    else
                    {
                        strsumVDOB = "0";
                    }
                }
                if (chkshowVPassNo.Checked == true)
                {
                    strshowVPassNo = "1";
                    strVPassNo = txtVPassNo.Text.ToString();
                    if (chkcompVPassNo.Checked == true)
                    {
                        strcompVPassNo = "1";
                    }
                    else
                    {
                        strcompVPassNo = "0";
                    }

                    if (chksumVPassNo.Checked == true)
                    {
                        strsumVPassNo = "1";
                    }
                    else
                    {
                        strsumVPassNo = "0";
                    }
                }
                if (chkshowVPassExpiry.Checked == true)
                {
                    strshowVPassExpiry = "1";
                    strVPassExpiry = txtVPassExpiry.Text.ToString();
                    if (chkcompVPassExpiry.Checked == true)
                    {
                        strcompVPassExpiry = "1";
                    }
                    else
                    {
                        strcompVPassExpiry = "0";
                    }

                    if (chksumVPassExpiry.Checked == true)
                    {
                        strsumVPassExpiry = "1";
                    }
                    else
                    {
                        strsumVPassExpiry = "0";
                    }
                }
                if (chkshowVPassIssueDate.Checked == true)
                {
                    strshowVPassIssueDate = "1";
                    strVPassIssueDate = txtVPassIssueDate.Text.ToString();
                    if (chkcompVPassIssueDate.Checked == true)
                    {
                        strcompVPassIssueDate = "1";
                    }
                    else
                    {
                        strcompVPassIssueDate = "0";
                    }

                    if (chksumVPassIssueDate.Checked == true)
                    {
                        strsumVPassIssueDate = "1";
                    }
                    else
                    {
                        strsumVPassIssueDate = "0";
                    }
                }
                if (chkshowVEmbarkation.Checked == true)
                {
                    strshowVEmbarkation = "1";
                    strVEmbarkation = txtVEmbarkation.Text.ToString();
                    if (chkcompVEmbarkation.Checked == true)
                    {
                        strcompVEmbarkation = "1";
                    }
                    else
                    {
                        strcompVEmbarkation = "0";
                    }

                    if (chksumVEmbarkation.Checked == true)
                    {
                        strsumVEmbarkation = "1";
                    }
                    else
                    {
                        strsumVEmbarkation = "0";
                    }
                }
                if (chkshowVArrivalDate.Checked == true)
                {
                    strshowVArrivalDate = "1";
                    strVArrivalDate = txtVArrivalDate.Text.ToString();
                    if (chkcompVArrivalDate.Checked == true)
                    {
                        strcompVArrivalDate = "1";
                    }
                    else
                    {
                        strcompVArrivalDate = "0";
                    }

                    if (chksumVArrivalDate.Checked == true)
                    {
                        strsumVArrivalDate = "1";
                    }
                    else
                    {
                        strsumVArrivalDate = "0";
                    }
                }
                if (chkshowVCountry.Checked == true)
                {
                    strshowVCountry = "1";
                    strVCountry = txtVCountry.Text.ToString();
                    if (chkcompVCountry.Checked == true)
                    {
                        strcompVCountry = "1";
                    }
                    else
                    {
                        strcompVCountry = "0";
                    }

                    if (chksumVCountry.Checked == true)
                    {
                        strsumVCountry = "1";
                    }
                    else
                    {
                        strsumVCountry = "0";
                    }
                }
                if (chkshowUDF_CName.Checked == true)
                {
                    strshowUDF_CName = "1";
                    strUDF_CName = txtUDF_CName.Text.ToString();
                    if (chkcompUDF_CName.Checked == true)
                    {
                        strcompUDF_CName = "1";
                    }
                    else
                    {
                        strcompUDF_CName = "0";
                    }

                    if (chksumUDF_CName.Checked == true)
                    {
                        strsumUDF_CName = "1";
                    }
                    else
                    {
                        strsumUDF_CName = "0";
                    }
                }
                if (chkshowUDF_DelegateType.Checked == true)
                {
                    strshowUDF_DelegateType = "1";
                    strUDF_DelegateType = txtUDF_DelegateType.Text.ToString();
                    if (chkcompUDF_DelegateType.Checked == true)
                    {
                        strcompUDF_DelegateType = "1";
                    }
                    else
                    {
                        strcompUDF_DelegateType = "0";
                    }

                    if (chksumUDF_DelegateType.Checked == true)
                    {
                        strsumUDF_DelegateType = "1";
                    }
                    else
                    {
                        strsumUDF_DelegateType = "0";
                    }
                }
                if (chkshowUDF_ProfCategory.Checked == true)
                {
                    strshowUDF_ProfCategory = "1";
                    strUDF_ProfCategory = txtUDF_ProfCategory.Text.ToString();
                    if (chkcompUDF_ProfCategory.Checked == true)
                    {
                        strcompUDF_ProfCategory = "1";
                    }
                    else
                    {
                        strcompUDF_ProfCategory = "0";
                    }

                    if (chksumUDF_ProfCategory.Checked == true)
                    {
                        strsumUDF_ProfCategory = "1";
                    }
                    else
                    {
                        strsumUDF_ProfCategory = "0";
                    }
                }
                if (chkshowUDF_ProfCategoryOther.Checked == true)
                {
                    strshowUDF_ProfCategoryOther = "1";
                    strUDF_ProfCategoryOther = txtUDF_ProfCategoryOther.Text.ToString();
                    if (chkcompUDF_ProfCategoryOther.Checked == true)
                    {
                        strcompUDF_ProfCategoryOther = "1";
                    }
                    else
                    {
                        strcompUDF_ProfCategoryOther = "0";
                    }

                    if (chksumUDF_ProfCategoryOther.Checked == true)
                    {
                        strsumUDF_ProfCategoryOther = "1";
                    }
                    else
                    {
                        strsumUDF_ProfCategoryOther = "0";
                    }
                }
                if (chkshowUDF_CPcode.Checked == true)
                {
                    strshowUDF_CPcode = "1";
                    strUDF_CPcode = txtUDF_CPcode.Text.ToString();
                    if (chkcompUDF_CPcode.Checked == true)
                    {
                        strcompUDF_CPcode = "1";
                    }
                    else
                    {
                        strcompUDF_CPcode = "0";
                    }

                    if (chksumUDF_CPcode.Checked == true)
                    {
                        strsumUDF_CPcode = "1";
                    }
                    else
                    {
                        strsumUDF_CPcode = "0";
                    }
                }
                if (chkshowUDF_CLDepartment.Checked == true)
                {
                    strshowUDF_CLDepartment = "1";
                    strUDF_CLDepartment = txtUDF_CLDepartment.Text.ToString();
                    if (chkcompUDF_CLDepartment.Checked == true)
                    {
                        strcompUDF_CLDepartment = "1";
                    }
                    else
                    {
                        strcompUDF_CLDepartment = "0";
                    }

                    if (chksumUDF_CLDepartment.Checked == true)
                    {
                        strsumUDF_CLDepartment = "1";
                    }
                    else
                    {
                        strsumUDF_CLDepartment = "0";
                    }
                }
                if (chkshowUDF_CAddress.Checked == true)
                {
                    strshowUDF_CAddress = "1";
                    strUDF_CAddress = txtUDF_CAddress.Text.ToString();
                    if (chkcompUDF_CAddress.Checked == true)
                    {
                        strcompUDF_CAddress = "1";
                    }
                    else
                    {
                        strcompUDF_CAddress = "0";
                    }

                    if (chksumUDF_CAddress.Checked == true)
                    {
                        strsumUDF_CAddress = "1";
                    }
                    else
                    {
                        strsumUDF_CAddress = "0";
                    }
                }
                if (chkshowUDF_CLCompany.Checked == true)
                {
                    strshowUDF_CLCompany = "1";
                    strUDF_CLCompany = txtUDF_CLCompany.Text.ToString();
                    if (chkcompUDF_CLCompany.Checked == true)
                    {
                        strcompUDF_CLCompany = "1";
                    }
                    else
                    {
                        strcompUDF_CLCompany = "0";
                    }

                    if (chksumUDF_CLCompany.Checked == true)
                    {
                        strsumUDF_CLCompany = "1";
                    }
                    else
                    {
                        strsumUDF_CLCompany = "0";
                    }
                }
                if (chkshowUDF_CLCompanyOther.Checked == true)
                {
                    strshowUDF_CLCompanyOther = "1";
                    strUDF_CLCompanyOther = txtUDF_CLCompanyOther.Text.ToString();
                    if (chkcompUDF_CLCompanyOther.Checked == true)
                    {
                        strcompUDF_CLCompanyOther = "1";
                    }
                    else
                    {
                        strcompUDF_CLCompanyOther = "0";
                    }

                    if (chksumUDF_CLCompanyOther.Checked == true)
                    {
                        strsumUDF_CLCompanyOther = "1";
                    }
                    else
                    {
                        strsumUDF_CLCompanyOther = "0";
                    }
                }
                if (chkshowUDF_CCountry.Checked == true)
                {
                    strshowUDF_CCountry = "1";
                    strUDF_CCountry = txtUDF_CCountry.Text.ToString();
                    if (chkcompUDF_CCountry.Checked == true)
                    {
                        strcompUDF_CCountry = "1";
                    }
                    else
                    {
                        strcompUDF_CCountry = "0";
                    }

                    if (chksumUDF_CCountry.Checked == true)
                    {
                        strsumUDF_CCountry = "1";
                    }
                    else
                    {
                        strsumUDF_CCountry = "0";
                    }
                }
                if (chkshowSupervisorName.Checked == true)
                {
                    strshowSupervisorName = "1";
                    strSupervisorName = txtSupervisorName.Text.ToString();
                    if (chkcompSupervisorName.Checked == true)
                    {
                        strcompSupervisorName = "1";
                    }
                    else
                    {
                        strcompSupervisorName = "0";
                    }

                    if (chksumSupervisorName.Checked == true)
                    {
                        strsumSupervisorName = "1";
                    }
                    else
                    {
                        strsumSupervisorName = "0";
                    }
                }
                if (chkshowSupervisorDesignation.Checked == true)
                {
                    strshowSupervisorDesignation = "1";
                    strSupervisorDesignation = txtSupervisorDesignation.Text.ToString();
                    if (chkcompSupervisorDesignation.Checked == true)
                    {
                        strcompSupervisorDesignation = "1";
                    }
                    else
                    {
                        strcompSupervisorDesignation = "0";
                    }

                    if (chksumSupervisorDesignation.Checked == true)
                    {
                        strsumSupervisorDesignation = "1";
                    }
                    else
                    {
                        strsumSupervisorDesignation = "0";
                    }
                }
                if (chkshowSupervisorContact.Checked == true)
                {
                    strshowSupervisorContact = "1";
                    strSupervisorContact = txtSupervisorContact.Text.ToString();
                    if (chkcompSupervisorContact.Checked == true)
                    {
                        strcompSupervisorContact = "1";
                    }
                    else
                    {
                        strcompSupervisorContact = "0";
                    }

                    if (chksumSupervisorContact.Checked == true)
                    {
                        strsumSupervisorContact = "1";
                    }
                    else
                    {
                        strsumSupervisorContact = "0";
                    }
                }
                if (chkshowSupervisorEmail.Checked == true)
                {
                    strshowSupervisorEmail = "1";
                    strSupervisorEmail = txtSupervisorEmail.Text.ToString();
                    if (chkcompSupervisorEmail.Checked == true)
                    {
                        strcompSupervisorEmail = "1";
                    }
                    else
                    {
                        strcompSupervisorEmail = "0";
                    }

                    if (chksumSupervisorEmail.Checked == true)
                    {
                        strsumSupervisorEmail = "1";
                    }
                    else
                    {
                        strsumSupervisorEmail = "0";
                    }
                }
                if (chkshowOtherSalutation.Checked == true)
                {
                    strshowOtherSalutation = "1";
                    strOtherSalutation = txtOtherSalutation.Text.ToString();
                    if (chkcompOtherSalutation.Checked == true)
                    {
                        strcompOtherSalutation = "1";
                    }
                    else
                    {
                        strcompOtherSalutation = "0";
                    }

                    if (chksumOtherSalutation.Checked == true)
                    {
                        strsumOtherSalutation = "1";
                    }
                    else
                    {
                        strsumOtherSalutation = "0";
                    }
                }
                if (chkshowOtherProfession.Checked == true)
                {
                    strshowOtherProfession = "1";
                    strOtherProfession = txtOtherProfession.Text.ToString();
                    if (chkcompOtherProfession.Checked == true)
                    {
                        strcompOtherProfession = "1";
                    }
                    else
                    {
                        strcompOtherProfession = "0";
                    }

                    if (chksumOtherProfession.Checked == true)
                    {
                        strsumOtherProfession = "1";
                    }
                    else
                    {
                        strsumOtherProfession = "0";
                    }
                }
                if (chkshowOtherDepartment.Checked == true)
                {
                    strshowOtherDepartment = "1";
                    strOtherDepartment = txtOtherDepartment.Text.ToString();
                    if (chkcompOtherDepartment.Checked == true)
                    {
                        strcompOtherDepartment = "1";
                    }
                    else
                    {
                        strcompOtherDepartment = "0";
                    }

                    if (chksumOtherDepartment.Checked == true)
                    {
                        strsumOtherDepartment = "1";
                    }
                    else
                    {
                        strsumOtherDepartment = "0";
                    }
                }
                if (chkshowOtherOrganization.Checked == true)
                {
                    strshowOtherOrganization = "1";
                    strOtherOrganization = txtOtherOrganization.Text.ToString();
                    if (chkcompOtherOrganization.Checked == true)
                    {
                        strcompOtherOrganization = "1";
                    }
                    else
                    {
                        strcompOtherOrganization = "0";
                    }

                    if (chksumOtherOrganization.Checked == true)
                    {
                        strsumOtherOrganization = "1";
                    }
                    else
                    {
                        strsumOtherOrganization = "0";
                    }
                }
                if (chkshowOtherInstitution.Checked == true)
                {
                    strshowOtherInstitution = "1";
                    strOtherInstitution = txtOtherInstitution.Text.ToString();
                    if (chkcompOtherInstitution.Checked == true)
                    {
                        strcompOtherInstitution = "1";
                    }
                    else
                    {
                        strcompOtherInstitution = "0";
                    }

                    if (chksumOtherInstitution.Checked == true)
                    {
                        strsumOtherInstitution = "1";
                    }
                    else
                    {
                        strsumOtherInstitution = "0";
                    }
                }
                if (chkshowAEmail.Checked == true)
                {
                    strshowAEmail = "1";
                    strAEmail = txtAEmail.Text.ToString();
                    if (chkcompAEmail.Checked == true)
                    {
                        strcompAEmail = "1";
                    }
                    else
                    {
                        strcompAEmail = "0";
                    }

                    if (chksumAEmail.Checked == true)
                    {
                        strsumAEmail = "1";
                    }
                    else
                    {
                        strsumAEmail = "0";
                    }
                }
                if (chkshowIsSMS.Checked == true)
                {
                    strshowIsSMS = "1";
                    strIsSMS = txtIsSMS.Text.ToString();
                    if (chkcompIsSMS.Checked == true)
                    {
                        strcompIsSMS = "1";
                    }
                    else
                    {
                        strcompIsSMS = "0";
                    }

                    if (chksumIsSMS.Checked == true)
                    {
                        strsumIsSMS = "1";
                    }
                    else
                    {
                        strsumIsSMS = "0";
                    }
                }
                if (chkshowAge.Checked == true)
                {
                    strshowAge = "1";
                    strAge = txtAge.Text.ToString();
                    if (chkcompAge.Checked == true)
                    {
                        strcompAge = "1";
                    }
                    else
                    {
                        strcompAge = "0";
                    }

                    if (chksumAge.Checked == true)
                    {
                        strsumAge = "1";
                    }
                    else
                    {
                        strsumAge = "0";
                    }
                }
                if (chkshowGender.Checked == true)
                {
                    strshowGender = "1";
                    strGender = txtGender.Text.ToString();
                    if (chkcompGender.Checked == true)
                    {
                        strcompGender = "1";
                    }
                    else
                    {
                        strcompGender = "0";
                    }

                    if (chksumGender.Checked == true)
                    {
                        strsumGender = "1";
                    }
                    else
                    {
                        strsumGender = "0";
                    }
                }
                if (chkshowDOB.Checked == true)
                {
                    strshowDOB = "1";
                    strDOB = txtDOB.Text.ToString();
                    if (chkcompDOB.Checked == true)
                    {
                        strcompDOB = "1";
                    }
                    else
                    {
                        strcompDOB = "0";
                    }

                    if (chksumDOB.Checked == true)
                    {
                        strsumDOB = "1";
                    }
                    else
                    {
                        strsumDOB = "0";
                    }
                }
                if (chkshowAdditional4.Checked == true)
                {
                    strshowAdditional4 = "1";
                    strAdditional4 = txtAdditional4.Text.ToString();
                    if (chkcompAdditional4.Checked == true)
                    {
                        strcompAdditional4 = "1";
                    }
                    else
                    {
                        strcompAdditional4 = "0";
                    }

                    if (chksumAdditional4.Checked == true)
                    {
                        strsumAdditional4 = "1";
                    }
                    else
                    {
                        strsumAdditional4 = "0";
                    }
                }
                if (chkshowAdditional5.Checked == true)
                {
                    strshowAdditional5 = "1";
                    strAdditional5 = txtAdditional5.Text.ToString();
                    if (chkcompAdditional5.Checked == true)
                    {
                        strcompAdditional5 = "1";
                    }
                    else
                    {
                        strcompAdditional5 = "0";
                    }

                    if (chksumAdditional5.Checked == true)
                    {
                        strsumAdditional5 = "1";
                    }
                    else
                    {
                        strsumAdditional5 = "0";
                    }
                }
                #endregion

                string sql1 = "Update tb_Form Set form_input_isshow=" + strshowSalutation + ",form_input_isrequired=" + strcompSalutation + ",form_input_text=N'" + strSalutation + "',form_input_active=" + strsumSalutation + " Where form_input_name='" + _Salutation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFname + ",form_input_isrequired=" + strcompFname + ",form_input_text=N'" + strFname + "',form_input_active=" + strsumFname + " Where form_input_name='" + _FName + "'  And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowLname + ",form_input_isrequired=" + strcompLname + ",form_input_text=N'" + strLname + "',form_input_active=" + strsumLname + " Where form_input_name='" + _LName + "'  And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOname + ",form_input_isrequired=" + strcompOname + ",form_input_text=N'" + strOname + "',form_input_active=" + strsumOname + " Where form_input_name='" + _Oname + "'  And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowPassportNo + ",form_input_isrequired=" + strcompPassportNo + ",form_input_text=N'" + strPassportNo + "',form_input_active=" + strsumPassportNo + " Where form_input_name='" + _PassportNo + "'  And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowIsRegistered + ",form_input_isrequired=" + strcompIsRegistered + ",form_input_text=N'" + strIsRegistered + "',form_input_active=" + strsumIsRegistered + " Where form_input_name='" + _IsRegistered + "'  And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowRegSpecific + ",form_input_isrequired=" + strcompRegSpecific + ",form_input_text=N'" + strRegSpecific + "',form_input_active=" + strsumRegSpecific + " Where form_input_name='" + _RegSpecific + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowIDNo + ",form_input_isrequired=" + strcompIDNo + ",form_input_text=N'" + strIDNo + "',form_input_active=" + strsumIDNo + " Where form_input_name='" + _IDNo + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDesignation + ",form_input_isrequired=" + strcompDesignation + ",form_input_text=N'" + strDesignation + "',form_input_active=" + strsumDesignation + " Where form_input_name='" + _Designation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowProfession + ",form_input_isrequired=" + strcompProfession + ",form_input_text=N'" + strProfession + "',form_input_active=" + strsumProfession + " Where form_input_name='" + _Profession + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDepartment + ",form_input_isrequired=" + strcompDepartment + ",form_input_text=N'" + strDepartment + "',form_input_active=" + strsumDepartment + " Where form_input_name='" + _Department + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOrganization + ",form_input_isrequired=" + strcompOrganization + ",form_input_text=N'" + strOrganization + "',form_input_active=" + strsumOrganization + " Where form_input_name='" + _Organization + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowInstitution + ",form_input_isrequired=" + strcompInstitution + ",form_input_text=N'" + strInstitution + "',form_input_active=" + strsumInstitution + " Where form_input_name='" + _Institution + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress1 + ",form_input_isrequired=" + strcompAddress1 + ",form_input_text=N'" + strAddress1 + "',form_input_active=" + strsumAddress1 + " Where form_input_name='" + _Address1 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress2 + ",form_input_isrequired=" + strcompAddress2 + ",form_input_text=N'" + strAddress2 + "',form_input_active=" + strsumAddress2 + " Where form_input_name='" + _Address2 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress3 + ",form_input_isrequired=" + strcompAddress3 + ",form_input_text=N'" + strAddress3 + "',form_input_active=" + strsumAddress3 + " Where form_input_name='" + _Address3 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress4 + ",form_input_isrequired=" + strcompAddress4 + ",form_input_text=N'" + strAddress4 + "',form_input_active=" + strsumAddress4 + " Where form_input_name='" + _Address4 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCity + ",form_input_isrequired=" + strcompCity + ",form_input_text=N'" + strCity + "',form_input_active=" + strsumCity + " Where form_input_name='" + _City + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowState + ",form_input_isrequired=" + strcompState + ",form_input_text=N'" + strState + "',form_input_active=" + strsumState + " Where form_input_name='" + _State + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowPostalCode + ",form_input_isrequired=" + strcompPostalCode + ",form_input_text=N'" + strPostalCode + "',form_input_active=" + strsumPostalCode + " Where form_input_name='" + _PostalCode + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCountry + ",form_input_isrequired=" + strcompCountry + ",form_input_text=N'" + strCountry + "',form_input_active=" + strsumCountry + " Where form_input_name='" + _Country + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowRCountry + ",form_input_isrequired=" + strcompRCountry + ",form_input_text=N'" + strRCountry + "',form_input_active=" + strsumRCountry + " Where form_input_name='" + _RCountry + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelCC + ",form_input_isrequired=" + strcompTelCC + ",form_input_text=N'" + strTelCC + "',form_input_active=" + strsumTelCC + " Where form_input_name='" + _Telcc + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelAC + ",form_input_isrequired=" + strcompTelAC + ",form_input_text=N'" + strTelAC + "',form_input_active=" + strsumTelAC + " Where form_input_name='" + _Telac + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTel + ",form_input_isrequired=" + strcompTel + ",form_input_text=N'" + strTel + "',form_input_active=" + strsumTel + " Where form_input_name='" + _Tel + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobileCC + ",form_input_isrequired=" + strcompMobileCC + ",form_input_text=N'" + strMobileCC + "',form_input_active=" + strsumMobileCC + " Where form_input_name='" + _Mobilecc + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobileAC + ",form_input_isrequired=" + strcompMobileAC + ",form_input_text=N'" + strMobileAC + "',form_input_active=" + strsumMobileAC + " Where form_input_name='" + _Mobileac + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMobile + ",form_input_isrequired=" + strcompMobile + ",form_input_text=N'" + strMobile + "',form_input_active=" + strsumMobile + " Where form_input_name='" + _Mobile + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxCC + ",form_input_isrequired=" + strcompFaxCC + ",form_input_text=N'" + strFaxCC + "',form_input_active=" + strsumFaxCC + " Where form_input_name='" + _Faxcc + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxAC + ",form_input_isrequired=" + strcompFaxAC + ",form_input_text=N'" + strFaxAC + "',form_input_active=" + strsumFaxAC + " Where form_input_name='" + _Faxac + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFax + ",form_input_isrequired=" + strcompFax + ",form_input_text=N'" + strFax + "',form_input_active=" + strsumFax + " Where form_input_name='" + _Fax + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmail + ",form_input_isrequired=" + strcompEmail + ",form_input_text=N'" + strEmail + "',form_input_active=" + strsumEmail + " Where form_input_name='" + _Email + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmailConfirmation + ",form_input_isrequired=" + strcompEmailConfirmation + ",form_input_text=N'" + strEmailConfirmation + "',form_input_active=" + strsumEmailConfirmation + " Where form_input_name='" + _EmailConfirmation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAffiliation + ",form_input_isrequired=" + strcompAffiliation + ",form_input_text=N'" + strAffiliation + "',form_input_active=" + strsumAffiliation + " Where form_input_name='" + _Affiliation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDietary + ",form_input_isrequired=" + strcompDietary + ",form_input_text=N'" + strDietary + "',form_input_active=" + strsumDietary + " Where form_input_name='" + _Dietary + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowNationality + ",form_input_isrequired=" + strcompNationality + ",form_input_text=N'" + strNationality + "',form_input_active=" + strsumNationality + " Where form_input_name='" + _Nationality + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowMembershipNo + ",form_input_isrequired=" + strcompMembershipNo + ",form_input_text=N'" + strMembershipNo + "',form_input_active=" + strsumMembershipNo + " Where form_input_name='" + _MembershipNo + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVname + ",form_input_isrequired=" + strcompVname + ",form_input_text=N'" + strVname + "',form_input_active=" + strsumVname + " Where form_input_name='" + _VName + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVDOB + ",form_input_isrequired=" + strcompVDOB + ",form_input_text=N'" + strVDOB + "',form_input_active=" + strsumVDOB + " Where form_input_name='" + _VDOB + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVPassNo + ",form_input_isrequired=" + strcompVPassNo + ",form_input_text=N'" + strVPassNo + "',form_input_active=" + strsumVPassNo + " Where form_input_name='" + _VPassNo + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVPassExpiry + ",form_input_isrequired=" + strcompVPassExpiry + ",form_input_text=N'" + strVPassExpiry + "',form_input_active=" + strsumVPassExpiry + " Where form_input_name='" + _VPassExpiry + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVPassIssueDate + ",form_input_isrequired=" + strcompVPassIssueDate + ",form_input_text=N'" + strVPassIssueDate + "',form_input_active=" + strsumVPassIssueDate + " Where form_input_name='" + _VPassIssueDate + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVEmbarkation + ",form_input_isrequired=" + strcompVEmbarkation + ",form_input_text=N'" + strVEmbarkation + "',form_input_active=" + strsumVEmbarkation + " Where form_input_name='" + _VEmbarkation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVArrivalDate + ",form_input_isrequired=" + strcompVArrivalDate + ",form_input_text=N'" + strVArrivalDate + "',form_input_active=" + strsumVArrivalDate + " Where form_input_name='" + _VArrivalDate + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowVCountry + ",form_input_isrequired=" + strcompVCountry + ",form_input_text=N'" + strVCountry + "',form_input_active=" + strsumVCountry + " Where form_input_name='" + _VCountry + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CName + ",form_input_isrequired=" + strcompUDF_CName + ",form_input_text=N'" + strUDF_CName + "',form_input_active=" + strsumUDF_CName + " Where form_input_name='" + _UDF_CName + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_DelegateType + ",form_input_isrequired=" + strcompUDF_DelegateType + ",form_input_text=N'" + strUDF_DelegateType + "',form_input_active=" + strsumUDF_DelegateType + " Where form_input_name='" + _UDF_DelegateType + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_ProfCategory + ",form_input_isrequired=" + strcompUDF_ProfCategory + ",form_input_text=N'" + strUDF_ProfCategory + "',form_input_active=" + strsumUDF_ProfCategory + " Where form_input_name='" + _UDF_ProfCategory + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_ProfCategoryOther + ",form_input_isrequired=" + strcompUDF_ProfCategoryOther + ",form_input_text=N'" + strUDF_ProfCategoryOther + "',form_input_active=" + strsumUDF_ProfCategoryOther + " Where form_input_name='" + _UDF_ProfCategoryOther + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CPcode + ",form_input_isrequired=" + strcompUDF_CPcode + ",form_input_text=N'" + strUDF_CPcode + "',form_input_active=" + strsumUDF_CPcode + " Where form_input_name='" + _UDF_CPcode + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CLDepartment + ",form_input_isrequired=" + strcompUDF_CLDepartment + ",form_input_text=N'" + strUDF_CLDepartment + "',form_input_active=" + strsumUDF_CLDepartment + " Where form_input_name='" + _UDF_CLDepartment + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CAddress + ",form_input_isrequired=" + strcompUDF_CAddress + ",form_input_text=N'" + strUDF_CAddress + "',form_input_active=" + strsumUDF_CAddress + " Where form_input_name='" + _UDF_CAddress + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CLCompany + ",form_input_isrequired=" + strcompUDF_CLCompany + ",form_input_text=N'" + strUDF_CLCompany + "',form_input_active=" + strsumUDF_CLCompany + " Where form_input_name='" + _UDF_CLCompany + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CLCompanyOther + ",form_input_isrequired=" + strcompUDF_CLCompanyOther + ",form_input_text=N'" + strUDF_CLCompanyOther + "',form_input_active=" + strsumUDF_CLCompanyOther + " Where form_input_name='" + _UDF_CLCompanyOther + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowUDF_CCountry + ",form_input_isrequired=" + strcompUDF_CCountry + ",form_input_text=N'" + strUDF_CCountry + "',form_input_active=" + strsumUDF_CCountry + " Where form_input_name='" + _UDF_CCountry + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowSupervisorName + ",form_input_isrequired=" + strcompSupervisorName + ",form_input_text=N'" + strSupervisorName + "',form_input_active=" + strsumSupervisorName + " Where form_input_name='" + _SupervisorName + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowSupervisorDesignation + ",form_input_isrequired=" + strcompSupervisorDesignation + ",form_input_text=N'" + strSupervisorDesignation + "',form_input_active=" + strsumSupervisorDesignation + " Where form_input_name='" + _SupervisorDesignation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowSupervisorContact + ",form_input_isrequired=" + strcompSupervisorContact + ",form_input_text=N'" + strSupervisorContact + "',form_input_active=" + strsumSupervisorContact + " Where form_input_name='" + _SupervisorContact + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowSupervisorEmail + ",form_input_isrequired=" + strcompSupervisorEmail + ",form_input_text=N'" + strSupervisorEmail + "',form_input_active=" + strsumSupervisorEmail + " Where form_input_name='" + _SupervisorEmail + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherSalutation + ",form_input_isrequired=" + strcompOtherSalutation + ",form_input_text=N'" + strOtherSalutation + "',form_input_active=" + strsumOtherSalutation + " Where form_input_name='" + _OtherSalutation + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherProfession + ",form_input_isrequired=" + strcompOtherProfession + ",form_input_text=N'" + strOtherProfession + "',form_input_active=" + strsumOtherProfession + " Where form_input_name='" + _OtherProfession + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherDepartment + ",form_input_isrequired=" + strcompOtherDepartment + ",form_input_text=N'" + strOtherDepartment + "',form_input_active=" + strsumOtherDepartment + " Where form_input_name='" + _OtherDepartment + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherOrganization + ",form_input_isrequired=" + strcompOtherOrganization + ",form_input_text=N'" + strOtherOrganization + "',form_input_active=" + strsumOtherOrganization + " Where form_input_name='" + _OtherOrganization + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowOtherInstitution + ",form_input_isrequired=" + strcompOtherInstitution + ",form_input_text=N'" + strOtherInstitution + "',form_input_active=" + strsumOtherInstitution + " Where form_input_name='" + _OtherInstitution + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAEmail + ",form_input_isrequired=" + strcompAEmail + ",form_input_text=N'" + strAEmail + "',form_input_active=" + strsumAEmail + " Where form_input_name='" + _AEmail + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowIsSMS + ",form_input_isrequired=" + strcompIsSMS + ",form_input_text=N'" + strIsSMS + "',form_input_active=" + strsumIsSMS + " Where form_input_name='" + _IsSMS + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAge + ",form_input_isrequired=" + strcompAge + ",form_input_text=N'" + strAge + "',form_input_active=" + strsumAge + " Where form_input_name='" + _Age + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowGender + ",form_input_isrequired=" + strcompGender + ",form_input_text=N'" + strGender + "',form_input_active=" + strsumGender + " Where form_input_name='" + _Gender + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowDOB + ",form_input_isrequired=" + strcompDOB + ",form_input_text=N'" + strDOB + "',form_input_active=" + strsumDOB + " Where form_input_name='" + _DOB + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional4 + ",form_input_isrequired=" + strcompAdditional4 + ",form_input_text=N'" + strAdditional4 + "',form_input_active=" + strsumAdditional4 + " Where form_input_name='" + _Additional4 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional5 + ",form_input_isrequired=" + strcompAdditional5 + ",form_input_text=N'" + strAdditional5 + "',form_input_active=" + strsumAdditional5 + " Where form_input_name='" + _Additional5 + "' And form_type='" + FormType.TypeDelegate + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";

                fn.ExecuteSQL(sql1);

                InsertQRCodeFields(flowid, showid);//*

                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-success alert-dismissible");
                lblMsg.Text = "Success.";

                setDynamicForm(flowid, showid);

                FlowControler flwObj = new FlowControler(fn);
                Dictionary<string, string> nValues = new Dictionary<string, string>();
                nValues = flwObj.GetFrontendFormByBckForm(flowid, System.IO.Path.GetFileName(Request.Url.AbsolutePath));
                if (nValues.Count > 0)
                {
                    string page = nValues["page"].ToString();
                    string step = nValues["step"].ToString();
                    string route = flwObj.MakeFullURL(page, flowid, showid, null, step);
                    route = route + "&t=" + cFUnz.EncryptValue(BackendStaticValueClass.isAdmin);

                    System.Drawing.Rectangle resolution =
                    System.Windows.Forms.Screen.PrimaryScreen.Bounds;
                    int width = resolution.Width;
                    int height = resolution.Height;

                    if (width <= 1014)
                    {
                        width = width - 100;
                    }
                    else
                    {
                        width = 1000;
                    }

                    RadWindow newWindow = new RadWindow();
                    newWindow.NavigateUrl = route;
                    newWindow.Height = 800;
                    newWindow.Width = width;
                    newWindow.VisibleOnPageLoad = true;
                    newWindow.KeepInScreenBounds = true;
                    newWindow.VisibleOnPageLoad = true;
                    newWindow.Visible = true;
                    newWindow.VisibleStatusbar = false;
                    RadWindowManager1.Windows.Add(newWindow);
                }
            }
            catch (Exception ex)
            {
                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-danger alert-dismissible");
                lblMsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region InsertQRCodeFields (check exist and delete old first after that insert)
    protected void InsertQRCodeFields(string flowid, string showid)
    {
        try
        {
            string formtype = FormType.TypeDelegate;
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("SHWID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("form_type", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            spar3.Value = formtype;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);

            DataTable dt = fn.GetDatasetByCommand("Select * From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID", "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                string deleteOldQRFields = "Delete From tb_site_flow_QRFieldList Where Form_type=@form_type And FlowID=@FlowID And ShowID=@SHWID";
                fn.ExecuteSQLWithParameters(deleteOldQRFields, pList);
            }

            int qrFieldCount = 0;
            #region Insert Into tb_site_flow_QRFieldList table
            string sql = string.Empty;
            if (!string.IsNullOrEmpty(txtqrSalutation.Text) && !string.IsNullOrWhiteSpace(txtqrSalutation.Text))//chkshowSalutation.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Salutation, txtqrSalutation.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrFname.Text) && !string.IsNullOrWhiteSpace(txtqrFname.Text))//chkshowFname.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_FName, txtqrFname.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrLname.Text) && !string.IsNullOrWhiteSpace(txtqrLname.Text))//chkshowLname.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_LName, txtqrLname.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrOname.Text) && !string.IsNullOrWhiteSpace(txtqrOname.Text))//chkshowOname.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_OName, txtqrOname.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrPassportNo.Text) && !string.IsNullOrWhiteSpace(txtqrPassportNo.Text))//chkshowPassportNo.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_PassNo, txtqrPassportNo.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrIsRegistered.Text) && !string.IsNullOrWhiteSpace(txtqrIsRegistered.Text))//chkshowIsRegistered.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_isReg, txtqrIsRegistered.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrRegSpecific.Text) && !string.IsNullOrWhiteSpace(txtqrRegSpecific.Text))//chkshowRegSpecific.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_sgregistered, txtqrRegSpecific.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrIDNo.Text) && !string.IsNullOrWhiteSpace(txtqrIDNo.Text))//chkshowIDNo.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_IDno, txtqrIDNo.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrDesignation.Text) && !string.IsNullOrWhiteSpace(txtqrDesignation.Text))//chkshowDesignation.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Designation, txtqrDesignation.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrProfession.Text) && !string.IsNullOrWhiteSpace(txtqrProfession.Text))//chkshowProfession.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Profession, txtqrProfession.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrDepartment.Text) && !string.IsNullOrWhiteSpace(txtqrDepartment.Text))//chkshowDepartment.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Department, txtqrDepartment.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrOrganization.Text) && !string.IsNullOrWhiteSpace(txtqrOrganization.Text))//chkshowOrganization.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Organization, txtqrOrganization.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrInstitution.Text) && !string.IsNullOrWhiteSpace(txtqrInstitution.Text))//chkshowInstitution.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Institution, txtqrInstitution.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrAddress1.Text) && !string.IsNullOrWhiteSpace(txtqrAddress1.Text))//chkshowAddress1.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Address1, txtqrAddress1.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrAddress2.Text) && !string.IsNullOrWhiteSpace(txtqrAddress2.Text))//chkshowAddress2.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Address2, txtqrAddress2.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrAddress3.Text) && !string.IsNullOrWhiteSpace(txtqrAddress3.Text))//chkshowAddress3.Checked &&
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Address3, txtqrAddress3.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrAddress4.Text) && !string.IsNullOrWhiteSpace(txtqrAddress4.Text))//chkshowAddress4.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Address4, txtqrAddress4.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrCity.Text) && !string.IsNullOrWhiteSpace(txtqrCity.Text))//chkshowCity.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_City, txtqrCity.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrState.Text) && !string.IsNullOrWhiteSpace(txtqrState.Text))//chkshowState.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_State, txtqrState.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrPostalCode.Text) && !string.IsNullOrWhiteSpace(txtqrPostalCode.Text))//chkshowPostalCode.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_PostalCode, txtqrPostalCode.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrCountry.Text) && !string.IsNullOrWhiteSpace(txtqrCountry.Text))//chkshowCountry.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Country, txtqrCountry.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrRCountry.Text) && !string.IsNullOrWhiteSpace(txtqrRCountry.Text))//chkshowRCountry.Checked &&
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_RCountry, txtqrRCountry.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrTelCC.Text) && !string.IsNullOrWhiteSpace(txtqrTelCC.Text))//chkshowTelCC.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Telcc, txtqrTelCC.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrTelAC.Text) && !string.IsNullOrWhiteSpace(txtqrTelAC.Text))//chkshowTelAC.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Telac, txtqrTelAC.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrTel.Text) && !string.IsNullOrWhiteSpace(txtqrTel.Text))//chkshowTel.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Tel, txtqrTel.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrMobileCC.Text) && !string.IsNullOrWhiteSpace(txtqrMobileCC.Text))//chkshowMobileCC.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Mobcc, txtqrMobileCC.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrMobileAC.Text) && !string.IsNullOrWhiteSpace(txtqrMobileAC.Text))//chkshowMobileAC.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Mobac, txtqrMobileAC.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrMobile.Text) && !string.IsNullOrWhiteSpace(txtqrMobile.Text))//chkshowMobile.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Mobile, txtqrMobile.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrFaxCC.Text) && !string.IsNullOrWhiteSpace(txtqrFaxCC.Text))//chkshowFaxCC.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Faxcc, txtqrFaxCC.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrFaxAC.Text) && !string.IsNullOrWhiteSpace(txtqrFaxAC.Text))//chkshowFaxAC.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Faxac, txtqrFaxAC.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrFax.Text) && !string.IsNullOrWhiteSpace(txtqrFax.Text))//chkshowFax.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Fax, txtqrFax.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrEmail.Text) && !string.IsNullOrWhiteSpace(txtqrEmail.Text))//chkshowEmail.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Email, txtqrEmail.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrAffiliation.Text) && !string.IsNullOrWhiteSpace(txtqrAffiliation.Text))//chkshowAffiliation.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Affiliation, txtqrAffiliation.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrDietary.Text) && !string.IsNullOrWhiteSpace(txtqrDietary.Text))//chkshowDietary.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Dietary, txtqrDietary.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrNationality.Text) && !string.IsNullOrWhiteSpace(txtqrNationality.Text))//chkshowNationality.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Nationality, txtqrNationality.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrMembershipNo.Text) && !string.IsNullOrWhiteSpace(txtqrMembershipNo.Text))//chkshowMembershipNo.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Membershipno, txtqrMembershipNo.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrVname.Text) && !string.IsNullOrWhiteSpace(txtqrVname.Text))//chkshowVname.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_vName, txtqrVname.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrVDOB.Text) && !string.IsNullOrWhiteSpace(txtqrVDOB.Text))//chkshowVDOB.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_vDOB, txtqrVDOB.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrVPassNo.Text) && !string.IsNullOrWhiteSpace(txtqrVPassNo.Text))//chkshowVPassNo.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_vPassno, txtqrVPassNo.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrVPassIssueDate.Text) && !string.IsNullOrWhiteSpace(txtqrVPassIssueDate.Text))//chkshowVPassIssueDate.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_vIssueDate, txtqrVPassIssueDate.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrVPassExpiry.Text) && !string.IsNullOrWhiteSpace(txtqrVPassExpiry.Text))//chkshowVPassExpiry.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_vPassexpiry, txtqrVPassExpiry.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrVEmbarkation.Text) && !string.IsNullOrWhiteSpace(txtqrVEmbarkation.Text))//chkshowVEmbarkation.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_vEmbarkation, txtqrVEmbarkation.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrVArrivalDate.Text) && !string.IsNullOrWhiteSpace(txtqrVArrivalDate.Text))//chkshowVArrivalDate.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_vArrivalDate, txtqrVArrivalDate.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrVCountry.Text) && !string.IsNullOrWhiteSpace(txtqrVCountry.Text))//chkshowVCountry.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_vCountry, txtqrVCountry.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrUDF_CName.Text) && !string.IsNullOrWhiteSpace(txtqrUDF_CName.Text))//chkshowUDF_CName.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.UDF_CName, txtqrUDF_CName.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrUDF_DelegateType.Text) && !string.IsNullOrWhiteSpace(txtqrUDF_DelegateType.Text))//chkshowUDF_DelegateType.Checked &&
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.UDF_DelegateType, txtqrUDF_DelegateType.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrUDF_ProfCategory.Text) && !string.IsNullOrWhiteSpace(txtqrUDF_ProfCategory.Text))//chkshowUDF_ProfCategory.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.UDF_ProfCategory, txtqrUDF_ProfCategory.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrUDF_ProfCategoryOther.Text) && !string.IsNullOrWhiteSpace(txtqrUDF_ProfCategoryOther.Text))//chkshowUDF_ProfCategoryOther.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.UDF_ProfCategoryOther, txtqrUDF_ProfCategoryOther.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrUDF_CPcode.Text) && !string.IsNullOrWhiteSpace(txtqrUDF_CPcode.Text))//chkshowUDF_CPcode.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.UDF_CPcode, txtqrUDF_CPcode.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrUDF_CLDepartment.Text) && !string.IsNullOrWhiteSpace(txtqrUDF_CLDepartment.Text))//chkshowUDF_CLDepartment.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.UDF_CLDepartment, txtqrUDF_CLDepartment.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrUDF_CAddress.Text) && !string.IsNullOrWhiteSpace(txtqrUDF_CAddress.Text))//chkshowUDF_CAddress.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.UDF_CAddress, txtqrUDF_CAddress.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrUDF_CLCompany.Text) && !string.IsNullOrWhiteSpace(txtqrUDF_CLCompany.Text))//chkshowUDF_CLCompany.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.UDF_CLCompany, txtqrUDF_CLCompany.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrUDF_CLCompanyOther.Text) && !string.IsNullOrWhiteSpace(txtqrUDF_CLCompanyOther.Text))//chkshowUDF_CLCompanyOther.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.UDF_CLCompanyOther, txtqrUDF_CLCompanyOther.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrUDF_CCountry.Text) && !string.IsNullOrWhiteSpace(txtqrUDF_CCountry.Text))//chkshowUDF_CCountry.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.UDF_CCountry, txtqrUDF_CCountry.Text.Trim());
                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrSupervisorName.Text) && !string.IsNullOrWhiteSpace(txtqrSupervisorName.Text))//chkshowSupervisorName.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_SupervisorName, txtqrSupervisorName.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrSupervisorDesignation.Text) && !string.IsNullOrWhiteSpace(txtqrSupervisorDesignation.Text))//chkshowSupervisorDesignation.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_SupervisorDesignation, txtqrSupervisorDesignation.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrSupervisorContact.Text) && !string.IsNullOrWhiteSpace(txtqrSupervisorContact.Text))//chkshowSupervisorContact.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_SupervisorContact, txtqrSupervisorContact.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrSupervisorEmail.Text) && !string.IsNullOrWhiteSpace(txtqrSupervisorEmail.Text))//chkshowSupervisorEmail.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_SupervisorEmail, txtqrSupervisorEmail.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrOtherSalutation.Text) && !string.IsNullOrWhiteSpace(txtqrOtherSalutation.Text))//chkshowOtherSalutation.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_SalutationOthers, txtqrOtherSalutation.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrOtherProfession.Text) && !string.IsNullOrWhiteSpace(txtqrOtherProfession.Text))//chkshowOtherProfession.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_otherProfession, txtqrOtherProfession.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrOtherDepartment.Text) && !string.IsNullOrWhiteSpace(txtqrOtherDepartment.Text))//chkshowOtherDepartment.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_otherDepartment, txtqrOtherDepartment.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrOtherOrganization.Text) && !string.IsNullOrWhiteSpace(txtqrOtherOrganization.Text))//chkshowOtherOrganization.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_otherOrganization, txtqrOtherOrganization.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrOtherInstitution.Text) && !string.IsNullOrWhiteSpace(txtqrOtherInstitution.Text))//chkshowOtherInstitution.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_otherInstitution, txtqrOtherInstitution.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrAEmail.Text) && !string.IsNullOrWhiteSpace(txtqrAEmail.Text))//chkshowAEmail.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_aemail, txtqrAEmail.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrIsSMS.Text) && !string.IsNullOrWhiteSpace(txtqrIsSMS.Text))//chkshowIsSMS.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_isSMS, txtqrIsSMS.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrAge.Text) && !string.IsNullOrWhiteSpace(txtqrAge.Text))//chkshowAge.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Age, txtqrAge.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrGender.Text) && !string.IsNullOrWhiteSpace(txtqrGender.Text))//chkshowGender.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Gender, txtqrGender.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrDOB.Text) && !string.IsNullOrWhiteSpace(txtqrDOB.Text))//chkshowDOB.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_DOB, txtqrDOB.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrAdditional4.Text) && !string.IsNullOrWhiteSpace(txtqrAdditional4.Text))//chkshowAdditional4.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Additional4, txtqrAdditional4.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            if (!string.IsNullOrEmpty(txtqrAdditional5.Text) && !string.IsNullOrWhiteSpace(txtqrAdditional5.Text))//chkshowAdditional5.Checked && 
            {
                qrFieldCount++;
                sql = string.Format("Insert Into tb_site_flow_QRFieldList (ShowID,FlowID,Form_type,FieldID,FieldOrder)"
                            + " Values('{0}', '{1}', '{2}', '{3}', {4})"
                            , showid, flowid, formtype, tbRegDelegateFields.reg_Additional5, txtqrAdditional5.Text.Trim());

                fn.ExecuteSQL(sql);
            }
            #endregion

            #region insert tb_site_flow_QRFieldMaster
            if (qrFieldCount > 0)
            {
                string qrID = string.Empty;
                List<SqlParameter> pListmaster = new List<SqlParameter>();
                SqlParameter sparmaster1 = new SqlParameter("SHWID", SqlDbType.NVarChar);
                SqlParameter sparmaster2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
                sparmaster1.Value = showid;
                sparmaster2.Value = flowid;
                pListmaster.Add(sparmaster1);
                pListmaster.Add(sparmaster2);
                string sqlQRFieldMaster = "Select* From tb_site_flow_QRFieldMaster Where ShowID=@SHWID And FlowID=@FlowID";
                DataTable dtMaster = fn.GetDatasetByCommand(sqlQRFieldMaster, "ds", pListmaster).Tables[0];
                if (dtMaster.Rows.Count > 0)
                {
                    qrID = dtMaster.Rows[0]["QRID"].ToString();
                }
                else
                {
                    RunNumber run = new RunNumber(fn);
                    qrID = run.GetRunNUmber("QRMAS_KEY");
                    string sqlQRMasterInsert = "Insert Into tb_site_flow_QRFieldMaster (QRID, ShowID, FlowID, Form_type, DSource)"
                                                + " Values (@QRID, @SHWID, @FlowID, @Form_type, @DSource)";
                    List<SqlParameter> pListQRMasterInsert = new List<SqlParameter>();
                    SqlParameter sparQRMasterInsert1 = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    SqlParameter sparQRMasterInsert2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
                    SqlParameter sparQRMasterInsert3 = new SqlParameter("QRID", SqlDbType.NVarChar);
                    SqlParameter sparQRMasterInsert4 = new SqlParameter("Form_type", SqlDbType.NVarChar);
                    SqlParameter sparQRMasterInsert5 = new SqlParameter("DSource", SqlDbType.NVarChar);
                    sparQRMasterInsert1.Value = showid;
                    sparQRMasterInsert2.Value = flowid;
                    sparQRMasterInsert3.Value = qrID;
                    sparQRMasterInsert4.Value = BackendRegType.backendRegType_Delegate;
                    sparQRMasterInsert5.Value = datasource_delegate;
                    pListQRMasterInsert.Add(sparQRMasterInsert1);
                    pListQRMasterInsert.Add(sparQRMasterInsert2);
                    pListQRMasterInsert.Add(sparQRMasterInsert3);
                    pListQRMasterInsert.Add(sparQRMasterInsert4);
                    pListQRMasterInsert.Add(sparQRMasterInsert5);
                    fn.ExecuteSQLWithParameters(sqlQRMasterInsert, pListQRMasterInsert);
                }

                if(!string.IsNullOrEmpty(qrID))
                {
                    ////update QRID in tb_site_flow_QRFieldList table
                    string sqlQRFieldListUpdate = "Update tb_site_flow_QRFieldList Set QRID=@QRID Where ShowID=@SHWID And FlowID=@FlowID And Form_type=@Form_type";
                    List<SqlParameter> pListQRMasterUpdate = new List<SqlParameter>();
                    SqlParameter sparQRMasterUpdate1 = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    SqlParameter sparQRMasterUpdate2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
                    SqlParameter sparQRMasterUpdate3 = new SqlParameter("QRID", SqlDbType.NVarChar);
                    SqlParameter sparQRMasterUpdate4 = new SqlParameter("Form_type", SqlDbType.NVarChar);
                    sparQRMasterUpdate1.Value = showid;
                    sparQRMasterUpdate2.Value = flowid;
                    sparQRMasterUpdate3.Value = qrID;
                    sparQRMasterUpdate4.Value = BackendRegType.backendRegType_Delegate;
                    pListQRMasterUpdate.Add(sparQRMasterUpdate1);
                    pListQRMasterUpdate.Add(sparQRMasterUpdate2);
                    pListQRMasterUpdate.Add(sparQRMasterUpdate3);
                    pListQRMasterUpdate.Add(sparQRMasterUpdate4);
                    fn.ExecuteSQLWithParameters(sqlQRFieldListUpdate, pListQRMasterUpdate);
                }
            }
            #endregion

            setDynamicForm(flowid, showid);
        }
        catch (Exception ex)
        {
            divMsg.Visible = true;
            divMsg.Attributes.Remove("class");
            divMsg.Attributes.Add("class", "alert alert-danger alert-dismissible");
            lblMsg.Text = "Error occur: " + ex.Message;
        }
    }
    #endregion

    #region tbRegDelegateFields
    private static class tbRegDelegateFields
    {
        public static string Regno = "Regno";
        public static string RegGroupID = "RegGroupID";
        public static string con_CategoryId = "con_CategoryId";
        public static string con_promoCatId = "con_promoCatId";
        public static string reg_Salutation = "reg_Salutation";
        public static string reg_FName = "reg_FName";
        public static string reg_LName = "reg_LName";
        public static string reg_OName = "reg_OName";
        public static string reg_PassNo = "reg_PassNo";
        public static string reg_isReg = "reg_isReg";
        public static string reg_sgregistered = "reg_sgregistered";
        public static string reg_IDno = "reg_IDno";
        public static string reg_staffid = "reg_staffid";
        public static string reg_Designation = "reg_Designation";
        public static string reg_Profession = "reg_Profession";
        public static string reg_Jobtitle_alliedstu = "reg_Jobtitle_alliedstu";
        public static string reg_Department = "reg_Department";
        public static string reg_Organization = "reg_Organization";
        public static string reg_Institution = "reg_Institution";
        public static string reg_Address1 = "reg_Address1";
        public static string reg_Address2 = "reg_Address2";
        public static string reg_Address3 = "reg_Address3";
        public static string reg_Address4 = "reg_Address4";
        public static string reg_City = "reg_City";
        public static string reg_State = "reg_State";
        public static string reg_PostalCode = "reg_PostalCode";
        public static string reg_Country = "reg_Country";
        public static string reg_RCountry = "reg_RCountry";
        public static string reg_Telcc = "reg_Telcc";
        public static string reg_Telac = "reg_Telac";
        public static string reg_Tel = "reg_Tel";
        public static string reg_Mobcc = "reg_Mobcc";
        public static string reg_Mobac = "reg_Mobac";
        public static string reg_Mobile = "reg_Mobile";
        public static string reg_Faxcc = "reg_Faxcc";
        public static string reg_Faxac = "reg_Faxac";
        public static string reg_Fax = "reg_Fax";
        public static string reg_Email = "reg_Email";
        public static string reg_Affiliation = "reg_Affiliation";
        public static string reg_Dietary = "reg_Dietary";
        public static string reg_Nationality = "reg_Nationality";
        public static string reg_Membershipno = "reg_Membershipno";
        public static string reg_vName = "reg_vName";
        public static string reg_vDOB = "reg_vDOB";
        public static string reg_vPassno = "reg_vPassno";
        public static string reg_vPassexpiry = "reg_vPassexpiry";
        public static string reg_vCountry = "reg_vCountry";
        public static string reg_vIssueDate = "reg_vIssueDate";
        public static string reg_vArrivalDate = "reg_vArrivalDate";
        public static string reg_vEmbarkation = "reg_vEmbarkation";
        public static string UDF_DelegateType = "UDF_DelegateType";
        public static string UDF_ProfCategory = "UDF_ProfCategory";
        public static string UDF_ProfCategoryOther = "UDF_ProfCategoryOther";
        public static string UDF_CName = "UDF_CName";
        public static string UDF_CPcode = "UDF_CPcode";
        public static string UDF_CLDepartment = "UDF_CLDepartment";
        public static string UDF_CAddress = "UDF_CAddress";
        public static string UDF_CLCompany = "UDF_CLCompany";
        public static string UDF_CLCompanyOther = "UDF_CLCompanyOther";
        public static string UDF_CCountry = "UDF_CCountry";
        public static string reg_SupervisorName = "reg_SupervisorName";
        public static string reg_SupervisorDesignation = "reg_SupervisorDesignation";
        public static string reg_SupervisorContact = "reg_SupervisorContact";
        public static string reg_SupervisorEmail = "reg_SupervisorEmail";
        public static string reg_SalutationOthers = "reg_SalutationOthers";
        public static string reg_otherProfession = "reg_otherProfession";
        public static string reg_otherDepartment = "reg_otherDepartment";
        public static string reg_otherOrganization = "reg_otherOrganization";
        public static string reg_otherInstitution = "reg_otherInstitution";
        public static string reg_aemail = "reg_aemail";
        public static string reg_remark = "reg_remark";
        public static string reg_remarkGUpload = "reg_remarkGUpload";
        public static string reg_isSMS = "reg_isSMS";
        public static string reg_approveStatus = "reg_approveStatus";
        public static string reg_datecreated = "reg_datecreated";
        public static string recycle = "recycle";
        public static string reg_urlFlowID = "reg_urlFlowID";
        public static string reg_Stage = "reg_Stage";
        public static string reg_Age = "reg_Age";
        public static string reg_DOB = "reg_DOB";
        public static string reg_Gender = "reg_Gender";
        public static string reg_Additional4 = "reg_Additional4";
        public static string reg_Additional5 = "reg_Additional5";
        public static string reg_Status = "reg_Status";
        public static string ShowID = "ShowID";
    }
    #endregion

    protected void btnSetupEmail_Onclick(object sender, EventArgs e)
    {
        string url = "Event_Flow_EmailConfig?" + Request.QueryString;
        Response.Redirect(url);
    }
}