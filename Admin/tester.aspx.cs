﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
public partial class Admin_tester : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btntester_Onclick(object sender, EventArgs e)
    {
        Functionality fn = new Functionality();
        FlowControler fControl = new FlowControler(fn);
        string showid = "SHW1001";
        string flwID = "F13";
        Dictionary<string, string> nValues = new Dictionary<string, string>();


        nValues = fControl.GetAdminNextRoute(flwID, "0");
        if (nValues.Count > 0)
        {
            string page = nValues["nURL"].ToString();
            string step = nValues["nStep"].ToString();
            string FlowID = nValues["FlowID"].ToString();

            if (page == "") page = "Event_Config_Final";


            string path = fControl.MakeFullURL(page, FlowID, showid,"", step);

            Response.Redirect(path);


        }


    }
}
