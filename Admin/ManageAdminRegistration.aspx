﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="ManageAdminRegistration.aspx.cs" Inherits="Admin_ManageAdminRegistration" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Dashboard | Manage Admin Registration</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="centercontent">
         <div class="pageheader">
            <h1 class="pagetitle">Manage Admin Registration</h1>
            <span class="pagedesc">Page For Managing Admin Registration.</span>

            <ul class="hornav">
                <li class="current"><a href="#Index">Manage Admin Registration</a></li>
            </ul>
        </div>
        <div id="contentwrapper" class="contentwrapper">
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnAdd">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnUpdateRecord">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="PanelKeyDetail" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
            <asp:Panel runat="server" ID="PanelKeyList">
                <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                    EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                    OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="20" Skin="Bootstrap">
                    <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true"></Selecting>
                    </ClientSettings>
                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="admin_id">
                        <Columns>

                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="admin_id" FilterControlAltText="Filter admin_id"
                                HeaderText="ID" SortExpression="admin_id" UniqueName="admin_id">
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="admin_username" FilterControlAltText="Filter admin_username"
                                HeaderText="User Name" SortExpression="admin_username" UniqueName="admin_username">
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="admin_password" FilterControlAltText="Filter admin_password"
                                HeaderText="Password" SortExpression="admin_password" UniqueName="admin_password">
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="role_name" FilterControlAltText="Filter role_name"
                                HeaderText="Role" SortExpression="role_name" UniqueName="role_name">
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="admin_isdeleted" FilterControlAltText="Filter admin_isdeleted"
                                HeaderText="IsDeleted" SortExpression="admin_isdeleted" UniqueName="admin_isdeleted">
                            </telerik:GridBoundColumn>

                            <telerik:GridButtonColumn ConfirmTextFormatString="Do you want to Delete User of {0}?" ConfirmTextFields="admin_id" CommandArgument="admin_id"
                                ConfirmDialogType="RadWindow" CommandName="Delete" ButtonType="LinkButton" UniqueName="Delete" Text="Delete"
                                HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                            </telerik:GridButtonColumn>

                            <telerik:GridButtonColumn ConfirmTextFormatString="Do you want to Un-Delete User of {0}?" ConfirmTextFields="admin_id" CommandArgument="admin_id"
                                ConfirmDialogType="RadWindow" CommandName="UnDelete" ButtonType="LinkButton" UniqueName="UnDelete" Text="Un_Delete"
                                HeaderText="Un-Delete" ItemStyle-HorizontalAlign="Center">
                            </telerik:GridButtonColumn>

                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </asp:Panel>

            <br />
            <br />
            <div class="mybuttoncss">
                <asp:Button runat="server" Text ="Add New" ID="btnAddNew" OnClick="btnAddNew_Click" />
            </div>
            <br />
            <br />
            <asp:Panel runat="server" ID="PanelKeyDetail">
                <div class='page-header'>
                    <h2>Detail</h2>
                </div>
                <table id="tblShow" class="stdtable">
                      <tr class="EditFormHeader">
                        <td style="width: 170px;">
                            User Name
                        </td>
                        <td>
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtUserName" runat="server"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfvUserName" runat="server" Display="Dynamic"
                                ControlToValidate="txtUserName" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="EditFormHeader">
                        <td style="width: 170px;">
                             Password
                        </td>
                        <td>
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtPassword" runat="server" TextMode="Password"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" Display="Dynamic"
                                ControlToValidate="txtPassword" ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <br />
                            <asp:Label ID="lblOldPassword" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr class="EditFormHeader">
                        <td style="width: 170px;">
                             Confirm Password
                        </td>
                        <td>
                            <telerik:RadTextBox RenderMode="Lightweight" ID="txtConfirmPassword" runat="server" TextMode="Password"></telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                                ErrorMessage="* Required" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword"
                                ErrorMessage="Not match." Display="Dynamic" ForeColor="Red"></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr class="EditFormHeader">
                        <td style="width: 170px;">
                             Role Name
                        </td>
                        <td>
                            <telerik:RadComboBox RenderMode="Lightweight" ID="ddlRole" runat="server" Height="200" Width="305" 
                                EmptyMessage="Choose" MarkFirstMatch="true" EnableLoadOnDemand="true"
                                Skin="Office2010Silver">
                                <Items>
                                    <telerik:RadComboBoxItem Value="Select" Text="Select" />
                                </Items>
                            </telerik:RadComboBox>
                            <asp:RequiredFieldValidator InitialValue="Select" runat="server" ID="rfvRole" ControlToValidate="ddlRole"
                            Display="Dynamic" ErrorMessage="* Required" CssClass="validator" ForeColor="Red"/>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                <%--<telerik:RadButton ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click"></telerik:RadButton>--%>
                <asp:Button ID="btnUpdateRecord" runat="server" Text="Update Record" OnClick="btnUpdateRecord_Click" UseSubmitBehavior="true"></asp:Button>
                <asp:Label runat="server" ID="lbls" ForeColor="Red"></asp:Label>
                <asp:HiddenField ID="hfID" runat="server" Value="" />
            </asp:Panel>
        </telerik:RadAjaxPanel>
    </div>
</div>
</asp:Content>

