﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using System.Text;
using Corpit.Site.Utilities;
using Corpit.Payment;
using System.IO;
using ClosedXML.Excel;
using Corpit.Site.Email;
using Corpit.Logging;
using System.Text.RegularExpressions;

public partial class Admin_MasterRegistrationList_OSEA : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    QuesFunctionality qfn = new QuesFunctionality();
    CommonFuns cFun = new CommonFuns();

    protected int count = 0;
    protected string htmltb;
    static StringBuilder StrBuilder = new StringBuilder();
    string showID = string.Empty;
    string flowID = string.Empty;
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string other_value = "Others";
    static string prostudent = "Student";
    static string proalliedhealth = "Allied Health";
    static string noInvoice = "-10";
    private static string[] checkingmdaFlowName = new string[] { "MFA2018iReg", "MMA2018iReg", "OSHA2018iReg" };//***MDA
    private static string[] checkingBadgeShowName = new string[] { "VNU_AGRI", "VNU_HORTI" };
    private static string[] checkingReceiptInVisibleShowName = new string[] { "VNU_AGRI", "VNU_HORTI" };
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (Session["userid"] != null)
                {
                    string userID = Session["userid"].ToString();

                    if (!string.IsNullOrEmpty(userID))
                    {
                        binddata();

                        if (Session["roleid"].ToString() != "1")
                        {
                            lblUser.Text = userID;
                            getShowID(userID);
                            showlist.Visible = false;


                            GKeyMaster.ExportSettings.ExportOnlyData = true;
                            GKeyMaster.ExportSettings.IgnorePaging = true;
                            GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                            GKeyMaster.ExportSettings.FileName = "IndividualReg_" + DateTime.Now.ToString("ddMMyyyy");


                            string constr = fn.ConnString;
                            using (SqlConnection con = new SqlConnection(constr))
                            {
                                using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID = '" + showID + "' and Status='Active'"))
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.Connection = con;
                                    con.Open();
                                    ddl_flowList.Items.Clear();
                                    ddl_flowList.DataSource = cmd.ExecuteReader();
                                    ddl_flowList.DataTextField = "FLW_Desc";
                                    ddl_flowList.DataValueField = "FLW_ID";
                                    ddl_flowList.DataBind();
                                    con.Close();
                                }
                            }
                        }
                        else
                            showID = ddl_showList.SelectedValue;
                    }
                    else
                    {
                        Response.Redirect("Event_Config");
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }

    protected void binddata()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select status_name,status_usedid From ref_Status"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_paymentStatus.Items.Clear();
                ddl_paymentStatus.DataSource = cmd.ExecuteReader();
                ddl_paymentStatus.DataTextField = "status_name";
                ddl_paymentStatus.DataValueField = "status_usedid";
                ddl_paymentStatus.DataBind();
                con.Close();

                ddl_paymentStatus.Items.Insert(0, new ListItem("All", "-1"));
            }

            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show where Status='Active' And SHW_Name Like 'OSEA 2018'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_showList.Items.Clear();
                ddl_showList.DataSource = cmd.ExecuteReader();
                ddl_showList.DataTextField = "SHW_Name";
                ddl_showList.DataValueField = "SHW_ID";
                ddl_showList.DataBind();
                con.Close();
            }

            using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID = '" + ddl_showList.SelectedValue + "' and Status='Active'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_flowList.Items.Clear();
                ddl_flowList.DataSource = cmd.ExecuteReader();
                ddl_flowList.DataTextField = "FLW_Desc";
                ddl_flowList.DataValueField = "FLW_ID";
                ddl_flowList.DataBind();
                con.Close();
            }
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_RegDelegate and tb_Invoice tables for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID))
        {
            try
            {
                int status = Convert.ToInt16(ddl_paymentStatus.SelectedValue);

                #region Old
                //MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
                //msregIndiv.ShowID = showID;
                //msregIndiv.FlowID = flowID;
                //if (status == -1)
                //{
                //    DataTable dtIndiv= msregIndiv.getDataAllRegList();
                //    DataView view = dtIndiv.DefaultView;
                //    view.Sort = "reg_datecreated DESC";
                //    DataTable sortedDate = view.ToTable();
                //    GKeyMaster.DataSource = sortedDate;
                //}
                //else if (status == -20)//***MDA
                //{
                //    DataTable dtRegList = msregIndiv.getDataAllRegList();
                //    DataTable dtPreRegList = getPrepopulateDataByShowFlow(showID, flowID);

                //    DataTable TableC = dtRegList.AsEnumerable()
                //            .Where(ra => dtPreRegList.AsEnumerable()
                //                                .Any(rb => rb.Field<int>("RefNo") == ra.Field<int>("Regno")))
                //            .CopyToDataTable();
                //    GKeyMaster.DataSource = TableC;
                //}//***MDA
                //else
                //    GKeyMaster.DataSource = msregIndiv.getDataByPaymentStatus(status);
                #endregion
                bool isBind = false;
                GKeyMaster.DataSource = getDataSource(ref isBind);
                if (isBind)
                {
                    GKeyMaster.DataBind();
                    GKeyMaster.AllowPaging = false;
                }

                FlowControler Flw = new FlowControler(fn);
                bool confExist = Flw.checkPageExist(flowID, SiteDefaultValue.constantConfName);
                if (!confExist)
                {
                    this.GKeyMaster.MasterTableView.GetColumn("CongressSelection").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("PaymentMethod").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("Invoice_grandtotal").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("PaidPrice").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("Outstanding").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("Invoice_discount").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("InvoiceStatus").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("UpdatePayment").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("DownloadInvoice").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("DownloadReceipt").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmail").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("DownloadBadge").Display = false;

                    this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmailOSEA").Display = false;//***only for OSEA
                    this.GKeyMaster.MasterTableView.GetColumn("UpdateStatus").Display = false;//***only for OSEA
                }
                else
                {
                    this.GKeyMaster.MasterTableView.GetColumn("CongressSelection").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("PaymentMethod").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("Invoice_grandtotal").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("PaidPrice").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("Outstanding").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("Invoice_discount").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("InvoiceStatus").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("UpdatePayment").Display = checkPaymentMode(showID);//true;
                    this.GKeyMaster.MasterTableView.GetColumn("DownloadInvoice").Display = checkPaymentMode(showID);//true;
                    this.GKeyMaster.MasterTableView.GetColumn("DownloadReceipt").Display = checkReceiptInVisible(showID);//true;
                    this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmail").Display = true;
                    this.GKeyMaster.MasterTableView.GetColumn("DownloadBadge").Display = checkBadge(showID);//true;

                    this.GKeyMaster.MasterTableView.GetColumn("Download").Display = false;

                    this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmailOSEA").Display = false;//***only for OSEA
                    this.GKeyMaster.MasterTableView.GetColumn("UpdateStatus").Display = false;//***only for OSEA
                }

                this.GKeyMaster.MasterTableView.GetColumn("Download").Display = false;//***only for OSEA
                this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmailOSEA").Display = true;// false;//***only for OSEA
                this.GKeyMaster.MasterTableView.GetColumn("UpdateStatus").Display = true;//***only for OSEA
            }
            catch (Exception ex)
            {
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region checkPaymentMode
    private bool checkPaymentMode(string showID)
    {
        bool isUsedChequeTT = false;
        try
        {
            CommonDataObj cmdObj = new CommonDataObj(fn);
            DataTable dt = cmdObj.getPaymentMethods(showID);
            if (dt.Rows.Count > 0)
            {
                SiteSettings st = new SiteSettings(fn, showID);
                foreach (DataRow dr in dt.Rows)
                {
                    string usedid = dr["method_usedid"].ToString();
                    if (usedid == ((int)PaymentType.TT).ToString())
                    {
                        isUsedChequeTT = true;
                    }
                    if (usedid == ((int)PaymentType.Cheque).ToString())
                    {
                        isUsedChequeTT = true;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return isUsedChequeTT;
    }
    #endregion

    #region checkBadge
    private bool checkBadge(string showid)
    {
        bool isBadgeExist = false;
        try
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            if (checkingBadgeShowName.Contains(shw.SHW_Name))
            {
                isBadgeExist = true;
            }
        }
        catch (Exception ex)
        { }

        return isBadgeExist;
    }
    #endregion

    #region checkReceiptInVisible
    private bool checkReceiptInVisible(string showid)
    {
        bool isBadgeExist = true;
        try
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            if (checkingReceiptInVisibleShowName.Contains(shw.SHW_Name))
            {
                isBadgeExist = false;
            }
        }
        catch (Exception ex)
        { }

        return isBadgeExist;
    }
    #endregion

    #region Page_PreRender
    protected void Page_PreRender(object o, EventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
            {
                getShowID(Session["userid"].ToString());
            }
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
        if (!string.IsNullOrEmpty(showID))
        {
            #region Delegate
            DataTable dtfrm = new DataTable();
            FormManageObj frmObj = new FormManageObj(fn);
            frmObj.showID = showID;
            frmObj.flowID = flowID;
            dtfrm = frmObj.getDynFormForDelegate().Tables[0];

            #region Declaration
            int vis_Salutation = 0;
            int vis_Fname = 0;
            int vis_Lname = 0;
            int vis_OName = 0;
            int vis_PassNo = 0;
            int vis_isReg = 0;
            int vis_regSpecific = 0;
            int vis_IDNo = 0;
            int vis_Designation = 0;
            int vis_Profession = 0;
            int vis_Department = 0;
            int vis_Organization = 0;
            int vis_Institution = 0;
            int vis_Address1 = 0;
            int vis_Address2 = 0;
            int vis_Address3 = 0;
            int vis_Address4 = 0;
            int vis_City = 0;
            int vis_State = 0;
            int vis_PostalCode = 0;
            int vis_Country = 0;
            int vis_RCountry = 0;
            int vis_Tel = 0;
            int vis_Mobile = 0;
            int vis_Fax = 0;
            int vis_Email = 0;
            int vis_Affiliation = 0;
            int vis_Dietary = 0;
            int vis_Nationality = 0;
            int vis_MembershipNo = 0;

            int vis_VName = 0;
            int vis_VDOB = 0;
            int vis_VPassNo = 0;
            int vis_VPassExpiry = 0;
            int vis_VPassIssueDate = 0;
            int vis_VEmbarkation = 0;
            int vis_VArrivalDate = 0;
            int vis_VCountry = 0;

            int vis_UDF_CName = 0;
            int vis_UDF_DelegateType = 0;
            int vis_UDF_ProfCategory = 0;
            int vis_UDF_CPcode = 0;
            int vis_UDF_CLDepartment = 0;
            int vis_UDF_CAddress = 0;
            int vis_UDF_CLCompany = 0;
            int vis_UDF_CCountry = 0;

            int vis_SupName = 0;
            int vis_SupDesignation = 0;
            int vis_SupContact = 0;
            int vis_SupEmail = 0;

            int vis_Age = 0;
            int vis_Gender = 0;
            int vis_DOB = 0;
            int vis_Additional4 = 0;
            int vis_Additional5 = 0;
            #endregion

            if (dtfrm.Rows.Count > 0)
            {
                for (int x = 0; x < dtfrm.Rows.Count; x++)
                {
                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Salutation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Salutation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Salutation);

                                GKeyMaster.MasterTableView.GetColumn("reg_Salutation").HeaderText = labelname;

                                vis_Salutation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Salutation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Fname == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_FName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fname);
                                GKeyMaster.MasterTableView.GetColumn("reg_FName").HeaderText = labelname;

                                vis_Fname++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_FName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Lname == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_LName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Lname);
                                GKeyMaster.MasterTableView.GetColumn("reg_LName").HeaderText = labelname;

                                vis_Lname++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_LName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _OName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_OName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_OName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_OName);
                                GKeyMaster.MasterTableView.GetColumn("reg_OName").HeaderText = "Address 2";// labelname;
                                vis_OName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_OName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_PassNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_PassNo").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PassNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_PassNo").HeaderText = labelname;

                                vis_PassNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_PassNo").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _isReg)//Is registered MOH/Resident?
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_isReg == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_isReg").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_isReg);
                                GKeyMaster.MasterTableView.GetColumn("reg_isReg").HeaderText = labelname;

                                vis_isReg++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_isReg").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)//MCR/SNB/PRN
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_regSpecific == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_sgregistered").Display = true;

                                vis_regSpecific++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_sgregistered").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _IDNo)//MCR/SNB/PRN No.
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_IDNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_IDno").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_IDNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_IDno").HeaderText = labelname;

                                vis_IDNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_IDno").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Designation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Designation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                                GKeyMaster.MasterTableView.GetColumn("reg_Designation").HeaderText = labelname;

                                vis_Designation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Designation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Profession == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Profession").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                                GKeyMaster.MasterTableView.GetColumn("reg_Profession").HeaderText = labelname;

                                vis_Profession++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Profession").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Department)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Department == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Department").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Department);
                                GKeyMaster.MasterTableView.GetColumn("reg_Department").HeaderText = labelname;

                                vis_Department++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Department").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Organization == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Organization").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Organization);
                                GKeyMaster.MasterTableView.GetColumn("reg_Organization").HeaderText = labelname;

                                vis_Organization++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Organization").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Institution == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Institution").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Institution);
                                GKeyMaster.MasterTableView.GetColumn("reg_Institution").HeaderText = labelname;

                                vis_Institution++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Institution").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address1 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address1").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address1);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address1").HeaderText = labelname;
                                vis_Address1++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address1").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address2 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address2").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address2);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address2").HeaderText = labelname;
                                vis_Address2++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address2").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address3 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address3").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address3);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address3").HeaderText = labelname;

                                vis_Address3++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address3").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address4 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address4").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address4);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address4").HeaderText = labelname;

                                vis_Address4++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address4").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _City)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_City == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_City").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_City);
                                GKeyMaster.MasterTableView.GetColumn("reg_City").HeaderText = labelname;

                                vis_City++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_City").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _State)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_State == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_State").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_State);
                                GKeyMaster.MasterTableView.GetColumn("reg_State").HeaderText = labelname;

                                vis_State++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_State").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_PostalCode == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_PostalCode").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PostalCode);
                                GKeyMaster.MasterTableView.GetColumn("reg_PostalCode").HeaderText = labelname;

                                vis_PostalCode++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_PostalCode").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Country)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Country == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Country").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Country);
                                GKeyMaster.MasterTableView.GetColumn("reg_Country").HeaderText = labelname;

                                vis_Country++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Country").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_RCountry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_RCountry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_RCountry);
                                GKeyMaster.MasterTableView.GetColumn("reg_RCountry").HeaderText = labelname;
                                vis_RCountry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_RCountry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Tel == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Tel").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Tel);
                                GKeyMaster.MasterTableView.GetColumn("reg_Tel").HeaderText = labelname;

                                vis_Tel++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Tel").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Mobile == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Mobile").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Mobile);
                                GKeyMaster.MasterTableView.GetColumn("reg_Mobile").HeaderText = labelname;

                                vis_Mobile++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Mobile").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Fax == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Fax").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fax);
                                GKeyMaster.MasterTableView.GetColumn("reg_Fax").HeaderText = labelname;
                                vis_Fax++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Fax").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Email == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Email").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Email);
                                GKeyMaster.MasterTableView.GetColumn("reg_Email").HeaderText = labelname;
                                vis_Email++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Email").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Affiliation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Affiliation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Affiliation);
                                GKeyMaster.MasterTableView.GetColumn("reg_Affiliation").HeaderText = labelname;

                                vis_Affiliation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Affiliation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Dietary == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Dietary").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Dietary);
                                GKeyMaster.MasterTableView.GetColumn("reg_Dietary").HeaderText = labelname;

                                vis_Dietary++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Dietary").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Nationality == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Nationality").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Nationality);
                                GKeyMaster.MasterTableView.GetColumn("reg_Nationality").HeaderText = labelname;

                                vis_Nationality++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Nationality").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Age)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Age == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Age").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Age);
                                GKeyMaster.MasterTableView.GetColumn("reg_Age").HeaderText = labelname;

                                vis_Age++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Age").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_DOB == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_DOB").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_DOB);
                                GKeyMaster.MasterTableView.GetColumn("reg_DOB").HeaderText = labelname;

                                vis_DOB++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_DOB").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Gender == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Gender").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Gender);
                                GKeyMaster.MasterTableView.GetColumn("reg_Gender").HeaderText = labelname;
                                vis_Gender++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Gender").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        //if (isshow == 1)
                        //{
                        //    if (vis_MembershipNo == 0)
                        //    {
                        //        GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").Display = true;
                        //        string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_MembershipNo);
                        //        GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").HeaderText = labelname;
                        //        vis_MembershipNo++;
                        //    }
                        //}
                        //else
                        //{
                        //    GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").Display = false;
                        //}
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VName);
                                GKeyMaster.MasterTableView.GetColumn("reg_vName").HeaderText = labelname;
                                vis_VName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VDOB == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vDOB").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VDOB);
                                GKeyMaster.MasterTableView.GetColumn("reg_vDOB").HeaderText = labelname;
                                vis_VDOB++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vDOB").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VPassNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassno").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassno").HeaderText = labelname;
                                vis_VPassNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vPassno").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VPassExpiry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassexpiry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassExpiry);
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassexpiry").HeaderText = labelname;
                                vis_VPassExpiry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vPassexpiry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VPassIssueDate == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vIssueDate").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassIssueDate);
                                GKeyMaster.MasterTableView.GetColumn("reg_vIssueDate").HeaderText = labelname;
                                vis_VPassIssueDate++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vIssueDate").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VEmbarkation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vEmbarkation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VEmbarkation);
                                GKeyMaster.MasterTableView.GetColumn("reg_vEmbarkation").HeaderText = labelname;
                                vis_VEmbarkation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vEmbarkation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VArrivalDate == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vArrivalDate").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VArrivalDate);
                                GKeyMaster.MasterTableView.GetColumn("reg_vArrivalDate").HeaderText = labelname;
                                vis_VArrivalDate++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vArrivalDate").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VCountry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vCountry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VCountry);
                                GKeyMaster.MasterTableView.GetColumn("reg_vCountry").HeaderText = labelname;
                                vis_VCountry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vCountry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CName);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CName").HeaderText = labelname;
                                vis_UDF_CName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_DelegateType == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_DelegateType").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_DelegateType);
                                GKeyMaster.MasterTableView.GetColumn("UDF_DelegateType").HeaderText = labelname;
                                vis_UDF_DelegateType++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_DelegateType").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_ProfCategory == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_ProfCategory").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_ProfCategory);
                                GKeyMaster.MasterTableView.GetColumn("UDF_ProfCategory").HeaderText = labelname;
                                vis_UDF_ProfCategory++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_ProfCategory").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CPcode == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CPcode").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CPcode);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CPcode").HeaderText = labelname;
                                vis_UDF_CPcode++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CPcode").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CLDepartment == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLDepartment").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLDepartment);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLDepartment").HeaderText = labelname;
                                vis_UDF_CLDepartment++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CLDepartment").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CAddress == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CAddress").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CAddress);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CAddress").HeaderText = labelname;
                                vis_UDF_CAddress++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CAddress").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CLCompany == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLCompany").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLCompany);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLCompany").HeaderText = labelname;
                                vis_UDF_CLCompany++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CLCompany").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CCountry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CCountry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CCountry);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CCountry").HeaderText = labelname;
                                vis_UDF_CCountry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CCountry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupName);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorName").HeaderText = labelname;
                                vis_SupName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupDesignation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorDesignation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupDesignation);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorDesignation").HeaderText = labelname;
                                vis_SupDesignation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorDesignation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupContact == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorContact").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupContact);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorContact").HeaderText = labelname;
                                vis_SupContact++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorContact").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupEmail == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorEmail").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupEmail);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorEmail").HeaderText = labelname;
                                vis_SupEmail++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorEmail").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Additional4 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional4").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional4);
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional4").HeaderText = labelname;

                                vis_Additional4++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Additional4").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Additional5 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional5").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional5);
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional5").HeaderText = labelname;
                                vis_Additional5++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Additional5").Display = false;
                        }
                    }
                }
            }
            ddl_paymentStatus_SelectedIndexChanged(this, EventArgs.Empty);
            #endregion
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;
            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
        if (!string.IsNullOrEmpty(showID) && !string.IsNullOrEmpty(flowID))
        {
            string regno = "";

            foreach (GridDataItem item in GKeyMaster.SelectedItems)
            {
                regno = item["Regno"].Text;
            }

            if (e.CommandName == "Delete")
            {
                GridDataItem item = (GridDataItem)e.Item;
                string del_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
                if (!string.IsNullOrEmpty(del_Regno))
                {
                    int isDeleted = delRecord(del_Regno);
                    if (isDeleted > 0)
                    {
                        GKeyMaster.Rebind();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + del_Regno + " is deleted.');", true);
                        return;
                    }
                }
            }
            else if (e.CommandName == "Edit")
            {
                GridDataItem item = (GridDataItem)e.Item;
                string edit_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
                if (!string.IsNullOrEmpty(edit_Regno))
                {
                    Response.Redirect("RegIndiv_Edit.aspx?regno=" + cFun.EncryptValue(edit_Regno) + "&SHW=" + cFun.EncryptValue(showID) + "&FLW=" + cFun.EncryptValue(flowID), false);
                }
            }

            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                //GridBoundColumn boundColumn;

                ////Important: first Add column to the collection 
                //boundColumn = new GridBoundColumn();
                //this.GKeyMaster.MasterTableView.Columns.Add(boundColumn);

                ////Then set properties 
                //boundColumn.DataField = "CustomerID";
                //boundColumn.HeaderText = "CustomerID";
                GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                GKeyMaster.ExportSettings.FileName = "DelegateList";
                GKeyMaster.ExportSettings.IgnorePaging = false;
                GKeyMaster.ExportSettings.ExportOnlyData = true;
                GKeyMaster.ExportSettings.OpenInNewWindow = true;
                GKeyMaster.MasterTableView.ExportToExcel();
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    /// <summary> 
    /// Item databound for the radgrid, set the header text here based on the form management ***Item[UniqueName]
    /// </summary>
    protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;
            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
        if (!string.IsNullOrEmpty(showID))
        {
            if (e.Item is GridHeaderItem)
            {
                GridHeaderItem item = e.Item as GridHeaderItem;

                #region Delegate
                DataTable dtfrm = new DataTable();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showID;
                frmObj.flowID = flowID;
                dtfrm = frmObj.getDynFormForDelegate().Tables[0];

                if (dtfrm.Rows.Count > 0)
                {
                    for (int x = 0; x < dtfrm.Rows.Count; x++)
                    {
                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Salutation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Salutation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Salutation);
                                }
                            }
                            else
                            {
                                (item["reg_Salutation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_FName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_FName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fname);
                                }
                            }
                            else
                            {
                                (item["reg_FName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_LName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_LName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Lname);
                                }
                            }
                            else
                            {
                                (item["reg_LName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _OName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_OName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_OName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_OName);
                                }
                            }
                            else
                            {
                                (item["reg_OName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_PassNo"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_PassNo"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PassNo);
                                }
                            }
                            else
                            {
                                (item["reg_PassNo"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_isReg"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_isReg"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_isReg);
                                }
                            }
                            else
                            {
                                (item["reg_isReg"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_sgregistered"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_sgregistered"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_regSpecific);
                                }
                            }
                            else
                            {
                                (item["reg_sgregistered"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_IDno"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_IDno"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_IDNo);
                                }
                            }
                            else
                            {
                                (item["reg_IDno"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Designation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Designation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                                }
                            }
                            else
                            {
                                (item["reg_Designation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Profession"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Profession"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Profession);
                                }
                            }
                            else
                            {
                                (item["reg_Profession"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Department)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Department"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Department"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Department);
                                }
                            }
                            else
                            {
                                (item["reg_Department"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Organization"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Organization"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Organization);
                                }
                            }
                            else
                            {
                                (item["reg_Organization"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Institution"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Institution"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Institution);
                                }
                            }
                            else
                            {
                                (item["reg_Institution"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address1"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address1"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address1);
                                }
                            }
                            else
                            {
                                (item["reg_Address1"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address2"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address2"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address2);
                                }
                            }
                            else
                            {
                                (item["reg_Address2"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address3"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address3"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address3);
                                }
                            }
                            else
                            {
                                (item["reg_Address3"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address4"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address4"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address4);
                                }
                            }
                            else
                            {
                                (item["reg_Address4"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _City)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_City"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_City"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_City);
                                }
                            }
                            else
                            {
                                (item["reg_City"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _State)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_State"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_State"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_State);
                                }
                            }
                            else
                            {
                                (item["reg_State"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_PostalCode"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_PostalCode"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PostalCode);
                                }
                            }
                            else
                            {
                                (item["reg_PostalCode"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Country)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Country"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Country"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Country);
                                }
                            }
                            else
                            {
                                (item["reg_Country"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_RCountry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_RCountry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_RCountry);
                                }
                            }
                            else
                            {
                                (item["reg_RCountry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Telcc"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Telcc"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Telcc);
                                }
                            }
                            else
                            {
                                (item["reg_Telcc"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Telac)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Telac"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Telac"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Telac);
                                }
                            }
                            else
                            {
                                (item["reg_Telac"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Tel"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Tel"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Tel);
                                }
                            }
                            else
                            {
                                (item["reg_Tel"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Mobcc"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Mobcc"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Mobilecc);
                                }
                            }
                            else
                            {
                                (item["reg_Mobcc"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Mobac"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Mobac"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Mobileac);
                                }
                            }
                            else
                            {
                                (item["reg_Mobac"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Mobile"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Mobile"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Mobile);
                                }
                            }
                            else
                            {
                                (item["reg_Mobile"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Faxcc"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Faxcc"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Faxcc);
                                }
                            }
                            else
                            {
                                (item["reg_Faxcc"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Faxac"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Faxac"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Faxac);
                                }
                            }
                            else
                            {
                                (item["reg_Faxac"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Fax"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Fax"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fax);
                                }
                            }
                            else
                            {
                                (item["reg_Fax"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Email"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Email"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Email);
                                }
                            }
                            else
                            {
                                (item["reg_Email"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Affiliation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Affiliation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Affiliation);
                                }
                            }
                            else
                            {
                                (item["reg_Affiliation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Dietary"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Dietary"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Dietary);
                                }
                            }
                            else
                            {
                                (item["reg_Dietary"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Nationality"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Nationality"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Nationality);
                                }
                            }
                            else
                            {
                                (item["reg_Nationality"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Age)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Age"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Age"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Age);
                                }
                            }
                            else
                            {
                                (item["reg_Age"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_DOB"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_DOB"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_DOB);
                                }
                            }
                            else
                            {
                                (item["reg_DOB"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Gender"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Gender"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Gender);
                                }
                            }
                            else
                            {
                                (item["reg_Gender"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            //if (isshow == 1)
                            //{
                            //    if (string.IsNullOrEmpty((item["reg_Membershipno"].Controls[0] as LinkButton).Text))
                            //    {
                            //        (item["reg_Membershipno"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_MembershipNo);
                            //    }
                            //}
                            //else
                            //{
                            //    (item["reg_Membershipno"].Controls[0] as LinkButton).Text = "";
                            //}
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VName);
                                }
                            }
                            else
                            {
                                (item["reg_vName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vDOB"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vDOB"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VDOB);
                                }
                            }
                            else
                            {
                                (item["reg_vDOB"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vPassno"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vPassno"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassNo);
                                }
                            }
                            else
                            {
                                (item["reg_vPassno"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vPassexpiry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vPassexpiry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassExpiry);
                                }
                            }
                            else
                            {
                                (item["reg_vPassexpiry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vIssueDate"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vIssueDate"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassIssueDate);
                                }
                            }
                            else
                            {
                                (item["reg_vIssueDate"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vEmbarkation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vEmbarkation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VEmbarkation);
                                }
                            }
                            else
                            {
                                (item["reg_vEmbarkation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vArrivalDate"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vArrivalDate"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VArrivalDate);
                                }
                            }
                            else
                            {
                                (item["reg_vArrivalDate"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vCountry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vCountry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VCountry);
                                }
                            }
                            else
                            {
                                (item["reg_vCountry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CName);
                                }
                            }
                            else
                            {
                                (item["UDF_CName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_DelegateType"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_DelegateType"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_DelegateType);
                                }
                            }
                            else
                            {
                                (item["UDF_DelegateType"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_ProfCategory"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_ProfCategory"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_ProfCategory);
                                }
                            }
                            else
                            {
                                (item["UDF_ProfCategory"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CPcode"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CPcode"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CPcode);
                                }
                            }
                            else
                            {
                                (item["UDF_CPcode"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CLDepartment"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CLDepartment"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLDepartment);
                                }
                            }
                            else
                            {
                                (item["UDF_CLDepartment"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CAddress"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CAddress"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CAddress);
                                }
                            }
                            else
                            {
                                (item["UDF_CAddress"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CLCompany"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CLCompany"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLCompany);
                                }
                            }
                            else
                            {
                                (item["UDF_CLCompany"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CCountry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CCountry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CCountry);
                                }
                            }
                            else
                            {
                                (item["UDF_CCountry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupName);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorDesignation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorDesignation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupDesignation);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorDesignation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorContact"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorContact"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupContact);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorContact"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorEmail"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorEmail"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupEmail);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorEmail"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Additional4"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Additional4"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional4);
                                }
                            }
                            else
                            {
                                (item["reg_Additional4"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Additional5"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Additional5"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional5);
                                }
                            }
                            else
                            {
                                (item["reg_Additional5"].Controls[0] as LinkButton).Text = "";
                            }
                        }
                    }
                }
                #endregion
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    protected void GKeyMaster_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        Session["pgno"] = GKeyMaster.CurrentPageIndex + 1;
        if (string.IsNullOrEmpty(txtFromDate.Text.Trim()) || string.IsNullOrEmpty(txtToDate.Text.Trim()))
        {
            MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
            msregIndiv.ShowID = showID;
            msregIndiv.FlowID = flowID;
            GKeyMaster.DataSource = msregIndiv.getDataAllRegList();
        }
        else
        {
            GKeyMaster.DataSource = getDataAllRegList(txtFromDate.Text.Trim(), txtToDate.Text.Trim());
        }
    }

    #region delRecord
    private int delRecord(string regno)
    {
        int isDeleted = 0;
        if (!string.IsNullOrEmpty(showID))
        {
            try
            {
                //isDeleted = fn.ExecuteSQL(string.Format("Update tb_RegGroup Set recycle=1 Where RegGroupID={0} And ShowID='{1}'", groupid, showid));
                isDeleted += fn.ExecuteSQL(string.Format("Update tb_RegDelegate Set recycle=1 Where Regno={0} And ShowID='{1}'", regno, showID));
            }
            catch (Exception ex)
            {

            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

        return isDeleted;
    }
    #endregion

    protected void ddl_paymentStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        //txtFromDate.Text = "";
        //txtToDate.Text = "";
        GKeyMaster.Rebind();
    }

    protected void ddl_showList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;

        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID = '" + showID + "' and Status='Active'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_flowList.Items.Clear();
                ddl_flowList.DataSource = cmd.ExecuteReader();
                ddl_flowList.DataTextField = "FLW_Desc";
                ddl_flowList.DataValueField = "FLW_ID";
                ddl_flowList.DataBind();
                con.Close();
            }
        }
        flowID = ddl_flowList.SelectedValue;

        FlowControler Flw = new FlowControler(fn);//***MDA
        FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowID);
        if (checkingmdaFlowName.Contains(flwMasterConfig.FlowName))
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") == null)
            {
                ddl_paymentStatus.Items.Add("Pre-populate");
                ddl_paymentStatus.Items[ddl_paymentStatus.Items.Count - 1].Value = "-20";
            }
            else
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = true;
            }
        }
        else
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") != null)
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = false;

                ddl_paymentStatus.SelectedIndex = 0;
            }
        }//***MDA

        GKeyMaster.Rebind();
    }

    protected void ddl_flowList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;
        flowID = ddl_flowList.SelectedValue;
        txtFromDate.Text = "";
        txtToDate.Text = "";
        FlowControler Flw = new FlowControler(fn);//***MDA
        FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowID);
        if (checkingmdaFlowName.Contains(flwMasterConfig.FlowName))
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") == null)
            {
                ddl_paymentStatus.Items.Add("Pre-populate");
                ddl_paymentStatus.Items[ddl_paymentStatus.Items.Count - 1].Value = "-20";
            }
            else
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = true;
            }
        }
        else
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") != null)
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = false;

                ddl_paymentStatus.SelectedIndex = 0;
            }
        }//***MDA

        GKeyMaster.Rebind();
    }
    protected void RadGd1_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem)e.Item;
            string edit_RegGroupID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
            string edit_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
            if (!string.IsNullOrEmpty(edit_Regno))
            {

                ImageButton downloadlink = (ImageButton)item["Download"].Controls[0];
                downloadlink.Attributes.Add
               ("onclick", "window.open('../AcknowledgeLetter.aspx?DID=" + cFun.EncryptValue(edit_Regno) +
               "&SHW=" + cFun.EncryptValue(showID) + "'); return false");


            }


        }
    }

    public DataTable GetDetailReport(string qnaireID)
    {
        string flwid = ddl_flowList.SelectedValue;
        FlowControler flwControl = new FlowControler(fn);
        FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flwid);
        string logid = string.Empty;
        string item = string.Empty;
        StringBuilder str = new StringBuilder();
        string sqlQ = "select * from gen_Question where status='Active' and quest_class <> 'Header' and quest_id in (select top 500 quest_id from gen_QnaireQuest where qnaire_id='" + qnaireID + "' order by qnaire_seq)";
        DataTable dtQ = qfn.GetDatasetByCommand(sqlQ, "sdtQ").Tables[0];
        //Regno	Salutation	First Name	Last Name	Company	Job Title	Email	Mobile	Address	Country	Type	Category

        //str.Append("<tr>");
        DataTable NewDt = new DataTable();
        if (checkingmdaFlowName.Contains(flwMasterConfig.FlowName))//***MDA
        {
            #region MDA
            str.Append("<tr>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "No" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Registration ID" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Created Date" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Update Date" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Registration Status" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Salutation" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "FirstName" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Last Name" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Job Title" + "</td>");
            //str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Department" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Name" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Address Line1" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Address Line2" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Address Line3" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Postcode" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "City" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "State/Province" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Country" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Tel No Code" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Tel No" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Mobile Code" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Mobile No" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Fax Code" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Faxno" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Email" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Email Alternate" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Website" + "</td>");
            #endregion
        }//***MDA
        else
        {
            #region Other Show
            if (GKeyMaster.Items.Count > 0)
            {
                GKeyMaster.MasterTableView.GetColumn("Download").Display = false;
                GKeyMaster.MasterTableView.GetColumn("Delete").Display = false;
                GKeyMaster.MasterTableView.GetColumn("Edit").Display = false;
                GKeyMaster.MasterTableView.GetColumn("UpdatePayment").Display = false;
                GKeyMaster.MasterTableView.GetColumn("DownloadInvoice").Display = false;
                GKeyMaster.MasterTableView.GetColumn("DownloadReceipt").Display = false;
                GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmail").Display = false;
                GKeyMaster.MasterTableView.GetColumn("DownloadBadge").Display = false;
                GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmailOSEA").Display = false;//***only for OSEA
                GKeyMaster.MasterTableView.GetColumn("UpdateStatus").Display = false;//***only for OSEA
                GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").Visible = false;
                GKeyMaster.MasterTableView.GetColumn("UserRegno").Visible = true;
                GKeyMaster.ExportSettings.ExportOnlyData = true;

                if (string.IsNullOrEmpty(txtKey.Text) && string.IsNullOrWhiteSpace(txtKey.Text)
                    && string.IsNullOrEmpty(txtFromDate.Text) && string.IsNullOrWhiteSpace(txtFromDate.Text)
                    && string.IsNullOrEmpty(txtToDate.Text) && string.IsNullOrWhiteSpace(txtToDate.Text)
                    && ddlApprove.SelectedIndex == 0)
                {
                }
                GKeyMaster.AllowPaging = false;
                GKeyMaster.Rebind();

                NewDt.Columns.Add("No", typeof(string));
                //str.Append("<td style='background-color:#b3b3b3'>" + "No" + "</td>");//rowspan='2' 
                //foreach (GridDataItem row in GKeyMaster.Items) // loops through each rows in RadGrid
                {
                    foreach (GridColumn col in GKeyMaster.Columns) //loops through each column in RadGrid
                    {
                        if (col.Visible == true && col.Display == true)
                        {
                            //str.Append("<td style='background-color:#b3b3b3'>" + col.HeaderText + "</td>");//rowspan='2' 
                            NewDt.Columns.Add(col.HeaderText, typeof(string));
                        }
                    }
                }
            }
            #endregion
        }

        #region QA header
        #region with main question header
        ////***bind question udpated by th on 16-7-2018
        if (dtQ.Rows.Count > 0)
        {
            for (int q = 0; q < dtQ.Rows.Count; q++)
            {
                int span = GetQuestCount(dtQ.Rows[q]["quest_id"].ToString());
                //str.Append("<td style='background-color:#b3b3b3'>" + dtQ.Rows[q]["quest_desc"].ToString() + "</td>");
                NewDt.Columns.Add(dtQ.Rows[q]["quest_desc"].ToString(), typeof(string));

                string sqlQItem = "Select * From gen_QuestItem Where qitem_Type='Text' And quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "' And quest_id In"
                                + " (select top 500 quest_id from gen_QnaireQuest where qnaire_id='" + qnaireID + "' order by qnaire_seq)";
                DataTable txtBoxDT = qfn.GetDatasetByCommand(sqlQItem, "ds").Tables[0];
                if(txtBoxDT.Rows.Count > 0)
                {
                    //str.Append("<td style='background-color:#b3b3b3'>" + dtQ.Rows[q]["quest_desc"].ToString() + "_OST" + "</td>");
                    NewDt.Columns.Add(dtQ.Rows[q]["quest_desc"].ToString() + "_OST", typeof(string));
                }
            }
        }
        //str.Append("</tr>");
        ////***bind question udpated by th on 16-7-2018
        #endregion
        #region with all answers header(Comment)
        ////***comment question item(answer) by th on 16-7-2018 for OSEA because no need to show individual answer
        //str.Append("<tr>");
        //if (dtQ.Rows.Count > 0)
        //{
        //    for (int q = 0; q < dtQ.Rows.Count; q++)
        //    {
        //        string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
        //        DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
        //        if (dtI.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dtI.Rows.Count; i++)
        //            {
        //                str.Append("<td style='background-color:#b3b3b3'>" + dtI.Rows[i]["qitem_desc"].ToString() + "</td>");
        //            }
        //        }
        //    }
        //}
        //str.Append("</tr>");
        ////***comment question item(answer) by th on 16-7-2018 for OSEA because no need to show individual answer
        #endregion

        string sqlU = "select * from tbl" + qnaireID + " order by user_reference";
        DataTable dtU = qfn.GetDatasetByCommand(sqlU, "sdtU").Tables[0];
        #endregion

        if (checkingmdaFlowName.Contains(flwMasterConfig.FlowName))//***MDA
        {
            #region MDA
            DataTable dtDelegateAll = new DataTable();
            if (ddl_paymentStatus.SelectedItem.Value == "-20")//***MDA
            {
                string sqlDelegateAll = "select * from  tb_RegDelegate where reg_urlFlowID='" + ddl_flowList.SelectedValue + "' and recycle='0' " + checkDateForExcel()
                    + " And Regno In (Select RefNo From tb_RegPrePopulate Where (recycle=0 Or recycle Is Null) And ShowID='" + showID
                    + "' And reg_urlFlowID='" + ddl_flowList.SelectedValue + "'"
                    + " And (RefNo Is Not Null And RefNo<>'')) order by Regno";// full outer join ref_Salutation on reg_Salutation=Sal_ID full outer join ref_country on reg_Country=Cty_GUID and reg_Status='1' 
                dtDelegateAll = fn.GetDatasetByCommand(sqlDelegateAll, "sdtDelegateAll").Tables[0];
            }//***MDA
            else
            {
                string sqlDelegateAll = "select * from  tb_RegDelegate where reg_urlFlowID='" + ddl_flowList.SelectedValue + "' and recycle='0' " + checkDateForExcel() + " order by Regno";// full outer join ref_Salutation on reg_Salutation=Sal_ID full outer join ref_country on reg_Country=Cty_GUID and reg_Status='1' 
                dtDelegateAll = fn.GetDatasetByCommand(sqlDelegateAll, "sdtDelegateAll").Tables[0];
            }

            string sqlA1 = "select * from gen_QuestItem  where  status='Active' and quest_id in (select quest_id from gen_QnaireQuest where qnaire_id='" + qnaireID + "' and status='Active' and quest_id in(select quest_id from gen_Question where quest_class<>'Header'))";
            DataTable dtA1 = qfn.GetDatasetByCommand(sqlA1, "sdtA1").Tables[0];
            if (dtDelegateAll.Rows.Count > 0)
            {
                for (int da = 0; da < dtDelegateAll.Rows.Count; da++)
                {
                    if (dtU.Rows.Count > 0)
                    {
                        string daregno = dtDelegateAll.Rows[da]["Regno"].ToString();
                        string sqlDelegate = "select * from  tb_RegDelegate as d full outer join tb_RegCompany as c on d.RegGroupID=c.RegGroupID where Regno in ('" + daregno + "') ";//full outer join ref_Salutation on d.reg_Salutation=Sal_ID full outer join ref_country on d.reg_Country=Cty_GUID 
                        DataTable dtDelegate = fn.GetDatasetByCommand(sqlDelegate, "sdtDelegate").Tables[0];
                        if (dtDelegate.Rows.Count > 0)
                        {
                            for (int d = 0; d < dtDelegate.Rows.Count; d++)
                            {
                                str.Append("<tr>");
                                str.Append("<td>" + (count + 1).ToString() + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["Regno"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_datecreated"] + "</td>");
                                str.Append("<td> </td>");
                                str.Append("<td>" + getInvoiceStatus(dtDelegate.Rows[d]["reg_Status"] != DBNull.Value ? dtDelegate.Rows[d]["reg_Status"].ToString() : "0") + "</td>");
                                string salutationother = dtDelegate.Rows[d]["reg_Salutation"] != DBNull.Value ? dtDelegate.Rows[d]["reg_SalutationOthers"].ToString() : "";
                                str.Append("<td>" + bindSalutation((dtDelegate.Rows[d]["reg_Salutation"] != DBNull.Value ? dtDelegate.Rows[d]["reg_Salutation"].ToString() : ""), salutationother) + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_FName"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_LName"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Designation"] + "</td>");
                                //str.Append("<td>" + bindDepartment((dtDelegate.Rows[d]["reg_Department"] != DBNull.Value ? dtDelegate.Rows[d]["reg_Department"].ToString() : "") , (dtDelegate.Rows[d]["reg_otherDepartment"] != DBNull.Value ? dtDelegate.Rows[d]["reg_otherDepartment"].ToString() : "")) + "</td>");

                                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                                {
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Name"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Address1"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Address2"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Address3"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_ZipCode"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_City"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_State"] + "</td>");
                                    string indivCountry = dtDelegate.Rows[d]["reg_Country"] != null ? dtDelegate.Rows[d]["reg_Country"].ToString() : "";
                                    string comCountry = dtDelegate.Rows[d]["RC_Country"] != null ? dtDelegate.Rows[d]["RC_Country"].ToString() : "";
                                    string countryName = bindCountry(!string.IsNullOrEmpty(indivCountry) && indivCountry != "0" ? indivCountry : comCountry);
                                    str.Append("<td>" + countryName + "</td>");
                                }
                                else
                                {
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Address1"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Address2"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Address3"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Address4"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_PostalCode"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_City"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_State"] + "</td>");
                                    str.Append("<td>" + bindCountry(dtDelegate.Rows[d]["reg_Country"] != DBNull.Value ? dtDelegate.Rows[d]["reg_Country"].ToString() : "") + "</td>");
                                }

                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Telcc"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Telac"] + "-" + dtDelegate.Rows[d]["reg_Tel"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Mobcc"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Mobac"] + "-" + dtDelegate.Rows[d]["reg_Mobile"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Faxcc"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Faxac"] + "-" + dtDelegate.Rows[d]["reg_Fax"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Email"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Additional5"] + "</td>");
                                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                                {
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Website"] + "</td>");
                                }
                                else
                                {
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Additional4"] + "</td>");
                                }
                                count++;
                            }
                        }
                        #region Old (Comment)
                        //if (dtU.Rows.Count > 0)
                        //{
                        //    for (int a = 0; a < dtU.Rows.Count; a++)
                        //    {
                        //        #region old
                        //        //if (logid != dtA1.Rows[a]["qitem_id"].ToString() && item != dtU.Rows[u]["qnaire_log_id"].ToString() && !string.IsNullOrEmpty(dtA1.Rows[a]["qitem_class"].ToString()))
                        //        //{
                        //        //    var res = fn.GetDataByCommand("select  count(qnaire_log_id) as R from gen_QnaireResult where qitem_id='" + dtA1.Rows[a]["qitem_id"] + "' and qnaire_log_id='" + dtU.Rows[u]["qnaire_log_id"] + "'", "R");
                        //        //    if (res == "0")
                        //        //    {
                        //        //        res = "";
                        //        //    }
                        //        //    str.Append("<td>" + res + "</td>");
                        //        //    item = dtA1.Rows[a]["qitem_id"].ToString();
                        //        //    logid = dtU.Rows[u]["qnaire_log_id"].ToString();
                        //        //}
                        //        #endregion
                        //        #region new from bex
                        //        if (item != dtU.Rows[0]["qnaire_log_id"].ToString())
                        //        {
                        //            string sqlresult = "SELECT * INTO #TempTable FROM tblQLST10081 "
                        //                       + "ALTER TABLE #TempTable "
                        //                       + "DROP COLUMN qnaire_log_id,user_reference,status,create_date,update_date "
                        //                       + "SELECT * FROM #TempTable";
                        //            DataTable dtresult = fn.GetDatasetByCommand(sqlresult, "sdtResult").Tables[0];
                        //            if (dtresult.Rows.Count > 0)
                        //            {
                        //                foreach (DataRow drR in dtresult.Rows)
                        //                {
                        //                    str.Append("<td>" + drR[] + "</td>");
                        //                }
                        //            }

                        //            item = dtA1.Rows[a]["qitem_id"].ToString();
                        //            logid = dtU.Rows[u]["qnaire_log_id"].ToString();

                        //        }

                        //    }

                        //}
                        #endregion
                        string qnaireid = (from DataRow dr in dtU.Rows
                                           where (string)dr["user_reference"] == daregno
                                           select (string)dr["qnaire_log_id"]).FirstOrDefault();
                        if (!string.IsNullOrEmpty(qnaireid))
                        {
                            string sqlResult = "SELECT * INTO #TempTable FROM tbl" + qnaireID + " where user_reference='" + daregno + "' "
                                                    + "ALTER TABLE #TempTable "
                                                    + "DROP COLUMN qnaire_log_id,user_reference,status,create_date,update_date "
                                                    + "SELECT * FROM #TempTable";
                            DataTable dtResut = qfn.GetDatasetByCommand(sqlResult, "sqResult").Tables[0];
                            if (dtResut.Rows.Count > 0)
                            {
                                foreach (DataRow drR in dtResut.Rows)
                                {
                                    foreach (DataColumn dc in dtResut.Columns)
                                    {
                                        string hutt = drR[dc].ToString();
                                        if (hutt == "0")
                                        {
                                            str.Append("<td></td>");
                                        }
                                        else
                                        {
                                            str.Append("<td>" + hutt + "</td>");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (dtQ.Rows.Count > 0)
                            {
                                for (int q = 0; q < dtQ.Rows.Count; q++)
                                {
                                    string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
                                    DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
                                    if (dtI.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtI.Rows.Count; i++)
                                        {
                                            str.Append("<td></td>");
                                        }
                                    }

                                }
                            }
                        }
                        str.Append("</tr>");
                    }
                }
            }
            #endregion
        }//***MDA
        else
        {
            try
            {
                #region Other Shows
                #region with all answers header(Comment)
                //string sqlA1 = "select * from gen_QuestItem  where  status='Active' and quest_id in (select quest_id from gen_QnaireQuest where qnaire_id='" + qnaireID + "' and status='Active' and quest_id in(select quest_id from gen_Question where quest_class<>'Header'))";
                //DataTable dtA1 = qfn.GetDatasetByCommand(sqlA1, "sdtA1").Tables[0];
                #endregion

                foreach (GridDataItem row in GKeyMaster.Items) // loops through each rows in RadGrid
                {
                    //str.Append("<tr> ");
                    DataRow newdr = NewDt.NewRow();
                    //str.Append("<td>" + (count + 1).ToString() + "</td>");
                    newdr["No"] = (count + 1).ToString();

                    foreach (GridColumn col in GKeyMaster.Columns) //loops through each column in RadGrid
                    {
                        if (col.Visible == true && col.Display == true)
                        {
                            if(col.UniqueName == "UserRegno")
                            {

                            }
                            //var test = DataBinder.Eval(row.DataItem, col.UniqueName).ToString();
                            string dataValue = row[col.UniqueName].Text;
                            if (row[col.UniqueName].Text == "&nbsp;")
                            {
                                if (row.DataItem == null)
                                { }
                                else
                                {
                                    dataValue = DataBinder.Eval(row.DataItem, col.UniqueName).ToString();
                                }
                            }
                            //str.Append("<td>" + dataValue + "</td>");
                            newdr[col.HeaderText] = dataValue;
                        }
                    }

                    #region Questionnaire
                    string daregno = row["Regno"].Text;
                    #region with all answers header(Comment)
                    //if (dtU.Rows.Count > 0)
                    //{
                    //    string qnaireid = (from DataRow dr in dtU.Rows
                    //                       where (string)dr["user_reference"] == daregno
                    //                       select (string)dr["qnaire_log_id"]).FirstOrDefault();
                    //    if (!string.IsNullOrEmpty(qnaireid))
                    //    {
                    #endregion
                    #region Comment by th On 16-7-2018 (add answer in answer columns)
                    //string sqlResult = "SELECT * INTO #TempTable FROM tbl" + qnaireID + " where user_reference='" + daregno + "' "
                    //                        + "ALTER TABLE #TempTable "
                    //                        + "DROP COLUMN qnaire_log_id,user_reference,status,create_date,update_date "
                    //                        + "SELECT * FROM #TempTable";
                    //DataTable dtResut = qfn.GetDatasetByCommand(sqlResult, "sqResult").Tables[0];
                    //if (dtResut.Rows.Count > 0)
                    //{
                    //    foreach (DataRow drR in dtResut.Rows)
                    //    {
                    //        foreach (DataColumn dc in dtResut.Columns)
                    //        {
                    //            string hutt = drR[dc].ToString();
                    //            if (hutt == "0")
                    //            {
                    //                str.Append("<td></td>");
                    //            }
                    //            else
                    //            {
                    //                str.Append("<td>" + hutt + "</td>");
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion
                    #region with main question header
                    if (dtQ.Rows.Count > 0)
                    {
                        for (int a = 0; a < dtQ.Rows.Count; a++)
                        {
                            if (logid != dtQ.Rows[a]["quest_id"].ToString())
                            {
                                string res = string.Empty;
                                string sql = "select r.qitem_id,r.qitem_input_txt,r.qitem_input_num from gen_QnaireLog as l join gen_QnaireResult as r on l.qnaire_log_id=r.qnaire_log_id where l.user_reference='" + daregno + "' and r.quest_id='" + dtQ.Rows[a]["quest_id"].ToString() + "' and qnaire_id='" + qnaireID + "'";
                                DataTable dtres = qfn.GetDatasetByCommand(sql, "R").Tables[0];
                                if (dtres.Rows.Count > 0)
                                {
                                    string answers = string.Empty;
                                    foreach (DataRow dRres in dtres.Rows)
                                    {
                                        if (dRres["qitem_input_num"].ToString() == "0")
                                        {
                                            //res += (dRres["qitem_input_txt"] != DBNull.Value ? (!string.IsNullOrEmpty(dRres["qitem_input_txt"].ToString()) ? dRres["qitem_input_txt"].ToString() + "/" : "") : "");
                                            ////res += (dRres["qitem_input_txt"] != DBNull.Value ? (dRres["qitem_input_txt"].ToString() == "0" ? "" : dRres["qitem_input_txt"].ToString()) : "") + ",";
                                        }
                                        else
                                        {
                                            string qitem = getQuestionItem(dRres["qitem_id"].ToString());
                                            string qItemResult = string.Empty;
                                            if(!string.IsNullOrEmpty(qitem) && qitem != "0")
                                            {
                                                string[] numbers = Regex.Split(qitem, @"\D+");
                                                if(numbers.Length > 0)
                                                {
                                                    int qItemInt = cFun.ParseInt(numbers[0].ToString());
                                                    qItemResult = qItemInt.ToString();
                                                }
                                            }

                                            //if(answers != qItemResult)
                                            {
                                                res += qItemResult + "/";
                                            }
                                            answers = qItemResult;
                                        }
                                    }
                                }
                                else
                                {
                                    res = "";// "NA,";
                                }
                                if (!string.IsNullOrEmpty(res))
                                {
                                    res = res.Remove(res.Length - 1);
                                }
                                //str.Append("<td>" + (!string.IsNullOrEmpty(res) ? "'" + res : res) + "</td>");
                                newdr[dtQ.Rows[a]["quest_desc"].ToString()] = res;
                                logid = dtQ.Rows[a]["quest_id"].ToString();
                            }

                            #region Others
                            string sqlQItem = "Select * From gen_QuestItem Where qitem_Type='Text' And quest_id='" + dtQ.Rows[a]["quest_id"].ToString() + "' And quest_id In"
                                    + " (select top 500 quest_id from gen_QnaireQuest where qnaire_id='" + qnaireID + "' order by qnaire_seq)";
                            DataTable txtBoxDT = qfn.GetDatasetByCommand(sqlQItem, "ds").Tables[0];
                            if (txtBoxDT.Rows.Count > 0)
                            {
                                string res = string.Empty;
                                string sql = "select r.qitem_id,r.qitem_input_txt,r.qitem_input_num from gen_QnaireLog as l join gen_QnaireResult as r on l.qnaire_log_id=r.qnaire_log_id where l.user_reference='" + daregno + "' and r.quest_id='" + dtQ.Rows[a]["quest_id"].ToString() + "' and qnaire_id='" + qnaireID + "' And qitem_input_num=0";
                                DataTable dtres = qfn.GetDatasetByCommand(sql, "R").Tables[0];
                                if (dtres.Rows.Count > 0)
                                {
                                    res = (dtres.Rows[0]["qitem_input_txt"] != DBNull.Value ? (!string.IsNullOrEmpty(dtres.Rows[0]["qitem_input_txt"].ToString()) ? dtres.Rows[0]["qitem_input_txt"].ToString() : "") : "");
                                    //res += (dRres["qitem_input_txt"] != DBNull.Value ? (dRres["qitem_input_txt"].ToString() == "0" ? "" : dRres["qitem_input_txt"].ToString()) : "") + ",";
                                }
                                //str.Append("<td>" + res + "</td>");
                                newdr[dtQ.Rows[a]["quest_desc"].ToString() + "_OST"] = res;
                            }
                            #endregion
                        }
                    }
                    #endregion

                    #region with all answers header(Comment)
                    //}
                    //    else
                    //    {
                    //        if (dtQ.Rows.Count > 0)
                    //        {
                    //            for (int q = 0; q < dtQ.Rows.Count; q++)
                    //            {
                    //                string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
                    //                DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
                    //                if (dtI.Rows.Count > 0)
                    //                {
                    //                    for (int i = 0; i < dtI.Rows.Count; i++)
                    //                    {
                    //                        str.Append("<td></td>");
                    //                    }
                    //                }

                    //            }
                    //        }
                    //    }
                    #endregion
                    //str.Append("</tr>");
                    NewDt.Rows.Add(newdr);
                    #region with all answers header(Comment)
                    //}
                    #endregion
                    #endregion
                    count++;
                }

                GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").Visible = true;
                GKeyMaster.MasterTableView.GetColumn("UserRegno").Visible = false;
                #endregion
            }
            catch(Exception ex)
            {

            }
        }
        htmltb += str.ToString();

        return NewDt;
    }

    protected void lnkExcel_Clicked(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                FlowControler Flw = new FlowControler(fn);
                flowID = ddl_flowList.SelectedValue;
                bool confExist = Flw.checkPageExist(flowID, SiteDefaultValue.constantConfName);
                if (!confExist)
                //if (showID != "EAD351")
                {
                    string QnaireID = GetQIDFromFLW(flowID);
                    ExportExcel(QnaireID);
                }
                else
                {
                    GKeyMaster.AllowPaging = false;
                    //GKeyMaster.Rebind();
                    GKeyMaster.MasterTableView.GetColumn("Download").Display = false;
                    GKeyMaster.MasterTableView.GetColumn("Delete").Display = false;
                    GKeyMaster.MasterTableView.GetColumn("Edit").Display = false;
                    GKeyMaster.MasterTableView.GetColumn("UpdatePayment").Display = false;
                    GKeyMaster.MasterTableView.GetColumn("DownloadInvoice").Display = false;
                    GKeyMaster.MasterTableView.GetColumn("DownloadReceipt").Display = false;
                    GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmail").Display = false;
                    GKeyMaster.MasterTableView.GetColumn("DownloadBadge").Display = false;
                    GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmailOSEA").Display = false;//***only for OSEA
                    GKeyMaster.MasterTableView.GetColumn("UpdateStatus").Display = false;//***only for OSEA
                    GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").Display = false;
                    GKeyMaster.MasterTableView.GetColumn("UserRegno").Display = true;
                    GKeyMaster.ExportSettings.ExportOnlyData = true;
                    GKeyMaster.ExportSettings.IgnorePaging = true;
                    GKeyMaster.ExportSettings.OpenInNewWindow = true;

                    GKeyMaster.MasterTableView.ExportToExcel();
                    //GKeyMaster.AllowPaging = true;
                    //GKeyMaster.Rebind();
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }
        GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").Display = true;
        GKeyMaster.MasterTableView.GetColumn("UserRegno").Display = false;
    }

    public void ExportExcel(string QnaireID)
    {
        try
        {
            string todayDate = DateTime.Now.ToString("dd-MM-yyyy");
            string filename = "MasterRegistrationList_";
            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()) && !string.IsNullOrEmpty(txtToDate.Text.Trim()))
            {
                string sdate = txtFromDate.Text.ToString();
                string[] startdate1 = sdate.Split('/');
                string startdate = startdate1[1] + "-" + startdate1[0] + "-" + startdate1[2];
                string edate = txtToDate.Text.ToString();
                string[] enddate1 = edate.Split('/');
                string enddate = enddate1[1] + "-" + enddate1[0] + "-" + enddate1[2];
                filename += "From" + startdate + "To" + enddate + "_" + todayDate;
            }
            else if(ddlApprove.SelectedIndex > 0)
            {
                //if (ddlApprove.SelectedItem.Value == "99")
                //{
                //    filename = "UpdatedData" + "_" + todayDate;
                //}
                //else
                {
                    filename = ddlApprove.SelectedItem.Text + "_" + todayDate;
                }
            }
            else
            {
                filename += ddlApprove.SelectedItem.Text + "_" + todayDate;
            }
            ////string flowName = ddl_flowList.SelectedItem.Text;
            ////string name = flowName.Replace(" ", "");
            StringBuilder StrBuilder = new StringBuilder();
            htmltb = string.Empty;
            DataTable NewDt=GetDetailReport(QnaireID);
            //StrBuilder.Append("<table cellpadding='0' cellspacing='0' border='1' class='Messages' id='idTbl' runat='server'>");
            //StrBuilder.Append(htmltb.ToString());
            //StrBuilder.Append("</table>");
            //HttpContext.Current.Response.Clear();
            //HttpContext.Current.Response.Buffer = true;
            //HttpContext.Current.Response.Charset = "";
            //HttpContext.Current.Response.ContentType = "application/msexcel";
            //HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode;
            //HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            //////HttpContext.Current.Response.AddHeader("Content-Disposition", "filename=Delegate-Detailed-Report-" + name + ".xls");
            //HttpContext.Current.Response.AddHeader("Content-Disposition", "filename=" + filename + ".xls");
            ////HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            ////string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            ////HttpContext.Current.Response.Write(style);
            ////StringWriter sw = new StringWriter();
            ////HtmlTextWriter htw = new HtmlTextWriter(sw);
            ////htw.Write(style + StrBuilder);
            //HttpContext.Current.Response.Write(StrBuilder);//sw.ToString()
            //HttpContext.Current.Response.End();
            //HttpContext.Current.Response.Flush();
            ExportExcelXLSX(NewDt, filename.Replace(" ", ""), "List");
        }
        catch (System.Threading.ThreadAbortException exf)
        {

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);

        }
    }

    public string GetQIDFromFLW(string flowID)
    {
        string result = string.Empty;

        string query = "select SQ_QAID from tb_site_QA_Master where SQ_FLW_ID='" + flowID + "' and SQ_Category='D'";
        result = fn.GetDataByCommand(query, "SQ_QAID");

        return result;
    }

    public int GetQuestCount(string questID)
    {
        int Qcount = 0;
        string sql = "select * from gen_QuestItem where status='Active' and quest_id='" + questID + "'";
        DataSet ds = new DataSet();
        ds = qfn.GetDatasetByCommand(sql, "dsds");

        Qcount = ds.Tables[0].Rows.Count;

        return Qcount;

    }

    private string getQuestionItem(string qitem_id)
    {
        string rtn = string.Empty;
        string sql = "select qitem_desc from gen_QuestItem where qitem_id='" + qitem_id + "'";
        rtn = qfn.GetDataByCommand(sql, "qitem_desc");
        return rtn;
    }


    #region bindName
    public string bindSalutation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getSalutationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindProfession(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getProfessionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDepartment(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDepartmentNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindOrganisation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getOrganisationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindInstitution(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getInstitutionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindCountry(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    CountryObj conCtr = new CountryObj(fn);
                    name = conCtr.getCountryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindAffiliation(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getAffiliationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDietary(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDietaryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindIndustry(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getIndustryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    #region bindPhoneNo
    public string bindPhoneNo(string cc, string ac, string phoneno, string type)
    {
        string name = string.Empty;
        bool isShowCC = false, isShowAC = false, isShowPhoneNo = false;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string flowid = ddl_flowList.SelectedItem.Value;
                if (!string.IsNullOrEmpty(showID))
                {
                    DataSet ds = new DataSet();
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showID;
                    frmObj.flowID = flowid;
                    ds = frmObj.getDynFormForDelegate();

                    for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                    {
                        if (type == "Tel")
                        {
                            #region type="Tel"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                        else if (type == "Mob")
                        {
                            #region type="Mob"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                        else if (type == "Fax")
                        {
                            #region Type="Fax"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                    }

                    if (isShowCC)
                    {
                        name = "+" + cc;
                    }
                    if (isShowAC)
                    {
                        name += " " + ac;
                    }
                    if (isShowPhoneNo)
                    {
                        name += " " + phoneno;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion

    #region Invoice
    public string getInvoiceStatus(string invStatus)
    {
        string result = string.Empty;
        try
        {
            //InvoiceControler invControler = new InvoiceControler(fn);
            //StatusSettings stuSettings = new StatusSettings(fn);
            //string invStatus = invControler.getInvoiceStatus(regno);
            string sqlStatus = "select status_name from ref_Status Where status_usedid='" + invStatus + "'";
            result = fn.GetDataByCommand(sqlStatus, "status_name");
            if (result == "0" || string.IsNullOrEmpty(result))
            {
                result = "Pending";
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getPaymentMethod(string paymentMethod)
    {
        string result = string.Empty;
        try
        {
            string sqlStatus = "Select method_name From tmp_refPaymentMethod Where method_usedid='" + paymentMethod + "'";
            result = fn.GetDataByCommand(sqlStatus, "method_name");
            if (result == "0" || string.IsNullOrEmpty(result))
            {
                result = "N/A";
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getPaidPrice(string InvoiceID)
    {
        string result = string.Empty;
        try
        {
            PaymentControler pControl = new PaymentControler(fn);
            decimal totalPaidAmt = pControl.GetPaymentTotalPaidAmtByInvNo(InvoiceID);
            result = String.Format("{0:f2}", totalPaidAmt);
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string calculateOutstanding(string grandTotal, string InvoiceID)
    {
        string result = string.Empty;
        try
        {
            Double grandtotalAmount = 0;
            if (!string.IsNullOrEmpty(grandTotal))
            {
                Double.TryParse(grandTotal, out grandtotalAmount);
            }
            string paidprice = getPaidPrice(InvoiceID);
            Double paidAmount = 0;
            if (!string.IsNullOrEmpty(paidprice))
            {
                Double.TryParse(paidprice, out paidAmount);
            }
            result = String.Format("{0:f2}", (grandtotalAmount - paidAmount));
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getCongressSelection(string regno, string groupid, string invStatus)
    {
        string result = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                OrderControler oControl = new OrderControler(fn);
                //string currenturl= Request.Url.AbsoluteUri;
                string fullUrl = "GRP=" + cFun.EncryptValue(groupid) + "&INV=" + cFun.EncryptValue(regno) + "&SHW=" + cFun.EncryptValue(showID)
                    + "&FLW=" + cFun.EncryptValue(ddl_flowList.SelectedItem.Value);
                FlowURLQuery urlQuery = new FlowURLQuery(fullUrl);

                StatusSettings stuSettings = new StatusSettings(fn);
                if (invStatus == stuSettings.Pending.ToString()
                    || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString()
                    || invStatus == noInvoice)
                {
                    OrderItemList oList = new OrderItemList();
                    oList = oControl.GetPendingOrderList(urlQuery);//***Pending Order
                    result = getCongressSelectionString(oList);
                }
                else
                {
                    List<OrderItemList> oSucList = getPaidOrder(urlQuery);//***Paid Order
                    if (oSucList.Count > 0)
                    {
                        foreach (OrderItemList ordList in oSucList)
                        {
                            result = getCongressSelectionString(ordList);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    private List<OrderItemList> getPaidOrder(FlowURLQuery urlQuery)
    {
        List<OrderItemList> oPaidOrderItemList = new List<OrderItemList>();
        try
        {
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cFun.DecryptValue(urlQuery.DelegateID);
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj != null && invListObj.Count > 0)
            {
                foreach (Invoice invObj in invListObj)
                {
                    OrderControler oControl = new OrderControler(fn);
                    oPaidOrderItemList = oControl.GetAllOrderedListByInvoiceID(urlQuery, invObj.InvoiceID);
                }
            }
        }
        catch (Exception ex)
        { oPaidOrderItemList = new List<OrderItemList>(); }

        return oPaidOrderItemList;
    }
    private string getCongressSelectionString(OrderItemList OList)
    {
        string selectedCongress = string.Empty;
        try
        {
            foreach (OrderItem oItem in OList.OrderList)
            {
                selectedCongress += oItem.ItemDescription + ",";
            }
            selectedCongress = selectedCongress.TrimEnd(',');
        }
        catch (Exception ex)
        { }

        return selectedCongress;
    }
    public bool isPaymentVisible(string invStatus)
    {
        bool isVisible = false;
        try
        {
            StatusSettings stuSettings = new StatusSettings(fn);
            //invStatus = getInvoiceStatus(invStatus);
            if (invStatus == stuSettings.Pending.ToString() || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
            {
                isVisible = true;
            }
        }
        catch (Exception ex)
        { }

        return isVisible;
    }
    public bool isDownloadReceiptVisible(string invStatus)
    {
        bool isVisible = false;
        try
        {
            StatusSettings stuSettings = new StatusSettings(fn);
            //invStatus = getInvoiceStatus(invStatus);
            if (invStatus == stuSettings.Success.ToString())
            {
                isVisible = true;
            }
        }
        catch (Exception ex)
        { }

        return isVisible;
    }
    #endregion
    #endregion

    #region btnUpdatePayment_Click
    protected void btnUpdatePayment_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string invoiceID = arg[2];
                string flowid = ddl_flowList.SelectedItem.Value;
                string step = flwObj.GetLatestStep(flowid);
                //string fullUrl = "FLW=" + cFun.DecryptValue(ddl_flowList.SelectedValue) + "&STP=" + cFun.DecryptValue(step)
                //+ "&GRP=" + cFun.DecryptValue(groupid) + "&INV=" + cFun.DecryptValue(regno) + "&SHW=" + cFun.DecryptValue(showID);
                string page = "PaymentUpdate.aspx";
                string route = flwObj.MakeFullURL(page, flowid, showID, groupid, step, regno);//, BackendRegType.backendRegType_Delegate);
                route += "&INVID=" + cFun.EncryptValue(invoiceID);
                Response.Redirect(route);
            }
            catch (Exception ex)
            {
                //Response.Redirect("Login.aspx");
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region btnDownloadInvoice_Command
    protected void btnDownloadInvoice_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                InvoiceControler invControler = new InvoiceControler(fn);
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string invoiceID = arg[2];
                string flowid = ddl_flowList.SelectedItem.Value;
                string step = flwObj.GetLatestStep(flowid);
                string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                string fullUrl = "?SHW=" + cFun.EncryptValue(showID) + "&DID=" + cFun.EncryptValue(invOwnerID) + "&VDD=" + cFun.EncryptValue(invoiceID);
                string page = "../DownloadInvoiceLetter.aspx";
                string route = page + fullUrl;
                string script = String.Format("window.open('{0}','_blank');", route);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                //ClientScript.RegisterStartupScript(GetType(), "scr", script, true);
                //Response.Redirect(route);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region ConfirmationEmail
    #region btnDownloadReceipt_Command
    protected void btnDownloadReceipt_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                InvoiceControler invControler = new InvoiceControler(fn);
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string invoiceID = arg[2];
                string flowid = ddl_flowList.SelectedItem.Value;
                string step = flwObj.GetLatestStep(flowid);
                string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                string fullUrl = "?SHW=" + cFun.EncryptValue(showID) + "&DID=" + cFun.EncryptValue(invOwnerID) + "&VDD=" + cFun.EncryptValue(invoiceID);
                string page = "../DownloadReceiptLetter.aspx";
                string route = page + fullUrl;
                string script = String.Format("window.open('{0}','_blank');", route);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                //ClientScript.RegisterStartupScript(GetType(), "scr", script, true);
                //Response.Redirect(route);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
    #region btnSendConfirmationEmail_Command
    protected void btnSendConfirmationEmail_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                InvoiceControler invControler = new InvoiceControler(fn);
                EmailHelper eHelper = new EmailHelper();
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string invoiceID = arg[2];
                string eType = EmailHTMLTemlateType.Confirmation;
                string emailRegType = BackendRegType.backendRegType_Delegate;// "D";
                EmailDSourceKey sourcKey = new EmailDSourceKey();
                //string showID = showid;// "MBF242";
                string flowid = ddl_flowList.SelectedItem.Value;
                string flowID = flowid;// "F322";
                string groupID = groupid;// "2421113";
                string delegateID = regno;// "24210092";//Blank

                sourcKey.AddKeyToList("ShowID", showID);
                sourcKey.AddKeyToList("RegGroupID", groupID);
                sourcKey.AddKeyToList("Regno", delegateID);
                sourcKey.AddKeyToList("FlowID", flowID);
                sourcKey.AddKeyToList("INVID", invoiceID);

                string updatedUser = string.Empty;
                if (Session["userid"] != null)
                {
                    updatedUser = Session["userid"].ToString();
                }
                eHelper.SendEmailFromBackend(eType, sourcKey, emailRegType, updatedUser);
                //string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already sent the confirmation email.');", true);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
    #region btnSendConfirmationEmailOSEA_Command
    protected void btnSendConfirmationEmailOSEA_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                InvoiceControler invControler = new InvoiceControler(fn);
                EmailHelper eHelper = new EmailHelper();
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string approvestatus = arg[2];

                if (!string.IsNullOrEmpty(approvestatus) && !string.IsNullOrWhiteSpace(approvestatus))
                {
                    if (cFun.ParseInt(approvestatus) == (int)RegApproveRejectStatus.Confirmed
                        || cFun.ParseInt(approvestatus) == (int)RegApproveRejectStatus.Approved
                        || cFun.ParseInt(approvestatus) == (int)RegApproveRejectStatus.VIP)
                    {
                        string eType = EmailHTMLTemlateType.Confirmation;
                        string emailRegType = BackendRegType.backendRegType_Delegate;// "D";
                        string emailCondition = string.Empty;
                        if (cFun.ParseInt(approvestatus) == (int)RegApproveRejectStatus.Confirmed)
                        {
                            emailCondition = EmailSendConditionType.CONFIRM;
                        }
                        else if (cFun.ParseInt(approvestatus) == (int)RegApproveRejectStatus.Approved)
                        {
                            emailCondition = EmailSendConditionType.APPROVE;
                        }
                        else if (cFun.ParseInt(approvestatus) == (int)RegApproveRejectStatus.VIP)
                        {
                            emailCondition = EmailSendConditionType.VIP;
                        }
                        EmailDSourceKey sourcKey = new EmailDSourceKey();
                        string flowid = ddl_flowList.SelectedItem.Value;
                        string flowID = flowid;
                        string groupID = groupid;
                        string delegateID = regno;

                        sourcKey.AddKeyToList("ShowID", showID);
                        sourcKey.AddKeyToList("RegGroupID", groupID);
                        sourcKey.AddKeyToList("Regno", delegateID);
                        sourcKey.AddKeyToList("FlowID", flowID);

                        string updatedUser = string.Empty;
                        if (Session["userid"] != null)
                        {
                            updatedUser = Session["userid"].ToString();
                        }
                        bool isEmailSend = eHelper.SendEmailFromBackendByCondition(emailCondition, sourcKey, emailRegType, updatedUser);
                        #region Logging
                        LogGenEmail lggenemail = new LogGenEmail(fn);
                        LogActionObj lactObj = new LogActionObj();
                        lggenemail.type = LogType.generalType;
                        lggenemail.RefNumber = groupID + "," + regno;
                        lggenemail.description = "Re-send confrimation email to " + delegateID;
                        lggenemail.remark = "Re-send confrimation email to " + delegateID + " and Send " + emailCondition + " email(email sending status:" + isEmailSend + ")";
                        lggenemail.step = "backend";
                        lggenemail.writeLog();
                        #endregion

                        string msg = isEmailSend == true ? "Already sent the confirmation email(email sending status:" + isEmailSend + ")." : "Email sending fail.";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + msg + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('The status of the record is not able to re-send confirmation, please check!');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('The status of the record is not able to re-send confirmation, please check!');", true);
                }
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
    #endregion

    #region btnDownloadBadge_Command
    protected void btnDownloadBadge_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                InvoiceControler invControler = new InvoiceControler(fn);
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string invoiceID = arg[2];
                string flowid = ddl_flowList.SelectedItem.Value;
                //"DownloadBadge?SHW=NjJunAD0GCAcTe8pUOEhMA==&DID=n2Kikqcp8RaMaLz4kNj9cg=="
                string fullUrl = "?SHW=" + cFun.EncryptValue(showID) + "&DID=" + cFun.EncryptValue(regno);
                string page = "../DownloadBadge.aspx";
                string route = page + fullUrl;
                string script = String.Format("window.open('{0}','_blank');", route);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                //ClientScript.RegisterStartupScript(GetType(), "scr", script, true);
                //Response.Redirect(route);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region getPrepopulateDataByShowFlow
    private DataTable getPrepopulateDataByShowFlow(string showid, string flowid)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = fn.GetDatasetByCommand(string.Format("Select * From tb_RegPrePopulate Where (recycle=0 Or recycle Is Null) And ShowID='{0}' And reg_urlFlowID='{1}'"
                + " And (RefNo Is Not Null And RefNo<>'')"
                , showid, flowid), "ds").Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        #region Old
        //try
        //{
        //    if (Session["roleid"].ToString() != "1")
        //        getShowID(Session["userid"].ToString());
        //    else
        //        showID = ddl_showList.SelectedValue;

        //    flowID = ddl_flowList.SelectedValue;
        //    if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()) && !string.IsNullOrEmpty(txtToDate.Text.Trim()))
        //    {
        //        string fromD = txtFromDate.Text.ToString();
        //        string toD = txtToDate.Text.ToString();
        //        int status = Convert.ToInt16(ddl_paymentStatus.SelectedValue);

        //        if (status == -1)
        //        {
        //            GKeyMaster.DataSource = getDataAllRegList(fromD, toD);
        //        }
        //        else if (status == -20)//***MDA
        //        {
        //            DataTable dtRegList = getDataAllRegList(fromD, toD);
        //            DataTable dtPreRegList = getPrepopulateDataByShowFlow(showID, flowID);

        //            DataTable TableC = dtRegList.AsEnumerable()
        //                    .Where(ra => dtPreRegList.AsEnumerable()
        //                                        .Any(rb => rb.Field<int>("RefNo") == ra.Field<int>("Regno")))
        //                    .CopyToDataTable();
        //            GKeyMaster.DataSource = TableC;
        //        }//***MDA
        //        else
        //            GKeyMaster.DataSource = getDataByPaymentStatus(status, fromD, toD);


        //        if (GKeyMaster.Items.Count > 0)
        //        {
        //            GKeyMaster.AllowPaging = false;
        //            GKeyMaster.Rebind();
        //        }

        //    }
        //    else
        //    {
        //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please fill Date');", true);
        //        return;
        //    }



        //}
        //catch (Exception ex)
        //{ }
        #endregion
        ddlApprove.SelectedIndex = 0;
        txtKey.Text = "";
        GKeyMaster_NeedDataSource(this, null);
    }

    public DataTable getDataAllRegList(string sdate, string edate)
    {
        DataTable dt = new DataTable();

        try
        {
            string[] startdate1 = sdate.Split('/');
            //string startdate = startdate1[2] + "-" + startdate1[0] + "-" + startdate1[1];
            string startdate = startdate1[2] + "/" + startdate1[0] + "/" + startdate1[1];
            string[] enddate1 = edate.Split('/');
            //string enddate = enddate1[2] + "-" + enddate1[0] + "-" + enddate1[1];
            string enddate = enddate1[2] + "/" + enddate1[0] + "/" + enddate1[1];
            string query = "Select * From RegIndiv_All Where ShowID=@SHWID";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);


            if (!string.IsNullOrEmpty(flowID))
            {
                query += " and reg_urlFlowID=@reg_urlFlowID and reg_datecreated  BETWEEN '" + startdate + "' and DATEADD(s,-1,DATEADD(d,1,'" + enddate + "'))";
                spar = new SqlParameter("reg_urlFlowID", SqlDbType.NVarChar);
                spar.Value = flowID;
                pList.Add(spar);
            }

            dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }

    public DataTable getDataByPaymentStatus(int status, string sdate, string edate)
    {
        DataTable dt = new DataTable();

        try
        {
            string[] startdate1 = sdate.Split('/');
            string startdate = startdate1[2] + "-" + startdate1[0] + "-" + startdate1[1];
            string[] enddate1 = edate.Split('/');
            string enddate = enddate1[2] + "-" + enddate1[0] + "-" + enddate1[1];
            string query = "Select * From RegIndiv_All Where ShowID=@SHWID and Invoice_status=@Status";

            if (status == 3)
                query = "Select * From RegIndiv_All Where ShowID=@SHWID and (Invoice_status=@Status or (Invoice_status=-10 and reg_Status=1))";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);

            if (!string.IsNullOrEmpty(flowID))
            {
                query += " and reg_urlFlowID=@reg_urlFlowID and reg_datecreated  BETWEEN '" + startdate + "' and '" + enddate + "'";
                spar = new SqlParameter("reg_urlFlowID", SqlDbType.NVarChar);
                spar.Value = flowID;
                pList.Add(spar);
            }

            spar = new SqlParameter("Status", SqlDbType.Int);
            spar.Value = status;
            pList.Add(spar);

            dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }

    public string checkDateForExcel()
    {
        string start = txtFromDate.Text.Trim();
        string end = txtToDate.Text.Trim();
        string res = string.Empty;
        if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
        {
            res = "and reg_datecreated  BETWEEN '" + start + "' and '" + end + "'";
        }
        else
        {
            res = "";
        }
        return res;
    }

    public string ShowApproveRejectStatus(object objStatus)
    {
        string rtn = "";
        if (objStatus != null)
        {
            if (objStatus.GetType() != typeof(System.DBNull))
            {
                string recStatus = (string)objStatus;
                
                if ( ((int)RegApproveRejectStatus.Rejected).ToString() == recStatus)
                    rtn = "Auto Rejected";
                else if (((int)RegApproveRejectStatus.Confirmed).ToString() == recStatus)
                    rtn = RegApproveRejectStatus.Confirmed.ToString();
                else if (((int)RegApproveRejectStatus.VIP).ToString() == recStatus)
                    rtn = RegApproveRejectStatus.VIP.ToString();
                else if (((int)RegApproveRejectStatus.Approved).ToString() == recStatus)
                    rtn = RegApproveRejectStatus.Approved.ToString();
                else if (((int)RegApproveRejectStatus.Declined).ToString() == recStatus)
                    rtn = RegApproveRejectStatus.Declined.ToString();
            }
        }
        return rtn;
    }
    public bool isShowOSWAResendConfirmationEmail(object objStatus, string regStatus)
    {
        bool isVisible = false;
        try
        {
            if (objStatus != null)
            {
                if (objStatus.GetType() != typeof(System.DBNull))
                {
                    string recStatus = (string)objStatus;
                    if (regStatus == "1" &&
                        (((int)RegApproveRejectStatus.Confirmed).ToString() == recStatus
                        || ((int)RegApproveRejectStatus.Approved).ToString() == recStatus
                        || ((int)RegApproveRejectStatus.VIP).ToString() == recStatus))
                    {
                        isVisible = true;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return isVisible;
    }

    #region btnUpdateStatus_Command
    protected void btnUpdateStatus_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string flowid = ddl_flowList.SelectedItem.Value;
                string step = flwObj.GetLatestStep(flowid);
                string page = "RegApproveRejectOSEA.aspx";
                string route = flwObj.MakeFullURL(page, flowid, showID, groupid, step, regno);
                Response.Redirect(route);
            }
            catch (Exception ex)
            {
                //Response.Redirect("Login.aspx");
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    protected void btnKeysearch_Click(object sender, EventArgs e)
    {
        #region Old
        //try
        //{
        //    MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //    getShowID(Session["userid"].ToString());
        //    msregIndiv.ShowID = showID;
        //    flowID = ddl_flowList.SelectedValue;
        //    msregIndiv.FlowID = flowID;
        //    if (!string.IsNullOrEmpty(txtKey.Text.Trim()))
        //    {
        //        string key = txtKey.Text.Trim();
        //        DataTable dtIndiv = msregIndiv.getDataAllRegList();
        //        if (dtIndiv.Rows.Count > 0)
        //        {
        //            //DataRow[] result = dtIndiv.Select("Regno='" + key + "'");
        //            //GKeyMaster.DataSource = result;
        //            DataView dv = new DataView();
        //            dv = dtIndiv.DefaultView;
        //            if (Regex.IsMatch(key, @"^\d+$"))
        //            {
        //                dv.RowFilter = "reg_Membershipno='" + key + "'";
        //            }
        //            else
        //            {
        //                dv.RowFilter = "reg_FName like '%" + key + "%' Or reg_LName like '%" + key + "%' Or reg_Email like '%" + key + "%' Or reg_Address3 like '%" + key + "%'";
        //            }
        //            DataTable dt = dv.ToTable();
        //            GKeyMaster.DataSource = dt;
        //            GKeyMaster.DataBind();
        //            if (dt.Rows.Count > 0)
        //            {
        //                GKeyMaster.AllowPaging = false;
        //                GKeyMaster.Rebind();
        //            }
        //        }
        //    }
        //}catch(Exception ex)
        //{ }
        #endregion
        ddlApprove.SelectedIndex = 0;
        txtFromDate.Text = "";
        txtToDate.Text = "";
        GKeyMaster_NeedDataSource(this, null);
    }

    protected void ddlApprove_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region Old
        //try
        //{
        //    MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //    if (Session["roleid"].ToString() != "1")
        //        getShowID(Session["userid"].ToString());
        //    else
        //        showID = ddl_showList.SelectedValue;
        //    msregIndiv.ShowID = showID;
        //    flowID = ddl_flowList.SelectedValue;
        //    msregIndiv.FlowID = flowID;
        //    if (!string.IsNullOrEmpty(ddlApprove.SelectedValue))
        //    {
        //        string value = ddlApprove.SelectedValue;
        //        DataTable dtIndiv = msregIndiv.getDataAllRegList();
        //        if (dtIndiv.Rows.Count > 0)
        //        {
        //            DataView dv = new DataView();
        //            dv = dtIndiv.DefaultView;
        //            if (value == "22")
        //            {
        //                dv.RowFilter = "(reg_approveStatus='" + value + "' Or reg_approveStatus='" + (int)RegApproveRejectStatus.Confirmed + "')";
        //                //+ " And (reg_Additional5 IS NULL Or reg_Additional5='')";
        //            }
        //            else if (value == "33")
        //            {
        //                dv.RowFilter = "reg_approveStatus='" + value + "' And (reg_Status=1)";
        //            }
        //            else if (value == "66")
        //            {
        //                dv.RowFilter = "reg_approveStatus='" + value + "'";
        //            }
        //            else if (value == "88")
        //            {
        //                dv.RowFilter = "reg_approveStatus='" + value + "'";
        //            }
        //            else if (value == "00")
        //            {
        //                dv.RowFilter = "(reg_Status is NULL Or reg_Status=0)";// And reg_Additional5 IS NOT NULL AND reg_Additional5 <> ''";
        //            }
        //            else if (value == "99")
        //            {
        //                dv.RowFilter = "reg_Status='1' And reg_Additional5 IS NOT NULL AND reg_Additional5 <> '' And reg_isReg=1";
        //            }
        //            DataTable dt = dv.ToTable();
        //            GKeyMaster.DataSource = dt;
        //            GKeyMaster.DataBind();
        //            if (dt.Rows.Count > 0)
        //            {
        //                GKeyMaster.AllowPaging = false;
        //                GKeyMaster.Rebind();
        //            }
        //        }
        //    }

        //}
        //catch (Exception ex)
        //{

        //}
        #endregion
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtKey.Text = "";
        GKeyMaster_NeedDataSource(this, null);
    }

    private DataTable getDataSource(ref bool isBind)
    {
        DataTable dt = new DataTable();
        try
        {
            MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;
            msregIndiv.ShowID = showID;
            flowID = ddl_flowList.SelectedValue;
            msregIndiv.FlowID = flowID;
            DataTable dtIndiv = msregIndiv.getDataAllRegList();
            if (!string.IsNullOrEmpty(txtKey.Text.Trim()))
            {
                isBind = true;
                string key = txtKey.Text.Trim();
                if (dtIndiv.Rows.Count > 0)
                {
                    //DataRow[] result = dtIndiv.Select("Regno='" + key + "'");
                    //GKeyMaster.DataSource = result;
                    DataView dv = new DataView();
                    dv = dtIndiv.DefaultView;
                    if (Regex.IsMatch(key, @"^\d+$"))
                    {
                        dv.RowFilter = "reg_Membershipno='" + key + "'";
                    }
                    else
                    {
                        dv.RowFilter = "reg_FName like '%" + key + "%' Or reg_LName like '%" + key + "%' Or reg_Email like '%" + key + "%' Or reg_Address3 like '%" + key + "%'";
                    }
                    dv.Sort = "reg_datecreated DESC";
                    dt = dv.ToTable();
                }
            }
            else if (ddlApprove.SelectedIndex > 0)
            {
                string value = ddlApprove.SelectedValue;
                if (dtIndiv.Rows.Count > 0)
                {
                    DataView dv = new DataView();
                    dv = dtIndiv.DefaultView;
                    if (value == "22")
                    {
                        isBind = true;
                        dv.RowFilter = "(reg_approveStatus='" + value + "' Or reg_approveStatus='" + (int)RegApproveRejectStatus.Confirmed + "')";
                        //+ " And (reg_Additional5 IS NULL Or reg_Additional5='')";
                    }
                    else if (value == "33")
                    {
                        isBind = true;
                        dv.RowFilter = "reg_approveStatus='" + value + "' And (reg_Status=1)";
                    }
                    else if (value == "66")
                    {
                        isBind = true;
                        dv.RowFilter = "reg_approveStatus='" + value + "'";
                    }
                    else if (value == "88")
                    {
                        isBind = true;
                        dv.RowFilter = "reg_approveStatus='" + value + "'";
                    }
                    else if (value == "00")
                    {
                        isBind = true;
                        dv.RowFilter = "(reg_Status is NULL Or reg_Status=0)";// And reg_Additional5 IS NOT NULL AND reg_Additional5 <> ''";
                    }
                    else if (value == "99")
                    {
                        isBind = true;
                        dv.RowFilter = "reg_Status='1' And reg_Additional5 IS NOT NULL AND reg_Additional5 <> '' And reg_isReg=1";
                    }
                    dv.Sort = "reg_datecreated DESC";
                    dt = dv.ToTable();
                }
            }
            else if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()) && !string.IsNullOrEmpty(txtToDate.Text.Trim()))
            {
                isBind = true;
                string fromD = txtFromDate.Text.ToString();
                string toD = txtToDate.Text.ToString();
                int status = Convert.ToInt16(ddl_paymentStatus.SelectedValue);

                if (status == -1)
                {
                    dt = getDataAllRegList(fromD, toD);
                }
                else if (status == -20)//***MDA
                {
                    DataTable dtRegList = getDataAllRegList(fromD, toD);
                    DataTable dtPreRegList = getPrepopulateDataByShowFlow(showID, flowID);

                    DataTable TableC = dtRegList.AsEnumerable()
                            .Where(ra => dtPreRegList.AsEnumerable()
                                                .Any(rb => rb.Field<int>("RefNo") == ra.Field<int>("Regno")))
                            .CopyToDataTable();
                    dt = TableC;
                }//***MDA
                else
                    dt = getDataByPaymentStatus(status, fromD, toD);
            }
            else
            {
                dt = dtIndiv;
            }
        }
        catch (Exception ex)
        { }

        return dt;
    }

    #region ExportExcelXLSX
    public static void ExportExcelXLSX(DataTable dtData, string FileName, string sheetName)
    {
        XLWorkbook wb = new XLWorkbook();
        var ws = wb.Worksheets.Add(sheetName);
        ws.Cell(2, 1).InsertTable(dtData);
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        ws.Row(1).Delete();
        ws.Tables.FirstOrDefault().ShowAutoFilter = false;//remove first blank column and filter
        ws.FirstRow().AddConditionalFormat().WhenGreaterThan(20000).Fill.SetBackgroundColor(XLColor.Gray);//add bg color at first row(header)
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format(@"attachment;filename={0}.xlsx", FileName));

        using (MemoryStream memoryStream = new MemoryStream())
        {
            wb.SaveAs(memoryStream);
            memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
            memoryStream.Close();
        }

        HttpContext.Current.Response.End();
    }
    #endregion
}

