﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Settings_Master.aspx.cs" Inherits="Admin_Event_Settings_Master" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        <%--function ValidateCheckBoxListHear(sender, args) {
            var checkBoxList = document.getElementById("<%=chkPaymentMethod.ClientID %>");
            var checkboxes = checkBoxList.getElementsByTagName("input");
            var isValid = false;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isValid = true;
                    break;
                }
            }
            args.IsValid = isValid;
        }--%>

        function uploadComplete(sender, args) {
            var filename = args.get_fileName();
            $get("imgbanner").src = "./Site/" + filename;
            $get("imgbanner").style.display = '';
            document.getElementById('ContentPlaceHolder1_imgbanner').style.display = 'none';
        }
    </script>

    <style type="text/css">
        .stdtable2 tbody tr td:first-child,
        .stdtable2 tbody tr td:last-child,
        .stdtable2 tbody tr td:first-child td,
        .stdtable2 tbody tr:last-child td {
            border-right: 0px none !important;
            border-left: 0px none !important;
            border-bottom: 0px none !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="Event" class="subcontent">
                <asp:Panel ID="pnlmessagebar" runat="server" CssClass="notibar announcement" Visible="false">
                    <p class="txtgreen">
                        <asp:Label ID="lblemsg" runat="server" Text="Label"></asp:Label>
                    </p>
                </asp:Panel>

                <div class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group" style="display:none;">
                            <label class="col-md-2 control-label">Show Name</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtEventName" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Page Title</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txt_title" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Page Banner</label>
                            <div class="col-md-6">
                                <asp:FileUpload runat="server" ID="fupload" />
                                <asp:Image runat="server" ID="imgbanner" Style="width: 400px;" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Footer/Copyright Message</label>
                            <div class="col-md-6">
                                <CKEditor:CKEditorControl ID="txtCopyright" runat="server" Height="200px" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Event Prefix</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtPrefix" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-md-2 control-label" style="color:#ff6a00">Registration Closing Date</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtRegClosingDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="images/icons/calendar.png" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Registration Closing Message</label>
                            <div class="col-md-6">
                                <CKEditor:CKEditorControl ID="txtRegcloseMsg" runat="server" Height="200px" Width="98%" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Site Master</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlsitemaster" runat="server" CssClass="form-control"></asp:DropDownList>
                                <asp:LinkButton ID="lnksitemaster" runat="server" Text="Manage Site Master" OnClick="lnksitemaster_Click"></asp:LinkButton>
                            </div>
                        </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label">Site Re-login Master</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlsitereloginmaster" runat="server" CssClass="form-control"></asp:DropDownList>
                                <asp:LinkButton ID="lnksitereloginmaster" runat="server" Text="Manage Site Re-login Master" OnClick="lnksitereloginmaster_Click"></asp:LinkButton>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label class="col-md-2 control-label">Site CSS Bandle</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlcssbandle" runat="server" CssClass="form-control"></asp:DropDownList>
                                <asp:LinkButton ID="lnkcssbandle" runat="server" Text="Manage Site CSS Bandle" OnClick="lnkcssbandle_Click"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">SMTP Username</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtsmtpuser" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">SMTP Password</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtsmtppswd" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">SMTP Client</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtsmtpclient" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Use Default Credentials</label>
                            <div class="col-md-6">
                                <asp:DropDownList runat="server" ID="ddludc" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Enable Ssl</label>
                            <div class="col-md-6">
                                <asp:DropDownList runat="server" ID="ddlenablessl" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">From Email Address</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtfea" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">From Email Description</label>
                            <div class="col-md-6">
                                <asp:TextBox runat="server" ID="txtfed" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                      
                        <div class="form-group">
                            <label class="col-md-2 control-label">Event Currency</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="form-control"></asp:DropDownList>
                                <asp:LinkButton ID="lnkCurrency" runat="server" Text="Manage Currency" OnClick="lnkCurrency_Click"></asp:LinkButton>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label class="col-md-2 control-label">Admin Fee</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtAdminFee" runat="server" CssClass="form-control" MaxLength="3"></asp:TextBox>
                                %
                                <asp:FilteredTextBoxExtender ID="ftbAdminFee" runat="server" TargetControlID="txtAdminFee" FilterType="Numbers, Custom" ValidChars="." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">GST Fee</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtGSTFee" runat="server" CssClass="form-control" MaxLength="3"></asp:TextBox>
                                %
                                <asp:FilteredTextBoxExtender ID="ftb" runat="server" TargetControlID="txtGSTFee" FilterType="Numbers, Custom" ValidChars="." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Early Bird Closing Date</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtEarlyClosingDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="images/icons/calendar.png" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Enable Timer?</label>
                            <div class="col-md-6">
                                <asp:RadioButtonList ID="rbTimer" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                    AutoPostBack="true" OnSelectedIndexChanged="rbTimer_SelectedIndexChanged">
                                    <asp:ListItem Text="No" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="form-group" id="trTimerType" runat="server" visible="false">
                            <label class="col-md-2 control-label">Timer Type</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlTimerType" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="-----Select-----" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Second" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Minute" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="vcTimerType" runat="server" Enabled="false"
                                    ControlToValidate="ddlTimerType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Enable Questionnaire?</label>
                            <div class="col-md-6">
                                <asp:RadioButton ID="rbQues1" runat="server" GroupName="rbq" Text="No" />
                                <asp:RadioButton ID="rbQues2" runat="server" GroupName="rbq" Text="Yes" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Questionnaire Prefix</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtQuestionPrefix" runat="server" Text="QLST" Enabled="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Enable Terms & Conditions?</label>
                            <div class="col-md-6">
                                <asp:RadioButton ID="rbTerms1" runat="server" GroupName="rbterm" Text="No" />
                                <asp:RadioButton ID="rbTerms2" runat="server" GroupName="rbterm" Text="Yes" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Payment Method</label>
                            <div class="col-md-6">
                                <%--<asp:CheckBoxList ID="chkPaymentMethod" runat="server" RepeatDirection="Vertical"></asp:CheckBoxList>--%>
                                <asp:CheckBox ID="chkCreditCard" runat="server" Text="Credit Card" />
                                <asp:HiddenField ID="hfCreditCard" runat="server" Value="" />
                                <br />
                                <CKEditor:CKEditorControl ID="txtCreditCard" runat="server" Height="200px" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                <br />
                                <asp:CheckBox ID="chkTT" runat="server" Text="Telegraphic Transfer" />
                                <asp:HiddenField ID="hfTT" runat="server" Value="" />
                                <br />
                                <CKEditor:CKEditorControl ID="txtTT" runat="server" Height="200px" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                <br />
                                <asp:CheckBox ID="chkWaived" runat="server" Text="Waived" />
                                <asp:HiddenField ID="hfWaived" runat="server" Value="" />
                                <br />
                                <CKEditor:CKEditorControl ID="txtWaived" runat="server" Height="200px" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                <br />
                                <asp:CheckBox ID="chkCheque" runat="server" Text="Cheque" />
                                <asp:HiddenField ID="hfCheque" runat="server" Value="" />
                                <br />
                                <CKEditor:CKEditorControl ID="txtCheque" runat="server" Height="200px" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Allow Partial Payment?</label>
                            <div class="col-md-6">
                                <asp:RadioButton ID="rbPartial1" runat="server" GroupName="rbpartial" Text="No" />
                                <asp:RadioButton ID="rbPartial2" runat="server" GroupName="rbpartial" Text="Yes" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Mater Conference Selection Type</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlMasterConferenceType" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="Single" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Multiple" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Max Promo Usable Item Per Transaction for Promo Code Calculation</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtMaxItemPerTrans" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbMaxItemPerTrans" runat="server" TargetControlID="txtMaxItemPerTrans" FilterType="Numbers" ValidChars="0123456789" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Enable Visa Letter?</label>
                            <div class="col-md-6">
                                <asp:RadioButton ID="rbVisa1" runat="server" GroupName="rbvisa" Text="No" />
                                <asp:RadioButton ID="rbVisa2" runat="server" GroupName="rbvisa" Text="Yes" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Local Cheque / Bank Draft Text</label>
                            <div class="col-md-6">
                                <CKEditor:CKEditorControl ID="txtChequeText" runat="server" Height="200px" Width="98%" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Credit Card Text</label>
                            <div class="col-md-6">
                                <CKEditor:CKEditorControl ID="txtCreditText" runat="server" Height="200px" Width="98%" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2 control-label"></div>
                            <div class="col-md-6">
                                <asp:Button ID="btnSave" runat="server" Text="Next" OnClick="SaveForm" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>

    <script src="js/datetimepicker/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="js/datetimepicker/jquery.dynDateTime.min.js" type="text/javascript"></script>
    <script src="js/datetimepicker/calendar-en.min.js" type="text/javascript"></script>
    <link href="css/calendar-blue.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
            $("#<%=txtEarlyClosingDate.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%d/%m/%Y %H:%M",
                daFormat: "%l;%M %p, %e %m, %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
            $("#<%=txtRegClosingDate.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%d/%m/%Y %H:%M",
                daFormat: "%l;%M %p, %e %m, %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
    </script>
</asp:Content>
