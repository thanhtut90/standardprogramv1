﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using System.Text;
using Corpit.Site.Utilities;
using Corpit.Payment;
using System.IO;
using ClosedXML.Excel;
using Corpit.Site.Email;
using System.Text.RegularExpressions;
using Corpit.Logging;
using System.Globalization;

public partial class Admin_MasterRegistrationList_MDAVisitorBMReport : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    QuesFunctionality qfn = new QuesFunctionality();
    CommonFuns cFun = new CommonFuns();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (Session["userid"] != null)
                {
                    string userID = Session["userid"].ToString();

                    if (!string.IsNullOrEmpty(userID))
                    {
                        binddata();

                        if (Session["roleid"].ToString() != "1")
                        {
                            lblUser.Text = userID;
                            showlist.Visible = false;
                            string showID = "";
                            if (Session["roleid"].ToString() != "1")
                            {
                                showID = getShowID(userID);
                                ddl_showList.SelectedValue = showID;
                            }
                            else
                                showID = ddl_showList.SelectedValue;

                            GKeyMaster.ExportSettings.ExportOnlyData = true;
                            GKeyMaster.ExportSettings.IgnorePaging = true;
                            GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                            GKeyMaster.ExportSettings.FileName = ddl_showList.SelectedItem.Text.Replace(" ", "") + "VisitorBMReport" + DateTime.Now.ToString("ddMMyyyy");
                        }
                    }
                    else
                    {
                        Response.Redirect("Event_Config");
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }
    #region Bind Data
    protected string getShowID(string userID)
    {
        string showID = "";
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
            if(showID == "0")
            {
                showID = "";
            }
        }
        catch (Exception ex)
        { }

        return showID;
    }
    protected void binddata()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show where Status='Active'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_showList.Items.Clear();
                ddl_showList.DataSource = cmd.ExecuteReader();
                ddl_showList.DataTextField = "SHW_Name";
                ddl_showList.DataValueField = "SHW_ID";
                ddl_showList.DataBind();
                con.Close();
            }
        }
    }
    #endregion

    #region GKeyMaster
    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_RegDelegate and tb_Invoice tables for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            string showID = "";
            string userID = lblUser.Text.Trim();
            if (Session["roleid"].ToString() != "1")
                showID = getShowID(userID);
            else
                showID = ddl_showList.SelectedValue;

            DataTable dtReg = generateVisitorBMReport(showID, "");
            GKeyMaster.DataSource = dtReg;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string showID = "";
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                showID = getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID))
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                this.GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                this.GKeyMaster.ExportSettings.FileName = "VisitorBMReport" + DateTime.Now.ToString("ddMMyyyy");
                this.GKeyMaster.ExportSettings.ExportOnlyData = true;
                this.GKeyMaster.ExportSettings.IgnorePaging = true;
                this.GKeyMaster.ExportSettings.OpenInNewWindow = true;
                this.GKeyMaster.MasterTableView.ExportToExcel();
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
    #region GKeyMaster_PageIndexChanged
    protected void GKeyMaster_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        Session["pgno"] = GKeyMaster.CurrentPageIndex + 1;

        if (!string.IsNullOrEmpty(txtKey.Text) && !string.IsNullOrWhiteSpace(txtKey.Text))
        {
            btnKeysearch_Click(this, null);
        }
        else
        {
            if (string.IsNullOrEmpty(txtFromDate.Text.Trim()) || string.IsNullOrEmpty(txtToDate.Text.Trim()))
            {
                btnSearch_Click(this, null);
            }
            else
            {
                try
                {
                    string showID = "";
                    string userID = lblUser.Text.Trim();
                    if (Session["roleid"].ToString() != "1")
                        showID = getShowID(userID);
                    else
                        showID = ddl_showList.SelectedValue;

                    DataTable dtReg = generateVisitorBMReport(showID, "");
                    GKeyMaster.DataSource = dtReg;
                    GKeyMaster.DataBind();
                }
                catch (Exception ex)
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
    }
    #endregion
    #endregion

    #region lnkExcel_Clicked
    protected void lnkExcel_Clicked(object sender, EventArgs e)
    {
        try
        {
            string showID = "";
            string userID = lblUser.Text.Trim();
            if (Session["roleid"].ToString() != "1")
            {
                showID = getShowID(userID);
                ddl_showList.SelectedValue = showID;
            }
            else
                showID = ddl_showList.SelectedValue;

            this.GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
            this.GKeyMaster.ExportSettings.FileName = ddl_showList.SelectedItem.Text.Replace(" ", "") + "VisitorBMReport" + DateTime.Now.ToString("ddMMyyyy");
            this.GKeyMaster.ExportSettings.ExportOnlyData = true;
            this.GKeyMaster.ExportSettings.IgnorePaging = true;
            this.GKeyMaster.ExportSettings.OpenInNewWindow = true;
            this.GKeyMaster.MasterTableView.ExportToExcel();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }
    }
    #endregion

    #region By Key Word
    protected void btnKeysearch_Click(object sender, EventArgs e)
    {
        string showID = "";
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                showID = getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        string searchkey = "";
        if (!string.IsNullOrEmpty(txtKey.Text) && !string.IsNullOrWhiteSpace(txtKey.Text))
        {
            string key = txtKey.Text.Trim();
            if (Regex.IsMatch(key, @"^\d+$"))
            {
                searchkey = "Regno='" + key + "'";
            }
            else
            {
                string countryIDs = getCountriesByName(key);
                searchkey = "reg_FName like '%" + key + "%' Or reg_LName like '%" + key + "%' Or reg_Email like '%" + key + "%'"
                                //+ " Or reg_Address1 like '%" + key + "%'"
                                //+ " Or reg_Designation like '%" + key + "%'"
                                + " Or reg_Country In (" + countryIDs + ")";
            }
        }

        DataTable dtReg = generateVisitorBMReport(showID, searchkey);
        GKeyMaster.DataSource = dtReg;
        GKeyMaster.DataBind();
    }
    private string getCountriesByName(string countryName)
    {
        string result = string.Empty;
        try
        {
            string sql = "SELECT TOP 1 Substring ((SELECT ', ' + Convert(nvarchar,Cty_GUID) + '' FROM ref_country"
                        + " WHERE Country Like '%" + countryName + "%' And Cty_GUID = dbo.ref_country.Cty_GUID FOR XML PATH('')), 2, 200000) AS countryIDs";
            result = fn.GetDataByCommand(sql, "countryIDs");
            if (string.IsNullOrEmpty(result))
            {
                result = "0";
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    #endregion

    #region By Date
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string showID = "";
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                showID = getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        string searchkey = "";
        if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()) && !string.IsNullOrEmpty(txtToDate.Text.Trim()))
        {
            searchkey = checkDateForExcel();
        }

        DataTable dtReg = generateVisitorBMReport(showID, searchkey);
        GKeyMaster.DataSource = dtReg;
        GKeyMaster.DataBind();
    }
    public string checkDateForExcel()
    {
        string start = txtFromDate.Text.Trim();
        string end = txtToDate.Text.Trim();
        string res = string.Empty;
        if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
        {
            res = "and reg_datecreated  BETWEEN '" + start + "' and '" + end + "'";
        }
        else
        {
            res = "";
        }
        return res;
    }
    #endregion

    #region generateVisitorBMReport
    private static string _checkWireTubeShowName = "Wire and Tube 2019";
    private static string _WireQuestID = "QSTN2077";
    private static string _TubeQuestID = "QSTN2078";
    private static string _checkPPIShowName = "PPI 2019";
    private static string _PrintingQuestID = "QSTN2067";
    private static string _PackagingQuestID = "QSTN2068";
    private static string _checkMFTShowName = "MFT 2019";
    private static string _AreaSpecialityMFTQuestID = "QSTN2051";
    public DataTable generateVisitorBMReport(string showid, string search = "")
    {
        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(showid))
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            try
            {
                string sql = "Select Regno 'RegID',reg_OName 'Name on Badge',reg_Designation 'Job Title',reg_Address1 'Company Name',reg_Address3 'Address 1'"
                            + " ,RegCountry 'Country',reg_Email 'Email' "
                            //+ " ,Case When DepartmentName Like '%Others%' Or DepartmentName Like '%Other%' Then reg_otherDepartment Else DepartmentName End 'Nature of business' "
                            + " ,Case When d.Desc2 Like '%Others%' Or d.Desc2 Like '%Other%' Then reg_otherDepartment Else d.Desc2 End 'Nature of business' "
                            + " From GetRegIndivAllByShowID('" + showid + "') as r Left Join ref_Department as d On r.reg_Department=d.ID "
                            + " Where reg_Status=1 And d.ShowID='" + showid + "' Order By reg_datecreated";
                DataTable dtRegList = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                #region filter
                if (!string.IsNullOrEmpty(search))
                {
                    DataView dv = new DataView();
                    dv = dtRegList.DefaultView;
                    dv.RowFilter = search;
                    dtRegList = dv.ToTable();
                }
                #endregion
                if (shw.SHW_Name != _checkWireTubeShowName && shw.SHW_Name != _checkPPIShowName)
                {
                    /*added on 5-8-2019*/
                    if (shw.SHW_Name == _checkMFTShowName)
                    {
                        dtRegList.Columns.Add("Area of Specialty");
                    }
                    /*added on 5-8-2019*/
                    dtRegList.Columns.Add("Product Interest");
                }
                else
                {
                    if (shw.SHW_Name == _checkWireTubeShowName)
                    {
                        dtRegList.Columns.Add("Product Interest (Wire)");
                        dtRegList.Columns.Add("Product Interest (Tube)");
                    }
                    else if (shw.SHW_Name == _checkPPIShowName)
                    {
                        dtRegList.Columns.Add("Product Interest (Printing)");
                        dtRegList.Columns.Add("Product Interest (Packaging)");
                    }
                }
                if (dtRegList.Rows.Count > 0)
                {
                    string qnaireID = GetQIDFromShowID(showid);
                    int rowcount = 0;
                    foreach (DataRow dr in dtRegList.Rows)
                    {
                        #region bind Product Interest/ Area of Speciality
                        if (shw.SHW_Name != _checkWireTubeShowName && shw.SHW_Name != _checkPPIShowName)
                        {
                            /*added on 5-8-2019*/
                            if (shw.SHW_Name == _checkMFTShowName)
                            {
                                dr["Area of Specialty"] = getSelectedItemsByQuestID(dr["RegID"].ToString(), qnaireID, _AreaSpecialityMFTQuestID);
                            }
                            /*added on 5-8-2019*/
                            dr["Product Interest"] = getSelectedProductInterest(dr["RegID"].ToString(), qnaireID);
                        }
                        else
                        {
                            if (shw.SHW_Name == _checkWireTubeShowName)
                            {
                                dr["Product Interest (Wire)"] = getSelectedProductInterestByQuestID(dr["RegID"].ToString(), qnaireID, _WireQuestID);
                                dr["Product Interest (Tube)"] = getSelectedProductInterestByQuestID(dr["RegID"].ToString(), qnaireID, _TubeQuestID);
                            }
                            else if (shw.SHW_Name == _checkPPIShowName)
                            {
                                dr["Product Interest (Printing)"] = getSelectedProductInterestByQuestID(dr["RegID"].ToString(), qnaireID, _PrintingQuestID);
                                dr["Product Interest (Packaging)"] = getSelectedProductInterestByQuestID(dr["RegID"].ToString(), qnaireID, _PackagingQuestID);
                            }
                        }
                        #endregion
                        rowcount++;
                    }
                }

                dt = dtRegList;//***
            }
            catch (Exception ex)
            { }
        }
        return dt;
    }
    public string GetQIDFromShowID(string showid, string category = null)
    {
        string result = string.Empty;

        if (!string.IsNullOrEmpty(showid))
        {
            if (string.IsNullOrEmpty(showid))
            {
                category = "D";
            }
            string query = "Select Distinct SQ_QAID From tb_site_QA_Master Where SQ_SHWID='" + showid + "' And SQ_FLW_ID In "
                            + " (Select tb_site_flow_master.FLW_ID From tb_site_flow_master Where ShowID='" + showid + "' And Status='Active')";
            result = fn.GetDataByCommand(query, "SQ_QAID");
        }
        return result;
    }
    #region getSelectedProductInterest
    private static string _checkQuestionProductInterestMDA = "Product Interest";
    public string getSelectedProductInterest(string regno, string qnaireID)
    {
        string result = string.Empty;
        try
        {
            if ((!string.IsNullOrEmpty(regno) && !string.IsNullOrWhiteSpace(regno))
                && (!string.IsNullOrEmpty(qnaireID) && !string.IsNullOrWhiteSpace(qnaireID)))
            {
                string sql = "Select Top 1 Substring((Select  ';' + qitem_desc + '' From ("
                                + " Select Case When qi.qitem_desc2 Like '%Others%' Or qi.qitem_desc2 Like '%Other%' Then qr.qitem_input_txt Else qi.qitem_desc2 End qitem_desc,qi.qitem_id From gen_QnaireLog as ql Inner Join gen_QnaireResult as qr "
                                + " On ql.qnaire_log_id=qr.qnaire_log_id "
                                + " Inner Join gen_QuestItem as qi On qr.qitem_id=qi.qitem_id "
                                + " Where ql.qnaire_id='" + qnaireID + "' And ql.status='Active' "
                                + " And qr.status='Active' And qi.status='Active' "
                                + " And (qi.qitem_desc2 Not Like '%Others%' And qi.qitem_desc2 Not Like '%Other%') "
                                + " And qr.quest_id In (Select quest_id From gen_Question Where quest_id In "
                                + " (Select quest_id From gen_QnaireQuest Where qnaire_id='" + qnaireID + "' And status='Active') "
                                + " And quest_desc Like '%" + _checkQuestionProductInterestMDA  + "%' And status='Active') "
                                + " And ql.user_reference='" + regno + "'"
                                + " ) t1 For XML PATH('')),2,200000) as ProductInterests";
                result = StripHTML(HttpUtility.HtmlDecode(qfn.GetDataByCommand(sql, "ProductInterests")).Replace("&nbsp;","").Replace("<br/>"," "));
                if (result == "0")
                {
                    result = "";
                }
                else
                {
                    result = result.TrimStart(';');
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getSelectedProductInterestByQuestID(string regno, string qnaireID, string questid)
    {
        string result = string.Empty;
        try
        {
            if ((!string.IsNullOrEmpty(regno) && !string.IsNullOrWhiteSpace(regno))
                && (!string.IsNullOrEmpty(qnaireID) && !string.IsNullOrWhiteSpace(qnaireID)))
            {
                string sql = "Select Top 1 Substring((Select  ';' + qitem_desc + '' From ("
                                + " Select Case When qi.qitem_desc2 Like '%Others%' Or qi.qitem_desc2 Like '%Other%' Then qr.qitem_input_txt Else qi.qitem_desc2 End qitem_desc,qi.qitem_id From gen_QnaireLog as ql Inner Join gen_QnaireResult as qr "
                                + " On ql.qnaire_log_id=qr.qnaire_log_id "
                                + " Inner Join gen_QuestItem as qi On qr.qitem_id=qi.qitem_id "
                                + " Where ql.qnaire_id='" + qnaireID + "' And ql.status='Active' "
                                + " And qr.status='Active' And qi.status='Active' "
                                + " And (qi.qitem_desc2 Not Like '%Others%' And qi.qitem_desc2 Not Like '%Other%') "
                                + " And qr.quest_id In ('" + questid + "') "
                                + " And qr.quest_id In (Select quest_id From gen_Question Where quest_id In "
                                + " (Select quest_id From gen_QnaireQuest Where qnaire_id='" + qnaireID + "' And status='Active') "
                                + " And quest_desc Like '%" + _checkQuestionProductInterestMDA + "%' And status='Active') "
                                + " And ql.user_reference='" + regno + "'"
                                + " ) t1 For XML PATH('')),2,200000) as ProductInterests";
                result = StripHTML(HttpUtility.HtmlDecode(qfn.GetDataByCommand(sql, "ProductInterests")).Replace("&nbsp;", "").Replace("<br/>", " "));
                if (result == "0")
                {
                    result = "";
                }
                else
                {
                    result = result.TrimStart(';');
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getSelectedItemsByQuestID(string regno, string qnaireID, string questid)
    {
        string result = string.Empty;
        try
        {
            if ((!string.IsNullOrEmpty(regno) && !string.IsNullOrWhiteSpace(regno))
                && (!string.IsNullOrEmpty(qnaireID) && !string.IsNullOrWhiteSpace(qnaireID)))
            {
                string sql = "Select Top 1 Substring((Select  ';' + qitem_desc + '' From ("
                                + " Select Case When qi.qitem_desc2 Like '%Others%' Or qi.qitem_desc2 Like '%Other%' Then qr.qitem_input_txt Else qi.qitem_desc2 End qitem_desc,qi.qitem_id From gen_QnaireLog as ql Inner Join gen_QnaireResult as qr "
                                + " On ql.qnaire_log_id=qr.qnaire_log_id "
                                + " Inner Join gen_QuestItem as qi On qr.qitem_id=qi.qitem_id "
                                + " Where ql.qnaire_id='" + qnaireID + "' And ql.status='Active' "
                                + " And qr.status='Active' And qi.status='Active' "
                                + " And (qi.qitem_desc2 Not Like '%Others%' And qi.qitem_desc2 Not Like '%Other%') "
                                + " And qr.quest_id In ('" + questid + "') "
                                //+ " And qr.quest_id In (Select quest_id From gen_Question Where quest_id In "
                                //+ " (Select quest_id From gen_QnaireQuest Where qnaire_id='" + qnaireID + "' And status='Active') "
                                //+ " And quest_desc Like '%" + _checkQuestionProductInterestMDA + "%' And status='Active') "
                                + " And ql.user_reference='" + regno + "'"
                                + " ) t1 For XML PATH('')),2,200000) as ProductInterests";
                result = StripHTML(HttpUtility.HtmlDecode(qfn.GetDataByCommand(sql, "ProductInterests")).Replace("&nbsp;", "").Replace("<br/>", " "));
                if (result == "0")
                {
                    result = "";
                }
                else
                {
                    result = result.TrimStart(';');
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public static string StripHTML(string input)
    {
        return Regex.Replace(input, "<.*?>", String.Empty);
    }
    #endregion
    #endregion
}
