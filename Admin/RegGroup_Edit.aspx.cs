﻿using Corpit.BackendMaster;
using Corpit.Registration;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegGroup_Edit : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _Designation = "Designation";
    static string _Department = "Department";
    static string _Company = "Company";
    static string _Industry = "Industry";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _VisitDate = "Visit Date";
    static string _VisitTime = "Visit Time";
    static string _Password = "Password";
    static string _OtherSalutation = "Other Salutation";
    static string _OtherDesignation = "Other Designation";
    static string _OtherIndustry = "Other Industry";

    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Age = "Age";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                if (Request.Params["groupid"] != null)
                {
                    string groupid = cFun.DecryptValue(Request.QueryString["groupid"]);

                    if (!string.IsNullOrEmpty(flowid) && flowid != "0")
                    {
                        if(!setDynamicForm(flowid, showid))
                        {
                            Response.Redirect("MasterRegistrationList_Group.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("MasterRegistrationList_Group.aspx");
                    }
                }
                else
                {
                    Response.Redirect("MasterRegistrationList_Group.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                if (Request.Params["groupid"] != null)
                {
                    string groupid = cFun.DecryptValue(Request.QueryString["groupid"]);

                    RegGroupObj rgg = new RegGroupObj(fn);
                    DataTable dt = rgg.getRegGroupByID(groupid, cFun.DecryptValue(showid));

                    if (dt.Rows.Count > 0)
                    {
                        bindDropdown(showid);
                        populateUserDetails(dt);
                    }
                    else
                    {
                        Response.Redirect("MasterRegistrationList_Group.aspx");
                    }
                }
                else
                {
                    Response.Redirect("MasterRegistrationList_Group.aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region populateUserDetails
    private void populateUserDetails(DataTable dtReg)
    {
        if (dtReg.Rows.Count > 0)
        {
            hfRegGroupID.Value = dtReg.Rows[0]["RegGroupID"].ToString();
            hfStage.Value = dtReg.Rows[0]["RG_Stage"].ToString();

            string rg_ismultiple = dtReg.Rows[0]["RG_IsMultiple"].ToString();
            hfIsMultiple.Value = rg_ismultiple;
            string rg_referralcode = dtReg.Rows[0]["RG_ReferralCode"].ToString();
            hfReferralCode.Value = rg_referralcode;

            string reggroupid = dtReg.Rows[0]["RegGroupID"].ToString();
            string rg_salutation = dtReg.Rows[0]["RG_Salutation"].ToString();
            string rg_contactfname = dtReg.Rows[0]["RG_ContactFName"].ToString();
            string rg_contactlname = dtReg.Rows[0]["RG_ContactLName"].ToString();
            string rg_designation = dtReg.Rows[0]["RG_Designation"].ToString();
            string rg_department = dtReg.Rows[0]["RG_Department"].ToString();
            string rg_company = dtReg.Rows[0]["RG_Company"].ToString();
            string rg_industry = dtReg.Rows[0]["RG_Industry"].ToString();
            string rg_address1 = dtReg.Rows[0]["RG_Address1"].ToString();
            string rg_address2 = dtReg.Rows[0]["RG_Address2"].ToString();
            string rg_address3 = dtReg.Rows[0]["RG_Address3"].ToString();
            string rg_city = dtReg.Rows[0]["RG_City"].ToString();
            string rg_stateprovince = dtReg.Rows[0]["RG_StateProvince"].ToString();
            string rg_postalcode = dtReg.Rows[0]["RG_PostalCode"].ToString();
            string rg_country = dtReg.Rows[0]["RG_Country"].ToString();
            string rg_rcountry = dtReg.Rows[0]["RG_RCountry"].ToString();
            string rg_telcc = dtReg.Rows[0]["RG_Telcc"].ToString();
            string rg_telac = dtReg.Rows[0]["RG_Telac"].ToString();
            string rg_tel = dtReg.Rows[0]["RG_Tel"].ToString();
            string rg_mobilecc = dtReg.Rows[0]["RG_Mobilecc"].ToString();
            string rg_mobileac = dtReg.Rows[0]["RG_Mobileac"].ToString();
            string rg_mobile = dtReg.Rows[0]["RG_Mobile"].ToString();
            string rg_faxcc = dtReg.Rows[0]["RG_Faxcc"].ToString();
            string rg_faxac = dtReg.Rows[0]["RG_Faxac"].ToString();
            string rg_fax = dtReg.Rows[0]["RG_Fax"].ToString();
            string rg_contactemail = dtReg.Rows[0]["RG_ContactEmail"].ToString();
            string rg_remark = dtReg.Rows[0]["RG_Remark"].ToString();
            string rg_type = dtReg.Rows[0]["RG_Type"].ToString();
            string rg_remarkgupload = dtReg.Rows[0]["RG_RemarkGUpload"].ToString();
            string rg_salother = dtReg.Rows[0]["RG_SalOther"].ToString();
            string rg_designationother = dtReg.Rows[0]["RG_DesignationOther"].ToString();
            string rg_industryothers = dtReg.Rows[0]["RG_IndustryOthers"].ToString();
            string rg_visitdate = dtReg.Rows[0]["RG_VisitDate"].ToString();
            string rg_visittime = dtReg.Rows[0]["RG_VisitTime"].ToString();
            string rg_password = dtReg.Rows[0]["RG_Password"].ToString();
            string rg_isfromsales = dtReg.Rows[0]["RG_IsFromSales"].ToString();
            string rg_issendemail = dtReg.Rows[0]["RG_IsSendEmail"].ToString();
            string rg_indsendemail_status = dtReg.Rows[0]["RG_IndSendEmail_Status"].ToString();
            string rg_createddate = dtReg.Rows[0]["RG_CreatedDate"].ToString();
            string recycle = dtReg.Rows[0]["recycle"].ToString();
            string rg_stage = dtReg.Rows[0]["RG_Stage"].ToString();

            string rg_age = dtReg.Rows[0]["RG_Age"].ToString();
            string rg_dob = dtReg.Rows[0]["RG_DOB"].ToString();
            string rg_gender = dtReg.Rows[0]["RG_Gender"].ToString();
            string rg_additional4 = dtReg.Rows[0]["RG_Additional4"].ToString();
            string rg_additional5 = dtReg.Rows[0]["RG_Additional5"].ToString();

            try
            {
                if (!String.IsNullOrEmpty(rg_salutation))
                {
                    ListItem listItem = ddlSalutation.Items.FindByValue(rg_salutation);
                    if (listItem != null)
                    {
                        ddlSalutation.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtSalOther.Text = rg_salother;
            txtFName.Text = rg_contactfname;
            txtLName.Text = rg_contactlname;
            txtDesignation.Text = rg_designation;
            txtDepartment.Text = rg_department;
            txtCompany.Text = rg_company;

            try
            {
                if (!String.IsNullOrEmpty(rg_country))
                {
                    ListItem listItem = ddlCountry.Items.FindByValue(rg_country);
                    if (listItem != null)
                    {
                        ddlCountry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            try
            {
                if (!String.IsNullOrEmpty(rg_rcountry))
                {
                    ListItem listItem = ddlRCountry.Items.FindByValue(rg_rcountry);
                    if (listItem != null)
                    {
                        ddlRCountry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtAddress1.Text = rg_address1;
            txtAddress2.Text = rg_address2;
            txtAddress3.Text = rg_address3;
            txtCity.Text = rg_city;
            txtState.Text = rg_stateprovince;
            txtPostalcode.Text = rg_postalcode;
            try
            {
                if (!String.IsNullOrEmpty(rg_industry))
                {
                    ListItem listItem = ddlIndustry.Items.FindByValue(rg_industry);
                    if (listItem != null)
                    {
                        ddlIndustry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtIndusOther.Text = rg_industryothers;
            txtTelcc.Text = rg_telcc;
            txtTelac.Text = rg_telac;
            txtTel.Text = rg_tel;
            txtMobcc.Text = rg_mobilecc;
            txtMobac.Text = rg_mobileac;
            txtMobile.Text = rg_mobile;
            txtFaxcc.Text = rg_faxcc;
            txtFaxac.Text = rg_faxac;
            txtFax.Text = rg_fax;
            txtEmail.Text = rg_contactemail;

            txtVisitDate.Text = rg_visitdate;
            txtVisitTime.Text = rg_visittime;
            txtPassword.Text = rg_password;

            txtAge.Text = rg_age;
            txtDOB.Text = !string.IsNullOrEmpty(rg_dob) ? Convert.ToDateTime(rg_dob).ToString("dd/MM/yyyy") : "";
            try
            {
                if (!String.IsNullOrEmpty(rg_gender))
                {
                    ListItem listItem = ddlGender.Items.FindByValue(rg_gender);
                    if (listItem != null)
                    {
                        ddlGender.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtAdditional4.Text = rg_additional4;
            txtAdditional5.Text = rg_additional5;
        }
    }
    #endregion

    #region btnupdate_Click (Update the respective data into "tb_RegGroup" table)
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        lbls.Text = "";
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
            if (!string.IsNullOrEmpty(hfRegGroupID.Value))
            {
                UpdateDataGroup(showid);
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region UpdateDataGroup & Load update the related data into tb_RegGroup table
    private void UpdateDataGroup(string showid)
    {
        if (Page.IsValid)
        {
            if (!cFun.validatePhoneCode(txtTelcc.Text.ToString())
            //|| !cFun.validatePhoneCode(txtTelac.Text.ToString())
            || !cFun.validatePhoneCode(txtMobcc.Text.ToString())
            //|| !cFun.validatePhoneCode(txtMobac.Text.ToString())
            || !cFun.validatePhoneCode(txtFaxcc.Text.ToString())
            //|| !cFun.validatePhoneCode(txtFaxac.Text.ToString())
            )
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Phone code is not valid.');", true);
                return;
            }

            RegGroupObj rgg = new RegGroupObj(fn);

            string groupid = string.Empty;
            string salutation = string.Empty;
            string fname = string.Empty;
            string lname = string.Empty;
            string designation = string.Empty;
            string otherdesignation = string.Empty;//*
            string department = string.Empty;
            string company = string.Empty;
            string industry = string.Empty;
            string address1 = string.Empty;
            string address2 = string.Empty;
            string address3 = string.Empty;
            string city = string.Empty;
            string state = string.Empty;
            string postalcode = string.Empty;
            string country = string.Empty;
            string rcountry = string.Empty;
            string telcc = string.Empty;
            string telac = string.Empty;
            string tel = string.Empty;
            string mobilecc = string.Empty;
            string mobileac = string.Empty;
            string mobile = string.Empty;
            string faxcc = string.Empty;
            string faxac = string.Empty;
            string fax = string.Empty;
            string email = string.Empty;
            string remark = string.Empty;
            string Type_NHG_NonNHG = string.Empty;
            string sal_other = string.Empty;
            string desig_other = string.Empty;
            string indus_other = string.Empty;
            string visitdate = string.Empty;
            string visittime = string.Empty;
            string password = string.Empty;
            string ismul = cFun.solveSQL(hfIsMultiple.Value);
            string reffalcode = cFun.solveSQL(hfReferralCode.Value);
            int isFromsale = 0;
            int isSendEmail = 0;
            int isIndivSendEmail = 0;
            //string createdate = "getdate()";
            string createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            int recycle = 0;
            string stage = string.Empty;

            int age = 0;
            DateTime? dob = null;
            string dob_str = string.Empty;
            string gender = string.Empty;
            string additional4 = string.Empty;
            string additional5 = string.Empty;

            groupid = hfRegGroupID.Value;

            salutation = cFun.solveSQL(ddlSalutation.SelectedItem.Value.Trim());
            fname = cFun.solveSQL(txtFName.Text.Trim());
            lname = cFun.solveSQL(txtLName.Text.Trim());
            designation = cFun.solveSQL(txtDesignation.Text.Trim());
            otherdesignation = cFun.solveSQL(txtOtherDesignation.Text.Trim());//*
            department = cFun.solveSQL(txtDepartment.Text.Trim());
            company = cFun.solveSQL(txtCompany.Text.Trim());
            address1 = cFun.solveSQL(txtAddress1.Text.Trim());
            address2 = cFun.solveSQL(txtAddress2.Text.Trim());
            address3 = cFun.solveSQL(txtAddress3.Text.Trim());
            city = cFun.solveSQL(txtCity.Text.Trim());
            postalcode = cFun.solveSQL(txtPostalcode.Text.Trim());
            state = cFun.solveSQL(txtState.Text.Trim());
            country = ddlCountry.SelectedItem.Value.ToString();
            rcountry = ddlRCountry.SelectedItem.Value.ToString();
            telcc = txtTelcc.Text.ToString();
            telac = txtTelac.Text.ToString();
            tel = txtTel.Text.ToString();
            mobilecc = txtMobcc.Text.ToString();
            mobileac = txtMobac.Text.ToString();
            mobile = txtMobile.Text.ToString();
            faxcc = txtFaxcc.Text.ToString();
            faxac = txtFaxac.Text.ToString();
            fax = txtFax.Text.ToString();
            email = cFun.solveSQL(txtEmail.Text.Trim());

            if ((ddlSalutation.SelectedIndex == ddlSalutation.Items.Count - 1) && !String.IsNullOrEmpty(txtSalOther.Text))
            {
                sal_other = cFun.solveSQL(txtSalOther.Text.ToString());
            }

            industry = ddlIndustry.SelectedItem.Value.ToString();
            if ((ddlIndustry.SelectedIndex == ddlIndustry.Items.Count - 1) && !String.IsNullOrEmpty(txtIndusOther.Text))
            {
                indus_other = cFun.solveSQL(txtIndusOther.Text.ToString());
            }
            visitdate = cFun.solveSQL(txtVisitDate.Text.Trim());
            visittime = cFun.solveSQL(txtVisitTime.Text.Trim());
            password = cFun.solveSQL(txtPassword.Text.Trim());

            if (!String.IsNullOrWhiteSpace(txtAge.Text))
            {
                age = Convert.ToInt32(cFun.solveSQL(txtAge.Text.Trim()));
            }
            if (!String.IsNullOrWhiteSpace(txtDOB.Text))
            {
                if (!cFun.validateDate(txtDOB.Text.Trim()))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                    return;
                }

                dob = DateTime.ParseExact(txtDOB.Text.Trim(), "dd/MM/yyyy", null);
                dob_str = dob.Value.ToString("yyyy-MM-dd hh:mm:ss");
            }
            gender = cFun.solveSQL(ddlGender.SelectedItem.Value.Trim());
            additional4 = cFun.solveSQL(txtAdditional4.Text.Trim());
            additional5 = cFun.solveSQL(txtAdditional5.Text.Trim());

            stage = cFun.solveSQL(hfStage.Value);

            rgg.groupid = groupid;
            rgg.salutation = salutation;
            rgg.fname = fname;
            rgg.lname = lname;
            rgg.designation = designation;
            rgg.desig_other = otherdesignation;//*
            rgg.department = department;
            rgg.company = company;
            rgg.industry = industry;
            rgg.address1 = address1;
            rgg.address2 = address2;
            rgg.address3 = address3;
            rgg.city = city;
            rgg.state = state;
            rgg.postalcode = postalcode;
            rgg.country = country;
            rgg.rcountry = rcountry;
            rgg.telcc = telcc;
            rgg.telac = telac;
            rgg.tel = tel;
            rgg.mobilecc = mobilecc;
            rgg.mobileac = mobileac;
            rgg.mobile = mobile;
            rgg.faxcc = faxcc;
            rgg.faxac = faxac;
            rgg.fax = fax;
            rgg.email = email;
            rgg.remark = remark;
            rgg.Type_NHG_NonNHG = Type_NHG_NonNHG;
            rgg.sal_other = sal_other;
            rgg.desig_other = desig_other;
            rgg.indus_other = indus_other;
            rgg.visitdate = visitdate;
            rgg.visittime = visittime;
            rgg.password = password;
            rgg.ismul = ismul;
            rgg.reffalcode = reffalcode;
            rgg.isFromsale = isFromsale;
            rgg.isSendEmail = isSendEmail;
            rgg.isIndivSendEmail = isIndivSendEmail;
            rgg.createdate = createdate;
            rgg.recycle = recycle;
            rgg.stage = stage;

            rgg.age = age;
            rgg.dob = dob_str;
            rgg.gender = gender;
            rgg.additional4 = additional4;
            rgg.additional5 = additional5;

            rgg.showID = showid;

            //*Update
            /*int isSuccess = 0;
            bool isAlreadyExist = rgg.checkUpdateExist();
            if (isAlreadyExist == true)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This user already exists!');", true);
                return;
            }
            else
            {
                isSuccess = rgg.updateRegGroup();
            }*/

            int isSuccess = 0;
            isSuccess = rgg.updateRegGroup();

            if (isSuccess > 0)
            {
                ClearForm();
                lbls.Text = "Success";

                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success.');window.location='MasterRegistrationList_Group.aspx';", true);
                return;
            }
            else
            {
                lbls.Text = "Something is wrong.";

                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fail.');window.location='MasterRegistrationList_Group.aspx';", true);
                return;
            }
        }
    }
    #endregion

    #region PanelKeyDetial (form controls portion) & set dynamic form controls visibility or invisibility and so on

    #region setDynamicForm (Generate Dynamic Form and use tb_Form)
    protected bool setDynamicForm(string flowid, string showid)
    {
        bool isValidShow = false;

        DataSet ds = new DataSet();
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForGroup();

        string formtype = FormType.TypeGroup;

        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
        {
            isValidShow = true;

            #region set divSalutation visibility is true or false if form_input_name is Salutation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                if (isshow == 1)
                {
                    divSalutation.Visible = true;
                }
                else
                {
                    divSalutation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Salutation, formtype);
                if (isrequired == 1)
                {
                    lblSalutation.Text = labelname + "<span class=\"red\">*</span>";
                    //ddlSalutation.Attributes.Add("required", "");
                    vcSal.Enabled = true;
                }
                else
                {
                    lblSalutation.Text = labelname;
                    //ddlSalutation.Attributes.Remove("required");
                    vcSal.Enabled = false;
                }
            }
            #endregion

            #region set divFName visibility is true or false if form_input_name is FName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFName.Visible = true;
                }
                else
                {
                    divFName.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Fname, formtype);
                if (isrequired == 1)
                {
                    lblFName.Text = labelname + "<span class=\"red\">*</span>";
                    //txtFName.Attributes.Add("required", "");
                    vcFName.Enabled = true;
                }
                else
                {
                    lblFName.Text = labelname;
                    //txtFName.Attributes.Remove("required");
                    vcFName.Enabled = false;
                }
            }
            #endregion

            #region set divLName visibility is true or false if form_input_name is LName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divLName.Visible = true;
                }
                else
                {
                    divLName.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Lname, formtype);
                if (isrequired == 1)
                {
                    lblLName.Text = labelname + "<span class=\"red\">*</span>";
                    //txtLName.Attributes.Add("required", "");
                    vcLName.Enabled = true;
                }
                else
                {
                    lblLName.Text = labelname;
                    //txtLName.Attributes.Remove("required");
                    vcLName.Enabled = false;
                }
            }
            #endregion

            #region set divDesignation visibility is true or false if form_input_name is Designation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDesignation.Visible = true;
                }
                else
                {
                    divDesignation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Designation, formtype);
                if (isrequired == 1)
                {
                    lblDesignation.Text = labelname + "<span class=\"red\">*</span>";
                    //txtDesignation.Attributes.Add("required", "");
                    vcDesig.Enabled = true;
                }
                else
                {
                    lblDesignation.Text = labelname;
                    //txtDesignation.Attributes.Remove("required");
                    vcDesig.Enabled = false;
                }
            }
            #endregion

            #region set divOtherDesignation visibility is true or false if form_input_name is Other Designation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OtherDesignation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divOtherDesignation.Visible = true;
                }
                else
                {
                    divOtherDesignation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_OtherDesignation, formtype);
                if (isrequired == 1)
                {
                    lblOtherDesignation.Text = labelname + "<span class=\"red\">*</span>";
                    //txtDesignation.Attributes.Add("required", "");
                    vcOtherDesignation.Enabled = true;
                }
                else
                {
                    lblOtherDesignation.Text = labelname;
                    //txtDesignation.Attributes.Remove("required");
                    vcOtherDesignation.Enabled = false;
                }
            }
            #endregion

            #region set divDepartment visibility is true or false if form_input_name is Department according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDepartment.Visible = true;
                }
                else
                {
                    divDepartment.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Department, formtype);
                if (isrequired == 1)
                {
                    lblDepartment.Text = labelname + "<span class=\"red\">*</span>";
                    //txtDepartment.Attributes.Add("required", "");
                    vcDeptm.Enabled = true;
                }
                else
                {
                    lblDepartment.Text = labelname;
                    //txtDepartment.Attributes.Remove("required");
                    vcDeptm.Enabled = false;
                }
            }
            #endregion

            #region set divCompany visibility is true or false if form_input_name is Company according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Company)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCompany.Visible = true;
                }
                else
                {
                    divCompany.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Company, formtype);
                if (isrequired == 1)
                {
                    lblCompany.Text = labelname + "<span class=\"red\">*</span>";
                    //txtCompany.Attributes.Add("required", "");
                    vcCom.Enabled = true;
                }
                else
                {
                    lblCompany.Text = labelname;
                    //txtCompany.Attributes.Remove("required");
                    vcCom.Enabled = false;
                }
            }
            #endregion

            #region set divIndustry visibility is true or false if form_input_name is Industry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Industry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divIndustry.Visible = true;
                }
                else
                {
                    divIndustry.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Industry, formtype);
                if (isrequired == 1)
                {
                    lblIndustry.Text = labelname + "<span class=\"red\">*</span>";
                    //ddlIndustry.Attributes.Add("required", "");
                    vcIndus.Enabled = true;
                }
                else
                {
                    lblIndustry.Text = labelname;
                    //ddlIndustry.Attributes.Remove("required");
                    vcIndus.Enabled = false;
                }
            }
            #endregion

            #region set divAddress1 visibility is true or false if form_input_name is Address1 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress1.Visible = true;
                }
                else
                {
                    divAddress1.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Address1, formtype);
                if (isrequired == 1)
                {
                    lblAddress1.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAddress1.Attributes.Add("required", "");
                    vcAdd1.Enabled = true;
                }
                else
                {
                    lblAddress1.Text = labelname;
                    //txtAddress1.Attributes.Remove("required");
                    vcAdd1.Enabled = false;
                }
            }
            #endregion

            #region set divAddress2 visibility is true or false if form_input_name is Address2 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress2.Visible = true;
                }
                else
                {
                    divAddress2.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : "";// frmObj.getDefaultLableNameByInputNameType(_Address2, formtype);
                if (isrequired == 1)
                {
                    lblAddress2.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAddress2.Attributes.Add("required", "");
                    vcAdd2.Enabled = true;
                }
                else
                {
                    lblAddress2.Text = labelname;
                    //txtAddress2.Attributes.Remove("required");
                    vcAdd2.Enabled = false;
                }
            }
            #endregion

            #region set divAddress3 visibility is true or false if form_input_name is Address3 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress3.Visible = true;
                }
                else
                {
                    divAddress3.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : "";// frmObj.getDefaultLableNameByInputNameType(_Address3, formtype);
                if (isrequired == 1)
                {
                    lblAddress3.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAddress3.Attributes.Add("required", "");
                    vcAdd3.Enabled = true;
                }
                else
                {
                    lblAddress3.Text = labelname;
                    //txtAddress3.Attributes.Remove("required");
                    vcAdd3.Enabled = false;
                }
            }
            #endregion

            #region set divCity visibility is true or false if form_input_name is City according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCity.Visible = true;
                }
                else
                {
                    divCity.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_City, formtype);
                if (isrequired == 1)
                {
                    lblCity.Text = labelname + "<span class=\"red\">*</span>";
                    //txtCity.Attributes.Add("required", "");
                    vcCity.Enabled = true;
                }
                else
                {
                    lblCity.Text = labelname;
                    //txtCity.Attributes.Remove("required");
                    vcCity.Enabled = false;
                }
            }
            #endregion

            #region set divState visibility is true or false if form_input_name is State according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divState.Visible = true;
                }
                else
                {
                    divState.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_State, formtype);
                if (isrequired == 1)
                {
                    lblState.Text = labelname + "<span class=\"red\">*</span>";
                    //txtState.Attributes.Add("required", "");
                    vcState.Enabled = true;
                }
                else
                {
                    lblState.Text = labelname;
                    //txtState.Attributes.Remove("required");
                    vcState.Enabled = false;
                }
            }
            #endregion

            #region set divPostalcode visibility is true or false if form_input_name is Postal code according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divPostalcode.Visible = true;
                }
                else
                {
                    divPostalcode.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_PostalCode, formtype);
                if (isrequired == 1)
                {
                    lblPostalcode.Text = labelname + "<span class=\"red\">*</span>";
                    //txtPostalcode.Attributes.Add("required", "");
                    vcPCode.Enabled = true;
                }
                else
                {
                    lblPostalcode.Text = labelname;
                    //txtPostalcode.Attributes.Remove("required");
                    vcPCode.Enabled = false;
                }
            }
            #endregion

            #region set divCountry visibility is true or false if form_input_name is Country according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCountry.Visible = true;
                }
                else
                {
                    divCountry.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Country, formtype);
                if (isrequired == 1)
                {
                    lblCountry.Text = labelname + "<span class=\"red\">*</span>";
                    //ddlCountry.Attributes.Add("required", "");
                    vcCountry.Enabled = true;
                }
                else
                {
                    lblCountry.Text = labelname;
                    //ddlCountry.Attributes.Remove("required");
                    vcCountry.Enabled = false;
                }
            }
            #endregion

            #region set divRCountry visibility is true or false if form_input_name is RCountry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divRCountry.Visible = true;
                }
                else
                {
                    divRCountry.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_RCountry, formtype);
                if (isrequired == 1)
                {
                    lblRCountry.Text = labelname + "<span class=\"red\">*</span>";
                    //ddlRCountry.Attributes.Add("required", "");
                    vcRCountry.Enabled = true;
                }
                else
                {
                    lblRCountry.Text = labelname;
                    //ddlRCountry.Attributes.Remove("required");
                    vcRCountry.Enabled = false;
                }
            }
            #endregion

            #region set divTelcc visibility is true or false if form_input_name is Telcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelcc.Visible = true;
                }
                else
                {
                    divTelcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtTelcc.Attributes.Add("required", "");
                    vcTelcc.Enabled = true;
                }
                else
                {
                    //txtTelcc.Attributes.Remove("required");
                    vcTelcc.Enabled = false;
                }
            }
            #endregion

            #region set divTelac visibility is true or false if form_input_name is Telac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelac.Visible = true;
                }
                else
                {
                    divTelac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtTelac.Attributes.Add("required", "");
                    vcTelac.Enabled = true;
                }
                else
                {
                    //txtTelac.Attributes.Remove("required");
                    vcTelac.Enabled = false;
                }
            }
            #endregion

            #region set divTel visibility is true or false if form_input_name is Tel according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTel.Visible = true;
                    divTelNo.Visible = true;
                }
                else
                {
                    divTel.Visible = false;
                    divTelNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Tel, formtype);
                if (isrequired == 1)
                {
                    lblTel.Text = labelname + "<span class=\"red\">*</span>";
                    //txtTel.Attributes.Add("required", "");
                    vcTel.Enabled = true;
                }
                else
                {
                    lblTel.Text = labelname;
                    //txtTel.Attributes.Remove("required");
                    vcTel.Enabled = false;
                }
            }
            #endregion

            #region set divMobcc visibility is true or false if form_input_name is Mobilecc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobcc.Visible = true;
                }
                else
                {
                    divMobcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtMobcc.Attributes.Add("required", "");
                    vcMobcc.Enabled = true;
                }
                else
                {
                    //txtMobcc.Attributes.Remove("required");
                    vcMobcc.Enabled = false;
                }
            }
            #endregion

            #region set divMobac visibility is true or false if form_input_name is Mobileac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobac.Visible = true;
                }
                else
                {
                    divMobac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtMobac.Attributes.Add("required", "");
                    vcMobac.Enabled = true;
                }
                else
                {
                    //txtMobac.Attributes.Remove("required");
                    vcMobac.Enabled = false;
                }
            }
            #endregion

            #region set divMobile visibility is true or false if form_input_name is Mobile according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobile.Visible = true;
                    divMobileNo.Visible = true;
                }
                else
                {
                    divMobile.Visible = false;
                    divMobileNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Mobile, formtype);
                if (isrequired == 1)
                {
                    lblMobile.Text = labelname + "<span class=\"red\">*</span>";
                    //txtMobile.Attributes.Add("required", "");
                    vcMob.Enabled = true;
                }
                else
                {
                    lblMobile.Text = labelname;
                    //txtMobile.Attributes.Remove("required");
                    vcMob.Enabled = false;
                }
            }
            #endregion

            #region set divFaxcc visibility is true or false if form_input_name is Faxcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxcc.Visible = true;
                }
                else
                {
                    divFaxcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtFaxcc.Attributes.Add("required", "");
                    vcFaxcc.Enabled = true;
                }
                else
                {
                    //txtFaxcc.Attributes.Remove("required");
                    vcFaxcc.Enabled = false;
                }
            }
            #endregion

            #region set divFaxac visibility is true or false if form_input_name is Faxac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxac.Visible = true;
                }
                else
                {
                    divFaxac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtFaxac.Attributes.Add("required", "");
                    vcFaxac.Enabled = true;
                }
                else
                {
                    //txtFaxac.Attributes.Remove("required");
                    vcFaxac.Enabled = false;
                }
            }
            #endregion

            #region set divFax visibility is true or false if form_input_name is Fax according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFax.Visible = true;
                    divFaxNo.Visible = true;
                }
                else
                {
                    divFax.Visible = false;
                    divFaxNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Fax, formtype);
                if (isrequired == 1)
                {
                    lblFax.Text = labelname + "<span class=\"red\">*</span>";
                    //txtFax.Attributes.Add("required", "");
                    vcFax.Enabled = true;
                }
                else
                {
                    lblFax.Text = labelname;
                    //txtFax.Attributes.Remove("required");
                    vcFax.Enabled = false;
                }
            }
            #endregion

            #region set divEmail visibility is true or false if form_input_name is Email according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmail.Visible = true;
                }
                else
                {
                    divEmail.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Email, formtype);
                if (isrequired == 1)
                {
                    lblEmail.Text = labelname + "<span class=\"red\">*</span>";
                    //txtEmail.Attributes.Add("required", "");
                    vcEmail.Enabled = true;
                }
                else
                {
                    lblEmail.Text = labelname;
                    //txtEmail.Attributes.Remove("required");
                    vcEmail.Enabled = false;
                }
            }
            #endregion

            #region set divEmailConfirmation visibility is true or false if form_input_name is ConfirmEmail according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _EmailConfirmation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmailConfirmation.Visible = true;
                    //txtEmailConfirmation.Attributes.Add("required", "");
                }
                else
                {
                    divEmailConfirmation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_EmailConfirmation, formtype);
                if (isrequired == 1)
                {
                    lblEmailConfirmation.Text = labelname + "<span class=\"red\">*</span>";
                    //txtEmailConfirmation.Attributes.Add("required", "");
                    vcEConfirm.Enabled = true;
                }
                else
                {
                    lblEmailConfirmation.Text = labelname;
                    //txtEmailConfirmation.Attributes.Remove("required");
                    vcEConfirm.Enabled = false;
                }
            }
            #endregion

            #region set divAge visibility is true or false if form_input_name is Age according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAge.Visible = true;
                }
                else
                {
                    divAge.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Age, formtype);
                if (isrequired == 1)
                {
                    lblAge.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAge.Attributes.Add("required", "");
                    vcAge.Enabled = true;
                }
                else
                {
                    lblAge.Text = labelname;
                    //txtAge.Attributes.Remove("required");
                    vcAge.Enabled = false;
                }
            }
            #endregion

            #region set divDOB visibility is true or false if form_input_name is DOB according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDOB.Visible = true;
                }
                else
                {
                    divDOB.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_DOB, formtype);
                if (isrequired == 1)
                {
                    lblDOB.Text = labelname + "<span class=\"red\">*</span>";
                    //txtDOB.Attributes.Add("required", "");
                    vcDOB.Enabled = true;
                }
                else
                {
                    lblDOB.Text = labelname;
                    //txtDOB.Attributes.Remove("required");
                    vcDOB.Enabled = false;
                }
            }
            #endregion

            #region set divGender visibility is true or false if form_input_name is Gender according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                if (isshow == 1)
                {
                    divGender.Visible = true;
                }
                else
                {
                    divGender.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Gender, formtype);
                if (isrequired == 1)
                {
                    lblGender.Text = labelname + "<span class=\"red\">*</span>";
                    //ddlGender.Attributes.Add("required", "");
                    //vcGender.Enabled = true;
                }
                else
                {
                    lblGender.Text = labelname;
                    //ddlGender.Attributes.Remove("required");
                    //vcGender.Enabled = false;
                }
            }
            #endregion

            #region set divVisitDate visibility is true or false if form_input_name is VisitDate according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VisitDate)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitDate.Visible = true;
                }
                else
                {
                    divVisitDate.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VisitDate, formtype);
                if (isrequired == 1)
                {
                    lblVisitDate.Text = labelname + "<span class=\"red\">*</span>";
                    //txtVisitDate.Attributes.Add("required", "");
                    vcVisitDate.Enabled = true;
                }
                else
                {
                    lblVisitDate.Text = labelname;
                    //txtVisitDate.Attributes.Remove("required");
                    vcVisitDate.Enabled = false;
                }
            }
            #endregion

            #region set divVisitTime visibility is true or false if form_input_name is VisitTime according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VisitTime)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitTime.Visible = true;
                }
                else
                {
                    divVisitTime.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VisitTime, formtype);
                if (isrequired == 1)
                {
                    lblVisitTime.Text = labelname + "<span class=\"red\">*</span>";
                    //txtVisitTime.Attributes.Add("required", "");
                    vcVisitTime.Enabled = true;
                }
                else
                {
                    lblVisitTime.Text = labelname;
                    //txtVisitTime.Attributes.Remove("required");
                    vcVisitTime.Enabled = false;
                }
            }
            #endregion

            #region set divPassword visibility is true or false if form_input_name is Password according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Password)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divPassword.Visible = true;
                }
                else
                {
                    divPassword.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Password, formtype);
                if (isrequired == 1)
                {
                    lblPassword.Text = labelname + "<span class=\"red\">*</span>";
                    //txtPassword.Attributes.Add("required", "");
                    vcPassword.Enabled = true;
                }
                else
                {
                    lblPassword.Text = labelname;
                    //txtPassword.Attributes.Remove("required");
                    vcPassword.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional4 visibility is true or false if form_input_name is Additional4 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional4.Visible = true;
                }
                else
                {
                    divAdditional4.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Additional4, formtype);
                if (isrequired == 1)
                {
                    lblAdditional4.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAdditional4.Attributes.Add("required", "");
                    vcAdditional4.Enabled = true;
                }
                else
                {
                    lblAdditional4.Text = labelname;
                    //txtAdditional4.Attributes.Remove("required");
                    vcAdditional4.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional5 visibility is true or false if form_input_name is Additional5 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional5.Visible = true;
                }
                else
                {
                    divAdditional5.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Additional5, formtype);
                if (isrequired == 1)
                {
                    lblAdditional5.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAdditional5.Attributes.Add("required", "");
                    vcAdditional5.Enabled = true;
                }
                else
                {
                    lblAdditional5.Text = labelname;
                    //txtAdditional5.Attributes.Remove("required");
                    vcAdditional5.Enabled = false;
                }
            }
            #endregion
        }

        setDivCss_TelMobFax(divTelcc.Visible, divTelac.Visible, divTelNo.Visible, "Tel");//*
        setDivCss_TelMobFax(divMobcc.Visible, divMobac.Visible, divMobileNo.Visible, "Mob");//*
        setDivCss_TelMobFax(divFaxcc.Visible, divFaxac.Visible, divFaxNo.Visible, "Fax");//*

        return isValidShow;
    }
    #endregion

    #region bindDropdown & bind respective data to Salutation and Country dropdown lists
    protected void bindDropdown(string showid)
    {
        DataSet dsSalutation = new DataSet();
        DataSet dsCountry = new DataSet();
        DataSet dsIndustry = new DataSet();

        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsSalutation = cmdObj.getSalutation(showid);

        CountryObj couObj = new CountryObj(fn);
        dsCountry = couObj.getAllCountry();

        dsIndustry = cmdObj.getIndustry(showid);

        if (dsSalutation.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsSalutation.Tables[0].Rows.Count; i++)
            {
                ddlSalutation.Items.Add(dsSalutation.Tables[0].Rows[i]["Sal_Name"].ToString());
                ddlSalutation.Items[i + 1].Value = dsSalutation.Tables[0].Rows[i]["Sal_ID"].ToString();
            }
        }
        if (dsCountry.Tables[0].Rows.Count != 0)
        {
            for (int x = 0; x < dsCountry.Tables[0].Rows.Count; x++)
            {
                ddlCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();

                ddlRCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlRCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();
            }
        }
        if (dsIndustry.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsIndustry.Tables[0].Rows.Count; i++)
            {
                ddlIndustry.Items.Add(dsIndustry.Tables[0].Rows[i]["Industry"].ToString());
                ddlIndustry.Items[i + 1].Value = dsIndustry.Tables[0].Rows[i]["ID"].ToString();
            }
        }
    }
    #endregion

    #region ClearForm
    private void ClearForm()
    {
        hfRegGroupID.Value = "";
        hfStage.Value = "";
        ddlSalutation.SelectedIndex = 0;
        txtSalOther.Text = "";
        txtFName.Text = "";
        txtLName.Text = "";
        txtDesignation.Text = "";
        txtDepartment.Text = "";
        txtCompany.Text = "";
        ddlIndustry.SelectedIndex = 0;
        txtIndusOther.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtCity.Text = "";
        txtState.Text = "";
        txtPostalcode.Text = "";
        ddlCountry.SelectedIndex = 0;
        ddlRCountry.SelectedIndex = 0;
        txtTelcc.Text = "";
        txtTelac.Text = "";
        txtTel.Text = "";
        txtMobcc.Text = "";
        txtMobac.Text = "";
        txtMobile.Text = "";
        txtFaxcc.Text = "";
        txtFaxac.Text = "";
        txtFax.Text = "";
        txtEmail.Text = "";
        txtAge.Text = "";
        txtDOB.Text = "";
        ddlGender.SelectedIndex = 0;
        txtVisitDate.Text = "";
        txtVisitTime.Text = "";
        txtPassword.Text = "";
        txtAdditional4.Text = "";
        txtAdditional5.Text = "";
    }
    #endregion

    #region ddlSalutation_SelectedIndexChanged (set 'other salutation div' visibility if the selection of ddlSalutation dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherSalutation and form_type='G')
    protected void ddlSalutation_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
            try
            {
                if (ddlSalutation.Items.Count > 0)
                {
                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(ddlSalutation.SelectedItem.Text))
                    {
                        FormManageObj frmObj = new FormManageObj(fn);
                        frmObj.showID = showid;
                        frmObj.flowID = flowid;
                        DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeGroup, _OtherSalutation);
                        if (dt.Rows.Count > 0)
                        {
                            int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                            if (isshow == 1)
                            {
                                divSalOther.Visible = true;
                            }
                            else
                            {
                                divSalOther.Visible = false;
                            }

                            if (isrequired == 1)
                            {
                                //txtSalOther.Attributes.Add("required", "");
                                vcSalOther.Enabled = true;
                            }
                            else
                            {
                                //txtSalOther.Attributes.Remove("required");
                                vcSalOther.Enabled = false;
                            }
                        }
                    }
                    else
                    {
                        divSalOther.Visible = false;
                        //txtSalOther.Attributes.Remove("required");
                        vcSalOther.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region ddlIndustry_SelectedIndexChanged (set 'other industry div' visibility if the selection of ddlIndustry dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherIndustry and form_type='G')
    protected void ddlIndustry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = Request.QueryString["SHW"].ToString();
            string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
            try
            {
                if (ddlIndustry.Items.Count > 0)
                {
                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(ddlIndustry.SelectedItem.Text))
                    {
                        FormManageObj frmObj = new FormManageObj(fn);
                        frmObj.showID = showid;
                        frmObj.flowID = flowid;
                        DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeGroup, _OtherIndustry);
                        if (dt.Rows.Count > 0)
                        {
                            int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                            if (isshow == 1)
                            {
                                divIndusOther.Visible = true;
                            }
                            else
                            {
                                divIndusOther.Visible = false;
                            }

                            if (isrequired == 1)
                            {
                                //txtIndusOther.Attributes.Add("required", "");
                                vcIndusOther.Enabled = true;
                            }
                            else
                            {
                                //txtIndusOther.Attributes.Remove("required");
                                vcIndusOther.Enabled = false;
                            }
                        }
                    }
                    else
                    {
                        divIndusOther.Visible = false;
                        //txtIndusOther.Attributes.Remove("required");
                        vcIndusOther.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region ddlCountry_SelectedIndexChanged (bind country code data to txtTelcc, txtMobcc, txtFaxcc textboxes according to the selected country)
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlCountry.Items.Count > 0)
            {
                string countryid = ddlCountry.SelectedItem.Value;

                if (ddlCountry.SelectedIndex == 0)
                {
                    countryid = Number.Zero;
                }

                CountryObj couObj = new CountryObj(fn);
                DataTable dt = couObj.getCountryByID(countryid);
                if (dt.Rows.Count > 0)
                {
                    string code = dt.Rows[0]["countryen"].ToString();

                    txtTelcc.Text = code;
                    txtMobcc.Text = code;
                    txtFaxcc.Text = code;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region ddlRCountry_SelectedIndexChanged (bind country code data to txtTelcc, txtMobcc, txtFaxcc textboxes according to the selected country)
    protected void ddlRCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlRCountry.Items.Count > 0)
            {
                string countryid = ddlRCountry.SelectedItem.Value;

                if (ddlRCountry.SelectedIndex == 0)
                {
                    countryid = Number.Zero;
                }

                CountryObj couObj = new CountryObj(fn);
                DataTable dt = couObj.getCountryByID(countryid);
                if (dt.Rows.Count > 0)
                {
                    string code = dt.Rows[0]["countryen"].ToString();

                    txtTelcc.Text = code;
                    txtMobcc.Text = code;
                    txtFaxcc.Text = code;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #endregion

    #region setDivCss_TelMobFax
    public void setDivCss_TelMobFax(bool isShowCC, bool isShowAC, bool isShowPhoneNo, string type)
    {
        string name = string.Empty;
        try
        {
            if (type == "Tel")
            {
                #region type="Tel"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");
                    divTelcc.Attributes.Add("class", "col-xs-6");

                    divTelac.Attributes.Remove("class");
                    divTelac.Attributes.Add("class", "col-xs-6");

                    divTelNo.Attributes.Remove("class");
                }
                #endregion
            }
            else if (type == "Mob")
            {
                #region type="Mob"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divMobac.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");

                    divMobac.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");
                    divMobcc.Attributes.Add("class", "col-xs-6");

                    divMobac.Attributes.Remove("class");
                    divMobac.Attributes.Add("class", "col-xs-6");

                    divMobileNo.Attributes.Remove("class");
                }
                #endregion
            }
            else if (type == "Fax")
            {
                #region Type="Fax"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");
                    divFaxcc.Attributes.Add("class", "col-xs-6");

                    divFaxac.Attributes.Remove("class");
                    divFaxac.Attributes.Add("class", "col-xs-6");

                    divFaxNo.Attributes.Remove("class");
                }
                #endregion
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

}