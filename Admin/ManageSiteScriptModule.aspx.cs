﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.Sql;
using System.Threading;
using Telerik.Web.UI;
using System.IO;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Utilities;

public partial class Admin_ManageSiteScriptModule : System.Web.UI.Page
{
    Functionality fn = new Functionality();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Master.setopensettings();
            PanelKeyDetail.Visible = false;
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_site_module table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            string query = "Select * From tb_site_module Order By module_id";
            DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dt;
            }
            else
            {
                lbls.Text = "There is no record.";
            }
        }
        catch (Exception)
        {
            lbls.Text = "There is no record.";
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        string moduleid = "";
        foreach (GridDataItem item in GKeyMaster.SelectedItems)
        {
            moduleid = item["module_id"].Text;
        }

        if (e.CommandName == "Delete")
        {
            GridDataItem item = (GridDataItem)e.Item;

            string moduleID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["module_id"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["module_id"].ToString() : "";
            if (!string.IsNullOrEmpty(moduleID))
            {
                int isDeleted = fn.ExecuteSQL(string.Format("Delete From tb_site_module Where module_id='{0}'", moduleID));
                if (isDeleted > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already deleted.');", true);
                }
                else
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Deleting error.');", true);
                }

                ResetControls();
                PanelKeyDetail.Visible = false;
                btnAdd.Visible = false;
                btnUpdateRecord.Visible = false;
                lbls.Text = "";
            }
        }
        else
        {
            if (moduleid != "" && moduleid != null)
            {
                btnUpdateRecord.Visible = true;
                btnAdd.Visible = false;
                PanelKeyDetail.Visible = true;

                loadvalue(moduleid);
            }
        }
    }
    #endregion

    #region btnAddNew_Click (Load when click Add New button)
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        ResetControls();
        btnUpdateRecord.Visible = false;
        btnAdd.Visible = true;
        PanelKeyDetail.Visible = true;
    }
    #endregion

    #region btnAdd_Click (Insert the respective data into tb_site_module table)
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CommonFuns cFun = new CommonFuns();
            lbls.Text = "";
            string moduleid = cFun.solveSQL(txtModuleID.Text.Trim());
            string scriptid = cFun.solveSQL(txtScriptID.Text.Trim());
            string desc = cFun.solveSQL(txtDescription.Text.Trim());

            try
            {
                DataTable dtmaster = fn.GetDatasetByCommand("Select * From tb_site_module Where module_id='" + moduleid + "'", "ds").Tables[0];

                if (dtmaster.Rows.Count > 0)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This script module already exist.');", true);
                    return;
                }
                else
                {
                    string query = string.Format("Insert Into tb_site_module (module_id, script_id, script_desc)"
                        + " Values ('{0}', '{1}', '{2}')"
                        , moduleid, scriptid, desc);

                    int rowInserted = fn.ExecuteSQL(query);
                    if (rowInserted == 1)
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                    }
                    else
                    {
                        GKeyMaster.Rebind();
                        btnUpdateRecord.Visible = false;
                        btnAdd.Visible = false;
                        PanelKeyDetail.Visible = false;
                        GKeyMaster.Rebind();
                        ResetControls();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Saving error, try again.');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                lbls.Text = ex.Message;
            }
        }
    }
    #endregion

    #region loadvalue (Load RadGrid selected row data from tb_site_module table to bind the respective controls while edit)
    private void loadvalue(string moduleid)
    {
        string query = "Select * From tb_site_module Where module_id='" + moduleid + "'";

        DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
        if(dt.Rows.Count > 0)
        {
            hfID.Value = moduleid;
            txtModuleID.Text = moduleid;
            txtModuleID.Enabled = false;
            txtScriptID.Text = dt.Rows[0]["script_id"].ToString();
            txtDescription.Text = dt.Rows[0]["script_desc"].ToString();
        }
    }
    #endregion

    #region btnUpdateRecord_Click (Update the respective data into "tb_site_module" table)
    protected void btnUpdateRecord_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CommonFuns cFun = new CommonFuns();
            lbls.Text = "";
            string moduleid = cFun.solveSQL(txtModuleID.Text.Trim());
            string scriptid = cFun.solveSQL(txtScriptID.Text.Trim());
            string desc = cFun.solveSQL(txtDescription.Text.Trim());

            try
            {
                string query = string.Format("Update tb_site_module Set script_id='{0}', script_desc='{1}'"
                    + " Where module_id='{2}'", scriptid, desc, moduleid);

                int rowUpdated = fn.ExecuteSQL(query);
                if (rowUpdated == 1)
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Success');", true);
                }
                else
                {
                    GKeyMaster.Rebind();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updating error, try again.');", true);
                }

                btnUpdateRecord.Visible = false;
                btnAdd.Visible = false;
                PanelKeyDetail.Visible = false;
                GKeyMaster.Rebind();
                ResetControls();
            }
            catch (Exception ex)
            {
                lbls.Text += ex.Message;
            }
        }
    }
    #endregion

    #region ResetControls
    private void ResetControls()
    {
        hfID.Value = "";
        lbls.Text = "";
        txtModuleID.Text = "";
        txtModuleID.Enabled = true;
        txtScriptID.Text = "";
        txtDescription.Text = "";
    }
    #endregion
}