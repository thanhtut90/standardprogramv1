﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using System.Text;
using Corpit.Site.Utilities;
using Corpit.Payment;
using System.IO;
using ClosedXML.Excel;
using Corpit.Site.Email;
using System.Text.RegularExpressions;
using Corpit.Logging;
using System.Globalization;

public partial class Admin_MDAThaiTrackingReport : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    QuesFunctionality qfn = new QuesFunctionality();
    CommonFuns cFun = new CommonFuns();

    protected int count = 0;
    protected string htmltb;
    static StringBuilder StrBuilder = new StringBuilder();
    string showID = string.Empty;
    string flowID = string.Empty;
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string other_value = "Others";
    static string prostudent = "Student";
    static string proalliedhealth = "Allied Health";
    static string noInvoice = "-10";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (Session["userid"] != null)
                {
                    string userID = Session["userid"].ToString();

                    if (!string.IsNullOrEmpty(userID))
                    {
                        binddata();

                        if (Session["roleid"].ToString() != "1")
                        {
                            lblUser.Text = userID;
                            getShowID(userID);
                            ShowControler shwCtr = new ShowControler(fn);
                            Show shw = shwCtr.GetShow(showID);
                            GKeyMaster.ExportSettings.ExportOnlyData = true;
                            GKeyMaster.ExportSettings.IgnorePaging = true;
                            GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                            GKeyMaster.ExportSettings.FileName = shw.SHW_Name + "TrackingReport_" + DateTime.Now.ToString("ddMMyyyy");
                        }
                        else
                        {
                            Response.Redirect("Login.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("Event_Config");
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }

    protected void binddata()
    {
        //string constr = fn.ConnString;
        //using (SqlConnection con = new SqlConnection(constr))
        //{
        //    using (SqlCommand cmd = new SqlCommand("Select status_name,status_usedid From ref_Status"))
        //    {
        //        cmd.CommandType = CommandType.Text;
        //        cmd.Connection = con;
        //        con.Open();
        //        ddl_paymentStatus.Items.Clear();
        //        ddl_paymentStatus.DataSource = cmd.ExecuteReader();
        //        ddl_paymentStatus.DataTextField = "status_name";
        //        ddl_paymentStatus.DataValueField = "status_usedid";
        //        ddl_paymentStatus.DataBind();
        //        con.Close();

        //        ddl_paymentStatus.Items.Insert(0, new ListItem("All", "-1"));
        //    }

        //    using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show where Status='Active'"))
        //    {
        //        cmd.CommandType = CommandType.Text;
        //        cmd.Connection = con;
        //        con.Open();
        //        ddl_showList.Items.Clear();
        //        ddl_showList.DataSource = cmd.ExecuteReader();
        //        ddl_showList.DataTextField = "SHW_Name";
        //        ddl_showList.DataValueField = "SHW_ID";
        //        ddl_showList.DataBind();
        //        con.Close();
        //    }

        //    //using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID = '" + ddl_showList.SelectedValue + "' and Status='Active'"))
        //    using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master as fm Inner Join tb_site_flow as f On fm.FLW_ID=f.flow_id where ShowID = '" + ddl_showList.SelectedValue + "' and fm.Status='Active' And f.script_id Like '%Conference%'"))/*changed by than on 23-4-2019*/
        //    {
        //        cmd.CommandType = CommandType.Text;
        //        cmd.Connection = con;
        //        con.Open();
        //        ddl_flowList.Items.Clear();
        //        ddl_flowList.DataSource = cmd.ExecuteReader();
        //        ddl_flowList.DataTextField = "FLW_Desc";
        //        ddl_flowList.DataValueField = "FLW_ID";
        //        ddl_flowList.DataBind();
        //        con.Close();
        //    }
        //}
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_RegDelegate and tb_Invoice tables for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID))
        {
            DataTable dtReg = generateRegReport(showID);
            GKeyMaster.DataSource = dtReg;
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            this.GKeyMaster.ExportSettings.ExportOnlyData = true;
            this.GKeyMaster.ExportSettings.IgnorePaging = true;
            this.GKeyMaster.ExportSettings.OpenInNewWindow = true;


            this.GKeyMaster.MasterTableView.ExportToExcel();
        }
    }
    #endregion

    /// <summary> 
    /// Item databound for the radgrid, set the header text here based on the form management ***Item[UniqueName]
    /// </summary>
    protected void GKeyMaster_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        Session["pgno"] = GKeyMaster.CurrentPageIndex + 1;

        //if (!string.IsNullOrEmpty(txtKey.Text) && !string.IsNullOrWhiteSpace(txtKey.Text))
        //{
        //    btnKeysearch_Click(this, null);
        //}
        //else
        //{
        //    if (string.IsNullOrEmpty(txtFromDate.Text.Trim()) || string.IsNullOrEmpty(txtToDate.Text.Trim()))
        //    {
        //        MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //        msregIndiv.ShowID = showID;
        //        msregIndiv.FlowID = flowID;
        //        GKeyMaster.DataSource = msregIndiv.getDataAllRegList();
        //    }
        //    else
        //    {
        //        GKeyMaster.DataSource = getDataAllRegList(txtFromDate.Text.Trim(), txtToDate.Text.Trim());
        //    }
        //}
    }

    #region delRecord
    private int delRecord(string regno)
    {
        int isDeleted = 0;
        if (!string.IsNullOrEmpty(showID))
        {
            try
            {
                //isDeleted = fn.ExecuteSQL(string.Format("Update tb_RegGroup Set recycle=1 Where RegGroupID={0} And ShowID='{1}'", groupid, showid));
                isDeleted += fn.ExecuteSQL(string.Format("Update tb_RegDelegate Set recycle=1 Where Regno={0} And ShowID='{1}'", regno, showID));
            }
            catch (Exception ex)
            {

            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

        return isDeleted;
    }
    #endregion
    protected void RadGd1_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem)e.Item;
            string edit_RegGroupID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
            string edit_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
            if (!string.IsNullOrEmpty(edit_Regno))
            {

                ImageButton downloadlink = (ImageButton)item["Download"].Controls[0];
                downloadlink.Attributes.Add
               ("onclick", "window.open('../AcknowledgeLetter.aspx?DID=" + cFun.EncryptValue(edit_Regno) +
               "&SHW=" + cFun.EncryptValue(showID) + "'); return false");


            }


        }
    }

    protected void lnkExcel_Clicked(object sender, EventArgs e)
    {
        try
        {
            this.GKeyMaster.ExportSettings.ExportOnlyData = true;
            this.GKeyMaster.ExportSettings.IgnorePaging = true;
            this.GKeyMaster.ExportSettings.OpenInNewWindow = true;
            this.GKeyMaster.MasterTableView.ExportToExcel();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }
    }

    private string getPromoCode(string groupid)
    {
        string result = string.Empty;
        string sql = "Select Promo_Code From tb_User_Promo Where Regno='" + groupid + "'";
        result = fn.GetDataByCommand(sql, "Promo_Code");
        if(result == "0")
        {
            result = "";
        }

        return result;
    }

    #region New
    public DataTable generateRegReport(string showid)
    {
        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(showid))
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showID);
            try
            {
                DataTable dtRegList = new DataTable();
                string sql = "Select t.RefID, CONVERT(VARCHAR(10), t.TDate, 103) + ' '  + convert(VARCHAR(8), t.TDate, 14) as TDate, t.TrackCode, r.Regno as Regno, r.reg_FName 'First Name' ,"
                            + " r.reg_LName 'Last Name', r.reg_Email as  Email, r.reg_Address1 as Company, (Select Country From ref_Country Where ref_Country.Cty_GUID= r.reg_Country) as Country"
                            + " ,Case When reg_Status='0' Then 'Incomplete' When reg_Status Is Null Then '' Else 'Success' End as 'Reg Status'"
                            + " From tb_Log_Tracking as t Left Join tb_RegDelegate as r On t.RefID=r.RegGroupID Where t.ShowID='" + showid + "'";
                dtRegList = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dtRegList.Rows.Count > 0)
                {
                }
                dt = dtRegList;//***
            }
            catch (Exception ex)
            { }
        }
        return dt;
    }
    #endregion
}
