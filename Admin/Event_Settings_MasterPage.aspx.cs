﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Corpit.Utilities;
using Corpit.BackendMaster;
using Corpit.Site.Utilities;
using Corpit.Site.Email;
public partial class Admin_Event_Settings_MasterPage : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    private static string _eventname = "eventname";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                bindShowName(showid);
                ShowController sControler = new ShowController(fn);
                if (!sControler.checkSettingsCreated(showid, REF_RunNumber.REF_SHOW,DefaultREfType.SHOW_SETTING))
                {
                    //  InsertNewSiteSettings(showid,lbl_eventName.Text);
                    sControler.InsertShowSiteSettings(showid, DefaultREfType.SHOW_SETTING);
                }
                else
                {
                    CheckProgress(showid);
                }
                 
                if (!sControler.checkSettingsCreated(showid, REF_RunNumber.REF_SITE, DefaultREfType.SITE_SETTING))
                {
                    //  InsertNewSiteSettings(showid,lbl_eventName.Text);
                    sControler.InsertShowSiteSettings(showid, DefaultREfType.SITE_SETTING);
                }
            }

            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void CheckProgress(string showid)
    {
        string query = "Select * From tb_site_settings Where ShowID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dtSettings = fn.GetDatasetByCommand(query, "dsSettings", pList).Tables[0];

        if (dtSettings.Rows.Count > 0)
        {
            for (int i = 0; i < dtSettings.Rows.Count; i++) {
                if (dtSettings.Rows[i]["settings_name"].ToString() == "Site_CSSBandle")
                    if (!string.IsNullOrEmpty(dtSettings.Rows[i]["settings_value"].ToString()))
                        img_webSetting.ImageUrl = "images/tick.png";
                    else
                        img_webSetting.ImageUrl = "images/cross.png";
                if (dtSettings.Rows[i]["settings_name"].ToString() == "RegClosedDate")
                    if (!string.IsNullOrEmpty(dtSettings.Rows[i]["settings_value"].ToString()))
                        img_eventSetting.ImageUrl = "images/tick.png";
                    else
                        img_eventSetting.ImageUrl = "images/cross.png";
                if (dtSettings.Rows[i]["settings_name"].ToString() == "Currency")
                    if (!string.IsNullOrEmpty(dtSettings.Rows[i]["settings_value"].ToString()))
                        img_paymentSetting.ImageUrl = "images/tick.png";
                    else
                        img_paymentSetting.ImageUrl = "images/cross.png";
                //if (dtSettings.Rows[i]["settings_name"].ToString() == "SMTPUsernname")
                //    if (!string.IsNullOrEmpty(dtSettings.Rows[i]["settings_value"].ToString()))
                //        img_emailSetting.ImageUrl = "images/tick.png";
                //    else
                //        img_emailSetting.ImageUrl = "images/cross.png";

               
                
            }

            bool isEmailSetUpFinished = false;
            SiteEmailConfigHelper siteEConfigHelper = new SiteEmailConfigHelper(fn);
            SiteEmailConfig siteEConfig = siteEConfigHelper.GetSiteEmailConfig(showid);
            if (siteEConfig != null && !string.IsNullOrEmpty(siteEConfig.RefEmailProjID))
            {
                ePoratalShowConfigJsonData showConfig = new ePoratalShowConfigJsonData();
                showConfig.ShowID = siteEConfig.RefEmailProjID;
                EmailHelper sHelper = new EmailHelper();
                showConfig = sHelper.GetShowConfigSettings(showConfig);
                if (showConfig != null && !string.IsNullOrEmpty(showConfig.SMTPUsername))
                    isEmailSetUpFinished = true;
            }
            if (isEmailSetUpFinished)
                img_emailSetting.ImageUrl = "images/tick.png";
            else
                img_emailSetting.ImageUrl = "images/cross.png";
        }
    }

    #region bindShowName
    private void bindShowName(string showid)
    {
        string query = "Select SHW_Name From tb_Show Where SHW_ID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dt = new DataTable();
        dt = fn.GetDatasetByCommand(query, "SHW_Name", pList).Tables[0];

        if (dt.Rows.Count == 0) {
            Response.Redirect("Event_Config");
        }
        else {
            string showName = dt.Rows[0]["SHW_Name"].ToString();
            lbl_eventName.Text = showName == "0" ? string.Empty : showName;
        }
    }
    #endregion

    #region checkCreated (check the record count of tmp_tbSiteSettings table is equal to the record counts of tb_site_settings table)
    private bool checkCreated(string showid)
    {
        bool isCreated = false;
        DataTable dt = fn.GetDatasetByCommand("Select * From tmp_tbSiteSettings", "ds").Tables[0];

        string query = "Select * From tb_site_settings Where ShowID=@SHWID And settings_name In (Select settings_name From tmp_tbSiteSettings)";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dtSettings = fn.GetDatasetByCommand(query, "dsSettings", pList).Tables[0];
        if (dt.Rows.Count == dtSettings.Rows.Count)
        {
            isCreated = true;
        }

        return isCreated;
    }
    #endregion

    #region InsertNewSiteSettings and using tb_site_settings table & Insert new blank records but settings_name, ShowID into tb_site_settings table
    protected void InsertNewSiteSettings(string showid,string eventname)
    {
        try
        {
            string settingValue = string.Empty;
            string sql = string.Empty;

            DataTable dt = fn.GetDatasetByCommand("Select * From tmp_tbSiteSettings", "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string settingName = dr["settings_name"].ToString();

                    string query = "Select * From tb_site_settings Where ShowID=@SHWID And settings_name='" + settingName + "'";
                    List<SqlParameter> pList = new List<SqlParameter>();
                    SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    spar.Value = showid;
                    pList.Add(spar);

                    DataTable dtSettings = fn.GetDatasetByCommand(query, "dsSettings", pList).Tables[0];
                    if (dtSettings.Rows.Count == 0)
                    {
                        sql = string.Format("Insert Into tb_site_settings (settings_name,settings_value,ShowID)"
                                    + " Values('{0}', '{1}', '{2}')", settingName, settingValue, showid);

                        fn.ExecuteSQL(sql);
                    }
                }
                sql = "Update tb_site_settings Set settings_value='" + _eventname + "' Where settings_name='" + eventname + "' And ShowID='" + showid + "';";
                fn.ExecuteSQL(sql);
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    protected void btn_editWebSetting_Click(object sender, EventArgs e)
    {
        Response.Redirect("Event_Settings_WebSettings?SHW=" + Request.Params["SHW"]);
    }

    protected void btn_editEventSetting_Click(object sender, EventArgs e)
    {
        Response.Redirect("Event_Settings_EventSettings?SHW=" + Request.Params["SHW"]);
    }

    protected void btn_editPaymentSetting_Click(object sender, EventArgs e)
    {
        Response.Redirect("Event_Settings_PaymentSettings?SHW=" + Request.Params["SHW"]);
    }

    protected void btn_editEmailSetting_Click(object sender, EventArgs e)
    {
        Response.Redirect("Event_Settings_EmailSettings?SHW=" + Request.Params["SHW"]);
    }

    protected void NavEventFlowForm(object sender, EventArgs e)
    {
        try
        {
            if (img_webSetting.ImageUrl != "images/tick.png") { 
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please complete the web setting details');", true);
                return;
            }
            if (img_eventSetting.ImageUrl != "images/tick.png")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please complete the event setting details');", true);
                return;
            }
            if (img_paymentSetting.ImageUrl != "images/tick.png")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please complete the payment setting details');", true);
                return;
            }
            if (img_emailSetting.ImageUrl != "images/tick.png")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please complete the email setting details');", true);
                return;
            }
   

            if (Session["roleid"].ToString() == "1")
                Response.Redirect("Event_Flow_Mangaement?SHW=" + Request.Params["SHW"],false);
            else
                Response.Redirect("Event_Config",false);
        }
        catch (Exception ex)
        {
            Response.Redirect("Login");
        }
    }
}