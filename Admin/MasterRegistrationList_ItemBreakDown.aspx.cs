﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using System.Text;
using Corpit.Site.Utilities;
using Corpit.Payment;
using System.IO;
using ClosedXML.Excel;
using Corpit.Site.Email;
using System.Text.RegularExpressions;
using Corpit.Logging;
using System.Globalization;

public partial class Admin_MasterRegistrationList_ItemBreakDown : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    QuesFunctionality qfn = new QuesFunctionality();
    CommonFuns cFun = new CommonFuns();

    protected int count = 0;
    protected string htmltb;
    static StringBuilder StrBuilder = new StringBuilder();
    string showID = string.Empty;
    string flowID = string.Empty;
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string other_value = "Others";
    static string prostudent = "Student";
    static string proalliedhealth = "Allied Health";
    static string noInvoice = "-10";
    private static string[] checkingmdaFlowName = new string[] { "MFA2018iReg", "MFA2018GroupReg", "MMA2018iReg", "MMA2018GroupReg", "OSHA2018iReg", "OSHA2018GroupReg" };//***MDA
    private static string[] checkingBadgeShowName = new string[] { "VNU_AGRI", "VNU_HORTI" };
    private static string[] checkingReceiptInVisibleShowName = new string[] { "VNU_AGRI", "VNU_HORTI" };
    private static string[] checkingSARCShowName = new string[] { "SARC" };
    private static string[] checkingAcknowledgementMDAShowName = new string[] { "MFA 2018", "MMA 2018", "OSHA 2018" };//***MDA 31-7-2018
    private static string[] checkingFJShowName = new string[] { "Food Japan" };//***MDA 31-7-2018
    private static string[] checkingVendorRegistrationShowName = new string[] { "Vendor Registration" };//***Vendor Registration 17-9-2018
    private static string[] checkingOnsiteShowName = new string[] { "World Congress on Maritime Heritage 2019" };//***4-3-2019
    private static string[] checkingWCMHShowName = new string[] { "World Congress on Maritime Heritage 2019" };//***4-3-2019
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (Session["userid"] != null)
                {
                    string userID = Session["userid"].ToString();

                    if (!string.IsNullOrEmpty(userID))
                    {
                        binddata();

                        if (Session["roleid"].ToString() != "1")
                        {
                            lblUser.Text = userID;
                            getShowID(userID);
                            showlist.Visible = false;

                            GKeyMaster.ExportSettings.ExportOnlyData = true;
                            GKeyMaster.ExportSettings.IgnorePaging = true;
                            GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                            GKeyMaster.ExportSettings.FileName = "RegItemBreakDown_" + DateTime.Now.ToString("ddMMyyyy");


                            string constr = fn.ConnString;
                            using (SqlConnection con = new SqlConnection(constr))
                            {
                                //using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID = '" + showID + "' and Status='Active'"))
                                using (SqlCommand cmd = new SqlCommand("Select Distinct FLW_ID,FLW_Desc From tb_site_flow_master as fm Inner Join tb_site_flow as f On fm.FLW_ID=f.flow_id where ShowID = '" + showID + "' and fm.Status='Active' And f.script_id Like '%Conference%' And fm.FLW_Desc Not Like '%Old%'"))/*changed by than on 17-7-2019*/
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.Connection = con;
                                    con.Open();
                                    ddl_flowList.Items.Clear();
                                    ddl_flowList.DataSource = cmd.ExecuteReader();
                                    ddl_flowList.DataTextField = "FLW_Desc";
                                    ddl_flowList.DataValueField = "FLW_ID";
                                    ddl_flowList.DataBind();
                                    con.Close();
                                }
                            }
                        }
                        else
                            showID = ddl_showList.SelectedValue;
                    }
                    else
                    {
                        Response.Redirect("Event_Config");
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }

    protected void binddata()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select status_name,status_usedid From ref_Status"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_paymentStatus.Items.Clear();
                ddl_paymentStatus.DataSource = cmd.ExecuteReader();
                ddl_paymentStatus.DataTextField = "status_name";
                ddl_paymentStatus.DataValueField = "status_usedid";
                ddl_paymentStatus.DataBind();
                con.Close();

                ddl_paymentStatus.Items.Insert(0, new ListItem("All", "-1"));
            }

            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show where Status='Active'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_showList.Items.Clear();
                ddl_showList.DataSource = cmd.ExecuteReader();
                ddl_showList.DataTextField = "SHW_Name";
                ddl_showList.DataValueField = "SHW_ID";
                ddl_showList.DataBind();
                con.Close();
            }

            //using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID = '" + ddl_showList.SelectedValue + "' and Status='Active'"))
            using (SqlCommand cmd = new SqlCommand("Select Distinct FLW_ID,FLW_Desc From tb_site_flow_master as fm Inner Join tb_site_flow as f On fm.FLW_ID=f.flow_id where ShowID = '" + ddl_showList.SelectedValue + "' and fm.Status='Active' And f.script_id Like '%Conference%' And fm.FLW_Desc Not Like '%Old%'"))/*changed by than on 17-7-2019*/
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_flowList.Items.Clear();
                ddl_flowList.DataSource = cmd.ExecuteReader();
                ddl_flowList.DataTextField = "FLW_Desc";
                ddl_flowList.DataValueField = "FLW_ID";
                ddl_flowList.DataBind();
                con.Close();
            }
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_RegDelegate and tb_Invoice tables for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID))
        {
            DataTable dtReg = generateRegReport(showID, flowID);
            GKeyMaster.DataSource = dtReg;
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region checkPaymentMode
    private bool checkPaymentMode(string showID)
    {
        bool isUsedChequeTT = false;
        try
        {
            CommonDataObj cmdObj = new CommonDataObj(fn);
            DataTable dt = cmdObj.getPaymentMethods(showID);
            if (dt.Rows.Count > 0)
            {
                SiteSettings st = new SiteSettings(fn, showID);
                foreach (DataRow dr in dt.Rows)
                {
                    string usedid = dr["method_usedid"].ToString();
                    if (usedid == ((int)PaymentType.TT).ToString())
                    {
                        isUsedChequeTT = true;
                    }
                    if (usedid == ((int)PaymentType.Cheque).ToString())
                    {
                        isUsedChequeTT = true;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return isUsedChequeTT;
    }
    #endregion

    #region checkBadge
    private bool checkBadge(string showid)
    {
        bool isBadgeExist = false;
        try
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            if (checkingBadgeShowName.Contains(shw.SHW_Name))
            {
                isBadgeExist = true;
            }
        }
        catch (Exception ex)
        { }

        return isBadgeExist;
    }
    #endregion

    #region checkReceiptInVisible
    private bool checkReceiptInVisible(string showid)
    {
        bool isBadgeExist = true;
        try
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            if (checkingReceiptInVisibleShowName.Contains(shw.SHW_Name))
            {
                isBadgeExist = false;
            }
        }
        catch (Exception ex)
        { }

        return isBadgeExist;
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            this.GKeyMaster.ExportSettings.ExportOnlyData = true;
            this.GKeyMaster.ExportSettings.IgnorePaging = true;
            this.GKeyMaster.ExportSettings.OpenInNewWindow = true;


            this.GKeyMaster.MasterTableView.ExportToExcel();
        }
    }
    #endregion

    /// <summary> 
    /// Item databound for the radgrid, set the header text here based on the form management ***Item[UniqueName]
    /// </summary>
    protected void GKeyMaster_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        Session["pgno"] = GKeyMaster.CurrentPageIndex + 1;

        //if (!string.IsNullOrEmpty(txtKey.Text) && !string.IsNullOrWhiteSpace(txtKey.Text))
        //{
        //    btnKeysearch_Click(this, null);
        //}
        //else
        //{
        //    if (string.IsNullOrEmpty(txtFromDate.Text.Trim()) || string.IsNullOrEmpty(txtToDate.Text.Trim()))
        //    {
        //        MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //        msregIndiv.ShowID = showID;
        //        msregIndiv.FlowID = flowID;
        //        GKeyMaster.DataSource = msregIndiv.getDataAllRegList();
        //    }
        //    else
        //    {
        //        GKeyMaster.DataSource = getDataAllRegList(txtFromDate.Text.Trim(), txtToDate.Text.Trim());
        //    }
        //}
    }

    #region delRecord
    private int delRecord(string regno)
    {
        int isDeleted = 0;
        if (!string.IsNullOrEmpty(showID))
        {
            try
            {
                //isDeleted = fn.ExecuteSQL(string.Format("Update tb_RegGroup Set recycle=1 Where RegGroupID={0} And ShowID='{1}'", groupid, showid));
                isDeleted += fn.ExecuteSQL(string.Format("Update tb_RegDelegate Set recycle=1 Where Regno={0} And ShowID='{1}'", regno, showID));
            }
            catch (Exception ex)
            {

            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

        return isDeleted;
    }
    #endregion

    protected void ddl_paymentStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        //txtFromDate.Text = "";
        //txtToDate.Text = "";
        GKeyMaster.Rebind();
    }

    protected void ddl_showList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;

        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            //using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID = '" + showID + "' and Status='Active'"))
            using (SqlCommand cmd = new SqlCommand("Select Distinct FLW_ID,FLW_Desc From tb_site_flow_master as fm Inner Join tb_site_flow as f On fm.FLW_ID=f.flow_id where ShowID = '" + showID + "' and fm.Status='Active' And f.script_id Like '%Conference%' And fm.FLW_Desc Not Like '%Old%'"))/*changed by than on 17-7-2019*/
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_flowList.Items.Clear();
                ddl_flowList.DataSource = cmd.ExecuteReader();
                ddl_flowList.DataTextField = "FLW_Desc";
                ddl_flowList.DataValueField = "FLW_ID";
                ddl_flowList.DataBind();
                con.Close();
            }
        }
        flowID = ddl_flowList.SelectedValue;

        FlowControler Flw = new FlowControler(fn);//***MDA
        FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowID);
        if (checkingmdaFlowName.Contains(flwMasterConfig.FlowName))
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") == null)
            {
                ddl_paymentStatus.Items.Add("Pre-populate");
                ddl_paymentStatus.Items[ddl_paymentStatus.Items.Count - 1].Value = "-20";
            }
            else
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = true;
            }
        }
        else
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") != null)
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = false;

                ddl_paymentStatus.SelectedIndex = 0;
            }
        }//***MDA

        GKeyMaster.Rebind();
    }

    protected void ddl_flowList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;
        flowID = ddl_flowList.SelectedValue;
        txtFromDate.Text = "";
        txtToDate.Text = "";
        FlowControler Flw = new FlowControler(fn);//***MDA
        FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowID);
        if (checkingmdaFlowName.Contains(flwMasterConfig.FlowName))
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") == null)
            {
                ddl_paymentStatus.Items.Add("Pre-populate");
                ddl_paymentStatus.Items[ddl_paymentStatus.Items.Count - 1].Value = "-20";
            }
            else
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = true;
            }
        }
        else
        {
            if (ddl_paymentStatus.Items.FindByValue("-20") != null)
            {
                ddl_paymentStatus.Items.FindByValue("-20").Enabled = false;

                ddl_paymentStatus.SelectedIndex = 0;
            }
        }//***MDA

        GKeyMaster.Rebind();
    }
    protected void RadGd1_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem)e.Item;
            string edit_RegGroupID = item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["RegGroupID"].ToString() : "";
            string edit_Regno = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"] != null ? item.OwnerTableView.DataKeyValues[item.ItemIndex]["Regno"].ToString() : "";
            if (!string.IsNullOrEmpty(edit_Regno))
            {

                ImageButton downloadlink = (ImageButton)item["Download"].Controls[0];
                downloadlink.Attributes.Add
               ("onclick", "window.open('../AcknowledgeLetter.aspx?DID=" + cFun.EncryptValue(edit_Regno) +
               "&SHW=" + cFun.EncryptValue(showID) + "'); return false");


            }


        }
    }

    public void GetDetailReport(string qnaireID)
    {
        string flwid = ddl_flowList.SelectedValue;
        FlowControler flwControl = new FlowControler(fn);
        FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flwid);
        string logid = string.Empty;
        string item = string.Empty;
        StringBuilder str = new StringBuilder();
        string sqlQ = "select * from gen_Question where status='Active' and quest_class <> 'Header' and quest_id in (select top 500 quest_id from gen_QnaireQuest where qnaire_id='" + qnaireID + "' And status='Active' order by qnaire_seq)";
        DataTable dtQ = qfn.GetDatasetByCommand(sqlQ, "sdtQ").Tables[0];
        //Regno	Salutation	First Name	Last Name	Company	Job Title	Email	Mobile	Address	Country	Type	Category

        TransmitData(qnaireID);//*****QA Data Transmit

        #region MDA
        //str.Append("<tr>");
        if (checkingmdaFlowName.Contains(flwMasterConfig.FlowName))//***MDA
        {
            #region MDA
            str.Append("<tr>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "No" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Registration ID" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Created Date" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Update Date" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Registration Status" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Salutation" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "FirstName" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Last Name" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Job Title" + "</td>");
            //str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Department" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Name" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Address Line1" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Address Line2" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Address Line3" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Postcode" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "City" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "State/Province" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Country" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Tel No Code" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Tel No" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Mobile Code" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Mobile No" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Fax Code" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Faxno" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Company Email" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Email Alternate" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Website" + "</td>");
            str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "Promo Code" + "</td>");
            #endregion

            if (checkingmdaFlowName.Contains(flwMasterConfig.FlowName))//***MDA-check New/Repeat Visitor
            {
                str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "New/Repeat Visitor" + "</td>");
            }//***MDA-check New/Repeat Visitor

            if (dtQ.Rows.Count > 0)
            {
                for (int q = 0; q < dtQ.Rows.Count; q++)
                {
                    int span = GetQuestCount(dtQ.Rows[q]["quest_id"].ToString());
                    str.Append("<td colspan='" + span + "' style='background-color:#b3b3b3'>" + dtQ.Rows[q]["quest_desc"].ToString() + "</td>");
                }
            }
            str.Append("</tr>");
            str.Append("<tr>");
            if (dtQ.Rows.Count > 0)
            {
                for (int q = 0; q < dtQ.Rows.Count; q++)
                {
                    string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
                    DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
                    if (dtI.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtI.Rows.Count; i++)
                        {
                            str.Append("<td style='background-color:#b3b3b3'>" + dtI.Rows[i]["qitem_desc"].ToString() + "</td>");
                        }
                    }
                }
            }
            str.Append("</tr>");
            string sqlU = "select * from tbl" + qnaireID + " order by user_reference";
            DataTable dtU = qfn.GetDatasetByCommand(sqlU, "sdtU").Tables[0];

            DataTable dtDelegateAll = new DataTable();
            if (ddl_paymentStatus.SelectedItem.Value == "-20")//***MDA
            {
                string sqlDelegateAll = "select * from  tb_RegDelegate where reg_urlFlowID='" + ddl_flowList.SelectedValue + "' and recycle='0' " + checkDateForExcel()
                    + " And Regno In (Select RefNo From tb_RegPrePopulate Where (recycle=0 Or recycle Is Null) And ShowID='" + showID
                    + "' And reg_urlFlowID='" + ddl_flowList.SelectedValue + "'"
                    + " And (RefNo Is Not Null And RefNo<>'')) order by Regno";// full outer join ref_Salutation on reg_Salutation=Sal_ID full outer join ref_country on reg_Country=Cty_GUID and reg_Status='1' 
                dtDelegateAll = fn.GetDatasetByCommand(sqlDelegateAll, "sdtDelegateAll").Tables[0];
            }//***MDA
            else
            {
                string sqlDelegateAll = "select * from  tb_RegDelegate where reg_urlFlowID='" + ddl_flowList.SelectedValue + "' and recycle='0' " + checkDateForExcel() + " order by Regno";// full outer join ref_Salutation on reg_Salutation=Sal_ID full outer join ref_country on reg_Country=Cty_GUID and reg_Status='1' 
                dtDelegateAll = fn.GetDatasetByCommand(sqlDelegateAll, "sdtDelegateAll").Tables[0];
            }

            string sqlA1 = "select * from gen_QuestItem  where  status='Active' and quest_id in (select quest_id from gen_QnaireQuest where qnaire_id='" + qnaireID + "' and status='Active' and quest_id in(select quest_id from gen_Question where quest_class<>'Header'))";
            DataTable dtA1 = qfn.GetDatasetByCommand(sqlA1, "sdtA1").Tables[0];
            if (dtDelegateAll.Rows.Count > 0)
            {
                for (int da = 0; da < dtDelegateAll.Rows.Count; da++)
                {
                    if (dtU.Rows.Count > 0)
                    {
                        string daregno = dtDelegateAll.Rows[da]["Regno"].ToString();
                        string sqlDelegate = "select * from  tb_RegDelegate as d full outer join tb_RegCompany as c on d.RegGroupID=c.RegGroupID where Regno in ('" + daregno + "') ";//full outer join ref_Salutation on d.reg_Salutation=Sal_ID full outer join ref_country on d.reg_Country=Cty_GUID 
                        DataTable dtDelegate = fn.GetDatasetByCommand(sqlDelegate, "sdtDelegate").Tables[0];
                        if (dtDelegate.Rows.Count > 0)
                        {
                            for (int d = 0; d < dtDelegate.Rows.Count; d++)
                            {
                                str.Append("<tr>");
                                str.Append("<td>" + (count + 1).ToString() + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["Regno"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_datecreated"] + "</td>");
                                str.Append("<td> </td>");
                                str.Append("<td>" + getInvoiceStatus(dtDelegate.Rows[d]["reg_Status"] != DBNull.Value ? dtDelegate.Rows[d]["reg_Status"].ToString() : "0") + "</td>");
                                string salutationother = dtDelegate.Rows[d]["reg_Salutation"] != DBNull.Value ? dtDelegate.Rows[d]["reg_SalutationOthers"].ToString() : "";
                                str.Append("<td>" + bindSalutation((dtDelegate.Rows[d]["reg_Salutation"] != DBNull.Value ? dtDelegate.Rows[d]["reg_Salutation"].ToString() : ""), salutationother) + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_FName"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_LName"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Designation"] + "</td>");
                                //str.Append("<td>" + bindDepartment((dtDelegate.Rows[d]["reg_Department"] != DBNull.Value ? dtDelegate.Rows[d]["reg_Department"].ToString() : "") , (dtDelegate.Rows[d]["reg_otherDepartment"] != DBNull.Value ? dtDelegate.Rows[d]["reg_otherDepartment"].ToString() : "")) + "</td>");

                                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                                {
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Name"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Address1"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Address2"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Address3"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_ZipCode"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_City"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_State"] + "</td>");
                                    string indivCountry = dtDelegate.Rows[d]["reg_Country"] != null ? dtDelegate.Rows[d]["reg_Country"].ToString() : "";
                                    string comCountry = dtDelegate.Rows[d]["RC_Country"] != null ? dtDelegate.Rows[d]["RC_Country"].ToString() : "";
                                    string countryName = bindCountry(!string.IsNullOrEmpty(indivCountry) && indivCountry != "0" ? indivCountry : comCountry);
                                    str.Append("<td>" + countryName + "</td>");
                                }
                                else
                                {
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Address1"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Address2"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Address3"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Address4"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_PostalCode"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_City"] + "</td>");
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_State"] + "</td>");
                                    str.Append("<td>" + bindCountry(dtDelegate.Rows[d]["reg_Country"] != DBNull.Value ? dtDelegate.Rows[d]["reg_Country"].ToString() : "") + "</td>");
                                }

                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Telcc"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Telac"] + "-" + dtDelegate.Rows[d]["reg_Tel"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Mobcc"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Mobac"] + "-" + dtDelegate.Rows[d]["reg_Mobile"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Faxcc"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Faxac"] + "-" + dtDelegate.Rows[d]["reg_Fax"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Email"] + "</td>");
                                str.Append("<td>" + dtDelegate.Rows[d]["reg_Additional5"] + "</td>");
                                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                                {
                                    str.Append("<td>" + dtDelegate.Rows[d]["RC_Website"] + "</td>");
                                }
                                else
                                {
                                    str.Append("<td>" + dtDelegate.Rows[d]["reg_Additional4"] + "</td>");
                                }

                                str.Append("<td>" + getPromoCode(dtDelegate.Rows[d]["RegGroupID"].ToString()) + "</td>");

                                if (checkingmdaFlowName.Contains(flwMasterConfig.FlowName))//***MDA-check New/Repeat Visitor
                                {
                                    str.Append("<td>" + checkNewVisitorMDA(dtDelegate.Rows[d]["Regno"].ToString(), qnaireID) + "</td>");
                                }//***MDA-check New/Repeat Visitor

                                count++;
                            }
                        }
                        #region Old (Comment)
                        //if (dtU.Rows.Count > 0)
                        //{
                        //    for (int a = 0; a < dtU.Rows.Count; a++)
                        //    {
                        //        #region old
                        //        //if (logid != dtA1.Rows[a]["qitem_id"].ToString() && item != dtU.Rows[u]["qnaire_log_id"].ToString() && !string.IsNullOrEmpty(dtA1.Rows[a]["qitem_class"].ToString()))
                        //        //{
                        //        //    var res = fn.GetDataByCommand("select  count(qnaire_log_id) as R from gen_QnaireResult where qitem_id='" + dtA1.Rows[a]["qitem_id"] + "' and qnaire_log_id='" + dtU.Rows[u]["qnaire_log_id"] + "'", "R");
                        //        //    if (res == "0")
                        //        //    {
                        //        //        res = "";
                        //        //    }
                        //        //    str.Append("<td>" + res + "</td>");
                        //        //    item = dtA1.Rows[a]["qitem_id"].ToString();
                        //        //    logid = dtU.Rows[u]["qnaire_log_id"].ToString();
                        //        //}
                        //        #endregion
                        //        #region new from bex
                        //        if (item != dtU.Rows[0]["qnaire_log_id"].ToString())
                        //        {
                        //            string sqlresult = "SELECT * INTO #TempTable FROM tblQLST10081 "
                        //                       + "ALTER TABLE #TempTable "
                        //                       + "DROP COLUMN qnaire_log_id,user_reference,status,create_date,update_date "
                        //                       + "SELECT * FROM #TempTable";
                        //            DataTable dtresult = fn.GetDatasetByCommand(sqlresult, "sdtResult").Tables[0];
                        //            if (dtresult.Rows.Count > 0)
                        //            {
                        //                foreach (DataRow drR in dtresult.Rows)
                        //                {
                        //                    str.Append("<td>" + drR[] + "</td>");
                        //                }
                        //            }

                        //            item = dtA1.Rows[a]["qitem_id"].ToString();
                        //            logid = dtU.Rows[u]["qnaire_log_id"].ToString();

                        //        }

                        //    }

                        //}
                        #endregion
                        string qnaireid = (from DataRow dr in dtU.Rows
                                           where (string)dr["user_reference"] == daregno
                                           select (string)dr["qnaire_log_id"]).FirstOrDefault();
                        if (!string.IsNullOrEmpty(qnaireid))
                        {
                            string sqlResult = "SELECT * INTO #TempTable FROM tbl" + qnaireID + " where user_reference='" + daregno + "' "
                                                    + "ALTER TABLE #TempTable "
                                                    + "DROP COLUMN qnaire_log_id,user_reference,status,create_date,update_date "
                                                    + "SELECT * FROM #TempTable";
                            DataTable dtResut = qfn.GetDatasetByCommand(sqlResult, "sqResult").Tables[0];
                            if (dtResut.Rows.Count > 0)
                            {
                                foreach (DataRow drR in dtResut.Rows)
                                {
                                    foreach (DataColumn dc in dtResut.Columns)
                                    {
                                        string hutt = drR[dc].ToString();
                                        if (hutt == "0")
                                        {
                                            str.Append("<td></td>");
                                        }
                                        else
                                        {
                                            str.Append("<td>" + hutt + "</td>");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (dtQ.Rows.Count > 0)
                            {
                                for (int q = 0; q < dtQ.Rows.Count; q++)
                                {
                                    string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
                                    DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
                                    if (dtI.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtI.Rows.Count; i++)
                                        {
                                            str.Append("<td></td>");
                                        }
                                    }

                                }
                            }
                        }
                        str.Append("</tr>");
                    }
                }
            }
            htmltb += str.ToString();
        }
        #endregion

        #region Other Show
        else
        {
            int status = Convert.ToInt16(ddl_paymentStatus.SelectedValue);

            MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
            msregIndiv.ShowID = showID;
            msregIndiv.FlowID = flowID;
            DataTable dtReg = new DataTable();
            if (status == -1)
            {
                DataTable dt = msregIndiv.getDataAllRegList();
                DataView dv = new DataView();
                dv = dt.DefaultView;
                dv.Sort = "reg_datecreated DESC";
                dt = dv.ToTable();
                dtReg = dt;// msregIndiv.getDataAllRegList();
            }
            else if (status == -20)//***MDA
            {
                DataTable dtRegList = msregIndiv.getDataAllRegList();
                DataTable dtPreRegList = getPrepopulateDataByShowFlow(showID, flowID);

                DataTable TableC = dtRegList.AsEnumerable()
                        .Where(ra => dtPreRegList.AsEnumerable()
                                            .Any(rb => rb.Field<int>("RefNo") == ra.Field<int>("Regno")))
                        .CopyToDataTable();

                DataView dv = new DataView();
                dv = TableC.DefaultView;
                dv.Sort = "reg_datecreated DESC";
                TableC = dv.ToTable();
                dtReg = TableC;
            }//***MDA
            else
            {
                DataTable dt = msregIndiv.getDataByPaymentStatus(status);
                DataView dv = new DataView();
                dv = dt.DefaultView;
                dv.Sort = "reg_datecreated DESC";
                dt = dv.ToTable();
                dtReg = dt;// msregIndiv.getDataByPaymentStatus(status);
            }

            if (dtReg.Rows.Count > 0)
            {
                try
                {
                    #region header
                    #region checkConference Exist
                    //FlowControler Flw = new FlowControler(fn);
                    //bool confExist = Flw.checkPageExist(flowID, SiteDefaultValue.constantConfName);
                    //if (!confExist)
                    //{
                    //    this.GKeyMaster.MasterTableView.GetColumn("CongressSelection").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("PaymentMethod").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("Invoice_grandtotal").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("PaidPrice").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("Outstanding").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("Invoice_discount").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("InvoiceStatus").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("UpdatePayment").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("DownloadInvoice").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("DownloadReceipt").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmail").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("DownloadBadge").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("Download").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmailNormal").Display = false;//***for FJ
                    //    GKeyMaster.ExportSettings.ExportOnlyData = true;

                    //    if (string.IsNullOrEmpty(txtKey.Text) && string.IsNullOrWhiteSpace(txtKey.Text)
                    //        && string.IsNullOrEmpty(txtFromDate.Text) && string.IsNullOrWhiteSpace(txtFromDate.Text)
                    //        && string.IsNullOrEmpty(txtToDate.Text) && string.IsNullOrWhiteSpace(txtToDate.Text))
                    //    {
                    //    }
                    //    GKeyMaster.AllowPaging = false;
                    //    GKeyMaster.Rebind();

                    //    NewDt.Columns.Add("No", typeof(string));
                    //    //str.Append("<td style='background-color:#b3b3b3'>" + "No" + "</td>");//rowspan='2' 
                    //    //foreach (GridDataItem row in GKeyMaster.Items) // loops through each rows in RadGrid
                    //    {
                    //        foreach (GridColumn col in GKeyMaster.Columns) //loops through each column in RadGrid
                    //        {
                    //            if (col.Visible == true && col.Display == true)
                    //            {
                    //                //str.Append("<td style='background-color:#b3b3b3'>" + col.HeaderText + "</td>");//rowspan='2' 
                    //                NewDt.Columns.Add(col.HeaderText, typeof(string));
                    //            }
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    this.GKeyMaster.MasterTableView.GetColumn("CongressSelection").Display = true;
                    //    this.GKeyMaster.MasterTableView.GetColumn("PaymentMethod").Display = true;
                    //    this.GKeyMaster.MasterTableView.GetColumn("Invoice_grandtotal").Display = true;
                    //    this.GKeyMaster.MasterTableView.GetColumn("PaidPrice").Display = true;
                    //    this.GKeyMaster.MasterTableView.GetColumn("Outstanding").Display = true;
                    //    this.GKeyMaster.MasterTableView.GetColumn("Invoice_discount").Display = true;
                    //    this.GKeyMaster.MasterTableView.GetColumn("InvoiceStatus").Display = true;
                    //    this.GKeyMaster.MasterTableView.GetColumn("UpdatePayment").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("DownloadInvoice").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("DownloadReceipt").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmail").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("DownloadBadge").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("Download").Display = false;
                    //    this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmailNormal").Display = false;//***for FJ
                    //    GKeyMaster.ExportSettings.ExportOnlyData = true;
                    //}
                    #endregion

                    this.GKeyMaster.MasterTableView.GetColumn("Download").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("Delete").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("Edit").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("UpdatePayment").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("DownloadInvoice").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("DownloadReceipt").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("SendConfirmationEmail").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("DownloadBadge").Display = false;
                    this.GKeyMaster.MasterTableView.GetColumn("SendVisitorConfirmationEmail").Display = false;//added on 18-12-2018 (for visitor reg)
                    this.GKeyMaster.MasterTableView.GetColumn("IsNewVisitorMDA").Display = false;//***MDA-check New/Repeat Visitor
                    this.GKeyMaster.ExportSettings.ExportOnlyData = true;
                    this.GKeyMaster.AllowPaging = false;
                    this.GKeyMaster.Rebind();
                    //NewDt.Columns.Add("No", typeof(string));
                    str.Append("<tr>");
                    str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + "No" + "</td>");
                    //foreach (GridDataItem row in GKeyMaster.Items) // loops through each rows in RadGrid
                    {
                        foreach (GridColumn col in GKeyMaster.Columns) //loops through each column in RadGrid
                        {
                            if (col.Visible == true && col.Display == true)
                            {
                                if (col.HeaderText != "")
                                {
                                    if (col.UniqueName != "Download" && col.UniqueName != "Delete"
                                        && col.UniqueName != "Edit" && col.UniqueName != "UpdatePayment"
                                        && col.UniqueName != "DownloadInvoice" && col.UniqueName != "DownloadReceipt"
                                        && col.UniqueName != "SendConfirmationEmail" && col.UniqueName != "DownloadBadge"
                                        && col.UniqueName != "IsNewVisitorMDA"
                                        && col.UniqueName != "SendVisitorConfirmationEmail")
                                    {
                                        //NewDt.Columns.Add(col.HeaderText, typeof(string));
                                        str.Append("<td rowspan='2' style='background-color:#b3b3b3'>" + col.HeaderText + "</td>");
                                    }
                                }
                            }
                        }
                    }

                    #region QA header
                    if (dtQ.Rows.Count > 0)
                    {
                        for (int q = 0; q < dtQ.Rows.Count; q++)
                        {
                            int span = GetQuestCount(dtQ.Rows[q]["quest_id"].ToString());
                            //NewDt.Columns.Add(dtQ.Rows[q]["quest_desc"].ToString(), typeof(string));
                            str.Append("<td colspan='" + span + "' style='background-color:#b3b3b3'>" + dtQ.Rows[q]["quest_desc"].ToString() + "</td>");
                        }
                    }
                    str.Append("</tr>");
                    str.Append("<tr>");
                    if (dtQ.Rows.Count > 0)
                    {
                        for (int q = 0; q < dtQ.Rows.Count; q++)
                        {
                            string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
                            DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
                            if (dtI.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtI.Rows.Count; i++)
                                {
                                    //NewDt.Columns.Add(dtI.Rows[i]["qitem_desc"].ToString(), typeof(string));
                                    str.Append("<td style='background-color:#b3b3b3'>" + dtI.Rows[i]["qitem_desc"].ToString() + "</td>");
                                }
                            }
                        }
                    }
                    #endregion
                    str.Append("</tr>");
                    #endregion

                    string sqlU = "select * from tbl" + qnaireID + " order by user_reference";
                    DataTable dtU = qfn.GetDatasetByCommand(sqlU, "sdtU").Tables[0];

                    foreach (GridDataItem row in GKeyMaster.Items) // loops through each rows in RadGrid
                    {
                        //DataRow newdr = NewDt.NewRow();
                        //newdr["No"] = (count + 1).ToString();
                        str.Append("<tr>");
                        str.Append("<td>" + (count + 1).ToString() + "</td>");

                        string regno = string.Empty;

                        foreach (GridColumn col in GKeyMaster.Columns) //loops through each column in RadGrid
                        {
                            if (col.Visible == true && col.Display == true)
                            {
                                if (col.UniqueName != "Download" && col.UniqueName != "Delete"
                                        && col.UniqueName != "Edit" && col.UniqueName != "UpdatePayment"
                                        && col.UniqueName != "DownloadInvoice" && col.UniqueName != "DownloadReceipt"
                                        && col.UniqueName != "SendConfirmationEmail" && col.UniqueName != "DownloadBadge"
                                        && col.UniqueName != "IsNewVisitorMDA"
                                        && col.UniqueName != "SendVisitorConfirmationEmail")
                                {
                                    //var test = DataBinder.Eval(row.DataItem, col.UniqueName).ToString();
                                    string dataValue = row[col.UniqueName].Text;
                                    if (col.UniqueName == "Regno")
                                    {
                                        regno = dataValue;
                                    }

                                    if (col.UniqueName == "reg_Salutation")
                                    {
                                        //DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        //dataValue = bindSalutation(drReg[0]["reg_Salutation"].ToString(), drReg[0]["reg_SalutationOthers"].ToString());
                                    }
                                    else if (col.UniqueName == "reg_Department")
                                    {
                                        //DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        //dataValue = bindDepartment(drReg[0]["reg_Department"].ToString(), drReg[0]["reg_otherDepartment"].ToString());
                                    }
                                    else if (col.UniqueName == "reg_Organization")
                                    {
                                        //DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        //dataValue = bindOrganisation(drReg[0]["reg_Organization"].ToString(), drReg[0]["reg_otherOrganization"].ToString());
                                    }
                                    else if (col.UniqueName == "reg_Profession")
                                    {
                                        //DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        //dataValue = bindProfession(drReg[0]["reg_Profession"].ToString(), drReg[0]["reg_otherProfession"].ToString());
                                    }
                                    else if (col.UniqueName == "reg_Institution")
                                    {
                                        //DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        //dataValue = bindInstitution(drReg[0]["reg_Institution"].ToString(), drReg[0]["reg_otherInstitution"].ToString());
                                    }
                                    else if (col.UniqueName == "reg_Country")
                                    {
                                        //DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        //dataValue = bindCountry(drReg[0]["reg_Country"].ToString());
                                    }
                                    else if (col.UniqueName == "reg_RCountry")
                                    {
                                        DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        dataValue = bindCountry(drReg[0]["reg_RCountry"].ToString());
                                    }
                                    else if (col.UniqueName == "reg_Tel")
                                    {
                                        DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        dataValue = drReg[0]["reg_Telcc"].ToString() + drReg[0]["reg_Telac"].ToString() + drReg[0]["reg_Tel"].ToString();
                                    }
                                    else if (col.UniqueName == "reg_Mobile")
                                    {
                                        DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        dataValue = drReg[0]["reg_Mobcc"].ToString() + drReg[0]["reg_Mobac"].ToString() + drReg[0]["reg_Mobile"].ToString();
                                    }
                                    else if (col.UniqueName == "reg_Fax")
                                    {
                                        DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        dataValue = drReg[0]["reg_Faxcc"].ToString() + drReg[0]["reg_Faxac"].ToString() + drReg[0]["reg_Fax"].ToString();
                                    }
                                    if (col.UniqueName == "reg_Affiliation")
                                    {
                                        //DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        //dataValue = bindAffiliation(drReg[0]["reg_Affiliation"].ToString());
                                    }
                                    if (col.UniqueName == "reg_Dietary")
                                    {
                                        //DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        //dataValue = bindDietary(drReg[0]["reg_Dietary"].ToString());
                                    }
                                    if (col.UniqueName == "reg_DOB")
                                    {
                                        DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        dataValue = drReg[0]["reg_DOB"].ToString();// bindDOBVender(drReg[0]["reg_DOB"].ToString(), showID);
                                    }
                                    else if (col.UniqueName == "status_name")
                                    {
                                        DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        dataValue = !string.IsNullOrEmpty(drReg[0]["status_name"].ToString()) ? drReg[0]["status_name"].ToString() : "Pending";
                                    }
                                    else if (col.UniqueName == "CongressSelection")
                                    {
                                        DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        dataValue = getCongressSelection(drReg[0]["Regno"].ToString(), drReg[0]["RegGroupID"].ToString(), drReg[0]["Invoice_status"].ToString(), drReg[0]["InvoiceID"].ToString());
                                    }
                                    else if (col.UniqueName == "PaymentMethod")
                                    {
                                        DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        dataValue = getPaymentMethod(drReg[0]["PaymentMethod"].ToString(), drReg[0]["showid"].ToString());
                                    }
                                    else if (col.UniqueName == "PaidPrice")
                                    {
                                        DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        dataValue = getPaidPrice(drReg[0]["InvoiceID"].ToString());
                                    }
                                    else if (col.UniqueName == "Outstanding")
                                    {
                                        if(regno == "36710237")
                                        {

                                        }
                                        DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        dataValue = calculateOutstanding(drReg[0]["Invoice_grandtotal"].ToString(), drReg[0]["InvoiceID"].ToString());
                                    }
                                    else if (col.UniqueName == "InvoiceStatus")
                                    {
                                        DataRow[] drReg = dtReg.Select("Regno='" + regno + "'");
                                        dataValue = getInvoiceStatus(drReg[0]["Invoice_status"].ToString());
                                    }
                                    else
                                    {
                                        if (row[col.UniqueName].Text == "&nbsp;")
                                        {
                                            if (row.DataItem == null)
                                            { }
                                            else
                                            {
                                                dataValue = DataBinder.Eval(row.DataItem, col.UniqueName).ToString();
                                            }
                                        }
                                    }

                                    //newdr[col.HeaderText] = dataValue;
                                    str.Append("<td>" + dataValue + "</td>");
                                }
                            }
                        }

                        #region QA
                        string daregno = row["Regno"].Text;
                        string qnaireid = (from DataRow dr in dtU.Rows
                                           where (string)dr["user_reference"] == daregno
                                           select (string)dr["qnaire_log_id"]).FirstOrDefault();
                        if (!string.IsNullOrEmpty(qnaireid))
                        {
                            string sqlResult = "SELECT * INTO #TempTable FROM tbl" + qnaireID + " where user_reference='" + daregno + "' "
                                                    + "ALTER TABLE #TempTable "
                                                    + "DROP COLUMN qnaire_log_id,user_reference,status,create_date,update_date "
                                                    + "SELECT * FROM #TempTable";
                            DataTable dtResut = qfn.GetDatasetByCommand(sqlResult, "sqResult").Tables[0];
                            if (dtResut.Rows.Count > 0)
                            {
                                foreach (DataRow drR in dtResut.Rows)
                                {
                                    foreach (DataColumn dc in dtResut.Columns)
                                    {
                                        string hutt = drR[dc].ToString();
                                        if (hutt == "0")
                                        {
                                            str.Append("<td></td>");
                                        }
                                        else
                                        {
                                            str.Append("<td>" + hutt + "</td>");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (dtQ.Rows.Count > 0)
                            {
                                for (int q = 0; q < dtQ.Rows.Count; q++)
                                {
                                    string sqlI = "select * from gen_QuestItem where status='Active' and quest_id='" + dtQ.Rows[q]["quest_id"].ToString() + "'";
                                    DataTable dtI = qfn.GetDatasetByCommand(sqlI, "sdtI").Tables[0];
                                    if (dtI.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dtI.Rows.Count; i++)
                                        {
                                            str.Append("<td></td>");
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        //NewDt.Rows.Add(newdr);
                        str.Append("</tr>");
                        count++;
                    }
                }
                catch (Exception ex)
                {

                }
                htmltb += str.ToString();
            }
        }
        #endregion
    }

    protected void lnkExcel_Clicked(object sender, EventArgs e)
    {
        try
        {
            this.GKeyMaster.ExportSettings.ExportOnlyData = true;
            this.GKeyMaster.ExportSettings.IgnorePaging = true;
            this.GKeyMaster.ExportSettings.OpenInNewWindow = true;
            this.GKeyMaster.MasterTableView.ExportToExcel();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }
    }

    public void ExportExcel(string QnaireID)
    {
        try
        {
            string flowName = ddl_flowList.SelectedItem.Text;
            string name = flowName.Replace(" ", "");
            StringBuilder StrBuilder = new StringBuilder();
            htmltb = string.Empty;
            GetDetailReport(QnaireID);
            StrBuilder.Append("<table cellpadding='0' cellspacing='0' border='1' class='Messages' id='idTbl' runat='server'>");
            StrBuilder.Append(htmltb.ToString());
            StrBuilder.Append("</table>");
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/msexcel";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode;
            HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            HttpContext.Current.Response.AddHeader("Content-Disposition", "filename=Delegate-Detailed-Report-" + name + ".xls");
            HttpContext.Current.Response.Write(StrBuilder);
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush();
        }
        catch (System.Threading.ThreadAbortException exf)
        {

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);

        }
    }

    public string GetQIDFromFLW(string flowID)
    {
        string result = string.Empty;

        string query = "select SQ_QAID from tb_site_QA_Master where SQ_FLW_ID='" + flowID + "' and SQ_Category='D'";
        result = fn.GetDataByCommand(query, "SQ_QAID");

        return result;
    }

    public int GetQuestCount(string questID)
    {
        int Qcount = 0;
        string sql = "select * from gen_QuestItem where status='Active' and quest_id='" + questID + "'";
        DataSet ds = new DataSet();
        ds = qfn.GetDatasetByCommand(sql, "dsds");

        Qcount = ds.Tables[0].Rows.Count;

        return Qcount;

    }

    #region bindName
    public string bindSalutation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getSalutationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindProfession(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getProfessionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDepartment(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDepartmentNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindOrganisation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getOrganisationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindInstitution(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getInstitutionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindCountry(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    CountryObj conCtr = new CountryObj(fn);
                    name = conCtr.getCountryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindAffiliation(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getAffiliationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDietary(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDietaryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindIndustry(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getIndustryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    #region bindPhoneNo
    public string bindPhoneNo(string cc, string ac, string phoneno, string type)
    {
        string name = string.Empty;
        bool isShowCC = false, isShowAC = false, isShowPhoneNo = false;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string flowid = ddl_flowList.SelectedItem.Value;
                if (!string.IsNullOrEmpty(showID))
                {
                    DataSet ds = new DataSet();
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showID;
                    frmObj.flowID = flowid;
                    ds = frmObj.getDynFormForDelegate();

                    for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                    {
                        if (type == "Tel")
                        {
                            #region type="Tel"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                        else if (type == "Mob")
                        {
                            #region type="Mob"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                        else if (type == "Fax")
                        {
                            #region Type="Fax"
                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowCC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowAC = true;
                                }
                            }

                            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                            {
                                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                                if (isshow == 1)
                                {
                                    isShowPhoneNo = true;
                                }
                            }
                            #endregion
                        }
                    }

                    if (isShowCC)
                    {
                        name = "+" + cc;
                    }
                    if (isShowAC)
                    {
                        name += " " + ac;
                    }
                    if (isShowPhoneNo)
                    {
                        name += " " + phoneno;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion

    #region Invoice
    public string getInvoiceStatus(string invStatus)
    {
        string result = string.Empty;
        try
        {
            //InvoiceControler invControler = new InvoiceControler(fn);
            //StatusSettings stuSettings = new StatusSettings(fn);
            //string invStatus = invControler.getInvoiceStatus(regno);
            string sqlStatus = "select status_name from ref_Status Where status_usedid='" + invStatus + "'";
            result = fn.GetDataByCommand(sqlStatus, "status_name");
            if (result == "0" || string.IsNullOrEmpty(result))
            {
                result = "Pending";
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getPaymentMethod(string paymentMethod, string showid)
    {
        string result = string.Empty;
        try
        {
            string sqlStatus = "Select method_name From ref_PaymentMethod Where method_usedid='" + paymentMethod + "' And ShowID='" + showid + "'";//tmp_refPaymentMethod [changed on 27-3-2019 th]
            result = fn.GetDataByCommand(sqlStatus, "method_name");
            if (result == "0" || string.IsNullOrEmpty(result))
            {
                result = "N/A";
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getPaidPrice(string InvoiceID)
    {
        string result = string.Empty;
        try
        {
            PaymentControler pControl = new PaymentControler(fn);
            decimal totalPaidAmt = pControl.GetPaymentTotalPaidAmtByInvNo(InvoiceID);
            result = String.Format("{0:f2}", totalPaidAmt);
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string calculateOutstanding(string grandTotal, string InvoiceID)
    {
        string result = string.Empty;
        try
        {
            Double grandtotalAmount = 0;
            if (!string.IsNullOrEmpty(grandTotal))
            {
                Double.TryParse(grandTotal, out grandtotalAmount);
            }
            string paidprice = getPaidPrice(InvoiceID);
            Double paidAmount = 0;
            if (!string.IsNullOrEmpty(paidprice))
            {
                Double.TryParse(paidprice, out paidAmount);
            }
            result = String.Format("{0:f2}", (grandtotalAmount - paidAmount));
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getCongressSelection(string regno, string groupid, string invStatus, string invoiceID)
    {
        string result = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                showID = ddl_showList.SelectedValue;
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                OrderControler oControl = new OrderControler(fn);
                //string currenturl= Request.Url.AbsoluteUri;
                string fullUrl = "GRP=" + cFun.EncryptValue(groupid) + "&INV=" + cFun.EncryptValue(regno) + "&SHW=" + cFun.EncryptValue(showID)
                    + "&FLW=" + cFun.EncryptValue(ddl_flowList.SelectedItem.Value);
                FlowURLQuery urlQuery = new FlowURLQuery(fullUrl);

                StatusSettings stuSettings = new StatusSettings(fn);
                if (invStatus == stuSettings.Pending.ToString()
                    || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString()
                    || invStatus == noInvoice)
                {
                    bool isCorrectOrder = false;
                    OrderItemList oList = new OrderItemList();
                    oList = oControl.GetPendingOrderList(urlQuery);//***Pending Order
                    result = getCongressSelectionPriceString(oList, ddl_flowList.SelectedItem.Value, regno, ref isCorrectOrder);//getCongressSelectionString(oList, ddl_flowList.SelectedItem.Value, regno);
                }
                else
                {
                    List<OrderItemList> oSucList = getPaidOrder(urlQuery, invoiceID);//***Paid Order
                    if (oSucList.Count > 0)
                    {
                        foreach (OrderItemList ordList in oSucList)
                        {
                            bool isCorrectOrder = false;
                            result += getCongressSelectionPriceString(ordList, ddl_flowList.SelectedItem.Value, regno, ref isCorrectOrder);//getCongressSelectionString(ordList, ddl_flowList.SelectedItem.Value, regno);
                            //if(isCorrectOrder)
                            //{
                            //    break;
                            //}
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    private List<OrderItemList> getPaidOrder(FlowURLQuery urlQuery, string invoiceID)
    {
        List<OrderItemList> oPaidOrderItemList = new List<OrderItemList>();
        try
        {
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cFun.DecryptValue(urlQuery.DelegateID);
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj != null && invListObj.Count > 0)
            {
                foreach (Invoice invObj in invListObj)
                {
                    if (invObj.InvoiceID == invoiceID)
                    {
                        OrderControler oControl = new OrderControler(fn);
                        oPaidOrderItemList = oControl.GetAllOrderedListByInvoiceID(urlQuery, invObj.InvoiceID);
                    }
                }
            }
        }
        catch (Exception ex)
        { oPaidOrderItemList = new List<OrderItemList>(); }

        return oPaidOrderItemList;
    }
    private string getCongressSelectionString(OrderItemList OList, string flowid, string regno)
    {
        string selectedCongress = string.Empty;
        try
        {
            foreach (OrderItem oItem in OList.OrderList)
            {
                FlowControler flwControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    if (oItem.RegDelegateID == regno)
                    {
                        if (string.IsNullOrEmpty(HttpUtility.HtmlDecode(oItem.ReportDisplayName)))
                        {
                            selectedCongress += HttpUtility.HtmlDecode(oItem.ItemDescription) + (!string.IsNullOrEmpty(oItem.ItemDescription_ShowedInTemplate) ? "[" + HttpUtility.HtmlDecode(oItem.ItemDescription_ShowedInTemplate) + "]" : "") + (";" + oItem.Qty) + "^";
                        }
                        else
                        {
                            selectedCongress += HttpUtility.HtmlDecode(oItem.ReportDisplayName) + (";" + oItem.Qty) + "^";
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(HttpUtility.HtmlDecode(oItem.ReportDisplayName)))
                    {
                        selectedCongress += HttpUtility.HtmlDecode(oItem.ItemDescription) + (!string.IsNullOrEmpty(oItem.ItemDescription_ShowedInTemplate) ? "[" + HttpUtility.HtmlDecode(oItem.ItemDescription_ShowedInTemplate) + "]" : "") + (";" + oItem.Qty) + "^";
                    }
                    else
                    {
                        selectedCongress += HttpUtility.HtmlDecode(oItem.ReportDisplayName) + (";" + oItem.Qty) + "^";
                    }
                }
            }
            selectedCongress = selectedCongress.TrimEnd('^');
        }
        catch (Exception ex)
        { }

        return selectedCongress;
    }
    private string getCongressSelectionPriceString(OrderItemList OList, string flowid, string regno, ref bool isCorrectOrder)
    {
        string selectedCongress = string.Empty;
        try
        {
            foreach (OrderItem oItem in OList.OrderList)
            {
                FlowControler flwControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    if (oItem.RegDelegateID == regno)
                    {
                        isCorrectOrder = true;
                        if (string.IsNullOrEmpty(HttpUtility.HtmlDecode(oItem.ReportDisplayName)))
                        {
                            selectedCongress += HttpUtility.HtmlDecode(oItem.ItemDescription) + (!string.IsNullOrEmpty(oItem.ItemDescription_ShowedInTemplate) ? "[" + HttpUtility.HtmlDecode(oItem.ItemDescription_ShowedInTemplate) + "]" : "") + (";" + oItem.Price + "!" + oItem.Qty) + "^";
                        }
                        else
                        {
                            selectedCongress += HttpUtility.HtmlDecode(oItem.ReportDisplayName) + (";" + oItem.Price + "!" + oItem.Qty) + "^";
                        }
                    }
                }
                else
                {
                    isCorrectOrder = true;
                    if (string.IsNullOrEmpty(HttpUtility.HtmlDecode(oItem.ReportDisplayName)))
                    {
                        selectedCongress += HttpUtility.HtmlDecode(oItem.ItemDescription) + (!string.IsNullOrEmpty(oItem.ItemDescription_ShowedInTemplate) ? "[" + HttpUtility.HtmlDecode(oItem.ItemDescription_ShowedInTemplate) + "]" : "") + (";" + oItem.Price + "!" + oItem.Qty) + "^";
                    }
                    else
                    {
                        selectedCongress += HttpUtility.HtmlDecode(oItem.ReportDisplayName) + (";" + oItem.Price + "!" + oItem.Qty) + "^";
                    }
                }
            }
            selectedCongress = selectedCongress.TrimEnd('^');
        }
        catch (Exception ex)
        { }

        return selectedCongress;
    }
    public bool isPaymentVisible(string invStatus)
    {
        bool isVisible = false;
        try
        {
            StatusSettings stuSettings = new StatusSettings(fn);
            //invStatus = getInvoiceStatus(invStatus);
            if (invStatus == stuSettings.Pending.ToString() || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
            {
                isVisible = true;
            }
        }
        catch (Exception ex)
        { }

        return isVisible;
    }
    public bool isDownloadReceiptVisible(string invStatus)
    {
        bool isVisible = false;
        try
        {
            StatusSettings stuSettings = new StatusSettings(fn);
            //invStatus = getInvoiceStatus(invStatus);
            if (invStatus == stuSettings.Success.ToString())
            {
                isVisible = true;
            }
        }
        catch (Exception ex)
        { }

        return isVisible;
    }
    public string getEarlyBirdRegular(string InvoiceID, string showid)
    {
        string result = "N/A";
        try
        {
            if (!string.IsNullOrEmpty(InvoiceID))
            {
                string sql = "Select CASE WHEN DATEDIFF(dd, CONVERT(datetime, (Select settings_value From"
                                + " tb_site_settings Where ShowID='" + showid + "' AND settings_name='EarlyBirdEndDate'), 103), InvoiceDate) <= 0 THEN 'Early Bird Price' ELSE 'Regular Price' END AS PriceType"
                                + " From tb_Invoice Where isdeleted=0 And InvoiceId='" + InvoiceID + "'";
                result = fn.GetDataByCommand(sql, "PriceType");
                if(result == "0")
                {
                    result = "N/A";
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    #endregion
    #endregion

    #region btnUpdatePayment_Click
    protected void btnUpdatePayment_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string invoiceID = arg[2];
                string flowid = ddl_flowList.SelectedItem.Value;
                string step = flwObj.GetLatestStep(flowid);
                //string fullUrl = "FLW=" + cFun.DecryptValue(ddl_flowList.SelectedValue) + "&STP=" + cFun.DecryptValue(step)
                //+ "&GRP=" + cFun.DecryptValue(groupid) + "&INV=" + cFun.DecryptValue(regno) + "&SHW=" + cFun.DecryptValue(showID);
                string page = "PaymentUpdate.aspx";
                string route = flwObj.MakeFullURL(page, flowid, showID, groupid, step, regno);//, BackendRegType.backendRegType_Delegate);
                route += "&INVID=" + cFun.EncryptValue(invoiceID);
                Response.Redirect(route);
            }
            catch (Exception ex)
            {
                //Response.Redirect("Login.aspx");
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region btnDownloadInvoice_Command
    protected void btnDownloadInvoice_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                InvoiceControler invControler = new InvoiceControler(fn);
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string invoiceID = arg[2];
                string flowid = ddl_flowList.SelectedItem.Value;
                string step = flwObj.GetLatestStep(flowid);
                string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                string fullUrl = "?SHW=" + cFun.EncryptValue(showID) + "&DID=" + cFun.EncryptValue(invOwnerID) + "&VDD=" + cFun.EncryptValue(invoiceID);
                string page = "../DownloadInvoiceLetter.aspx";
                string route = page + fullUrl;
                string script = String.Format("window.open('{0}','_blank');", route);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                //ClientScript.RegisterStartupScript(GetType(), "scr", script, true);
                //Response.Redirect(route);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region ConfirmationEmail
    #region btnDownloadReceipt_Command
    protected void btnDownloadReceipt_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                InvoiceControler invControler = new InvoiceControler(fn);
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string invoiceID = arg[2];
                string flowid = ddl_flowList.SelectedItem.Value;
                string step = flwObj.GetLatestStep(flowid);
                string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                string fullUrl = "?SHW=" + cFun.EncryptValue(showID) + "&DID=" + cFun.EncryptValue(invOwnerID) + "&VDD=" + cFun.EncryptValue(invoiceID);
                string page = "../DownloadReceiptLetter.aspx";
                string route = page + fullUrl;
                string script = String.Format("window.open('{0}','_blank');", route);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                //ClientScript.RegisterStartupScript(GetType(), "scr", script, true);
                //Response.Redirect(route);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
    #region btnSendConfirmationEmail_Command
    protected void btnSendConfirmationEmail_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                InvoiceControler invControler = new InvoiceControler(fn);
                EmailHelper eHelper = new EmailHelper();
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string invoiceID = arg[2];
                string eType = EmailHTMLTemlateType.Confirmation;
                bool isExistEmailType = checkEmailTypeForConfirmation(showID, EmailHTMLTemlateType.Confirmation);
                if (!isExistEmailType)
                {
                    isExistEmailType = checkEmailTypeForConfirmation(showID, EmailHTMLTemlateType.RECEIPT);
                    if (isExistEmailType)
                    {
                        eType = EmailHTMLTemlateType.RECEIPT;
                    }
                }
                string emailRegType = BackendRegType.backendRegType_Delegate;// "D";
                EmailDSourceKey sourcKey = new EmailDSourceKey();
                //string showID = showid;// "MBF242";
                string flowid = ddl_flowList.SelectedItem.Value;
                string flowID = flowid;// "F322";
                string groupID = groupid;// "2421113";
                string delegateID = regno;// "24210092";//Blank

                sourcKey.AddKeyToList("ShowID", showID);
                sourcKey.AddKeyToList("RegGroupID", groupID);
                sourcKey.AddKeyToList("Regno", delegateID);
                sourcKey.AddKeyToList("FlowID", flowID);
                sourcKey.AddKeyToList("INVID", invoiceID);

                string updatedUser = string.Empty;
                if (Session["userid"] != null)
                {
                    updatedUser = Session["userid"].ToString();
                }
                eHelper.SendEmailFromBackend(eType, sourcKey, emailRegType, updatedUser);
                //string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already sent the confirmation email.');", true);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion
    #endregion

    #region btnDownloadBadge_Command
    protected void btnDownloadBadge_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                InvoiceControler invControler = new InvoiceControler(fn);
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string invoiceID = arg[2];
                string flowid = ddl_flowList.SelectedItem.Value;
                //"DownloadBadge?SHW=NjJunAD0GCAcTe8pUOEhMA==&DID=n2Kikqcp8RaMaLz4kNj9cg=="
                string fullUrl = "?SHW=" + cFun.EncryptValue(showID) + "&DID=" + cFun.EncryptValue(regno);
                string page = "../DownloadBadge.aspx";
                string route = page + fullUrl;
                string script = String.Format("window.open('{0}','_blank');", route);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                //ClientScript.RegisterStartupScript(GetType(), "scr", script, true);
                //Response.Redirect(route);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region getPrepopulateDataByShowFlow
    private DataTable getPrepopulateDataByShowFlow(string showid, string flowid)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = fn.GetDatasetByCommand(string.Format("Select * From tb_RegPrePopulate Where (recycle=0 Or recycle Is Null) And ShowID='{0}' And reg_urlFlowID='{1}'"
                + " And (RefNo Is Not Null And RefNo<>'')"
                , showid, flowid), "ds").Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    #endregion

    #region Search
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;
            if (!string.IsNullOrEmpty(txtFromDate.Text.Trim()) && !string.IsNullOrEmpty(txtToDate.Text.Trim()))
            {
                string fromD = txtFromDate.Text.ToString();
                string toD = txtToDate.Text.ToString();
                int status = Convert.ToInt16(ddl_paymentStatus.SelectedValue);

                if (status == -1)
                {
                    GKeyMaster.DataSource = getDataAllRegList(fromD, toD);
                }
                else if (status == -20)//***MDA
                {
                    DataTable dtRegList = getDataAllRegList(fromD, toD);
                    DataTable dtPreRegList = getPrepopulateDataByShowFlow(showID, flowID);

                    DataTable TableC = dtRegList.AsEnumerable()
                            .Where(ra => dtPreRegList.AsEnumerable()
                                                .Any(rb => rb.Field<int>("RefNo") == ra.Field<int>("Regno")))
                            .CopyToDataTable();
                    GKeyMaster.DataSource = TableC;
                }//***MDA
                else
                    GKeyMaster.DataSource = getDataByPaymentStatus(status, fromD, toD);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please fill Date');", true);
                return;
            }
        }
        catch (Exception ex)
        { }
    }
    public DataTable getDataAllRegList(string sdate, string edate)
    {
        DataTable dt = new DataTable();

        try
        {
            string[] startdate1 = sdate.Split('/');
            //string startdate = startdate1[2] + "-" + startdate1[0] + "-" + startdate1[1];
            string startdate = startdate1[2] + "/" + startdate1[0] + "/" + startdate1[1];
            string[] enddate1 = edate.Split('/');
            //string enddate = enddate1[2] + "-" + enddate1[0] + "-" + enddate1[1];
            string enddate = enddate1[2] + "/" + enddate1[0] + "/" + enddate1[1];
            /*string query = "Select * From RegIndiv_All Where ShowID=@SHWID";*/
            /*change on 1-2-2019 than*/
            string query = "Select * From GetRegIndivAllByShowID(@SHWID)";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);

            if (!string.IsNullOrEmpty(flowID))
            {
                //query += " and reg_urlFlowID=@reg_urlFlowID and reg_datecreated  BETWEEN '" + startdate + "' and '" + enddate + "'";
                /*query += " and reg_urlFlowID=@reg_urlFlowID and reg_datecreated  BETWEEN '" + startdate + "' and DATEADD(s,-1,DATEADD(d,1,'" + enddate + "'))";*/
                /*change on 1-2-2019 than*/
                query += "Select * From GetRegIndivAll(@SHWID,@reg_urlFlowID) Where reg_datecreated  BETWEEN '" + startdate + "' and DATEADD(s,-1,DATEADD(d,1,'" + enddate + "'))";
                spar = new SqlParameter("reg_urlFlowID", SqlDbType.NVarChar);
                spar.Value = flowID;
                pList.Add(spar);
            }

            dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    public DataTable getDataByPaymentStatus(int status, string sdate, string edate)
    {
        DataTable dt = new DataTable();

        try
        {
            string[] startdate1 = sdate.Split('/');
            string startdate = startdate1[2] + "-" + startdate1[0] + "-" + startdate1[1];
            string[] enddate1 = edate.Split('/');
            string enddate = enddate1[2] + "-" + enddate1[0] + "-" + enddate1[1];
            /*string query = "Select * From RegIndiv_All Where ShowID=@SHWID and Invoice_status=@Status";*/
            /*change on 1-2-2019 than*/
            string query = "Select * From GetRegIndivAllByShowID(@SHWID) Where Invoice_status=@Status";

            if (status == 3)
            {
                /*query = "Select * From RegIndiv_All Where ShowID=@SHWID and (Invoice_status=@Status or (Invoice_status=-10 and reg_Status=1))";*/
                /*change on 1-2-2019 than*/
                query = "Select * From GetRegIndivAllByShowID(@SHWID) Where (Invoice_status=@Status or (Invoice_status=-10 and reg_Status=1))";
            }
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);

            if (!string.IsNullOrEmpty(flowID))
            {
                query += " and reg_urlFlowID=@reg_urlFlowID and reg_datecreated  BETWEEN '" + startdate + "' and '" + enddate + "'";
                spar = new SqlParameter("reg_urlFlowID", SqlDbType.NVarChar);
                spar.Value = flowID;
                pList.Add(spar);
            }

            spar = new SqlParameter("Status", SqlDbType.Int);
            spar.Value = status;
            pList.Add(spar);

            dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dt;
    }
    public string checkDateForExcel()
    {
        string start = txtFromDate.Text.Trim();
        string end = txtToDate.Text.Trim();
        string res = string.Empty;
        if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
        {
            res = "and reg_datecreated  BETWEEN '" + start + "' and '" + end + "'";
        }
        else
        {
            res = "";
        }
        return res;
    }
    #endregion

    #region checkNewVisitorMDA
    private static string _newVisitorMDA = "New Visitor";
    private static string _repeatVisitorMDA = "Repeat Visitor";
    private static string _checkQuestionMDA = "'Did you visit MEDICAL FAIR ASIA 2016?', 'Did you visit MEDICAL MANUFACTURING ASIA in 2016?', 'Did you visit OS+H Asia in 2016'";
    
    public string checkNewVisitorMDA(string regno, string qnaireID)
    {
        string result = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(regno) && !string.IsNullOrWhiteSpace(regno))
            {
                if (string.IsNullOrEmpty(qnaireID) || string.IsNullOrWhiteSpace(qnaireID))
                {
                    qnaireID = getQnaireID();
                }

                string sql = "Select * From gen_QnaireLog Where qnaire_log_id In("
                            + " Select qnaire_log_id From gen_QnaireResult Where qnaire_log_id In"
                            + " (Select qnaire_log_id From gen_QnaireLog Where qnaire_id='" + qnaireID + "' And status='Active')"
                            + " And qitem_id In (Select qitem_id From gen_QuestItem Where"
                            + " quest_id In (Select quest_id From gen_Question Where quest_id In"
                            + " (Select quest_id From gen_QnaireQuest Where qnaire_id='" + qnaireID + "' And status='Active')"
                            + " And quest_desc In (" + _checkQuestionMDA + ") And status='Active')"
                            + " And qitem_desc Like 'Yes' And status='Active')"
                            + " And status='Active'"
                            + " )"
                            + " And user_reference='" + regno + "'";
                DataTable dt = qfn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    result = _repeatVisitorMDA;
                }
                else
                {
                    result = _newVisitorMDA;
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getQnaireID()
    {
        string result = string.Empty;
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                string flowid = ddl_flowList.SelectedItem.Value;

                FlowControler flwControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
                string flowType = flwMasterConfig.FlowType;
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                {
                    flowType = BackendRegType.backendRegType_Delegate;
                }
                result = getQID(showID, flowid, flowType);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

        return result;
    }
    private string getQID(string showid, string flowid, string type)
    {
        QuestionnaireControler qCtr = new QuestionnaireControler(fn);
        DataTable dt = qCtr.getQID(type, flowid, showid);
        if (dt.Rows.Count > 0)
            return dt.Rows[0]["SQ_QAID"].ToString();
        else
            return "QLST";
    }
    #endregion

    /// <summary>
    /// added on 13-7-2018 th
    /// Keyword Search
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnKeysearch_Click(object sender, EventArgs e)
    {
        try
        {
            try
            {
                if (Session["roleid"].ToString() != "1")
                    getShowID(Session["userid"].ToString());
                else
                    showID = ddl_showList.SelectedValue;

                flowID = ddl_flowList.SelectedValue;
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }

            string searchkey = "";
            if (!string.IsNullOrEmpty(txtKey.Text) && !string.IsNullOrWhiteSpace(txtKey.Text))
            {
                string key = txtKey.Text.Trim();
                if (Regex.IsMatch(key, @"^\d+$"))
                {
                    searchkey = "Regno='" + key + "'";
                }
                else
                {
                    string countryIDs = getCountriesByName(key);
                    searchkey = "reg_FName like '%" + key + "%' Or reg_LName like '%" + key + "%' Or reg_Email like '%" + key + "%'"
                                    //+ " Or reg_Address1 like '%" + key + "%'"
                                    //+ " Or reg_Designation like '%" + key + "%'"
                                    + " Or reg_Country In (" + countryIDs + ")";
                }
            }
            DataTable dtReg = generateRegReport(showID, flowID, searchkey);
            GKeyMaster.DataSource = dtReg;
            GKeyMaster.DataBind();
        }
        catch (Exception ex)
        { }
    }
    private string getCountriesByName(string countryName)
    {
        string result = string.Empty;
        try
        {
            string sql = "SELECT TOP 1 Substring ((SELECT ', ' + Convert(nvarchar,Cty_GUID) + '' FROM ref_country"
                        + " WHERE Country Like '%" + countryName + "%' And Cty_GUID = dbo.ref_country.Cty_GUID FOR XML PATH('')), 2, 200000) AS countryIDs";
            result = fn.GetDataByCommand(sql, "countryIDs");
            if(string.IsNullOrEmpty(result))
            {
                result = "0";
            }
        }
        catch(Exception ex)
        { }

        return result;
    }

    #region re-send confirmation Email FJ (31-7-2018)
    //protected void btnSendConfirmationEmailNormal_Command(object sender, CommandEventArgs e)
    //{
    //    //////*****Only for FJ
    //    if (Session["userid"] != null)
    //    {
    //        try
    //        {
    //            if (Session["roleid"] != null)
    //            {
    //                if (Session["roleid"].ToString() != "1")
    //                    getShowID(Session["userid"].ToString());
    //                else
    //                    showID = ddl_showList.SelectedValue;
    //            }
    //            else
    //                showID = ddl_showList.SelectedValue;
    //            ShowControler shwCtr = new ShowControler(fn);
    //            Show shw = shwCtr.GetShow(showID);
    //            if (checkingFJShowName.Contains(shw.SHW_Name))
    //            {
    //                string flowid = ddl_flowList.SelectedItem.Value;
    //                FlowControler flwObj = new FlowControler(fn);
    //                InvoiceControler invControler = new InvoiceControler(fn);
    //                EmailHelper eHelper = new EmailHelper();
    //                string[] arg = new string[2];
    //                arg = e.CommandArgument.ToString().Split(';');
    //                string regno = arg[0];
    //                string groupid = arg[1];
    //                string invoiceID = arg[2];
    //                string invStatus = arg[3];
    //                string regStatus = arg[4];
    //                FlowControler Flw = new FlowControler(fn);
    //                StatusSettings stuSettings = new StatusSettings(fn);
    //                bool confExist = Flw.checkPageExist(flowid, SiteDefaultValue.constantConfName);
    //                if (confExist)
    //                {
    //                    #region public ticket purchase
    //                    ////invStatus = getInvoiceStatus(invStatus);
    //                    //if (invStatus == stuSettings.Success.ToString())
    //                    //{
    //                    //    string emailRegType = BackendRegType.backendRegType_Delegate;// "D";
    //                    //    EmailDSourceKey sourcKey = new EmailDSourceKey();
    //                    //    string flowID = flowid;
    //                    //    string groupID = groupid;
    //                    //    string delegateID = regno;//if not, Blank
    //                    //    string emailCondition = EmailSendConditionType.PAID;

    //                    //    sourcKey.AddKeyToList("ShowID", showID);
    //                    //    sourcKey.AddKeyToList("RegGroupID", groupID);
    //                    //    sourcKey.AddKeyToList("Regno", delegateID);
    //                    //    sourcKey.AddKeyToList("FlowID", flowID);
    //                    //    sourcKey.AddKeyToList("INVID", invoiceID);

    //                    //    string updatedUser = string.Empty;
    //                    //    if (Session["userid"] != null)
    //                    //    {
    //                    //        updatedUser = Session["userid"].ToString();
    //                    //    }
    //                    //    bool isEmailSend = eHelper.SendEmailFromBackendByCondition(emailCondition, sourcKey, emailRegType, updatedUser);
    //                    //    #region Logging
    //                    //    LogGenEmail lggenemail = new LogGenEmail(fn);
    //                    //    LogActionObj lactObj = new LogActionObj();
    //                    //    lggenemail.type = LogType.generalType;
    //                    //    lggenemail.RefNumber = groupID + "," + regno;
    //                    //    lggenemail.description = "Re-send confrimation email to " + delegateID;
    //                    //    lggenemail.remark = "Re-send confrimation email to " + delegateID + " and Send " + emailCondition + " email(email sending status:" + isEmailSend + ")";
    //                    //    lggenemail.step = "backend";
    //                    //    lggenemail.writeLog();
    //                    //    #endregion
    //                    //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already sent the confirmation email(email sending status:" + isEmailSend + ")');", true);
    //                    //}
    //                    #endregion
    //                }
    //                else
    //                {
    //                    #region normal reg
    //                    if (regStatus == stuSettings.Success.ToString())
    //                    {
    //                        string emailRegType = BackendRegType.backendRegType_Delegate;// "D";
    //                        EmailDSourceKey sourcKey = new EmailDSourceKey();
    //                        string flowID = flowid;
    //                        string groupID = groupid;
    //                        string delegateID = regno;//if not, Blank
    //                        string eType = EmailHTMLTemlateType.Confirmation;

    //                        sourcKey.AddKeyToList("ShowID", showID);
    //                        sourcKey.AddKeyToList("RegGroupID", groupID);
    //                        sourcKey.AddKeyToList("Regno", delegateID);
    //                        sourcKey.AddKeyToList("FlowID", flowID);

    //                        string updatedUser = string.Empty;
    //                        if (Session["userid"] != null)
    //                        {
    //                            updatedUser = Session["userid"].ToString();
    //                        }
    //                        bool isEmailSend = eHelper.SendEmailFromBackend(eType, sourcKey, emailRegType, updatedUser);
    //                        #region Logging
    //                        LogGenEmail lggenemail = new LogGenEmail(fn);
    //                        LogActionObj lactObj = new LogActionObj();
    //                        lggenemail.type = LogType.generalType;
    //                        lggenemail.RefNumber = groupID + "," + regno;
    //                        lggenemail.description = "Re-send confrimation email to " + delegateID;
    //                        lggenemail.remark = "Re-send confrimation email to " + delegateID + " and Send " + eType + " email(email sending status:" + isEmailSend + ")";
    //                        lggenemail.step = "backend";
    //                        lggenemail.writeLog();
    //                        #endregion
    //                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already sent the confirmation email(email sending status:" + isEmailSend + ")');", true);
    //                    }
    //                    #endregion
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        { }
    //    }
    //    else
    //    {
    //        Response.Redirect("Login.aspx");
    //    }
    //    //////*****Only for FJ
    //}
    //public bool isResendConfirmationEmailVisible(string regStatus)
    //{
    //    bool isVisible = false;
    //    try
    //    {
    //        if (Session["roleid"].ToString() != "1")
    //            getShowID(Session["userid"].ToString());
    //        else
    //            showID = ddl_showList.SelectedValue;
    //        ShowControler shwCtr = new ShowControler(fn);
    //        Show shw = shwCtr.GetShow(showID);
    //        if (checkingFJShowName.Contains(shw.SHW_Name))
    //        {
    //            flowID = ddl_flowList.SelectedValue;
    //            StatusSettings stuSettings = new StatusSettings(fn);
    //            FlowControler Flw = new FlowControler(fn);
    //            bool confExist = Flw.checkPageExist(flowID, SiteDefaultValue.constantConfName);
    //            if (confExist)
    //            {
    //            }
    //            else
    //            {
    //                if (regStatus == stuSettings.Success.ToString())
    //                {
    //                    isVisible = true;
    //                }
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    { }

    //    return isVisible;
    //}
    #endregion

    protected void ddlSortBy_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    #region QATransmit Data
    string Isql = string.Empty;
    string questsql = string.Empty;
    public void TransmitData(string QnaireID)
    {
        string Regno = "";
            string sqlAllLog = "select * from gen_QnaireLog where status='Active' and qnaire_id='" + QnaireID + "'"
                            + " And qnaire_log_id Not In (select qnaire_log_id from tbl" + QnaireID + " where status='Active') order by qnaire_log_id";
            DataTable dtAllLog = qfn.GetDatasetByCommand(sqlAllLog, "sQAll").Tables[0];
            //  DataTable dt = qfn.GetDatasetByCommand("SELECT name FROM sys.columns WHERE object_id = OBJECT_ID('dbo.tbl" + QnaireID + "')", "sQNCol").Tables[0];
            int TableCount = CheckExistingTable(QnaireID);
            if (TableCount > 0)
            {
                if (dtAllLog.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtAllLog.Rows)
                    {

                        string logid = dr["qnaire_log_id"].ToString();
                        Regno = dr["user_reference"].ToString();
                        string status = dr["status"].ToString();
                        string CreatDate = Convert.ToDateTime(dr["create_date"]).ToString("MM/dd/yyyy HH:mm:ss.fff", CultureInfo.CurrentCulture);
                        string UpdateDate = Convert.ToDateTime(dr["lupdate_date"]).ToString("MM/dd/yyyy HH:mm:ss.fff", CultureInfo.CurrentCulture);

                        int Logcount = CheckExistingLogID(logid, QnaireID);
                        if (Logcount == 0)
                        {
                            try
                            {
                                Isql = "insert into tbl" + QnaireID
                                     + " values"
                                     + "('" + logid + "','" + Regno + "',";
                                AddItemIDResult(logid, QnaireID);
                                Isql += "'" + status + "','" + CreatDate + "','" + UpdateDate + "')";
                                DataSet dtllist = qfn.GetDatasetByCommand(Isql, "sLog");
                            }
                            catch (Exception ex)
                            { }
                        }
                        else
                        {
                            //Update For old Record
                        }
                    }
                    Response.Write(@"<script language='javascript'>alert('You have successfully transmitted datas in table tbl" + QnaireID + ".');</script>");
                }
            }
            else
            {
                Response.Write(@"<script language='javascript'>alert('Please create table first');</script>");
            }
        //}
        //catch (Exception ex)
        //{
        //    //Response.Write(Regno);
        //    //Response.End();
        //}
    }
    public int CheckExistingTable(string QnaireID)
    {
        int Qcount = 0;

        string sql = "SELECT* FROM sys.tables where name = 'tbl" + QnaireID + "'";
        DataSet ds = new DataSet();
        ds = qfn.GetDatasetByCommand(sql, "sQNRes");
        Qcount = ds.Tables[0].Rows.Count;

        return Qcount;
    }
    public int CheckExistingLogID(string logid, string QnaireID)
    {
        int Qcount = 0;

        string sql = "select qnaire_log_id from tbl" + QnaireID + " where qnaire_log_id='" + logid + "' and status='Active'";
        DataSet ds = new DataSet();
        ds = qfn.GetDatasetByCommand(sql, "dsds");
        Qcount = ds.Tables[0].Rows.Count;

        return Qcount;
    }
    public void AddItemIDResult(string logid, string QnaireID)
    {
        DataTable dt = qfn.GetDatasetByCommand("SELECT name FROM sys.columns WHERE object_id = OBJECT_ID('dbo.tbl" + QnaireID + "')  and column_id>2 and column_id<(SELECT COUNT(*)FROM   sys.columns WHERE object_id = OBJECT_ID('dbo.tbl" + QnaireID + "'))-2", "sQNCol").Tables[0];
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                string ItemID = dr["name"].ToString();
                string sqlRAll = "select distinct* from gen_QnaireResult where qnaire_log_id='" + logid + "' and qitem_id='" + ItemID + "' and status='Active'";
                DataTable dtRAll = qfn.GetDatasetByCommand(sqlRAll, "sQAll").Tables[0];
                if (dtRAll.Rows.Count > 0)
                {
                    foreach (DataRow drR in dtRAll.Rows)
                    {
                        //string item_id = drR["qitem_id"].ToString();
                        string Input_num = drR["qitem_input_num"].ToString();
                        string Input_text = drR["qitem_input_txt"].ToString();
                        if (Input_num == "1")
                        {
                            Isql += "'1',";
                        }
                        else
                        {
                            Isql += "'" + Input_text + "',";
                        }
                    }
                }
                else
                {
                    Isql += "'0',";

                }
            }
        }
        else
        {

        }
    }
    #endregion

    private string getPromoCode(string groupid)
    {
        string result = string.Empty;
        string sql = "Select Promo_Code From tb_User_Promo Where Regno='" + groupid + "'";
        result = fn.GetDataByCommand(sql, "Promo_Code");
        if(result == "0")
        {
            result = "";
        }

        return result;
    }

    #region Visitor
    protected void btnSendVisitorConfirmationEmail_Command(object sender, CommandEventArgs e)
    {
        if (Session["userid"] != null)
        {
            try
            {
                if (Session["roleid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                        getShowID(Session["userid"].ToString());
                    else
                        showID = ddl_showList.SelectedValue;
                }
                else
                    showID = ddl_showList.SelectedValue;
                FlowControler flwObj = new FlowControler(fn);
                InvoiceControler invControler = new InvoiceControler(fn);
                EmailHelper eHelper = new EmailHelper();
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                string regno = arg[0];
                string groupid = arg[1];
                string eType = EmailHTMLTemlateType.Confirmation;
                string emailRegType = BackendRegType.backendRegType_Delegate;// "D";
                EmailDSourceKey sourcKey = new EmailDSourceKey();
                //string showID = showid;// "MBF242";
                string flowid = ddl_flowList.SelectedItem.Value;
                string flowID = flowid;// "F322";
                string groupID = groupid;// "2421113";
                string delegateID = regno;// "24210092";//Blank

                sourcKey.AddKeyToList("ShowID", showID);
                sourcKey.AddKeyToList("RegGroupID", groupID);
                sourcKey.AddKeyToList("Regno", delegateID);
                sourcKey.AddKeyToList("FlowID", flowID);
                sourcKey.AddKeyToList("INVID", "");

                string updatedUser = string.Empty;
                if (Session["userid"] != null)
                {
                    updatedUser = Session["userid"].ToString();
                }
                eHelper.SendEmailFromBackend(eType, sourcKey, emailRegType, updatedUser);
                //string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already sent the confirmation email.');", true);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    public bool isSendVisitorConfirmationEmailVisible(string regStatus)
    {
        bool isVisible = false;
        try
        {
            StatusSettings stuSettings = new StatusSettings(fn);
            if (regStatus == stuSettings.Success.ToString())
            {
                isVisible = true;
            }
        }
        catch (Exception ex)
        { }

        return isVisible;
    }
    private bool checkEmailTypeForConfirmation(string showid, string emailType)
    {
        bool isExist = false;
        string sql = "Select * From tb_site_flow_Email Where showid='" + showid + "' And EmailType='" + emailType + "'";
        DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        if(dt.Rows.Count > 0)
        {
            isExist = true;
        }
        return isExist;
    }
    #endregion

    #region Onsite
    public string getOnsiteAdditionalValue(string regno, string reggroupid, string showid, string type)
    {
        string name = string.Empty;
        try
        {
            string sqlPostConf = "Select * From tb_Onsite_Additional Where ShowID=@ShowID And Regno=@Regno And GroupId=@GroupId";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Regno", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("GroupId", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = regno;
            spar3.Value = reggroupid;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);
            DataTable dt = fn.GetDatasetByCommand(sqlPostConf, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                if (type == "PostConference")
                {
                    string selectedPostConference = dt.Rows[0]["AdditionalValue1"] != DBNull.Value ? dt.Rows[0]["AdditionalValue1"].ToString() : "";
                    name = selectedPostConference;
                }
                else if (type == "BreakoutSession")
                {
                    string selectedBreakoutSession = dt.Rows[0]["AdditionalValue2"] != DBNull.Value ? dt.Rows[0]["AdditionalValue2"].ToString() : "";
                    name = selectedBreakoutSession;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    public string getPrintStatus(string regno, string showid)
    {
        string rtn = "";
        try
        {
            string sql = string.Format("select * from tb_Onsite_PrintLog where showID = '{0}' and Pregno = '{1}'", showid, regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 0)
                rtn = "YES";
        }
        catch { }
        return rtn;
    }
    public string getPrintedDate(string regno, string showid)
    {
        string rtn = "";
        try
        {
            string sql = string.Format("select * from tb_Onsite_PrintLog where showID = '{0}' and Pregno = '{1}' Order By PDate", showid, regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 0)
            {
                string printDate = dt.Rows[0]["PDate"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["PDate"].ToString()) ? dt.Rows[0]["PDate"].ToString() : "") : "";
                if(!string.IsNullOrEmpty(printDate))
                {
                    rtn = DateTime.Parse(printDate).ToString("dd/MM/yyyy hh:mm:ss");
                }
            }
        }
        catch { }
        return rtn;
    }
    public string getRePrintedDate(string regno, string showid)
    {
        string rtn = "";
        try
        {
            string sql = string.Format("select * from tb_Onsite_PrintLog where showID = '{0}' and Pregno = '{1}' Order By PDate Desc", showid, regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 1)
            {
                string printDate = dt.Rows[0]["PDate"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["PDate"].ToString()) ? dt.Rows[0]["PDate"].ToString() : "") : "";
                if (!string.IsNullOrEmpty(printDate))
                {
                    rtn = DateTime.Parse(printDate).ToString("dd/MM/yyyy hh:mm:ss");
                }
            }
        }
        catch { }
        return rtn;
    }
    #region WCMH
    private static string catWCMHCategoryDefault = "Delegate";
    private static string catWCMHRed1 = "WCMHAA8N";
    private static string catWCMHRed2 = "WCMHT5NK";
    private static string catWCMHColorRed = "Red";
    private static string catWCMHGreen = "FLJJJZ0CTR";
    private static string catWCMHColorGreen = "Green";
    private static string catWCMHOrganiser = "811";
    private static string catWCMHColorOrganiser = "LightGrey";
    private static string catWCMHCategoryOrganiser = "Organiser";
    private static string catWCMHCrew = "812";
    private static string catWCMHColorCrew = "DarkGrey";
    private static string catWCMHCategoryCrew = "Crew";
    private static string catWCMHInvitedGuest = "813";
    private static string catWCMHColorInvitedGuest = "Yellow";
    private static string catWCMHCategoryInvitedGuest = "Invited Guest";
    private static string catWCMHMedia = "814";
    private static string catWCMHColorMedia = "Teal";
    private static string catWCMHCategoryMedia = "Media";
    private static string catWCMHSpeaker = "815";
    private static string catWCMHColorSpeaker = "Brown";
    private static string catWCMHCategorySpeaker = "Speaker";
    private static string WCMHShowID = "GBL362";
    public string checkCategoryWCMH(string showid, string regno)
    {
        string categoryColor = "";
        try
        {
            if (showid == WCMHShowID)
            {
                categoryColor = catWCMHCategoryDefault;//***
                string sqlCheckPromo = "Select * From tb_User_Promo Where ShowID='" + showid + "' And Regno In (Select RegGroupID From tb_RegDelegate Where ShowID='GBL362' And recycle=0 And Regno='" + regno + "')";
                //DataTable dt = fn.GetDatasetByCommand(sqlCheckPromo, "ds").Tables[0];
                //if (dt.Rows.Count > 0)
                //{
                //    string promocode = dt.Rows[0]["Promo_Code"] != DBNull.Value ? dt.Rows[0]["Promo_Code"].ToString() : "";
                //    if (promocode == catWCMHRed1 || promocode == catWCMHRed2)
                //    {
                //        categoryColor = promocode;
                //    }
                //    else if (promocode == catWCMHGreen)
                //    {
                //        categoryColor = promocode;
                //    }
                //}
                //else
                {
                    string category = fn.GetDataByCommand("Select con_CategoryId From tb_RegDelegate Where recycle=0 And ShowID='" + showid + "' And Regno='" + regno + "'", "con_CategoryId");
                    if (category == catWCMHOrganiser)
                    {
                        categoryColor = catWCMHCategoryOrganiser;
                    }
                    else if (category == catWCMHCrew)
                    {
                        categoryColor = catWCMHCategoryCrew;
                    }
                    else if (category == catWCMHInvitedGuest)
                    {
                        categoryColor = catWCMHCategoryInvitedGuest;
                    }
                    else if (category == catWCMHMedia)
                    {
                        categoryColor = catWCMHCategoryMedia;
                    }
                    else if (category == catWCMHSpeaker)
                    {
                        categoryColor = catWCMHCategorySpeaker;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return categoryColor;
    }
    #endregion
    #endregion

    #region New
    public DataTable generateRegReport(string showid, string flowid, string search="")
    {
        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(showID))
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showID);
            FlowControler Flw = new FlowControler(fn);
            FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowid);
            bool confExist = Flw.checkPageExist(flowid, SiteDefaultValue.constantConfName);
            string regnoSS = "";
            try
            {
                int columnCount = 0;
                #region getData
                DataTable dtRegList = new DataTable();
                int status = Convert.ToInt16(ddl_paymentStatus.SelectedValue);
                MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
                msregIndiv.ShowID = showID;
                msregIndiv.FlowID = flowID;
                if (status == -1)
                {
                    dtRegList = msregIndiv.getDataAllRegList();
                    DataView dv = new DataView();
                    dv = dtRegList.DefaultView;
                    dv.Sort = "reg_datecreated DESC";
                    dtRegList = dv.ToTable();
                }
                else if (status == -20)//***MDA
                {
                    DataTable dtReg = msregIndiv.getDataAllRegList();
                    DataTable dtPreRegList = getPrepopulateDataByShowFlow(showID, flowID);

                    DataTable TableC = dtReg.AsEnumerable()
                            .Where(ra => dtPreRegList.AsEnumerable()
                                                .Any(rb => rb.Field<int>("RefNo") == ra.Field<int>("Regno")))
                            .CopyToDataTable();

                    DataView dv = new DataView();
                    dv = TableC.DefaultView;
                    dv.Sort = "reg_datecreated DESC";
                    dtRegList = dv.ToTable();
                }//***MDA
                else
                {
                    DataTable dtReg = msregIndiv.getDataByPaymentStatus(status);
                    DataView dv = new DataView();
                    dv = dtReg.DefaultView;
                    dv.Sort = "reg_datecreated DESC";
                    dtRegList = dv.ToTable();
                }

                #region filter
                if (!string.IsNullOrEmpty(search))
                {
                    DataView dv = new DataView();
                    dv = dtRegList.DefaultView;
                    dv.RowFilter = search;
                    dtRegList = dv.ToTable();
                }
                #endregion

                dtRegList.Columns.Add("isReg");
                #endregion
                #region bind congress header
                if (confExist)
                {
                    //////*main congress selection
                    getMainCongressHeaders(showid, flowid, ref dtRegList, ref columnCount);
                    //////*congress selection
                    //////*dependent congress selection
                    getDependentCongressHeaders(showid, flowid, ref dtRegList, ref columnCount);
                    //////*dependent congress selection
                    dtRegList.Columns.Add("Early Bird / Regular");
                    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)/*Individual Registration can only see*/
                    {
                        dtRegList.Columns.Add("Payment Method");
                        dtRegList.Columns.Add("GST");
                        dtRegList.Columns.Add("Other Fee");
                        dtRegList.Columns.Add("Sub-total Price");
                        dtRegList.Columns.Add("Total Price");
                        dtRegList.Columns.Add("Paid Price");
                        dtRegList.Columns.Add("Outstanding");
                        dtRegList.Columns.Add("Discount Price");
                        dtRegList.Columns.Add("Invoice Status");
                    }
                }
                #endregion
                if (dtRegList.Rows.Count > 0)
                {
                    int rowcount = 0;
                    foreach (DataRow dr in dtRegList.Rows)
                    {
                        #region bind regdelegatedata
                        //dr["reg_Salutation"] = bindSalutation(dr["reg_Salutation"].ToString(), dr["reg_SalutationOthers"].ToString());
                        dr["isReg"] = dr["reg_isReg"] != null ? (dr["reg_isReg"].ToString() == "1" ? "Yes" : "No") : "No";
                        //dr["reg_Profession"] = bindProfession(dr["reg_Profession"].ToString(), dr["reg_otherProfession"].ToString());
                        //dr["reg_Department"] = bindDepartment(dr["reg_Department"].ToString(), dr["reg_otherDepartment"].ToString());
                        //dr["reg_Organization"] = bindOrganisation(dr["reg_Organization"].ToString(), dr["reg_otherOrganization"].ToString());
                        //dr["reg_Institution"] = bindInstitution(dr["reg_Institution"].ToString(), dr["reg_otherInstitution"].ToString());
                        //dr["reg_Country"] = bindCountry(dr["reg_Country"].ToString());
                        dr["reg_RCountry"] = bindCountry(dr["reg_RCountry"].ToString());
                        dr["reg_Tel"] = dr["reg_Telcc"].ToString() + dr["reg_Telac"].ToString() + dr["reg_Tel"].ToString();
                        dr["reg_Mobile"] = dr["reg_Mobcc"].ToString() + dr["reg_Mobac"].ToString() + dr["reg_Mobile"].ToString();
                        dr["reg_Fax"] = dr["reg_Faxcc"].ToString() + dr["reg_Faxac"].ToString() + dr["reg_Fax"].ToString();
                        //dr["reg_Affiliation"] = bindAffiliation(dr["reg_Affiliation"].ToString());
                        //dr["reg_Dietary"] = bindDietary(dr["reg_Dietary"].ToString());
                        dr["reg_vCountry"] = bindCountry(dr["reg_vCountry"].ToString());
                        dr["UDF_CCountry"] = bindCountry(dr["UDF_CCountry"].ToString());
                        dr["status_name"] = !string.IsNullOrEmpty(dr["status_name"].ToString()) ? dr["status_name"].ToString() : "Pending";
                        #endregion
                        #region bind congress selection
                        if (confExist)
                        {
                            regnoSS = dr["Regno"].ToString();
                            string selectedCongress = getCongressSelection(dr["Regno"].ToString(), dr["RegGroupID"].ToString(), dr["Invoice_status"].ToString(), dr["InvoiceID"].ToString());
                            if (!string.IsNullOrEmpty(selectedCongress) && !string.IsNullOrWhiteSpace(selectedCongress) && selectedCongress != "0")
                            {
                                string[] strselectedCongress = selectedCongress.Split('^');
                                if (strselectedCongress.Length > 0)
                                {
                                    foreach (var confr in strselectedCongress)
                                    {
                                        #region conference/qty
                                        //string[] strConfr = confr.Split(';');
                                        //dr[StripHTML(strConfr[0])] = strConfr[1];
                                        #endregion
                                        #region conference column | qty column
                                        for (int colcount = 1; colcount <= 2; colcount++)
                                        {
                                            string[] strConfr = confr.Split(';');
                                            string[] strConfrQty = strConfr[1].Split('!');
                                            if (dtRegList.Columns.Contains(StripHTML(strConfr[0])))
                                            {
                                                if (colcount == 1)
                                                {
                                                    dr[StripHTML(strConfr[0])] = strConfrQty[0];
                                                }
                                                else if (colcount == 2)
                                                {
                                                    dr[StripHTML(strConfr[0] + "(Qty)")] = strConfrQty[1];
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }
                            dr["Early Bird / Regular"] = getEarlyBirdRegular(dr["InvoiceID"].ToString(), dr["showid"].ToString());
                            #region bind invoice
                            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)/*Individual Registration can only see*/
                            {
                                dr["Payment Method"] = getPaymentMethod(dr["PaymentMethod"].ToString(), dr["showid"].ToString());
                                dr["GST"] = dr["Invoice_GST"].ToString();
                                dr["Other Fee"] = dr["Invoice_OtherFee"].ToString();
                                dr["Sub-total Price"] = dr["Invoice_total"].ToString();
                                dr["Total Price"] = dr["Invoice_grandtotal"].ToString();
                                dr["Paid Price"] = getPaidPrice(dr["InvoiceID"].ToString());
                                dr["Outstanding"] = calculateOutstanding(dr["Invoice_grandtotal"].ToString(), dr["InvoiceID"].ToString());
                                dr["Discount Price"] = dr["Invoice_discount"].ToString();
                                dr["Invoice Status"] = getInvoiceStatus(dr["Invoice_status"].ToString());
                            }
                            #endregion
                        }
                        #endregion
                        rowcount++;
                    }
                }

                createHeaderText(showid, flowid, dtRegList, flwMasterConfig, ref columnCount);
                dt = dtRegList;//***
            }
            catch (Exception ex)
            { }
        }
        return dt;
    }
    private DataTable createHeaderText(string showid, string flowid, DataTable dtRegList, FlowMaster flwMasterConfig, ref int columnCount)
    {
        try
        {
            DataTable dtfrm = new DataTable();
            FormManageObj frmObj = new FormManageObj(fn);
            frmObj.showID = showid;
            frmObj.flowID = flowid;
            dtfrm = frmObj.getDynFormForDelegate().Tables[0];
            columnCount = 0;
            if (dtfrm.Rows.Count > 0)
            {
                #region remove un-use column
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)/*Individual Registration can't be showed RegGroupID*/
                {
                    dtRegList.Columns.Remove("RegGroupID");
                }
                dtRegList.Columns.Remove("reg_Salutation");
                dtRegList.Columns.Remove("reg_Profession");
                dtRegList.Columns.Remove("reg_Institution");
                dtRegList.Columns.Remove("reg_Country");
                dtRegList.Columns.Remove("reg_Department");
                dtRegList.Columns.Remove("reg_Organization");
                dtRegList.Columns.Remove("reg_Affiliation");
                dtRegList.Columns.Remove("reg_Dietary");

                dtRegList.Columns.Remove("con_CategoryId");
                dtRegList.Columns.Remove("con_promoCatId");
                dtRegList.Columns.Remove("reg_isReg");
                dtRegList.Columns.Remove("reg_staffid");
                dtRegList.Columns.Remove("reg_Jobtitle_alliedstu");
                dtRegList.Columns.Remove("reg_Telcc");
                dtRegList.Columns.Remove("reg_Telac");
                dtRegList.Columns.Remove("reg_Mobcc");
                dtRegList.Columns.Remove("reg_Mobac");
                dtRegList.Columns.Remove("reg_Faxcc");
                dtRegList.Columns.Remove("reg_Faxac");
                dtRegList.Columns.Remove("UDF_CLCompanyOther");
                dtRegList.Columns.Remove("UDF_ProfCategoryOther");
                dtRegList.Columns.Remove("reg_SalutationOthers");
                dtRegList.Columns.Remove("reg_otherProfession");
                dtRegList.Columns.Remove("reg_otherDepartment");
                dtRegList.Columns.Remove("reg_otherOrganization");
                dtRegList.Columns.Remove("reg_otherInstitution");
                dtRegList.Columns.Remove("reg_aemail");
                dtRegList.Columns.Remove("reg_remark");
                dtRegList.Columns.Remove("reg_remarkGUpload");
                dtRegList.Columns.Remove("reg_isSMS");
                dtRegList.Columns.Remove("reg_approveStatus");
                dtRegList.Columns.Remove("recycle");
                dtRegList.Columns.Remove("reg_urlFlowID");
                dtRegList.Columns.Remove("reg_Stage");
                dtRegList.Columns.Remove("reg_Status");
                dtRegList.Columns.Remove("ShowID");
                dtRegList.Columns.Remove("Invoice_GST");
                dtRegList.Columns.Remove("Invoice_AdminFee");
                dtRegList.Columns.Remove("Invoice_OtherFee");
                dtRegList.Columns.Remove("Invoice_total");
                dtRegList.Columns.Remove("Invoice_discount");
                dtRegList.Columns.Remove("Invoice_grandtotal");
                dtRegList.Columns.Remove("Invoice_status");
                dtRegList.Columns.Remove("PaymentMethod");
                dtRegList.Columns.Remove("InvoiceNo");
                dtRegList.Columns.Remove("InvoiceID");
                //dtRegList.Columns.Remove("InstitutionName");
                //dtRegList.Columns.Remove("ProfessionName");
                dtRegList.Columns.Remove("isRegDesc");
                //dtRegList.Columns.Remove("Salutation");
                //dtRegList.Columns.Remove("RegCountry");
                dtRegList.Columns.Remove("UserRegno");
                dtRegList.Columns.Remove("RegStatus");
                #endregion
                #region update Delegate column name dynamically
                for (int x = 0; x < dtfrm.Rows.Count; x++)
                {
                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Salutation);
                            dtRegList.Columns["Salutation"].ColumnName = columnText;//reg_Salutation
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("Salutation");//reg_Salutation
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fname);
                            dtRegList.Columns["reg_FName"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_FName");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Lname);
                            dtRegList.Columns["reg_LName"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_LName");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _OName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_OName);
                            dtRegList.Columns["reg_OName"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_OName");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PassNo);
                            dtRegList.Columns["reg_PassNo"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_PassNo");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_isReg);
                            dtRegList.Columns["isReg"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("isReg");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_regSpecific);
                            dtRegList.Columns["reg_sgregistered"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_sgregistered");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_IDNo);
                            dtRegList.Columns["reg_IDno"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_IDno");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                            dtRegList.Columns["reg_Designation"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Designation");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Profession);
                            dtRegList.Columns["ProfessionName"].ColumnName = columnText;//reg_Profession
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("ProfessionName");//reg_Profession
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Department)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Department);
                            dtRegList.Columns["DepartmentName"].ColumnName = columnText;//reg_Department
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("DepartmentName");//reg_Department
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Organization);
                            dtRegList.Columns["OrganizationName"].ColumnName = columnText;//reg_Organization
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("OrganizationName");//reg_Organization
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Institution);
                            dtRegList.Columns["InstitutionName"].ColumnName = columnText;//reg_Institution
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("InstitutionName");//reg_Institution
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address1);
                            dtRegList.Columns["reg_Address1"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Address1");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address2);
                            dtRegList.Columns["reg_Address2"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Address2");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address3);
                            dtRegList.Columns["reg_Address3"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Address3");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address4);
                            dtRegList.Columns["reg_Address4"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Address4");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _City)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_City);
                            dtRegList.Columns["reg_City"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_City");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _State)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_State);
                            dtRegList.Columns["reg_State"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_State");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PostalCode);
                            dtRegList.Columns["reg_PostalCode"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_PostalCode");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Country)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Country);
                            dtRegList.Columns["RegCountry"].ColumnName = columnText;//reg_Country
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("RegCountry");//reg_Country
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_RCountry);
                            dtRegList.Columns["reg_RCountry"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_RCountry");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Tel);
                            dtRegList.Columns["reg_Tel"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Tel");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Mobile);
                            dtRegList.Columns["reg_Mobile"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Mobile");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fax);
                            dtRegList.Columns["reg_Fax"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Fax");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Email);
                            dtRegList.Columns["reg_Email"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Email");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Affiliation);
                            dtRegList.Columns["AffiliationName"].ColumnName = columnText;//reg_Affiliation
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("AffiliationName");//reg_Affiliation
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Dietary);
                            dtRegList.Columns["DietaryName"].ColumnName = columnText;//reg_Dietary
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("DietaryName");//reg_Dietary
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Nationality);
                            dtRegList.Columns["reg_Nationality"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Nationality");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Age)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Age);
                            dtRegList.Columns["reg_Age"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Age");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_DOB);
                            dtRegList.Columns["reg_DOB"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_DOB");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Gender);
                            dtRegList.Columns["reg_Gender"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Gender");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_MembershipNo);
                            dtRegList.Columns["reg_Membershipno"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Membershipno");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VName);
                            dtRegList.Columns["reg_vName"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_vName");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VDOB);
                            dtRegList.Columns["reg_vDOB"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_vDOB");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassNo);
                            dtRegList.Columns["reg_vPassno"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_vPassno");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassExpiry);
                            dtRegList.Columns["reg_vPassexpiry"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_vPassexpiry");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassIssueDate);
                            dtRegList.Columns["reg_vIssueDate"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_vIssueDate");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VEmbarkation);
                            dtRegList.Columns["reg_vEmbarkation"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_vEmbarkation");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VArrivalDate);
                            dtRegList.Columns["reg_vArrivalDate"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_vArrivalDate");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VCountry);
                            dtRegList.Columns["reg_vCountry"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_vCountry");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CName);
                            dtRegList.Columns["UDF_CName"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("UDF_CName");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_DelegateType);
                            dtRegList.Columns["UDF_DelegateType"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("UDF_DelegateType");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_ProfCategory);
                            dtRegList.Columns["UDF_ProfCategory"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("UDF_ProfCategory");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CPcode);
                            dtRegList.Columns["UDF_CPcode"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("UDF_CPcode");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLDepartment);
                            dtRegList.Columns["UDF_CLDepartment"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("UDF_CLDepartment");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CAddress);
                            dtRegList.Columns["UDF_CAddress"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("UDF_CAddress");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLCompany);
                            dtRegList.Columns["UDF_CLCompany"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("UDF_CLCompany");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CCountry);
                            dtRegList.Columns["UDF_CCountry"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("UDF_CCountry");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupName);
                            dtRegList.Columns["reg_SupervisorName"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_SupervisorName");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupDesignation);
                            dtRegList.Columns["reg_SupervisorDesignation"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_SupervisorDesignation");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupContact);
                            dtRegList.Columns["reg_SupervisorContact"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_SupervisorContact");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupEmail);
                            dtRegList.Columns["reg_SupervisorEmail"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_SupervisorEmail");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional4);
                            dtRegList.Columns["reg_Additional4"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Additional4");
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);
                        if (isshow == 1)
                        {
                            string columnText = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional5);
                            dtRegList.Columns["reg_Additional5"].ColumnName = columnText;
                            columnCount++;
                        }
                        else
                        {
                            dtRegList.Columns.Remove("reg_Additional5");
                        }
                    }
                }
                #endregion
                #region update additional column name
                dtRegList.Columns["Promo_Code"].ColumnName = "Promo Code";
                dtRegList.Columns["reg_datecreated"].ColumnName = "Created Date";
                dtRegList.Columns["status_name"].ColumnName = "Status";
                #endregion
            }
        }
        catch(Exception ex)
        { }
        return dtRegList;
    }
    private void getMainCongressHeaders(string showid, string flowid, ref DataTable dtMaster, ref int columnCount)
    {
        try
        {
            string sqlMainConfr = "Select Distinct ReportDisplayName,MAX(con_OrderSeq) From tb_ConfItem Where ShowID='" + showid + "' And isDeleted=0 "
                            + " And reg_categoryId In (Select ref_reg_Category.reg_CategoryId From ref_reg_Category Where ShowID='" + showid + "' And RefValue='" + flowid + "') "
                            + " GROUP BY ReportDisplayName ORDER BY MAX(con_OrderSeq)";
            DataTable dtMainConfr = fn.GetDatasetByCommand(sqlMainConfr, "ds").Tables[0];
            if(dtMainConfr.Rows.Count > 0)
            {
                foreach(DataRow drM in dtMainConfr.Rows)
                {
                    if (drM["ReportDisplayName"] != DBNull.Value)
                    {
                        if (!string.IsNullOrEmpty(drM["ReportDisplayName"].ToString()))
                        {
                            for (int colcount = 1; colcount <= 2; colcount++)
                            {
                                if (colcount == 1)
                                {
                                    dtMaster.Columns.Add(StripHTML(drM["ReportDisplayName"].ToString()));
                                    columnCount++;
                                }
                                else if(colcount == 2)
                                {
                                    dtMaster.Columns.Add(StripHTML(drM["ReportDisplayName"].ToString() + "(Qty)"));
                                    columnCount++;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        { }
    }
    private void getDependentCongressHeaders(string showid, string flowid, ref DataTable dtMaster, ref int columnCount)
    {
        try
        {
            DataColumnCollection columns = dtMaster.Columns;

            string sqlMainConfr = "Select Distinct ReportDisplayName,MAX(con_OrderSeq) From tb_ConfDependentItem Where ShowID='" + showid + "' And isDeleted=0 "
                            + " And con_itemId In (Select tb_ConfItem.con_itemId From tb_ConfItem Where ShowID='" + showid + "' And isDeleted=0 "
                            + " And reg_categoryId In (Select ref_reg_Category.reg_CategoryId From ref_reg_Category Where ShowID='" + showid + "' And RefValue='" + flowid + "')) "
                            + " GROUP BY ReportDisplayName ORDER BY MAX(con_OrderSeq)";
            DataTable dtMainConfr = fn.GetDatasetByCommand(sqlMainConfr, "ds").Tables[0];
            if (dtMainConfr.Rows.Count > 0)
            {
                foreach (DataRow drM in dtMainConfr.Rows)
                {
                    if (drM["ReportDisplayName"] != DBNull.Value)
                    {
                        if (!string.IsNullOrEmpty(drM["ReportDisplayName"].ToString()))
                        {
                            string dependentColumnName = StripHTML(drM["ReportDisplayName"].ToString());
                            if (!columns.Contains(dependentColumnName))
                            {
                                for (int colcount = 1; colcount <= 2; colcount++)
                                {
                                    if (colcount == 1)
                                    {
                                        dtMaster.Columns.Add(dependentColumnName);
                                        columnCount++;
                                    }
                                    else if (colcount == 2)
                                    {
                                        dtMaster.Columns.Add(dependentColumnName + "(Qty)");
                                        columnCount++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    public static string StripHTML(string input)
    {
        return Regex.Replace(input, "<.*?>", String.Empty);
    }
    #endregion
}
