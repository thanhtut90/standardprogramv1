﻿using Corpit.BackendMaster;
using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Email;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegApproveRejectOSEA : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string other_value = "Others";
    static string prostudent = "Student";
    static string proalliedhealth = "Allied Health";
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["GRP"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                if (Request.Params["INV"] != null)
                {
                    string regno = cFun.DecryptValue(Request.QueryString["INV"]);

                    if (!string.IsNullOrEmpty(flowid) && flowid != "0")
                    {

                    }
                    else
                    {
                        Response.Redirect("MasterRegistrationList_OSEA");
                    }
                }
                else
                {
                    Response.Redirect("MasterRegistrationList_OSEA");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["GRP"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());

                if (Request.Params["INV"] != null)
                {
                    string regno = cFun.DecryptValue(Request.QueryString["INV"]);

                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    DataTable dt = rgd.getRegDelegateByID(regno, showid);
                    if (dt.Rows.Count > 0)
                    {
                        populateUserDetails(dt);
                    }
                    else
                    {
                        Response.Redirect("MasterRegistrationList_OSEA");
                    }
                }
                else
                {
                    Response.Redirect("MasterRegistrationList_OSEA");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null && Request.Params["GRP"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
                string GroupRegID = cFun.DecryptValue(Request.QueryString["GRP"].ToString());

                if (Request.Params["INV"] != null)
                {
                    if (ddlStatus.SelectedIndex > 0)
                    {
                        int approvestatus = cFun.ParseInt(ddlStatus.SelectedItem.Value);
                        if (approvestatus == (int)RegApproveRejectStatus.Declined || approvestatus == (int)RegApproveRejectStatus.VIP
                                  || approvestatus == (int)RegApproveRejectStatus.Confirmed
                                  || approvestatus == (int)RegApproveRejectStatus.Approved || approvestatus == (int)RegApproveRejectStatus.Rejected)
                        {
                            string regno = cFun.DecryptValue(Request.QueryString["INV"]);
                            RegDelegateObj rdgObj = new RegDelegateObj(fn);
                            int isSuccess = rdgObj.UpdateApproveRejectStatus(regno, approvestatus, showid);//***update approve status

                            StatusSettings stuSettings = new StatusSettings(fn);
                            rdgObj.updateStatus(regno, stuSettings.Success, showid);//***update reg status
                            #region Logging change status
                            LogTableExecution lgtableexe = new LogTableExecution(fn);
                            lgtableexe.userid = regno;
                            lgtableexe.refid = regno;
                            lgtableexe.tablename = RegClass.typeDelegatetbl;
                            lgtableexe.whereclause = ExecuteType.wherestr + string.Format(" Regno='{0}' And RegGroupID='{1}' ", regno, GroupRegID);
                            lgtableexe.remarks = ExecuteType.updatestatusExe + GenStaticString.forstr + regno + GenStaticString.forstr + RegClass.typeDelegatetbl;
                            lgtableexe.saveTableExecutionLog();
                            #endregion

                            if (isSuccess > 0)
                            {
                                ////Send Email
                                EmailHelper eHelper = new EmailHelper();
                                string eType = EmailHTMLTemlateType.Confirmation;
                                string emailRegType = BackendRegType.backendRegType_Delegate;// "D";
                                string emailCondition = string.Empty;
                                if (approvestatus == (int)RegApproveRejectStatus.Declined)
                                {
                                    emailCondition = EmailSendConditionType.DECLINE;
                                }
                                else if (approvestatus == (int)RegApproveRejectStatus.VIP)
                                {
                                    emailCondition = EmailSendConditionType.VIP;
                                }
                                #region no need to send email (Comment)
                                //else if (approvestatus == (int)RegApproveRejectStatus.Confirmed)
                                //{
                                //    emailCondition = EmailSendConditionType.CONFIRM;
                                //}
                                //else if (approvestatus == (int)RegApproveRejectStatus.Approved)
                                //{
                                //    emailCondition = EmailSendConditionType.APPROVE;
                                //}
                                //else if (approvestatus == (int)RegApproveRejectStatus.Rejected)
                                //{
                                //    emailCondition = EmailSendConditionType.REJECT;
                                //}
                                #endregion

                                bool isEmailSend = false;
                                if (!string.IsNullOrEmpty(emailCondition) || !string.IsNullOrWhiteSpace(emailCondition))
                                {
                                    EmailDSourceKey sourcKey = new EmailDSourceKey();
                                    string showID = showid;
                                    string flowID = flowid;
                                    string groupID = GroupRegID;
                                    string delegateID = regno;

                                    sourcKey.AddKeyToList("ShowID", showID);
                                    sourcKey.AddKeyToList("RegGroupID", groupID);
                                    sourcKey.AddKeyToList("Regno", delegateID);
                                    sourcKey.AddKeyToList("FlowID", flowID);
                                    string updatedUser = string.Empty;
                                    if (Session["userid"] != null)
                                    {
                                        updatedUser = Session["userid"].ToString();
                                    }
                                    isEmailSend = eHelper.SendEmailFromBackendByCondition(emailCondition, sourcKey, emailRegType, updatedUser);

                                    #region Logging
                                    LogGenEmail lggenemail = new LogGenEmail(fn);
                                    LogActionObj lactObj = new LogActionObj();
                                    lggenemail.type = LogType.generalType;
                                    lggenemail.RefNumber = groupID + "," + regno;
                                    lggenemail.description = lactObj.actupdate + "reg_approveStatus";
                                    lggenemail.remark = "Update from backend " + cFun.DecryptValue(delegateID) + " and Send " + emailCondition + " email(email sending status:" + isEmailSend + ")";
                                    lggenemail.step = "backend";
                                    lggenemail.writeLog();
                                    #endregion
                                }

                                string msg = "Already updated"
                                    + ((approvestatus == (int)RegApproveRejectStatus.Declined || approvestatus == (int)RegApproveRejectStatus.VIP) ? " and sent email(email sending status:" + isEmailSend + ")." : ".");
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + msg + "');window.location.href='MasterRegistrationList_OSEA';", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Fail!');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please choose valid status!');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please choose status!');", true);
                    }
                }
                else
                {
                    Response.Redirect("MasterRegistrationList_OSEA");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }
    private void populateUserDetails(DataTable dtReg)
    {
        if (dtReg.Rows.Count > 0)
        {
            hfRegGroupID.Value = dtReg.Rows[0]["RegGroupID"].ToString();
            hfRegno.Value = dtReg.Rows[0]["Regno"].ToString();
            hfcategoryID.Value = dtReg.Rows[0]["con_CategoryId"].ToString();
            hfStage.Value = dtReg.Rows[0]["reg_Stage"].ToString();

            string reggroupid = dtReg.Rows[0]["RegGroupID"].ToString();
            string con_categoryid = dtReg.Rows[0]["con_CategoryId"].ToString();
            string reg_salutation = dtReg.Rows[0]["reg_Salutation"].ToString();
            string reg_fname = dtReg.Rows[0]["reg_FName"].ToString();
            string reg_lname = dtReg.Rows[0]["reg_LName"].ToString();
            string reg_email = dtReg.Rows[0]["reg_Email"].ToString();
            string reg_sgregistered = dtReg.Rows[0]["reg_sgregistered"].ToString();
            string reg_approvestatus = dtReg.Rows[0]["reg_approveStatus"].ToString();

            txtRegno.Text = dtReg.Rows[0]["Regno"].ToString();
            txtFName.Text = reg_fname;
            txtLName.Text = reg_lname;
            txtEmail.Text = reg_email;
            ddlStatus.ClearSelection();
            ListItem li = ddlStatus.Items.FindByValue(reg_approvestatus);
            if (li != null)
                li.Selected = true;

        }
    }
}