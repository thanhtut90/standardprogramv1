﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="QuestionnaireAddItem.aspx.cs" Inherits="QuestionnaireAddItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Manage Questionnaire</h1>

        </div>
        <div id="contentwrapper" class="contentwrapper">
            <asp:Panel runat="server" ID="Panel1">
                <div class="mybuttoncss" style="padding-bottom:10px;">
                    <asp:LinkButton ID="btnUpload" runat="server" CssClass="btn btn-success" OnClick="UploadQA">
                        <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Upload
                    </asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnClick="EditQA">
                        <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Edit
                    </asp:LinkButton>
                    <br /><br />
                    <iframe runat="server" id="questionnaireFrame" style="width:100%;height:900px"/>
                    <br /><br />
                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-success" OnClick="EndQA">
                        <span aria-hidden="true" class="glyphicon glyphicon-plus"></span> Next
                    </asp:LinkButton>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>

