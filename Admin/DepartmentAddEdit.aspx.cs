﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Utilities;
using Corpit.Registration;

public partial class Admin_DepartmentAddEdit : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected string shwID;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["SHW"] != null)
            {
                shwID = Request.QueryString["SHW"];
                lblShowID.Text = shwID;
                binddropdown();
                Master.setgenopen();

                if (Request.Params["dept"] != null)
                {
                    string instiID = Request.QueryString["dept"].ToString();
                    binddata(instiID);
                    btnSave.Text = "Update";
                }
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    #region binddropdown (bind data from ref_Institution table to ddlInstitution)
    public void binddropdown()
    {
        try
        {
            DataSet ds = new DataSet();
            CommonDataObj commonObj = new CommonDataObj(fn);
            ds = commonObj.getInstitutionList(shwID);

            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlInstitution.Items.Add("--Please Select--");
                ddlInstitution.Items[0].Value = "0";
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ddlInstitution.Items.Add(ds.Tables[0].Rows[i]["Institution"].ToString());
                    ddlInstitution.Items[i + 1].Value = ds.Tables[0].Rows[i]["ID"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region binddata (get data from ref_Department table and bind those data to respective controls)
    protected void binddata(string id)
    {
        string name = string.Empty;
        string sorting = string.Empty;
        string insti = string.Empty;

        DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand("Select * From ref_Department Where ID='" + id + "' and ShowID='" + shwID + "'", "ds");

        if (ds.Tables[0].Rows.Count != 0)
        {
            name = ds.Tables[0].Rows[0]["Department"].ToString();
            sorting = ds.Tables[0].Rows[0]["Sorting"].ToString();
            insti = ds.Tables[0].Rows[0]["InstitutionID"].ToString();

            txtName.Text = name;
            txtSorting.Text = sorting;
            try
            {
                if (!String.IsNullOrEmpty(insti))
                {
                    ListItem listItem = ddlInstitution.Items.FindByValue(insti);
                    if (listItem != null)
                    {
                        ddlInstitution.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
    #endregion

    #region SaveForm (save or updata data to ref_Department table)
    protected void SaveForm(object sender, EventArgs e)
    {
        try
        {
            string insti = string.Empty;
            string name = string.Empty;
            string sorting = string.Empty;

            insti = cFun.solveSQL(ddlInstitution.SelectedValue.ToString());
            name = cFun.solveSQL(txtName.Text.ToString());
            sorting = cFun.solveSQL(txtSorting.Text.ToString());

            try
            {
                SetUpController controller = new SetUpController(fn);
                DepartmentObj departmentObj = new DepartmentObj();
                departmentObj.institutionID = Convert.ToInt32(insti);
                departmentObj.department = name;
                departmentObj.sorting = Convert.ToInt32(sorting);
                departmentObj.ShowID = lblShowID.Text;

                int success = 0;
                if (Request.Params["dept"] == null)
                {
                    success = controller.insertDepartment(departmentObj);

                    if (success > 0)
                    {
                        clearform();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('New department added.');window.location='ManageDepartment.aspx?SHW=" + lblShowID.Text + "';", true);
                    }
                }
                else
                {
                    departmentObj.department_id = Convert.ToInt32(Request.QueryString["dept"].ToString());
                    success = controller.updateDepartment(departmentObj);

                    if (success > 0)
                    {
                        clearform();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Department updated.');window.location='ManageDepartment.aspx?SHW=" + lblShowID.Text + "';", true);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        catch (Exception ex)
        {
            pnlmessagebar.Visible = true;
            lblemsg.ForeColor = System.Drawing.Color.Red;
            lblemsg.Text = "Error occur: " + ex.Message;
        }
    }
    #endregion

    #region clearform (clear data from Controls)
    protected void clearform()
    {
        txtName.Text = string.Empty;
        txtSorting.Text = string.Empty;
        ddlInstitution.SelectedIndex = 0;
        btnSave.Text = "Save";
    }
    #endregion
}