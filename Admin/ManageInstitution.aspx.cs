﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.Registration;
using Corpit.Utilities;

public partial class Admin_ManageInstitution : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!IsPostBack)
            {
                if (Request.Params["SHW"] != null)
                {
                    lblShowid.Text = Request.QueryString["SHW"].ToString();

                    binOrganisation(lblShowid.Text);
                    BindGrid(lblShowid.Text);
                    //Master.setgenopen();
                }
                else
                {
                    Response.Redirect("~/404.aspx");
                }
            }
        }
    }

    #region BindGrid (bind data from ref_Institution, ref_Organisation tables to GridView(gvList)
    protected void BindGrid(string showid)
    {
        DataSet dsInstitution = new DataSet();
        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsInstitution = cmdObj.getInstitutionList(showid);
        if (dsInstitution.Tables[0].Rows.Count > 0)
        {
            gvList.DataSource = dsInstitution;
            gvList.DataBind();
            pnllist.Visible = true;
        }
        else
        {
            pnllist.Visible = false;
        }
    }
    #endregion

    #region gvcommand (delete data from ref_Institution table according to ID)
    protected void gvcommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "deleterow")
        {
            string ID = e.CommandArgument.ToString();

            InstitutionObj objInsti = new InstitutionObj();
            objInsti.ID = Convert.ToInt32(ID);
            objInsti.ShowID = lblShowid.Text;

            SetUpController setupCtrlr = new SetUpController(fn);
            int isSuccess = setupCtrlr.deleteInstitution(objInsti);

            BindGrid(lblShowid.Text);
        }
        else if(e.CommandName == "editrow")
        {
            string ID = e.CommandArgument.ToString();
            string showid = lblShowid.Text;

            SetUpController setupCtrlr = new SetUpController(fn);
            DataTable dt = setupCtrlr.getInstitutionByID(ID, showid);
            if(dt.Rows.Count > 0)
            {
                lblInstituionID.Text = ID;
                txtInstitutionName.Text = dt.Rows[0]["Institution"].ToString();
                string orgid = dt.Rows[0]["OrgID"].ToString();
                try
                {
                    if (!String.IsNullOrEmpty(orgid))
                    {
                        ListItem listItem = ddlOrganisation.Items.FindByValue(orgid);
                        if (listItem != null)
                        {
                            ddlOrganisation.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                txtAddress1.Text = dt.Rows[0]["Address1"].ToString();
                txtAddress2.Text = dt.Rows[0]["Address2"].ToString();
                txtZipCode.Text = dt.Rows[0]["ZipCode"].ToString();
                txtSortorder.Text = dt.Rows[0]["Sorting"].ToString();

                btnAdd.Text = "Update";
            }
        }
    }
    #endregion

    #region bindOrganisation
    private void binOrganisation(string showid)
    {
        DataSet dsOrganisation = new DataSet();
        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsOrganisation = cmdObj.getOrganization(showid);
        if (dsOrganisation.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsOrganisation.Tables[0].Rows.Count; i++)
            {
                ddlOrganisation.Items.Add(dsOrganisation.Tables[0].Rows[i]["Organisation"].ToString());
                ddlOrganisation.Items[i+1].Value = dsOrganisation.Tables[0].Rows[i]["ID"].ToString();
            }
        }
    }
    #endregion

    #region btnAdd_Click (Add/Update)
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string instiName = cFun.solveSQL(txtInstitutionName.Text.ToString());
        int orgid = Convert.ToInt32(ddlOrganisation.SelectedValue);
        string address1 = cFun.solveSQL(txtAddress1.Text.ToString());
        string address2 = cFun.solveSQL(txtAddress2.Text.ToString());
        string zipcode = cFun.solveSQL(txtZipCode.Text.ToString());
        string sortorder = txtSortorder.Text.ToString();
        string showid = lblShowid.Text;

        try
        {
            InstitutionObj objInsti = new InstitutionObj();
            objInsti.Institution = instiName;
            objInsti.OrgID = orgid;
            objInsti.Address1 = address1;
            objInsti.Address2 = address2;
            objInsti.ZipCode = zipcode;
            objInsti.Sorting = Convert.ToInt32(sortorder);
            objInsti.ShowID = showid;

            SetUpController setupCtrlr = new SetUpController(fn);
            if (string.IsNullOrEmpty(lblInstituionID.Text) || lblInstituionID.Text == "0")
            {
                int isSuccess = setupCtrlr.insertInstitution(objInsti);
            }
            else
            {
                objInsti.ID = Convert.ToInt32(lblInstituionID.Text);
                int isSuccess = setupCtrlr.updateInstitution(objInsti);
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }

        BindGrid(lblShowid.Text);
        clearControls();
    }
    #endregion

    #region Cancel
    private void clearControls()
    {
        lblInstituionID.Text = "";
        txtInstitutionName.Text = "";
        ddlOrganisation.SelectedIndex = 0;
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtZipCode.Text = "";
        txtSortorder.Text = "";
        btnAdd.Text = "Add";
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        clearControls();
    }
    #endregion
}