﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_Promo_Table : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null)
            {
                string showid = Request.QueryString["SHW"].ToString();

                bindPromo(showid);
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region bindPromo
    protected void bindPromo(string showid)
    {
        string query = "Select * From tb_PromoCode Where ShowID=@SHWID";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataSet ds = new DataSet();
        ds = fn.GetDatasetByCommand(query, "Ds", pList);
        if (ds.Tables[0].Rows.Count != 0)
        {
            gvList.DataSource = ds;
            gvList.DataBind();
        }
    }
    #endregion
}