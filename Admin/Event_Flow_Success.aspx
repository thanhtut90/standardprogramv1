﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Flow_Success.aspx.cs" Inherits="Event_Flow_Success" %>

<%@ MasterType VirtualPath="~/Admin/Master.master" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .page-header
        {
            font-size: 18px !important;
            border-bottom: 0 !important;
        }
        .box-header .box-title
        {
            margin: 10px 0 20px 0;
            font-size: 22px !important;
        }
        .box-header
        {
            padding:0px !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="centercontent">
        <div class="pageheader">
            <h1 class="pagetitle">Manage Success Page</h1>
        </div>

        <div id="contentwrapper" class="contentwrapper" style="padding-top: 50px;">
            <div style="padding-top: 40px;">
                <div class='page-header'>
                    <div class="form-horizontal">
                        <div class="box-body">
                            <div class="form-group">
                                <asp:Label ID="lblSuccessMsg" runat="server" CssClass="col-md-2 control-label">Success Page Message</asp:Label>
                                <div class="col-md-8">
                                    <CKEditor:CKEditorControl ID="txtSuccessMsg" runat="server" Height="300px" Width="98%" 
                                        BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2">&nbsp;</div>
                                <div class="col-md-8">
                                    <asp:LinkButton ID="btnUpdateFlowInfo" runat="server" OnClick="btnUpdateFlowInfo_Click" CssClass="btn btn-success">
                                        <span aria-hidden="true" class="glyphicon glyphicon-edit"></span> Save
                                    </asp:LinkButton>    <asp:Button ID="btnSetupEmail" runat="server" Text="Set up Email" OnClick="btnSetupEmail_Onclick" CssClass="btn btn-success"  />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="padding-left: 30%;">
                <asp:TextBox runat="server" ID="txtSelectedRoute" Visible="false"></asp:TextBox>
                <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
                <asp:TextBox runat="server" ID="txtFlowID" Visible="false" Text=""></asp:TextBox>
                <asp:TextBox runat="server" ID="txtSuccessMsgTmpID" Visible="false" Text=""></asp:TextBox>
                <telerik:RadWindowManager ID="RadWindowManager1" runat="server" ></telerik:RadWindowManager>
            </div>

        </div>

        <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" CausesValidation="false" CssClass="btn btn-success pull-right" />
     
    </div>
</asp:Content>

