﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Settings_WebSettings.aspx.cs" Inherits="Admin_Event_Settings_WebSettings" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="box-title">2-1. Manage Web Settings</h1>

    <br />
    <br />
            <div id="Event" class="subcontent">
                <asp:Panel ID="pnlmessagebar" runat="server" CssClass="notibar announcement" Visible="false">
                    <p class="txtgreen">
                        <asp:Label ID="lblemsg" runat="server" Text="Label"></asp:Label>
                    </p>
                </asp:Panel>
                <div class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group" style="display:none;">
                            <label class="col-md-2 control-label">Show Name</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtEventName" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Page Title</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txt_title" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Page Banner</label>
                            <div class="col-md-6">
                                <asp:FileUpload runat="server" ID="fupload"/>
                                <asp:Image runat="server" ID="imgbanner" Style="width: 400px;" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Footer/Copyright Message</label>
                            <div class="col-md-6">
                                <CKEditor:CKEditorControl ID="txtCopyright" runat="server" Height="200px" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label class="col-md-2 control-label">Site Master</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlsitemaster" runat="server" CssClass="form-control"></asp:DropDownList>
                                <asp:LinkButton ID="lnksitemaster" runat="server" Text="Manage Site Master" OnClick="lnksitemaster_Click"></asp:LinkButton>
                            </div>
                        </div>
                          <div class="form-group">
                            <label class="col-md-2 control-label">Site Re-login Master</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlsitereloginmaster" runat="server" CssClass="form-control"></asp:DropDownList>
                                <asp:LinkButton ID="lnksitereloginmaster" runat="server" Text="Manage Site Re-login Master" OnClick="lnksitereloginmaster_Click"></asp:LinkButton>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label class="col-md-2 control-label">Site CSS Bandle</label>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlcssbandle" runat="server" CssClass="form-control"></asp:DropDownList>
                                <asp:LinkButton ID="lnkcssbandle" runat="server" Text="Manage Site CSS Bandle" OnClick="lnkcssbandle_Click"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2 control-label"></div>
                            <div class="col-md-6">
                                <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="Back" CssClass="btn btn-primary" />
                                &nbsp;
                                <asp:Button ID="btnSave" runat="server" Text="Next" OnClick="SaveForm" CssClass="btn btn-primary" />
                                &nbsp;
                                <asp:Button ID="btnSavePreview" runat="server" Text="Save And Preview" OnClick="btnSavePreview_Click" CssClass="btn btn-primary" />
                            </div>
                        </div>

                        <telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>
                     </div>
                </div>
            </div>
</asp:Content>

