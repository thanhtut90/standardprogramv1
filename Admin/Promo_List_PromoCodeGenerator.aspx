﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Promo_List_PromoCodeGenerator.aspx.cs" Inherits="Admin_Promo_List_PromoCodeGenerator" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .rgOptions
        {
            display:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3 class="box-title">Promo List of <asp:Label ID="lblPromoName" runat="server" Text=""></asp:Label></h3>

    <asp:HyperLink ID="hplBack" runat="server" CssClass="btn btn-info"><i class="fa fa-arrow-left"></i> Back</asp:HyperLink>
    <br />
    <br />
    <div class="box-body table-responsive no-padding">
        <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" 
            EnableHeaderContextMenu="false" Skin="Bootstrap"
            EnableHeaderContextFilterMenu="false" AllowPaging="false" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="false"
            PageSize="20" OnItemCommand="GKeyMaster_ItemCommand" >
            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="false">
                <Selecting AllowRowSelect="false"></Selecting>
            </ClientSettings>
            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="PromoID" HeaderStyle-Font-Bold="true">
                <CommandItemSettings ShowExportToExcelButton="true"  ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                <Columns>
                    <telerik:GridTemplateColumn HeaderText="No." >
                        <ItemTemplate>
                            <%#Container.ItemIndex+1%>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridBoundColumn DataField="PromoCode" FilterControlAltText="Filter PromoCode"
                        HeaderText="PromoCode" SortExpression="PromoCode" UniqueName="PromoCode" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                    </telerik:GridBoundColumn>

                    <telerik:GridTemplateColumn HeaderText="Status" >
                        <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# setStatus(Eval("PromoCode").ToString(), Eval("PromoID").ToString())%>'
                            ForeColor='<%# setColor(Eval("PromoCode").ToString(), Eval("PromoID").ToString())%>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <%--<telerik:GridTemplateColumn HeaderText="Regno" >
                        <ItemTemplate>
                            <%#setRegno(Eval("PromoCode").ToString())%>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>--%>

                    <telerik:GridTemplateColumn HeaderText="Criteria" >
                        <ItemTemplate>
                            <%# setCriteria(Eval("PromoID").ToString(), Eval("ShowID").ToString())%>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Valid From" >
                        <ItemTemplate>
                            <asp:Label ID="lblFromDate" runat="server" Text='<%# Bind("Valid_FromDate", "{0:G}") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Valid To" >
                        <ItemTemplate>
                            <asp:Label ID="lblToDate" runat="server" Text='<%# Bind("Valid_ToDate", "{0:G}") %>'></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderText="Action" >
                        <ItemTemplate>
                            <asp:Button class="btn btn-primary" ID="btnCancelPromoCode" runat="server" CausesValidation="false"
                                     CommandArgument='<%# Eval("PromoID") %>' CommandName="CancelPromoCode" Text="Cancel Promo Code" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </div>
</asp:Content>

