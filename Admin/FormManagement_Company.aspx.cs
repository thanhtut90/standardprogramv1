﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Corpit.BackendMaster;
using Corpit.Utilities;
using Corpit.Site.Utilities;
using System.Data.SqlClient;
using Telerik.Web.UI;

public partial class Admin_FormManagement_Company : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFUnz = new CommonFuns();
    #region Form Parameters
    static string _Name = "Name";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _City = "City";
    static string _State = "State";
    static string _ZipCode = "Zip Code";
    static string _Country = "Country";
    static string _TelCC = "Telcc";
    static string _TelAC = "Telac";
    static string _Tel = "Tel";
    static string _FaxCC = "Faxcc";
    static string _FaxAC = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Website = "Website";
    static string _Additional1 = "Additional1";
    static string _Additional2 = "Additional2";
    static string _Additional3 = "Additional3";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string showid = cFUnz.DecryptValue(urlQuery.CurrShowID);
                string flowid = cFUnz.DecryptValue(urlQuery.FlowID);

                bool isCreated = checkCreated(flowid, showid);
                if (!isCreated)
                {
                    InsertNewForm(flowid, showid);
                }

                setDynamicForm(flowid, showid);

                if (Request.QueryString["STP"] != null && Request.QueryString["a"] == null)
                {
                    string curStep = cFUnz.DecryptValue(urlQuery.CurrIndex);

                    FlowMaster fMaster = new FlowMaster();
                    fMaster.FlowID = flowid;
                    fMaster.FlowStatus = curStep;
                    FlowControler fControler = new FlowControler(fn);
                    fControler.UpdateFlowMasterStatus(fMaster);
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region checkCreated (check the record count of tmp_Form table is equal to the record counts of tb_Form table)
    private bool checkCreated(string flowid, string showid)
    {
        bool isCreated = false;
        DataTable dt = fn.GetDatasetByCommand("Select * From tmp_Form Where form_type='" + FormType.TypeCompany + "'", "ds").Tables[0];

        string query = "Select * From tb_Form Where form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID=@SHWID And form_input_name In (Select form_input_name From tmp_Form Where form_type='" + FormType.TypeCompany + "')";
        List<SqlParameter> pList = new List<SqlParameter>();
        SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
        spar.Value = showid;
        pList.Add(spar);

        DataTable dtForm = fn.GetDatasetByCommand(query, "dsForm", pList).Tables[0];
        if (dt.Rows.Count == dtForm.Rows.Count)
        {
            isCreated = true;
        }

        return isCreated;
    }
    #endregion

    #region Generate Dynamic Form and use table tb_Form & bind data to related controls
    protected void setDynamicForm(string flowid, string showid)
    {
        DataSet ds = new DataSet();
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForCompany();

        StringBuilder sb = new StringBuilder();
        for (int x = 0; x < ds.Tables[0].Rows.Count; x++) 
        {
            #region form_input_name=Name & set chkshowName and chkcompName checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtName textbox
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Name)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                    int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                    string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        chkshowName.Checked = true;

                        string jscript = string.Empty;
                        jscript = "document.getElementById('dvname').style.display = '';";
                        jscript += "document.getElementById('dvsumname').style.display = '';";
                        jscript += "document.getElementById('" + txtName.ClientID + "').style.display = '';";
                        sb.Append(jscript);

                        ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                        //txtSal.Attributes.Add("display", "");
                    }
                    else
                    {
                        chkshowName.Checked = false;
                    }

                    if (isrequired == 1)
                    {
                        chkcompName.Checked = true;
                    }
                    else
                    {
                        chkcompName.Checked = false;
                    }

                    if (issummary == 1)
                    {
                        chksumName.Checked = true;
                    }
                    else
                    {
                        chksumName.Checked = false;
                    }

                    txtName.Text = inputtext;
                }
                #endregion

            #region form_input_name=Address1 & set chkshowAddress1 and chkcompAddress1 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAddress1 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAddress1.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaddress1').style.display = '';";
                    jscript += "document.getElementById('dvsumaddress1').style.display = '';";
                    jscript += "document.getElementById('" + txtAddress1.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAddress1.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAddress1.Checked = true;
                }
                else
                {
                    chkcompAddress1.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAddress1.Checked = true;
                }
                else
                {
                    chksumAddress1.Checked = false;
                }

                txtAddress1.Text = inputtext;
            }
            #endregion

            #region form_input_name=Address2 & set chkshowAddress2 and chkcompAddress2 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAddress2 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAddress2.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaddress2').style.display = '';";
                    jscript += "document.getElementById('dvsumaddress2').style.display = '';";
                    jscript += "document.getElementById('" + txtAddress2.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAddress2.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAddress2.Checked = true;
                }
                else
                {
                    chkcompAddress2.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAddress2.Checked = true;
                }
                else
                {
                    chksumAddress2.Checked = false;
                }

                txtAddress2.Text = inputtext;
            }
            #endregion

            #region form_input_name=Address3 & set chkshowAddress3 and chkcompAddress3 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAddress3 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAddress3.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvaddress3').style.display = '';";
                    jscript += "document.getElementById('dvsumaddress3').style.display = '';";
                    jscript += "document.getElementById('" + txtAddress3.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAddress3.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAddress3.Checked = true;
                }
                else
                {
                    chkcompAddress3.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAddress3.Checked = true;
                }
                else
                {
                    chksumAddress3.Checked = false;
                }

                txtAddress3.Text = inputtext;
            }
            #endregion
            
            #region form_input_name=City & set chkshowCity and chkcompCity checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtCity textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowCity.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvcity').style.display = '';";
                    jscript += "document.getElementById('dvsumcity').style.display = '';";
                    jscript += "document.getElementById('" + txtCity.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowCity.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompCity.Checked = true;
                }
                else
                {
                    chkcompCity.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumCity.Checked = true;
                }
                else
                {
                    chksumCity.Checked = false;
                }

                txtCity.Text = inputtext;
            }
            #endregion

            #region form_input_name=State & set chkshowState and chkcompState checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtState textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowState.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvstate').style.display = '';";
                    jscript += "document.getElementById('dvsumstate').style.display = '';";
                    jscript += "document.getElementById('" + txtState.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowState.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompState.Checked = true;
                }
                else
                {
                    chkcompState.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumState.Checked = true;
                }
                else
                {
                    chksumState.Checked = false;
                }

                txtState.Text = inputtext;
            }
            #endregion

            #region form_input_name=ZipCode & set chkshowZipCode and chkcompZipCode checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtZipCode textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _ZipCode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowZipCode.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvzipcode').style.display = '';";
                    jscript += "document.getElementById('dvsumzipcode').style.display = '';";
                    jscript += "document.getElementById('" + txtZipCode.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowZipCode.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompZipCode.Checked = true;
                }
                else
                {
                    chkcompZipCode.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumZipCode.Checked = true;
                }
                else
                {
                    chksumZipCode.Checked = false;
                }

                txtZipCode.Text = inputtext;
            }
            #endregion

            #region form_input_name=Country & set chkshowCountry and chkcompCountry checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtCountry textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowCountry.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvcountry').style.display = '';";
                    jscript += "document.getElementById('dvsumcountry').style.display = '';";
                    jscript += "document.getElementById('" + txtCountry.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowCountry.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompCountry.Checked = true;
                }
                else
                {
                    chkcompCountry.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumCountry.Checked = true;
                }
                else
                {
                    chksumCountry.Checked = false;
                }

                txtCountry.Text = inputtext;
            }
            #endregion

            #region form_input_name=TelCC & set chkshowTelCC and chkcompTelCC checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtTelCC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _TelCC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowTelCC.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvtelcc').style.display = '';";
                    jscript += "document.getElementById('dvsumtelcc').style.display = '';";
                    jscript += "document.getElementById('" + txtTelCC.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowTelCC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompTelCC.Checked = true;
                }
                else
                {
                    chkcompTelCC.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumTelCC.Checked = true;
                }
                else
                {
                    chksumTelCC.Checked = false;
                }

                txtTelCC.Text = inputtext;
            }
            #endregion

            #region form_input_name=TelAC & set chkshowTelAC and chkcompTelAC checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtTelAC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _TelAC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowTelAC.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvtelac').style.display = '';";
                    jscript += "document.getElementById('dvsumtelac').style.display = '';";
                    jscript += "document.getElementById('" + txtTelAC.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowTelAC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompTelAC.Checked = true;
                }
                else
                {
                    chkcompTelAC.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumTelAC.Checked = true;
                }
                else
                {
                    chksumTelAC.Checked = false;
                }


                txtTelAC.Text = inputtext;
            }
            #endregion

            #region form_input_name=Tel & set chkshowTel and chkcompTel checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtTel textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowTel.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvtel').style.display = '';";
                    jscript += "document.getElementById('dvsumtel').style.display = '';";
                    jscript += "document.getElementById('" + txtTel.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowTel.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompTel.Checked = true;
                }
                else
                {
                    chkcompTel.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumTel.Checked = true;
                }
                else
                {
                    chksumTel.Checked = false;
                }

                txtTel.Text = inputtext;
            }
            #endregion

            #region form_input_name=FaxCC & set chkshowFaxCC and chkcompFaxCC checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtFaxCC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _FaxCC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowFaxCC.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvfaxcc').style.display = '';";
                    jscript += "document.getElementById('dvsumfaxcc').style.display = '';";
                    jscript += "document.getElementById('" + txtFaxCC.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowFaxCC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompFaxCC.Checked = true;
                }
                else
                {
                    chkcompFaxCC.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumFaxCC.Checked = true;
                }
                else
                {
                    chksumFaxCC.Checked = false;
                }

                txtFaxCC.Text = inputtext;
            }
            #endregion

            #region form_input_name=FaxAC & set chkshowFaxAC and chkcompFaxAC checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtFaxAC textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _FaxAC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowFaxAC.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvfaxac').style.display = '';";
                    jscript += "document.getElementById('dvsumfaxac').style.display = '';";
                    jscript += "document.getElementById('" + txtFaxAC.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowFaxAC.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompFaxAC.Checked = true;
                }
                else
                {
                    chkcompFaxAC.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumFaxAC.Checked = true;
                }
                else
                {
                    chksumFaxAC.Checked = false;
                }

                txtFaxAC.Text = inputtext;
            }
            #endregion

            #region form_input_name=Fax & set chkshowFax and chkcompFax checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtFax textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowFax.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvfax').style.display = '';";
                    jscript += "document.getElementById('dvsumfax').style.display = '';";
                    jscript += "document.getElementById('" + txtFax.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowFax.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompFax.Checked = true;
                }
                else
                {
                    chkcompFax.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumFax.Checked = true;
                }
                else
                {
                    chksumFax.Checked = false;
                }
                txtFax.Text = inputtext;
            }
            #endregion

            #region form_input_name=Email & set chkshowEmail and chkcompEmail checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtEmail textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowEmail.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvemail').style.display = '';";
                    jscript += "document.getElementById('dvsumemail').style.display = '';";
                    jscript += "document.getElementById('" + txtEmail.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowEmail.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompEmail.Checked = true;
                }
                else
                {
                    chkcompEmail.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumEmail.Checked = true;
                }
                else
                {
                    chkcompEmail.Checked = false;
                }


                txtEmail.Text = inputtext;
            }
            #endregion

            #region form_input_name=EmailConfirmation & set chkshowEmailConfirmation and chkcompEmailConfirmation checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtEmailConfirmation textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _EmailConfirmation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowEmailConfirmation.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvemailconfirmation').style.display = '';";
                    jscript += "document.getElementById('dvsumemailconfirmation').style.display = '';";
                    jscript += "document.getElementById('" + txtEmailConfirmation.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowEmailConfirmation.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompEmailConfirmation.Checked = true;
                }
                else
                {
                    chkcompEmailConfirmation.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumEmailConfirmation.Checked = true;
                }
                else
                {
                    chksumEmailConfirmation.Checked = false;
                }

                txtEmailConfirmation.Text = inputtext;
            }
            #endregion

            #region form_input_name=Website & set chkshowWebsite and chkcompWebsite checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtWebsite textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Website)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowWebsite.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvwebsite').style.display = '';";
                    jscript += "document.getElementById('dvsumwebsite').style.display = '';";
                    jscript += "document.getElementById('" + txtWebsite.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowWebsite.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompWebsite.Checked = true;
                }
                else
                {
                    chkcompWebsite.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumWebsite.Checked = true;
                }
                else
                {
                    chksumWebsite.Checked = false;
                }

                txtWebsite.Text = inputtext;
            }
            #endregion

            #region form_input_name=Additional1 & set chkshowAdditional1 and chkcompAdditional1 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional1 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAdditional1.Checked = true;

                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvadditional1').style.display = '';";
                    jscript += "document.getElementById('dvsumadditional1').style.display = '';";
                    jscript += "document.getElementById('" + txtAdditional1.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAdditional1.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAdditional1.Checked = true;
                }
                else
                {
                    chkcompAdditional1.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAdditional1.Checked = true;
                }
                else
                {
                    chksumAdditional1.Checked = false;
                }

                txtAdditional1.Text = inputtext;
            }
            #endregion

            #region form_input_name=Additional2 & set chkshowAdditional2 and chkcompAdditional2 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional2 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAdditional2.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvadditional2').style.display = '';";
                    jscript += "document.getElementById('dvsumadditional2').style.display = '';";
                    jscript += "document.getElementById('" + txtAdditional2.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAdditional2.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAdditional2.Checked = true;
                }
                else
                {
                    chkcompAdditional2.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAdditional2.Checked = true;
                }
                else
                {
                    chksumAdditional2.Checked = false;
                }

                txtAdditional2.Text = inputtext;
            }
            #endregion

            #region form_input_name=Additional3 & set chkshowAdditional3 and chkcompAdditional3 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional3 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAdditional3.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvadditional3').style.display = '';";
                    jscript += "document.getElementById('dvsumadditional3').style.display = '';";
                    jscript += "document.getElementById('" + txtAdditional3.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAdditional3.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAdditional3.Checked = true;
                }
                else
                {
                    chkcompAdditional3.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAdditional3.Checked = true;
                }
                else
                {
                    chksumAdditional3.Checked = false;
                }

                txtAdditional3.Text = inputtext;
            }
            #endregion

            #region form_input_name=Additional4 & set chkshowAdditional4 and chkcompAdditional4 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional4 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAdditional4.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvadditional4').style.display = '';";
                    jscript += "document.getElementById('dvsumadditional4').style.display = '';";
                    jscript += "document.getElementById('" + txtAdditional4.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAdditional4.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAdditional4.Checked = true;
                }
                else
                {
                    chkcompAdditional4.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAdditional4.Checked = true;
                }
                else
                {
                    chksumAdditional4.Checked = false;
                }

                txtAdditional4.Text = inputtext;
            }
            #endregion

            #region form_input_name=Additional5 & set chkshowAdditional5 and chkcompAdditional5 checked properties true or false according to form_input_isshow and form_input_isrequired & set form_input_text value to txtAdditional5 textbox
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                string inputtext = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                int issummary = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                if (isshow == 1)
                {
                    chkshowAdditional5.Checked = true;
                    string jscript = string.Empty;
                    jscript = "document.getElementById('dvadditional5').style.display = '';";
                    jscript += "document.getElementById('dvsumadditional5').style.display = '';";
                    jscript += "document.getElementById('" + txtAdditional5.ClientID + "').style.display = '';";
                    sb.Append(jscript);

                    ////ClientScript.RegisterStartupScript(this.GetType(), "show", jscript, true);
                    //txtSal.Attributes.Add("display", "");
                }
                else
                {
                    chkshowAdditional5.Checked = false;
                }

                if (isrequired == 1)
                {
                    chkcompAdditional5.Checked = true;
                }
                else
                {
                    chkcompAdditional5.Checked = false;
                }
                if (issummary == 1)
                {
                    chksumAdditional5.Checked = true;
                }
                else
                {
                    chksumAdditional5.Checked = false;
                }

                txtAdditional5.Text = inputtext;
            }
            #endregion
        }
        ClientScript.RegisterStartupScript(this.GetType(), "show", sb.ToString(), true);
    }
    #endregion 

    #region SaveForm and using tb_Form table & update tb_Form table set form_input_isshow, form_input_isrequired and form_input_text fields according to form_input_name
    protected void SaveForm(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = cFUnz.DecryptValue(Request.QueryString["SHW"].ToString());
            string flowid = cFUnz.DecryptValue(Request.QueryString["FLW"].ToString());

            try
            {
                #region Declaration
                string strshowName = "0";
                string strcompName = "0";
                string strName = string.Empty;
                string strsumName = "0";

                string strshowAddress1 = "0";
                string strcompAddress1 = "0";
                string strAddress1 = string.Empty;
                string strsumAddress1 = "0";

                string strshowAddress2 = "0";
                string strcompAddress2 = "0";
                string strAddress2 = string.Empty;
                string strsumAddress2 = "0";

                string strshowAddress3 = "0";
                string strcompAddress3 = "0";
                string strAddress3 = string.Empty;
                string strsumAddress3 = "0";

                string strshowCity = "0";
                string strcompCity = "0";
                string strCity = string.Empty;
                string strsumCity = "0";

                string strshowState = "0";
                string strcompState = "0";
                string strState = string.Empty;
                string strsumState = "0";

                string strshowZipCode = "0";
                string strcompZipCode = "0";
                string strZipCode = string.Empty;
                string strsumZipCode = "0";

                string strshowCountry = "0";
                string strcompCountry = "0";
                string strCountry = string.Empty;
                string strsumCountry = "0";

                string strshowTelCC = "0";
                string strcompTelCC = "0";
                string strTelCC = string.Empty;
                string strsumTelCC = "0";

                string strshowTelAC = "0";
                string strcompTelAC = "0";
                string strTelAC = string.Empty;
                string strsumTelAC = "0";

                string strshowTel = "0";
                string strcompTel = "0";
                string strTel = string.Empty;
                string strsumTel = "0";

                string strshowFaxCC = "0";
                string strcompFaxCC = "0";
                string strFaxCC = string.Empty;
                string strsumFaxCC = "0";

                string strshowFaxAC = "0";
                string strcompFaxAC = "0";
                string strFaxAC = string.Empty;
                string strsumFaxAC = "0";

                string strshowFax = "0";
                string strcompFax = "0";
                string strFax = string.Empty;
                string strsumFax = "0";

                string strshowEmail = "0";
                string strcompEmail = "0";
                string strEmail = string.Empty;
                string strsumEmail = "0";

                string strshowEmailConfirmation = "0";
                string strcompEmailConfirmation = "0";
                string strEmailConfirmation = string.Empty;
                string strsumEmailConfirmation = "0";

                string strshowWebsite = "0";
                string strcompWebsite = "0";
                string strWebsite = string.Empty;
                string strsumWebsite = "0";

                string strshowAdditional1 = "0";
                string strcompAdditional1 = "0";
                string strAdditional1 = string.Empty;
                string strsumAdditional1 = "0";

                string strshowAdditional2 = "0";
                string strcompAdditional2 = "0";
                string strAdditional2 = string.Empty;
                string strsumAdditional2 = "0";

                string strshowAdditional3 = "0";
                string strcompAdditional3 = "0";
                string strAdditional3 = string.Empty;
                string strsumAdditional3 = "0";

                string strshowAdditional4 = "0";
                string strcompAdditional4 = "0";
                string strAdditional4 = string.Empty;
                string strsumAdditional4 = "0";

                string strshowAdditional5 = "0";
                string strcompAdditional5 = "0";
                string strAdditional5 = string.Empty;
                string strsumAdditional5 = "0";
                #endregion

                #region set variable values
                if (chkshowName.Checked == true)
                {
                    strshowName = "1";
                    strName = txtName.Text.ToString();
                    if (chkcompName.Checked == true)
                    {
                        strcompName = "1";
                    }
                    else
                    {
                        strcompName = "0";
                    }

                    if (chksumName.Checked == true)
                    {
                        strsumName = "1";
                    }
                    else
                    {
                        strsumName = "0";
                    }
                }
                if (chkshowAddress1.Checked == true)
                {
                    strshowAddress1 = "1";
                    strAddress1 = txtAddress1.Text.ToString();
                    if (chkcompAddress1.Checked == true)
                    {
                        strcompAddress1 = "1";
                    }
                    else
                    {
                        strcompAddress1 = "0";
                    }

                    if (chksumAddress1.Checked == true)
                    {
                        strsumAddress1 = "1";
                    }
                    else
                    {
                        strsumAddress1 = "0";
                    }
                }
                if (chkshowAddress2.Checked == true)
                {
                    strshowAddress2 = "1";
                    strAddress2 = txtAddress2.Text.ToString();
                    if (chkcompAddress2.Checked == true)
                    {
                        strcompAddress2 = "1";
                    }
                    else
                    {
                        strcompAddress2 = "0";
                    }

                    if (chksumAddress2.Checked == true)
                    {
                        strsumAddress2 = "1";
                    }
                    else
                    {
                        strsumAddress2 = "0";
                    }
                }
                if (chkshowAddress3.Checked == true)
                {
                    strshowAddress3 = "1";
                    strAddress3 = txtAddress3.Text.ToString();
                    if (chkcompAddress3.Checked == true)
                    {
                        strcompAddress3 = "1";
                    }
                    else
                    {
                        strcompAddress3 = "0";
                    }

                    if (chksumAddress3.Checked == true)
                    {
                        strsumAddress3 = "1";
                    }
                    else
                    {
                        strsumAddress3 = "0";
                    }
                }
                if (chkshowCity.Checked == true)
                {
                    strshowCity = "1";
                    strCity = txtCity.Text.ToString();
                    if (chkcompCity.Checked == true)
                    {
                        strcompCity = "1";
                    }
                    else
                    {
                        strcompCity = "0";
                    }

                    if (chksumCity.Checked == true)
                    {
                        strsumCity = "1";
                    }
                    else
                    {
                        strsumCity = "0";
                    }
                }
                if (chkshowState.Checked == true)
                {
                    strshowState = "1";
                    strState = txtState.Text.ToString();
                    if (chkcompState.Checked == true)
                    {
                        strcompState = "1";
                    }
                    else
                    {
                        strcompState = "0";
                    }

                    if (chksumState.Checked == true)
                    {
                        strsumState = "1";
                    }
                    else
                    {
                        strsumState = "0";
                    }
                }
                if (chkshowZipCode.Checked == true)
                {
                    strshowZipCode = "1";
                    strZipCode = txtZipCode.Text.ToString();
                    if (chkcompZipCode.Checked == true)
                    {
                        strcompZipCode = "1";
                    }
                    else
                    {
                        strcompZipCode = "0";
                    }

                    if (chksumZipCode.Checked == true)
                    {
                        strsumZipCode = "1";
                    }
                    else
                    {
                        strsumZipCode = "0";
                    }
                }
                if (chkshowCountry.Checked == true)
                {
                    strshowCountry = "1";
                    strCountry = txtCountry.Text.ToString();
                    if (chkcompCountry.Checked == true)
                    {
                        strcompCountry = "1";
                    }
                    else
                    {
                        strcompCountry = "0";
                    }

                    if (chksumCountry.Checked == true)
                    {
                        strsumCountry = "1";
                    }
                    else
                    {
                        strsumCountry = "0";
                    }
                }
                if (chkshowTelCC.Checked == true)
                {
                    strshowTelCC = "1";
                    strTelCC = txtTelCC.Text.ToString();
                    if (chkcompTelCC.Checked == true)
                    {
                        strcompTelCC = "1";
                    }
                    else
                    {
                        strcompTelCC = "0";
                    }

                    if (chksumTelCC.Checked == true)
                    {
                        strsumTelCC = "1";
                    }
                    else
                    {
                        strsumTelCC = "0";
                    }
                }
                if (chkshowTelAC.Checked == true)
                {
                    strshowTelAC = "1";
                    strTelAC = txtTelAC.Text.ToString();
                    if (chkcompTelAC.Checked == true)
                    {
                        strcompTelAC = "1";
                    }
                    else
                    {
                        strcompTelAC = "0";
                    }

                    if (chksumTelAC.Checked == true)
                    {
                        strsumTelAC = "1";
                    }
                    else
                    {
                        strsumTelAC = "0";
                    }
                }
                if (chkshowTel.Checked == true)
                {
                    strshowTel = "1";
                    strTel = txtTel.Text.ToString();
                    if (chkcompTel.Checked == true)
                    {
                        strcompTel = "1";
                    }
                    else
                    {
                        strcompTel = "0";
                    }

                    if (chksumTel.Checked == true)
                    {
                        strsumTel = "1";
                    }
                    else
                    {
                        strsumTel = "0";
                    }
                }
                if (chkshowFaxCC.Checked == true)
                {
                    strshowFaxCC = "1";
                    strFaxCC = txtFaxCC.Text.ToString();
                    if (chkcompFaxCC.Checked == true)
                    {
                        strcompFaxCC = "1";
                    }
                    else
                    {
                        strcompFaxCC = "0";
                    }

                    if (chksumFaxCC.Checked == true)
                    {
                        strsumFaxCC = "1";
                    }
                    else
                    {
                        strsumFaxCC = "0";
                    }
                }
                if (chkshowFaxAC.Checked == true)
                {
                    strshowFaxAC = "1";
                    strFaxAC = txtFaxAC.Text.ToString();
                    if (chkcompFaxAC.Checked == true)
                    {
                        strcompFaxAC = "1";
                    }
                    else
                    {
                        strcompFaxAC = "0";
                    }

                    if (chksumFaxAC.Checked == true)
                    {
                        strsumFaxAC = "1";
                    }
                    else
                    {
                        strsumFaxAC = "0";
                    }
                }
                if (chkshowFax.Checked == true)
                {
                    strshowFax = "1";
                    strFax = txtFax.Text.ToString();
                    if (chkcompFax.Checked == true)
                    {
                        strcompFax = "1";
                    }
                    else
                    {
                        strcompFax = "0";
                    }

                    if (chksumFax.Checked == true)
                    {
                        strsumFax = "1";
                    }
                    else
                    {
                        strsumFax = "0";
                    }
                }
                if (chkshowEmail.Checked == true)
                {
                    strshowEmail = "1";
                    strEmail = txtEmail.Text.ToString();
                    if (chkcompEmail.Checked == true)
                    {
                        strcompEmail = "1";
                    }
                    else
                    {
                        strcompEmail = "0";
                    }

                    if (chksumEmail.Checked == true)
                    {
                        strsumEmail = "1";
                    }
                    else
                    {
                        strsumEmail = "0";
                    }
                }
                if (chkshowEmailConfirmation.Checked == true)
                {
                    strshowEmailConfirmation = "1";
                    strEmailConfirmation = txtEmailConfirmation.Text.ToString();
                    if (chkcompEmailConfirmation.Checked == true)
                    {
                        strcompEmailConfirmation = "1";
                    }
                    else
                    {
                        strcompEmailConfirmation = "0";
                    }

                    if (chksumEmailConfirmation.Checked == true)
                    {
                        strsumEmailConfirmation = "1";
                    }
                    else
                    {
                        strsumEmailConfirmation = "0";
                    }
                }
                if (chkshowWebsite.Checked == true)
                {
                    strshowWebsite = "1";
                    strWebsite = txtWebsite.Text.ToString();
                    if (chkcompWebsite.Checked == true)
                    {
                        strcompWebsite = "1";
                    }
                    else
                    {
                        strcompWebsite = "0";
                    }

                    if (chksumWebsite.Checked == true)
                    {
                        strsumWebsite = "1";
                    }
                    else
                    {
                        strsumWebsite = "0";
                    }
                }
                if (chkshowAdditional1.Checked == true)
                {
                    strshowAdditional1 = "1";
                    strAdditional1 = txtAdditional1.Text.ToString();
                    if (chkcompAdditional1.Checked == true)
                    {
                        strcompAdditional1 = "1";
                    }
                    else
                    {
                        strcompAdditional1 = "0";
                    }

                    if (chksumAdditional1.Checked == true)
                    {
                        strsumAdditional1 = "1";
                    }
                    else
                    {
                        strsumAdditional1 = "0";
                    }
                }
                if (chkshowAdditional2.Checked == true)
                {
                    strshowAdditional2 = "1";
                    strAdditional2 = txtAdditional2.Text.ToString();
                    if (chkcompAdditional2.Checked == true)
                    {
                        strcompAdditional2 = "1";
                    }
                    else
                    {
                        strcompAdditional2 = "0";
                    }

                    if (chksumAdditional2.Checked == true)
                    {
                        strsumAdditional2 = "1";
                    }
                    else
                    {
                        strsumAdditional2 = "0";
                    }
                }
                if (chkshowAdditional3.Checked == true)
                {
                    strshowAdditional3 = "1";
                    strAdditional3 = txtAdditional3.Text.ToString();
                    if (chkcompAdditional3.Checked == true)
                    {
                        strcompAdditional3 = "1";
                    }
                    else
                    {
                        strcompAdditional3 = "0";
                    }

                    if (chksumAdditional3.Checked == true)
                    {
                        strsumAdditional3 = "1";
                    }
                    else
                    {
                        strsumAdditional3 = "0";
                    }
                }
                if (chkshowAdditional4.Checked == true)
                {
                    strshowAdditional4 = "1";
                    strAdditional4 = txtAdditional4.Text.ToString();
                    if (chkcompAdditional4.Checked == true)
                    {
                        strcompAdditional4 = "1";
                    }
                    else
                    {
                        strcompAdditional4 = "0";
                    }

                    if (chksumAdditional4.Checked == true)
                    {
                        strsumAdditional4 = "1";
                    }
                    else
                    {
                        strsumAdditional4 = "0";
                    }
                }
                if (chkshowAdditional5.Checked == true)
                {
                    strshowAdditional5 = "1";
                    strAdditional5 = txtAdditional5.Text.ToString();
                    if (chkcompAdditional5.Checked == true)
                    {
                        strcompAdditional5 = "1";
                    }
                    else
                    {
                        strcompAdditional5 = "0";
                    }

                    if (chksumAdditional5.Checked == true)
                    {
                        strsumAdditional5 = "1";
                    }
                    else
                    {
                        strsumAdditional5 = "0";
                    }
                }
                #endregion

                string sql1 = "Update tb_Form Set form_input_isshow=" + strshowName + ",form_input_isrequired=" + strcompName + ",form_input_text=N'" + strName + "',form_input_active=" + strsumName + " Where form_input_name='" + _Name + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress1 + ",form_input_isrequired=" + strcompAddress1 + ",form_input_text=N'" + strAddress1 + "',form_input_active=" + strsumAddress1 + " Where form_input_name='" + _Address1 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress2 + ",form_input_isrequired=" + strcompAddress2 + ",form_input_text=N'" + strAddress2 + "',form_input_active=" + strsumAddress2 + " Where form_input_name='" + _Address2 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress3 + ",form_input_isrequired=" + strcompAddress3 + ",form_input_text=N'" + strAddress3 + "',form_input_active=" + strsumAddress3 + " Where form_input_name='" + _Address3 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCity + ",form_input_isrequired=" + strcompCity + ",form_input_text=N'" + strCity + "',form_input_active=" + strsumCity + " Where form_input_name='" + _City + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowState + ",form_input_isrequired=" + strcompState + ",form_input_text=N'" + strState + "',form_input_active=" + strsumState + " Where form_input_name='" + _State + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowZipCode + ",form_input_isrequired=" + strcompZipCode + ",form_input_text=N'" + strZipCode + "',form_input_active=" + strsumZipCode + " Where form_input_name='" + _ZipCode + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCountry + ",form_input_isrequired=" + strcompCountry + ",form_input_text=N'" + strCountry + "',form_input_active=" + strsumCountry + " Where form_input_name='" + _Country + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelCC + ",form_input_isrequired=" + strcompTelCC + ",form_input_text=N'" + strTelCC + "',form_input_active=" + strsumTelCC + " Where form_input_name='" + _TelCC + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelAC + ",form_input_isrequired=" + strcompTelAC + ",form_input_text=N'" + strTelAC + "',form_input_active=" + strsumTelAC + " Where form_input_name='" + _TelAC + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTel + ",form_input_isrequired=" + strcompTel + ",form_input_text=N'" + strTel + "',form_input_active=" + strsumTel + " Where form_input_name='" + _Tel + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxCC + ",form_input_isrequired=" + strcompFaxCC + ",form_input_text=N'" + strFaxCC + "',form_input_active=" + strsumFaxCC + " Where form_input_name='" + _FaxCC + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxAC + ",form_input_isrequired=" + strcompFaxAC + ",form_input_text=N'" + strFaxAC + "',form_input_active=" + strsumFaxAC + " Where form_input_name='" + _FaxAC + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFax + ",form_input_isrequired=" + strcompFax + ",form_input_text=N'" + strFax + "',form_input_active=" + strsumFax + " Where form_input_name='" + _Fax + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmail + ",form_input_isrequired=" + strcompEmail + ",form_input_text=N'" + strEmail + "',form_input_active=" + strsumEmail + " Where form_input_name='" + _Email + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmailConfirmation + ",form_input_isrequired=" + strcompEmailConfirmation + ",form_input_text=N'" + strEmailConfirmation + "',form_input_active=" + strsumEmailConfirmation + " Where form_input_name='" + _EmailConfirmation + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowWebsite + ",form_input_isrequired=" + strcompWebsite + ",form_input_text=N'" + strWebsite + "',form_input_active=" + strsumWebsite + " Where form_input_name='" + _Website + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional1 + ",form_input_isrequired=" + strcompAdditional1 + ",form_input_text=N'" + strAdditional1 + "',form_input_active=" + strsumAdditional1 + " Where form_input_name='" + _Additional1 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional2 + ",form_input_isrequired=" + strcompAdditional2 + ",form_input_text=N'" + strAdditional2 + "',form_input_active=" + strsumAdditional2 + " Where form_input_name='" + _Additional2 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional3 + ",form_input_isrequired=" + strcompAdditional3 + ",form_input_text=N'" + strAdditional3 + "',form_input_active=" + strsumAdditional3 + " Where form_input_name='" + _Additional3 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional4 + ",form_input_isrequired=" + strcompAdditional4 + ",form_input_text=N'" + strAdditional4 + "',form_input_active=" + strsumAdditional4 + " Where form_input_name='" + _Additional4 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional5 + ",form_input_isrequired=" + strcompAdditional5 + ",form_input_text=N'" + strAdditional5 + "',form_input_active=" + strsumAdditional5 + " Where form_input_name='" + _Additional5 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";

                fn.ExecuteSQL(sql1);

                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-success alert-dismissible");
                lblMsg.Text = "Success.";

                setDynamicForm(flowid, showid);

                Dictionary<string, string> nValues = new Dictionary<string, string>();
                FlowControler fControl = new FlowControler(fn);
                string curstep = cFUnz.DecryptValue(Request.QueryString["STP"].ToString());
                nValues = fControl.GetAdminNextRoute(flowid, curstep);
                if (nValues.Count > 0)
                {
                    //*Update Currenct Flow Step Status
                    Flow flw = new Flow();
                    flw.FlowID = flowid;
                    flw.FlowConfigStatus = YesOrNo.Yes;
                    flw.FlowStep = curstep;
                    fControl.UpdateFlowIndivConfigStatus(flw);
                    //*

                    string path = string.Empty;
                    if (Request.Params["a"] != null)
                    {
                        string admintype = cFUnz.DecryptValue(Request.QueryString["a"].ToString());
                        if (admintype == BackendStaticValueClass.isFlowEdit)
                        {
                            string page = "Event_Flow_Dashboard.aspx";

                            path = fControl.MakeFullURL(page, flowid, showid);
                        }
                    }
                    else
                    {
                        string page = nValues["nURL"].ToString();
                        string step = nValues["nStep"].ToString();
                        string FlowID = nValues["FlowID"].ToString();

                        if (page == "") page = "Event_Config_Final";

                        path = fControl.MakeFullURL(page, FlowID, showid, "", step, null, "C");
                    }

                    if (path == "") path = "404.aspx";
                    Response.Redirect(path);
                }
            }
            catch (Exception ex)
            {
                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-danger alert-dismissible");
                lblMsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region InsertNewForm and using tb_Form & Insert new blank records but form_input_name, form_control_id, formType, ShowID into tb_Form table
    protected void InsertNewForm(string flowid, string showid)
    {
        try
        {
            int isShow = 0;
            int isRequired = 0;
            int isActive = 0;
            string inputText = string.Empty;
            string formType = "C";

            DataTable dt = fn.GetDatasetByCommand("Select * From tmp_Form Where form_type='" + FormType.TypeCompany + "'", "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string formname = dr["form_input_name"].ToString();
                    string controlid = dr["form_control_id"].ToString();
                    string orderno = dr["form_input_order"].ToString();

                    string query = "Select * From tb_Form Where FlowID='" + flowid + "' And ShowID=@SHWID And form_input_name='" + formname + "' And form_type='" + FormType.TypeCompany + "'";
                    List<SqlParameter> pList = new List<SqlParameter>();
                    SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                    spar.Value = showid;
                    pList.Add(spar);

                    DataTable dtForm = fn.GetDatasetByCommand(query, "dsForm", pList).Tables[0];
                    if (dtForm.Rows.Count == 0)
                    {
                        string sql = string.Format("Insert Into tb_Form (form_input_name,form_control_id,form_input_isshow,form_input_isrequired,form_input_active,"
                                    + "form_input_order,form_input_text,form_type,ShowID,FlowID)"
                                    + " Values('{0}', '{1}', {2}, {3}, {4}, {5}, '{6}', '{7}', '{8}', '{9}')", formname, controlid, isShow, isRequired, isActive,
                                        orderno, inputText, formType, showid, flowid);

                        fn.ExecuteSQL(sql);
                    }
                }
            }
            setDynamicForm(flowid, showid);
        }
        catch (Exception ex)
        {
            divMsg.Visible = true;
            divMsg.Attributes.Remove("class");
            divMsg.Attributes.Add("class", "alert alert-danger alert-dismissible");
            lblMsg.Text = "Error occur: " + ex.Message;
        }
    }
    #endregion

    #region btnSavePreview_Click
    protected void btnSavePreview_Click(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null && Request.Params["FLW"] != null)
        {
            string showid = cFUnz.DecryptValue(Request.QueryString["SHW"].ToString());
            string flowid = cFUnz.DecryptValue(Request.QueryString["FLW"].ToString());

            try
            {
                #region Declaration
                string strshowName = "0";
                string strcompName = "0";
                string strName = string.Empty;
                string strsumName = "0";

                string strshowAddress1 = "0";
                string strcompAddress1 = "0";
                string strAddress1 = string.Empty;
                string strsumAddress1 = "0";

                string strshowAddress2 = "0";
                string strcompAddress2 = "0";
                string strAddress2 = string.Empty;
                string strsumAddress2 = "0";

                string strshowAddress3 = "0";
                string strcompAddress3 = "0";
                string strAddress3 = string.Empty;
                string strsumAddress3 = "0";

                string strshowCity = "0";
                string strcompCity = "0";
                string strCity = string.Empty;
                string strsumCity = "0";

                string strshowState = "0";
                string strcompState = "0";
                string strState = string.Empty;
                string strsumState = "0";

                string strshowZipCode = "0";
                string strcompZipCode = "0";
                string strZipCode = string.Empty;
                string strsumZipCode = "0";

                string strshowCountry = "0";
                string strcompCountry = "0";
                string strCountry = string.Empty;
                string strsumCountry = "0";

                string strshowTelCC = "0";
                string strcompTelCC = "0";
                string strTelCC = string.Empty;
                string strsumTelCC = "0";

                string strshowTelAC = "0";
                string strcompTelAC = "0";
                string strTelAC = string.Empty;
                string strsumTelAC = "0";

                string strshowTel = "0";
                string strcompTel = "0";
                string strTel = string.Empty;
                string strsumTel = "0";

                string strshowFaxCC = "0";
                string strcompFaxCC = "0";
                string strFaxCC = string.Empty;
                string strsumFaxCC = "0";

                string strshowFaxAC = "0";
                string strcompFaxAC = "0";
                string strFaxAC = string.Empty;
                string strsumFaxAC = "0";

                string strshowFax = "0";
                string strcompFax = "0";
                string strFax = string.Empty;
                string strsumFax = "0";

                string strshowEmail = "0";
                string strcompEmail = "0";
                string strEmail = string.Empty;
                string strsumEmail = "0";

                string strshowEmailConfirmation = "0";
                string strcompEmailConfirmation = "0";
                string strEmailConfirmation = string.Empty;
                string strsumEmailConfirmation = "0";

                string strshowWebsite = "0";
                string strcompWebsite = "0";
                string strWebsite = string.Empty;
                string strsumWebsite = "0";

                string strshowAdditional1 = "0";
                string strcompAdditional1 = "0";
                string strAdditional1 = string.Empty;
                string strsumAdditional1 = "0";

                string strshowAdditional2 = "0";
                string strcompAdditional2 = "0";
                string strAdditional2 = string.Empty;
                string strsumAdditional2 = "0";

                string strshowAdditional3 = "0";
                string strcompAdditional3 = "0";
                string strAdditional3 = string.Empty;
                string strsumAdditional3 = "0";

                string strshowAdditional4 = "0";
                string strcompAdditional4 = "0";
                string strAdditional4 = string.Empty;
                string strsumAdditional4 = "0";

                string strshowAdditional5 = "0";
                string strcompAdditional5 = "0";
                string strAdditional5 = string.Empty;
                string strsumAdditional5 = "0";
                #endregion

                #region set variable values
                if (chkshowName.Checked == true)
                {
                    strshowName = "1";
                    strName = txtName.Text.ToString();
                    if (chkcompName.Checked == true)
                    {
                        strcompName = "1";
                    }
                    else
                    {
                        strcompName = "0";
                    }

                    if (chksumName.Checked == true)
                    {
                        strsumName = "1";
                    }
                    else
                    {
                        strsumName = "0";
                    }
                }
                if (chkshowAddress1.Checked == true)
                {
                    strshowAddress1 = "1";
                    strAddress1 = txtAddress1.Text.ToString();
                    if (chkcompAddress1.Checked == true)
                    {
                        strcompAddress1 = "1";
                    }
                    else
                    {
                        strcompAddress1 = "0";
                    }

                    if (chksumAddress1.Checked == true)
                    {
                        strsumAddress1 = "1";
                    }
                    else
                    {
                        strsumAddress1 = "0";
                    }
                }
                if (chkshowAddress2.Checked == true)
                {
                    strshowAddress2 = "1";
                    strAddress2 = txtAddress2.Text.ToString();
                    if (chkcompAddress2.Checked == true)
                    {
                        strcompAddress2 = "1";
                    }
                    else
                    {
                        strcompAddress2 = "0";
                    }

                    if (chksumAddress2.Checked == true)
                    {
                        strsumAddress2 = "1";
                    }
                    else
                    {
                        strsumAddress2 = "0";
                    }
                }
                if (chkshowAddress3.Checked == true)
                {
                    strshowAddress3 = "1";
                    strAddress3 = txtAddress3.Text.ToString();
                    if (chkcompAddress3.Checked == true)
                    {
                        strcompAddress3 = "1";
                    }
                    else
                    {
                        strcompAddress3 = "0";
                    }

                    if (chksumAddress3.Checked == true)
                    {
                        strsumAddress3 = "1";
                    }
                    else
                    {
                        strsumAddress3 = "0";
                    }
                }
                if (chkshowCity.Checked == true)
                {
                    strshowCity = "1";
                    strCity = txtCity.Text.ToString();
                    if (chkcompCity.Checked == true)
                    {
                        strcompCity = "1";
                    }
                    else
                    {
                        strcompCity = "0";
                    }

                    if (chksumCity.Checked == true)
                    {
                        strsumCity = "1";
                    }
                    else
                    {
                        strsumCity = "0";
                    }
                }
                if (chkshowState.Checked == true)
                {
                    strshowState = "1";
                    strState = txtState.Text.ToString();
                    if (chkcompState.Checked == true)
                    {
                        strcompState = "1";
                    }
                    else
                    {
                        strcompState = "0";
                    }

                    if (chksumState.Checked == true)
                    {
                        strsumState = "1";
                    }
                    else
                    {
                        strsumState = "0";
                    }
                }
                if (chkshowZipCode.Checked == true)
                {
                    strshowZipCode = "1";
                    strZipCode = txtZipCode.Text.ToString();
                    if (chkcompZipCode.Checked == true)
                    {
                        strcompZipCode = "1";
                    }
                    else
                    {
                        strcompZipCode = "0";
                    }

                    if (chksumZipCode.Checked == true)
                    {
                        strsumZipCode = "1";
                    }
                    else
                    {
                        strsumZipCode = "0";
                    }
                }
                if (chkshowCountry.Checked == true)
                {
                    strshowCountry = "1";
                    strCountry = txtCountry.Text.ToString();
                    if (chkcompCountry.Checked == true)
                    {
                        strcompCountry = "1";
                    }
                    else
                    {
                        strcompCountry = "0";
                    }

                    if (chksumCountry.Checked == true)
                    {
                        strsumCountry = "1";
                    }
                    else
                    {
                        strsumCountry = "0";
                    }
                }
                if (chkshowTelCC.Checked == true)
                {
                    strshowTelCC = "1";
                    strTelCC = txtTelCC.Text.ToString();
                    if (chkcompTelCC.Checked == true)
                    {
                        strcompTelCC = "1";
                    }
                    else
                    {
                        strcompTelCC = "0";
                    }

                    if (chksumTelCC.Checked == true)
                    {
                        strsumTelCC = "1";
                    }
                    else
                    {
                        strsumTelCC = "0";
                    }
                }
                if (chkshowTelAC.Checked == true)
                {
                    strshowTelAC = "1";
                    strTelAC = txtTelAC.Text.ToString();
                    if (chkcompTelAC.Checked == true)
                    {
                        strcompTelAC = "1";
                    }
                    else
                    {
                        strcompTelAC = "0";
                    }

                    if (chksumTelAC.Checked == true)
                    {
                        strsumTelAC = "1";
                    }
                    else
                    {
                        strsumTelAC = "0";
                    }
                }
                if (chkshowTel.Checked == true)
                {
                    strshowTel = "1";
                    strTel = txtTel.Text.ToString();
                    if (chkcompTel.Checked == true)
                    {
                        strcompTel = "1";
                    }
                    else
                    {
                        strcompTel = "0";
                    }

                    if (chksumTel.Checked == true)
                    {
                        strsumTel = "1";
                    }
                    else
                    {
                        strsumTel = "0";
                    }
                }
                if (chkshowFaxCC.Checked == true)
                {
                    strshowFaxCC = "1";
                    strFaxCC = txtFaxCC.Text.ToString();
                    if (chkcompFaxCC.Checked == true)
                    {
                        strcompFaxCC = "1";
                    }
                    else
                    {
                        strcompFaxCC = "0";
                    }

                    if (chksumFaxCC.Checked == true)
                    {
                        strsumFaxCC = "1";
                    }
                    else
                    {
                        strsumFaxCC = "0";
                    }
                }
                if (chkshowFaxAC.Checked == true)
                {
                    strshowFaxAC = "1";
                    strFaxAC = txtFaxAC.Text.ToString();
                    if (chkcompFaxAC.Checked == true)
                    {
                        strcompFaxAC = "1";
                    }
                    else
                    {
                        strcompFaxAC = "0";
                    }

                    if (chksumFaxAC.Checked == true)
                    {
                        strsumFaxAC = "1";
                    }
                    else
                    {
                        strsumFaxAC = "0";
                    }
                }
                if (chkshowFax.Checked == true)
                {
                    strshowFax = "1";
                    strFax = txtFax.Text.ToString();
                    if (chkcompFax.Checked == true)
                    {
                        strcompFax = "1";
                    }
                    else
                    {
                        strcompFax = "0";
                    }

                    if (chksumFax.Checked == true)
                    {
                        strsumFax = "1";
                    }
                    else
                    {
                        strsumFax = "0";
                    }
                }
                if (chkshowEmail.Checked == true)
                {
                    strshowEmail = "1";
                    strEmail = txtEmail.Text.ToString();
                    if (chkcompEmail.Checked == true)
                    {
                        strcompEmail = "1";
                    }
                    else
                    {
                        strcompEmail = "0";
                    }

                    if (chksumEmail.Checked == true)
                    {
                        strsumEmail = "1";
                    }
                    else
                    {
                        strsumEmail = "0";
                    }
                }
                if (chkshowEmailConfirmation.Checked == true)
                {
                    strshowEmailConfirmation = "1";
                    strEmailConfirmation = txtEmailConfirmation.Text.ToString();
                    if (chkcompEmailConfirmation.Checked == true)
                    {
                        strcompEmailConfirmation = "1";
                    }
                    else
                    {
                        strcompEmailConfirmation = "0";
                    }

                    if (chksumEmailConfirmation.Checked == true)
                    {
                        strsumEmailConfirmation = "1";
                    }
                    else
                    {
                        strsumEmailConfirmation = "0";
                    }
                }
                if (chkshowWebsite.Checked == true)
                {
                    strshowWebsite = "1";
                    strWebsite = txtWebsite.Text.ToString();
                    if (chkcompWebsite.Checked == true)
                    {
                        strcompWebsite = "1";
                    }
                    else
                    {
                        strcompWebsite = "0";
                    }

                    if (chksumWebsite.Checked == true)
                    {
                        strsumWebsite = "1";
                    }
                    else
                    {
                        strsumWebsite = "0";
                    }
                }
                if (chkshowAdditional1.Checked == true)
                {
                    strshowAdditional1 = "1";
                    strAdditional1 = txtAdditional1.Text.ToString();
                    if (chkcompAdditional1.Checked == true)
                    {
                        strcompAdditional1 = "1";
                    }
                    else
                    {
                        strcompAdditional1 = "0";
                    }

                    if (chksumAdditional1.Checked == true)
                    {
                        strsumAdditional1 = "1";
                    }
                    else
                    {
                        strsumAdditional1 = "0";
                    }
                }
                if (chkshowAdditional2.Checked == true)
                {
                    strshowAdditional2 = "1";
                    strAdditional2 = txtAdditional2.Text.ToString();
                    if (chkcompAdditional2.Checked == true)
                    {
                        strcompAdditional2 = "1";
                    }
                    else
                    {
                        strcompAdditional2 = "0";
                    }

                    if (chksumAdditional2.Checked == true)
                    {
                        strsumAdditional2 = "1";
                    }
                    else
                    {
                        strsumAdditional2 = "0";
                    }
                }
                if (chkshowAdditional3.Checked == true)
                {
                    strshowAdditional3 = "1";
                    strAdditional3 = txtAdditional3.Text.ToString();
                    if (chkcompAdditional3.Checked == true)
                    {
                        strcompAdditional3 = "1";
                    }
                    else
                    {
                        strcompAdditional3 = "0";
                    }

                    if (chksumAdditional3.Checked == true)
                    {
                        strsumAdditional3 = "1";
                    }
                    else
                    {
                        strsumAdditional3 = "0";
                    }
                }
                if (chkshowAdditional4.Checked == true)
                {
                    strshowAdditional4 = "1";
                    strAdditional4 = txtAdditional4.Text.ToString();
                    if (chkcompAdditional4.Checked == true)
                    {
                        strcompAdditional4 = "1";
                    }
                    else
                    {
                        strcompAdditional4 = "0";
                    }

                    if (chksumAdditional4.Checked == true)
                    {
                        strsumAdditional4 = "1";
                    }
                    else
                    {
                        strsumAdditional4 = "0";
                    }
                }
                if (chkshowAdditional5.Checked == true)
                {
                    strshowAdditional5 = "1";
                    strAdditional5 = txtAdditional5.Text.ToString();
                    if (chkcompAdditional5.Checked == true)
                    {
                        strcompAdditional5 = "1";
                    }
                    else
                    {
                        strcompAdditional5 = "0";
                    }

                    if (chksumAdditional5.Checked == true)
                    {
                        strsumAdditional5 = "1";
                    }
                    else
                    {
                        strsumAdditional5 = "0";
                    }
                }
                #endregion

                string sql1 = "Update tb_Form Set form_input_isshow=" + strshowName + ",form_input_isrequired=" + strcompName + ",form_input_text=N'" + strName + "',form_input_active=" + strsumName + " Where form_input_name='" + _Name + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress1 + ",form_input_isrequired=" + strcompAddress1 + ",form_input_text=N'" + strAddress1 + "',form_input_active=" + strsumAddress1 + " Where form_input_name='" + _Address1 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress2 + ",form_input_isrequired=" + strcompAddress2 + ",form_input_text=N'" + strAddress2 + "',form_input_active=" + strsumAddress2 + " Where form_input_name='" + _Address2 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAddress3 + ",form_input_isrequired=" + strcompAddress3 + ",form_input_text=N'" + strAddress3 + "',form_input_active=" + strsumAddress3 + " Where form_input_name='" + _Address3 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCity + ",form_input_isrequired=" + strcompCity + ",form_input_text=N'" + strCity + "',form_input_active=" + strsumCity + " Where form_input_name='" + _City + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowState + ",form_input_isrequired=" + strcompState + ",form_input_text=N'" + strState + "',form_input_active=" + strsumState + " Where form_input_name='" + _State + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowZipCode + ",form_input_isrequired=" + strcompZipCode + ",form_input_text=N'" + strZipCode + "',form_input_active=" + strsumZipCode + " Where form_input_name='" + _ZipCode + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowCountry + ",form_input_isrequired=" + strcompCountry + ",form_input_text=N'" + strCountry + "',form_input_active=" + strsumCountry + " Where form_input_name='" + _Country + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelCC + ",form_input_isrequired=" + strcompTelCC + ",form_input_text=N'" + strTelCC + "',form_input_active=" + strsumTelCC + " Where form_input_name='" + _TelCC + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTelAC + ",form_input_isrequired=" + strcompTelAC + ",form_input_text=N'" + strTelAC + "',form_input_active=" + strsumTelAC + " Where form_input_name='" + _TelAC + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowTel + ",form_input_isrequired=" + strcompTel + ",form_input_text=N'" + strTel + "',form_input_active=" + strsumTel + " Where form_input_name='" + _Tel + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxCC + ",form_input_isrequired=" + strcompFaxCC + ",form_input_text=N'" + strFaxCC + "',form_input_active=" + strsumFaxCC + " Where form_input_name='" + _FaxCC + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFaxAC + ",form_input_isrequired=" + strcompFaxAC + ",form_input_text=N'" + strFaxAC + "',form_input_active=" + strsumFaxAC + " Where form_input_name='" + _FaxAC + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowFax + ",form_input_isrequired=" + strcompFax + ",form_input_text=N'" + strFax + "',form_input_active=" + strsumFax + " Where form_input_name='" + _Fax + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmail + ",form_input_isrequired=" + strcompEmail + ",form_input_text=N'" + strEmail + "',form_input_active=" + strsumEmail + " Where form_input_name='" + _Email + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowEmailConfirmation + ",form_input_isrequired=" + strcompEmailConfirmation + ",form_input_text=N'" + strEmailConfirmation + "',form_input_active=" + strsumEmailConfirmation + " Where form_input_name='" + _EmailConfirmation + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowWebsite + ",form_input_isrequired=" + strcompWebsite + ",form_input_text=N'" + strWebsite + "',form_input_active=" + strsumWebsite + " Where form_input_name='" + _Website + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional1 + ",form_input_isrequired=" + strcompAdditional1 + ",form_input_text=N'" + strAdditional1 + "',form_input_active=" + strsumAdditional1 + " Where form_input_name='" + _Additional1 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional2 + ",form_input_isrequired=" + strcompAdditional2 + ",form_input_text=N'" + strAdditional2 + "',form_input_active=" + strsumAdditional2 + " Where form_input_name='" + _Additional2 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional3 + ",form_input_isrequired=" + strcompAdditional3 + ",form_input_text=N'" + strAdditional3 + "',form_input_active=" + strsumAdditional3 + " Where form_input_name='" + _Additional3 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional4 + ",form_input_isrequired=" + strcompAdditional4 + ",form_input_text=N'" + strAdditional4 + "',form_input_active=" + strsumAdditional4 + " Where form_input_name='" + _Additional4 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";
                sql1 += "Update tb_Form Set form_input_isshow=" + strshowAdditional5 + ",form_input_isrequired=" + strcompAdditional5 + ",form_input_text=N'" + strAdditional5 + "',form_input_active=" + strsumAdditional5 + " Where form_input_name='" + _Additional5 + "' And form_type='" + FormType.TypeCompany + "' And FlowID='" + flowid + "' And ShowID='" + showid + "'";

                fn.ExecuteSQL(sql1);

                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-success alert-dismissible");
                lblMsg.Text = "Success.";

                setDynamicForm(flowid, showid);

                FlowControler flwObj = new FlowControler(fn);
                Dictionary<string, string> nValues = new Dictionary<string, string>();
                nValues = flwObj.GetFrontendFormByBckForm(flowid, System.IO.Path.GetFileName(Request.Url.AbsolutePath));
                if (nValues.Count > 0)
                {
                    string page = nValues["page"].ToString();
                    string step = nValues["step"].ToString();
                    string route = flwObj.MakeFullURL(page, flowid, showid, null, step);
                    route = route + "&t=" + cFUnz.EncryptValue(BackendStaticValueClass.isAdmin);

                    System.Drawing.Rectangle resolution =
                    System.Windows.Forms.Screen.PrimaryScreen.Bounds;
                    int width = resolution.Width;
                    int height = resolution.Height;

                    if (width <= 1014)
                    {
                        width = width - 100;
                    }
                    else
                    {
                        width = 1000;
                    }

                    RadWindow newWindow = new RadWindow();
                    newWindow.NavigateUrl = route;
                    newWindow.Height = 800;
                    newWindow.Width = width;
                    newWindow.VisibleOnPageLoad = true;
                    newWindow.KeepInScreenBounds = true;
                    newWindow.VisibleOnPageLoad = true;
                    newWindow.Visible = true;
                    newWindow.VisibleStatusbar = false;
                    RadWindowManager1.Windows.Add(newWindow);
                }
            }
            catch (Exception ex)
            {
                divMsg.Visible = true;
                divMsg.Attributes.Remove("class");
                divMsg.Attributes.Add("class", "alert alert-danger alert-dismissible");
                lblMsg.Text = "Error occur: " + ex.Message;
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

}