﻿using Corpit.BackendMaster;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_DashboardMDA_NewRepeatChart : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFuz = new CommonFuns();
    CultureInfo cul = new CultureInfo("en-GB");
    QuesFunctionality qfn = new QuesFunctionality();
    private static string[] checkingMDAShowName = new string[] { "MFA 2018", "MMA 2018", "OSHA 2018" };
    private static string[] checkingMDAThaiShowName = new string[] { "MFT 2019", "PPI 2019", "TPlas 2019", "Wire and Tube 2019" };

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string[] showList = new string[2];

            try
            {
                if (Session["roleid"].ToString() == "1")
                {
                    showList = getAllShowList();
                }
                else
                {
                    showList = getShowList();
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }

            if (!string.IsNullOrEmpty(showList[0]))
            {
                loadChart(showList[0], true);
            }
            else
            {
                Response.Redirect("Event_Config.aspx");
            }
        }
    }
    protected string[] getAllShowList()
    {
        string query = "Select * from tb_Show Order By SHW_ID";
        string showList = string.Empty;
        string showName = string.Empty;

        DataTable dt = fn.GetDatasetByCommand(query, "dsShow").Tables[0];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (showList == string.Empty)
            {
                showList = dt.Rows[i]["SHW_ID"].ToString();
                showName = dt.Rows[i]["SHW_Name"].ToString();
            }
            else
            {
                showList += "," + dt.Rows[i]["SHW_ID"].ToString();
                showName += "," + dt.Rows[i]["SHW_Name"].ToString();
            }
        }
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        return list;
    }
    protected string[] getShowList()
    {
        string showList = string.Empty;
        string showName = string.Empty;
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = Session["userid"].ToString();
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                query = "Select * from tb_Show WHERE SHW_ID=@showID";
                spar = new SqlParameter("showID", SqlDbType.NVarChar);

                string[] showLists = dt.Rows[0]["us_showid"].ToString().Split(',');
                for (int i = 0; i < showLists.Length; i++)
                {
                    pList.Clear();
                    spar.Value = showLists[i];
                    pList.Add(spar);

                    DataTable dtShow = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];

                    if (showName == string.Empty)
                    {
                        showName = dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                    else
                    {
                        showName += "," + dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                }
                list[0] = dt.Rows[0]["us_showid"].ToString();
                list[1] = showName;
                return list;
            }
            return list;
        }
        catch (Exception ex)
        {
            return list;
        }
    }

    protected void loadChart(string showid, bool isShowMDANewRepeatChart)
    {
        Tuple<string, string> getType = checkFlowTypeGI(showid);

        try
        {
            if (getType != null)
            {
                #region MDA New Visitor or Repeat Visitor
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);
                if (checkingMDAShowName.Contains(shw.SHW_Name))
                {
                    divMDA.Visible = true;
                    if (isShowMDANewRepeatChart)
                    {
                        if (!string.IsNullOrEmpty(getType.Item1) && !string.IsNullOrEmpty(getType.Item2))
                        {
                            ChartMDA.DataSource = ByGroupIndivMDAChart(showid);
                            ChartMDA.DataBind();
                        }
                        else
                        {
                            ChartMDA.DataSource = ByGroupIndivMDAChart(showid);
                            ChartMDA.DataBind();
                        }
                    }
                }
                else if (checkingMDAThaiShowName.Contains(shw.SHW_Name))//MDA Thai
                {
                    divMDA.Visible = true;
                    if (isShowMDANewRepeatChart)
                    {
                        if (!string.IsNullOrEmpty(getType.Item1) && !string.IsNullOrEmpty(getType.Item2))
                        {
                            ChartMDA.DataSource = ByGroupIndivMDAThaiChart(showid);
                            ChartMDA.DataBind();
                        }
                        else
                        {
                            ChartMDA.DataSource = ByGroupIndivMDAThaiChart(showid);
                            ChartMDA.DataBind();
                        }
                    }
                }
                #endregion
            }
        }
        catch (Exception ex)
        { }
    }

    #region checkFlowTypeGI (get Group or Individual from tb_site_flow_master table)
    private Tuple<string, string> checkFlowTypeGI(string showid)
    {
        Tuple<string, string> tplType;
        GeneralObj grnObj = new GeneralObj(fn);
        DataTable dtFlowMas = grnObj.getAllSiteFlowMaster();

        string grp = string.Empty;
        string inv = string.Empty;
        if (dtFlowMas.Rows.Count > 0)
        {
            foreach (DataRow dr in dtFlowMas.Rows)
            {
                if (dr["ShowID"].ToString() == showid)
                {
                    string flowtype = dr["FLW_Type"].ToString();
                    if (flowtype == SiteFlowType.FLOW_GROUP)
                    {
                        grp = SiteFlowType.FLOW_GROUP;
                    }
                    else
                    {
                        inv = SiteFlowType.FLOW_INDIVIDUAL;
                    }
                }
            }
        }

        tplType = new Tuple<string, string>(grp, inv);

        return tplType;
    }
    #endregion
    #region checkNewVisitorMDA
    #region ByGroupIndivMDAChart
    private DataTable ByGroupIndivMDAChart(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Total");
        dt.Columns.Add("Desc");
        dt.Columns.Add("TotalPercentage");
        MasterRegGroup msregGroup = new MasterRegGroup(fn);
        msregGroup.ShowID = showid;
        DataTable dtmas = msregGroup.getRegByGroupIndivList();
        int masCount = dtmas.Rows.Count;
        Double catCount;
        Double TotalCount = masCount;


        DataTable dtSub = new DataTable();
        dtSub.Columns.Add("Total", typeof(int));
        dtSub.Columns.Add("Desc");
        dtSub.Columns.Add("TotalPercentage");
        int countMas = 0;
        if (dtmas.Rows.Count > 0)
        {
            foreach (DataRow drmas in dtmas.Rows)
            {
                string regno = drmas["Regno"].ToString();
                string flowid = drmas["reg_urlFlowID"] != DBNull.Value ? drmas["reg_urlFlowID"].ToString() : "";
                string NewRepeatVisitor = checkNewVisitorMDA(regno, flowid, showid);
                DataRow drow = dtSub.NewRow();
                drow["Desc"] = NewRepeatVisitor;
                drow["Total"] = countMas++;
                drow["TotalPercentage"] = "0";
                dtSub.Rows.Add(drow);
            }
        }

        if (dtSub.Rows.Count > 0)
        {
            DataTable dtTotal = dtSub.AsEnumerable()
                .GroupBy(r => new { Col1 = r["Desc"] })
                .Select(g =>
                {
                    var row = dtSub.NewRow();

                    row["Total"] = g.Count();// g.Min(r => r.Field<int>("Total"));
                    row["Desc"] = g.Key.Col1;
                    row["TotalPercentage"] = "0";

                    return row;

                })
                .CopyToDataTable();

            for (int i = 0; i < dtTotal.Rows.Count; i++)
            {
                catCount = cFuz.ParseInt(dtTotal.Rows[i]["Total"].ToString());
                Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
                DataRow drow = dt.NewRow();
                drow["Desc"] = dtTotal.Rows[i]["Desc"].ToString();
                drow["Total"] = catCount;
                drow["TotalPercentage"] = caldbscan.ToString();
                dt.Rows.Add(drow);
            }
        }
        else
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["Total"] = "0";
            drow["TotalPercentage"] = "0";
        }

        return dt;
    }
    private DataTable ByGroupIndivMDAThaiChart(string showid)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Total");
        //dtSub.Columns.Add("Total", typeof(int));
        dt.Columns.Add("Desc");
        dt.Columns.Add("TotalPercentage");
        DataTable dtmas = getMDAThaiNewRepeatList(showid);
        int masCount = dtmas.Rows.Count;
        if (dtmas.Rows.Count > 0)
        {
            foreach (DataRow drmas in dtmas.Rows)
            {
                //string regno = drmas["Regno"].ToString();
                DataRow drow = dt.NewRow();
                drow["Desc"] = drmas["NewRepeatType"];
                drow["Total"] = drmas["RegCount"] != DBNull.Value ? (!string.IsNullOrEmpty(drmas["RegCount"].ToString()) ? drmas["RegCount"].ToString() : "0") : "0";
                drow["TotalPercentage"] = drmas["RegPercentage"] != DBNull.Value ? (!string.IsNullOrEmpty(drmas["RegPercentage"].ToString()) ? drmas["RegPercentage"].ToString() : "0") : "0";
                dt.Rows.Add(drow);
            }
        }
        else
        {
            DataRow drow = dt.NewRow();
            drow["Desc"] = "No Record";
            drow["Total"] = "0";
            drow["TotalPercentage"] = "0";
        }

        return dt;
    }
    #endregion
    private static string _newVisitorMDA = "New Visitor";
    private static string _repeatVisitorMDA = "Repeat Visitor";
    private static string _checkQuestionMDA = "'Did you visit MEDICAL FAIR ASIA 2016?', 'Did you visit MEDICAL MANUFACTURING ASIA in 2016?', 'Did you visit OS+H Asia in 2016'";

    public string checkNewVisitorMDA(string regno, string flowid, string showid)
    {
        string result = string.Empty;
        try
        {
            result = _newVisitorMDA;
            if (!string.IsNullOrEmpty(regno) && !string.IsNullOrWhiteSpace(regno))
            {
                string qnaireID = getQnaireID(flowid, showid);

                string sqlResult = "SELECT * INTO #TempTable FROM tbl" + qnaireID + " where user_reference='" + regno + "' "
                                               + "ALTER TABLE #TempTable "
                                               + "DROP COLUMN qnaire_log_id,user_reference,status,create_date,update_date "
                                               + "SELECT * FROM #TempTable";
                DataTable dtResut = qfn.GetDatasetByCommand(sqlResult, "sqResult").Tables[0];

                //    string sql = "Select * From gen_QnaireLog Where qnaire_log_id In("
                //            + " Select qnaire_log_id From gen_QnaireResult Where qnaire_log_id In"
                //            + " (Select qnaire_log_id From gen_QnaireLog Where qnaire_id='" + qnaireID + "' And status='Active')"
                //            + " And qitem_id In (Select qitem_id From gen_QuestItem Where"
                //            + " quest_id In (Select quest_id From gen_Question Where quest_id In"
                //            + " (Select quest_id From gen_QnaireQuest Where qnaire_id='" + qnaireID + "' And status='Active')"
                //            + " And quest_desc In (" + _checkQuestionMDA + ") And status='Active')"
                //            + " And qitem_desc Like 'Yes' And status='Active')"
                //            + " And status='Active'"
                //            + " )"
                //            + " And user_reference='" + regno + "'";
                //DataTable dt = qfn.GetDatasetByCommand(sql, "ds").Tables[0];
                //if (dt.Rows.Count > 0)
                //{
                if (dtResut.Rows.Count > 0)
                {
                    ShowControler shwCtr = new ShowControler(fn);
                    Show shw = shwCtr.GetShow(showid);
                    if (shw.SHW_Name == checkingMDAShowName[0])
                    {
                        if (dtResut.Rows[0]["QITM5656"].ToString() == "1")
                        {
                            result = _repeatVisitorMDA;
                        }
                    }
                    else if (shw.SHW_Name == checkingMDAShowName[1])
                    {
                        if (dtResut.Rows[0]["QITM5764"].ToString() == "1")
                        {
                            result = _repeatVisitorMDA;
                        }
                    }
                    else if (shw.SHW_Name == checkingMDAShowName[2])
                    {
                        if (dtResut.Rows[0]["QITM5535"].ToString() == "1")
                        {
                            result = _repeatVisitorMDA;
                        }
                    }
                    else if (shw.SHW_Name == checkingMDAShowName[3])//MFT 2019
                    {
                        if (dtResut.Rows[0]["QITM5535"].ToString() == "1")
                        {
                            result = _repeatVisitorMDA;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    public string getQnaireID(string flowid, string showid)
    {
        string result = string.Empty;
        if (Session["userid"] != null)
        {
            try
            {
                FlowControler flwControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
                string flowType = flwMasterConfig.FlowType;
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                {
                    flowType = BackendRegType.backendRegType_Delegate;
                }
                result = getQID(showid, flowid, flowType);
            }
            catch (Exception ex)
            { }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }

        return result;
    }
    private string getQID(string showid, string flowid, string type)
    {
        QuestionnaireControler qCtr = new QuestionnaireControler(fn);
        DataTable dt = qCtr.getQID(type, flowid, showid);
        if (dt.Rows.Count > 0)
            return dt.Rows[0]["SQ_QAID"].ToString();
        else
            return "QLST";
    }

    private DataTable getMDAThaiNewRepeatList(string showid)
    {
        DataTable dtResut = new DataTable();
        try
        {
            string sql = "Select * From GetNewRepeatChartByGroupIndivListByShowID('" + showid + "')";
            dtResut = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        }
        catch(Exception ex)
        { }

        return dtResut;
    }
    #endregion

}