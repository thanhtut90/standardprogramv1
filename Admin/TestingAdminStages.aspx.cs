﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_TestingAdminStages : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["SHW"] != null)
        {
            string showid = cFun.DecryptValue(Request.Params["SHW"].ToString());

            if (Request.Params["FLW"] != null)
            {
                string flowid = cFun.DecryptValue(Request.Params["FLW"].ToString());

                //if (curid != "" && curid != null)
                //{
                //    txtSelectedRoute.Text = curid;
                //    bool isHasTemplate = LoadFlowDetails(curid);
                //    if (isHasTemplate) PanelKeyDetail.Visible = true;
                //    else PanelKeyDetail.Visible = false;
                //}
            }
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_Show table for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        DataTable NewDt = new DataTable();

        NewDt.Columns.Add("ID", typeof(string));
        NewDt.Columns.Add("Title", typeof(string));
        NewDt.Columns.Add("Link", typeof(string));

        FlowControler flwObj = new FlowControler(fn);
        for (int i = 1; i <= 2; i++)
        {
            DataRow newdr = NewDt.NewRow();

            newdr["ID"] = i;
            if (i == 1)
            {
                newdr["Title"] = "Delegate";

                string showID = "JLK1216";
                string page = "../RegDelegate.aspx";
                string FlowID = "F200";
                string step = "1";
                string route = flwObj.MakeFullURL(page, FlowID, showID, null, step);
                newdr["Link"] = route + "&t=" + cFun.EncryptValue("isadmin");
            }
            else if (i == 2)
            {
                newdr["Title"] = "Conference";

                string showID = "JLK1216";
                string page = "../Conference.aspx";
                string FlowID = "F200";
                string step = "2";
                string route = flwObj.MakeFullURL(page, FlowID, showID, null, step);
                newdr["Link"] = route + "&t=" + cFun.EncryptValue("isadmin");
            }

            NewDt.Rows.Add(newdr);
        }

        GKeyMaster.DataSource = NewDt;
    }
    #endregion

}