﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="FormManagement_Group.aspx.cs" Inherits="Admin_FormManagement_Group" ValidateRequest="false" EnableEventValidation="false" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        input[type="checkbox"], input[type="radio"]
        {
            margin : 4px !important;
        }
                label {
    font-weight: 400 !important;
}
    </style>

    <script type="text/javascript">
        function showField(type) {
            if (type == 'sal') {

                var cbox = document.getElementById('<%=chkshowSal.ClientID %>');
                var content1 = document.getElementById('dvsal');
                var content2 = document.getElementById('<%=txtSal.ClientID %>');
                var content3 = document.getElementById('dvsumsal');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'fname') {

                var cbox = document.getElementById('<%=chkshowFname.ClientID %>');
                var content1 = document.getElementById('dvfname');
                var content2 = document.getElementById('<%=txtFname.ClientID %>');
                var content3 = document.getElementById('dvsumfname');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'lname') {

                var cbox = document.getElementById('<%=chkshowLname.ClientID %>');
                var content1 = document.getElementById('dvlname');
                var content2 = document.getElementById('<%=txtLname.ClientID %>');
                var content3 = document.getElementById('dvsumlname');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'designation') {

                var cbox = document.getElementById('<%=chkshowDesignation.ClientID %>');
                var content1 = document.getElementById('dvdesignation');
                var content2 = document.getElementById('<%=txtDesignation.ClientID %>');
                var content3 = document.getElementById('dvsumdesignation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'department') {

                var cbox = document.getElementById('<%=chkshowDepartment.ClientID %>');
                var content1 = document.getElementById('dvdepartment');
                var content2 = document.getElementById('<%=txtDepartment.ClientID %>');
                var content3 = document.getElementById('dvsumdepartment');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'company') {

                var cbox = document.getElementById('<%=chkshowCompany.ClientID %>');
                var content1 = document.getElementById('dvcompany');
                var content2 = document.getElementById('<%=txtCompany.ClientID %>');
                var content3 = document.getElementById('dvsumcompany');
                if (cbox.checked) {
                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'industry') {

                var cbox = document.getElementById('<%=chkshowIndustry.ClientID %>');
                var content1 = document.getElementById('dvindustry');
                var content2 = document.getElementById('<%=txtIndustry.ClientID %>');
                var content3 = document.getElementById('dvsumindustry');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'address1') {

                var cbox = document.getElementById('<%=chkshowAddress1.ClientID %>');
                var content1 = document.getElementById('dvaddress1');
                var content2 = document.getElementById('<%=txtAddress1.ClientID %>');
                var content3 = document.getElementById('dvsumaddress1');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'address2') {

                var cbox = document.getElementById('<%=chkshowAddress2.ClientID %>');
                var content1 = document.getElementById('dvaddress2');
                var content2 = document.getElementById('<%=txtAddress2.ClientID %>');
                var content3 = document.getElementById('dvsumaddress2');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'address3') {

                var cbox = document.getElementById('<%=chkshowAddress3.ClientID %>');
                var content1 = document.getElementById('dvaddress3');
                var content2 = document.getElementById('<%=txtAddress3.ClientID %>');
                var content3 = document.getElementById('dvsumaddress3');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'city') {

                var cbox = document.getElementById('<%=chkshowCity.ClientID %>');
                var content1 = document.getElementById('dvcity');
                var content2 = document.getElementById('<%=txtCity.ClientID %>');
                var content3 = document.getElementById('dvsumcity');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'state') {

                var cbox = document.getElementById('<%=chkshowState.ClientID %>');
                var content1 = document.getElementById('dvstate');
                var content2 = document.getElementById('<%=txtState.ClientID %>');
                var content3 = document.getElementById('dvsumstate');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'postalcode') {

                var cbox = document.getElementById('<%=chkshowPostalCode.ClientID %>');
                var content1 = document.getElementById('dvpostalcode');
                var content2 = document.getElementById('<%=txtPostalCode.ClientID %>');
                var content3 = document.getElementById('dvsumpostalcode');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'country') {

                var cbox = document.getElementById('<%=chkshowCountry.ClientID %>');
                var content1 = document.getElementById('dvcountry');
                var content2 = document.getElementById('<%=txtCountry.ClientID %>');
                var content3 = document.getElementById('dvsumcountry');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'rcountry') {

                var cbox = document.getElementById('<%=chkshowRCountry.ClientID %>');
                var content1 = document.getElementById('dvrcountry');
                var content2 = document.getElementById('<%=txtRCountry.ClientID %>');
                var content3 = document.getElementById('dvsumrcountry');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'telcc') {

                var cbox = document.getElementById('<%=chkshowTelCC.ClientID %>');
                var content1 = document.getElementById('dvtelcc');
                var content2 = document.getElementById('<%=txtTelCC.ClientID %>');
                var content3 = document.getElementById('dvsumtelcc');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'telac') {

                var cbox = document.getElementById('<%=chkshowTelAC.ClientID %>');
                var content1 = document.getElementById('dvtelac');
                var content2 = document.getElementById('<%=txtTelAC.ClientID %>');
                var content3 = document.getElementById('dvsumtelac');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'tel') {

                var cbox = document.getElementById('<%=chkshowTel.ClientID %>');
                var content1 = document.getElementById('dvtel');
                var content2 = document.getElementById('<%=txtTel.ClientID %>');
                var content3 = document.getElementById('dvsumtel');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'mobilecc') {

                var cbox = document.getElementById('<%=chkshowMobileCC.ClientID %>');
                var content1 = document.getElementById('dvmobilecc');
                var content2 = document.getElementById('<%=txtMobileCC.ClientID %>');
                var content3 = document.getElementById('dvsummobilecc');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'mobileac') {

                var cbox = document.getElementById('<%=chkshowMobileAC.ClientID %>');
                var content1 = document.getElementById('dvmobileac');
                var content2 = document.getElementById('<%=txtMobileAC.ClientID %>');
                var content3 = document.getElementById('dvsummobileac');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'mobile') {

                var cbox = document.getElementById('<%=chkshowMobile.ClientID %>');
                var content1 = document.getElementById('dvmobile');
                var content2 = document.getElementById('<%=txtMobile.ClientID %>');
                var content3 = document.getElementById('dvsummobile');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'faxcc') {

                var cbox = document.getElementById('<%=chkshowFaxCC.ClientID %>');
                var content1 = document.getElementById('dvfaxcc');
                var content2 = document.getElementById('<%=txtFaxCC.ClientID %>');
                var content3 = document.getElementById('dvsumfaxcc');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }

            else if (type == 'faxac') {

                var cbox = document.getElementById('<%=chkshowFaxAC.ClientID %>');
                var content1 = document.getElementById('dvfaxac');
                var content2 = document.getElementById('<%=txtFaxAC.ClientID %>');
                var content3 = document.getElementById('dvsumfaxac');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }

            else if (type == 'fax') {

                var cbox = document.getElementById('<%=chkshowFax.ClientID %>');
                var content1 = document.getElementById('dvfax');
                var content2 = document.getElementById('<%=txtFax.ClientID %>');
                var content3 = document.getElementById('dvsumfax');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'email') {

                var cbox = document.getElementById('<%=chkshowEmail.ClientID %>');
                var content1 = document.getElementById('dvemail');
                var content2 = document.getElementById('<%=txtEmail.ClientID %>');
                var content3 = document.getElementById('dvsumemail');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'emailconfirmation') {

                var cbox = document.getElementById('<%=chkshowEmailConfirmation.ClientID %>');
                var content1 = document.getElementById('dvemailconfirmation');
                var content2 = document.getElementById('<%=txtEmailConfirmation.ClientID %>');
                var content3 = document.getElementById('dvsumemailconfirmation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }

            else if (type == 'visitdate') {

                var cbox = document.getElementById('<%=chkshowVisitDate.ClientID %>');
                var content1 = document.getElementById('dvvisitdate');
                var content2 = document.getElementById('<%=txtVisitDate.ClientID %>');
                var content3 = document.getElementById('dvsumvisitdate');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }

            else if (type == 'visittime') {

                var cbox = document.getElementById('<%=chkshowVisitTime.ClientID %>');
                var content1 = document.getElementById('dvvisittime');
                var content2 = document.getElementById('<%=txtVisitTime.ClientID %>');
                var content3 = document.getElementById('dvsumvisittime');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'password') {

                var cbox = document.getElementById('<%=chkshowPassword.ClientID %>');
                var content1 = document.getElementById('dvpassword');
                var content2 = document.getElementById('<%=txtPassword.ClientID %>');
                var content3 = document.getElementById('dvsumpassword');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'gender') {

                var cbox = document.getElementById('<%=chkshowGender.ClientID %>');
                var content1 = document.getElementById('dvgender');
                var content2 = document.getElementById('<%=txtGender.ClientID %>');
                var content3 = document.getElementById('dvsumgender');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'dob') {

                var cbox = document.getElementById('<%=chkshowDOB.ClientID %>');
                var content1 = document.getElementById('dvDOB');
                var content2 = document.getElementById('<%=txtDOB.ClientID %>');
                var content3 = document.getElementById('dvsumDOB');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'age') {

                var cbox = document.getElementById('<%=chkshowAge.ClientID %>');
                var content1 = document.getElementById('dvage');
                var content2 = document.getElementById('<%=txtAge.ClientID %>');
                var content3 = document.getElementById('dvsumage');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'additional4') {

                var cbox = document.getElementById('<%=chkshowAdditional4.ClientID %>');
                var content1 = document.getElementById('dvadditional4');
                var content2 = document.getElementById('<%=txtAdditional4.ClientID %>');
                var content3 = document.getElementById('dvsumadditional4');
                 if (cbox.checked) {

                     content1.style.display = '';
                     content2.style.display = '';
                     content3.style.display = '';

                 } else {
                     content1.style.display = 'none';
                     content2.style.display = 'none';
                     content3.style.display = 'none';
                 }
            }
            else if (type == 'additional5') {

                var cbox = document.getElementById('<%=chkshowAdditional5.ClientID %>');
                var content1 = document.getElementById('dvadditional5');
                var content2 = document.getElementById('<%=txtAdditional5.ClientID %>');
                var content3 = document.getElementById('dvsumadditional5');
                 if (cbox.checked) {

                     content1.style.display = '';
                     content2.style.display = '';
                     content3.style.display = '';

                 } else {
                     content1.style.display = 'none';
                     content2.style.display = 'none';
                     content3.style.display = 'none';
                 }
            }
            else if (type == 'othersalutation') {

                var cbox = document.getElementById('<%=chkshowOtherSalutation.ClientID %>');
                var content1 = document.getElementById('dvothersalutation');
                var content2 = document.getElementById('<%=txtOtherSalutation.ClientID %>');
                var content3 = document.getElementById('dvsumothersalutation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'otherdesignation') {

                var cbox = document.getElementById('<%=chkshowOtherDesignation.ClientID %>');
                var content1 = document.getElementById('dvotherdesignation');
                var content2 = document.getElementById('<%=txtOtherDesignation.ClientID %>');
                var content3 = document.getElementById('dvsumotherdesignation');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
            else if (type == 'otherindustry') {

                var cbox = document.getElementById('<%=chkshowOtherIndustry.ClientID %>');
                var content1 = document.getElementById('dvotherindustry');
                var content2 = document.getElementById('<%=txtOtherIndustry.ClientID %>');
                var content3 = document.getElementById('dvsumotherindustry');
                if (cbox.checked) {

                    content1.style.display = '';
                    content2.style.display = '';
                    content3.style.display = '';

                } else {
                    content1.style.display = 'none';
                    content2.style.display = 'none';
                    content3.style.display = 'none';
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h3 class="box-title">Manage Form (Contact Person)</h3>

    Select the field to be used for the registration form.
    <br />
    <br />

    <div class="row">
        <div class="col-md-6">
            <div class="box-body">
                <div class="alert alert-success alert-dismissible" id="divMsg" runat="server" visible="false">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <asp:Label ID="lblMsg" runat="server" ></asp:Label>
                </div>
            </div>
        </div>
    </div>

    <%--<p style="text-align:right;"><a class="ds_next" href='<%= ResolveClientUrl("~/Register.aspx") %>' target="_blank">Go To Registration Form</a></p>--%>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Field Name</th>
                <th>Compulsory Field</th>
                <th>Field Display Text</th>
                <th>Display in Summary & Report</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <asp:CheckBox ID="chkshowSal" runat="server" Text=" Salutation" onclick="showField('sal');" />
   ( <a href="ManageSalutation.aspx?SHW=<%=shwID %>" class="text-danger" target="_blank" >Manage Salutation</a>)
                </td>
                <td><div id="dvsal" style="display:none;"><asp:CheckBox ID="chkcompSal" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtSal" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumsal" style="display:none;"><asp:CheckBox ID="chksumSal" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowFname" runat="server" Text=" First Name" onclick="showField('fname');"/></td>
                <td><div id="dvfname" style="display:none;"><asp:CheckBox ID="chkcompFname" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtFname" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumfname" style="display:none;"><asp:CheckBox ID="chksumFname" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowLname" runat="server" Text=" Last Name" onclick="showField('lname');"/></td>
                <td><div id="dvlname" style="display:none;"><asp:CheckBox ID="chkcompLname" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtLname" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumlname" style="display:none;"><asp:CheckBox ID="chksumLname" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowDesignation" runat="server" Text=" Designation" onclick="showField('designation');"/></td>
                <td><div id="dvdesignation" style="display:none;"><asp:CheckBox ID="chkcompDesignation" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtDesignation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumdesignation" style="display:none;"><asp:CheckBox ID="chksumDesignation" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowDepartment" runat="server" Text=" Department" onclick="showField('department');"/></td>
                <td><div id="dvdepartment" style="display:none;"><asp:CheckBox ID="chkcompDepartment" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtDepartment" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumdepartment" style="display:none;"><asp:CheckBox ID="chksumDepartment" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowCompany" runat="server" Text=" Company" onclick="showField('company');"/></td>
                <td><div id="dvcompany" style="display:none;"><asp:CheckBox ID="chkcompCompany" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtCompany" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumcompany" style="display:none;"><asp:CheckBox ID="chksumCompany" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>                
            <tr>
                <td>
                    <asp:CheckBox ID="chkshowIndustry" runat="server" Text=" Industry" onclick="showField('industry');"/>
                    ( <a href="ManageIndustry.aspx?SHW=<%=shwID %>" class="text-danger" target="_blank" >Manage Industry</a>)
                </td>
                <td><div id="dvindustry" style="display:none;"><asp:CheckBox ID="chkcompIndustry" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtIndustry" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumindustry" style="display:none;"><asp:CheckBox ID="chksumIndustry" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAddress1" runat="server" Text=" Address1 " onclick="showField('address1');"/></td>
                <td><div id="dvaddress1" style="display:none;"><asp:CheckBox ID="chkcompAddress1" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAddress1" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaddress1" style="display:none;"><asp:CheckBox ID="chksumAddress1" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAddress2" runat="server" Text="Address2" onclick="showField('address2');"/></td>
                <td><div id="dvaddress2" style="display:none;"><asp:CheckBox ID="chkcompAddress2" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAddress2" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaddress2" style="display:none;"><asp:CheckBox ID="chksumAddress2" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
                
            <tr>
                <td><asp:CheckBox ID="chkshowAddress3" runat="server" Text=" Address3" onclick="showField('address3');"/></td>
                <td><div id="dvaddress3" style="display:none;"><asp:CheckBox ID="chkcompAddress3" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAddress3" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumaddress3" style="display:none;"><asp:CheckBox ID="chksumAddress3" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowCity" runat="server" Text=" City" onclick="showField('city');"/></td>
                <td><div id="dvcity" style="display:none;"><asp:CheckBox ID="chkcompCity" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtCity" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumcity" style="display:none;"><asp:CheckBox ID="chksumCity" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowState" runat="server" Text=" State" onclick="showField('state');"/></td>
                <td><div id="dvstate" style="display:none;"><asp:CheckBox ID="chkcompState" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtState" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumstate" style="display:none;"><asp:CheckBox ID="chksumState" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>                               
            <tr>
                <td><asp:CheckBox ID="chkshowPostalCode" runat="server" Text=" Postal Code" onclick="showField('postalcode');"/></td>
                <td><div id="dvpostalcode" style="display:none;"><asp:CheckBox ID="chkcompPostalCode" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtPostalCode" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumpostalcode" style="display:none;"><asp:CheckBox ID="chksumPostalCode" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowCountry" runat="server" Text=" Country" onclick="showField('country');"/>
                      (<a href="ManageCountry.aspx" class="text-danger" target="_blank" >Manage Country</a>)
                </td>
                <td><div id="dvcountry" style="display:none;"><asp:CheckBox ID="chkcompCountry" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtCountry" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumcountry" style="display:none;"><asp:CheckBox ID="chksumCountry" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowRCountry" runat="server" Text=" RCountry" onclick="showField('rcountry');"/></td>
                <td><div id="dvrcountry" style="display:none;"><asp:CheckBox ID="chkcompRCountry" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtRCountry" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumrcountry" style="display:none;"><asp:CheckBox ID="chksumRCountry" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowTelCC" runat="server" Text=" TelCC" onclick="showField('telcc');"/></td>
                <td><div id="dvtelcc" style="display:none;"><asp:CheckBox ID="chkcompTelCC" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtTelCC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumtelcc" style="display:none;"><asp:CheckBox ID="chksumTelCC" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowTelAC" runat="server" Text=" TelAC" onclick="showField('telac');"/></td>
                <td><div id="dvtelac" style="display:none;"><asp:CheckBox ID="chkcompTelAC" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtTelAC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumtelac" style="display:none;"><asp:CheckBox ID="chksumTelAC" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowTel" runat="server" Text=" Telephone" onclick="showField('tel');"/></td>
                <td><div id="dvtel" style="display:none;"><asp:CheckBox ID="chkcompTel" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtTel" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumtel" style="display:none;"><asp:CheckBox ID="chksumTel" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowMobileCC" runat="server" Text=" MobileCC" onclick="showField('mobilecc');"/></td>
                <td><div id="dvmobilecc" style="display:none;"><asp:CheckBox ID="chkcompMobileCC" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtMobileCC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsummobilecc" style="display:none;"><asp:CheckBox ID="chksumMobileCC" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowMobileAC" runat="server" Text=" MobileAC" onclick="showField('mobileac');"/></td>
                <td><div id="dvmobileac" style="display:none;"><asp:CheckBox ID="chkcompMobileAC" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtMobileAC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsummobileac" style="display:none;"><asp:CheckBox ID="chksumMobileAC" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowMobile" runat="server" Text=" Mobile" onclick="showField('mobile');"/></td>
                <td><div id="dvmobile" style="display:none;"><asp:CheckBox ID="chkcompMobile" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtMobile" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsummobile" style="display:none;"><asp:CheckBox ID="chksumMobile" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowFaxCC" runat="server" Text=" FaxCC" onclick="showField('faxcc');" /></td>
                <td><div id="dvfaxcc" style="display:none;"><asp:CheckBox ID="chkcompFaxCC" runat="server" Text="  Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtFaxCC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumfaxcc" style="display:none;"><asp:CheckBox ID="chksumFaxCC" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowFaxAC" runat="server" Text=" FaxAC" onclick="showField('faxac');"/></td>
                <td><div id="dvfaxac" style="display:none;"><asp:CheckBox ID="chkcompFaxAC" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtFaxAC" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumfaxac" style="display:none;"><asp:CheckBox ID="chksumFaxAC" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowFax" runat="server" Text=" Fax" onclick="showField('fax');"/></td>
                <td><div id="dvfax" style="display:none;"><asp:CheckBox ID="chkcompFax" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtFax" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumfax" style="display:none;"><asp:CheckBox ID="chksumFax" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowEmail" runat="server" Text=" Email" onclick="showField('email');"/></td>
                <td><div id="dvemail" style="display:none;"><asp:CheckBox ID="chkcompEmail" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtEmail" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumemail" style="display:none;"><asp:CheckBox ID="chksumEmail" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowEmailConfirmation" runat="server" Text="Email Confirmation" onclick="showField('emailconfirmation');"/></td>
                <td><div id="dvemailconfirmation" style="display:none;"><asp:CheckBox ID="chkcompEmailConfirmation" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtEmailConfirmation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumemailconfirmation" style="display:none;"><asp:CheckBox ID="chksumEmailConfirmation" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowVisitDate" runat="server" Text=" Visit Date" onclick="showField('visitdate');"/></td>
                <td><div id="dvvisitdate" style="display:none;"><asp:CheckBox ID="chkcompVisitDate" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtVisitDate" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumvisitdate" style="display:none;"><asp:CheckBox ID="chksumVisitDate" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowVisitTime" runat="server" Text=" Visit Time" onclick="showField('visittime');"/></td>
                <td><div id="dvvisittime" style="display:none;"><asp:CheckBox ID="chkcompVisitTime" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtVisitTime" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumvisittime" style="display:none;"><asp:CheckBox ID="chksumVisitTime" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowPassword" runat="server" Text=" Password" onclick="showField('password');"/></td>
                <td><div id="dvpassword" style="display:none;"><asp:CheckBox ID="chkcompPassword" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtPassword" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumpassword" style="display:none;"><asp:CheckBox ID="chksumPassword" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowGender" runat="server" Text=" Gender" onclick="showField('gender');"/></td>
                <td><div id="dvgender" style="display:none;"><asp:CheckBox ID="chkcompGender" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtGender" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumgender" style="display:none;"><asp:CheckBox ID="chksumGender" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowDOB" runat="server" Text=" DOB" onclick="showField('dob');"/></td>
                <td><div id="dvDOB" style="display:none;"><asp:CheckBox ID="chkcompDOB" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtDOB" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumDOB" style="display:none;"><asp:CheckBox ID="chksumDOB" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAge" runat="server" Text=" Age" onclick="showField('age');"/></td>
                <td><div id="dvage" style="display:none;"><asp:CheckBox ID="chkcompAge" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAge" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumage" style="display:none;"><asp:CheckBox ID="chksumAge" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAdditional4" runat="server" Text=" Additional4" onclick="showField('additional4');"/>
                    ( <a href="ManageRefAdditionalList.aspx?SHW=<%=shwID %>&TYP=<%= Corpit.Registration.RefAdditionalListType.Additional4 %>" class="text-danger" target="_blank" >Manage List Items(for dropdownlist Additional4 field)</a>)</td>
                <td><div id="dvadditional4" style="display:none;"><asp:CheckBox ID="chkcompAdditional4" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAdditional4" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumadditional4" style="display:none;"><asp:CheckBox ID="chksumAdditional4" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowAdditional5" runat="server" Text=" Additional5" onclick="showField('additional5');"/>
                    ( <a href="ManageRefAdditionalList.aspx?SHW=<%=shwID %>&TYP=<%= Corpit.Registration.RefAdditionalListType.Additional5 %>" class="text-danger" target="_blank" >Manage List Items(for dropdownlist Additional5 field)</a>)</td>
                <td><div id="dvadditional5" style="display:none;"><asp:CheckBox ID="chkcompAdditional5" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtAdditional5" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumadditional5" style="display:none;"><asp:CheckBox ID="chksumAdditional5" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowOtherSalutation" runat="server" Text=" Other Salutation" onclick="showField('othersalutation');"/></td>
                <td><div id="dvothersalutation" style="display:none;"><asp:CheckBox ID="chkcompOtherSalutation" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtOtherSalutation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumothersalutation" style="display:none;"><asp:CheckBox ID="chksumOtherSalutation" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowOtherDesignation" runat="server" Text="Other Designation" onclick="showField('otherdesignation');"/></td>
                <td><div id="dvotherdesignation" style="display:none;"><asp:CheckBox ID="chkcompOtherDesignation" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtOtherDesignation" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumotherdesignation" style="display:none;"><asp:CheckBox ID="chksumOtherDesignation" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
            <tr>
                <td><asp:CheckBox ID="chkshowOtherIndustry" runat="server" Text=" Other Industry" onclick="showField('otherindustry');"/></td>
                <td><div id="dvotherindustry" style="display:none;"><asp:CheckBox ID="chkcompOtherIndustry" runat="server" Text=" Mark as Compulsory" /></div></td>
                <td><asp:TextBox CssClass="form-control" ID="txtOtherIndustry" runat="server" style="display:none;"></asp:TextBox></td>
                <td><div id="dvsumotherindustry" style="display:none;"><asp:CheckBox ID="chksumOtherIndustry" runat="server" Text="  Display in Summary & Report" /></div></td>
            </tr>
        </tbody>
    </table>
    <asp:Button ID="btnSave" runat="server" Text="Save and Next" OnClick="SaveForm" CssClass="btn btn-primary"/>
    &nbsp;
    <asp:Button ID="btnSavePreview" runat="server" Text="Save And Preview" OnClick="btnSavePreview_Click" CssClass="btn btn-primary" />
    &nbsp;    <asp:Button ID="btnSetupEmail" runat="server" Text="Set up Email" OnClick="btnSetupEmail_Onclick" CssClass="btn btn-success"  />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>
</asp:Content>