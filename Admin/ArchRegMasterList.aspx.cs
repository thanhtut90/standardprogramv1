﻿using Corpit.Registration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Text;

public partial class ArchRegMasterList : System.Web.UI.Page
{
    #region DECLARATION
    static string SHOW_EN = "WVF342";
    static string SHOW_TH = "PXC343";
    Functionality fn = new Functionality();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindInfo();
            BindGrid(true);
        }
        else
        {
            BindGrid(false);
        }
    }

    protected void BindGrid(bool rebind, string keyword = "")
    {
        try
        {
            string query = string.Format("Select ROW_NUMBER() over (order by regno) as sn, * From (Select d.regno, d.reg_datecreated, d.reg_FName, d.reg_LName, d.reg_OName, d.reg_Email, d.reg_Tel, d.reg_Mobile, d.reg_TelCC, d.reg_TelAC, d.reg_MobCC, d.reg_MobAC, 'ARC-' + CONVERT(varchar(100), d.regNo) as refID, isnull(s.sal_name, d.reg_Salutation) as salutation, c.Country from tb_RegDelegate as d Left Join ref_Salutation as s on CONVERT(nvarchar(50),s.sal_ID) = d.reg_salutation Left Join ref_Country as c on c.cty_GUID = d.reg_Country Where reg_status=1 and (d.ShowId = '{0}' or d.ShowId = '{1}') and d.recycle = 0) RegDelegate Where(reg_FName like N'%{2}%' or reg_LName like N'%{2}%' or reg_OName like N'%{2}%' or reg_Email like N'%{2}%' or refID like N'%{2}%')", SHOW_EN, SHOW_TH, (!string.IsNullOrEmpty(keyword)) ? keyword : tbFilter.Text.Replace("=", "").Replace(";", "").Replace("'", "''"));

            hfQuery.Value = query;

            DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
            gvRegReport.DataSource = dt;

            if (rebind)
            {
                gvRegReport.DataBind();
                gvRegReport.SetPageIndex(0);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void BindInfo()
    {
        int total = 0;
        string query = "";
        DataTable dt;

        query = string.Format("Select regno from tb_RegDelegate as d inner join ref_country as c on c.cty_guid=d.reg_country Where d.reg_status=1 and (d.showid='{0}' or d.showid='{1}') and d.recycle=0 and country like 'thailand'", SHOW_EN, SHOW_TH);
        dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
        tdThaiRegCount.InnerText = dt.Rows.Count.ToString("N0");
        total += dt.Rows.Count;

        query = string.Format("Select regno from tb_RegDelegate as d inner join ref_country as c on c.cty_guid=d.reg_country Where d.reg_status=1 and (d.showid='{0}' or d.showid='{1}') and d.recycle=0 and country not like 'thailand'", SHOW_EN, SHOW_TH);
        dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
        tdOverseaRegCount.InnerText = dt.Rows.Count.ToString("N0");
        total += dt.Rows.Count;

        tdRegTotalCount.InnerText = total.ToString("N0");

        total = 0;
        query = string.Format("select regno from tb_regDelegate where reggroupid in (select distinct(reggroupid) from tb_regdelegate where reg_status=1 and (showid='{0}' or showid='{1}') and recycle=0 group by reggroupid having count(reggroupid)=1) and reg_status=1 and recycle=0", SHOW_EN, SHOW_TH);
        dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
        tdIndVisitorCount.InnerText = dt.Rows.Count.ToString("N0");
        total += dt.Rows.Count;

        query = string.Format("select regno from tb_regDelegate where reggroupid in (select distinct(reggroupid) from tb_regdelegate where reg_status=1 and (showid='{0}' or showid='{1}' ) and recycle=0 group by reggroupid having count(reggroupid)>1) and reg_status=1 and recycle=0", SHOW_EN, SHOW_TH);
        dt = fn.GetDatasetByCommand(query, "ds").Tables[0];
        tdOverseaVisitorCount.InnerText = dt.Rows.Count.ToString("N0");
    }

    public string GetAnswerByQuestDisplayNo(string regno, int questNo)
    {
        int count = 1;
        string answer = "";

        string query = string.Format("select l.regno, l.reggroupid, q.Question, a.Answer, r.QA_AnswerText, a.LabelInfrontCheckBox from tb_QAResultLog as l Left join tb_QAResult_Temp as r on r.QALogID = l.QALogID Left Join tb_QAQuestions_temp as q on q.QA_QuestId = r.QA_QuestId Left Join tb_QAAnswers_temp as a on a.QA_AnsId = r.QA_AnswerId where l.regno = '{0}' and q.Quest_DisplayNo = '{1}' and l.deleteflag = 0", regno, questNo);
        DataTable dt = fn.GetDatasetByCommand(query, "ds").Tables[0];

        if (dt.Rows.Count > 0)
        {
            foreach (DataRow row in dt.Rows)
            {
                if (questNo == 3)
                {
                    if (count > 1)
                    {
                        if (!string.IsNullOrEmpty(row["QA_AnswerText"].ToString()))
                            answer += (", " + string.Format("{0} ({1})", row["LabelInfrontCheckBox"].ToString(), row["QA_AnswerText"].ToString()));
                        else
                            answer += (", " + row["LabelInfrontCheckBox"].ToString());
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(row["QA_AnswerText"].ToString()))
                            answer += string.Format("{0} ({1})", row["LabelInfrontCheckBox"].ToString(), row["QA_AnswerText"].ToString());
                        else
                            answer += row["LabelInfrontCheckBox"].ToString();
                    }
                }
                else
                {
                    if (count > 1)
                    {
                        if (!string.IsNullOrEmpty(row["QA_AnswerText"].ToString()))
                            answer += (", " + string.Format("{0} ({1})", row["answer"].ToString(), row["QA_AnswerText"].ToString()));
                        else
                            answer += (", " + row["answer"].ToString());
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(row["QA_AnswerText"].ToString()))
                            answer += string.Format("{0} ({1})", row["answer"].ToString(), row["QA_AnswerText"].ToString());
                        else
                            answer += row["answer"].ToString();
                    }
                }
                count++;
            }
        }

        return answer;
    }

    protected void gvRegReport_DataBound(object sender, EventArgs e)
    {
        foreach (GridViewRow row in gvRegReport.Rows)
        {
            row.Cells[1].Text = DateTime.Parse(row.Cells[1].Text).ToString("dd-MMM-yy");
        }
    }

    protected void gvRegReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRegReport.PageIndex = e.NewPageIndex;
        gvRegReport.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string keyword = tbFilter.Text.Replace("=", "").Replace(";", "").Replace("'", "''");

        if (!string.IsNullOrEmpty(keyword))
        {
            BindGrid(true, keyword);
            btnReset.Visible = true;
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        tbFilter.Text = string.Empty;
        btnReset.Visible = false;

        BindGrid(true);
    }

    protected void btnExportVisitorIndv_Click(object sender, EventArgs e)
    {
        #region start
        /*string query = string.Format("Select ROW_NUMBER() over (order by d.regno) as sn, d.regno, d.reggroupid, d.reg_datecreated, d.reg_FName, d.reg_LName, d.reg_OName, d.reg_Email, d.reg_Tel, d.reg_Mobile, d.reg_TelCC, d.reg_TelAC, d.reg_MobCC, d.reg_MobAC, 'ARC-' + CONVERT(varchar(100), d.regNo) as refID, isnull(s.sal_name, d.reg_Salutation) as salutation, c.Country from tb_RegDelegate as d Left Join ref_Salutation as s on CONVERT(nvarchar(50),s.sal_ID) = d.reg_salutation Left Join ref_Country as c on c.cty_GUID= d.reg_Country Where d.reggroupid in (select distinct(reggroupid) from tb_regdelegate where reg_status=1 and (showid='{0}' or showid='{1}') and recycle=0 group by reggroupid having count(reggroupid)=1)", SHOW_EN, SHOW_TH);

        QuestionnaireControler qaController = new QuestionnaireControler(fn);

        DataSet ds = new DataSet();

        DataTable NewDt = new DataTable();

        #region Header
        NewDt.Columns.Add("No.", typeof(string));
        NewDt.Columns.Add("Date", typeof(string));
        NewDt.Columns.Add("Reference ID", typeof(string));
        NewDt.Columns.Add("Title", typeof(string));
        NewDt.Columns.Add("First Name", typeof(string));
        NewDt.Columns.Add("Last Name", typeof(string));
        NewDt.Columns.Add("Company", typeof(string));
        NewDt.Columns.Add("Email", typeof(string));
        NewDt.Columns.Add("Tel.", typeof(string));
        NewDt.Columns.Add("Mobile", typeof(string));
        NewDt.Columns.Add("Country", typeof(string));
        NewDt.Columns.Add("Age", typeof(string));
        NewDt.Columns.Add("Website", typeof(string));
        NewDt.Columns.Add("Address No", typeof(string));
        NewDt.Columns.Add("MOO", typeof(string));
        NewDt.Columns.Add("SOI", typeof(string));
        NewDt.Columns.Add("Road", typeof(string));
        NewDt.Columns.Add("Sub-District", typeof(string));
        NewDt.Columns.Add("District", typeof(string));
        NewDt.Columns.Add("City", typeof(string));
        NewDt.Columns.Add("Tel. Ext.", typeof(string));
        NewDt.Columns.Add("Which career or type of business you are in? (Choose only 1 answer)", typeof(string));
        NewDt.Columns.Add("What position in your career?", typeof(string));
        NewDt.Columns.Add("Which products / services you are interested in? (Multiple answers are allowed)", typeof(string));
        NewDt.Columns.Add("Please identify the purpose of your visit (Multiple answers are allowed)", typeof(string));
        NewDt.Columns.Add("How important are the following to you in your planning to visit this Exposition? (Multiple answers are allowed) / สิ่งที่สาคัญต่อการตัดสินใจวางแผนมาเข้าชมงาน (เลือกได้มากกว่า 1 รายการ)", typeof(string));
        NewDt.Columns.Add("How did you know about architect ’18 ? (Multiple answers are allowed) / ท่านทราบข่าวการจัดงานนี้จากที่ใด (เลือกได้มากกว่า 1 รายการ)", typeof(string));
        #endregion

        string sql = query;

        DataTable dt = fn.GetDatasetByCommand(sql, "tb_RegDelegate").Tables[0];

        if (dt.Rows.Count > 0)
        {
            #region Body
            foreach (DataRow dr in dt.Rows)
            {
                DataRow newdr = NewDt.NewRow();

                newdr["No."] = dr["sn"];
                newdr["Date"] = DateTime.Parse(dr["reg_datecreated"].ToString()).ToString("dd-MMM-yy");
                newdr["Reference ID"] = dr["refId"];
                newdr["Title"] = dr["salutation"];
                newdr["First Name"] = dr["reg_FName"];
                newdr["Last Name"] = dr["reg_LName"];
                newdr["Company"] = dr["reg_OName"];
                newdr["Email"] = dr["reg_Email"];
                newdr["Tel."] = (!string.IsNullOrEmpty(dr["reg_TelAC"].ToString())) ? string.Format("{0}, Ext. {1}", dr["reg_Tel"].ToString(), dr["reg_TelAC"].ToString()) : dr["reg_Tel"];
                newdr["Mobile"] = dr["reg_mobile"];
                newdr["Country"] = dr["country"];

                newdr["Which career or type of business you are in? (Choose only 1 answer) / ท่านประกอบอาชีพหรือประกอบธุรกิจด้านใด (ตอบได้เพียง 1 ข้อ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 1);
                newdr["What position in your career? / ตำแหน่งทางอาชีพของท่าน"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 2);
                newdr["Which products / services you are interested in? (Multiple answers are allowed) / ผลิตภัณฑ์และบริการที่ท่านสนใจ (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 3);
                newdr["Please identify the purpose of your visit (Multiple answers are allowed) / วัตถุประสงค์ในการเข้าชมงาน (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 4);
                newdr["How important are the following to you in your planning to visit this Exposition? (Multiple answers are allowed) / สิ่งที่สาคัญต่อการตัดสินใจวางแผนมาเข้าชมงาน (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 5);
                newdr["How did you know about architect ’18 ? (Multiple answers are allowed) / ท่านทราบข่าวการจัดงานนี้จากที่ใด (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 6);

                NewDt.Rows.Add(newdr);
            }
            #endregion
        }

        string filename = "IndividualVisitorRegList" + DateTime.Now.ToString("ddMMMyyyy");
        ExportToExcel.ExportArchExcelXLSX(NewDt, filename, "List");*/
        #endregion
        ExportToExcelArch("individual");
    }

    protected void btnExportVisitorGroup_Click(object sender, EventArgs e)
    {
        #region start
        /*string query = string.Format("Select ROW_NUMBER() over (order by d.regno) as sn, d.regno, d.reggroupid, d.reg_datecreated, d.reg_FName, d.reg_LName, d.reg_OName, d.reg_Email, d.reg_Tel, d.reg_Mobile, d.reg_TelCC, d.reg_TelAC, d.reg_MobCC, d.reg_MobAC, d.reg_Fax, d.reg_FaxAC, d.reg_FaxCC, d.reg_Address1 as AddressNo, d.reg_Address2 as MOO, d.reg_Address3 as SOI, d.reg_Address4 as Road, d.reg_PassNo as SubDistrict, d.reg_State as District, d.reg_city, d.reg_postalcode, d.reg_staffid as age, d.reg_Additional4, 'ARC-' + CONVERT(varchar(100), d.regNo) as refID, isnull(s.sal_name, d.reg_Salutation) as salutation, c.Country from tb_RegDelegate as d Left Join ref_Salutation as s on CONVERT(nvarchar(50),s.sal_ID) = d.reg_salutation Left Join ref_Country as c on c.cty_GUID= d.reg_Country Where d.reggroupid in (select distinct(reggroupid) from tb_regdelegate where reg_status=1 and (showid='{0}' or showid='{1}') and recycle=0 group by reggroupid having count(reggroupid)>1)", SHOW_EN, SHOW_TH);
        QuestionnaireControler qaController = new QuestionnaireControler(fn);

        DataSet ds = new DataSet();

        DataTable NewDt = new DataTable();

        #region Header
        NewDt.Columns.Add("No.", typeof(string));
        NewDt.Columns.Add("Date", typeof(string));
        NewDt.Columns.Add("Reference ID", typeof(string));
        NewDt.Columns.Add("Title", typeof(string));
        NewDt.Columns.Add("First Name", typeof(string));
        NewDt.Columns.Add("Last Name", typeof(string));
        NewDt.Columns.Add("Company", typeof(string));
        NewDt.Columns.Add("Email", typeof(string));
        NewDt.Columns.Add("Tel.", typeof(string));
        NewDt.Columns.Add("Mobile", typeof(string));
        NewDt.Columns.Add("Country", typeof(string));
        NewDt.Columns.Add("Which career or type of business you are in? (Choose only 1 answer) / ท่านประกอบอาชีพหรือประกอบธุรกิจด้านใด (ตอบได้เพียง 1 ข้อ)", typeof(string));
        NewDt.Columns.Add("What position in your career? / ตำแหน่งทางอาชีพของท่าน", typeof(string));
        NewDt.Columns.Add("Which products / services you are interested in? (Multiple answers are allowed) / ผลิตภัณฑ์และบริการที่ท่านสนใจ (เลือกได้มากกว่า 1 รายการ)", typeof(string));
        NewDt.Columns.Add("Please identify the purpose of your visit (Multiple answers are allowed) / วัตถุประสงค์ในการเข้าชมงาน (เลือกได้มากกว่า 1 รายการ)", typeof(string));
        NewDt.Columns.Add("How important are the following to you in your planning to visit this Exposition? (Multiple answers are allowed) / สิ่งที่สาคัญต่อการตัดสินใจวางแผนมาเข้าชมงาน (เลือกได้มากกว่า 1 รายการ)", typeof(string));
        NewDt.Columns.Add("How did you know about architect ’18 ? (Multiple answers are allowed) / ท่านทราบข่าวการจัดงานนี้จากที่ใด (เลือกได้มากกว่า 1 รายการ)", typeof(string));
        #endregion

        string sql = query;

        DataTable dt = fn.GetDatasetByCommand(sql, "tb_RegDelegate").Tables[0];

        if (dt.Rows.Count > 0)
        {
            #region Body
            foreach (DataRow dr in dt.Rows)
            {
                DataRow newdr = NewDt.NewRow();

                newdr["No."] = dr["sn"];
                newdr["Date"] = DateTime.Parse(dr["reg_datecreated"].ToString()).ToString("dd-MMM-yy");
                newdr["Reference ID"] = dr["refId"];
                newdr["Title"] = dr["salutation"];
                newdr["First Name"] = dr["reg_FName"];
                newdr["Last Name"] = dr["reg_LName"];
                newdr["Company"] = dr["reg_OName"];
                newdr["Email"] = dr["reg_Email"];
                newdr["Tel."] = (!string.IsNullOrEmpty(dr["reg_TelAC"].ToString())) ? string.Format("{0}, Ext. {1}", dr["reg_Tel"].ToString(), dr["reg_TelAC"].ToString()) : dr["reg_Tel"];
                newdr["Mobile"] = dr["reg_mobile"];
                newdr["Country"] = dr["country"];

                newdr["Which career or type of business you are in? (Choose only 1 answer) / ท่านประกอบอาชีพหรือประกอบธุรกิจด้านใด (ตอบได้เพียง 1 ข้อ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 1);
                newdr["What position in your career? / ตำแหน่งทางอาชีพของท่าน"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 2);
                newdr["Which products / services you are interested in? (Multiple answers are allowed) / ผลิตภัณฑ์และบริการที่ท่านสนใจ (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 3);
                newdr["Please identify the purpose of your visit (Multiple answers are allowed) / วัตถุประสงค์ในการเข้าชมงาน (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 4);
                newdr["How important are the following to you in your planning to visit this Exposition? (Multiple answers are allowed) / สิ่งที่สาคัญต่อการตัดสินใจวางแผนมาเข้าชมงาน (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 5);
                newdr["How did you know about architect ’18 ? (Multiple answers are allowed) / ท่านทราบข่าวการจัดงานนี้จากที่ใด (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 6);

                NewDt.Rows.Add(newdr);
            }
            #endregion
        }

        string filename = "GroupVisitorRegList" + DateTime.Now.ToString("ddMMMyyyy");
        ExportToExcel.ExportArchExcelXLSX(NewDt, filename, "List");*/
        #endregion
        ExportToExcelArch("group");
    }

    protected void btnExportMasterList_Click(object sender, EventArgs e)
    {
        string query = string.Format("Select ROW_NUMBER() over (order by d.regno) as sn, d.regno, d.reggroupid, d.reg_datecreated, d.reg_FName, d.reg_LName, d.reg_OName, d.reg_Email, d.reg_Tel, d.reg_Mobile, d.reg_TelCC, d.reg_TelAC, d.reg_MobCC, d.reg_MobAC, 'ARC-' + CONVERT(varchar(100), d.regNo) as refID, isnull(s.sal_name, d.reg_Salutation) as salutation, c.Country from tb_RegDelegate as d Left Join ref_Salutation as s on CONVERT(nvarchar(50),s.sal_ID) = d.reg_salutation Left Join ref_Country as c on c.cty_GUID= d.reg_Country Where d.reg_status=1 and (d.showid='{0}' or d.showid='{1}') and d.recycle=0", SHOW_EN, SHOW_TH);

        QuestionnaireControler qaController = new QuestionnaireControler(fn);

        DataSet ds = new DataSet();

        DataTable NewDt = new DataTable();

        #region Header
        NewDt.Columns.Add("No.", typeof(string));
        NewDt.Columns.Add("Date", typeof(string));
        NewDt.Columns.Add("Reference ID", typeof(string));
        NewDt.Columns.Add("Title", typeof(string));
        NewDt.Columns.Add("First Name", typeof(string));
        NewDt.Columns.Add("Last Name", typeof(string));
        NewDt.Columns.Add("Company", typeof(string));
        NewDt.Columns.Add("Email", typeof(string));
        NewDt.Columns.Add("Tel.", typeof(string));
        NewDt.Columns.Add("Mobile", typeof(string));
        NewDt.Columns.Add("Country", typeof(string));
        NewDt.Columns.Add("Which career or type of business you are in? (Choose only 1 answer) / ท่านประกอบอาชีพหรือประกอบธุรกิจด้านใด (ตอบได้เพียง 1 ข้อ)", typeof(string));
        NewDt.Columns.Add("What position in your career? / ตำแหน่งทางอาชีพของท่าน", typeof(string));
        NewDt.Columns.Add("Which products / services you are interested in? (Multiple answers are allowed) / ผลิตภัณฑ์และบริการที่ท่านสนใจ (เลือกได้มากกว่า 1 รายการ)", typeof(string));
        NewDt.Columns.Add("Please identify the purpose of your visit (Multiple answers are allowed) / วัตถุประสงค์ในการเข้าชมงาน (เลือกได้มากกว่า 1 รายการ)", typeof(string));
        NewDt.Columns.Add("How important are the following to you in your planning to visit this Exposition? (Multiple answers are allowed) / สิ่งที่สาคัญต่อการตัดสินใจวางแผนมาเข้าชมงาน (เลือกได้มากกว่า 1 รายการ)", typeof(string));
        NewDt.Columns.Add("How did you know about architect ’18 ? (Multiple answers are allowed) / ท่านทราบข่าวการจัดงานนี้จากที่ใด (เลือกได้มากกว่า 1 รายการ)", typeof(string));
        #endregion

        string sql = query;

        DataTable dt = fn.GetDatasetByCommand(sql, "tb_RegDelegate").Tables[0];

        if (dt.Rows.Count > 0)
        {
            #region Body
            foreach (DataRow dr in dt.Rows)
            {
                DataRow newdr = NewDt.NewRow();

                newdr["No."] = dr["sn"];
                newdr["Date"] = DateTime.Parse(dr["reg_datecreated"].ToString(), CultureInfo.InvariantCulture).ToString("dd-MMM-yy");
                newdr["Reference ID"] = dr["refId"];
                newdr["Title"] = dr["salutation"];
                newdr["First Name"] = dr["reg_FName"];
                newdr["Last Name"] = dr["reg_LName"];
                newdr["Company"] = dr["reg_OName"];
                newdr["Email"] = dr["reg_Email"];
                newdr["Tel."] = (!string.IsNullOrEmpty(dr["reg_TelAC"].ToString())) ? string.Format("{0}, Ext. {1}", dr["reg_Tel"].ToString(), dr["reg_TelAC"].ToString()) : dr["reg_Tel"];
                newdr["Mobile"] = dr["reg_mobile"];
                newdr["Country"] = dr["country"];

                newdr["Which career or type of business you are in? (Choose only 1 answer) / ท่านประกอบอาชีพหรือประกอบธุรกิจด้านใด (ตอบได้เพียง 1 ข้อ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 1);
                newdr["What position in your career? / ตำแหน่งทางอาชีพของท่าน"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 2);
                newdr["Which products / services you are interested in? (Multiple answers are allowed) / ผลิตภัณฑ์และบริการที่ท่านสนใจ (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 3);
                newdr["Please identify the purpose of your visit (Multiple answers are allowed) / วัตถุประสงค์ในการเข้าชมงาน (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 4);
                newdr["How important are the following to you in your planning to visit this Exposition? (Multiple answers are allowed) / สิ่งที่สาคัญต่อการตัดสินใจวางแผนมาเข้าชมงาน (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 5);
                newdr["How did you know about architect ’18 ? (Multiple answers are allowed) / ท่านทราบข่าวการจัดงานนี้จากที่ใด (เลือกได้มากกว่า 1 รายการ)"] = GetAnswerByQuestDisplayNo(dr["regno"].ToString(), 6);

                NewDt.Rows.Add(newdr);
            }
            #endregion
        }

        string filename = "VisitorMasterList" + DateTime.Now.ToString("ddMMMyyyy");
        ExportToExcel.ExportArchExcelXLSX(NewDt, filename, "List");
    }

    protected void btnReSendEmail_Click(object sender, EventArgs e)
    {
    }

    protected void btnLogOut_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Session.Abandon();
        Response.Redirect("LoginArch.aspx");
    }

    protected void gvRegReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            int NumCells = e.Row.Cells.Count;
            for (int i = 0; i < NumCells - 1; i++)
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
            }
        }
    }

    public string GenerateDetailReport(string type)
    {
        string html = "";
        string ansRowHtml = "";
        string subAnsRowHtml = "";
        string query = "";
        StringBuilder str = new StringBuilder();
        QuestionnaireControler qaController = new QuestionnaireControler(fn);

        DataTable dt = new DataTable();
        DataTable dtReport = new DataTable();
        DataTable dtQA = new DataTable();

        #region Header
        dtReport.Columns.Add("No.", typeof(string));
        dtReport.Columns.Add("Date", typeof(string));
        dtReport.Columns.Add("Reference ID", typeof(string));
        dtReport.Columns.Add("Title", typeof(string));
        dtReport.Columns.Add("First Name", typeof(string));
        dtReport.Columns.Add("Last Name", typeof(string));
        dtReport.Columns.Add("Company", typeof(string));
        dtReport.Columns.Add("Email", typeof(string));
        dtReport.Columns.Add("Age", typeof(string));
        dtReport.Columns.Add("Tel.", typeof(string));
        dtReport.Columns.Add("Tel. Ext.", typeof(string));
        dtReport.Columns.Add("Fax.", typeof(string));
        dtReport.Columns.Add("Fax. Ext.", typeof(string));
        dtReport.Columns.Add("Mobile", typeof(string));
        dtReport.Columns.Add("Address No", typeof(string));
        dtReport.Columns.Add("MOO", typeof(string));
        dtReport.Columns.Add("SOI", typeof(string));
        dtReport.Columns.Add("Road", typeof(string));
        dtReport.Columns.Add("Sub-District", typeof(string));
        dtReport.Columns.Add("District", typeof(string));
        dtReport.Columns.Add("City", typeof(string));
        dtReport.Columns.Add("Postal Code", typeof(string));
        dtReport.Columns.Add("Country", typeof(string));
        dtReport.Columns.Add("Website", typeof(string));

        query = "select * from tb_QAAnswers_temp where qa_id='10000' and deleteflag=0";
        dtQA = fn.GetDatasetByCommand(query, "ds").Tables[0];

        foreach (DataRow dr in dtQA.Rows)
        {
            if (dr["LabelInfrontCheckbox"].ToString().TrimEnd(' ') != "1.1")
                dtReport.Columns.Add(dr["LabelInfrontCheckBox"].ToString().TrimEnd(' '), typeof(string));
            if (string.IsNullOrEmpty(dr["ParentAnsId"].ToString()))
            {
                if (dr["LabelInfrontCheckbox"].ToString().TrimEnd(' ') != "1.1")
                {
                    if (dr["LabelInfrontCheckbox"].ToString().StartsWith("3."))
                        ansRowHtml += string.Format("<th rowspan=\"2\">{0}</th>", dr["LabelInfrontCheckbox"].ToString());
                    else
                        ansRowHtml += string.Format("<th rowspan=\"2\">{0}</th>", dr["Answer"].ToString());

                    if (dr["LabelInfrontCheckbox"].ToString() == "1.13")
                    {
                        dtReport.Columns.Add("1.13_Others", typeof(string));
                        ansRowHtml += "<th rowspan=\"2\">1.13 (Others) Please Specify</th>";
                    }

                    if (dr["LabelInfrontCheckbox"].ToString().TrimEnd(' ') == "3.18")
                    {
                        ansRowHtml = ansRowHtml
                            .Replace("3.1 ", "3.1 Sanitary Wares, Tiles & Stones")
                            .Replace("3.2 ", "3.2 Roof, Wall, Insulation")
                            .Replace("3.3 ", "3.3 Automation System, CCTV & Access Control")
                            .Replace("3.4 ", "3.4 Construction Equipment, Tools, Machinery")
                            .Replace("3.5 ", "3.5 Paint & Coating")
                            .Replace("3.6 ", "3.6 Kitchen & Kitchen Appliance")
                            .Replace("3.7 ", "3.7 Doors & Windows")
                            .Replace("3.8 ", "3.8 Furniture & Decoration")
                            .Replace("3.9 ", "3.9 Lighting, Electrical Appliances, Audio Visual Equipment")
                            .Replace("3.10 ", "3.10 Elevator & Escalator")
                            .Replace("3.11 ", "3.11 Adhesive, Sealant, Chemical")
                            .Replace("3.12 ", "3.12 Swimming Pool, Sauna, Steam Bath")
                            .Replace("3.13 ", "3.13 Aluminium & Steel")
                            .Replace("3.14 ", "3.14 Flooring")
                            .Replace("3.15 ", "3.15 Home Builder, Contractor, Interior Decorator")
                            .Replace("3.16 ", "3.16 Carpet, Curtain, Wall Covering")
                            .Replace("3.17 ", "3.17 Designer")
                            .Replace("3.18 ", "3.18 Others");
                        dtReport.Columns.Add("3.18_Others", typeof(string));
                        ansRowHtml += "<th rowspan=\"2\">3.18 (Others) Please Specify</th>";
                    }
                }
                else
                {
                    ansRowHtml += string.Format("<th colspan=\"4\">{0}</th>", dr["Answer"].ToString().TrimEnd(' '));
                }
            }
            else
            {
                subAnsRowHtml += string.Format("<th>{0}</th>", dr["Answer"].ToString().TrimEnd(' '));
            }
        }

        //First Row
        str.Append("<tr style=\"background-color: #808080;\">");
        str.Append("<th rowspan=\"3\">No.</th>");
        str.Append("<th rowspan=\"3\">Date</th>");
        str.Append("<th rowspan=\"3\">Reference ID</th>");
        str.Append("<th rowspan=\"3\">Title</th>");
        str.Append("<th rowspan=\"3\">First Name</th>");
        str.Append("<th rowspan=\"3\">Last Name</th>");
        str.Append("<th rowspan=\"3\">Company</th>");
        str.Append("<th rowspan=\"3\">Email</th>");
        str.Append("<th rowspan=\"3\">Age</th>");
        str.Append("<th rowspan=\"3\">Tel.</th>");
        str.Append("<th rowspan=\"3\">Tel. Ext. No.</th>");
        str.Append("<th rowspan=\"3\">Fax</th>");
        str.Append("<th rowspan=\"3\">Fax Ext. No.</th>");
        str.Append("<th rowspan=\"3\">Mobile</th>");
        str.Append("<th rowspan=\"3\">Address No</th>");
        str.Append("<th rowspan=\"3\">MOO</th>");
        str.Append("<th rowspan=\"3\">SOI</th>");
        str.Append("<th rowspan=\"3\">Road</th>");
        str.Append("<th rowspan=\"3\">Sub-District</th>");
        str.Append("<th rowspan=\"3\">District</th>");
        str.Append("<th rowspan=\"3\">City</th>");
        str.Append("<th rowspan=\"3\">Postal Code</th>");
        str.Append("<th rowspan=\"3\">Country</th>");
        str.Append("<th rowspan=\"3\">Website</th>");
        str.Append("<th colspan=\"17\">1. Which career or type of business you are in? (Choose only 1 answer)</th>");
        str.Append("<th colspan=\"5\">2. What position in your career?</th>");
        str.Append("<th colspan=\"19\">3. Which products / services you are interested in? (Multiple answers are allowed)</th>");
        str.Append("<th colspan=\"8\">4. Please identify the purpose of your visit (Multiple answers are allowed)</th>");
        str.Append("<th colspan=\"6\">5. How important are the following to you in your planning to visit this Exposition? (Multiple answers are allowed)</th>");
        str.Append("<th colspan=\"12\">6. How did you know about architect ’18 ? (Multiple answers are allowed)</th>");
        str.Append("</tr>");

        //Second Row
        str.Append("<tr style=\"background-color: #808080;\">");
        str.Append(ansRowHtml);
        str.Append("</tr>");

        //Third Row
        str.Append("<tr style=\"background-color: #808080;\">");
        str.Append(subAnsRowHtml);
        str.Append("</tr>");
        #endregion

        #region Body
        if (type == "individual")
            query = string.Format("Select ROW_NUMBER() over (order by d.regno) as sn, d.regno, d.reggroupid, d.reg_datecreated, d.reg_FName, d.reg_LName, d.reg_OName, d.reg_Email, d.reg_Tel, d.reg_Mobile, d.reg_TelCC, d.reg_TelAC, d.reg_MobCC, d.reg_MobAC, d.reg_Fax, d.reg_FaxAC, d.reg_FaxCC, d.reg_Address1 as AddressNo, d.reg_Address2 as MOO, d.reg_Address3 as SOI, d.reg_Address4 as Road, d.reg_PassNo as SubDistrict, d.reg_State as District, d.reg_city, d.reg_postalcode, d.reg_staffid as age, d.reg_Additional4, 'ARC-' + CONVERT(varchar(100), d.regNo) as refID, isnull(s.sal_name, d.reg_Salutation) as salutation, c.Country from tb_RegDelegate as d Left Join ref_Salutation as s on CONVERT(nvarchar(50),s.sal_ID) = d.reg_salutation Left Join ref_Country as c on c.cty_GUID= d.reg_Country Where d.reggroupid in (select distinct(reggroupid) from tb_regdelegate where reg_status=1 and (showid='{0}' or showid='{1}') and recycle=0 group by reggroupid having count(reggroupid)=1)", SHOW_EN, SHOW_TH);
        else
            query = string.Format("Select ROW_NUMBER() over (order by d.regno) as sn, d.regno, d.reggroupid, d.reg_datecreated, d.reg_FName, d.reg_LName, d.reg_OName, d.reg_Email, d.reg_Tel, d.reg_Mobile, d.reg_TelCC, d.reg_TelAC, d.reg_MobCC, d.reg_MobAC, d.reg_Fax, d.reg_FaxAC, d.reg_FaxCC, d.reg_Address1 as AddressNo, d.reg_Address2 as MOO, d.reg_Address3 as SOI, d.reg_Address4 as Road, d.reg_PassNo as SubDistrict, d.reg_State as District, d.reg_city, d.reg_postalcode, d.reg_staffid as age, d.reg_Additional4, 'ARC-' + CONVERT(varchar(100), d.regNo) as refID, isnull(s.sal_name, d.reg_Salutation) as salutation, c.Country from tb_RegDelegate as d Left Join ref_Salutation as s on CONVERT(nvarchar(50),s.sal_ID) = d.reg_salutation Left Join ref_Country as c on c.cty_GUID= d.reg_Country Where d.reggroupid in (select distinct(reggroupid) from tb_regdelegate where reg_status=1 and (showid='{0}' or showid='{1}') and recycle=0 group by reggroupid having count(reggroupid)>1)", SHOW_EN, SHOW_TH);

        dt = fn.GetDatasetByCommand(query, "ds").Tables[0];

        foreach (DataRow dr in dt.Rows)
        {
            DataRow NewRow = dtReport.NewRow();
            NewRow[0] = dr["sn"].ToString();
            NewRow[1] = DateTime.Parse(dr["reg_datecreated"].ToString()).ToString("dd-MMM-yy");
            NewRow[2] = dr["refId"].ToString();
            NewRow[3] = dr["salutation"].ToString();
            NewRow[4] = dr["reg_FName"].ToString();
            NewRow[5] = dr["reg_LName"].ToString();
            NewRow[6] = dr["reg_OName"].ToString();
            NewRow[7] = dr["reg_Email"].ToString();
            NewRow[8] = dr["age"].ToString();
            NewRow[9] = dr["reg_Tel"].ToString();
            NewRow[10] = dr["reg_TelAC"].ToString();
            NewRow[11] = dr["reg_Fax"].ToString();
            NewRow[12] = dr["reg_FaxAC"].ToString();
            NewRow[13] = dr["reg_Mobile"].ToString();
            NewRow[14] = dr["AddressNo"].ToString();
            NewRow[15] = dr["MOO"].ToString();
            NewRow[16] = dr["SOI"].ToString();
            NewRow[17] = dr["Road"].ToString();
            NewRow[18] = dr["SubDistrict"].ToString();
            NewRow[19] = dr["District"].ToString();
            NewRow[20] = dr["reg_City"].ToString();
            NewRow[21] = dr["reg_PostalCode"].ToString();
            NewRow[22] = dr["country"].ToString();
            NewRow[23] = dr["reg_Additional4"].ToString();

            query = string.Format("select l.regno, l.reggroupid, q.Question, a.QA_AnsId, a.Answer, r.QA_AnswerText, a.LabelInfrontCheckBox from tb_QAResultLog as l Left join tb_QAResult_Temp as r on r.QALogID = l.QALogID Left Join tb_QAQuestions_temp as q on q.QA_QuestId = r.QA_QuestId Left Join tb_QAAnswers_temp as a on a.QA_AnsId = r.QA_AnswerId where l.regno = '{0}' and l.deleteflag = 0", dr["regno"].ToString());
            dtQA = fn.GetDatasetByCommand(query, "ds").Tables[0];

            foreach (DataRow row in dtQA.Rows)
            {
                string name = row["LabelInfrontCheckBox"].ToString().TrimEnd(' ');
                if (name != "1.1")
                    NewRow[name] = "1";

                if (row["LabelInfrontCheckBox"].ToString() == "1.13" || row["LabelInfrontCheckBox"].ToString().TrimEnd(' ') == "3.18")
                {
                    NewRow[string.Format("{0}_Others", row["LabelInfrontCheckBox"].ToString().TrimEnd(' '))] = row["QA_AnswerText"].ToString();
                }
            }

            str.Append("<tr>");
            for (int i=0; i < dtReport.Columns.Count; i++)
            {
                str.Append(string.Format("<td>&#8203;{0}</td>", NewRow[i]));
            }
            str.Append("</tr>");
            dtReport.Rows.Add(NewRow);
        }
        #endregion

        html = str.ToString();
        return html;
    }

    public void ExportToExcelArch(string type)
    {
        try
        {
            StringBuilder StrBuilder = new StringBuilder();
            string fname = string.Empty;

            if(type == "individual")
                fname = "IndividualVisitorRegList" + DateTime.Now.ToString("ddMMMyyyy");
            else
                fname = "GroupVisitorRegList" + DateTime.Now.ToString("ddMMMyyyy");

            string htmltb = GenerateDetailReport(type);
            StrBuilder.Append("<table cellpadding='0' cellspacing='0' border='1' class='Messages' id='idTbl' runat='server'>");
            StrBuilder.Append(htmltb.ToString());
            StrBuilder.Append("</table>");
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/msexcel";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode;
            HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fname + ".xls");
            HttpContext.Current.Response.Write(StrBuilder);
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush();

        }
        catch (Exception ex) { }
    }

    protected void gvRegReport_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ReSend")
        {
            GridViewRow gvr = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

            int RowIndex = gvr.RowIndex;
            string regno = gvr.Cells[2].Text.Replace("ARC-", "");
        }
    }
}