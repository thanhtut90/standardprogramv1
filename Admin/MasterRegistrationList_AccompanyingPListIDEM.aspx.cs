﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Corpit.BackendMaster;
using Telerik.Web.UI;
using Corpit.Registration;
using Corpit.Utilities;
using System.Text;
using Corpit.Site.Utilities;
using Corpit.Payment;
using System.IO;
using ClosedXML.Excel;
using Corpit.Site.Email;
using System.Text.RegularExpressions;
using Corpit.Logging;
using System.Globalization;

public partial class Admin_MasterRegistrationList_AccompanyingPListIDEM : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    string showID = string.Empty;
    string flowID = string.Empty;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (Session["userid"] != null)
                {
                    string userID = Session["userid"].ToString();

                    if (!string.IsNullOrEmpty(userID))
                    {
                        binddata();

                        if (Session["roleid"].ToString() != "1")
                        {
                            lblUser.Text = userID;
                            getShowID(userID);
                            showlist.Visible = false;


                            GKeyMaster.ExportSettings.ExportOnlyData = true;
                            GKeyMaster.ExportSettings.IgnorePaging = true;
                            GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                            GKeyMaster.ExportSettings.FileName = "AccompanyingPersonList_" + DateTime.Now.ToString("ddMMyyyy");


                            string constr = fn.ConnString;
                            using (SqlConnection con = new SqlConnection(constr))
                            {
                                using (SqlCommand cmd = new SqlCommand("Select Distinct FLW_ID,FLW_Desc From tb_site_flow_master as fm Inner Join tb_site_flow as f On fm.FLW_ID=f.flow_id where ShowID = '" + showID + "' and fm.Status='Active' And fm.FLW_Desc Not Like '%Old%'"))/*changed by than on 17-7-2019*/
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.Connection = con;
                                    con.Open();
                                    ddl_flowList.Items.Clear();
                                    ddl_flowList.DataSource = cmd.ExecuteReader();
                                    ddl_flowList.DataTextField = "FLW_Desc";
                                    ddl_flowList.DataValueField = "FLW_ID";
                                    ddl_flowList.DataBind();
                                    con.Close();
                                }
                            }
                        }
                        else
                            showID = ddl_showList.SelectedValue;
                    }
                    else
                    {
                        Response.Redirect("Event_Config");
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }

    protected void binddata()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show where Status='Active'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_showList.Items.Clear();
                ddl_showList.DataSource = cmd.ExecuteReader();
                ddl_showList.DataTextField = "SHW_Name";
                ddl_showList.DataValueField = "SHW_ID";
                ddl_showList.DataBind();
                con.Close();
            }

            using (SqlCommand cmd = new SqlCommand("Select Distinct FLW_ID,FLW_Desc From tb_site_flow_master as fm Inner Join tb_site_flow as f On fm.FLW_ID=f.flow_id where ShowID = '" + ddl_showList.SelectedValue + "' and fm.Status='Active' And fm.FLW_Desc Not Like '%Old%'"))/*changed by than on 17-7-2019*/
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_flowList.Items.Clear();
                ddl_flowList.DataSource = cmd.ExecuteReader();
                ddl_flowList.DataTextField = "FLW_Desc";
                ddl_flowList.DataValueField = "FLW_ID";
                ddl_flowList.DataBind();
                con.Close();
            }
        }
    }

    #region GKeyMaster_NeedDataSource (Binding data into RadGrid & get DataSource From tb_RegDelegate and tb_Invoice tables for RadGrid)
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            if (Session["roleid"].ToString() != "1")
                getShowID(Session["userid"].ToString());
            else
                showID = ddl_showList.SelectedValue;

            flowID = ddl_flowList.SelectedValue;
        }
        catch (Exception ex)
        {
            Response.Redirect("Login.aspx");
        }

        if (!string.IsNullOrEmpty(showID))
        {
            DataTable dtReg = generateRegReport(showID, flowID);
            GKeyMaster.DataSource = dtReg;
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #endregion

    #region GKeyMaster_ItemCommand (Load when RadGrid row selected to edit & the selected row data will bind to the respective controls)
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            this.GKeyMaster.ExportSettings.ExportOnlyData = true;
            this.GKeyMaster.ExportSettings.IgnorePaging = true;
            this.GKeyMaster.ExportSettings.OpenInNewWindow = true;

            this.GKeyMaster.MasterTableView.ExportToExcel();
        }
    }
    #endregion

    /// <summary> 
    /// Item databound for the radgrid, set the header text here based on the form management ***Item[UniqueName]
    /// </summary>
    protected void GKeyMaster_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        Session["pgno"] = GKeyMaster.CurrentPageIndex + 1;

        //if (!string.IsNullOrEmpty(txtKey.Text) && !string.IsNullOrWhiteSpace(txtKey.Text))
        //{
        //    btnKeysearch_Click(this, null);
        //}
        //else
        //{
        //    if (string.IsNullOrEmpty(txtFromDate.Text.Trim()) || string.IsNullOrEmpty(txtToDate.Text.Trim()))
        //    {
        //        MasterRegIndiv msregIndiv = new MasterRegIndiv(fn);
        //        msregIndiv.ShowID = showID;
        //        msregIndiv.FlowID = flowID;
        //        GKeyMaster.DataSource = msregIndiv.getDataAllRegList();
        //    }
        //    else
        //    {
        //        GKeyMaster.DataSource = getDataAllRegList(txtFromDate.Text.Trim(), txtToDate.Text.Trim());
        //    }
        //}
    }

    protected void ddl_showList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;

        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select Distinct FLW_ID,FLW_Desc From tb_site_flow_master as fm Inner Join tb_site_flow as f On fm.FLW_ID=f.flow_id where ShowID = '" + showID + "' and fm.Status='Active' And fm.FLW_Desc Not Like '%Old%'"))/*changed by than on 17-7-2019*/
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_flowList.Items.Clear();
                ddl_flowList.DataSource = cmd.ExecuteReader();
                ddl_flowList.DataTextField = "FLW_Desc";
                ddl_flowList.DataValueField = "FLW_ID";
                ddl_flowList.DataBind();
                con.Close();
            }
        }
        flowID = ddl_flowList.SelectedValue;

        GKeyMaster.Rebind();
    }

    protected void ddl_flowList_SelectedIndexChanged(object sender, EventArgs e)
    {
        showID = ddl_showList.SelectedValue;
        flowID = ddl_flowList.SelectedValue;
        GKeyMaster.Rebind();
    }

    protected void lnkExcel_Clicked(object sender, EventArgs e)
    {
        try
        {
            this.GKeyMaster.ExportSettings.ExportOnlyData = true;
            this.GKeyMaster.ExportSettings.IgnorePaging = true;
            this.GKeyMaster.ExportSettings.OpenInNewWindow = true;
            this.GKeyMaster.MasterTableView.ExportToExcel();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
        }
    }

    #region New
    public DataTable generateRegReport(string showid, string flowid, string search = "")
    {
        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(showID))
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showID);
            try
            {
                dt = getAccompanyingPersonList(showid);
            }
            catch (Exception ex)
            { }
        }
        return dt;
    }
    private DataTable getAccompanyingPersonList(string showid)
    {
        DataTable dt = new DataTable();
        try
        {
            string sql = "Select * From GetAccompanyingPersonListByShowID('" + showid + "') Order By SrNo Asc";
            dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        }
        catch(Exception ex)
        { }

        return dt;
    }
    #endregion
}