﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="Event_Flow_EmailConfig.aspx.cs" Inherits="Event_Flow_EmailConfig" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    </telerik:RadAjaxManager>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default"></telerik:RadAjaxLoadingPanel>
    <div class="centercontent">
        <asp:Panel runat="server" Visible="false">
            <asp:TextBox runat="server" ID="txtFlowKey"></asp:TextBox>

            <asp:TextBox runat="server" ID="txtEmailID"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtEmailRefKey"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtPortalRefShowID"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtPortalTemplateID"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtPortalEmailID"></asp:TextBox>

        </asp:Panel>
        <asp:Panel ID="PanelEmpty" runat="server">
            <asp:LinkButton runat="server" ID="btnLinkToConfig" OnClick="btnLinkToConfig_Onclick" Text="Please Set up Email Config First"></asp:LinkButton>
        </asp:Panel>
        <asp:Panel ID="PanelDisplay" runat="server">
            <asp:Panel runat="server" ID="EmailList" Visible="true">
                <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                    EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                    OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="15" Skin="Bootstrap">
                    <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true"></Selecting>
                    </ClientSettings>
                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="EmailID">
                        <Columns>
                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="EmailID" FilterControlAltText="Filter EmailID"
                                HeaderText="Email ID" Visible="true" SortExpression="EmailID" UniqueName="EmailID" AutoPostBackOnFilter="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Email_Desc" FilterControlAltText="Filter Email_Desc"
                                HeaderText="Email Description" Visible="true" SortExpression="Email_Desc" UniqueName="Email_Desc" AutoPostBackOnFilter="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Status" FilterControlAltText="Filter Status"
                                HeaderText="Email Status" Visible="true" SortExpression="Status" UniqueName="Status" AutoPostBackOnFilter="true">
                            </telerik:GridBoundColumn>

                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Email_BaseDSource" FilterControlAltText="Filter Status"
                                HeaderText="Email BaseSource" Visible="true" SortExpression="Email_BaseDSource" UniqueName="Email_BaseDSource" AutoPostBackOnFilter="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="EmailPortalRefKey" FilterControlAltText="Filter Status"
                                HeaderText="Email RefKey" Visible="true" SortExpression="EmailPortalRefKey" UniqueName="EmailPortalRefKey" AutoPostBackOnFilter="true">
                            </telerik:GridBoundColumn>

                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
                <asp:Button runat="server" ID="btnNewTemplate" OnClick="btnaddnewrecord_Click" Text="Add New" CssClass="btn btn-danger"/> 
                &nbsp&nbsp&nbsp  <asp:Button runat="server" ID="btnTemplateConfig" OnClick="btnTemplateConfig_Click" Text="Set up PDF Template" CssClass="btn btn-danger"/>
            </asp:Panel>
            <asp:Panel runat="server" ID="panelsitetemplate" Visible="false">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-horizontal">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Email Description</label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="txtEmailDesc" runat="server" CssClass="form-control" Visible="true"></asp:TextBox>
                                                <asp:TextBox ID="txtPortalRefID" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Email Data Source</label>
                                            <div class="col-md-6">
                                                <asp:DropDownList runat="server" ID="ddltempdatasource" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddltempdatasource_SelectedIndexChanged"></asp:DropDownList>
                                                <span class="text-bold">Filter Key:   
                                        <asp:Label runat="server" ID="lbltblfk" CssClass="text-danger"></asp:Label>
                                                    <br />
                                                </span>
                                                <span class="text-bold">Email Address Column:    
                                        <asp:Label runat="server" ID="lbltblcec" CssClass="text-danger"></asp:Label>
                                                </span>
                                            </div>
                                        </div>
                                           <div class="form-group">
                                            <label class="col-md-2 control-label">Acknowledgement Email</label>
                                            <div class="col-md-6">
                                                <asp:DropDownList runat="server" ID="ddlIsAcknowledgement"  CssClass="form-control">
                                                    <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                </asp:DropDownList>                                            
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="col-md-2 control-label">Send To All Delegate</label>
                                            <div class="col-md-6">
                                                <asp:DropDownList runat="server" ID="ddlSendToAllDelegate"  CssClass="form-control">
                                                    <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                </asp:DropDownList>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Email Type</label>
                                            <div class="col-md-6">
                                                <asp:DropDownList runat="server" ID="ddlEmailType"  CssClass="form-control">
                                                    <asp:ListItem Text="Normal" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Confirmation" Value="Confirmation"></asp:ListItem>
                                                    <asp:ListItem Text="Invoice" Value="INV"></asp:ListItem>
                                                    <asp:ListItem Text="Receipt" Value="RECEIPT"></asp:ListItem>
                                                    <asp:ListItem Text="Visa" Value="VISA"></asp:ListItem>
                                                     <asp:ListItem Text="Acknowledge" Value="Acknowledge"></asp:ListItem>
                                                     
                                                    
                                                </asp:DropDownList>                                            
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="col-md-2 control-label">Email Send Condition</label>
                                            <div class="col-md-6">
                                                <asp:DropDownList runat="server" ID="ddlSendCondition"  CssClass="form-control">
                                                    <asp:ListItem Text="No Condition" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="UnPaid" Value="UNPAID"></asp:ListItem>
                                                    <asp:ListItem Text="Piad" Value="PAID"></asp:ListItem>
                                                    <asp:ListItem Text="Visa" Value="VISA"></asp:ListItem>
                                                    <asp:ListItem Text="Rejected" Value="REJECT"></asp:ListItem>
                                                    <asp:ListItem Text="Confirm" Value="CONFIRM"></asp:ListItem>
                                                     <asp:ListItem Text="VIP" Value="VIP"></asp:ListItem>
                                                     <asp:ListItem Text="APPROVE" Value="APPROVE"></asp:ListItem>
                                                     <asp:ListItem Text="DECLINE" Value="DECLINE"></asp:ListItem>
                                                </asp:DropDownList>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Email Subject</label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="txtsubject" runat="server" CssClass="form-control" Visible="true"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvsubject" runat="server" Display="Dynamic"
                                                    ControlToValidate="txtsubject" ErrorMessage="* Required" CssClass="text-danger"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Valid Date</label>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dateFrom" Skin="Bootstrap" runat="server" DateInput-Label="From: ">
                                                    </telerik:RadDatePicker>
                                                </div>
                                                <div class="col-md-6">
                                                    <telerik:RadDatePicker RenderMode="Lightweight" ID="dateTo" Skin="Bootstrap" runat="server" DateInput-Label="To: ">
                                                    </telerik:RadDatePicker>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Email Body</label>
                                            <div class="col-md-10">
                                                <CKEditor:CKEditorControl ID="txtvalue" runat="server" Height="457px" Width="98%" BasePath="~/Admin/ckeditor" EnterMode="BR" ShiftEnterMode="P"></CKEditor:CKEditorControl>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Button runat="server" ID="btnsave" Text="Save" OnClick="btnsave_Click" CssClass="btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div>
                                    <ul class="hornav">
                                        <li>Edit</li>
                                    </ul>
                                     <asp:LinkButton runat="server" ID="linkbtndefaultbanner" OnClick="linkbtndefaultbanner_Click" Text="Default Banner"></asp:LinkButton>
                                      <br />
                                       <asp:LinkButton runat="server" ID="btnEncryShow" OnClick="lbtntemp_Click" Text="Encryted ShowID" CommandArgument="EncryptedShowID"></asp:LinkButton><br />
                                      <asp:LinkButton runat="server" ID="btnEncryRegno" OnClick="lbtntemp_Click" Text="Encryted Regno" CommandArgument="EncryptedRegno"></asp:LinkButton><br />
                                      <asp:LinkButton runat="server" ID="btnBarCode" OnClick="lbtntemp_Click" Text="Barcode URL" CommandArgument="BarcodeURL"></asp:LinkButton><br />
                                      <asp:LinkButton runat="server" ID="LinkButton2" OnClick="lbtntemp_Click" Text="Group QRCode URL" CommandArgument="QRcodeGroupURL"></asp:LinkButton><br />
                                      <asp:LinkButton runat="server" ID="LinkButton3" OnClick="lbtntemp_Click" Text="Delegate QRCode URL" CommandArgument="QRcodeDelegateURL"></asp:LinkButton><br />

                                      <asp:LinkButton runat="server" ID="LinkButton1" OnClick="lbtntemp_Click" Text="Acknowledgement URL" CommandArgument="AcknowledgeURL"></asp:LinkButton><br />
                                     <asp:LinkButton runat="server" ID="LinkButton5" OnClick="lbtntemp_Click" Text="Invoice URL" CommandArgument="InvoiceURL"></asp:LinkButton><br />
                                      <asp:LinkButton runat="server" ID="LinkButton6" OnClick="lbtntemp_Click" Text="Receipt URL" CommandArgument="ReceiptURL"></asp:LinkButton><br />
                                    <asp:LinkButton runat="server" ID="LinkButton7" OnClick="lbtntemp_Click" Text="Visa URL" CommandArgument="VisaURL"></asp:LinkButton><br />
                                     <asp:LinkButton runat="server" ID="LinkButton10" OnClick="lbtntemp_Click" Text="Badge URL" CommandArgument="BadgeURL"></asp:LinkButton><br />
                                    <asp:LinkButton runat="server" ID="LinkButton4" OnClick="lbtntemp_Click" Text="Delegate List" CommandArgument="DelegateList"></asp:LinkButton><br />
                                     <asp:LinkButton runat="server" ID="LinkButton8" OnClick="lbtntemp_Click" Text="Conference List" CommandArgument="ConferenceList"></asp:LinkButton><br />
                                       <asp:LinkButton runat="server" ID="LinkButton9" OnClick="lbtntemp_Click" Text="TodayDate" CommandArgument="TodayDate"></asp:LinkButton><br />
                                         <asp:LinkButton runat="server" ID="LinkButton11" OnClick="lbtntemp_Click" Text="Invoice Payment Method" CommandArgument="PayMethod"></asp:LinkButton><br />
                                    <asp:LinkButton runat="server" ID="LinkButton13" OnClick="lbtntemp_Click" Text="Invoice No" CommandArgument="InvoiceNo"></asp:LinkButton><br />
                                    <asp:LinkButton runat="server" ID="LinkButton14" OnClick="lbtntemp_Click" Text="Order No" CommandArgument="OrderNo"></asp:LinkButton><br />
                                       <asp:LinkButton runat="server" ID="LinkButton12" OnClick="lbtntemp_Click" Text="Promo Code" CommandArgument="PromoCode"></asp:LinkButton><br />
                                    <asp:Repeater runat="server" ID="repeater1" DataSourceID="tempdatasource">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" Text='<%# Eval("Column_name") %>' CommandArgument='<%# Eval("Column_name") %>'
                                                ID="lbtntemp" OnClick="lbtntemp_Click" CausesValidation="false"></asp:LinkButton>
                                            <br />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>


                    </ContentTemplate>
                </asp:UpdatePanel>


                <asp:Button runat="server" ID="btnadd" Text="Add" OnClick="btnadd_Click" Visible="false" />&nbsp;&nbsp;
                    <asp:Button runat="server" ID="btnpreview" Text="Preview" OnClick="btnpreview_Click" CssClass="btn btn-primary" Visible="false" />
            </asp:Panel>

        </asp:Panel>
        <%-- </telerik:RadAjaxPanel>--%>
        <%-- </div>
        </div>--%>
        <asp:Panel runat="server" ID="panelpreview" Visible="false">
            <asp:Label runat="server" ID="lblpreview"></asp:Label>
            <br />
            <asp:Button runat="server" ID="btnback" Text="Back" OnClick="btnback_Click" />
        </asp:Panel>
    </div>

    <asp:SqlDataSource ID="tempdatasource" runat="server" ConnectionString="<%$ ConnectionStrings:CMSConnString %>"
        SelectCommand="select Column_name from Information_schema.columns where Table_name =@tablename">
        <SelectParameters>
            <asp:Parameter Name="tablename" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

