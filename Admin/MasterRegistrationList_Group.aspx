﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="MasterRegistrationList_Group.aspx.cs" Inherits="Admin_MasterRegistrationList_Member" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }
    </script>
    <style type="text/css">
        .tdstyle1
        {
            width:170px;
        }

        .tdstyle2
        {
            width:300px;
        }

        form input[type="text"]
        {
            width:39% !important;
        }

        .ajax__calendar_container
        {
            width: 320px !important;
            height:280px !important;
        }

        .ajax__calendar_body
        {
            width: 100% !important;
            height:100% !important;
        }

        td
        {
            vertical-align:middle;
        }

    </style>
   
<script type="text/javascript">
    var popupWindow = null;
    function centeredPopup(url, winName, w, h, scroll) {
        LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
        TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
        settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=' + scroll + ',resizable';
        //href = 'url' + 'groupid=' + url;
        popupWindow = window.open(url, winName, settings);
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    &nbsp;&nbsp;
    <asp:LinkButton ID="btnAddNew" runat="server" CssClass="btn btn-success" OnClick="lnkExcel_Clicked">
        <span aria-hidden="true" class="glyphicon glyphicon-export"></span> EXPORT EXCEL
    </asp:LinkButton>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Label ID="lblUser" runat="server" Visible="false"></asp:Label>
            <h3 class="box-title">Master Registration List (Group) <asp:Label ID="lblGroupID" runat="server" Text=""></asp:Label>.</h3>
            <br />
            <asp:Panel ID="showList" runat="server">Show List : <asp:DropDownList ID="ddl_showList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_showList_SelectedIndexChanged"></asp:DropDownList></asp:Panel>
            Flow : <asp:DropDownList ID="ddl_flowList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_flowList_SelectedIndexChanged"></asp:DropDownList>
            <%--Payment Status : --%><asp:DropDownList ID="ddl_paymentStatus" runat="server" OnSelectedIndexChanged="ddl_paymentStatus_SelectedIndexChanged" AutoPostBack="true" Visible="false"></asp:DropDownList>
            <br />
            <br />
            <div style="overflow-x:scroll;">
                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientEvents-OnRequestStart="onRequestStart">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="GKeyMaster">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                        <%--<telerik:AjaxSetting AjaxControlID="btnupdate">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="GKeyMaster"></telerik:AjaxUpdatedControl>
                                <telerik:AjaxUpdatedControl ControlID="PanelKeyDetial"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>--%>
                    </AjaxSettings>
                </telerik:RadAjaxManager>

                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" EnableAJAX="true">
                    <asp:Panel runat="server" ID="PanelKeyList">
                        <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true"
                            EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                            onneeddatasource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="5" OnItemDataBound="grid_ItemDataBound" OnItemCreated="RadGd1_ItemCreated">
                            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                            </ClientSettings>
                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="RegGroupID">
                                <CommandItemSettings ShowExportToExcelButton="False"  ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                                <Columns>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RegGroupID" FilterControlAltText="Filter RegGroupID"
                                        HeaderText="RegGroupID" SortExpression="RegGroupID" UniqueName="RegGroupID">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridButtonColumn CommandName="Download" ButtonType="ImageButton" ImageUrl="images/download.png" UniqueName="Download"
                                          HeaderText="Acknowledge Letter" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>

                                    <telerik:GridButtonColumn CommandName="View" ButtonType="ImageButton" ImageUrl="images/member.png" UniqueName="ViewMember"
                                          HeaderText="View Member" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>

                                    <telerik:GridButtonColumn ConfirmTextFormatString="Do you want to Delete {0}?" ConfirmTextFields="RegGroupID"
                                        ConfirmDialogType="RadWindow" CommandName="Delete" ButtonType="ImageButton" ImageUrl="images/delete.jpg" UniqueName="Delete"
                                        HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>

                                    <telerik:GridButtonColumn CommandName="Edit" ButtonType="ImageButton" ImageUrl="images/edit.jpg" UniqueName="Edit"
                                        HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                    </telerik:GridButtonColumn>


                                    <telerik:GridTemplateColumn DataField="RG_Salutation" HeaderText="" UniqueName="RG_Salutation"
                                        FilterControlAltText="Filter RG_Salutation" SortExpression="RG_Salutation" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblSalutation" runat="server"><%# bindSalutation(Eval("RG_Salutation").ToString(), Eval("RG_SalOther").ToString()) %></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_ContactFName" FilterControlAltText="Filter RG_ContactFName"
                                        HeaderText="" SortExpression="RG_ContactFName" UniqueName="RG_ContactFName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_ContactLName" FilterControlAltText="Filter RG_ContactLName"
                                        HeaderText="" SortExpression="RG_ContactLName" UniqueName="RG_ContactLName">
                                    </telerik:GridBoundColumn>

                                     <telerik:GridTemplateColumn HeaderText="" SortExpression="RG_Designation" UniqueName="RG_Designation"
                                        FilterControlAltText="Filter RG_Designation">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_designation" runat="server">
                                                <%#!string.IsNullOrEmpty(Eval("RG_DesignationOther").ToString()) ? Eval("RG_Designation").ToString() + ": " + Eval("RG_DesignationOther").ToString() : Eval("RG_Designation").ToString()%></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_Department" FilterControlAltText="Filter RG_Department"
                                        HeaderText="" SortExpression="RG_Department" UniqueName="RG_Department">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="RG_Industry" HeaderText="" UniqueName="RG_Industry"
                                        FilterControlAltText="Filter RG_Industry" SortExpression="RG_Industry" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblIndustry" runat="server"><%# bindIndustry(Eval("RG_Industry").ToString(), Eval("RG_IndustryOthers").ToString()) %></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                     <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_Company" FilterControlAltText="Filter RG_Company"
                                        HeaderText="" SortExpression="RG_Company" UniqueName="RG_Company">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_Address1" FilterControlAltText="Filter RG_Address1"
                                        HeaderText="" SortExpression="RG_Address1" UniqueName="RG_Address1">
                                    </telerik:GridBoundColumn>

                                      <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_Address2" FilterControlAltText="Filter RG_Address2"
                                        HeaderText="" SortExpression="RG_Address2" UniqueName="RG_Address2">
                                    </telerik:GridBoundColumn>

                                      <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_Address3" FilterControlAltText="Filter RG_Address3"
                                        HeaderText="" SortExpression="RG_Address3" UniqueName="RG_Address3">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_City" FilterControlAltText="Filter RG_City"
                                        HeaderText="" SortExpression="RG_City" UniqueName="RG_City">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_PostalCode" FilterControlAltText="Filter RG_PostalCode"
                                        HeaderText="" SortExpression="RG_PostalCode" UniqueName="RG_PostalCode">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="RG_Country" HeaderText="" UniqueName="RG_Country"
                                        FilterControlAltText="Filter RG_Country" SortExpression="RG_Country" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblCountry" runat="server"><%# bindCountry(Eval("RG_Country").ToString())%></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="RG_RCountry" HeaderText="" UniqueName="RG_RCountry"
                                        FilterControlAltText="Filter RG_RCountry" SortExpression="RG_RCountry" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblRCountry" runat="server"><%# bindCountry(Eval("RG_RCountry").ToString())%></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="RG_Tel" UniqueName="RG_Tel"
                                        FilterControlAltText="Filter RG_Tel">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_tel" runat="server"><%# Eval("RG_Telcc")%><%#Eval("RG_Telac")%><%#Eval("RG_Tel")%></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="RG_Mobile" UniqueName="RG_Mobile"
                                        FilterControlAltText="Filter RG_Mobile">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_mobile" runat="server"><%# Eval("RG_Mobilecc")%><%#Eval("RG_Mobileac")%><%#Eval("RG_Mobile")%></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="RG_Fax" UniqueName="RG_Fax"
                                        FilterControlAltText="Filter RG_Fax">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_fax" runat="server"><%# Eval("RG_Faxcc")%><%# Eval("RG_Faxac")%><%#Eval("RG_Fax")%></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>


                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_ContactEmail" FilterControlAltText="Filter RG_ContactEmail"
                                        HeaderText="" SortExpression="RG_ContactEmail" UniqueName="RG_ContactEmail">
                                    </telerik:GridBoundColumn>

                                    <%--<telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_Remark" FilterControlAltText="Filter RG_Remark"
                                        HeaderText="" SortExpression="RG_Remark" UniqueName="RG_Remark">
                                    </telerik:GridBoundColumn>

                                      <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_Type" FilterControlAltText="Filter RG_Type"
                                        HeaderText="" SortExpression="RG_Type" UniqueName="RG_Type">
                                    </telerik:GridBoundColumn>--%>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_VisitDate" FilterControlAltText="Filter RG_VisitDate"
                                        HeaderText="" SortExpression="RG_VisitDate" UniqueName="RG_VisitDate">
                                    </telerik:GridBoundColumn>

                                     <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_VisitTime" FilterControlAltText="Filter RG_VisitTime"
                                        HeaderText="" SortExpression="RG_VisitTime" UniqueName="RG_VisitTime">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_Age" FilterControlAltText="Filter RG_Age"
                                        HeaderText="" SortExpression="RG_Age" UniqueName="RG_Age">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_DOB" FilterControlAltText="Filter RG_DOB"
                                        HeaderText="" SortExpression="RG_DOB" UniqueName="RG_DOB">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_Gender" FilterControlAltText="Filter RG_Gender"
                                        HeaderText="" SortExpression="RG_Gender" UniqueName="RG_Gender">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_Additional4" FilterControlAltText="Filter RG_Additional4"
                                        HeaderText="" SortExpression="RG_Additional4" UniqueName="RG_Additional4">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_Additional5" FilterControlAltText="Filter RG_Additional5"
                                        HeaderText="" SortExpression="RG_Additional5" UniqueName="RG_Additional5">
                                    </telerik:GridBoundColumn>


                                    <telerik:GridTemplateColumn HeaderText="Status" SortExpression="status_name" UniqueName="status_name"
                                        FilterControlAltText="Filter status_name">
                                        <ItemTemplate>
                                         <asp:Label ID="lbl_status" runat="server"> <%#!string.IsNullOrEmpty(Eval("status_name").ToString()) ? Eval("status_name").ToString() : "Pending"%></asp:Label>
                                      </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RG_CreatedDate" FilterControlAltText="Filter RG_CreatedDate"
                                        HeaderText="Created Date" SortExpression="RG_CreatedDate" UniqueName="RG_CreatedDate">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Name" FilterControlAltText="Filter RC_Name"
                                        HeaderText="Company Name" SortExpression="RC_Name" UniqueName="RC_Name">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Address1" FilterControlAltText="Filter RC_Address1"
                                        HeaderText="Company Address" SortExpression="RC_Address1" UniqueName="RC_Address1">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Address2" FilterControlAltText="Filter RC_Address2"
                                        HeaderText="Company Address2" SortExpression="RC_Address2" UniqueName="RC_Address2">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Address3" FilterControlAltText="Filter RC_Address3"
                                        HeaderText="Company Address3" SortExpression="RC_Address3" UniqueName="RC_Address3">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_City" FilterControlAltText="Filter RC_City"
                                        HeaderText="Company City" SortExpression="RC_City" UniqueName="RC_City">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_State" FilterControlAltText="Filter RC_State"
                                        HeaderText="Company State" SortExpression="RC_State" UniqueName="RC_State">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_ZipCode" FilterControlAltText="Filter RC_ZipCode"
                                        HeaderText="Company Postal Code" SortExpression="RC_ZipCode" UniqueName="RC_ZipCode">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="RC_Country" HeaderText="Company Country" UniqueName="RC_Country"
                                        FilterControlAltText="Filter RC_Country" SortExpression="RC_Country" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblComCountry" runat="server"><%# bindCountry(Eval("RC_Country").ToString())%></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Company Tel" SortExpression="RC_Tel" UniqueName="RC_Tel"
                                        FilterControlAltText="Filter RC_Tel">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ctel" runat="server"><%# Eval("RC_Telcc")%><%#Eval("RC_Telac")%><%#Eval("RC_Tel")%></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Company Fax" SortExpression="RC_Fax" UniqueName="RC_Fax"
                                        FilterControlAltText="Filter RC_Fax">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_cfax" runat="server"><%# Eval("RC_Faxcc")%><%# Eval("RC_Faxac")%><%#Eval("RC_Fax")%></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Email" FilterControlAltText="Filter RC_Email"
                                        HeaderText="Company Email" SortExpression="RC_Email" UniqueName="RC_Email">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Website" FilterControlAltText="Filter RC_Website"
                                        HeaderText="Company Website" SortExpression="RC_Website" UniqueName="RC_Website">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Additional1" FilterControlAltText="Filter RC_Additional1"
                                        HeaderText="Company Additional1" SortExpression="RC_Additional1" UniqueName="RC_Additional1">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Additional2" FilterControlAltText="Filter RC_Additional2"
                                        HeaderText="Company Additional2" SortExpression="RC_Additional2" UniqueName="RC_Additional2">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Additional3" FilterControlAltText="Filter RC_Additional3"
                                        HeaderText="Company Additional3" SortExpression="RC_Additional3" UniqueName="RC_Additional3">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Additional4" FilterControlAltText="Filter RC_Additional4"
                                        HeaderText="Company Additional4" SortExpression="RC_Additional4" UniqueName="RC_Additional4">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_Additional5" FilterControlAltText="Filter RC_Additional5"
                                        HeaderText="Company Additional5" SortExpression="RC_Additional5" UniqueName="RC_Additional5">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RC_CreatedDate" FilterControlAltText="Filter RC_CreatedDate"
                                        HeaderText="Company Created Date" SortExpression="RC_CreatedDate" UniqueName="RC_CreatedDate">
                                    </telerik:GridBoundColumn>


                                    <telerik:GridTemplateColumn HeaderText="Congress Selection" UniqueName="CongressSelection" Exportable="true">
                                        <ItemTemplate>
                                            <%#getCongressSelection("", Eval("RegGroupID").ToString(), Eval("Invoice_status").ToString(), Eval("InvoiceID").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Payment Method" UniqueName="PaymentMethod" Exportable="true">
                                        <ItemTemplate>
                                            <%#getPaymentMethod(Eval("PaymentMethod").ToString(),Eval("showid").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Invoice_grandtotal" FilterControlAltText="Filter Invoice_grandtotal"
                                        HeaderText="Total Price" SortExpression="Invoice_grandtotal" UniqueName="Invoice_grandtotal">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="Paid Price" UniqueName="PaidPrice" Exportable="true">
                                        <ItemTemplate>
                                            <%#getPaidPrice(Eval("InvoiceID").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="Outstanding" UniqueName="Outstanding" Exportable="true">
                                        <ItemTemplate>
                                            <%#calculateOutstanding(Eval("Invoice_grandtotal").ToString(), Eval("InvoiceID").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Invoice_discount" FilterControlAltText="Filter Invoice_discount"
                                        HeaderText="Discount Price" SortExpression="Invoice_discount" UniqueName="Invoice_discount">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="Invoice Status" UniqueName="InvoiceStatus" Exportable="true">
                                        <ItemTemplate>
                                            <%#getInvoiceStatus(Eval("Invoice_status").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="remarks" FilterControlAltText="Filter remarks"
                                        HeaderText="Invoice Remark" SortExpression="remarks" UniqueName="remarks">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </asp:Panel>
                </telerik:RadAjaxPanel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

