﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master.master" AutoEventWireup="true" CodeFile="SummaryByCountryWtChart.aspx.cs" Inherits="Admin_SummaryByCountryWtChart" %>
<%@ MasterType virtualpath="~/Admin/Master.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
                args.set_enableAjax(false);
        }
        function exportChartToImageGCountry() {

            var $ = $telerik.$;
            //get reference to the ClientExportManager object
            var exportManager = $find('<%=RadClientExportManager1.ClientID%>');

            var filename = 'Group Registration By Country(By Number of Persons)' + '.png';

            //specify the image settings fileName, proxy, width, height
            var imageSettings = {
                fileName: filename
             }
            //set the image settings
            exportManager.set_imageSettings(imageSettings);
            //export the element/container
            //exportManager.exportImage($(".RadHtmlChart"));
            exportManager.exportImage($("#ctl00_ContentPlaceHolder1_ChartGroupCountry"));
        }
        function exportChartToImageGCountryGrp() {

            var $ = $telerik.$;
            //get reference to the ClientExportManager object
            var exportManager = $find('<%=RadClientExportManager1.ClientID%>');

            var filename = 'Group Registration By Country(By Number of Groups)' + '.png';

            //specify the image settings fileName, proxy, width, height
            var imageSettings = {
                fileName: filename
             }
            //set the image settings
            exportManager.set_imageSettings(imageSettings);
            //export the element/container
            //exportManager.exportImage($(".RadHtmlChart"));
            exportManager.exportImage($("#ctl00_ContentPlaceHolder1_ChartGroupCountryGrp"));
        }
        function exportChartToImageICountry() {

            var $ = $telerik.$;
            //get reference to the ClientExportManager object
            var exportManager = $find('<%=RadClientExportManager1.ClientID%>');

            var filename = 'Individual Registration By Country' + '.png';

            //specify the image settings fileName, proxy, width, height
            var imageSettings = {
                fileName: filename
             }
            //set the image settings
            exportManager.set_imageSettings(imageSettings);
            //export the element/container
            //exportManager.exportImage($(".RadHtmlChart"));
            exportManager.exportImage($("#ctl00_ContentPlaceHolder1_ChartIndivCountry"));
        }
    </script>
    <style type="text/css">
        .tdstyle1
        {
            width:170px;
        }

        .tdstyle2
        {
            width:300px;
        }

        form input[type="text"]
        {
            width:39% !important;
        }

        .ajax__calendar_container
        {
            width: 320px !important;
            height:280px !important;
        }

        .ajax__calendar_body
        {
            width: 100% !important;
            height:100% !important;
        }

        td
        {
            vertical-align:middle;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"></telerik:RadAjaxManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <h3 class="box-title">Summary (By Country)</h3>
            <asp:Panel ID="showlist" runat="server">
                Show List : <asp:DropDownList ID="ddl_showList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_showList_SelectedIndexChanged"></asp:DropDownList>
            </asp:Panel>
            <div class="table-responsive">

                <telerik:RadClientExportManager runat="server" ID="RadClientExportManager1"></telerik:RadClientExportManager>

                <div class="col-lg-12" id="divCountryGroup" runat="server">
                    <!-- Default box -->
                    <div class="box" style="width:100%;">
                        <div class="box-header with-border">
                            <h1 class="box-title" style="font-size:25px;">By Country (Group Registration)</h1>
                            <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div style="width:100%;">
                                <%--<telerik:RadHtmlChart runat="server" ID="ChartGroupCountry"  CssClass="fb-sized">
                                    <PlotArea>
                                        <Series>
                                            <telerik:PieSeries StartAngle="45" DataFieldY="TotalPercentage" ExplodeField="IsExploded" Name="PieSeriesName"
                                                NameField="Desc">
                                                <LabelsAppearance DataFormatString="{0}%">
                                                </LabelsAppearance>
                                                <TooltipsAppearance Color="White" DataFormatString="{0}%"></TooltipsAppearance>
                                            </telerik:PieSeries>
                                        </Series>
                                        <YAxis>
                                        </YAxis>
                                    </PlotArea>
                                    <ChartTitle Text="Group Registration By Country"></ChartTitle>
                                </telerik:RadHtmlChart>--%>
                                <telerik:RadButton RenderMode="Lightweight" runat="server" OnClientClicked="exportChartToImageGCountryGrp" 
                                Text="Export Group Registration By Country(By Number of Groups) to Image" AutoPostBack="false" Skin="Office2007" ID="btnChartGroupCountryGrp" />
                                <telerik:RadHtmlChart runat="server" ID="ChartGroupCountryGrp" Width="100%" Transitions="true" Skin="Silk">
                                    <%--<Zoom Enabled="true">
                                        <MouseWheel Enabled="true" />
                                        <Selection Enabled="true" ModifierKey="Shift" />
                                    </Zoom>--%>
                                    <PlotArea>
                                        <Series>
                                            <telerik:BarSeries Name="Registration" Stacked="false" Spacing="0.4" DataFieldY="TotalNumber" Gap="1.5">
                                                <Appearance>
                                                    <FillStyle BackgroundColor="#4572A7"></FillStyle>
                                                </Appearance>
                                                <LabelsAppearance>
                                                    <TextStyle Color="#4572A7" FontSize="13" Bold="true"/>
                                                    <ClientTemplate>
                                                        #=dataItem.TotalNumber# (#=dataItem.TotalPercentage# %)
                                                    </ClientTemplate>
                                                </LabelsAppearance>
                                                <TooltipsAppearance ClientTemplate="#=dataItem.Desc# <br /> <strong> Registration </strong> : #=dataItem.TotalNumber# (#=dataItem.TotalPercentage# %)"></TooltipsAppearance>
                                            </telerik:BarSeries>
                                        </Series>
                                        <Appearance>
                                            <FillStyle BackgroundColor="Transparent"></FillStyle>
                                        </Appearance>
                                        <XAxis AxisCrossingValue="0" Color="black" MajorTickType="Outside" MinorTickType="None" DataLabelsField="Desc"
                                            Reversed="false">
                                            <LabelsAppearance DataFormatString="{0}" RotationAngle="360" Skip="0" Step="1"><TextStyle Color="Black" Bold="true" /></LabelsAppearance>
                                            <TitleAppearance Position="Center" RotationAngle="0" Text="Country">
                                                <TextStyle Color="#666" Bold="true" />
                                            </TitleAppearance>
                                        </XAxis>
                                        <YAxis AxisCrossingValue="0" Color="black" Reversed="false">
                                            <LabelsAppearance RotationAngle="0" Skip="0" Step="1"><TextStyle Color="#666666" /></LabelsAppearance>
                                            <TitleAppearance Position="Center" RotationAngle="0" Text="Registrants">
                                                <TextStyle Color="#6D869F" Bold="true" />
                                            </TitleAppearance>
                                        </YAxis>
                                    </PlotArea>
                                    <Appearance>
                                        <FillStyle BackgroundColor="Transparent"></FillStyle>
                                    </Appearance>
                                    <ChartTitle Text="Group Registration By Country(By Number of Groups)">
                                        <Appearance Align="Center" BackgroundColor="Transparent" Position="Top">
                                            <TextStyle Color="#3E576F" FontSize="20" />
                                        </Appearance>
                                    </ChartTitle>
                                    <Legend>
                                        <Appearance BackgroundColor="Transparent" Position="Bottom">
                                        </Appearance>
                                    </Legend>
                                </telerik:RadHtmlChart>
                            </div>
                            <div style="width:100%;">
                                <%--<telerik:RadHtmlChart runat="server" ID="ChartGroupCountry"  CssClass="fb-sized">
                                    <PlotArea>
                                        <Series>
                                            <telerik:PieSeries StartAngle="45" DataFieldY="TotalPercentage" ExplodeField="IsExploded" Name="PieSeriesName"
                                                NameField="Desc">
                                                <LabelsAppearance DataFormatString="{0}%">
                                                </LabelsAppearance>
                                                <TooltipsAppearance Color="White" DataFormatString="{0}%"></TooltipsAppearance>
                                            </telerik:PieSeries>
                                        </Series>
                                        <YAxis>
                                        </YAxis>
                                    </PlotArea>
                                    <ChartTitle Text="Group Registration By Country"></ChartTitle>
                                </telerik:RadHtmlChart>--%>
                                <telerik:RadButton RenderMode="Lightweight" runat="server" OnClientClicked="exportChartToImageGCountry" 
                                Text="Export Group Registration By Country(By Number of Persons) to Image" AutoPostBack="false" Skin="Office2007" ID="btnChartGroupCountry" />
                                <telerik:RadHtmlChart runat="server" ID="ChartGroupCountry" Width="100%" Transitions="true" Skin="Silk">
                                    <%--<Zoom Enabled="true">
                                        <MouseWheel Enabled="true" />
                                        <Selection Enabled="true" ModifierKey="Shift" />
                                    </Zoom>--%>
                                    <PlotArea>
                                        <Series>
                                            <telerik:BarSeries Name="Registration" Stacked="false" Spacing="0.4" DataFieldY="TotalNumber" Gap="1.5">
                                                <Appearance>
                                                    <FillStyle BackgroundColor="#4572A7"></FillStyle>
                                                </Appearance>
                                                <LabelsAppearance>
                                                    <TextStyle Color="#4572A7" FontSize="13" Bold="true"/>
                                                    <ClientTemplate>
                                                        #=dataItem.TotalNumber# (#=dataItem.TotalPercentage# %)
                                                    </ClientTemplate>
                                                </LabelsAppearance>
                                                <TooltipsAppearance ClientTemplate="#=dataItem.Desc# <br /> <strong> Registration </strong> : #=dataItem.TotalNumber# (#=dataItem.TotalPercentage# %)"></TooltipsAppearance>
                                            </telerik:BarSeries>
                                        </Series>
                                        <Appearance>
                                            <FillStyle BackgroundColor="Transparent"></FillStyle>
                                        </Appearance>
                                        <XAxis AxisCrossingValue="0" Color="black" MajorTickType="Outside" MinorTickType="None" DataLabelsField="Desc"
                                            Reversed="false">
                                            <LabelsAppearance DataFormatString="{0}" RotationAngle="360" Skip="0" Step="1"><TextStyle Color="Black" Bold="true" /></LabelsAppearance>
                                            <TitleAppearance Position="Center" RotationAngle="0" Text="Country">
                                                <TextStyle Color="#666" Bold="true" />
                                            </TitleAppearance>
                                        </XAxis>
                                        <YAxis AxisCrossingValue="0" Color="black" Reversed="false">
                                            <LabelsAppearance RotationAngle="0" Skip="0" Step="1"><TextStyle Color="#666666" /></LabelsAppearance>
                                            <TitleAppearance Position="Center" RotationAngle="0" Text="Registrants">
                                                <TextStyle Color="#6D869F" Bold="true" />
                                            </TitleAppearance>
                                        </YAxis>
                                    </PlotArea>
                                    <Appearance>
                                        <FillStyle BackgroundColor="Transparent"></FillStyle>
                                    </Appearance>
                                    <ChartTitle Text="Group Registration By Country(By Number of Persons)">
                                        <Appearance Align="Center" BackgroundColor="Transparent" Position="Top">
                                            <TextStyle Color="#3E576F" FontSize="20" />
                                        </Appearance>
                                    </ChartTitle>
                                    <Legend>
                                        <Appearance BackgroundColor="Transparent" Position="Bottom">
                                        </Appearance>
                                    </Legend>
                                </telerik:RadHtmlChart>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <asp:Button ID="Button1" runat="server" Text="Export to Excel Group Report By Country(By Number of Persons)" OnClick="genCountryGExcel" />
                <asp:GridView ID="gvSummaryByCountryG" runat="server" DataKeyNames="Country" AutoGenerateColumns="true" CssClass="table" AllowPaging="false"
                    GridLines="None">
                    <HeaderStyle CssClass="theadstyle" />
                </asp:GridView>
                <br />
                <br />

                <div class="col-lg-12" id="divCountryIndiv" runat="server">
                    <!-- Default box -->
                    <div class="box" style="width:100%;">
                        <div class="box-header with-border">
                            <h1 class="box-title" style="font-size:25px;">By Country (Individual Registration)</h1>
                            <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div style="width:100%;">
                                <%--<telerik:RadHtmlChart runat="server" ID="ChartIndivCountry"  CssClass="fb-sized">
                                    <PlotArea>
                                        <Series>
                                            <telerik:PieSeries StartAngle="45" DataFieldY="TotalPercentage" ExplodeField="IsExploded" Name="PieSeriesName"
                                                NameField="Desc">
                                                <LabelsAppearance DataFormatString="{0}%">
                                                </LabelsAppearance>
                                                <TooltipsAppearance Color="White" DataFormatString="{0}%"></TooltipsAppearance>
                                            </telerik:PieSeries>
                                        </Series>
                                        <YAxis>
                                        </YAxis>
                                    </PlotArea>
                                    <ChartTitle Text="Individual Registration By Country"></ChartTitle>
                                </telerik:RadHtmlChart>--%>
                                <telerik:RadButton RenderMode="Lightweight" runat="server" OnClientClicked="exportChartToImageICountry" 
                                Text="Export Individual Registration By Country to Image" AutoPostBack="false" Skin="Office2007" />
                                <telerik:RadHtmlChart runat="server" ID="ChartIndivCountry" Width="100%" Transitions="true" Skin="Silk">
                                    <%-- <Zoom Enabled="true">
                                        <MouseWheel Enabled="true" />
                                        <Selection Enabled="true" ModifierKey="Shift" />
                                    </Zoom>--%>
                                    <PlotArea>
                                        <Series>
                                            <telerik:BarSeries Name="Registration" Stacked="false" Spacing="0.4" DataFieldY="TotalNumber" Gap="1.5">
                                                <Appearance>
                                                    <FillStyle BackgroundColor="#4572A7"></FillStyle>
                                                </Appearance>
                                                <LabelsAppearance>
                                                    <TextStyle Color="#4572A7" FontSize="13" Bold="true"/>
                                                    <ClientTemplate>
                                                        #=dataItem.TotalNumber# (#=dataItem.TotalPercentage# %)
                                                    </ClientTemplate>
                                                </LabelsAppearance>
                                                <TooltipsAppearance ClientTemplate="#=dataItem.Desc# <br /> <strong> Registration </strong> : #=dataItem.TotalNumber# (#=dataItem.TotalPercentage# %)"></TooltipsAppearance>
                                            </telerik:BarSeries>
                                        </Series>
                                        <Appearance>
                                            <FillStyle BackgroundColor="Transparent"></FillStyle>
                                        </Appearance>
                                        <XAxis AxisCrossingValue="0" Color="black" MajorTickType="Outside" MinorTickType="None" DataLabelsField="Desc"
                                            Reversed="false">
                                            <LabelsAppearance DataFormatString="{0}" RotationAngle="360" Skip="0" Step="1"><TextStyle Color="Black" Bold="true" /></LabelsAppearance>
                                            <TitleAppearance Position="Center" RotationAngle="0" Text="Country">
                                                <TextStyle Color="#666" Bold="true" />
                                            </TitleAppearance>
                                        </XAxis>
                                        <YAxis AxisCrossingValue="0" Color="black" Reversed="false">
                                            <LabelsAppearance RotationAngle="0" Skip="0" Step="1"><TextStyle Color="#666666" /></LabelsAppearance>
                                            <TitleAppearance Position="Center" RotationAngle="0" Text="Registrants">
                                                <TextStyle Color="#6D869F" Bold="true" />
                                            </TitleAppearance>
                                        </YAxis>
                                    </PlotArea>
                                    <Appearance>
                                        <FillStyle BackgroundColor="Transparent"></FillStyle>
                                    </Appearance>
                                    <ChartTitle Text="Individual Registration By Country">
                                        <Appearance Align="Center" BackgroundColor="Transparent" Position="Top">
                                            <TextStyle Color="#3E576F" FontSize="20" />
                                        </Appearance>
                                    </ChartTitle>
                                    <Legend>
                                        <Appearance BackgroundColor="Transparent" Position="Bottom">
                                        </Appearance>
                                    </Legend>
                                </telerik:RadHtmlChart>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer"></div>
                        <!-- /.box-footer-->
                    </div>
                    <!-- /.box -->
                </div>
                <br />
                <br />
                <asp:Button ID="Button2" runat="server" Text="Export to Excel Individual Report By Country" OnClick="genCountryIndivExcel" />
                <asp:GridView ID="gvSummaryByCountry" runat="server" DataKeyNames="Country" AutoGenerateColumns="true" CssClass="table" AllowPaging="false"
                    GridLines="None">
                    <HeaderStyle CssClass="theadstyle" />
                </asp:GridView>
                <br />
                <br />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Button1" />
            <asp:PostBackTrigger ControlID="Button2" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

