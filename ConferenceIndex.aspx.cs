﻿using Corpit.Abstract;
using Corpit.Promo;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ConferenceIndex : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    static string ASCAShowName = "ASCA 2019 Online Conference Registration";
    static string ERASDelegateIndivRegFlowName = "ERASIndividualReg2019";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string flwID = cFun.DecryptValue(urlQuery.FlowID);
            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flwID);
            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null && Session["RegStatus"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string regstage = Session["regstage"].ToString();
                string regstatus = Session["RegStatus"].ToString();
                if (Session["Regno"] != null)
                {
                    string regno = Session["Regno"].ToString();

                    StatusSettings statusSet = new StatusSettings(fn);
                    InvoiceControler invControler = new InvoiceControler(fn);
                    string invStatus = invControler.getInvoiceStatus(regno);

                    DataTable dtAbsSetting = getAbstractSetting(flowid, showid);
                    if (dtAbsSetting.Rows.Count > 0)
                    {
                        if (invStatus == statusSet.Pending.ToString() || invStatus == RegClass.noInvoice
                            || invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString())
                        {
                            string absShowID = dtAbsSetting.Rows[0]["AbsShowID"].ToString();
                            string checkAbsSubmittedUrl = dtAbsSetting.Rows[0]["AbsSubmittedCheckApiLink"].ToString();
                            string newUrlQuery = "FLW=" + urlQuery.FlowID + "&STP=" + urlQuery.CurrIndex
                            + "&GRP=" + cFun.EncryptValue(groupid) + "&INV=" + cFun.EncryptValue(regno) + "&SHW=" + urlQuery.CurrShowID;
                            urlQuery = new FlowURLQuery(newUrlQuery);

                            string promocode = getUsedPromoCode(groupid, flowid, showid, urlQuery);
                            int con_categoryID = 0;
                            string abspromocode = getPromoCodeAccordingAbsRules(regno, showid, flowid, absShowID, checkAbsSubmittedUrl);
                            if(!string.IsNullOrEmpty(abspromocode))
                            {
                                promocode = abspromocode;
                            }

                            bool hasRightToUseOtherPromoCode = hasRighttouseOtherPromoCode(regno, showid, flowid);
                            if (hasRightToUseOtherPromoCode)/*regno == "37410327" [AsCA registrant(e0185968@u.nus.edu)]*/
                            {
                                promocode = getUsedPromoCode(groupid, flowid, showid, urlQuery);
                            }

                            /*added on 22-10-2019 according to Constance suggestion in Work In Progress sheet 116*/
                            ShowControler shwCtrl = new ShowControler(fn);
                            Show shw = shwCtrl.GetShow(showid);
                            if(shw.SHW_Name.Contains(ASCAShowName))
                            {
                                promocode = getUsedPromoCode(groupid, flowid, showid, urlQuery);
                            }
                            /**/

                            //if (!string.IsNullOrEmpty(promocode))
                            {
                                CategoryObj catObj = new CategoryObj(fn);
                                int.TryParse(catObj.checkCategory(urlQuery, regno, promocode.Trim()), out con_categoryID);
                                RegDelegateObj rgd = new RegDelegateObj(fn);
                                rgd.updateCategoryID(regno, con_categoryID, showid);
                            }
                        }
                    }
                    RedirectToNormalFlow(regno, groupid, regstatus, regstage, flowid, showid);
                }
                else
                {
                    Response.Redirect("ReLogin.aspx?Event=" + fmaster.FlowName.Trim());
                }
            }
            else
            {
                Response.Redirect("ReLogin.aspx?Event=" + fmaster.FlowName.Trim());
            }
        }
    }
    #region getPromoCodeAccordingAbsRules
    public string getPromoCodeAccordingAbsRules(string regno, string showid, string flowid, string absShowID, string checkAbsSubmittedUrl)
    {
        string returnPromoCode = string.Empty;

        try
        {
            bool isOk = checkAbsSubmittedAPI(regno, absShowID, checkAbsSubmittedUrl);
            if (isOk)
            {
                string query = "Select * From ref_AbsPromoRule Where ShowID=@ShowID And FlowID=@FlowID And rule_isdeleted=0 Order By rule_Priority";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
                SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
                spar1.Value = showid;
                spar2.Value = flowid;
                pList.Add(spar1);
                pList.Add(spar2);
                DataTable dt = fn.GetDatasetByCommand(query, "dsCatRule", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string ruleSql = dr["rule_name"].ToString();
                        string nextstep = dr["rule_nextstep"] != DBNull.Value ? dr["rule_nextstep"].ToString() : "";
                        string isByRegno = dr["isByRegno"] != DBNull.Value ? dr["isByRegno"].ToString() : "";

                        if (isByRegno == "1")
                        {
                            ruleSql += regno;
                            ruleSql += " And ShowID=@ShowID And reg_urlFlowID=@FlowID";
                        }
                        DataTable dtResult = fn.GetDatasetByCommand(ruleSql, "dsResult", pList).Tables[0];
                        if (dtResult.Rows.Count > 0)
                        {
                            returnPromoCode = dr["rule_returnvalue"].ToString();
                            return returnPromoCode;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return returnPromoCode;
    }
    public bool checkAbsSubmittedAPI(string regno, string absShowID, string checkAbsSubmittedUrl)
    {
        bool isOk = false;
        try
        {
            AbstractSubmittedParam absSObj = new AbstractSubmittedParam();
            absSObj.AbsShowID = absShowID;
            absSObj.RegPortalRegno = regno;
            string jsonStr = JsonConvert.SerializeObject(absSObj);
            string rtnValue = PostAbsApi(jsonStr, checkAbsSubmittedUrl);
            if (!string.IsNullOrEmpty(rtnValue) && rtnValue != "ERROR")
            {
                AbstractSubmittedStatusJson rtnStatus = JsonConvert.DeserializeObject<AbstractSubmittedStatusJson>((string)rtnValue);
                if (!string.IsNullOrEmpty(rtnStatus.Status) && rtnStatus.Status.ToUpper() == "OK")
                {
                    isOk = true;
                    string absRegno = rtnStatus.AbsRegno;
                }
            }
        }
        catch (Exception ex)
        {
            isOk = false;
        }
        return isOk;
    }
    public string PostAbsApi(string jData, string APILink)
    {
        string rtnData = "";
        try
        {
            string jsonStr = jData;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(APILink);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonStr);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                rtnData = streamReader.ReadToEnd();
            }
        }
        catch (Exception ex) { rtnData = "ERROR"; }
        return rtnData;
    }
    #endregion
    #region RedirectToNormalFlow
    private void RedirectToNormalFlow(string regno, string groupid, string regstatus, string regstage, string flowid, string showid)
    {
        StatusSettings statusSet = new StatusSettings(fn);
        InvoiceControler invControler = new InvoiceControler(fn);
        StatusSettings stuSettings = new StatusSettings(fn);

        FlowControler flwControl = new FlowControler(fn);
        FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
        string lateststage = flwControl.GetLatestStep(flowid);

        if (!string.IsNullOrEmpty(lateststage) && regstage == lateststage && cFun.ParseInt(regstatus) == statusSet.Success)
        {
            string invStatus = invControler.getInvoiceStatus(regno);
            if (invStatus == stuSettings.Pending.ToString())
            {
                string page = ReLoginStaticValueClass.regConfirmationPage;
                string currentstage = flwControl.GetConirmationStage(flowid, SiteDefaultValue.constantConfirmationPage);
                string route = flwControl.MakeFullURL(page, flowid, showid, groupid, currentstage, regno);
                //Response.Redirect(route, false);
                Response.Write("<script>");
                Response.Write("window.open('" + route + "','_blank');window.location.href = '" + Request.UrlReferrer.AbsoluteUri + "';");
                Response.Write("</script>");
            }
            if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                || invStatus == RegClass.noInvoice
                || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
            {
                string page = ReLoginStaticValueClass.regHomePage;
                string route = flwControl.MakeFullURL(page, flowid, showid);
                Response.Redirect(route, false);
            }
        }
        else
        {
            string invStatus = invControler.getInvoiceStatus(regno);
            if (invStatus == RegClass.noInvoice || invStatus == stuSettings.Pending.ToString())
            {
                FlowControler flwObj = new FlowControler(fn, flowid, regstage);
                string page = flwObj.CurrIndexModule;
                string currentstage = regstage;//"2";
                string route = flwObj.MakeFullURL(page, flowid, showid, groupid, currentstage, regno);
                //Response.Redirect(route, false);
                /*added on 29-10-2019*/
                string additionalParam = getScriptModuleTypeForQA(flowid, page, currentstage);
                if(!string.IsNullOrEmpty(additionalParam) && additionalParam != "0")
                {
                    route += additionalParam;
                }
                /*added on 29-10-2019*/
                Response.Write("<script>");
                Response.Write("window.open('" + route + "','_blank');window.location.href = '" + Request.UrlReferrer.AbsoluteUri + "';");
                Response.Write("</script>");
            }
            else if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
            {
                string page = ReLoginStaticValueClass.regHomePage;
                string route = flwControl.MakeFullURL(page, flowid, showid);
                Response.Redirect(route, false);
            }
        }
    }
    #endregion
    #region getAbstractSetting
    private DataTable getAbstractSetting(string flowid, string showid)
    {
        DataTable dtResult = new DataTable();
        try
        {
            string sql = "Select * From tb_AbstractSettings Where ShowID=@ShowID And FlowID=@FlowID";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            pList.Add(spar1);
            pList.Add(spar2);
            dtResult = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dtResult;
    }
    #endregion
    #region getUsedPromoCode(added on 11-4-2019)
    private string getUsedPromoCode(string groupid, string flowid, string showid, FlowURLQuery urlQuery)
    {
        string returnPromoCode = "";
        CategoryClass catClass = new CategoryClass();
        FlowControler fCon = new FlowControler(fn);
        FlowMaster flwMaster = fCon.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
        if (flwMaster.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
        {
            string type = flwMaster.FlowCategoryConfigType;
            if (type == catClass.flowcat_ByPromoCode.ToString() || type == catClass.flowcat_ByBusinessLogicWithPromo.ToString() || type == catClass.flowcat_ByBusinessLogicWithPromoNew.ToString())//According to Business Logic (Customize) *Like SHBC
            {
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);
                if (shw.SHW_Name == ASCAShowName)
                {
                    PromoController pCtrl = new PromoController(fn);
                    returnPromoCode = pCtrl.getUsedPromoCodeByGroupID(groupid, flowid, showid);
                }
                else if(flwMaster.FlowName == ERASDelegateIndivRegFlowName)
                {
                    PromoController pCtrl = new PromoController(fn);
                    returnPromoCode = pCtrl.getUsedPromoCodeByGroupID(groupid, flowid, showid);
                }
                else/*added on 31-12-2019*/
                {
                    PromoController pCtrl = new PromoController(fn);
                    returnPromoCode = pCtrl.getUsedPromoCodeByGroupID(groupid, flowid, showid);
                }
            }
        }

        return returnPromoCode;
    }
    #endregion
    #region hasRighttouseOtherPromoCode
    private bool hasRighttouseOtherPromoCode(string regno, string showid, string flowid)
    {
        bool hasRight = false;
        try
        {
            string sql = string.Format("Select * From ref_AbsSubmitter_UsedSystemPromo Where ShowID='{0}' And FlowID='{1}' And Regno='{2}'", showid, flowid, regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if(dt.Rows.Count > 0)
            {
                hasRight = true;
            }
        }
        catch(Exception ex)
        { }

        return hasRight;
    }
    #endregion
    #region getScriptModuleTypeForQA
    private string getScriptModuleTypeForQA(string flowid, string scriptid, string stepseq)
    {
        string moduleType = "";
        try
        {
            if (scriptid.Contains("Questionnaire"))
            {
                string sql = "Select Distinct t.ModuleType ModuleType From tb_site_flow as f Inner Join tb_site_flow_master fm On f.flow_id=fm.FLW_ID "
                                + " Left Join tb_site_Flow_Template as t On fm.FLW_REF_TEMPLATE=t.Route_ID "
                                + " Left Join tb_site_module as m On t.Module_Id=m.module_id Where flow_id='" + flowid + "' And f.script_id=m.script_id "
                                + " And f.script_id Like '" + scriptid + "' And f.step_seq='" + stepseq + "'";
                moduleType = fn.GetDataByCommand(sql, "ModuleType");
                if (string.IsNullOrEmpty(moduleType) || moduleType == "0")
                {
                    moduleType = "";
                }
                else
                {
                    moduleType = "&Type=" + moduleType;
                }
            }
        }
        catch(Exception ex)
        { }

        return moduleType;
    }
    #endregion
}