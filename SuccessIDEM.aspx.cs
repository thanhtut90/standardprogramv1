﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Logging;
using System.Data;
using Corpit.Email;
using System.Text.RegularExpressions;

public partial class SuccessIDEM : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cComFuz = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();
    protected static string RegNoParam = "%RegNo%";
    protected static string RegGroupNoParam = "%RegGroupID%";
    protected static string ConfirmationUrlParam = "%ConfirmationUrl%";

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        SetSiteMaster(showid);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);

            insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actsuccess, urlQuery);

            RegGroupObj rgg = new RegGroupObj(fn);
            rgg.updateGroupCurrentStep(urlQuery);

            RegDelegateObj rgd = new RegDelegateObj(fn);
            rgd.updateDelegateCurrentStep(urlQuery);

            string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
            FlowControler flwControl = new FlowControler(fn);
            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);

            StatusSettings stuSettings = new StatusSettings(fn);

            string regstatus = "0";
            DataTable dt = new DataTable();
            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
            {
                dt = rgg.getRegGroupByID(GroupRegID, showid);
                if (dt.Rows.Count > 0)
                {
                    regstatus = dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                }

                rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                rgd.updateDelegateRegStatus(GroupRegID, stuSettings.Success, showid);
            }
            else//*flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL
            {
                rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                if (!string.IsNullOrEmpty(DelegateID))
                {
                    dt = rgd.getDataByGroupIDRegno(GroupRegID, DelegateID, showid);
                    if (dt.Rows.Count > 0)
                    {
                        regstatus = dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                    }

                    rgd.updateStatus(DelegateID, stuSettings.Success, showid);
                }
            }

            #region bindSuccessMessage
            lblSuccessMessage.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowSuccessMsg) ? flwMasterConfig.FlowSuccessMsg : "").Replace(RegNoParam, DelegateID).Replace(RegGroupNoParam, GroupRegID);
            if (string.IsNullOrEmpty(lblSuccessMessage.Text))
                PanelMsg.Visible = false;
            #endregion

            if (cComFuz.ParseInt(regstatus) == stuSettings.Pending)
            {
                FlowControler flw = new FlowControler(fn, urlQuery);
                //  flw.SendCurrentStepEmail(urlQuery);
                EmailHelper esender = new EmailHelper();
                esender.SendCurrentFlowStepEmail(urlQuery);
            }

            try
            {
                CommonFuns cFuz = new CommonFuns();
                string DID = "";
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    DID = GroupRegID;
                }
                else
                    DID = DelegateID;
                bindConfirmationUrl(showid, DID, flowid, GroupRegID);/*added on 15-7-2019*/
                HTMLTemplateControler htmlControl = new HTMLTemplateControler();
                string template = htmlControl.CreateAcknowledgeLetterHTMLTemplate(showid, DID);
                string rtnFile = htmlControl.CreatePDF(template, showid, DID, "Acknowledge");
                if (!string.IsNullOrEmpty(rtnFile))
                {
                    ShowPDF.Visible = true;
                    ShowPDF.Attributes["src"] = rtnFile;
                }
            }
            catch { }
        }
    }

    #region PageSetting
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
            Page.MasterPageFile = masterPage;
    }

    #endregion

    #region insertLogFlowAction
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        string step = cComFuz.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    #region bindConfirmationUrl (added on 15-7-2019)
    private void bindConfirmationUrl(string showid, string regno, string flowid, string groupid)
    {
        try
        {
            string encrytedShowID = cComFuz.EncryptValue(showid);
            string encrytedRegNO = cComFuz.EncryptValue(regno);
            string url = GetRootURL();
            if (!string.IsNullOrEmpty(url))
            {
                InvoiceControler invControler = new InvoiceControler(fn);
                string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                string invID = "";
                Invoice invLatest = invControler.GetLatestCompleteInvoice(invOwnerID, groupid);
                string invSuccess = ((int)StatusValue.Success).ToString();
                if (invLatest != null)
                {
                    if (invLatest.Status == invSuccess)
                    {
                        invID = invLatest.InvoiceID;
                    }
                }
                if(!string.IsNullOrEmpty(invID))
                {
                    url += string.Format("/DownloadConfirmationLetter?SHW={0}&DID={1}&VDD={2}", encrytedShowID, encrytedRegNO, cComFuz.EncryptValue(invID));
                }
                else
                {
                    url += string.Format("/DownloadConfirmationLetter?SHW={0}&DID={1}", encrytedShowID, encrytedRegNO);
                }
                lblSuccessMessage.Text = lblSuccessMessage.Text.Replace(ConfirmationUrlParam, url);
            }
        }
        catch { }
    }
    string adminRoot = "/admin";
    public string GetRootURL()
    {
        string url = HttpContext.Current.Request.Url.AbsoluteUri;
        if (!string.IsNullOrEmpty(url))
        {
            if (url.LastIndexOf("?") >= 0)
                url = url.Substring(0, url.LastIndexOf("?"));
            url = url.ToLower();
            url = url.Substring(0, url.LastIndexOf("/"));
            bool isAdmin = Regex.IsMatch(url, adminRoot);
            if (isAdmin)
                url = url.Replace(adminRoot, "");


        }

        return url;
    }
    #endregion
}