﻿using Corpit.Logging;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class QuestionnaireExit : System.Web.UI.Page
{
    CommonFuns cFun = new CommonFuns();
    Functionality fn = new Functionality();
    LogActionObj rlgobj = new LogActionObj();

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            FlowControler flwController = new FlowControler(fn);
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);

            if (!string.IsNullOrEmpty(showid))
            {
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string regno = cFun.DecryptValue(urlQuery.DelegateID);

                FlowControler flwObj = new FlowControler(fn, urlQuery);
                FlowMaster flwConfig = flwController.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));

                string page = flwObj.NextStepURL();
                string step = flwObj.NextStep;
                string FlowID = flwObj.FlowID;
                string grpNum = urlQuery.GoupRegID;

                DataTable dtAbsSetting = getAbstractSetting(flowid, showid);

                if (flwConfig.FlowLimitDelegateQty == "1")
                {
                    string route = flwObj.MakeFullURL(page, FlowID, showid, grpNum, step);
                    if (dtAbsSetting.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please proceed to click on the Abstract/Forum Registration tab.');window.location='" + route + "';", true);
                    }
                    else
                    {
                        Response.Redirect(route, false);
                    }
                }
                else
                {
                    string route = flwObj.MakeFullURL(page, FlowID, showid, grpNum, step, regno);
                    if (dtAbsSetting.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please proceed to click on the Abstract/Forum Registration tab.');window.location='" + route + "';", true);
                    }
                    else
                    {
                        Response.Redirect(route, false);
                    }
                }
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion
    private DataTable getAbstractSetting(string flowid, string showid)
    {
        DataTable dtResult = new DataTable();
        try
        {
            string sql = "Select * From tb_AbstractSettings Where ShowID=@ShowID And FlowID=@FlowID";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            pList.Add(spar1);
            pList.Add(spar2);
            dtResult = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dtResult;
    }
}