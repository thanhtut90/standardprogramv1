﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReloginFormMaster.master" AutoEventWireup="true" CodeFile="ReloginLock.aspx.cs" Inherits="ReloginLock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="form-group row">
        <div class="col-md-2">&nbsp;</div>
        <div class="col-md-8 text-center">
            <h4><asp:Label ID="lblMessage" runat="server" Text="Sorry, please try later!"></asp:Label></h4>
        </div>
        <div class="col-md-2">&nbsp;</div>
    </div>
</asp:Content>

