﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Security;
using Corpit.Registration;
using System.Data;
using System.Data.SqlClient;
using Corpit.Logging;
public partial class Activation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Session.Clear();
            //Session.Abandon();
            bool ok = false;
            try
            {
                Functionality fn = new Functionality();
                ActivationControler aC = new ActivationControler(fn);
                UserActivation passData = GetDataFromURLParms();
                FlowMaster newFlow = UpgradetoActivatedFlow(passData);
                string flowName = newFlow.FlowName;
                if (!string.IsNullOrEmpty(passData.ATVCode))
                {
                    bool isValid = aC.IsValidActivation(passData);
                    if (isValid)
                    {
                        bool isOK = aC.UpdateSuccess(passData);
                        //Flow change
                        if (isOK)
                        {
                            //FlowMaster newFlow = UpgradetoActivatedFlow(passData);

                            if (!string.IsNullOrEmpty(newFlow.ShowID) && !string.IsNullOrEmpty(newFlow.FlowID))
                            {
                                ok = true;
                                SendLoginEmail(newFlow, passData);
                                Redirect(passData.RefNo, passData.GroupID, newFlow.ShowID, newFlow.FlowID);
                            }
                        }
                    }
                }

                if (!ok)
                {
                    //Response.Redirect("ReLogin.aspx");
                    /*updated because when redirect to relogin.aspx page, need to pass flowname (on 3-5-2019)*/
                    string url = "ReLogin.aspx?Event=" + flowName.Trim();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Your account has been activated. Please login with your user name and password sent to you in the relogin email.');window.location.href='" + url + "';", true);
                }
            }
            catch
            {
            }
        }
    }

    private UserActivation GetDataFromURLParms()
    {
        UserActivation passData = new UserActivation();
        CommonFuns cFun = new CommonFuns();
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string regno = cFun.DecryptValue(urlQuery.DelegateID);
        string grpID = cFun.DecryptValue(urlQuery.GoupRegID);
        string actCode = "";
        if (Request.QueryString["ATV"] != null)
        {
            actCode = Request.QueryString["ATV"].ToString();
            actCode = cFun.DecryptValue(actCode);
        }
        else actCode = "";
        if (!string.IsNullOrEmpty(showid) && !string.IsNullOrEmpty(flowid) && !string.IsNullOrEmpty(regno) && !string.IsNullOrEmpty(actCode))
        {

            passData.ShowID = showid;
            passData.FlowID = flowid;
            passData.RefNo = regno;
            passData.GroupID = grpID;
            passData.ATVCode = actCode;

        }

        return passData;
    }

    private FlowMaster UpgradetoActivatedFlow(UserActivation passData)
    {
        FlowMaster rtnFlow = new FlowMaster();
        try
        {
            Functionality fn = new Functionality();
            CommonFuns cFun = new CommonFuns();
            FlowControler flowContr = new FlowControler(fn);
            FlowMaster currFlow = flowContr.GetFlowMasterConfig(passData.FlowID);
            if (!string.IsNullOrEmpty(currFlow.FlowID) && !string.IsNullOrEmpty(currFlow.ActivateFlowName))
            {
                FlowMaster toUpgradeFlow = flowContr.GetByEventName(currFlow.ActivateFlowName);
                if (!string.IsNullOrEmpty(toUpgradeFlow.ShowID) && !string.IsNullOrEmpty(toUpgradeFlow.FlowID))
                {
                    //update flow and step
                    RegDelegateObj rObj = new RegDelegateObj(fn);
                    RegGroupObj gObj = new RegGroupObj(fn);
                    int i = rObj.updateStep(passData.RefNo, toUpgradeFlow.FlowID, "2", toUpgradeFlow.ShowID);
                    int y = gObj.updateStep(passData.GroupID, toUpgradeFlow.FlowID, "2", toUpgradeFlow.ShowID);
                    if (i > 0 && y > 0)
                    {
                        rtnFlow.FlowID = toUpgradeFlow.FlowID;
                        rtnFlow.ShowID = toUpgradeFlow.ShowID;
                        rtnFlow.FlowName = toUpgradeFlow.FlowName;
                        StatusSettings stuSettings = new StatusSettings(fn);
                        rObj.updateStatus(passData.RefNo, stuSettings.Pending, toUpgradeFlow.ShowID);//***
                    }
                }
            }
        }
        catch { }

        return rtnFlow;
    }


    private void SendLoginEmail(FlowMaster newFlow, UserActivation userData)
    {
        try
        {

            Functionality fn = new Functionality();
            FlowURLQuery urlQuery = new FlowURLQuery("");
            CommonFuns cFuz = new CommonFuns();
            urlQuery.CurrIndex = cFuz.EncryptValue("1");
            urlQuery.CurrShowID = cFuz.EncryptValue(newFlow.ShowID);
            urlQuery.FlowID = cFuz.EncryptValue(newFlow.FlowID);
            urlQuery.GoupRegID = cFuz.EncryptValue(userData.GroupID);
            string regno = "";
            //For Group
            if (userData.RefNo == urlQuery.GoupRegID)
                regno = "";
            else
                regno = cFuz.EncryptValue(userData.RefNo);

            urlQuery.DelegateID = regno;
            FlowControler flw = new FlowControler(fn, urlQuery);

            EmailHelper esender = new EmailHelper();
            esender.SendCurrentFlowStepEmail(urlQuery);
        }
        catch { }
    }

    private void Redirect(string regno, string groupid, string showid, string flowid)
    {
        Functionality fn = new Functionality();
        CommonFuns cFun = new CommonFuns();


        if (!string.IsNullOrEmpty(showid) && !string.IsNullOrEmpty(flowid))
        {
            string username = cFun.solveSQL(regno);
            string password = "";
            //    updateCurStep(urlQuery);//***
            string cmdStr = string.Empty;
            DataTable dt = new DataTable(); StatusSettings statusSet = new StatusSettings(fn);
            InvoiceControler invControler = new InvoiceControler(fn);
            StatusSettings stuSettings = new StatusSettings(fn);
            FlowControler flwControl = new FlowControler(fn);
            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
            string flowname = flwMasterConfig.FlowName;
            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
            {
                #region Group Registration
                password = cFun.solveSQL(getEmailPassword(groupid, regno, showid, true));//***
                RegGroupObj rgObj = new RegGroupObj(fn);
                dt = rgObj.getGroupReLoginAuthenData(username, password, flowid);

                if (dt.Rows.Count > 0)
                {
                    groupid = dt.Rows[0]["RegGroupID"].ToString();
                    Session["Groupid"] = groupid;
                    Session["Flowid"] = flowid;
                    Session["Showid"] = showid;
                    string regstage = dt.Rows[0]["RG_Stage"].ToString();
                    Session["regstage"] = regstage;
                    string regstatus = dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                    Session["RegStatus"] = regstatus;

                    LoginLog lg = new LoginLog(fn);
                    string userip = GetUserIP();
                    lg.loginid = groupid;
                    lg.loginip = userip;
                    lg.logintype = LoginLogType.regLogin;
                    lg.saveLoginLog();

                    string lateststage = flwControl.GetLatestStep(flowid);

                    if (!string.IsNullOrEmpty(lateststage) && regstage == lateststage && cFun.ParseInt(regstatus) == statusSet.Success)
                    {
                        string invStatus = invControler.getInvoiceStatus(groupid);
                        if (invStatus == stuSettings.Pending.ToString())
                        {
                            string page = ReLoginStaticValueClass.regIndexPage;
                            string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                            Response.Redirect(route, false);
                        }
                        if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                            || invStatus == RegClass.noInvoice
                            || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                        {
                            string page = ReLoginStaticValueClass.regHomePage;
                            string route = flwControl.MakeFullURL(page, flowid, showid);
                            Response.Redirect(route, false);
                        }
                    }
                    else
                    {
                        string invStatus = invControler.getInvoiceStatus(groupid);
                        if (invStatus == RegClass.noInvoice || invStatus == stuSettings.Pending.ToString())
                        {
                            string page = ReLoginStaticValueClass.regIndexPage;
                            string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                            Response.Redirect(route, false);
                        }
                        else if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                            || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                        {
                            string page = ReLoginStaticValueClass.regHomePage;
                            string route = flwControl.MakeFullURL(page, flowid, showid);
                            Response.Redirect(route, false);
                        }
                    }
                }
                else
                {
                    Response.Redirect("ReLogin.aspx?Event=" + flowname.Trim());
                }
                #endregion
            }
            else
            {
                #region Individual Registration
                password = cFun.solveSQL(getEmailPassword(groupid, regno, showid, false));//***
                RegDelegateObj rdObj = new RegDelegateObj(fn);
                dt = rdObj.getDelegateReLoginAuthenData(username, password, flowid);
                if (dt.Rows.Count > 0)
                {
                    groupid = dt.Rows[0]["RegGroupID"].ToString();
                    regno = dt.Rows[0]["Regno"].ToString();
                    Session["Regno"] = regno;
                    Session["Groupid"] = groupid;
                    Session["Flowid"] = flowid;
                    Session["Showid"] = showid;
                    string regstage = dt.Rows[0]["reg_Stage"].ToString();
                    Session["regstage"] = regstage;
                    string regstatus = dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                    Session["RegStatus"] = regstatus;

                    LoginLog lg = new LoginLog(fn);
                    string userip = GetUserIP();
                    lg.loginid = groupid;
                    lg.loginip = userip;
                    lg.logintype = "Registration Login";
                    lg.saveLoginLog();

                    DataTable dtMenu = new DataTable();
                    dtMenu = flwControl.GetReloginMenu(flowid, "0");
                    if (dtMenu.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtMenu.Rows)
                        {
                            string scriptid = dr["reloginref_scriptID"].ToString().Trim();
                            string menuname = dr["reloginmenu_name"].ToString().Trim();
                            string stepseq = dr["step_seq"].ToString().Trim();
                            string menupageHeader = dr["step_label"].ToString().Trim();
                            if (menuname == SiteDefaultValue.constantDelegateName)
                            {
                                if (flwMasterConfig.FlowType != SiteFlowType.FLOW_GROUP)
                                {
                                    string path = Request.Url.AbsolutePath;
                                    string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                    string[] strpath = path.Split('/');
                                    if (strpath.Length > 1)
                                    {
                                        path = strpath[1] + ".aspx";
                                    }
                                    else
                                    {
                                        path = ".aspx";
                                    }

                                    string page = scriptid;
                                    string route = flwControl.MakeFullURL(page, flowid, showid, groupid, stepseq, regno);//regstage
                                    Response.Redirect(route, false);
                                }
                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect("ReLogin.aspx?Event=" + flowname.Trim());
                }
                #endregion
            }
        }
        else
        {
            string url = "404.aspx";
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sorry, failure attempt.');window.location.href='" + url + "';", true);
        }
    }
    private string getEmailPassword(string groupid, string regno, string showid, bool isGroup)
    {
        Functionality fn = new Functionality();
        string returnValue = "";
        try
        {
            DataTable dtReg = new DataTable();
            if (isGroup)
            {
                RegGroupObj rgg = new RegGroupObj(fn);
                dtReg = rgg.getRegGroupByID(groupid, showid);
                if (dtReg.Rows.Count > 0)
                {
                    returnValue = dtReg.Rows[0]["RG_ContactEmail"].ToString();
                }
            }
            else
            {
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);
                RegDelegateObj rgd = new RegDelegateObj(fn);
                dtReg = rgd.getDataByGroupIDRegno(groupid, regno, showid);
                if (dtReg.Rows.Count > 0)
                {
                    returnValue = dtReg.Rows[0]["reg_Email"].ToString();
                }
            }
        }
        catch (Exception ex)
        { }

        return returnValue;
    }
    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }

    private void updateCurStep(FlowURLQuery urlQuery)
    {
        try
        {
            Functionality fn = new Functionality();
            CommonFuns cFun = new CommonFuns();
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cFun.DecryptValue(urlQuery.DelegateID);
            FlowControler flwObj = new FlowControler(fn, urlQuery);
            string showID = urlQuery.CurrShowID;
            string page = flwObj.NextStepURL();
            string step = flwObj.NextStep;
            string FlowID = flwObj.FlowID;
            string fullUrl = "FLW=" + flowid + "&STP=" + step + "&GRP=" + cFun.DecryptValue(groupid) + "&INV=" + delegateid + "&SHW=" + showID;
            FlowURLQuery newUrlQuery = new FlowURLQuery(fullUrl);

            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj.Count == 0)
            {
                //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
                RegGroupObj rgg = new RegGroupObj(fn);
                rgg.updateGroupCurrentStep(newUrlQuery);

                //*update reg_urlFlowID(current flowid) and reg_Stage(current step) of tb_RegDelegate table
                RegDelegateObj rgd = new RegDelegateObj(fn);
                rgd.updateDelegateCurrentStep(newUrlQuery);
            }
        }
        catch (Exception ex)
        { }
    }

}