﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FoodJapanWelcome.aspx.cs" Inherits="FoodJapanWelcome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/Icon1.png" />

    <link href="Content/Default/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Content/Default/Site.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="Scripts/jquery-2.1.0.js"></script>
    <script type="text/javascript" src="Scripts/jquery.plugin.js"></script>
    <script type="text/javascript" src="Scripts/bootstrap.min.js"></script>

    <style type="text/css">
        #lnkTrade
        {
            color:white !important;
            background-color: darkgreen !important;/*rgb(238, 77, 12)*/
            white-space:normal !important;
            word-wrap:break-word;
            /*padding:6px 6px !important;*/
        }
        #btnTrade
        {
            color:white !important;
            background-color:rgb(238, 77, 12) !important;
            white-space:normal !important;
            word-wrap:break-word;
        }
        #btnTradeGroup
        {
            background-color:#1c4f7e !important;
        }
        #btnTradeGroup
        {
            color:white !important;
            background-color:rgb(238, 77, 12) !important;
            white-space:normal !important;
            word-wrap:break-word;
            padding:6px 20px !important;
            background-color:#1c4f7e !important;
        }
        /*#lnkPublic
        {
            color:white !important;
            background-color:rgb(220, 159, 22) !important;
            white-space:normal !important;
            word-wrap:break-word;
            padding:6px 20px !important;
        }*/
        #lnkPublic
        {
            color:white !important;
            background-color:rgb(220, 159, 22) !important;
            white-space:normal !important;
            word-wrap:break-word;
            padding:6px 18px !important;
        }
        #btnPublic
        {
            color:white !important;
            background-color:rgb(220, 159, 22) !important;
            white-space:normal !important;
            word-wrap:break-word;
            padding:6px 18px !important;
        }
        #btnSignup,#lnkSignup
        {
            color:white !important;
            background-color:green !important;/*olivedrab*/
            white-space:normal !important;
            word-wrap:break-word;
            /*height:55px;
            border: 1px solid transparent;*/
            padding:6px 5px !important;
        }

        /*.btn{
            white-space:normal !important;
            word-wrap:break-word; 
        }*/

        .col-middle {
          vertical-align: middle;
          display: table-cell;
          float:none;
        }

        body
        {
            background-color: white;/*#f8e4ff;*/
            font-size: 16px !important;
        }
    </style>

</head>
<body>
    <div class="container">
    <div id="header">
        <a href="http://www.oishii-world.com/en" target="_blank">
            <asp:Image ID="imgBanner" runat="server" border="0" ImageUrl="https://event-reg.biz/DefaultBanner/images/FoodJapan2019/FoodJapan2019Banner.jpg"
                    style = "position:relative; right:10px; top:0px; z-index:1; width:100%;"/>
        </a>
        <br />
    </div>

    <form id="SignupForm" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="form-group row" id="divKeyPortion" runat="server" visible="false">
            <div class="col-md-4">
                &nbsp;
            </div>
            <div class="col-md-4 text-center">
                <asp:Image ID="imgLogo" runat="server" border="0" src="Admin/image/Site/2017FinalKeyVisual.jpg"
                style = "width:100%" ImageAlign="Middle"/>
            </div>
            <div class="col-md-4">
                &nbsp;
            </div>
        </div>

        <%--<br />--%>
        <div class="form-group row">
            <div class="col-md-2" style="display:none;"><%--col-md-4 according to their request(15-8-2018)--%>
                <h3><u>Workshops / ワークショップ</u></h3>
                <table style="width:100%">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
						    &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr id="trMasterclassOpen" runat="server">
                        <td>
                            <table style="width:100%;">
                            <tr>
                                <td>
                                    <b><asp:Label runat="server" ID="Label5" Text="Dates:"></asp:Label></b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label7" Text="Workshops for Public Visitors - 27 October (Saturday)"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b><asp:Label runat="server" ID="Label13" Text="Venue:"></asp:Label></b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label9" Text="Hall 401"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%--<b><asp:Label runat="server" ID="Label20" Text="Workshop Fee:"></asp:Label></b>--%>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%--<asp:Label runat="server" ID="Label21" Text="SGD 45 per session"></asp:Label>--%>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <%--<tr>
                                <td>
                                    <b><asp:Label runat="server" ID="Label13" Text="Venue:"></asp:Label></b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="Label9" Text="Hall 401"></asp:Label>
                                </td>
                            </tr>--%>
							
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <%--<tr>
                                <td>
                                    Comment*<b><asp:Label runat="server" ID="Label8" Text="Admission Fee:"></asp:Label></b>*
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Comment*<asp:Label runat="server" ID="Label10" Text="Masterclasses - "></asp:Label>
                                    <asp:Label runat="server" ID="Label17" Text="SGD 15 to 35 per class" ForeColor="Red"></asp:Label>*
                                     <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Comment*<asp:Label runat="server" ID="Label11" Text="Workshops - "></asp:Label>
                                    <asp:Label runat="server" ID="Label18" Text="SGD 20 to 45 per class" ForeColor="Red"></asp:Label>*
                                    <br /><br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>--%>
                            </table>
                        </td>
                    </tr>
                    <tr id="trMasterclassClosed" runat="server" visible="false">
                        <td>
                            <table style="width:100%" id="tblClosed" runat="server" visible="false">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Online sign up for Masterclasses has closed.
                                        <br />
                                        Please sign up onsite during event days.
                                        <br />
                                        Sign up for Workshops is still open and will close on 27 October, 2359hrs.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                    <tr>
                        <td style="padding-top:5px;">
                            <asp:LinkButton ID="lnkSignup" runat="server" CssClass="col-md-12 btn btn-default" PostBackUrl="#" Visible="false"
                        Text="Coming Soon (Onsite Signup available on event days) 準備中 (空きがある場合のみ、当日申込み可能)"></asp:LinkButton>
                                <asp:Button ID="btnSignup" runat="server" Text="Sign Up 申込む" 
                                CssClass="col-md-3 btn btn-default" OnClick="btnSignup_Click"/> <%----%>
                        </td>
                    </tr>
                    <%--<tr>
                        <td>(Registration will open in September)</td>
                    </tr>--%>
                </table>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5"><%--col-md-4 according to their request(15-8-2018)--%>
                <h3><u><font style="font-weight:bold;color:#fe440a;">For Trade Visitors only / 業界関係者</font></u></h3>
                <table style="width:100%">
                    <tr>
                        <td>
                            <br />
                            <asp:Label runat="server" ID="Label14" Text="*You are required to present your business card " ForeColor="Red"></asp:Label>
                            <br />
                            <asp:Label runat="server" ID="Label16" Text="at the registration counter" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:Label runat="server" ID="Label15" Text="*お名刺の提出がない場合は、入場できません。" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top:10px;">
                            <b><asp:Label runat="server" ID="txtOT" Text="Dates and Opening Hours  "></asp:Label></b>
                            <b>/ 開催時間 :</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblDT" Text="31 October and 1 November 2019 (Thursday and Friday)"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblTT" Text="10.00am - 5.30pm daily"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><asp:Label runat="server" ID="lblAT" Text="Admission "></asp:Label></b>
                            <b>/ 入場料 :</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblFT" Text="Free admission for pre-registration"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblPT" Text="SGD 20 for on-site registration"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="Label1" Text="事前登録の場合無料"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="Label2" Text="当日登録の場合 SGD 20"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br /><br /><br />
                            <asp:LinkButton ID="lnkTrade" runat="server" CssClass="col-md-12 btn btn-primary" PostBackUrl="#" Visible="true"
                        Text="ONLINE SIGNUP CLOSED (Onsite Signup available on event days) オンライン受付終了(空きがある場合のみ、当日申込み可能)"></asp:LinkButton>
                            <%--CssClass="col-md-offset-1 col-md-8 btn btn-primary" Text="PRE-REGISTRATION CLOSED　(Registration is available onsite at SGD 20/pax) 事前登録 受付終了(会場でのご登録 : SGD 20/1名)"--%>
                            <asp:Button ID="btnTrade" runat="server" Text="Individual Pre-register for FREE admission 業界関係者　個別登録 (無料)" Visible="false"
                                CssClass="col-md-offset-1 col-md-8 btn btn-default" OnClick="btnTrade_Click"/><%--col-md-10 according to their request(15-8-2018)--%>
                            <br /><br /><br />
                            <asp:Button ID="btnTradeGroup" runat="server" Text="Group Pre-register for FREE admission 業界関係者　団体登録 (無料）" Visible="false"
                                CssClass="col-md-offset-1 col-md-8 btn btn-default" OnClick="btnTradeGroup_Click"/><%--col-md-10 according to their request(15-8-2018)--%>
                                <%--PRE-REGISTRATION CLOSED (Registration is available onsite at SGD 20/pax) 事前登録受付終了 (会場でのご登録  SG$20 /1名)--%>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-5"><%--col-md-4 according to their request(15-8-2018)--%>
                <h3><u><font style="font-weight:bold;">Public / 一般</font></u></h3>
                <table style="width:100%">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><asp:Label runat="server" ID="lblOP" Text="Dates and Opening Hours: "></asp:Label></b>
                            <b>/ 開催時間 :</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblDP" Text="2 November 2019 (Saturday)"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblTP" Text="11.00am - 4.30pm"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b><asp:Label runat="server" ID="lblAP" Text="Admission "></asp:Label></b>
                            <b>/ 入場料 :</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblPP" Text="SGD 4 per pax"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblFP" Text="Free entry for children 12 years old and below"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="Label12" Text="(Tickets may be purchased online or on-site)"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="Label3" Text="SGD 4 / 一人"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="Label4" Text="12歳以下の場合は無料"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkPublic" runat="server" CssClass="col-md-8 btn btn-default" PostBackUrl="#" Visible="true"
                        Text="ONLINE TICKETS SALES CLOSED (Tickets are sold onsite at SGD 4 each) オンライン販売終了(当日券販売: SGD 4/1名)"></asp:LinkButton>
                            <%--col-md-9 according to their request(15-8-2018) (Tickets are sold onsite at SGD 4 each)  (当日券販売: SGD 4/1名)   Coming Soon (SGD 4 per person) 準備中 (当日券販売: SGD 4/1名)      Text="ONLINE TICKETS SALES CLOSED オンライン販売終了"--%>
                            <asp:Button ID="btnPublic" runat="server" Text="Purchase Tickets 一般チケット購入" Visible="false"
                                CssClass="col-md-offset-2 col-md-4 btn btn-default" OnClick="btnPublic_Click"/> <%----%><br />
                            
                        </td>
                    </tr>
                    <%--<tr>
                        <td>
                            (Online purchase will start in early October)
                        </td>
                    </tr>--%>
                </table>

            </div>
        </div>
    </form>
</div>
</body>
</html>
