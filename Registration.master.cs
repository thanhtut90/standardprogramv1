﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
using Corpit.BackendMaster;
using System.Data;

public partial class Registration : System.Web.UI.MasterPage
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    private static string[] prepopulateshows = new string[] { "BUM338" };//MFA
    private static string[] prepopulateMMAshows = new string[] { "CEK336" };//MMA

    #region Page_Init [added by th on 24-7-2018]
    protected void Page_Init(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            ShowControler showControler = new ShowControler(fn);
            Show sh = showControler.GetShow(showid);
            if (prepopulateshows.Contains(showid) || prepopulateMMAshows.Contains(showid))
            {
                integrateGoogleTagManager(showid);
            }
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            ShowController shwCtrl = new ShowController(fn);
            if (shwCtrl.checkValidShow(showid))
            {
                SiteSettings sSetting = new SiteSettings(fn, showid);
                sSetting.LoadBaseSiteProperties(showid);
                FlowControler flwObj = new FlowControler(fn, urlQuery);
                FlowMaster fMaster = flwObj.GetFlowMasterConfig(flwObj.FlowID);
                if (Request.Params["t"] != null)
                {
                    string admintype = cFun.DecryptValue(Request.QueryString["t"].ToString());
                    if (admintype == BackendStaticValueClass.isWebSettings)
                    {
                    }
                    else
                    {
                        if (sSetting.isRegisterClosed())
                        {
                            if (fMaster.isOnsite != 1)
                                Response.Redirect("RegClosed.aspx?SHW=" + cFun.EncryptValue(showid));
                        }
                    }
                }
                else
                {
                    if (sSetting.isRegisterClosed())
                    {
                        if (fMaster.isOnsite != 1)
                            Response.Redirect("RegClosed.aspx?SHW=" + cFun.EncryptValue(showid));
                    }
                }

                //if (!IsPostBack)
                {
                    string delegateID = cFun.DecryptValue(urlQuery.DelegateID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    InitSiteSettings(showid, flowid, delegateID);
                    LoadPageSetting();
                }

                if (sSetting.hastimer == "1")
                {
                    timerSession.Enabled = true;
                    if (Session["timeout"] != null)
                    {
                        timerSession_Tick(this, null);
                    }
                    else
                    {
                        mpeExpired.Show();
                    }
                }
                if (!string.IsNullOrEmpty(sSetting.GoogleAnalyticCode))
                    txtGoogleAnalyticCode.Text = sSetting.GoogleAnalyticCode;
                if (!string.IsNullOrEmpty(flwObj.GoogleSnippetID))
                    txtGoogleSnippetID.Text = flwObj.GoogleSnippetID;

                if (!string.IsNullOrEmpty(sSetting.FacebookAnalyticCode)) txtFacebookAnalyticCode.Text = sSetting.FacebookAnalyticCode;
                txtNextStepIndex.Text = flwObj.NextStep;
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid, string flowid, string delegateID)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        SiteBanner.ImageUrl = sSetting.SiteBanner;
       Global.PageTitle = sSetting.SiteTitle; 
        Global.Copyright = sSetting.SiteFooter;
        Global.PageBanner = sSetting.SiteBanner;

        if (!string.IsNullOrEmpty(sSetting.SiteCssBandlePath))
            BundleRefence.Path = "~/Content/" + sSetting.SiteCssBandlePath + "/";

        //Show hide Banner
        ShowControler showControler = new ShowControler(fn);
        Show sh = showControler.GetShow(showid);
        if (sh.HideShowBanner == "1")
        {
            SiteBanner.Visible = false;
            if (prepopulateshows.Contains(showid))
            {
                FlowControler Flw = new FlowControler(fn);
                FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowid);
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                {
                    bool isPrePopulateExist = checkPrePopulateDataExistByColumn("RefNo", delegateID);
                    if (isPrePopulateExist)
                    {
                        SiteBanner.Visible = true;
                    }
                }
            }
        }
    }
    private void LoadPageSetting()
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        SetPageSettting(flwObj.CurrIndexTiTle);
    }

    public void SetPageSettting(string title)
    {
        SiteHeader.Text = title;
    }

    #endregion

    #region Timer
    protected void timerSession_Tick(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            if (Session["timeout"] != null)
            {
                SiteSettings st = new SiteSettings(fn, showid);
                if (st.hastimer_type == TimerType.isSecond)//*(TimerType.isSecond=1)
                {
                    DateTime dttimeout = Convert.ToDateTime(Session["timeout"].ToString());
                    int secondsleft = (int)decimal.Parse(dttimeout.Subtract(DateTime.Now).TotalSeconds.ToString()) + 1;

                    lblTimerSecond.Text = secondsleft.ToString();

                    if (Session["isextended"] == null)
                    {
                        if (secondsleft <= 2 && secondsleft > 1)
                        {
                            Session["isextended"] = "1";
                            mpeSession.Show();
                        }
                    }

                    if (secondsleft <= 0)
                    {
                        Session["timeout"] = null;
                        Session["isextended"] = null;

                        Session.Clear();
                        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri.ToString());
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "@#$", "getSecondsRemaining();", true);
                }
                else//*(TimerType.isMinute=2)
                {
                    #region With Minutes (Comment)
                    DateTime dttimeout = Convert.ToDateTime(Session["timeout"].ToString());
                    int minutesleft = (int)decimal.Parse(dttimeout.Subtract(DateTime.Now).TotalMinutes.ToString()) + 1;

                    lblTimer.Text = minutesleft.ToString();
                    if (Session["isextended"] == null)
                    {
                        if (minutesleft <= 2 && minutesleft > 1)
                        {
                            Session["isextended"] = "1";
                            mpeSession.Show();
                        }
                    }

                    if (minutesleft <= 0)
                    {
                        Session["timeout"] = null;
                        Session["isextended"] = null;

                        Session.Clear();
                        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri.ToString());
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "@#$", "getMinutesRemaining();", true);
                    #endregion
                }
            }
            else
            {
                String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    protected void ButtonPop_Click(object sender, EventArgs e)
    {
        mpeExpired.Show();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        mpeSession.Show();
    }

    protected void btnOkay_Click(object sender, EventArgs e)
    {
        Session["timeout"] = DateTime.Now.AddMinutes(12).ToString();
        timerSession_Tick(this, null);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "@#$", "getMinutesRemaining();", true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect("Welcome.aspx");
    }

    protected void btnStartAgain_Click(object sender, EventArgs e)
    {
        Response.Redirect("Welcome.aspx");
    }

    protected void btnVisit_Click(object sender, EventArgs e)
    {
        String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
        String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
    }
    #endregion


    #region prepopulateData
    public bool checkPrePopulateDataExistByColumn(string columnName, string columnValue)
    {
        bool isExist = false;

        try
        {
            string sql = "Select * From tb_RegPrePopulate Where " + columnName + "='" + columnValue + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];//, pList
            if (dt.Rows.Count > 0)
            {
                isExist = true;
            }
        }
        catch { }

        return isExist;
    }
    #endregion

    #region integrateGoogleTagManager [added by th on 24-7-2018]
    private void integrateGoogleTagManager(string showid)
    {
        if (prepopulateshows.Contains(showid))
        {
            string myScript = "\n<!-- Google Tag Manager --><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'}); \n";
            //myScript += "alert('hi');";
            myScript += "\n var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:''; \n";
            myScript += "\n j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl; \n";
            myScript += "\n f.parentNode.insertBefore(j,f); \n";
            myScript += "\n })(window,document,'script','dataLayer','GTM-N24F3CX'); \n";
            myScript += "\n </script> \n <!-- End Google Tag Manager -->";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, false);
            Page.Header.Controls.Add(new LiteralControl(myScript));

            string bodyScript = string.Empty;
            bodyScript += "<!-- Google Tag Manager (noscript) -->";
            bodyScript += "<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-N24F3CX\" ";
            bodyScript += "height=\"0\" width=\"0\" style=\"display: none; visibility: hidden\"></iframe></noscript>";
            bodyScript += "<!-- End Google Tag Manager (noscript) -->";
            regBody.Controls.AddAt(0, new LiteralControl(bodyScript));
        }
        else if (prepopulateMMAshows.Contains(showid))
        {
            string myScript = "\n<!-- Google Tag Manager --><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'}); \n";
            //myScript += "alert('hi');";
            myScript += "\n var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:''; \n";
            myScript += "\n j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl; \n";
            myScript += "\n f.parentNode.insertBefore(j,f); \n";
            myScript += "\n })(window,document,'script','dataLayer','GTM-NGT9J7W'); \n";
            myScript += "\n </script> \n <!-- End Google Tag Manager -->";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, false);
            Page.Header.Controls.Add(new LiteralControl(myScript));

            string bodyScript = string.Empty;
            bodyScript += "<!-- Google Tag Manager (noscript) -->";
            bodyScript += "<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-NGT9J7W\" ";
            bodyScript += "height=\"0\" width=\"0\" style=\"display: none; visibility: hidden\"></iframe></noscript>";
            bodyScript += "<!-- End Google Tag Manager (noscript) -->";
            regBody.Controls.AddAt(0, new LiteralControl(bodyScript));
        }
    }
    #endregion
}
