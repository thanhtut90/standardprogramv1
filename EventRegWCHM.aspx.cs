﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
public partial class EventRegWCHM : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    private static string WCMHIndivFlow = "WorldCongressReg";
    private static string WCMHGroupFlow = "WorldCongressGroupReg";
    public class tmpDataList
    {
        public string showID { get; set; }
        public string FlowID { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            tmpDataList tmpList = GetShowIDByEventName(WCMHIndivFlow);
            if (string.IsNullOrEmpty(tmpList.showID))
                Response.Redirect("404.aspx");
            else
            {
                InitSiteSettings(tmpList.showID);
            }
        }
    }
    private tmpDataList GetShowIDByEventName(string eventName)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            cList.showID = "";
            cList.FlowID = "";
            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                cList.showID = dList.Rows[0]["showID"].ToString();
                cList.FlowID = dList.Rows[0]["FLW_ID"].ToString();
            }

        }
        catch { }
        return cList;
    }

    protected void btnRegSingapore_Click(object sender, EventArgs e)
    {
        tmpDataList tmpList = GetShowIDByEventName(WCMHIndivFlow);
        if (string.IsNullOrEmpty(tmpList.showID))
            Response.Redirect("404.aspx");
        else
        {
            string page = "DefaultRegIndex";
            string step = "";
            string grpNum = "";
            FlowControler Flw = new FlowControler(fn);
            string url = Flw.MakeFullURL(page, cFun.EncryptValue(tmpList.FlowID), cFun.EncryptValue(tmpList.showID), grpNum, step);
            Response.Redirect(url);
        }
    }

    protected void btnRegNonSingapore_Click(object sender, EventArgs e)
    {
        tmpDataList tmpList = GetShowIDByEventName(WCMHGroupFlow);
        if (string.IsNullOrEmpty(tmpList.showID))
            Response.Redirect("404.aspx");
        else
        {
            string page = "DefaultRegIndex";
            string step = "";
            string grpNum = "";
            FlowControler Flw = new FlowControler(fn);
            string url = Flw.MakeFullURL(page, cFun.EncryptValue(tmpList.FlowID), cFun.EncryptValue(tmpList.showID), grpNum, step);
            Response.Redirect(url);
        }
    }

    private void InitSiteSettings(string showid)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        SiteBanner.ImageUrl = sSetting.SiteBanner;
    }
}