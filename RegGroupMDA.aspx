﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="RegGroupMDA.aspx.cs" Inherits="RegGroupMDA" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="Content/StyleQA.css" />
    <style>
        .text-4 {
            color: #ff0000;
        }

        .space-top20 {
            padding-top: 25px;
            border-top: 1px dashed;
 
        }

        .col-sm-6 {
            padding-top: 7px;
        }

        .form-control {
            height: 35px !important;
        }

        .paddingLeft3px {
            padding-left: 3px;
        }

        .PaddingRight1 {
            padding-right: 1px;
        }

        .PaddingLeft1 {
            padding-left: 3px;
        }

        .textUpperCase {
            text-transform: uppercase;
        }
        
        .chkFooter label
        {
            font-weight:unset !important;
            padding-left:5px !important;
            vertical-align:top !important;
        }
        .chkFooter input[type="checkbox"]
        {
            margin-top: 2px !important;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <script>

                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(function () {
                    /*
                    $('.flex-grid [data-accordion]').accordion();

                    $('.QAPullDown').on('change', function () {
                        QAPulldownOnchange($(this));
                    });

                    $('.QAPullDown').each(function () {
                        QAPulldownOnchange($(this)); // open other thext box when postback
                    });
                    */
                    $('.cssLName').on("keyup", function () {
                        var lName = $(this).val();
                        var fName = $(this).parent().parent().parent().find('.cssFName').val()
                        var fullName = fName + ' ' + lName;
                        var full = $(this).parent().parent().parent().find('.cssOName');
                        full.val(fullName);
                    });
                    $('.cssFName').on("keyup", function () {
                        var fName = $(this).val();
                        var lname = $(this).parent().parent().parent().find('.cssLName').val()
                        var fullName = fName + ' ' + lname;
                        var full = $(this).parent().parent().parent().find('.cssOName');
                        full.val(fullName);
                    });

                    $('.css1LName').on("keyup", function () {
                        var txtVal = $(this).val();
                        var fullName = $('.css1FName').val() + ' ' + txtVal;
                        $('.css1OName').val(fullName);
                    });
                    $('.css1FName').on("keyup", function () {
                        var txtVal = $(this).val();
                        var fullName = txtVal + ' ' + $('.css1LName').val();
                        $('.css1OName').val(fullName);
                    });
                });


                $(document).ready(function () {
                    /*
                     $('.QAPullDown').on('change', function () {
                         //var ddlvalue = $(this).text();                       
                         QAPulldownOnchange($(this));
                     });
                     $('.QAPullDown').each(function () {
                         QAPulldownOnchange($(this));
                     });
                     */
                    $('.cssLName').on("keyup", function () {
                        var lName = $(this).val();
                        var fName = $(this).parent().parent().parent().find('.cssFName').val()
                        var fullName = fName + ' ' + lName;
                        var full = $(this).parent().parent().parent().find('.cssOName');
                        full.val(fullName);
                    });
                    $('.cssFName').on("keyup", function () {
                        var fName = $(this).val();
                        var lname = $(this).parent().parent().parent().find('.cssLName').val()
                        var fullName = fName + ' ' + lname;
                        var full = $(this).parent().parent().parent().find('.cssOName');
                        full.val(fullName);
                    });
                    $('.css1LName').on("keyup", function () {
                        var txtVal = $(this).val();
                        var fullName = $('.css1FName').val() + ' ' + txtVal;
                        $('.css1OName').val(fullName);
                    });
                    $('.css1FName').on("keyup", function () {
                        var txtVal = $(this).val();
                        var fullName = txtVal + ' ' + $('.css1LName').val();
                        $('.css1OName').val(fullName);
                    });
                });

                function QAPulldownOnchange(curreDropdown) {

                    // var sText = $(".QAPullDown option:selected").text();
                    var sText = $(curreDropdown).find("option:selected").text();
                    sText = sText.toLowerCase();
                    var substring = "other";
                    if (sText.includes(substring))
                        $(curreDropdown).closest('td').find(".QAPullDownOtherDiv").show();
                    else
                        $(curreDropdown).closest('td').find(".QAPullDownOtherDiv").hide();
                }
                function RestrictSpace() {
                    if (event.keyCode == 32) {
                        return false;
                    }
                }
            </script>
            <div id="divHDR1" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblHDR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR2" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-11" style="padding-left: 50px;">
                        <asp:Label ID="lblHDR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR3" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblHDR3" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>

            <div class="container " style="padding-top: 30px; padding-left: 6px;">
                      <div class="col-md-5">

                        <p style="font-weight: bold; text-decoration: underline;">Person 1 / Group Co-Ordinator</p>

                    </div>
                <div class="col-md-8">
                  
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div>
                                <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                                <asp:DropDownList ID="txt1Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt1Sal_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0">Please Select</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:CompareValidator ID="vcSal" runat="server"
                                ControlToValidate="txt1Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            <div runat="server" id="divSal1Other" visible="false" style="padding-top: 7px;">
                                <br />
                                <asp:TextBox ID="txt1SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vcSalOther" runat="server"
                                    ControlToValidate="txt1SalOther" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revSalOther" runat="server" ControlToValidate="txt1SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div>
                                <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt1LName" runat="server" CssClass=" form-control input-sm  textUpperCase css1LName" placeholder=""></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server"
                                ControlToValidate="txt1LName" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator26" runat="server" ControlToValidate="txt1LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-group col-md-4">
                            <div>
                                <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt1FName" runat="server" CssClass=" form-control input-sm  textUpperCase css1FName" placeholder=""></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server"
                                ControlToValidate="txt1FName" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator27" runat="server" ControlToValidate="txt1FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-8">
                            <div>
                                <label class=" form-control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt1FullName" runat="server" CssClass=" form-control input-sm  textUpperCase css1OName" placeholder=""></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server"
                                ControlToValidate="txt1FullName" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator28" runat="server" ControlToValidate="txt1FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>

                        <div class="form-group col-md-4">
                            <div>
                                <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt1JobTitle" runat="server" CssClass=" form-control input-sm  " placeholder=""></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ControlToValidate="txt1JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator29" runat="server" ControlToValidate="txt1JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <div>
                                <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt1Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>

                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server"
                                ControlToValidate="txt1Email" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt1Email" ValidationExpression="^\s*.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+\s*$" />
                        </div>
                        <div class="form-group col-md-6">
                            <div class="form-group">
                                <label class="control-label">Website ( เว็บไซท์)</label>
                                <asp:TextBox ID="txtComWebsite" runat="server" CssClass=" form-control input-sm " ></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="padding-left: 20px;">
                                    <label class="control-label">Company Telephone (โทร)<span class="text-4">*</span></label>
                                </div>
                                <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1">
                                    <asp:TextBox ID="txtComTelCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                         <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" TargetControlID="txtComTelCC" FilterType="Numbers" ValidChars="+0123456789" />
                                </div>
                                <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1">
                                    <asp:TextBox ID="txtComTelAC" runat="server" CssClass=" form-control input-sm " placeholder="Area Code"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" TargetControlID="txtComTelAC" FilterType="Numbers" ValidChars="+0123456789" />
                                </div>
                                <div class="form-group col-md-8 col-xs-6 PaddingLeft1 PaddingRight1">
                                    <asp:TextBox ID="txtComTel" runat="server" CssClass=" form-control input-sm " ></asp:TextBox>                                    
                                    <asp:FilteredTextBoxExtender ID="VtxtComTel" runat="server" TargetControlID="txtComTel" FilterType="Numbers" ValidChars="+0123456789" />
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator124" runat="server" ControlToValidate="txtComTel" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                  </div>

                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group">
                                <div class="row" style="padding-left: 20px;">
                                    <label class="control-label">Mobile no (มือถือ)<span class="text-4">*</span></label>
                                </div>
                                <div class="form-group col-md-4 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="divMobCC">
                                    <asp:TextBox ID="txt1MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" TargetControlID="txt1MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                                </div>

                                <div class="form-group col-md-8 col-xs-9 PaddingLeft1 PaddingRight1" runat="server" id="divMob">
                                    <asp:TextBox ID="txt1Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txt1Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator125" runat="server" ControlToValidate="txt1Mobile" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="form-group col-md-4">
                            <label class="control-label">Time/Date of Visit<span class="text-4">*</span></label>
                            <asp:DropDownList ID="ddlVisitDate" runat="server" CssClass=" form-control input-sm">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>

                            <asp:CompareValidator ID="CompareValidator20" runat="server" ControlToValidate="ddlVisitDate" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>

                        </div>
                       <div class="form-group col-md-6">
                        <label class="control-label">Number of Persons (จำนวนผู้เข้าเยี่ยมชมงาน) <span class="text-4">*</span></label>
                        <asp:DropDownList ID="ddlTotalMember" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="ddlTotalMember_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    </div>
                </div>
                <%-- End Fist --%>
                <div class="col-md-4" style="border-left: 1px solid  #cac2c2;">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="form-group">
                                <label class="control-label">Company (บริษัท)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txtCompany" runat="server" CssClass=" form-control input-sm  textUpperCase"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="vctxtCompany" runat="server"
                                ControlToValidate="txtCompany" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revtxtCompany" runat="server" ControlToValidate="txtCompany" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="form-group">
                                <label class="control-label">Company Address line 1 (ที่อยู่)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txtComAdd" runat="server" CssClass=" form-control input-sm "></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server"
                                ControlToValidate="txtComAdd" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator24" runat="server" ControlToValidate="txtComAdd" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s&()+|\{}'#.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="form-group">
                                <label class="control-label">Company Address line 2 (ที่อยู่) </label>
                                <asp:TextBox ID="txtComAdd2" runat="server" CssClass=" form-control input-sm "></asp:TextBox>
                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator64" runat="server" ControlToValidate="txtComAdd2" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s&()+|\{}'#.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 16px;">

                        <div class="form-group col-md-6" id="toggleCityText" style="display: block">
                            <label class="control-label">City (เมือง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txtComCity" runat="server" CssClass=" form-control input-sm "></asp:TextBox>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server"
                                ControlToValidate="txtComCity" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator25" runat="server" ControlToValidate="txtComCity" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>

                        <div class="form-group col-md-6 paddingLeft3px">
                            <label class="control-label">State/Province (จังหวัด)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txtComState" runat="server" CssClass=" form-control input-sm "></asp:TextBox>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator62" runat="server" ControlToValidate="txtComState" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator62" runat="server" ControlToValidate="txtComState" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label">Country (ประเทศ)*<span class="text-4">*</span></label>
                            <asp:DropDownList ID="ddlComCountry" runat="server" CssClass=" form-control input-sm">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>

                            <asp:CompareValidator ID="vcCountry" runat="server"
                                ControlToValidate="ddlComCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                        <div class="form-group col-md-6 paddingLeft3px">
                            <label class="control-label">Zip Code (รหัสไปรษณีย์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txtComPostCode" runat="server" CssClass=" form-control input-sm "></asp:TextBox>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator63" runat="server" ControlToValidate="txtComPostCode" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator63" runat="server" ControlToValidate="txtComPostCode" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                        </div>
                    </div>
                </div>
            </div>
          <div class="container " style="padding-top: 30px; padding-left: 6px;">
                <div class="col-md-8">
                    <div class="row">
                        <div class="form-group col-md-7" runat="server" id="divProfession">
                            <asp:Label ID="lblProfession" runat="server" CssClass="control-label" Text=""
                                Font-Bold="true"></asp:Label><span class="text-4">*</span>
                            <asp:DropDownList runat="server" ID="ddlProfession" AutoPostBack="true" CssClass=" form-control input-sm"
                                OnSelectedIndexChanged="ddlProfession_SelectedIndexChanged">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>


                            <asp:CompareValidator ID="vcProfession" runat="server"
                                ControlToValidate="ddlProfession" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>

                        </div>
                    </div>
                    <div class="row" runat="server" id="divAffiliation">
                        <div class="form-group col-md-7" >
                               <asp:Label ID="lblAffiliation" runat="server" CssClass="form-control-label" Text=""></asp:Label> <span class="text-4">*</span>
                          <asp:DropDownList ID="ddlAffiliation" runat="server" CssClass="form-control input-sm">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                                                    <asp:CompareValidator ID="vcAffil" runat="server"
                            ControlToValidate="ddlAffiliation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                         
                    </div>

                    <div class="row">
                        <div class="form-group col-md-7" runat="server" id="divDepartment">
                            <asp:Label ID="lblDepartment" runat="server" CssClass="control-label" Text=""
                                Font-Bold="true"></asp:Label><span class="text-4">*</span>
                            <asp:DropDownList runat="server" ID="ddlDepartment" AutoPostBack="true" CssClass=" form-control input-sm"
                                OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                <%--<asp:ListItem Value="0">Please Select</asp:ListItem>--%>
                            </asp:DropDownList>

                            <asp:CompareValidator ID="vcDeptm" runat="server"
                                ControlToValidate="ddlDepartment" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                        <div class="form-group col-md-7" runat="server" id="divDepartmentOther" visible="false">

                            <asp:TextBox ID="txtDepartmentOther" runat="server" CssClass=" form-control input-sm"
                                onblur="limitText(this,2);"></asp:TextBox><%--onKeyDown="limitText(this,5);" --%>
                            <asp:RequiredFieldValidator ID="vcDeptmOther" runat="server"
                                ControlToValidate="txtDepartmentOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revDepartmentOther" runat="server" ControlToValidate="txtDepartmentOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                        </div>
                    </div>
                    <div class="row">

                        <div class="form-group col-md-7" runat="server" id="divOrganization">
                            <asp:Label ID="lblOrganization" runat="server" CssClass="control-label" Text=""
                                 Font-Bold="true"></asp:Label><span class="text-4">*</span>
                            <asp:DropDownList ID="ddlOrganization" runat="server" CssClass=" form-control input-sm"
                                OnSelectedIndexChanged="ddlOrganization_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:CompareValidator ID="vcOrg" runat="server"
                                ControlToValidate="ddlOrganization" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>

                        <div class="form-group col-md-7" runat="server" id="divOrgOther" visible="false">
                            <asp:TextBox ID="txtOrgOther" runat="server" CssClass=" form-control input-sm"
                                onblur="limitText(this,2);"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="vcOrgOther" runat="server"
                                ControlToValidate="txtOrgOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revOrgOther" runat="server" ControlToValidate="txtOrgOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                </div>
            </div>


            <div id="divQA" runat="server" class="container" style="padding-top: 20px;padding-left: 20px;">
                <asp:Panel ID="PanelMain" CssClass="flex-grid" runat="server"></asp:Panel>

            </div>
  
            <!-- 1  -->
 
            <!-- End -->
            <!-- 2  -->
            <asp:Panel runat="server" ID="Panel2" Visible="true">
                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 2</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt2Sal" runat="server" CssClass=" form-control input-sm  input-sm" OnSelectedIndexChanged="txt2Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:CompareValidator ID="CompareValidator1" runat="server"
                            ControlToValidate="txt2Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <div runat="server" id="divSal2Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt2SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="txt2SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txt2SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt2LName" runat="server" CssClass=" form-control input-sm  textUpperCase cssLName  input-sm" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server"
                            ControlToValidate="txt2LName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator30" runat="server" ControlToValidate="txt2LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt2FName" runat="server" CssClass=" form-control input-sm  textUpperCase cssFName  input-sm" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server"
                            ControlToValidate="txt2FName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator31" runat="server" ControlToValidate="txt2FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt2FullName" runat="server" CssClass=" form-control input-sm  textUpperCase cssOName  input-sm" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server"
                            ControlToValidate="txt2FullName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator32" runat="server" ControlToValidate="txt2FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt2JobTitle" runat="server" CssClass=" form-control input-sm    input-sm" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ControlToValidate="txt2JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator33" runat="server" ControlToValidate="txt2JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt2Email" runat="server" CssClass=" form-control input-sm input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ControlToValidate="txt2Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator34" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt2Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div1">
                                <asp:TextBox ID="txt2MobileCC" runat="server" CssClass=" form-control input-sm input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" TargetControlID="txt2MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div2">
                                <asp:TextBox ID="txt2Mobile" runat="server" CssClass=" form-control input-sm input-sm" placeholder=""></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txt2Mobile" FilterType="Numbers" ValidChars="+0123456789" />

                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!-- 3  -->
            <asp:Panel runat="server" ID="Panel3" Visible="true">
                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 3</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt3Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt3Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txt3Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>

                        <div runat="server" id="divSal3Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt3SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                ControlToValidate="txt3SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txt3SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt3LName" runat="server" CssClass=" form-control input-sm  textUpperCase cssLName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ControlToValidate="txt3LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator35" runat="server" ControlToValidate="txt3LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt3FName" runat="server" CssClass=" form-control input-sm  textUpperCase cssFName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ControlToValidate="txt3FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator36" runat="server" ControlToValidate="txt3FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt3FullName" runat="server" CssClass=" form-control input-sm  textUpperCase cssOName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ControlToValidate="txt3FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator38" runat="server" ControlToValidate="txt3FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>


                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt3JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ControlToValidate="txt3JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator37" runat="server" ControlToValidate="txt3JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt3Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ControlToValidate="txt3Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt3Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div3">
                                <asp:TextBox ID="txt3MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" TargetControlID="txt3MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div4">
                                <asp:TextBox ID="txt3Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txt3Mobile" FilterType="Numbers" ValidChars="+0123456789" />

                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!-- 4  -->
            <asp:Panel runat="server" ID="Panel4" Visible="true">
                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 4</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt4Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt4Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txt4Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>

                        <div runat="server" id="divSal4Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt4SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                ControlToValidate="txt4SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txt4SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt4LName" runat="server" CssClass=" form-control input-sm  textUpperCase cssLName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ControlToValidate="txt4LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator39" runat="server" ControlToValidate="txt4LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt4FName" runat="server" CssClass=" form-control input-sm  textUpperCase cssFName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ControlToValidate="txt4FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator40" runat="server" ControlToValidate="txt4FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt4FullName" runat="server" CssClass=" form-control input-sm  textUpperCase cssOName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator39" runat="server" ControlToValidate="txt4FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator41" runat="server" ControlToValidate="txt4FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt4JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator40" runat="server" ControlToValidate="txt4JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator42" runat="server" ControlToValidate="txt4JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>


                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt4Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator41" runat="server" ControlToValidate="txt4Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt4Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div5">
                                <asp:TextBox ID="txt4MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" TargetControlID="txt4MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div6">
                                <asp:TextBox ID="txt4Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txt4Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!-- 5  -->
            <asp:Panel runat="server" ID="Panel5" Visible="true">
                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 5</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt5Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt5Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txt5Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>


                        <div runat="server" id="divSal5Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt5SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                ControlToValidate="txt5SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txt5SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt5LName" runat="server" CssClass=" form-control input-sm  textUpperCase cssLName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator44" runat="server" ControlToValidate="txt5LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator45" runat="server" ControlToValidate="txt5LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt5FName" runat="server" CssClass=" form-control input-sm  textUpperCase cssFName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator43" runat="server" ControlToValidate="txt5FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator44" runat="server" ControlToValidate="txt5FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt5FullName" runat="server" CssClass=" form-control input-sm  textUpperCase cssOName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator42" runat="server" ControlToValidate="txt5FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator43" runat="server" ControlToValidate="txt5FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt5JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator45" runat="server" ControlToValidate="txt5JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator46" runat="server" ControlToValidate="txt5JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt5Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator46" runat="server" ControlToValidate="txt5Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt5Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div7">
                                <asp:TextBox ID="txt5MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" TargetControlID="txt5MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div8">
                                <asp:TextBox ID="txt5Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txt5Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--6 -->
            <asp:Panel runat="server" ID="Panel6" Visible="false">


                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 6</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt6Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt6Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="txt6Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>

                        <div runat="server" id="divSal6Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt6SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                                ControlToValidate="txt6SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txt6SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt6LName" runat="server" CssClass=" form-control input-sm  textUpperCase cssLName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator47" runat="server" ControlToValidate="txt6LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator47" runat="server" ControlToValidate="txt6LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt6FName" runat="server" CssClass=" form-control input-sm  textUpperCase cssFName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator48" runat="server" ControlToValidate="txt6FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator48" runat="server" ControlToValidate="txt6FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt6FullName" runat="server" CssClass=" form-control input-sm  textUpperCase cssOName" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator49" runat="server" ControlToValidate="txt6FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator49" runat="server" ControlToValidate="txt6FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>


                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt6JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator50" runat="server" ControlToValidate="txt6JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator50" runat="server" ControlToValidate="txt6JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>


                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt6Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator51" runat="server" ControlToValidate="txt6Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator51" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt6Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div9">
                                <asp:TextBox ID="txt6MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" TargetControlID="txt6MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div10">
                                <asp:TextBox ID="txt6Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txt6Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--7 -->
            <asp:Panel runat="server" ID="Panel7" Visible="false">


                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 7</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt7Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt7Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txt7Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>


                        <div runat="server" id="divSal7Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt7SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                                ControlToValidate="txt7SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txt7SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt7LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator52" runat="server" ControlToValidate="txt7LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator52" runat="server" ControlToValidate="txt7LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt7FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator53" runat="server" ControlToValidate="txt7FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator53" runat="server" ControlToValidate="txt7FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt7FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator54" runat="server" ControlToValidate="txt7FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator54" runat="server" ControlToValidate="txt7FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt7JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator55" runat="server" ControlToValidate="txt7JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator55" runat="server" ControlToValidate="txt7JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt7Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator56" runat="server" ControlToValidate="txt7Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator56" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt7Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div11">
                                <asp:TextBox ID="txt7MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" TargetControlID="txt7MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div12">
                                <asp:TextBox ID="txt7Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txt7Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--8 -->
            <asp:Panel runat="server" ID="Panel8" Visible="false">
                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 8</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt8Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt8Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txt8Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>

                        <div runat="server" id="divSal8Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt8SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                                ControlToValidate="txt8SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ControlToValidate="txt8SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt8LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator57" runat="server" ControlToValidate="txt8LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator57" runat="server" ControlToValidate="txt8LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt8FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator58" runat="server" ControlToValidate="txt8FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator58" runat="server" ControlToValidate="txt8FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>


                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt8FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ControlToValidate="txt8FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator59" runat="server" ControlToValidate="txt8FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>


                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt8JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator60" runat="server" ControlToValidate="txt8JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator60" runat="server" ControlToValidate="txt8JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt8Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator61" runat="server" ControlToValidate="txt8Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator61" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt8Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div13">
                                <asp:TextBox ID="txt8MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" TargetControlID="txt8MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div14">
                                <asp:TextBox ID="txt8Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txt8Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--9 -->
            <asp:Panel runat="server" ID="Panel9" Visible="false">
                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 9</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt9Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt9Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:CompareValidator ID="CompareValidator8" runat="server" ControlToValidate="txt9Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>


                        <div runat="server" id="divSal9Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt9SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                                ControlToValidate="txt9SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="txt9SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt9LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator76" runat="server" ControlToValidate="txt9LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator77" runat="server" ControlToValidate="txt9LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt9FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator77" runat="server" ControlToValidate="txt9FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator78" runat="server" ControlToValidate="txt9FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt9FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator78" runat="server" ControlToValidate="txt9FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator79" runat="server" ControlToValidate="txt9FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt9JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator79" runat="server" ControlToValidate="txt9JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator80" runat="server" ControlToValidate="txt9JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt9Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator64" runat="server" ControlToValidate="txt9Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator65" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt9Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div15">
                                <asp:TextBox ID="txt9MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" TargetControlID="txt9MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div16">
                                <asp:TextBox ID="txt9Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txt9Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--10 -->
            <asp:Panel runat="server" ID="Panel10" Visible="false">
                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 10</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt10Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt10Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                         <asp:CompareValidator ID="CompareValidator9" runat="server" ControlToValidate="txt10Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>


                        <div runat="server" id="divSal10Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt10SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server"
                                ControlToValidate="txt10SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ControlToValidate="txt10SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt10LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator80" runat="server" ControlToValidate="txt10LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator81" runat="server" ControlToValidate="txt10LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt10FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator81" runat="server" ControlToValidate="txt10FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator82" runat="server" ControlToValidate="txt10FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt10FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator82" runat="server" ControlToValidate="txt10FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator83" runat="server" ControlToValidate="txt10FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt10JobTitle" runat="server" CssClass=" form-control input-sm  " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator83" runat="server" ControlToValidate="txt10JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator84" runat="server" ControlToValidate="txt10JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt10Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator65" runat="server" ControlToValidate="txt10Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator66" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt10Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div17">
                                <asp:TextBox ID="txt10MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" TargetControlID="txt10MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div18">
                                <asp:TextBox ID="txt10Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txt10Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--11 -->
            <asp:Panel runat="server" ID="Panel11" Visible="false">


                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 11</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt11Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt11Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:CompareValidator ID="CompareValidator10" runat="server" ControlToValidate="txt11Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <div runat="server" id="divSal11Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt11SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server"
                                ControlToValidate="txt11SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" ControlToValidate="txt11SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt11LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator84" runat="server" ControlToValidate="txt11LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator85" runat="server" ControlToValidate="txt11LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt11FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator85" runat="server" ControlToValidate="txt11FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator86" runat="server" ControlToValidate="txt11FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt11FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator86" runat="server" ControlToValidate="txt11FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator87" runat="server" ControlToValidate="txt11FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt11JobTitle" runat="server" CssClass=" form-control input-sm  " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator87" runat="server" ControlToValidate="txt11JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator88" runat="server" ControlToValidate="txt11JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt11Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator66" runat="server" ControlToValidate="txt11Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator67" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt11Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div19">
                                <asp:TextBox ID="txt11MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="txt11MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div20">
                                <asp:TextBox ID="txt11Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txt11Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--12 -->
            <asp:Panel runat="server" ID="Panel12" Visible="false">


                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 12</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt12Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt12Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
<asp:CompareValidator ID="CompareValidator11" runat="server" ControlToValidate="txt12Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <div runat="server" id="divSal12Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt12SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server"
                                ControlToValidate="txt12SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" ControlToValidate="txt12SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt12LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator88" runat="server" ControlToValidate="txt12LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator89" runat="server" ControlToValidate="txt12LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt12FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator89" runat="server" ControlToValidate="txt12FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator90" runat="server" ControlToValidate="txt12FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt12FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator90" runat="server" ControlToValidate="txt12FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator91" runat="server" ControlToValidate="txt12FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt12JobTitle" runat="server" CssClass=" form-control input-sm  " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator91" runat="server" ControlToValidate="txt12JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator92" runat="server" ControlToValidate="txt12JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt12Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator67" runat="server" ControlToValidate="txt12Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator68" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt12Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div21">
                                <asp:TextBox ID="txt12MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="txt12MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div22">
                                <asp:TextBox ID="txt12Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txt12Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--13 -->
            <asp:Panel runat="server" ID="Panel13" Visible="false">


                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 13</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt13Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt13Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
<asp:CompareValidator ID="CompareValidator12" runat="server" ControlToValidate="txt13Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <div runat="server" id="divSal13Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt13SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server"
                                ControlToValidate="txt13SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server" ControlToValidate="txt13SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt13LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator92" runat="server" ControlToValidate="txt13LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator93" runat="server" ControlToValidate="txt13LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt13FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator93" runat="server" ControlToValidate="txt13FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator94" runat="server" ControlToValidate="txt13FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt13FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator94" runat="server" ControlToValidate="txt13FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator95" runat="server" ControlToValidate="txt13FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt13JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator95" runat="server" ControlToValidate="txt13JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator96" runat="server" ControlToValidate="txt13JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt13Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator68" runat="server" ControlToValidate="txt13Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator69" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt13Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div23">
                                <asp:TextBox ID="txt13MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" TargetControlID="txt13MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div24">
                                <asp:TextBox ID="txt13Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txt13Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--14 -->
            <asp:Panel runat="server" ID="Panel14" Visible="false">


                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 14</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt14Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt14Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
<asp:CompareValidator ID="CompareValidator13" runat="server" ControlToValidate="txt14Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <div runat="server" id="divSal14Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt14SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server"
                                ControlToValidate="txt14SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server" ControlToValidate="txt14SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt14LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator96" runat="server" ControlToValidate="txt14LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator97" runat="server" ControlToValidate="txt14LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt14FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator97" runat="server" ControlToValidate="txt14FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator98" runat="server" ControlToValidate="txt14FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt14FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator98" runat="server" ControlToValidate="txt14FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator99" runat="server" ControlToValidate="txt14FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt14JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator99" runat="server" ControlToValidate="txt14JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator100" runat="server" ControlToValidate="txt14JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt14Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator69" runat="server" ControlToValidate="txt14Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator70" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt14Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div25">
                                <asp:TextBox ID="txt14MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" TargetControlID="txt14MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div26">
                                <asp:TextBox ID="txt14Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                 <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="txt14Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--15 -->
            <asp:Panel runat="server" ID="Panel15" Visible="false">


                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 15</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt15Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt15Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:CompareValidator ID="CompareValidator14" runat="server" ControlToValidate="txt15Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <div runat="server" id="divSal15Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt15SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server"
                                ControlToValidate="txt15SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server" ControlToValidate="txt15SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt15LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator100" runat="server" ControlToValidate="txt15LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator101" runat="server" ControlToValidate="txt15LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt15FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator101" runat="server" ControlToValidate="txt15FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator102" runat="server" ControlToValidate="txt15FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt15FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator102" runat="server" ControlToValidate="txt15FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator103" runat="server" ControlToValidate="txt15FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt15JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator103" runat="server" ControlToValidate="txt15JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator104" runat="server" ControlToValidate="txt15JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt15Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator70" runat="server" ControlToValidate="txt15Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator71" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt15Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div27">
                                <asp:TextBox ID="txt15MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" TargetControlID="txt15MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div28">
                                <asp:TextBox ID="txt15Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="txt15Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--16 -->
            <asp:Panel runat="server" ID="Panel16" Visible="false">

                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 16</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt16Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt16Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:CompareValidator ID="CompareValidator15" runat="server" ControlToValidate="txt16Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <div runat="server" id="divSal16Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt16SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server"
                                ControlToValidate="txt16SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server" ControlToValidate="txt16SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt16LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator104" runat="server" ControlToValidate="txt16LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator105" runat="server" ControlToValidate="txt16LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt16FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator105" runat="server" ControlToValidate="txt16FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator106" runat="server" ControlToValidate="txt16FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt16FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator106" runat="server" ControlToValidate="txt16FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator107" runat="server" ControlToValidate="txt16FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt16JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator107" runat="server" ControlToValidate="txt16JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator108" runat="server" ControlToValidate="txt16JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt16Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator71" runat="server" ControlToValidate="txt16Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator72" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt16Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div29">
                                <asp:TextBox ID="txt16MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" TargetControlID="txt16MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div30">
                                <asp:TextBox ID="txt16Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="txt16Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--17 -->
            <asp:Panel runat="server" ID="Panel17" Visible="false">

                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 17</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt17Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt17Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
<asp:CompareValidator ID="CompareValidator16" runat="server" ControlToValidate="txt17Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <div runat="server" id="divSal17Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt17SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server"
                                ControlToValidate="txt17SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server" ControlToValidate="txt17SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt17LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator108" runat="server" ControlToValidate="txt17LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator109" runat="server" ControlToValidate="txt17LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt17FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator109" runat="server" ControlToValidate="txt17FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator110" runat="server" ControlToValidate="txt17FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt17FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator110" runat="server" ControlToValidate="txt17FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator111" runat="server" ControlToValidate="txt17FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt17JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator111" runat="server" ControlToValidate="txt17JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator112" runat="server" ControlToValidate="txt17JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt17Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator72" runat="server" ControlToValidate="txt17Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator73" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt17Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div31">
                                <asp:TextBox ID="txt17MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" TargetControlID="txt17MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div32">
                                <asp:TextBox ID="txt17Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="txt17Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--18 -->
            <asp:Panel runat="server" ID="Panel18" Visible="false">

                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 18</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt18Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt18Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
<asp:CompareValidator ID="CompareValidator17" runat="server" ControlToValidate="txt18Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <div runat="server" id="divSal18Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt18SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server"
                                ControlToValidate="txt18SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server" ControlToValidate="txt18SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt18LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator112" runat="server" ControlToValidate="txt18LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator113" runat="server" ControlToValidate="txt18LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt18FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator113" runat="server" ControlToValidate="txt18FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator114" runat="server" ControlToValidate="txt18FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt18FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator114" runat="server" ControlToValidate="txt18FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator115" runat="server" ControlToValidate="txt18FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt18JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator115" runat="server" ControlToValidate="txt18JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator116" runat="server" ControlToValidate="txt18JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt18Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator73" runat="server" ControlToValidate="txt18Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator74" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt18Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div33">
                                <asp:TextBox ID="txt18MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" TargetControlID="txt18MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div34">
                                <asp:TextBox ID="txt18Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="txt18Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--19 -->
            <asp:Panel runat="server" ID="Panel19" Visible="false">

                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 19</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt19Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt19Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
<asp:CompareValidator ID="CompareValidator18" runat="server" ControlToValidate="txt19Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <div runat="server" id="divSal19Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt19SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server"
                                ControlToValidate="txt19SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server" ControlToValidate="txt19SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt19LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator116" runat="server" ControlToValidate="txt19LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator117" runat="server" ControlToValidate="txt19LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt19FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator117" runat="server" ControlToValidate="txt19FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator118" runat="server" ControlToValidate="txt19FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt19FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator118" runat="server" ControlToValidate="txt19FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator119" runat="server" ControlToValidate="txt19FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt19JobTitle" runat="server" CssClass=" form-control input-sm  " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator119" runat="server" ControlToValidate="txt19JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator120" runat="server" ControlToValidate="txt19JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label> 
                            <asp:TextBox ID="txt19Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator74" runat="server" ControlToValidate="txt19Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator75" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt19Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div35">
                                <asp:TextBox ID="txt19MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server" TargetControlID="txt19MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div36">
                                <asp:TextBox ID="txt19Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="txt19Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
            <!--20 -->
            <asp:Panel runat="server" ID="Panel20" Visible="false">

                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12">

                        <p style="font-weight: bold; text-decoration: underline;">Person 20</p>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Title (คำนำหน้าชื่อ)<span class="text-4">*</span></label>
                            <asp:DropDownList ID="txt20Sal" runat="server" CssClass=" form-control input-sm" OnSelectedIndexChanged="txt20Sal_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
<asp:CompareValidator ID="CompareValidator19" runat="server" ControlToValidate="txt20Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        <div runat="server" id="divSal20Other" visible="false" style="padding-top: 7px;">
                            <br />
                            <asp:TextBox ID="txt20SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server"
                                ControlToValidate="txt20SalOther" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server" ControlToValidate="txt20SalOther" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)"
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Surname (นามสกุล)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt20LName" runat="server" CssClass=" form-control input-sm cssLName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator120" runat="server" ControlToValidate="txt20LName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator121" runat="server" ControlToValidate="txt20LName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">First Name (ชื่อ)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt20FName" runat="server" CssClass=" form-control input-sm cssFName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator121" runat="server" ControlToValidate="txt20FName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator122" runat="server" ControlToValidate="txt20FName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label class="control-label">Full Name to be printed on badge (ชื่อเต็มที่จะให้พิมพ์ลงบนบัตรเข้าชมงาน)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt20FullName" runat="server" CssClass=" form-control input-sm cssOName textUpperCase" placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator122" runat="server" ControlToValidate="txt20FullName" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator123" runat="server" ControlToValidate="txt20FullName" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="col-sm-12 col-md-2">
                        <div class="form-group">
                            <label class="control-label">Job Title (ตำแหน่ง)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt20JobTitle" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator123" runat="server" ControlToValidate="txt20JobTitle" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator124" runat="server" ControlToValidate="txt20JobTitle" ErrorMessage="Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)" ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>

                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Email (อีเมล์)<span class="text-4">*</span></label>
                            <asp:TextBox ID="txt20Email" runat="server" CssClass=" form-control input-sm " placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator75" runat="server" ControlToValidate="txt20Email" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator76" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt20Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-12 col-md-4">
                        <div class="form-group">
                            <div class="row" style="padding-left: 20px;">
                                <label class="control-label">Mobile no (มือถือ)</label>
                            </div>
                            <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div37">
                                <asp:TextBox ID="txt20MobileCC" runat="server" CssClass=" form-control input-sm " placeholder="Country"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender42" runat="server" TargetControlID="txt20MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                            <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div38">
                                <asp:TextBox ID="txt20Mobile" runat="server" CssClass=" form-control input-sm " placeholder=""></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" TargetControlID="txt20Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                            </div>

                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!-- End -->
 
            <div class="container ">
                <div class="col-md-12">
                    <div class="row" id="divFTR1" runat="server" visible="false">
                        <div class="form-group row">
                            <div class="clear"></div>
                            <div class="col-md-12">
                                <asp:Label ID="lblFTR1" runat="server"></asp:Label>
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="row" id="divFTRCHK" runat="server" visible="false">
                        <div class="form-group row">
                            <div class="clear"></div>
                            <div class="col-md-12" style="padding-left: 30px;">
                                <asp:CheckBoxList ID="chkFTRCHK" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                                <%--<asp:CheckBox ID="chkFTRCHK" runat="server" />&nbsp;&nbsp;<asp:Label ID="lblFTRCHK" runat="server"></asp:Label>
                            <asp:Label ID="lblFTRCHKIsSkip" runat="server" Visible="false" Text="0"></asp:Label>--%>
                                <asp:Label ID="lblErrFTRCHK" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="divFTR2" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10">
                        <asp:Label ID="lblFTR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>

                    </ContentTemplate>
       
    </asp:UpdatePanel>
    
    <div class="form-group container" style="padding-top: 30px; padding-bottom: 20px;">
        <asp:Panel runat="server" ID="PanelWithoutBack" Visible="true">
            <div class="col-lg-offset-4 col-lg-3 col-sm-offset-4 col-sm-3 center-block">
                <asp:Button runat="server" ID="btnSave" CssClass="btn MainButton btn-block" Text="Submit"   OnClick="btnSave_Click" />
            </div>
        </asp:Panel>
    </div>
     <asp:Panel runat="server" ID="Panel1" Visible="false">
        </asp:Panel>
</asp:Content>

