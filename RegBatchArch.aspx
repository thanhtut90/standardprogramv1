﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="RegBatchArch.aspx.cs" Inherits="RegGroupARC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .red {
            color: red;
        }
    </style>

    <script type="text/javascript">
        function validateDate(elementRef) {
            var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

            var dateValue = elementRef.value;

            if (dateValue.length != 10) {
                alert('Entry must be in the format dd/mm/yyyy.');

                return false;
            }

            // mm/dd/yyyy format... 
            var valueArray = dateValue.split('/');

            if (valueArray.length != 3) {
                alert('Entry must be in the format dd/mm/yyyy.');

                return false;
            }

            var monthValue = parseFloat(valueArray[1]);
            var dayValue = parseFloat(valueArray[0]);
            var yearValue = parseFloat(valueArray[2]);

            if ((isNaN(monthValue) == true) || (isNaN(dayValue) == true) || (isNaN(yearValue) == true)) {
                alert('Non-numeric entry detected\nEntry must be in the format dd/mm/yyyy.');

                return false;
            }

            if (((yearValue % 4) == 0) && (((yearValue % 100) != 0) || ((yearValue % 400) == 0)))
                monthDays[1] = 29;
            else
                monthDays[1] = 28;

            if ((monthValue < 1) || (monthValue > 12)) {
                alert('Invalid month entered\nEntry must be in the format dd/mm/yyyy.');

                return false;
            }

            var monthDaysArrayIndex = monthValue - 1;
            if ((dayValue < 1) || (dayValue > monthDays[monthDaysArrayIndex])) {
                alert('Invalid day entered\nEntry must be in the format dd/mm/yyyy.');

                return false;
            }

            return true;
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-group row">
                <div class="col-md-12">
                    <h4>
                        <asp:Label runat="server" ID="lblHeader" Text="To register your colleagues, please complete the form below. Otherwise please click submit."></asp:Label></h4>
                    <br />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2 col-md-offset-1">
                    <asp:Label ID="lblSalutation" runat="server" Text="Salutation" Font-Bold="true"></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:DropDownList runat="server" ID="ddlSalutations" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlSalutations_SelectedIndexChanged">
                        <asp:ListItem Text="Select.." Value="0" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:CompareValidator ID="vcSalutation" ControlToValidate="ddlSalutations" runat="server" ValueToCompare="0" Operator="NotEqual" Type="Integer"
                        ErrorMessage="* Required" ForeColor="Red"></asp:CompareValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divOtherSal" visible="false">
                <div class="col-md-2 col-md-offset-1">
                    &nbsp;
                </div>
                <div class="col-md-5">
                        <asp:TextBox ID="txtOtherSal" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcOtherSal" ControlToValidate="txtOtherSal" runat="server"
                        ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2 col-md-offset-1">
                    <asp:Label ID="lblFName" runat="server" Text="First Name" Font-Bold="true"></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcFName" ControlToValidate="txtFName" runat="server"
                        ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2 col-md-offset-1">
                    <asp:Label ID="lblLName" runat="server" Text="Last Name" Font-Bold="true"></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtLName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcLName" ControlToValidate="txtLName" runat="server"
                        ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2 col-md-offset-1">
                    <asp:Label ID="lblDesignation" runat="server" Text="Job Title" Font-Bold="true"></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:DropDownList runat="server" ID="ddlDesignation" CssClass="form-control" OnSelectedIndexChanged="ddlDesignation_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Text="Select.." Value="0" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:CompareValidator ID="vcDesig" ControlToValidate="ddlDesignation" ValueToCompare="0" Operator="NotEqual" Type="Integer" runat="server" ErrorMessage="* Required" ForeColor="Red"></asp:CompareValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divOtherDeg" visible="false">
                <div class="col-md-2 col-md-offset-1">
                    &nbsp;
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtOtherDeg" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcOtherDesig" ControlToValidate="txtOtherDeg" runat="server"
                        ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2 col-md-offset-1">
                    <asp:Label ID="lblEmail" runat="server" Text="Email" Font-Bold="true"></asp:Label>
                </div>
                <div class="col-md-5">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3">
                    <asp:RequiredFieldValidator ID="vcEmail" ControlToValidate="txtEmail" runat="server" Display="Dynamic"
                        ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="validateEmail"
                        runat="server" ErrorMessage="Invalid Email" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="txtEmail"
                        ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2 col-md-offset-1">
                    <asp:Label ID="lblMobile" runat="server" Text="Mobile" Font-Bold="true"></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:FilteredTextBoxExtender ID="ftbMobile" runat="server"
                        TargetControlID="txtMobile"
                        FilterType="Numbers"
                        ValidChars="+0123456789" />
                    <asp:RequiredFieldValidator ID="vcMobile" ControlToValidate="txtMobile" runat="server"
                        ErrorMessage="* Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div runat="server" id="divEditBtns" visible="false">
                        <div class="col-md-6">
                            <asp:Button ID="btnUpdate" runat="server" Text="Update Member" class="btn btn-block MainButton" OnClick="btnUpdate_Click" CausesValidation="true" ValidateRequestMode="Enabled" Style="margin-top: 5px; margin-bottom: 5px;" />
                        </div>
                        <div class="col-md-6">
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-block MainButton" OnClick="btnCancel_Click" CausesValidation="false" ValidateRequestMode="Disabled" Style="margin-top: 5px; margin-bottom: 5px;" />
                        </div>
                    </div>
                    <asp:Button ID="btnSave" runat="server" Text="Add Member" class="btn btn-block MainButton" OnClick="btnSave_Click" CausesValidation="true" ValidateRequestMode="Enabled" Style="margin-top: 5px; margin-bottom: 5px;" />
                </div>
                <div class="col-md-6">
                    <asp:Button ID="btnNext" runat="server" Text="Submit" class="btn btn-block MainButton" OnClick="btnNext_Click" CausesValidation="false" ValidateRequestMode="Disabled" Style="margin-top: 5px; margin-bottom: 5px;" />
                </div>
            </div>

            <asp:Label ID="lblShowid" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="lblCurrentRegNo" runat="server" Visible="false"></asp:Label>
            <br />
            <br />

            <div>
                <h3 runat="server" id="lblGrpMemHeader">Group Members</h3>
                <asp:Panel ID="pnllist" CssClass="table-responsive" runat="server" Visible="true">
                    <asp:GridView ID="gvList" runat="server" DataKeyNames="regNo"
                        OnDataBound="gvList_DataBound"
                        OnRowEditing="GridView1_RowEditing"
                        OnRowUpdating="GridView1_RowUpdating"
                        OnRowCancelingEdit="GridView1_RowCancelingEdit" AutoGenerateColumns="false" GridLines="None"
                        CssClass="table">
                        <HeaderStyle CssClass="theadstyle" />
                        <Columns>
                            <asp:TemplateField HeaderText="Salutation">
                                <ItemTemplate>
                                    <asp:Label ID="lblmSalutation" runat="server" Text='<%# Eval("Sal_Name") %>'></asp:Label>
                                    <asp:Label ID="lblmOtherSal" runat="server" Text='<%# Eval("reg_SalutationOthers") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblmFName" runat="server" Text='<%# Eval("reg_FName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblmLName" runat="server" Text='<%# Eval("reg_LName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Job Title">
                                <ItemTemplate>
                                    <asp:Label ID="lblmDesignation" runat="server" Text='<%# Eval("reg_Designation") %>'></asp:Label>
                                    <asp:Label ID="lblmOtherDesig" runat="server" Text='<%# Eval("reg_OtherProfession") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <asp:Label ID="lblmEmail" runat="server" Text='<%# Eval("reg_Email") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mobile">
                                <ItemTemplate>
                                    <asp:Label ID="lblmMobile" runat="server" Text='<%# Eval("reg_Mobile") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ShowEditButton="True" CausesValidation="false" />
                            <%--  <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text='<%# Eval("editText") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="50px" Visible="false">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" OnClientClick="return confirm ('Are you sure you want to delete this record?')" Text="Delete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div style="color: Red">
                        <asp:Label ID="lblMsg" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

