﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventRegTube.aspx.cs" Inherits="EventRegTube" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <link href="Content/Default/bootstrap.css" rel="stylesheet" />
    <link href="Content/Green/Site.css" rel="stylesheet" />

    <!-- Global site tag (gtag.js) - Google Ads: 759766638 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-759766638"></script>
    <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-759766638'); </script>
    <script>
        function gtag_report_conversion() {
            $(document).ready(function () {
                gtag('event', 'conversion', { 'send_to': 'AW-759766638/BeFmCOLQjZYBEO68pOoC' });
                return false;
            });
        }
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '374405366713016'); 
    fbq('track', 'PageView');


    </script>
    <noscript>
    <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=374405366713016&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

</head>
<body>
    <form id="form1" runat="server">
      <div class="container">
            <div class="row">
                <asp:Image ID="SiteBanner" runat="server" CssClass="img-responsive"  Width="100%" />
            </div>
            <br />
        </div>
        <div class="container" style="padding-top: 40px;">
            <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4" style="padding-bottom:20px;">
                <asp:Button runat="server" ID="btnIndividual" class="btn MainButton btn-lg btn-block" Text="Register here" OnClick="btnIndiv_Onclick"
                     OnClientClick="gtag_report_conversion();" />
            </div>
            <%--<div class="col-md-5 col-sm-5">
                <asp:Button runat="server" ID="Button1" class="btn MainButton  btn-lg btn-block" Text="Group Pre-Registration" OnClick="btnGroup_Onclick" />
                <br />
                <p style="font-weight: bold; text-align: left;padding-left:25px;"><br/>
                </p>
            </div>--%>
        </div>
    </form>
</body>
</html>
