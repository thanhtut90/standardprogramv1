﻿using Corpit.Abstract;
using Corpit.Logging;
using Corpit.Promo;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AbstractConferenceIndex : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    static string ASCAShowName = "ASCA 2019 Online Conference Registration";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.Params["event"] != null && Request.Params["r"] != null && Request.Params["e"] != null)
            {
                tmpDataList tmpList = GetShowIDByEventName(Request.QueryString["event"].ToString());
                if (string.IsNullOrEmpty(tmpList.showID))
                {
                    Response.Write("<script>alert('Invalid.');window.location='404.aspx';</script>");
                }
                else
                {
                    string showid = tmpList.showID;
                    string flowid = tmpList.FlowID;
                    string regno = cFun.DecryptValue(Request.QueryString["r"].ToString());
                    string emailaddresss = cFun.DecryptValue(Request.QueryString["e"].ToString());
                    string username = regno;
                    string groupid = "";
                    RegDelegateObj rdObj = new RegDelegateObj(fn);
                    InvoiceControler invControler = new InvoiceControler(fn);
                    StatusSettings statusSet = new StatusSettings(fn);
                    FlowControler flwControl = new FlowControler(fn);
                    FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
                    string flowname = flwMasterConfig.FlowName;
                    DataTable dt = rdObj.getDelegateReLoginAuthenData(username, emailaddresss, flowid);
                    if (dt.Rows.Count > 0)
                    {
                        groupid = dt.Rows[0]["RegGroupID"].ToString();
                        regno = dt.Rows[0]["Regno"].ToString();
                        Session["Regno"] = regno;
                        Session["Groupid"] = groupid;
                        Session["Flowid"] = flowid;
                        Session["Showid"] = showid;
                        string regstage = dt.Rows[0]["reg_Stage"].ToString();
                        Session["regstage"] = regstage;
                        string regstatus = dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                        Session["RegStatus"] = regstatus;

                        LoginLog lg = new LoginLog(fn);
                        string userip = getUserIP();
                        lg.loginid = groupid;
                        lg.loginip = userip;
                        lg.logintype = "Registration Login";
                        lg.saveLoginLog();

                        string invStatus = invControler.getInvoiceStatus(regno);

                        DataTable dtAbsSetting = getAbstractSetting(flowid, showid);
                        if (dtAbsSetting.Rows.Count > 0)
                        {
                            if (invStatus == statusSet.Pending.ToString() || invStatus == RegClass.noInvoice
                                || invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString())
                            {
                                string absShowID = dtAbsSetting.Rows[0]["AbsShowID"].ToString();
                                string checkAbsSubmittedUrl = dtAbsSetting.Rows[0]["AbsSubmittedCheckApiLink"].ToString();
                                string newUrlQuery = "FLW=" + cFun.EncryptValue(flowid) + "&STP=" + cFun.EncryptValue(regstage)
                                + "&GRP=" + cFun.EncryptValue(groupid) + "&INV=" + cFun.EncryptValue(regno) + "&SHW=" + cFun.EncryptValue(showid);
                                FlowURLQuery urlQuery = new FlowURLQuery(newUrlQuery);

                                string promocode = getUsedPromoCode(groupid, flowid, showid, urlQuery);
                                int con_categoryID = 0;
                                string abspromocode = getPromoCodeAccordingAbsRules(regno, showid, flowid, absShowID, checkAbsSubmittedUrl);
                                if (!string.IsNullOrEmpty(abspromocode))
                                {
                                    promocode = abspromocode;
                                }
                                //if (!string.IsNullOrEmpty(promocode))
                                {
                                    CategoryObj catObj = new CategoryObj(fn);
                                    int.TryParse(catObj.checkCategory(urlQuery, regno, promocode.Trim()), out con_categoryID);
                                    RegDelegateObj rgd = new RegDelegateObj(fn);
                                    rgd.updateCategoryID(regno, con_categoryID, showid);
                                }
                            }
                        }
                        RedirectToNormalFlow(regno, groupid, regstatus, regstage, flowid, showid);
                    }
                    else
                    {
                        Response.Redirect("ReLogin.aspx?Event=" + flowname.Trim());
                    }
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid.');window.location='404.aspx';</script>");
            }
        }
    }
    #region getPromoCodeAccordingAbsRules
    public string getPromoCodeAccordingAbsRules(string regno, string showid, string flowid, string absShowID, string checkAbsSubmittedUrl)
    {
        string returnPromoCode = string.Empty;

        try
        {
            bool isOk = checkAbsSubmittedAPI(regno, absShowID, checkAbsSubmittedUrl);
            if (isOk)
            {
                string query = "Select * From ref_AbsPromoRule Where ShowID=@ShowID And FlowID=@FlowID And rule_isdeleted=0 Order By rule_Priority";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
                SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
                spar1.Value = showid;
                spar2.Value = flowid;
                pList.Add(spar1);
                pList.Add(spar2);
                DataTable dt = fn.GetDatasetByCommand(query, "dsCatRule", pList).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string ruleSql = dr["rule_name"].ToString();
                        string nextstep = dr["rule_nextstep"] != DBNull.Value ? dr["rule_nextstep"].ToString() : "";
                        string isByRegno = dr["isByRegno"] != DBNull.Value ? dr["isByRegno"].ToString() : "";

                        if (isByRegno == "1")
                        {
                            ruleSql += regno;
                            ruleSql += " And ShowID=@ShowID And reg_urlFlowID=@FlowID";
                        }
                        DataTable dtResult = fn.GetDatasetByCommand(ruleSql, "dsResult", pList).Tables[0];
                        if (dtResult.Rows.Count > 0)
                        {
                            returnPromoCode = dr["rule_returnvalue"].ToString();
                            return returnPromoCode;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return returnPromoCode;
    }
    public bool checkAbsSubmittedAPI(string regno, string absShowID, string checkAbsSubmittedUrl)
    {
        bool isOk = false;
        try
        {
            AbstractSubmittedParam absSObj = new AbstractSubmittedParam();
            absSObj.AbsShowID = absShowID;
            absSObj.RegPortalRegno = regno;
            string jsonStr = JsonConvert.SerializeObject(absSObj);
            string rtnValue = PostAbsApi(jsonStr, checkAbsSubmittedUrl);
            if (!string.IsNullOrEmpty(rtnValue) && rtnValue != "ERROR")
            {
                AbstractSubmittedStatusJson rtnStatus = JsonConvert.DeserializeObject<AbstractSubmittedStatusJson>((string)rtnValue);
                if (!string.IsNullOrEmpty(rtnStatus.Status) && rtnStatus.Status.ToUpper() == "OK")
                {
                    isOk = true;
                    string absRegno = rtnStatus.AbsRegno;
                }
            }
        }
        catch (Exception ex)
        {
            isOk = false;
        }
        return isOk;
    }
    public string PostAbsApi(string jData, string APILink)
    {
        string rtnData = "";
        try
        {
            string jsonStr = jData;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(APILink);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonStr);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                rtnData = streamReader.ReadToEnd();
            }
        }
        catch (Exception ex) { rtnData = "ERROR"; }
        return rtnData;
    }
    #endregion
    #region RedirectToNormalFlow
    private void RedirectToNormalFlow(string regno, string groupid, string regstatus, string regstage, string flowid, string showid)
    {
        StatusSettings statusSet = new StatusSettings(fn);
        InvoiceControler invControler = new InvoiceControler(fn);
        StatusSettings stuSettings = new StatusSettings(fn);

        FlowControler flwControl = new FlowControler(fn);
        FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
        string lateststage = flwControl.GetLatestStep(flowid);

        if (!string.IsNullOrEmpty(lateststage) && regstage == lateststage && cFun.ParseInt(regstatus) == statusSet.Success)
        {
            string invStatus = invControler.getInvoiceStatus(regno);
            if (invStatus == stuSettings.Pending.ToString())
            {
                string page = ReLoginStaticValueClass.regConfirmationPage;
                string currentstage = flwControl.GetConirmationStage(flowid, SiteDefaultValue.constantConfirmationPage);
                string route = flwControl.MakeFullURL(page, flowid, showid, groupid, currentstage, regno);
                Response.Redirect(route, false);
            }
            if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                || invStatus == RegClass.noInvoice
                || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
            {
                string page = ReLoginStaticValueClass.regHomePage;
                string route = flwControl.MakeFullURL(page, flowid, showid);
                Response.Redirect(route, false);
            }
        }
        else
        {
            string invStatus = invControler.getInvoiceStatus(regno);
            if (invStatus == RegClass.noInvoice || invStatus == stuSettings.Pending.ToString())
            {
                FlowControler flwObj = new FlowControler(fn, flowid, regstage);
                string page = flwObj.CurrIndexModule;
                string currentstage = regstage;//"2";
                string route = flwObj.MakeFullURL(page, flowid, showid, groupid, currentstage, regno);
                Response.Redirect(route, false);
            }
            else if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
            {
                string page = ReLoginStaticValueClass.regHomePage;
                string route = flwControl.MakeFullURL(page, flowid, showid);
                Response.Redirect(route, false);
            }
        }
    }
    #endregion
    #region getAbstractSetting
    private DataTable getAbstractSetting(string flowid, string showid)
    {
        DataTable dtResult = new DataTable();
        try
        {
            string sql = "Select * From tb_AbstractSettings Where ShowID=@ShowID And FlowID=@FlowID";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            pList.Add(spar1);
            pList.Add(spar2);
            dtResult = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dtResult;
    }
    #endregion
    #region getUsedPromoCode(added on 11-4-2019)
    private string getUsedPromoCode(string groupid, string flowid, string showid, FlowURLQuery urlQuery)
    {
        string returnPromoCode = "";
        CategoryClass catClass = new CategoryClass();
        FlowControler fCon = new FlowControler(fn);
        FlowMaster flwMaster = fCon.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
        if (flwMaster.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
        {
            string type = flwMaster.FlowCategoryConfigType;
            if (type == catClass.flowcat_ByPromoCode.ToString() || type == catClass.flowcat_ByBusinessLogicWithPromo.ToString() || type == catClass.flowcat_ByBusinessLogicWithPromoNew.ToString())//According to Business Logic (Customize) *Like SHBC
            {
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);
                if (shw.SHW_Name == ASCAShowName)
                {
                    PromoController pCtrl = new PromoController(fn);
                    returnPromoCode = pCtrl.getUsedPromoCodeByGroupID(groupid, flowid, showid);
                }
            }
        }

        return returnPromoCode;
    }
    #endregion
    #region GetShowIDByEventName
    public class tmpDataList
    {
        public string showID { get; set; }
        public string FlowID { get; set; }
    }
    private tmpDataList GetShowIDByEventName(string eventName)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            cList.showID = "";
            cList.FlowID = "";
            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                cList.showID = dList.Rows[0]["showID"].ToString();
                cList.FlowID = dList.Rows[0]["FLW_ID"].ToString();
            }

        }
        catch { }
        return cList;
    }
    #endregion
    #region getUserIP
    public string getUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
    #endregion
}