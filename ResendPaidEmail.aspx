﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResendPaidEmail.aspx.cs" Inherits="ResendPaidEmail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Show ID:<asp:TextBox ID="txtShowID" runat="server"></asp:TextBox>
            <br />
            Flow ID:<asp:TextBox ID="txtFlowID" runat="server"></asp:TextBox>
            <br />
            Reg Group ID:<asp:TextBox ID="txtRegGroupID" runat="server"></asp:TextBox>
            <br />
            Regno ID:<asp:TextBox ID="txtRegno" runat="server"></asp:TextBox>
            <br />
            <asp:DropDownList ID="ddlInvoiceStatus" runat="server">
                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                 <asp:ListItem Text="PAID" Value="PAID"></asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="btnSend" runat="server" Text="ReSend" OnClick="btnSend_Click" />
        </div>
    </form>
</body>
</html>
