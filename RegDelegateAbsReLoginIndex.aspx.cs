﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegDelegateAbsReLoginIndex : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string regno = cFun.DecryptValue(urlQuery.DelegateID);
            if (!string.IsNullOrEmpty(showid) && !string.IsNullOrEmpty(flowid))
            {
                string username = cFun.solveSQL(regno);
                string password = "";
                updateCurStep(urlQuery);//***
                string cmdStr = string.Empty;
                DataTable dt = new DataTable(); StatusSettings statusSet = new StatusSettings(fn);
                InvoiceControler invControler = new InvoiceControler(fn);
                StatusSettings stuSettings = new StatusSettings(fn);
                FlowControler flwControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
                string flowname = flwMasterConfig.FlowName;
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    #region Group Registration
                    password = cFun.solveSQL(getEmailPassword(groupid, regno, showid, true));//***
                    RegGroupObj rgObj = new RegGroupObj(fn);
                    dt = rgObj.getGroupReLoginAuthenData(username, password, flowid);

                    if (dt.Rows.Count > 0)
                    {
                        groupid = dt.Rows[0]["RegGroupID"].ToString();
                        Session["Groupid"] = groupid;
                        Session["Flowid"] = flowid;
                        Session["Showid"] = showid;
                        string regstage = dt.Rows[0]["RG_Stage"].ToString();
                        Session["regstage"] = regstage;
                        string regstatus = dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                        Session["RegStatus"] = regstatus;

                        LoginLog lg = new LoginLog(fn);
                        string userip = GetUserIP();
                        lg.loginid = groupid;
                        lg.loginip = userip;
                        lg.logintype = LoginLogType.regLogin;
                        lg.saveLoginLog();

                        string lateststage = flwControl.GetLatestStep(flowid);

                        if (!string.IsNullOrEmpty(lateststage) && regstage == lateststage && cFun.ParseInt(regstatus) == statusSet.Success)
                        {
                            string invStatus = invControler.getInvoiceStatus(groupid);
                            if (invStatus == stuSettings.Pending.ToString())
                            {
                                string page = ReLoginStaticValueClass.regIndexPage;
                                string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                                Response.Redirect(route, false);
                            }
                            if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                || invStatus == RegClass.noInvoice
                                || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                            {
                                string page = ReLoginStaticValueClass.regHomePage;
                                string route = flwControl.MakeFullURL(page, flowid, showid);
                                Response.Redirect(route, false);
                            }
                        }
                        else
                        {
                            string invStatus = invControler.getInvoiceStatus(groupid);
                            if (invStatus == RegClass.noInvoice || invStatus == stuSettings.Pending.ToString())
                            {
                                string page = ReLoginStaticValueClass.regIndexPage;
                                string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                                Response.Redirect(route, false);
                            }
                            else if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                            {
                                string page = ReLoginStaticValueClass.regHomePage;
                                string route = flwControl.MakeFullURL(page, flowid, showid);
                                Response.Redirect(route, false);
                            }
                        }
                    }
                    else
                    {
                        Response.Redirect("ReLogin.aspx?Event=" + flowname.Trim());
                    }
                    #endregion
                }
                else
                {
                    #region Individual Registration
                    password = cFun.solveSQL(getEmailPassword(groupid, regno, showid, false));//***
                    RegDelegateObj rdObj = new RegDelegateObj(fn);
                    dt = rdObj.getDelegateReLoginAuthenData(username, password, flowid);
                    if (dt.Rows.Count > 0)
                    {
                        groupid = dt.Rows[0]["RegGroupID"].ToString();
                        regno = dt.Rows[0]["Regno"].ToString();
                        Session["Regno"] = regno;
                        Session["Groupid"] = groupid;
                        Session["Flowid"] = flowid;
                        Session["Showid"] = showid;
                        string regstage = dt.Rows[0]["reg_Stage"].ToString();
                        Session["regstage"] = regstage;
                        string regstatus = dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                        Session["RegStatus"] = regstatus;

                        LoginLog lg = new LoginLog(fn);
                        string userip = GetUserIP();
                        lg.loginid = groupid;
                        lg.loginip = userip;
                        lg.logintype = "Registration Login";
                        lg.saveLoginLog();

                        DataTable dtMenu = new DataTable();
                        dtMenu = flwControl.GetReloginMenu(flowid, "0");
                        if (dtMenu.Rows.Count > 0)
                        {
                            string invStatus = invControler.getInvoiceStatus(regno);
                            foreach (DataRow dr in dtMenu.Rows)
                            {
                                string scriptid = dr["reloginref_scriptID"].ToString().Trim();
                                string menuname = dr["reloginmenu_name"].ToString().Trim();
                                string stepseq = dr["step_seq"].ToString().Trim();
                                string menupageHeader = dr["step_label"].ToString().Trim();
                                if (cFun.ParseInt(regstatus) == statusSet.Success
                                       && (invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString()))
                                {
                                    if (flwMasterConfig.FlowType != SiteFlowType.FLOW_GROUP)
                                    {
                                        FlowControler flwObj = new FlowControler(fn, urlQuery);
                                        string page = flwObj.GetFrontendPageByStep(flowid, regstage);

                                        if (!string.IsNullOrEmpty(page))
                                        {
                                            string step = regstage;
                                            string route = flwObj.MakeFullURL(page, flowid, showid, groupid, step, regno, BackendRegType.backendRegType_Delegate);
                                            Response.Redirect(route, false);
                                        }
                                        else
                                        {
                                            Response.Redirect("404.aspx");
                                        }
                                    }
                                }
                                else//cFun.ParseInt(regstatus) == statusSet.Success && (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString() || invStatus == RegClass.noInvoice
                                {
                                    if (menuname == SiteDefaultValue.constantDelegateName)
                                    {
                                        if (flwMasterConfig.FlowType != SiteFlowType.FLOW_GROUP)
                                        {
                                            string path = Request.Url.AbsolutePath;
                                            string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                            string[] strpath = path.Split('/');
                                            if (strpath.Length > 1)
                                            {
                                                path = strpath[1] + ".aspx";
                                            }
                                            else
                                            {
                                                path = ".aspx";
                                            }

                                            string page = scriptid;
                                            string route = flwControl.MakeFullURL(page, flowid, showid, groupid, stepseq, regno);//regstage
                                            Response.Redirect(route, false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Response.Redirect("ReLogin.aspx?Event=" + flowname.Trim());
                    }
                    #endregion
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    private string getEmailPassword(string groupid, string regno, string showid, bool isGroup)
    {
        string returnValue = "";
        try
        {
            DataTable dtReg = new DataTable();
            if (isGroup)
            {
                RegGroupObj rgg = new RegGroupObj(fn);
                dtReg = rgg.getRegGroupByID(groupid, showid);
                if (dtReg.Rows.Count > 0)
                {
                    returnValue = dtReg.Rows[0]["RG_ContactEmail"].ToString();
                }
            }
            else
            {
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);
                RegDelegateObj rgd = new RegDelegateObj(fn);
                dtReg = rgd.getDataByGroupIDRegno(groupid, regno, showid);
                if (dtReg.Rows.Count > 0)
                {
                    returnValue = dtReg.Rows[0]["reg_Email"].ToString();
                }
            }
        }
        catch(Exception ex)
        { }

        return returnValue;
    }
    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
    #region updateCurStep - Check already paid, if not update current step
    private void updateCurStep(FlowURLQuery urlQuery)
    {
        try
        {
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cFun.DecryptValue(urlQuery.DelegateID);
            FlowControler flwObj = new FlowControler(fn, urlQuery);
            string showID = urlQuery.CurrShowID;
            string page = flwObj.NextStepURL();
            string step = flwObj.NextStep;
            string FlowID = flwObj.FlowID;
            string fullUrl = "FLW=" + flowid + "&STP=" + step + "&GRP=" + cFun.DecryptValue(groupid) + "&INV=" + delegateid + "&SHW=" + showID;
            FlowURLQuery newUrlQuery = new FlowURLQuery(fullUrl);

            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj.Count == 0)
            {
                //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
                RegGroupObj rgg = new RegGroupObj(fn);
                rgg.updateGroupCurrentStep(newUrlQuery);

                //*update reg_urlFlowID(current flowid) and reg_Stage(current step) of tb_RegDelegate table
                RegDelegateObj rgd = new RegDelegateObj(fn);
                rgd.updateDelegateCurrentStep(newUrlQuery);
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
}