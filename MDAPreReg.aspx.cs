﻿using Corpit.BackendMaster;
using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MDAPreReg : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    static string delegateRegNo = "Delegate_RefRegno";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.Params["reg"] != null)
            {
                string regDelegateRefNo = Request.QueryString["reg"].ToString();
                Response.Redirect("EventRegPreRegMDA.aspx?reg=" + cFun.EncryptValue(regDelegateRefNo));
                //string route = "EventRegPreRegMDA.aspx?reg=" + cFun.EncryptValue(regDelegateRefNo);
                //this.mdaFrame.Attributes.Add("src", route);
            }
            else
            {
                Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
            }
        }
    }
}