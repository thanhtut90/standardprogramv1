﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReLoginMaster.master" AutoEventWireup="true" CodeFile="Reg_Conference.aspx.cs" Inherits="Reg_Conference" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .accordionHeader1:after {
            font-family: 'Glyphicons Halflings';
            content: "\2212";
            float: right;
        }
        .accordionHeader1.collapsed:after {
            /* symbol for "collapsed" panels */
            content: "\2b";
        }

        .nav-tabs > li.active > a
        {
            color:#be1e2d !important;
            border-bottom-color: transparent !important;
        }
        .table
        {
            margin-bottom:0px !important;
        }
    </style>
    <link rel="stylesheet" href="Content/animate.min.css"/>
    <script src="Scripts/jquery-1.11.0.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div id="rcontent" runat="server">
                <h3 class="ConfrmHeader">Congress Selection</h3>
                <div class="row">
                    <div class="col-md-offset-2 col-md-8  col-xs-12 ConfItemPanel ">
                        <div class="table-responsive">
                            <div id="accordion" class="accordion">
                                <div id="divShowOrderList" runat="server"></div>
                            </div>
                            <br />
                            <div id="accordion" class="accordion">
                                <div id="divShowPendingOrderList" runat="server"></div>
                            </div>
                        </div>
                        <br /><br />
                        <asp:Label ID="lblGrandTotal" runat="server" Visible="false"></asp:Label>
                    </div>
                </div>
                <br />
                <div class="clear"></div>
            </div>

            <br />
            <div id="divSubmit" runat="server" class="form-group" visible="false">
                <div class="form-group container" style="padding-top:20px;">
                    <div class="col-lg-offset-4 col-lg-3" >
                        <asp:Button runat="server" ID="btnSubmit" CssClass="btn MainButton btn-block" Text="Additional Items" OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

