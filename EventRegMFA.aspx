﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventRegMFA.aspx.cs" Inherits="EventRegMFA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <link href="Content/Default/bootstrap.css" rel="stylesheet" />
    <link href="Content/RED/Site.css" rel="stylesheet" />
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-N24F3CX');</script>
    <!-- End Google Tag Manager -->
</head>
<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N24F3CX"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <form id="form1" runat="server">
      <div class="container" style="padding-top: 40px;display:none; ">
            <div class=" col-md-offset-1 col-sm-offset-1" style="padding-left:20px;">
                <h4>Be among the first 1,000 to pre-register and receive a gift when you visit.</h4>
            </div>
        </div>
        <div class="container" style="padding-top: 40px;">
            <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1" style="padding-bottom:20px;">
                <asp:Button runat="server" ID="btnIndividual" class="btn MainButton btn-lg btn-block" Text="Visitor Pre-Registration" OnClick="btnIndiv_Onclick" />
            </div>
            <div class="col-md-5 col-sm-5">
                <asp:Button runat="server" ID="Button1" class="btn MainButton  btn-lg btn-block" Text="Group Pre-Registration" OnClick="btnGroup_Onclick" />
                <br />
                <p style="font-weight: bold; text-align: left;padding-left:25px;">(for 5 persons and above from the same organisation) <br/>
              Please contact Tan Yanling(<a href="mailto:yanling@mda.com.sg">yanling@mda.com.sg</a>) for group delegation comprising more than one company.
            </p>
            </div>
        </div>
    </form>
</body>
</html>
