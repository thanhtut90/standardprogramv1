﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FoodJapanWelcome : System.Web.UI.Page
{
    #region Declaration
    static string _pagebanner = "page_banner";
    Functionality fn = new Functionality();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Boolean isclose = false;// fn.checkClosing();

            if (isclose)
            {
                divKeyPortion.Visible = true;

                tblClosed.Visible = true;
                //trMasterclassClosed.Visible = true;
                btnSignup.Visible = false;
                lnkSignup.Visible = true;
                //trMasterclassOpen.Visible = false;

                btnTrade.Visible = false;
                lnkTrade.Visible = true;

                btnPublic.Visible = false;
                lnkPublic.Visible = true;
            }
            else
            {
                divKeyPortion.Visible = false;

                Boolean isMasterclassClose = true;// fn.checkMasterclassRegClosing();
                if (isMasterclassClose)
                {
                    tblClosed.Visible = true;
                    //trMasterclassClosed.Visible = true;
                    btnSignup.Visible = false;
                    lnkSignup.Visible = true;
                    //trMasterclassOpen.Visible = false;
                }

                Boolean isTradeClose = false;// fn.checkTradeRegClosing();
                if (isTradeClose)
                {
                    btnTrade.Visible = false;
                    lnkTrade.Visible = true;
                }

                Boolean isPublicClose = false;// true;// fn.checkPublicRegClosing();
                if (isPublicClose)
                {
                    btnPublic.Visible = false;
                    lnkPublic.Visible = true;
                }
            }

            Session.Clear();
            Session.Abandon();

            pageSet();

            //setIncompleteToAsDeleted();//*

            //setGroupIncompleteToAsDeleted();//*
        }
    }

    #region pageSet & set Banner Image
    protected void pageSet()
    {
        //string strimage = string.Empty;

        //strimage = fn.GetDataByCommand("Select settings_value from tb_site_settings where settings_name='" + _pagebanner + "'", "settings_value");

        //imgBanner.ImageUrl = "~/" + strimage;
        //imgBanner.Visible = true;
    }
    #endregion

    protected void btnTrade_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/EventReg?EVent=RegFoodJapan");
    }

    protected void btnPublic_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/EventReg?EVent=FoodJapanPublic");
    }

    protected void btnSignup_Click(object sender, EventArgs e)
    {
        Response.Redirect("MasterMain.aspx");
        //Response.Redirect("#");
    }

    #region setIncompleteToAsDeleted
    private void setIncompleteToAsDeleted()
    {
        try
        {
            string sql = "Select * From (Select r.*,c.Invoice_total,c.Invoice_GST,c.Invoice_AdminFee,c.Invoice_status,c.Invoice_grandtotal,c.PaidAmt,c.PaymentMethod,c.InvoiceNo,c.InvoiceId From tb_RegDelegate as r Left Join tb_Invoice as c On "
                       + "r.regno = c.Regno Where r.recycle = 0 And(r.reg_staffid Is Null Or r.reg_staffid='') And r.RegGroupID = 0 "
                       + "And r.Regno Not In(Select Regno From tb_Invoice Where regno Is Not Null And c.Invoice_status = 1 And c.invoiceLock = 1)) as r";

            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];

            if(dt.Rows.Count > 0)
            {
                foreach(DataRow dr in dt.Rows)
                {
                    string regno = dr["Regno"].ToString();
                    fn.ExecuteSQL("Update tb_RegDelegate Set recycle=1 Where Regno='" + regno + "'");
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region setGroupIncompleteToAsDeleted
    private void setGroupIncompleteToAsDeleted()
    {
        try
        {
            string sql = "Select * From (Select r.*,c.Invoice_status, c.Invoice_grandtotal,c.PaidAmt,c.PaymentMethod,c.InvoiceNo,c.InvoiceId From tb_RegGroup as r, tb_Invoice as c " +
                    "Where r.recycle=0 And c.isdeleted=0 And c.RegGroupId = r.RegGroupID UNION Select r.*,'-1' as Invoice_status, '0' as Invoice_grandtotal,'0' as PaidAmt,'0' as PaymentMethod, 'N/A' as InvoiceNo, '0' as InvoiceId From tb_RegGroup as r " +
                    "Where r.RegGroupID NOT IN (Select RegGroupId From tb_Invoice Where RegGroupId is not null)) as r" +
                    " Where r.recycle=0 And r.RegGroupID NOT IN (Select RegGroupId From tb_Invoice Where RegGroupId is not null)";

            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string regGroupID = dr["RegGroupID"].ToString();
                    fn.ExecuteSQL("Update tb_RegGroup Set recycle=1 Where RegGroupID='" + regGroupID + "'");
                    fn.ExecuteSQL("Update tb_RegDelegate Set recycle=1 Where RegGroupID='" + regGroupID + "'");
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion


    protected void btnTradeGroup_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/EventReg?EVent=GroupReg");
    }
}