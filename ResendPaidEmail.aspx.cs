﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Email;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ResendPaidEmail : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        #region public ticket purchase
        if (ddlInvoiceStatus.SelectedIndex > 0 && ddlInvoiceStatus.SelectedValue == EmailSendConditionType.PAID.ToString())
        {
            if (!string.IsNullOrEmpty(txtShowID.Text) && !string.IsNullOrWhiteSpace(txtShowID.Text)
                && !string.IsNullOrEmpty(txtFlowID.Text) && !string.IsNullOrWhiteSpace(txtFlowID.Text)
                && !string.IsNullOrEmpty(txtRegGroupID.Text) && !string.IsNullOrWhiteSpace(txtRegGroupID.Text)
                && !string.IsNullOrEmpty(txtRegno.Text) && !string.IsNullOrWhiteSpace(txtRegno.Text))
            {
                string showID = txtShowID.Text.Trim();
                DataTable dtShow = fn.GetDatasetByCommand("Select * From tb_Show Where SHW_ID='" + showID + "'", "ds").Tables[0];
                if (dtShow.Rows.Count > 0)
                {
                    string flowid = txtFlowID.Text.Trim();
                    DataTable dtFlow = fn.GetDatasetByCommand("Select * From tb_site_flow_master Where ShowID='" + showID + "' And FLW_ID='" + flowid + "' And Status='Active'", "ds").Tables[0];
                    if (dtFlow.Rows.Count > 0)
                    {
                        string groupid = txtRegGroupID.Text.Trim();
                        DataTable dtGroup = fn.GetDatasetByCommand("Select * From tb_RegGroup Where ShowID='" + showID + "' And RG_urlFlowID='" + flowid + "' And recycle=0 And RegGroupID=" + groupid, "ds").Tables[0];
                        if (dtGroup.Rows.Count > 0)
                        {
                            string regno = txtRegno.Text.Trim();
                            DataTable dtRegDelegate = fn.GetDatasetByCommand("Select * From tb_RegDelegate Where ShowID='" + showID + "' And reg_urlFlowID='" + flowid + "' And recycle=0 And RegGroupID=" + groupid + " And Regno=" + regno, "ds").Tables[0];
                            if (dtRegDelegate.Rows.Count > 0)
                            {
                                string invoiceID = fn.GetDataByCommand("Select InvoiceID From tb_Invoice Where InvOwnerID=" + regno + " And Invoice_status=1 And invoice_loacked=1", "InvoiceID");
                                if (!string.IsNullOrEmpty(invoiceID) && invoiceID != "0")
                                {
                                    string emailRegType = BackendRegType.backendRegType_Delegate;// "D";
                                    EmailDSourceKey sourcKey = new EmailDSourceKey();
                                    string flowID = flowid;
                                    string groupID = groupid;
                                    string delegateID = regno;//if not, Blank
                                    string emailCondition = EmailSendConditionType.PAID;

                                    sourcKey.AddKeyToList("ShowID", showID);
                                    sourcKey.AddKeyToList("RegGroupID", groupID);
                                    sourcKey.AddKeyToList("Regno", delegateID);
                                    sourcKey.AddKeyToList("FlowID", flowID);
                                    sourcKey.AddKeyToList("INVID", invoiceID);

                                    string updatedUser = string.Empty;
                                    if (Session["userid"] != null)
                                    {
                                        updatedUser = Session["userid"].ToString();
                                    }
                                    EmailHelper eHelper = new EmailHelper();
                                    bool isEmailSend = eHelper.SendEmailFromBackendByCondition(emailCondition, sourcKey, emailRegType, updatedUser);
                                    #region Logging
                                    LogGenEmail lggenemail = new LogGenEmail(fn);
                                    LogActionObj lactObj = new LogActionObj();
                                    lggenemail.type = LogType.generalType;
                                    lggenemail.RefNumber = groupID + "," + regno;
                                    lggenemail.description = "Re-send confrimation email to " + delegateID;
                                    lggenemail.remark = "Re-send confrimation email to " + delegateID + " and Send " + emailCondition + " email(email sending status:" + isEmailSend + ")";
                                    lggenemail.step = "backend";
                                    lggenemail.writeLog();
                                    #endregion
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already sent the confirmation email(email sending status:" + isEmailSend + ")');", true);
                                }
                                else
                                {
                                    Response.Write("<script>alert('There is no invoice.');</script>");//window.location='404.aspx';
                                }
                            }
                            else
                            {
                                Response.Write("<script>alert('Not valid delegate.');</script>");//window.location='404.aspx';
                            }
                        }
                        else
                        {
                            Response.Write("<script>alert('Not valid delegate.');</script>");//window.location='404.aspx';
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('Not valid flow.');</script>");//window.location='404.aspx';
                    }
                }
                else
                {
                    Response.Write("<script>alert('Not valid show.');</script>");//window.location='404.aspx';
                }
            }
            else
            {
                Response.Write("<script>alert('Please enter textbox(es).');</script>");//window.location='404.aspx';
            }
        }
        #endregion
    }
}