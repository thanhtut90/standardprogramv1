﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegClosed.aspx.cs" Inherits="RegClosed" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><%= Global.PageTitle %></title>
    
    <webopt:BundleReference ID="BundleRefence" runat="server" Path="~/bundles/BLUES" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <%--<asp:ScriptReference Name="MsAjaxBundle" />--%>
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="respond" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />

                <asp:ScriptReference Path="~/Content/countdown/jquery.plugin.js" />
                <asp:ScriptReference Path="~/Content/countdown/jquery.countdown.js" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div class="col-lg-8 col-lg-offset-2 col-xs-12 SiteMasterFrame">
            <div class="row">
                <asp:Image ID="SiteBanner" runat="server" CssClass="img-responsive"  Width="100%" />
            </div>
            <div class="row">
            </div>
            <div class="row SiteContent " style="">
                <h3><asp:Label ID="lblClosedMessage" runat="server" Text="Registration Closed!"></asp:Label></h3>
            </div>
        </div>
    </form>
</body>
</html>
