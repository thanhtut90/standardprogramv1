﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="SuccOSEA.aspx.cs" Inherits="SuccOSEA" %>
<%@ MasterType VirtualPath="~/Registration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta property="og:url"           content="https://www.facebook.com/osea.asia/" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="OSEA" />
    <meta property="og:description"   content="I am registered for OSEA2018.  Pre-register and let's meet up there!" />
    <meta property="og:image"         content="https://www.your-domain.com/path/image.jpg" />
    <script>
        function close_window() {
            if (confirm("Close Window?")) {
                //window.open('', '_self').close();
                //window.open('', '_self'); window.close();
                //window.self.close();
                netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserWrite");
                window.close();
            }
        }

        function closeWindow() {
            netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserWrite");
            alert("This will close the window");
            window.open('','_self');
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Panel runat="server" ID="PanelMsg">
        <p>
            <asp:Button ID="btnRegisterColleague" runat="server" Text="Register for Colleagues" 
                CssClass="btn btn-success"
                OnClick="btnRegisterColleague_Click" Visible="true" />
        </p>
        <p style="padding-bottom:30px;">
            <asp:Label ID="lblSuccessMessage" runat="server"></asp:Label>
        </p>

        <!-- Load Facebook SDK for JavaScript -->
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <!-- Your share button code -->
        <div class="row">
            <div class="col-md-2 col-xs-2">
                 <div class="fb-share-button" data-size="large" 
                data-href="https://www.facebook.com/osea.asia/" 
                data-layout="button">
                </div>
            </div>
            <div class="col-md-2 col-xs-2" style="padding-left:10px;">
                <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.osea-asia.com%2Fto-visit%2Fvisitor-pre-registration%2F&title=I+am+registered+for+OSEA2018.++Pre-register+and+let%E2%80%99s+meet+up+there%21">
                    <img src="http://203.127.83.146/CorpitDefault/images/OSEA2018/LinkedInShareIcon.png" />
                </a>
            </div>
            <%--<div class="col-sm-2" style="padding-right:0px;">
                <a href="https://www.facebook.com/osea.asia/" target="_blank"><img src="http://203.127.83.146/CorpitDefault/images/OSEA2018/facebookicon.png" /></a>&nbsp;&nbsp;
                <a href="https://www.linkedin.com/groups/2743037" target="_blank"><img src="http://203.127.83.146/CorpitDefault/images/OSEA2018/linkedinicon.png" /></a>
            </div>--%>
        </div>

        <br /><br /><br />
        <a id="btnGoToWebsite" runat="server" target="_blank"
                class="btn btn-success"
                href="https://www.osea-asia.com/" visible="true">Back to Website</a>

        <%--<a class="btn btn-warning" href="#" onclick="close_window();return false;">Close</a>--%>

        <asp:Button ID="btnClose" runat="server" Text="Close" 
                CssClass="btn btn-warning"
                OnClick="btnClose_Click" Visible="false" />
    </asp:Panel>
    <div>
        <iframe runat="server" id="ShowPDF" src="" width="100%" height="1200" scrolling="no"  frameborder="0" visible="false" ></iframe>
    </div>
</asp:Content>

