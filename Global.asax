﻿<%@ Application Language="C#" %>
<%@ Import Namespace="StandardWebTemplate" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="Corpit.Site.Utilities" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        RouteConfig.RegisterRoutes(RouteTable.Routes);
        BundleConfig.RegisterBundles(BundleTable.Bundles);

    }
    void Application_BeginRequest(object sender, EventArgs e)
    {
        Functionality fn = new Functionality();

        // InitSiteSettings();

        string currency = Global.Currency;

        currency = fn.GetDataByCommand("Select settings_value from tb_site_settings where settings_name='currency'", "settings_value");
        Global.Currency = currency;
    }

</script>
