﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReLoginMaster.master" AutoEventWireup="true" CodeFile="Reg_Confirmation.aspx.cs" Inherits="Reg_Confirmation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script> 

    <link rel="stylesheet" type="text/css" href="css/jquery.countdown.css" />
    <style type="text/css">
        #defaultCountdown {
            width: 295px;
            height: 20px;
        }
    </style>

    <script type="text/javascript" src="scripts/jquery.plugin.js"></script>
    <script type="text/javascript" src="scripts/jquery.countdown.js"></script>

    <script type="text/javascript">

        function MakeRbExclusive(rb) {
            var rbs = $('#rbData :radio');
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i] != rb) rbs[i].checked = false;
            }
        }

        function Reload() {
            window.location.reload();
        }
        </script>


    <style type="text/css">
        .style1
        {
            color: #3EBB96;
            font-family:Arial;
        }
    .style2
    {
        font-size: 14px;
        color: #3EBB96;
        text-decoration: none;
        font-family: Arial;
    }
    </style>

    <style type="text/css">
        .accordionHeader1:after {
            font-family: 'Glyphicons Halflings';
            content: "\2212";
            float: right;
        }
        .accordionHeader1.collapsed:after {
            /* symbol for "collapsed" panels */
            content: "\2b";
        }

        .nav-tabs > li.active > a
        {
            color:#be1e2d !important;
            border-bottom-color: transparent !important;
        }
        .table
        {
            margin-bottom:0px !important;
        }
    </style>
    <link rel="stylesheet" href="Content/animate.min.css"/>
    <script src="Scripts/jquery-1.11.0.min.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="fcontent">
        <div style="padding: 0px 30px 30px 30px; width: auto; margin:0 auto; position: relative; border-radius: 0px"><%--background-color: #F1F1F1; --%>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <br/><br/>
                    <div id="lcontent">
                        <div id="divContactPerson" runat="server" visible="false">
                            <br />
                            <h3 class="ConfrmHeader">Group Co-ordinator Information</h3>
                            <br />

                            <div class="table-responsive col-lg-offset-1 col-md-offset-1">
                                <table class="table borderless">
                                    <tr id="trGSalutation" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGSalutation" runat="server" Text="Title"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGSalutation" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGFName" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGFName" runat="server" Text="First Name"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGFName" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGLName" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGLName" runat="server" Text="Surname"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGLName" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGDesignation" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGDesignation" runat="server" Text="Designation"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGDesignation" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGDepartment" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGDepartment" runat="server" Text="Department"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGDepartment" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGCompany" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGCompany" runat="server" Text="Company"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGCompany" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGIndustry" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGIndustry" runat="server" Text="Industry"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGIndustry" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGAddress1" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAddress1" runat="server" Text="Address1"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAddress1" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGAddress2" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAddress2" runat="server" Text="Address2"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAddress2" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGAddress3" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAddress3" runat="server" Text="Address3"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAddress3" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGCountry" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGCountry" runat="server" Text="Country"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGCountry" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGRCountry" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGRCountry" runat="server" Text="RCountry"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGRCountry" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGCity" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGCity" runat="server" Text="City"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGCity" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGState" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGState" runat="server" Text="State"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGState" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGPostalCode" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGPostalCode" runat="server" Text="Postal Code"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGPostalCode" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGTel" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGTel" runat="server" Text="Telephone"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGTel" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGMobile" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGMobile" runat="server" Text="Mobile"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGMobile" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGFax" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGFax" runat="server" Text="Fax"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGFax" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGEmail" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGEmail" runat="server" Text="Email"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGEmail" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                     <tr id="trGAge" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAge" runat="server" Text="Age"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAge" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                     <tr id="trGDOB" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGDOB" runat="server" Text="Date of Birth"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGDOB" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                     <tr id="trGGender" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGGender" runat="server" Text="Gender"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGGender" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGVisitDate" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGVisitDate" runat="server" Text="Visit Date"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGVisitDate" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGVisitTime" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGVisitTime" runat="server" Text="Visit Time"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGVisitTime" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGPassword" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGPassword" runat="server" Text="Password"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGPassword" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                     <tr id="trGAdditional4" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAdditional4" runat="server" Text="Additional4"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAdditional4" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGAdditional5" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAdditional5" runat="server" Text="Additional5"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAdditional5" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div id="divCompany" runat="server" visible="false">
                            <br />
                            <h3 class="ConfrmHeader">Company Information</h3>
                            <br />

                            <div class="table-responsive col-lg-offset-1 col-md-offset-1">
                                <table class="table borderless">
                                    <tr id="trCName" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCName" runat="server" Text="Name"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCName" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAddress1" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAddress1" runat="server" Text="Address1"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAddress1" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAddress2" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAddress2" runat="server" Text="Address2"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAddress2" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAddress3" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAddress3" runat="server" Text="Address3"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAddress3" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCCity" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCCity" runat="server" Text="City"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCCity" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCState" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCState" runat="server" Text="State"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCState" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCCountry" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCCountry" runat="server" Text="Country"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCCountry" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCZipcode" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCZipcode" runat="server" Text="Zip Code"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCZipcode" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCTel" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCTel" runat="server" Text="Telephone"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCTel" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCFax" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCFax" runat="server" Text="Fax"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCFax" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCEmail" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCEmail" runat="server" Text="Email"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCEmail" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCWebsite" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCWebsite" runat="server" Text="Website"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCWebsite" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAdditional1" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAdditional1" runat="server" Text="Additional1"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAdditional1" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAdditional2" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAdditional2" runat="server" Text="Additional2"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAdditional2" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAdditional3" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAdditional3" runat="server" Text="Additional3"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAdditional3" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAdditional4" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAdditional4" runat="server" Text="Additional4"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAdditional4" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAdditional5" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAdditional5" runat="server" Text="Additional5"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAdditional5" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div id="divDelegate" runat="server" visible="false">
                            <h5 style="font-family: 'Trebuchet-MS';font-weight:bold;"><asp:Label ID="lblIDEMComeBackLaterMsg" runat="server" Visible="false" Text="If you wish to add workshops, please return at a later date to check."></asp:Label></h5>
                            <br />
                            <div class="row">
                                <div class="col-lg-4" ><%--col-lg-offset-9 --%>
                                    <asp:Button runat="server" ID="btnAddtional" CssClass="btn btn-danger btn-block" Text="Additional Purchase" OnClick="btnAddtional_Click" Visible="false"/>
                                </div>
                            </div>
                            <br />
                            <h3 class="ConfrmHeader">Your Registration Details</h3>
                            <br />

                            <div class="table-responsive col-lg-offset-1 col-md-offset-1">
                                <asp:Repeater ID="rptDelegateTable" runat="server" OnItemDataBound="rptDelegateTable_ItemDataBound">
                                    <ItemTemplate>
                                        <table class="table borderless">
                                            <tr id="trSrNo" runat="server" class="confrmMemHeaderStyle">
                                                <td colspan="3">
                                                    <asp:Label ID="lblSrNo" runat="server" Text='Member '></asp:Label><%#Container.ItemIndex+1 %>
                                                </td>
                                            </tr>
                                            <tr id="trDSalutation" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDSal" runat="server" Text="Title"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDSal" runat="server"><%# bindSalutation(Eval("reg_Salutation").ToString(), Eval("reg_SalutationOthers").ToString()) %></asp:Label></td>
                                            </tr>
                                            <tr id="trDFName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDFName" runat="server" Text="First Name"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDFName" runat="server" Text='<%#Eval("reg_FName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDLName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDLName" runat="server" Text="Surname"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDLName" runat="server" Text='<%#Eval("reg_LName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDOName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDOName" runat="server" Text="Other Name"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDOName" runat="server" Text='<%#Eval("reg_OName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDPassno" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDPassno" runat="server" Text="NRIC/Passport No."></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDPassno" runat="server" Text='<%#Eval("reg_PassNo")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDIsReg" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDIsReg" runat="server" Text="Are you a Singapore registered doctor/nurse/pharmacist?"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDIsReg" runat="server"><%#Eval("reg_isReg") != null ? (Eval("reg_isReg").ToString() == "1" ? "Yes" : "No") : "No"%></asp:Label></td>
                                            </tr>
                                            <tr id="trDRegSpecific" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDRegSpecific" runat="server" Text="MCR/SNB/PRN"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDRegSpecific" runat="server" Text='<%#Eval("reg_sgregistered")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDIDNo" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDIDNo" runat="server" Text="MCR/SNB/PRN No."></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDIDNo" runat="server" Text='<%#Eval("reg_IDno")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDDesignation" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDDesignation" runat="server" Text="Designation"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDDesignation" runat="server" Text='<%#Eval("reg_Designation")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDProfession" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDProfession" runat="server" Text="Profession"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDProfession" runat="server" Text='<%# bindProfession(Eval("reg_Profession").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDOrg" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDOrg" runat="server" Text="Organization"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDOrg" runat="server" Text='<%# bindOrganisation(Eval("reg_Organization").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDInstitution" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDInstitution" runat="server" Text="Institution"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDInstitution" runat="server" Text='<%# bindInstitution(Eval("reg_Institution").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDDept" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDDept" runat="server" Text="Department"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDDept" runat="server" Text='<%# bindDepartment(Eval("reg_Department").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAddress1" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAddress1" runat="server" Text="Address1"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAddress1" runat="server" Text='<%#Eval("reg_Address1")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAddress2" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAddress2" runat="server" Text="Address2"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAddress2" runat="server" Text='<%#Eval("reg_Address2")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAddress3" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAddress3" runat="server" Text="Address3"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAddress3" runat="server" Text='<%#Eval("reg_Address3")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAddress4" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAddress4" runat="server" Text="Address4"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAddress4" runat="server" Text='<%#Eval("reg_Address4")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDCity" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDCity" runat="server" Text="City"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDCity" runat="server" Text='<%#Eval("reg_City")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDState" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDState" runat="server" Text="State"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDState" runat="server" Text='<%#Eval("reg_State")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDCountry" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDCountry" runat="server" Text="Country"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDCountry" runat="server" Text='<%# bindCountry(Eval("reg_Country").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDPostal" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDPostal" runat="server" Text="Postal Code"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDPostal" runat="server" Text='<%#Eval("reg_PostalCode")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDRCountry" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDRCountry" runat="server" Text="RCountry"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDRCountry" runat="server" Text='<%# bindCountry(Eval("reg_RCountry").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDTel" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDTel" runat="server" Text="Telephone"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDTel" runat="server"><%# bindPhoneNo(Eval("reg_Telcc").ToString(), Eval("reg_Telac").ToString(), Eval("reg_Tel").ToString(), "Tel")%></asp:Label></td>
                                            </tr>
                                            <tr id="trDMobile" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDMobile" runat="server" Text="Mobile"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDMobile" runat="server"><%# bindPhoneNo(Eval("reg_Mobcc").ToString(), Eval("reg_Mobac").ToString(), Eval("reg_Mobile").ToString(), "Mob")%></asp:Label></td>
                                            </tr>
                                            <tr id="trDFax" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDFax" runat="server" Text="Fax"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDFax" runat="server"><%# bindPhoneNo(Eval("reg_Faxcc").ToString(), Eval("reg_Faxac").ToString(), Eval("reg_Fax").ToString(), "Fax")%></asp:Label></td>
                                            </tr>
                                            <tr id="trDEmail" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDEmail" runat="server" Text="Email"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDEmail" runat="server" Text='<%#Eval("reg_Email")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAffiliation" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAffiliation" runat="server" Text="Affiliation"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAffiliation" runat="server" Text='<%# bindAffiliation(Eval("reg_Affiliation").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDDietary" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDDietary" runat="server" Text="Dietary"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDDietary" runat="server" Text='<%# bindDietary(Eval("reg_Dietary").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDNationality" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDNationality" runat="server" Text="Nationality"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="Label4" runat="server" Text='<%#Eval("reg_Nationality")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAge" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAge" runat="server" Text="Age"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAge" runat="server" Text='<%#Eval("reg_Age")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDDOB" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDDOB" runat="server" Text="Date of Birth"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDDOB" runat="server" Text='<%#Eval("reg_DOB")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDGender" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDGender" runat="server" Text="Gender"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDGender" runat="server" Text='<%#Eval("reg_Gender")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDMembershipNo" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDMembershipNo" runat="server" Text="MembershipNo"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDMembershipNo" runat="server" Text='<%#Eval("reg_Membershipno")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAdditional4" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAdditional4" runat="server" Text="Additional4"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAdditional4" runat="server" Text='<%#Eval("reg_Additional4")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAdditional5" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAdditional5" runat="server" Text="Additional5"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAdditional5" runat="server" Text='<%#Eval("reg_Additional5")%>'></asp:Label></td>
                                            </tr>

                                            <tr id="trDVName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVName" runat="server" Text="Visitor Name"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVName" runat="server" Text='<%#Eval("reg_vName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDVDOB" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVDOB" runat="server" Text="Visitor DOB"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVDOB" runat="server" Text='<%#Eval("reg_vDOB")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDVPass" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVPass" runat="server" Text="Visitor Passport No."></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVPass" runat="server" Text='<%#Eval("reg_vPassno")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDVPassExpiry" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVPassExpiry" runat="server" Text="Visitor Passport Expiry"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVPassExpiry" runat="server" Text='<%#Eval("reg_vPassexpiry")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDVCountry" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVCountry" runat="server" Text="Visitor Country"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVCountry" runat="server" Text='<%# bindCountry(Eval("reg_vCountry").ToString())%>'></asp:Label></td>
                                            </tr>

                                            <tr id="trDUDF_CName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CName" runat="server" Text="UDFC Name"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CName" runat="server" Text='<%#Eval("UDF_CName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_DelegateType" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_DelegateType" runat="server" Text="UDF Delegate Type"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_DelegateType" runat="server" Text='<%#Eval("UDF_DelegateType")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_ProfCategory" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_ProfCategory" runat="server" Text="UDF Prof Category"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_ProfCategory" runat="server"><%#Eval("UDF_ProfCategory")%> <%#Eval("UDF_ProfCategoryOther")%></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_CPcode" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CPcode" runat="server" Text="UDFC Postal Code"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CPcode" runat="server" Text='<%#Eval("UDF_CPcode")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_CLDepartment" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CLDepartment" runat="server" Text="UDFCL Department"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CLDepartment" runat="server" Text='<%#Eval("UDF_CLDepartment")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_CAddress" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CAddress" runat="server" Text="UDFC Address"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CAddress" runat="server" Text='<%#Eval("UDF_CAddress")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_CLCompany" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CLCompany" runat="server" Text="UDFCL Company"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CLCompany" runat="server"><%#Eval("UDF_CLCompany")%><%#Eval("UDF_CLCompanyOther")%></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_CCountry" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CCountry" runat="server" Text="UDFC Country"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CCountry" runat="server" Text='<%# bindCountry(Eval("UDF_CCountry").ToString())%>'></asp:Label></td>
                                            </tr>

                                            <tr id="trDSupName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDSupName" runat="server" Text="Supervisor Name"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDSupName" runat="server" Text='<%#Eval("reg_SupervisorName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDSupDesignation" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDSupDesignation" runat="server" Text="Supervisor Designation"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDSupDesignation" runat="server" Text='<%#Eval("reg_SupervisorDesignation")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDSupContact" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDSupContact" runat="server" Text="Supervisor Contact"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDSupContact" runat="server" Text='<%#Eval("reg_SupervisorContact")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDSupEmail" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDSupEmail" runat="server" Text="Supervisor Email"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDSupEmail" runat="server" Text='<%#Eval("reg_SupervisorEmail")%>'></asp:Label></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>

                            <div style="overflow-x:auto;overflow-y:hidden;display:none;" class="table-responsive">
                                <table style="width:100%;" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <%-- <tr>
                                                <th><asp:Label ID="lblRegistrationID" runat="server" Text="Registration ID"></asp:Label></th>
                                                <th>:</th>
                                                <th><asp:Label ID="_lblRegno" runat="server" Text="Label"></asp:Label></th>
                                            </tr>--%>
                                            <th>No.</th>

                                            <th id="trSalutation" runat="server" scope="row">
                                                <asp:Label ID="lblSal" runat="server" Text="Title"></asp:Label>
                                            </th>
                                            <th id="trFName" runat="server" scope="row">
                                                <asp:Label ID="lblFName" runat="server" Text="First Name"></asp:Label>
                                            </th>
                                            <th id="trLName" runat="server" scope="row">
                                                <asp:Label ID="lblLName" runat="server" Text="Surname"></asp:Label>
                                            </th>
                                            <th id="trOName" runat="server" scope="row">
                                                <asp:Label ID="lblOName" runat="server" Text="Other Name"></asp:Label>
                                            </th>
                                            <th id="trPassno" runat="server" scope="row">
                                                <asp:Label ID="lblPassno" runat="server" Text="NRIC/Passport No."></asp:Label>
                                            </th>
                                            <th id="trIsReg" runat="server" scope="row">
                                                <asp:Label ID="lblIsReg" runat="server" Text="Are you a Singapore registered doctor/nurse/pharmacist?"></asp:Label>
                                            </th>
                                            <th id="trRegSpecific" runat="server" scope="row">
                                                <asp:Label ID="lblRegSpecific" runat="server" Text="MCR/SNB/PRN"></asp:Label>
                                            </th>
                                            <th id="trIDNo" runat="server" scope="row">
                                                <asp:Label ID="lblIDNo" runat="server" Text="MCR/SNB/PRN No."></asp:Label>
                                            </th>
                                            <th id="trDesignation" runat="server" scope="row">
                                                <asp:Label ID="lblDesignation" runat="server" Text="Designation"></asp:Label>
                                            </th>
                                            <th id="trProfession" runat="server" scope="row">
                                                <asp:Label ID="lblProfession" runat="server" Text="Profession"></asp:Label>
                                            </th>
                                            <th id="trOrg" runat="server" scope="row">
                                                <asp:Label ID="lblOrg" runat="server" Text="Organization"></asp:Label>
                                            </th>
                                            <th id="trInstitution" runat="server" scope="row">
                                                <asp:Label ID="lblInstitution" runat="server" Text="Institution"></asp:Label>
                                            </th>
                                            <th id="trDept" runat="server" scope="row">
                                                <asp:Label ID="lblDept" runat="server" Text="Department"></asp:Label>
                                            </th>
                                            <th id="trAddress1" runat="server" scope="row">
                                                <asp:Label ID="lblAddress1" runat="server" Text="Address1"></asp:Label>
                                            </th>
                                            <th id="trAddress2" runat="server" scope="row">
                                                <asp:Label ID="lblAddress2" runat="server" Text="Address2"></asp:Label>
                                            </th>
                                            <th id="trAddress3" runat="server" scope="row">
                                                <asp:Label ID="lblAddress3" runat="server" Text="Address3"></asp:Label>
                                            </th>
                                            <th id="trAddress4" runat="server" scope="row">
                                                <asp:Label ID="lblAddress4" runat="server" Text="Address4"></asp:Label>
                                            </th>
                                            <th id="trCity" runat="server" scope="row">
                                                <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                                            </th>
                                            <th id="trState" runat="server" scope="row">
                                                <asp:Label ID="lblState" runat="server" Text="State"></asp:Label>
                                            </th>
                                            <th id="trCountry" runat="server" scope="row">
                                                <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                                            </th>
                                            <th id="trPostal" runat="server" scope="row">
                                                <asp:Label ID="lblPostal" runat="server" Text="Postal Code"></asp:Label>
                                            </th>
                                            <th id="trRCountry" runat="server" scope="row">
                                                <asp:Label ID="lblRCountry" runat="server" Text="RCountry"></asp:Label>
                                            </th>
                                            <th id="trTel" runat="server" scope="row">
                                                <asp:Label ID="lblTel" runat="server" Text="Telephone"></asp:Label>
                                            </th>
                                            <th id="trMobile" runat="server" scope="row">
                                                <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                                            </th>
                                            <th id="trFax" runat="server" scope="row">
                                                <asp:Label ID="lblFax" runat="server" Text="Fax"></asp:Label>
                                            </th>
                                            <th id="trEmail" runat="server" scope="row">
                                                <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                                            </th>
                                                <th id="trAffiliation" runat="server" scope="row">
                                                <asp:Label ID="lblAffiliation" runat="server" Text="Affiliation"></asp:Label>
                                            </th>
                                            <th id="trDietary" runat="server" scope="row">
                                                <asp:Label ID="lblDietary" runat="server" Text="Dietary"></asp:Label>
                                            </th>
                                            <th id="trNationality" runat="server" scope="row">
                                                <asp:Label ID="lblNationality" runat="server" Text="Nationality"></asp:Label>
                                            </th>
                                            <th id="trAge" runat="server" scope="row">
                                                <asp:Label ID="lblAge" runat="server" Text="Age"></asp:Label>
                                            </th>
                                            <th id="trDOB" runat="server" scope="row">
                                                <asp:Label ID="lblDOB" runat="server" Text="Date of Birth"></asp:Label>
                                            </th>
                                            <th id="trGender" runat="server" scope="row">
                                                <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                            </th>
                                            <th id="trMembershipNo" runat="server" scope="row">
                                                <asp:Label ID="lblMembershipNo" runat="server" Text="MembershipNo"></asp:Label>
                                            </th>
                                            <th id="trAdditional4" runat="server" scope="row">
                                                <asp:Label ID="lblAdditional4" runat="server" Text="Additional4"></asp:Label>
                                            </th>
                                            <th id="trAdditional5" runat="server" scope="row">
                                                <asp:Label ID="lblAdditional5" runat="server" Text="Additional5"></asp:Label>
                                            </th>

                                            <th id="trVName" runat="server" scope="row">
                                                <asp:Label ID="lblVName" runat="server" Text="Visitor Name"></asp:Label>
                                            </th>
                                            <th id="trVDOB" runat="server" scope="row">
                                                <asp:Label ID="lblVDOB" runat="server" Text="Visitor DOB"></asp:Label>
                                            </th>
                                            <th id="trVPass" runat="server" scope="row">
                                                <asp:Label ID="lblVPass" runat="server" Text="Visitor Passport No."></asp:Label>
                                            </th>
                                            <th id="trVPassExpiry" runat="server" scope="row">
                                                <asp:Label ID="lblVPassExpiry" runat="server" Text="Visitor Passport Expiry"></asp:Label>
                                            </th>
                                            <th id="trVCountry" runat="server" scope="row">
                                                <asp:Label ID="lblVCountry" runat="server" Text="Visitor Country"></asp:Label>
                                            </th>

                                            <th id="trUDF_CName" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CName" runat="server" Text="UDFC Name"></asp:Label>
                                            </th>
                                            <th id="trUDF_DelegateType" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_DelegateType" runat="server" Text="UDF Delegate Type"></asp:Label>
                                            </th>
                                            <th id="trUDF_ProfCategory" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_ProfCategory" runat="server" Text="UDF Prof Category"></asp:Label>
                                            </th>
                                            <th id="trUDF_CPcode" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CPcode" runat="server" Text="UDFC Postal Code"></asp:Label>
                                            </th>
                                            <th id="trUDF_CLDepartment" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CLDepartment" runat="server" Text="UDFCL Department"></asp:Label>
                                            </th>
                                            <th id="trUDF_CAddress" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CAddress" runat="server" Text="UDFC Address"></asp:Label>
                                            </th>
                                            <th id="trUDF_CLCompany" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CLCompany" runat="server" Text="UDFCL Company"></asp:Label>
                                            </th>
                                            <th id="trUDF_CCountry" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CCountry" runat="server" Text="UDFC Country"></asp:Label>
                                            </th>

                                            <th id="trSupName" runat="server" scope="row">
                                                <asp:Label ID="lblSupName" runat="server" Text="Supervisor Name"></asp:Label>
                                            </th>
                                            <th id="trSupDesignation" runat="server" scope="row">
                                                <asp:Label ID="lblSupDesignation" runat="server" Text="Supervisor Designation"></asp:Label>
                                            </th>
                                            <th id="trSupContact" runat="server" scope="row">
                                                <asp:Label ID="lblSupContact" runat="server" Text="Supervisor Contact"></asp:Label>
                                            </th>
                                            <th id="trSupEmail" runat="server" scope="row">
                                                <asp:Label ID="lblSupEmail" runat="server" Text="Supervisor Email"></asp:Label>
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <asp:Repeater ID="rptItem" runat="server" OnItemDataBound="rptitemdatabound">
                                            <ItemTemplate>
                                                <%--<table>--%>
                                                <%--<tr>
                                                    <td><asp:Label ID="lblRegistrationID" runat="server" Text="Registration ID"></asp:Label></td>
                                                    <td>:</td>
                                                    <td><asp:Label ID="_lblRegno" runat="server" Text="Label"></asp:Label></td>
                                                </tr>--%>
                                                <tr>
                                                    <td><%#Container.ItemIndex+1 %></td>

                                                    <td id="tdSalutation" runat="server"><asp:Label ID="_lblsal" runat="server"><%# bindSalutation(Eval("reg_Salutation").ToString(), Eval("reg_SalutationOthers").ToString()) %></asp:Label></td>

                                                    <td id="tdFName" runat="server"><asp:Label ID="_lblFName" runat="server" Text='<%#Eval("reg_FName")%>'></asp:Label></td>

                                                    <td id="tdLName" runat="server"><asp:Label ID="_lblLName" runat="server" Text='<%#Eval("reg_LName")%>'></asp:Label></td>

                                                    <td id="tdOName" runat="server"><asp:Label ID="_lblOName" runat="server" Text='<%#Eval("reg_OName")%>'></asp:Label></td>

                                                    <td id="tdPassno" runat="server"><asp:Label ID="_lblPassno" runat="server" Text='<%#Eval("reg_PassNo")%>'></asp:Label></td>

                                                    <td id="tdIsReg" runat="server"><asp:Label ID="_lblIsReg" runat="server"><%#Eval("reg_isReg") != null ? (Eval("reg_isReg").ToString() == "1" ? "Yes" : "No") : "No"%></asp:Label></td>

                                                    <td id="tdRegSpecific" runat="server"><asp:Label ID="_lblRegSpecific" runat="server" Text='<%#Eval("reg_sgregistered")%>'></asp:Label></td>

                                                    <td id="tdIDNo" runat="server"><asp:Label ID="_lblIDNo" runat="server" Text='<%#Eval("reg_IDno")%>'></asp:Label></td>

                                                    <td id="tdDesignation" runat="server"><asp:Label ID="_lblDesignation" runat="server" Text='<%#Eval("reg_Designation")%>'></asp:Label></td>

                                                    <td id="tdProfession" runat="server"><asp:Label ID="_lblProfession" runat="server" Text='<%# bindProfession(Eval("reg_Profession").ToString())%>'></asp:Label></td>

                                                    <td id="tdOrg" runat="server"><asp:Label ID="_lblOrg" runat="server" Text='<%# bindOrganisation(Eval("reg_Organization").ToString())%>'></asp:Label></td>

                                                    <td id="tdInstitution" runat="server"><asp:Label ID="_lblInstitution" runat="server" Text='<%# bindInstitution(Eval("reg_Institution").ToString())%>'></asp:Label></td>

                                                    <td id="tdDept" runat="server"><asp:Label ID="_lblDept" runat="server" Text='<%# bindDepartment(Eval("reg_Department").ToString())%>'></asp:Label></td>

                                                    <td id="tdAddress1" runat="server"><asp:Label ID="_lblAddress1" runat="server" Text='<%#Eval("reg_Address1")%>'></asp:Label></td>

                                                    <td id="tdAddress2" runat="server"><asp:Label ID="_lblAddress2" runat="server" Text='<%#Eval("reg_Address2")%>'></asp:Label></td>

                                                    <td id="tdAddress3" runat="server"><asp:Label ID="_lblAddress3" runat="server" Text='<%#Eval("reg_Address3")%>'></asp:Label></td>

                                                    <td id="tdAddress4" runat="server"><asp:Label ID="_lblAddress4" runat="server" Text='<%#Eval("reg_Address4")%>'></asp:Label></td>

                                                    <td id="tdCity" runat="server"><asp:Label ID="_lblCity" runat="server" Text='<%#Eval("reg_City")%>'></asp:Label></td>

                                                    <td id="tdState" runat="server"><asp:Label ID="_lblState" runat="server" Text='<%#Eval("reg_State")%>'></asp:Label></td>

                                                    <td id="tdCountry" runat="server"><asp:Label ID="_lblCountry" runat="server" Text='<%# bindCountry(Eval("reg_Country").ToString())%>'></asp:Label></td>

                                                    <td id="tdPostal" runat="server"><asp:Label ID="_lblPostal" runat="server" Text='<%#Eval("reg_PostalCode")%>'></asp:Label></td>

                                                    <td id="tdRCountry" runat="server"><asp:Label ID="_lblRCountry" runat="server" Text='<%# bindCountry(Eval("reg_RCountry").ToString())%>'></asp:Label></td>

                                                    <td id="tdTel" runat="server"><asp:Label ID="_lblTel" runat="server"><%# bindPhoneNo(Eval("reg_Telcc").ToString(), Eval("reg_Telac").ToString(), Eval("reg_Tel").ToString(), "Tel")%></asp:Label></td>

                                                    <td id="tdMobile" runat="server"><asp:Label ID="_lblMobile" runat="server"><%# bindPhoneNo(Eval("reg_Mobcc").ToString(), Eval("reg_Mobac").ToString(), Eval("reg_Mobile").ToString(), "Mob")%></asp:Label></td>

                                                    <td id="tdFax" runat="server"><asp:Label ID="_lblFax" runat="server"><%# bindPhoneNo(Eval("reg_Faxcc").ToString(), Eval("reg_Faxac").ToString(), Eval("reg_Fax").ToString(), "Fax")%></asp:Label></td>

                                                    <td id="tdEmail" runat="server"><asp:Label ID="_lblEmail" runat="server" Text='<%#Eval("reg_Email")%>'></asp:Label></td>

                                                    <td id="tdAffiliation" runat="server"><asp:Label ID="_lblAffiliation" runat="server" Text='<%# bindAffiliation(Eval("reg_Affiliation").ToString())%>'></asp:Label></td>

                                                    <td id="tdDietary" runat="server"><asp:Label ID="_lblDietary" runat="server" Text='<%# bindDietary(Eval("reg_Dietary").ToString())%>'></asp:Label></td>

                                                    <td id="tdNationality" runat="server"><asp:Label ID="_lblNationality" runat="server" Text='<%#Eval("reg_Nationality")%>'></asp:Label></td>

                                                    <td id="tdAge" runat="server"><asp:Label ID="_lblAge" runat="server" Text='<%#Eval("reg_Age")%>'></asp:Label></td>

                                                    <td id="tdDOB" runat="server"><asp:Label ID="_lblDOB" runat="server" Text='<%#Eval("reg_DOB")%>'></asp:Label></td>

                                                    <td id="tdGender" runat="server"><asp:Label ID="_lblGender" runat="server" Text='<%#Eval("reg_Gender")%>'></asp:Label></td>

                                                    <td id="tdMembershipNo" runat="server"><asp:Label ID="_lblMembershipNo" runat="server" Text='<%#Eval("reg_Membershipno")%>'></asp:Label></td>

                                                    <td id="tdAdditional4" runat="server"><asp:Label ID="_lblAdditional4" runat="server" Text='<%#Eval("reg_Additional4")%>'></asp:Label></td>

                                                    <td id="tdAdditional5" runat="server"><asp:Label ID="_lblAdditional5" runat="server" Text='<%#Eval("reg_Additional5")%>'></asp:Label></td>

                                                    <td id="tdVName" runat="server"><asp:Label ID="_lblVName" runat="server" Text='<%#Eval("reg_vName")%>'></asp:Label></td>

                                                    <td id="tdVDOB" runat="server"><asp:Label ID="_lblVDOB" runat="server" Text='<%#Eval("reg_vDOB")%>'></asp:Label></td>

                                                    <td id="tdVPass" runat="server"><asp:Label ID="_lblVPass" runat="server" Text='<%#Eval("reg_vPassno")%>'></asp:Label></td>

                                                    <td id="tdVPassExpiry" runat="server"><asp:Label ID="_lblVPassExpiry" runat="server" Text='<%#Eval("reg_vPassexpiry")%>'></asp:Label></td>

                                                    <td id="tdVCountry" runat="server"><asp:Label ID="_lblVCountry" runat="server" Text='<%# bindCountry(Eval("reg_vCountry").ToString())%>'></asp:Label></td>

                                                    <td id="tdUDF_CName" runat="server"><asp:Label ID="_lblUDF_CName" runat="server" Text='<%#Eval("UDF_CName")%>'></asp:Label></td>

                                                    <td id="tdUDF_DelegateType" runat="server"><asp:Label ID="_lblUDF_DelegateType" runat="server" Text='<%#Eval("UDF_DelegateType")%>'></asp:Label></td>

                                                    <td id="tdUDF_ProfCategory" runat="server"><asp:Label ID="_lblUDF_ProfCategory" runat="server"><%#Eval("UDF_ProfCategory")%> <%#Eval("UDF_ProfCategoryOther")%></asp:Label></td>

                                                    <td id="tdUDF_CPcode" runat="server"><asp:Label ID="_lblUDF_CPcode" runat="server" Text='<%#Eval("UDF_CPcode")%>'></asp:Label></td>

                                                    <td id="tdUDF_CLDepartment" runat="server"><asp:Label ID="_lblUDF_CLDepartment" runat="server" Text='<%#Eval("UDF_CLDepartment")%>'></asp:Label></td>

                                                    <td id="tdUDF_CAddress" runat="server"><asp:Label ID="_lblUDF_CAddress" runat="server" Text='<%#Eval("UDF_CAddress")%>'></asp:Label></td>

                                                    <td id="tdUDF_CLCompany" runat="server"><asp:Label ID="_lblUDF_CLCompany" runat="server"><%#Eval("UDF_CLCompany")%><%#Eval("UDF_CLCompanyOther")%></asp:Label></td>

                                                    <td id="tdUDF_CCountry" runat="server"><asp:Label ID="_lblUDF_CCountry" runat="server" Text='<%# bindCountry(Eval("UDF_CCountry").ToString())%>'></asp:Label></td>

                                                    <td id="tdSupName" runat="server"><asp:Label ID="_lblSupName" runat="server" Text='<%#Eval("reg_SupervisorName")%>'></asp:Label></td>

                                                    <td id="tdSupDesignation" runat="server"><asp:Label ID="_lblSupDesignation" runat="server" Text='<%#Eval("reg_SupervisorDesignation")%>'></asp:Label></td>

                                                    <td id="tdSupContact" runat="server"><asp:Label ID="_lblSupContact" runat="server" Text='<%#Eval("reg_SupervisorContact")%>'></asp:Label></td>

                                                    <td id="tdSupEmail" runat="server"><asp:Label ID="_lblSupEmail" runat="server" Text='<%#Eval("reg_SupervisorEmail")%>'></asp:Label></td>
                                                </tr>
                                            <%--</table>--%>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                            </div>
                        </div>

                        <table width="100%">
                            <tr>
                                <td colspan="3" style="border-top:0px solid #C8C8C8;">&nbsp;</td>
                            </tr>
                        </table>
                    </div>

                    <div class="table-responsive col-lg-offset-1 col-md-offset-1">
                        <asp:Label runat="server" ID="lblAccompanyingPersonList" Visible="false"></asp:Label>
                    </div>

                    <br />
                    <div id="rcontent" runat="server">
                        <h3 class="ConfrmHeader">Congress Selection</h3>
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8  col-xs-12 ConfItemPanel ">
                                <div class="table-responsive">
                                    <div id="accordion" class="accordion">
                                        <div id="divShowOrderList" runat="server"></div>
                                    </div>
                                    <br />
                                    <div id="accordion" class="accordion">
                                        <div id="divShowPendingOrderList" runat="server"></div>
                                        <div class="row" style="padding-top:20px;">
                                            <div class="col-lg-offset-9 col-lg-3" >
                                                <asp:Button runat="server" ID="btnSubmit" CssClass="btn MainButton btn-block" Text="Proceed" OnClick="btnSubmit_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Label ID="lblGrandTotal" runat="server" Visible="false"></asp:Label>
                            </div>
                        </div>
                        <br />
 
                        <table width="100%">
                            <tr>
                                <td colspan="3" style="border-top:0px solid #C8C8C8;">&nbsp;</td>
                            </tr>
                        </table>
 
                        <div class="clear"></div>

                        <%--<div class="row">
                            <h3 class="ConfrmHeader">Payment Mode</h3>
                            <div class="col-md-8  col-xs-12 ConfItemPanel ">
                                <asp:Label ID="lblmethod" runat="server" Text="" Font-Bold="true"></asp:Label>
                                <asp:HiddenField ID="hfpaymethod" runat="server" Value="0" />
                            </div>
                        </div>--%>
                    </div>

                    <%--<p id="stepcommand" class="txtmiddle">
                        <asp:Button ID="btnPrev" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="PrevClick" Visible="false"
                            Height="47px" Width="131px" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary"
                            OnClick="NextClick" CausesValidation = "true" Height="47px" Width="131px" />
                    </p>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>