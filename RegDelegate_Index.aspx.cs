﻿using Corpit.BackendMaster;
using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegDelegate_Index : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string others_value = "Others";
    static string other_value = "Other";

    private static string[] checkingFJShowName = new string[] { "Food Japan" };
    private static string[] checkingTranportShowName = new string[] { "TRANSPORTA 2019" };
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                //checkIsReLogin();//***[added on 5-11-2018 th]

                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string flowID = cFun.DecryptValue(urlQuery.FlowID);

                setListHeader(flowid, showid);

                bindDelegateList(groupid, showid, flowID);
                //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
                RegGroupObj rgg = new RegGroupObj(fn);
                rgg.updateGroupCurrentStep(urlQuery);

                insertLogFlowAction(groupid, "", rlgobj.actview, urlQuery);

                /*check tb_site_flow_master table to display btnConfChange button visibility according to flow master settings 
                (if value is YesOrNo.Yes then button must be show to enable change selected conference quantity)*/
                FlowControler flwController = new FlowControler(fn);
                FlowMaster flwConfig = flwController.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
                if (flwConfig.FlowLimitDelegateQty == YesOrNo.Yes)
                {
                    btnConfChange.Visible = true;
                }
                else
                {
                    btnConfChange.Visible = false;
                }

                duplicateQuestionnaire(urlQuery);//*

                LoadNextButtonTextSetting();//***added on 9-11-2018
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);
                if (checkingFJShowName.Contains(shw.SHW_Name))
                {
                    btnNext.Text = "Add New Member | 追加登録";
                }
                if (checkingTranportShowName.Contains(shw.SHW_Name))
                {
                    btnNext.Text = "Add Child";
                }

                }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region checkIsReLogin [added on 5-11-2018 th]
    private void checkIsReLogin()
    {
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            FlowControler fCtrl = new FlowControler(fn);
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string regno = cFun.DecryptValue(urlQuery.DelegateID);
            string currentstage = cFun.DecryptValue(urlQuery.CurrIndex);
            if (!string.IsNullOrEmpty(showid))
            {
                if (Session["RegStatus"] != null)
                {
                    bool isSuccess = Session["RegStatus"].ToString() == "1" ? true : true;// false;
                    if (isSuccess)
                    {
                        string page = string.Empty;
                        string step = "";
                        Dictionary<string, string> nValues = fCtrl.GetCurrentReloginRoute(flowid, currentstage);
                        if (nValues.Count > 0)
                        {
                            page = nValues["nURL"].ToString();
                            step = nValues["nStep"].ToString();
                        }
                        string grpNum = "";
                        grpNum = groupid;
                        string route = fCtrl.MakeFullURL(page, flowid, showid, grpNum, step, regno);
                        Response.Redirect(route);
                    }
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    private void LoadNextButtonTextSetting()
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        string nextButtonText = btnNext.Text.Trim();
        string[] strText = nextButtonText.Split(' ');
        if (strText.Count() > 1)
        {
            string pageTitle = flwObj.CurrIndexTiTle;
            //string[] strpageTitle = pageTitle.Split(' ');
            //if (strpageTitle.Count() > 0 && strpageTitle.Count() <= 2)
            if(pageTitle.Contains("Delegate"))
            {
                btnNext.Text = nextButtonText.Replace("Member", "Delegate");
            }
            else if (pageTitle.Contains("Attendee"))
            {
                btnNext.Text = nextButtonText.Replace("Member", "Attendee");
            }
        }
    }
    #endregion

    #region setListHeader (set table header visibility and header text dynamically according to the settings of tb_Form table where form_type='D')
    protected void setListHeader(string flowid, string showid)
    {
        DataSet ds = new DataSet();

        #region Delegate
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForDelegate();

        if (ds.Tables[0].Rows.Count > 0)
        {
            divList.Visible = true;
            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
            {
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);


                    if (isshow == 1)
                    {
                        trSalutation.Visible = true;
                        lblSal.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSalutation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trFName.Visible = true;
                        lblFName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trFName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trLName.Visible = true;
                        lblLName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trLName.Visible = false;
                    }

                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trOName.Visible = true;
                        lblOName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trOName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trPassno.Visible = true;
                        lblPassNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trPassno.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trIsReg.Visible = true;
                        lblIsReg.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trIsReg.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trRegSpecific.Visible = true;
                        lblRegSpecific.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trRegSpecific.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trIDNo.Visible = true;
                        lblIDNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trIDNo.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDesignation.Visible = true;
                        lblDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDesignation.Visible = false;
                    }

                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trProfession.Visible = true;
                        lblProfession.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trProfession.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDept.Visible = true;
                        lblDept.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDept.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trOrg.Visible = true;
                        lblOrg.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trOrg.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trInstitution.Visible = true;
                        lblInstitution.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trInstitution.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress1.Visible = true;
                        lblAddress1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress1.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress2.Visible = true;
                        lblAddress2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress2.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress3.Visible = true;
                        lblAddress3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress3.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress4.Visible = true;
                        lblAddress4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCity.Visible = true;
                        lblCity.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCity.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trState.Visible = true;
                        lblState.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trState.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trPostal.Visible = true;
                        lblPostal.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trPostal.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCountry.Visible = true;
                        lblCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trRCountry.Visible = true;
                        lblRCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trRCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trTel.Visible = true;
                        lblTel.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trTel.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trMobile.Visible = true;
                        lblMobile.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trMobile.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trFax.Visible = true;
                        lblFax.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trFax.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trEmail.Visible = true;
                        lblEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trEmail.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAffiliation.Visible = true;
                        lblAffiliation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAffiliation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDietary.Visible = true;
                        lblDietary.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDietary.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trNationality.Visible = true;
                        lblNationality.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trNationality.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAge.Visible = true;
                        lblAge.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAge.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDOB.Visible = true;
                        lblDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDOB.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGender.Visible = true;
                        lblGender.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGender.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trMembershipNo.Visible = true;
                        lblMembershipNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trMembershipNo.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAdditional4.Visible = true;
                        lblAdditional4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAdditional4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAdditional5.Visible = true;
                        lblAdditional5.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAdditional5.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVName.Visible = true;
                        lblVName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVDOB.Visible = true;
                        lblVDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVDOB.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVPass.Visible = true;
                        lblVPass.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVPass.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVPassExpiry.Visible = true;
                        lblVPassExpiry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVPassExpiry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVPassIssueDate.Visible = true;
                        lblVPassIssueDate.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVPassIssueDate.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVEmbarkation.Visible = true;
                        lblVEmbarkation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVEmbarkation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVArrivalDate.Visible = true;
                        lblVArrivalDate.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVArrivalDate.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVCountry.Visible = true;
                        lblVCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVCountry.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CName.Visible = true;
                        lblUDF_CName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CName.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_DelegateType.Visible = true;
                        lblUDF_DelegateType.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_DelegateType.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_ProfCategory.Visible = true;
                        lblUDF_ProfCategory.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_ProfCategory.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CPcode.Visible = true;
                        lblUDF_CPcode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CPcode.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CLDepartment.Visible = true;
                        lblUDF_CLDepartment.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CLDepartment.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CAddress.Visible = true;
                        lblUDF_CAddress.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CAddress.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CLCompany.Visible = true;
                        lblUDF_CLCompany.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CLCompany.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CCountry.Visible = true;
                        lblUDF_CCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CCountry.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupName.Visible = true;
                        lblSupName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupName.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupDesignation.Visible = true;
                        lblSupDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupDesignation.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupContact.Visible = true;
                        lblSupContact.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupContact.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupEmail.Visible = true;
                        lblSupEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupEmail.Visible = false;
                    }
                }
            }
        }
        else
        {
            divList.Visible = false;
        }
        #endregion
    }
    #endregion

    #region bindDelegateList (bind delegate list to repeater control (rptItem) according to current RegGroupID)
    private void bindDelegateList(string groupID, string showid, string flowid)
    {
        RegDelegateObj rgd = new RegDelegateObj(fn);
        DataTable dt = new DataTable();
        dt = rgd.getRegDelegateByGroupID(groupID, showid);

        if (dt.Rows.Count > 0)
        {
            btnCheckout.Visible = true;
            divList.Visible = true;
            rptItem.DataSource = dt;
            rptItem.DataBind();

            #region check Member Count(added on 21-8-2018)
            int memberCount = dt.Rows.Count;
            FlowControler flwController = new FlowControler(fn);
            FlowMaster flwConfig = flwController.GetFlowMasterConfig(cFun.DecryptValue(flowid));
            if (memberCount < flwConfig.GroupMinMember)
            {
                //btnNext.Visible = true;
                btnCheckout.Visible = false;
            }
            else
            {
                //btnNext.Visible = false;
                btnCheckout.Visible = true;
            }
            if (memberCount >= flwConfig.GroupMaxMember)
            {
                btnNext.Visible = false;
            }
            else
            {
                btnNext.Visible = true;
            }
            #endregion
        }
        else
        {
            btnCheckout.Visible = false;
            divList.Visible = false;
        }
    }
    #endregion

    #region rptitemdatabound (set repeater item cells' visibility dynamically according to the settings of tb_Form table where form_type='D')
    protected void rptitemdatabound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                // This event is raised for the header, the footer, separators, and items.
                // Execute the following logic for Items and Alternating Items.
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.HtmlControls.HtmlTableCell tdSalutation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSalutation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdFName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdFName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdLName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdLName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdOName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdOName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdPassNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPassno");
                    System.Web.UI.HtmlControls.HtmlTableCell tdisReg = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdIsReg");
                    System.Web.UI.HtmlControls.HtmlTableCell tdregSpecific = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdRegSpecific");
                    System.Web.UI.HtmlControls.HtmlTableCell tdIDNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdIDNo");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDesignation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDesignation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdProfession = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdProfession");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDepartment = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDept");
                    System.Web.UI.HtmlControls.HtmlTableCell tdOrganization = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdOrg");
                    System.Web.UI.HtmlControls.HtmlTableCell tdInstitution = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdInstitution");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress1 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress1");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress2 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress2");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress3 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress3");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress4 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress4");
                    System.Web.UI.HtmlControls.HtmlTableCell tdCity = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCity");
                    System.Web.UI.HtmlControls.HtmlTableCell tdState = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdState");
                    System.Web.UI.HtmlControls.HtmlTableCell tdPostalCode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPostal");
                    System.Web.UI.HtmlControls.HtmlTableCell tdCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdRCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdRCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdTel = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdTel");
                    System.Web.UI.HtmlControls.HtmlTableCell tdMobile = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdMobile");
                    System.Web.UI.HtmlControls.HtmlTableCell tdFax = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdFax");
                    System.Web.UI.HtmlControls.HtmlTableCell tdEmail = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdEmail");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAffiliation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAffiliation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDietary = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDietary");
                    System.Web.UI.HtmlControls.HtmlTableCell tdNationality = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdNationality");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAge = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAge");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDOB = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDOB");
                    System.Web.UI.HtmlControls.HtmlTableCell tdGender = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdGender");
                    System.Web.UI.HtmlControls.HtmlTableCell tdMembershipNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdMembershipNo");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAdditional4 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAdditional4");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAdditional5 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAdditional5");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVDOB = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVDOB");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVPassNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVPass");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVPassExpiry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVPassExpiry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVPassIssueDate = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVPassIssueDate");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVEmbarkation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVEmbarkation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVArrivalDate = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVArrivalDate");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_DelegateType = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_DelegateType");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_ProfCategory = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_ProfCategory");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CPcode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CPcode");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CLDepartment = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CLDepartment");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CAddress = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CAddress");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CLCompany = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CLCompany");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupDesignation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupDesignation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupContact = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupContact");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupEmail = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupEmail");

                    DataSet ds = new DataSet();
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    ds = frmObj.getDynFormForDelegate();

                    for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                    {
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);


                            if (isshow == 1)
                            {
                                tdSalutation.Visible = true;
                            }
                            else
                            {
                                tdSalutation.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdFName.Visible = true;
                            }
                            else
                            {
                                tdFName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdLName.Visible = true;

                            }
                            else
                            {
                                tdLName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdOName.Visible = true;
                            }
                            else
                            {
                                tdOName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdPassNo.Visible = true;
                            }
                            else
                            {
                                tdPassNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdisReg.Visible = true;
                            }
                            else
                            {
                                tdisReg.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdregSpecific.Visible = true;
                            }
                            else
                            {
                                tdregSpecific.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdIDNo.Visible = true;
                            }
                            else
                            {
                                tdIDNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDesignation.Visible = true;
                            }
                            else
                            {
                                tdDesignation.Visible = false;
                            }

                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdProfession.Visible = true;
                            }
                            else
                            {
                                tdProfession.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDepartment.Visible = true;
                            }
                            else
                            {
                                tdDepartment.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdOrganization.Visible = true;
                            }
                            else
                            {
                                tdOrganization.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdInstitution.Visible = true;
                            }
                            else
                            {
                                tdInstitution.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress1.Visible = true;
                            }
                            else
                            {
                                tdAddress1.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress2.Visible = true;
                            }
                            else
                            {
                                tdAddress2.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress3.Visible = true;
                            }
                            else
                            {
                                tdAddress3.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress4.Visible = true;
                            }
                            else
                            {
                                tdAddress4.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdCity.Visible = true;
                            }
                            else
                            {
                                tdCity.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdState.Visible = true;
                            }
                            else
                            {
                                tdState.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdPostalCode.Visible = true;
                            }
                            else
                            {
                                tdPostalCode.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdCountry.Visible = true;
                            }
                            else
                            {
                                tdCountry.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdRCountry.Visible = true;
                            }
                            else
                            {
                                tdRCountry.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdTel.Visible = true;
                            }
                            else
                            {
                                tdTel.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdMobile.Visible = true;
                            }
                            else
                            {
                                tdMobile.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdFax.Visible = true;
                            }
                            else
                            {
                                tdFax.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdEmail.Visible = true;
                            }
                            else
                            {
                                tdEmail.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAffiliation.Visible = true;
                            }
                            else
                            {
                                tdAffiliation.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDietary.Visible = true;
                            }
                            else
                            {
                                tdDietary.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdNationality.Visible = true;
                            }
                            else
                            {
                                tdNationality.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAge.Visible = true;
                            }
                            else
                            {
                                tdAge.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDOB.Visible = true;
                            }
                            else
                            {
                                tdDOB.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdGender.Visible = true;
                            }
                            else
                            {
                                tdGender.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdMembershipNo.Visible = true;
                            }
                            else
                            {
                                tdMembershipNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAdditional4.Visible = true;
                            }
                            else
                            {
                                tdAdditional4.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAdditional5.Visible = true;
                            }
                            else
                            {
                                tdAdditional5.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVName.Visible = true;
                            }
                            else
                            {
                                tdVName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVDOB.Visible = true;
                            }
                            else
                            {
                                tdVDOB.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVPassNo.Visible = true;
                            }
                            else
                            {
                                tdVPassNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVPassExpiry.Visible = true;
                            }
                            else
                            {
                                tdVPassExpiry.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVPassIssueDate.Visible = true;
                            }
                            else
                            {
                                tdVPassIssueDate.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVEmbarkation.Visible = true;
                            }
                            else
                            {
                                tdVEmbarkation.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVArrivalDate.Visible = true;
                            }
                            else
                            {
                                tdVArrivalDate.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVCountry.Visible = true;
                            }
                            else
                            {
                                tdVCountry.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CName.Visible = true;
                            }
                            else
                            {
                                tdUDF_CName.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_DelegateType.Visible = true;
                            }
                            else
                            {
                                tdUDF_DelegateType.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_ProfCategory.Visible = true;
                            }
                            else
                            {
                                tdUDF_ProfCategory.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CPcode.Visible = true;
                            }
                            else
                            {
                                tdUDF_CPcode.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CLDepartment.Visible = true;
                            }
                            else
                            {
                                tdUDF_CLDepartment.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CAddress.Visible = true;
                            }
                            else
                            {
                                tdUDF_CAddress.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CLCompany.Visible = true;
                            }
                            else
                            {
                                tdUDF_CLCompany.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CCountry.Visible = true;
                            }
                            else
                            {
                                tdUDF_CCountry.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupName.Visible = true;
                            }
                            else
                            {
                                tdSupName.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupDesignation.Visible = true;
                            }
                            else
                            {
                                tdSupDesignation.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupContact.Visible = true;
                            }
                            else
                            {
                                tdSupContact.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupEmail.Visible = true;
                            }
                            else
                            {
                                tdSupEmail.Visible = false;
                            }
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region rptitemcommand (repeater controls item command 'Edit' event & get next route and redirect to next page with 'delegate id' according to the site flow settings (tb_site_flow table))
    protected void rptitemcommand(object source, RepeaterCommandEventArgs e)
    {
        string regno = e.CommandArgument.ToString();

        if (e.CommandName == "edit")
        {
            try
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                FlowControler flwObj = new FlowControler(fn, urlQuery);
                string showID = urlQuery.CurrShowID;
                string page = flwObj.NextStepURL();
                string step = flwObj.NextStep;
                string FlowID = flwObj.FlowID;
                string grpNum = "";
                grpNum = urlQuery.GoupRegID;
                string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                Response.Redirect(route);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
        if (e.CommandName == "deleteDelegate")
        {
            try
            {
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                FlowControler flwObj = new FlowControler(fn, urlQuery);
                string showID = cFun.DecryptValue(urlQuery.CurrShowID);
                string FlowID = cFun.DecryptValue(flwObj.FlowID);
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                RegDelegateObj rgdObj = new RegDelegateObj(fn);
                rgdObj.deleteDelegate(regno, groupid, showID);
                OrderControler oCtrl = new OrderControler(fn);
                oCtrl.DeleteOrderByDelegateRegno(groupid, regno);
                bindDelegateList(groupid, showID, FlowID);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
    #endregion

    #region btnNext_Click (get next route and redirect to next page according to the site flow settings (tb_site_flow table))
    protected void btnNext_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        string showID = urlQuery.CurrShowID;
        string page = flwObj.NextStepURL();
        string step = flwObj.NextStep;
        string FlowID = flwObj.FlowID;
        string grpNum = "";
        grpNum = urlQuery.GoupRegID;

        insertLogFlowAction(cFun.DecryptValue(grpNum), "", rlgobj.actnext, urlQuery);

        string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step);
        Response.Redirect(route);
    }
    #endregion

    #region btnConfChange_Click (redirect to Conference.aspx to change selected conference quantity)
    protected void btnConfChange_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn);
        string showID = urlQuery.CurrShowID;
        string flowID = cFun.DecryptValue(urlQuery.FlowID);
        flwObj.getPreviousRoute(flowID, cFun.DecryptValue(urlQuery.CurrIndex));
        string page = flwObj.CurrIndexModule;
        string step = flwObj.CurrIndex;
        string FlowID = flowID;
        string grpNum = "";
        grpNum = urlQuery.GoupRegID;

        insertLogFlowAction(cFun.DecryptValue(grpNum), "", "Click change conference items quantity", urlQuery);

        string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step);
        Response.Redirect(route);
    }
    #endregion

    #region btnCheckout_Click (update registration status for all delegates for current RegGroupID to '1' & get checkout route and redirect to the checkout page according to the site flow settings (tb_site_flow table))
    protected void btnCheckout_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        if (!string.IsNullOrEmpty(showid))
        {
            FlowControler flwController = new FlowControler(fn);
            FlowMaster flwConfig = flwController.GetFlowMasterConfig(flowid);
            string confSkip = flwConfig.FlowConfSkip;//***added on 24-10-2018

            try
            {
                //StatusSettings stuSettings = new StatusSettings(fn);
                if (flwConfig.FlowLimitDelegateQty == YesOrNo.Yes)
                {
                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    DataTable dt = new DataTable();
                    dt = rgd.getRegDelegateByGroupID(cFun.DecryptValue(urlQuery.GoupRegID), showid);

                    OrderControler oControl = new OrderControler(fn);
                    int allOrderQty = oControl.getAllOrderQtyByGroupID(cFun.DecryptValue(urlQuery.GoupRegID));
                    if (dt.Rows.Count > allOrderQty)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Members count is greater than selected conference quantities.');", true);
                    }
                    else if (dt.Rows.Count < allOrderQty)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Members count is less than selected conference quantities.');", true);
                    }
                    else
                    {
                        FlowControler flwObj = new FlowControler(fn, urlQuery);
                        string showID = urlQuery.CurrShowID;
                        string page = flwObj.NextCheckoutURL();
                        string step = flwObj.NextCheckOutStep;
                        string FlowID = flwObj.FlowID;
                        string grpNum = "";
                        grpNum = urlQuery.GoupRegID;

                        //RegDelegateObj rgd = new RegDelegateObj(fn);
                        //rgd.updateDelegateRegStatus(grpNum, stuSettings.Success);

                        insertLogFlowAction(cFun.DecryptValue(grpNum), "", rlgobj.actcheckout, urlQuery);

                        string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step);
                        Response.Redirect(route);
                    }
                }
                else
                {
                    #region ***changed on 24-10-2018 (check if order is not tally with member count)
                    if (confSkip == YesOrNo.Yes)
                    {
                        FlowControler flwObj = new FlowControler(fn, urlQuery);
                        string showID = urlQuery.CurrShowID;
                        string page = flwObj.NextCheckoutURL();
                        string step = flwObj.NextCheckOutStep;
                        string FlowID = flwObj.FlowID;
                        string grpNum = "";
                        grpNum = urlQuery.GoupRegID;

                        //RegDelegateObj rgd = new RegDelegateObj(fn);
                        //rgd.updateDelegateRegStatus(grpNum, stuSettings.Success);

                        insertLogFlowAction(cFun.DecryptValue(grpNum), "", rlgobj.actcheckout, urlQuery);

                        string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step);
                        Response.Redirect(route);
                    }
                    else
                    {
                        RegDelegateObj rgd = new RegDelegateObj(fn);
                        DataTable dt = new DataTable();
                        dt = rgd.getRegDelegateByGroupID(cFun.DecryptValue(urlQuery.GoupRegID), showid);

                        OrderControler oControl = new OrderControler(fn);
                        int allOrderQty = oControl.getAllOrderQtyByGroupID(cFun.DecryptValue(urlQuery.GoupRegID));
                        if (dt.Rows.Count > allOrderQty)//***check if order is not tally with member count
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please make sure to select the conference for all member(s).');", true);
                        }
                        else
                        {
                            FlowControler flwObj = new FlowControler(fn, urlQuery);
                            string showID = urlQuery.CurrShowID;
                            string page = flwObj.NextCheckoutURL();
                            string step = flwObj.NextCheckOutStep;
                            string FlowID = flwObj.FlowID;
                            string grpNum = "";
                            grpNum = urlQuery.GoupRegID;

                            //RegDelegateObj rgd = new RegDelegateObj(fn);
                            //rgd.updateDelegateRegStatus(grpNum, stuSettings.Success);

                            insertLogFlowAction(cFun.DecryptValue(grpNum), "", rlgobj.actcheckout, urlQuery);

                            string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step);
                            Response.Redirect(route);
                        }
                    }
                    #endregion
                }
            }
            catch(Exception ex)
            { }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }
    #endregion

    #region bindName
    public string bindSalutation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getSalutationNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }

                if (name == others_value || name == other_value)
                {
                    name = otherValue;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindProfession(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getProfessionNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDepartment(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getDepartmentNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindOrganisation(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getOrganisationNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindInstitution(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getInstitutionNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindCountry(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                CountryObj conCtr = new CountryObj(fn);
                name = conCtr.getCountryNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindAffiliation(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getAffiliationNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDietary(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getDietaryNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion

    #region bindPhoneNo
    public string bindPhoneNo(string cc, string ac, string phoneno, string type)
    {
        string name = string.Empty;
        bool isShowCC = false, isShowAC = false, isShowPhoneNo = false;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                DataSet ds = new DataSet();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showid;
                frmObj.flowID = flowid;
                ds = frmObj.getDynFormForDelegate();

                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    if (type == "Tel")
                    {
                        #region type="Tel"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                    else if (type == "Mob")
                    {
                        #region type="Mob"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                    else if (type == "Fax")
                    {
                        #region Type="Fax"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                }

                if (isShowCC)
                {
                    name = "+" + cc;
                }
                if (isShowAC)
                {
                    name += " " + ac;
                }
                if (isShowPhoneNo)
                {
                    name += " " + phoneno;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion

    #region duplicateQuestionnaire for who registered in the contact person page as a delegate.
    private void duplicateQuestionnaire(FlowURLQuery urlQuery)
    {
        try
        {
            //Duplicate Group QA to Delegate
            FlowControler flwControl = new FlowControler(fn);
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flwID = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flwID);
            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
            {
                if (flwMasterConfig.isGAutoAttending || flwMasterConfig.isGIsAttendingVisible)
                {
                    DataTable dt = new DataTable();
                    RegGroupObj rgg = new RegGroupObj(fn);
                    dt = rgg.getRegGroupByID(groupid, showid);
                    if (dt.Rows.Count != 0)
                    {
                        string rg_referralcode = dt.Rows[0]["RG_ReferralCode"].ToString();
                        if (!string.IsNullOrEmpty(rg_referralcode) && !string.IsNullOrWhiteSpace(rg_referralcode))
                        {
                            string regno = rg_referralcode;
                            RegDelegateObj rgd = new RegDelegateObj(fn);
                            DataTable dtRegDelegate = rgd.getDataByGroupIDRegno(groupid, regno, showid);
                            if (dtRegDelegate.Rows.Count > 0)
                            {
                                int currentStage = Convert.ToInt32(dtRegDelegate.Rows[0]["reg_Stage"] != DBNull.Value ? dtRegDelegate.Rows[0]["reg_Stage"].ToString() : "0");
                                if (currentStage <= 1)
                                {
                                    QAHelper qaHelp = new QAHelper();
                                    string delegateQAID = qaHelp.GetQAIDfromFlow(showid, flwID, "D");
                                    string groupQAID = qaHelp.GetQAIDfromFlow(showid, flwID, "G");
                                    if (string.IsNullOrEmpty(groupQAID))
                                        groupQAID = qaHelp.GetQAIDfromFlow(showid, flwID, "C");

                                    if (!string.IsNullOrEmpty(groupQAID) && !string.IsNullOrEmpty(delegateQAID))
                                    {
                                        string QALINK = "";
                                        if (groupQAID != delegateQAID) //ifDiffent QAID
                                            QALINK = string.Format(@"rtype=duplicateAnswerDiffQA&qnaireID={0}&&newqnaireID={1}&userID={2}&newuserID={3}", groupQAID, delegateQAID, groupid, regno);
                                        else
                                            QALINK = string.Format(@"rtype=duplicateAnswer&qnaireID={0}&userID={1}&newuserID={2}", groupQAID, groupid, regno);
                                        QAHelper qHelp = new QAHelper();
                                        bool OK = qHelp.DuplicateQA(QALINK);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

}