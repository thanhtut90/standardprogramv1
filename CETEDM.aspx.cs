﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CETEDM : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            if(Request.QueryString["CEI"] != null)
            {
                aRegistrationUrl.HRef = "https://event-reg.biz/registration/eventreg?event=CET2019&CEI=" + Request.QueryString["CEI"].ToString();
            }
        }
    }
}