﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System.Data.SqlClient;
using System.Data;
using Corpit.Registration;
using Corpit.Logging;
using System.Configuration;
using System.Web.UI.HtmlControls;

public partial class Questionairre : System.Web.UI.Page
{
    CommonFuns cFun = new CommonFuns();
    Functionality fn = new Functionality();
    LogActionObj rlgobj = new LogActionObj();
    private static string[] checkingOSEAShowName = new string[] { "OSEA 2018" };
    private static string[] checkingIDEMShowName = new string[] { "IDEM Online Registration" };
    private static string[] checkingHuaweiShowName = new string[] { "Huawei Developer Day 2019" };

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);

            if (Request.Params["TYPE"] != null)
            {
                string type = cFun.DecryptValue(Request.QueryString["TYPE"].ToString());

                if (!string.IsNullOrEmpty(showid))
                {
                    bindFlowNote(showid, urlQuery);//***added on 25-6-2018

                    //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
                    RegGroupObj rgg = new RegGroupObj(fn);
                    rgg.updateGroupCurrentStep(urlQuery);

                    //*update reg_urlFlowID(current flowid) and reg_Stage(current step) of tb_RegDelegate table
                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    rgd.updateDelegateCurrentStep(urlQuery);

                    string qID = getQID(showid, flowid, type);
                    string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                    string regno = cFun.DecryptValue(urlQuery.DelegateID);
                    string route = string.Empty;

                    string questID = getQuestID(showid, flowid, type);
                    string questIDParam = string.Empty;
                    if(!string.IsNullOrEmpty(questID))
                    {
                        questIDParam = "&quest_id=" + questID;
                    }

                    FlowControler flwObj = new FlowControler(fn, urlQuery);
                    FlowControler flwController = new FlowControler(fn);
                    FlowMaster flwConfig = flwController.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));

                    string url = HttpContext.Current.Request.Url.AbsoluteUri;
                    if (!string.IsNullOrEmpty(url))
                    {
                        url = url.Substring(0, url.LastIndexOf("?"));
                        url = url.Substring(0, url.LastIndexOf("/"));
                    }


                    if (flwConfig.FlowLimitDelegateQty == Number.One)
                    {
                        //  route = flwObj.MakeFullURL("http://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath + "/QuestionnaireExit", urlQuery.FlowID, showid, urlQuery.GoupRegID, urlQuery.CurrIndex);
                        route = flwObj.MakeFullURL( url+ "/QuestionnaireExit", urlQuery.FlowID, showid, urlQuery.GoupRegID, urlQuery.CurrIndex);
                        //route = route.Replace("&", "%26");
                    }
                    else
                    {
                        route = flwObj.MakeFullURL(url + "/QuestionnaireExit", urlQuery.FlowID, showid, urlQuery.GoupRegID, urlQuery.CurrIndex, regno);
                        //route = route.Replace("&", "%26");
                    }

                    //***Added for Back Button Click Url(bk_url) - added on 12-7-2018
                    string currentstep = cFun.DecryptValue(urlQuery.CurrIndex);
                    flwObj.getPreviousRoute(flowid, currentstep);
                    string showID = urlQuery.CurrShowID;
                    string page = flwObj.CurrIndexModule;
                    string step = flwObj.CurrIndex;
                    string FlowID = flowid;
                    string grpNum = groupid;// urlQuery.GoupRegID;
                    EmailHelper eHelper = new EmailHelper();
                    string hostsiteurl = eHelper.GetRootURL();
                    string backUrl = hostsiteurl + "/" + flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno, BackendRegType.backendRegType_Delegate);
                    //***Added for Back Button Click Url(bk_url) - added on 12-7-2018

                    string qaUrl = ConfigurationManager.AppSettings["QuestionnaireURL"];
                    //***Changes for OSEA(13-7-2018)
                    ShowControler shwCtr = new ShowControler(fn);
                    Show shw = shwCtr.GetShow(showid);
                    if (checkingOSEAShowName.Contains(shw.SHW_Name))
                    {
                        qaUrl = ConfigurationManager.AppSettings["QuestionnaireURLOSEA"];
                    }
                    //***Changes for OSEA(13-7-2018)

                        string hyperlink = qaUrl + "?qnaire_id=" + qID + "&user_id=" + regno + questIDParam + "&rtn_url=" + Server.UrlEncode(route)
                        + "&bk_url=" + Server.UrlEncode(backUrl);//***Added for Back Button Click Url(bk_url) - added on 12-7-2018
                    this.questionaireFrame.Attributes.Add("src", hyperlink);
                    //string insert = "document.getElementById(\"ContentPlaceHolder1_questionaireFrame\").setAttribute(\"src\", \"  " + hyperlink + "\");";
                    //ScriptManager.RegisterStartupScript(this, GetType(), "", insert, true);

                    insertLogFlowAction(groupid, regno, rlgobj.actview, urlQuery);

                    IDEMSettings(showid);
                    HuaweiSettings(showid);/*added on 29-10-2019*/
                }
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    private string getQID(string showid, string flowid, string type)
    {
        QuestionnaireControler qCtr = new QuestionnaireControler(fn);
        DataTable dt = qCtr.getQID(type, flowid, showid);
        if (dt.Rows.Count > 0)
            return dt.Rows[0]["SQ_QAID"].ToString();
        else
            return "QLST";
    }
    private string getQuestID(string showid, string flowid, string type)
    {
        string questid = string.Empty;
        try
        {
            QuestionnaireControler qCtr = new QuestionnaireControler(fn);
            DataTable dt = qCtr.getQID(type, flowid, showid);
            if (dt.Rows.Count > 0)
            {
                questid = dt.Rows[0]["SQ_QUESID"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["SQ_QUESID"].ToString()) ? dt.Rows[0]["SQ_QUESID"].ToString() : "") : "";
            }
        }
        catch(Exception ex)
        { }

        return questid;
    }

    #region bindFlowNote//***added on 25-6-2018
    private void bindFlowNote(string showid, FlowURLQuery urlQuery)
    {
        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = UpdatePanel1.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            divFooter.Visible = true;
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = UpdatePanel1.FindControl("chk" + ftnObj.note_Type);
                                if (ctrlChk != null)
                                {
                                    CheckBoxList chkFTRCHK = UpdatePanel1.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    ListItem newItem = new ListItem(displayTextTmpt, ftnObj.note_ID);
                                    chkFTRCHK.Items.Add(newItem);
                                }
                            }
                        }
                        else
                        {
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region IDEM added on 
    private void IDEMSettings(string showid)
    {
        ShowControler shwCtr = new ShowControler(fn);
        Show shw = shwCtr.GetShow(showid);
        if (checkingIDEMShowName.Contains(shw.SHW_Name))
        {
            string myScript = "<script type='text/javascript'>var frameListener; \n";
            myScript += "$(window).load(function () {frameListener = setInterval('frameLoaded()', 50);});";
            myScript += "\n function frameLoaded() { var frmHead = $('#ContentPlaceHolder1_questionaireFrame').contents().find('head'); \n";
            myScript += "\n if (frmHead != null) {clearInterval(frameListener); \n";
            myScript += "\n frmHead.append($('<link/>', { rel: 'stylesheet', href: 'https://www.event-reg.biz/defaultbanner/Style/IDEMQADefaultStyle.css', type: 'text/css' })); \n";
            myScript += "\n }}</script> \n";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, false);
            Page.Header.Controls.Add(new LiteralControl(myScript));
        }
    }
    #endregion
    #region Huawei added on 
    private void HuaweiSettings(string showid)
    {
        ShowControler shwCtr = new ShowControler(fn);
        Show shw = shwCtr.GetShow(showid);
        if (checkingHuaweiShowName.Contains(shw.SHW_Name))
        {
            string myScript = "<script type='text/javascript'>var frameListener; \n";
            myScript += "$(window).load(function () {frameListener = setInterval('frameLoaded()', 50);});";
            myScript += "\n function frameLoaded() { var frmHead = $('#ContentPlaceHolder1_questionaireFrame').contents().find('head'); \n";
            myScript += "\n if (frmHead != null) {clearInterval(frameListener); \n";
            myScript += "\n frmHead.append($('<link/>', { rel: 'stylesheet', href: 'https://www.event-reg.biz/defaultbanner/Style/HuaweiQADefaultStyle.css', type: 'text/css' })); \n";
            myScript += "\n }}</script> \n";
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, false);
            Page.Header.Controls.Add(new LiteralControl(myScript));
        }
    }
    #endregion
}