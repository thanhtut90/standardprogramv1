﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reg_Home : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected string showname;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
        {
            Label mpLabel = (Label)Page.Master.FindControl("SiteHeader");
            if (mpLabel != null)
            {
                mpLabel.Text = ReLoginStaticValueClass.regHomeName;
            }

            //***added on 7-9-2018
            string showid = Session["Showid"].ToString();
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            showname = shw.SHW_Name.ToLower();
            //***added on 7-9-2018
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string currentstage = Session["regstage"].ToString();

                //lblHomeMessage.Text = "Welcome to your home!";

                bindMsg(flowid);//***added on 9-10-2018
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMasterReLogin;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion


    #region bindMsg
    private void bindMsg(string flowid)
    {
        FlowControler flwControl = new FlowControler(fn);
        FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
        if (!string.IsNullOrEmpty(flwMasterConfig.FlowReloginIndexIncompletePageMsg))
        {
            divMsg.Visible = false;
            divReloginHomeMsg.Visible = true;
            lblTemplate.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowReloginIndexIncompletePageMsg) ? flwMasterConfig.FlowReloginIndexIncompletePageMsg : "");
        }
        else
        {
            divMsg.Visible = true;
            divReloginHomeMsg.Visible = false;
        }
    }
    #endregion
}