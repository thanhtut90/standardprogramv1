﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Site.Email;
using Corpit.Utilities;
using Corpit.Registration;
public partial class ViewConfirmation : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFuz = new CommonFuns();
    string showName = "Architect 2018_EN";
    string QALINK = @"rtype=requestAnswer&qnaireID=QLST10080&questID=QSTN2505&userID=";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["DID"] != null)
            {

                string showID = cFuz.DecryptValue(Request.Params["SHW"].ToString());
                string regno = cFuz.DecryptValue(Request.Params["DID"].ToString()); 
               
                string rtnFile = CreatePDF(showID, regno);
                ShowPDF.Attributes["src"] = @"http://203.127.83.146/RegistrationPortal/"+ rtnFile;
            }
        }
    }

    private string CreatePDF(string showID, string regNO)
    {
        string rtnFile = "";
        try
        {

            string query = "     select  reg_Fname,reg_lName, reg_country,reg_Designation,reg_passno,reg_OName,r.Country,reg_Additional5,regGroupID,reg_urlFlowID from tb_RegDelegate d left join ref_country r on r.Cty_GUID= reg_country where     SHOWID=@show and Regno=@regno";

            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("show", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);

            SqlParameter spar2 = new SqlParameter("regno", SqlDbType.NVarChar);
            spar2.Value = regNO;
            pList.Add(spar2);

            DataTable dt = fn.GetDatasetByCommand(query, "dsSettings", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                string name = dt.Rows[0]["reg_Fname"].ToString() + " " + dt.Rows[0]["reg_lName"].ToString();
                string job = dt.Rows[0]["reg_passno"].ToString();
                string company = dt.Rows[0]["reg_OName"].ToString();
                string country = dt.Rows[0]["Country"].ToString();
                string isGroupUser = dt.Rows[0]["reg_Additional5"].ToString();
                string groupID = dt.Rows[0]["regGroupID"].ToString();
                string flowID = dt.Rows[0]["reg_urlFlowID"].ToString();
                string jobValue = "";
                if (isGroupUser == "1")
                    jobValue = dt.Rows[0]["reg_Designation"].ToString();
                else
                {
                    // QAHelper qHelp = new QAHelper();
                    // QALINK = QALINK + regNO;
                    // jobValue = qHelp.GetValuefromQA(QALINK);
                    //int indx=  jobValue.IndexOf(" ");
                    // jobValue = jobValue.Substring(indx);
                    // jobValue = jobValue.Trim();
                    QuestionnaireControler qControler = new QuestionnaireControler(fn);
                    jobValue = qControler.getJobTitle(regNO, groupID, flowID, showID);
                    int indx = jobValue.IndexOf(" ");
                    jobValue = jobValue.Substring(indx);
                    jobValue = jobValue.Trim();
                }
                string html = System.IO.File.ReadAllText(Server.MapPath(@"Template\ArchEng.html"));
                 html = html.Replace("%regno%", regNO);
                 html = html.Replace("%Name%", name);
                html = html.Replace("%Job%", jobValue);
                html = html.Replace("%Company%", company);
                html = html.Replace("%Country%", country);
                string barcode = CreatBarcode(showID, regNO);
                html = html.Replace("%Barcode%", barcode);

                if (!string.IsNullOrEmpty(html))
                {
                    CreateFileInSite cFile = new CreateFileInSite(fn);
                    cFile = new CreateFileInSite(fn);
                    cFile.FolderType = "Acknowledge";
                    cFile.ShowID = showID;
                    string path = cFile.CrateFolderInSite();
                    string fileName = regNO + ".pdf";
                    string pdfOutput = cFuz.MakeFullFilePath(path, fileName);
                    string file_pdf = pdfOutput;
                    try
                    {
                        var pdfBytes = (new NReco.PdfGenerator.HtmlToPdfConverter()).GeneratePdf(html);
                        System.IO.File.WriteAllBytes(file_pdf, pdfBytes);
                        rtnFile = "Acknowledge/"+ showName+ "/"+ fileName;
                    }
                    catch (Exception ex) { }
                }
            }

        }
        catch (Exception ex)
        {

        }
        return rtnFile;
    }

    private string CreatBarcode(string showID, string regno)
    {
        jBQCode jData = new jBQCode();
        jData.ShowName = showID;
        jData.BarCode = "ARC-" + regno;
        jData.Regno = regno;
        BarcodeMaker bMaker = new BarcodeMaker();
        string url = bMaker.MakeBarcode(jData);

        return url;
    }
}