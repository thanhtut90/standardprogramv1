﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="QuestionnaireTest.aspx.cs" Inherits="Questionairre" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }

        $(document).ready(function () {
            $('#questionaireFrame2').load(function () {
                $('#questionaireFrame2')
                        .contents().find("body")
                        .html("test iframe");
                $('#questionaireFrame2')
                    .contents().find("head")
                    .append($('<link rel="stylesheet" type="text/css" href="style.css">')
                );
            });
        });
    </script>
    <script src="Scripts/iframeResizer.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <iframe runat="server" id="questionaireFrame2" src="Test.aspx" ></iframe>
    <iframe runat="server" id="questionaireFrame" visible="false" ></iframe>
</asp:Content>

