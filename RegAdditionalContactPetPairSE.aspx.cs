﻿using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Corpit.Promo;
using Corpit.Logging;
using Corpit.BackendMaster;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using Corpit.Questionnaire;

public partial class RegAdditionalContactPetPairSE : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    private static string[] checkingFJShowName = new string[] { "Food Japan" };
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "LoadQAAcordion()", true);
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                bindFlowNote(showid, urlQuery);//***added on 25-6-2018

                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string regno = cFun.DecryptValue(urlQuery.DelegateID);

                if (Request.Params["t"] != null)
                {
                    string admintype = cFun.DecryptValue(Request.QueryString["t"].ToString());
                    if (admintype == BackendStaticValueClass.isAdmin)
                    {
                        btnSave.Visible = false;
                        btnSave.Enabled = false;
                    }
                    else
                    {
                        bindPageLoad(showid, flowid, groupid, regno, urlQuery);
                    }
                }
                else
                {
                    bindPageLoad(showid, flowid, groupid, regno, urlQuery);
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region bindPageLoad
    private void bindPageLoad(string showid, string flowid, string groupid, string regno, FlowURLQuery urlQuery)
    {
        btnSave.Visible = true;
        btnSave.Enabled = true;

        CommonFuns cFun = new CommonFuns();

        if (!String.IsNullOrEmpty(groupid))
        {
            //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
            RegGroupObj rgg = new RegGroupObj(fn);
            rgg.updateGroupCurrentStep(urlQuery);

            //*update reg_urlFlowID(current flowid) and reg_Stage(current step) of tb_RegDelegate table
            RegDelegateObj rgd = new RegDelegateObj(fn);
            rgd.updateDelegateCurrentStep(urlQuery);

            populateUserDetails(groupid, regno, showid, flowid);

            insertLogFlowAction(groupid, regno, rlgobj.actview, urlQuery);
            FlowControler fCon = new FlowControler(fn);
            FlowMaster flwMaster = fCon.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
            if (flwMaster.FlowType == SiteFlowType.FLOW_GROUP)
            {
                PanelShowBackButton.Visible = true;
                PanelWithoutBack.Visible = false;
            }
            else
            {
                PanelWithoutBack.Visible = true;
                PanelShowBackButton.Visible = false;
            }
        }
        else
        {
            Response.Redirect("DefaultRegIndex.aspx");
        }
    }
    #endregion

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region populateUserDetails (get all relevant data according to RegGroupID and Regno from tb_RegDelegate and bind data to the respective controls)
    private void populateUserDetails(string id, string regno, string showid, string flowid)
    {
        if (!string.IsNullOrEmpty(regno))
        {
            string sql = "Select* From tb_RegAdditionalInfoPetFair Where ShowID='" + showid + "' And FlowID='" + flowid
                + "' And Regno='" + regno + "' And RegGroupID='" + id + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                foreach(DataRow dr in dt.Rows)
                {
                    string Type = dr["Type"].ToString();
                    string Name = dr["Name"].ToString();
                    string Position = dr["Position"].ToString();
                    string EmailAddress = dr["Email"].ToString();
                    string Phone = dr["Phone"].ToString();
                    if(Type == "StandCoordinator")
                    {
                        txtFName.Text = Name;
                        txtLName.Text = Position;
                        txtOName.Text = EmailAddress;
                        txtTel.Text = Phone;
                    }
                    if (Type == "CEOManagingDirector")
                    {
                        txtIDNo.Text = Name;
                        txtJobtitle.Text = Position;
                        txtEmail.Text = EmailAddress;
                        txtFax.Text = Phone;
                    }
                    if (Type == "PRMarketingManager")
                    {
                        txtAddress1.Text = Name;
                        txtAddress2.Text = Position;
                        txtAddress3.Text = EmailAddress;
                        txtMobile.Text = Phone;
                    }
                    if (Type == "Spokesperson")
                    {
                        txtCity.Text = Name;
                        txtState.Text = Position;
                        txtPostalcode.Text = EmailAddress;
                        txtSpokenPhone.Text = Phone;
                    }
                }
            }
        }
    }
    #endregion

    #region ClearForm (clear data from controls)
    private void ClearForm()
    {
        txtFName.Text = "";
        txtLName.Text = "";
        txtOName.Text = "";
        txtSpokenPhone.Text = "";
        txtIDNo.Text = "";
        txtJobtitle.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtCity.Text = "";
        txtPostalcode.Text = "";
        txtState.Text = "";
        txtTelcc.Text = "";
        txtTelac.Text = "";
        txtTel.Text = "";
        txtMobcc.Text = "";
        txtMobac.Text = "";
        txtMobile.Text = "";
        txtFaxcc.Text = "";
        txtFaxac.Text = "";
        txtFax.Text = "";
        txtEmail.Text = "";
    }
    #endregion

    #region btnSave_Click (create one record (just RegGroupID and recycle=0) into tb_RegGroup table if current groupid not exist in tb_RegGroup & save/update data into tb_RegDelegate table & get next route and redirect to next page according to the site flow settings (tb_site_flow table))
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string regno = cFun.DecryptValue(urlQuery.DelegateID);
            Boolean isvalidpage = isValidPage(showid, urlQuery);
            if (isvalidpage)
            {
                if (!string.IsNullOrEmpty(showid))
                {
                    int isSuccess = 0;
                    try
                    {
                        isSuccess = RemoveAdditionalPetFair(regno, groupid, flowid, showid);
                        if (isSuccess > 0)
                        {
                            isSuccess = 0;
                            if (!string.IsNullOrEmpty(txtFName.Text.Trim()) || !string.IsNullOrEmpty(txtLName.Text.Trim())
                                 || !string.IsNullOrEmpty(txtOName.Text.Trim()) || !string.IsNullOrEmpty(txtTel.Text.Trim()))
                            {
                                isSuccess = SaveAdditionalPetFair("StandCoordinator", regno, groupid, flowid, showid);
                            }
                            if (!string.IsNullOrEmpty(txtIDNo.Text.Trim()) || !string.IsNullOrEmpty(txtJobtitle.Text.Trim())
                                || !string.IsNullOrEmpty(txtEmail.Text.Trim()) || !string.IsNullOrEmpty(txtFax.Text.Trim()))
                            {
                                isSuccess = SaveAdditionalPetFair("CEOManagingDirector", regno, groupid, flowid, showid);
                            }
                            if (!string.IsNullOrEmpty(txtAddress1.Text.Trim()) || !string.IsNullOrEmpty(txtAddress2.Text.Trim())
                                || !string.IsNullOrEmpty(txtAddress3.Text.Trim()) || !string.IsNullOrEmpty(txtMobile.Text.Trim()))
                            {
                                isSuccess = SaveAdditionalPetFair("PRMarketingManager", regno, groupid, flowid, showid);
                            }
                            if (!string.IsNullOrEmpty(txtCity.Text.Trim()) || !string.IsNullOrEmpty(txtState.Text.Trim())
                                || !string.IsNullOrEmpty(txtPostalcode.Text.Trim()) || !string.IsNullOrEmpty(txtSpokenPhone.Text.Trim()))
                            {
                                isSuccess = SaveAdditionalPetFair("Spokesperson", regno, groupid, flowid, showid);
                            }
                        }

                        if (isSuccess <= 4)
                        {
                            saveRegAdditional(showid, regno, urlQuery);//***added on 25-6-2018

                            string actType = "AdditionalInsert";
                            insertLogFlowAction(groupid, regno, actType, urlQuery);

                            ClearForm();

                            FlowControler flwObj = new FlowControler(fn, urlQuery);
                            string showID = urlQuery.CurrShowID;
                            string page = flwObj.NextStepURL();
                            string step = flwObj.NextStep;
                            string FlowID = flwObj.FlowID;
                            string grpNum = "";
                            grpNum = urlQuery.GoupRegID;
                            string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno, BackendRegType.backendRegType_Delegate);
                            Response.Redirect(route, false);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogGenEmail lggenemail = new LogGenEmail(fn);
                        lggenemail.type = GenLogDefaultValue.errorException;
                        lggenemail.RefNumber = groupid + "," + regno;
                        lggenemail.description = ex.Message;
                        lggenemail.remark = RegClass.typeDeg + cFun.DecryptValue(urlQuery.FlowID);
                        lggenemail.step = cFun.DecryptValue(urlQuery.CurrIndex);
                        lggenemail.writeLog();

                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
        }
    }
    #endregion

    #region RemoveAdditionalPetFair
    private int RemoveAdditionalPetFair(string regno, string regGroupID, string flowid, string showid)
    {
        int isSuccess = 0;
        try
        {
            string sql = "Select * From tb_RegAdditionalInfoPetFair Where ShowID='" + showid + "' And FlowID='" + flowid
                    + "' And Regno='" + regno + "' And RegGroupID='" + regGroupID + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if(dt.Rows.Count > 0)
            {
                string sqlDelete = "Delete From tb_RegAdditionalInfoPetFair Where ShowID='" + showid + "' And FlowID='" + flowid
                       + "' And Regno='" + regno + "' And RegGroupID='" + regGroupID + "'";
                fn.ExecuteSQL(sqlDelete);
            }
            isSuccess = 1;
        }
        catch(Exception)
        { }

        return isSuccess;
    }
    #endregion

    #region SaveAdditionalPetFair
    private int SaveAdditionalPetFair(string type, string regno, string regGroupID, string flowid, string showid)
    {
        int isSuccess = 0;
        try
        {
            string Name = "";
            string Position = "";
            string EmailAddress = "";
            string Phone = "";
            if (type == "StandCoordinator")
            {
                Name = txtFName.Text;
                Position = txtLName.Text;
                EmailAddress = txtOName.Text;
                Phone = txtTel.Text;
            }
            if (type == "CEOManagingDirector")
            {
                Name = txtIDNo.Text;
                Position = txtJobtitle.Text;
                EmailAddress = txtEmail.Text;
                Phone = txtFax.Text;
            }
            if (type == "PRMarketingManager")
            {
                Name = txtAddress1.Text;
                Position = txtAddress2.Text;
                EmailAddress = txtAddress3.Text;
                Phone = txtMobile.Text;
            }
            if (type == "Spokesperson")
            {
                Name = txtCity.Text;
                Position = txtState.Text;
                EmailAddress = txtPostalcode.Text;
                Phone = txtSpokenPhone.Text;
            }
            string sql = "Insert Into tb_RegAdditionalInfoPetFair (Regno, RegGroupID, Name, Position, Email, Phone, Type, ShowID, FlowID)"
                + " Values (@Regno, @RegGroupID, @Name, @Position, @Email, @Phone, @Type, @ShowID, @FlowID)";

            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("Regno", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("RegGroupID", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("Name", SqlDbType.NVarChar);
            SqlParameter spar4 = new SqlParameter("Position", SqlDbType.NVarChar);
            SqlParameter spar5 = new SqlParameter("Email", SqlDbType.NVarChar);
            SqlParameter spar6 = new SqlParameter("Phone", SqlDbType.NVarChar);
            SqlParameter spar7 = new SqlParameter("Type", SqlDbType.NVarChar);
            SqlParameter spar8 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar9 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            spar1.Value = regno;
            spar2.Value = regGroupID;
            spar3.Value = Name;
            spar4.Value = Position;
            spar5.Value = EmailAddress;
            spar6.Value = Phone;
            spar7.Value = type;
            spar8.Value = showid;
            spar9.Value = flowid;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);
            pList.Add(spar4);
            pList.Add(spar5);
            pList.Add(spar6);
            pList.Add(spar7);
            pList.Add(spar8);
            pList.Add(spar9);
            isSuccess = fn.ExecuteSQLWithParameters(sql, pList);
        }
        catch (Exception)
        { }

        return isSuccess;
    }
    #endregion

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    #region setDivCss_TelMobFax
    public void setDivCss_TelMobFax(bool isShowCC, bool isShowAC, bool isShowPhoneNo, string type)
    {
        string name = string.Empty;
        try
        {
            if (type == "Tel")
            {
                #region type="Tel"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");
                    divTelcc.Attributes.Add("class", "col-xs-6");

                    divTelac.Attributes.Remove("class");
                    divTelac.Attributes.Add("class", "col-xs-6");

                    divTelNo.Attributes.Remove("class");
                }
                #endregion
            }
            else if (type == "Mob")
            {
                #region type="Mob"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divMobac.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");

                    divMobac.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");
                    divMobcc.Attributes.Add("class", "col-xs-6");

                    divMobac.Attributes.Remove("class");
                    divMobac.Attributes.Add("class", "col-xs-6");

                    divMobileNo.Attributes.Remove("class");
                }
                #endregion
            }
            else if (type == "Fax")
            {
                #region Type="Fax"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");
                    divFaxcc.Attributes.Add("class", "col-xs-6");

                    divFaxac.Attributes.Remove("class");
                    divFaxac.Attributes.Add("class", "col-xs-6");

                    divFaxNo.Attributes.Remove("class");
                }
                #endregion
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #region btnBack
    protected void btnBack_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            FlowControler flwObj = new FlowControler(fn);
            string flowID = cFun.DecryptValue(urlQuery.FlowID);
            string currentIndx = cFun.DecryptValue(urlQuery.CurrIndex);
            flwObj.getPreviousRoute(flowID, currentIndx);
            string showID = urlQuery.CurrShowID;
            string page = flwObj.CurrIndexModule;
            string step = flwObj.CurrIndex;
            string grpNum = "";
            grpNum = urlQuery.GoupRegID;
            string regno = urlQuery.DelegateID;
            string route = flwObj.MakeFullURL(page, flowID, showID, grpNum, step, regno, BackendRegType.backendRegType_Delegate);
            Response.Redirect(route, false);
        }
    }
    #endregion

    #region isValidPage//***added on 25-6-2018
    protected Boolean isValidPage(string showid, FlowURLQuery urlQuery)
    {
        Boolean isvalid = true;

        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = UpdatePanel1.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = UpdatePanel1.FindControl("chk" + ftnObj.note_Type);
                                Control ctrlLbl = UpdatePanel1.FindControl("lblErr" + ftnObj.note_Type);
                                if (ctrlChk != null && ctrlLbl != null)
                                {
                                    CheckBoxList chkNote = UpdatePanel1.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    Label lblErrNote = UpdatePanel1.FindControl("lblErr" + ftnObj.note_Type) as Label;
                                    if (divFooter.Visible == true)
                                    {
                                        int countTerms = chkNote.Items.Count;
                                        if (countTerms > 0)
                                        {
                                            lblErrNote.Visible = false;
                                            string id = ftnObj.note_ID;
                                            int isSkip = 0;
                                            if (ftnObj != null)
                                            {
                                                isSkip = ftnObj.note_isSkip;
                                            }
                                            ListItem liItem = chkNote.Items.FindByValue(id);
                                            if (liItem != null)
                                            {
                                                if (isSkip == 0)
                                                {
                                                    if (liItem.Selected == false)
                                                    {
                                                        lblErrNote.Visible = true;
                                                        isvalid = false;
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('Please complete the compulsory field(s).');</script>", false);
                                                        return isvalid;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return isvalid;
    }
    #endregion
    #region bindFlowNote//***added on 25-6-2018
    private void bindFlowNote(string showid, FlowURLQuery urlQuery)
    {
        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = UpdatePanel1.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            divFooter.Visible = true;
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = UpdatePanel1.FindControl("chk" + ftnObj.note_Type);
                                if (ctrlChk != null)
                                {
                                    CheckBoxList chkFTRCHK = UpdatePanel1.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    ListItem newItem = new ListItem(displayTextTmpt, ftnObj.note_ID);
                                    chkFTRCHK.Items.Add(newItem);
                                }
                            }
                        }
                        else
                        {
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #region saveRegAdditional//***added on 25-6-2018
    protected void saveRegAdditional(string showid, string regno, FlowURLQuery urlQuery)
    {
        try
        {
            if (divFTRCHK.Visible == true)
            {
                int countTerms = chkFTRCHK.Items.Count;
                //int countCheckedTerms = chkFTRCHK.Items.Cast<ListItem>().Count(li => li.Selected);
                if (countTerms > 0)
                {
                    foreach (ListItem liItem in chkFTRCHK.Items)
                    {
                        string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                        string delegateid = regno;
                        string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
                        string delegateType = BackendRegType.backendRegType_Group;
                        string ownerID = groupid;
                        if (!string.IsNullOrEmpty(delegateid))
                        {
                            delegateType = BackendRegType.backendRegType_Delegate;
                            ownerID = delegateid;
                        }
                        RegAdditionalObj regAddObj = new RegAdditionalObj();
                        regAddObj.ad_ShowID = showid;
                        regAddObj.ad_FlowID = cFun.DecryptValue(urlQuery.FlowID);
                        regAddObj.ad_OwnerID = ownerID;
                        regAddObj.ad_FlowStep = currentStep;
                        regAddObj.ad_DelegateType = delegateType;
                        regAddObj.ad_Value = liItem.Selected == true ? "1" : "0";
                        regAddObj.ad_Type = FlowTemplateNoteType.FooterWithCheckBox;
                        regAddObj.ad_NoteID = cFun.ParseInt(liItem.Value);
                        RegAdditionalControler regAddCtrl = new RegAdditionalControler(fn);
                        regAddCtrl.SaveRegAdditional(regAddObj);
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
}