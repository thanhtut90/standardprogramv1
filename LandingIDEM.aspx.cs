﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LandingIDEM : System.Web.UI.Page
{
    #region Declaration
    static string _pagebanner = "page_banner";
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Boolean isclose = false;// fn.checkClosing();

            if (isclose)
            {
            }
            else
            {
            }

            Session.Clear();
            Session.Abandon();

            pageSet();
        }
    }

    #region pageSet & set Banner Image
    protected void pageSet()
    {
        //string strimage = string.Empty;

        //strimage = fn.GetDataByCommand("Select settings_value from tb_site_settings where settings_name='" + _pagebanner + "'", "settings_value");

        //imgBanner.ImageUrl = "~/" + strimage;
        //imgBanner.Visible = true;
    }
    #endregion

    protected void btnTrade_Click(object sender, EventArgs e)
    {
        //Response.Redirect("~/EventReg?EVent=Visitor Registration");
        Response.Redirect("~/LandingIDEMVisitor");
    }

    protected void btnTradeGroup_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/EventReg?EVent=Visitor Group Registration");
    }

    protected void btnConference_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/EventReg?EVent=Conference Delegate");
    }

    #region Sign In
    protected static string reloginLockPage = "ReloginLock.aspx";
    protected static string _IDEMShowID = "OSH388";
    protected void btnSignIn_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {
                logRegLogin();//***
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string showid = _IDEMShowID;
                Boolean isvalid = isValid();
                if (isvalid)
                {
                    if (!string.IsNullOrEmpty(showid))
                    {
                        string username = cFun.solveSQL(txtEmail.Text.ToString().Trim());
                        string password = cFun.solveSQL(txtPassword.Text.ToString().Trim());
                        string cmdStr = string.Empty;
                        DataTable dt = new DataTable();
                        StatusSettings statusSet = new StatusSettings(fn);
                        InvoiceControler invControler = new InvoiceControler(fn);
                        StatusSettings stuSettings = new StatusSettings(fn);

                        string flowid = string.Empty;
                        bool isGroup = false;
                        dt = getGroupReLoginAuthenData(username, password, showid);
                        if(dt.Rows.Count > 0)
                        {
                            string dbPassword = dt.Rows[0]["Password"] != DBNull.Value ? dt.Rows[0]["Password"].ToString() : "";
                            if (password.ToString() == dbPassword.ToString())
                            {
                                flowid = dt.Rows[0]["RG_urlFlowID"] != DBNull.Value ? dt.Rows[0]["RG_urlFlowID"].ToString() : "";
                                isGroup = true;
                            }
                        }
                        else
                        {
                            dt = getDelegateReLoginAuthenData(username, password, showid);
                            if (dt.Rows.Count > 0)
                            {
                                string dbPassword = dt.Rows[0]["Password"] != DBNull.Value ? dt.Rows[0]["Password"].ToString() : "";
                                if (password.ToString() == dbPassword.ToString())
                                {
                                    flowid = dt.Rows[0]["reg_urlFlowID"] != DBNull.Value ? dt.Rows[0]["reg_urlFlowID"].ToString() : "";
                                    isGroup = false;
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(flowid))
                        {
                            FlowControler flwControl = new FlowControler(fn);
                            bool confExist = flwControl.checkPageExist(flowid, SiteDefaultValue.constantConfName);
                            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
                            //if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                            if(isGroup)
                            {
                                #region group registration
                                dt = getGroupReLoginAuthenData(username, password, showid);
                                if (dt.Rows.Count > 0)
                                {
                                    string groupid = dt.Rows[0]["RegGroupID"].ToString();
                                    Session["Groupid"] = groupid;
                                    Session["Flowid"] = flowid;
                                    Session["Showid"] = showid;
                                    string regstage = dt.Rows[0]["RG_Stage"].ToString();
                                    Session["regstage"] = regstage;
                                    string regstatus = dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                                    Session["RegStatus"] = regstatus;

                                    LoginLog lg = new LoginLog(fn);
                                    string userip = GetUserIP();
                                    lg.loginid = groupid;
                                    lg.loginip = userip;
                                    lg.logintype = LoginLogType.regLogin;
                                    lg.saveLoginLog();

                                    string lateststage = flwControl.GetLatestStep(flowid);

                                    if (!string.IsNullOrEmpty(lateststage) && regstage == lateststage && cFun.ParseInt(regstatus) == statusSet.Success)
                                    {
                                        string invStatus = invControler.getInvoiceStatus(groupid);
                                        if (invStatus == stuSettings.Pending.ToString())
                                        {
                                            string page = ReLoginStaticValueClass.regIndexPage;

                                            bool isUnlock = isRegLoginUnLock();//**
                                            if (isUnlock)
                                            {
                                                string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                                                Response.Redirect(route, false);
                                            }
                                            else
                                            {
                                                Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                            }
                                        }
                                        if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                            || invStatus == RegClass.noInvoice
                                            || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                                        {
                                            string page = ReLoginStaticValueClass.regHomePage;

                                            bool isUnlock = isRegLoginUnLock();//**
                                            if (isUnlock)
                                            {
                                                string route = flwControl.MakeFullURL(page, flowid, showid);
                                                Response.Redirect(route, false);
                                            }
                                            else
                                            {
                                                Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string invStatus = invControler.getInvoiceStatus(groupid);
                                        if (invStatus == RegClass.noInvoice || invStatus == stuSettings.Pending.ToString())
                                        {
                                            string page = ReLoginStaticValueClass.regIndexPage;

                                            bool isUnlock = isRegLoginUnLock();//**
                                            if (isUnlock)
                                            {
                                                string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                                                Response.Redirect(route, false);
                                            }
                                            else
                                            {
                                                Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                            }
                                        }
                                        else if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                            || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                                        {
                                            string page = ReLoginStaticValueClass.regHomePage;

                                            bool isUnlock = isRegLoginUnLock();//**
                                            if (isUnlock)
                                            {
                                                string route = flwControl.MakeFullURL(page, flowid, showid);
                                                Response.Redirect(route, false);
                                            }
                                            else
                                            {
                                                Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    txtEmail.Text = "";
                                    txtPassword.Text = "";
                                    lblerror.Text = "Wrong combination of Registration ID and Email Address";
                                    lblerror.Visible = true;
                                    bool isLock = isRegLoginLock();
                                    if (isLock)
                                    {
                                        Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Individual Registration
                                dt = getDelegateReLoginAuthenData(username, password, showid);
                                if (dt.Rows.Count > 0)
                                {
                                    string groupid = dt.Rows[0]["RegGroupID"].ToString();
                                    string regno = dt.Rows[0]["Regno"].ToString();
                                    Session["Regno"] = regno;
                                    Session["Groupid"] = groupid;
                                    Session["Flowid"] = flowid;
                                    Session["Showid"] = showid;
                                    string regstage = dt.Rows[0]["reg_Stage"].ToString();
                                    Session["regstage"] = regstage;
                                    string regstatus = dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                                    Session["RegStatus"] = regstatus;

                                    LoginLog lg = new LoginLog(fn);
                                    string userip = GetUserIP();
                                    lg.loginid = groupid;
                                    lg.loginip = userip;
                                    lg.logintype = "Registration Login";
                                    lg.saveLoginLog();

                                    string lateststage = flwControl.GetLatestStep(flowid);

                                    if (!string.IsNullOrEmpty(lateststage) && regstage == lateststage && cFun.ParseInt(regstatus) == statusSet.Success)
                                    {
                                        string invStatus = invControler.getInvoiceStatus(regno);
                                        if (invStatus == stuSettings.Pending.ToString())
                                        {
                                            string page = ReLoginStaticValueClass.regIndexPage;

                                            bool isUnlock = isRegLoginUnLock();//**
                                            if (isUnlock)
                                            {
                                                string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                                                Response.Redirect(route, false);
                                            }
                                            else
                                            {
                                                Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                            }
                                        }
                                        else if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                            || invStatus == RegClass.noInvoice
                                            || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                                        {
                                            string page = ReLoginStaticValueClass.regHomePage;

                                            bool isUnlock = isRegLoginUnLock();//**
                                            if (isUnlock)
                                            {
                                                string route = flwControl.MakeFullURL(page, flowid, showid);
                                                Response.Redirect(route, false);
                                            }
                                            else
                                            {
                                                Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string invStatus = invControler.getInvoiceStatus(regno);
                                        if (invStatus == RegClass.noInvoice || invStatus == stuSettings.Pending.ToString())
                                        {
                                            if (confExist)
                                            {
                                                string page = ReLoginStaticValueClass.regIndexPage;

                                                bool isUnlock = isRegLoginUnLock();//**
                                                if (isUnlock)
                                                {
                                                    string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                                                    Response.Redirect(route, false);
                                                }
                                                else
                                                {
                                                    Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                                }
                                            }
                                            else
                                            {
                                                if (cFun.ParseInt(regstatus) == statusSet.Success)
                                                {
                                                    string page = ReLoginStaticValueClass.regHomePage;

                                                    bool isUnlock = isRegLoginUnLock();//**
                                                    if (isUnlock)
                                                    {
                                                        string route = flwControl.MakeFullURL(page, flowid, showid);
                                                        Response.Redirect(route, false);
                                                    }
                                                    else
                                                    {
                                                        Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                                    }
                                                }
                                                else
                                                {
                                                    string page = ReLoginStaticValueClass.regIndexPage;

                                                    bool isUnlock = isRegLoginUnLock();//**
                                                    if (isUnlock)
                                                    {
                                                        string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                                                        Response.Redirect(route, false);
                                                    }
                                                    else
                                                    {
                                                        Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                                    }
                                                }
                                            }
                                        }
                                        else if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                            || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                                        {
                                            string page = ReLoginStaticValueClass.regHomePage;

                                            bool isUnlock = isRegLoginUnLock();//**
                                            if (isUnlock)
                                            {
                                                string route = flwControl.MakeFullURL(page, flowid, showid);
                                                Response.Redirect(route, false);
                                            }
                                            else
                                            {
                                                Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    txtEmail.Text = "";
                                    txtPassword.Text = "";
                                    lblerror.Text = "Wrong combination of Registration ID and Email Address";
                                    lblerror.Visible = true;
                                    bool isLock = isRegLoginLock();
                                    if (isLock)
                                    {
                                        Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                                    }
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            lblerror.Text = "*Sorry, you are not a valid user.*";
                            lblerror.Visible = true;
                            isvalid = false;
                        }
                    }
                    else
                    {
                        Response.Redirect("404.aspx");
                    }
                }
            }
        }
        catch(Exception ex)
        { }
    }
    protected Boolean isValid()
    {
        Boolean isvalid = true;

        if (string.IsNullOrEmpty(txtPassword.Text) || string.IsNullOrWhiteSpace(txtPassword.Text))
        {
            lblerror.Text = "*Please enter your password*";
            lblerror.Visible = true;
            isvalid = false;
        }
        if (string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrWhiteSpace(txtEmail.Text))
        {
            lblerror.Text = "*Please enter your email address*";
            lblerror.Visible = true;
            isvalid = false;
        }

        if(isvalid == true)
        {
            lblerror.Visible = false;
        }
        return isvalid;
    }
    private void logRegLogin()
    {
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'";
            DataTable dtRelogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtRelogin.Rows.Count > 0)
            {
                string sqlReLoginUpdate = "Update tb_LogReLoginLocking Set relogin_attemptTimes=relogin_attemptTimes+1 Where relogin_machineip='" + machineIP + "'";
                fn.ExecuteSQL(sqlReLoginUpdate);
                string AttemptTime = fn.GetDataByCommand("Select relogin_attemptTimes From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'", "relogin_attemptTimes");
                int attempedTime = 0;
                int.TryParse(AttemptTime, out attempedTime);
                if (attempedTime > 5)
                {
                    Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                }
            }
            else
            {
                string sqlReLoginInsert = "Insert Into tb_LogReLoginLocking (relogin_machineip)"
                                + " Values ('" + machineIP + "')";
                fn.ExecuteSQL(sqlReLoginInsert);
                Session["ReloginMachineIP"] = machineIP;
            }
        }
        catch (Exception ex)
        { }
    }
    private bool isRegLoginLock()
    {
        bool isLock = false;
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'";
            DataTable dtRelogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtRelogin.Rows.Count > 0)
            {
                string sqlReLoginUpdate = string.Empty;
                string AttemptTime = fn.GetDataByCommand("Select relogin_attemptTimes From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'", "relogin_attemptTimes");
                int attempedTime = 0;
                int.TryParse(AttemptTime, out attempedTime);
                if (attempedTime > 5)
                {
                    sqlReLoginUpdate = "Update tb_LogReLoginLocking Set relogin_lockdatetime=getdate(),relogin_islock=1 Where relogin_machineip='" + machineIP + "'";
                    fn.ExecuteSQL(sqlReLoginUpdate);
                    isLock = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isLock;
    }
    private bool isRegLoginUnLock()
    {
        bool isUnlock = false;
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'";
            DataTable dtRelogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtRelogin.Rows.Count > 0)
            {
                string sqlReLoginUpdate = string.Empty;
                string AttemptTime = fn.GetDataByCommand("Select relogin_lockdatetime From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'", "relogin_lockdatetime");
                DateTime attempedDateTime = cFun.ParseDateTime(AttemptTime);
                DateTime datetimenow = DateTime.Now;
                TimeSpan duration = datetimenow - attempedDateTime;
                double durationTime = duration.TotalMinutes;
                if (durationTime >= 20)//***
                {
                    sqlReLoginUpdate = "Update tb_LogReLoginLocking Set relogin_lockdatetime=Null,relogin_islock=0,relogin_attemptTimes=0 Where relogin_machineip='" + machineIP + "'";
                    fn.ExecuteSQL(sqlReLoginUpdate);
                    isUnlock = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isUnlock;
    }
    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
    #region getdata
    #region getGroupReLoginAuthenData
    public DataTable getGroupReLoginAuthenData(string emailaddress, string password, string showid)
    {
        DataTable dtResult = new DataTable();

        try
        {
            string query = "Select * From tb_RegGroup as d Left Join tb_RegLoginPassword as rl On d.RegGroupID=rl.OwnerID "
                + " Where RG_ContactEmail=@username And rl.Password=@password And d.ShowID=@ShowID And recycle=0 "
                + " And rl.ShowID=@ShowID And rl.Status=@Status";

            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("username", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("password", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar4 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = emailaddress;
            spar2.Value = password;
            spar3.Value = showid;
            spar4.Value = RegLoginPasswordStatus.Active;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);
            pList.Add(spar4);
            dtResult = fn.GetDatasetByCommand(query, "dsResult", pList).Tables[0];
        }
        catch { }

        return dtResult;
    }
    #endregion
    #region getDelegateReLoginAuthenData
    public DataTable getDelegateReLoginAuthenData(string emailaddress, string passwordMembershipNo, string showid)
    {
        DataTable dtResult = new DataTable();

        try
        {
            string query = "Select * From tb_RegDelegate as d Left Join tb_RegLoginPassword as rl On d.Regno=rl.OwnerID "
                + " Where reg_Email=@username And rl.Password=@password And d.ShowID=@ShowID And recycle=0 "
                + " And rl.ShowID=@ShowID And rl.Status=@Status";

            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("username", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("password", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar4 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = emailaddress;
            spar2.Value = passwordMembershipNo;
            spar3.Value = showid;
            spar4.Value = RegLoginPasswordStatus.Active;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);
            pList.Add(spar4);
            dtResult = fn.GetDatasetByCommand(query, "dsResult", pList).Tables[0];
        }
        catch { }

        return dtResult;
    }
    #endregion
    #endregion
    #endregion
}