﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Test" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <title>
      
    </title>
 <script  src="http://code.jquery.com/jquery-3.3.1.min.js"  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="  crossorigin="anonymous"></script>

  <script type="text/javascript">
  $(document).ready(function() {
  //pulling all <style></style> css of parent document
  if (parent) {
        var oHead = document.getElementsByTagName("head")[0];
        var arrStyleSheets = parent.document.getElementsByTagName("style");
        for (var i = 0; i < arrStyleSheets.length; i++)
            oHead.appendChild(arrStyleSheets[i].cloneNode(true));
    }
 //pulling all external style css(<link href="css.css">) of parent document
    $("link[rel=stylesheet]",parent.document).each(function(){
        var cssLink = document.createElement("link")
        cssLink.href = "http://"+parent.document.domain+$(this).attr("href");
        cssLink .rel = "stylesheet";
        cssLink .type = "text/css";
        document.body.appendChild(cssLink);
    });   
  });
</script>
 
</head>
<body>
    <form id="form1" runat="server">
 div> 
         

        <div>
            Text : 
        <asp:TextBox ID="txtRegno" runat="server"></asp:TextBox>
            <br />
            Result :
            <asp:Label ID="lblResult" runat="server"></asp:Label>
            <br />
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />

            <br />
            <br />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="Email Test" OnClick="btnTestEmail_Click" class="MainButton"/>

            <asp:Button ID="Button2" runat="server" Text="Email Temp List" OnClick="btnTestEmailTemp_Click" /> <br />

                     <asp:Button ID="Button3" runat="server" Text="Barcode List" OnClick="btnTestBarcide_Click" />

        </div>
    </form>
</body>
</html>
