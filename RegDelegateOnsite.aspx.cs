﻿using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Corpit.Promo;
using Corpit.Logging;
using Corpit.BackendMaster;
using System.Data.SqlClient;
using Corpit.RegOnsite;
using Newtonsoft.Json;
using System.Configuration;
using System.Net;
using System.IO;
public partial class RegDelegateOnsite : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //**Added on 17-Oct-2018 to disable Print Thai Button and Job Title Textbox/Label
            btnSaveThai.Visible = false;
            txtDesignation.Visible = false;
            lblDesignation.Visible = false;

            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string regno= cFun.DecryptValue(urlQuery.DelegateID);
            bindDropdown(showid);
            LoadData(regno);
        }
    }
    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = "RegistrationOnsite.master";
        }
    }
    #endregion
    public void btnSave_Click(object sender, EventArgs e)
    {
        string url = "";
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
         
        //Call Sunny Program
        SaveAndPrint(url, true);
    }

    public void btnSaveThai_Click(object sender, EventArgs e)
    {
        string url = "";
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (showid == "EAD351")
            url = @"http://203.125.155.188/badgePrintSARC/BadgePrntOut.aspx?ID=";
        else
            url = @"http://203.125.155.188/badgePrint/BadgePrntOutVNU.aspx?ID=";

        SaveAndPrint(url, false);
    }

    private void SaveAndPrint(string url,bool isSunny)
    {
        isSunny = true;
        Functionality fn = new Functionality();
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
        RegDelegateObj rgd = new RegDelegateObj(fn);

        string regno = cFun.DecryptValue(urlQuery.DelegateID);
        bool hasid = false;
        if (String.IsNullOrEmpty(regno))
        {
            regno = rgd.GenDelegateNumber(showid);
        }
        else
        {
            hasid = true;
        }
        string salutation = ddlSalutation.SelectedItem.Value.ToString();
        string othersal = "";
        OthersSettings othersetting = new OthersSettings(fn);
        List<string> lstOthersValue = othersetting.lstOthersValue;
        if ((lstOthersValue.Contains(ddlSalutation.SelectedItem.Text)) && txtSalOther.Text != "" && txtSalOther.Text != string.Empty)
        {
            othersal = txtSalOther.Text.Trim();
        }

        RegOnsiteDelegate regOnsite = new RegOnsiteDelegate();
        string delegateType = ddlCategory.SelectedItem.Value.Trim();
        //**Added on 17-Oct-2018 for FoodJapan Bussiness Category
        string profession = ddlProfession.SelectedItem.Text.Trim();
        if(profession == "Please Select" || profession.Contains("Select"))
        {
            profession = "";
        }
        //**Added on 17-Oct-2018 for FoodJapan Main Job Function
        string organization = ddlOrganization.SelectedItem.Text.Trim();
        if (organization == "Please Select" || organization.Contains("Select"))
        {
            organization = "";
        }

        if (string.IsNullOrEmpty(delegateType)) delegateType = "VISITOR";
        regOnsite.ShowID = showid;
        regOnsite.reg_urlFlowID = flowid;
        regOnsite.RegGroupID = groupid;
        regOnsite.Regno = regno;
        regOnsite.reg_Salutation = salutation;//
        regOnsite.reg_FName = txtFName.Text.Trim();
        //**Added on 18-Oct-2018 to use reg_LName field as FoodJapan Company
        regOnsite.reg_LName = txtCompany.Text;
        regOnsite.reg_OName = txtFullName.Text.Trim();
        //**Added on 18-Oct-2018 to use reg_Designation field as FoodJapan Bussiness Category
        regOnsite.reg_Designation = profession;
        //**Added on 18-Oct-2018 to use reg_Organization field as FoodJapan Main Job Function
        regOnsite.reg_Organization = organization;
        regOnsite.reg_Country = ddlCountry.SelectedItem.Value.ToString();
        regOnsite.reg_Email = txtEmail.Text;
        regOnsite.reg_Mobcc = txtMobcc.Text;
        regOnsite.reg_Mobac = txtMobac.Text;
        regOnsite.reg_Mobile = txtMobile.Text;
        regOnsite.reg_Category = delegateType;
        regOnsite.reg_SalutationOthers = othersal;
        regOnsite.recycle = "0";
        regOnsite.reg_CreatedBy = "";

        RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);

        int isOK = 0;
        if (hasid)
        {
            if(txtisOnsite.Text=="1")
            oHelp.UpdateRegDelegate(regOnsite);
            else
                oHelp.UpdateRegDelegateOnline(regOnsite);
        }
        else
            oHelp.AddRegDelegate(regOnsite);

        {
            //jBQCode jData = new jBQCode();
            //jData.ShowName = showid;
            //jData.BarCode = regno;
            //jData.Regno = regno;
            //BarcodeMaker bMaker = new BarcodeMaker();
            //string burl = bMaker.MakeBarcode(jData);
            //if (!string.IsNullOrEmpty(burl))
            {
                //OnsiteShowConfig oSConig = oHelp.GetOnsiteShowConfig(showid);
                //if (oSConig.PrintCategoryFooter == "0") delegateType = ""; // NOT required to Print Cateogry
                ////PrintDataObj pObj = new PrintDataObj();
                ////pObj.ShowID = showid;
                ////pObj.Regno = regno;
                ////pObj.FullName = (txtFName.Text).Trim();
                ////pObj.Jobtitle = txtDesignation.Text.Trim();
                ////pObj.Company = txtCompany.Text.Trim();
                ////pObj.Country = ddlCountry.SelectedItem.Text; ;
                ////pObj.Category = delegateType;
                ////pObj.BCodeURL = burl;

                ////string pJson = JsonConvert.SerializeObject(pObj);
                ////string rtnNum = "";
                ////CallSendEmailApi(pJson, ref rtnNum);
                ////if (!string.IsNullOrEmpty(rtnNum))
                {
                    oHelp.AddPrintLog(showid, regno);
                    CommonFuns cFun = new CommonFuns();
                    //rtnNum = cFun.EncryptValue(rtnNum);
                    if (isSunny)
                    {
                        url = string.Format("https://www.event-reg.biz/registration/onsite/OnsitePrint.aspx?SH={0}&Regno={1}", showid, regno);
                        Response.Redirect(url);

                        //url = string.Format("http://192.168.1.100/RegOnsite/onsite/OnsitePrint.aspx?SH={0}&Regno={1}", showid, regno);
                        //Response.Redirect(url);

                        //        url = string.Format("http://192.168.1.100/RegOnsite/onsite/OnsitePrint.aspx?SH={0}&Regno={1}", showid, regno);
                        //        string jVal = string.Format("window.open('{0}','_newtab');", url);
                        //        Page.ClientScript.RegisterStartupScript(
                        //this.GetType(), "OpenWindow", jVal, true);
                    }
                    //Response.Redirect(url + rtnNum);
                    ////if (showid == "EAD351")
                    ////    Response.Redirect("http://203.125.155.188/badgePrintSARC/BadgePrntOut.aspx?ID=" + rtnNum);
                    ////else
                    ////    Response.Redirect("http://203.125.155.188/badgePrint/BadgePrntOutVNU.aspx?ID=" + rtnNum);
                }
            }
        }
    }
    #region ddlSalutation_SelectedIndexChanged (set 'other salutation div' visibility if the selection of ddlSalutation dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherSal and form_type='D')
    protected void ddlSalutation_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlSalutation.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlSalutation.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherSal);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divSalOther.Visible = true;
                        }
                        else
                        {
                            divSalOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtSalOther.Attributes.Add("required", "");
                            vcSalOther.Enabled = true;
                        }
                        else
                        {
                            //txtSalOther.Attributes.Remove("required");
                            vcSalOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divSalOther.Visible = false;
                    //txtSalOther.Attributes.Remove("required");
                    vcSalOther.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region ddlCountry_SelectedIndexChanged (bind country code data to txtTelcc, txtMobcc, txtFaxcc textboxes according to the selected country)
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlCountry.Items.Count > 0)
            {
                string countryid = ddlCountry.SelectedItem.Value;

                if (ddlCountry.SelectedIndex == 0)
                {
                    countryid = Number.Zero;
                }

                CountryObj couObj = new CountryObj(fn);
                DataTable dt = couObj.getCountryByID(countryid);
                if (dt.Rows.Count > 0)
                {
                    string code = dt.Rows[0]["countryen"].ToString();

                    txtMobcc.Text = code;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region bindDropdown & bind respective data to Salutation, Country, Affiliation, Dietary, Profession and OrgType dropdown lists
    protected void bindDropdown(string showID)
    {
        DataSet dsSalutation = new DataSet();
        DataSet dsCountry = new DataSet();

        //**Added on 17-Oct-2018 for FoodJapan Bussiness Category
        DataSet dsProfession = new DataSet();
        //**Added on 17-Oct-2018 for FoodJapan Main Job Function
        DataSet dsOrganization = new DataSet();

        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsSalutation = cmdObj.getSalutation(showID);

        //**Added on 17-Oct-2018 for FoodJapan Bussiness Category
        dsProfession = cmdObj.getProfession(showID);
        //**Added on 17-Oct-2018 for FoodJapan Main Job Function
        dsOrganization = cmdObj.getOrganization(showID);

        if (dsSalutation.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsSalutation.Tables[0].Rows.Count; i++)
            {
                ddlSalutation.Items.Add(dsSalutation.Tables[0].Rows[i]["Sal_Name"].ToString());
                ddlSalutation.Items[i + 1].Value = dsSalutation.Tables[0].Rows[i]["Sal_ID"].ToString();
            }
        }
        CountryObj couObj = new CountryObj(fn);
        dsCountry = couObj.getAllCountry();
        if (dsCountry.Tables[0].Rows.Count != 0)
        {
            for (int x = 0; x < dsCountry.Tables[0].Rows.Count; x++)
            {
                ddlCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();
            }
        }

        RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
        DataTable dCat = oHelp.CategoryList(showID);
        for (int i = 0; i < dCat.Rows.Count; i++)
        {
            ddlCategory.Items.Add(dCat.Rows[i]["CategoryName"].ToString());
            ddlCategory.Items[i + 1].Value = dCat.Rows[i]["CategoryName"].ToString();
        }

        //**Added on 17-Oct-2018 to bind Profession field to Bussiness Category ddl
        if (dsProfession.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsProfession.Tables[0].Rows.Count; i++)
            {
                ddlProfession.Items.Add(dsProfession.Tables[0].Rows[i]["Profession"].ToString());
                ddlProfession.Items[i + 1].Value = dsProfession.Tables[0].Rows[i]["ID"].ToString();
            }
        }

        //**Added on 17-Oct-2018 to bind Organization field to Main Job Function ddl
        if (dsOrganization.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsOrganization.Tables[0].Rows.Count; i++)
            {
                ddlOrganization.Items.Add(dsOrganization.Tables[0].Rows[i]["organisation"].ToString());
                ddlOrganization.Items[i + 1].Value = dsOrganization.Tables[0].Rows[i]["ID"].ToString();
            }
        }

    }
    #endregion

    #region PrintRequest
    public bool CallSendEmailApi(string jsonStr, ref string rtnNumber)
    {
        bool isOK = false;
        try
        {
            string rtnValue = PostEmailPortalApi(jsonStr, "PrintRequest");
            if (!string.IsNullOrEmpty(rtnValue) && rtnValue != "ERROR")
            {
                PrintRtnStatus rtnConfig = JsonConvert.DeserializeObject<PrintRtnStatus>(rtnValue);
                if (!string.IsNullOrEmpty(rtnConfig.Status) && rtnConfig.Status.ToUpper() != "ERROR")
                {
                    isOK = true;
                    rtnNumber = rtnConfig.Status;
                }
                else
                    isOK = false;
            }

        }
        catch (Exception)
        {
            isOK = false;
        }
        return isOK;
    }
    public string PostEmailPortalApi(string jData, string callPage)
    {
        string rtnData = "";
        try
        {
            string jsonStr = jData;

            if (ConfigurationManager.AppSettings["OnsitePrintPortalURL"] != null)
            {
                string url = ConfigurationManager.AppSettings["OnsitePrintPortalURL"].ToString() + "/" + callPage;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(jsonStr);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                string status = "";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    rtnData = streamReader.ReadToEnd();
                }
            }
        }
        catch (Exception ex) { rtnData = "ERROR"; }
        return rtnData;
    }


    #endregion

    private void LoadData(string regno)
    {

        string sql = "  select * from tb_RegDelegateOnsite where regno=@regno";

        SqlParameter spar1 = new SqlParameter("regno", SqlDbType.NVarChar);
        spar1.Value = regno;
        List<SqlParameter> pList = new List<SqlParameter>();
        pList.Add(spar1);

        string showID = "";
        string FlowID = ""; 
        string groupID = "";
        string sal = "";
        string name = "";
        string job = "";
        string compay = "";
        string country = "";
        string mcc = "";
        string mac = "";
        string m = "";
        string email = "";
        string category = "";
        //**Added on 17-Oct-2018 for FoodJapan Bussiness Category
        string profession = "";
        //**Added on 17-Oct-2018 for FoodJapan Main Job Function
        string organization = "";

        DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

        if (dList.Rows.Count > 0)
        {
            showID = dList.Rows[0]["showID"].ToString();
            FlowID = dList.Rows[0]["reg_urlFlowID"].ToString();
            regno = dList.Rows[0]["regno"].ToString();
            groupID = dList.Rows[0]["RegGroupID"].ToString();
            sal = dList.Rows[0]["reg_Salutation"].ToString();
            //**Added on 18-Oct-2018 to use reg_LName as FoodJapan Company
            compay = dList.Rows[0]["reg_LName"].ToString();
            name = dList.Rows[0]["reg_FName"].ToString();
            //**Added on 18-Oct-2018 to reuse reg_Designation field as FoodJapan Bussiness Category
            //job = dList.Rows[0]["reg_Designation"].ToString();
            profession = dList.Rows[0]["reg_Designation"].ToString();
            //**Added on 18-Oct-2018 to use reg_Organization field as FoodJapan Main Job Function
            //compay = dList.Rows[0]["reg_Organization"].ToString();
            organization = dList.Rows[0]["reg_Organization"].ToString();
            country = dList.Rows[0]["reg_Country"].ToString();
            mcc = dList.Rows[0]["reg_Mobcc"].ToString();
            mac = dList.Rows[0]["reg_Mobac"].ToString();
            m = dList.Rows[0]["reg_Mobile"].ToString();
            email = dList.Rows[0]["reg_Email"].ToString();
            category = dList.Rows[0]["reg_Category"].ToString().ToUpper() ;
            txtisOnsite.Text = "1";
        }
        else
        {
            //sql = "  select * from tb_RegDelegate where regno=@regno";

            //SqlParameter spar2 = new SqlParameter("regno", SqlDbType.NVarChar);
            //spar2.Value = regno;
            //List<SqlParameter> pList2 = new List<SqlParameter>();
            //pList2.Add(spar2);
            //DataTable onLine = fn.GetDatasetByCommand(sql, "DT", pList2).Tables["DT"];

            //if (onLine.Rows.Count > 0)
            //{
            //    showID = onLine.Rows[0]["showID"].ToString();
            //    FlowID = onLine.Rows[0]["reg_urlFlowID"].ToString();
            //    regno = onLine.Rows[0]["regno"].ToString();
            //    groupID = onLine.Rows[0]["RegGroupID"].ToString();
            //    sal = onLine.Rows[0]["reg_Salutation"].ToString();
            //    name = onLine.Rows[0]["reg_FName"].ToString();
            //    //job = dList.Rows[0]["reg_Designation"].ToString();
            //    compay = onLine.Rows[0]["reg_Address1"].ToString();
            //    country = onLine.Rows[0]["reg_Country"].ToString();
            //    mcc = onLine.Rows[0]["reg_Mobcc"].ToString();
            //    mac = onLine.Rows[0]["reg_Mobac"].ToString();
            //    m = onLine.Rows[0]["reg_Mobile"].ToString();
            //    email = onLine.Rows[0]["reg_Email"].ToString();
            //    category = "VISITOR";
            //    txtisOnsite.Text = "0";
            //}
        }

        ListItem li = ddlSalutation.Items.FindByValue(sal);
        if (li != null) li.Selected = true;

        ListItem c = ddlCountry.Items.FindByValue(country);
        if (c != null) c.Selected = true;

        ListItem cat = ddlCategory.Items.FindByValue(category);
        if (cat != null) cat.Selected = true;

        //**Added on 18-Oct-2018 for FoodJapan Bussiness Category
        if(string.IsNullOrEmpty(profession) || string.IsNullOrWhiteSpace(profession))
        {
            profession = "Please Select";
        }
        ListItem liProfession = ddlProfession.Items.FindByText(profession);
        if (liProfession != null) liProfession.Selected = true;

        //**Added on 18-Oct-2018 for FoodJapan Main Job Function
        if (string.IsNullOrEmpty(organization) || string.IsNullOrWhiteSpace(organization))
        {
            organization = "Please Select";
        }
        ListItem liOrganization = ddlOrganization.Items.FindByText(organization);
        if (liOrganization != null) liOrganization.Selected = true;

        txtFName.Text = name;
        txtCompany.Text = compay;
        txtDesignation.Text = job;
        txtMobcc.Text = mcc;
        txtMobac.Text = mac;
        txtMobile.Text = m;
        txtEmail.Text = email;
        txtShowID.Text = showID;
       // txtRegno.Text = regno;
    }
}
