﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReLoginMaster.master" AutoEventWireup="true" CodeFile="Reg_Home.aspx.cs" Inherits="Reg_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .msgLgFont
        {
            font-size:14px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="form-group" id="divMsg" runat="server">
        <div class="form-group row">
            <div class="col-md-12 msgLgFont">
                Welcome to <%=showname %> relogin page.
                <br /><br />
                In this page you will be able to edit current or add new member registration. 
                <br /><br />
                Click on the respective tab above to proceed.

                <%--<asp:Label ID="lblHomeMessage" runat="server" ></asp:Label>--%>
            </div>
        </div>
    </div>

    <div class="form-group" id="divReloginHomeMsg" runat="server" visible="false">
        <div class="form-group row">
            <div class="col-md-12 msgLgFont">
                <asp:Label ID="lblTemplate" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>

