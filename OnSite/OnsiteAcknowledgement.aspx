﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnSite/OnsiteMaster.master" AutoEventWireup="true" CodeFile="OnsiteAcknowledgement.aspx.cs" Inherits="OnSite_OnsiteAcknowledgement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="DivMain" class="row">
        <%--<div id="DivHeader" style="padding-top: 30px;" class="container">
            <h3>Onsite Acknowledgement</h3>
        </div>--%>
        <div id="DivSearchBox" class="container" style="padding-top: 30px;">
            <div class="row">
                <%--<div class="col-md-2">&nbsp;</div>--%>
                <div class="col-md-12">
                    <asp:Label ID="lblAcknowledgementMsg" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" CssClass="btn btn-success" />
                </div>
            </div>
            <br />
            <br />
        </div>
        <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
        <asp:TextBox runat="server" ID="txtRegGroupID" Visible="false" Text=""></asp:TextBox>
        <asp:Label runat="server" ID="lblRegno" Visible="false" CssClass="form-control"></asp:Label>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
</asp:Content>

