﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
public partial class OnSite_Login : System.Web.UI.Page
{
    Functionality fn = new Functionality();
 
    public class tmpDataList
    {
        public string showID { get; set; }
        public string FlowID { get; set; }
    }
    private static string[] checkingAcknowledgementMDAThaiShowName = new string[] { "MFT 2019", "PPI 2019", "TPlas 2019", "Wire and Tube 2019", "SMEX-Thai" };//***MDA Thai 18-2-2019

    private static string[] checkingECCShowName = new string[] { "ECC 2019" };
    private static string[] checkingAFCShowName = new string[] { "AFC 2019" };
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CommonFuns cFuz = new CommonFuns();
            if (Request.QueryString["Event"] != null)
            {
                tmpDataList tmpList = GetShowIDByEventName(Request.QueryString["Event"].ToString());
                if (string.IsNullOrEmpty(tmpList.showID))
                    Response.Redirect("404.aspx");
                else
                {


                }
            }
        }
    }
    protected void btnSubmit_Onclick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            {
                try
                {
                    Functionality fn = new Functionality();
                    CommonFuns cFun = new CommonFuns();
                    Boolean isvalid = isValid();
                    if (isvalid)
                    {
                        string username = cFun.solveSQL(txtUsername.Text.ToString().Trim());
                        string password = cFun.solveSQL(txtPassword.Text.ToString().Trim());


                        LoginControler loginCtr = new LoginControler(fn);
                        LoginUser lUser = loginCtr.CheckAdminLogin(username, password);
                        //   DataTable dt = loginCtr.authenticateLogin(username, password);
                        //if (dt.Rows.Count > 0)
                        if (!string.IsNullOrEmpty(lUser.UserID))
                        {
                            string roleid = lUser.RoleID.ToString();
                            string sesUser = lUser.UserName.ToString();
                            string sesUserID = lUser.UserID.ToString();
                            Session["roleid"] = roleid;
                            Session["username"] = sesUser;
                            Session["userid"] = sesUserID;

                            tmpDataList tmpList = GetShowIDByEventName(Request.QueryString["Event"].ToString());
                            if (string.IsNullOrEmpty(tmpList.showID))
                                Response.Redirect("404.aspx");
                            else
                            {
                                CommonFuns cFuz = new CommonFuns();
                                string showID = tmpList.showID;
                                string FlowID = tmpList.FlowID;

                                string sqlFLowMaster = "Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID='" + showID + "' and Status='Active' And isOnsiteDataEntry=1 Order By FLW_ID Desc";
                                DataTable dt = fn.GetDatasetByCommand(sqlFLowMaster, "ds").Tables[0];
                                if (dt.Rows.Count > 0)
                                {
                                    Response.Redirect("OnsiteSearchNormalFlow.aspx?Evt=" + cFuz.EncryptValue(showID));
                                }
                                else
                                {
                                    ShowControler shwCtr = new ShowControler(fn);
                                    Show shw = shwCtr.GetShow(showID);
                                    if (checkingAcknowledgementMDAThaiShowName.Contains(shw.SHW_Name))
                                    {
                                        Response.Redirect("OnsiteSearchMDA.aspx?Evt=" + cFuz.EncryptValue(showID));
                                    }
                                    else if(checkingECCShowName.Contains(shw.SHW_Name))/*added on 10-9-2019*/
                                    {
                                        Response.Redirect("OnsiteSearchECC.aspx?Evt=" + cFuz.EncryptValue(showID));
                                    }
                                    else if (checkingAFCShowName.Contains(shw.SHW_Name))/*added on 7-10-2019*/
                                    {
                                        Response.Redirect("OnsiteSearchAFC.aspx?Evt=" + cFuz.EncryptValue(showID));
                                    }
                                    else
                                    {
                                        Response.Redirect("OnsiteSearch.aspx?Evt=" + cFuz.EncryptValue(showID));
                                    }
                                }
                            }
                        }
                        else
                        {
                            lblpasswerror.Text = "Invalid username or password";
                            lblpasswerror.Visible = true;
                        }
                    }
                }
                catch (Exception ex) { }
            }
        }
    }
    protected Boolean isValid()
    {
        Boolean isvalid = true;

        if (txtPassword.Text.ToString().Trim() == "" || txtPassword.Text.ToString().Trim() == string.Empty)
        {
            lblpasswerror.Text = "Please insert your password";
            lblpasswerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblpasswerror.Visible = false;
        }
        if (txtUsername.Text.ToString().Trim() == "" || txtUsername.Text.ToString().Trim() == string.Empty)
        {
            lblpasswerror.Text = "Please insert your username";
            lblpasswerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblpasswerror.Visible = false;
        }
        return isvalid;
    }

    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }


    private tmpDataList GetShowIDByEventName(string eventName)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            cList.showID = "";
            cList.FlowID = "";
            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                cList.showID = dList.Rows[0]["showID"].ToString();
                cList.FlowID = dList.Rows[0]["FLW_ID"].ToString();
            }
            else
            {
                CommonFuns cFun = new CommonFuns();
                string showid = cFun.DecryptValue(Request.QueryString["Event"].ToString());
                sql = "select * from tb_show where SHW_ID=@Show";
                SqlParameter spShow = new SqlParameter("Show", SqlDbType.NVarChar);
                spShow.Value = showid; 
                List<SqlParameter> pShowLIst = new List<SqlParameter>();
                pShowLIst.Add(spShow);
                DataTable dShow = fn.GetDatasetByCommand(sql, "DT", pShowLIst).Tables["DT"];

                if (dShow.Rows.Count > 0)
                {
                    cList.showID = dShow.Rows[0]["SHW_ID"].ToString();
                    cList.FlowID = "";
                }
            }

        }
        catch { }
        return cList;
    }
}