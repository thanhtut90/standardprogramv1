﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using Corpit.RegOnsite;
using Newtonsoft.Json;
using System.Data;
using Corpit.Registration;
using Corpit.Utilities;
using System.Data.SqlClient;
using Corpit.Site.Utilities;

public partial class OnSite_OnsiteSearch : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    string SSBFEIndivReg = "F440";
    string FoodJapanIndivReg = "F376";
    string FoodJapanGroupReg = "F377";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null)
            {

                //if (Session["userid"] != null)
                {
                    txtSearch.Focus();
                    txtSearch.Attributes.Add("autocomplete", "off");


                    CommonFuns cFun = new CommonFuns();
                    string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                    if (string.IsNullOrWhiteSpace(showid))
                        Response.Redirect("~/404.aspx");
                    else
                        txtShowID.Text = showid;

                }
                //else
                //{
                //    Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
                //}
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    protected void btnEntry_Onclick(object sender, EventArgs e)
    {
        if (Request.QueryString["Evt"] != null)
        {
            string url = "https://www.event-reg.biz/registration/OnsiteIndex?Event=OnSiteRegFoodJapan";//RegFoodJapan
            string jVal = string.Format("window.open('{0}','_newtab');", url);
            Page.ClientScript.RegisterStartupScript(
    this.GetType(), "OpenWindow", jVal, true);
        }
    }
    protected void btnSearch_Onclick(object sender, EventArgs e)
    {
        string strSerarch = txtSearch.Text;
        if (string.IsNullOrEmpty(strSerarch))
        { }
        else {
            string regno = GetRegNO(strSerarch);
            string showID = txtShowID.Text.Trim();
            if (!string.IsNullOrEmpty(showID))
            {
                if (!string.IsNullOrEmpty(regno))
                {
                    //if (ChkPrintDirect.Checked)
                    //{
                    //    //     OnsitePrintHelper
                    //    CallPrint(showID, regno);
                    //    txtSearch.Text = "";
                    //}
                    //else
                    {
                        SearchNload(showID, txtSearch.Text, false);
                    }
                }
                else
                {
                    //Search and Load List
                    SearchNload(showID, txtSearch.Text, false);
                }

                txtSearch.Focus();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }
    protected void btnPrint_Onclick(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        string regno = btn.CommandArgument;
        string showID = txtShowID.Text.Trim();
        if (!string.IsNullOrEmpty(showID))
        {
            CallPrint(showID, regno);
        }
    }
    protected void btnPrintSunny_Onclick(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        string regno = btn.CommandArgument;
        string showID = txtShowID.Text.Trim();
        if (!string.IsNullOrEmpty(showID))
        {
            #region for ECG
            //bool isForm1B = false;
            //if (regno.Contains("ECGMOE"))
            //{
            //    isForm1B = true;
            //}

            //bool isPaidDelegate = false;
            //if (!isForm1B)
            //    isPaidDelegate = checkData(showID, regno);
            //else
            //    isPaidDelegate = checkDataForm1B(showID, regno);

            //if (isPaidDelegate)
            //{
            //    string printStatus = "";
            //    if (!isForm1B)
            //        printStatus = GetPrintStatus(regno);
            //    else
            //        printStatus = GetPrintStatusForm1B(regno);
            //    if (printStatus == "YES")
            //    {
            //        //Response.Write("<script>alert('Sorry, you already printed. Please go to the counter.');</script>");
            //        ////Response.Write("<script language=javascript>");
            //        ////Response.Write("if(confirm('El vehiculo no existe, Deseas crear el registro?')){window.location.href='IngresoVehiculos.aspx'}");
            //        ////Response.Write("</script>");
            //        //txtRegno.Text = regno;
            //        // ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            //        Response.Write("<script>alert('Sorry, you already Printed.');</script>");
            //    }
            //    //else
            //    //{
            //    //    Response.Redirect("OnsiteScanSuccess.aspx?Evt=" + cFun.EncryptValue(showID) + "&r=" + cFun.EncryptValue(regno) + "&type=2");
            //    //}
            //    else {
            //        #region ECG
            //        //if (!isForm1B)
            //        //{
            //        //    RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            //        //    oHelp.AddPrintLog(showID, regno);
            //        //}
            //        //else
            //        //{
            //        //    ECGFunctionality eFun = new ECGFunctionality();
            //        //    RegOnsiteHelper oHelp = new RegOnsiteHelper(eFun);
            //        //    oHelp.AddPrintLog(showID, regno);
            //        //}
            //        //Response.Write("<script>alert('Success');</script>");
            //        #endregion

            //        CallPrintSunny(showID, regno);
            //    }
            //}
            //else
            //{
            //    //Response.Write("<script>alert('Sorry, your registration is not confirmed yet. Please go to registration counter.');</script>");
            //    Response.Write("<script>alert('Payment is not completed. Please proceed to helpdesk to make payment');</script>");
            //}
            #endregion
            #region for paid registration
            //bool isPaidDelegate = false;
            //isPaidDelegate = checkData(showID, regno);
            //if (isPaidDelegate)
            //{
            //    string printStatus = "";
            //    printStatus = GetPrintStatus(regno);
            //    if (printStatus == "YES")
            //    {
            //        //Response.Write("<script>alert('Sorry, you already printed. Please go to the counter.');</script>");
            //        ////Response.Write("<script language=javascript>");
            //        ////Response.Write("if(confirm('El vehiculo no existe, Deseas crear el registro?')){window.location.href='IngresoVehiculos.aspx'}");
            //        ////Response.Write("</script>");
            //        //txtRegno.Text = regno;
            //        // ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            //        string msg = "You already Printed.";
            //        string NRICCheckMsg = "";
            //        CallPrintSunny(showID, regno, msg, ref NRICCheckMsg);
            //        if (!string.IsNullOrEmpty(NRICCheckMsg))
            //        {
            //            //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + pObj.Category.ToUpper() + "');window.location='" + url + "';", true);
            //            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please request for NRIC Nbr.');", true);
            //            return;
            //        }
            //    }
            //    //else
            //    //{
            //    //    Response.Redirect("OnsiteScanSuccess.aspx?Evt=" + cFun.EncryptValue(showID) + "&r=" + cFun.EncryptValue(regno) + "&type=2");
            //    //}
            //    else
            //    {
            //        string msg = "";
            //        string NRICCheckMsg = "";
            //        CallPrintSunny(showID, regno, msg, ref NRICCheckMsg);
            //        if (!string.IsNullOrEmpty(msg))
            //        {
            //            //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + pObj.Category.ToUpper() + "');window.location='" + url + "';", true);
            //            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please request for NRIC Nbr.');", true);
            //            return;
            //        }
            //    }
            //}
            //else
            //{
            //    //Response.Write("<script>alert('Sorry, your registration is not confirmed yet. Please go to registration counter.');</script>");
            //    Response.Write("<script>alert('Payment is not completed. Please proceed to helpdesk to make payment');</script>");
            //}
            #endregion
            #region no payment
            bool isSuccessDelegate = false;
            isSuccessDelegate = checkData(showID, regno);

            if (isSuccessDelegate)
            {
                #region check print status
                //string printStatus = "";
                //printStatus = GetPrintStatus(regno);
                //if (printStatus == "YES")
                //{
                //    //Response.Write("<script>alert('Sorry, you already printed. Please go to the counter.');</script>");
                //    ////Response.Write("<script language=javascript>");
                //    ////Response.Write("if(confirm('El vehiculo no existe, Deseas crear el registro?')){window.location.href='IngresoVehiculos.aspx'}");
                //    ////Response.Write("</script>");
                //    //txtRegno.Text = regno;
                //    // ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                //    Response.Write("<script>alert('Sorry, you already Printed.');</script>");
                //}
                //else
                #endregion
                {
                    CallPrintSunny(showID, regno);
                }
            }
            else
            {
                Response.Write("<script>alert('Sorry, your registration is not confirmed yet. Please go to registration counter.');</script>");
            }
            #endregion
        }
    }
    protected void btnEdit_Onclick(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        string regno = btn.CommandArgument;
        string showID = txtShowID.Text.Trim();
        if (!string.IsNullOrEmpty(showID))
        {
            string url = "";

            url = string.Format("~/OnsiteEditIndex.aspx?Edit={0}", regno, showID);

            string jVal = string.Format("window.open('{0}','_newtab');", url);
      
            Page.ClientScript.RegisterStartupScript(
  this.GetType(), "OpenWindow", jVal, true);

        }
    }
    
    #region CallPrint

    private void CallPrint(string showID, string regno)
    {
        string altertNoti = "";
        PrintDataObj pObj = GetRegobj(showID, regno, ref altertNoti);

        if (!string.IsNullOrEmpty(pObj.Regno))
        {


            string pJson = JsonConvert.SerializeObject(pObj);
            string rtnNum = "";

            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            oHelp.AddPrintLog(showID, regno);
            CommonFuns cFun = new CommonFuns();
            rtnNum = cFun.EncryptValue(rtnNum);

            string url = "";

            url = string.Format("OnsitePrint.aspx?Regno={0}&SH={1}" , regno, showID);

            string jVal = string.Format("window.open('{0}','_newtab');", url);
          //  if (!string.IsNullOrEmpty(altertNoti)) jVal = string.Format("alert('{1}');window.open('{0}','_newtab');", url, altertNoti);
            Page.ClientScript.RegisterStartupScript(
  this.GetType(), "OpenWindow", jVal, true);

        }
        else
        {
            altertNoti = "Invalid Regno";
            string jVal = string.Format("alert('{0}');", altertNoti);
            Page.ClientScript.RegisterStartupScript(
  this.GetType(), "OpenWindow", jVal, true);
        }
    }

    private void CallPrintSunny(string showID, string regno)/*, string msg, ref string NRICmsg) (CET)*/
    {
        string altertNoti = "";
        PrintDataObj pObj = GetRegobj(showID, regno, ref altertNoti);

        if (!string.IsNullOrEmpty(pObj.Regno))
        {
            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            oHelp.AddPrintLog(showID, regno);

            string url = string.Format("OnsitePrint.aspx?SH={0}&Regno={1}&type={2}", showID, regno,"2");
            #region no use
            ////string jVal = string.Format("window.open('{0}','_newtab');", url);

            ////          Page.ClientScript.RegisterStartupScript(
            ////this.GetType(), "OpenWindow", jVal, true);
            ////          //    Response.Redirect(url);
            #endregion
            #region get special items
            //string hasSpecialItems = getGalaMasterclassWorkshop(showID, regno);//HasGalaDinner(showID, regno);
            //hasSpecialItems = hasSpecialItems + altertNoti;
            #endregion
            #region CET
            //bool hasNRIC = checkGotNRICNumber(showID, regno);
            //if (!hasNRIC)
            //{
            //    NRICmsg = "Please request for NRIC Nbr.";
            //}
            //else
            //{
            //    if (!string.IsNullOrEmpty(msg))
            //    {
            //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + pObj.Category.ToUpper() + "');window.location='" + url + "';", true);
            //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + msg + "');window.location='" + url + "';", true);
            //    }
            //    else
            //    {
            //        //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + pObj.Category.ToUpper() + "');window.location='" + url + "';", true);
            //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "window.location='" + url + "';", true);
            //    }
            //}
            #endregion

            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "window.location='" + url + "';", true);
        }
        else
        {
            altertNoti = "Invalid Regno";
            string jVal = string.Format("alert('{0}');", altertNoti);
            Page.ClientScript.RegisterStartupScript(
       this.GetType(), "OpenWindow", jVal, true);
        }
    }

    private PrintDataObj GetRegobj(string showID, string regno, ref string altertNoti)
    {
        PrintDataObj pObj = new PrintDataObj();
        try
        {
            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            DataTable dt = oHelp.onsiteDelegateByRegno(showID, regno);
            string FullName = "";
            string FirstName = "";
            string LastName = "";
            string Jobtitle = "";
            string Company = "";
            string Country = "";
            string EmailAddress = "";
            string MobileNo = "";
            string Category = "";
            if (dt.Rows.Count > 0) //ONsite
            {
                Jobtitle = dt.Rows[0]["reg_Address1"].ToString(); //dt.Rows[0]["reg_Designation"].ToString();
                Company = (dt.Rows[0]["reg_oName"].ToString()).Trim();// dt.Rows[0]["reg_organization"].ToString();
                Country = dt.Rows[0]["RegCountry"].ToString();
                Category = dt.Rows[0]["reg_Category"].ToString().Trim().ToUpper();
                FullName = (dt.Rows[0]["reg_FName"].ToString()).Trim();// (dt.Rows[0]["reg_oName"].ToString()).Trim();
                FirstName = (dt.Rows[0]["reg_FName"].ToString()).Trim();
                LastName = (dt.Rows[0]["reg_LName"].ToString()).Trim();
                EmailAddress = (dt.Rows[0]["reg_Email"].ToString()).Trim();
                MobileNo = (dt.Rows[0]["reg_Telcc"].ToString() + dt.Rows[0]["reg_Telac"].ToString() + dt.Rows[0]["reg_Tel"].ToString()).Trim(); //(dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString()).Trim();
            }
            else { //Pre Reg
                //dt = oHelp.PreRegDelegateByRegno(showID, regno);
                string sql = "Select Case When reg_oname is null Or reg_oname='' Then reg_FName Else reg_OName End as FullName,reg_otherInstitution,isNull(InstitutionName,reg_otherInstitution) InstitutionName,* From GetRegIndivAllByShowID('" + showID + "') "
                    + " Where reg_urlFlowID In ('" + FoodJapanIndivReg + "','" + FoodJapanGroupReg + "') And Regno='" + regno + "'";
                dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    //preReg
                    string flName = dt.Rows[0]["reg_FName"].ToString();// + " " + dt.Rows[0]["reg_LName"].ToString();
                    FullName = flName;// (dt.Rows[0]["reg_oName"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_oName"].ToString()) ? dt.Rows[0]["reg_oName"].ToString() : flName) : flName).Trim();
                    FirstName = dt.Rows[0]["reg_FName"].ToString().Trim();
                    LastName = dt.Rows[0]["reg_LName"].ToString().Trim();
                    Jobtitle = dt.Rows[0]["reg_Address1"].ToString();//dt.Rows[0]["reg_Designation"].ToString();
                    Company = dt.Rows[0]["reg_oName"].ToString();//dt.Rows[0]["InstitutionName"].ToString();
                    Country = dt.Rows[0]["RegCountry"].ToString();
                    EmailAddress = dt.Rows[0]["reg_Email"].ToString();
                    MobileNo = dt.Rows[0]["reg_Telcc"].ToString() + dt.Rows[0]["reg_Telac"].ToString() + dt.Rows[0]["reg_Tel"].ToString(); //dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString();
                    Category = dt.Rows[0]["reg_Profession"].ToString();// getCategorySHC(regno, showID);// "";

                    #region paid registration
                    //string regGroupID = dt.Rows[0]["RegGroupID"].ToString();
                    //bool isPaid = oHelp.isDelegatePaid(showID, regno, regGroupID);
                    //if (!isPaid)
                    //    altertNoti = " Please proceed to onsite registration counter to complete your payment    ";

                    ////if (dt.Rows[0]["reg_Additional5"].ToString() == "1")
                    ////    altertNoti += "Delegate has requested for vegetarian meal.";
                    #endregion

                    #region ASVAC
                    //if (dt.Rows[0]["reg_Dietary"].ToString() == "266")
                    //    altertNoti += ",Vegetarian";
                    #endregion

                    #region visitor registration
                    string regStatus = dt.Rows[0]["status_name"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["status_name"].ToString()) ? dt.Rows[0]["status_name"].ToString() : "0") : "0";
                    if (regStatus != "Success")
                    {
                        altertNoti = " Sorry, your registration is not complete yet.";
                    }
                    #endregion
                }
                else regno = "";

            }
            if (!string.IsNullOrEmpty(regno))
            {
                jBQCode jData = new jBQCode();
                jData.ShowName = showID;
                jData.BarCode = regno;
                jData.Regno = regno;
                BarcodeMaker bMaker = new BarcodeMaker();
                string burl = bMaker.MakeBarcode(jData);
                if (!string.IsNullOrEmpty(burl))
                {
                    pObj.ShowID = showID;
                    pObj.Regno = regno;
                    pObj.FullName = FullName.ToUpper();
                    pObj.FirstName = FirstName.ToUpper();
                    pObj.LastName = LastName.ToUpper();
                    pObj.Jobtitle = Jobtitle.ToUpper();
                    pObj.Company = Company.ToUpper();
                    pObj.Country = Country.ToUpper();
                    pObj.EmailAddress = EmailAddress;
                    pObj.MobileNo = MobileNo;
                    pObj.Category = Category.ToUpper();
                    pObj.BCodeURL = burl;
                }
            }
        }
        catch { }


        return pObj;
    }
    #endregion
    #region GetRegno
    private string GetRegNO(string str)
    {
        string rtn = "";
        try
        {
            if (str.Length > 0)
            {
                string tmp = "";
                string[] arrList = str.Split('-'); // MFA2018-233333378 fromat
                if (arrList.Length == 2)
                    tmp = arrList[1];
                else
                    tmp = str;

                if (isRegno(tmp))
                    rtn = tmp;
            }
        }
        catch { }
        return rtn;
    }
    private bool isRegno(string input)
    {
        bool isRegno = false;
        try
        {
            isRegno = Regex.IsMatch(input, @"^\d+$");

        }
        catch { }
        return isRegno;
    }
    #endregion

    #region SearchNLoad
    private void SearchNload(string showID, string str, bool isRegno)
    {
        if (isRegno)
        { }
        else
        {
            string sql = "";
            #region Old Comment
            //           sql = @"  
            // select * from (
            //  select ShowID,regno, Salutation, reg_FName FullName,reg_LName company,ProfessionName , OrganizationName JobTitle,RegCountry Country,case when reg_urlFlowID= 'F378' Then 'Visitor' else 'Trade Visitor' End RegCategory , reg_urlFlowID,reg_email Email from DelegateWithRefValue 
            //  where ShowID='{0}' and recycle=0 and reg_status=1 and reg_urlFlowID in ('F376','F377')
            //  union
            //  select ShowID,regno, Salutation, reg_FName FullName,reg_LName company,ProfessionName , OrganizationName JobTitle,RegCountry Country,case when reg_urlFlowID= 'F378' Then 'Visitor' else 'Trade Visitor' End RegCategory , reg_urlFlowID,reg_email Email from DelegateWithRefValue 
            //  where ShowID='{0}' and recycle=0 and reg_status=1 and reg_urlFlowID in ('F378') and regno in (select InvOwnerID from tb_Invoice where RegGroupId In ( select RegGroupId from tb_RegDelegate where ShowID= '{0}' and recycle=0 and reg_status=1 and reg_urlFlowID in ('F378')))
            //  union
            //  select showID,regno,Salutation,reg_FName FullName,reg_LName company,ProfessionName ProfessionName,OrganizationName JobTitle,RegCountry Country,reg_Category RegCategory ,reg_urlFlowID,reg_email Email
            //   from OnsiteDelegateWithRefValue where ShowID='{0}'
            //  )A 
            //";



            //           sql = string.Format(sql, showID);
            //          string sqlwhere = "";
            //          sqlwhere = @"    where regno like N'%{0}%' or company like N'%{0}%' or FullName like N'%{0}%' or Email like N'%{0}%' 
            //";
            //          sql = sql + sqlwhere;
            //          sql = string.Format(sql, str);
            //          DataTable dList = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            #endregion
            sql = "Select Case When reg_oname is null Or reg_oname='' Then reg_FName + ' ' + reg_LName Else reg_OName End as FullName,reg_otherInstitution,isNull(InstitutionName,reg_otherInstitution) InstitutionName,* From GetRegIndivAllByShowID(@SHWID)"
                + " Where reg_urlFlowID In ('" + FoodJapanIndivReg + "','" + FoodJapanGroupReg + "')";

            #region ECG
            //            sql = @"select * from (
            //Select ShowID,convert(nvarchar(500),regno) regno,RegGroupID,reg_Additional4,reg_Additional5,reg_Email, Case When reg_oname is null Or reg_oname='' Then reg_FName + ' ' + reg_LName Else reg_OName End as FullName

            // From GetRegIndivAllByShowID('SCZ367')

            //union
            //select 'ECGMOE' as ShowID, regno,0 RegGroupID , Designation as reg_Additional4, school as  reg_Additional5,email as reg_Email, Name as FullName 
            // from ECGSymposium2019.dbo.vw_regList_Teacher )a 

            //                  ";
            #endregion

            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);
            string sqlwhere = "";
            sqlwhere = @" And (regno like N'%{0}%' or reg_FName like N'%{0}%' or reg_OName like N'%{0}%' or reg_Email like N'%{0}%')";//reg_LName like N'%{0}%' or // or FullName like N'%{0}%' ";// or company like N'%{0}%' or Email like N'%{0}%' 
            sql = sql + sqlwhere;
            sql = string.Format(sql, str);
            DataTable dList = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
            if (dList.Rows.Count > 0)
            {
                RptList.DataSource = dList;
                RptList.DataBind();
                PanelList.Visible = true;
            }
            else
                PanelList.Visible = false;
        }
    }
    public string GetStatus(string regno, string regGroupID, string showID, string invoicestatus, string paymentMethod)//, string reg_status)
    {
        string rtn = "Unpaid";
        string invoiceStatus = !string.IsNullOrEmpty(invoicestatus) ? invoicestatus : "0";
        if (invoiceStatus == "1")// || (paymentMethod == "5" || paymentMethod == "6"))
        {
            rtn = "Paid";
        }
        //RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
        //bool isPaid = oHelp.isDelegatePaid(showID, regno, regGroupID);
        //if (isPaid)
        //{
        //    rtn = "Paid";
        //}
        return rtn;
    }
    public string GetPrintStatus(string regno)
    {
        string rtn = "";
        try
        {
            string sql = string.Format("select* from tb_Onsite_PrintLog where showID = '{0}' and Pregno = '{1}'", txtShowID.Text, regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 0)
                rtn = "YES";
        }
        catch { }
        return rtn;
    }
    #region SHC
    private string _CHAIRMAN = "CHAIRMAN";
    private string _CREW = "CREW";
    private string _EXHIBITOR = "EXHIBITOR";
    private string _FACULTY = "FACULTY";
    private string _ORGANISER = "ORGANISER";
    private string _SPEAKER = "SPEAKER";
    private static string SHCShowID = "ZHB359";
    public string getCategorySHC(string regno, string showid)
    {
        string result = "";
        try
        {
            string sql = "Select * From tmp_SHCSendReminderEmailDB Where Regno='" + regno + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                result = dt.Rows[0]["Category"] != DBNull.Value ? dt.Rows[0]["Category"].ToString() : "";
            }
            else
            {
                //result = "DELEGATE";
                ////string query = "Select f.aff_name From tb_RegDelegate as r Left Join ref_Affiliation as f On r.reg_Affiliation=f.affid Where r.ShowID=@SHWID And Regno='" + regno + "'";
                string query = "Select c.reg_CategoryName From tb_RegDelegate as r Left Join ref_reg_Category as c On r.con_CategoryId=c.reg_CategoryId Where r.ShowID=@SHWID And Regno='" + regno + "'";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);
                result = fn.GetDataByCommand(query, "reg_CategoryName", pList);
                if (result == _CHAIRMAN || result == _CREW || result == _EXHIBITOR || result == _FACULTY || result == _ORGANISER || result == _SPEAKER)//(category == "Yes")
                {
                    //category = "PRECEPTORSHIP";
                }
                else
                {
                    result = "DELEGATE";
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    #endregion
    #endregion

    #region PrintRequest
    #region WCMH
    //private static string catWCMHColorDefaultBlue = "~/images/blue.png";
    private static string catWCMHCategoryDefault = "Online";
    private static string catWCMHRed1 = "WCMHAA8N";
    private static string catWCMHRed2 = "WCMHT5NK";
    private static string catWCMHColorRed = "Red";
    private static string catWCMHGreen = "FLJJJZ0CTR";
    private static string catWCMHColorGreen = "Green";
    private static string catWCMHOrganiser = "811";
    private static string catWCMHColorOrganiser = "LightGrey";
    private static string catWCMHCategoryOrganiser = "Organiser";
    private static string catWCMHCrew = "812";
    private static string catWCMHColorCrew = "DarkGrey";
    private static string catWCMHCategoryCrew = "Crew";
    private static string catWCMHInvitedGuest = "813";
    private static string catWCMHColorInvitedGuest = "Yellow";
    private static string catWCMHCategoryInvitedGuest = "Guest";
    private static string catWCMHMedia = "814";
    private static string catWCMHColorMedia = "Teal";
    private static string catWCMHCategoryMedia = "Media";
    private static string catWCMHSpeaker = "815";
    private static string catWCMHColorSpeaker = "Brown";
    private static string catWCMHCategorySpeaker = "Speaker";
    private static string WCMHShowID = "GBL362";
    public string checkCategoryColorWCMH(string showid, string regno)
    {
        string categoryColor = "";
        try
        {
            if (showid == WCMHShowID)
            {
                categoryColor = catWCMHCategoryDefault;//***
                string sqlCheckPromo = "Select * From tb_User_Promo Where ShowID='" + showid + "' And Regno In (Select RegGroupID From tb_RegDelegate Where ShowID='GBL362' And recycle=0 And Regno='" + regno + "')";
                DataTable dt = fn.GetDatasetByCommand(sqlCheckPromo, "ds").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    string promocode = dt.Rows[0]["Promo_Code"] != DBNull.Value ? dt.Rows[0]["Promo_Code"].ToString() : "";
                    if (promocode == catWCMHRed1 || promocode == catWCMHRed2)
                    {
                        categoryColor = catWCMHColorRed;
                    }
                    else if (promocode == catWCMHGreen)
                    {
                        categoryColor = catWCMHColorGreen;
                    }
                }
                else
                {
                    string category = fn.GetDataByCommand("Select con_CategoryId From tb_RegDelegate Where recycle=0 And ShowID='" + showid + "' And Regno='" + regno + "'", "con_CategoryId");
                    if (category == catWCMHOrganiser)
                    {
                        categoryColor = catWCMHColorOrganiser;
                    }
                    else if (category == catWCMHCrew)
                    {
                        categoryColor = catWCMHColorCrew;
                    }
                    else if (category == catWCMHInvitedGuest)
                    {
                        categoryColor = catWCMHColorInvitedGuest;
                    }
                    else if (category == catWCMHMedia)
                    {
                        categoryColor = catWCMHColorMedia;
                    }
                    else if (category == catWCMHSpeaker)
                    {
                        categoryColor = catWCMHColorSpeaker;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return categoryColor;
    }
    public string checkCategoryWCMH(string showid, string regno)
    {
        string categoryColor = "";
        try
        {
            if (showid == WCMHShowID)
            {
                categoryColor = catWCMHCategoryDefault;//***
                string sqlCheckPromo = "Select * From tb_User_Promo Where ShowID='" + showid + "' And Regno In (Select RegGroupID From tb_RegDelegate Where ShowID='GBL362' And recycle=0 And Regno='" + regno + "')";
                DataTable dt = fn.GetDatasetByCommand(sqlCheckPromo, "ds").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    string promocode = dt.Rows[0]["Promo_Code"] != DBNull.Value ? dt.Rows[0]["Promo_Code"].ToString() : "";
                    if (promocode == catWCMHRed1 || promocode == catWCMHRed2)
                    {
                        categoryColor = promocode;
                    }
                    else if (promocode == catWCMHGreen)
                    {
                        categoryColor = promocode;
                    }
                }
                else
                {
                    string category = fn.GetDataByCommand("Select con_CategoryId From tb_RegDelegate Where recycle=0 And ShowID='" + showid + "' And Regno='" + regno + "'", "con_CategoryId");
                    if (category == catWCMHOrganiser)
                    {
                        categoryColor = catWCMHCategoryOrganiser;
                    }
                    else if (category == catWCMHCrew)
                    {
                        categoryColor = catWCMHCategoryCrew;
                    }
                    else if (category == catWCMHInvitedGuest)
                    {
                        categoryColor = catWCMHCategoryInvitedGuest;
                    }
                    else if (category == catWCMHMedia)
                    {
                        categoryColor = catWCMHCategoryMedia;
                    }
                    else if (category == catWCMHSpeaker)
                    {
                        categoryColor = catWCMHCategorySpeaker;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return categoryColor;
    }
    #endregion
    public bool SetPrintVisiable(string statusname, string regno, string groupid, string flowid, string showid)//(string regno, string regGroupID, string showID, string invoicestatus, string paymentMethod)
    {
        bool rtn = false;
        #region Old Comment
        //bool rtn = true;
        //if (regtype == "Visitor") rtn = false;
        #endregion
        #region 2019
        #region for paid registration
        //RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
        //bool isPaid = oHelp.isDelegatePaid(showID, regno, regGroupID);
        //if (isPaid)// || (paymentMethod == "5" || paymentMethod == "6"))
        //{
        //    rtn = true;
        //}
        #region WCMH
        //else//(WCMH)
        //{
        //    string category = fn.GetDataByCommand("Select con_CategoryId From tb_RegDelegate Where recycle=0 And ShowID='" + showID + "' And Regno='" + regno + "'", "con_CategoryId");
        //    if ( 
        //        (category == catWCMHOrganiser || category == catWCMHCrew
        //        || category == catWCMHInvitedGuest || category == catWCMHMedia
        //        || category == catWCMHSpeaker))//regno.StartsWith("3628") &&
        //    {
        //        rtn = true;
        //    }
        //}
        #endregion
        #endregion
        #region Visitor(free) registration
        //if (!string.IsNullOrEmpty(statusname) && statusname == "Success")
        //{
        //    rtn = true;
        //}
        #endregion
        #region Visitor Food Japan
        RegDelegateObj rgd = new RegDelegateObj(fn);
        string createdated = rgd.getRegDate(regno, groupid, showid);
        bool isEarlyReg = isNotOnsiteRegFJ(createdated);
        if (isEarlyReg)
        {
            if (!string.IsNullOrEmpty(statusname) && statusname == "Success")
            {
                rtn = true;
            }
        }
        else
        {
            string receiptNoFJ = getVisitorReceiptFJ(regno, groupid, flowid, showid);
            if(!string.IsNullOrEmpty(receiptNoFJ))
            {
                rtn = false;
            }
        }
        #endregion
        #endregion
        return rtn;
    }
    public bool SetEditButtonVisiable(string regno, string regGroupID, string showID, string categoryID, string statusname)
    {
        bool rtn = false;
        try
        {
            #region Old Comment (WCMH)
            ////if (!regno.StartsWith("3628"))
            //if (
            //    (categoryID != catWCMHOrganiser && categoryID != catWCMHCrew
            //    && categoryID != catWCMHInvitedGuest && categoryID != catWCMHMedia
            //    && categoryID != catWCMHSpeaker))
            //{
            //    rtn = true;
            //}
            #endregion
            #region 2019
            //if (!string.IsNullOrEmpty(statusname) && statusname == "Success")
            {
                rtn = true;
            }
            #endregion
        }
        catch (Exception ex)
        { }

        return rtn;
    }
    public string SetEditUrl(string regno, string groupid, string FlowID, string showID, string categoryID, string statusname)
    {
        string rtn = "#";
        try
        {
            #region Old Comment (WCMH)
            ////if (!regno.StartsWith("3628"))
            //if (
            //    (categoryID != catWCMHOrganiser && categoryID != catWCMHCrew
            //    && categoryID != catWCMHInvitedGuest && categoryID != catWCMHMedia
            //    && categoryID != catWCMHSpeaker))
            //{
            //    string route = "https://www.event-reg.biz/registration/OnsiteEditIndex.aspx?EID=" + regno;
            //    rtn = route;
            //}
            #endregion
            #region 2019
            ////if (!string.IsNullOrEmpty(statusname) && statusname == "Success")
            //{
            //    string route = "~/OnsiteEditIndex.aspx?EID=" + regno;//https://www.event-reg.biz/registration/OnsiteEditIndex.aspx?EID=
            //    rtn = route;
            //}
            #endregion

            #region for Food Japan
            FlowControler fCon = new FlowControler(fn);
            FlowMaster flwMaster = fCon.GetFlowMasterConfig(FlowID);
            if (flwMaster.FlowType == SiteFlowType.FLOW_INDIVIDUAL)//***
            {
                string editStep = flwMaster.isMainDelegateEditStep.ToString();
                FlowControler editFlwCtrl = new FlowControler(fn, FlowID, editStep);
                string page = editFlwCtrl.CurrIndexModule;
                string step = editStep;
                string grpNum = groupid;
                string route = fCon.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                rtn = "../" + route;
            }
            else
            {
                string editStep = flwMaster.isMemberDelegateEditStep.ToString();
                FlowControler editFlwCtrl = new FlowControler(fn, FlowID, editStep);
                string page = editFlwCtrl.CurrIndexModule;
                string step = editStep;
                string grpNum = groupid;
                string route = fCon.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                rtn = "../" + route;
            }
            #endregion
            }
        catch (Exception ex)
        { }

        return rtn;
    }
    public bool SetEditOtherCatButtonVisiable(string regno, string regGroupID, string showID)
    {
        bool rtn = false;
        try
        {
            string category = fn.GetDataByCommand("Select con_CategoryId From tb_RegDelegate Where recycle=0 And ShowID='" + showID + "' And Regno='" + regno + "'", "con_CategoryId");
            if (
                (category == catWCMHOrganiser || category == catWCMHCrew
                || category == catWCMHInvitedGuest || category == catWCMHMedia
                || category == catWCMHSpeaker))////regno.StartsWith("3628") &&
            {
                rtn = true;
            }
        }
        catch(Exception ex)
        { }

        return rtn;
    }
    public string SetEditUrlOtherCat(string regno, string regGroupID, string FlowID, string showID)
    {
        string rtn = "#";
        try
        {
            string category = fn.GetDataByCommand("Select con_CategoryId From tb_RegDelegate Where recycle=0 And ShowID='" + showID + "' And Regno='" + regno + "'", "con_CategoryId");
            if (
                (category == catWCMHOrganiser || category == catWCMHCrew
                || category == catWCMHInvitedGuest || category == catWCMHMedia
                || category == catWCMHSpeaker))////regno.StartsWith("3628") &&
            {
                string grpNum = cFun.EncryptValue(regGroupID);
                string step = "1";
                string page = "https://www.event-reg.biz/registration/RegDelegateOnsiteWCMHAddionalEdit.aspx";
                FlowControler Flw = new FlowControler(fn);
                string route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                rtn = route;
            }
        }
        catch (Exception ex)
        { }

        return rtn;
    }
    #endregion

    private bool checkData(string showID, string strRegno)
    {
        bool isPaid = false;
        try
        {
            #region for paid registration
            //string query = "Select reg_otherInstitution,isNull(InstitutionName,reg_otherInstitution) InstitutionName,* From GetRegIndivAllByShowID(@SHWID) Where Regno='" + strRegno + "'";
            //List<SqlParameter> pList = new List<SqlParameter>();
            //SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            //spar.Value = showID;
            //pList.Add(spar);
            //DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
            //if (dt.Rows.Count > 0)
            //{
            //    string invoiceStatus = dt.Rows[0]["Invoice_status"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["Invoice_status"].ToString()) ? dt.Rows[0]["Invoice_status"].ToString() : "0") : "0";
            //    string paymentMethod = dt.Rows[0]["PaymentMethod"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["PaymentMethod"].ToString()) ? dt.Rows[0]["PaymentMethod"].ToString() : "0") : "0";
            //    if (invoiceStatus == "1")// || (paymentMethod == "5" || paymentMethod == "6"))
            //    {
            //        isPaid = true;
            //    }
            //    else
            //    {
            //        bool isOtherCat = checkIsOtherCategory(showID, strRegno);
            //        isPaid = isOtherCat;
            //    }
            //}
            #endregion
            #region Visitor(free) registration
            string query = "Select * From GetRegIndivAllByShowID(@SHWID) "
                + " Where reg_urlFlowID In ('" + FoodJapanIndivReg + "','" + FoodJapanGroupReg + "') And Regno='" + strRegno + "'";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                string regStatus = dt.Rows[0]["status_name"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["status_name"].ToString()) ? dt.Rows[0]["status_name"].ToString() : "0") : "0";
                if (regStatus == "Success")
                {
                    isPaid = true;
                }
            }
            #endregion
        }
        catch (Exception ex)
        { }

        return isPaid;
    }
    public bool checkIsOtherCategory(string showid, string regno)
    {
        bool isOtherCat = false;
        try
        {
            if (showid == WCMHShowID)
            {
                string category = fn.GetDataByCommand("Select con_CategoryId From tb_RegDelegate Where recycle=0 And ShowID='" + showid + "' And Regno='" + regno + "'", "con_CategoryId");
                if (category == catWCMHOrganiser)
                {
                    isOtherCat = true;
                }
                else if (category == catWCMHCrew)
                {
                    isOtherCat = true;
                }
                else if (category == catWCMHInvitedGuest)
                {
                    isOtherCat = true;
                }
                else if (category == catWCMHMedia)
                {
                    isOtherCat = true;
                }
                else if (category == catWCMHSpeaker)
                {
                    isOtherCat = true;
                }
            }
            else if (showid == SHCShowID)
            {
                string query = "Select c.reg_CategoryName From tb_RegDelegate as r Left Join ref_reg_Category as c On r.con_CategoryId=c.reg_CategoryId Where r.ShowID=@SHWID And Regno='" + regno + "'";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);
                string categoryName = fn.GetDataByCommand(query, "reg_CategoryName", pList);
                if (categoryName == _CHAIRMAN || categoryName == _CREW || categoryName == _EXHIBITOR || categoryName == _FACULTY || categoryName == _ORGANISER || categoryName == _SPEAKER)
                {
                    isOtherCat = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isOtherCat;
    }
    #region btnClose_Click
    protected void btnClose_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["Evt"] != null)
        {
            Response.Redirect("OnsiteSearch.aspx?Evt=" + Request.QueryString["Evt"].ToString());
        }
    }
    #endregion
    #region btnConfirm_Click
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        string regno = btn.CommandArgument;
        if (!string.IsNullOrEmpty(regno) && !string.IsNullOrWhiteSpace(regno)
            && !string.IsNullOrEmpty(txtShowID.Text) && !string.IsNullOrWhiteSpace(txtShowID.Text))
            //!string.IsNullOrEmpty(txtRegno.Text) && !string.IsNullOrWhiteSpace(txtRegno.Text)
        {
            //string regno = txtRegno.Text.Trim();
            string showID = txtShowID.Text.Trim();
            ////CallPrintSunny(showID, regno);
            //Response.Redirect("OnsiteScanSuccess.aspx?Evt=" + cFun.EncryptValue(showID) + "&r=" + cFun.EncryptValue(regno) + "&type=2");
            Response.Redirect("OnsiteAcknowledgement.aspx?Evt=" + cFun.EncryptValue(showID) + "&r=" + cFun.EncryptValue(regno) + "&type=2");
        }
        else
        {
            Response.Redirect("~/404.aspx");
        }
    }
    #endregion

    public string GetPrintStatusForm1B(string regno)
    {
        string rtn = "";
        try
        {
            ECGFunctionality eFun = new ECGFunctionality();

            string sql = string.Format("select* from tb_Onsite_PrintLog where showID = '{0}' and Pregno = '{1}'", txtShowID.Text, regno);
            DataTable dt = eFun.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 0)
                rtn = "YES";
        }
        catch { }
        return rtn;
    }
    private bool checkDataForm1B(string showID, string strRegno)
    {
        bool isPaid = false;
        try
        {
            ECGFunctionality eFun = new ECGFunctionality();
            string sql = "select * from  vw_regList_Teacher where regno=@Rno";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("Rno", SqlDbType.NVarChar);
            spar.Value = strRegno;
            pList.Add(spar);
            DataTable dt = eFun.GetDatasetByCommand(sql, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                isPaid = true;
            }
        }
        catch { }

        return isPaid;
    }

    #region SSBFE
    public string getCategorySSBFE(string regno, string flowid, string categoryName)
    {
        string result = "";
        try
        {
            if (!string.IsNullOrEmpty(categoryName))
            {
                result = categoryName;
            }
            else
            {
                FlowControler fControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = fControl.GetFlowMasterConfig(flowid);
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    result = "Exhibitor";
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    #endregion

    #region Check Gala Dinner
    public bool HasGalaDinner(string ShowID, string Regno)
    {
        bool hasGala = false;
        try
        {
            string sql = @"select * from tb_OrderDetials od,tb_ConfItem c where  Regid='{1}' and c.con_itemId=od.ItemId   and ShowID='{0}'  And c.isDeleted=0
and orderNO in (select OrderNo from tb_Invoice i, tb_Order o where i.InvoiceId = o.InvoiceID and i.Invoice_status = 1) and con_ItemName like '%Gala%'";
            sql = string.Format(sql, ShowID, Regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables[0];
            if (dt.Rows.Count > 0)
            {
                hasGala = true;
            }
            else
            {
                string sqlDependent = @"select * from tb_OrderDetials od,tb_ConfDependentItem d where  Regid='{1}' and d.con_DependentID=od.ItemId   and ShowID='{0}'  And d.isDeleted=0
and orderNO in (select OrderNo from tb_Invoice i, tb_Order o where i.InvoiceId = o.InvoiceID and i.Invoice_status = 1) and con_ItemName like '%Gala%'";
                sqlDependent = string.Format(sqlDependent, ShowID, Regno);
                DataTable dtDependent = fn.GetDatasetByCommand(sqlDependent, "DT").Tables[0];
                if (dtDependent.Rows.Count > 0)
                {
                    hasGala = true;
                }
            }
        }
        catch (Exception ex) { }

        return hasGala;
    }
    #endregion

    #region get master class and workshop
    //    public string getGalaMasterclassWorkshop(string ShowID, string Regno)
    //    {
    //        string itemName = "";
    //        try
    //        {
    //            string sql = @"select * from tb_OrderDetials od,tb_ConfItem c where  Regid='{1}' and c.con_itemId=od.ItemId   and ShowID='{0}'  And c.isDeleted=0
    //and orderNO in (select OrderNo from tb_Invoice i, tb_Order o where i.InvoiceId = o.InvoiceID and i.Invoice_status = 1) and (con_ItemName like '%Masterclass%' Or con_ItemName like '%Workshop%' Or con_ItemName like '%Gala%' Or con_ItemName like '%Accompanying%')";
    //            sql = string.Format(sql, ShowID, Regno);
    //            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables[0];
    //            if (dt.Rows.Count > 0)
    //            {
    //                foreach(DataRow dr in dt.Rows)
    //                {
    //                    itemName += (dr["con_ItemName"] != DBNull.Value ? dr["con_ItemName"].ToString() : "") + ",";
    //                }
    //            }
    //            //else
    //            {
    //                string sqlDependent = @"select * from tb_OrderDetials od,tb_ConfDependentItem d where  Regid='{1}' and d.con_DependentID=od.ItemId   and ShowID='{0}'  And d.isDeleted=0
    //and orderNO in (select OrderNo from tb_Invoice i, tb_Order o where i.InvoiceId = o.InvoiceID and i.Invoice_status = 1) and (con_ItemName like '%Masterclass%' Or con_ItemName like '%Workshop%' Or con_ItemName like '%Gala%' Or con_ItemName like '%Accompanying%')";
    //                sqlDependent = string.Format(sqlDependent, ShowID, Regno);
    //                DataTable dtDependent = fn.GetDatasetByCommand(sqlDependent, "DT").Tables[0];
    //                if (dtDependent.Rows.Count > 0)
    //                {
    //                    foreach (DataRow drDependent in dtDependent.Rows)
    //                    {
    //                        itemName += (drDependent["con_ItemName"] != DBNull.Value ? drDependent["con_ItemName"].ToString() : "") + ",";
    //                    }
    //                }
    //            }
    //        }
    //        catch { }

    //        return itemName.TrimEnd(',');
    //    }
    #endregion

    #region CET
    public bool checkGotNRICNumber(string ShowID, string Regno)
    {
        bool hasNRIC = false;
        try
        {
            string sql = @"Select * From tb_RegDelegate Where ShowID='{0}' And Regno='{1}' And recycle=0";
            sql = string.Format(sql, ShowID, Regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables[0];
            if (dt.Rows.Count > 0)
            {
                string nricNo = dt.Rows[0]["reg_PassNo"] != DBNull.Value ? dt.Rows[0]["reg_PassNo"].ToString() : "";
                string citizenType = dt.Rows[0]["reg_Affiliation"] != DBNull.Value ? dt.Rows[0]["reg_Affiliation"].ToString() : "";
                if((citizenType == "41" || citizenType == "42") && (!string.IsNullOrEmpty(nricNo) && !string.IsNullOrWhiteSpace(nricNo)))
                {
                    hasNRIC = true;
                }
                else if(citizenType == "43")
                {
                    hasNRIC = true;
                }
            }
        }
        catch { }

        return hasNRIC;
    }
    #endregion

    #region Food Japan
    public string getVisitorReceiptFJ(string regno, string groupid, string flowid, string showid)
    {
        string rtnValue = "";
        try
        {
            string sql = string.Format("Select ReceiptNumber From tb_RegVisitorOnsitePaymentReceipt Where RefRegno='{0}' And RegGroupID='{1}' And FlowID='{2}' And ShowID='{3}'"
                , regno, groupid, flowid, showid);
            rtnValue = fn.GetDataByCommand(sql, "ReceiptNumber");
            if(rtnValue == "0")
            {
                rtnValue = "";
            }
        }
        catch(Exception ex)
        { }

        return rtnValue;
    }
    public bool isNotOnsiteRegFJ(string RegisteredDate)
    {
        bool isEarly = false;
        DateTime regDeadlineDate = cFun.ParseDateTime("30/10/2019 23:59:59");
        DateTime RegDate = DateTime.Today;
        if (!string.IsNullOrEmpty(RegisteredDate))
            RegDate = DateTime.Parse(RegisteredDate);
        if (DateTime.Compare(regDeadlineDate, RegDate) > 0)
        {
            if (DateTime.Compare(regDeadlineDate, DateTime.Now) > 0)
                isEarly = true;
        }

        return isEarly;
    }
    public bool SetUpdateReceiptVisible(string statusname, string regno, string groupid, string flowid, string showid)//(string regno, string regGroupID, string showID, string invoicestatus, string paymentMethod)
    {
        bool rtn = false;
        #region Visitor Food Japan
        RegDelegateObj rgd = new RegDelegateObj(fn);
        string createdated = rgd.getRegDate(regno, groupid, showid);
        bool isEarlyReg = isNotOnsiteRegFJ(createdated);
        if (isEarlyReg)
        {
            if (!string.IsNullOrEmpty(statusname) && statusname == "Success")
            {
                rtn = false;
            }
        }
        else
        {
            rtn = true;
            //string receiptNoFJ = getVisitorReceiptFJ(regno, groupid, flowid, showid);
            //if (!string.IsNullOrEmpty(receiptNoFJ))
            //{
            //    rtn = false;
            //}
        }
        #endregion
        return rtn;
    }
    protected void btnUpdateReceiptNumber_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        string regno = btn.CommandArgument;
        string showID = txtShowID.Text.Trim();
        if (!string.IsNullOrEmpty(showID))
        {
            string url = string.Format("OnsiteUpdateReceipt_Visitor.aspx?Evt={0}&Regno={1}&type={2}", showID, regno, "2");
            string jVal = string.Format("window.open('{0}','_newtab');", url);
            Page.ClientScript.RegisterStartupScript(
            this.GetType(), "OpenWindow", jVal, true);
        }
    }
    #endregion
}
public class ECGFunctionality : BaseFunctionality
{
    public ECGFunctionality()
    {
        if (ConfigurationManager.ConnectionStrings["ECGCMSConnString"] != null)
        {
            string constr = ConfigurationManager.ConnectionStrings["ECGCMSConnString"].ConnectionString;
            base.ConnString = constr;

        }
    }


}
