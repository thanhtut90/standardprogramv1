﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.RegOnsite;
using Newtonsoft.Json;
using System.Data;
using Corpit.Registration;
using Corpit.Utilities;
using System.Data.SqlClient;

public partial class OnSite_OnsiteScanBadgePrint : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    //string showSARC = "EAD351";
    CommonFuns cFunz = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string regno = "";
            string showID = "";
            if (Request.QueryString["Regno"] != null)
            {
                regno = Request.QueryString["Regno"].ToString();
            }
            else if (Session["RegUserNo"] != null)
            {
                regno = Session["RegUserNo"].ToString();
            }
            if (Request.QueryString["SH"] != null)
            {
                showID = Request.QueryString["SH"].ToString();
            }
            if (!string.IsNullOrEmpty(regno))
            {
                string altertNoti = "";
                PrintDataObj pObj = GetRegobj(showID, regno, ref altertNoti);
                txtName.Text = GetTxtPrintRead(pObj.FullName);
                txtCompany.Text = GetTxtPrintRead(pObj.Company);
                txtCountry.Text = GetTxtPrintRead(pObj.Country);
                txtCategory.Text = GetTxtPrintRead(pObj.Category);
                txtRegno.Text = GetTxtPrintRead(regno);

                txtFirstName.Text = GetTxtPrintRead(pObj.FirstName);
                txtLastName.Text = GetTxtPrintRead(pObj.LastName);
                txtEmailAddress.Text = GetTxtPrintRead(pObj.EmailAddress);
                txtMobileNo.Text = GetTxtPrintRead(pObj.MobileNo);

                if (Request.QueryString["type"] != null)
                {
                    txtRtnType.Text = Request.QueryString["type"].ToString();
                }

                txtShowID.Text = cFunz.EncryptValue(showID);
            }
        }
    }
    private string GetTxtPrintRead(string str)
    {
        string rtn = ".";
        if (string.IsNullOrEmpty(str)) rtn = ".";
        else
            //rtn= str.Trim().Replace(" ", "^");
            rtn = str.Trim().Replace(" ", "~");

        return rtn;
    }
    #region GetRegobj
    #region Old Comment
    //private PrintDataObj GetRegobj(string showID, string regno, ref string altertNoti)
    //{
    //    PrintDataObj pObj = new PrintDataObj();
    //    try
    //    {
    //        RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
    //        DataTable dt = oHelp.onsiteDelegateByRegno(showID, regno);
    //        string FullName = "";
    //        string Jobtitle = "";
    //        string Company = "";
    //        string Country = "";
    //        string Category = "";
    //        if (dt.Rows.Count > 0) //ONsite
    //        {

    //            Jobtitle = dt.Rows[0]["reg_Designation"].ToString();
    //            Company = dt.Rows[0]["reg_organization"].ToString();
    //            Country = dt.Rows[0]["RegCountry"].ToString();
    //            if (showID == showSARC)
    //            {
    //                FullName = (dt.Rows[0]["Salutation"].ToString() + " " + dt.Rows[0]["reg_oName"].ToString()).Trim();
    //                Category = "";

    //            }
    //            else
    //            {
    //                Category = dt.Rows[0]["reg_Category"].ToString().Trim().ToUpper();
    //                FullName = (dt.Rows[0]["reg_oName"].ToString()).Trim();
    //            }
    //        }
    //        else { //Pre Reg
    //            dt = oHelp.PreRegDelegateByRegno(showID, regno);
    //            if (dt.Rows.Count > 0)
    //            {
    //                //preReg
    //                if (showID == showSARC)
    //                {
    //                    FullName = (dt.Rows[0]["Salutation"].ToString() + " " + dt.Rows[0]["reg_oName"].ToString()).Trim();
    //                    Jobtitle = dt.Rows[0]["reg_Designation"].ToString();
    //                    Company = dt.Rows[0]["InstitutionName"].ToString();
    //                    Country = dt.Rows[0]["RegCountry"].ToString();
    //                    Category = "";

    //                    string regGroupID = dt.Rows[0]["RegGroupID"].ToString();
    //                    bool isPaid = oHelp.isDelegatePaid(showID, regno, regGroupID);

    //                    if (!isPaid)
    //                        altertNoti = " Please proceed to onsite registration counter to complete your payment    ";

    //                    if (dt.Rows[0]["reg_Additional5"].ToString() == "1")
    //                        altertNoti += "Delegate has requested for vegetarian meal.";

    //                }
    //                else
    //                {
    //                    FullName = (dt.Rows[0]["reg_Fname"].ToString() + " " + dt.Rows[0]["reg_Lname"].ToString()).Trim().ToUpper();
    //                    Jobtitle = dt.Rows[0]["reg_Designation"].ToString().ToUpper();
    //                    Company = dt.Rows[0]["reg_Address1"].ToString().ToUpper();
    //                    Country = dt.Rows[0]["RegCountry"].ToString().ToUpper();
    //                    Category = "VISITOR".ToUpper();
    //                }



    //            }
    //            else regno = "";

    //        }
    //        if (!string.IsNullOrEmpty(regno))
    //        {
    //            jBQCode jData = new jBQCode();
    //            jData.ShowName = showID;
    //            jData.BarCode = regno;
    //            jData.Regno = regno;
    //            BarcodeMaker bMaker = new BarcodeMaker();
    //            string burl = bMaker.MakeBarcode(jData);
    //            if (!string.IsNullOrEmpty(burl))
    //            {
    //                pObj.ShowID = showID;
    //                pObj.Regno = regno;
    //                pObj.FullName = FullName.ToUpper();
    //                pObj.Jobtitle = Jobtitle.ToUpper();
    //                pObj.Company = Company.ToUpper();
    //                pObj.Country = Country.ToUpper();
    //                pObj.Category = Category.ToUpper();
    //                pObj.BCodeURL = burl;
    //            }
    //        }
    //    }
    //    catch { }


    //    return pObj;
    //}
    #endregion
    private PrintDataObj GetRegobj(string showID, string regno, ref string altertNoti)
    {
        PrintDataObj pObj = new PrintDataObj();
        try
        {
            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            DataTable dt = oHelp.onsiteDelegateByRegno(showID, regno);
            string FullName = "";
            string FirstName = "";
            string LastName = "";
            string Jobtitle = "";
            string Company = "";
            string Country = "";
            string EmailAddress = "";
            string MobileNo = "";
            string Category = "";
            if (dt.Rows.Count > 0) //ONsite
            {
                Jobtitle = dt.Rows[0]["reg_Designation"].ToString();
                Company = dt.Rows[0]["reg_organization"].ToString();
                Country = dt.Rows[0]["RegCountry"].ToString();
                Category = dt.Rows[0]["reg_Category"].ToString().Trim().ToUpper();
                FullName = (dt.Rows[0]["reg_oName"].ToString()).Trim();
                FirstName = (dt.Rows[0]["reg_FName"].ToString()).Trim();
                LastName = (dt.Rows[0]["reg_LName"].ToString()).Trim();
                EmailAddress = (dt.Rows[0]["reg_Email"].ToString()).Trim();
                MobileNo = (dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString()).Trim();
            }
            else
            { //Pre Reg
                dt = oHelp.PreRegDelegateByRegno(showID, regno);
                if (dt.Rows.Count > 0)
                {
                    //preReg
                    string flName = dt.Rows[0]["reg_FName"].ToString() + " " + dt.Rows[0]["reg_LName"].ToString();
                    FullName = (dt.Rows[0]["reg_oName"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_oName"].ToString()) ? dt.Rows[0]["reg_oName"].ToString() : flName) : flName).Trim();
                    FirstName = dt.Rows[0]["reg_FName"].ToString().Trim();
                    LastName = dt.Rows[0]["reg_LName"].ToString().Trim();
                    Jobtitle = dt.Rows[0]["reg_Address2"].ToString();//dt.Rows[0]["reg_Designation"].ToString();
                    Company = dt.Rows[0]["reg_Address1"].ToString();//dt.Rows[0]["InstitutionName"].ToString();
                    Country = dt.Rows[0]["RegCountry"].ToString();
                    EmailAddress = dt.Rows[0]["reg_Email"].ToString();
                    MobileNo = dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString();
                    Category = getCategorySHC(regno, showID);// "";

                    string regGroupID = dt.Rows[0]["RegGroupID"].ToString();
                    bool isPaid = oHelp.isDelegatePaid(showID, regno, regGroupID);

                    if (!isPaid)
                    {
                        if (Category == _CHAIRMAN || Category == _CREW || Category == _EXHIBITOR || Category == _FACULTY || Category == _ORGANISER || Category == _SPEAKER)
                        {
                            isPaid = true;
                        }
                        else
                        {
                            altertNoti = " Please proceed to onsite registration counter to complete your payment    ";
                        }
                    }
                }
                else
                {
                    regno = "";
                }
            }
            if (!string.IsNullOrEmpty(regno))
            {
                jBQCode jData = new jBQCode();
                jData.ShowName = showID;
                jData.BarCode = regno;
                jData.Regno = regno;
                BarcodeMaker bMaker = new BarcodeMaker();
                string burl = bMaker.MakeBarcode(jData);
                if (!string.IsNullOrEmpty(burl))
                {
                    pObj.ShowID = showID;
                    pObj.Regno = regno;
                    pObj.FullName = FullName.ToUpper();
                    pObj.FirstName = FirstName.ToUpper();
                    pObj.LastName = LastName.ToUpper();
                    pObj.Jobtitle = Jobtitle.ToUpper();
                    pObj.Company = Company.ToUpper();
                    pObj.Country = Country.ToUpper();
                    pObj.EmailAddress = EmailAddress;
                    pObj.MobileNo = MobileNo;
                    pObj.Category = Category.ToUpper();
                    pObj.BCodeURL = burl;
                }
            }
        }
        catch { }


        return pObj;
    }
    #region SHC
    private string _CHAIRMAN = "CHAIRMAN";
    private string _CREW = "CREW";
    private string _EXHIBITOR = "EXHIBITOR";
    private string _FACULTY = "FACULTY";
    private string _ORGANISER = "ORGANISER";
    private string _SPEAKER = "SPEAKER";
    private string getCategorySHC(string regno, string showid)
    {
        string result = "";
        try
        {
            string sql = "Select * From tmp_SHCSendReminderEmailDB Where Regno='" + regno + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                result = dt.Rows[0]["Category"] != DBNull.Value ? dt.Rows[0]["Category"].ToString() : "";

                if(result == "PRECEPTORSHIP")
                {
                    result = "DELEGATE-P";
                }
            }
            else
            {
                //result = "DELEGATE";
                ////string query = "Select f.aff_name From tb_RegDelegate as r Left Join ref_Affiliation as f On r.reg_Affiliation=f.affid Where r.ShowID=@SHWID And Regno='" + regno + "'";
                string query = "Select c.reg_CategoryName From tb_RegDelegate as r Left Join ref_reg_Category as c On r.con_CategoryId=c.reg_CategoryId Where r.ShowID=@SHWID And Regno='" + regno + "'";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);
                result = fn.GetDataByCommand(query, "reg_CategoryName", pList);
                if (result == _CHAIRMAN || result == _CREW || result == _EXHIBITOR || result == _FACULTY || result == _ORGANISER || result == _SPEAKER)//(category == "Yes")
                {
                    //category = "PRECEPTORSHIP";
                }
                else
                {
                    result = "DELEGATE";
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    #endregion
    #endregion
}