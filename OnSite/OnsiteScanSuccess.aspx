﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnSite/OnsiteMaster.master" AutoEventWireup="true" CodeFile="OnsiteScanSuccess.aspx.cs" Inherits="OnSite_OnsiteScanSuccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="DivMain" class="row">
        <div id="DivHeader" style="padding-top: 30px;" class="container">
            <h4>Onsite Scan Success</h4>
        </div>
        <div id="DivSearchBox" class="container" style="padding-top: 30px;">
            <div class="row">
                <div class="col-md-4">Registration Number</div>
                <div class="col-md-4">
                    <asp:Label runat="server" ID="lblRegno" CssClass="form-control"></asp:Label>
                </div>
            </div>
            <br />
            <br />
            <div class="row">
       <div class="col-md-5"> 
                <div class="col-md-12">Please select post conference tour.<br />
                    <asp:RadioButtonList ID="rbItems" runat="server" CssClass="vertical-radio-buttons">
                        <asp:ListItem Text="Asian Civilisation Museum" Value="Asian Civilisation Museum"></asp:ListItem>
                        <asp:ListItem Text="Singapore Maritime Gallery" Value="Singapore Maritime Gallery"></asp:ListItem>
                        <asp:ListItem Text="Royal Albatross" Value="Royal Albatross"></asp:ListItem>
                        <asp:ListItem Text="Maritime Experiential Museum" Value="Maritime Experiential Museum"></asp:ListItem>
                        <asp:ListItem Text="S.E.A Aquarium" Value="S.E.A Aquarium"></asp:ListItem>
                    </asp:RadioButtonList>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator
                            ID="rfvItems" Enabled="false"
                            runat="server" ForeColor="Red"
                            ControlToValidate="rbItems"
                            ErrorMessage="*Required">
                        </asp:RequiredFieldValidator>
                </div>
           </div>
                   <div class="col-md-5"> 
                            <div class="col-md-12">Which breakout session are you most interested in?<br />
                    <asp:RadioButtonList ID="rbBreakoutSessions" runat="server" CssClass="vertical-radio-buttons">
                        <asp:ListItem Text="Asia Pacific - Regional Breakout" Value="Asia Pacific - Regional Breakout"></asp:ListItem>
                        <asp:ListItem Text="Indian Ocean - Regional Breakout" Value="Indian Ocean - Regional Breakout"></asp:ListItem>
                        <asp:ListItem Text="Atlantic Ocean - Regional Breakout" Value="Atlantic Ocean - Regional Breakout"></asp:ListItem>
                        <asp:ListItem Text='"Today is the Future of the Past" World Maritime Heritage Society' Value='"Today is the Future of the Past" World Maritime Heritage Society'></asp:ListItem>
                    </asp:RadioButtonList>
                    </div>
                    <div class="col-md-2">
                         <asp:RequiredFieldValidator
                                ID="rfvBreakoutSessions" Enabled="false"
                                runat="server" ForeColor="Red"
                                ControlToValidate="rbBreakoutSessions"
                                ErrorMessage="*Required">
                            </asp:RequiredFieldValidator>
                </div>
                   </div>
            </div>
       
            <div class="row">
                <div class="col-md-2">&nbsp;</div>
                <div class="col-md-5" id="divColorCategory" runat="server">
                    <h4>Please insert the paper as shown.<br /></h4>
                    <asp:Image ID="imgColorCategory" runat="server" ImageUrl="#" />
                </div>
            </div>
            <br />
            <br />
            <div class="row">
                <div class="col-md-6">&nbsp;</div>
                <div class="col-md-2">
                    <asp:Button runat="server" ID="btnSubmit" CssClass="btn MainButton btn-success" Text="Submit" OnClick="btnSubmit_Click" />
                </div>
            </div>
        </div>
        <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
        <asp:TextBox runat="server" ID="txtRegGroupID" Visible="false" Text=""></asp:TextBox>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
</asp:Content>

