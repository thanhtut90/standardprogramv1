﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnSite/OnsiteMaster.master" AutoEventWireup="true" CodeFile="OnsiteReport_MasterList.aspx.cs" Inherits="OnsiteReport_MasterList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../Content/Default/bootstrap.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:Panel runat="server" ID="PanelKeyList" Visible="true" CssClass="container">
        <asp:TextBox runat="server" ID="txtShowID" Visible="false"></asp:TextBox>
        <div class="row">
            <div class="col-md-2"><asp:Label ID="lblSearch" runat="server" Text="Search By : " Font-Size="Large"></asp:Label></div>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="txtSearch" CssClass="form-control" AutoCompleteType="Disabled" Placeholder="Reg no. / Name / Company Name"> </asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Button runat="server" ID="btnSearch" CssClass="btn MainButton btn-block" Text="Search" OnClick="btnSearch_Click" /> 
            </div>
        </div>
        <br />
        <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster"  runat="server"  
            OnNeedDataSource="GKeyMaster_NeedDataSource" PageSize="10" Skin="Bootstrap">
            <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true"></ClientSettings>
            <MasterTableView AutoGenerateColumns="True" DataKeyNames="Registration Number" AllowPaging="true" CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="True" ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                <Columns>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </asp:Panel>
</asp:Content>

