﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnSite/OnsiteMaster.master" AutoEventWireup="true" CodeFile="OnsiteSearchMDA_ShowAllInfo.aspx.cs" Inherits="OnSite_OnsiteSearchMDA_ShowAllInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../Content/Green/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/Green/Site.css" rel="stylesheet" />
    <%--<script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/jquery-1.10.2.min.js"></script>--%>
    <script src="../Admin/js/plugins/jquery-1.9.1.min.js"></script>
    <script src="../Admin/Content/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }

        function closeModal() {
            $('#myModal').modal('hide');
            $('.modal-backdrop').remove();
        }
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="DivMain" class="row">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="DivHeader" style="padding-top: 30px;" class="container">
            <h2>Onsite Registraton Search</h2>
        </div>
        <div id="DivSearchBox" class="container" style="padding-top: 30px;">
            <div class="col-md-2" style="display:none;">Registration Flow List: </div>
            <div class="col-md-4" style="display:none;">
                <asp:DropDownList ID="ddl_flowList" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
            <div class="col-md-4">
                <asp:Panel ID="PanelShow" runat="server" DefaultButton="btnSearch">
                    <asp:TextBox runat="server" ID="txtSearch" CssClass="form-control" AutoCompleteType="Disabled"> </asp:TextBox>
                </asp:Panel>
            </div>
            <div class="col-md-2">
                <asp:Button runat="server" ID="btnSearch" CssClass="btn MainButton btn-block" Text="Search" OnClick="btnSearch_Onclick" /> 
            </div>
            <div class="col-md-2"> <asp:Button runat="server" ID="Button2" CssClass="btn  btn-danger" Text="Data Entry" OnClick="btnEntry_Onclick" Visible="false"/></div>
        </div>
        <div style="overflow-x: scroll;">
            <div style="padding-top: 30px;" class="container">
                <asp:Panel runat="server" ID="PanelList" Visible="false">
                    <asp:Label ID="lblUser" runat="server" Visible="false"></asp:Label>
                    <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true" 
                        EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                        OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="1000" OnItemDataBound="grid_ItemDataBound" 
                        OnPageIndexChanged="GKeyMaster_PageIndexChanged">
                            <ClientSettings AllowKeyboardNavigation="false" EnablePostBackOnRowClick="false">
                                <Selecting AllowRowSelect="true"></Selecting>
                                <%--<Scrolling AllowScroll="True" EnableVirtualScrollPaging="True" UseStaticHeaders="True"
                                        SaveScrollPosition="True"></Scrolling>--%>
                            </ClientSettings>
                            <%--<HeaderStyle Width="90px" />--%>
                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="Regno,RegGroupID" AllowFilteringByColumn="True" ShowFooter="false">
                                <CommandItemSettings ShowExportToExcelButton="False" ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                                <Columns>
                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RegGroupID" FilterControlAltText="Filter RegGroupID"
                                        HeaderText="RegGroupID" SortExpression="RegGroupID" UniqueName="RegGroupID" Visible="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                        HeaderText="Edit" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" class="btn btn-success btn-sm" CommandName="Editspp" 
                                                CommandArgument='<%#Eval("Regno") + ";" + Eval("RegGroupID") + ";" + Eval("reg_urlFlowID") %>' 
                                                OnCommand="lnkEdit_Command" CausesValidation="false">View QR Code</asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Regno" FilterControlAltText="Filter Regno"
                                        HeaderText="Regno" SortExpression="Regno" UniqueName="Regno">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="reg_Salutation" HeaderText="" UniqueName="reg_Salutation"
                                        FilterControlAltText="Filter reg_Salutation" SortExpression="reg_Salutation" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindSalutation(Eval("reg_Salutation").ToString(), Eval("reg_SalutationOthers").ToString()) %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_FName" FilterControlAltText="Filter reg_FName"
                                        HeaderText="" SortExpression="reg_FName" UniqueName="reg_FName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_LName" FilterControlAltText="Filter reg_LName"
                                        HeaderText="" SortExpression="reg_LName" UniqueName="reg_LName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_OName" FilterControlAltText="Filter reg_OName"
                                        HeaderText="" SortExpression="reg_OName" UniqueName="reg_OName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_PassNo" FilterControlAltText="Filter reg_PassNo"
                                        HeaderText="" SortExpression="reg_PassNo" UniqueName="reg_PassNo">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="reg_isReg" HeaderText="" UniqueName="reg_isReg"
                                        FilterControlAltText="Filter reg_isReg" SortExpression="reg_isReg" Exportable="true">
                                        <ItemTemplate>
                                            <%#Eval("reg_isReg") != null ? (Eval("reg_isReg").ToString() == "1" ? "Yes" : "No") : "No"%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_sgregistered" FilterControlAltText="Filter reg_sgregistered"
                                        HeaderText="" SortExpression="reg_sgregistered" UniqueName="reg_sgregistered">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_IDno" FilterControlAltText="Filter reg_IDno"
                                        HeaderText="" SortExpression="reg_IDno" UniqueName="reg_IDno">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Designation" FilterControlAltText="Filter reg_Designation"
                                        HeaderText="" SortExpression="reg_Designation" UniqueName="reg_Designation">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="reg_Profession" HeaderText="" UniqueName="reg_Profession"
                                        FilterControlAltText="Filter reg_Profession" SortExpression="reg_Profession" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindProfession(Eval("reg_Profession").ToString(), Eval("reg_otherProfession").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="reg_Department" HeaderText="" UniqueName="reg_Department"
                                        FilterControlAltText="Filter reg_Department" SortExpression="reg_Department" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindDepartment(Eval("reg_Department").ToString(), Eval("reg_otherDepartment").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="reg_Organization" HeaderText="" UniqueName="reg_Organization"
                                        FilterControlAltText="Filter reg_Organization" SortExpression="reg_Organization" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindOrganisation(Eval("reg_Organization").ToString(), Eval("reg_otherOrganization").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="reg_Institution" HeaderText="" UniqueName="reg_Institution"
                                        FilterControlAltText="Filter reg_Institution" SortExpression="reg_Institution" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindInstitution(Eval("reg_Institution").ToString(), Eval("reg_otherInstitution").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address1" FilterControlAltText="Filter reg_Address1"
                                        HeaderText="" SortExpression="reg_Address1" UniqueName="reg_Address1">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address2" FilterControlAltText="Filter reg_Address2"
                                        HeaderText="" SortExpression="reg_Address2" UniqueName="reg_Address2">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address3" FilterControlAltText="Filter reg_Address3"
                                        HeaderText="" SortExpression="reg_Address3" UniqueName="reg_Address3">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address4" FilterControlAltText="Filter reg_Address4"
                                        HeaderText="" SortExpression="reg_Address4" UniqueName="reg_Address4">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_City" FilterControlAltText="Filter reg_City"
                                        HeaderText="" SortExpression="reg_City" UniqueName="reg_City">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_State" FilterControlAltText="Filter reg_State"
                                        HeaderText="" SortExpression="reg_State" UniqueName="reg_State">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_PostalCode" FilterControlAltText="Filter reg_PostalCode"
                                        HeaderText="" SortExpression="reg_PostalCode" UniqueName="reg_PostalCode">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="reg_Country" HeaderText="" UniqueName="reg_Country"
                                        FilterControlAltText="Filter reg_Country" SortExpression="reg_Country" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindCountry(Eval("reg_Country").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="reg_RCountry" HeaderText="" UniqueName="reg_RCountry"
                                        FilterControlAltText="Filter reg_RCountry" SortExpression="reg_RCountry" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindCountry(Eval("reg_RCountry").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Tel" UniqueName="reg_Tel"
                                        FilterControlAltText="Filter reg_Tel" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("reg_Telcc")%><%#Eval("reg_Telac")%><%#Eval("reg_Tel")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Mobile" UniqueName="reg_Mobile"
                                        FilterControlAltText="Filter reg_Mobile" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("reg_Mobcc")%><%#Eval("reg_Mobac")%><%#Eval("reg_Mobile")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="reg_Fax" UniqueName="reg_Fax"
                                        FilterControlAltText="Filter reg_Fax" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("reg_Faxcc")%><%# Eval("reg_Faxac")%><%#Eval("reg_Fax")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Email" FilterControlAltText="Filter reg_Email"
                                        HeaderText="" SortExpression="reg_Email" UniqueName="reg_Email">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="reg_Affiliation" HeaderText="" UniqueName="reg_Affiliation"
                                        FilterControlAltText="Filter reg_Affiliation" SortExpression="reg_Affiliation" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindAffiliation(Eval("reg_Affiliation").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridTemplateColumn DataField="reg_Dietary" HeaderText="" UniqueName="reg_Dietary"
                                        FilterControlAltText="Filter reg_Dietary" SortExpression="reg_Dietary" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindDietary(Eval("reg_Dietary").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Nationality" FilterControlAltText="Filter reg_Nationality"
                                        HeaderText="" SortExpression="reg_Nationality" UniqueName="reg_Nationality">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Age" FilterControlAltText="Filter reg_Age"
                                        HeaderText="" SortExpression="reg_Age" UniqueName="reg_Age">
                                    </telerik:GridBoundColumn>

                                     <telerik:GridTemplateColumn DataField="reg_DOB" HeaderText="" UniqueName="reg_DOB"
                                        FilterControlAltText="Filter reg_DOB" SortExpression="reg_DOB" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindDOB(Eval("reg_DOB").ToString(),Eval("showid").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Gender" FilterControlAltText="Filter reg_Gender"
                                        HeaderText="" SortExpression="reg_Gender" UniqueName="reg_Gender">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Membershipno" FilterControlAltText="Filter reg_Membershipno"
                                        HeaderText="" SortExpression="reg_Membershipno" UniqueName="reg_Membershipno">
                                    </telerik:GridBoundColumn>


                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vName" FilterControlAltText="Filter reg_vName"
                                        HeaderText="" SortExpression="reg_vName" UniqueName="reg_vName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vDOB" FilterControlAltText="Filter reg_vDOB"
                                        HeaderText="" SortExpression="reg_vDOB" UniqueName="reg_vDOB">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vPassno" FilterControlAltText="Filter reg_vPassno"
                                        HeaderText="" SortExpression="reg_vPassno" UniqueName="reg_vPassno">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vIssueDate" FilterControlAltText="Filter reg_vIssueDate"
                                        HeaderText="" SortExpression="reg_vIssueDate" UniqueName="reg_vIssueDate">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vPassexpiry" FilterControlAltText="Filter reg_vPassexpiry"
                                        HeaderText="" SortExpression="reg_vPassexpiry" UniqueName="reg_vPassexpiry">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vEmbarkation" FilterControlAltText="Filter reg_vEmbarkation"
                                        HeaderText="" SortExpression="reg_vEmbarkation" UniqueName="reg_vEmbarkation">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_vArrivalDate" FilterControlAltText="Filter reg_vArrivalDate"
                                        HeaderText="" SortExpression="reg_vArrivalDate" UniqueName="reg_vArrivalDate">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="reg_vCountry" HeaderText="" UniqueName="reg_vCountry"
                                        FilterControlAltText="Filter reg_vCountry" SortExpression="reg_vCountry" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindCountry(Eval("reg_vCountry").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>


                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CName" FilterControlAltText="Filter UDF_CName"
                                        HeaderText="" SortExpression="UDF_CName" UniqueName="UDF_CName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_DelegateType" FilterControlAltText="Filter UDF_DelegateType"
                                        HeaderText="" SortExpression="UDF_DelegateType" UniqueName="UDF_DelegateType">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="" SortExpression="UDF_ProfCategory" UniqueName="UDF_ProfCategory"
                                        FilterControlAltText="Filter UDF_ProfCategory" Exportable="true">
                                        <ItemTemplate>
                                            <%# Eval("UDF_ProfCategory")%><%#Eval("UDF_ProfCategoryOther")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CPcode" FilterControlAltText="Filter UDF_CPcode"
                                        HeaderText="" SortExpression="UDF_CPcode" UniqueName="UDF_CPcode">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CLDepartment" FilterControlAltText="Filter UDF_CLDepartment"
                                        HeaderText="" SortExpression="UDF_CLDepartment" UniqueName="UDF_CLDepartment">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CAddress" FilterControlAltText="Filter UDF_CAddress"
                                        HeaderText="" SortExpression="UDF_CAddress" UniqueName="UDF_CAddress">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="UDF_CLCompany" FilterControlAltText="Filter UDF_CLCompany"
                                        HeaderText="" SortExpression="UDF_CLCompany" UniqueName="UDF_CLCompany">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn DataField="UDF_CCountry" HeaderText="" UniqueName="UDF_CCountry"
                                        FilterControlAltText="Filter UDF_CCountry" SortExpression="UDF_CCountry" Exportable="true">
                                        <ItemTemplate>
                                            <%# bindCountry(Eval("UDF_CCountry").ToString())%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>


                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorName" FilterControlAltText="Filter reg_SupervisorName"
                                        HeaderText="" SortExpression="reg_SupervisorName" UniqueName="reg_SupervisorName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorDesignation" FilterControlAltText="Filter reg_SupervisorDesignation"
                                        HeaderText="" SortExpression="reg_SupervisorDesignation" UniqueName="reg_SupervisorDesignation">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorContact" FilterControlAltText="Filter reg_SupervisorContact"
                                        HeaderText="" SortExpression="reg_SupervisorContact" UniqueName="reg_SupervisorContact">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_SupervisorEmail" FilterControlAltText="Filter reg_SupervisorEmail"
                                        HeaderText="" SortExpression="reg_SupervisorEmail" UniqueName="reg_SupervisorEmail">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Additional4" FilterControlAltText="Filter reg_Additional4"
                                        HeaderText="" SortExpression="reg_Additional4" UniqueName="reg_Additional4">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Additional5" FilterControlAltText="Filter reg_Additional5"
                                        HeaderText="" SortExpression="reg_Additional5" UniqueName="reg_Additional5">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn HeaderText="Status" SortExpression="status_name" UniqueName="status_name"
                                        FilterControlAltText="Filter status_name" Exportable="true">
                                        <ItemTemplate>
                                            <%#!string.IsNullOrEmpty(Eval("status_name").ToString()) ? Eval("status_name").ToString() : "Pending"%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_datecreated" FilterControlAltText="Filter reg_datecreated"
                                        HeaderText="Created Date" SortExpression="reg_datecreated" UniqueName="reg_datecreated">
                                    </telerik:GridBoundColumn>

                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                </asp:Panel>
            </div>
        </div>
    </div>
    <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
</asp:Content>

