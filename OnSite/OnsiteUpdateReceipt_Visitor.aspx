﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnSite/OnsiteMaster.master" AutoEventWireup="true" CodeFile="OnsiteUpdateReceipt_Visitor.aspx.cs" Inherits="OnSite_OnsiteUpdateReceipt_Visitor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        function limitText(limitField, limitNum) {
            var txtValue = limitField.value.replace(/\s/g, '')
            if (txtValue.length < limitNum) {
                //limitField.value = limitField.value.substring(0, limitNum);
                alert("Minimum 2 characters!");
            }
        }
        function validate() {
            var limitNum = 2;
            var errmsg = "";
            var isValid = true;
            var fname = document.getElementById("<%=txtReceiptNumber.ClientID%>").value;
            var fnameValue = fname.replace(/\s/g, '')
            if (fnameValue.length < limitNum) {
                isValid = false;
                errmsg += "Please enter Receipt Number";
            }
            var lname = document.getElementById("<%=txtSubmittedBy.ClientID%>").value;
            var lnameValue = lname.replace(/\s/g, '')
            if (lnameValue.length < limitNum) {
                if(!isValid) { errmsg += ", " }else{ errmsg += " " }
                errmsg += "Please enter Submitter Name";
                isValid = false;
            }
            if(!isValid)
            {
                alert("Minimum 2 characters in" + errmsg);
            }
            return isValid;
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="DivMain" class="row">
        <div id="DivHeader" style="padding-top: 30px;" class="container">
            <h4>Onsite - Update Receipt (Visitor)</h4>
        </div>
        <div id="divReceiptNumber" class="container" style="padding-top: 30px;">
            <div class="col-md-2">Receipt Number</div>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="txtReceiptNumber" CssClass="form-control" AutoCompleteType="Disabled" Enabled="true"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:RequiredFieldValidator ID="vcReceiptNumber" runat="server"
                ControlToValidate="txtReceiptNumber" Display="Dynamic"
                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revReceiptNumber" runat="server" ControlToValidate="txtReceiptNumber" ErrorMessage="Invalid entry"
                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div id="divSubmittedBy" class="container" style="padding-top: 30px;">
            <div class="col-md-2">Submitted By</div>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="txtSubmittedBy" CssClass="form-control" AutoCompleteType="Disabled" Enabled="true"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:RequiredFieldValidator ID="vctxtSubmittedBy" runat="server"
                ControlToValidate="txtSubmittedBy" Display="Dynamic"
                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revSubmittedBy" runat="server" ControlToValidate="txtSubmittedBy" ErrorMessage="Invalid entry"
                ValidationExpression="^[A-Za-z0-9\s-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                
            </div>
        </div>
        <div id="divButton" class="container" style="padding-top: 30px;">
            <div class="col-md-2">&nbsp;</div>
            <div class="col-md-4">
                &nbsp;
            </div>
            <div class="col-md-2">
                <asp:Button runat="server" ID="btnSubmit" CssClass="btn MainButton btn-success" Text="Submit" OnClick="btnSubmit_Click"
                    OnClientClick ="return validate();" />
            </div>
        </div>
        <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
        <asp:TextBox runat="server" ID="txtRegno" Visible="false" Text=""></asp:TextBox>
    </div>
</asp:Content>

