﻿using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.RegOnsite;
using System.Configuration;
using Corpit.Registration;
using Corpit.Site.Utilities;
using System.Text.RegularExpressions;

public partial class OnSite_OnsiteScanPrint_FJPublic : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    string FoodJapanPublicReg = "F378";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null)
            {
                txtScanResult.Focus();
                txtScanResult.Attributes.Add("autocomplete", "off");
                string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                if (string.IsNullOrWhiteSpace(showid))
                {
                    Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
                }
                else
                {
                    txtShowID.Text = showid;
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
            }
        }
    }
    protected void btnScan_Click(object sender, EventArgs e)
    {
        string strScanResult = txtScanResult.Text;
        if (string.IsNullOrEmpty(strScanResult) || string.IsNullOrWhiteSpace(strScanResult))
        {
            Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
        }
        else
        {
            string regno = strScanResult;
            if (Regex.IsMatch(regno, @"^\d+$"))
            { }
            else
            {
                regno = strScanResult.Substring(3, strScanResult.Length - 3);
            }
            string showID = txtShowID.Text.Trim();
            if (!string.IsNullOrEmpty(showID))
            {
                bool isSuccessDelegate = false;
                string regName = "";
                isSuccessDelegate = checkData(showID, regno, ref regName);

                if (isSuccessDelegate)
                {
                    string printStatus = "";
                    printStatus = GetPrintStatus(regno);
                    if (printStatus == "YES")
                    {
                        txtScanResult.Text = "";
                        txtScanResult.Focus();
                        Response.Write("<script>alert('Sorry, you already Collected.');</script>");
                    }
                    else
                    {
                        string msg = "";
                        CallPrintSunny(showID, regno, ref msg);
                        txtScanResult.Text = "";
                        txtScanResult.Focus();
                        if (string.IsNullOrEmpty(msg))
                        {
                            Response.Write("<script>alert('Collected successfully.');</script>");
                        }
                        else
                        {
                            Response.Write("<script>alert('" + msg + "');</script>");
                        }
                    }
                }
                else
                {
                    txtScanResult.Text = "";
                    txtScanResult.Focus();
                    Response.Write("<script>alert('Sorry, your registration is not confirmed yet. Please go to registration counter.');</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
            }
        }
    }
    private bool checkData(string showID, string strRegno,ref string regName)
    {
        bool isPaid = false;
        try
        {
            #region comment for paid registration
            string query = "Select reg_otherInstitution,isNull(InstitutionName,reg_otherInstitution) InstitutionName,* From GetRegIndivAllByShowID(@SHWID) "
                + " Where reg_urlFlowID In ('" + FoodJapanPublicReg + "') And Regno='" + strRegno + "'";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                string invoiceStatus = dt.Rows[0]["Invoice_status"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["Invoice_status"].ToString()) ? dt.Rows[0]["Invoice_status"].ToString() : "0") : "0";
                string paymentMethod = dt.Rows[0]["PaymentMethod"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["PaymentMethod"].ToString()) ? dt.Rows[0]["PaymentMethod"].ToString() : "0") : "0";
                if (invoiceStatus == "1")
                {
                    isPaid = true;
                    regName = dt.Rows[0]["reg_Fname"].ToString();
                }
            }
            #endregion
            #region Visitor (free) registration
            //string query = "Select * From GetRegIndivAllByShowID(@SHWID) "
            //    + " Where reg_urlFlowID In ('" + FoodJapanIndivReg + "','" + FoodJapanGroupReg + "') And Regno='" + strRegno + "'";
            //List<SqlParameter> pList = new List<SqlParameter>();
            //SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            //spar.Value = showID;
            //pList.Add(spar);
            //DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
            //if (dt.Rows.Count > 0)
            //{
            //    string regStatus = dt.Rows[0]["status_name"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["status_name"].ToString()) ? dt.Rows[0]["status_name"].ToString() : "0") : "0";
            //    if (regStatus == "Success")
            //    {
            //        isPaid = true;
            //    }
            //}
            #endregion
        }
        catch (Exception ex)
        { }

        return isPaid;
    }
    public string GetPrintStatus(string regno)
    {
        string rtn = "";
        try
        {
            string sql = string.Format("select* from tb_Onsite_PrintLog where showID = '{0}' and Pregno = '{1}'", txtShowID.Text, regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 0)
                rtn = "YES";
        }
        catch { }
        return rtn;
    }
    #region CallPrintSunny
    private void CallPrintSunny(string showID, string regno, ref string msg)
    {
        string altertNoti = "";
        PrintDataObj pObj = GetRegobj(showID, regno, ref altertNoti);

        if (!string.IsNullOrEmpty(pObj.Regno))
        {
            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            oHelp.AddPrintLog(showID, regno);
            msg = altertNoti;
        }
        else
        {
            altertNoti = "Invalid Regno";
            string jVal = string.Format("alert('{0}');", altertNoti);
            Page.ClientScript.RegisterStartupScript(
       this.GetType(), "OpenWindow", jVal, true);
        }
    }
    private PrintDataObj GetRegobj(string showID, string regno, ref string altertNoti)
    {
        PrintDataObj pObj = new PrintDataObj();
        try
        {
            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            DataTable dt = oHelp.onsiteDelegateByRegno(showID, regno);
            string FullName = "";
            string FirstName = "";
            string LastName = "";
            string Jobtitle = "";
            string Company = "";
            string Country = "";
            string EmailAddress = "";
            string MobileNo = "";
            string Category = "";
            if (dt.Rows.Count > 0) //ONsite
            {
                Jobtitle = dt.Rows[0]["reg_Address1"].ToString(); //dt.Rows[0]["reg_Designation"].ToString();
                Company = (dt.Rows[0]["reg_oName"].ToString()).Trim();// dt.Rows[0]["reg_organization"].ToString();
                Country = dt.Rows[0]["RegCountry"].ToString();
                Category = dt.Rows[0]["reg_Category"].ToString().Trim().ToUpper();
                FullName = (dt.Rows[0]["reg_FName"].ToString()).Trim();// (dt.Rows[0]["reg_oName"].ToString()).Trim();
                FirstName = (dt.Rows[0]["reg_FName"].ToString()).Trim();
                LastName = (dt.Rows[0]["reg_LName"].ToString()).Trim();
                EmailAddress = (dt.Rows[0]["reg_Email"].ToString()).Trim();
                MobileNo = (dt.Rows[0]["reg_Telcc"].ToString() + dt.Rows[0]["reg_Telac"].ToString() + dt.Rows[0]["reg_Tel"].ToString()).Trim(); //(dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString()).Trim();
            }
            else
            { //Pre Reg
                //dt = oHelp.PreRegDelegateByRegno(showID, regno);
                string sql = "Select Case When reg_oname is null Or reg_oname='' Then reg_FName + ' ' + reg_LName Else reg_OName End as FullName,reg_otherInstitution,isNull(InstitutionName,reg_otherInstitution) InstitutionName,* From GetRegIndivAllByShowID('" + showID + "') "
                    + " Where reg_urlFlowID In ('" + FoodJapanPublicReg + "') And Regno='" + regno + "'";
                dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    //preReg
                    string flName = dt.Rows[0]["reg_FName"].ToString();// + " " + dt.Rows[0]["reg_LName"].ToString();
                    FullName = flName;// (dt.Rows[0]["reg_oName"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_oName"].ToString()) ? dt.Rows[0]["reg_oName"].ToString() : flName) : flName).Trim();
                    FirstName = dt.Rows[0]["reg_FName"].ToString().Trim();
                    LastName = dt.Rows[0]["reg_LName"].ToString().Trim();
                    Jobtitle = dt.Rows[0]["reg_Address1"].ToString();//dt.Rows[0]["reg_Designation"].ToString();
                    Company = dt.Rows[0]["reg_oName"].ToString();//dt.Rows[0]["InstitutionName"].ToString();
                    Country = dt.Rows[0]["RegCountry"].ToString();
                    EmailAddress = dt.Rows[0]["reg_Email"].ToString();
                    MobileNo = dt.Rows[0]["reg_Telcc"].ToString() + dt.Rows[0]["reg_Telac"].ToString() + dt.Rows[0]["reg_Tel"].ToString(); //dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString();
                    Category = dt.Rows[0]["reg_Profession"].ToString();//getCategorySHC(regno, showID);// "";

                    #region paid registration
                    string regGroupID = dt.Rows[0]["RegGroupID"].ToString();
                    bool isPaid = oHelp.isDelegatePaid(showID, regno, regGroupID);

                    if (!isPaid)
                        altertNoti = " Please proceed to onsite registration counter to complete your payment ";
                    else
                    {
                        string invoiceid = dt.Rows[0]["InvoiceID"].ToString();
                        string groupid = dt.Rows[0]["RegGroupID"].ToString();
                        string flowid = dt.Rows[0]["reg_urlFlowID"].ToString();
                        altertNoti = getCongressSelection(regno, groupid, flowid, showID, invoiceid);
                    }
                    #endregion

                    #region visitor registration
                    //string regStatus = dt.Rows[0]["status_name"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["status_name"].ToString()) ? dt.Rows[0]["status_name"].ToString() : "0") : "0";
                    //if (regStatus != "Success")
                    //{
                    //    altertNoti = " Sorry, your registration is not complete yet.";
                    //}
                    #endregion
                }
                else
                {
                    regno = "";
                }
            }
            if (!string.IsNullOrEmpty(regno))
            {
                jBQCode jData = new jBQCode();
                jData.ShowName = showID;
                jData.BarCode = regno;
                jData.Regno = regno;
                BarcodeMaker bMaker = new BarcodeMaker();
                string burl = bMaker.MakeBarcode(jData);
                if (!string.IsNullOrEmpty(burl))
                {
                    pObj.ShowID = showID;
                    pObj.Regno = regno;
                    pObj.FullName = FullName.ToUpper();
                    pObj.FirstName = FirstName.ToUpper();
                    pObj.LastName = LastName.ToUpper();
                    pObj.Jobtitle = Jobtitle.ToUpper();
                    pObj.Company = Company.ToUpper();
                    pObj.Country = Country.ToUpper();
                    pObj.EmailAddress = EmailAddress;
                    pObj.MobileNo = MobileNo;
                    pObj.Category = Category.ToUpper();
                    pObj.BCodeURL = burl;
                }
            }
        }
        catch { }


        return pObj;
    }
    private string getCongressSelection(string regno, string groupid, string flowid, string showid, string invoiceID)
    {
        string rtnTicketCount = "";
        try
        {
            string fullUrl = "GRP=" + cFun.EncryptValue(groupid) + "&INV=" + cFun.EncryptValue(regno) + "&SHW=" + cFun.EncryptValue(showid)
                + "&FLW=" + cFun.EncryptValue(flowid);
            FlowURLQuery urlQuery = new FlowURLQuery(fullUrl);
            List<OrderItemList> oSucList = getPaidOrder(urlQuery, invoiceID);//***Paid Order
            if (oSucList.Count > 0)
            {
                foreach (OrderItemList ordList in oSucList)
                {
                    rtnTicketCount = getCongressSelectionString(ordList);
                    if (!string.IsNullOrEmpty(rtnTicketCount))
                    {
                        break;
                    }
                }
            }
        }
        catch(Exception ex)
        { }

        return rtnTicketCount;
    }
    private List<OrderItemList> getPaidOrder(FlowURLQuery urlQuery, string invoiceID)
    {
        List<OrderItemList> oPaidOrderItemList = new List<OrderItemList>();
        try
        {
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cFun.DecryptValue(urlQuery.DelegateID);
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj != null && invListObj.Count > 0)
            {
                foreach (Invoice invObj in invListObj)
                {
                    if (invObj.InvoiceID == invoiceID)
                    {
                        OrderControler oControl = new OrderControler(fn);
                        oPaidOrderItemList = oControl.GetAllOrderedListByInvoiceID(urlQuery, invObj.InvoiceID);
                    }
                }
            }
        }
        catch (Exception ex)
        { oPaidOrderItemList = new List<OrderItemList>(); }

        return oPaidOrderItemList;
    }
    private string getCongressSelectionString(OrderItemList OList)
    {
        string selectedCongress = string.Empty;
        try
        {
            foreach (OrderItem oItem in OList.OrderList)
            {
                if (string.IsNullOrEmpty(HttpUtility.HtmlDecode(oItem.ReportDisplayName)))
                {
                    selectedCongress += ("Ticket Count:" + oItem.Qty) + " \\n " + HttpUtility.HtmlDecode(oItem.ItemDescription) + (!string.IsNullOrEmpty(oItem.ItemDescription_ShowedInTemplate) ? "[" + HttpUtility.HtmlDecode(oItem.ItemDescription_ShowedInTemplate) + "]" : "") + "^";
                }
                else
                {
                    selectedCongress += ("Ticket Count:" + oItem.Qty) + " \\n " + HttpUtility.HtmlDecode(oItem.ReportDisplayName) + "^";
                }
            }
            selectedCongress = selectedCongress.TrimEnd('^');
        }
        catch (Exception ex)
        { }

        return selectedCongress;
    }
    #endregion
}