﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using Corpit.RegOnsite;
using Newtonsoft.Json;
using System.Data;
using Corpit.Registration;
using Corpit.Utilities;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using System.Text;
using Corpit.BackendMaster;
using Telerik.Web.UI;

public partial class OnSite_OnsiteSearchNormalFlow : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    QuesFunctionality qfn = new QuesFunctionality();
    CommonFuns cFun = new CommonFuns();

    protected int count = 0;
    protected string htmltb;
    static StringBuilder StrBuilder = new StringBuilder();
    string showID = string.Empty;
    string flowID = string.Empty;
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string other_value = "Others";
    static string prostudent = "Student";
    static string proalliedhealth = "Allied Health";
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null)
            {
                if (Session["userid"] != null)
                {
                    txtSearch.Focus();
                    txtSearch.Attributes.Add("autocomplete", "off");

                    string userID = Session["userid"].ToString();
                    if (Session["roleid"].ToString() != "1")
                    {
                        lblUser.Text = userID;
                    }

                    CommonFuns cFun = new CommonFuns();
                    string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                    if (string.IsNullOrWhiteSpace(showid))
                        Response.Redirect("~/404.aspx");
                    else
                        txtShowID.Text = showid;
                    bindFlowList(showid);
                }
                else
                {
                    Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
                }
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }
    private void bindFlowList(string showid)
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID='" + showid + "' and Status='Active' And isOnsiteDataEntry=1 Order By FLW_ID Desc"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_flowList.Items.Clear();
                ddl_flowList.DataSource = cmd.ExecuteReader();
                ddl_flowList.DataTextField = "FLW_Desc";
                ddl_flowList.DataValueField = "FLW_ID";
                ddl_flowList.DataBind();
                con.Close();
            }
        }
    }
    protected void btnEntry_Onclick(object sender, EventArgs e)
    {
        if (Request.QueryString["Evt"] != null)
        {
            try
            {
                if (Session["roleid"] != null && Session["userid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                    {
                        getShowID(Session["userid"].ToString());
                    }
                    else
                        showID = txtShowID.Text.Trim();

                    flowID = ddl_flowList.SelectedValue;
                }
                else
                {
                    Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
                }

                string flowName = fn.GetDataByCommand("Select FLW_Name From tb_site_flow_master where ShowID='" + showID + "' and Status='Active' And FLW_ID='" + flowID + "'", "FLW_Name");
                string url = "https://www.event-reg.biz/registration/eventreg?event=" + flowName;
                string jVal = string.Format("window.open('{0}','_newtab');", url);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", jVal, true);
            }
            catch (Exception ex)
            {
                Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
            }
        }
    }
    protected void btnSearch_Onclick(object sender, EventArgs e)
    {
        string strSerarch = txtSearch.Text;
        if (string.IsNullOrEmpty(strSerarch))
        { }
        else
        {
            string regno = GetRegNO(strSerarch);
            string showID = txtShowID.Text.Trim();
            if (!string.IsNullOrEmpty(showID))
            {
                if (!string.IsNullOrEmpty(regno))
                {
                    SearchNload(showID, txtSearch.Text, false);
                }
                else
                {
                    //Search and Load List
                    SearchNload(showID, txtSearch.Text, false);
                }

                txtSearch.Focus();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }
    #region GetRegno
    private string GetRegNO(string str)
    {
        string rtn = "";
        try
        {
            if (str.Length > 0)
            {
                string tmp = "";
                string[] arrList = str.Split('-'); // MFA2018-233333378 fromat
                if (arrList.Length == 2)
                    tmp = arrList[1];
                else
                    tmp = str;

                if (isRegno(tmp))
                    rtn = tmp;
            }
        }
        catch { }
        return rtn;
    }
    private bool isRegno(string input)
    {
        bool isRegno = false;
        try
        {
            isRegno = Regex.IsMatch(input, @"^\d+$");

        }
        catch { }
        return isRegno;
    }
    #endregion
    #region SearchNLoad
    private void SearchNload(string showID, string str, bool isRegno)
    {
        if (isRegno)
        { }
        else
        {
            string sql = "";
            sql = "Select Case When reg_oname is null Or reg_oname='' Then reg_FName + ' ' + reg_LName Else reg_OName End as FullName,* From GetRegIndivAll(@SHWID,@reg_urlFlowID)";//GetRegIndivAllByShowID(@SHWID)
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar1.Value = showID;
            pList.Add(spar1);
            SqlParameter spar2 = new SqlParameter("reg_urlFlowID", SqlDbType.NVarChar);
            spar2.Value = ddl_flowList.SelectedItem.Value;
            pList.Add(spar2);
            string sqlwhere = "";
            sqlwhere = @" Where regno like N'%{0}%' or reg_oName like N'%{0}%' or reg_FName like N'%{0}%' or reg_LName like N'%{0}%' or reg_Email like N'%{0}%' ";// or company like N'%{0}%' or Email like N'%{0}%' 
            sql = sql + sqlwhere;
            sql = string.Format(sql, str);
            DataTable dList = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
            if (dList.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dList;
                GKeyMaster.DataBind();
                PanelList.Visible = true;
            }
            else
                PanelList.Visible = false;
        }
    }
    #endregion
    #region GKeyMaster
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
    }
    protected void Page_PreRender(object o, EventArgs e)
    {
        try
        {
            if (Session["roleid"] != null && Session["userid"] != null)
            {
                if (Session["roleid"].ToString() != "1")
                {
                    getShowID(Session["userid"].ToString());
                }
                else
                    showID = txtShowID.Text.Trim();

                flowID = ddl_flowList.SelectedValue;
            }
            else
            {
                Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
        }
        if (!string.IsNullOrEmpty(showID))
        {
            #region Delegate
            DataTable dtfrm = new DataTable();
            FormManageObj frmObj = new FormManageObj(fn);
            frmObj.showID = showID;
            frmObj.flowID = flowID;
            dtfrm = frmObj.getDynFormForDelegate().Tables[0];

            #region Declaration
            int vis_Salutation = 0;
            int vis_Fname = 0;
            int vis_Lname = 0;
            int vis_OName = 0;
            int vis_PassNo = 0;
            int vis_isReg = 0;
            int vis_regSpecific = 0;
            int vis_IDNo = 0;
            int vis_Designation = 0;
            int vis_Profession = 0;
            int vis_Department = 0;
            int vis_Organization = 0;
            int vis_Institution = 0;
            int vis_Address1 = 0;
            int vis_Address2 = 0;
            int vis_Address3 = 0;
            int vis_Address4 = 0;
            int vis_City = 0;
            int vis_State = 0;
            int vis_PostalCode = 0;
            int vis_Country = 0;
            int vis_RCountry = 0;
            int vis_Tel = 0;
            int vis_Mobile = 0;
            int vis_Fax = 0;
            int vis_Email = 0;
            int vis_Affiliation = 0;
            int vis_Dietary = 0;
            int vis_Nationality = 0;
            int vis_MembershipNo = 0;

            int vis_VName = 0;
            int vis_VDOB = 0;
            int vis_VPassNo = 0;
            int vis_VPassExpiry = 0;
            int vis_VPassIssueDate = 0;
            int vis_VEmbarkation = 0;
            int vis_VArrivalDate = 0;
            int vis_VCountry = 0;

            int vis_UDF_CName = 0;
            int vis_UDF_DelegateType = 0;
            int vis_UDF_ProfCategory = 0;
            int vis_UDF_CPcode = 0;
            int vis_UDF_CLDepartment = 0;
            int vis_UDF_CAddress = 0;
            int vis_UDF_CLCompany = 0;
            int vis_UDF_CCountry = 0;

            int vis_SupName = 0;
            int vis_SupDesignation = 0;
            int vis_SupContact = 0;
            int vis_SupEmail = 0;

            int vis_Age = 0;
            int vis_Gender = 0;
            int vis_DOB = 0;
            int vis_Additional4 = 0;
            int vis_Additional5 = 0;
            #endregion

            if (dtfrm.Rows.Count > 0)
            {
                for (int x = 0; x < dtfrm.Rows.Count; x++)
                {
                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Salutation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Salutation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Salutation);

                                GKeyMaster.MasterTableView.GetColumn("reg_Salutation").HeaderText = labelname;

                                vis_Salutation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Salutation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Fname == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_FName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fname);
                                GKeyMaster.MasterTableView.GetColumn("reg_FName").HeaderText = labelname;

                                vis_Fname++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_FName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Lname == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_LName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Lname);
                                GKeyMaster.MasterTableView.GetColumn("reg_LName").HeaderText = labelname;

                                vis_Lname++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_LName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _OName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_OName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_OName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_OName);
                                GKeyMaster.MasterTableView.GetColumn("reg_OName").HeaderText = labelname;
                                vis_OName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_OName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_PassNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_PassNo").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PassNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_PassNo").HeaderText = labelname;

                                vis_PassNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_PassNo").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _isReg)//Is registered MOH/Resident?
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_isReg == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_isReg").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_isReg);
                                GKeyMaster.MasterTableView.GetColumn("reg_isReg").HeaderText = labelname;

                                vis_isReg++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_isReg").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)//MCR/SNB/PRN
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_regSpecific == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_sgregistered").Display = true;

                                vis_regSpecific++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_sgregistered").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _IDNo)//MCR/SNB/PRN No.
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_IDNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_IDno").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_IDNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_IDno").HeaderText = labelname;

                                vis_IDNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_IDno").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Designation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Designation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                                GKeyMaster.MasterTableView.GetColumn("reg_Designation").HeaderText = labelname;

                                vis_Designation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Designation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Profession == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Profession").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                                GKeyMaster.MasterTableView.GetColumn("reg_Profession").HeaderText = labelname;

                                vis_Profession++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Profession").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Department)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Department == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Department").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Department);
                                GKeyMaster.MasterTableView.GetColumn("reg_Department").HeaderText = labelname;

                                vis_Department++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Department").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Organization == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Organization").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Organization);
                                GKeyMaster.MasterTableView.GetColumn("reg_Organization").HeaderText = labelname;

                                vis_Organization++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Organization").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Institution == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Institution").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Institution);
                                GKeyMaster.MasterTableView.GetColumn("reg_Institution").HeaderText = labelname;

                                vis_Institution++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Institution").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address1 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address1").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address1);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address1").HeaderText = labelname;
                                vis_Address1++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address1").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address2 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address2").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address2);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address2").HeaderText = labelname;
                                vis_Address2++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address2").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address3 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address3").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address3);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address3").HeaderText = labelname;

                                vis_Address3++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address3").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Address4 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Address4").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address4);
                                GKeyMaster.MasterTableView.GetColumn("reg_Address4").HeaderText = labelname;

                                vis_Address4++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Address4").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _City)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_City == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_City").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_City);
                                GKeyMaster.MasterTableView.GetColumn("reg_City").HeaderText = labelname;

                                vis_City++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_City").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _State)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_State == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_State").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_State);
                                GKeyMaster.MasterTableView.GetColumn("reg_State").HeaderText = labelname;

                                vis_State++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_State").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_PostalCode == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_PostalCode").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PostalCode);
                                GKeyMaster.MasterTableView.GetColumn("reg_PostalCode").HeaderText = labelname;

                                vis_PostalCode++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_PostalCode").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Country)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Country == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Country").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Country);
                                GKeyMaster.MasterTableView.GetColumn("reg_Country").HeaderText = labelname;

                                vis_Country++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Country").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_RCountry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_RCountry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_RCountry);
                                GKeyMaster.MasterTableView.GetColumn("reg_RCountry").HeaderText = labelname;
                                vis_RCountry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_RCountry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Tel == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Tel").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Tel);
                                GKeyMaster.MasterTableView.GetColumn("reg_Tel").HeaderText = labelname;

                                vis_Tel++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Tel").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Mobile == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Mobile").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Mobile);
                                GKeyMaster.MasterTableView.GetColumn("reg_Mobile").HeaderText = labelname;

                                vis_Mobile++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Mobile").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Fax == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Fax").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fax);
                                GKeyMaster.MasterTableView.GetColumn("reg_Fax").HeaderText = labelname;
                                vis_Fax++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Fax").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Email == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Email").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Email);
                                GKeyMaster.MasterTableView.GetColumn("reg_Email").HeaderText = labelname;
                                vis_Email++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Email").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Affiliation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Affiliation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Affiliation);
                                GKeyMaster.MasterTableView.GetColumn("reg_Affiliation").HeaderText = labelname;

                                vis_Affiliation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Affiliation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Dietary == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Dietary").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Dietary);
                                GKeyMaster.MasterTableView.GetColumn("reg_Dietary").HeaderText = labelname;

                                vis_Dietary++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Dietary").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Nationality == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Nationality").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Nationality);
                                GKeyMaster.MasterTableView.GetColumn("reg_Nationality").HeaderText = labelname;

                                vis_Nationality++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Nationality").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Age)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Age == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Age").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Age);
                                GKeyMaster.MasterTableView.GetColumn("reg_Age").HeaderText = labelname;

                                vis_Age++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Age").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_DOB == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_DOB").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_DOB);
                                GKeyMaster.MasterTableView.GetColumn("reg_DOB").HeaderText = labelname;

                                vis_DOB++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_DOB").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Gender == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Gender").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Gender);
                                GKeyMaster.MasterTableView.GetColumn("reg_Gender").HeaderText = labelname;
                                vis_Gender++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Gender").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_MembershipNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_MembershipNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").HeaderText = labelname;
                                vis_MembershipNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Membershipno").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VName);
                                GKeyMaster.MasterTableView.GetColumn("reg_vName").HeaderText = labelname;
                                vis_VName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VDOB == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vDOB").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VDOB);
                                GKeyMaster.MasterTableView.GetColumn("reg_vDOB").HeaderText = labelname;
                                vis_VDOB++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vDOB").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VPassNo == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassno").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassNo);
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassno").HeaderText = labelname;
                                vis_VPassNo++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vPassno").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VPassExpiry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassexpiry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassExpiry);
                                GKeyMaster.MasterTableView.GetColumn("reg_vPassexpiry").HeaderText = labelname;
                                vis_VPassExpiry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vPassexpiry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VPassIssueDate == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vIssueDate").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassIssueDate);
                                GKeyMaster.MasterTableView.GetColumn("reg_vIssueDate").HeaderText = labelname;
                                vis_VPassIssueDate++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vIssueDate").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VEmbarkation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vEmbarkation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VEmbarkation);
                                GKeyMaster.MasterTableView.GetColumn("reg_vEmbarkation").HeaderText = labelname;
                                vis_VEmbarkation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vEmbarkation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VArrivalDate == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vArrivalDate").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VArrivalDate);
                                GKeyMaster.MasterTableView.GetColumn("reg_vArrivalDate").HeaderText = labelname;
                                vis_VArrivalDate++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vArrivalDate").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_VCountry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_vCountry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VCountry);
                                GKeyMaster.MasterTableView.GetColumn("reg_vCountry").HeaderText = labelname;
                                vis_VCountry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_vCountry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CName);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CName").HeaderText = labelname;
                                vis_UDF_CName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_DelegateType == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_DelegateType").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_DelegateType);
                                GKeyMaster.MasterTableView.GetColumn("UDF_DelegateType").HeaderText = labelname;
                                vis_UDF_DelegateType++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_DelegateType").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_ProfCategory == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_ProfCategory").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_ProfCategory);
                                GKeyMaster.MasterTableView.GetColumn("UDF_ProfCategory").HeaderText = labelname;
                                vis_UDF_ProfCategory++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_ProfCategory").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CPcode == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CPcode").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CPcode);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CPcode").HeaderText = labelname;
                                vis_UDF_CPcode++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CPcode").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CLDepartment == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLDepartment").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLDepartment);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLDepartment").HeaderText = labelname;
                                vis_UDF_CLDepartment++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CLDepartment").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CAddress == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CAddress").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CAddress);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CAddress").HeaderText = labelname;
                                vis_UDF_CAddress++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CAddress").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CLCompany == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLCompany").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLCompany);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CLCompany").HeaderText = labelname;
                                vis_UDF_CLCompany++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CLCompany").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_UDF_CCountry == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("UDF_CCountry").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CCountry);
                                GKeyMaster.MasterTableView.GetColumn("UDF_CCountry").HeaderText = labelname;
                                vis_UDF_CCountry++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("UDF_CCountry").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupName == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorName").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupName);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorName").HeaderText = labelname;
                                vis_SupName++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorName").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupDesignation == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorDesignation").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupDesignation);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorDesignation").HeaderText = labelname;
                                vis_SupDesignation++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorDesignation").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupContact == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorContact").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupContact);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorContact").HeaderText = labelname;
                                vis_SupContact++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorContact").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_SupEmail == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorEmail").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupEmail);
                                GKeyMaster.MasterTableView.GetColumn("reg_SupervisorEmail").HeaderText = labelname;
                                vis_SupEmail++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_SupervisorEmail").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Additional4 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional4").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional4);
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional4").HeaderText = labelname;

                                vis_Additional4++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Additional4").Display = false;
                        }
                    }

                    if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                    {
                        int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                        if (isshow == 1)
                        {
                            if (vis_Additional5 == 0)
                            {
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional5").Display = true;
                                string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional5);
                                GKeyMaster.MasterTableView.GetColumn("reg_Additional5").HeaderText = labelname;
                                vis_Additional5++;
                            }
                        }
                        else
                        {
                            GKeyMaster.MasterTableView.GetColumn("reg_Additional5").Display = false;
                        }
                    }
                }
            }
            #endregion
        }
        else
        {
            Response.Redirect("~/404.aspx");
        }
    }
    protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            if (Session["roleid"] != null && Session["userid"] != null)
            {
                if (Session["roleid"].ToString() != "1")
                    getShowID(Session["userid"].ToString());
                else
                    Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
                flowID = ddl_flowList.SelectedValue;
            }
            else
            {
                Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
        }
        if (!string.IsNullOrEmpty(showID))
        {
            if (e.Item is GridHeaderItem)
            {
                GridHeaderItem item = e.Item as GridHeaderItem;

                #region Delegate
                DataTable dtfrm = new DataTable();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showID;
                frmObj.flowID = flowID;
                dtfrm = frmObj.getDynFormForDelegate().Tables[0];

                if (dtfrm.Rows.Count > 0)
                {
                    for (int x = 0; x < dtfrm.Rows.Count; x++)
                    {
                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Salutation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Salutation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Salutation);
                                }
                            }
                            else
                            {
                                (item["reg_Salutation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_FName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_FName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fname);
                                }
                            }
                            else
                            {
                                (item["reg_FName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_LName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_LName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Lname);
                                }
                            }
                            else
                            {
                                (item["reg_LName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _OName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_OName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_OName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_OName);
                                }
                            }
                            else
                            {
                                (item["reg_OName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_PassNo"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_PassNo"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PassNo);
                                }
                            }
                            else
                            {
                                (item["reg_PassNo"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_isReg"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_isReg"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_isReg);
                                }
                            }
                            else
                            {
                                (item["reg_isReg"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_sgregistered"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_sgregistered"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_regSpecific);
                                }
                            }
                            else
                            {
                                (item["reg_sgregistered"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_IDno"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_IDno"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_IDNo);
                                }
                            }
                            else
                            {
                                (item["reg_IDno"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Designation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Designation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                                }
                            }
                            else
                            {
                                (item["reg_Designation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Profession"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Profession"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Profession);
                                }
                            }
                            else
                            {
                                (item["reg_Profession"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Department)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Department"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Department"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Department);
                                }
                            }
                            else
                            {
                                (item["reg_Department"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Organization"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Organization"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Organization);
                                }
                            }
                            else
                            {
                                (item["reg_Organization"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Institution"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Institution"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Institution);
                                }
                            }
                            else
                            {
                                (item["reg_Institution"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address1"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address1"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address1);
                                }
                            }
                            else
                            {
                                (item["reg_Address1"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address2"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address2"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address2);
                                }
                            }
                            else
                            {
                                (item["reg_Address2"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address3"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address3"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address3);
                                }
                            }
                            else
                            {
                                (item["reg_Address3"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Address4"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Address4"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address4);
                                }
                            }
                            else
                            {
                                (item["reg_Address4"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _City)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_City"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_City"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_City);
                                }
                            }
                            else
                            {
                                (item["reg_City"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _State)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_State"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_State"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_State);
                                }
                            }
                            else
                            {
                                (item["reg_State"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_PostalCode"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_PostalCode"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PostalCode);
                                }
                            }
                            else
                            {
                                (item["reg_PostalCode"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Country)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Country"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Country"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Country);
                                }
                            }
                            else
                            {
                                (item["reg_Country"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_RCountry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_RCountry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_RCountry);
                                }
                            }
                            else
                            {
                                (item["reg_RCountry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Tel"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Tel"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Tel);
                                }
                            }
                            else
                            {
                                (item["reg_Tel"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Mobile"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Mobile"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Mobile);
                                }
                            }
                            else
                            {
                                (item["reg_Mobile"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Fax"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Fax"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fax);
                                }
                            }
                            else
                            {
                                (item["reg_Fax"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Email"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Email"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Email);
                                }
                            }
                            else
                            {
                                (item["reg_Email"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Affiliation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Affiliation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Affiliation);
                                }
                            }
                            else
                            {
                                (item["reg_Affiliation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Dietary"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Dietary"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Dietary);
                                }
                            }
                            else
                            {
                                (item["reg_Dietary"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Nationality"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Nationality"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Nationality);
                                }
                            }
                            else
                            {
                                (item["reg_Nationality"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Age)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Age"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Age"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Age);
                                }
                            }
                            else
                            {
                                (item["reg_Age"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_DOB"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_DOB"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_DOB);
                                }
                            }
                            else
                            {
                                (item["reg_DOB"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Gender"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Gender"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Gender);
                                }
                            }
                            else
                            {
                                (item["reg_Gender"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Membershipno"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Membershipno"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_MembershipNo);
                                }
                            }
                            else
                            {
                                (item["reg_Membershipno"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VName);
                                }
                            }
                            else
                            {
                                (item["reg_vName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vDOB"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vDOB"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VDOB);
                                }
                            }
                            else
                            {
                                (item["reg_vDOB"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vPassno"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vPassno"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassNo);
                                }
                            }
                            else
                            {
                                (item["reg_vPassno"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vPassexpiry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vPassexpiry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassExpiry);
                                }
                            }
                            else
                            {
                                (item["reg_vPassexpiry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vIssueDate"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vIssueDate"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VPassIssueDate);
                                }
                            }
                            else
                            {
                                (item["reg_vIssueDate"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vEmbarkation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vEmbarkation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VEmbarkation);
                                }
                            }
                            else
                            {
                                (item["reg_vEmbarkation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vArrivalDate"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vArrivalDate"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VArrivalDate);
                                }
                            }
                            else
                            {
                                (item["reg_vArrivalDate"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_vCountry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_vCountry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_VCountry);
                                }
                            }
                            else
                            {
                                (item["reg_vCountry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CName);
                                }
                            }
                            else
                            {
                                (item["UDF_CName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_DelegateType"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_DelegateType"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_DelegateType);
                                }
                            }
                            else
                            {
                                (item["UDF_DelegateType"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_ProfCategory"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_ProfCategory"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_ProfCategory);
                                }
                            }
                            else
                            {
                                (item["UDF_ProfCategory"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CPcode"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CPcode"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CPcode);
                                }
                            }
                            else
                            {
                                (item["UDF_CPcode"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CLDepartment"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CLDepartment"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLDepartment);
                                }
                            }
                            else
                            {
                                (item["UDF_CLDepartment"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CAddress"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CAddress"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CAddress);
                                }
                            }
                            else
                            {
                                (item["UDF_CAddress"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CLCompany"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CLCompany"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CLCompany);
                                }
                            }
                            else
                            {
                                (item["UDF_CLCompany"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["UDF_CCountry"].Controls[0] as LinkButton).Text))
                                {
                                    (item["UDF_CCountry"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_UDF_CCountry);
                                }
                            }
                            else
                            {
                                (item["UDF_CCountry"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorName"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupName);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorName"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorDesignation"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorDesignation"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupDesignation);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorDesignation"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorContact"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorContact"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupContact);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorContact"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_SupervisorEmail"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_SupervisorEmail"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_SupEmail);
                                }
                            }
                            else
                            {
                                (item["reg_SupervisorEmail"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Additional4"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Additional4"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional4);
                                }
                            }
                            else
                            {
                                (item["reg_Additional4"].Controls[0] as LinkButton).Text = "";
                            }
                        }

                        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                        {
                            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

                            if (isshow == 1)
                            {
                                if (string.IsNullOrEmpty((item["reg_Additional5"].Controls[0] as LinkButton).Text))
                                {
                                    (item["reg_Additional5"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Additional5);
                                }
                            }
                            else
                            {
                                (item["reg_Additional5"].Controls[0] as LinkButton).Text = "";
                            }
                        }
                    }
                }
                #endregion
            }
        }
        else
        {
            Response.Redirect("~/404.aspx");
        }
    }
    protected void GKeyMaster_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
    }
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
    }
    protected void lnkEdit_Command(object sender, CommandEventArgs e)
    {
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ';' });
        string regno = commandArgs[0];
        string reggroupid = commandArgs[1];
        string flowid = commandArgs[2];
        string showID = txtShowID.Text.Trim();
        string page = "";
        string step = "";
        FlowControler Flw = new FlowControler(fn);
        Dictionary<string, string> nValues = Flw.GetNextRoute(flowid);
        if (nValues.Count > 0)
        {
            page = nValues["nURL"].ToString();
            step = nValues["nStep"].ToString();
            flowid = nValues["FlowID"].ToString();
        }
        string route = "https://www.event-reg.biz/registration/" + Flw.MakeFullURL(page, flowid, showID, reggroupid, step, regno);
        //Response.Redirect(route);
        string jVal = string.Format("window.open('{0}','_newtab');", route);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", jVal, true);
    }
    #endregion
    #region getShowID
    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }
    #endregion
    #region bindName
    public string bindSalutation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getSalutationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindProfession(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getProfessionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDepartment(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDepartmentNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindOrganisation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getOrganisationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindInstitution(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getInstitutionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindCountry(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    CountryObj conCtr = new CountryObj(fn);
                    name = conCtr.getCountryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindAffiliation(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getAffiliationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDietary(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDietaryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindIndustry(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getIndustryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    public string bindDOB(string dob, string showid)
    {
        string name = dob;
        //try
        //{
        //    ShowControler shwCtr = new ShowControler(fn);
        //    Show shw = shwCtr.GetShow(showID);
        //    if (!string.IsNullOrEmpty(dob))
        //    {
        //        name = DateTime.Parse(dob).ToString("yyyyMMdd");
        //    }
        //}
        //catch (Exception ex)
        //{ }

        return name;
    }
    #endregion
}