﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnSite/OnsiteMaster.master" AutoEventWireup="true" CodeFile="OnsiteDataEntry.aspx.cs" Inherits="OnsiteDataEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../Content/Green/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/Green/Site.css" rel="stylesheet" />
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/jquery-1.10.2.min.js"></script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="DivMain" class="row">
        <div id="DivHeader" style="padding-top: 30px;" class="container">
            <h2>Onsite Registraton Data Entry</h2>
        </div>
        <div id="DivSearchBox" class="container" style="padding-top: 30px;">

            <div class="col-md-4">
                <asp:Panel ID="PanelShow" runat="server" DefaultButton="btnSearch">
                    <asp:TextBox runat="server" ID="txtSearch" CssClass="form-control" AutoCompleteType="Disabled"> </asp:TextBox>
                  
                </asp:Panel>
            </div>
            <div class="col-md-2">
                <asp:Button runat="server" ID="btnSearch" CssClass="btn MainButton btn-block" Text="Search" OnClick="btnSearch_Onclick" />
            </div>
        </div>
        <div>
            <div style="padding-top: 30px;" class="container">
                <asp:Panel runat="server" ID="PanelList" Visible="false">
                    <table class="table">
                        <tr>  
                            <td>RegNO</td>
                            <td>Full Name</td>
                            <td>Job Title</td>
                            <td>Company </td> 
                            <td>Email </td>
                
                            <td></td>
                             
                        </tr>

                        <asp:Repeater runat="server" ID="RptList">
                            <ItemTemplate>
                                <tr>
                                    
                                    <td><%#Eval("Regno")%> </td>
                                    <td><%#Eval("reg_FName")%> </td>
                                    <td><%#Eval("reg_Designation")%> </td>
                                    <td><%#Eval("reg_Address1")%> </td> 
                                    <td><%#Eval("reg_Email")%> </td>
                             
                                    <td><%#GetStatus( Eval("Regno").ToString() )%> </td>
                                    <td><%#GetPrintStatus( Eval("Regno").ToString() )%> </td>
                                   
                                      <td>
                                           <a href='https://event-reg.biz/Registration/OnsiteQAIndex.aspx?EID=<%#Eval("regno")%> ' class="btn btn-danger" target="_blank">Edit</a>

                                    </td>
                                 
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </asp:Panel>
            </div>
        </div>
    </div>
    <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
</asp:Content>

