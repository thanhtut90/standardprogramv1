﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using Corpit.RegOnsite;
using Newtonsoft.Json;
using System.Data;
using Corpit.Registration;
using Corpit.Utilities;
public partial class OnsiteDataEntry : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    string showSARC = "EAD351";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null)
            {

                if (Session["userid"] != null)
                {
                    txtSearch.Focus();
                    txtSearch.Attributes.Add("autocomplete", "off");


                    CommonFuns cFun = new CommonFuns();
                    string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                    if (string.IsNullOrWhiteSpace(showid))
                        Response.Redirect("~/404.aspx");
                    else
                        txtShowID.Text = showid;

                }
                else
                {
                    Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
                }
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }

        }
    }

    protected void btnSearch_Onclick(object sender, EventArgs e)
    {
        string strSerarch = txtSearch.Text;
        if (string.IsNullOrEmpty(strSerarch))
        { }
        else {
            string regno = GetRegNO(strSerarch);
            string showID = txtShowID.Text.Trim();
            if (!string.IsNullOrEmpty(showID))
            {
                if (!string.IsNullOrEmpty(regno))
                {
                    SearchNload(showID, txtSearch.Text, false);
                }
                else
                {
                    //Search and Load List
                    SearchNload(showID, txtSearch.Text, false);
                }

                txtSearch.Focus();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }
    protected void btnEdit_Onclick(object sender, EventArgs e)
    {

    }
    #region GetRegno
    private string GetRegNO(string str)
    {
        string rtn = "";
        try
        {
            if (str.Length > 0)
            {
                string tmp = "";
                string[] arrList = str.Split('-'); // MFA2018-233333378 fromat
                if (arrList.Length == 2)
                    tmp = arrList[1];
                else
                    tmp = str;

                if (isRegno(tmp))
                    rtn = tmp;
            }
        }
        catch { }
        return rtn;
    }
    private bool isRegno(string input)
    {
        bool isRegno = false;
        try
        {
            isRegno = Regex.IsMatch(input, @"^\d+$");

        }
        catch { }
        return isRegno;
    }
    #endregion

    #region SearchNLoad
    private void SearchNload(string showID, string str, bool isRegno)
    {
        if (isRegno)
        { }
        else
        {
            string sql = "";
            sql = @" select * from tb_RegDelegate where regno in(select Regno from tb_RegDelegateOnsite where showid='{0}') ";

            sql = string.Format(sql, showID);
            string sqlwhere = "";
            sqlwhere = @"     and ( regno like N'%{0}%' or reg_Address1 like N'%{0}%' or reg_Fname like N'%{0}%' or reg_Email like N'%{0}%' ) ";
            sql = sql + sqlwhere;
            sql = string.Format(sql, str);
            DataTable dList = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dList.Rows.Count > 0)
            {
                RptList.DataSource = dList;
                RptList.DataBind();
                PanelList.Visible = true;
            }
            else
                PanelList.Visible = false;
        }
    }

    public string GetStatus(string regno)
    {
        string rtn = "";

        return rtn;
    }
    public string GetPrintStatus(string regno)
    {
        string rtn = "";
        try
        {
            string sql = string.Format("select* from tb_Onsite_PrintLog where showID = '{0}' and Pregno = '{1}'", txtShowID.Text, regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 0)
                rtn = "YES";
        }
        catch { }
        return rtn;
    }

    #endregion
     
}