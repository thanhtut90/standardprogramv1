﻿using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.RegOnsite;
using System.Configuration;
public partial class OnSite_OnsiteUpdateReceipt_Visitor : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    string FoodJapanIndivReg = "F376";
    string FoodJapanGroupReg = "F377";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null && Request.QueryString["Regno"] != null)
            {
                txtReceiptNumber.Focus();
                txtReceiptNumber.Attributes.Add("autocomplete", "off");
                txtSubmittedBy.Attributes.Add("autocomplete", "off");
                string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                string regno = cFun.DecryptValue(Request.QueryString["Regno"].ToString());
                if (string.IsNullOrWhiteSpace(showid))
                {
                    Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
                }
                else
                {
                    txtShowID.Text = showid;
                    txtRegno.Text = regno;
                    bindData();
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
            }
        }
    }
    private void bindData()
    {
        string regno = txtRegno.Text.Trim();
        string showID = txtShowID.Text.Trim();
        if (!string.IsNullOrEmpty(showID) && !string.IsNullOrEmpty(regno))
        {
            string altertNoti = "";
            string flowid = "";
            PrintDataObj pObj = GetRegobj(showID, regno, ref altertNoti, ref flowid);
            if (!string.IsNullOrEmpty(pObj.Regno))
            {
                string sql = string.Format("Select * From tb_RegVisitorOnsitePaymentReceipt Where RefRegno='{0}' And RegGroupID='{1}' And FlowID='{2}' And ShowID='{3}'"
                        , regno, pObj.GroupID, flowid, showID);
                DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    txtReceiptNumber.Text = dt.Rows[0]["ReceiptNumber"].ToString().Trim();
                    txtSubmittedBy.Text = dt.Rows[0]["SubmittedBy"].ToString().Trim();
                }
            }
        }
    }
    private PrintDataObj GetRegobj(string showID, string regno, ref string altertNoti, ref string flowid)
    {
        PrintDataObj pObj = new PrintDataObj();
        try
        {
            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            DataTable dt = oHelp.onsiteDelegateByRegno(showID, regno);
            string FullName = "";
            string FirstName = "";
            string LastName = "";
            string Jobtitle = "";
            string Company = "";
            string Country = "";
            string EmailAddress = "";
            string MobileNo = "";
            string Category = "";
            string groupid = "";
            if (dt.Rows.Count > 0) //ONsite
            {
                groupid = dt.Rows[0]["RegGroupID"].ToString();
                Jobtitle = dt.Rows[0]["reg_Address1"].ToString(); //dt.Rows[0]["reg_Designation"].ToString();
                Company = (dt.Rows[0]["reg_oName"].ToString()).Trim();// dt.Rows[0]["reg_organization"].ToString();
                Country = dt.Rows[0]["RegCountry"].ToString();
                Category = dt.Rows[0]["reg_Category"].ToString().Trim().ToUpper();
                FullName = (dt.Rows[0]["reg_FName"].ToString()).Trim();// (dt.Rows[0]["reg_oName"].ToString()).Trim();
                FirstName = (dt.Rows[0]["reg_FName"].ToString()).Trim();
                LastName = (dt.Rows[0]["reg_LName"].ToString()).Trim();
                EmailAddress = (dt.Rows[0]["reg_Email"].ToString()).Trim();
                MobileNo = (dt.Rows[0]["reg_Telcc"].ToString() + dt.Rows[0]["reg_Telac"].ToString() + dt.Rows[0]["reg_Tel"].ToString()).Trim(); //(dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString()).Trim();

                flowid = (dt.Rows[0]["reg_urlFlowID"].ToString()).Trim();
            }
            else
            { //Pre Reg
                //dt = oHelp.PreRegDelegateByRegno(showID, regno);
                string sql = "Select Case When reg_oname is null Or reg_oname='' Then reg_FName + ' ' + reg_LName Else reg_OName End as FullName,reg_otherInstitution,isNull(InstitutionName,reg_otherInstitution) InstitutionName,* From GetRegIndivAllByShowID('" + showID + "') "
                    + " Where reg_urlFlowID In ('" + FoodJapanIndivReg + "','" + FoodJapanGroupReg + "') And Regno='" + regno + "'";
                dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    //preReg
                    groupid = dt.Rows[0]["RegGroupID"].ToString();
                    string flName = dt.Rows[0]["reg_FName"].ToString();// + " " + dt.Rows[0]["reg_LName"].ToString();
                    FullName = flName;// (dt.Rows[0]["reg_oName"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_oName"].ToString()) ? dt.Rows[0]["reg_oName"].ToString() : flName) : flName).Trim();
                    FirstName = dt.Rows[0]["reg_FName"].ToString().Trim();
                    LastName = dt.Rows[0]["reg_LName"].ToString().Trim();
                    Jobtitle = dt.Rows[0]["reg_Address1"].ToString();//dt.Rows[0]["reg_Designation"].ToString();
                    Company = dt.Rows[0]["reg_oName"].ToString();//dt.Rows[0]["InstitutionName"].ToString();
                    Country = dt.Rows[0]["RegCountry"].ToString();
                    EmailAddress = dt.Rows[0]["reg_Email"].ToString();
                    MobileNo = dt.Rows[0]["reg_Telcc"].ToString() + dt.Rows[0]["reg_Telac"].ToString() + dt.Rows[0]["reg_Tel"].ToString(); //dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString();
                    Category = dt.Rows[0]["reg_Profession"].ToString();//getCategorySHC(regno, showID);// "";

                    flowid = (dt.Rows[0]["reg_urlFlowID"].ToString()).Trim();

                    #region paid registration
                    //string regGroupID = dt.Rows[0]["RegGroupID"].ToString();
                    //bool isPaid = oHelp.isDelegatePaid(showID, regno, regGroupID);

                    //if (!isPaid)
                    //    altertNoti = " Please proceed to onsite registration counter to complete your payment    ";
                    #endregion

                    #region visitor registration
                    string regStatus = dt.Rows[0]["status_name"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["status_name"].ToString()) ? dt.Rows[0]["status_name"].ToString() : "0") : "0";
                    if (regStatus != "Success")
                    {
                        altertNoti = " Sorry, your registration is not complete yet.";
                    }
                    #endregion
                }
                else
                {
                    regno = "";
                }
            }
            if (!string.IsNullOrEmpty(regno))
            {
                jBQCode jData = new jBQCode();
                jData.ShowName = showID;
                jData.BarCode = regno;
                jData.Regno = regno;
                BarcodeMaker bMaker = new BarcodeMaker();
                string burl = bMaker.MakeBarcode(jData);
                if (!string.IsNullOrEmpty(burl))
                {
                    pObj.ShowID = showID;
                    pObj.Regno = regno;
                    pObj.GroupID = groupid;
                    pObj.FullName = FullName.ToUpper();
                    pObj.FirstName = FirstName.ToUpper();
                    pObj.LastName = LastName.ToUpper();
                    pObj.Jobtitle = Jobtitle.ToUpper();
                    pObj.Company = Company.ToUpper();
                    pObj.Country = Country.ToUpper();
                    pObj.EmailAddress = EmailAddress;
                    pObj.MobileNo = MobileNo;
                    pObj.Category = Category.ToUpper();
                    pObj.BCodeURL = burl;
                }
            }
        }
        catch { }


        return pObj;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string parameter = "?Evt=" + Request.QueryString["Evt"];
            string strReceiptNumber = txtReceiptNumber.Text;
            if (string.IsNullOrEmpty(strReceiptNumber) || string.IsNullOrWhiteSpace(strReceiptNumber))
            {
                Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
            }
            else
            {
                string regno = txtRegno.Text.Trim();
                string showID = txtShowID.Text.Trim();
                string ReceiptNumber = cFun.solveSQL(txtReceiptNumber.Text.Trim());
                string SubmittedBy = cFun.solveSQL(txtSubmittedBy.Text.Trim());
                if (!string.IsNullOrEmpty(showID) && !string.IsNullOrEmpty(regno))
                {
                    string altertNoti = "";
                    string flowid = "";
                    PrintDataObj pObj = GetRegobj(showID, regno, ref altertNoti, ref flowid);
                    if (!string.IsNullOrEmpty(pObj.Regno))
                    {
                        string remark = ReceiptNumber + "/" + SubmittedBy;
                        string sql = string.Format("Select * From tb_RegVisitorOnsitePaymentReceipt Where RefRegno='{0}' And RegGroupID='{1}' And FlowID='{2}' And ShowID='{3}'"
                                , regno, pObj.GroupID, flowid, showID);
                        DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                        if(dt.Rows.Count > 0)
                        {
                            string sqlUpdate = string.Format("Update tb_RegVisitorOnsitePaymentReceipt Set ReceiptNumber='" + ReceiptNumber + "', SubmittedBy='" + SubmittedBy + "', Remarks=Remarks + ';" + remark + "'"
                                        + " Where RefRegno='{0}' And RegGroupID='{1}' And FlowID='{2}' And ShowID='{3}'"
                                        , regno, pObj.GroupID, flowid, showID);
                            int isSuccess = fn.ExecuteSQL(sqlUpdate);
                        }
                        else
                        {
                            string sqlInsert = string.Format("Insert Into tb_RegVisitorOnsitePaymentReceipt (ReceiptNumber, RefRegno, RegGroupID, FlowID, ShowID, SubmittedBy, Remarks)"
                                        + " Values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}','{6}')"
                                        ,ReceiptNumber, regno, pObj.GroupID, flowid, showID, SubmittedBy, remark);
                            int isSuccess = fn.ExecuteSQL(sqlInsert);
                        }
                        //Response.Write("<script>alert('Updated successfully!');window.location='OnsiteSearch.aspx" + parameter + "';</script>");
                        Response.Write("<script>alert('Updated successfully!');self.close();</script>");
                    }
                    else
                    {
                        Response.Write("<script>alert('Sorry, your registration is not confirmed yet. Please go to the registration counter.');self.close();</script>");
                    }
                }
                else
                {
                    //Response.Write("<script>alert('Invalid!');window.location='OnsiteSearch.aspx" + parameter + "';</script>");
                    Response.Write("<script>alert('Invalid!');self.close();</script>");
                }
            }
        }
    }
}