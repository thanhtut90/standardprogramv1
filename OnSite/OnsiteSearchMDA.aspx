﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnSite/OnsiteMaster.master" AutoEventWireup="true" CodeFile="OnsiteSearchMDA.aspx.cs" Inherits="OnSite_OnsiteSearchMDA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../Content/Green/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/Green/Site.css" rel="stylesheet" />
    <%--<script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/jquery-1.10.2.min.js"></script>--%>
    <script src="../Admin/js/plugins/jquery-1.9.1.min.js"></script>
    <script src="../Admin/Content/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }

        function closeModal() {
            $('#myModal').modal('hide');
            $('.modal-backdrop').remove();
        }
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="DivMain" class="row">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="DivHeader" style="padding-top: 30px;" class="container">
            <h2>Onsite Registraton Search</h2>
        </div>
        <div id="DivSearchBox" class="container" style="padding-top: 30px;">
            <div class="col-md-2" style="display:none;">Registration Flow List: </div>
            <div class="col-md-4" style="display:none;">
                <asp:DropDownList ID="ddl_flowList" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
            <div class="col-md-4">
                <asp:Panel ID="PanelShow" runat="server" DefaultButton="btnSearch">
                    <asp:TextBox runat="server" ID="txtSearch" CssClass="form-control" AutoCompleteType="Disabled"> </asp:TextBox>
                </asp:Panel>
            </div>
            <div class="col-md-2">
                <asp:Button runat="server" ID="btnSearch" CssClass="btn MainButton btn-block" Text="Search" OnClick="btnSearch_Onclick" /> 
            </div>
            <div class="col-md-2"> <asp:Button runat="server" ID="Button2" CssClass="btn  btn-danger" Text="Data Entry" OnClick="btnEntry_Onclick" Visible="false"/></div>
        </div>
        <div style="overflow-x: scroll;">
            <div style="padding-top: 30px;" class="container">
                <asp:Panel runat="server" ID="PanelList" Visible="false">
                    <asp:Label ID="lblUser" runat="server" Visible="false"></asp:Label>
                    <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster" AllowFilteringByColumn="true" runat="server" FilterType="HeaderContext" EnableHeaderContextMenu="true" 
                        EnableHeaderContextFilterMenu="true" AllowPaging="True" PagerStyle-AlwaysVisible="true" AllowSorting="true" GroupingEnabled="true"
                        OnNeedDataSource="GKeyMaster_NeedDataSource" OnItemCommand="GKeyMaster_ItemCommand" PageSize="1000" OnItemDataBound="grid_ItemDataBound" 
                        OnPageIndexChanged="GKeyMaster_PageIndexChanged">
                            <ClientSettings AllowKeyboardNavigation="false" EnablePostBackOnRowClick="false">
                                <Selecting AllowRowSelect="true"></Selecting>
                                <%--<Scrolling AllowScroll="True" EnableVirtualScrollPaging="True" UseStaticHeaders="True"
                                        SaveScrollPosition="True"></Scrolling>--%>
                            </ClientSettings>
                            <%--<HeaderStyle Width="90px" />--%>
                            <MasterTableView CommandItemDisplay="Top" AutoGenerateColumns="False" DataKeyNames="Regno,RegGroupID" AllowFilteringByColumn="True" ShowFooter="false">
                                <CommandItemSettings ShowExportToExcelButton="False" ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                                <Columns>
                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="RegGroupID" FilterControlAltText="Filter RegGroupID"
                                        HeaderText="RegGroupID" SortExpression="RegGroupID" UniqueName="RegGroupID" Visible="false">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                        HeaderText="Edit" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" class="btn btn-success btn-sm" CommandName="Editspp" 
                                                CommandArgument='<%#Eval("Regno") + ";" + Eval("RegGroupID") + ";" + Eval("reg_urlFlowID") %>' 
                                                OnCommand="lnkEdit_Command" CausesValidation="false">View QR Code</asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="Regno" FilterControlAltText="Filter Regno"
                                        HeaderText="Regno" SortExpression="Regno" UniqueName="Regno">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_OName" FilterControlAltText="Filter reg_OName"
                                        HeaderText="" SortExpression="reg_OName" UniqueName="reg_OName">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Address1" FilterControlAltText="Filter reg_Address1"
                                        HeaderText="" SortExpression="reg_Address1" UniqueName="reg_Address1">
                                    </telerik:GridBoundColumn>

                                    <telerik:GridBoundColumn FilterCheckListEnableLoadOnDemand="true" DataField="reg_Email" FilterControlAltText="Filter reg_Email"
                                        HeaderText="" SortExpression="reg_Email" UniqueName="reg_Email">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                </asp:Panel>
            </div>
        </div>
    </div>
    <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
</asp:Content>

