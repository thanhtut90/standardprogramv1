﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.RegOnsite;
using Newtonsoft.Json;
using System.Data;
using Corpit.Registration;
using Corpit.Utilities;
using System.Data.SqlClient;
using Corpit.Site.Utilities;

public partial class OnSite_OnsitePrint : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    string showSSBFE = "RWW384";
    string FoodJapanIndivReg = "F376";
    string FoodJapanGroupReg = "F377";
    CommonFuns cFunz = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string regno = "";
            string showID = "";
            if (Request.QueryString["Regno"] != null)
            {
                regno = Request.QueryString["Regno"].ToString();
            }
            else if (Session["RegUserNo"] != null)
            {
                regno = Session["RegUserNo"].ToString();
            }
            if (Request.QueryString["SH"] != null)
            {
                showID = Request.QueryString["SH"].ToString();
            }
            if (!string.IsNullOrEmpty(regno))
            {
                string altertNoti = "";
                string badgeCategory = "";
                PrintDataObj pObj = GetRegobj(showID, regno, ref altertNoti, ref badgeCategory);
                txtName.Text = GetTxtPrintRead(pObj.FullName);
                txtCompany.Text = GetTxtPrintRead(pObj.Company);
                txtCountry.Text = GetTxtPrintRead(pObj.Country);
                txtCategory.Text = GetTxtPrintRead(pObj.Category);
                txtRegno.Text = GetTxtPrintRead(regno);

                txtFirstName.Text = GetTxtPrintRead(pObj.FirstName);
                txtLastName.Text = GetTxtPrintRead(pObj.LastName);
                txtEmailAddress.Text = GetTxtPrintRead(pObj.EmailAddress);
                txtMobileNo.Text = GetTxtPrintRead(pObj.MobileNo);
                txtBadgeCategory.Text = GetTxtPrintRead(badgeCategory);

                if (Request.QueryString["type"] != null)
                {
                    txtRtnType.Text = Request.QueryString["type"].ToString();
                }

                txtShowID.Text = cFunz.EncryptValue(showID);
            }
        }
    }
    private string GetTxtPrintRead(string str)
    {
        string rtn = ".";
        if (string.IsNullOrEmpty(str)) rtn = ".";
        else
            //rtn= str.Trim().Replace(" ", "^");
            rtn = str.Trim().Replace(" ", "~");

        return rtn;
    }
    #region GetRegobj
    #region Old Comment
    //private PrintDataObj GetRegobj(string showID, string regno, ref string altertNoti)
    //{
    //    PrintDataObj pObj = new PrintDataObj();
    //    try
    //    {
    //        RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
    //        DataTable dt = oHelp.onsiteDelegateByRegno(showID, regno);
    //        string FullName = "";
    //        string Jobtitle = "";
    //        string Company = "";
    //        string Country = "";
    //        string Category = "";
    //        if (dt.Rows.Count > 0) //ONsite
    //        {

    //            Jobtitle = dt.Rows[0]["reg_Designation"].ToString();
    //            Company = dt.Rows[0]["reg_organization"].ToString();
    //            Country = dt.Rows[0]["RegCountry"].ToString();
    //            if (showID == showSARC)
    //            {
    //                FullName = (dt.Rows[0]["Salutation"].ToString() + " " + dt.Rows[0]["reg_oName"].ToString()).Trim();
    //                Category = "";

    //            }
    //            else
    //            {
    //                Category = dt.Rows[0]["reg_Category"].ToString().Trim().ToUpper();
    //                FullName = (dt.Rows[0]["reg_oName"].ToString()).Trim();
    //            }
    //        }
    //        else { //Pre Reg
    //            dt = oHelp.PreRegDelegateByRegno(showID, regno);
    //            if (dt.Rows.Count > 0)
    //            {
    //                //preReg
    //                if (showID == showSARC)
    //                {
    //                    FullName = (dt.Rows[0]["Salutation"].ToString() + " " + dt.Rows[0]["reg_oName"].ToString()).Trim();
    //                    Jobtitle = dt.Rows[0]["reg_Designation"].ToString();
    //                    Company = dt.Rows[0]["InstitutionName"].ToString();
    //                    Country = dt.Rows[0]["RegCountry"].ToString();
    //                    Category = "";

    //                    string regGroupID = dt.Rows[0]["RegGroupID"].ToString();
    //                    bool isPaid = oHelp.isDelegatePaid(showID, regno, regGroupID);

    //                    if (!isPaid)
    //                        altertNoti = " Please proceed to onsite registration counter to complete your payment    ";

    //                    if (dt.Rows[0]["reg_Additional5"].ToString() == "1")
    //                        altertNoti += "Delegate has requested for vegetarian meal.";

    //                }
    //                else
    //                {
    //                    FullName = (dt.Rows[0]["reg_Fname"].ToString() + " " + dt.Rows[0]["reg_Lname"].ToString()).Trim().ToUpper();
    //                    Jobtitle = dt.Rows[0]["reg_Designation"].ToString().ToUpper();
    //                    Company = dt.Rows[0]["reg_Address1"].ToString().ToUpper();
    //                    Country = dt.Rows[0]["RegCountry"].ToString().ToUpper();
    //                    Category = "VISITOR".ToUpper();
    //                }



    //            }
    //            else regno = "";

    //        }
    //        if (!string.IsNullOrEmpty(regno))
    //        {
    //            jBQCode jData = new jBQCode();
    //            jData.ShowName = showID;
    //            jData.BarCode = regno;
    //            jData.Regno = regno;
    //            BarcodeMaker bMaker = new BarcodeMaker();
    //            string burl = bMaker.MakeBarcode(jData);
    //            if (!string.IsNullOrEmpty(burl))
    //            {
    //                pObj.ShowID = showID;
    //                pObj.Regno = regno;
    //                pObj.FullName = FullName.ToUpper();
    //                pObj.Jobtitle = Jobtitle.ToUpper();
    //                pObj.Company = Company.ToUpper();
    //                pObj.Country = Country.ToUpper();
    //                pObj.Category = Category.ToUpper();
    //                pObj.BCodeURL = burl;
    //            }
    //        }
    //    }
    //    catch { }


    //    return pObj;
    //}
    #endregion
    #region for paid registration
    //private PrintDataObj GetRegobj(string showID, string regno, ref string altertNoti)
    //{
    //    PrintDataObj pObj = new PrintDataObj();
    //    try
    //    {
    //        RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
    //        DataTable dt = oHelp.onsiteDelegateByRegno(showID, regno);
    //        string FullName = "";
    //        string FirstName = "";
    //        string LastName = "";
    //        string Jobtitle = "";
    //        string Company = "";
    //        string Country = "";
    //        string EmailAddress = "";
    //        string MobileNo = "";
    //        string Category = "";
    //        if (dt.Rows.Count > 0) //ONsite
    //        {
    //            Jobtitle = dt.Rows[0]["reg_Designation"].ToString();
    //            Company = dt.Rows[0]["reg_Address1"].ToString();//reg_organization
    //            Country = dt.Rows[0]["RegCountry"].ToString();
    //            Category = dt.Rows[0]["reg_Category"].ToString().Trim().ToUpper();
    //            FullName = (dt.Rows[0]["reg_oName"].ToString()).Trim();
    //            FirstName = (dt.Rows[0]["reg_FName"].ToString()).Trim();
    //            LastName = (dt.Rows[0]["reg_LName"].ToString()).Trim();
    //            EmailAddress = (dt.Rows[0]["reg_Email"].ToString()).Trim();
    //            MobileNo = (dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString()).Trim();
    //        }
    //        else
    //        { //Pre Reg
    //            dt = oHelp.PreRegDelegateByRegno(showID, regno);
    //            if (dt.Rows.Count > 0)
    //            {
    //                //preReg
    //                string flName = dt.Rows[0]["reg_FName"].ToString() + " " + dt.Rows[0]["reg_LName"].ToString();
    //                FullName = dt.Rows[0]["reg_OName"].ToString().Trim(); //(dt.Rows[0]["reg_oName"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_oName"].ToString()) ? dt.Rows[0]["reg_oName"].ToString() : flName) : flName).Trim();
    //                FirstName = "";// dt.Rows[0]["reg_FName"].ToString().Trim();//reg_FName
    //                LastName = "";// dt.Rows[0]["reg_LName"].ToString().Trim();
    //                Jobtitle = dt.Rows[0]["reg_Address1"].ToString();//dt.Rows[0]["reg_Designation"].ToString();
    //                Company = dt.Rows[0]["InstitutionName"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["InstitutionName"].ToString()) ? dt.Rows[0]["InstitutionName"].ToString() : dt.Rows[0]["reg_otherInstitution"].ToString()) : dt.Rows[0]["reg_otherInstitution"].ToString();//dt.Rows[0]["reg_Address1"].ToString();
    //                Country = dt.Rows[0]["RegCountry"].ToString();
    //                EmailAddress = getCETSessions(showID, regno);//getMasterclassOrWorkshop(showID, regno, "Masterclass");// dt.Rows[0]["reg_Email"].ToString();
    //                MobileNo = "";// getMasterclassOrWorkshop(showID, regno, "Workshop");//dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString();
    //                Category = getCategorySHC(regno, showID);// "";

    //                string regGroupID = dt.Rows[0]["RegGroupID"].ToString();
    //                bool isPaid = oHelp.isDelegatePaid(showID, regno, regGroupID);

    //                if (!isPaid)
    //                {
    //                    //////if (Category == _CHAIRMAN || Category == _CREW || Category == _EXHIBITOR || Category == _FACULTY || Category == _ORGANISER || Category == _SPEAKER)
    //                    ////{
    //                    ////    isPaid = true;
    //                    ////}
    //                    ////else
    //                    {
    //                        altertNoti = " Please proceed to onsite registration counter to complete your payment    ";
    //                    }
    //                    //string flowid = dt.Rows[0]["reg_urlFlowID"].ToString();
    //                    //FlowControler fControl = new FlowControler(fn);
    //                    //FlowMaster flwMasterConfig = fControl.GetFlowMasterConfig(flowid);
    //                    ////if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
    //                    //{
    //                    //    DataTable dtRegInvoice = fn.GetDatasetByCommand("select* from tb_Invoice where InvOwnerID in ('" + regno + "','" + regGroupID + "')", "ds").Tables[0];
    //                    //    if (dtRegInvoice.Rows.Count > 0)
    //                    //    {
    //                    //        string paymentMethod = dtRegInvoice.Rows[0]["PaymentMethod"] != DBNull.Value ? (!string.IsNullOrEmpty(dtRegInvoice.Rows[0]["PaymentMethod"].ToString()) ? dtRegInvoice.Rows[0]["PaymentMethod"].ToString() : "0") : "0";
    //                    //        if ((paymentMethod == "5" || paymentMethod == "6"))
    //                    //        //if (Category == _CHAIRMAN || Category == _CREW || Category == _EXHIBITOR || Category == _FACULTY || Category == _ORGANISER || Category == _SPEAKER)
    //                    //        {
    //                    //            isPaid = true;
    //                    //        }
    //                    //        else
    //                    //        {
    //                    //            altertNoti = " Please proceed to onsite registration counter to complete your payment    ";
    //                    //        }
    //                    //    }
    //                    //    else
    //                    //    {
    //                    //        altertNoti = " Please proceed to onsite registration counter to complete your payment    ";
    //                    //    }
    //                    //}
    //                }
    //            }
    //            else
    //            {
    //                regno = "";
    //            }
    //        }
    //        if (!string.IsNullOrEmpty(regno))
    //        {
    //            jBQCode jData = new jBQCode();
    //            jData.ShowName = showID;
    //            jData.BarCode = regno;
    //            jData.Regno = regno;
    //            BarcodeMaker bMaker = new BarcodeMaker();
    //            string burl = bMaker.MakeBarcode(jData);
    //            if (!string.IsNullOrEmpty(burl))
    //            {
    //                pObj.ShowID = showID;
    //                pObj.Regno = regno;
    //                pObj.FullName = FullName.ToUpper();
    //                pObj.FirstName = FirstName.ToUpper();
    //                pObj.LastName = LastName.ToUpper();
    //                pObj.Jobtitle = Jobtitle.ToUpper();
    //                pObj.Company = Company.ToUpper();
    //                pObj.Country = Country.ToUpper();
    //                pObj.EmailAddress = EmailAddress;
    //                pObj.MobileNo = MobileNo;
    //                pObj.Category = Category.ToUpper();
    //                pObj.BCodeURL = burl;
    //            }
    //        }
    //    }
    //    catch { }


    //    return pObj;
    //}
    #region SHC
    private string _CHAIRMAN = "CHAIRMAN";
    private string _CREW = "CREW";
    private string _EXHIBITOR = "EXHIBITOR";
    private string _FACULTY = "FACULTY";
    private string _ORGANISER = "ORGANISER";
    private string _SPEAKER = "SPEAKER";
    private string getCategorySHC(string regno, string showid)
    {
        string result = "";
        try
        {
            string sql = "Select * From tmp_SHCSendReminderEmailDB Where Regno='" + regno + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                result = dt.Rows[0]["Category"] != DBNull.Value ? dt.Rows[0]["Category"].ToString() : "";

                if(result == "PRECEPTORSHIP")
                {
                    result = "DELEGATE-P";
                }
            }
            else
            {
                //result = "DELEGATE";
                ////string query = "Select f.aff_name From tb_RegDelegate as r Left Join ref_Affiliation as f On r.reg_Affiliation=f.affid Where r.ShowID=@SHWID And Regno='" + regno + "'";
                string query = "Select c.reg_CategoryName From tb_RegDelegate as r Left Join ref_reg_Category as c On r.con_CategoryId=c.reg_CategoryId Where r.ShowID=@SHWID And Regno='" + regno + "'";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);
                result = fn.GetDataByCommand(query, "reg_CategoryName", pList);
                if (result == _CHAIRMAN || result == _CREW || result == _EXHIBITOR || result == _FACULTY || result == _ORGANISER || result == _SPEAKER)//(category == "Yes")
                {
                    //category = "PRECEPTORSHIP";
                }
                else
                {
                    result = "DELEGATE";
                }
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    #endregion
    #endregion
    #region no payment
    private PrintDataObj GetRegobj(string showID, string regno, ref string altertNoti, ref string badgeCategory)
    {
        PrintDataObj pObj = new PrintDataObj();
        try
        {
            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            DataTable dt = oHelp.onsiteDelegateByRegno(showID, regno);
            string FullName = "";
            string FirstName = "";
            string LastName = "";
            string Jobtitle = "";
            string Company = "";
            string Country = "";
            string EmailAddress = "";
            string MobileNo = "";
            string Category = "";
            if (dt.Rows.Count > 0) //ONsite
            {
                Jobtitle = dt.Rows[0]["reg_Designation"].ToString();
                Company = dt.Rows[0]["reg_oName"].ToString();
                Country = dt.Rows[0]["RegCountry"].ToString();
                Category = dt.Rows[0]["reg_Category"].ToString().Trim().ToUpper();
                FullName = (dt.Rows[0]["reg_FName"].ToString()).Trim();
                FirstName = (dt.Rows[0]["reg_FName"].ToString()).Trim();
                LastName = (dt.Rows[0]["reg_LName"].ToString()).Trim();
                EmailAddress = (dt.Rows[0]["reg_Email"].ToString()).Trim();
                MobileNo = (dt.Rows[0]["reg_Telcc"].ToString() + dt.Rows[0]["reg_Telac"].ToString() + dt.Rows[0]["reg_Tel"].ToString()).Trim(); //(dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString()).Trim();

                #region SSBFE
                //if (showID == showSSBFE)
                //{
                //    string flowid = dt.Rows[0]["reg_urlFlowID"].ToString();
                //    FlowControler fControl = new FlowControler(fn);
                //    FlowMaster flwMasterConfig = fControl.GetFlowMasterConfig(flowid);
                //    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                //    {
                //        Company = dt.Rows[0]["DepartmentName"].ToString();/*Company*/
                //        MobileNo = dt.Rows[0]["InstitutionName"].ToString();/*Industry*/
                //        EmailAddress = "N";/*Promo Code*/
                //    }
                //    else
                //    {
                //        Company = dt.Rows[0]["reg_Designation"].ToString();/*Company*/
                //        MobileNo = dt.Rows[0]["OrganizationName"].ToString();/*Industry*/
                //        EmailAddress = dt.Rows[0]["Promo_Code"].ToString();/*Promo Code*/
                //        EmailAddress = !string.IsNullOrEmpty(EmailAddress) ? "Y" : "N";
                //    }
                //}
                #endregion

                badgeCategory = getBadgeCategory((dt.Rows[0]["reg_Address4"].ToString()).Trim());
            }
            else
            { //Pre Reg
                //dt = oHelp.PreRegDelegateByRegno(showID, regno);
                string sql = "Select Case When reg_oname is null Or reg_oname='' Then reg_FName Else reg_OName End as FullName,* From GetRegIndivAllByShowID('" + showID + "') "
                     + " Where reg_urlFlowID In ('" + FoodJapanIndivReg + "','" + FoodJapanGroupReg + "') And Regno='" + regno + "'";
                dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    //preReg
                    string flName = dt.Rows[0]["reg_FName"].ToString();// + " " + dt.Rows[0]["reg_LName"].ToString();
                    FullName = flName;// (dt.Rows[0]["reg_oName"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_oName"].ToString()) ? dt.Rows[0]["reg_oName"].ToString() : flName) : flName).Trim();
                    FirstName = dt.Rows[0]["reg_FName"].ToString().Trim();
                    LastName = dt.Rows[0]["reg_LName"].ToString().Trim();
                    Jobtitle = dt.Rows[0]["reg_Address1"].ToString();//dt.Rows[0]["reg_Designation"].ToString();
                    Company = dt.Rows[0]["reg_oName"].ToString();//dt.Rows[0]["InstitutionName"].ToString();
                    Country = dt.Rows[0]["RegCountry"].ToString();
                    EmailAddress = dt.Rows[0]["reg_Email"].ToString();
                    MobileNo = dt.Rows[0]["reg_Telcc"].ToString() + dt.Rows[0]["reg_Telac"].ToString() + dt.Rows[0]["reg_Tel"].ToString(); //dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString();
                    Category = dt.Rows[0]["reg_Profession"].ToString();/*Business Category (Food Japan)*/

                    #region SSBFE
                    //if (showID == showSSBFE)
                    //{
                    //    string flowid = dt.Rows[0]["reg_urlFlowID"].ToString();
                    //    FlowControler fControl = new FlowControler(fn);
                    //    FlowMaster flwMasterConfig = fControl.GetFlowMasterConfig(flowid);
                    //    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                    //    {
                    //        Company = dt.Rows[0]["DepartmentName"].ToString();/*Company*/
                    //        MobileNo = dt.Rows[0]["InstitutionName"].ToString();/*Industry*/
                    //        EmailAddress = "N";/*Promo Code*/
                    //        Category = "Exhibitor";
                    //    }
                    //    else
                    //    {
                    //        Company = dt.Rows[0]["reg_Designation"].ToString();/*Company*/
                    //        MobileNo = dt.Rows[0]["OrganizationName"].ToString();/*Industry*/
                    //        EmailAddress = getPromoCode(dt.Rows[0]["RegGroupID"].ToString(), showID);/*Promo Code*/
                    //        EmailAddress = !string.IsNullOrEmpty(EmailAddress) ? "Y" : "N";
                    //        Category = dt.Rows[0]["AffiliationName"].ToString();
                    //    }
                    //}
                    #endregion

                    badgeCategory = getBadgeCategory((dt.Rows[0]["reg_Address4"].ToString()).Trim());
                }
                else
                {
                    regno = "";
                }
            }

            if(string.IsNullOrEmpty(Category) || Category == "0")
            {
                Category = "00";
            }

            if (!string.IsNullOrEmpty(regno))
            {
                jBQCode jData = new jBQCode();
                jData.ShowName = showID;
                jData.BarCode = regno;
                jData.Regno = regno;
                BarcodeMaker bMaker = new BarcodeMaker();
                string burl = bMaker.MakeBarcode(jData);
                if (!string.IsNullOrEmpty(burl))
                {
                    pObj.ShowID = showID;
                    pObj.Regno = regno;
                    pObj.FullName = FullName.ToUpper();
                    pObj.FirstName = FirstName.ToUpper();
                    pObj.LastName = LastName.ToUpper();
                    pObj.Jobtitle = Jobtitle.ToUpper();
                    pObj.Company = Company.ToUpper();
                    pObj.Country = Country.ToUpper();
                    pObj.EmailAddress = EmailAddress;
                    pObj.MobileNo = MobileNo.ToUpper();
                    pObj.Category = Category.ToUpper();
                    pObj.BCodeURL = burl;
                }
            }
        }
        catch { }


        return pObj;
    }
    private string getPromoCode(string regroupid, string showid)
    {
        string promocode = "";
        try
        {
            string sql = "Select Promo_Code From tb_User_Promo Where ShowID='" + showid + "' And Regno='" + regroupid + "'";
            promocode = fn.GetDataByCommand(sql, "Promo_Code");
            if(promocode == "0")
            {
                promocode = "";
            }
        }
        catch(Exception ex)
        { }
        return promocode;
    }
    #endregion
    #endregion

    #region get master class and workshop
    public string getMasterclassOrWorkshop(string ShowID, string Regno, string type)
    {
        string itemName = "";
        try
        {
            string sql = @"select * from tb_OrderDetials od,tb_ConfItem c where  Regid='{1}' and c.con_itemId=od.ItemId   and ShowID='{0}'  And c.isDeleted=0
and orderNO in (select OrderNo from tb_Invoice i, tb_Order o where i.InvoiceId = o.InvoiceID and i.Invoice_status = 1) and con_ItemName like '%" + type + "%'"; 
            sql = string.Format(sql, ShowID, Regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables[0];
            if (dt.Rows.Count > 0)
            {
                itemName = dt.Rows[0]["con_ItemName"] != DBNull.Value ? dt.Rows[0]["con_ItemName"].ToString() : "";
            }
            else
            {
                string sqlDependent = @"select * from tb_OrderDetials od,tb_ConfDependentItem d where  Regid='{1}' and d.con_DependentID=od.ItemId   and ShowID='{0}'  And d.isDeleted=0
and orderNO in (select OrderNo from tb_Invoice i, tb_Order o where i.InvoiceId = o.InvoiceID and i.Invoice_status = 1) and con_ItemName like '%" + type + "%'";
                sqlDependent = string.Format(sqlDependent, ShowID, Regno);
                DataTable dtDependent = fn.GetDatasetByCommand(sqlDependent, "DT").Tables[0];
                if (dtDependent.Rows.Count > 0)
                {
                    itemName = dtDependent.Rows[0]["con_ItemName"] != DBNull.Value ? dtDependent.Rows[0]["con_ItemName"].ToString() : "";
                }
            }
        }
        catch { }

        return itemName;
    }
    #endregion

    #region get sessions (CET)
    private static string _BreakfastWithMaster = "Breakfast with Master";
    private static string _SessionA = "Session A";
    private static string _SessionB = "Session B";
    private static string _FiresideChat = "Fireside Chat";
    public string getCETSessions(string ShowID, string Regno)
    {
        string itemName = "1";
        try
        {
            /*BreakfastWithMaster*/
            string sqlBWM = "select * from tb_OrderDetials od,tb_ConfItem c where  Regid='" + Regno + "' and c.con_itemId=od.ItemId   and ShowID='" + ShowID + "'  And c.isDeleted=0 "
                            + " And orderNO in (select OrderNo from tb_Invoice i, tb_Order o where i.InvoiceId = o.InvoiceID and i.Invoice_status = 1) "
                            + " And con_GroupId In (Select tb_Conf_Group.con_GroupId From tb_Conf_Group Where ShowID='" + ShowID + "' "
                            + " And con_GroupName Like '%" + _BreakfastWithMaster + "%')";
            sqlBWM = string.Format(sqlBWM, ShowID, Regno);
            DataTable dtBWM = fn.GetDatasetByCommand(sqlBWM, "DT").Tables[0];
            if (dtBWM.Rows.Count > 0)
            {
                itemName = (dtBWM.Rows[0]["con_itemId"] != DBNull.Value ? dtBWM.Rows[0]["con_itemId"].ToString() : "1") + "(";
            }
            else
            {
                itemName = "1" + "(";
            }

            /*SessionA*/
            string sqlSessionA = "select * from tb_OrderDetials od,tb_ConfItem c where  Regid='" + Regno + "' and c.con_itemId=od.ItemId   and ShowID='" + ShowID + "'  And c.isDeleted=0 "
                            + " And orderNO in (select OrderNo from tb_Invoice i, tb_Order o where i.InvoiceId = o.InvoiceID and i.Invoice_status = 1) "
                            + " And con_GroupId In (Select tb_Conf_Group.con_GroupId From tb_Conf_Group Where ShowID='" + ShowID + "' "
                            + " And con_GroupName Like '%" + _SessionA + "%')";
            sqlSessionA = string.Format(sqlSessionA, ShowID, Regno);
            DataTable dtSessionA = fn.GetDatasetByCommand(sqlSessionA, "DT").Tables[0];
            if (dtSessionA.Rows.Count > 0)
            {
                itemName += (dtSessionA.Rows[0]["con_itemId"] != DBNull.Value ? dtSessionA.Rows[0]["con_itemId"].ToString() : "1") + "(";
            }
            else
            {
                itemName += "1" + "(";
            }

            /*SessionB*/
            string sqlSessionB = "select * from tb_OrderDetials od,tb_ConfItem c where  Regid='" + Regno + "' and c.con_itemId=od.ItemId   and ShowID='" + ShowID + "'  And c.isDeleted=0 "
                            + " And orderNO in (select OrderNo from tb_Invoice i, tb_Order o where i.InvoiceId = o.InvoiceID and i.Invoice_status = 1) "
                            + " And con_GroupId In (Select tb_Conf_Group.con_GroupId From tb_Conf_Group Where ShowID='" + ShowID + "' "
                            + " And con_GroupName Like '%" + _SessionB + "%')";
            sqlSessionB = string.Format(sqlSessionB, ShowID, Regno);
            DataTable dtSessionB = fn.GetDatasetByCommand(sqlSessionB, "DT").Tables[0];
            if (dtSessionB.Rows.Count > 0)
            {
                itemName += (dtSessionB.Rows[0]["con_itemId"] != DBNull.Value ? dtSessionB.Rows[0]["con_itemId"].ToString() : "1") + "(";
            }
            else
            {
                itemName += "1" + "(";
            }

            /*FiresideChat*/
            string sqlFiresideChat = "select * from tb_OrderDetials od,tb_ConfItem c where  Regid='" + Regno + "' and c.con_itemId=od.ItemId   and ShowID='" + ShowID + "'  And c.isDeleted=0 "
                            + " And orderNO in (select OrderNo from tb_Invoice i, tb_Order o where i.InvoiceId = o.InvoiceID and i.Invoice_status = 1) "
                            + " And con_GroupId In (Select tb_Conf_Group.con_GroupId From tb_Conf_Group Where ShowID='" + ShowID + "' "
                            + " And con_GroupName Like '%" + _FiresideChat + "%')";
            sqlFiresideChat = string.Format(sqlFiresideChat, ShowID, Regno);
            DataTable dtFiresideChat = fn.GetDatasetByCommand(sqlFiresideChat, "DT").Tables[0];
            if (dtFiresideChat.Rows.Count > 0)
            {
                itemName += (dtFiresideChat.Rows[0]["con_itemId"] != DBNull.Value ? dtFiresideChat.Rows[0]["con_itemId"].ToString() : "1");
            }
            else
            {
                itemName += "1";
            }
        }
        catch { }

        return itemName;
    }
    #endregion

    #region getBadgeCategory
    private string getBadgeCategory(string category)
    {
        string rtn = category;
        try
        {
            if(string.IsNullOrEmpty(category) || category == "0")
            {
                rtn = "VISITOR";
            }
        }
        catch(Exception ex)
        { }
        return rtn;
    }
    #endregion
}