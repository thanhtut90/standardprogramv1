﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using Corpit.RegOnsite;
using Newtonsoft.Json;
using System.Data;
using Corpit.Registration;
using Corpit.Utilities;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using System.Text;
using Corpit.BackendMaster;
using Telerik.Web.UI;

public partial class OnSite_OnsiteSearchECC : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    QuesFunctionality qfn = new QuesFunctionality();
    CommonFuns cFun = new CommonFuns();

    protected int count = 0;
    protected string htmltb;
    static StringBuilder StrBuilder = new StringBuilder();
    string showID = string.Empty;
    string flowID = string.Empty;
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string other_value = "Others";
    static string prostudent = "Student";
    static string proalliedhealth = "Allied Health";
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null)
            {
                if (Session["userid"] != null)
                {
                    txtSearch.Focus();
                    txtSearch.Attributes.Add("autocomplete", "off");

                    string userID = Session["userid"].ToString();
                    if (Session["roleid"].ToString() != "1")
                    {
                        lblUser.Text = userID;
                    }

                    CommonFuns cFun = new CommonFuns();
                    string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                    if (string.IsNullOrWhiteSpace(showid))
                        Response.Redirect("~/404.aspx");
                    else
                        txtShowID.Text = showid;
                    bindFlowList(showid);
                }
                else
                {
                    Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
                }
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }
    private void bindFlowList(string showid)
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select FLW_ID,FLW_Desc From tb_site_flow_master where ShowID='" + showid + "' and Status='Active' And isOnsiteDataEntry=1 Order By FLW_ID Desc"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddl_flowList.Items.Clear();
                ddl_flowList.DataSource = cmd.ExecuteReader();
                ddl_flowList.DataTextField = "FLW_Desc";
                ddl_flowList.DataValueField = "FLW_ID";
                ddl_flowList.DataBind();
                con.Close();
            }
        }
    }
    protected void btnEntry_Onclick(object sender, EventArgs e)
    {
        if (Request.QueryString["Evt"] != null)
        {
            try
            {
                if (Session["roleid"] != null && Session["userid"] != null)
                {
                    if (Session["roleid"].ToString() != "1")
                    {
                        getShowID(Session["userid"].ToString());
                    }
                    else
                        showID = txtShowID.Text.Trim();

                    flowID = ddl_flowList.SelectedValue;
                }
                else
                {
                    Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
                }

                string flowName = fn.GetDataByCommand("Select FLW_Name From tb_site_flow_master where ShowID='" + showID + "' and Status='Active' And FLW_ID='" + flowID + "'", "FLW_Name");
                string url = "https://www.event-reg.biz/registration/eventreg?event=" + flowName;
                string jVal = string.Format("window.open('{0}','_newtab');", url);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", jVal, true);
            }
            catch (Exception ex)
            {
                Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
            }
        }
    }
    protected void btnSearch_Onclick(object sender, EventArgs e)
    {
        string strSerarch = txtSearch.Text;
        if (string.IsNullOrEmpty(strSerarch))
        { }
        else
        {
            string regno = GetRegNO(strSerarch);
            string showID = txtShowID.Text.Trim();
            if (!string.IsNullOrEmpty(showID))
            {
                if (!string.IsNullOrEmpty(regno))
                {
                    SearchNload(showID, txtSearch.Text, false);
                }
                else
                {
                    //Search and Load List
                    SearchNload(showID, txtSearch.Text, false);
                }

                txtSearch.Focus();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }
    #region GetRegno
    private string GetRegNO(string str)
    {
        string rtn = "";
        try
        {
            if (str.Length > 0)
            {
                string tmp = "";
                string[] arrList = str.Split('-'); // MFA2018-233333378 fromat
                if (arrList.Length == 2)
                    tmp = arrList[1];
                else
                    tmp = str;

                if (isRegno(tmp))
                    rtn = tmp;
            }
        }
        catch { }
        return rtn;
    }
    private bool isRegno(string input)
    {
        bool isRegno = false;
        try
        {
            isRegno = Regex.IsMatch(input, @"^\d+$");

        }
        catch { }
        return isRegno;
    }
    #endregion
    #region SearchNLoad
    private void SearchNload(string showID, string str, bool isRegno)
    {
        if (isRegno)
        { }
        else
        {
            string sql = "";
            sql = "Select Case When reg_oname is null Or reg_oname='' Then reg_FName + ' ' + reg_LName Else reg_OName End as FullName,* From GetRegIndivAllByShowID(@SHWID)";//GetRegIndivAll(@SHWID,@reg_urlFlowID)
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar1.Value = showID;
            pList.Add(spar1);
            //SqlParameter spar2 = new SqlParameter("reg_urlFlowID", SqlDbType.NVarChar);
            //spar2.Value = ddl_flowList.SelectedItem.Value;
            //pList.Add(spar2);
            string sqlwhere = "";
            sqlwhere = @" Where reg_status=1 And (regno like N'%{0}%' or Reg_FName like N'%{0}%' or reg_Email like N'%{0}%' or reg_Address1 like N'%{0}%')";
            sql = sql + sqlwhere;
            sql = string.Format(sql, str);
            DataTable dList = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
            if (dList.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dList;
                GKeyMaster.DataBind();
                PanelList.Visible = true;
            }
            else
                PanelList.Visible = false;
        }
    }
    #endregion
    #region GKeyMaster
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
    }
    protected void Page_PreRender(object o, EventArgs e)
    {
        try
        {
            if (Session["roleid"] != null && Session["userid"] != null)
            {
                if (Session["roleid"].ToString() != "1")
                {
                    getShowID(Session["userid"].ToString());
                }
                else
                    showID = txtShowID.Text.Trim();

                flowID = ddl_flowList.SelectedValue;
            }
            else
            {
                Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
        }
        if (!string.IsNullOrEmpty(showID))
        {
            #region Delegate
            DataTable dtfrm = new DataTable();
            FormManageObj frmObj = new FormManageObj(fn);
            frmObj.showID = showID;
            frmObj.flowID = flowID;
            dtfrm = frmObj.getDynFormForDelegate().Tables[0];

            #region Declaration
            int vis_Salutation = 0;
            int vis_Fname = 0;
            int vis_Lname = 0;
            int vis_OName = 0;
            int vis_PassNo = 0;
            int vis_isReg = 0;
            int vis_regSpecific = 0;
            int vis_IDNo = 0;
            int vis_Designation = 0;
            int vis_Profession = 0;
            int vis_Department = 0;
            int vis_Organization = 0;
            int vis_Institution = 0;
            int vis_Address1 = 0;
            int vis_Address2 = 0;
            int vis_Address3 = 0;
            int vis_Address4 = 0;
            int vis_City = 0;
            int vis_State = 0;
            int vis_PostalCode = 0;
            int vis_Country = 0;
            int vis_RCountry = 0;
            int vis_Tel = 0;
            int vis_Mobile = 0;
            int vis_Fax = 0;
            int vis_Email = 0;
            int vis_Affiliation = 0;
            int vis_Dietary = 0;
            int vis_Nationality = 0;
            int vis_MembershipNo = 0;

            int vis_VName = 0;
            int vis_VDOB = 0;
            int vis_VPassNo = 0;
            int vis_VPassExpiry = 0;
            int vis_VPassIssueDate = 0;
            int vis_VEmbarkation = 0;
            int vis_VArrivalDate = 0;
            int vis_VCountry = 0;

            int vis_UDF_CName = 0;
            int vis_UDF_DelegateType = 0;
            int vis_UDF_ProfCategory = 0;
            int vis_UDF_CPcode = 0;
            int vis_UDF_CLDepartment = 0;
            int vis_UDF_CAddress = 0;
            int vis_UDF_CLCompany = 0;
            int vis_UDF_CCountry = 0;

            int vis_SupName = 0;
            int vis_SupDesignation = 0;
            int vis_SupContact = 0;
            int vis_SupEmail = 0;

            int vis_Age = 0;
            int vis_Gender = 0;
            int vis_DOB = 0;
            int vis_Additional4 = 0;
            int vis_Additional5 = 0;
            #endregion

            //if (dtfrm.Rows.Count > 0)
            //{
            //    for (int x = 0; x < dtfrm.Rows.Count; x++)
            //    {
            //        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _OName)
            //        {
            //            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

            //            if (isshow == 1)
            //            {
            //                if (vis_OName == 0)
            //                {
            //                    GKeyMaster.MasterTableView.GetColumn("reg_OName").Display = true;
            //                    string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_OName);
            //                    GKeyMaster.MasterTableView.GetColumn("reg_OName").HeaderText = labelname;
            //                    vis_OName++;
            //                }
            //            }
            //            else
            //            {
            //                GKeyMaster.MasterTableView.GetColumn("reg_OName").Display = false;
            //            }
            //        }

            //        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
            //        {
            //            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

            //            if (isshow == 1)
            //            {
            //                if (vis_Address1 == 0)
            //                {
            //                    GKeyMaster.MasterTableView.GetColumn("reg_Address1").Display = true;
            //                    string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address1);
            //                    GKeyMaster.MasterTableView.GetColumn("reg_Address1").HeaderText = labelname;
            //                    vis_Address1++;
            //                }
            //            }
            //            else
            //            {
            //                GKeyMaster.MasterTableView.GetColumn("reg_Address1").Display = false;
            //            }
            //        }

            //        if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
            //        {
            //            int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

            //            if (isshow == 1)
            //            {
            //                if (vis_Email == 0)
            //                {
            //                    GKeyMaster.MasterTableView.GetColumn("reg_Email").Display = true;
            //                    string labelname = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Email);
            //                    GKeyMaster.MasterTableView.GetColumn("reg_Email").HeaderText = labelname;
            //                    vis_Email++;
            //                }
            //            }
            //            else
            //            {
            //                GKeyMaster.MasterTableView.GetColumn("reg_Email").Display = false;
            //            }
            //        }
            //    }
            //}
            #endregion
        }
        else
        {
            Response.Redirect("~/404.aspx");
        }
    }
    protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            if (Session["roleid"] != null && Session["userid"] != null)
            {
                if (Session["roleid"].ToString() != "1")
                    getShowID(Session["userid"].ToString());
                else
                    Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
                flowID = ddl_flowList.SelectedValue;
            }
            else
            {
                Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("OnsiteLogin.aspx?Event=" + cFun.EncryptValue(txtShowID.Text.Trim()));
        }
        if (!string.IsNullOrEmpty(showID))
        {
            //if (e.Item is GridHeaderItem)
            //{
            //    GridHeaderItem item = e.Item as GridHeaderItem;

            //    #region Delegate
            //    DataTable dtfrm = new DataTable();
            //    FormManageObj frmObj = new FormManageObj(fn);
            //    frmObj.showID = showID;
            //    frmObj.flowID = flowID;
            //    dtfrm = frmObj.getDynFormForDelegate().Tables[0];

            //    if (dtfrm.Rows.Count > 0)
            //    {
            //        for (int x = 0; x < dtfrm.Rows.Count; x++)
            //        {
            //            if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _OName)
            //            {
            //                int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

            //                if (isshow == 1)
            //                {
            //                    if (string.IsNullOrEmpty((item["reg_OName"].Controls[0] as LinkButton).Text))
            //                    {
            //                        (item["reg_OName"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_OName);
            //                    }
            //                }
            //                else
            //                {
            //                    (item["reg_OName"].Controls[0] as LinkButton).Text = "";
            //                }
            //            }

            //            if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Address1)
            //            {
            //                int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

            //                if (isshow == 1)
            //                {
            //                    if (string.IsNullOrEmpty((item["reg_Address1"].Controls[0] as LinkButton).Text))
            //                    {
            //                        (item["reg_Address1"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Address1);
            //                    }
            //                }
            //                else
            //                {
            //                    (item["reg_Address1"].Controls[0] as LinkButton).Text = "";
            //                }
            //            }

            //            if (dtfrm.Rows[x]["form_input_name"].ToString().Trim() == _Email)
            //            {
            //                int isshow = Convert.ToInt16(dtfrm.Rows[x]["form_input_isshow"]);

            //                if (isshow == 1)
            //                {
            //                    if (string.IsNullOrEmpty((item["reg_Email"].Controls[0] as LinkButton).Text))
            //                    {
            //                        (item["reg_Email"].Controls[0] as LinkButton).Text = !string.IsNullOrEmpty(dtfrm.Rows[x]["form_input_text"].ToString()) ? dtfrm.Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Email);
            //                    }
            //                }
            //                else
            //                {
            //                    (item["reg_Email"].Controls[0] as LinkButton).Text = "";
            //                }
            //            }
            //        }
            //    }
            //    #endregion
            //}
        }
        else
        {
            Response.Redirect("~/404.aspx");
        }
    }
    protected void GKeyMaster_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
    }
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
    }
    protected void lnkEdit_Command(object sender, CommandEventArgs e)
    {
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ';' });
        string regno = commandArgs[0];
        string reggroupid = commandArgs[1];
        string flowid = commandArgs[2];
        string showID = txtShowID.Text.Trim();
        FlowControler Flw = new FlowControler(fn);
        string page = "DownloadBadge";
        string step = Flw.GetLatestStep(flowid);
        string route = "https://www.event-reg.biz/registration/" + page + "?SHW=" + cFun.EncryptValue(showID) + "&DID=" + cFun.EncryptValue(regno);//Flw.MakeFullURL(page, flowid, showID, reggroupid, step, regno);
        //Response.Redirect(route);
        string jVal = string.Format("window.open('{0}','_newtab');", route);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", jVal, true);
    }
    #endregion
    #region getShowID
    protected void getShowID(string userID)
    {
        try
        {
            string query = "Select us_showid From tb_Admin_Show where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = userID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            showID = dt.Rows[0]["us_showid"].ToString();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + ex.Message + "');", true);
            return;
        }
    }
    #endregion
    #region bindName
    public string bindSalutation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getSalutationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindProfession(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getProfessionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDepartment(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDepartmentNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindOrganisation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getOrganisationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindInstitution(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getInstitutionNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindCountry(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    CountryObj conCtr = new CountryObj(fn);
                    name = conCtr.getCountryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindAffiliation(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getAffiliationNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDietary(string id)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getDietaryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindIndustry(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                string userID = lblUser.Text.Trim();
                getShowID(userID);
            }
            else
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
            //if (!string.IsNullOrEmpty(lblUser.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(id))
                {
                    SetUpController setupCtr = new SetUpController(fn);
                    name = setupCtr.getIndustryNameByID(id, showID);
                    if (string.IsNullOrEmpty(name))
                    {
                        name = id;
                    }

                    OthersSettings othersetting = new OthersSettings(fn);
                    List<string> lstOthersValue = othersetting.lstOthersValue;

                    if (lstOthersValue.Contains(name))
                    {
                        name = otherValue;
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    public string bindDOB(string dob, string showid)
    {
        string name = dob;
        //try
        //{
        //    ShowControler shwCtr = new ShowControler(fn);
        //    Show shw = shwCtr.GetShow(showID);
        //    if (!string.IsNullOrEmpty(dob))
        //    {
        //        name = DateTime.Parse(dob).ToString("yyyyMMdd");
        //    }
        //}
        //catch (Exception ex)
        //{ }

        return name;
    }
    #endregion
}