﻿using Corpit.RegOnsite;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OnSite_OnsiteAcknowledgement : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null && Request.QueryString["r"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                string regno = cFun.DecryptValue(Request.QueryString["r"].ToString());
                if (string.IsNullOrWhiteSpace(showid))
                {
                    Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
                }
                else
                {
                    txtShowID.Text = showid;
                    lblRegno.Text = regno;
                    checkStatusAndBindAcknowledgementMessage(showid, regno);
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
            }
        }
    }
    private void checkStatusAndBindAcknowledgementMessage(string showID, string regno)
    {
        bool isRegComplete = checkRegStatus(showID, regno);
        if (isRegComplete)
        {
            HTMLTemplateControler htmlControl = new HTMLTemplateControler();
            string template = htmlControl.CreateAcknowledgeLetterHTMLTemplate(showID, regno);// htmlControl.CreateInvoiceReceiptHTMLTemplate(showid, DelegateID, "", "ACKNOWLEDGE");
            //template = Server.HtmlDecode(template);
            if (!string.IsNullOrEmpty(template))
            {
                lblAcknowledgementMsg.Text = template;
            }
        }
        else
        {
            Response.Write("<script>alert('Sorry, your registration is not confirmed yet. Please go to registration counter.');window.location='OnsiteSearch.aspx?Evt='" + cFun.EncryptValue(showID) + ";</script>");
        }
    }
    private bool checkRegStatus(string showID, string strRegno)
    {
        bool isRegComplete = false;
        try
        {
            string query = "Select * From GetRegIndivAllByShowID(@SHWID) Where Regno='" + strRegno + "'";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                txtRegGroupID.Text = dt.Rows[0]["RegGroupID"].ToString();
                string regStatus = dt.Rows[0]["reg_Status"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_Status"].ToString()) ? dt.Rows[0]["reg_Status"].ToString() : "0") : "0";
                if (regStatus == "1")
                {
                    isRegComplete = true;
                }
            }
        }
        catch(Exception ex)
        { }

        return isRegComplete;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["Evt"] != null)
        {
            Response.Redirect("OnsiteSearch.aspx?Evt=" + Request.QueryString["Evt"].ToString());
        }
        else
        {
            Response.Write("<script>alert('Page is invalid!');window.location='404.aspx';</script>");
        }
    }
}