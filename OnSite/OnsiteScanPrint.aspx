﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnSite/OnsiteMaster.master" AutoEventWireup="true" CodeFile="OnsiteScanPrint.aspx.cs" Inherits="OnSite_OnsiteScanPrint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="DivMain" class="row">
        <div id="DivHeader" style="padding-top: 30px;" class="container">
            <h4>Onsite Scan Confirmation</h4>
        </div>
        <div id="DivSearchBox" class="container" style="padding-top: 30px;">
            <div class="col-md-2">Scan Data</div>
            <div class="col-md-4">
                <asp:TextBox runat="server" ID="txtScanResult" CssClass="form-control" AutoCompleteType="Disabled" Enabled="true"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Button runat="server" ID="btnScan" CssClass="btn MainButton btn-success" Text="Submit" OnClick="btnScan_Click" />
            </div>
        </div>
        <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
    </div>
</asp:Content>

