﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using Corpit.RegOnsite;
using Newtonsoft.Json;
using System.Data;
using Corpit.Registration;
using Corpit.Utilities;
using System.Data.SqlClient;

public partial class OnsiteEntrySearch : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["userid"] != null)
            {
                txtSearch.Focus();
                txtSearch.Attributes.Add("autocomplete", "off");

                string[] showList = new string[2];
                if (Session["roleid"].ToString() == "1")
                {
                    showList = getAllShowList();
                }
                else
                {
                    showList = getShowList();
                }
                if (!string.IsNullOrEmpty(showList[0]))
                {
                    if (showList[0].Contains(","))
                    {
                        Panel1.Visible = true;
                        string[] showLists = showList[0].Split(',');
                        loadShowList(showList);
                        string showid = showLists[0];
                        txtShowID.Text = showid;
                    }
                    else
                    {
                        Panel1.Visible = false;
                        string showid = showList[0];
                        txtShowID.Text = showid;
                    }
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    protected void btnSearch_Onclick(object sender, EventArgs e)
    {
        string strSerarch = txtSearch.Text;
        if (string.IsNullOrEmpty(strSerarch))
        { }
        else
        {
            string regno = GetRegNO(strSerarch);
            string showID = txtShowID.Text.Trim();
            if (!string.IsNullOrEmpty(showID))
            {
                if (!string.IsNullOrEmpty(regno))
                {
                    SearchNload(showID, txtSearch.Text, false);
                }
                else
                {
                    //Search And Load List
                    SearchNload(showID, txtSearch.Text, false);
                }

                txtSearch.Focus();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    #region GetRegno
    private string GetRegNO(string str)
    {
        string rtn = "";
        try
        {
            if (str.Length > 0)
            {
                string tmp = "";
                string[] arrList = str.Split('-'); // MFA2018-233333378 fromat
                if (arrList.Length == 2)
                {
                    tmp = arrList[1];
                }
                else
                {
                    tmp = str;
                }

                //if (isRegno(tmp))//check whether digit(number)
                //{
                //    rtn = tmp;
                //}
            }
        }
        catch { }
        return rtn;
    }
    private bool isRegno(string input)
    {
        bool isRegno = false;
        try
        {
            isRegno = Regex.IsMatch(input, @"^\d+$");
        }
        catch { }
        return isRegno;
    }
    public string getEncryptValue(string input)
    {
        string isRegno = string.Empty;
        try
        {
            isRegno = cFun.EncryptValue(input);
        }
        catch { }
        return isRegno;
    }
    #endregion

    #region SearchNLoad
    private void SearchNload(string showID, string str, bool isRegno)
    {
        if (isRegno)
        { }
        else
        {
            string sql = "";
            sql = @"Select * From tb_RegDelegateOnsite Where recycle=0 And ShowID='{0}'";

            sql = string.Format(sql, showID);
            string sqlwhere = "";
            sqlwhere = @" And (Regno Like N'%{0}%' Or reg_OName Like N'%{0}%' Or reg_FName Like N'%{0}%' Or reg_LName Like N'%{0}%' Or reg_Email Like N'%{0}%')";
            sql = sql + sqlwhere;
            sql = string.Format(sql, str);
            DataTable dList = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dList.Rows.Count > 0)
            {
                RptList.DataSource = dList;
                RptList.DataBind();
                PanelList.Visible = true;
            }
            else
            {
                PanelList.Visible = false;
            }
        }
    }
    #endregion

    #region showList
    protected string[] getAllShowList()
    {
        string query = "Select * From tb_Show Order By SHW_ID";
        string showList = string.Empty;
        string showName = string.Empty;

        DataTable dt = fn.GetDatasetByCommand(query, "dsShow").Tables[0];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (showList == string.Empty)
            {
                showList = dt.Rows[i]["SHW_ID"].ToString();
                showName = dt.Rows[i]["SHW_Name"].ToString();
            }
            else
            {
                showList += "," + dt.Rows[i]["SHW_ID"].ToString();
                showName += "," + dt.Rows[i]["SHW_Name"].ToString();
            }
        }
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        return list;
    }
    protected string[] getShowList()
    {
        string showList = string.Empty;
        string showName = string.Empty;
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        try
        {
            string query = "Select us_showid From tb_Admin_Show Where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = Session["userid"].ToString();
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                query = "Select * From tb_Show WHERE SHW_ID=@showID";
                spar = new SqlParameter("showID", SqlDbType.NVarChar);

                string[] showLists = dt.Rows[0]["us_showid"].ToString().Split(',');
                for (int i = 0; i < showLists.Length; i++)
                {
                    pList.Clear();
                    spar.Value = showLists[i];
                    pList.Add(spar);

                    DataTable dtShow = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];

                    if (showName == string.Empty)
                    {
                        showName = dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                    else
                    {
                        showName += "," + dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                }
                list[0] = dt.Rows[0]["us_showid"].ToString();
                list[1] = showName;
                return list;
            }
            return list;
        }
        catch (Exception ex)
        {
            return list;
        }
    }
    protected void loadShowList(string[] showList)
    {
        ddl_showList.Items.Clear();
        string[] showIDList = showList[0].Split(',');
        string[] showNameList = showList[1].Split(',');

        for (int i = 0; i < showIDList.Length; i++)
        {
            ListItem items = new ListItem(showNameList[i], showIDList[i]);
            ddl_showList.Items.Insert(i, items);
        }
    }
    #endregion

}