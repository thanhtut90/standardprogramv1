﻿using Corpit.Registration;
using Corpit.RegOnsite;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OnSite_OnsiteOtherDataEntry_WCMH : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null && Request.QueryString["FLW"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
                if (string.IsNullOrWhiteSpace(showid) || string.IsNullOrWhiteSpace(flowid))
                {
                    Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
                }
                else
                {
                    txtShowID.Text = showid;
                    txtFlowID.Text = flowid;
                    bindWCMHCategory(showid, flowid);
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
            }

        }
    }
    private void bindWCMHCategory(string showID, string flowid)
    {
        DataSet dtCategory = fn.GetDatasetByCommand("Select reg_CategoryId,reg_CategoryName,* From ref_reg_Category Where ShowID='" + showID + "' And RefValue='" + flowid + "' And reg_CategoryId In (811,812,813,814,815)", "ds");
        if (dtCategory.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dtCategory.Tables[0].Rows.Count; i++)
            {
                ddlCategory.Items.Add(dtCategory.Tables[0].Rows[i]["reg_CategoryName"].ToString());
                ddlCategory.Items[i + 1].Value = dtCategory.Tables[0].Rows[i]["reg_CategoryId"].ToString();
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if(Page.IsValid)
        {
            if (Request.QueryString["Evt"] != null && Request.QueryString["FLW"] != null)
            {
                string showID = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                string flowid = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
                //string showID = txtShowID.Text.Trim();
                //string flowid = txtFlowID.Text.Trim();
                if (!string.IsNullOrEmpty(showID) && !string.IsNullOrEmpty(flowid))
                {
                    string fname = txtFName.Text.Trim();
                    string lname = txtLName.Text.Trim();
                    string company = txtCompany.Text.Trim();
                    string category = ddlCategory.SelectedItem.Value;
                    RegGroupObj rgp = new RegGroupObj(fn);
                    string groupid = rgp.GenGroupNumber(showID);
                    rgp.createRegGroupIDOnly(groupid, 0, showID);

                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    string regno = rgd.GenDelegateNumber(showID);
                    rgd.groupid = groupid;
                    rgd.regno = regno;
                    rgd.con_categoryID = cFun.ParseInt(category);
                    rgd.salutation = "0";
                    rgd.fname = fname;
                    rgd.lname = lname;
                    rgd.oname = "";
                    rgd.passno = "";
                    rgd.isreg = 0;
                    rgd.regspecific = "";//MCR/SNB/PRN
                    rgd.idno = "";//MCR/SNB/PRN No.
                    rgd.staffid = "";//no use in design for this field
                    rgd.designation = "";
                    rgd.jobtitle = "";//if Profession is Allied Health
                    rgd.profession = "0";
                    rgd.department = "";
                    rgd.organization = "";
                    rgd.institution = "";
                    rgd.address1 = company;
                    rgd.address2 = "";
                    rgd.address3 = "";
                    rgd.address4 = "";
                    rgd.city = "";
                    rgd.state = "";
                    rgd.postalcode = "";
                    rgd.country = "0";
                    rgd.rcountry = "0";
                    rgd.telcc = "";
                    rgd.telac = "";
                    rgd.tel = "";
                    rgd.mobilecc = "";
                    rgd.mobileac = "";
                    rgd.mobile = "";
                    rgd.faxcc = "";
                    rgd.faxac = "";
                    rgd.fax = "";
                    rgd.email = "";
                    rgd.affiliation = "";
                    rgd.dietary = "";
                    rgd.nationality = "";
                    rgd.age = 0;
                    rgd.dob = "";
                    rgd.gender = "";
                    rgd.additional4 = "";
                    rgd.additional5 = "";
                    rgd.memberno = "";

                    rgd.vname = "";
                    rgd.vdob = "";
                    rgd.vpassno = "";
                    rgd.vpassexpiry = "";
                    rgd.vpassissuedate = "";
                    rgd.vembarkation = "";
                    rgd.varrivaldate = "";
                    rgd.vcountry = "0";

                    rgd.udfcname = "";
                    rgd.udfdeltype = "";
                    rgd.udfprofcat = "";
                    rgd.udfprofcatother = "";
                    rgd.udfcpcode = "";
                    rgd.udfcldept = "";
                    rgd.udfcaddress = "";
                    rgd.udfclcompany = "";
                    rgd.udfclcompanyother = "";
                    rgd.udfccountry = "";

                    rgd.supname = "";
                    rgd.supdesignation = "";
                    rgd.supcontact = "";
                    rgd.supemail = "";

                    rgd.othersal = "";
                    rgd.otherprof = "";
                    rgd.otherdept = "";
                    rgd.otherorg = "";
                    rgd.otherinstitution = "";

                    rgd.aemail = "";
                    rgd.isSMS = 0;

                    rgd.remark = "";
                    rgd.remark_groupupload = "";
                    rgd.approvestatus = 0;
                    string createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                    rgd.createdate = createdate;
                    rgd.recycle = 0;
                    rgd.stage = "4";
                    rgd.showID = showID;
                    int isSuccess = rgd.saveRegDelegate();

                    rgp.updateStep(groupid, flowid, "4", showID);
                    rgd.updateStep(regno, flowid, "4", showID);

                    StatusSettings stuSettings = new StatusSettings(fn);
                    rgp.updateStatus(groupid, stuSettings.Success, showID);
                    rgd.updateStatus(regno, stuSettings.Success, showID);
                    Response.Write("<script>alert('Added!');window.location='OnsiteOtherDataEntry_WCMH.aspx?Evt=" + Request.QueryString["Evt"] + "&FLW=" + Request.QueryString["FLW"] + "';</script>");
                }
                else
                {
                    Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
                }
            }
        }
    }
}