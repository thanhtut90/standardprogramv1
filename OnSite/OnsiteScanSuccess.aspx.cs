﻿using Corpit.RegOnsite;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OnSite_OnsiteScanSuccess : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null && Request.QueryString["r"] != null)
            {
                string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                string regno = cFun.DecryptValue(Request.QueryString["r"].ToString());
                if (string.IsNullOrWhiteSpace(showid))
                {
                    Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
                }
                else
                {
                    txtShowID.Text = showid;
                    lblRegno.Text = regno;
                    checkStatusAndBindCategory(showid, regno);
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
            }

        }
    }
    private void checkStatusAndBindCategory(string showID, string regno)
    {
        bool isPaidDelegate = checkData(showID, regno);
        if (isPaidDelegate)
        {
            string type = "";
            if(Request.QueryString["type"] != null)
            {
                type = Request.QueryString["type"].ToString();
            }

            string printStatus = GetPrintStatus(regno);
            if (printStatus == "YES" && type != "2")
            {
                Response.Write("<script>alert('Sorry, you already printed. Please go to the counter.');window.location='OnsiteScanPrint.aspx?Evt=" + cFun.EncryptValue(showID) + "';</script>");
            }
            else
            {
                string categoryColor = checkCategoryWCMH(showID, regno);
                if(!string.IsNullOrEmpty(categoryColor) && !string.IsNullOrWhiteSpace(categoryColor))
                {
                    divColorCategory.Visible = true;
                    imgColorCategory.ImageUrl = categoryColor;
                }
            }
        }
        else
        {
            Response.Write("<script>alert('Sorry, your registration is not confirmed yet. Please go to registration counter.');window.location='404.aspx';</script>");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string strScanResult = lblRegno.Text;
        if (string.IsNullOrEmpty(strScanResult) || string.IsNullOrWhiteSpace(strScanResult))
        {
            Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
        }
        else
        {
            string regno = strScanResult;
            string showID = txtShowID.Text.Trim();
            if (!string.IsNullOrEmpty(showID))
            {
                bool isPaidDelegate = checkData(showID, regno);
                if(isPaidDelegate)
                {
                    string type = "";
                    if (Request.QueryString["type"] != null)
                    {
                        type = Request.QueryString["type"].ToString();
                    }

                    string printStatus = GetPrintStatus(regno);
                    if (printStatus == "YES" && type != "2")
                    {
                        Response.Write("<script>alert('Sorry, you already printed. Please go to the counter.');window.location='OnsiteScanPrint.aspx?Evt=" + cFun.EncryptValue(showID) + "';</script>");
                    }
                    else
                    {
                        //if (rbItems.SelectedValue == "")
                        //{
                        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please select post conference tour.');", true);
                        //    return;
                        //}
                        //else
                        {
                            insertAdditional(regno, txtRegGroupID.Text.Trim(), showID);
                            CallPrintSunny(showID, regno);//***
                        }
                    }
                }
                else
                {
                    Response.Write("<script>alert('Sorry, your registration is not confirmed yet. Please go to registration counter.');window.location='OnsiteScanPrint.aspx?Evt=" + cFun.EncryptValue(showID) + "';</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
            }
        }
    }
    private bool checkData(string showID, string strRegno)
    {
        bool isPaid = false;
        try
        {
            string query = "Select * From GetRegIndivAllByShowID(@SHWID) Where Regno='" + strRegno + "'";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                txtRegGroupID.Text = dt.Rows[0]["RegGroupID"].ToString();
                string invoiceStatus = dt.Rows[0]["Invoice_status"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["Invoice_status"].ToString()) ? dt.Rows[0]["Invoice_status"].ToString() : "0") : "0";
                if (invoiceStatus == "1")
                {
                    isPaid = true;
                }
                else
                {
                    bool isOtherCat = checkIsOtherCategory(showID, strRegno);
                    isPaid = isOtherCat;
                }
            }
        }
        catch(Exception ex)
        { }

        return isPaid;
    }
    public string GetPrintStatus(string regno)
    {
        string rtn = "";
        try
        {
            string sql = string.Format("select* from tb_Onsite_PrintLog where showID = '{0}' and Pregno = '{1}'", txtShowID.Text, regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 0)
                rtn = "YES";
        }
        catch { }
        return rtn;
    }
    private void insertAdditional(string regno, string reggroupid, string showid)
    {
        try
        {
            #region insert additional
            string postconf = rbItems.SelectedItem.Text;
            string breakoutsession = rbBreakoutSessions.SelectedItem.Text;
            string sqlPostConf = "Insert Into tb_Onsite_Additional (ShowID,Regno,GroupId,AdditionalValue1,AdditionalValue2,AdditionalValue3,CreatedDate)"
                                + " Values (@ShowID,@Regno,@GroupId,@AdditionalValue1,@AdditionalValue2,@AdditionalValue3,GetDate())";
            using (SqlConnection con = new SqlConnection(fn.ConnString))
            {
                using (SqlCommand command = new SqlCommand(sqlPostConf))
                {
                    command.Connection = con;
                    command.Parameters.AddWithValue("@ShowID", showid);
                    command.Parameters.AddWithValue("@Regno", regno);
                    command.Parameters.AddWithValue("@GroupId", reggroupid);
                    command.Parameters.AddWithValue("@AdditionalValue1", postconf);
                    command.Parameters.AddWithValue("@AdditionalValue2", breakoutsession);
                    command.Parameters.AddWithValue("@AdditionalValue3", "");
                    con.Open();
                    int isSuccess = command.ExecuteNonQuery();
                    con.Close();
                }
            }
            #endregion
        }
        catch(Exception ex)
        { }
    }
    private void CallPrintSunny(string showID, string regno)
    {
        string altertNoti = "";
        PrintDataObj pObj = GetRegobj(showID, regno, ref altertNoti);

        if (!string.IsNullOrEmpty(pObj.Regno))
        {
            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            oHelp.AddPrintLog(showID, regno);

            string url = string.Format("OnsitePrint.aspx?SH={0}&Regno={1}&type={2}", showID, regno, Request.QueryString["type"]);
            //          string jVal = string.Format("window.open('{0}','_newtab');", url);

            //          Page.ClientScript.RegisterStartupScript(
            //this.GetType(), "OpenWindow", jVal, true);
            //          //    Response.Redirect(url);
            Response.Redirect(url);
        }
        else
        {
            altertNoti = "Invalid Regno";
            string jVal = string.Format("alert('{0}');", altertNoti);
            Page.ClientScript.RegisterStartupScript(
       this.GetType(), "OpenWindow", jVal, true);
        }
    }
    private PrintDataObj GetRegobj(string showID, string regno, ref string altertNoti)
    {
        PrintDataObj pObj = new PrintDataObj();
        try
        {
            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            DataTable dt = oHelp.onsiteDelegateByRegno(showID, regno);
            string FullName = "";
            string Jobtitle = "";
            string Company = "";
            string Country = "";
            string Category = "";
            if (dt.Rows.Count > 0) //ONsite
            {
                Jobtitle = dt.Rows[0]["reg_Designation"].ToString();
                Company = dt.Rows[0]["reg_organization"].ToString();
                Country = dt.Rows[0]["RegCountry"].ToString();
                Category = dt.Rows[0]["reg_Category"].ToString().Trim().ToUpper();
                FullName = (dt.Rows[0]["reg_oName"].ToString()).Trim();
            }
            else
            { //Pre Reg
                dt = oHelp.PreRegDelegateByRegno(showID, regno);
                if (dt.Rows.Count > 0)
                {
                    //preReg
                    string flName = dt.Rows[0]["reg_FName"].ToString() + " " + dt.Rows[0]["reg_LName"].ToString();
                    FullName = (dt.Rows[0]["reg_oName"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_oName"].ToString()) ? dt.Rows[0]["reg_oName"].ToString() : flName) : flName).Trim();
                    Jobtitle = dt.Rows[0]["reg_Address2"].ToString();//dt.Rows[0]["reg_Designation"].ToString();
                    Company = dt.Rows[0]["reg_Address1"].ToString();//dt.Rows[0]["InstitutionName"].ToString();
                    Country = dt.Rows[0]["RegCountry"].ToString();
                    Category = "";

                    string regGroupID = dt.Rows[0]["RegGroupID"].ToString();
                    bool isPaid = oHelp.isDelegatePaid(showID, regno, regGroupID);

                    if (!isPaid)
                        altertNoti = " Please proceed to onsite registration counter to complete your payment    ";
                }
                else
                {
                    regno = "";
                }
            }
            if (!string.IsNullOrEmpty(regno))
            {
                jBQCode jData = new jBQCode();
                jData.ShowName = showID;
                jData.BarCode = regno;
                jData.Regno = regno;
                BarcodeMaker bMaker = new BarcodeMaker();
                string burl = bMaker.MakeBarcode(jData);
                if (!string.IsNullOrEmpty(burl))
                {
                    pObj.ShowID = showID;
                    pObj.Regno = regno;
                    pObj.FullName = FullName.ToUpper();
                    pObj.Jobtitle = Jobtitle.ToUpper();
                    pObj.Company = Company.ToUpper();
                    pObj.Country = Country.ToUpper();
                    pObj.Category = Category.ToUpper();
                    pObj.BCodeURL = burl;
                }
            }
        }
        catch { }


        return pObj;
    }
    #region WCMH
    private static string catWCMHColorDefaultBlue = "~/images/blue.png";
    private static string catWCMHRed1 = "WCMHAA8N";
    private static string catWCMHRed2 = "WCMHT5NK";
    private static string catWCMHColorRed = "~/images/red.png";
    private static string catWCMHGreen = "FLJJJZ0CTR";
    private static string catWCMHColorGreen = "~/images/green.png";
    private static string catWCMHOrganiser = "811";
    private static string catWCMHColorOrganiser = "~/images/lightgrey.png";
    private static string catWCMHCrew = "812";
    private static string catWCMHColorCrew = "~/images/darkgrey.png";
    private static string catWCMHInvitedGuest = "813";
    private static string catWCMHColorInvitedGuest = "~/images/yellow.png";
    private static string catWCMHMedia = "814";
    private static string catWCMHColorMedia = "~/images/teal.png";
    private static string catWCMHSpeaker = "815";
    private static string catWCMHColorSpeaker = "~/images/brown.png";
    private static string WCMHShowID = "GBL362";
    private string checkCategoryWCMH(string showid, string regno)
    {
        string categoryColor = "";
        try
        {
            if (showid == WCMHShowID)
            {
                categoryColor = catWCMHColorDefaultBlue;//***
                string sqlCheckPromo = "Select * From tb_User_Promo Where ShowID='" + showid + "' And Regno In (Select RegGroupID From tb_RegDelegate Where ShowID='GBL362' And recycle=0 And Regno='" + regno + "')";
                DataTable dt = fn.GetDatasetByCommand(sqlCheckPromo, "ds").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    string promocode = dt.Rows[0]["Promo_Code"] != DBNull.Value ? dt.Rows[0]["Promo_Code"].ToString() : "";
                    if (promocode == catWCMHRed1 || promocode == catWCMHRed2)
                    {
                        categoryColor = catWCMHColorRed;
                    }
                    else if (promocode == catWCMHGreen)
                    {
                        categoryColor = catWCMHColorGreen;
                    }
                }
                else
                {
                    string category = fn.GetDataByCommand("Select con_CategoryId From tb_RegDelegate Where recycle=0 And ShowID='" + showid + "' And Regno='" + regno + "'", "con_CategoryId");
                    if(category == catWCMHOrganiser)
                    {
                        categoryColor = catWCMHColorOrganiser;
                    }
                    else if(category == catWCMHCrew)
                    {
                        categoryColor = catWCMHColorCrew;
                    }
                    else if (category == catWCMHInvitedGuest)
                    {
                        categoryColor = catWCMHColorInvitedGuest;
                    }
                    else if (category == catWCMHMedia)
                    {
                        categoryColor = catWCMHColorMedia;
                    }
                    else if (category == catWCMHSpeaker)
                    {
                        categoryColor = catWCMHColorSpeaker;
                    }
                }
            }
        }
        catch(Exception ex)
        { }

        return categoryColor;
    }
    public bool checkIsOtherCategory(string showid, string regno)
    {
        bool isOtherCat = false;
        try
        {
            if (showid == WCMHShowID)
            {
                string category = fn.GetDataByCommand("Select con_CategoryId From tb_RegDelegate Where recycle=0 And ShowID='" + showid + "' And Regno='" + regno + "'", "con_CategoryId");
                if (category == catWCMHOrganiser)
                {
                    isOtherCat = true;
                }
                else if (category == catWCMHCrew)
                {
                    isOtherCat = true;
                }
                else if (category == catWCMHInvitedGuest)
                {
                    isOtherCat = true;
                }
                else if (category == catWCMHMedia)
                {
                    isOtherCat = true;
                }
                else if (category == catWCMHSpeaker)
                {
                    isOtherCat = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isOtherCat;
    }
    #endregion
}