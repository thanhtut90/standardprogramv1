﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OnsiteScanBadgePrint.aspx.cs" Inherits="OnSite_OnsiteScanBadgePrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../Scripts/jquery-1.10.2.js"></script>
    <link href="../Content/Default/bootstrap.min.css" rel="stylesheet" />
    <title></title>
        <script>
        $(document).ready(function () {
            WshShell = new ActiveXObject("Wscript.Shell"); //Create WScript Object 

            var exePath = "C:PrintExe/cert/shccert.exe ";

            var name = $(".Name").val();
            var company = $(".Company").val(); 
       
            var country = $(".Country").val();
            var category = $(".Category").val();
            var regno = $(".Regno").val();

            var firstname = $(".FirstName").val();
            var lastname = $(".LastName").val(); 
            var emailaddress = $(".EmailAddress").val();
            var mobileno = $(".MobileNo").val();
            var rtnType = $(".RtnType").val();

            var exeParm = firstname + "~" + lastname + " " + rtnType;
            WshShell.run(exePath + exeParm);
            // 
            setTimeout("NavigateReg()", 3000);
        });
        function NavigateReg()
        {
            var rtnType = $(".RtnType").val();
            var showiddecrypt = $(".ShowID").val();
            if (rtnType == "1" || rtnType == "3") {
                self.location = "OnsiteScanBadge.aspx?Evt=" + showiddecrypt;
            }
            else if (rtnType == "2") {
                self.location = "OnsiteSearch.aspx?Evt=" + showiddecrypt;
            }
            //////if (rtnType == "2") {
            //////    //////window.close();
            //////}
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="display: none;">
            <asp:TextBox runat="server" ID="txtName" CssClass="Name"></asp:TextBox> 
            <asp:TextBox runat="server" ID="txtCompany" CssClass="Company"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtCountry" CssClass="Country"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtCategory" CssClass="Category"></asp:TextBox>              
            <asp:TextBox runat="server" ID="txtRegno" CssClass="Regno"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtRtnType" CssClass="RtnType" Text="1"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtShowID" CssClass="ShowID" Text="1"></asp:TextBox>

            <asp:TextBox runat="server" ID="txtFirstName" CssClass="FirstName"></asp:TextBox> 
            <asp:TextBox runat="server" ID="txtLastName" CssClass="LastName"></asp:TextBox> 
            <asp:TextBox runat="server" ID="txtEmailAddress" CssClass="EmailAddress"></asp:TextBox> 
            <asp:TextBox runat="server" ID="txtMobileNo" CssClass="MobileNo"></asp:TextBox> 
        </div>
    </form>
</body>
</html>
