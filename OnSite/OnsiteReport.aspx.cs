﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Utilities;
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
public partial class OnSite_OnsiteReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null)
            {

                if (Session["userid"] != null)
                {
                    GKeyMaster.ExportSettings.ExportOnlyData = true;
                    GKeyMaster.ExportSettings.IgnorePaging = true;
                    GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                    GKeyMaster.ExportSettings.FileName = "OnsiteReport" + DateTime.Now.ToString("ddMMyyyy");


                    CommonFuns cFun = new CommonFuns();
                    string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                    if (string.IsNullOrWhiteSpace(showid))
                        Response.Redirect("~/404.aspx");
                    else
                        txtShowID.Text = showid;

                }
                else
                {
                    Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
                }
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }

        }
    }
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            Functionality fn = new Functionality();
            string sql = " select * from tb_Onsite_PrintReport ";
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dt;
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void btnReport_OnClick(object sender, EventArgs e)
    {
        try
        {
            InitialCountry();
            string showID = txtShowID.Text;
            Functionality fn = new Functionality();
            string colDay = "";
            string total = "";
            string coutry = "";
            string sql = @"
             
   select DAY ( Pdate ) OnsietDay,  Pdate,isnull(reg_Country,'NA') Country,Count(*) Total from (
 select  Convert(DATE,Pdate) PDate,showID,Pregno from tb_Onsite_PrintLog where  showID='{0}'
 )P  join tb_RegDelegateOnsite ro on pRegno=regno and P.showID=ro.ShowID
 group by Pdate,reg_Country  order by PDate
                ";


            sql = string.Format(sql, showID);
            DataTable dtOnsite = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];

            string preFix = "Onsite";
            for (int i = 0; i < dtOnsite.Rows.Count; i++)
            {
                coutry = dtOnsite.Rows[i]["Country"].ToString();
                total = dtOnsite.Rows[i]["Total"].ToString();
                colDay = dtOnsite.Rows[i]["OnsietDay"].ToString();
                colDay = preFix + colDay;
                UpdateCountry(colDay, coutry, total);
            }

            sql = @"
             
   select DAY ( Pdate ) OnsietDay,  Pdate,isnull(reg_Country,'NA') Country,Count(*) Total from (
 select  Convert(DATE,Pdate) PDate,showID,Pregno from tb_Onsite_PrintLog where  showID='{0}'
 )P  join tb_RegDelegate ro on pRegno=regno and P.showID=ro.ShowID where regno not in (select regno from tb_RegDelegateOnsite)
 group by Pdate,reg_Country  order by PDate
                ";
            preFix = "Pre";
            sql = string.Format(sql, showID);
            DataTable dtPre = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            for (int i = 0; i < dtPre.Rows.Count; i++)
            {
                coutry = dtPre.Rows[i]["Country"].ToString();
                total = dtPre.Rows[i]["Total"].ToString();
                colDay = dtPre.Rows[i]["OnsietDay"].ToString();
                colDay = preFix + colDay;
                UpdateCountry(colDay, coutry, total);
            }


            GKeyMaster.Rebind();
            PanelKeyList.Visible = true;
        }
        catch { }
    }
    private void UpdateCountry(string colName, string countryID, string total)
    {
        try
        {
            Functionality fn = new Functionality();
            string sql = " update tb_Onsite_PrintReport set {0} = '{1}' where countryID = '{2}'";
            sql = string.Format(sql, colName, total, countryID);
            fn.ExecuteSQL(sql);
        }
        catch { }
    }

    private void InitialCountry()
    {
        string sql = @"  update tb_Onsite_PrintReport set  [Onsite16]=0
      ,[Pre16]=0
      ,[Onsite17]=0
      ,[Pre17]=0
      ,[Onsite18]=0
      ,[Pre18]=0
      ,[Onsite20]=0
      ,[Pre20]=0
      ,[Onsite21]=0
      ,[Pre21]=0
      ,[Onsite22]=0
      ,[Pre22]=0
      ,[Onsite23]=0
      ,[Pre23]=0
      ,[Onsite24]=0
      ,[Pre24]=0";
        Functionality fn = new Functionality();
        fn.ExecuteSQL(sql);
    }
}