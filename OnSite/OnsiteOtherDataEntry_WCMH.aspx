﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnSite/OnsiteMaster.master" AutoEventWireup="true" CodeFile="OnsiteOtherDataEntry_WCMH.aspx.cs" Inherits="OnSite_OnsiteOtherDataEntry_WCMH" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="DivMain" class="row">
        <div id="DivHeader" style="padding-top: 30px;" class="container">
            <h4>Onsite Scan Success</h4>
        </div>
        <div id="DivSearchBox" class="container" style="padding-top: 30px;">
            <div class="row">
                <div class="col-md-4">Please select post conference tour.</div>
                <div class="col-md-4">
                    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control">
                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:CompareValidator ID="vcCategory" runat="server" 
                    ControlToValidate="ddlCategory" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">Fist Name</div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                         <asp:RequiredFieldValidator ID="vcFName" runat="server"
                        ControlToValidate="txtFName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">Last Name</div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtLName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcLName" runat="server"
                        ControlToValidate="txtLName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">Company/Organization</div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtCompany" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcCompany" runat="server"
                        ControlToValidate="txtCompany" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>
            <br />
            <br />
            <div class="row">
                <div class="col-md-6">&nbsp;</div>
                <div class="col-md-2">
                    <asp:Button runat="server" ID="btnSubmit" CssClass="btn MainButton btn-success" Text="Submit" OnClick="btnSubmit_Click" />
                </div>
            </div>
        </div>
        <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
        <asp:TextBox runat="server" ID="txtFlowID" Visible="false" Text=""></asp:TextBox>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
</asp:Content>

