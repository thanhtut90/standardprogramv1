﻿using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.RegOnsite;
using System.Configuration;
public partial class OnSite_OnsiteScanBadge : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null)
            {
                txtScanResult.Focus();
                txtScanResult.Attributes.Add("autocomplete", "off");
                string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                if (string.IsNullOrWhiteSpace(showid))
                {
                    Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
                }
                else
                {
                    txtShowID.Text = showid;
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
            }
        }
    }
    protected void btnScan_Click(object sender, EventArgs e)
    {
        string strScanResult = txtScanResult.Text;
        if (string.IsNullOrEmpty(strScanResult) || string.IsNullOrWhiteSpace(strScanResult))
        {
            Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
        }
        else
        {
            string regno = strScanResult;
            string showID = txtShowID.Text.Trim();
            if (!string.IsNullOrEmpty(showID))
            {
                bool isPaidDelegate = false;
                string regName = "";
                    isPaidDelegate = checkData(showID, regno,ref regName);

                if (isPaidDelegate)
                {
                    string printStatus = "";
                        printStatus = GetPrintStatus(regno);
                    if (printStatus == "YES")
                    {
                        Response.Write("<script>alert('Sorry, you already Printed.');</script>");
                        txtScanResult.Text = "";
                        txtScanResult.Focus();
                    }
                    else
                    {
                        CallPrintSunny(showID, regno);//***
                    }
                }
                else
                {
                    Response.Write("<script>alert('Sorry, your registration is not confirmed yet. Please go to registration counter.');</script>");
                    txtScanResult.Text = "";
                    txtScanResult.Focus();
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid!');window.location='404.aspx';</script>");
            }
        }
    }
    private bool checkData(string showID, string strRegno,ref string regName)
    {
        bool isPaid = false;
        try
        {
            string query = "Select * From GetRegIndivAllByShowID(@SHWID) Where Regno='" + strRegno + "'";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showID;
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                string invoiceStatus = dt.Rows[0]["Invoice_status"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["Invoice_status"].ToString()) ? dt.Rows[0]["Invoice_status"].ToString() : "0") : "0";
                if (invoiceStatus == "1")
                {
                    isPaid = true;
                    regName = dt.Rows[0]["reg_Fname"].ToString();
                }
                else
                {
                    bool isOtherCat = checkIsOtherCategory(showID, strRegno);
                    isPaid = isOtherCat;
                }
            }

        }
        catch (Exception ex)
        { }

        return isPaid;
    }
    public bool checkIsOtherCategory(string showid, string regno)
    {
        bool isOtherCat = false;
        try
        {
            if (showid == SHCShowID)
            {
                string query = "Select c.reg_CategoryName From tb_RegDelegate as r Left Join ref_reg_Category as c On r.con_CategoryId=c.reg_CategoryId Where r.ShowID=@SHWID And Regno='" + regno + "'";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);
                string categoryName = fn.GetDataByCommand(query, "reg_CategoryName", pList);
                if (categoryName == _CHAIRMAN || categoryName == _CREW || categoryName == _EXHIBITOR || categoryName == _FACULTY || categoryName == _ORGANISER || categoryName == _SPEAKER)
                {
                    isOtherCat = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isOtherCat;
    }
    public string GetPrintStatus(string regno)
    {
        string rtn = "";
        try
        {
            string sql = string.Format("select* from tb_Onsite_BadgePrintLog where showID = '{0}' and Pregno = '{1}'", txtShowID.Text, regno);
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 0)
                rtn = "YES";
        }
        catch { }
        return rtn;
    }
    public class ECGFunctionality : BaseFunctionality
    {
        public ECGFunctionality()
        {
            if (ConfigurationManager.ConnectionStrings["ECGCMSConnString"] != null)
            {
                string constr = ConfigurationManager.ConnectionStrings["ECGCMSConnString"].ConnectionString;
                base.ConnString = constr;

            }
        }


    }

    #region CallPrintSunny
    private void CallPrintSunny(string showID, string regno)
    {
        string altertNoti = "";
        PrintDataObj pObj = GetRegobj(showID, regno, ref altertNoti);

        if (!string.IsNullOrEmpty(pObj.Regno))
        {
            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            oHelp.AddPrintLog(showID, regno);

            string printerType = pObj.Category == "DELEGATE-P" ? "3" : "1";

            string url = string.Format("OnsiteScanBadgePrint.aspx?SH={0}&Regno={1}&type={2}", showID, regno, printerType);
            ////          string jVal = string.Format("window.open('{0}','_newtab');", url);

            ////          Page.ClientScript.RegisterStartupScript(
            ////this.GetType(), "OpenWindow", jVal, true);
            ////          //    Response.Redirect(url);
            //Response.Redirect(url);
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + pObj.Category.ToUpper() + "');window.location='" + url + "';", true);
        }
        else
        {
            altertNoti = "Invalid Regno";
            string jVal = string.Format("alert('{0}');", altertNoti);
            Page.ClientScript.RegisterStartupScript(
       this.GetType(), "OpenWindow", jVal, true);
        }
    }
    private PrintDataObj GetRegobj(string showID, string regno, ref string altertNoti)
    {
        PrintDataObj pObj = new PrintDataObj();
        try
        {
            RegOnsiteHelper oHelp = new RegOnsiteHelper(fn);
            DataTable dt = oHelp.onsiteDelegateByRegno(showID, regno);
            string FullName = "";
            string FirstName = "";
            string LastName = "";
            string Jobtitle = "";
            string Company = "";
            string Country = "";
            string EmailAddress = "";
            string MobileNo = "";
            string Category = "";
            if (dt.Rows.Count > 0) //ONsite
            {
                Jobtitle = dt.Rows[0]["reg_Designation"].ToString();
                Company = dt.Rows[0]["reg_organization"].ToString();
                Country = dt.Rows[0]["RegCountry"].ToString();
                Category = dt.Rows[0]["reg_Category"].ToString().Trim().ToUpper();
                FullName = (dt.Rows[0]["reg_oName"].ToString()).Trim();
                FirstName = (dt.Rows[0]["reg_FName"].ToString()).Trim();
                LastName = (dt.Rows[0]["reg_LName"].ToString()).Trim();
                EmailAddress = (dt.Rows[0]["reg_Email"].ToString()).Trim();
                MobileNo = (dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString()).Trim();
            }
            else
            { //Pre Reg
                dt = oHelp.PreRegDelegateByRegno(showID, regno);
                if (dt.Rows.Count > 0)
                {
                    //preReg
                    string flName = dt.Rows[0]["reg_FName"].ToString() + " " + dt.Rows[0]["reg_LName"].ToString();
                    FullName = (dt.Rows[0]["reg_oName"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_oName"].ToString()) ? dt.Rows[0]["reg_oName"].ToString() : flName) : flName).Trim();
                    FirstName = dt.Rows[0]["reg_FName"].ToString().Trim();
                    LastName = dt.Rows[0]["reg_LName"].ToString().Trim();
                    Jobtitle = dt.Rows[0]["reg_Address2"].ToString();//dt.Rows[0]["reg_Designation"].ToString();
                    Company = dt.Rows[0]["reg_Address1"].ToString();//dt.Rows[0]["InstitutionName"].ToString();
                    Country = dt.Rows[0]["RegCountry"].ToString();
                    EmailAddress = dt.Rows[0]["reg_Email"].ToString();
                    MobileNo = dt.Rows[0]["reg_Mobcc"].ToString() + dt.Rows[0]["reg_Mobac"].ToString() + dt.Rows[0]["reg_Mobile"].ToString();
                    Category = getCategorySHC(regno, showID);// "";

                    string regGroupID = dt.Rows[0]["RegGroupID"].ToString();
                    bool isPaid = oHelp.isDelegatePaid(showID, regno, regGroupID);

                    if (!isPaid)
                        altertNoti = " Please proceed to onsite registration counter to complete your payment    ";
                }
                else
                {
                    regno = "";
                }
            }
            if (!string.IsNullOrEmpty(regno))
            {
                jBQCode jData = new jBQCode();
                jData.ShowName = showID;
                jData.BarCode = regno;
                jData.Regno = regno;
                BarcodeMaker bMaker = new BarcodeMaker();
                string burl = bMaker.MakeBarcode(jData);
                if (!string.IsNullOrEmpty(burl))
                {
                    pObj.ShowID = showID;
                    pObj.Regno = regno;
                    pObj.FullName = FullName.ToUpper();
                    pObj.FirstName = FirstName.ToUpper();
                    pObj.LastName = LastName.ToUpper();
                    pObj.Jobtitle = Jobtitle.ToUpper();
                    pObj.Company = Company.ToUpper();
                    pObj.Country = Country.ToUpper();
                    pObj.EmailAddress = EmailAddress;
                    pObj.MobileNo = MobileNo;
                    pObj.Category = Category.ToUpper();
                    pObj.BCodeURL = burl;
                }
            }
        }
        catch { }


        return pObj;
    }
    #region SHC
    private string _CHAIRMAN = "CHAIRMAN";
    private string _CREW = "CREW";
    private string _EXHIBITOR = "EXHIBITOR";
    private string _FACULTY = "FACULTY";
    private string _ORGANISER = "ORGANISER";
    private string _SPEAKER = "SPEAKER";
    private static string SHCShowID = "ZHB359";
    private string getCategorySHC(string regno, string showid)
    {
        string result = "";
        try
        {
            string sql = "Select * From tmp_SHCSendReminderEmailDB Where Regno='" + regno + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if(dt.Rows.Count > 0)
            {
                result = dt.Rows[0]["Category"] != DBNull.Value ? dt.Rows[0]["Category"].ToString() : "";

                if (result == "PRECEPTORSHIP")
                {
                    result = "DELEGATE-P";
                }
            }
            else
            {
                //result = "DELEGATE";
                ////string query = "Select f.aff_name From tb_RegDelegate as r Left Join ref_Affiliation as f On r.reg_Affiliation=f.affid Where r.ShowID=@SHWID And Regno='" + regno + "'";
                string query = "Select c.reg_CategoryName From tb_RegDelegate as r Left Join ref_reg_Category as c On r.con_CategoryId=c.reg_CategoryId Where r.ShowID=@SHWID And Regno='" + regno + "'";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
                spar.Value = showid;
                pList.Add(spar);
                result = fn.GetDataByCommand(query, "reg_CategoryName", pList);
                if (result == _CHAIRMAN || result == _CREW || result == _EXHIBITOR || result == _FACULTY || result == _ORGANISER || result == _SPEAKER)//(category == "Yes")
                {
                    //category = "PRECEPTORSHIP";
                }
                else
                {
                    result = "DELEGATE";
                }
            }
        }
        catch(Exception ex)
        { }

        return result;
    }
    #endregion
    #endregion
}