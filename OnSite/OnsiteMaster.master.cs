﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
using System.Data;

public partial class OnSite_OnsiteMaster : System.Web.UI.MasterPage
{
    protected string encryptshowid;
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null)
            {
                Functionality fn = new Functionality();
                string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                encryptshowid = cFun.EncryptValue(showid);
                if (!string.IsNullOrEmpty(showid))
                {
                    SiteSettings sSetting = new SiteSettings(fn, showid);
                    sSetting.LoadBaseSiteProperties(showid);
                    SiteBanner.ImageUrl ="~/"+   sSetting.SiteBanner;
                    if (showid != "GBL362")
                    {
                        SiteBanner.Visible = true;
                    }
                    else
                    {
                        SiteBanner.Visible = false;
                    }

                    //hpOnsiteSearch.NavigateUrl = "~/OnSite/OnsiteSearch.aspx?Evt=" + encryptshowid;
                    //hpOnsiteReport.NavigateUrl = "~/OnSite/OnsiteReport.aspx?Evt=" + encryptshowid;
                    //hpOnsiteReportMasterList.NavigateUrl = "~/OnSite/OnsiteReport_MasterList.aspx?Evt=" + encryptshowid;
                    //if (Session["roleid"] != null)
                    //{
                    //    string roleid = Session["roleid"].ToString();
                    //    if(roleid != "4")
                    //    {
                    //        liOnsiteReport.Visible = false;// true;
                    //        liOnsiteReportMasterList.Visible = true;
                    //    }
                    //}
                }
                else
                {
                        Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
                }
            }
            else
            {
                Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"]);
            }
        }
    }
}
