﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnSite/OnsiteMaster.master" AutoEventWireup="true" CodeFile="OnsiteReport.aspx.cs" Inherits="OnSite_OnsiteReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../Content/Default/bootstrap.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div style="padding-top:30px;padding-left:50px;padding-bottom:20px;">

    <asp:Button runat="server" ID="btnReport" OnClick="btnReport_OnClick"  Text="Generate Report" CssClass="btn btn-danger"/>
        </div>
    <asp:TextBox runat="server" ID="txtShowID" Visible="false"></asp:TextBox>
     <asp:Panel runat="server" ID="PanelKeyList" Visible="false" CssClass="container">
                <telerik:RadGrid RenderMode="Auto" ID="GKeyMaster"  runat="server"  
                    OnNeedDataSource="GKeyMaster_NeedDataSource"  PageSize="10" Skin="Bootstrap">
                    <ClientSettings AllowKeyboardNavigation="true" EnablePostBackOnRowClick="true">
                        
                    </ClientSettings>
                    <MasterTableView AutoGenerateColumns="True" DataKeyNames="CountryID"  AllowPaging="true" CommandItemDisplay="Top">
                         <CommandItemSettings ShowExportToExcelButton="True" ShowRefreshButton="False" ShowAddNewRecordButton="False" />
                        <Columns>
                             
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </asp:Panel>
             
</asp:Content>

