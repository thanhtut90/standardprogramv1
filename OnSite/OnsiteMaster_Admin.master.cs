﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
using System.Data;
using System.Data.SqlClient;

public partial class OnsiteMaster_Admin : System.Web.UI.MasterPage
{
    Functionality fn = new Functionality();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["userid"] != null)
            {
                string[] showList = new string[2];
                if (Session["roleid"].ToString() == "1")
                {
                    SiteBanner.ImageUrl = "~/images/site/CorpitBanner.jpg";
                    SiteBanner.Visible = true;
                }
                else
                {
                    showList = getShowList();
                    string showid = showList[0];
                    if (!string.IsNullOrEmpty(showid))
                    {
                        SiteSettings sSetting = new SiteSettings(fn, showid);
                        sSetting.LoadBaseSiteProperties(showid);
                        SiteBanner.ImageUrl = "~/" + sSetting.SiteBanner;
                        SiteBanner.Visible = true;
                    }
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }
    }

    #region showList
    protected string[] getShowList()
    {
        string showList = string.Empty;
        string showName = string.Empty;
        string[] list = new string[2];
        list[0] = showList;
        list[1] = showName;

        try
        {
            string query = "Select us_showid From tb_Admin_Show Where us_userid=@userid";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("userid", SqlDbType.NVarChar);
            spar.Value = Session["userid"].ToString();
            pList.Add(spar);
            DataTable dt = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];

            if (dt.Rows.Count > 0)
            {
                query = "Select * From tb_Show Where SHW_ID=@showID";
                spar = new SqlParameter("showID", SqlDbType.NVarChar);

                string[] showLists = dt.Rows[0]["us_showid"].ToString().Split(',');
                for (int i = 0; i < showLists.Length; i++)
                {
                    pList.Clear();
                    spar.Value = showLists[i];
                    pList.Add(spar);

                    DataTable dtShow = fn.GetDatasetByCommand(query, "dsShow", pList).Tables[0];

                    if (showName == string.Empty)
                    {
                        showName = dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                    else
                    {
                        showName += "," + dtShow.Rows[0]["SHW_Name"].ToString();
                    }
                }
                list[0] = dt.Rows[0]["us_showid"].ToString();
                list[1] = showName;
                return list;
            }
            return list;
        }
        catch (Exception ex)
        {
            return list;
        }
    }
    #endregion
}
