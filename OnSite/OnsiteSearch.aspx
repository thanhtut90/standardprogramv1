﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OnSite/OnsiteMaster.master" AutoEventWireup="true" CodeFile="OnsiteSearch.aspx.cs" Inherits="OnSite_OnsiteSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../Content/Green/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/Green/Site.css" rel="stylesheet" />
    <%--<script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/jquery-1.10.2.min.js"></script>--%>
    <script src="../Admin/js/plugins/jquery-1.9.1.min.js"></script>
    <script src="../Admin/Content/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }

        function closeModal() {
            $('#myModal').modal('hide');
            $('.modal-backdrop').remove();
        }
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="DivMain" class="row">
        <div id="DivHeader" class="container"><%--style="padding-top: 30px;" --%>
            <h4 style="margin-top:0px;">Onsite Registration Search</h4>
        </div>
        <div id="DivSearchBox" class="container"><%-- style="padding-top: 30px;"--%>

            <div class="col-md-4">
                <asp:Panel ID="PanelShow" runat="server" DefaultButton="btnSearch">
                    <asp:TextBox runat="server" ID="txtSearch" CssClass="form-control" AutoCompleteType="Disabled"> </asp:TextBox>
                    <%--Print Directly 
                    <asp:CheckBox runat="server" ID="ChkPrintDirect" />--%>
                </asp:Panel>
            </div>
            <div class="col-md-2">
                <asp:Button runat="server" ID="btnSearch" CssClass="btn MainButton btn-block" Text="Search" OnClick="btnSearch_Onclick" /> 
            </div>
                        <%--<div class="col-md-2"> <asp:Button runat="server" ID="Button2" CssClass="btn  btn-danger" Text="Data Entry" OnClick="btnEntry_Onclick" Visible="true"/></div>--%>
        </div>
        <div style="width:100%; height: 280px; overflow: auto;">
            <div style="padding-top: 10px;" class="container"><%--style="padding-top: 30px;" --%>
                <asp:Panel runat="server" ID="PanelList" Visible="false">
                    <table class="table">
                        <tr>  
                            <td>RegNO</td>
                            <td>Badge Category</td>
                            <td>Registration Date</td>
                            <td>Remark</td>
                            <td>Full Name</td>
                            <%--<td>First Name</td>
                            <td>Last Name</td>--%>
                            <td>Designation</td>
                            <td>Company</td>
                            <%--<td>Country</td>--%>
                            <%--<td>Email </td>--%>
                            <td>Status</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <%--<td></td>--%>
                        </tr>

                        <asp:Repeater runat="server" ID="RptList">
                            <ItemTemplate>
                                <tr>
                                    <td><%#Eval("Regno")%> </td>
                                    <td><%#string.IsNullOrEmpty(Eval("reg_Address4").ToString()) || Eval("reg_Address4").ToString() == "0" ? "VISITOR" : Eval("reg_Address4").ToString()%> </td>
                                    <td><%#Eval("reg_datecreated")%> </td>
                                    <td><%#getVisitorReceiptFJ(Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("reg_urlFlowID").ToString(), Eval("ShowID").ToString())%> </td>
                                    <%--<td><%# getCategorySHC(Eval("Regno").ToString(), Eval("ShowID").ToString())%></td>--%>
                                    <%--<td><%#getCategorySSBFE(Eval("Regno").ToString(), Eval("reg_urlFlowID").ToString(), Eval("AffiliationName").ToString())%> </td>--%>
                                    <%--<td><%#Eval("FullName")%> </td>--%>
                                    <td><%#Eval("reg_FName")%> </td>
                                    <%-- <td><%#Eval("reg_LName")%> </td>--%>
                                    <%--<td><%#Eval("reg_OName")%> </td>--%><%--JobTitle--%><%--reg_Additional4--%>
                                    <td><%#Eval("reg_Address1")%> </td><%--Company--%><%--reg_Additional5--%><%--reg_PassNo--%><%--reg_Designation--%>
                                    <%--<td><%#Eval("RegCountry")%> </td>--%>
                                    <td><%#Eval("reg_OName")%> </td><%--InstitutionName--%>
                                    <%--<td><%#Eval("reg_Email")%> </td>--%><%--Email--%>
                                    <%--<td><%#GetStatus( Eval("Regno").ToString() )%> </td>--%>
                                    <%--<td><%#GetStatus(Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("ShowID").ToString(), Eval("Invoice_status").ToString(), Eval("PaymentMethod").ToString())%> </td>--%>
                                    <td><%#!string.IsNullOrEmpty(Eval("status_name").ToString()) ? Eval("status_name").ToString() : "Pending"%></td>
                                    <td>
                                        <asp:Button runat="server" ID="Button1" CssClass="btn MainButton btn-block" Text="Print " OnClick="btnPrintSunny_Onclick" CommandArgument='<%#Eval("Regno")%> ' 
                                            Visible='<%#SetPrintVisiable(Eval("status_name").ToString(), Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("reg_urlFlowID").ToString(), Eval("ShowID").ToString())%>' />
                                            <%--Visible='<%#SetPrintVisiable(Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("ShowID").ToString(), Eval("Invoice_status").ToString(), Eval("PaymentMethod").ToString())%>' />--%>
                                        <%--OnClick="btnConfirm_Click" --%>
                                    </td>
                                    <td>
                                        <%--<a href='<%#SetEditUrl(Eval("Regno").ToString(), Eval("reg_urlFlowID").ToString(), Eval("ShowID").ToString(), Eval("ShowID").ToString(), Eval("status_name").ToString())%>' class="btn btn-danger" target="_blank" id="btnEdit" runat="server" 
                                        visible='<%#SetEditButtonVisiable(Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("ShowID").ToString(), Eval("con_CategoryId").ToString(), Eval("status_name").ToString())%>'>Edit</a>--%>
                                       <%-- <a href='<%#SetEditUrlOtherCat(Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("reg_urlFlowID").ToString(), Eval("ShowID").ToString())%>' 
                                            class="btn btn-danger" target="_blank" id="btnEditOtherCat" runat="server" 
                                            visible='<%#SetEditOtherCatButtonVisiable(Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("ShowID").ToString())%>'>Edit</a>--%>
                                        <a href='<%#SetEditUrl(Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("reg_urlFlowID").ToString(), Eval("ShowID").ToString(), Eval("reg_Address4").ToString(), Eval("status_name").ToString())%>' class="btn btn-danger" target="_blank" id="btnEdit" runat="server" 
                                        visible='<%#SetEditButtonVisiable(Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("ShowID").ToString(), Eval("con_CategoryId").ToString(), Eval("status_name").ToString())%>'>Edit</a>
                                         
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="btnUpdateReceiptNumber" CssClass="btn MainButton btn-block" Text="Update Receipt Number" OnClick="btnUpdateReceiptNumber_Click" CommandArgument='<%#Eval("Regno")%> ' 
                                            Visible='<%#SetUpdateReceiptVisible(Eval("status_name").ToString(), Eval("Regno").ToString(), Eval("RegGroupID").ToString(), Eval("reg_urlFlowID").ToString(), Eval("ShowID").ToString())%>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            
                        </asp:Repeater>
                    </table>
                </asp:Panel>
            </div>
        </div>
        <div class="form-group row">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div style="padding-top: 30px;" class="container">
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><asp:Label ID="lblModalTitle" runat="server" Text="Confirmation"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                You already printed. Do you want to re-print?
                                <br />
                                <asp:TextBox ID="txtRegno" runat="server" Enabled="false" CssClass="form-control" Visible="false"></asp:TextBox>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="btnClose" runat="server" class="btn btn-default" Text="No" OnClick="btnClose_Click" CausesValidation="false"/>
                                <asp:Button ID="btnConfirm" runat="server" class="btn btn-primary" Text="Yes" OnClick="btnConfirm_Click"/>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<!-- Bootstrap Modal Dialog -->
                <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title"><asp:Label ID="lblModalTitle" runat="server" Text="Confirmation"></asp:Label></h4>
                                    </div>
                                    <div class="modal-body">
                                        You already printed. Do you want to re-print?
                                        <br />
                                        <asp:TextBox ID="txtRegno" runat="server" Enabled="false" CssClass="form-control" Visible="false"></asp:TextBox>
                                    </div>
                                    <div class="modal-footer">
                                        <asp:Button ID="btnClose" runat="server" class="btn btn-default" Text="No" OnClick="btnClose_Click" CausesValidation="false"/>
                                        <asp:Button ID="btnConfirm" runat="server" class="btn btn-primary" Text="Yes" OnClick="btnConfirm_Click"/>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
    <asp:TextBox runat="server" ID="txtShowID" Visible="false" Text=""></asp:TextBox>
</asp:Content>

