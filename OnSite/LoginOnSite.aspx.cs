﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Logging;

public partial class LoginOnSite : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    private static string onsiteAdmin = "[Onsite Admin]";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    #region Login Click
    protected void btnSubmit_Onclick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                Functionality fn = new Functionality();
                CommonFuns cFun = new CommonFuns();
                Boolean isvalid = isValid();
                if (isvalid)
                {
                    string username = cFun.solveSQL(txtUsername.Text.ToString().Trim());
                    string password = cFun.solveSQL(txtPassword.Text.ToString().Trim());

                    LoginControler loginCtr = new LoginControler(fn);
                    LoginUser lUser = loginCtr.CheckAdminLogin(username, password);
                    //   DataTable dt = loginCtr.authenticateLogin(username, password);
                    //if (dt.Rows.Count > 0)
                    if (!string.IsNullOrEmpty(lUser.UserID))
                    {
                        string roleid = lUser.RoleID.ToString();
                        string sesUser = lUser.UserName.ToString();
                        string sesUserID = lUser.UserID.ToString();
                        Session["roleid"] = roleid;
                        Session["username"] = sesUser;
                        Session["userid"] = sesUserID;

                        LoginLog lg = new LoginLog(fn);
                        string userip = GetUserIP();
                        lg.loginid = sesUser;
                        lg.loginip = userip;
                        lg.logintype = LoginLogType.adminLogin + onsiteAdmin;
                        lg.saveLoginLog();

                        Response.Redirect("OnsiteEntrySearch.aspx");
                    }
                    else
                    {
                        lblpasswerror.Text = "Invalid username or password";
                        lblpasswerror.Visible = true;
                    }
                }
            }
            catch (Exception ex) { }
        }
    }
    protected Boolean isValid()
    {
        Boolean isvalid = true;

        if (txtPassword.Text.ToString().Trim() == "" || txtPassword.Text.ToString().Trim() == string.Empty)
        {
            lblpasswerror.Text = "Please insert your password";
            lblpasswerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblpasswerror.Visible = false;
        }
        if (txtUsername.Text.ToString().Trim() == "" || txtUsername.Text.ToString().Trim() == string.Empty)
        {
            lblpasswerror.Text = "Please insert your username";
            lblpasswerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblpasswerror.Visible = false;
        }
        return isvalid;
    }
    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
    #endregion
}