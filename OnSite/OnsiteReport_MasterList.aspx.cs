﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Utilities;
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
using System.Text.RegularExpressions;

public partial class OnsiteReport_MasterList : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Evt"] != null)
            {
                if (Session["userid"] != null)
                {
                    GKeyMaster.ExportSettings.ExportOnlyData = true;
                    GKeyMaster.ExportSettings.IgnorePaging = true;
                    GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                    GKeyMaster.ExportSettings.FileName = "OnsiteReport_MasterList" + DateTime.Now.ToString("ddMMyyyy");

                    string showid = cFun.DecryptValue(Request.QueryString["Evt"].ToString());
                    if (string.IsNullOrWhiteSpace(showid))
                        Response.Redirect("~/404.aspx");
                    else
                        txtShowID.Text = showid;

                    if (Session["roleid"] != null)
                    {
                        string roleid = Session["roleid"].ToString();
                        if (roleid == "4")
                        {
                            Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
                        }
                    }
                }
                else
                {
                    Response.Redirect("OnsiteLogin.aspx?Event=" + Request.QueryString["Evt"].ToString());
                }
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }
    protected void GKeyMaster_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            Functionality fn = new Functionality();
            string sql = "";//ShowID,
            #region Old (datetime)
            //         sql = @"  
            //select * from (
            //select Regno 'Registration Number', reg_FName 'Full Name',reg_LName Company, ProfessionName 'Business Category' , OrganizationName 'Main Job Function',RegCountry Country,case when reg_urlFlowID= 'F378' Then 'Visitor' else 'Trade Visitor' End 'Reg Category' , reg_email Email ,(Select Convert(NVARCHAR(23), min(PDate), 121) From tb_Onsite_PrintLog Where PRegno=DelegateWithRefValue.Regno And ShowID='{0}'
            //group by PRegno
            //     ) as PrintDate from DelegateWithRefValue 
            //where ShowID='{0}' and recycle=0 and reg_status=1
            //union
            //select Regno 'Registration Number',reg_FName 'Full Name',reg_LName Company,ProfessionName 'Business Category' ,OrganizationName 'Main Job Function',RegCountry Country,reg_Category 'Reg Category' ,reg_email Email
            // ,(Select Convert(NVARCHAR(23), min(PDate), 121) From tb_Onsite_PrintLog Where PRegno=OnsiteDelegateWithRefValue.Regno And ShowID='{0}'
            //group by PRegno
            //     ) as PrintDate from OnsiteDelegateWithRefValue where ShowID='{0}'
            //)A ";
            #endregion
            sql = @"  
                   select Regno 'Registration Number',reg_FName 'Full Name',reg_LName Company, ProfessionName 'Business Category', OrganizationName 'Main Job Function',RegCountry Country,reg_Category 'Reg Category' , reg_email Email ,convert(varchar, PrintDate, 103) as PrintDate,convert(varchar, PrintDate, 108) as PrintTime from (
                   select Regno , reg_FName,reg_LName, ProfessionName , OrganizationName,RegCountry,case when reg_urlFlowID= 'F378' Then 'Visitor' else 'Trade Visitor' End reg_Category , reg_email ,(Select min(PDate) From tb_Onsite_PrintLog Where PRegno=DelegateWithRefValue.Regno And ShowID='{0}'
                   group by PRegno
                        ) as PrintDate from DelegateWithRefValue 
                   where ShowID='{0}' and recycle=0 and reg_status=1
                   union
                   select Regno ,reg_FName,reg_LName ,ProfessionName ,OrganizationName,RegCountry ,reg_Category ,reg_email
                    ,(Select min(PDate) From tb_Onsite_PrintLog Where PRegno=OnsiteDelegateWithRefValue.Regno And ShowID='{0}'
                   group by PRegno
                        ) as PrintDate from OnsiteDelegateWithRefValue where ShowID='{0}'
                   )A ";
            sql = string.Format(sql, txtShowID.Text.Trim());
            DataTable dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dt;
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string strSerarch = txtSearch.Text;
        if (string.IsNullOrEmpty(strSerarch))
        {
            getData(cFun.solveSQL(txtSearch.Text.Trim()));
        }
        else
        {
            string regno = GetRegNO(strSerarch);
            string showID = txtShowID.Text.Trim();
            if (!string.IsNullOrEmpty(showID))
            {
                if (!string.IsNullOrEmpty(regno))
                {
                    getData(cFun.solveSQL(txtSearch.Text.Trim()));
                }
                else
                {
                    getData(cFun.solveSQL(txtSearch.Text.Trim()));
                }

                txtSearch.Focus();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }
    #region GetRegno
    private string GetRegNO(string str)
    {
        string rtn = "";
        try
        {
            if (str.Length > 0)
            {
                string tmp = "";
                string[] arrList = str.Split('-'); // MFA2018-233333378 fromat
                if (arrList.Length == 2)
                    tmp = arrList[1];
                else
                    tmp = str;

                if (isRegno(tmp))
                    rtn = tmp;
            }
        }
        catch { }
        return rtn;
    }
    private bool isRegno(string input)
    {
        bool isRegno = false;
        try
        {
            isRegno = Regex.IsMatch(input, @"^\d+$");

        }
        catch { }
        return isRegno;
    }
    #endregion
    private DataTable getData(string searchValue)
    {
        DataTable dt = new DataTable();
        try
        {
            Functionality fn = new Functionality();
            string sql = "";//ShowID,
            #region Old (datetime)
            //         sql = @"  
            //   select Regno 'Registration Number', reg_FName 'Full Name',* from (
            //select Regno , reg_FName ,reg_LName Company, ProfessionName 'Business Category' , OrganizationName 'Main Job Function',RegCountry Country,case when reg_urlFlowID= 'F378' Then 'Visitor' else 'Trade Visitor' End 'Reg Category' , reg_email Email ,(Select Convert(NVARCHAR(23), min(PDate), 121) From tb_Onsite_PrintLog Where PRegno=DelegateWithRefValue.Regno And ShowID='{0}'
            //group by PRegno
            //     ) as PrintDate from DelegateWithRefValue 
            //where ShowID='{0}' and recycle=0 and reg_status=1
            //union
            //select Regno ,reg_FName ,reg_LName Company,ProfessionName 'Business Category' ,OrganizationName 'Main Job Function',RegCountry Country,reg_Category 'Reg Category' ,reg_email Email
            // ,(Select Convert(NVARCHAR(23), min(PDate), 121) From tb_Onsite_PrintLog Where PRegno=OnsiteDelegateWithRefValue.Regno And ShowID='{0}'
            //group by PRegno
            //     ) as PrintDate from OnsiteDelegateWithRefValue where ShowID='{0}'
            //)A ";
            #endregion
            sql = @"  
                select Regno 'Registration Number', reg_FName 'Full Name',reg_LName Company,ProfessionName 'Business Category' , OrganizationName 'Main Job Function',RegCountry Country,reg_Category 'Reg Category' ,reg_email Email,convert(varchar, PrintDate, 103) as PrintDate,convert(varchar, PrintDate, 108) as PrintTime from (
                select Regno , reg_FName ,reg_LName, ProfessionName, OrganizationName,RegCountry ,case when reg_urlFlowID= 'F378' Then 'Visitor' else 'Trade Visitor' End reg_Category , reg_email ,(Select min(PDate) From tb_Onsite_PrintLog Where PRegno=DelegateWithRefValue.Regno And ShowID='{0}'
                group by PRegno
                    ) as PrintDate from DelegateWithRefValue 
                where ShowID='{0}' and recycle=0 and reg_status=1
                union
                select Regno ,reg_FName ,reg_LName,ProfessionName ,OrganizationName ,RegCountry ,reg_Category ,reg_email 
                ,(Select min(PDate) From tb_Onsite_PrintLog Where PRegno=OnsiteDelegateWithRefValue.Regno And ShowID='{0}'
                group by PRegno
                    ) as PrintDate from OnsiteDelegateWithRefValue where ShowID='{0}'
                )A ";
            sql = string.Format(sql, txtShowID.Text.Trim());
            if (!string.IsNullOrEmpty(searchValue))
            {
                string sqlwhere = "";
                sqlwhere = @"    where Regno like N'%{0}%' or reg_LName like N'%{0}%' or reg_FName like N'%{0}%'";
                sql = sql + sqlwhere;
                sql = string.Format(sql, searchValue);
            }
            dt = fn.GetDatasetByCommand(sql, "DT").Tables["DT"];
            if (dt.Rows.Count > 0)
            {
                GKeyMaster.DataSource = dt;
                GKeyMaster.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
        return dt;
    }
}