﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Payment;
using Corpit.Logging;
using Corpit.BackendMaster;
using System.Data.SqlClient;

public partial class PreviewConfirmation : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    #region Contact Person (Group)
    static string _GSalutation = "Salutation";
    static string _GFname = "FName";
    static string _GLname = "LName";
    static string _GDesignation = "Designation";
    static string _GDepartment = "Department";
    static string _GCompany = "Company";
    static string _GIndustry = "Industry";
    static string _GAddress1 = "Address1";
    static string _GAddress2 = "Address2";
    static string _GAddress3 = "Address3";
    static string _GCity = "City";
    static string _GState = "State";
    static string _GPostalCode = "Postal Code";
    static string _GCountry = "Country";
    static string _GRCountry = "RCountry";
    static string _GTelcc = "Telcc";
    static string _GTelac = "Telac";
    static string _GTel = "Tel";
    static string _GMobilecc = "Mobilecc";
    static string _GMobileac = "Mobileac";
    static string _GMobile = "Mobile";
    static string _GFaxcc = "Faxcc";
    static string _GFaxac = "Faxac";
    static string _GFax = "Fax";
    static string _GEmail = "Email";
    static string _GEmailConfirmation = "Email Confirmation";
    static string _GVisitDate = "Visit Date";
    static string _GVisitTime = "Visit Time";
    static string _GPassword = "Password";
    static string _GOtherSalutation = "Other Salutation";
    static string _GOtherDesignation = "Other Designation";
    static string _GOtherIndustry = "Other Industry";

    static string _GGender = "Gender";
    static string _GDOB = "DOB";
    static string _GAge = "Age";
    static string _GAdditional4 = "Additional4";
    static string _GAdditional5 = "Additional5";
    #endregion

    #region Company
    static string _CName = "Name";
    static string _CAddress1 = "Address1";
    static string _CAddress2 = "Address2";
    static string _CAddress3 = "Address3";
    static string _CCity = "City";
    static string _CState = "State";
    static string _CZipCode = "Zip Code";
    static string _CCountry = "Country";
    static string _CTelcc = "Telcc";
    static string _CTelac = "Telac";
    static string _CTel = "Tel";
    static string _CFaxcc = "Faxcc";
    static string _CFaxac = "Faxac";
    static string _CFax = "Fax";
    static string _CEmail = "Email";
    static string _CEmailConfirmation = "Email Confirmation";
    static string _CWebsite = "Website";
    static string _CAdditional1 = "Additional1";
    static string _CAdditional2 = "Additional2";
    static string _CAdditional3 = "Additional3";
    static string _CAdditional4 = "Additional4";
    static string _CAdditional5 = "Additional5";
    #endregion

    #region Delegate
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string other_value = "Others";
    static string prostudent = "Student";
    static string proalliedhealth = "Allied Health";
    #endregion

    public string galaDetails = "";
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                DataTable dtDelegate = setDynamicForm(flowid, showid);
                SiteSettings st = new SiteSettings(fn, showid);

                if (st.hasterm == YesOrNo.Yes)
                {
                    divTerm.Visible = true;
                    bindTermsCondition(showid);
                }
                else
                {
                    divTerm.Visible = false;
                }

                InvoiceControler iControler = new InvoiceControler(fn);
                bindPaymentMode(showid);
                if (iControler.checkPaymentMethodUsed(showid))
                {
                    divPaymentMethod.Visible = true;
                }
                else
                {
                    divPaymentMethod.Visible = false;
                }

                if (Request.Params["t"] != null)
                {
                    string admintype = cFun.DecryptValue(Request.QueryString["t"].ToString());
                    if (admintype == BackendStaticValueClass.isAdmin)
                    {
                        bindPersonnalInfo(showid, flowid, dtDelegate, admintype);

                        bindConferenceCalPrice(showid, urlQuery, true);
                    }
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region bindConferenceCalPrice
    private void bindConferenceCalPrice(string showid, FlowURLQuery urlQuery, bool isPreview)
    {
        SiteSettings st = new SiteSettings(fn, showid);
        #region Conference
        string subTotal = Number.zeroDecimal.ToString();
        lblSubTotalCurr.Text = st.SiteCurrency;
        lblAdminFeeCurr.Text = st.SiteCurrency;
        lblGSTCurr.Text = st.SiteCurrency;
        lblGrandCurr.Text = st.SiteCurrency;

        lblShowOrderList.Text = LoadOrderedList(urlQuery, ref subTotal, isPreview);
        subTotal = cFun.FormatCurrency(subTotal.ToString());
        lblSubTotal.Text = subTotal;
        //lblGrandTotal.Text = subTotal;

        if (st.AllowPartialPayment == YesOrNo.Yes)
        {
            PanelPartial.Visible = true;
        }

        decimal adminfee = cFun.ParseDecimal(Number.Zero);
        decimal gstfee = cFun.ParseDecimal(Number.Zero);
        if (rbCreditCard.Checked == true)
        {
            adminfee = cFun.ParseDecimal(st.adminfee);

            gstfee = cFun.ParseDecimal(st.gstfee);
        }

        decimal ttadminfee = cFun.ParseDecimal(Number.Zero);
        if (rbTT.Checked == true)
        {
            ttadminfee = cFun.ParseDecimal(st.ttAdminFee);
        }

        decimal subTotalPrice = 0;
        decimal.TryParse(subTotal, out subTotalPrice);
        decimal TotalPrice = subTotalPrice;

        decimal realAdminFee = Math.Round(subTotalPrice * adminfee, 2);
        lblAdminFeeText.Text = (adminfee * 100).ToString();
        lblAdminFee.Text = realAdminFee.ToString();
        TotalPrice += realAdminFee;

        decimal realGstFee = Math.Round(TotalPrice * gstfee, 2);
        lblGSTFeeText.Text = (gstfee * 100).ToString();
        lblGstFee.Text = realGstFee.ToString();
        TotalPrice += realGstFee;

        decimal realTTAdminFee = subTotalPrice == Number.zeroDecimal ? Number.zeroDecimal : ttadminfee;//Math.Round(TotalPrice * ttadminfee, 2);
        lblTTAdminFeeText.Text = (ttadminfee).ToString();
        lblTTAdminFee.Text = realTTAdminFee.ToString();
        TotalPrice += realTTAdminFee;

        lblGrandTotal.Text = TotalPrice.ToString();

        showFeeByPaymentMethod(showid);
        #endregion
    }
    #endregion

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region Set Dynamic Form (set div visibility and tr visibility dynamically (generate dynamic form) according to the settings of tb_Form table where form_type='G'(for group contact person) or  form_type='C'(for company) or  form_type='D'(for delegate))
    protected DataTable setDynamicForm(string flowid, string showid)
    {
        DataTable dtD = new DataTable();

        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;

        DataSet ds = new DataSet();

        try
        {
            #region Contact Person (Group)
            ds = frmObj.getDynFormForGroup();

            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
            {
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GSalutation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGSalutation.Visible = true;
                        lblGSalutation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGSalutation.Text = frmObj.getDummyByInputName(_GSalutation, FormType.TypeGroup);
                    }
                    else
                    {
                        trGSalutation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GFname)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGFName.Visible = true;
                        lblGFName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGFName.Text = frmObj.getDummyByInputName(_GFname, FormType.TypeGroup);
                    }
                    else
                    {
                        trGFName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GLname)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGLName.Visible = true;
                        lblGLName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGLName.Text = frmObj.getDummyByInputName(_GLname, FormType.TypeGroup);
                    }
                    else
                    {
                        trGLName.Visible = false;
                    }

                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GDesignation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGDesignation.Visible = true;
                        lblGDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGDesignation.Text = frmObj.getDummyByInputName(_GDesignation, FormType.TypeGroup);
                    }
                    else
                    {
                        trGDesignation.Visible = false;
                    }

                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GDepartment)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGDepartment.Visible = true;
                        lblGDepartment.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGDepartment.Text = frmObj.getDummyByInputName(_GDepartment, FormType.TypeGroup);
                    }
                    else
                    {
                        trGDepartment.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GCompany)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGCompany.Visible = true;
                        lblGCompany.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGCompany.Text = frmObj.getDummyByInputName(_GCompany, FormType.TypeGroup);
                    }
                    else
                    {
                        trGCompany.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GIndustry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGIndustry.Visible = true;
                        lblGIndustry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGIndustry.Text = frmObj.getDummyByInputName(_GIndustry, FormType.TypeGroup);
                    }
                    else
                    {
                        trGIndustry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAddress1)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAddress1.Visible = true;
                        lblGAddress1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGAddress1.Text = frmObj.getDummyByInputName(_GAddress1, FormType.TypeGroup);
                    }
                    else
                    {
                        trGAddress1.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAddress2)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAddress2.Visible = true;
                        lblGAddress2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGAddress2.Text = frmObj.getDummyByInputName(_GAddress2, FormType.TypeGroup);
                    }
                    else
                    {
                        trGAddress2.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAddress3)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAddress3.Visible = true;
                        lblGAddress3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGAddress3.Text = frmObj.getDummyByInputName(_GAddress3, FormType.TypeGroup);
                    }
                    else
                    {
                        trGAddress3.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GCity)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGCity.Visible = true;
                        lblGCity.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGCity.Text = frmObj.getDummyByInputName(_GCity, FormType.TypeGroup);
                    }
                    else
                    {
                        trGCity.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GState)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGState.Visible = true;
                        lblGState.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGState.Text = frmObj.getDummyByInputName(_GState, FormType.TypeGroup);
                    }
                    else
                    {
                        trGState.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GPostalCode)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGPostalCode.Visible = true;
                        lblGPostalCode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGPostalCode.Text = frmObj.getDummyByInputName(_GPostalCode, FormType.TypeGroup);
                    }
                    else
                    {
                        trGPostalCode.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGCountry.Visible = true;
                        lblGCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGCountry.Text = frmObj.getDummyByInputName(_GCountry, FormType.TypeGroup);
                    }
                    else
                    {
                        trGCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GRCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGRCountry.Visible = true;
                        lblGRCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGRCountry.Text = frmObj.getDummyByInputName(_GRCountry, FormType.TypeGroup);
                    }
                    else
                    {
                        trGRCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GTel)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGTel.Visible = true;
                        lblGTel.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGTel.Text = frmObj.getDummyByInputName(_GTel, FormType.TypeGroup);
                    }
                    else
                    {
                        trGTel.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GMobile)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGMobile.Visible = true;
                        lblGMobile.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGMobile.Text = frmObj.getDummyByInputName(_GMobile, FormType.TypeGroup);
                    }
                    else
                    {
                        trGMobile.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GFax)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGFax.Visible = true;
                        lblGFax.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGFax.Text = frmObj.getDummyByInputName(_GFax, FormType.TypeGroup);
                    }
                    else
                    {
                        trGFax.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GEmail)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGEmail.Visible = true;
                        lblGEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGEmail.Text = frmObj.getDummyByInputName(_GEmail, FormType.TypeGroup);
                    }
                    else
                    {
                        trGEmail.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAge)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAge.Visible = true;
                        lblGAge.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGAge.Text = frmObj.getDummyByInputName(_GAge, FormType.TypeGroup);
                    }
                    else
                    {
                        trGAge.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GDOB)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGDOB.Visible = true;
                        lblGDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGDOB.Text = frmObj.getDummyByInputName(_GDOB, FormType.TypeGroup);
                    }
                    else
                    {
                        trGDOB.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GGender)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGGender.Visible = true;
                        lblGGender.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGGender.Text = frmObj.getDummyByInputName(_GGender, FormType.TypeGroup);
                    }
                    else
                    {
                        trGGender.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GVisitDate)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGVisitDate.Visible = true;
                        lblGVisitDate.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGVisitDate.Text = frmObj.getDummyByInputName(_GVisitDate, FormType.TypeGroup);
                    }
                    else
                    {
                        trGVisitDate.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GVisitTime)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGVisitTime.Visible = true;
                        lblGVisitTime.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGVisitTime.Text = frmObj.getDummyByInputName(_GVisitTime, FormType.TypeGroup);
                    }
                    else
                    {
                        trGVisitTime.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GPassword)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGPassword.Visible = true;
                        lblGPassword.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGPassword.Text = frmObj.getDummyByInputName(_GPassword, FormType.TypeGroup);
                    }
                    else
                    {
                        trGPassword.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAdditional4)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAdditional4.Visible = true;
                        lblGAdditional4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGAdditional4.Text = frmObj.getDummyByInputName(_GAdditional4, FormType.TypeGroup);
                    }
                    else
                    {
                        trGAdditional4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _GAdditional5)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGAdditional5.Visible = true;
                        lblGAdditional5.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblGAdditional5.Text = frmObj.getDummyByInputName(_GAdditional5, FormType.TypeGroup);
                    }
                    else
                    {
                        trGAdditional5.Visible = false;
                    }
                }
            }
            #endregion

            #region Company
            ds = frmObj.getDynFormForCompany();

            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
            {
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCName.Visible = true;
                        lblCName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCName.Text = frmObj.getDummyByInputName(_CName, FormType.TypeGroup);
                    }
                    else
                    {
                        trCName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAddress1)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAddress1.Visible = true;
                        lblCAddress1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCAddress1.Text = frmObj.getDummyByInputName(_CAddress1, FormType.TypeGroup);
                    }
                    else
                    {
                        trCAddress1.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAddress2)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAddress2.Visible = true;
                        lblCAddress2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCAddress2.Text = frmObj.getDummyByInputName(_CAddress2, FormType.TypeGroup);
                    }
                    else
                    {
                        trCAddress2.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAddress3)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAddress3.Visible = true;
                        lblCAddress3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCAddress3.Text = frmObj.getDummyByInputName(_CAddress3, FormType.TypeGroup);
                    }
                    else
                    {
                        trCAddress3.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CCity)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCCity.Visible = true;
                        lblCCity.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCCity.Text = frmObj.getDummyByInputName(_CCity, FormType.TypeGroup);
                    }
                    else
                    {
                        trCCity.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CState)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCState.Visible = true;
                        lblCState.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCState.Text = frmObj.getDummyByInputName(_CState, FormType.TypeGroup);
                    }
                    else
                    {
                        trCState.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CZipCode)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCZipcode.Visible = true;
                        lblCZipcode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCZipcode.Text = frmObj.getDummyByInputName(_CZipCode, FormType.TypeGroup);
                    }
                    else
                    {
                        trCZipcode.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCCountry.Visible = true;
                        lblCCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCCountry.Text = frmObj.getDummyByInputName(_CCountry, FormType.TypeGroup);
                    }
                    else
                    {
                        trCCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CTel)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCTel.Visible = true;
                        lblCTel.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCTel.Text = frmObj.getDummyByInputName(_CTel, FormType.TypeGroup);
                    }
                    else
                    {
                        trCTel.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CFax)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCFax.Visible = true;
                        lblCFax.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCFax.Text = frmObj.getDummyByInputName(_CFax, FormType.TypeGroup);
                    }
                    else
                    {
                        trCFax.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CEmail)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCEmail.Visible = true;
                        lblCEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCEmail.Text = frmObj.getDummyByInputName(_CEmail, FormType.TypeGroup);
                    }
                    else
                    {
                        trCEmail.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CWebsite)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCWebsite.Visible = true;
                        lblCWebsite.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCWebsite.Text = frmObj.getDummyByInputName(_CWebsite, FormType.TypeGroup);
                    }
                    else
                    {
                        trCWebsite.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAdditional1)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAdditional1.Visible = true;
                        lblCAdditional1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCAdditional1.Text = frmObj.getDummyByInputName(_CAdditional1, FormType.TypeGroup);
                    }
                    else
                    {
                        trCAdditional1.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAdditional2)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAdditional2.Visible = true;
                        lblCAdditional2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCAdditional2.Text = frmObj.getDummyByInputName(_CAdditional2, FormType.TypeGroup);
                    }
                    else
                    {
                        trCAdditional2.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAdditional3)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAdditional3.Visible = true;
                        lblCAdditional3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCAdditional3.Text = frmObj.getDummyByInputName(_CAdditional3, FormType.TypeGroup);
                    }
                    else
                    {
                        trCAdditional3.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAdditional4)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAdditional4.Visible = true;
                        lblCAdditional4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCAdditional4.Text = frmObj.getDummyByInputName(_CAdditional4, FormType.TypeGroup);
                    }
                    else
                    {
                        trCAdditional4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _CAdditional5)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCAdditional5.Visible = true;
                        lblCAdditional5.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                        _lblCAdditional5.Text = frmObj.getDummyByInputName(_CAdditional5, FormType.TypeGroup);
                    }
                    else
                    {
                        trCAdditional5.Visible = false;
                    }
                }
            }
            #endregion

            #region Delegate
            ds = frmObj.getDynFormForDelegate();

            DataRow newDr = dtD.NewRow();

            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
            {
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Salutation, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);
                    if (isshow == 1)
                    {
                        trSalutation.Visible = true;
                        lblSal.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSalutation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Fname, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trFName.Visible = true;
                        lblFName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trFName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Lname, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trLName.Visible = true;
                        lblLName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trLName.Visible = false;
                    }

                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OName)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_OName, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trOName.Visible = true;
                        lblOName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trOName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_PassNo, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trPassno.Visible = true;
                        lblPassno.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trPassno.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_isReg, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trIsReg.Visible = true;
                        lblIsReg.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trIsReg.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_regSpecific, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trRegSpecific.Visible = true;
                        lblRegSpecific.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trRegSpecific.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_IDNo, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trIDNo.Visible = true;
                        lblIDNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trIDNo.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Designation, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDesignation.Visible = true;
                        lblDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDesignation.Visible = false;
                    }

                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Profession, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trProfession.Visible = true;
                        lblDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trProfession.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Department, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDept.Visible = true;
                        lblDept.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDept.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Organization, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trOrg.Visible = true;
                        lblOrg.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trOrg.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Institution, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trInstitution.Visible = true;
                        lblInstitution.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trInstitution.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Address1, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress1.Visible = true;
                        lblAddress1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress1.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Address2, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress2.Visible = true;
                        lblAddress2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress2.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Address3, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress3.Visible = true;
                        lblAddress3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress3.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Address4, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress4.Visible = true;
                        lblAddress4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_City, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCity.Visible = true;
                        lblCity.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCity.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_State, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trState.Visible = true;
                        lblState.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trState.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_PostalCode, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trPostal.Visible = true;
                        lblPostal.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trPostal.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Country, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCountry.Visible = true;
                        lblCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_RCountry, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trRCountry.Visible = true;
                        lblRCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trRCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Tel, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trTel.Visible = true;
                        lblTel.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trTel.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Mobile, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trMobile.Visible = true;
                        lblMobile.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trMobile.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Fax, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trFax.Visible = true;
                        lblFax.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trFax.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Email, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trEmail.Visible = true;
                        lblEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trEmail.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Affiliation, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAffiliation.Visible = true;
                        lblAffiliation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAffiliation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Dietary, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDietary.Visible = true;
                        lblDietary.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDietary.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Nationality, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trNationality.Visible = true;
                        lblNationality.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trNationality.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Age, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAge.Visible = true;
                        lblAge.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAge.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_DOB, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDOB.Visible = true;
                        lblDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDOB.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Gender, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGender.Visible = true;
                        lblGender.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGender.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_MembershipNo, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trMembershipNo.Visible = true;
                        lblMembershipNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trMembershipNo.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Additional4, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAdditional4.Visible = true;
                        lblAdditional4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAdditional4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_Additional5, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAdditional5.Visible = true;
                        lblAdditional5.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAdditional5.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_VName, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVName.Visible = true;
                        lblVName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_VDOB, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVDOB.Visible = true;
                        lblVDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVDOB.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_VPassNo, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVPass.Visible = true;
                        lblVPass.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVPass.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_VPassExpiry, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVPassExpiry.Visible = true;
                        lblVPassExpiry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVPassExpiry.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_VCountry, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVCountry.Visible = true;
                        lblVCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVCountry.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_UDF_CName, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CName.Visible = true;
                        lblUDF_CName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CName.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_UDF_DelegateType, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_DelegateType.Visible = true;
                        lblUDF_DelegateType.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_DelegateType.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_UDF_ProfCategory, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_ProfCategory.Visible = true;
                        lblUDF_ProfCategory.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_ProfCategory.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_UDF_CPcode, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CPcode.Visible = true;
                        lblUDF_CPcode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CPcode.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_UDF_CLDepartment, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CLDepartment.Visible = true;
                        lblUDF_CLDepartment.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CLDepartment.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_UDF_CAddress, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CAddress.Visible = true;
                        lblUDF_CAddress.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CAddress.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_UDF_CLCompany, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CLCompany.Visible = true;
                        lblUDF_CLCompany.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CLCompany.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_UDF_CCountry, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CCountry.Visible = true;
                        lblUDF_CCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CCountry.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_SupName, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupName.Visible = true;
                        lblSupName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupName.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_SupDesignation, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupDesignation.Visible = true;
                        lblSupDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupDesignation.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_SupContact, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupContact.Visible = true;
                        lblSupContact.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupContact.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                {
                    DataTable dtDummy = frmObj.getDummyDataTableByInputName(_SupEmail, FormType.TypeDelegate);
                    if (dtDummy.Rows.Count > 0)
                    {
                        string settingheadName = dtDummy.Rows[0]["form_tblColumnMatch"] != null ? dtDummy.Rows[0]["form_tblColumnMatch"].ToString() : "";
                        string dummyData = dtDummy.Rows[0]["form_dummydata"] != null ? dtDummy.Rows[0]["form_dummydata"].ToString() : "";
                        if (!string.IsNullOrEmpty(settingheadName))
                        {
                            dtD.Columns.Add(settingheadName, typeof(string));
                            newDr[settingheadName] = dummyData;
                        }
                    }

                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupEmail.Visible = true;
                        lblSupEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupEmail.Visible = false;
                    }
                }
            }
            dtD.Rows.Add(newDr);
            #endregion
        }
        catch(Exception ex)
        { }

        return dtD;
    }
    #endregion

    #region bindTermsCondition (bind terms&conditions data from tb_Terms table to chkTerms control)
    private void bindTermsCondition(string showid)
    {
        try
        {
            #region Comment bind Terms on chkTerms
            //CommonDataObj cmdObj = new CommonDataObj(fn);
            //DataTable dt = cmdObj.getTerms();
            //if (dt.Rows.Count > 0)
            //{
            //    foreach (DataRow dr in dt.Rows)
            //    {
            //        ListItem item = new ListItem(dr["terms_Desc"].ToString(), dr["terms_ID"].ToString());
            //        chkTerms.Items.Add(item);
            //    }
            //}
            #endregion

            SiteSettings st = new SiteSettings(fn, showid);
            lblTerms.Text = Server.HtmlDecode(st.termsTemplate);
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region bindPaymentMode (bind payment methods data from ref_PaymentMethod table to rbPaymentMode control)
    private void bindPaymentMode(string showID)
    {
        try
        {
            CommonDataObj cmdObj = new CommonDataObj(fn);
            DataTable dt = cmdObj.getPaymentMethods(showID);
            if (dt.Rows.Count > 0)
            {
                SiteSettings st = new SiteSettings(fn, showID);
                foreach (DataRow dr in dt.Rows)
                {
                    string usedid = dr["method_usedid"].ToString();

                    if(usedid == ((int)PaymentType.CreditCard).ToString())
                    {
                        trCreditCard.Visible = true;
                        lblCreditCard.Text = Server.HtmlDecode(st.creditcardText);
                    }
                    if (usedid == ((int)PaymentType.TT).ToString())
                    {
                        trTT.Visible = true;
                        lblTT.Text = Server.HtmlDecode(st.ttText);
                    }
                    if (usedid == ((int)PaymentType.Waved).ToString())
                    {
                        trWaived.Visible = true;
                        lblWaived.Text = Server.HtmlDecode(st.waivedText);
                    }
                    if (usedid == ((int)PaymentType.Cheque).ToString())
                    {
                        trCheque.Visible = true;
                        lblCheque.Text = Server.HtmlDecode(st.chequeText);
                    }
                }
            }
            else
            {      // lblPaymentModeTitle.Style.Add("display","none");
                lblPaymentModeTitle.Visible = false;
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region bindPersonnalInfo (bind contact person(group) info, company info, delegate info to repective controls if those data exist for current RegGroupID)
    protected void bindPersonnalInfo(string showid, string flowid, DataTable dtD, string isPreview=null)
    {
        string userid = string.Empty;

        try
        {
            #region Contact Person Info (Group)
            FlowControler fControler = new FlowControler(fn);
            FlowMaster fMaster = fControler.GetFlowMasterConfig(flowid);
            if (isPreview != null)
            {
                if (fMaster.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    divContactPerson.Visible = true;
                }
                else
                {
                    divContactPerson.Visible = false;
                }
            }
            #endregion

            #region Company Info
            if (isPreview != null)
            {
                bool confExist = fControler.checkPageExist(flowid, SiteDefaultValue.constantRegCompany);
                if (fMaster.FlowType == SiteFlowType.FLOW_GROUP && confExist)
                {
                    divCompany.Visible = true;
                }
                else
                {
                    divCompany.Visible = false;
                }
            }
            #endregion

            #region Delegatge Info
            if (dtD.Rows.Count > 0)
            {
                divDelegate.Visible = true;

                rptItem.DataSource = dtD;
                rptItem.DataBind();
            }
            else
            {
                divDelegate.Visible = false;
            }

            if (isPreview != null)
            {
                if (fMaster.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                {
                    divDelegate.Visible = true;
                }
                else
                {
                    divDelegate.Visible = false;
                }
            }
            #endregion
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region rptitemdatabound (set repeater item cells' visibility dynamically according to the settings of tb_Form table where form_type='D')
    protected void rptitemdatabound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                // This event is raised for the header, the footer, separators, and items.
                // Execute the following logic for Items and Alternating Items.
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.HtmlControls.HtmlTableCell tdSalutation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSalutation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdFName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdFName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdLName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdLName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdOName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdOName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdPassNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPassno");
                    System.Web.UI.HtmlControls.HtmlTableCell tdisReg = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdIsReg");
                    System.Web.UI.HtmlControls.HtmlTableCell tdregSpecific = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdRegSpecific");
                    System.Web.UI.HtmlControls.HtmlTableCell tdIDNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdIDNo");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDesignation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDesignation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdProfession = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdProfession");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDepartment = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDept");
                    System.Web.UI.HtmlControls.HtmlTableCell tdOrganization = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdOrg");
                    System.Web.UI.HtmlControls.HtmlTableCell tdInstitution = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdInstitution");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress1 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress1");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress2 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress2");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress3 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress3");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAddress4 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress4");
                    System.Web.UI.HtmlControls.HtmlTableCell tdCity = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCity");
                    System.Web.UI.HtmlControls.HtmlTableCell tdState = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdState");
                    System.Web.UI.HtmlControls.HtmlTableCell tdPostalCode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPostal");
                    System.Web.UI.HtmlControls.HtmlTableCell tdCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdRCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdRCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdTel = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdTel");
                    System.Web.UI.HtmlControls.HtmlTableCell tdMobile = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdMobile");
                    System.Web.UI.HtmlControls.HtmlTableCell tdFax = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdFax");
                    System.Web.UI.HtmlControls.HtmlTableCell tdEmail = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdEmail");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAffiliation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAffiliation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDietary = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDietary");
                    System.Web.UI.HtmlControls.HtmlTableCell tdNationality = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdNationality");

                    System.Web.UI.HtmlControls.HtmlTableCell tdAge = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAge");
                    System.Web.UI.HtmlControls.HtmlTableCell tdDOB = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDOB");
                    System.Web.UI.HtmlControls.HtmlTableCell tdGender = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdGender");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAdditional4 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAdditional4");
                    System.Web.UI.HtmlControls.HtmlTableCell tdAdditional5 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAdditional5");

                    System.Web.UI.HtmlControls.HtmlTableCell tdMembershipNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdMembershipNo");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVDOB = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVDOB");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVPassNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVPass");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVPassExpiry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVPassExpiry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdVCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_DelegateType = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_DelegateType");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_ProfCategory = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_ProfCategory");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CPcode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CPcode");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CLDepartment = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CLDepartment");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CAddress = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CAddress");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CLCompany = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CLCompany");
                    System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CCountry");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupName");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupDesignation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupDesignation");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupContact = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupContact");
                    System.Web.UI.HtmlControls.HtmlTableCell tdSupEmail = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupEmail");

                    DataSet ds = new DataSet();
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    ds = frmObj.getDynFormForDelegate();

                    for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                    {
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);


                            if (isshow == 1)
                            {
                                tdSalutation.Visible = true;
                            }
                            else
                            {
                                tdSalutation.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdFName.Visible = true;
                            }
                            else
                            {
                                tdFName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdLName.Visible = true;

                            }
                            else
                            {
                                tdLName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdOName.Visible = true;
                            }
                            else
                            {
                                tdOName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdPassNo.Visible = true;
                            }
                            else
                            {
                                tdPassNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdisReg.Visible = true;
                            }
                            else
                            {
                                tdisReg.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdregSpecific.Visible = true;
                            }
                            else
                            {
                                tdregSpecific.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdIDNo.Visible = true;
                            }
                            else
                            {
                                tdIDNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDesignation.Visible = true;
                            }
                            else
                            {
                                tdDesignation.Visible = false;
                            }

                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdProfession.Visible = true;
                            }
                            else
                            {
                                tdProfession.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDepartment.Visible = true;
                            }
                            else
                            {
                                tdDepartment.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdOrganization.Visible = true;
                            }
                            else
                            {
                                tdOrganization.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdInstitution.Visible = true;
                            }
                            else
                            {
                                tdInstitution.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress1.Visible = true;
                            }
                            else
                            {
                                tdAddress1.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress2.Visible = true;
                            }
                            else
                            {
                                tdAddress2.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress3.Visible = true;
                            }
                            else
                            {
                                tdAddress3.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAddress4.Visible = true;
                            }
                            else
                            {
                                tdAddress4.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdCity.Visible = true;
                            }
                            else
                            {
                                tdCity.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdState.Visible = true;
                            }
                            else
                            {
                                tdState.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdPostalCode.Visible = true;
                            }
                            else
                            {
                                tdPostalCode.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdCountry.Visible = true;
                            }
                            else
                            {
                                tdCountry.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdRCountry.Visible = true;
                            }
                            else
                            {
                                tdRCountry.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdTel.Visible = true;
                            }
                            else
                            {
                                tdTel.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdMobile.Visible = true;
                            }
                            else
                            {
                                tdMobile.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdFax.Visible = true;
                            }
                            else
                            {
                                tdFax.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdEmail.Visible = true;
                            }
                            else
                            {
                                tdEmail.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAffiliation.Visible = true;
                            }
                            else
                            {
                                tdAffiliation.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDietary.Visible = true;
                            }
                            else
                            {
                                tdDietary.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdNationality.Visible = true;
                            }
                            else
                            {
                                tdNationality.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAge.Visible = true;
                            }
                            else
                            {
                                tdAge.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdDOB.Visible = true;
                            }
                            else
                            {
                                tdDOB.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdGender.Visible = true;
                            }
                            else
                            {
                                tdGender.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAdditional4.Visible = true;
                            }
                            else
                            {
                                tdAdditional4.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdAdditional5.Visible = true;
                            }
                            else
                            {
                                tdAdditional5.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdMembershipNo.Visible = true;
                            }
                            else
                            {
                                tdMembershipNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVName.Visible = true;
                            }
                            else
                            {
                                tdVName.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVDOB.Visible = true;
                            }
                            else
                            {
                                tdVDOB.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVPassNo.Visible = true;
                            }
                            else
                            {
                                tdVPassNo.Visible = false;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVPassExpiry.Visible = true;
                            }
                            else
                            {
                                tdVPassExpiry.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdVCountry.Visible = true;
                            }
                            else
                            {
                                tdVCountry.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CName.Visible = true;
                            }
                            else
                            {
                                tdUDF_CName.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_DelegateType.Visible = true;
                            }
                            else
                            {
                                tdUDF_DelegateType.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_ProfCategory.Visible = true;
                            }
                            else
                            {
                                tdUDF_ProfCategory.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CPcode.Visible = true;
                            }
                            else
                            {
                                tdUDF_CPcode.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CLDepartment.Visible = true;
                            }
                            else
                            {
                                tdUDF_CLDepartment.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CAddress.Visible = true;
                            }
                            else
                            {
                                tdUDF_CAddress.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CLCompany.Visible = true;
                            }
                            else
                            {
                                tdUDF_CLCompany.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdUDF_CCountry.Visible = true;
                            }
                            else
                            {
                                tdUDF_CCountry.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupName.Visible = true;
                            }
                            else
                            {
                                tdSupName.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupDesignation.Visible = true;
                            }
                            else
                            {
                                tdSupDesignation.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupContact.Visible = true;
                            }
                            else
                            {
                                tdSupContact.Visible = false;
                            }
                        }
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                            if (isshow == 1)
                            {
                                tdSupEmail.Visible = true;
                            }
                            else
                            {
                                tdSupEmail.Visible = false;
                            }
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region Conference (GetOrderedListWithTemplate)
    private string LoadOrderedList(FlowURLQuery urlQuery, ref string SubTotal, bool isPreview)
    {
        string template = string.Empty;
        try
        {
            OrderControler oControl = new OrderControler(fn);
            List<OrderItemList> oItemList = getDummyOrder(urlQuery);
            template = oControl.GetOrderedListWithTemplate(urlQuery, ref SubTotal, "", oItemList);
        }
        catch(Exception ex)
        { }

        return template;
    }

    #region getDummyOrder
    private List<OrderItemList> getDummyOrder(FlowURLQuery urlQuery)
    {
        List<OrderItemList> oItemList = new List<OrderItemList>();

        ConferenceControler cControl = new ConferenceControler(fn);
        CategoryObj catObj = new CategoryObj(fn);
        string DeleagateID = cFun.DecryptValue(urlQuery.DelegateID);
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string catType = catObj.getCategoryID(DeleagateID, showid, flowid);
        DataTable dtOrder = cControl.GetConferenceList(catType, showid, flowid);

        decimal mainTotal = 0;
        OrderItemList oList = GetSelectedItems(ConfDefaultValue.conf_MainItem, dtOrder, showid, ref mainTotal);
        if (oList.OrderList.Count > 0)
        {
            oItemList.Add(oList);
        }

        return oItemList;
    }

    private OrderItemList GetSelectedItems(string ConfType, DataTable dtOrder, string showid, ref decimal ToalSum)
    {
        OrderItemList oList = new OrderItemList();
        decimal total = 0;
        if (dtOrder.Rows.Count > 0)
        {
            foreach (DataRow dr in dtOrder.Rows)
            {
                int qty = 1;
                string confItemID = dr["con_itemId"].ToString();

                ConferenceControler cControl = new ConferenceControler(fn);
                ConferenceItem cItem = cControl.GetConferenceItemByID(confItemID, showid);
                string price = cControl.GetConfItemPrice("", cItem, showid);
                OrderItem oItem = new OrderItem();
                oItem.ItemID = confItemID;
                oItem.Qty = qty;
                oItem.Price = cFun.ParseDecimal(price);
                oItem.ItemType = ConfType;
                oItem.ItemDescription = cItem.ConDisplayText;
                oList.OrderList.Add(oItem);
                total += oItem.Qty * oItem.Price;
            }
        }
        ToalSum = total;
        return oList;
    }
    #endregion

    #endregion

    #region radioPaymentsChange
    protected void radioPaymentsChange(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            showFeeByPaymentMethod(showid);
            bindConferenceCalPrice(showid, urlQuery, true);
        }
    }
    #endregion

    #region showFeeByPaymentMethod
    private void showFeeByPaymentMethod(string showid)
    {
        SiteSettings st = new SiteSettings(fn, showid);
        if (rbCreditCard.Checked == true)
        {
            decimal adminfee = cFun.ParseDecimal(st.adminfee);
            if (adminfee == 0 || adminfee == (decimal)Number.zeroDecimal)
            {
                trAdminFee.Visible = false;
            }
            else
            {
                trAdminFee.Visible = true;
            }

            decimal gstfee = cFun.ParseDecimal(st.gstfee);
            if (gstfee == 0 || gstfee == (decimal)Number.zeroDecimal)
            {
                trGST.Visible = false;
            }
            else
            {
                trGST.Visible = true;
            }

            trTTAdminFee.Visible = false;
        }
        else if(rbTT.Checked == true)
        {
            trAdminFee.Visible = false;
            trGST.Visible = false;

            decimal ttadminfee = cFun.ParseDecimal(st.ttAdminFee);
            if (ttadminfee == 0 || ttadminfee == (decimal)Number.zeroDecimal)
            {
                trTTAdminFee.Visible = false;
            }
            else
            {
                trTTAdminFee.Visible = true;
            }
        }
        else
        {
            trAdminFee.Visible = false;
            trGST.Visible = false;
            trTTAdminFee.Visible = false;
        }
    }
    #endregion
}