﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="SuccessIDEM.aspx.cs" Inherits="SuccessIDEM" %>
<%@ MasterType VirtualPath="~/Registration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <!--
    Event snippet for FR~Page Load _MK~SG_PC~IDEM2020_Thankyou on https://www.event-reg.biz/Registration/SuccessIDEM?FLW=HcLK9jvrps9WrAQtPfKp0Q==&SHW=CT6vFjNOWLIC/OECGDGWKg==&GRP=aSm7ihsgOHoGbIRPr5n9BA==&STP=CYb1EU4Jp03fdWiPrpJ8eA==&INV=TK5fEUuPio6zbcXXGb3JEw==: Please do not remove.
    Place this snippet on pages with events you’re tracking. 
    Creation date: 12/19/2019
    -->
    <script>
      gtag('event', 'conversion', {
        'allow_custom_scripts': true,
        'send_to': 'DC-6953330/mice/frpag021+standard'
      });
    </script>
    <noscript>
    <img src="https://ad.doubleclick.net/ddm/activity/src=6953330;type=mice;cat=frpag021;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=1?" width="1" height="1" alt=""/>
    </noscript>
    <!-- End of event snippet: Please do not remove -->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel runat="server" ID="PanelMsg">
        <p style="padding-bottom:30px;">
            <asp:Label ID="lblSuccessMessage" runat="server"></asp:Label>
        </p>
        <%--<br /><br /><br /><br /><br />--%>
    </asp:Panel>
    <div  > 
      <iframe runat="server" id="ShowPDF" src="" width="100%" height="1200" scrolling="no"  frameborder="0" visible="false" ></iframe>    
        </div>
</asp:Content>

