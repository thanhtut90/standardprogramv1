﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="ConfirmationBA.aspx.cs" Inherits="ConfirmationBA" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType virtualpath="~/Registration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script> 

    <link rel="stylesheet" type="text/css" href="css/jquery.countdown.css" />
    <style type="text/css">
        #defaultCountdown {
            width: 295px;
            height: 20px;
        }
        .chkFooter label
        {
            font-weight:unset !important;
            padding-left:5px !important;
            vertical-align:top !important;
        }
        .chkFooter input[type="checkbox"]
        {
            margin-top: 2px !important;
        }
        input[type="radio"]
        {
             margin: 0 5px 0 5px !important;
        }
    </style>

     <script type="text/javascript" src="scripts/jquery.plugin.js"></script>
    <script type="text/javascript" src="scripts/jquery.countdown.js"></script>

    <script type="text/javascript">

        function MakeRbExclusive(rb) {
            var rbs = $('#rbData :radio');
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i] != rb) rbs[i].checked = false;
            }
        }

        function Reload() {
            window.location.reload();
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }

        function PrintDiv() {
            var divToPrint = document.getElementById('fcontent');
            //var popupWin = window.open('', '', 'width=800,height=800');//_blank
            //popupWin.document.open();
            //popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
            //popupWin.document.close();
            ////popupWin.focus();
            var mywindow = window.open('', 'PRINT', 'height=800,width=800');

            mywindow.document.write('<html><head><title>' + document.title + '</title>');
            mywindow.document.write('</head><body >');
            mywindow.document.write('<h1>' + document.title + '</h1>');
            mywindow.document.write(divToPrint.innerHTML);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%--<table width="100%">
        <tr>
            <td colspan="4" style="text-align: right; padding-right: 20px;">
                <asp:Button runat="server" Text="Re-login" ID="btnRelogin" 
                    OnClick="btnRelogin_Click" Width="100px" Height="30px" CausesValidation="false" 
                    Visible="false" CssClass="style4" style="font-family: DINPro-Regular" />
                <div style="display: none;">
                    <asp:Timer ID="timerSession" runat="server" OnTick="timerSession_Tick" Interval="60000"></asp:Timer>
                    <asp:UpdatePanel ID="upTimer" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="lblTimer" runat="server" CssClass="style4" 
                                style="font-family: DINPro-Regular"></asp:TextBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerSession" EventName="tick" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div style="float: right;">
                    <div id="defaultCountdown" style="font-family: Calibri; font-size: 14px; padding: 2px;"></div>
                </div>
            </td>
        </tr>
    </table>--%>
    <div id="fcontent">
        <div style="padding: 0px 30px 30px 30px; width: auto; background-color: #ffffff; margin:0 auto; position: relative; border-radius: 0px" class="contentC">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <br/><br/>
                    <div id="divHDR1" runat="server" visible="false">
                        <div class="clear"></div>
                        <asp:Label ID="lblHDR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                    <div id="divHDR2" runat="server" visible="false">
                        <div class="clear"></div>
                        <asp:Label ID="lblHDR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>

                    <asp:Label ID="lblCheck" runat="server"><strong style="font-family:Arial;">Please check that your personal information is correct before completing your submission.</strong></asp:Label>
                    <div id="divHDR3" runat="server" visible="false">
                        <div class="clear"></div>
                        <asp:Label ID="lblHDR3" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                    <div id="lcontent">
                        <div id="divContactPerson" runat="server" visible="false">
                            <br />
                            <h3 class="ConfrmHeader"><asp:Label ID="lblGHDR1" runat="server" Text="Group Co-ordinator Information"></asp:Label></h3>
                            <br />

                            <div class="table-responsive col-lg-offset-1 col-md-offset-1">
                                <table class="table borderless">
                                    <tr id="trRegGroupID" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGRegGroupID" runat="server" Text="Group Registration ID"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGRegGroupID" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGSalutation" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGSalutation" runat="server" Text="Title"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGSalutation" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGFName" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGFName" runat="server" Text="First Name"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGFName" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGLName" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGLName" runat="server" Text="Surname"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGLName" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGDesignation" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGDesignation" runat="server" Text="Designation"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGDesignation" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGDepartment" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGDepartment" runat="server" Text="Department"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGDepartment" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGCompany" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGCompany" runat="server" Text="Company"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGCompany" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGIndustry" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGIndustry" runat="server" Text="Industry"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGIndustry" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGAddress1" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAddress1" runat="server" Text="Address1"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAddress1" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGAddress2" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAddress2" runat="server" Text="Address2"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAddress2" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGAddress3" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAddress3" runat="server" Text="Address3"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAddress3" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGCountry" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGCountry" runat="server" Text="Country"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGCountry" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGRCountry" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGRCountry" runat="server" Text="RCountry"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGRCountry" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGCity" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGCity" runat="server" Text="City"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGCity" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGState" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGState" runat="server" Text="State"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGState" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGPostalCode" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGPostalCode" runat="server" Text="Postal Code"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGPostalCode" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGTel" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGTel" runat="server" Text="Telephone"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGTel" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGMobile" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGMobile" runat="server" Text="Mobile"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGMobile" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGFax" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGFax" runat="server" Text="Fax"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGFax" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGEmail" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGEmail" runat="server" Text="Email"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGEmail" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                     <tr id="trGAge" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAge" runat="server" Text="Age"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAge" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                     <tr id="trGDOB" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGDOB" runat="server" Text="Date of Birth"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGDOB" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                     <tr id="trGGender" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGGender" runat="server" Text="Gender"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGGender" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGVisitDate" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGVisitDate" runat="server" Text="Visit Date"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGVisitDate" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGVisitTime" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGVisitTime" runat="server" Text="Visit Time"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGVisitTime" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGPassword" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGPassword" runat="server" Text="Password"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGPassword" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                     <tr id="trGAdditional4" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAdditional4" runat="server" Text="Additional4"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAdditional4" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGAdditional5" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblGAdditional5" runat="server" Text="Additional5"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblGAdditional5" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trGEdit" runat="server">
                                        <td class="confrmTDStyle"></td>
                                        <td class="confrmTDFullColomStyle"></td>
                                        <td><asp:Button ID="btnGEditMainDelegate" runat="server" Text="Edit" CssClass="btn MainButton" Visible="false"
                                            CommandName="EditMainDelegate" OnClick="btnEditMainDelegate_Click" CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div id="divCompany" runat="server" visible="false">
                            <br />
                            <h3 class="ConfrmHeader">Company Information</h3>
                            <br />

                            <div class="table-responsive col-lg-offset-1 col-md-offset-1">
                                <table class="table borderless">
                                    <tr id="trCName" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCName" runat="server" Text="Name"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCName" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAddress1" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAddress1" runat="server" Text="Address1"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAddress1" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAddress2" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAddress2" runat="server" Text="Address2"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAddress2" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAddress3" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAddress3" runat="server" Text="Address3"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAddress3" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCCity" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCCity" runat="server" Text="City"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCCity" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCState" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCState" runat="server" Text="State"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCState" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCCountry" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCCountry" runat="server" Text="Country"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCCountry" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCZipcode" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCZipcode" runat="server" Text="Zip Code"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCZipcode" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCTel" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCTel" runat="server" Text="Telephone"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCTel" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCFax" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCFax" runat="server" Text="Fax"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCFax" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCEmail" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCEmail" runat="server" Text="Email"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCEmail" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCWebsite" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCWebsite" runat="server" Text="Website"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCWebsite" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAdditional1" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAdditional1" runat="server" Text="Additional1"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAdditional1" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAdditional2" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAdditional2" runat="server" Text="Additional2"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAdditional2" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAdditional3" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAdditional3" runat="server" Text="Additional3"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAdditional3" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAdditional4" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAdditional4" runat="server" Text="Additional4"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAdditional4" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCAdditional5" runat="server">
                                        <td class="confrmTDStyle"><asp:Label ID="lblCAdditional5" runat="server" Text="Additional5"></asp:Label></td>
                                        <td class="confrmTDFullColomStyle">:</td>
                                        <td><asp:Label ID="_lblCAdditional5" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr id="trCEdit" runat="server">
                                        <td class="confrmTDStyle"></td>
                                        <td class="confrmTDFullColomStyle"></td>
                                        <td><asp:Button ID="btnCompanyEdit" runat="server" Text="Edit" CssClass="btn MainButton" Visible="false"
                                            CommandName="EditCompany" OnClick="btnCompanyEdit_Click" CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div id="divDelegate" runat="server" visible="false">
                            <br />
                            <h3 class="ConfrmHeader"><asp:Label ID="lblDHDR1" runat="server"  Text="Your Registration Details"></asp:Label></h3>
                            <br />

                            <div class="table-responsive col-lg-offset-1 col-md-offset-1">
                                <asp:Repeater ID="rptDelegateTable" runat="server" OnItemDataBound="rptDelegateTable_ItemDataBound">
                                    <ItemTemplate>
                                        <table class="table borderless">
                                            <tr id="trSrNo" runat="server" class="confrmMemHeaderStyle">
                                                <td colspan="3">
                                                    <asp:Label ID="lblSrNo" runat="server" Text="Delegate "></asp:Label><%#Container.ItemIndex+1 %>
                                                </td>
                                            </tr>
                                            <tr id="trDRegno" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDRegno" runat="server" Text="Registration ID"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDRegno" runat="server"><%# Eval("Regno") %></asp:Label></td>
                                            </tr>
                                            <tr id="trDSalutation" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDSal" runat="server" Text="Title"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDSal" runat="server"><%# bindSalutation(Eval("reg_Salutation").ToString(), Eval("reg_SalutationOthers").ToString()) %></asp:Label></td>
                                            </tr>
                                            <tr id="trDFName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDFName" runat="server" Text="First Name"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDFName" runat="server" Text='<%#Eval("reg_FName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDLName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDLName" runat="server" Text="Surname"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDLName" runat="server" Text='<%#Eval("reg_LName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDOName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDOName" runat="server" Text="Other Name"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDOName" runat="server" Text='<%#Eval("reg_OName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDPassno" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDPassno" runat="server" Text="NRIC/Passport No."></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDPassno" runat="server" Text='<%#Eval("reg_PassNo")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDIsReg" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDIsReg" runat="server" Text="Are you a Singapore registered doctor/nurse/pharmacist?"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDIsReg" runat="server"><%#Eval("reg_isReg") != null ? (Eval("reg_isReg").ToString() == "1" ? "Yes" : "No") : "No"%></asp:Label></td>
                                            </tr>
                                            <tr id="trDRegSpecific" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDRegSpecific" runat="server" Text="MCR/SNB/PRN"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDRegSpecific" runat="server" Text='<%#Eval("reg_sgregistered")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDIDNo" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDIDNo" runat="server" Text="MCR/SNB/PRN No."></asp:Label><asp:Label ID="lblProfID" runat="server" Text='<%#Eval("reg_Profession")%>' Visible="false"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDIDNo" runat="server" Text='<%#Eval("reg_IDno")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDDesignation" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDDesignation" runat="server" Text="Designation"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDDesignation" runat="server" Text='<%#Eval("reg_Designation")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDProfession" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDProfession" runat="server" Text="Profession"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDProfession" runat="server" Text='<%# bindProfession(Eval("reg_Profession").ToString())%>'></asp:Label>
                                                    <asp:Label ID="_lblDOProfession" runat="server" Text='<%# Eval("reg_otherProfession")%>' Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trDOrg" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDOrg" runat="server" Text="Organization"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDOrg" runat="server" Text='<%# bindOrganisation(Eval("reg_Organization").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDInstitution" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDInstitution" runat="server" Text="Institution"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDInstitution" runat="server" Text='<%# bindInstitution(Eval("reg_Institution").ToString(), Eval("reg_otherInstitution").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDDept" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDDept" runat="server" Text="Department"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDDept" runat="server" Text='<%# bindDepartment(Eval("reg_Department").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAddress1" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAddress1" runat="server" Text="Address1"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAddress1" runat="server" Text='<%#Eval("reg_Address1")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAddress2" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAddress2" runat="server" Text="Address2"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAddress2" runat="server" Text='<%#Eval("reg_Address2")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAddress3" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAddress3" runat="server" Text="Address3"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAddress3" runat="server" Text='<%#Eval("reg_Address3")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAddress4" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAddress4" runat="server" Text="Address4"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAddress4" runat="server" Text='<%#Eval("reg_Address4")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDCity" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDCity" runat="server" Text="City"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDCity" runat="server" Text='<%#Eval("reg_City")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDState" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDState" runat="server" Text="State"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDState" runat="server" Text='<%#Eval("reg_State")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDCountry" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDCountry" runat="server" Text="Country"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDCountry" runat="server" Text='<%# bindCountry(Eval("reg_Country").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDPostal" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDPostal" runat="server" Text="Postal Code"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDPostal" runat="server" Text='<%#Eval("reg_PostalCode")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDRCountry" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDRCountry" runat="server" Text="RCountry"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDRCountry" runat="server" Text='<%# bindCountry(Eval("reg_RCountry").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDTel" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDTel" runat="server" Text="Telephone"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDTel" runat="server"><%# bindPhoneNo(Eval("reg_Telcc").ToString(), Eval("reg_Telac").ToString(), Eval("reg_Tel").ToString(), "Tel")%></asp:Label></td>
                                            </tr>
                                            <tr id="trDMobile" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDMobile" runat="server" Text="Mobile"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDMobile" runat="server"><%# bindPhoneNo(Eval("reg_Mobcc").ToString(), Eval("reg_Mobac").ToString(), Eval("reg_Mobile").ToString(), "Mob")%></asp:Label></td>
                                            </tr>
                                            <tr id="trDFax" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDFax" runat="server" Text="Fax"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDFax" runat="server"><%# bindPhoneNo(Eval("reg_Faxcc").ToString(), Eval("reg_Faxac").ToString(), Eval("reg_Fax").ToString(), "Fax")%></asp:Label></td>
                                            </tr>
                                            <tr id="trDEmail" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDEmail" runat="server" Text="Email"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDEmail" runat="server" Text='<%#Eval("reg_Email")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAffiliation" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAffiliation" runat="server" Text="Affiliation"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAffiliation" runat="server" Text='<%# bindAffiliation(Eval("reg_Affiliation").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDDietary" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDDietary" runat="server" Text="Dietary"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDDietary" runat="server" Text='<%# bindDietary(Eval("reg_Dietary").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDNationality" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDNationality" runat="server" Text="Nationality"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="Label4" runat="server" Text='<%#Eval("reg_Nationality")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAge" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAge" runat="server" Text="Age"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAge" runat="server" Text='<%#Eval("reg_Age")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDDOB" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDDOB" runat="server" Text="Date of Birth"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDDOB" runat="server" Text='<%#getDate(Eval("reg_DOB").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDGender" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDGender" runat="server" Text="Gender"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDGender" runat="server" Text='<%#Eval("reg_Gender")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDMembershipNo" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDMembershipNo" runat="server" Text="MembershipNo"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDMembershipNo" runat="server" Text='<%#Eval("reg_Membershipno")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAdditional4" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAdditional4" runat="server" Text="Additional4"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAdditional4" runat="server" Text='<%#Eval("reg_Additional4")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDAdditional5" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDAdditional5" runat="server" Text="Additional5"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDAdditional5" runat="server" Text='<%#Eval("reg_Additional5")%>'></asp:Label></td>
                                            </tr>

                                            <tr id="trDVName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVName" runat="server" Text="Visitor Name"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVName" runat="server" Text='<%#Eval("reg_vName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDVDOB" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVDOB" runat="server" Text="Visitor DOB"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVDOB" runat="server" Text='<%#Eval("reg_vDOB")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDVPass" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVPass" runat="server" Text="Visitor Passport No."></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVPass" runat="server" Text='<%#Eval("reg_vPassno")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDVPassIssueDate" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVPassIssueDate" runat="server" Text="Visitor Passport Issue Date"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVPassIssueDate" runat="server" Text='<%#Eval("reg_vIssueDate")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDVPassExpiry" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVPassExpiry" runat="server" Text="Visitor Passport Expiry"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVPassExpiry" runat="server" Text='<%#Eval("reg_vPassexpiry")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDVPassEmbarkation" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVPassEmbarkation" runat="server" Text="Port of Embarkation"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVPassEmbarkation" runat="server" Text='<%#Eval("reg_vEmbarkation")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDVPassArrivalDate" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVPassArrivalDate" runat="server" Text="Visitor Arrival Date"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVPassArrivalDate" runat="server" Text='<%#Eval("reg_vArrivalDate")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDVCountry" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDVCountry" runat="server" Text="Visitor Country"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDVCountry" runat="server" Text='<%# bindCountry(Eval("reg_vCountry").ToString())%>'></asp:Label></td>
                                            </tr>

                                            <tr id="trDUDF_CName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CName" runat="server" Text="UDFC Name"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CName" runat="server" Text='<%#Eval("UDF_CName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_DelegateType" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_DelegateType" runat="server" Text="UDF Delegate Type"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_DelegateType" runat="server" Text='<%#Eval("UDF_DelegateType")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_ProfCategory" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_ProfCategory" runat="server" Text="UDF Prof Category"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_ProfCategory" runat="server"><%#Eval("UDF_ProfCategory")%> <%#Eval("UDF_ProfCategoryOther")%></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_CPcode" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CPcode" runat="server" Text="UDFC Postal Code"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CPcode" runat="server" Text='<%#Eval("UDF_CPcode")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_CLDepartment" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CLDepartment" runat="server" Text="UDFCL Department"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CLDepartment" runat="server" Text='<%#Eval("UDF_CLDepartment")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_CAddress" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CAddress" runat="server" Text="UDFC Address"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CAddress" runat="server" Text='<%#Eval("UDF_CAddress")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_CLCompany" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CLCompany" runat="server" Text="UDFCL Company"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CLCompany" runat="server"><%#Eval("UDF_CLCompany")%><%#Eval("UDF_CLCompanyOther")%></asp:Label></td>
                                            </tr>
                                            <tr id="trDUDF_CCountry" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDUDF_CCountry" runat="server" Text="UDFC Country"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDUDF_CCountry" runat="server" Text='<%# bindCountry(Eval("UDF_CCountry").ToString())%>'></asp:Label></td>
                                            </tr>

                                            <tr id="trDSupName" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDSupName" runat="server" Text="Supervisor Name"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDSupName" runat="server" Text='<%#Eval("reg_SupervisorName")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDSupDesignation" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDSupDesignation" runat="server" Text="Supervisor Designation"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDSupDesignation" runat="server" Text='<%#Eval("reg_SupervisorDesignation")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDSupContact" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDSupContact" runat="server" Text="Supervisor Contact"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDSupContact" runat="server" Text='<%#Eval("reg_SupervisorContact")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trDSupEmail" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDSupEmail" runat="server" Text="Supervisor Email"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDSupEmail" runat="server" Text='<%#Eval("reg_SupervisorEmail")%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trPromoCode" runat="server">
                                                <td class="confrmTDStyle"><asp:Label ID="lblDPromoCode" runat="server" Text="Invitation Code"></asp:Label></td>
                                                <td class="confrmTDFullColomStyle">:</td>
                                                <td><asp:Label ID="_lblDPromoCode" runat="server" Text='<%# bindPromoCode(Eval("RegGroupID").ToString())%>'></asp:Label></td>
                                            </tr>
                                            <tr id="trEdit" runat="server" visible="false">
                                                <td class="confrmTDStyle"></td>
                                                <td class="confrmTDFullColomStyle"></td>
                                                <td><asp:Button ID="btnMemberDelegateEdit" runat="server" Text="Edit" CssClass="btn MainButton"
                                                    CommandName="EditMemberDelegate" CommandArgument='<%# Eval("regNo") %>' OnClick="btnMemberDelegateEdit_Click"
                                                    CausesValidation="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>

                            <div style="overflow-x:auto;overflow-y:hidden;display:none;" class="table-responsive">
                                <table style="width:100%;" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <%-- <tr>
                                                <th><asp:Label ID="lblRegistrationID" runat="server" Text="Registration ID"></asp:Label></th>
                                                <th>:</th>
                                                <th><asp:Label ID="_lblRegno" runat="server" Text="Label"></asp:Label></th>
                                            </tr>--%>
                                            <th>No.</th>

                                            <th id="trSalutation" runat="server" scope="row">
                                                <asp:Label ID="lblSal" runat="server" Text="Title"></asp:Label>
                                            </th>
                                            <th id="trFName" runat="server" scope="row">
                                                <asp:Label ID="lblFName" runat="server" Text="First Name"></asp:Label>
                                            </th>
                                            <th id="trLName" runat="server" scope="row">
                                                <asp:Label ID="lblLName" runat="server" Text="Surname"></asp:Label>
                                            </th>
                                            <th id="trOName" runat="server" scope="row">
                                                <asp:Label ID="lblOName" runat="server" Text="Other Name"></asp:Label>
                                            </th>
                                            <th id="trPassno" runat="server" scope="row">
                                                <asp:Label ID="lblPassno" runat="server" Text="NRIC/Passport No."></asp:Label>
                                            </th>
                                            <th id="trIsReg" runat="server" scope="row">
                                                <asp:Label ID="lblIsReg" runat="server" Text="Are you a Singapore registered doctor/nurse/pharmacist?"></asp:Label>
                                            </th>
                                            <th id="trRegSpecific" runat="server" scope="row">
                                                <asp:Label ID="lblRegSpecific" runat="server" Text="MCR/SNB/PRN"></asp:Label>
                                            </th>
                                            <th id="trIDNo" runat="server" scope="row">
                                                <asp:Label ID="lblIDNo" runat="server" Text="MCR/SNB/PRN No."></asp:Label>
                                            </th>
                                            <th id="trDesignation" runat="server" scope="row">
                                                <asp:Label ID="lblDesignation" runat="server" Text="Designation"></asp:Label>
                                            </th>
                                            <th id="trProfession" runat="server" scope="row">
                                                <asp:Label ID="lblProfession" runat="server" Text="Profession"></asp:Label>
                                            </th>
                                            <th id="trOrg" runat="server" scope="row">
                                                <asp:Label ID="lblOrg" runat="server" Text="Organization"></asp:Label>
                                            </th>
                                            <th id="trInstitution" runat="server" scope="row">
                                                <asp:Label ID="lblInstitution" runat="server" Text="Institution"></asp:Label>
                                            </th>
                                            <th id="trDept" runat="server" scope="row">
                                                <asp:Label ID="lblDept" runat="server" Text="Department"></asp:Label>
                                            </th>
                                            <th id="trAddress1" runat="server" scope="row">
                                                <asp:Label ID="lblAddress1" runat="server" Text="Address1"></asp:Label>
                                            </th>
                                            <th id="trAddress2" runat="server" scope="row">
                                                <asp:Label ID="lblAddress2" runat="server" Text="Address2"></asp:Label>
                                            </th>
                                            <th id="trAddress3" runat="server" scope="row">
                                                <asp:Label ID="lblAddress3" runat="server" Text="Address3"></asp:Label>
                                            </th>
                                            <th id="trAddress4" runat="server" scope="row">
                                                <asp:Label ID="lblAddress4" runat="server" Text="Address4"></asp:Label>
                                            </th>
                                            <th id="trCity" runat="server" scope="row">
                                                <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                                            </th>
                                            <th id="trState" runat="server" scope="row">
                                                <asp:Label ID="lblState" runat="server" Text="State"></asp:Label>
                                            </th>
                                            <th id="trCountry" runat="server" scope="row">
                                                <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                                            </th>
                                            <th id="trPostal" runat="server" scope="row">
                                                <asp:Label ID="lblPostal" runat="server" Text="Postal Code"></asp:Label>
                                            </th>
                                            <th id="trRCountry" runat="server" scope="row">
                                                <asp:Label ID="lblRCountry" runat="server" Text="RCountry"></asp:Label>
                                            </th>
                                            <th id="trTel" runat="server" scope="row">
                                                <asp:Label ID="lblTel" runat="server" Text="Telephone"></asp:Label>
                                            </th>
                                            <th id="trMobile" runat="server" scope="row">
                                                <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                                            </th>
                                            <th id="trFax" runat="server" scope="row">
                                                <asp:Label ID="lblFax" runat="server" Text="Fax"></asp:Label>
                                            </th>
                                            <th id="trEmail" runat="server" scope="row">
                                                <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                                            </th>
                                                <th id="trAffiliation" runat="server" scope="row">
                                                <asp:Label ID="lblAffiliation" runat="server" Text="Affiliation"></asp:Label>
                                            </th>
                                            <th id="trDietary" runat="server" scope="row">
                                                <asp:Label ID="lblDietary" runat="server" Text="Dietary"></asp:Label>
                                            </th>
                                            <th id="trNationality" runat="server" scope="row">
                                                <asp:Label ID="lblNationality" runat="server" Text="Nationality"></asp:Label>
                                            </th>
                                            <th id="trAge" runat="server" scope="row">
                                                <asp:Label ID="lblAge" runat="server" Text="Age"></asp:Label>
                                            </th>
                                            <th id="trDOB" runat="server" scope="row">
                                                <asp:Label ID="lblDOB" runat="server" Text="Date of Birth"></asp:Label>
                                            </th>
                                            <th id="trGender" runat="server" scope="row">
                                                <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                            </th>
                                            <th id="trMembershipNo" runat="server" scope="row">
                                                <asp:Label ID="lblMembershipNo" runat="server" Text="MembershipNo"></asp:Label>
                                            </th>
                                            <th id="trAdditional4" runat="server" scope="row">
                                                <asp:Label ID="lblAdditional4" runat="server" Text="Additional4"></asp:Label>
                                            </th>
                                            <th id="trAdditional5" runat="server" scope="row">
                                                <asp:Label ID="lblAdditional5" runat="server" Text="Additional5"></asp:Label>
                                            </th>

                                            <th id="trVName" runat="server" scope="row">
                                                <asp:Label ID="lblVName" runat="server" Text="Visitor Name"></asp:Label>
                                            </th>
                                            <th id="trVDOB" runat="server" scope="row">
                                                <asp:Label ID="lblVDOB" runat="server" Text="Visitor DOB"></asp:Label>
                                            </th>
                                            <th id="trVPass" runat="server" scope="row">
                                                <asp:Label ID="lblVPass" runat="server" Text="Visitor Passport No."></asp:Label>
                                            </th>
                                            <th id="trVPassIssueDate" runat="server" scope="row">
                                                <asp:Label ID="lblVPassIssueDate" runat="server" Text="Visitor Passport Issue Date"></asp:Label>
                                            </th>
                                            <th id="trVPassExpiry" runat="server" scope="row">
                                                <asp:Label ID="lblVPassExpiry" runat="server" Text="Visitor Passport Expiry"></asp:Label>
                                            </th>
                                            <th id="trVEmbarkation" runat="server" scope="row">
                                                <asp:Label ID="lblVEmbarkation" runat="server" Text="Port of Embarkation"></asp:Label>
                                            </th>
                                            <th id="trVArrivalDate" runat="server" scope="row">
                                                <asp:Label ID="lblVArrivalDate" runat="server" Text="Visitor Arrival Date"></asp:Label>
                                            </th>
                                            <th id="trVCountry" runat="server" scope="row">
                                                <asp:Label ID="lblVCountry" runat="server" Text="Visitor Country"></asp:Label>
                                            </th>

                                            <th id="trUDF_CName" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CName" runat="server" Text="UDFC Name"></asp:Label>
                                            </th>
                                            <th id="trUDF_DelegateType" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_DelegateType" runat="server" Text="UDF Delegate Type"></asp:Label>
                                            </th>
                                            <th id="trUDF_ProfCategory" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_ProfCategory" runat="server" Text="UDF Prof Category"></asp:Label>
                                            </th>
                                            <th id="trUDF_CPcode" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CPcode" runat="server" Text="UDFC Postal Code"></asp:Label>
                                            </th>
                                            <th id="trUDF_CLDepartment" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CLDepartment" runat="server" Text="UDFCL Department"></asp:Label>
                                            </th>
                                            <th id="trUDF_CAddress" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CAddress" runat="server" Text="UDFC Address"></asp:Label>
                                            </th>
                                            <th id="trUDF_CLCompany" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CLCompany" runat="server" Text="UDFCL Company"></asp:Label>
                                            </th>
                                            <th id="trUDF_CCountry" runat="server" scope="row">
                                                <asp:Label ID="lblUDF_CCountry" runat="server" Text="UDFC Country"></asp:Label>
                                            </th>

                                            <th id="trSupName" runat="server" scope="row">
                                                <asp:Label ID="lblSupName" runat="server" Text="Supervisor Name"></asp:Label>
                                            </th>
                                            <th id="trSupDesignation" runat="server" scope="row">
                                                <asp:Label ID="lblSupDesignation" runat="server" Text="Supervisor Designation"></asp:Label>
                                            </th>
                                            <th id="trSupContact" runat="server" scope="row">
                                                <asp:Label ID="lblSupContact" runat="server" Text="Supervisor Contact"></asp:Label>
                                            </th>
                                            <th id="trSupEmail" runat="server" scope="row">
                                                <asp:Label ID="lblSupEmail" runat="server" Text="Supervisor Email"></asp:Label>
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <asp:Repeater ID="rptItem" runat="server" OnItemDataBound="rptitemdatabound">
                                            <ItemTemplate>
                                                <%--<table>--%>
                                                <%--<tr>
                                                    <td><asp:Label ID="lblRegistrationID" runat="server" Text="Registration ID"></asp:Label></td>
                                                    <td>:</td>
                                                    <td><asp:Label ID="_lblRegno" runat="server" Text="Label"></asp:Label></td>
                                                </tr>--%>
                                                <tr>
                                                    <td><%#Container.ItemIndex+1 %></td>

                                                    <td id="tdSalutation" runat="server"><asp:Label ID="_lblsal" runat="server"><%# bindSalutation(Eval("reg_Salutation").ToString(), Eval("reg_SalutationOthers").ToString()) %></asp:Label></td>

                                                    <td id="tdFName" runat="server"><asp:Label ID="_lblFName" runat="server" Text='<%#Eval("reg_FName")%>'></asp:Label></td>

                                                    <td id="tdLName" runat="server"><asp:Label ID="_lblLName" runat="server" Text='<%#Eval("reg_LName")%>'></asp:Label></td>

                                                    <td id="tdOName" runat="server"><asp:Label ID="_lblOName" runat="server" Text='<%#Eval("reg_OName")%>'></asp:Label></td>

                                                    <td id="tdPassno" runat="server"><asp:Label ID="_lblPassno" runat="server" Text='<%#Eval("reg_PassNo")%>'></asp:Label></td>

                                                    <td id="tdIsReg" runat="server"><asp:Label ID="_lblIsReg" runat="server"><%#Eval("reg_isReg") != null ? (Eval("reg_isReg").ToString() == "1" ? "Yes" : "No") : "No"%></asp:Label></td>

                                                    <td id="tdRegSpecific" runat="server"><asp:Label ID="_lblRegSpecific" runat="server" Text='<%#Eval("reg_sgregistered")%>'></asp:Label></td>

                                                    <td id="tdIDNo" runat="server"><asp:Label ID="_lblIDNo" runat="server" Text='<%#Eval("reg_IDno")%>'></asp:Label></td>

                                                    <td id="tdDesignation" runat="server"><asp:Label ID="_lblDesignation" runat="server" Text='<%#Eval("reg_Designation")%>'></asp:Label></td>

                                                    <td id="tdProfession" runat="server"><asp:Label ID="_lblProfession" runat="server" Text='<%# bindProfession(Eval("reg_Profession").ToString())%>'></asp:Label></td>

                                                    <td id="tdOrg" runat="server"><asp:Label ID="_lblOrg" runat="server" Text='<%# bindOrganisation(Eval("reg_Organization").ToString())%>'></asp:Label></td>

                                                    <td id="tdInstitution" runat="server"><asp:Label ID="_lblInstitution" runat="server" Text='<%# bindInstitution(Eval("reg_Institution").ToString(), Eval("reg_otherInstitution").ToString())%>'></asp:Label></td>

                                                    <td id="tdDept" runat="server"><asp:Label ID="_lblDept" runat="server" Text='<%# bindDepartment(Eval("reg_Department").ToString())%>'></asp:Label></td>

                                                    <td id="tdAddress1" runat="server"><asp:Label ID="_lblAddress1" runat="server" Text='<%#Eval("reg_Address1")%>'></asp:Label></td>

                                                    <td id="tdAddress2" runat="server"><asp:Label ID="_lblAddress2" runat="server" Text='<%#Eval("reg_Address2")%>'></asp:Label></td>

                                                    <td id="tdAddress3" runat="server"><asp:Label ID="_lblAddress3" runat="server" Text='<%#Eval("reg_Address3")%>'></asp:Label></td>

                                                    <td id="tdAddress4" runat="server"><asp:Label ID="_lblAddress4" runat="server" Text='<%#Eval("reg_Address4")%>'></asp:Label></td>

                                                    <td id="tdCity" runat="server"><asp:Label ID="_lblCity" runat="server" Text='<%#Eval("reg_City")%>'></asp:Label></td>

                                                    <td id="tdState" runat="server"><asp:Label ID="_lblState" runat="server" Text='<%#Eval("reg_State")%>'></asp:Label></td>

                                                    <td id="tdCountry" runat="server"><asp:Label ID="_lblCountry" runat="server" Text='<%# bindCountry(Eval("reg_Country").ToString())%>'></asp:Label></td>

                                                    <td id="tdPostal" runat="server"><asp:Label ID="_lblPostal" runat="server" Text='<%#Eval("reg_PostalCode")%>'></asp:Label></td>

                                                    <td id="tdRCountry" runat="server"><asp:Label ID="_lblRCountry" runat="server" Text='<%# bindCountry(Eval("reg_RCountry").ToString())%>'></asp:Label></td>

                                                    <td id="tdTel" runat="server"><asp:Label ID="_lblTel" runat="server"><%# bindPhoneNo(Eval("reg_Telcc").ToString(), Eval("reg_Telac").ToString(), Eval("reg_Tel").ToString(), "Tel")%></asp:Label></td>

                                                    <td id="tdMobile" runat="server"><asp:Label ID="_lblMobile" runat="server"><%# bindPhoneNo(Eval("reg_Mobcc").ToString(), Eval("reg_Mobac").ToString(), Eval("reg_Mobile").ToString(), "Mob")%></asp:Label></td>

                                                    <td id="tdFax" runat="server"><asp:Label ID="_lblFax" runat="server"><%# bindPhoneNo(Eval("reg_Faxcc").ToString(), Eval("reg_Faxac").ToString(), Eval("reg_Fax").ToString(), "Fax")%></asp:Label></td>

                                                    <td id="tdEmail" runat="server"><asp:Label ID="_lblEmail" runat="server" Text='<%#Eval("reg_Email")%>'></asp:Label></td>

                                                    <td id="tdAffiliation" runat="server"><asp:Label ID="_lblAffiliation" runat="server" Text='<%# bindAffiliation(Eval("reg_Affiliation").ToString())%>'></asp:Label></td>

                                                    <td id="tdDietary" runat="server"><asp:Label ID="_lblDietary" runat="server" Text='<%# bindDietary(Eval("reg_Dietary").ToString())%>'></asp:Label></td>

                                                    <td id="tdNationality" runat="server"><asp:Label ID="_lblNationality" runat="server" Text='<%#Eval("reg_Nationality")%>'></asp:Label></td>

                                                    <td id="tdAge" runat="server"><asp:Label ID="_lblAge" runat="server" Text='<%#Eval("reg_Age")%>'></asp:Label></td>

                                                    <td id="tdDOB" runat="server"><asp:Label ID="_lblDOB" runat="server" Text='<%#getDate(Eval("reg_DOB").ToString())%>'></asp:Label></td>

                                                    <td id="tdGender" runat="server"><asp:Label ID="_lblGender" runat="server" Text='<%#Eval("reg_Gender")%>'></asp:Label></td>

                                                    <td id="tdMembershipNo" runat="server"><asp:Label ID="_lblMembershipNo" runat="server" Text='<%#Eval("reg_Membershipno")%>'></asp:Label></td>

                                                    <td id="tdAdditional4" runat="server"><asp:Label ID="_lblAdditional4" runat="server" Text='<%#Eval("reg_Additional4")%>'></asp:Label></td>

                                                    <td id="tdAdditional5" runat="server"><asp:Label ID="_lblAdditional5" runat="server" Text='<%#Eval("reg_Additional5")%>'></asp:Label></td>

                                                    <td id="tdVName" runat="server"><asp:Label ID="_lblVName" runat="server" Text='<%#Eval("reg_vName")%>'></asp:Label></td>

                                                    <td id="tdVDOB" runat="server"><asp:Label ID="_lblVDOB" runat="server" Text='<%#Eval("reg_vDOB")%>'></asp:Label></td>

                                                    <td id="tdVPass" runat="server"><asp:Label ID="_lblVPass" runat="server" Text='<%#Eval("reg_vPassno")%>'></asp:Label></td>

                                                    <td id="tdVPassExpiry" runat="server"><asp:Label ID="_lblVPassExpiry" runat="server" Text='<%#Eval("reg_vPassexpiry")%>'></asp:Label></td>

                                                    <td id="tdVCountry" runat="server"><asp:Label ID="_lblVCountry" runat="server" Text='<%# bindCountry(Eval("reg_vCountry").ToString())%>'></asp:Label></td>

                                                    <td id="tdUDF_CName" runat="server"><asp:Label ID="_lblUDF_CName" runat="server" Text='<%#Eval("UDF_CName")%>'></asp:Label></td>

                                                    <td id="tdUDF_DelegateType" runat="server"><asp:Label ID="_lblUDF_DelegateType" runat="server" Text='<%#Eval("UDF_DelegateType")%>'></asp:Label></td>

                                                    <td id="tdUDF_ProfCategory" runat="server"><asp:Label ID="_lblUDF_ProfCategory" runat="server"><%#Eval("UDF_ProfCategory")%> <%#Eval("UDF_ProfCategoryOther")%></asp:Label></td>

                                                    <td id="tdUDF_CPcode" runat="server"><asp:Label ID="_lblUDF_CPcode" runat="server" Text='<%#Eval("UDF_CPcode")%>'></asp:Label></td>

                                                    <td id="tdUDF_CLDepartment" runat="server"><asp:Label ID="_lblUDF_CLDepartment" runat="server" Text='<%#Eval("UDF_CLDepartment")%>'></asp:Label></td>

                                                    <td id="tdUDF_CAddress" runat="server"><asp:Label ID="_lblUDF_CAddress" runat="server" Text='<%#Eval("UDF_CAddress")%>'></asp:Label></td>

                                                    <td id="tdUDF_CLCompany" runat="server"><asp:Label ID="_lblUDF_CLCompany" runat="server"><%#Eval("UDF_CLCompany")%><%#Eval("UDF_CLCompanyOther")%></asp:Label></td>

                                                    <td id="tdUDF_CCountry" runat="server"><asp:Label ID="_lblUDF_CCountry" runat="server" Text='<%# bindCountry(Eval("UDF_CCountry").ToString())%>'></asp:Label></td>

                                                    <td id="tdSupName" runat="server"><asp:Label ID="_lblSupName" runat="server" Text='<%#Eval("reg_SupervisorName")%>'></asp:Label></td>

                                                    <td id="tdSupDesignation" runat="server"><asp:Label ID="_lblSupDesignation" runat="server" Text='<%#Eval("reg_SupervisorDesignation")%>'></asp:Label></td>

                                                    <td id="tdSupContact" runat="server"><asp:Label ID="_lblSupContact" runat="server" Text='<%#Eval("reg_SupervisorContact")%>'></asp:Label></td>

                                                    <td id="tdSupEmail" runat="server"><asp:Label ID="_lblSupEmail" runat="server" Text='<%#Eval("reg_SupervisorEmail")%>'></asp:Label></td>
                                                </tr>
                                            <%--</table>--%>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                            </div>
                        </div>

                        <table width="100%">
                            <tr>
                                <td colspan="3" style="border-top:0px solid #C8C8C8;">&nbsp;</td>
                            </tr>
                        </table>
                    </div>

                    <br />
                    <div id="rcontent" runat="server" >
                        <h3 class="ConfrmHeader"><asp:Label ID="lblCongressSelectionHeader" runat="server" Text="Congress Selection"></asp:Label></h3>
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10  col-xs-12 ConfItemPanel ">
                                <asp:Label runat="server" ID="lblShowOrderList"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10  col-xs-12 text-right">
                                <div id="divPaymentSummary" runat="server"></div>
                                <%--<table class="table" style="font-size:14px;">
                                    <tr>
                                        <td colspan="3" width="70%">Sub Total:</td>
                                        <td width="30%">
                                            <asp:Label runat="server" ID="lblSubTotalCurr" Text="SGD"></asp:Label>  <asp:Label runat="server" ID="lblSubTotal" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trDiscount" runat="server" visible="false">
                                        <td colspan="3" width="70%"><asp:Label ID="lblDiscountText" runat="server" Text="Discount"></asp:Label></td>
                                        <td width="30%">
                                            <asp:Label runat="server" ID="lblDiscountCurr" Text="SGD"></asp:Label>  <asp:Label runat="server" ID="lblDiscountPrice" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trAdminFee" runat="server" visible="false">
                                        <td colspan="3" width="70%"><asp:Label ID="lblAdminFeeText" runat="server" Text="4"></asp:Label>% Credit Card (Admin Fee):</td>
                                        <td width="30%">
                                            <asp:Label runat="server" ID="lblAdminFeeCurr" Text="SGD"></asp:Label>  <asp:Label runat="server" ID="lblAdminFee" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trTTAdminFee" runat="server" visible="false">
                                        <td colspan="3" width="70%"><asp:Label ID="lblTTAdminFeeText" runat="server" Text="25"></asp:Label> Bank Transaction Charges (Telegraphic Transfer):</td>
                                        <td width="30%">
                                            <asp:Label runat="server" ID="lblTTCurr" Text="SGD"></asp:Label>  <asp:Label runat="server" ID="lblTTAdminFee" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                     <tr id="trGST" runat="server" visible="false">
                                        <td colspan="3" width="70%"><asp:Label ID="lblGSTFeeText" runat="server" Text="7"></asp:Label>% Goods & Services Tax (GST):</td>
                                        <td width="30%">
                                            <asp:Label runat="server" ID="lblGSTCurr" Text="SGD"></asp:Label>  <asp:Label runat="server" ID="lblGstFee" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" width="70%">Grand Total:</td>
                                        <td width="30%">
                                            <asp:Label runat="server" ID="lblGrandCurr" Text="SGD"></asp:Label>
                                            <asp:Label runat="server" ID="lblGrandTotal" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                </table>--%>
                                <asp:Label runat="server" ID="lblGrandTotal" Text="0.00" Visible="false"></asp:Label>
                            </div>
                        </div>
                        <asp:Panel runat="server" ID="PanelPartial" Visible="false">
                            <br />
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <asp:Label ID="lblPartialAmount" runat="server" CssClass="form-control-label" Font-Bold="true" Text="Partial Amount : "></asp:Label>
                                </div>
                                <div class="col-md-4">
                                    <asp:TextBox runat="server" ID="txtPartialAmount" CssClass="form-control"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftPartialAmount" runat="server" TargetControlID="txtPartialAmount" FilterType="Numbers, Custom" ValidChars="." />
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel runat="server" Visible="false">
                        <br />
 
                        <table width="100%">
                            <tr>
                                <td colspan="3" style="border-top:0px solid #C8C8C8;">&nbsp;</td>
                            </tr>
                        </table>
 
                        <div class="clear"></div>
                        <div class="margtop">
                            <h3>&nbsp;</h3>
                            <p> <asp:Label ID="lblmethod" runat="server" Text="Label" Visible="False"></asp:Label></p>
                            <asp:HiddenField ID="hfpaymethod" runat="server" Value="0" />
                        </div>
                            </asp:Panel>
                    </div>

                    <div id="divTerm" runat="server" visible="false">
                        <div class="clear"></div>
                        <table width="100%">
                            <tr>
                                <td colspan="3" style="border-top:1px solid #C8C8C8;">&nbsp;</td>
                            </tr>
                        </table>
                        <asp:Label ID="lblTerms" runat="server"></asp:Label>
                        <br />
                    </div>

                    <div id="divPaymentMethod" runat="server" visible="false">
                        <br />
                        <h3 class="ConfrmHeader" runat="server" id="lblPaymentModeTitle" >Mode of Payment</h3>
                        <div>
                            <table>
                                <tr id="trCreditCard" runat="server" visible="false">
                                    <td valign="top">
                                        <asp:RadioButton ID="rbCreditCard" runat="server" GroupName="Method" OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" Text="Credit Card" />
                                        <br />
                                        <asp:Label ID="lblCreditCard" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trCreditManual" runat="server" visible="false">
                                    <td valign="top">
                                        <asp:RadioButton ID="rbCreditManual" runat="server" GroupName="Method" OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" Text="Credit Card" />
                                        <br />
                                        <asp:Label ID="lblCreditCardManual" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trTT" runat="server" visible="false">
                                    <td valign="top">
                                        <asp:RadioButton ID="rbTT" runat="server" GroupName="Method" OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" Text="Telegraphic Transfer" />
                                        <br />
                                        <asp:Label ID="lblTT" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trWaived" runat="server" visible="false">
                                    <td valign="top">
                                        <asp:RadioButton ID="rbWaived" runat="server" GroupName="Method" OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" Text="Waived" />
                                        <br />
                                        <asp:Label ID="lblWaived" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trCheque" runat="server" visible="false">
                                    <td valign="top">
                                        <br /><asp:RadioButton ID="rbCheque" runat="server" GroupName="Method" OnCheckedChanged="radioPaymentsChange" AutoPostBack="true" Text="Local Cheque" />
                                        <br />
                                        <asp:Label ID="lblCheque" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row" id="divBillingAddress" runat="server" visible="false">
                        <div class="col-md-12">
                            <asp:Panel runat="server" ID="PanelBillingAddress">
                                <div class="row" style="margin-left: 20px; margin-top: 20px;">
                                    <h4>Billing Address</h4>
                                    <%--<asp:TextBox runat="server" ID="txtFName" CssClass="form-control" Visible="false"></asp:TextBox>
                                    <asp:TextBox runat="server" ID="txtLName" CssClass="form-control" Visible="false"></asp:TextBox>--%>
                                    <div class="form-group row">
                                        <div class="col-md-4">Billing Company<span style="color: #f11616">*</span></div>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" ID="txtBillingCompany" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvBillingCompany" ControlToValidate="txtBillingCompany" 
                                                Text="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">Billing Contact Person Name<span style="color: #f11616">*</span></div>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" ID="txtBillingName" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvBillingName" ControlToValidate="txtBillingName"
                                                Text="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">Billing Address Line 1<span style="color: #f11616">*</span></div>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" ID="txtBillingAddress" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvBillingAddress" ControlToValidate="txtBillingAddress"
                                                Text="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4"> Billing Address Line 2</div>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" ID="txtBillingAddress2" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                        
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">Billing City<span style="color: #f11616">*</span></div>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" ID="txtBillingCity" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvBillingCity" ControlToValidate="txtBillingCity"
                                                Text="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">Billing State/Province</div>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" ID="txtBillingState" CssClass="form-control"></asp:TextBox>
                                        </div>
                                   
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">Billing Postal Code</div>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" ID="txtBillingPostal" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">Billing Country<span style="color: #f11616">*</span></div>
                                        <div class="col-md-6">
                                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" AutoPostBack="true">
                                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:CompareValidator runat="server" ID="rfvCountry" ControlToValidate="ddlCountry"
                                                ValueToCompare="0" Operator="NotEqual" Type="Integer"
                                                Text="*Required" ForeColor="Red"></asp:CompareValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">Telephone<span style="color: #f11616">*</span></div>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" ID="txtBillingTel" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvBillingTel" ControlToValidate="txtBillingTel"
                                                Text="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row" >
                                        <div class="col-md-4"><span style="color: #f11616"></span>Fax</div>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" ID="txtBillingFax" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                        
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">Billing Contact Person Email<span style="color: #f11616">*</span></div>
                                        <div class="col-md-6">
                                            <asp:TextBox runat="server" ID="txtBillingEmail" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                            <asp:RequiredFieldValidator runat="server" ID="rfvBillingEmail" ControlToValidate="txtBillingEmail"
                                                Text="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>

                    <div id="divBRFooter" runat="server" visible="true">
                        <br /><br />
                    </div>
                    <div id="divFoodJapanMandatoryMsg" runat="server" class="form-group row" style="padding-left:20px;" visible="false">
	                    <div class="col-md-12">
		                    <span>Fields marked with an <span style="color:red;">*</span> are mandatory.<br />
		                    <span style="color:red;">＊</span>印の項目は必須項目です。全てご入力ください。</span>
	                    </div>
                        <br />
                    </div>

                    <div id="divFTRCHK1" runat="server" visible="false">
                        <div class="clear"></div>
                        <asp:CheckBoxList ID="chkFTRCHK1" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                        <asp:Label ID="lblErrFTRCHK1" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                        <br />
                        <br />
                    </div>
                    <div id="divFTR1" runat="server" visible="false">
                        <div class="clear"></div>
                        <asp:Label ID="lblFTR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                    <div id="divFTR2" runat="server" visible="false">
                        <div class="clear"></div>
                        <asp:Label ID="lblFTR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                    <div id="divFTRCHK" runat="server" visible="false">
                        <div class="clear"></div>
                        <asp:CheckBoxList ID="chkFTRCHK" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                        <%--<asp:CheckBox ID="chkFTRCHK" runat="server" />&nbsp;&nbsp;<asp:Label ID="lblFTRCHK" runat="server"></asp:Label>
                        <asp:Label ID="lblFTRCHKIsSkip" runat="server" Visible="false" Text="0"></asp:Label>--%>
                        <asp:Label ID="lblErrFTRCHK" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                    </div>

                    <div class="form-group">
                        <div class="form-group container" style="padding-top:80px;">
                            <div class="col-lg-offset-3 col-lg-3 center-block" id="divEditMainDelegate" runat="server">
                                <asp:Button runat="server" ID="btnEditMainDelegate" CssClass="btn btn-block MainButton" Visible="false"
                                     CommandName="EditMainDelegate"  OnClick="btnEditMainDelegate_Click" Text="Edit" CausesValidation="false" />
                                <asp:Button runat="server" ID="btnPrev" CssClass="btn btn-block MainButton" Visible="false"
                                     OnClick="PrevClick" Text="Back" CausesValidation="false" />
                            </div>
                            <%--<div class="col-lg-offset-4 col-lg-3 col-sm-offset-4 col-sm-3 center-block" > --%>
                            <div class="col-lg-3 center-block" id="divSubmit" runat="server"> 
						          <asp:Button ID="btnNext" runat="server" Text="Submit" CssClass="btn MainButton btn-block"
                                    OnClick="NextClick" CausesValidation = "true"    OnClientClick="gtag_report_conversion();"/>
                            </div>
                            <div class="col-lg-3 center-block" id="divPrintPage" runat="server" visible="false">
						          <asp:Button ID="btnPrintPage" runat="server" Text="Print" CssClass="btn MainButton btn-block" 
                                        OnClientClick="return PrintDiv();" 
                                        CausesValidation = "false" /><%--OnClick="btnPrintPage_Click" --%>
                            </div>
                        </div>
                    </div>
                    <div style="display:none;">
                        <asp:TextBox runat="server" Visible="false" Text="0" ID="regCountrySameWithShowCountry" ></asp:TextBox>
                    </div>
                    <%--<p id="stepcommand" class="pull-right" style="padding-top:40px;">
                        <asp:Button ID="btnPrev" runat="server" Text="Back" CssClass="btn btn-primary" OnClick="PrevClick" Visible="true"
                            Height="47px" Width="131px" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    </p>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>