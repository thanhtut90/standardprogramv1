﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegClosed : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["SHW"] != null)
            {
                string showID = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

                InitSiteSettings(showID);
                LoadPageSetting();

                string closedmsg = fn.GetDataByCommand("Select settings_value From tb_site_settings Where ShowID='" + showID + "' And settings_name='RegClosedMessage'", "settings_value");
                if(!string.IsNullOrEmpty(closedmsg) && closedmsg != "0")
                {
                    lblClosedMessage.Text = HttpUtility.HtmlDecode(closedmsg);
                }
            }
            //else
            //{
            //    Response.Redirect("404.aspx");
            //}
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        SiteBanner.ImageUrl = sSetting.SiteBanner;
        Global.PageTitle = sSetting.SiteTitle;
        Global.Copyright = sSetting.SiteFooter;
        Global.PageBanner = sSetting.SiteBanner;

        if (!string.IsNullOrEmpty(sSetting.SiteCssBandlePath))
            BundleRefence.Path = "~/Content/" + sSetting.SiteCssBandlePath + "/";

        //Show hide Banner
        ShowControler showControler = new ShowControler(fn);
        Show sh = showControler.GetShow(showid);
        if (sh.HideShowBanner == "1")
            SiteBanner.Visible = false;
    }
    private void LoadPageSetting()
    {
        //FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        //FlowControler flwObj = new FlowControler(fn, urlQuery);
        //SetPageSettting(flwObj.CurrIndexTiTle);
    }

    public void SetPageSettting(string title)
    {
    }
    #endregion
}