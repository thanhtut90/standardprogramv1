﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RegistrationOnsite.master" AutoEventWireup="true" CodeFile="RegDelegateOnsite.aspx.cs" Inherits="RegDelegateOnsite" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .red {
            color: red;
        }
    </style>

    <script type="text/javascript">
        function validateDate(elementRef) {
            var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

            var dateValue = elementRef.value;

            if (dateValue.length != 10) {
                alert('Entry must be in the format dd/mm/yyyy.');

                return false;
            }

            // mm/dd/yyyy format... 
            var valueArray = dateValue.split('/');

            if (valueArray.length != 3) {
                alert('Entry must be in the format dd/mm/yyyy.');

                return false;
            }

            var monthValue = parseFloat(valueArray[1]);
            var dayValue = parseFloat(valueArray[0]);
            var yearValue = parseFloat(valueArray[2]);

            if ((isNaN(monthValue) == true) || (isNaN(dayValue) == true) || (isNaN(yearValue) == true)) {
                alert('Non-numeric entry detected\nEntry must be in the format dd/mm/yyyy.');

                return false;
            }

            if (((yearValue % 4) == 0) && (((yearValue % 100) != 0) || ((yearValue % 400) == 0)))
                monthDays[1] = 29;
            else
                monthDays[1] = 28;

            if ((monthValue < 1) || (monthValue > 12)) {
                alert('Invalid month entered\nEntry must be in the format dd/mm/yyyy.');

                return false;
            }

            var monthDaysArrayIndex = monthValue - 1;
            if ((dayValue < 1) || (dayValue > monthDays[monthDaysArrayIndex])) {
                alert('Invalid day entered\nEntry must be in the format dd/mm/yyyy.');

                return false;
            }

            return true;
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divReg" runat="server" class="form-group">
                        <div class="form-group row" runat="server" id="div1">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="Label1" runat="server" CssClass="form-control-label" Text="Cateogry"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control"
                           AutoPostBack="true">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>   
                    </div>
                   
                </div>


                <div class="form-group row" runat="server" id="divSalutation">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblSalutation" runat="server" CssClass="form-control-label" Text="Salutation"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlSalutation" runat="server" CssClass="form-control"
                            OnSelectedIndexChanged="ddlSalutation_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcSal" runat="server"
                            ControlToValidate="ddlSalutation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divSalOther" visible="false">
                    <div class="col-md-3 col-md-offset-1">
                        &nbsp;
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtSalOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcSalOther" runat="server"
                            ControlToValidate="txtSalOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divFName">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblFName" runat="server" CssClass="form-control-label" Text="First Name"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcFName" runat="server"
                            ControlToValidate="txtFName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divLName" visible="false">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblLName" runat="server" CssClass="form-control-label" Text="Last Name"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtLName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div> 
                </div>

                  <div class="form-group row" runat="server" id="div2" visible="false">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="Label2" runat="server" CssClass="form-control-label" Text="Name On Badge"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtFullName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ControlToValidate="txtFullName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDesignation">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblDesignation" runat="server" CssClass="form-control-label" Text="Job Title"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2"> 
                    </div>
                </div>

                    <%--**Added on 17-Oct-2018 for Bussiness Category using Profession field for Food Japan--%>
                    <div class="form-group row" runat="server" id="divProfession">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblProfession" runat="server" CssClass="form-control-label" Text="Bussiness Category"></asp:Label>
                    </div> 
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlProfession" runat="server" CssClass="form-control"
                           AutoPostBack="true">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList> 
                    </div>
                    <div class="col-md-2"> </div>
                        </div>

                    <%--**Added on 17-Oct-2018 for Main Job Function using Organization field for Food Japan--%>
                    <div class="form-group row" runat="server" id="divOrganization">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblOrganization" runat="server" CssClass="form-control-label" Text="Main Job Function"></asp:Label>
                    </div> 
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlOrganization" runat="server" CssClass="form-control"
                           AutoPostBack="true">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList> 
                    </div>
                    <div class="col-md-2"> </div>
                        </div>

                <div class="form-group row" runat="server" id="divAddress1">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblAddress1" runat="server" CssClass="form-control-label" Text="Company"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtCompany" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2"> 
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divCountry">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblCountry" runat="server" CssClass="form-control-label" Text="Country"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcCountry" runat="server"
                            ControlToValidate="ddlCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>


                <div class="form-group row" runat="server" id="divMobile">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblMobile" runat="server" CssClass="form-control-label" Text="Mobile"></asp:Label>
                    </div>
                    <div class="col-md-5" runat="server">
                        <div class="col-xs-3" runat="server" id="divMobcc" style="padding-left: 0px;">
                            <asp:TextBox ID="txtMobcc" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobcc" runat="server"
                                TargetControlID="txtMobcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-3" runat="server" id="divMobac" style="padding-left: 0px;">
                            <asp:TextBox ID="txtMobac" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobac" runat="server"
                                TargetControlID="txtMobac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-6" runat="server" id="divMobileNo" style="padding-right: 0px; padding-left: 0px;">
                            <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobile" runat="server"
                                TargetControlID="txtMobile"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                    </div>
                
                </div>


                <div class="form-group row" runat="server" id="divEmail">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text="Email"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
            
                        <asp:RegularExpressionValidator ID="validateEmail"
                            runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                            ControlToValidate="txtEmail"
                            ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group container" style="padding-top: 20px;">

                    <asp:Panel runat="server" ID="PanelWithoutBack" Visible="true">
                        <div class="col-lg-offset-5 col-lg-3 col-sm-offset-4 col-sm-3 center-block">
                            <%--Changed on 17-Oct-2018 Text Property from Print Eng to Save --%>
                            <asp:Button runat="server" ID="btnSave" CssClass="btn MainButton btn-block" Text="Save" OnClick="btnSave_Click" />

                            <asp:Button runat="server" ID="btnSaveThai" CssClass="btn MainButton btn-block" Text="Print Thai" OnClick="btnSaveThai_Click" />
                        </div>
                    </asp:Panel>
                </div>
                <asp:TextBox runat="server" ID="txtShowID" Visible="false"></asp:TextBox>
                 <asp:TextBox runat="server" ID="txtisOnsite" Visible="false"></asp:TextBox>
            </div>

            <asp:HiddenField ID="hfRegno" runat="server" />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

