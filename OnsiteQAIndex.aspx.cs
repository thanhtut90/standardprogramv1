﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
public partial class OnsiteQAIndex : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    public class tmpDataList
    {
        public string showID { get; set; }
        public string groupID { get; set; }
        public string regno { get; set; }
        public string FlowID { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["EID"] != null)
        { 
             
            {
                string showID = "";
                string FlowID ="";
                string grpNum = string.Empty;
                string page = "";
                string step = "";

                FlowControler Flw = new FlowControler(fn);

                tmpDataList tmpList = GetByRegno(Request.QueryString["EID"].ToString());
                if (string.IsNullOrEmpty(tmpList.showID))
                    Response.Redirect("404.aspx");
                else
                {
                    page = "RegDelegate.aspx";
                    step = "1";
                    FlowID = tmpList.FlowID;
                    grpNum= tmpList.groupID;
                    string regno = tmpList.regno;
                    showID = tmpList.showID;
       
                    grpNum = cFun.EncryptValue(grpNum);
                    if (Request.QueryString["INV"] != null)
                    {
                        string invID = cFun.EncryptValue(cFun.DecryptValue(Request.QueryString["INV"].ToString()));
                        string route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step, invID);
                        Response.Redirect(route);
                    }
                    else
                    {
                        string route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                        Response.Redirect(route);
                    }
                }
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    private tmpDataList GetByRegno(string regno)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            cList.showID = "";
            cList.FlowID = "";
            string sql = "  select showid, regno, RegGroupID, reg_urlFlowID from tb_RegDelegateOnsite where regno=@regno";

            SqlParameter spar1 = new SqlParameter("regno", SqlDbType.NVarChar); 
            spar1.Value = regno; 
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            
 
            {
                sql = "  select showid, regno, RegGroupID, reg_urlFlowID from tb_RegDelegate where regno=@regno";

                SqlParameter spar2 = new SqlParameter("regno", SqlDbType.NVarChar);
                spar2.Value = regno;
                List<SqlParameter> pList2 = new List<SqlParameter>();
                pList2.Add(spar2);
                DataTable onLine = fn.GetDatasetByCommand(sql, "DT", pList2).Tables["DT"];

                cList.showID = onLine.Rows[0]["showID"].ToString();
                cList.FlowID = onLine.Rows[0]["reg_urlFlowID"].ToString();
                cList.regno = onLine.Rows[0]["regno"].ToString();
                cList.groupID = onLine.Rows[0]["RegGroupID"].ToString();
            }

        }
        catch (Exception ex) { }
        return cList;
    }
}