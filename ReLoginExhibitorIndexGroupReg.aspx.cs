﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReLoginExhibitorIndexGroupReg : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected static string reloginLockPage = "ReloginLock.aspx";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Event"] != null && Request.QueryString["EXH"] != null)
            {
                string eventName = Request.QueryString["Event"].ToString();
                string exhibitorID = cFun.DecryptValue(Request.QueryString["EXH"].ToString());
                tmpDataList tmpList = GetShowIDByEventName(eventName);
                if (string.IsNullOrEmpty(tmpList.showID))
                {
                    Response.Redirect("404.aspx");
                }
                else
                {
                    GroupLogin(exhibitorID, tmpList.FlowID, tmpList.showID, eventName);
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }
    #region Show Data
    public class tmpDataList
    {
        public string showID { get; set; }
        public string FlowID { get; set; }
    }
    private tmpDataList GetShowIDByEventName(string eventName)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            cList.showID = "";
            cList.FlowID = "";
            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                cList.showID = dList.Rows[0]["showID"].ToString();
                cList.FlowID = dList.Rows[0]["FLW_ID"].ToString();
            }

        }
        catch { }
        return cList;
    }
    #endregion
    #region GroupLogin
    private void GroupLogin(string exhibitorID, string flowid, string showid, string eventName)
    {
        try
        {
            DataTable dt = getRegGroupByExhibitorID(exhibitorID, showid);
            if (dt.Rows.Count > 0)
            {
                string regGroupID = dt.Rows[0]["RegGroupID"].ToString();
                string password = dt.Rows[0]["RG_ContactEmail"].ToString();

                StatusSettings statusSet = new StatusSettings(fn);
                InvoiceControler invControler = new InvoiceControler(fn);

                FlowControler flwControl = new FlowControler(fn);
                FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    RegGroupObj rgObj = new RegGroupObj(fn);
                    DataTable dtGroupLogin = rgObj.getGroupReLoginAuthenData(regGroupID, password, flowid);

                    if (dtGroupLogin.Rows.Count > 0)
                    {
                        string groupid = dtGroupLogin.Rows[0]["RegGroupID"].ToString();
                        Session["Groupid"] = groupid;
                        Session["Flowid"] = flowid;
                        Session["Showid"] = showid;
                        string regstage = dtGroupLogin.Rows[0]["RG_Stage"].ToString();
                        Session["regstage"] = regstage;
                        string regstatus = dtGroupLogin.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                        Session["RegStatus"] = regstatus;

                        LoginLog lg = new LoginLog(fn);
                        string userip = GetUserIP();
                        lg.loginid = groupid;
                        lg.loginip = userip;
                        lg.logintype = LoginLogType.regLogin;
                        lg.saveLoginLog();

                        string lateststage = flwControl.GetLatestStep(flowid);

                        string page = ReLoginStaticValueClass.regHomePage;
                        string step = "";
                        Dictionary<string, string> nValues = flwControl.GetNextRoute(flowid);
                        if (nValues.Count > 0)
                        {
                            page = nValues["nURL"].ToString();
                            step = nValues["nStep"].ToString();
                        }
                        Dictionary<string, string> nValuesReLogin = flwControl.GetCurrentReloginRoute(flowid, step);
                        if (nValuesReLogin.Count > 0)
                        {
                            page = nValuesReLogin["nURL"].ToString();
                            step = nValuesReLogin["nStep"].ToString();
                        }
                        string route = flwControl.MakeFullURL(page, flowid, showid, groupid, step);
                        Response.Redirect(route, false);

                        #region Normal Way (no use for MDA on 16-1-2020)
                        //if (!string.IsNullOrEmpty(lateststage) && regstage == lateststage && cFun.ParseInt(regstatus) == statusSet.Success)
                        //{
                        //    string invStatus = invControler.getInvoiceStatus(groupid);
                        //    if (invStatus == statusSet.Pending.ToString())
                        //    {
                        //        string page = ReLoginStaticValueClass.regIndexPage;

                        //        //bool isUnlock = isRegLoginUnLock();//**
                        //        //if (isUnlock)
                        //        //{
                        //        string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                        //        Response.Redirect(route, false);
                        //        //}
                        //        //else
                        //        //{
                        //        //    Response.Redirect(reloginLockPage + "?Event=" + eventName);
                        //        //}
                        //    }
                        //    if (invStatus == statusSet.Success.ToString() || invStatus == statusSet.Waived.ToString()
                        //        || invStatus == RegClass.noInvoice
                        //        || invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString())
                        //    {
                        //        string page = ReLoginStaticValueClass.regHomePage;

                        //        //bool isUnlock = isRegLoginUnLock();//**
                        //        //if (isUnlock)
                        //        //{
                        //        string route = flwControl.MakeFullURL(page, flowid, showid);
                        //        Response.Redirect(route, false);
                        //        //}
                        //        //else
                        //        //{
                        //        //    Response.Redirect(reloginLockPage + "?Event=" + eventName);
                        //        //}
                        //    }
                        //}
                        //else
                        //{
                        //    string invStatus = invControler.getInvoiceStatus(groupid);
                        //    if (invStatus == RegClass.noInvoice || invStatus == statusSet.Pending.ToString())
                        //    {
                        //        string page = ReLoginStaticValueClass.regIndexPage;

                        //        //bool isUnlock = isRegLoginUnLock();//**
                        //        //if (isUnlock)
                        //        //{
                        //        string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                        //        Response.Redirect(route, false);
                        //        //}
                        //        //else
                        //        //{
                        //        //    Response.Redirect(reloginLockPage + "?Event=" + eventName);
                        //        //}
                        //    }
                        //    else if (invStatus == statusSet.Success.ToString() || invStatus == statusSet.Waived.ToString()
                        //        || invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString())
                        //    {
                        //        string page = ReLoginStaticValueClass.regHomePage;

                        //        //bool isUnlock = isRegLoginUnLock();//**
                        //        //if (isUnlock)
                        //        //{
                        //        string route = flwControl.MakeFullURL(page, flowid, showid);
                        //        Response.Redirect(route, false);
                        //        //}
                        //        //else
                        //        //{
                        //        //    Response.Redirect(reloginLockPage + "?Event=" + eventName);
                        //        //}
                        //    }
                        //}
                        #endregion
                    }
                    else
                    {
                        string errMsg = "Invalid login!";
                        Response.Write("<script>alert('" + errMsg + "');window.location='404.aspx';</script>");
                        //bool isLock = isRegLoginLock();
                        //if (isLock)
                        //{
                        //    Response.Redirect(reloginLockPage + "?Event=" + eventName);
                        //}
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #region getRegGroupByID
    public DataTable getRegGroupByExhibitorID(string exhibitorID, string showid)
    {
        DataTable dtGroup = new DataTable();

        try
        {
            string query = string.Format("Select * From tb_RegGroup Where recycle=0 And ExhibitorPortalID='{0}' And ShowID=@SHWID", exhibitorID);
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            spar.Value = showid;
            pList.Add(spar);

            dtGroup = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch { }

        return dtGroup;
    }
    #endregion
    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
    private bool isRegLoginLock()
    {
        bool isLock = false;
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'";
            DataTable dtRelogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtRelogin.Rows.Count > 0)
            {
                string sqlReLoginUpdate = string.Empty;
                string AttemptTime = fn.GetDataByCommand("Select relogin_attemptTimes From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'", "relogin_attemptTimes");
                int attempedTime = 0;
                int.TryParse(AttemptTime, out attempedTime);
                if (attempedTime > 5)
                {
                    sqlReLoginUpdate = "Update tb_LogReLoginLocking Set relogin_lockdatetime=getdate(),relogin_islock=1 Where relogin_machineip='" + machineIP + "'";
                    fn.ExecuteSQL(sqlReLoginUpdate);
                    isLock = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isLock;
    }
    private bool isRegLoginUnLock()
    {
        bool isUnlock = false;
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'";
            DataTable dtRelogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtRelogin.Rows.Count > 0)
            {
                string sqlReLoginUpdate = string.Empty;
                string AttemptTime = fn.GetDataByCommand("Select relogin_lockdatetime From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'", "relogin_lockdatetime");
                DateTime attempedDateTime = cFun.ParseDateTime(AttemptTime);
                DateTime datetimenow = DateTime.Now;
                TimeSpan duration = datetimenow - attempedDateTime;
                double durationTime = duration.TotalMinutes;
                if (durationTime >= 20)//***
                {
                    sqlReLoginUpdate = "Update tb_LogReLoginLocking Set relogin_lockdatetime=Null,relogin_islock=0,relogin_attemptTimes=0 Where relogin_machineip='" + machineIP + "'";
                    fn.ExecuteSQL(sqlReLoginUpdate);
                    isUnlock = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isUnlock;
    }
    #endregion
}