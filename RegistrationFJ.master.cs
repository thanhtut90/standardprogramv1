﻿using Corpit.BackendMaster;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegistrationFJ : System.Web.UI.MasterPage
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    private static string[] checkingFJClosedFlowIDs = new string[] { "F376", "F377" };

    protected void Page_Load(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            ShowController shwCtrl = new ShowController(fn);
            if (shwCtrl.checkValidShow(showid))
            {
                SiteSettings sSetting = new SiteSettings(fn, showid);
                sSetting.LoadBaseSiteProperties(showid);
                FlowControler flwObj = new FlowControler(fn, urlQuery);
                FlowMaster fMaster = flwObj.GetFlowMasterConfig(flwObj.FlowID);
                if (Request.Params["t"] != null)
                {
                    string admintype = cFun.DecryptValue(Request.QueryString["t"].ToString());
                    if (admintype == "iswebsettings")
                    {
                    }
                    else
                    {
                        if (sSetting.isRegisterClosed() || checkingFJClosedFlowIDs.Contains(flwObj.FlowID))
                        {
                            if (fMaster.isOnsite != 1)
                                Response.Redirect("RegClosed.aspx?SHW=" + cFun.EncryptValue(showid));
                        }
                    }
                }
                else
                {
                    if (sSetting.isRegisterClosed() || checkingFJClosedFlowIDs.Contains(flwObj.FlowID))
                    {
                        if (fMaster.isOnsite != 1)
                            Response.Redirect("RegClosed.aspx?SHW=" + cFun.EncryptValue(showid));
                    }
                }

                if (!IsPostBack)
                {
                    InitSiteSettings(showid);
                    LoadPageSetting();
                    stepsettings(urlQuery);
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        SiteBanner.ImageUrl = sSetting.SiteBanner;
        Global.PageTitle = sSetting.SiteTitle;
        Global.Copyright = sSetting.SiteFooter;
        Global.PageBanner = sSetting.SiteBanner;

        if (!string.IsNullOrEmpty(sSetting.SiteCssBandlePath))
            BundleRefence.Path = "~/Content/" + sSetting.SiteCssBandlePath + "/";
    }
    private void LoadPageSetting()
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        SetPageSettting(flwObj.CurrIndexTiTle);
    }

    public void SetPageSettting(string title)
    {
        SiteHeader.Text = title;
    }

    #endregion

    #region step progress bar
    private void stepsettings(FlowURLQuery urlQuery)
    {
        try
        {
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
            FlowControler flwControl = new FlowControler(fn);
            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
            setStep(cFun.ParseInt(currentStep), flwMasterConfig.FlowType, flowid);
        }
        catch(Exception ex)
        { }
    }
    public void setStep(int pageindex, string flowType, string flowid)
    {
        if (flowType == SiteFlowType.FLOW_INDIVIDUAL)
        {
            FlowControler Flw = new FlowControler(fn);
            bool confExist = Flw.checkPageExist(flowid, SiteDefaultValue.constantConfName);
            if (!confExist)
            {
                if (pageindex == 1)
                {
                    step1.Attributes.Add("class", "btn btn-primary btn-circle");
                    step1.Attributes.Remove("disabled");
                    step2.Attributes.Add("class", "btn btn-default btn-circle");
                    step2.Attributes.Add("disabled", "disabled");
                    step3.Attributes.Add("class", "btn btn-default btn-circle");
                    step3.Attributes.Add("disabled", "disabled");
                    step4.Attributes.Add("class", "btn btn-default btn-circle");
                    step4.Attributes.Add("disabled", "disabled");

                    divIndividual.Visible = true;
                }
                else if (pageindex == 2)
                {
                    step1.Attributes.Add("class", "btn btn-default btn-circle");
                    step1.Attributes.Add("disabled", "disabled");
                    step2.Attributes.Add("class", "btn btn-primary btn-circle");
                    step2.Attributes.Remove("disabled");
                    step3.Attributes.Add("class", "btn btn-default btn-circle");
                    step3.Attributes.Add("disabled", "disabled");
                    step4.Attributes.Add("class", "btn btn-default btn-circle");
                    step4.Attributes.Add("disabled", "disabled");

                    divIndividual.Visible = true;
                }
                else if (pageindex == 3)
                {
                    step1.Attributes.Add("class", "btn btn-default btn-circle");
                    step1.Attributes.Add("disabled", "disabled");
                    step2.Attributes.Add("class", "btn btn-default btn-circle");
                    step2.Attributes.Add("disabled", "disabled");
                    step3.Attributes.Add("class", "btn btn-primary btn-circle");
                    step3.Attributes.Remove("disabled");
                    step4.Attributes.Add("class", "btn btn-default btn-circle");
                    step4.Attributes.Add("disabled", "disabled");

                    divIndividual.Visible = true;
                }
                else if (pageindex == 4)
                {
                    step1.Attributes.Add("class", "btn btn-default btn-circle");
                    step1.Attributes.Add("disabled", "disabled");
                    step2.Attributes.Add("class", "btn btn-default btn-circle");
                    step2.Attributes.Add("disabled", "disabled");
                    step3.Attributes.Add("class", "btn btn-default btn-circle");
                    step3.Attributes.Add("disabled", "disabled");
                    step4.Attributes.Add("class", "btn btn-primary btn-circle");
                    step4.Attributes.Remove("disabled");

                    divIndividual.Visible = true;
                }
            }
            else//With Conference
            {
                if (pageindex == 1)
                {
                    stepP1.Attributes.Add("class", "btn btn-primary btn-circle");
                    stepP1.Attributes.Remove("disabled");
                    stepP2.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP2.Attributes.Add("disabled", "disabled");
                    stepP3.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP3.Attributes.Add("disabled", "disabled");
                    stepP4.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP4.Attributes.Add("disabled", "disabled");
                    stepP5.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP5.Attributes.Add("disabled", "disabled");

                    divIndividualPublic.Visible = true;
                }
                else if (pageindex == 2)
                {
                    stepP1.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP1.Attributes.Add("disabled", "disabled");
                    stepP2.Attributes.Add("class", "btn btn-primary btn-circle");
                    stepP2.Attributes.Remove("disabled");
                    stepP3.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP3.Attributes.Add("disabled", "disabled");
                    stepP4.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP4.Attributes.Add("disabled", "disabled");
                    stepP5.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP5.Attributes.Add("disabled", "disabled");

                    divIndividualPublic.Visible = true;
                }
                else if (pageindex == 3)
                {
                    stepP1.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP1.Attributes.Add("disabled", "disabled");
                    stepP2.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP2.Attributes.Add("disabled", "disabled");
                    stepP3.Attributes.Add("class", "btn btn-primary btn-circle");
                    stepP3.Attributes.Remove("disabled");
                    stepP4.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP4.Attributes.Add("disabled", "disabled");
                    stepP5.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP5.Attributes.Add("disabled", "disabled");

                    divIndividualPublic.Visible = true;
                }
                else if (pageindex == 4)
                {
                    stepP1.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP1.Attributes.Add("disabled", "disabled");
                    stepP2.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP2.Attributes.Add("disabled", "disabled");
                    stepP3.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP3.Attributes.Add("disabled", "disabled");
                    stepP4.Attributes.Add("class", "btn btn-primary btn-circle");
                    stepP4.Attributes.Remove("disabled");
                    stepP5.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP5.Attributes.Add("disabled", "disabled");

                    divIndividualPublic.Visible = true;
                }
                else if (pageindex == 5)
                {
                    stepP1.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP1.Attributes.Add("disabled", "disabled");
                    stepP2.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP2.Attributes.Add("disabled", "disabled");
                    stepP3.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP3.Attributes.Add("disabled", "disabled");
                    stepP4.Attributes.Add("class", "btn btn-default btn-circle");
                    stepP4.Attributes.Add("disabled", "disabled");
                    stepP5.Attributes.Add("class", "btn btn-primary btn-circle");
                    stepP5.Attributes.Remove("disabled");

                    divIndividualPublic.Visible = true;
                }
            }
        }
        else//Group Flow
        {
            if(pageindex == 4 || pageindex == 5 || pageindex == 6)//****
            {
                pageindex = pageindex - 1;
            }//****

            if (pageindex == 1)
            {
                Gstep1.Attributes.Add("class", "btn btn-primary btn-circle");
                Gstep1.Attributes.Remove("disabled");
                Gstep2.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep2.Attributes.Add("disabled", "disabled");
                Gstep3.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep3.Attributes.Add("disabled", "disabled");
                Gstep4.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep4.Attributes.Add("disabled", "disabled");
                Gstep5.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep5.Attributes.Add("disabled", "disabled");

                divGroup.Visible = true;
            }
            else if (pageindex == 2)
            {
                Gstep1.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep1.Attributes.Add("disabled", "disabled");
                Gstep2.Attributes.Add("class", "btn btn-primary btn-circle");
                Gstep2.Attributes.Remove("disabled");
                Gstep3.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep3.Attributes.Add("disabled", "disabled");
                Gstep4.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep4.Attributes.Add("disabled", "disabled");
                Gstep5.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep5.Attributes.Add("disabled", "disabled");

                divGroup.Visible = true;
            }
            else if (pageindex == 3)
            {
                Gstep1.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep1.Attributes.Add("disabled", "disabled");
                Gstep2.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep2.Attributes.Add("disabled", "disabled");
                Gstep3.Attributes.Add("class", "btn btn-primary btn-circle");
                Gstep3.Attributes.Remove("disabled");
                Gstep4.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep4.Attributes.Add("disabled", "disabled");
                Gstep5.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep5.Attributes.Add("disabled", "disabled");

                divGroup.Visible = true;
            }
            else if (pageindex == 4)
            {
                Gstep1.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep1.Attributes.Add("disabled", "disabled");
                Gstep2.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep2.Attributes.Add("disabled", "disabled");
                Gstep3.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep3.Attributes.Add("disabled", "disabled");
                Gstep4.Attributes.Add("class", "btn btn-primary btn-circle");
                Gstep4.Attributes.Remove("disabled");
                Gstep5.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep5.Attributes.Add("disabled", "disabled");

                divGroup.Visible = true;
            }
            else if (pageindex == 5)
            {
                Gstep1.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep1.Attributes.Add("disabled", "disabled");
                Gstep2.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep2.Attributes.Add("disabled", "disabled");
                Gstep3.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep3.Attributes.Add("disabled", "disabled");
                Gstep4.Attributes.Add("class", "btn btn-default btn-circle");
                Gstep4.Attributes.Add("disabled", "disabled");
                Gstep5.Attributes.Add("class", "btn btn-primary btn-circle");
                Gstep5.Attributes.Remove("disabled");

                divGroup.Visible = true;
            }
        }
    }
    #endregion
}
