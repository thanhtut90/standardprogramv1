﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SendPrepopulateEmail : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindShowList();
        }
    }

    private void bindShowList()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show where Status='Active' And SHW_ID='YTD391'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddlShowID.Items.Clear();
                ddlShowID.DataSource = cmd.ExecuteReader();
                ddlShowID.DataTextField = "SHW_Name";
                ddlShowID.DataValueField = "SHW_ID";
                ddlShowID.DataBind();
                ddlShowID.Items.Insert(0, new ListItem("Select Show", "0"));
                con.Close();
            }
        }
    }


    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        if (ddlShowID.SelectedItem.Value != "0")
        {
            if (!string.IsNullOrEmpty(txtProjectUrl.Text) && !string.IsNullOrWhiteSpace(txtProjectUrl.Text))
            {
                string showid = ddlShowID.SelectedItem.Value;
                //string sql = "Select reg_urlFlowID,recycle,reg_Status,reg_isSMS,Delegate_RefRegno,reg_Email,* From tb_RegPrePopulate Where ShowID='" + showid + "'"
                //    + " And (reg_isSMS Is Null Or reg_isSMS=0) Order By ID";
                string sql="Select reg_urlFlowID,recycle,reg_Status,reg_isSMS,Delegate_RefRegno,reg_Email,* From tb_RegPrePopulate Where ShowID='YTD391'"
                                + " And RefNo Is Null And reg_isSMS=1 And Delegate_RefRegno Not In ('HUA10000000000003') Order By ID";
                DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int sentcount = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        string delegateRefRegno = dr["Delegate_RefRegno"].ToString();
                        string fullname = dr["reg_FName"].ToString();
                        string emailaddress = dr["reg_Email"].ToString();
                        string flowid = dr["reg_urlFlowID"].ToString();
                        string groupid = dr["RegGroupID"].ToString();
                        string contactName = dr["reg_Address3"].ToString();
                        string contactEmail = dr["reg_Address4"].ToString();
                        string prjUrl = txtProjectUrl.Text.Trim();
                        string regUrl = getUrl(delegateRefRegno, prjUrl);

                        Response.End();

                        string emailSubject = "【RSVP Reminder】 Join us at APAC HUAWEI Developer Day 2019";
                        string emailFromName = "HUAWEI";

                        string isSuccess = sEmailSend(delegateRefRegno, regUrl, emailaddress, fullname, contactName, contactEmail, emailSubject, emailFromName);
                        if(isSuccess == "T")
                        {
                            string updatesql = "Update tb_RegPrePopulate Set reg_isSMS=2 Where ShowID='" + showid + "' And Delegate_RefRegno='" + delegateRefRegno + "'";
                            fn.ExecuteSQL(updatesql);
                        }

                        string sqlLog = String.Format("Insert Into tmp_Prepopulate_EmailSendLog (Regno,RegGroupID,RegEmail,ShowID,FlowID,Status,RegLink)"
                                            + " Values (N'{0}',N'{1}',N'{2}',N'{3}',N'{4}',N'{5}',N'{6}')",
                                            delegateRefRegno, groupid, emailaddress, showid, flowid, isSuccess.ToString(), txtProjectUrl.Text.Trim());
                        fn.ExecuteSQL(sqlLog);
                        sentcount++;

                        Response.End();
                    }
                    //Response.End();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already sent the email.Count:" + sentcount + "');", true);
                }
            }
            else
            {
                Response.Write("<script>alert('Please insert Registration URL.');</script>");//window.location='404.aspx';
            }
        }
        else
        {
            Response.Write("<script>alert('Please select show.');</script>");//window.location='404.aspx';
        }
    }

    private string getUrl(string delegateID, string projecturl)
    {
        string url = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(delegateID))
            {
                string page = "EventRegPreReg.aspx?reg=" + delegateID;
                string route = projecturl + page;
                url = route;
            }
        }
        catch { }

        return url;
    }

    public string sEmailSend(string delegateRefRegno, string regUrl, string emailaddress, string fullname, string contactName, string contactEmail, string emailSubject, string emailFromName)
    {
        string isSuccess = "F";
        try
        {
            Alpinely.TownCrier.TemplateParser tm = new Alpinely.TownCrier.TemplateParser();
            Alpinely.TownCrier.MergedEmailFactory factory = new Alpinely.TownCrier.MergedEmailFactory(tm);
            string template = HttpContext.Current.Server.MapPath("~/Template/Huawei_RSPV_V2.html");//Huawei_RSPV.html (1st time)

            var tokenValues = new Dictionary<string, string>
                {
                    {"regUrl",regUrl},
                    {"FullName",fullname},
                    {"ContactName",contactName},
                    {"ContactEmail",contactEmail},
                };
            MailMessage message = factory
            .WithTokenValues(tokenValues)
            .WithHtmlBodyFromFile(template)
            .Create();

            //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//***
            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;//***

            //var from = new MailAddress("registration@event-reg.biz", ddlShowID.SelectedItem.Text);
            var from = new MailAddress("admin@event-reg.biz", emailFromName);
            message.From = from;
            message.To.Add(emailaddress);
            message.Bcc.Add("event@corpit.com.sg");

            if (!string.IsNullOrEmpty(contactEmail))
            {
                message.CC.Add(contactEmail);
            }

            message.Subject = emailSubject;
            SmtpClient emailClient = new SmtpClient("smtp.gmail.com");
            //System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("registration@event-reg.biz", "c0rpitblasT");
            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("admin@event-reg.biz", "c0rpitblasT");
            emailClient.UseDefaultCredentials = false;
            emailClient.EnableSsl = true;
            emailClient.Port = 587;//***
            emailClient.Credentials = SMTPUserInfo;
            emailClient.Send(message);

            isSuccess = "T";
        }
        catch (Exception ex)
        {
            isSuccess = ex.Message;// "F";
        }
        return isSuccess;
    }
}