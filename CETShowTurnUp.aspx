﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CETShowTurnUp.aspx.cs" Inherits="CETShowTurnUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <u>Show CET Turn Up (Total Number)</u><br /><br />
        Total Number : <asp:Label ID="lblTotalNumber" runat="server" Font-Bold="true"></asp:Label><br /><br />
        <asp:Button ID="btnShow" runat="server" Text="Show" OnClick="btnShow_Click" />
    </div>
    </form>
</body>
</html>
