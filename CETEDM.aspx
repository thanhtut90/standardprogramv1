﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CETEDM.aspx.cs" Inherits="CETEDM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>CET EDM</title>
    <style type="text/css">
        img {
            width: 100%;
            margin-top:-8px !important;
        }
    </style>
</head>
<body align="center">
    <form id="form1" runat="server">
        <div class="yfix">
            <table cellspacing="0" cellpadding="0" align="center" border="0" style="width:1000px;height:auto;" bgcolor="#ffffff">
                <tr>
                    <td width="100%" align="center">
                        <img alt="Header" src="Template/images/Header.png" />
                        <a id="aRegistrationUrl" runat="server" href="https://event-reg.biz/registration/eventreg?event=CET2019" target="_blank"><img alt="RegisterToday" src="Template/images/Register.png" /></a>
                        <img alt="Highlights" src="Template/images/Highlights.png" />
                        <a href="https://npcms2019.com/" target="_blank"><img alt="SymposiumFee" src="Template/images/clickhere.png" /></a>
                        <img alt="Footer" src="Template/images/companies.png" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
