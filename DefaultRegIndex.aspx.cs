﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
using Corpit.Logging;
public partial class DefaultRegIndex : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected override void OnPreInit(EventArgs e)
    {
        if (Request.QueryString["SHW"] != null)
        {
            string showID = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

            SetSiteMaster(showID);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string showID = "";
            if (Request.QueryString["SHW"] != null)
            {
                showID = cFun.DecryptValue(Request.QueryString["SHW"].ToString());

                string FlowID = "";
                if (Request.QueryString["FLW"] != null)
                {
                    FlowID = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
                }
                Functionality fn = new Functionality();
                SiteSettings st = new SiteSettings(fn, showID);
                string Type = ""; // Future Reg Close date can b vary depend on Delegate Type
                if (!st.IsRegClosed(Type))
                {
                    string page = "";
                    string step = "";
                    FlowControler Flw = new FlowControler(fn);

                    string grpNum = string.Empty;
                    if (Request.QueryString["GRP"] == null)
                    {
                        RegGroupObj rgp = new RegGroupObj(fn);
                        grpNum = rgp.GenGroupNumber(showID);// GetRunNUmber("GRP_KEY");
                    }
                    else
                    {
                        grpNum = cFun.DecryptValue(Request.QueryString["GRP"].ToString());
                    }

                    Dictionary<string, string> nValues = Flw.GetNextRoute(FlowID);
                    if (nValues.Count > 0)
                    {
                        page = nValues["nURL"].ToString();
                        step = nValues["nStep"].ToString();
                        FlowID = nValues["FlowID"].ToString();
                    }
                    //Add Tracking for MDA
                    TrackingLog(showID, FlowID, grpNum);
                    CETEDMClickingLog(showID, FlowID, grpNum);
                    grpNum = cFun.EncryptValue(grpNum);
                    if (Request.QueryString["INV"] != null)
                    {
                        string invID = cFun.EncryptValue(cFun.DecryptValue(Request.QueryString["INV"].ToString()));
                        string route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step, invID);
                        Response.Redirect(route);
                    }
                    else
                    {
                        string route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step);
                        Response.Redirect(route);
                    }
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion
    private void TrackingLog(string showid, string flowid, string groupID)
    {
        try
        {
            if (Request.QueryString["TRK"] != null)
            {
                LogTracking lTrack = new LogTracking(fn);
                lTrack.ShowID = showid;
                lTrack.FlowID = flowid;
                lTrack.TrackCode = Request.QueryString["TRK"].ToString();
                lTrack.RefID = groupID;
                lTrack.saveLog();
            }
        }
        catch { }
    }
    private void CETEDMClickingLog(string showid, string flowid, string groupID)
    {
        try
        {
            if (Request.QueryString["CEI"] != null)
            {
                LogTracking lTrack = new LogTracking(fn);
                lTrack.ShowID = showid;
                lTrack.FlowID = flowid;
                lTrack.TrackCode = Request.QueryString["CEI"].ToString();
                lTrack.RefID = groupID;
                lTrack.saveLog();
            }
        }
        catch { }
    }
}