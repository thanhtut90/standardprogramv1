﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="VendorPhtoUpload.aspx.cs" Inherits="VendorPhtoUpload" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .red
        {
            color:red;
        }
        /*.chkFooter
        {
            white-space:nowrap;
        }*/
        .chkFooter label
        {
            font-weight:unset !important;
            padding-left:5px !important;
            vertical-align:top !important;
        }
        .chkFooter input[type="checkbox"]
        {
            margin-top: 2px !important;
        }
        a:hover
        {
            text-decoration:none !important;
        }
        .rowLabel
        {
            padding-top: 7px;
        }
    </style>

    <script type="text/javascript">
        function validateDate(elementRef) {
                var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

                var dateValue = elementRef.value;

                if (dateValue.length != 8) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                //// mm/dd/yyyy format... 
                //var valueArray = dateValue.split('/');

                //if (valueArray.length != 3) {
                //    alert('Entry must be in the format dd/mm/yyyy.');

                //    return false;
                //}

                ////YYYYMMDD
                var monthValue = parseFloat(dateValue.substring(4, 6));
                var dayValue = parseFloat(dateValue.substring(6, 8));
                var yearValue = parseFloat(dateValue.substring(0, 4));

                if ((isNaN(monthValue) == true) || (isNaN(dayValue) == true) || (isNaN(yearValue) == true)) {
                    alert('Non-numeric entry detected');//\nEntry must be in the format dd/mm/yyyy.

                    return false;
                }

                if (((yearValue % 4) == 0) && (((yearValue % 100) != 0) || ((yearValue % 400) == 0)))
                    monthDays[1] = 29;
                else
                    monthDays[1] = 28;

                if ((monthValue < 1) || (monthValue > 12)) {
                    alert('Invalid month entered');//\nEntry must be in the format dd/mm/yyyy.

                    return false;
                }

                var monthDaysArrayIndex = monthValue - 1;
                if ((dayValue < 1) || (dayValue > monthDays[monthDaysArrayIndex])) {
                    alert('Invalid day entered');//\nEntry must be in the format dd/mm/yyyy.

                    return false;
                }

            return true;
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:TextBox ID="txtPassNo" runat="server" Visible="false"></asp:TextBox>
            <div class="form-group row" runat="server" id="divPhotoUpload">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblPhotoUpload" runat="server" CssClass="form-control-label" Text="Photo upload"></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:FileUpload runat="server" ID="fupPhotoUpload" CssClass="style4" 
                                style="font-family: DINPro-Regular" />
                            <br />
                            <ul style="list-style-type:disc;">
                                <li>Photo image is required for the production of pass.</li>
                                <li>JPEG image format (with .jpg file extension)</li>
                                <li>400 by 514 pixels image size</li>
                                <li>No more than 150 KBytes file size.</li>
                                <li>Save photo filemame as NRIC_Name of applicant e.g. SXXXX123A_Lim Siew Lan</li>
                            </ul>
                            <%--<asp:Label ID="lblPhotoUploadNote" runat="server" Text="Please upload png/jpg/jpeg file format." Font-Italic="true"></asp:Label>--%>
                            <br />
                            <br />
                            <asp:HyperLink ID="hpPhotoUpload" runat="server" Visible="false" Target="_blank" NavigateUrl="#"></asp:HyperLink>
                            <asp:Image runat="server" ID="imgPhotoUpload" Width="280px" Height="150px" Visible="false" ImageUrl="#" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblErrPhotoUpload" runat="server" ForeColor="Red" Text="*Required" Visible="false"></asp:Label>
                    <%--<asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToValidate="ddlStudentType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>--%>
                </div>
            </div>

            <div class="form-group">
                <div class="form-group container" style="padding-top:20px;">
                  <asp:Panel runat="server" ID="PanelShowBackButton" Visible="false" >
                      <div class="col-lg-2 col-sm-offset-4 col-sm-2 center-block" >
                        <asp:Button runat="server" ID="btnBack" CssClass="btn MainButton btn-block" Text="Cancel" OnClick="btnBack_Click" CausesValidation="false" Visible="false"/>
                    </div>
                      <div class="col-lg-2  col-sm-2 center-block" >
                        <asp:Button runat="server" ID="btnSave2" CssClass="btn MainButton btn-block" Text="Next" OnClick="btnSave_Click" />
                    </div>
                  </asp:Panel>
                     <asp:Panel runat="server" ID="PanelWithoutBack" Visible="true" >
                    <div class="col-lg-offset-5 col-lg-3 col-sm-offset-4 col-sm-3 center-block" >
                        <asp:Button runat="server" ID="btnSave" CssClass="btn MainButton btn-block" Text="Next" OnClick="btnSave_Click" />
                    </div>
                   </asp:Panel>
                </div>
            </div>

            <asp:HiddenField ID="hfRegno" runat="server" />
            <br />

</asp:Content>

