﻿using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Corpit.Promo;
using Corpit.Logging;
using Corpit.BackendMaster;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;

public partial class RegDelegateOSEA : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    protected static string[] EventCodeCountries_OSEA = { "malaysia", "indonesia", "thailand" };
    protected static string LastFirstOrderName1_OSEA = "Family Name, First Name";
    protected static string FirstLastOrderName2_OSEA = "First Name, Family Name";

    protected static string OSEAVIPRegno = "OSEAVIPRegno";
    protected static string OSEAVisitorRegno = "OSEAVisitorRegno";
    #endregion

    /// <summary>
    /// "memberno(reg_Membershipno)" in tb_RegDelegate table is used for their Registration Number.
    /// "reg_isReg" in tb_RegDelegate table is used as pre-populate updated date.
    /// "idno(reg_IDno)" in tb_RegDelegate table is used as pre-populate updated date.
    /// </summary>
    /// <param name="e"></param>

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                if (setDynamicForm(flowid, showid))
                {
                    bindDropdown();

                    bindFlowNote(showid, urlQuery);//***added on 25-6-2018

                    string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                    string regno = cFun.DecryptValue(urlQuery.DelegateID);

                    if (Request.Params["t"] != null)
                    {
                        string admintype = cFun.DecryptValue(Request.QueryString["t"].ToString());
                        if (admintype == BackendStaticValueClass.isAdmin)
                        {
                            btnSave.Visible = false;
                            btnSave.Enabled = false;
                        }
                        else
                        {
                            bindPageLoad(showid, flowid, groupid, regno, urlQuery);
                        }
                    }
                    else
                    {
                        bindPageLoad(showid, flowid, groupid, regno, urlQuery);
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region bindPageLoad
    private void bindPageLoad(string showid, string flowid, string groupid, string regno, FlowURLQuery urlQuery)
    {
        btnSave.Visible = true;
        btnSave.Enabled = true;

        CommonFuns cFun = new CommonFuns();

        if (!String.IsNullOrEmpty(groupid))
        {
            //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
            RegGroupObj rgg = new RegGroupObj(fn);
            rgg.updateGroupCurrentStep(urlQuery);

            //*update reg_urlFlowID(current flowid) and reg_Stage(current step) of tb_RegDelegate table
            RegDelegateObj rgd = new RegDelegateObj(fn);
            rgd.updateDelegateCurrentStep(urlQuery);

            populateUserDetails(groupid, regno, showid, flowid);

            rbreg_SelectedIndexChanged(this, null);

            insertLogFlowAction(groupid, regno, rlgobj.actview, urlQuery);

            CategoryClass catClass = new CategoryClass();
            FlowControler fCon = new FlowControler(fn);
            FlowMaster flwMaster = fCon.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
            string type = flwMaster.FlowCategoryConfigType;
            if (type == catClass.flowcat_ByPromoCode.ToString() || type == catClass.flowcat_ByBusinessLogicWithPromo.ToString())//According to Business Logic (Customize) *Like SHBC
            {
                divPromoCode.Visible = true;
                if (flwMaster.isPromoEnabled)//*added on 22-5-2018
                {
                    vcPromoCode.Enabled = true;
                }
                else
                {
                    vcPromoCode.Enabled = false;
                }//*added on 22-5-2018
            }
            else
            {
                divPromoCode.Visible = false;
            }
			
			if (flwMaster.FlowType == SiteFlowType.FLOW_GROUP)
            {
                PanelShowBackButton.Visible = true;
                PanelWithoutBack.Visible = false;
            }
            else
            {
                PanelWithoutBack.Visible = true;
                PanelShowBackButton.Visible = false;
            }
        }
        else
        {
            Response.Redirect("DefaultRegIndex.aspx");
        }
    }
    #endregion

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region bindDropdown & bind respective data to Salutation, Country, Affiliation, Dietary, Profession and OrgType dropdown lists
    protected void bindDropdown()
    {
        DataSet dsSalutation = new DataSet();
        DataSet dsCountry = new DataSet();
        DataSet dsAffiliation = new DataSet();
        DataSet dsdiet = new DataSet();
        DataSet dsProfession = new DataSet();
        DataSet dsOrgType = new DataSet();

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);

        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsSalutation = cmdObj.getSalutation(showid);

        CountryObj couObj = new CountryObj(fn);
        dsCountry = couObj.getAllCountry();

        dsAffiliation = cmdObj.getAffiliation(showid);
        dsdiet = cmdObj.getDietary(showid);
        dsProfession = cmdObj.getProfession(showid);
        dsOrgType = cmdObj.getOrganization(showid);

        if (dsSalutation.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsSalutation.Tables[0].Rows.Count; i++)
            {
                ddlSalutation.Items.Add(dsSalutation.Tables[0].Rows[i]["Sal_Name"].ToString());
                ddlSalutation.Items[i + 1].Value = dsSalutation.Tables[0].Rows[i]["Sal_ID"].ToString();
            }
        }
        if (dsCountry.Tables[0].Rows.Count != 0)
        {
            for (int x = 0; x < dsCountry.Tables[0].Rows.Count; x++)
            {
                ddlCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();

                ddlRCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlRCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();

                ddlVCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlVCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();

                ddlUDFCCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlUDFCCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();
            }
        }

        if (dsAffiliation.Tables[0].Rows.Count != 0)
        {
            for (int y = 0; y < dsAffiliation.Tables[0].Rows.Count; y++)
            {
                ddlAffiliation.Items.Add(dsAffiliation.Tables[0].Rows[y]["aff_name"].ToString());
                ddlAffiliation.Items[y + 1].Value = dsAffiliation.Tables[0].Rows[y]["affid"].ToString();
            }
        }
        if (dsdiet.Tables[0].Rows.Count != 0)
        {
            for (int z = 0; z < dsdiet.Tables[0].Rows.Count; z++)
            {
                ddlDietary.Items.Add(dsdiet.Tables[0].Rows[z]["diet_name"].ToString());
                ddlDietary.Items[z + 1].Value = dsdiet.Tables[0].Rows[z]["diet_id"].ToString();
            }
        }

        if (dsProfession.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsProfession.Tables[0].Rows.Count; i++)
            {
                ddlProfession.Items.Add(dsProfession.Tables[0].Rows[i]["Profession"].ToString());
                ddlProfession.Items[i + 1].Value = dsProfession.Tables[0].Rows[i]["ID"].ToString();
            }
        }

        if (dsOrgType.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsOrgType.Tables[0].Rows.Count; i++)
            {
                ddlOrganization.Items.Add(dsOrgType.Tables[0].Rows[i]["organisation"].ToString());
                ddlOrganization.Items[i + 1].Value = dsOrgType.Tables[0].Rows[i]["ID"].ToString();
            }
        }

        if(ddlOrganization.Items.Count > 0)//*
        {
            ddlOrganization_SelectedIndexChanged(this, null);
        }
    }
    #endregion

    #region setDynamicForm (set div visibility and validator controls' enability dynamically (generate dynamic form) according to the settings of tb_Form table where form_type='D')
    protected bool setDynamicForm(string flowid, string showid)
    {
        bool isValidShow = false;

        DataSet ds = new DataSet();
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForDelegate();

        int isVisitorVisible = 0;
        int isUDFVisible = 0;
        int isSupervisorVisible = 0;

        string formtype = FormType.TypeDelegate;

        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
        {
            isValidShow = true;

            #region set divSalutation visibility is true or false if form_input_name is Salutation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                if (isshow == 1)
                {
                    divSalutation.Visible = true;
                }
                else
                {
                    divSalutation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Salutation, formtype);
                if (isrequired == 1)
                {
                    lblSalutation.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlSalutation.Attributes.Add("required", "");
                    vcSal.Enabled = true;
                }
                else
                {
                    lblSalutation.Text = labelname;
                    //ddlSalutation.Attributes.Remove("required");
                    vcSal.Enabled = false;
                }
            }
            #endregion

            #region set divFName visibility is true or false if form_input_name is FName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFName.Visible = true;
                }
                else
                {
                    divFName.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Fname, formtype);
                txtFName.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblFName.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtFName.Attributes.Add("required", "");
                    vcFName.Enabled = true;
                }
                else
                {
                    lblFName.Text = labelname;
                    //txtFName.Attributes.Remove("required");
                    vcFName.Enabled = false;
                }
            }
            #endregion

            #region set divLName visibility is true or false if form_input_name is LName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divLName.Visible = true;
                }
                else
                {
                    divLName.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Lname, formtype);
                txtLName.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblLName.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtLName.Attributes.Add("required", "");
                    vcLName.Enabled = true;
                }
                else
                {
                    lblLName.Text = labelname;
                    //txtLName.Attributes.Remove("required");
                    vcLName.Enabled = false;
                }
            }
            #endregion

            #region set divOName visibility is true or false if form_input_name is OName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divOName.Visible = true;
                }
                else
                {
                    divOName.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_OName, formtype);
                labelname = "Address 2";//***
                txtOName.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblOName.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtOName.Attributes.Add("required", "");
                    vcOName.Enabled = true;
                }
                else
                {
                    lblOName.Text = "";// labelname;
                    //txtOName.Attributes.Remove("required");
                    vcOName.Enabled = false;
                }
            }
            #endregion

            #region set divPassNo visibility is true or false if form_input_name is PassNo according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divPassNo.Visible = true;
                }
                else
                {
                    divPassNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_PassNo, formtype);
                txtPassNo.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblPassNo.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtPassNo.Attributes.Add("required", "");
                    vcPassNo.Enabled = true;
                }
                else
                {
                    lblPassNo.Text = labelname;
                    //txtPassNo.Attributes.Remove("required");
                    vcPassNo.Enabled = false;
                }
            }
            #endregion

            #region set divIsReg visibility is true or false if form_input_name is IsReg according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _isReg)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divIsReg.Visible = true;
                }
                else
                {
                    divIsReg.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_isReg, formtype);
                if (isrequired == 1)
                {
                    lblIsReg.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //rbreg.Attributes.Add("required", "");
                }
                else
                {
                    lblIsReg.Text = labelname;
                    //rbreg.Attributes.Remove("required");
                }
            }
            #endregion

            #region set divRegSpecific visibility is true or false if form_input_name is RegSpecific according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divRegSpecific.Visible = true;
                }
                else
                {
                    divRegSpecific.Visible = false;
                }
            }
            #endregion

            #region set divIDNo visibility is true or false if form_input_name is IDNo according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)//MCR/SNB/PRN No.
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divIDNo.Visible = true;
                }
                else
                {
                    divIDNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_IDNo, formtype);
                txtIDNo.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblIDNo.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtIDNo.Attributes.Add("required", "");
                    vcIDNo.Enabled = true;
                }
                else
                {
                    lblIDNo.Text = labelname;
                    //txtIDNo.Attributes.Remove("required");
                    vcIDNo.Enabled = false;
                }
            }
            #endregion

            #region set divDesignation visibility is true or false if form_input_name is Designation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDesignation.Visible = true;
                }
                else
                {
                    divDesignation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Designation, formtype);
                txtDesignation.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblDesignation.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtDesignation.Attributes.Add("required", "");
                    vcDesig.Enabled = true;
                }
                else
                {
                    lblDesignation.Text = labelname;
                    //txtDesignation.Attributes.Remove("required");
                    vcDesig.Enabled = false;
                }
            }
            #endregion

            #region set divProfession visibility is true or false if form_input_name is Profession according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                divJobtitle.Visible = false;
                vcJobtitle.Enabled = false;
                divStudentType.Visible = false;
                vcStudentType.Enabled = false;
                divStudentOther.Visible = false;
                vcStudentOther.Enabled = false;
                divStudentUpload.Visible = false;
                divDoctor.Visible = false;

                if (isshow == 1)
                {
                    divProfession.Visible = true;
                }
                else
                {
                    divProfession.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Profession, formtype);
                if (isrequired == 1)
                {
                    lblProfession.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlProfession.Attributes.Add("required", "");
                    vcProfession.Enabled = true;
                }
                else
                {
                    lblProfession.Text = labelname;
                    //ddlProfession.Attributes.Remove("required");
                    vcProfession.Enabled = false;
                }
            }
            #endregion

            #region set divDepartment visibility is true or false if form_input_name is Department according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDepartment.Visible = true;
                }
                else
                {
                    divDepartment.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Department, formtype);
                if (isrequired == 1)
                {
                    lblDepartment.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlDepartment.Attributes.Add("required", "");
                    vcDeptm.Enabled = true;
                }
                else
                {
                    lblDepartment.Text = labelname;
                    //ddlDepartment.Attributes.Remove("required");
                    vcDeptm.Enabled = false;
                }
            }
            #endregion

            #region set divOrganization visibility is true or false if form_input_name is Company according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divOrganization.Visible = true;
                }
                else
                {
                    divOrganization.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Organization, formtype);
                if (isrequired == 1)
                {
                    lblOrganization.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlOrganization.Attributes.Add("required", "");
                    vcOrg.Enabled = true;
                }
                else
                {
                    lblOrganization.Text = labelname;
                    //ddlOrganization.Attributes.Remove("required");
                    vcOrg.Enabled = false;
                }
            }
            #endregion

            #region set divInstitution visibility is true or false if form_input_name is Institution according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divInstitution.Visible = true;
                }
                else
                {
                    divInstitution.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Institution, formtype);
                if (isrequired == 1)
                {
                    lblInstitution.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlInstitution.Attributes.Add("required", "");
                    vcInsti.Enabled = true;
                }
                else
                {
                    lblInstitution.Text = labelname;
                    //ddlInstitution.Attributes.Remove("required");
                    vcInsti.Enabled = false;
                }
            }
            #endregion

            #region set divAddress1 visibility is true or false if form_input_name is Address1 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress1.Visible = true;
                }
                else
                {
                    divAddress1.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Address1, formtype);
                txtAddress1.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblAddress1.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtAddress1.Attributes.Add("required", "");
                    vcAddress1.Enabled = true;
                }
                else
                {
                    lblAddress1.Text = labelname;
                    //txtAddress1.Attributes.Remove("required");
                    vcAddress1.Enabled = false;
                }
            }
            #endregion

            #region set divAddress2 visibility is true or false if form_input_name is Address2 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress2.Visible = true;
                }
                else
                {
                    divAddress2.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : "";// frmObj.getDefaultLableNameByInputNameType(_Address2, formtype);
                txtAddress2.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblAddress2.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtAddress2.Attributes.Add("required", "");
                    vcAddress2.Enabled = true;
                }
                else
                {
                    lblAddress2.Text = labelname;
                    //txtAddress2.Attributes.Remove("required");
                    vcAddress2.Enabled = false;
                }
            }
            #endregion

            #region set divAddress3 visibility is true or false if form_input_name is Address3 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress3.Visible = true;
                }
                else
                {
                    divAddress3.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : "";// frmObj.getDefaultLableNameByInputNameType(_Address3, formtype);
                txtAddress3.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblAddress3.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtAddress3.Attributes.Add("required", "");
                    vcAddress3.Enabled = true;
                }
                else
                {
                    lblAddress3.Text = labelname;
                    //txtAddress3.Attributes.Remove("required");
                    vcAddress3.Enabled = false;
                }
            }
            #endregion

            #region set divAddress4 visibility is true or false if form_input_name is Address4 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress4.Visible = true;
                }
                else
                {
                    divAddress4.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : "";// frmObj.getDefaultLableNameByInputNameType(_Address4, formtype);
                txtAddress4.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblAddress4.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtAddress4.Attributes.Add("required", "");
                    vcAddress4.Enabled = true;
                }
                else
                {
                    lblAddress4.Text = labelname;
                    //txtAddress4.Attributes.Remove("required");
                    vcAddress4.Enabled = false;
                }
            }
            #endregion

            #region set divCity visibility is true or false if form_input_name is City according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCity.Visible = true;
                }
                else
                {
                    divCity.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_City, formtype);
                txtCity.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblCity.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtCity.Attributes.Add("required", "");
                    vcCity.Enabled = true;
                }
                else
                {
                    lblCity.Text = labelname;
                    //txtCity.Attributes.Remove("required");
                    vcCity.Enabled = false;
                }
            }
            #endregion

            #region set divState visibility is true or false if form_input_name is State according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divState.Visible = true;
                }
                else
                {
                    divState.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_State, formtype);
                txtState.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblState.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtState.Attributes.Add("required", "");
                    vcState.Enabled = true;
                }
                else
                {
                    lblState.Text = labelname;
                    //txtState.Attributes.Remove("required");
                    vcState.Enabled = false;
                }
            }
            #endregion

            #region set divPostalcode visibility is true or false if form_input_name is Postal Code according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divPostalcode.Visible = true;
                }
                else
                {
                    divPostalcode.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_PostalCode, formtype);
                txtPostalcode.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblPostalcode.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtPostalcode.Attributes.Add("required", "");
                    vcPostalcode.Enabled = true;
                }
                else
                {
                    lblPostalcode.Text = labelname;
                    //txtPostalcode.Attributes.Remove("required");
                    vcPostalcode.Enabled = false;
                }
            }
            #endregion

            #region set divCountry visibility is true or false if form_input_name is Country according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCountry.Visible = true;
                }
                else
                {
                    divCountry.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Country, formtype);
                if (isrequired == 1)
                {
                    lblCountry.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlCountry.Attributes.Add("required", "");
                    vcCountry.Enabled = true;
                }
                else
                {
                    lblCountry.Text = labelname;
                    //ddlCountry.Attributes.Remove("required");
                    vcCountry.Enabled = false;
                }
            }
            #endregion

            #region set divRCountry visibility is true or false if form_input_name is RCountry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divRCountry.Visible = true;
                }
                else
                {
                    divRCountry.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_RCountry, formtype);
                if (isrequired == 1)
                {
                    lblRCountry.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlRCountry.Attributes.Add("required", "");
                    vcRCountry.Enabled = true;
                }
                else
                {
                    lblRCountry.Text = labelname;
                    //ddlRCountry.Attributes.Remove("required");
                    vcRCountry.Enabled = false;
                }
            }
            #endregion

            #region set divTelcc visibility is true or false if form_input_name is Telcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelcc.Visible = true;
                }
                else
                {
                    divTelcc.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Telcc, formtype);
                txtTelcc.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    //txtTelcc.Attributes.Add("required", "");
                    vcTelcc.Enabled = true;
                    //ftbTelcc.Enabled = true;
                }
                else
                {
                    //txtTelcc.Attributes.Remove("required");
                    vcTelcc.Enabled = false;
                    //ftbTelcc.Enabled = false;
                }
            }
            #endregion

            #region set divTelac visibility is true or false if form_input_name is Telac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelac.Visible = true;
                }
                else
                {
                    divTelac.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Telac, formtype);
                txtTelac.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    //txtTelac.Attributes.Add("required", "");
                    vcTelac.Enabled = true;
                    //ftbTelac.Enabled = true;
                }
                else
                {
                    //txtTelac.Attributes.Remove("required");
                    vcTelac.Enabled = false;
                    //ftbTelac.Enabled = false;
                }
            }
            #endregion

            #region set divTel visibility is true or false if form_input_name is Tel according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTel.Visible = true;
                    divTelNo.Visible = true;
                }
                else
                {
                    divTel.Visible = false;
                    divTelNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Tel, formtype);
                txtTel.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblTel.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtTel.Attributes.Add("required", "");
                    vcTel.Enabled = true;
                    //ftbTel.Enabled = true;
                }
                else
                {
                    lblTel.Text = labelname;
                    //txtTel.Attributes.Remove("required");
                    vcTel.Enabled = false;
                    //ftbTel.Enabled = false;
                }
            }
            #endregion

            #region set divMobcc visibility is true or false if form_input_name is Mobilecc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobcc.Visible = true;
                }
                else
                {
                    divMobcc.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Mobilecc, formtype);
                txtMobcc.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    //txtMobcc.Attributes.Add("required", "");
                    vcMobcc.Enabled = true;
                    //ftbMobcc.Enabled = true;
                }
                else
                {
                    //txtMobcc.Attributes.Remove("required");
                    vcMobcc.Enabled = false;
                    //ftbMobcc.Enabled = false;
                }
            }
            #endregion

            #region set divMobac visibility is true or false if form_input_name is Mobileac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobac.Visible = true;
                }
                else
                {
                    divMobac.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Mobileac, formtype);
                txtMobac.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    //txtMobac.Attributes.Add("required", "");
                    vcMobac.Enabled = true;
                    //ftbMobac.Enabled = true;
                }
                else
                {
                    //txtMobac.Attributes.Remove("required");
                    vcMobac.Enabled = false;
                    //ftbMobac.Enabled = false;
                }
            }
            #endregion

            #region set divMobile visibility is true or false if form_input_name is Mobile according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobile.Visible = true;
                    divMobileNo.Visible = true;
                }
                else
                {
                    divMobile.Visible = false;
                    divMobileNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Mobile, formtype);
                txtMobile.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblMobile.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtMobile.Attributes.Add("required", "");
                    vcMob.Enabled = true;
                    //ftbMobile.Enabled = true;
                }
                else
                {
                    lblMobile.Text = labelname;
                    //txtMobile.Attributes.Remove("required");
                    vcMob.Enabled = false;
                    //ftbMobile.Enabled = false;
                }
            }
            #endregion

            #region set divFaxcc visibility is true or false if form_input_name is Faxcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxcc.Visible = true;
                }
                else
                {
                    divFaxcc.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Faxcc, formtype);
                txtFaxcc.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    //txtFaxcc.Attributes.Add("required", "");
                    vcFaxcc.Enabled = true;
                    //ftbFaxcc.Enabled = true;
                }
                else
                {
                    //txtFaxcc.Attributes.Remove("required");
                    vcFaxcc.Enabled = false;
                    //ftbFaxcc.Enabled = false;
                }
            }
            #endregion

            #region set divFaxac visibility is true or false if form_input_name is Faxac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxac.Visible = true;
                }
                else
                {
                    divFaxac.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Faxac, formtype);
                txtFaxac.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    //txtFaxac.Attributes.Add("required", "");
                    vcFaxac.Enabled = true;
                    //ftbFaxac.Enabled = true;
                }
                else
                {
                    //txtFaxac.Attributes.Remove("required");
                    vcFaxac.Enabled = false;
                    //ftbFaxac.Enabled = false;
                }
            }
            #endregion

            #region set divFax visibility is true or false if form_input_name is Fax according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFax.Visible = true;
                    divFaxNo.Visible = true;
                }
                else
                {
                    divFax.Visible = false;
                    divFaxNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Fax, formtype);
                txtFax.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblFax.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtFax.Attributes.Add("required", "");
                    vcFax.Enabled = true;
                    //ftbFax.Enabled = true;
                }
                else
                {
                    lblFax.Text = labelname;
                    //txtFax.Attributes.Remove("required");
                    vcFax.Enabled = false;
                    //ftbFax.Enabled = false;
                }
            }
            #endregion

            #region set divEmail visibility is true or false if form_input_name is Email according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmail.Visible = true;
                }
                else
                {
                    divEmail.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Email, formtype);
                txtEmail.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblEmail.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtEmail.Attributes.Add("required", "");
                    vcEmail.Enabled = true;
                }
                else
                {
                    lblEmail.Text = labelname;
                    //txtEmail.Attributes.Remove("required");
                    vcEmail.Enabled = false;
                }
            }
            #endregion

            #region set divEmailConfirmation visibility is true or false if form_input_name is ConfirmEmail according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _EmailConfirmation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmailConfirmation.Visible = true;
                    //txtEmailConfirmation.Attributes.Add("required", "");
                }
                else
                {
                    divEmailConfirmation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_EmailConfirmation, formtype);
                txtEmailConfirmation.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblEmailConfirmation.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtEmailConfirmation.Attributes.Add("required", "");
                    vcEConfirm.Enabled = true;
                }
                else
                {
                    lblEmailConfirmation.Text = labelname;
                    //txtEmailConfirmation.Attributes.Remove("required");
                    vcEConfirm.Enabled = false;
                }
            }
            #endregion

            #region set divAffiliation visibility is true or false if form_input_name is Affiliation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAffiliation.Visible = true;
                }
                else
                {
                    divAffiliation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Affiliation, formtype);
                if (isrequired == 1)
                {
                    lblAffiliation.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlAffiliation.Attributes.Add("required", "");
                    vcAffil.Enabled = true;
                }
                else
                {
                    lblAffiliation.Text = labelname;
                    //ddlAffiliation.Attributes.Remove("required");
                    vcAffil.Enabled = false;
                }
            }
            #endregion

            #region set divDietary visibility is true or false if form_input_name is Dietary according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDietary.Visible = true;
                }
                else
                {
                    divDietary.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Dietary, formtype);
                if (isrequired == 1)
                {
                    lblDietary.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlDietary.Attributes.Add("required", "");
                    vcDietary.Enabled = true;
                }
                else
                {
                    lblDietary.Text = labelname;
                    //ddlDietary.Attributes.Remove("required");
                    vcDietary.Enabled = false;
                }
            }
            #endregion

            #region set divNationality visibility is true or false if form_input_name is Nationality according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divNationality.Visible = true;
                }
                else
                {
                    divNationality.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Nationality, formtype);
                txtNationality.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblNationality.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtNationality.Attributes.Add("required", "");
                    vcNation.Enabled = true;
                }
                else
                {
                    lblNationality.Text = labelname;
                    //txtNationality.Attributes.Remove("required");
                    vcNation.Enabled = false;
                }
            }
            #endregion

            #region set divAge visibility is true or false if form_input_name is Age according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAge.Visible = true;
                }
                else
                {
                    divAge.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Age, formtype);
                txtAge.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblAge.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtAge.Attributes.Add("required", "");
                    vcAge.Enabled = true;
                }
                else
                {
                    lblAge.Text = labelname;
                    //txtAge.Attributes.Remove("required");
                    vcAge.Enabled = false;
                }
            }
            #endregion

            #region set divDOB visibility is true or false if form_input_name is DOB according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDOB.Visible = true;
                }
                else
                {
                    divDOB.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_DOB, formtype);
                txtDOB.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblDOB.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtDOB.Attributes.Add("required", "");
                    vcDOB.Enabled = true;
                }
                else
                {
                    lblDOB.Text = labelname;
                    //txtDOB.Attributes.Remove("required");
                    vcDOB.Enabled = false;
                }
            }
            #endregion

            #region set divGender visibility is true or false if form_input_name is Gender according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                if (isshow == 1)
                {
                    divGender.Visible = true;
                }
                else
                {
                    divGender.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Gender, formtype);
                if (isrequired == 1)
                {
                    lblGender.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlGender.Attributes.Add("required", "");
                    //vcGender.Enabled = true;
                }
                else
                {
                    lblGender.Text = labelname;
                    //ddlGender.Attributes.Remove("required");
                    //vcGender.Enabled = false;
                }
            }
            #endregion

            #region set divMemberNo visibility is true or false if form_input_name is Membership No according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMemberNo.Visible = true;
                }
                else
                {
                    divMemberNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_MembershipNo, formtype);
                txtMemberNo.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblMemberNo.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtMemberNo.Attributes.Add("required", "");
                    vcMember.Enabled = true;
                }
                else
                {
                    lblMemberNo.Text = labelname;
                    //txtMemberNo.Attributes.Remove("required");
                    vcMember.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional4 visibility is true or false if form_input_name is Additional4 No according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional4.Visible = true;
                }
                else
                {
                    divAdditional4.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Additional4, formtype);
                txtAdditional4.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblAdditional4.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtAdditional4.Attributes.Add("required", "");
                    vcAdditional4.Enabled = true;
                }
                else
                {
                    lblAdditional4.Text = labelname;
                    //txtAdditional4.Attributes.Remove("required");
                    vcAdditional4.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional5 visibility is true or false if form_input_name is Additional5 No according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional5.Visible = true;
                }
                else
                {
                    divAdditional5.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Additional5, formtype);
                txtAdditional5.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblAdditional5.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtAdditional5.Attributes.Add("required", "");
                    vcAdditional5.Enabled = true;
                }
                else
                {
                    lblAdditional5.Text = labelname;
                    //txtAdditional5.Attributes.Remove("required");
                    vcAdditional5.Enabled = false;
                }
            }
            #endregion

            #region set divVName visibility is true or false if form_input_name is VName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVName.Visible = true;
                    isVisitorVisible ++;
                }
                else
                {
                    if(isVisitorVisible == 0)
                    {
                        divVName.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VName, formtype);
                txtVName.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblVName.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtVName.Attributes.Add("required", "");
                    vcVName.Enabled = true;
                }
                else
                {
                    lblVName.Text = labelname;
                    //txtVName.Attributes.Remove("required");
                    vcVName.Enabled = false;
                }
            }
            #endregion

            #region set divVDOB visibility is true or false if form_input_name is VDOB according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVDOB.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVDOB.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VDOB, formtype);
                txtVDOB.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblVDOB.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtVDOB.Attributes.Add("required", "");
                    vcVDOB.Enabled = true;
                }
                else
                {
                    lblVDOB.Text = labelname;
                    //txtVDOB.Attributes.Remove("required");
                    vcVDOB.Enabled = false;
                }
            }
            #endregion

            #region set divVPassNo visibility is true or false if form_input_name is VPassNo according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVPassNo.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVPassNo.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VPassNo, formtype);
                txtVPassNo.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblVPassNo.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtVPassNo.Attributes.Add("required", "");
                    vcVPassNo.Enabled = true;
                }
                else
                {
                    lblVPassNo.Text = labelname;
                    //txtVPassNo.Attributes.Remove("required");
                    vcVPassNo.Enabled = false;
                }
            }
            #endregion

            #region set divVPassExpiry visibility is true or false if form_input_name is VPassExpiry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVPassExpiry.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVPassExpiry.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VPassExpiry, formtype);
                txtVPassExpiry.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblVPassExpiry.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtVPassExpiry.Attributes.Add("required", "");
                    vcVPExpiry.Enabled = true;
                }
                else
                {
                    lblVPassExpiry.Text = labelname;
                    //txtVPassExpiry.Attributes.Remove("required");
                    vcVPExpiry.Enabled = false;
                }
            }
            #endregion

            #region set divVPassIssueDate visibility is true or false if form_input_name is VPassIssueDate according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVPassIssueDate.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVPassIssueDate.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VPassIssueDate, formtype);
                txtVPassIssueDate.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblVPassIssueDate.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtVPassIssueDate.Attributes.Add("required", "");
                    vcVPassIssueDate.Enabled = true;
                }
                else
                {
                    lblVPassIssueDate.Text = labelname;
                    //txtVPassIssueDate.Attributes.Remove("required");
                    vcVPassIssueDate.Enabled = false;
                }
            }
            #endregion

            #region set divVEmbarkation visibility is true or false if form_input_name is VEmbarkation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVEmbarkation.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVEmbarkation.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VEmbarkation, formtype);
                txtVEmbarkation.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblVEmbarkation.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtVEmbarkation.Attributes.Add("required", "");
                    vcVEmbarkation.Enabled = true;
                }
                else
                {
                    lblVEmbarkation.Text = labelname;
                    //txtVEmbarkation.Attributes.Remove("required");
                    vcVEmbarkation.Enabled = false;
                }
            }
            #endregion

            #region set divVArrivalDate visibility is true or false if form_input_name is VArrivalDate according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVArrivalDate.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVArrivalDate.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VArrivalDate, formtype);
                txtVArrivalDate.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblVArrivalDate.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtVArrivalDate.Attributes.Add("required", "");
                    vcVArrivalDate.Enabled = true;
                }
                else
                {
                    lblVArrivalDate.Text = labelname;
                    //txtVArrivalDate.Attributes.Remove("required");
                    vcVArrivalDate.Enabled = false;
                }
            }
            #endregion

            #region set divVCountry visibility is true or false if form_input_name is VCountry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVCountry.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVCountry.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VCountry, formtype);
                if (isrequired == 1)
                {
                    lblVCountry.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlVCountry.Attributes.Add("required", "");
                    vcVCountry.Enabled = true;
                }
                else
                {
                    lblVCountry.Text = labelname;
                    //ddlVCountry.Attributes.Remove("required");
                    vcVCountry.Enabled = false;
                }
            }
            #endregion

            #region set divUDFCName visibility is true or false if form_input_name is UDF_CName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFCName.Visible = true;
                    isUDFVisible ++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFCName.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_UDF_CName, formtype);
                txtUDFCName.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblUDFCName.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtUDFCName.Attributes.Add("required", "");
                    vcUDFCName.Enabled = true;
                }
                else
                {
                    lblUDFCName.Text = labelname;
                    //txtUDFCName.Attributes.Remove("required");
                    vcUDFCName.Enabled = false;
                }
            }
            #endregion

            #region set divUDFDelType visibility is true or false if form_input_name is UDF_DelegateType according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFDelType.Visible = true;
                    isUDFVisible ++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFDelType.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_UDF_DelegateType, formtype);
                if (isrequired == 1)
                {
                    lblUDFDelType.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtUDFDelType.Attributes.Add("required", "");
                    vcUDFDelType.Enabled = true;
                }
                else
                {
                    lblUDFDelType.Text = labelname;
                    //txtUDFDelType.Attributes.Remove("required");
                    vcUDFDelType.Enabled = false;
                }
            }
            #endregion

            #region set divUDFProCategory visibility is true or false if form_input_name is UDF_ProfCategory according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFProCategory.Visible = true;
                    isUDFVisible ++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFProCategory.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_UDF_ProfCategory, formtype);
                if (isrequired == 1)
                {
                    lblUDFProCategory.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlUDFProCategory.Attributes.Add("required", "");
                    vcUDFProCat.Enabled = true;
                }
                else
                {
                    lblUDFProCategory.Text = labelname;
                    //ddlUDFProCategory.Attributes.Remove("required");
                    vcUDFProCat.Enabled = false;
                }
            }
            #endregion

            #region set divUDFCpostalcode visibility is true or false if form_input_name is UDF_CPcode according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFCpostalcode.Visible = true;
                    isUDFVisible ++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFCpostalcode.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_UDF_CPcode, formtype);
                txtUDFCpostalcode.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblUDFCpostalcode.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtUDFCpostalcode.Attributes.Add("required", "");
                    vcUDFCpcode.Enabled = true;
                }
                else
                {
                    lblUDFCpostalcode.Text = labelname;
                    //txtUDFCpostalcode.Attributes.Remove("required");
                    vcUDFCpcode.Enabled = false;
                }
            }
            #endregion

            #region set divUDFCLDept visibility is true or false if form_input_name is UDF_CLDepartment according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFCLDept.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFCLDept.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_UDF_CLDepartment, formtype);
                if (isrequired == 1)
                {
                    lblUDFCLDept.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtUDFCLDept.Attributes.Add("required", "");
                    vcUDFCLDept.Enabled = true;
                }
                else
                {
                    lblUDFCLDept.Text = labelname;
                    //txtUDFCLDept.Attributes.Remove("required");
                    vcUDFCLDept.Enabled = false;
                }
            }
            #endregion

            #region set divUDFAddress visibility is true or false if form_input_name is UDF_CAddress according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFAddress.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFAddress.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_UDF_CAddress, formtype);
                txtUDFAddress.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblUDFAddress.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtUDFAddress.Attributes.Add("required", "");
                    vcUDFAddress.Enabled = true;
                }
                else
                {
                    lblUDFAddress.Text = labelname;
                    //txtUDFAddress.Attributes.Remove("required");
                    vcUDFAddress.Enabled = false;
                }
            }
            #endregion

            #region set divUDFCLCom visibility is true or false if form_input_name is UDF_CLCompany according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFCLCom.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFCLCom.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_UDF_CLCompany, formtype);
                if (isrequired == 1)
                {
                    lblUDFCLCom.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlUDFCLCom.Attributes.Add("required", "");
                    vcUDFCLCom.Enabled = true;
                }
                else
                {
                    lblUDFCLCom.Text = labelname;
                    //ddlUDFCLCom.Attributes.Remove("required");
                    vcUDFCLCom.Enabled = false;
                }
            }
            #endregion

            #region set divUDFCCountry visibility is true or false if form_input_name is UDF_CCountry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFCCountry.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFCCountry.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_UDF_CCountry, formtype);
                if (isrequired == 1)
                {
                    lblUDFCCountry.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //ddlUDFCCountry.Attributes.Add("required", "");
                    vcUDFCCountry.Enabled = true;
                }
                else
                {
                    lblUDFCCountry.Text = labelname;
                    //ddlUDFCCountry.Attributes.Remove("required");
                    vcUDFCCountry.Enabled = false;
                }
            }
            #endregion

            #region set divSupName visibility is true or false if form_input_name is Supervisor Name according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divSuperVisor.Visible = true;
                    divSupName.Visible = true;
                    isSupervisorVisible ++;
                }
                else
                {
                    if (isSupervisorVisible == 0)
                    {
                        divSupName.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_SupName, formtype);
                txtSupName.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblSupName.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtSupName.Attributes.Add("required", "");
                    vcSupName.Enabled = true;
                }
                else
                {
                    lblSupName.Text = labelname;
                    //txtSupName.Attributes.Remove("required");
                    vcSupName.Enabled = false;
                }
            }
            #endregion

            #region set divSupDesignation visibility is true or false if form_input_name is Supervisor Designation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divSuperVisor.Visible = true;
                    divSupDesignation.Visible = true;
                    isSupervisorVisible++;
                }
                else
                {
                    if (isSupervisorVisible == 0)
                    {
                        divSupDesignation.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_SupDesignation, formtype);
                txtSupDesignation.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblSupDesignation.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtSupDesignation.Attributes.Add("required", "");
                    vcSupDes.Enabled = true;
                }
                else
                {
                    lblSupDesignation.Text = labelname;
                    //txtSupDesignation.Attributes.Remove("required");
                    vcSupDes.Enabled = false;
                }
            }
            #endregion

            #region set divSupContact visibility is true or false if form_input_name is Supervisor Contact according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divSuperVisor.Visible = true;
                    divSupContact.Visible = true;
                    isSupervisorVisible++;
                }
                else
                {
                    if (isSupervisorVisible == 0)
                    {
                        divSupContact.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_SupContact, formtype);
                txtSupContact.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblSupContact.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtSupContact.Attributes.Add("required", "");
                    vcSupContact.Enabled = true;
                }
                else
                {
                    lblSupContact.Text = labelname;
                    //txtSupContact.Attributes.Remove("required");
                    vcSupContact.Enabled = false;
                }
            }
            #endregion

            #region set divSupEmail visibility is true or false if form_input_name is Supervisor Email according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divSuperVisor.Visible = true;
                    divSupEmail.Visible = true;
                    isSupervisorVisible++;
                }
                else
                {
                    if (isSupervisorVisible == 0)
                    {
                        divSupEmail.Visible = false;
                    }
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_SupEmail, formtype);
                txtSupEmail.Attributes.Add("placeholder", labelname);
                if (isrequired == 1)
                {
                    lblSupEmail.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; ";;
                    //txtSupEmail.Attributes.Add("required", "");
                    vcSupEmail.Enabled = true;
                }
                else
                {
                    lblSupEmail.Text = labelname;
                    //txtSupEmail.Attributes.Remove("required");
                    vcSupEmail.Enabled = false;
                }
            }
            #endregion
        }

        setDivCss_TelMobFax(divTelcc.Visible, divTelac.Visible, divTelNo.Visible, "Tel");//*
        setDivCss_TelMobFax(divMobcc.Visible, divMobac.Visible, divMobileNo.Visible, "Mob");//*
        setDivCss_TelMobFax(divFaxcc.Visible, divFaxac.Visible, divFaxNo.Visible, "Fax");//*

        return isValidShow;
    }
    #endregion

    #region populateUserDetails (get all relevant data according to RegGroupID and Regno from tb_RegDelegate and bind data to the respective controls)
    private void populateUserDetails(string id, string regno, string showid, string flowid)
    {
        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(regno))
        {
            RegDelegateObj rgd = new RegDelegateObj(fn);
            dt = rgd.getDataByGroupIDRegno(id, regno, showid);
            if (dt.Rows.Count != 0)
            {
                //string regno = dt.Rows[0]["Regno"].ToString();
                string reggroupid = dt.Rows[0]["RegGroupID"].ToString();
                string con_categoryid = dt.Rows[0]["con_CategoryId"].ToString();
                string reg_salutation = dt.Rows[0]["reg_Salutation"].ToString();
                string reg_fname = dt.Rows[0]["reg_FName"].ToString();
                string reg_lname = dt.Rows[0]["reg_LName"].ToString();
                string reg_oname = dt.Rows[0]["reg_OName"].ToString();
                string passno = dt.Rows[0]["reg_PassNo"].ToString();
                string reg_isreg = dt.Rows[0]["reg_isReg"].ToString();
                string reg_sgregistered = dt.Rows[0]["reg_sgregistered"].ToString();
                string reg_idno = dt.Rows[0]["reg_IDno"].ToString();
                string reg_staffid = dt.Rows[0]["reg_staffid"].ToString();
                string reg_designation = dt.Rows[0]["reg_Designation"].ToString();
                string reg_profession = dt.Rows[0]["reg_Profession"].ToString();
                string reg_jobtitle_alliedstu = dt.Rows[0]["reg_Jobtitle_alliedstu"].ToString();
                string reg_department = dt.Rows[0]["reg_Department"].ToString();
                string reg_organization = dt.Rows[0]["reg_Organization"].ToString();
                string reg_institution = dt.Rows[0]["reg_Institution"].ToString();
                string reg_address1 = dt.Rows[0]["reg_Address1"].ToString();
                string reg_address2 = dt.Rows[0]["reg_Address2"].ToString();
                string reg_address3 = dt.Rows[0]["reg_Address3"].ToString();
                string reg_address4 = dt.Rows[0]["reg_Address4"].ToString();
                string reg_city = dt.Rows[0]["reg_City"].ToString();
                string reg_state = dt.Rows[0]["reg_State"].ToString();
                string reg_postalcode = dt.Rows[0]["reg_PostalCode"].ToString();
                string reg_country = dt.Rows[0]["reg_Country"].ToString();
                string reg_rcountry = dt.Rows[0]["reg_RCountry"].ToString();
                string reg_telcc = dt.Rows[0]["reg_Telcc"].ToString();
                string reg_telac = dt.Rows[0]["reg_Telac"].ToString();
                string reg_tel = dt.Rows[0]["reg_Tel"].ToString();
                string reg_mobcc = dt.Rows[0]["reg_Mobcc"].ToString();
                string reg_mobac = dt.Rows[0]["reg_Mobac"].ToString();
                string reg_mobile = dt.Rows[0]["reg_Mobile"].ToString();
                string reg_faxcc = dt.Rows[0]["reg_Faxcc"].ToString();
                string reg_faxac = dt.Rows[0]["reg_Faxac"].ToString();
                string reg_fax = dt.Rows[0]["reg_Fax"].ToString();
                string reg_email = dt.Rows[0]["reg_Email"].ToString();
                string reg_affiliation = dt.Rows[0]["reg_Affiliation"].ToString();
                string reg_dietary = dt.Rows[0]["reg_Dietary"].ToString();
                string reg_nationality = dt.Rows[0]["reg_Nationality"].ToString();
                string reg_membershipno = dt.Rows[0]["reg_Membershipno"].ToString();
                string reg_vname = dt.Rows[0]["reg_vName"].ToString();
                string reg_vdob = dt.Rows[0]["reg_vDOB"].ToString();
                string reg_vpassno = dt.Rows[0]["reg_vPassno"].ToString();
                string reg_vpassexpiry = dt.Rows[0]["reg_vPassexpiry"].ToString();
                string reg_vpassissuedate = dt.Rows[0]["reg_vIssueDate"].ToString();
                string reg_vembarkation = dt.Rows[0]["reg_vEmbarkation"].ToString();
                string reg_varrivaldate = dt.Rows[0]["reg_vArrivalDate"].ToString();
                string reg_vcountry = dt.Rows[0]["reg_vCountry"].ToString();
                string udf_delegatetype = dt.Rows[0]["UDF_DelegateType"].ToString();
                string udf_profcategory = dt.Rows[0]["UDF_ProfCategory"].ToString();
                string udf_profcategoryother = dt.Rows[0]["UDF_ProfCategoryOther"].ToString();
                string udf_cname = dt.Rows[0]["UDF_CName"].ToString();
                string udf_cpcode = dt.Rows[0]["UDF_CPcode"].ToString();
                string udf_cldepartment = dt.Rows[0]["UDF_CLDepartment"].ToString();
                string udf_caddress = dt.Rows[0]["UDF_CAddress"].ToString();
                string udf_clcompany = dt.Rows[0]["UDF_CLCompany"].ToString();
                string udf_clcompanyother = dt.Rows[0]["UDF_CLCompanyOther"].ToString();
                string udf_ccountry = dt.Rows[0]["UDF_CCountry"].ToString();
                string reg_supervisorname = dt.Rows[0]["reg_SupervisorName"].ToString();
                string reg_supervisordesignation = dt.Rows[0]["reg_SupervisorDesignation"].ToString();
                string reg_supervisorcontact = dt.Rows[0]["reg_SupervisorContact"].ToString();
                string reg_supervisoremail = dt.Rows[0]["reg_SupervisorEmail"].ToString();
                string reg_salutationothers = dt.Rows[0]["reg_SalutationOthers"].ToString();
                string reg_otherprofession = dt.Rows[0]["reg_otherProfession"].ToString();
                string reg_otherdepartment = dt.Rows[0]["reg_otherDepartment"].ToString();
                string reg_otherorganization = dt.Rows[0]["reg_otherOrganization"].ToString();
                string reg_otherinstitution = dt.Rows[0]["reg_otherInstitution"].ToString();
                string reg_aemail = dt.Rows[0]["reg_aemail"].ToString();
                string reg_remark = dt.Rows[0]["reg_remark"].ToString();
                string reg_remarkgupload = dt.Rows[0]["reg_remarkGUpload"].ToString();
                string re_issms = dt.Rows[0]["reg_isSMS"].ToString();
                string reg_approvestatus = dt.Rows[0]["reg_approveStatus"].ToString();
                string reg_datecreated = dt.Rows[0]["reg_datecreated"].ToString();
                string recycle = dt.Rows[0]["recycle"].ToString();
                string reg_stage = dt.Rows[0]["reg_Stage"].ToString();

                string reg_age = dt.Rows[0]["reg_Age"].ToString();
                string reg_dob = dt.Rows[0]["reg_DOB"].ToString();
                string reg_gender = dt.Rows[0]["reg_Gender"].ToString();
                string reg_additional4 = dt.Rows[0]["reg_Additional4"].ToString();
                string reg_additional5 = dt.Rows[0]["reg_Additional5"].ToString();

                lblSpeRegno.Text = reg_membershipno;

                try
                {
                    if (!String.IsNullOrEmpty(reg_salutation))
                    {
                        ListItem listItem = ddlSalutation.Items.FindByValue(reg_salutation);
                        if (listItem != null)
                        {
                            ddlSalutation.ClearSelection();
                            listItem.Selected = true;
                        }

                        ddlSalutation_SelectedIndexChanged(this, null);
                    }
                }
                catch (Exception ex)
                {
                }
                txtSalOther.Text = reg_salutationothers;

                txtFName.Text = reg_fname;
                txtLName.Text = reg_lname;
                txtOName.Text = reg_oname;
                txtPassNo.Text = passno;

                ////***isreg is used for isUpdated pre-populate data
                //if (reg_isreg == Number.One)
                //{
                //    rbreg.Items[0].Selected = true;
                //    rbreg.Items[1].Selected = false;
                //}
                //else
                //{
                //    rbreg.Items[0].Selected = false;
                //    rbreg.Items[1].Selected = true;
                //}
                ////***isreg is used for isUpdated pre-populate data

                try
                {
                    if (!String.IsNullOrEmpty(reg_sgregistered))
                    {
                        ListItem listItem = rbregspecific.Items.FindByValue(reg_sgregistered);
                        if (listItem != null)
                        {
                            rbregspecific.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                txtIDNo.Text = reg_idno;

                if (reg_profession != "")
                {
                    if (!String.IsNullOrEmpty(reg_profession))
                    {
                        ListItem listItem = ddlProfession.Items.FindByValue(reg_profession);
                        if (listItem != null)
                        {
                            ddlProfession.ClearSelection();
                            listItem.Selected = true;
                        }
                        ddlProfession_SelectedIndexChanged(this, null);
                    }
                }
                txtProOther.Text = reg_otherprofession;
                txtDesignation.Text = reg_designation;
                txtJobtitle.Text = reg_jobtitle_alliedstu;

                try
                {
                    if (!String.IsNullOrEmpty(reg_organization))
                    {
                        ListItem listItem = ddlOrganization.Items.FindByValue(reg_organization);
                        if (listItem != null)
                        {
                            ddlOrganization.ClearSelection();
                            listItem.Selected = true;
                        }
                        ddlOrganization_SelectedIndexChanged(this, null);
                    }
                }
                catch (Exception ex)
                {
                }
                txtOrgOther.Text = reg_otherorganization;

                try
                {
                    if (!String.IsNullOrEmpty(reg_institution))
                    {
                        ListItem listItem = ddlInstitution.Items.FindByValue(reg_institution);
                        if (listItem != null)
                        {
                            ddlInstitution.ClearSelection();
                            listItem.Selected = true;
                        }
                        ddlInstitution_SelectedIndexChanged(this, null);
                    }
                }
                catch (Exception ex)
                {
                }
                txtInstiOther.Text = reg_otherinstitution;

                try
                {
                    if (!String.IsNullOrEmpty(reg_department))
                    {
                        ListItem listItem = ddlDepartment.Items.FindByValue(reg_department);
                        if (listItem != null)
                        {
                            ddlDepartment.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                txtDepartmentOther.Text = reg_otherdepartment;

                txtAddress1.Text = reg_address1;
                txtAddress2.Text = reg_address2;
                txtAddress3.Text = reg_address3;
                txtAddress4.Text = reg_address4;
                txtCity.Text = reg_city;
                txtState.Text = reg_state;
                txtPostalcode.Text = reg_postalcode;
                try
                {
                    if (!String.IsNullOrEmpty(reg_country))
                    {
                        bool isCountryID = reg_country.All(char.IsDigit);
                        if(!isCountryID)
                        {
                            CountryObj cObj = new CountryObj(fn);
                            reg_country = cObj.getCountryIDByName(reg_country, showid);
                        }
                        ListItem listItem = ddlCountry.Items.FindByValue(reg_country);
                        if (listItem != null)
                        {
                            ddlCountry.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                try
                {
                    if (!String.IsNullOrEmpty(reg_rcountry))
                    {
                        ListItem listItem = ddlRCountry.Items.FindByValue(reg_rcountry);
                        if (listItem != null)
                        {
                            ddlRCountry.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                txtTelcc.Text = reg_telcc;
                txtTelac.Text = reg_telac;
                txtTel.Text = reg_tel;
                txtMobcc.Text = reg_mobcc;
                txtMobac.Text = reg_mobac;
                txtMobile.Text = reg_mobile;
                txtFaxcc.Text = reg_faxcc;
                txtFaxac.Text = reg_faxac;
                txtFax.Text = reg_fax;
                txtEmail.Text = reg_email;
                txtEmailConfirmation.Text = reg_email;

                try
                {
                    if (!String.IsNullOrEmpty(reg_affiliation))
                    {
                        ListItem listItem = ddlAffiliation.Items.FindByValue(reg_affiliation);
                        if (listItem != null)
                        {
                            ddlAffiliation.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                try
                {
                    if (!String.IsNullOrEmpty(reg_dietary))
                    {
                        ListItem listItem = ddlDietary.Items.FindByValue(reg_dietary);
                        if (listItem != null)
                        {
                            ddlDietary.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                txtNationality.Text = reg_nationality;
                txtMemberNo.Text = reg_membershipno;

                txtVName.Text = reg_vname;
                txtVDOB.Text = reg_vdob;
                txtVPassNo.Text = reg_vpassno;
                txtVPassExpiry.Text = reg_vpassexpiry;
                txtVPassIssueDate.Text = reg_vpassissuedate;
                txtVEmbarkation.Text = reg_vembarkation;
                txtVArrivalDate.Text = reg_varrivaldate;
                try
                {
                    if (!String.IsNullOrEmpty(reg_vcountry))
                    {
                        ListItem listItem = ddlVCountry.Items.FindByValue(reg_vcountry);
                        if (listItem != null)
                        {
                            ddlVCountry.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                txtUDFDelType.Text = udf_delegatetype;
                try
                {
                    if (!String.IsNullOrEmpty(udf_profcategory))
                    {
                        ListItem listItem = ddlUDFProCategory.Items.FindByValue(udf_profcategory);
                        if (listItem != null)
                        {
                            ddlUDFProCategory.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                txtUDFProCatOther.Text = udf_profcategoryother;
                txtUDFCName.Text = udf_cname;
                txtUDFCpostalcode.Text = udf_cpcode;
                txtUDFCLDept.Text = udf_cldepartment;
                txtUDFAddress.Text = udf_caddress;
                try
                {
                    if (!String.IsNullOrEmpty(udf_clcompany))
                    {
                        ListItem listItem = ddlUDFCLCom.Items.FindByValue(udf_clcompany);
                        if (listItem != null)
                        {
                            ddlUDFCLCom.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                txtUDFCLComOther.Text = udf_clcompanyother;
                try
                {
                    if (!String.IsNullOrEmpty(udf_ccountry))
                    {
                        ListItem listItem = ddlUDFCCountry.Items.FindByValue(udf_ccountry);
                        if (listItem != null)
                        {
                            ddlUDFCCountry.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                txtSupName.Text = reg_supervisorname;
                txtSupDesignation.Text = reg_supervisordesignation;
                txtSupContact.Text = reg_supervisorcontact;
                txtSupEmail.Text = reg_supervisoremail;

                txtAge.Text = reg_age;
                txtDOB.Text = !string.IsNullOrEmpty(reg_dob) ? Convert.ToDateTime(reg_dob).ToString("dd/MM/yyyy") : "";
                try
                {
                    if (!String.IsNullOrEmpty(reg_gender))
                    {
                        ListItem listItem = ddlGender.Items.FindByValue(reg_gender);
                        if (listItem != null)
                        {
                            ddlGender.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                txtAdditional4.Text = reg_additional4;
                setEventCodeVisibility(ddlCountry.SelectedItem.Text, _Additional4, reg_additional4);//***for OSEA

                txtAdditional5.Text = reg_additional5;
                //setEventCodeVisibility(ddlCountry.SelectedItem.Text, _Additional5, reg_additional5);//***for OSEA
                PromoController prm = new PromoController(fn);
                DataTable promoList = prm.getPromoData(reg_additional5, showid, flowid);
                if (promoList.Rows.Count > 0)
                {
                    txtAdditional5.Enabled = false;
                    lblIsVIPCodeUsed.Text = "1";
                }

                lblIsRegisterColleague.Text = !string.IsNullOrEmpty(reg_remark) && reg_remark != "0" ? reg_remark : "";
            }
        }
    }
    #endregion

    #region ClearForm (clear data from controls)
    private void ClearForm()
    {
        ddlSalutation.SelectedIndex = 0;
        txtSalOther.Text = "";
        txtFName.Text = "";
        txtLName.Text = "";
        txtOName.Text = "";
        txtPassNo.Text = "";
        rbreg.SelectedIndex = 1;
        rbregspecific.ClearSelection();
        txtIDNo.Text = "";
        ddlProfession.SelectedIndex = 0;
        txtProOther.Text = "";
        ddlStudentType.SelectedIndex = 0;
        txtStudentOther.Text = "";
        txtJobtitle.Text = "";
        txtDesignation.Text = "";
        ddlDepartment.SelectedIndex = 0;
        txtDepartmentOther.Text = "";
        ddlOrganization.SelectedIndex = 0;
        txtOrgOther.Text = "";
        ddlInstitution.SelectedIndex = 0;
        txtInstiOther.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtAddress4.Text = "";
        txtCity.Text = "";
        txtPostalcode.Text = "";
        txtState.Text = "";
        ddlCountry.SelectedIndex = 0;
        ddlRCountry.SelectedIndex = 0;
        txtTelcc.Text = "";
        txtTelac.Text = "";
        txtTel.Text = "";
        txtMobcc.Text = "";
        txtMobac.Text = "";
        txtMobile.Text = "";
        txtFaxcc.Text = "";
        txtFaxac.Text = "";
        txtFax.Text = "";
        txtEmail.Text = "";
        ddlAffiliation.SelectedIndex = 0;
        ddlDietary.SelectedIndex = 0;
        txtNationality.Text = "";
        txtAge.Text = "";
        txtDOB.Text = "";
        ddlGender.SelectedIndex = 0;
        txtAdditional4.Text = "";
        txtAdditional5.Text = "";
        txtMemberNo.Text = "";
        txtVName.Text = "";
        txtVDOB.Text = "";
        txtVPassNo.Text = "";
        txtVPassExpiry.Text = "";
        txtVPassIssueDate.Text = "";
        txtVEmbarkation.Text = "";
        txtVArrivalDate.Text = "";
        ddlVCountry.SelectedIndex = 0;
        txtUDFCName.Text = "";
        txtUDFDelType.Text = "";
        ddlUDFProCategory.SelectedIndex = 0;
        txtUDFProCatOther.Text = "";
        txtUDFCpostalcode.Text = "";
        txtUDFCLDept.Text = "";
        txtUDFAddress.Text = "";
        ddlUDFCLCom.SelectedIndex = 0;
        txtUDFCLComOther.Text = "";
        ddlUDFCCountry.SelectedIndex = 0;
        txtSupName.Text = "";
        txtSupDesignation.Text = "";
        txtSupContact.Text = "";
        txtSupEmail.Text = "";
    }
    #endregion

    #region ddlSalutation_SelectedIndexChanged (set 'other salutation div' visibility if the selection of ddlSalutation dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherSal and form_type='D')
    protected void ddlSalutation_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlSalutation.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlSalutation.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherSal);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divSalOther.Visible = true;
                        }
                        else
                        {
                            divSalOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtSalOther.Attributes.Add("required", "");
                            vcSalOther.Enabled = true;
                        }
                        else
                        {
                            //txtSalOther.Attributes.Remove("required");
                            vcSalOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divSalOther.Visible = false;
                    //txtSalOther.Attributes.Remove("required");
                    vcSalOther.Enabled = false;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlCountry_SelectedIndexChanged (bind country code data to txtTelcc, txtMobcc, txtFaxcc textboxes according to the selected country)
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlCountry.Items.Count > 0)
            {
                string countryid = ddlCountry.SelectedItem.Value;

                if (ddlCountry.SelectedIndex == 0)
                {
                    countryid = Number.Zero;
                }

                CountryObj couObj = new CountryObj(fn);
                DataTable dt = couObj.getCountryByID(countryid);
                if (dt.Rows.Count > 0)
                {
                    string code = dt.Rows[0]["countryen"].ToString();

                    txtTelcc.Text = code;
                    txtMobcc.Text = code;
                    txtFaxcc.Text = code;

                    setEventCodeVisibility(ddlCountry.SelectedItem.Text, _Additional4);//***for OSEA
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlRCountry_SelectedIndexChanged (bind country code data to txtTelcc, txtMobcc, txtFaxcc textboxes according to the selected country)
    protected void ddlRCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlRCountry.Items.Count > 0)
            {
                string countryid = ddlRCountry.SelectedItem.Value;

                if (ddlRCountry.SelectedIndex == 0)
                {
                    countryid = Number.Zero;
                }

                CountryObj couObj = new CountryObj(fn);
                DataTable dt = couObj.getCountryByID(countryid);
                if (dt.Rows.Count > 0)
                {
                    string code = dt.Rows[0]["countryen"].ToString();

                    txtTelcc.Text = code;
                    txtMobcc.Text = code;
                    txtFaxcc.Text = code;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlProfession_SelectedIndexChanged (set 'other profession div' visibility if the selection of ddlProfession dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherProfession and form_type='D')
    protected void ddlProfession_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlProfession.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlProfession.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherProfession);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divProOther.Visible = true;
                        }
                        else
                        {
                            divProOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtProOther.Attributes.Add("required", "");
                            vcProOther.Enabled = true;
                        }
                        else
                        {
                            //txtProOther.Attributes.Remove("required");
                            vcProOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divProOther.Visible = false;
                    //txtProOther.Attributes.Remove("required");
                    vcProOther.Enabled = false;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlStudentType_SelectedIndexChanged (set 'other student div' visibility if the selection of ddlProfession dropdownlist is 'Other' or 'Others')
    protected void ddlStudentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlStudentType.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlStudentType.SelectedItem.Text))
                {
                    divStudentOther.Visible = true;
                    vcStudentOther.Enabled = true;
                }
                else
                {
                    divStudentOther.Visible = false;
                    vcStudentOther.Enabled = false;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlDepartment_SelectedIndexChanged (set 'other department div' visibility if the selection of ddlDepartment dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherDept and form_type='D')
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlDepartment.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlDepartment.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherDept);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divDepartmentOther.Visible = true;
                        }
                        else
                        {
                            divDepartmentOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtDepartmentOther.Attributes.Add("required", "");
                            vcDeptmOther.Enabled = true;
                        }
                        else
                        {
                            //txtDepartmentOther.Attributes.Remove("required");
                            vcDeptmOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divDepartmentOther.Visible = false;
                    //txtDepartmentOther.Attributes.Remove("required");
                    vcDeptmOther.Enabled = false;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlOrganization_SelectedIndexChanged (set 'other organization div' visibility if the selection of ddlOrganization dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherOrg and form_type='D')
    protected void ddlOrganization_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlOrganization.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlOrganization.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherOrg);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divOrgOther.Visible = true;
                        }
                        else
                        {
                            divOrgOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtOrgOther.Attributes.Add("required", "");
                            vcOrgOther.Enabled = true;
                        }
                        else
                        {
                            //txtOrgOther.Attributes.Remove("required");
                            vcOrgOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divOrgOther.Visible = false;
                    //txtOrgOther.Attributes.Remove("required");
                    vcOrgOther.Enabled = false;
                }

                GetInstitution();
                ddlInstitution_SelectedIndexChanged(this, null);
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlInstitution_SelectedIndexChanged (set 'other institution div' visibility if the selection of ddlInstitution dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherInstitution and form_type='D')
    protected void ddlInstitution_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlInstitution.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlInstitution.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherInstitution);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divInstiOther.Visible = true;
                        }
                        else
                        {
                            divInstiOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtInstiOther.Attributes.Add("required", "");
                            vcInstiOther.Enabled = true;
                        }
                        else
                        {
                            //txtInstiOther.Attributes.Remove("required");
                            vcInstiOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divInstiOther.Visible = false;
                    //txtInstiOther.Attributes.Remove("required");
                    vcInstiOther.Enabled = false;
                }

                GetDepartment();
                ddlDepartment_SelectedIndexChanged(this, null);
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlUDFProCategory_SelectedIndexChanged (set 'other UDFProCategory div' visibility if the selection of ddlUDFProCategory dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_UDF_ProfCategroyOther and form_type='D')
    protected void ddlUDFProCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlUDFProCategory.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlUDFProCategory.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _UDF_ProfCategroyOther);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divUDFProCatOther.Visible = true;
                        }
                        else
                        {
                            divUDFProCatOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtUDFProCatOther.Attributes.Add("required", "");
                            vcProCatOther.Enabled = true;
                        }
                        else
                        {
                            //txtUDFProCatOther.Attributes.Remove("required");
                            vcProCatOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divUDFProCatOther.Visible = false;
                    //txtUDFProCatOther.Attributes.Remove("required");
                    vcProCatOther.Enabled = false;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlUDFCLCom_SelectedIndexChanged (set 'other UDFCLCom div' visibility if the selection of ddlUDFCLCom dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_UDF_CLCompanyOther and form_type='D')
    protected void ddlUDFCLCom_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlUDFCLCom.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlUDFCLCom.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _UDF_CLCompanyOther);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divUDFCLComOther.Visible = true;
                        }
                        else
                        {
                            divUDFCLComOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtUDFCLComOther.Attributes.Add("required", "");
                            vcUDFCLComOther.Enabled = true;
                        }
                        else
                        {
                            //txtUDFCLComOther.Attributes.Remove("required");
                            vcUDFCLComOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divUDFCLComOther.Visible = false;
                    //txtUDFCLComOther.Attributes.Remove("required");
                    vcUDFCLComOther.Enabled = false;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region rbreg_SelectedIndexChanged (set 'divRegSpecific visibility', 'divIDNo visibility' and 'vcIDNo enability' true or false according to 'rbreg' control(are you singapore registered doctor/nurse/pharmacist?) selected value)
    protected void rbreg_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (rbreg.Items.Count > 0)
            {
                if (rbreg.SelectedItem.Value == Number.One)
                {
                    divRegSpecific.Visible = true;
                    divIDNo.Visible = true;
                    vcIDNo.Enabled = true;
                }
                else
                {
                    divRegSpecific.Visible = false;
                    divIDNo.Visible = false;
                    txtIDNo.Text = "";
                    vcIDNo.Enabled = false;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region GetInstitution (get institution according to the selected organization id(ddlOrganization.SelectedItem.Value) from ref_Institution table)
    protected void GetInstitution()
    {
        ddlInstitution.Items.Clear();
        ddlInstitution.ClearSelection();

        DataSet dsInstitution = new DataSet();
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            CommonDataObj cmdObj = new CommonDataObj(fn);
            dsInstitution = cmdObj.getInstitutionByOrganizationID(ddlOrganization.SelectedItem.Value, showid);

            if (dsInstitution.Tables[0].Rows.Count > 0)
            {
                for (int y = 0; y < dsInstitution.Tables[0].Rows.Count; y++)
                {
                    ddlInstitution.Items.Add(dsInstitution.Tables[0].Rows[y]["institution"].ToString());
                    ddlInstitution.Items[y].Value = dsInstitution.Tables[0].Rows[y]["ID"].ToString();
                }
            }
            else
            {
                ddlInstitution.Items.Add("Please Select");
                ddlInstitution.Items[0].Value = "0";
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region GetDepartment (get department according to the selected institution id(ddlInstitution.SelectedItem.Value) from ref_Department table)
    protected void GetDepartment()
    {
        ddlDepartment.Items.Clear();
        ddlDepartment.ClearSelection();

        DataSet dsDepartment = new DataSet();
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            CommonDataObj cmdObj = new CommonDataObj(fn);
            dsDepartment = cmdObj.getDepartmentByInstitutionID(ddlInstitution.SelectedItem.Value, showid);

            if (dsDepartment.Tables[0].Rows.Count > 0)
            {
                for (int y = 0; y < dsDepartment.Tables[0].Rows.Count; y++)
                {
                    ddlDepartment.Items.Add(dsDepartment.Tables[0].Rows[y]["department"].ToString());
                    ddlDepartment.Items[y].Value = dsDepartment.Tables[0].Rows[y]["ID"].ToString();
                }
            }
            else
            {
                ddlDepartment.Items.Add("Please Select");
                ddlDepartment.Items[0].Value = "0";
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region btnSave_Click (create one record (just RegGroupID and recycle=0) into tb_RegGroup table if current groupid not exist in tb_RegGroup & save/update data into tb_RegDelegate table & get next route and redirect to next page according to the site flow settings (tb_site_flow table))
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (!cFun.validatePhoneCode(txtTelcc.Text.ToString())
                //|| !cFun.validatePhoneCode(txtTelac.Text.ToString())
                || !cFun.validatePhoneCode(txtMobcc.Text.ToString())
                //|| !cFun.validatePhoneCode(txtMobac.Text.ToString())
                || !cFun.validatePhoneCode(txtFaxcc.Text.ToString())
                //|| !cFun.validatePhoneCode(txtFaxac.Text.ToString())
                )
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Phone code is not valid.');", true);
                return;
            }

            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
            bool isValid = true;

            Boolean isvalidpage = isValidPage(showid, urlQuery);
            if (isvalidpage)
            {
                CategoryClass catClass = new CategoryClass();
                FlowControler fCon = new FlowControler(fn);
                FlowMaster flwMaster = fCon.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
                string type = flwMaster.FlowCategoryConfigType;
                if ((type == catClass.flowcat_ByPromoCode.ToString() || type == catClass.flowcat_ByBusinessLogicWithPromo.ToString()) && flwMaster.isPromoEnabled)//According to Business Logic (Customize) *Like SHBC
                {
                    PromoController proCtrl = new PromoController(fn);
                    DataTable promoList = proCtrl.getPromoData(txtPromoCode.Text.Trim(), showid, flowid);
                    if (promoList.Rows.Count <= 0)
                    {
                        isValid = false;
                    }
                }

                if (!isValid)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('The code is not valid.');", true);
                    return;
                }
                else
                {
                    if (!string.IsNullOrEmpty(showid))
                    {
                        int isSuccess = 0;
                        string groupid = string.Empty;
                        Boolean hasid = false;

                        RegDelegateObj rgd = new RegDelegateObj(fn);
                        groupid = cFun.DecryptValue(urlQuery.GoupRegID);

                        string regno = Number.Zero;
                        regno = cFun.DecryptValue(urlQuery.DelegateID);
                        if (String.IsNullOrEmpty(regno))
                        {
                            regno = rgd.GenDelegateNumber(showid);
                        }
                        else
                        {
                            hasid = true;
                        }
                        try
                        {
                            FlowControler flwController = new FlowControler(fn);
                            FlowMaster flwConfig = flwController.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
                            if (flwConfig.FlowLimitDelegateQty == Number.One)
                            {
                            }
                            else
                            {
                                RegGroupObj rgg = new RegGroupObj(fn);
                                DataTable dtGroup = rgg.getRegGroupByID(groupid, showid);
                                if (dtGroup.Rows.Count == 0)
                                {
                                    rgg.createRegGroupIDOnly(groupid, 0, showid);
                                }
                            }

                            DataTable dt = rgd.getDataByGroupIDRegno(groupid, regno, showid);
                            if (dt.Rows.Count > 0)
                            {
                                hasid = true;
                            }
                            else
                            {
                                hasid = false;
                                if (flwConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                                {
                                    RegGroupObj rgp = new RegGroupObj(fn);
                                    DataTable dtDelegateG = rgd.getRegDelegateByGroupID(groupid, showid);
                                    if (dtDelegateG.Rows.Count > 0)
                                    {
                                        groupid = rgp.GenGroupNumber(showid);
                                        rgp.createRegGroupIDOnly(groupid, 0, showid);
                                    }
                                }
                            }
                            int con_categoryID = 0;
                            string salutation = string.Empty;
                            string fname = string.Empty;
                            string lname = string.Empty;
                            string oname = string.Empty;
                            string passno = string.Empty;
                            int isreg = 0;
                            string regspecific = string.Empty;//MCR/SNB/PRN
                            string idno = string.Empty;//MCR/SNB/PRN No.
                            string staffid = string.Empty;//no use in design for this field
                            string designation = string.Empty;
                            string jobtitle = string.Empty;//if Profession is Allied Health
                            string profession = string.Empty;
                            string department = string.Empty;
                            string organization = string.Empty;
                            string institution = string.Empty;
                            string address1 = string.Empty;
                            string address2 = string.Empty;
                            string address3 = string.Empty;
                            string address4 = string.Empty;
                            string city = string.Empty;
                            string state = string.Empty;
                            string postalcode = string.Empty;
                            string country = string.Empty;
                            string rcountry = string.Empty;
                            string telcc = string.Empty;
                            string telac = string.Empty;
                            string tel = string.Empty;
                            string mobilecc = string.Empty;
                            string mobileac = string.Empty;
                            string mobile = string.Empty;
                            string faxcc = string.Empty;
                            string faxac = string.Empty;
                            string fax = string.Empty;
                            string email = string.Empty;
                            string affiliation = string.Empty;
                            string dietary = string.Empty;
                            string nationality = string.Empty;
                            int age = 0;
                            DateTime? dob = null;
                            string dob_str = string.Empty;
                            string gender = string.Empty;
                            string additional4 = string.Empty;
                            string additional5 = string.Empty;
                            string memberno = string.Empty;

                            string vname = string.Empty;
                            string vdob = string.Empty;
                            string vpassno = string.Empty;
                            string vpassexpiry = string.Empty;
                            string vpassissuedate = string.Empty;
                            string vembarkation = string.Empty;
                            string varrivaldate = string.Empty;
                            string vcountry = string.Empty;

                            string udfcname = string.Empty;
                            string udfdeltype = string.Empty;
                            string udfprofcat = string.Empty;
                            string udfprofcatother = string.Empty;
                            string udfcpcode = string.Empty;
                            string udfcldept = string.Empty;
                            string udfcaddress = string.Empty;
                            string udfclcompany = string.Empty;
                            string udfclcompanyother = string.Empty;
                            string udfccountry = string.Empty;

                            string supname = string.Empty;
                            string supdesignation = string.Empty;
                            string supcontact = string.Empty;
                            string supemail = string.Empty;

                            string othersal = string.Empty;
                            string otherprof = string.Empty;
                            string otherdept = string.Empty;
                            string otherorg = string.Empty;
                            string otherinstitution = string.Empty;

                            string aemail = string.Empty;
                            int isSMS = 0;

                            string remark = string.Empty;
                            string remark_groupupload = string.Empty;
                            int approvestatus = 0;
                            string createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                            int recycle = 0;
                            string stage = string.Empty;

                            OthersSettings othersetting = new OthersSettings(fn);
                            List<string> lstOthersValue = othersetting.lstOthersValue;

                            //CategoryObj catObj = new CategoryObj(fn);
                            //int.TryParse(catObj.checkCategory(urlQuery, regno, txtPromoCode.Text.Trim()), out con_categoryID);

                            salutation = cFun.solveSQL(ddlSalutation.SelectedItem.Value.ToString());
                            if ((lstOthersValue.Contains(ddlSalutation.SelectedItem.Text)) && txtSalOther.Text != "" && txtSalOther.Text != string.Empty)
                            {
                                othersal = cFun.solveSQL(txtSalOther.Text.Trim());
                            }

                            fname = cFun.solveSQL(txtFName.Text.Trim());
                            lname = cFun.solveSQL(txtLName.Text.Trim());
                            oname = cFun.solveSQL(txtOName.Text.Trim());
                            passno = cFun.solveSQL(txtPassNo.Text);

                            isreg = rbreg.SelectedItem.Value != "" ? Convert.ToInt32(rbreg.SelectedItem.Value) : 0;
                            if (isreg == 1)
                            {
                                regspecific = rbregspecific.SelectedItem.Value;
                            }

                            idno = cFun.solveSQL(txtIDNo.Text.Trim());

                            designation = cFun.solveSQL(txtDesignation.Text.ToString());
                            jobtitle = cFun.solveSQL(txtJobtitle.Text.ToString());

                            profession = cFun.solveSQL(ddlProfession.SelectedItem.Value.ToString());
                            if ((lstOthersValue.Contains(ddlProfession.SelectedItem.Text)) && txtProOther.Text != "" && txtProOther.Text != string.Empty)
                            {
                                otherprof = cFun.solveSQL(txtProOther.Text.Trim());
                            }
                            if (ddlProfession.SelectedItem.Text.Trim() == LastFirstOrderName1_OSEA)//***for OSEA
                            {
                                otherprof = lname + " " + fname;
                            }
                            else if (ddlProfession.SelectedItem.Text.Trim() == FirstLastOrderName2_OSEA)
                            {
                                otherprof = fname + " " + lname;
                            }//***for OSEA

                            department = cFun.solveSQL(ddlDepartment.SelectedItem.Value.ToString());
                            if ((lstOthersValue.Contains(ddlDepartment.SelectedItem.Text)) && txtDepartmentOther.Text != "" && txtDepartmentOther.Text != string.Empty)
                            {
                                otherdept = cFun.solveSQL(txtDepartmentOther.Text.Trim());
                            }

                            organization = cFun.solveSQL(ddlOrganization.SelectedItem.Value.ToString());
                            if ((lstOthersValue.Contains(ddlOrganization.SelectedItem.Text)) && txtOrgOther.Text != "" && txtOrgOther.Text != string.Empty)
                            {
                                otherorg = cFun.solveSQL(txtOrgOther.Text.Trim());
                            }

                            institution = cFun.solveSQL(ddlInstitution.SelectedItem.Value.ToString());
                            if ((lstOthersValue.Contains(ddlInstitution.SelectedItem.Text)) && txtInstiOther.Text != "" && txtInstiOther.Text != string.Empty)
                            {
                                otherinstitution = cFun.solveSQL(txtInstiOther.Text.Trim());
                            }

                            address1 = cFun.solveSQL(txtAddress1.Text.Trim());
                            address2 = cFun.solveSQL(txtAddress2.Text.Trim());
                            address3 = cFun.solveSQL(txtAddress3.Text.Trim());
                            address4 = cFun.solveSQL(txtAddress4.Text.Trim());
                            city = cFun.solveSQL(txtCity.Text.Trim());
                            postalcode = cFun.solveSQL(txtPostalcode.Text.Trim());
                            state = cFun.solveSQL(txtState.Text.Trim());
                            country = cFun.solveSQL(ddlCountry.SelectedItem.Value.ToString());
                            rcountry = cFun.solveSQL(ddlRCountry.SelectedItem.Value.ToString());
                            telcc = txtTelcc.Text.ToString();
                            telac = txtTelac.Text.ToString();
                            tel = txtTel.Text.ToString();
                            mobilecc = txtMobcc.Text.ToString();
                            mobileac = txtMobac.Text.ToString();
                            mobile = txtMobile.Text.ToString();
                            faxcc = txtFaxcc.Text.ToString();
                            faxac = txtFaxac.Text.ToString();
                            fax = txtFax.Text.ToString();
                            email = cFun.solveSQL(txtEmail.Text.Trim());
                            affiliation = cFun.solveSQL(ddlAffiliation.SelectedItem.Value.ToString());
                            dietary = cFun.solveSQL(ddlDietary.SelectedItem.Value.ToString());
                            nationality = cFun.solveSQL(txtNationality.Text.Trim());
                            memberno = cFun.solveSQL(txtMemberNo.Text.Trim());

                            vname = cFun.solveSQL(txtVName.Text.Trim());
                            if (!String.IsNullOrWhiteSpace(txtVDOB.Text))
                            {
                                if (!cFun.validateDate(txtVDOB.Text.Trim()))
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                                    return;
                                }

                                vdob = cFun.solveSQL(txtVDOB.Text.Trim());
                            }
                            vpassno = cFun.solveSQL(txtVPassNo.Text.Trim());
                            if (!String.IsNullOrWhiteSpace(txtVPassExpiry.Text))
                            {
                                if (!cFun.validateDate(txtVPassExpiry.Text.Trim()))
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                                    return;
                                }

                                vpassexpiry = cFun.solveSQL(txtVPassExpiry.Text.Trim());
                            }
                            if (!String.IsNullOrWhiteSpace(txtVPassIssueDate.Text))
                            {
                                if (!cFun.validateDate(txtVPassIssueDate.Text.Trim()))
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                                    return;
                                }

                                vpassissuedate = cFun.solveSQL(txtVPassIssueDate.Text.Trim());
                            }
                            vembarkation = cFun.solveSQL(txtVEmbarkation.Text.Trim());
                            if (!String.IsNullOrWhiteSpace(txtVArrivalDate.Text))
                            {
                                if (!cFun.validateDate(txtVArrivalDate.Text.Trim()))
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                                    return;
                                }

                                varrivaldate = cFun.solveSQL(txtVArrivalDate.Text.Trim());
                            }
                            vcountry = cFun.solveSQL(ddlVCountry.SelectedItem.Value.Trim());

                            udfcname = cFun.solveSQL(txtUDFCName.Text.Trim());
                            udfdeltype = cFun.solveSQL(txtUDFDelType.Text.Trim());
                            udfprofcat = cFun.solveSQL(ddlUDFProCategory.SelectedItem.Value.Trim());
                            if ((lstOthersValue.Contains(ddlUDFProCategory.SelectedItem.Text)) && txtUDFProCatOther.Text != "" && txtUDFProCatOther.Text != string.Empty)
                            {
                                udfprofcatother = cFun.solveSQL(txtUDFProCatOther.Text.Trim());
                            }
                            udfcpcode = cFun.solveSQL(txtUDFCpostalcode.Text.Trim());
                            udfcldept = cFun.solveSQL(txtUDFCLDept.Text.Trim());
                            udfcaddress = cFun.solveSQL(txtUDFAddress.Text.Trim());
                            udfclcompany = cFun.solveSQL(ddlUDFCLCom.SelectedItem.Value.ToString());
                            if ((lstOthersValue.Contains(ddlUDFCLCom.SelectedItem.Text)) && txtUDFCLComOther.Text != "" && txtUDFCLComOther.Text != string.Empty)
                            {
                                udfclcompanyother = cFun.solveSQL(txtUDFCLComOther.Text.Trim());
                            }
                            udfccountry = cFun.solveSQL(ddlUDFCCountry.SelectedItem.Value.ToString());

                            supname = cFun.solveSQL(txtSupName.Text.Trim());
                            supdesignation = cFun.solveSQL(txtSupDesignation.Text.Trim());
                            supcontact = cFun.solveSQL(txtSupContact.Text.Trim());
                            supemail = cFun.solveSQL(txtSupEmail.Text.Trim());

                            if (!String.IsNullOrWhiteSpace(txtAge.Text))
                            {
                                age = Convert.ToInt32(cFun.solveSQL(txtAge.Text.Trim()));
                            }
                            if (!String.IsNullOrWhiteSpace(txtDOB.Text))
                            {
                                if (!cFun.validateDate(txtDOB.Text.Trim()))
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                                    return;
                                }

                                dob = DateTime.ParseExact(txtDOB.Text.Trim(), "dd/MM/yyyy", null);
                                dob_str = dob.Value.ToString("yyyy-MM-dd hh:mm:ss");
                            }
                            gender = cFun.solveSQL(ddlGender.SelectedItem.Value.Trim());

                            //***Check for OSEA
                            PromoController prm = new PromoController(fn);
                            #region EventCode Checking
                            if (!String.IsNullOrEmpty(txtAdditional4.Text) && !String.IsNullOrWhiteSpace(txtAdditional4.Text) && txtAdditional4.Enabled == true)
                            {
                                Tuple<string, string> responseMsg = prm.checkValidPromoCode(txtAdditional4.Text.Trim(), urlQuery);
                                if (string.IsNullOrEmpty(responseMsg.Item2))
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid Event Code! \\n Please clear the code to proceed.');", true);
                                    return;
                                }
                            }
                            additional4 = cFun.solveSQL(txtAdditional4.Text.Trim());
                            #endregion
                            #region VIPCode Checking
                            ////***(Comment because they want to prepopulate once VIP code keyin)
                            //if (!String.IsNullOrEmpty(txtAdditional5.Text) && !String.IsNullOrWhiteSpace(txtAdditional5.Text) && txtAdditional5.Enabled == true)
                            //{
                            //    Tuple<string, string> responseMsg = prm.checkValidPromoCode(txtAdditional5.Text.Trim(), urlQuery);
                            //    if (string.IsNullOrEmpty(responseMsg.Item2))
                            //    {
                            //        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid VIP Code! \\n Please clear the code to proceed.');", true);
                            //        return;
                            //    }
                            //}
                            additional5 = cFun.solveSQL(txtAdditional5.Text.Trim());
                            if(!string.IsNullOrEmpty(txtAdditional5.Text) && !string.IsNullOrWhiteSpace(txtAdditional5.Text))
                            {
                                if (lblIsVIPCodeUsed.Text == "0" || string.IsNullOrEmpty(lblIsVIPCodeUsed.Text) || string.IsNullOrWhiteSpace(lblIsVIPCodeUsed.Text))
                                {
                                    PrePopulate_RegDelegateObj preDelObj = new PrePopulate_RegDelegateObj(fn);
                                    int isSaved = preDelObj.updateDelegateRefNoByColumn(delegateRegNo, txtAdditional5.Text.Trim(), showid, regno);
                                    if (isSaved > 0)
                                    {
                                        string updateddate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                                        preDelObj.updateStatus(regno, (int)StatusValue.Success, showid, updateddate);
                                    }
                                }
                            }
                            else
                            {
                                additional5 = "";
                            }
                            #endregion

                            memberno = lblSpeRegno.Text.Trim();
                            //***Check for OSEA

                            stage = cFun.DecryptValue(urlQuery.CurrIndex);

                            rgd.groupid = groupid;
                            rgd.regno = regno;
                            rgd.con_categoryID = con_categoryID;
                            rgd.salutation = salutation;
                            rgd.fname = fname;
                            rgd.lname = lname;
                            rgd.oname = oname;
                            rgd.passno = passno;
                            rgd.isreg = isreg;
                            rgd.regspecific = regspecific;//MCR/SNB/PRN
                            rgd.idno = idno;//MCR/SNB/PRN No.
                            rgd.staffid = staffid;//no use in design for this field
                            rgd.designation = designation;
                            rgd.jobtitle = jobtitle;//if Profession is Allied Health
                            rgd.profession = profession;
                            rgd.department = department;
                            rgd.organization = organization;
                            rgd.institution = institution;
                            rgd.address1 = address1;
                            rgd.address2 = address2;
                            rgd.address3 = address3;
                            rgd.address4 = address4;
                            rgd.city = city;
                            rgd.state = state;
                            rgd.postalcode = postalcode;
                            rgd.country = country;
                            rgd.rcountry = rcountry;
                            rgd.telcc = telcc;
                            rgd.telac = telac;
                            rgd.tel = tel;
                            rgd.mobilecc = mobilecc;
                            rgd.mobileac = mobileac;
                            rgd.mobile = mobile;
                            rgd.faxcc = faxcc;
                            rgd.faxac = faxac;
                            rgd.fax = fax;
                            rgd.email = email;
                            rgd.affiliation = affiliation;
                            rgd.dietary = dietary;
                            rgd.nationality = nationality;
                            rgd.age = age;
                            rgd.dob = dob_str;
                            rgd.gender = gender;
                            rgd.additional4 = additional4;
                            rgd.additional5 = additional5;
                            rgd.memberno = memberno;

                            rgd.vname = vname;
                            rgd.vdob = vdob;
                            rgd.vpassno = vpassno;
                            rgd.vpassexpiry = vpassexpiry;
                            rgd.vpassissuedate = vpassissuedate;
                            rgd.vembarkation = vembarkation;
                            rgd.varrivaldate = varrivaldate;
                            rgd.vcountry = vcountry;

                            rgd.udfcname = udfcname;
                            rgd.udfdeltype = udfdeltype;
                            rgd.udfprofcat = udfprofcat;
                            rgd.udfprofcatother = udfprofcatother;
                            rgd.udfcpcode = udfcpcode;
                            rgd.udfcldept = udfcldept;
                            rgd.udfcaddress = udfcaddress;
                            rgd.udfclcompany = udfclcompany;
                            rgd.udfclcompanyother = udfclcompanyother;
                            rgd.udfccountry = udfccountry;

                            rgd.supname = supname;
                            rgd.supdesignation = supdesignation;
                            rgd.supcontact = supcontact;
                            rgd.supemail = supemail;

                            rgd.othersal = othersal;
                            rgd.otherprof = otherprof;
                            rgd.otherdept = otherdept;
                            rgd.otherorg = otherorg;
                            rgd.otherinstitution = otherinstitution;

                            rgd.aemail = aemail;
                            rgd.isSMS = isSMS;

                            rgd.remark = remark;
                            rgd.remark_groupupload = remark_groupupload;
                            rgd.approvestatus = approvestatus;
                            rgd.createdate = createdate;
                            rgd.recycle = recycle;
                            rgd.stage = stage;

                            rgd.showID = showid;

                            string actType = string.Empty;

                            bool isAlreadyExist = false;
                            string duplicateMsg = "The User already exist!!";
                            if (showid == "CPT327")
                                duplicateMsg = "We have found a duplicate entry in our records. If you’re still having problems with pre-registration, please email calvin@mda.com.sg for assistance.";
                            else if (showid == "BUM338" || showid == "CEK336")
                                duplicateMsg = "We have found a duplicate entry in our records. If you’re still having problems with pre-registration, please email yanling@mda.com.sg for assistance.";

                            if (hasid)
                            {
                                ////*Update
                                //isAlreadyExist = rgd.checkUpdateExist();
                                //if (isAlreadyExist == true)
                                //{
                                //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + duplicateMsg + "');", true);
                                //    return;
                                //}
                                //else
                                {
                                    isSuccess = rgd.updateRegDelegate();
                                    actType = rlgobj.actupdate;
                                }
                            }
                            else
                            {
                                ////*Save
                                //isAlreadyExist = rgd.checkInsertExist();
                                //if (isAlreadyExist == true)
                                //{
                                //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + duplicateMsg + "');", true);
                                //    return;
                                //}
                                //else
                                {
                                    isSuccess = rgd.saveRegDelegate();
                                    actType = rlgobj.actsave;

                                    //FlowControler flw = new FlowControler(fn, urlQuery);
                                    //flw.SendCurrentStepEmail(urlQuery);
                                }
                            }

                            if (isSuccess > 0)
                            {
                                //***updated by th on 15-7-2018(because groupid may change)
                                string newUrlQuery = "FLW=" + urlQuery.FlowID + "&STP=" + urlQuery.CurrIndex
                                    + "&GRP=" + cFun.EncryptValue(groupid) + "&INV=" + cFun.EncryptValue(regno) + "&SHW=" + urlQuery.CurrShowID;
                                urlQuery = new FlowURLQuery(newUrlQuery);
                                //***updated by th on 15-7-2018

                                CategoryObj catObj = new CategoryObj(fn);
                                int.TryParse(catObj.checkCategory(urlQuery, regno, txtPromoCode.Text.Trim()), out con_categoryID);
                                rgd.updateCategoryID(regno, con_categoryID, showid);

                                bool duplicateQA = false;
                                //Duplicate Group QA to Delegate
                                if (!hasid)
                                {
                                    FlowControler flwControl = new FlowControler(fn);
                                    string flwID = cFun.DecryptValue(urlQuery.FlowID);
                                    FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flwID);
                                    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                                    {
                                        QAHelper qaHelp = new QAHelper();
                                        string delegateQAID = qaHelp.GetQAIDfromFlow(showid, flwID, "D");
                                        string groupQAID = qaHelp.GetQAIDfromFlow(showid, flwID, "G");
                                        if (string.IsNullOrEmpty(groupQAID))
                                            groupQAID = qaHelp.GetQAIDfromFlow(showid, flwID, "C");

                                        if (!string.IsNullOrEmpty(groupQAID) && !string.IsNullOrEmpty(delegateQAID))
                                        {
                                            string QALINK = "";
                                            if (groupQAID != delegateQAID) //ifDiffent QAID
                                                QALINK = string.Format(@"rtype=duplicateAnswerDiffQA&qnaireID={0}&&newqnaireID={1}&userID={2}&newuserID={3}", groupQAID, delegateQAID, groupid, regno);
                                            else
                                                QALINK = string.Format(@"rtype=duplicateAnswer&qnaireID={0}&userID={1}&newuserID={2}", groupQAID, groupid, regno);
                                            QAHelper qHelp = new QAHelper();
                                            bool OK = qHelp.DuplicateQA(QALINK);
                                        }
                                    }
                                    else if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)//***
                                    {
                                        RegAdditionalControler addCtrl = new RegAdditionalControler(fn);
                                        string sourceRegno = addCtrl.getSourceIDRefValue(regno, showid, flowid);
                                        if (!string.IsNullOrEmpty(sourceRegno))
                                        {
                                            if (sourceRegno != "0" && sourceRegno != "1")
                                            {
                                                if (Request.QueryString["rmd"] == null)//***for OSEA
                                                {
                                                    QAHelper qaHelp = new QAHelper();
                                                    string delegateQAID = qaHelp.GetQAIDfromFlow(showid, flwID, "D");
                                                    if (!string.IsNullOrEmpty(delegateQAID))
                                                    {
                                                        string QALINK = "";
                                                        QALINK = string.Format(@"rtype=duplicateAnswer&qnaireID={0}&userID={1}&newuserID={2}", delegateQAID, sourceRegno, regno);
                                                        QAHelper qHelp = new QAHelper();
                                                        bool OK = qHelp.DuplicateQA(QALINK);
                                                        duplicateQA = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if(!string.IsNullOrEmpty(lblIsRegisterColleague.Text) && !string.IsNullOrWhiteSpace(lblIsRegisterColleague.Text))
                                    {
                                        if (lblIsRegisterColleague.Text.Trim() != "0")
                                        {
                                            FlowControler flwControl = new FlowControler(fn);
                                            string flwID = cFun.DecryptValue(urlQuery.FlowID);
                                            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flwID);
                                            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)//***
                                            {
                                                RegAdditionalControler addCtrl = new RegAdditionalControler(fn);
                                                string sourceRegno = addCtrl.getSourceIDRefValue(regno, showid, flowid);
                                                if (!string.IsNullOrEmpty(sourceRegno))
                                                {
                                                    if (sourceRegno != "0" && sourceRegno != "1")
                                                    {
                                                        if (Request.QueryString["rmd"] == null)//***for OSEA
                                                        {
                                                            QAHelper qaHelp = new QAHelper();
                                                            string delegateQAID = qaHelp.GetQAIDfromFlow(showid, flwID, "D");
                                                            if (!string.IsNullOrEmpty(delegateQAID))
                                                            {
                                                                string QALINK = "";
                                                                QALINK = string.Format(@"rtype=duplicateAnswer&qnaireID={0}&userID={1}&newuserID={2}", delegateQAID, sourceRegno, regno);
                                                                QAHelper qHelp = new QAHelper();
                                                                bool OK = qHelp.DuplicateQA(QALINK);
                                                                duplicateQA = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                #region UpdateRegno
                                OrderControler oControler = new OrderControler(fn);
                                bool isUpdateRegno = oControler.UpdateRegNo(urlQuery, regno);
                                #endregion

                                saveRegAdditional(showid, regno, urlQuery);//***added on 25-6-2018

                                if (Request.QueryString["rmd"] != null)//***for OSEA
                                {
                                    string recommendOwnerID = cFun.DecryptValue(Request.QueryString["rmd"].ToString());
                                    saveRegAdditionalRegColleague(showid, recommendOwnerID, regno);
                                }

                                rgd.updateDelegateCurrentStep(urlQuery);//***

                                insertLogFlowAction(groupid, regno, actType, urlQuery);

                                FlowControler flwObj = new FlowControler(fn, urlQuery);
                                string showID = urlQuery.CurrShowID;
                                string page = flwObj.NextStepURL();
                                string step = flwObj.NextStep;
                                string FlowID = flwObj.FlowID;
                                string grpNum = "";
                                grpNum = groupid;// urlQuery.GoupRegID;

                                SetUpController setCtrl = new SetUpController(fn);
                                string specialRegno = string.Empty;

                                if (step == Number.Two)//***If pre-populate or register colleague, to Skip Questionnaire
                                {
                                    if (!string.IsNullOrEmpty(additional5) && !string.IsNullOrWhiteSpace(additional5))
                                    {
                                        if (lblIsVIPCodeUsed.Text == "0" || string.IsNullOrEmpty(lblIsVIPCodeUsed.Text) || string.IsNullOrWhiteSpace(lblIsVIPCodeUsed.Text))
                                        {
                                            PrePopulate_RegDelegateObj preDelegateObj = new PrePopulate_RegDelegateObj(fn);
                                            PrePopulate_RegDelegateObj preDelObj = preDelegateObj.getPrePopulateDataByColumn(delegateRegNo, additional5);
                                            if (preDelObj != null)
                                            {
                                                int prepopulate_cat = preDelObj.con_categoryID;
                                                if (prepopulate_cat == (int)RegApproveRejectStatus.VIP)
                                                {
                                                    rgd.UpdateApproveRejectStatus(regno, (int)RegApproveRejectStatus.VIP, showid);//***
                                                    if (hasid == false)
                                                    {
                                                        specialRegno = setCtrl.GetSpecialRunNUmber(OSEAVIPRegno, showid);
                                                    }
                                                    else
                                                    {
                                                        //RegAdditionalControler addCtrl = new RegAdditionalControler(fn);
                                                        //string sourceRegno = addCtrl.getSourceIDRefValue(regno, showid, flowid);
                                                        //if (!string.IsNullOrEmpty(sourceRegno))
                                                        //{
                                                        if(string.IsNullOrEmpty(lblSpeRegno.Text) || string.IsNullOrWhiteSpace(lblSpeRegno.Text))
                                                        {
                                                            specialRegno = setCtrl.GetSpecialRunNUmber(OSEAVIPRegno, showid);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (hasid == false)
                                                    {
                                                        specialRegno = setCtrl.GetSpecialRunNUmber(OSEAVisitorRegno, showid);
                                                    }
                                                    else
                                                    {
                                                        //RegAdditionalControler addCtrl = new RegAdditionalControler(fn);
                                                        //string sourceRegno = addCtrl.getSourceIDRefValue(regno, showid, flowid);
                                                        //if (!string.IsNullOrEmpty(sourceRegno))
                                                        //{
                                                        if (string.IsNullOrEmpty(lblSpeRegno.Text) || string.IsNullOrWhiteSpace(lblSpeRegno.Text))
                                                        {
                                                            specialRegno = setCtrl.GetSpecialRunNUmber(OSEAVisitorRegno, showid);
                                                        }
                                                    }
                                                }

                                                bool isUpdated = isPrePopulateCheck(preDelObj);//***checking is updated or not pre-populate data
                                                int isUpdatedPrepopulate = 0;
                                                string isUpdatedPrepopulateDateTime = "";
                                                if (isUpdated)
                                                {
                                                    isUpdatedPrepopulate = 1;
                                                    isUpdatedPrepopulateDateTime = DateTime.Now.ToString();
                                                }
                                                rgd.updatePrepopulateUpdateOSEA(regno, groupid, isUpdatedPrepopulate, isUpdatedPrepopulateDateTime, showid);
                                                //***checking is updated or not pre-populate data

                                                //***qa pre-populate copy, copied of pre-populate to new regno
                                                if (hasid == false)
                                                {
                                                    FlowControler flwControl = new FlowControler(fn);
                                                    string flwID = cFun.DecryptValue(urlQuery.FlowID);
                                                    FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flwID);
                                                    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)//***
                                                    {
                                                        RegAdditionalControler addCtrl = new RegAdditionalControler(fn);
                                                        if (Request.QueryString["rmd"] == null)//***for OSEA
                                                        {
                                                            QAHelper qaHelp = new QAHelper();
                                                            string delegateQAID = qaHelp.GetQAIDfromFlow(showid, flwID, "D");
                                                            if (!string.IsNullOrEmpty(delegateQAID))
                                                            {
                                                                string QALINK = "";
                                                                QALINK = string.Format(@"rtype=duplicateAnswer&qnaireID={0}&userID={1}&newuserID={2}", delegateQAID, additional5, regno);
                                                                QAHelper qHelp = new QAHelper();
                                                                bool OK = qHelp.DuplicateQA(QALINK);
                                                                duplicateQA = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                //***
                                            }
                                        }
                                        else if(lblIsVIPCodeUsed.Text.Trim() == "1")
                                        {
                                            rgd.UpdateApproveRejectStatus(regno, (int)RegApproveRejectStatus.VIP, showid);//***
                                            if (hasid == false)
                                            {
                                                specialRegno = setCtrl.GetSpecialRunNUmber(OSEAVIPRegno, showid);
                                            }
                                            else
                                            {
                                                //RegAdditionalControler addCtrl = new RegAdditionalControler(fn);
                                                //string sourceRegno = addCtrl.getSourceIDRefValue(regno, showid, flowid);
                                                //if (!string.IsNullOrEmpty(sourceRegno))
                                                //{
                                                if (string.IsNullOrEmpty(lblSpeRegno.Text) || string.IsNullOrWhiteSpace(lblSpeRegno.Text))
                                                {
                                                    specialRegno = setCtrl.GetSpecialRunNUmber(OSEAVIPRegno, showid);
                                                }
                                            }
                                        }
                                        #region if they are from pre-populate or VIP, skip QA
                                        //string nextStep = Number.Three;//***Redirect To step 3 flow (RecommendFriendOSEA.aspx)
                                        //flwObj = new FlowControler(fn, flowid, nextStep);
                                        //showID = urlQuery.CurrShowID;
                                        //page = flwObj.CurrIndexModule;
                                        //step = nextStep;
                                        //FlowID = flowid;
                                        //grpNum = groupid;// urlQuery.GoupRegID;
                                        #endregion
                                    }
                                    else
                                    {
                                        if (hasid == false)
                                        {
                                            specialRegno = setCtrl.GetSpecialRunNUmber(OSEAVisitorRegno, showid);
                                        }
                                        else
                                        {
                                            //RegAdditionalControler addCtrl = new RegAdditionalControler(fn);
                                            //string sourceRegno = addCtrl.getSourceIDRefValue(regno, showid, flowid);
                                            //if (!string.IsNullOrEmpty(sourceRegno))
                                            //{
                                            if (string.IsNullOrEmpty(lblSpeRegno.Text) || string.IsNullOrWhiteSpace(lblSpeRegno.Text))
                                            {
                                                specialRegno = setCtrl.GetSpecialRunNUmber(OSEAVisitorRegno, showid);
                                            }
                                        }

                                        if (duplicateQA)//***Skip Questionnaire
                                        {
                                            string nextStep = Number.Three;//***Redirect To step 3 flow (RecommendFriendOSEA.aspx)
                                            flwObj = new FlowControler(fn, flowid, nextStep);
                                            showID = urlQuery.CurrShowID;
                                            page = flwObj.CurrIndexModule;
                                            step = nextStep;
                                            FlowID = flowid;
                                            grpNum = groupid;// urlQuery.GoupRegID;
                                        }
                                    }
                                }

                                ClearForm();

                                ////*save special Regno
                                //if (hasid == false)
                                {
                                    if (!string.IsNullOrEmpty(specialRegno))
                                    {
                                        rgd.updateMemberNoAsSpecialRegno(regno, groupid, specialRegno, showid);//***
                                    }
                                }
                                ////*save special Regno

                                if (flwConfig.FlowLimitDelegateQty == Number.One)
                                {
                                    string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, null, BackendRegType.backendRegType_Delegate);
                                    Response.Redirect(route, false);
                                }
                                else
                                {
                                    string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno, BackendRegType.backendRegType_Delegate);
                                    Response.Redirect(route, false);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogGenEmail lggenemail = new LogGenEmail(fn);
                            lggenemail.type = GenLogDefaultValue.errorException;
                            lggenemail.RefNumber = groupid + "," + regno;
                            lggenemail.description = ex.Message;
                            lggenemail.remark = RegClass.typeDeg + cFun.DecryptValue(urlQuery.FlowID);
                            lggenemail.step = cFun.DecryptValue(urlQuery.CurrIndex);
                            lggenemail.writeLog();

                        }
                    }
                    else
                    {
                        Response.Redirect("404.aspx");
                    }
                }
            }
        }
    }
    #endregion

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    #region setDivCss_TelMobFax
    public void setDivCss_TelMobFax(bool isShowCC, bool isShowAC, bool isShowPhoneNo, string type)
    {
        string name = string.Empty;
        try
        {
            if (type == "Tel")
            {
                #region type="Tel"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");
                    divTelcc.Attributes.Add("class", "col-xs-6");

                    divTelac.Attributes.Remove("class");
                    divTelac.Attributes.Add("class", "col-xs-6");

                    divTelNo.Attributes.Remove("class");
                }
                #endregion
            }
            else if (type == "Mob")
            {
                #region type="Mob"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divMobac.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");

                    divMobac.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");
                    divMobcc.Attributes.Add("class", "col-xs-6");

                    divMobac.Attributes.Remove("class");
                    divMobac.Attributes.Add("class", "col-xs-6");

                    divMobileNo.Attributes.Remove("class");
                }
                #endregion
            }
            else if (type == "Fax")
            {
                #region Type="Fax"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");
                    divFaxcc.Attributes.Add("class", "col-xs-6");

                    divFaxac.Attributes.Remove("class");
                    divFaxac.Attributes.Add("class", "col-xs-6");

                    divFaxNo.Attributes.Remove("class");
                }
                #endregion
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
   #region btnBack
    protected void btnBack_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            FlowControler flwObj = new FlowControler(fn);
            string flowID = cFun.DecryptValue(urlQuery.FlowID);
            string currentIndx = cFun.DecryptValue(urlQuery.CurrIndex);
            flwObj.getPreviousRoute(flowID, currentIndx);
            string showID = urlQuery.CurrShowID;
            string page = flwObj.CurrIndexModule;
            string step = flwObj.CurrIndex;             
            string grpNum = "";
            grpNum = urlQuery.GoupRegID;
            string regno = urlQuery.DelegateID;
            string route = flwObj.MakeFullURL(page, flowID, showID, grpNum, step, regno, BackendRegType.backendRegType_Delegate);
            Response.Redirect(route, false);
        }
    }
    #endregion

    #region isValidPage//***added on 25-6-2018
    protected Boolean isValidPage(string showid, FlowURLQuery urlQuery)
    {
        Boolean isvalid = true;

        try
        {
            if (divFTRCHK.Visible == true)
            {
                int countTerms = chkFTRCHK.Items.Count;
                if (countTerms > 0)
                {
                    TemplateControler tmpCtrl = new TemplateControler(fn);
                    foreach (ListItem liItem in chkFTRCHK.Items)
                    {
                        string id = liItem.Value.Trim();
                        FlowTemplateNoteObj ftnObj = tmpCtrl.getNoteByID(id, showid, FlowTemplateNoteType.FooterWithCheckBox, urlQuery);
                        int isSkip = 0;
                        if (ftnObj != null)
                        {
                            isSkip = ftnObj.note_isSkip;
                        }

                        if (isSkip == 0)
                        {
                            if (liItem.Selected == false)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('Please accept .');</script>", false);
                                isvalid = false;
                                return isvalid;
                            }
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        { }

        return isvalid;
    }
    #endregion
    #region bindFlowNote//***added on 25-6-2018
    private void bindFlowNote(string showid, FlowURLQuery urlQuery)
    {
        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = UpdatePanel1.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            divFooter.Visible = true;
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox)
                            {
                                Control ctrlChk = UpdatePanel1.FindControl("chk" + ftnObj.note_Type);
                                if (ctrlChk != null)
                                {
                                    CheckBoxList chkFTRCHK = UpdatePanel1.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    ListItem newItem = new ListItem(displayTextTmpt, ftnObj.note_ID);
                                    chkFTRCHK.Items.Add(newItem);
                                }
                            }
                        }
                        else
                        {
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #region saveRegAdditional//***added on 25-6-2018
    protected void saveRegAdditional(string showid, string regno, FlowURLQuery urlQuery)
    {
        try
        {
            if (divFTRCHK.Visible == true)
            {
                int countTerms = chkFTRCHK.Items.Count;
                //int countCheckedTerms = chkFTRCHK.Items.Cast<ListItem>().Count(li => li.Selected);
                if (countTerms > 0)
                {
                    foreach (ListItem liItem in chkFTRCHK.Items)
                    {
                        string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                        string delegateid = regno;
                        string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
                        string delegateType = BackendRegType.backendRegType_Group;
                        string ownerID = groupid;
                        if (!string.IsNullOrEmpty(delegateid))
                        {
                            delegateType = BackendRegType.backendRegType_Delegate;
                            ownerID = delegateid;
                        }
                        RegAdditionalObj regAddObj = new RegAdditionalObj();
                        regAddObj.ad_ShowID = showid;
                        regAddObj.ad_FlowID = cFun.DecryptValue(urlQuery.FlowID);
                        regAddObj.ad_OwnerID = ownerID;
                        regAddObj.ad_FlowStep = currentStep;
                        regAddObj.ad_DelegateType = delegateType;
                        regAddObj.ad_Value = liItem.Selected == true ? "1" : "0";
                        regAddObj.ad_Type = FlowTemplateNoteType.FooterWithCheckBox;
                        regAddObj.ad_NoteID = cFun.ParseInt(liItem.Value);
                        RegAdditionalControler regAddCtrl = new RegAdditionalControler(fn);
                        regAddCtrl.SaveRegAdditional(regAddObj);
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region OSEA Event Code Visible Setting
    private void setEventCodeVisibility(string countryName, string fieldName, string eventCode=null)
    {
        if (EventCodeCountries_OSEA.Contains(countryName.ToLower()))
        {
            divAdditional4.Visible = true;
            if(!string.IsNullOrEmpty(eventCode) && !string.IsNullOrWhiteSpace(eventCode))
            {
                if (fieldName == _Additional4)
                {
                    txtAdditional4.Text = eventCode;
                    txtAdditional4.Enabled = false;
                }
                //else if (fieldName == _Additional5)
                //{
                //    txtAdditional5.Text = eventCode;
                //    txtAdditional5.Enabled = false;
                //}
            }
        }
        else
        {
            divAdditional4.Visible = false;
            txtAdditional4.Text = "";
            txtAdditional4.Enabled = true;
            if (!string.IsNullOrEmpty(eventCode) && !string.IsNullOrWhiteSpace(eventCode))
            {
                //if (fieldName == _Additional5)
                //{
                //    txtAdditional5.Text = eventCode;
                //    txtAdditional5.Enabled = false;
                //}
            }
        }
    }
    #endregion

    protected void saveRegAdditionalRegColleague(string showid, string regno, string colleagueRegno)//regno=recommendpersonRegno
    {
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string delegateid = regno;
            string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
            string delegateType = BackendRegType.backendRegType_Group;
            string ownerID = groupid;
            if (!string.IsNullOrEmpty(delegateid))
            {
                delegateType = BackendRegType.backendRegType_Delegate;
                ownerID = delegateid;
            }
            RegAdditionalObj regAddObj = new RegAdditionalObj();
            regAddObj.ad_ShowID = showid;
            regAddObj.ad_FlowID = cFun.DecryptValue(urlQuery.FlowID);
            regAddObj.ad_OwnerID = ownerID;
            regAddObj.ad_FlowStep = currentStep;
            regAddObj.ad_DelegateType = delegateType;
            regAddObj.ad_Value = colleagueRegno;
            regAddObj.ad_Type = RegAdditionalType.recommendAFriend;
            regAddObj.ad_NoteID = 0;
            RegAdditionalControler regAddCtrl = new RegAdditionalControler(fn);
            regAddCtrl.SaveRegAdditional(regAddObj);
        }
        catch (Exception ex)
        { }
    }

    #region Pre-populate Data
    static string delegateRegNo = "Delegate_RefRegno";
    ////***regDelegateRefNo=>VIP Code or normal pre-populate Code(the client will give us)
    protected void txtAdditional5_TextChanged(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(txtAdditional5.Text) && !string.IsNullOrWhiteSpace(txtAdditional5.Text))
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
            string regno = cFun.DecryptValue(urlQuery.DelegateID);

            FlowControler Flw = new FlowControler(fn);
            FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowid);
            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
            {
                bool isDelegateRegistered = false;
                string route = "";
                bool isPrepopulate = PrepopulateData(showid, flowid, txtAdditional5.Text.Trim(), regno, ref isDelegateRegistered, ref route);
                if (isPrepopulate)
                {
                    if (isDelegateRegistered)
                    {
                        txtAdditional5.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sorry, this user already registered.');", true);
                        return;
                    }
                    else if(!string.IsNullOrEmpty(route))
                    {
                        Response.Redirect(route, false);
                    }
                }
                else
                {
                    if (isDelegateRegistered)
                    {
                        txtAdditional5.Text = "";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Sorry, this user already registered.');", true);
                        return;
                    }
                    else
                    {
                        PromoController prm = new PromoController(fn);
                        Tuple<string, string> responseMsg = prm.checkValidPromoCode(txtAdditional5.Text.Trim(), urlQuery);
                        if (string.IsNullOrEmpty(responseMsg.Item2))
                        {
                            //ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid VIP Code! \\n Please clear the code to proceed.');", true);
                            //return;

                            txtAdditional5.Text = "";
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid VIP Code!');", true);
                            return;
                        }
                        else
                        {
                            txtAdditional5.Enabled = false;
                            lblIsVIPCodeUsed.Text = "1";
                        }
                    }
                }
            }
            else
            {
                txtAdditional5.Text = "";
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid VIP Code!');", true);
                return;
            }
        }
        else
        {
            txtAdditional5.Text = "";
        }
    }
    private bool PrepopulateData(string showID, string FlowID, string regDelegateRefNo, string regno, ref bool isDelegateRegistered, ref string route)
    {
        bool isPrepopulate = false;

        PrePopulate_RegDelegateObj preDelegateObj = new PrePopulate_RegDelegateObj(fn);
        bool isPrePopulateExist = preDelegateObj.checkPrePopulateDataExistByColumn(delegateRegNo, regDelegateRefNo, ref regno);
        if (isPrePopulateExist)
        {
            PrePopulate_RegDelegateObj preDelObj = preDelegateObj.getPrePopulateDataByColumn(delegateRegNo, regDelegateRefNo);
            bool isExist = prepopulateChecking(showID, regDelegateRefNo, preDelObj, ref regno, ref isDelegateRegistered);
            if (isExist)
            {
                if(!isDelegateRegistered)
                {
                    isPrepopulate = true;

                    if (!string.IsNullOrEmpty(regno))
                    {
                        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                        string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
                        FlowControler flwObj = new FlowControler(fn, FlowID, currentStep);
                        string page = flwObj.CurrIndexModule;
                        RegDelegateObj regDelegateObj = new RegDelegateObj(fn);
                        DataTable dtRegDelegate = regDelegateObj.getRegDelegateByID(regno, showID);
                        if (dtRegDelegate.Rows.Count > 0)
                        {
                            string grpNum = dtRegDelegate.Rows[0]["RegGroupID"].ToString();
                            route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, currentStep, regno, BackendRegType.backendRegType_Delegate);
                        }
                    }
                    else
                    {
                        #region pre-populate
                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.salutation))
                            {
                                string itemOrigin = preDelObj.salutation.ToLower();
                                foreach (ListItem item in ddlSalutation.Items)
                                {
                                    string itemToCompare = item.Text.ToLower();
                                    if (itemOrigin == itemToCompare)
                                    {
                                        ddlSalutation.ClearSelection();
                                        item.Selected = true;
                                    }
                                }

                                ddlSalutation_SelectedIndexChanged(this, null);
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        txtSalOther.Text = preDelObj.othersal;

                        txtFName.Text = preDelObj.fname;
                        txtLName.Text = preDelObj.lname;
                        txtOName.Text = preDelObj.oname;
                        txtPassNo.Text = preDelObj.passno;

                        ////***isreg is used for isUpdated pre-populate data
                        //if (preDelObj.isreg == int.Parse(Number.One))
                        //{
                        //    rbreg.Items[0].Selected = true;
                        //    rbreg.Items[1].Selected = false;
                        //}
                        //else
                        //{
                        //    rbreg.Items[0].Selected = false;
                        //    rbreg.Items[1].Selected = true;
                        //}
                        ////***isreg is used for isUpdated pre-populate data

                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.regspecific))
                            {
                                ListItem listItem = rbregspecific.Items.FindByValue(preDelObj.regspecific);
                                if (listItem != null)
                                {
                                    rbregspecific.ClearSelection();
                                    listItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        txtIDNo.Text = preDelObj.idno;

                        if (preDelObj.profession != "")
                        {
                            if (!String.IsNullOrEmpty(preDelObj.profession))
                            {
                                string itemOrigin = preDelObj.profession.ToLower();
                                foreach (ListItem item in ddlProfession.Items)
                                {
                                    string itemToCompare = item.Text.ToLower();
                                    if (itemOrigin == itemToCompare)
                                    {
                                        ddlProfession.ClearSelection();
                                        item.Selected = true;
                                    }
                                }

                                ddlProfession_SelectedIndexChanged(this, null);
                            }
                        }
                        txtProOther.Text = preDelObj.otherprof;
                        txtDesignation.Text = preDelObj.designation;
                        txtJobtitle.Text = preDelObj.jobtitle;

                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.organization))
                            {
                                ListItem listItem = ddlOrganization.Items.FindByValue(preDelObj.organization);
                                if (listItem != null)
                                {
                                    ddlOrganization.ClearSelection();
                                    listItem.Selected = true;
                                }
                                ddlOrganization_SelectedIndexChanged(this, null);
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        txtOrgOther.Text = preDelObj.otherorg;

                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.institution))
                            {
                                ListItem listItem = ddlInstitution.Items.FindByValue(preDelObj.institution);
                                if (listItem != null)
                                {
                                    ddlInstitution.ClearSelection();
                                    listItem.Selected = true;
                                }
                                ddlInstitution_SelectedIndexChanged(this, null);
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        txtInstiOther.Text = preDelObj.otherinstitution;

                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.department))
                            {
                                ListItem listItem = ddlDepartment.Items.FindByValue(preDelObj.department);
                                if (listItem != null)
                                {
                                    ddlDepartment.ClearSelection();
                                    listItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        txtDepartmentOther.Text = preDelObj.otherdept;

                        txtAddress1.Text = preDelObj.address1;
                        txtAddress2.Text = preDelObj.address2;
                        txtAddress3.Text = preDelObj.address3;
                        txtAddress4.Text = preDelObj.address4;
                        txtCity.Text = preDelObj.city;
                        txtState.Text = preDelObj.state;
                        txtPostalcode.Text = preDelObj.postalcode;
                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.country))
                            {
                                string itemOrigin = preDelObj.country.ToLower();
                                foreach (ListItem item in ddlCountry.Items)
                                {
                                    string itemToCompare = item.Text.ToLower();
                                    if (itemOrigin == itemToCompare)
                                    {
                                        ddlCountry.ClearSelection();
                                        item.Selected = true;
                                        ddlCountry_SelectedIndexChanged(this, null);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.rcountry))
                            {
                                ListItem listItem = ddlRCountry.Items.FindByValue(preDelObj.rcountry);
                                if (listItem != null)
                                {
                                    ddlRCountry.ClearSelection();
                                    listItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        //txtTelcc.Text = preDelObj.telcc;
                        txtTelac.Text = preDelObj.telac;
                        txtTel.Text = preDelObj.tel;
                        //txtMobcc.Text = preDelObj.mobilecc;
                        txtMobac.Text = preDelObj.mobileac;
                        txtMobile.Text = preDelObj.mobile;
                        //txtFaxcc.Text = preDelObj.faxcc;
                        txtFaxac.Text = preDelObj.faxac;
                        txtFax.Text = preDelObj.fax;
                        txtEmail.Text = preDelObj.email;
                        //txtEmailConfirmation.Text = preDelObj.email;

                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.affiliation))
                            {
                                ListItem listItem = ddlAffiliation.Items.FindByValue(preDelObj.affiliation);
                                if (listItem != null)
                                {
                                    ddlAffiliation.ClearSelection();
                                    listItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.dietary))
                            {
                                ListItem listItem = ddlDietary.Items.FindByValue(preDelObj.dietary);
                                if (listItem != null)
                                {
                                    ddlDietary.ClearSelection();
                                    listItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        txtNationality.Text = preDelObj.nationality;
                        //txtMemberNo.Text = preDelObj.memberno;

                        txtVName.Text = preDelObj.vname;
                        txtVDOB.Text = preDelObj.vdob;
                        txtVPassNo.Text = preDelObj.vpassno;
                        txtVPassExpiry.Text = preDelObj.vpassexpiry;
                        txtVPassIssueDate.Text = preDelObj.vpassissuedate;
                        txtVEmbarkation.Text = preDelObj.vembarkation;
                        txtVArrivalDate.Text = preDelObj.varrivaldate;
                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.vcountry))
                            {
                                ListItem listItem = ddlVCountry.Items.FindByValue(preDelObj.vcountry);
                                if (listItem != null)
                                {
                                    ddlVCountry.ClearSelection();
                                    listItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        txtUDFDelType.Text = preDelObj.udfdeltype;
                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.udfprofcat))
                            {
                                ListItem listItem = ddlUDFProCategory.Items.FindByValue(preDelObj.udfprofcat);
                                if (listItem != null)
                                {
                                    ddlUDFProCategory.ClearSelection();
                                    listItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        txtUDFProCatOther.Text = preDelObj.udfprofcatother;
                        txtUDFCName.Text = preDelObj.udfcname;
                        txtUDFCpostalcode.Text = preDelObj.udfcpcode;
                        txtUDFCLDept.Text = preDelObj.udfcldept;
                        txtUDFAddress.Text = preDelObj.udfcaddress;
                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.udfclcompany))
                            {
                                ListItem listItem = ddlUDFCLCom.Items.FindByValue(preDelObj.udfclcompany);
                                if (listItem != null)
                                {
                                    ddlUDFCLCom.ClearSelection();
                                    listItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        txtUDFCLComOther.Text = preDelObj.udfclcompanyother;
                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.udfccountry))
                            {
                                ListItem listItem = ddlUDFCCountry.Items.FindByValue(preDelObj.udfccountry);
                                if (listItem != null)
                                {
                                    ddlUDFCCountry.ClearSelection();
                                    listItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        txtSupName.Text = preDelObj.supname;
                        txtSupDesignation.Text = preDelObj.supdesignation;
                        txtSupContact.Text = preDelObj.supcontact;
                        txtSupEmail.Text = preDelObj.supemail;

                        txtAge.Text = preDelObj.age.ToString();
                        txtDOB.Text = !string.IsNullOrEmpty(preDelObj.dob) ? Convert.ToDateTime(preDelObj.dob).ToString("dd/MM/yyyy") : "";
                        try
                        {
                            if (!String.IsNullOrEmpty(preDelObj.gender))
                            {
                                ListItem listItem = ddlGender.Items.FindByValue(preDelObj.gender);
                                if (listItem != null)
                                {
                                    ddlGender.ClearSelection();
                                    listItem.Selected = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        //txtAdditional4.Text = preDelObj.additional4;
                        //setEventCodeVisibility(ddlCountry.SelectedItem.Text, _Additional4, preDelObj.additional4);//***for OSEA

                        //txtAdditional5.Text = preDelObj.additional5;
                        //setEventCodeVisibility(ddlCountry.SelectedItem.Text, _Additional5, preDelObj.additional5);//***for OSEA
                        #endregion
                    }
                }
            }
        }

        return isPrepopulate;
    }
    private bool prepopulateChecking(string showid, string regDelegateRefNo, PrePopulate_RegDelegateObj preDelegateObj, ref string regno, ref bool isDelegateRegistered)
    {
        bool isExist = false;
        try
        {
            if (preDelegateObj != null)
            {
                isExist = true;
                int prepopulateStatus = (int)StatusValue.Pending;
                regno = preDelegateObj.refNo;
                if (!string.IsNullOrEmpty(regno))
                {
                    prepopulateStatus = preDelegateObj.getPreDelegateStatusByDelRefID(regno, showid);
                    if (prepopulateStatus == (int)StatusValue.Success)
                    {
                        RegDelegateObj regDelegateObj = new RegDelegateObj(fn);
                        DataTable dtRegDelegate = regDelegateObj.getRegDelegateByID(regno, showid);
                        if (dtRegDelegate.Rows.Count > 0)
                        {
                            int delegateStatus = regDelegateObj.getDelegateStatusByID(regno, showid);
                            if (delegateStatus == (int)StatusValue.Success)
                            {
                                isDelegateRegistered = true;
                            }
                        }
                        else
                        {
                            regno = "";
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            isExist = false;
        }

        return isExist;
    }
    private void updatePrepopulateData(string showID, string FlowID, string regDelegateRefNo, string groupid, string regno, PrePopulate_RegDelegateObj preDelObj)
    {
        FlowControler Flw = new FlowControler(fn);
        FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(FlowID);
        if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
        {
            bool isExist = false;
            bool isDelegateRegistered = false;
            string decryptShowID = cFun.DecryptValue(showID);
            isExist = prepopulateChecking(showID, regDelegateRefNo, preDelObj, ref regno, ref isDelegateRegistered);
            if (isExist)
            {
                if (!string.IsNullOrEmpty(regno) && !isDelegateRegistered)
                {
                    int isSaved = preDelObj.updateDelegateRefNoByColumn(delegateRegNo, regDelegateRefNo, showID, regno);
                    if (isSaved > 0)
                    {
                        string updateddate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                        preDelObj.updateStatus(regno, (int)StatusValue.Success, showID, updateddate);
                        isExist = true;
                    }
                }
            }
        }
    }
    #endregion

    #region isPrePopulateCheck
    private bool isPrePopulateCheck(PrePopulate_RegDelegateObj preDelObj)
    {
        bool isUpdated = false;
        try
        {
            if(preDelObj != null)
            {
                if(ddlSalutation.SelectedItem.Text.ToLower() != preDelObj.salutation.ToLower())
                {
                    isUpdated = true;
                }

                if (txtFName.Text != preDelObj.fname)
                {
                    isUpdated = true;
                }

                if (txtLName.Text != preDelObj.lname)
                {
                    isUpdated = true;
                }

                if (txtOName.Text != preDelObj.oname)
                {
                    isUpdated = true;
                }

                if (ddlProfession.SelectedItem.Text.ToLower() != preDelObj.profession.ToLower())
                {
                    isUpdated = true;
                }

                if (txtAddress1.Text != preDelObj.address1)
                {
                    isUpdated = true;
                }

                if (txtAddress2.Text != preDelObj.address2)
                {
                    isUpdated = true;
                }

                if (txtAddress3.Text != preDelObj.address3)
                {
                    isUpdated = true;
                }

                if (txtAddress4.Text != preDelObj.address4)
                {
                    isUpdated = true;
                }

                if (txtCity.Text != preDelObj.city)
                {
                    isUpdated = true;
                }

                if (txtState.Text != preDelObj.state)
                {
                    isUpdated = true;
                }

                if (txtPostalcode.Text != preDelObj.postalcode)
                {
                    isUpdated = true;
                }

                if (ddlCountry.SelectedItem.Text.ToLower() != preDelObj.country.ToLower())
                {
                    isUpdated = true;
                }

                if (txtTelcc.Text != preDelObj.telcc)
                {
                    isUpdated = true;
                }

                if (txtTelac.Text != preDelObj.telac)
                {
                    isUpdated = true;
                }

                if (txtTel.Text != preDelObj.tel)
                {
                    isUpdated = true;
                }

                if (txtMobcc.Text != preDelObj.mobilecc)
                {
                    isUpdated = true;
                }

                if (txtMobac.Text != preDelObj.mobileac)
                {
                    isUpdated = true;
                }

                if (txtMobile.Text != preDelObj.mobile)
                {
                    isUpdated = true;
                }

                if (txtFaxcc.Text != preDelObj.faxcc)
                {
                    isUpdated = true;
                }

                if (txtFaxac.Text != preDelObj.faxac)
                {
                    isUpdated = true;
                }

                if (txtFax.Text != preDelObj.fax)
                {
                    isUpdated = true;
                }

                if (txtEmail.Text != preDelObj.email)
                {
                    isUpdated = true;
                }

                if (txtNationality.Text != preDelObj.nationality)
                {
                    isUpdated = true;
                }
            }
        }
        catch(Exception ex)
        { }

        return isUpdated;
    }
    #endregion
}