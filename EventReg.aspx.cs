﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
public partial class EventReg : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    public class tmpDataList
    {
        public string showID { get; set; }
        public string FlowID { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["Event"] != null)
        {
            string eventName = Request.QueryString["Event"].ToString();
            /*
            if (eventName == "VendorRegistration")//***For Vendor Registration - added on 11-10-2018 according to Sunny request
            {
                Response.Redirect("https://www.event-reg.biz/registration/ReLogin?event=VendorRegistration");
            }
            if (eventName == "VendorRegistrationPrc74198785265496321")//***valid login (private link)
            {
                eventName = "VendorRegistration";
            }
            */
            //***For Vendor Registration - added on 11-10-2018 according to Sunny request

            if (eventName == "ASCAIndivActivation")//***For ASCA - added on 07-05-2019, redirect to normal flow
            {
                Response.Redirect("https://event-reg.biz/registration/eventreg?event=ASCADelegateReg");
            }

            tmpDataList tmpList = GetShowIDByEventName(eventName);
            if (string.IsNullOrEmpty(tmpList.showID))
                Response.Redirect("404.aspx");
            else
            {
                string url = "Welcome.aspx?SHW={0}&FLW={1}";
                url = string.Format(url, cFun.EncryptValue(tmpList.showID), cFun.EncryptValue(tmpList.FlowID));
                if (Request.QueryString["TRK"] != null)// MDA Tracking
                {
                    url = url + "&TRK=" + Request.QueryString["TRK"].ToString();
                }
                if (Request.QueryString["CEI"] != null)// CET Tracking
                {
                    url = url + "&CEI=" + Request.QueryString["CEI"].ToString();
                }
                Response.Redirect(url);
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    private tmpDataList GetShowIDByEventName(string eventName)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            cList.showID = "";
            cList.FlowID = "";
            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                cList.showID = dList.Rows[0]["showID"].ToString();
                cList.FlowID = dList.Rows[0]["FLW_ID"].ToString();
            }

        }
        catch { }
        return cList;
    }
}