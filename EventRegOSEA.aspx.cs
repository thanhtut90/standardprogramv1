﻿using Corpit.BackendMaster;
using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EventRegOSEA : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    static string delegateRegNo = "Delegate_RefRegno";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.Params["event"] != null)
            {
                string showID = Request.QueryString["event"].ToString();
                string regDelegateRefNo = string.Empty;
                if (Request.Params["reg"] != null)
                {
                    regDelegateRefNo = Request.QueryString["reg"].ToString();
                    Response.Redirect("EventRegOSEAIndex.aspx?event=" + cFun.EncryptValue(showID) + "&reg=" + cFun.EncryptValue(regDelegateRefNo));
                }
                else
                {
                    //Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
                    Response.Redirect("EventRegOSEAIndex.aspx?event=?event=" + cFun.EncryptValue(showID) + "&reg=" + regDelegateRefNo);
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid.');window.location='404.aspx';</script>");
            }
        }
    }
}