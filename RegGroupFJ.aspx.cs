﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Logging;
using Corpit.BackendMaster;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;

public partial class RegGroupFJ : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _Designation = "Designation";
    static string _Department = "Department";
    static string _Company = "Company";
    static string _Industry = "Industry";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _VisitDate = "Visit Date";
    static string _VisitTime = "Visit Time";
    static string _Password = "Password";
    static string _OtherSalutation = "Other Salutation";
    static string _OtherDesignation = "Other Designation";
    static string _OtherIndustry = "Other Industry";

    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Age = "Age";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    private static string[] checkingFJShowName = new string[] { "Food Japan" };
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                setIsDelegateVisibility(flowid, showid);//*

                if (setDynamicForm(flowid, showid))
                {
                    bindDropdown();

                    bindFlowNote(showid, urlQuery);//***added on 25-6-2018

                    string groupid = cFun.DecryptValue(urlQuery.GoupRegID);

                    if (Request.Params["t"] != null)
                    {
                        string admintype = cFun.DecryptValue(Request.QueryString["t"].ToString());
                        if (admintype == BackendStaticValueClass.isAdmin)
                        {
                            btnNext.Visible = false;
                            btnNext.Enabled = false;
                        }
                        else
                        {
                            bindPageLoad(showid, flowid, groupid, urlQuery);
                        }
                    }
                    else
                    {
                        bindPageLoad(showid, flowid, groupid, urlQuery);
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region bindPageLoad
    private void bindPageLoad(string showid, string flowid, string groupid, FlowURLQuery urlQuery)
    {
        btnNext.Visible = true;
        btnNext.Enabled = true;

        if (!String.IsNullOrEmpty(groupid))
        {
            populateUserDetails(groupid.ToString(), showid);

            insertLogFlowAction(groupid, "", rlgobj.actview, urlQuery);
        }
        else
        {
            Response.Redirect("DefaultRegIndex.aspx");
        }
    }
    #endregion

    #region setIsDelegateVisibility
    private void setIsDelegateVisibility(string flowid, string showid)
    {
        FlowControler fCtrl = new FlowControler(fn);
        FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
        if (fmaster != null)
        {
            if (fmaster.isGIsAttendingVisible)
            {
                divIsDelegate.Visible = true;
                ShowControler sControl = new ShowControler(fn);
                Show s = sControl.GetShow(showid);
                chkIsDelegate.Text = "I will attend Food Japan as a visitor. <br />&nbsp;&nbsp;&nbsp;&nbsp; 来場者として登録を希望します。";/// + s.SHW_Name;//I am pre-registering on behalf of my colleagues and attending Food Japan as a visitor
            }
            else
            {
                divIsDelegate.Visible = false;
                if(fmaster.isGAutoAttending)
                {
                    chkIsDelegate.Checked = true;
                }
            }
        }
    }
    #endregion

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile =  masterPage;
        }
    }
    #endregion

    #region bindDropdown & bind respective data to Salutation and Country dropdown lists
    protected void bindDropdown()
    {
        DataSet dsSalutation = new DataSet();
        DataSet dsCountry = new DataSet();
        DataSet dsIndustry = new DataSet();
        DataSet dsRefAdditionalList_Additional4 = new DataSet();
        DataSet dsRefAdditionalList_Additional5 = new DataSet();

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);

        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsSalutation = cmdObj.getSalutation(showid);

        CountryObj couObj = new CountryObj(fn);
        dsCountry = couObj.getAllCountry();

        dsIndustry = cmdObj.getIndustry(showid);
        dsRefAdditionalList_Additional4 = cmdObj.getRefAdditionalList(showid, RefAdditionalListType.Additional4);
        dsRefAdditionalList_Additional5 = cmdObj.getRefAdditionalList(showid, RefAdditionalListType.Additional5);

        if (dsSalutation.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsSalutation.Tables[0].Rows.Count; i++)
            {
                ddlSalutation.Items.Add(dsSalutation.Tables[0].Rows[i]["Sal_Name"].ToString());
                ddlSalutation.Items[i + 1].Value = dsSalutation.Tables[0].Rows[i]["Sal_ID"].ToString();
            }
        }
        if (dsCountry.Tables[0].Rows.Count != 0)
        {
            for (int x = 0; x < dsCountry.Tables[0].Rows.Count; x++)
            {
                ddlCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();

                ddlRCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlRCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();
            }
        }
        if (dsIndustry.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsIndustry.Tables[0].Rows.Count; i++)
            {
                ddlIndustry.Items.Add(dsIndustry.Tables[0].Rows[i]["Industry"].ToString());
                ddlIndustry.Items[i + 1].Value = dsIndustry.Tables[0].Rows[i]["ID"].ToString();
            }
        }
        if (dsRefAdditionalList_Additional4.Tables[0].Rows.Count > 0)
        {
            ddlAdditional4.Items.Clear();
            ddlAdditional4.Items.Add("Please Select");
            ddlAdditional4.Items[0].Value = "0";
            for (int i = 0; i < dsRefAdditionalList_Additional4.Tables[0].Rows.Count; i++)
            {
                ddlAdditional4.Items.Add(dsRefAdditionalList_Additional4.Tables[0].Rows[i]["refAdd_name"].ToString());
                ddlAdditional4.Items[i + 1].Value = dsRefAdditionalList_Additional4.Tables[0].Rows[i]["refAdd_id"].ToString();
            }
        }
        if (dsRefAdditionalList_Additional5.Tables[0].Rows.Count > 0)
        {
            ddlAdditional5.Items.Clear();
            ddlAdditional5.Items.Add("Please Select");
            ddlAdditional5.Items[0].Value = "0";
            for (int i = 0; i < dsRefAdditionalList_Additional5.Tables[0].Rows.Count; i++)
            {
                ddlAdditional5.Items.Add(dsRefAdditionalList_Additional5.Tables[0].Rows[i]["refAdd_name"].ToString());
                ddlAdditional5.Items[i + 1].Value = dsRefAdditionalList_Additional5.Tables[0].Rows[i]["refAdd_id"].ToString();
            }
        }
    }
    #endregion

    #region setDynamicForm (set div visibility and validator controls' enability dynamically (generate dynamic form) according to the settings of tb_Form table where form_type='G')
    protected bool setDynamicForm(string flowid, string showid)
    {
        bool isValidShow = false;

        DataSet ds = new DataSet();
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForGroup();

        string formtype = FormType.TypeGroup;

        CommonDataObj cmdObj = new CommonDataObj(fn);
        DataSet dsRefAdditionalList_Additional4 = cmdObj.getRefAdditionalList(showid, RefAdditionalListType.Additional4);
        DataSet dsRefAdditionalList_Additional5 = cmdObj.getRefAdditionalList(showid, RefAdditionalListType.Additional4);

        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
        {
            isValidShow = true;

            #region set divSalutation visibility is true or false if form_input_name is Salutation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                if (isshow == 1)
                {
                    divSalutation.Visible = true;
                }
                else
                {
                    divSalutation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Salutation, formtype);
                if (isrequired == 1)
                {
                    lblSalutation.Text = labelname + "<span class=\"red\">*</span>";
                    //ddlSalutation.Attributes.Add("required", "");
                    vcSal.Enabled = true;
                }
                else
                {
                    lblSalutation.Text = labelname;
                    //ddlSalutation.Attributes.Remove("required");
                    vcSal.Enabled = false;
                }
            }
            #endregion

            #region set divFName visibility is true or false if form_input_name is FName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFName.Visible = true;
                }
                else
                {
                    divFName.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Fname, formtype);
                if (isrequired == 1)
                {
                    lblFName.Text = labelname + "<span class=\"red\">*</span>";
                    //txtFName.Attributes.Add("required", "");
                    vcFName.Enabled = true;
                }
                else
                {
                    lblFName.Text = labelname;
                    //txtFName.Attributes.Remove("required");
                    vcFName.Enabled = false;
                }
            }
            #endregion

            #region set divLName visibility is true or false if form_input_name is LName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divLName.Visible = true;
                }
                else
                {
                    divLName.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Lname, formtype);
                if (isrequired == 1)
                {
                    lblLName.Text = labelname + "<span class=\"red\">*</span>";
                    //txtLName.Attributes.Add("required", "");
                    vcLName.Enabled = true;
                }
                else
                {
                    lblLName.Text = labelname;
                    //txtLName.Attributes.Remove("required");
                    vcLName.Enabled = false;
                }
            }
            #endregion

            #region set divDesignation visibility is true or false if form_input_name is Designation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDesignation.Visible = true;
                }
                else
                {
                    divDesignation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Designation, formtype);
                if (isrequired == 1)
                {
                    lblDesignation.Text = labelname + "<span class=\"red\">*</span>";
                    //txtDesignation.Attributes.Add("required", "");
                    vcDesig.Enabled = true;
                }
                else
                {
                    lblDesignation.Text = labelname;
                    //txtDesignation.Attributes.Remove("required");
                    vcDesig.Enabled = false;
                }
            }
            #endregion

            #region set divOtherDesignation visibility is true or false if form_input_name is Other Designation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OtherDesignation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divOtherDesignation.Visible = true;
                }
                else
                {
                    divOtherDesignation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_OtherDesignation, formtype);
                if (isrequired == 1)
                {
                    lblOtherDesignation.Text = labelname + "<span class=\"red\">*</span>";
                    //txtDesignation.Attributes.Add("required", "");
                    vcOtherDesignation.Enabled = true;
                }
                else
                {
                    lblOtherDesignation.Text = labelname;
                    //txtDesignation.Attributes.Remove("required");
                    vcOtherDesignation.Enabled = false;
                }
            }
            #endregion

            #region set divDepartment visibility is true or false if form_input_name is Department according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDepartment.Visible = true;
                }
                else
                {
                    divDepartment.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Department, formtype);
                if (isrequired == 1)
                {
                    lblDepartment.Text = labelname + "<span class=\"red\">*</span>";
                    //txtDepartment.Attributes.Add("required", "");
                    vcDeptm.Enabled = true;
                }
                else
                {
                    lblDepartment.Text = labelname;
                    //txtDepartment.Attributes.Remove("required");
                    vcDeptm.Enabled = false;
                }
            }
            #endregion

            #region set divCompany visibility is true or false if form_input_name is Company according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Company)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCompany.Visible = true;
                }
                else
                {
                    divCompany.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Company, formtype);
                if (isrequired == 1)
                {
                    lblCompany.Text = labelname + "<span class=\"red\">*</span>";
                    //txtCompany.Attributes.Add("required", "");
                    vcCom.Enabled = true;
                }
                else
                {
                    lblCompany.Text = labelname;
                    //txtCompany.Attributes.Remove("required");
                    vcCom.Enabled = false;
                }
            }
            #endregion

            #region set divIndustry visibility is true or false if form_input_name is Industry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Industry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divIndustry.Visible = true;
                }
                else
                {
                    divIndustry.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Industry, formtype);
                if (isrequired == 1)
                {
                    lblIndustry.Text = labelname + "<span class=\"red\">*</span>";
                    //ddlIndustry.Attributes.Add("required", "");
                    vcIndus.Enabled = true;
                }
                else
                {
                    lblIndustry.Text = labelname;
                    //ddlIndustry.Attributes.Remove("required");
                    vcIndus.Enabled = false;
                }
            }
            #endregion

            #region set divAddress1 visibility is true or false if form_input_name is Address1 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress1.Visible = true;
                }
                else
                {
                    divAddress1.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Address1, formtype);
                if (isrequired == 1)
                {
                    lblAddress1.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAddress1.Attributes.Add("required", "");
                    vcAdd1.Enabled = true;
                }
                else
                {
                    lblAddress1.Text = labelname;
                    //txtAddress1.Attributes.Remove("required");
                    vcAdd1.Enabled = false;
                }
            }
            #endregion

            #region set divAddress2 visibility is true or false if form_input_name is Address2 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress2.Visible = true;
                }
                else
                {
                    divAddress2.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : "";// frmObj.getDefaultLableNameByInputNameType(_Address2, formtype);
                if (isrequired == 1)
                {
                    lblAddress2.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAddress2.Attributes.Add("required", "");
                    vcAdd2.Enabled = true;
                }
                else
                {
                    lblAddress2.Text = labelname;
                    //txtAddress2.Attributes.Remove("required");
                    vcAdd2.Enabled = false;
                }
            }
            #endregion

            #region set divAddress3 visibility is true or false if form_input_name is Address3 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress3.Visible = true;
                }
                else
                {
                    divAddress3.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : "";// frmObj.getDefaultLableNameByInputNameType(_Address3, formtype);
                if (isrequired == 1)
                {
                    lblAddress3.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAddress3.Attributes.Add("required", "");
                    vcAdd3.Enabled = true;
                }
                else
                {
                    lblAddress3.Text = labelname;
                    //txtAddress3.Attributes.Remove("required");
                    vcAdd3.Enabled = false;
                }
            }
            #endregion

            #region set divCity visibility is true or false if form_input_name is City according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCity.Visible = true;
                }
                else
                {
                    divCity.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_City, formtype);
                if (isrequired == 1)
                {
                    lblCity.Text = labelname + "<span class=\"red\">*</span>";
                    //txtCity.Attributes.Add("required", "");
                    vcCity.Enabled = true;
                }
                else
                {
                    lblCity.Text = labelname;
                    //txtCity.Attributes.Remove("required");
                    vcCity.Enabled = false;
                }
            }
            #endregion

            #region set divState visibility is true or false if form_input_name is State according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divState.Visible = true;
                }
                else
                {
                    divState.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_State, formtype);
                if (isrequired == 1)
                {
                    lblState.Text = labelname + "<span class=\"red\">*</span>";
                    //txtState.Attributes.Add("required", "");
                    vcState.Enabled = true;
                }
                else
                {
                    lblState.Text = labelname;
                    //txtState.Attributes.Remove("required");
                    vcState.Enabled = false;
                }
            }
            #endregion

            #region set divPostalcode visibility is true or false if form_input_name is Postal code according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divPostalcode.Visible = true;
                }
                else
                {
                    divPostalcode.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_PostalCode, formtype);
                if (isrequired == 1)
                {
                    lblPostalcode.Text = labelname + "<span class=\"red\">*</span>";
                    //txtPostalcode.Attributes.Add("required", "");
                    vcPCode.Enabled = true;
                }
                else
                {
                    lblPostalcode.Text = labelname;
                    //txtPostalcode.Attributes.Remove("required");
                    vcPCode.Enabled = false;
                }
            }
            #endregion

            #region set divCountry visibility is true or false if form_input_name is Country according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCountry.Visible = true;
                }
                else
                {
                    divCountry.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Country, formtype);
                if (isrequired == 1)
                {
                    lblCountry.Text = labelname + "<span class=\"red\">*</span>";
                    //ddlCountry.Attributes.Add("required", "");
                    vcCountry.Enabled = true;
                }
                else
                {
                    lblCountry.Text = labelname;
                    //ddlCountry.Attributes.Remove("required");
                    vcCountry.Enabled = false;
                }
            }
            #endregion

            #region set divRCountry visibility is true or false if form_input_name is RCountry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divRCountry.Visible = true;
                }
                else
                {
                    divRCountry.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_RCountry, formtype);
                if (isrequired == 1)
                {
                    lblRCountry.Text = labelname + "<span class=\"red\">*</span>";
                    //ddlRCountry.Attributes.Add("required", "");
                    vcRCountry.Enabled = true;
                }
                else
                {
                    lblRCountry.Text = labelname;
                    //ddlRCountry.Attributes.Remove("required");
                    vcRCountry.Enabled = false;
                }
            }
            #endregion

            #region set divTelcc visibility is true or false if form_input_name is Telcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelcc.Visible = true;
                }
                else
                {
                    divTelcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtTelcc.Attributes.Add("required", "");
                    vcTelcc.Enabled = true;
                }
                else
                {
                    //txtTelcc.Attributes.Remove("required");
                    vcTelcc.Enabled = false;
                }
            }
            #endregion

            #region set divTelac visibility is true or false if form_input_name is Telac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelac.Visible = true;
                }
                else
                {
                    divTelac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtTelac.Attributes.Add("required", "");
                    vcTelac.Enabled = true;
                }
                else
                {
                    //txtTelac.Attributes.Remove("required");
                    vcTelac.Enabled = false;
                }
            }
            #endregion

            #region set divTel visibility is true or false if form_input_name is Tel according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTel.Visible = true;
                    divTelNo.Visible = true;
                }
                else
                {
                    divTel.Visible = false;
                    divTelNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Tel, formtype);
                if (isrequired == 1)
                {
                    lblTel.Text = labelname + "<span class=\"red\">*</span>";
                    //txtTel.Attributes.Add("required", "");
                    vcTel.Enabled = true;
                }
                else
                {
                    lblTel.Text = labelname;
                    //txtTel.Attributes.Remove("required");
                    vcTel.Enabled = false;
                }
            }
            #endregion

            #region set divMobcc visibility is true or false if form_input_name is Mobilecc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobcc.Visible = true;
                }
                else
                {
                    divMobcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtMobcc.Attributes.Add("required", "");
                    vcMobcc.Enabled = true;
                }
                else
                {
                    //txtMobcc.Attributes.Remove("required");
                    vcMobcc.Enabled = false;
                }
            }
            #endregion

            #region set divMobac visibility is true or false if form_input_name is Mobileac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobac.Visible = true;
                }
                else
                {
                    divMobac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtMobac.Attributes.Add("required", "");
                    vcMobac.Enabled = true;
                }
                else
                {
                    //txtMobac.Attributes.Remove("required");
                    vcMobac.Enabled = false;
                }
            }
            #endregion

            #region set divMobile visibility is true or false if form_input_name is Mobile according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobile.Visible = true;
                    divMobileNo.Visible = true;
                }
                else
                {
                    divMobile.Visible = false;
                    divMobileNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Mobile, formtype);
                if (isrequired == 1)
                {
                    lblMobile.Text = labelname + "<span class=\"red\">*</span>";
                    //txtMobile.Attributes.Add("required", "");
                    vcMob.Enabled = true;
                }
                else
                {
                    lblMobile.Text = labelname;
                    //txtMobile.Attributes.Remove("required");
                    vcMob.Enabled = false;
                }
            }
            #endregion

            #region set divFaxcc visibility is true or false if form_input_name is Faxcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxcc.Visible = true;
                }
                else
                {
                    divFaxcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtFaxcc.Attributes.Add("required", "");
                    vcFaxcc.Enabled = true;
                }
                else
                {
                    //txtFaxcc.Attributes.Remove("required");
                    vcFaxcc.Enabled = false;
                }
            }
            #endregion

            #region set divFaxac visibility is true or false if form_input_name is Faxac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxac.Visible = true;
                }
                else
                {
                    divFaxac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtFaxac.Attributes.Add("required", "");
                    vcFaxac.Enabled = true;
                }
                else
                {
                    //txtFaxac.Attributes.Remove("required");
                    vcFaxac.Enabled = false;
                }
            }
            #endregion

            #region set divFax visibility is true or false if form_input_name is Fax according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFax.Visible = true;
                    divFaxNo.Visible = true;
                }
                else
                {
                    divFax.Visible = false;
                    divFaxNo.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Fax, formtype);
                if (isrequired == 1)
                {
                    lblFax.Text = labelname + "<span class=\"red\">*</span>";
                    //txtFax.Attributes.Add("required", "");
                    vcFax.Enabled = true;
                }
                else
                {
                    lblFax.Text = labelname;
                    //txtFax.Attributes.Remove("required");
                    vcFax.Enabled = false;
                }
            }
            #endregion

            #region set divEmail visibility is true or false if form_input_name is Email according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmail.Visible = true;
                }
                else
                {
                    divEmail.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Email, formtype);
                if (isrequired == 1)
                {
                    lblEmail.Text = labelname + "<span class=\"red\">*</span>";
                    //txtEmail.Attributes.Add("required", "");
                    vcEmail.Enabled = true;
                }
                else
                {
                    lblEmail.Text = labelname;
                    //txtEmail.Attributes.Remove("required");
                    vcEmail.Enabled = false;
                }
            }
            #endregion

            #region set divEmailConfirmation visibility is true or false if form_input_name is ConfirmEmail according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _EmailConfirmation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmailConfirmation.Visible = true;
                    //txtEmailConfirmation.Attributes.Add("required", "");
                }
                else
                {
                    divEmailConfirmation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_EmailConfirmation, formtype);
                if (isrequired == 1)
                {
                    lblEmailConfirmation.Text = labelname + "<span class=\"red\">*</span>";
                    //txtEmailConfirmation.Attributes.Add("required", "");
                    vcEConfirm.Enabled = true;
                }
                else
                {
                    lblEmailConfirmation.Text = labelname;
                    //txtEmailConfirmation.Attributes.Remove("required");
                    vcEConfirm.Enabled = false;
                }
            }
            #endregion

            #region set divAge visibility is true or false if form_input_name is Age according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAge.Visible = true;
                }
                else
                {
                    divAge.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Age, formtype);
                if (isrequired == 1)
                {
                    lblAge.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAge.Attributes.Add("required", "");
                    vcAge.Enabled = true;
                }
                else
                {
                    lblAge.Text = labelname;
                    //txtAge.Attributes.Remove("required");
                    vcAge.Enabled = false;
                }
            }
            #endregion

            #region set divDOB visibility is true or false if form_input_name is DOB according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDOB.Visible = true;
                }
                else
                {
                    divDOB.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_DOB, formtype);
                if (isrequired == 1)
                {
                    lblDOB.Text = labelname + "<span class=\"red\">*</span>";
                    //txtDOB.Attributes.Add("required", "");
                    vcDOB.Enabled = true;
                }
                else
                {
                    lblDOB.Text = labelname;
                    //txtDOB.Attributes.Remove("required");
                    vcDOB.Enabled = false;
                }
            }
            #endregion

            #region set divGender visibility is true or false if form_input_name is Gender according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                if (isshow == 1)
                {
                    divGender.Visible = true;
                }
                else
                {
                    divGender.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Gender, formtype);
                if (isrequired == 1)
                {
                    lblGender.Text = labelname + "<span class=\"red\">*</span>";
                    //ddlGender.Attributes.Add("required", "");
                    //vcGender.Enabled = true;
                }
                else
                {
                    lblGender.Text = labelname;
                    //ddlGender.Attributes.Remove("required");
                    //vcGender.Enabled = false;
                }
            }
            #endregion

            #region set divVisitDate visibility is true or false if form_input_name is VisitDate according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VisitDate)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitDate.Visible = true;
                }
                else
                {
                    divVisitDate.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VisitDate, formtype);
                if (isrequired == 1)
                {
                    lblVisitDate.Text = labelname + "<span class=\"red\">*</span>";
                    //txtVisitDate.Attributes.Add("required", "");
                    vcVisitDate.Enabled = true;
                }
                else
                {
                    lblVisitDate.Text = labelname;
                    //txtVisitDate.Attributes.Remove("required");
                    vcVisitDate.Enabled = false;
                }
            }
            #endregion

            #region set divVisitTime visibility is true or false if form_input_name is VisitTime according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VisitTime)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitTime.Visible = true;
                }
                else
                {
                    divVisitTime.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_VisitTime, formtype);
                if (isrequired == 1)
                {
                    lblVisitTime.Text = labelname + "<span class=\"red\">*</span>";
                    //txtVisitTime.Attributes.Add("required", "");
                    vcVisitTime.Enabled = true;
                }
                else
                {
                    lblVisitTime.Text = labelname;
                    //txtVisitTime.Attributes.Remove("required");
                    vcVisitTime.Enabled = false;
                }
            }
            #endregion

            #region set divPassword visibility is true or false if form_input_name is Password according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Password)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divPassword.Visible = true;
                }
                else
                {
                    divPassword.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Password, formtype);
                if (isrequired == 1)
                {
                    lblPassword.Text = labelname + "<span class=\"red\">*</span>";
                    //txtPassword.Attributes.Add("required", "");
                    vcPassword.Enabled = true;
                }
                else
                {
                    lblPassword.Text = labelname;
                    //txtPassword.Attributes.Remove("required");
                    vcPassword.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional4 visibility is true or false if form_input_name is Additional4 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional4.Visible = true;
                }
                else
                {
                    divAdditional4.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Additional4, formtype);
                if (isrequired == 1)
                {
                    lblAdditional4.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAdditional4.Attributes.Add("required", "");
                    if (dsRefAdditionalList_Additional4.Tables[0].Rows.Count == 0)
                    {
                        vcAdditional4.Enabled = true;
                        txtAdditional4.Visible = true;
                        txtAdditional4.Enabled = true;

                        cvAdditional4.Enabled = false;
                        ddlAdditional4.Visible = false;
                        ddlAdditional4.Enabled = false;
                    }
                    else
                    {
                        vcAdditional4.Enabled = false;
                        txtAdditional4.Visible = false;
                        txtAdditional4.Enabled = false;

                        cvAdditional4.Enabled = true;
                        ddlAdditional4.Visible = true;
                        ddlAdditional4.Enabled = true;
                    }
                }
                else
                {
                    lblAdditional4.Text = labelname;
                    //txtAdditional4.Attributes.Remove("required");
                    vcAdditional4.Enabled = false;
                    cvAdditional4.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional5 visibility is true or false if form_input_name is Additional5 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional5.Visible = true;
                }
                else
                {
                    divAdditional5.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Additional5, formtype);
                if (isrequired == 1)
                {
                    lblAdditional5.Text = labelname + "<span class=\"red\">*</span>";
                    //txtAdditional5.Attributes.Add("required", "");
                    if (dsRefAdditionalList_Additional5.Tables[0].Rows.Count == 0)
                    {
                        vcAdditional5.Enabled = true;
                        txtAdditional5.Visible = true;
                        txtAdditional5.Enabled = true;

                        cvAdditional5.Enabled = false;
                        ddlAdditional5.Visible = false;
                        ddlAdditional5.Enabled = false;
                    }
                    else
                    {
                        vcAdditional5.Enabled = false;
                        txtAdditional5.Visible = false;
                        txtAdditional5.Enabled = false;

                        cvAdditional5.Enabled = true;
                        ddlAdditional5.Visible = true;
                        ddlAdditional5.Enabled = true;
                    }
                }
                else
                {
                    lblAdditional5.Text = labelname;
                    //txtAdditional5.Attributes.Remove("required");
                    vcAdditional5.Enabled = false;
                    cvAdditional5.Enabled = false;
                }
            }
            #endregion
        }

        setDivCss_TelMobFax(divTelcc.Visible, divTelac.Visible, divTelNo.Visible, "Tel");//*
        setDivCss_TelMobFax(divMobcc.Visible, divMobac.Visible, divMobileNo.Visible, "Mob");//*
        setDivCss_TelMobFax(divFaxcc.Visible, divFaxac.Visible, divFaxNo.Visible, "Fax");//*

        return isValidShow;
    }
    #endregion

    #region populateUserDetails (get all relevant data according to RegGroupID from tb_RegGroup and bind data to the respective controls)
    private void populateUserDetails(string id, string showid)
    {
        DataTable dt = new DataTable();
        RegGroupObj rgg = new RegGroupObj(fn);
        dt = rgg.getRegGroupByID(id, showid);
        if (dt.Rows.Count != 0)
        {
            string reggroupid = dt.Rows[0]["RegGroupID"].ToString();
            string rg_salutation = dt.Rows[0]["RG_Salutation"].ToString();
            string rg_contactfname = dt.Rows[0]["RG_ContactFName"].ToString();
            string rg_contactlname = dt.Rows[0]["RG_ContactLName"].ToString();
            string rg_designation = dt.Rows[0]["RG_Designation"].ToString();
            string rg_department = dt.Rows[0]["RG_Department"].ToString();
            string rg_company = dt.Rows[0]["RG_Company"].ToString();
            string rg_industry = dt.Rows[0]["RG_Industry"].ToString();
            string rg_address1 = dt.Rows[0]["RG_Address1"].ToString();
            string rg_address2 = dt.Rows[0]["RG_Address2"].ToString();
            string rg_address3 = dt.Rows[0]["RG_Address3"].ToString();
            string rg_city = dt.Rows[0]["RG_City"].ToString();
            string rg_stateprovince = dt.Rows[0]["RG_StateProvince"].ToString();
            string rg_postalcode = dt.Rows[0]["RG_PostalCode"].ToString();
            string rg_country = dt.Rows[0]["RG_Country"].ToString();
            string rg_rcountry = dt.Rows[0]["RG_RCountry"].ToString();
            string rg_telcc = dt.Rows[0]["RG_Telcc"].ToString();
            string rg_telac = dt.Rows[0]["RG_Telac"].ToString();
            string rg_tel = dt.Rows[0]["RG_Tel"].ToString();
            string rg_mobilecc = dt.Rows[0]["RG_Mobilecc"].ToString();
            string rg_mobileac = dt.Rows[0]["RG_Mobileac"].ToString();
            string rg_mobile = dt.Rows[0]["RG_Mobile"].ToString();
            string rg_faxcc = dt.Rows[0]["RG_Faxcc"].ToString();
            string rg_faxac = dt.Rows[0]["RG_Faxac"].ToString();
            string rg_fax = dt.Rows[0]["RG_Fax"].ToString();
            string rg_contactemail = dt.Rows[0]["RG_ContactEmail"].ToString();
            string rg_remark = dt.Rows[0]["RG_Remark"].ToString();
            string rg_type = dt.Rows[0]["RG_Type"].ToString();
            string rg_remarkgupload = dt.Rows[0]["RG_RemarkGUpload"].ToString();
            string rg_salother = dt.Rows[0]["RG_SalOther"].ToString();
            string rg_designationother = dt.Rows[0]["RG_DesignationOther"].ToString();
            string rg_industryothers = dt.Rows[0]["RG_IndustryOthers"].ToString();
            string rg_visitdate = dt.Rows[0]["RG_VisitDate"].ToString();
            string rg_visittime = dt.Rows[0]["RG_VisitTime"].ToString();
            string rg_password = dt.Rows[0]["RG_Password"].ToString();
            string rg_ismultiple = dt.Rows[0]["RG_IsMultiple"].ToString();
            string rg_referralcode = dt.Rows[0]["RG_ReferralCode"].ToString();
            string rg_isfromsales = dt.Rows[0]["RG_IsFromSales"].ToString();
            string rg_issendemail = dt.Rows[0]["RG_IsSendEmail"].ToString();
            string rg_indsendemail_status = dt.Rows[0]["RG_IndSendEmail_Status"].ToString();
            string rg_createddate = dt.Rows[0]["RG_CreatedDate"].ToString();
            string recycle = dt.Rows[0]["recycle"].ToString();
            string rg_stage = dt.Rows[0]["RG_Stage"].ToString();

            string rg_age = dt.Rows[0]["RG_Age"].ToString();
            string rg_dob = dt.Rows[0]["RG_DOB"].ToString();
            string rg_gender = dt.Rows[0]["RG_Gender"].ToString();
            string rg_additional4 = dt.Rows[0]["RG_Additional4"].ToString();
            string rg_additional5 = dt.Rows[0]["RG_Additional5"].ToString();

            try
            {
                if (!String.IsNullOrEmpty(rg_salutation))
                {
                    ListItem listItem = ddlSalutation.Items.FindByValue(rg_salutation);
                    if (listItem != null)
                    {
                        ddlSalutation.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtSalOther.Text = rg_salother;
            txtFName.Text = rg_contactfname;
            txtLName.Text = rg_contactlname;
            txtDesignation.Text = rg_designation;
            txtDepartment.Text = rg_department;
            txtCompany.Text = rg_company;

            try
            {
                if (!String.IsNullOrEmpty(rg_country))
                {
                    ListItem listItem = ddlCountry.Items.FindByValue(rg_country);
                    if (listItem != null)
                    {
                        ddlCountry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            try
            {
                if (!String.IsNullOrEmpty(rg_rcountry))
                {
                    ListItem listItem = ddlRCountry.Items.FindByValue(rg_rcountry);
                    if (listItem != null)
                    {
                        ddlRCountry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtAddress1.Text = rg_address1;
            txtAddress2.Text = rg_address2;
            txtAddress3.Text = rg_address3;
            txtCity.Text = rg_city;
            txtState.Text = rg_stateprovince;
            txtPostalcode.Text = rg_postalcode;
            try
            {
                if (!String.IsNullOrEmpty(rg_industry))
                {
                    ListItem listItem = ddlIndustry.Items.FindByValue(rg_industry);
                    if (listItem != null)
                    {
                        ddlIndustry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtIndusOther.Text = rg_industryothers;
            txtTelcc.Text = rg_telcc;
            txtTelac.Text = rg_telac;
            txtTel.Text = rg_tel;
            txtMobcc.Text = rg_mobilecc;
            txtMobac.Text = rg_mobileac;
            txtMobile.Text = rg_mobile;
            txtFaxcc.Text = rg_faxcc;
            txtFaxac.Text = rg_faxac;
            txtFax.Text = rg_fax;
            txtEmail.Text = rg_contactemail;
            txtEmailConfirmation.Text = rg_contactemail;

            txtVisitDate.Text = rg_visitdate;
            txtVisitTime.Text = rg_visittime;
            txtPassword.Text = rg_password;

            txtAge.Text = rg_age;
            txtDOB.Text = !string.IsNullOrEmpty(rg_dob) ? Convert.ToDateTime(rg_dob).ToString("dd/MM/yyyy") : "";
            try
            {
                if (!String.IsNullOrEmpty(rg_gender))
                {
                    ListItem listItem = ddlGender.Items.FindByValue(rg_gender);
                    if (listItem != null)
                    {
                        ddlGender.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            if (ddlAdditional4.Items.Count > 1)
            {
                try
                {
                    if (!String.IsNullOrEmpty(rg_additional4))
                    {
                        ListItem listItem = ddlAdditional4.Items.FindByText(rg_additional4);
                        if (listItem != null)
                        {
                            ddlAdditional4.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                txtAdditional4.Text = rg_additional4;
            }

            if (ddlAdditional5.Items.Count > 1)
            {
                try
                {
                    if (!String.IsNullOrEmpty(rg_additional5))
                    {
                        ListItem listItem = ddlAdditional5.Items.FindByText(rg_additional5);
                        if (listItem != null)
                        {
                            ddlAdditional5.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                txtAdditional5.Text = rg_additional5;
            }

            lblDelegate.Text = rg_referralcode;
            if(!string.IsNullOrEmpty(rg_referralcode) && !string.IsNullOrWhiteSpace(rg_referralcode))
            {
                chkIsDelegate.Checked = true;
            }
        }
    }
    #endregion

    #region ddlSalutation_SelectedIndexChanged (set 'other salutation div' visibility if the selection of ddlSalutation dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherSalutation and form_type='G')
    protected void ddlSalutation_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlSalutation.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlSalutation.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeGroup, _OtherSalutation);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divSalOther.Visible = true;
                        }
                        else
                        {
                            divSalOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtSalOther.Attributes.Add("required", "");
                            vcSalOther.Enabled = true;
                        }
                        else
                        {
                            //txtSalOther.Attributes.Remove("required");
                            vcSalOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divSalOther.Visible = false;
                    //txtSalOther.Attributes.Remove("required");
                    vcSalOther.Enabled = false;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlIndustry_SelectedIndexChanged (set 'other industry div' visibility if the selection of ddlIndustry dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherIndustry and form_type='G')
    protected void ddlIndustry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlIndustry.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlIndustry.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeGroup, _OtherIndustry);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divIndusOther.Visible = true;
                        }
                        else
                        {
                            divIndusOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtIndusOther.Attributes.Add("required", "");
                            vcIndusOther.Enabled = true;
                        }
                        else
                        {
                            //txtIndusOther.Attributes.Remove("required");
                            vcIndusOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divIndusOther.Visible = false;
                    //txtIndusOther.Attributes.Remove("required");
                    vcIndusOther.Enabled = false;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlCountry_SelectedIndexChanged (bind country code data to txtTelcc, txtMobcc, txtFaxcc textboxes according to the selected country)
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlCountry.Items.Count > 0)
            {
                string countryid = ddlCountry.SelectedItem.Value;

                if (ddlCountry.SelectedIndex == 0)
                {
                    countryid = Number.Zero;
                }

                CountryObj couObj = new CountryObj(fn);
                DataTable dt = couObj.getCountryByID(countryid);
                if (dt.Rows.Count > 0)
                {
                    string code = dt.Rows[0]["countryen"].ToString();

                    txtTelcc.Text = code;
                    txtMobcc.Text = code;
                    txtFaxcc.Text = code;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlRCountry_SelectedIndexChanged (bind country code data to txtTelcc, txtMobcc, txtFaxcc textboxes according to the selected country)
    protected void ddlRCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlRCountry.Items.Count > 0)
            {
                string countryid = ddlRCountry.SelectedItem.Value;

                if (ddlRCountry.SelectedIndex == 0)
                {
                    countryid = Number.Zero;
                }

                CountryObj couObj = new CountryObj(fn);
                DataTable dt = couObj.getCountryByID(countryid);
                if (dt.Rows.Count > 0)
                {
                    string code = dt.Rows[0]["countryen"].ToString();

                    txtTelcc.Text = code;
                    txtMobcc.Text = code;
                    txtFaxcc.Text = code;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region btnNext_Click (save/update data into tb_RegGroup table & get next route and redirect to next page according to the site flow settings (tb_site_flow table))
    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (!cFun.validatePhoneCode(txtTelcc.Text.ToString())
                //|| !cFun.validatePhoneCode(txtTelac.Text.ToString())
                || !cFun.validatePhoneCode(txtMobcc.Text.ToString())
                //|| !cFun.validatePhoneCode(txtMobac.Text.ToString())
                || !cFun.validatePhoneCode(txtFaxcc.Text.ToString())
                //|| !cFun.validatePhoneCode(txtFaxac.Text.ToString())
                )
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Phone code is not valid.');", true);
                return;
            }

            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                Boolean isvalidpage = isValidPage(showid, urlQuery);
                if (isvalidpage)
                {
                    int isSuccess = 0;
                    Boolean hasid = false;
                    string groupid = string.Empty;

                    RegGroupObj rgg = new RegGroupObj(fn);

                    groupid = cFun.DecryptValue(urlQuery.GoupRegID);

                    DataTable dt = rgg.getRegGroupByID(groupid, showid);
                    if (dt.Rows.Count > 0)
                    {
                        hasid = true;
                    }

                    string salutation = string.Empty;
                    string fname = string.Empty;
                    string lname = string.Empty;
                    string designation = string.Empty;
                    string otherdesignation = string.Empty;//*
                    string department = string.Empty;
                    string company = string.Empty;
                    string industry = string.Empty;
                    string address1 = string.Empty;
                    string address2 = string.Empty;
                    string address3 = string.Empty;
                    string city = string.Empty;
                    string state = string.Empty;
                    string postalcode = string.Empty;
                    string country = string.Empty;
                    string rcountry = string.Empty;
                    string telcc = string.Empty;
                    string telac = string.Empty;
                    string tel = string.Empty;
                    string mobilecc = string.Empty;
                    string mobileac = string.Empty;
                    string mobile = string.Empty;
                    string faxcc = string.Empty;
                    string faxac = string.Empty;
                    string fax = string.Empty;
                    string email = string.Empty;
                    string remark = string.Empty;
                    string Type_NHG_NonNHG = string.Empty;
                    string sal_other = string.Empty;
                    string desig_other = string.Empty;
                    string indus_other = string.Empty;
                    string visitdate = string.Empty;
                    string visittime = string.Empty;
                    string password = string.Empty;
                    string ismul = SiteFlowType.FLOW_GROUP;
                    string reffalcode = string.Empty;
                    int isFromsale = 0;
                    int isSendEmail = 0;
                    int isIndivSendEmail = 0;
                    string createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                    int recycle = 0;
                    string stage = string.Empty;

                    int age = 0;
                    DateTime? dob = null;
                    string dob_str = string.Empty;
                    string gender = string.Empty;
                    string additional4 = string.Empty;
                    string additional5 = string.Empty;

                    salutation = cFun.solveSQL(ddlSalutation.SelectedItem.Value.Trim());
                    fname = cFun.solveSQL(txtFName.Text.Trim());
                    lname = cFun.solveSQL(txtLName.Text.Trim());
                    designation = cFun.solveSQL(txtDesignation.Text.Trim());
                    otherdesignation = cFun.solveSQL(txtOtherDesignation.Text.Trim());//*
                    department = cFun.solveSQL(txtDepartment.Text.Trim());
                    company = cFun.solveSQL(txtCompany.Text.Trim());
                    address1 = cFun.solveSQL(txtAddress1.Text.Trim());
                    address2 = cFun.solveSQL(txtAddress2.Text.Trim());
                    address3 = cFun.solveSQL(txtAddress3.Text.Trim());
                    city = cFun.solveSQL(txtCity.Text.Trim());
                    postalcode = cFun.solveSQL(txtPostalcode.Text.Trim());
                    state = cFun.solveSQL(txtState.Text.Trim());
                    country = ddlCountry.SelectedItem.Value.ToString();
                    rcountry = ddlRCountry.SelectedItem.Value.ToString();
                    telcc = txtTelcc.Text.ToString();
                    telac = txtTelac.Text.ToString();
                    tel = txtTel.Text.ToString();
                    mobilecc = txtMobcc.Text.ToString();
                    mobileac = txtMobac.Text.ToString();
                    mobile = txtMobile.Text.ToString();
                    faxcc = txtFaxcc.Text.ToString();
                    faxac = txtFaxac.Text.ToString();
                    fax = txtFax.Text.ToString();
                    email = cFun.solveSQL(txtEmail.Text.Trim());

                    if ((ddlSalutation.SelectedIndex == ddlSalutation.Items.Count - 1) && !String.IsNullOrEmpty(txtSalOther.Text))
                    {
                        sal_other = cFun.solveSQL(txtSalOther.Text.ToString());
                    }

                    industry = ddlIndustry.SelectedItem.Value.ToString();
                    if ((ddlIndustry.SelectedIndex == ddlIndustry.Items.Count - 1) && !String.IsNullOrEmpty(txtIndusOther.Text))
                    {
                        indus_other = cFun.solveSQL(txtIndusOther.Text.ToString());
                    }
                    visitdate = cFun.solveSQL(txtVisitDate.Text.Trim());
                    visittime = cFun.solveSQL(txtVisitTime.Text.Trim());
                    password = cFun.solveSQL(txtPassword.Text.Trim());

                    if (!String.IsNullOrWhiteSpace(txtAge.Text))
                    {
                        age = Convert.ToInt32(cFun.solveSQL(txtAge.Text.Trim()));
                    }
                    if (!String.IsNullOrWhiteSpace(txtDOB.Text))
                    {
                        if (!cFun.validateDate(txtDOB.Text.Trim()))
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                            return;
                        }

                        dob = DateTime.ParseExact(txtDOB.Text.Trim(), "dd/MM/yyyy", null);
                        dob_str = dob.Value.ToString("yyyy-MM-dd hh:mm:ss");
                    }
                    gender = cFun.solveSQL(ddlGender.SelectedItem.Value.Trim());
                    additional4 = cFun.solveSQL(ddlAdditional4.Items.Count > 1 ? ddlAdditional4.SelectedItem.Text : txtAdditional4.Text.Trim());
                    additional5 = cFun.solveSQL(ddlAdditional5.Items.Count > 1 ? ddlAdditional5.SelectedItem.Text : txtAdditional5.Text.Trim());

                    stage = cFun.DecryptValue(urlQuery.CurrIndex);

                    string regno = string.Empty;

                    rgg.groupid = groupid;
                    rgg.salutation = salutation;
                    rgg.fname = fname;
                    rgg.lname = lname;
                    rgg.designation = designation;
                    rgg.desig_other = otherdesignation;//*
                    rgg.department = department;
                    rgg.company = company;
                    rgg.industry = industry;
                    rgg.address1 = address1;
                    rgg.address2 = address2;
                    rgg.address3 = address3;
                    rgg.city = city;
                    rgg.state = state;
                    rgg.postalcode = postalcode;
                    rgg.country = country;
                    rgg.rcountry = rcountry;
                    rgg.telcc = telcc;
                    rgg.telac = telac;
                    rgg.tel = tel;
                    rgg.mobilecc = mobilecc;
                    rgg.mobileac = mobileac;
                    rgg.mobile = mobile;
                    rgg.faxcc = faxcc;
                    rgg.faxac = faxac;
                    rgg.fax = fax;
                    rgg.email = email;
                    rgg.remark = remark;
                    rgg.Type_NHG_NonNHG = Type_NHG_NonNHG;
                    rgg.sal_other = sal_other;
                    rgg.indus_other = indus_other;
                    rgg.visitdate = visitdate;
                    rgg.visittime = visittime;
                    rgg.password = password;
                    rgg.ismul = ismul;
                    rgg.reffalcode = regno;//reffalcode;
                    rgg.isFromsale = isFromsale;
                    rgg.isSendEmail = isSendEmail;
                    rgg.isIndivSendEmail = isIndivSendEmail;
                    rgg.createdate = createdate;
                    rgg.recycle = recycle;
                    rgg.stage = stage;

                    rgg.age = age;
                    rgg.dob = dob_str;
                    rgg.gender = gender;
                    rgg.additional4 = additional4;
                    rgg.additional5 = additional5;

                    rgg.showID = showid;

                    try
                    {
                        string actType = string.Empty;

                        bool isAlreadyExist = false;
                        bool isAlreadyExistDelegate = false;

                        FlowControler fCon = new FlowControler(fn);
                        FlowMaster flwMaster = fCon.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));

                        SetUpController setCtrl = new SetUpController(fn);
                        //if (cFun.validateDate(dob.Value.ToString()))
                        {
                            if (hasid)
                            {
                                //*Update
                                isAlreadyExist = rgg.checkUpdateExist();

                                RegDelegateObj rgd = new RegDelegateObj(fn);
                                bool isdelegate = false;
                                bool isdelegateNew = false;
                                #region I want to attend the show.
                                if (chkIsDelegate.Checked == true)
                                {
                                    isdelegate = true;
                                    int con_categoryID = 0;
                                    CategoryObj catObj = new CategoryObj(fn);
                                    int.TryParse(catObj.checkCategory(urlQuery, regno, ""), out con_categoryID);

                                    string oname = string.Empty;
                                    string passno = string.Empty;

                                    int isreg = 0;
                                    string regspecific = string.Empty;//MCR/SNB/PRN
                                    string idno = string.Empty;//MCR/SNB/PRN No.
                                    string staffid = string.Empty;//no use in design for this field
                                    string jobtitle = string.Empty;//if Profession is Allied Health
                                    string profession = string.Empty;
                                    if (!String.IsNullOrEmpty(industry))//***Business Category
                                    {
                                        string businessCat = setCtrl.getIndustryNameByID(industry, showid);
                                        profession = setCtrl.getProfessionIDByName(businessCat, showid);
                                    }
                                    if(string.IsNullOrEmpty(profession))
                                    {
                                        profession = "0";
                                    }

                                    string organization = string.Empty;
                                    if (!String.IsNullOrEmpty(additional4))//***Main Job Function
                                    {
                                        string mainjob = additional4;//setCtrl.getRefAdditionalListNameByID(additional4, showid);
                                        organization = setCtrl.getOrganisationIDByName(mainjob, showid);
                                    }
                                    if (string.IsNullOrEmpty(organization))
                                    {
                                        organization = "0";
                                    }

                                    string institution = "0";
                                    string address4 = string.Empty;
                                    string affiliation = string.Empty;
                                    string dietary = string.Empty;
                                    if (!String.IsNullOrEmpty(rcountry))//***Country of Birth
                                    {
                                        CountryObj countryObj = new CountryObj(fn);
                                        string countryofbirth = countryObj.getCountryNameByID(rcountry, showid);
                                        dietary = setCtrl.getDietaryIDByName(countryofbirth, showid);
                                    }
                                    if (string.IsNullOrEmpty(dietary))
                                    {
                                        dietary = "0";
                                    }

                                    string nationality = string.Empty;
                                    string memberno = string.Empty;

                                    string vname = string.Empty;
                                    string vdob = string.Empty;
                                    string vpassno = string.Empty;
                                    string vpassexpiry = string.Empty;
                                    string vpassissuedate = string.Empty;
                                    string vembarkation = string.Empty;
                                    string varrivaldate = string.Empty;
                                    string vcountry = string.Empty;

                                    string udfcname = string.Empty;
                                    string udfdeltype = string.Empty;
                                    string udfprofcat = string.Empty;
                                    string udfprofcatother = string.Empty;
                                    string udfcpcode = string.Empty;
                                    string udfcldept = string.Empty;
                                    string udfcaddress = string.Empty;
                                    string udfclcompany = string.Empty;
                                    string udfclcompanyother = string.Empty;
                                    string udfccountry = string.Empty;

                                    string supname = string.Empty;
                                    string supdesignation = string.Empty;
                                    string supcontact = string.Empty;
                                    string supemail = string.Empty;

                                    string otherprof = string.Empty;
                                    string otherdept = string.Empty;
                                    string otherorg = string.Empty;

                                    string aemail = string.Empty;
                                    int isSMS = 0;

                                    string remark_groupupload = string.Empty;
                                    int approvestatus = 0;

                                    rgd.groupid = groupid;
                                    rgd.regno = regno;
                                    rgd.con_categoryID = con_categoryID;
                                    rgd.salutation = salutation;
                                    rgd.fname = fname;
                                    rgd.lname = lname;
                                    rgd.oname = oname;
                                    rgd.passno = passno;
                                    rgd.isreg = isreg;
                                    rgd.regspecific = regspecific;//MCR/SNB/PRN
                                    rgd.idno = idno;//MCR/SNB/PRN No.
                                    rgd.staffid = staffid;//no use in design for this field
                                    rgd.designation = designation;
                                    rgd.jobtitle = jobtitle;//if Profession is Allied Health
                                    rgd.profession = profession;
                                    rgd.department = department;
                                    rgd.organization = organization;//company;
                                    rgd.institution = institution;
                                    rgd.address1 = address1;
                                    rgd.address2 = address2;
                                    rgd.address3 = address3;
                                    rgd.address4 = address4;
                                    rgd.city = city;
                                    rgd.state = state;
                                    rgd.postalcode = postalcode;
                                    rgd.country = country;
                                    rgd.rcountry = "0";
                                    rgd.telcc = telcc;
                                    rgd.telac = telac;
                                    rgd.tel = tel;
                                    rgd.mobilecc = mobilecc;
                                    rgd.mobileac = mobileac;
                                    rgd.mobile = mobile;
                                    rgd.faxcc = faxcc;
                                    rgd.faxac = faxac;
                                    rgd.fax = fax;
                                    rgd.email = email;
                                    rgd.affiliation = affiliation;
                                    rgd.dietary = dietary;
                                    rgd.nationality = nationality;
                                    rgd.age = age;
                                    rgd.dob = dob_str;
                                    rgd.gender = gender;
                                    rgd.additional4 = "";
                                    rgd.additional5 = "";
                                    rgd.memberno = memberno;

                                    rgd.vname = vname;
                                    rgd.vdob = vdob;
                                    rgd.vpassno = vpassno;
                                    rgd.vpassexpiry = vpassexpiry;
                                    rgd.vpassissuedate = vpassissuedate;
                                    rgd.vembarkation = vembarkation;
                                    rgd.varrivaldate = varrivaldate;
                                    rgd.vcountry = vcountry;

                                    rgd.udfcname = udfcname;
                                    rgd.udfdeltype = udfdeltype;
                                    rgd.udfprofcat = udfprofcat;
                                    rgd.udfprofcatother = udfprofcatother;
                                    rgd.udfcpcode = udfcpcode;
                                    rgd.udfcldept = udfcldept;
                                    rgd.udfcaddress = udfcaddress;
                                    rgd.udfclcompany = udfclcompany;
                                    rgd.udfclcompanyother = udfclcompanyother;
                                    rgd.udfccountry = udfccountry;

                                    rgd.supname = supname;
                                    rgd.supdesignation = supdesignation;
                                    rgd.supcontact = supcontact;
                                    rgd.supemail = supemail;

                                    rgd.othersal = sal_other;
                                    rgd.otherprof = otherdesignation;
                                    rgd.otherdept = otherdept;
                                    rgd.otherorg = otherorg;
                                    rgd.otherinstitution = indus_other;

                                    rgd.aemail = aemail;
                                    rgd.isSMS = isSMS;

                                    rgd.remark = remark;
                                    rgd.remark_groupupload = remark_groupupload;
                                    rgd.approvestatus = approvestatus;
                                    rgd.createdate = createdate;
                                    rgd.recycle = recycle;
                                    rgd.stage = stage;

                                    rgd.showID = showid;

                                    if (!string.IsNullOrEmpty(lblDelegate.Text) || !string.IsNullOrWhiteSpace(lblDelegate.Text))
                                    {
                                        regno = lblDelegate.Text.Trim();
                                        rgd.regno = regno;
                                        //***updated on 23-8-2018
                                        if (flwMaster.isDuplicateChecking == 1)//***check in complete registration status
                                        {
                                            isAlreadyExistDelegate = rgd.checkUpdateExist();
                                        }
                                        else if (flwMaster.isDuplicateChecking == 2)//***check in all(complete,pending) registration status
                                        {
                                            isAlreadyExistDelegate = rgd.checkUpdateExistWithoutRegStatus();
                                        }
                                        else if (flwMaster.isDuplicateChecking == 3)//***check in all(complete,pending) registration status
                                        {
                                            isAlreadyExistDelegate = rgd.checkUpdateExistWithFlowID(flowid);
                                        }
                                        //***updated on 23-8-2018
                                        isdelegateNew = false;

                                        ShowControler shwCtr = new ShowControler(fn);//***for FJ
                                        Show shw = shwCtr.GetShow(showid);
                                        if (checkingFJShowName.Contains(shw.SHW_Name))
                                        {
                                            memberno = haveRefNo(regno, showid, flowid);
                                            if (string.IsNullOrEmpty(memberno) || string.IsNullOrWhiteSpace(memberno))
                                            {
                                                SetUpController setObj = new SetUpController(fn);
                                                string reandomCode = setObj.generateReferenceNo(showid, flowid, regno, 7);
                                                memberno = reandomCode;
                                            }
                                        }
                                        rgd.memberno = memberno;//***for FJ
                                    }
                                    else
                                    {
                                        regno = rgd.GenDelegateNumber(showid);
                                        rgd.regno = regno;
                                        //***updated on 23-8-2018
                                        if (flwMaster.isDuplicateChecking == 1)//***check in complete registration status
                                        {
                                            isAlreadyExistDelegate = rgd.checkInsertExist();
                                        }
                                        else if (flwMaster.isDuplicateChecking == 2)//***check in all(complete,pending) registration status
                                        {
                                            isAlreadyExistDelegate = rgd.checkInsertExistWithoutRegStatus();
                                        }
                                        else if (flwMaster.isDuplicateChecking == 3)//***check in all(complete,pending) registration status
                                        {
                                            isAlreadyExistDelegate = rgd.checkInsertExistWithFLowID(flowid);
                                        }
                                        //***added on 7-8-2018 - than [updated on 23-8-2018]
                                        isdelegateNew = true;

                                        ShowControler shwCtr = new ShowControler(fn);//***for FJ
                                        Show shw = shwCtr.GetShow(showid);
                                        if (checkingFJShowName.Contains(shw.SHW_Name))
                                        {
                                            memberno = haveRefNo(regno, showid, flowid);
                                            if (string.IsNullOrEmpty(memberno) || string.IsNullOrWhiteSpace(memberno))
                                            {
                                                SetUpController setObj = new SetUpController(fn);
                                                string reandomCode = setObj.generateReferenceNo(showid, flowid, regno, 7);
                                                memberno = reandomCode;
                                            }
                                        }
                                        rgd.memberno = memberno;//***for FJ
                                    }
                                }
                                #endregion

                                if (isAlreadyExist == true)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('User already exists! Please enter a different user name or email.');", true);
                                    return;
                                }
                                else
                                {
                                    if (isdelegate)
                                    {
                                        if (isAlreadyExistDelegate == true)
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('User already exists as delegate! Please enter a different user name or email.');", true);
                                            return;
                                        }
                                        else
                                        {
                                            if (isdelegateNew)
                                            {
                                                isSuccess = rgd.saveRegDelegate();
                                                rgd.updateStep(regno, cFun.DecryptValue(urlQuery.FlowID), cFun.DecryptValue(urlQuery.CurrIndex), showid);
                                            }
                                            else
                                            {
                                                isSuccess = rgd.updateRegDelegate();
                                            }
                                        }
                                    }

                                    rgg.reffalcode = regno;//reffalcode;
                                    isSuccess = rgg.updateRegGroup();
                                    actType = rlgobj.actupdate;
                                }
                            }
                            else
                            {
                                //*Save
                                isAlreadyExist = rgg.checkInsertExist();
                                if (isAlreadyExist == true)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('User already exists! Please enter a different user name or email.');", true);
                                    return;
                                }
                                else
                                {

                                    RegDelegateObj rgd = new RegDelegateObj(fn);
                                    bool isdelegate = false;
                                    bool isdelegateNew = false;
                                    #region I want to attend the show.
                                    if (chkIsDelegate.Checked == true)
                                    {
                                        isdelegate = true;
                                        int con_categoryID = 0;
                                        CategoryObj catObj = new CategoryObj(fn);
                                        int.TryParse(catObj.checkCategory(urlQuery, regno, ""), out con_categoryID);

                                        string oname = string.Empty;
                                        string passno = string.Empty;

                                        int isreg = 0;
                                        string regspecific = string.Empty;//MCR/SNB/PRN
                                        string idno = string.Empty;//MCR/SNB/PRN No.
                                        string staffid = string.Empty;//no use in design for this field
                                        string jobtitle = string.Empty;//if Profession is Allied Health
                                        string profession = string.Empty;
                                        if (!String.IsNullOrEmpty(industry))//***Business Category
                                        {
                                            string businessCat = setCtrl.getIndustryNameByID(industry, showid);
                                            profession = setCtrl.getProfessionIDByName(businessCat, showid);
                                        }
                                        if(string.IsNullOrEmpty(profession))
                                        {
                                            profession = "0";
                                        }

                                        string organization = string.Empty;
                                        if (!String.IsNullOrEmpty(additional4))//***Main Job Function
                                        {
                                            string mainjob = additional4;//setCtrl.getRefAdditionalListNameByID(additional4, showid);
                                            organization = setCtrl.getOrganisationIDByName(mainjob, showid);
                                        }
                                        if (string.IsNullOrEmpty(organization))
                                        {
                                            organization = "0";
                                        }

                                        string institution = "0";
                                        string address4 = string.Empty;
                                        string affiliation = string.Empty;
                                        string dietary = string.Empty;
                                        if (!String.IsNullOrEmpty(rcountry))//***Country of Birth
                                        {
                                            CountryObj countryObj = new CountryObj(fn);
                                            string countryofbirth = countryObj.getCountryNameByID(rcountry, showid);
                                            dietary = setCtrl.getDietaryIDByName(countryofbirth, showid);
                                        }
                                        if (string.IsNullOrEmpty(dietary))
                                        {
                                            dietary = "0";
                                        }

                                        string nationality = string.Empty;
                                        string memberno = string.Empty;

                                        string vname = string.Empty;
                                        string vdob = string.Empty;
                                        string vpassno = string.Empty;
                                        string vpassexpiry = string.Empty;
                                        string vpassissuedate = string.Empty;
                                        string vembarkation = string.Empty;
                                        string varrivaldate = string.Empty;
                                        string vcountry = string.Empty;

                                        string udfcname = string.Empty;
                                        string udfdeltype = string.Empty;
                                        string udfprofcat = string.Empty;
                                        string udfprofcatother = string.Empty;
                                        string udfcpcode = string.Empty;
                                        string udfcldept = string.Empty;
                                        string udfcaddress = string.Empty;
                                        string udfclcompany = string.Empty;
                                        string udfclcompanyother = string.Empty;
                                        string udfccountry = string.Empty;

                                        string supname = string.Empty;
                                        string supdesignation = string.Empty;
                                        string supcontact = string.Empty;
                                        string supemail = string.Empty;

                                        string otherprof = string.Empty;
                                        string otherdept = string.Empty;
                                        string otherorg = string.Empty;

                                        string aemail = string.Empty;
                                        int isSMS = 0;

                                        string remark_groupupload = string.Empty;
                                        int approvestatus = 0;

                                        rgd.groupid = groupid;
                                        rgd.regno = regno;
                                        rgd.con_categoryID = con_categoryID;
                                        rgd.salutation = salutation;
                                        rgd.fname = fname;
                                        rgd.lname = lname;
                                        rgd.oname = oname;
                                        rgd.passno = passno;
                                        rgd.isreg = isreg;
                                        rgd.regspecific = regspecific;//MCR/SNB/PRN
                                        rgd.idno = idno;//MCR/SNB/PRN No.
                                        rgd.staffid = staffid;//no use in design for this field
                                        rgd.designation = designation;
                                        rgd.jobtitle = jobtitle;//if Profession is Allied Health
                                        rgd.profession = profession;
                                        rgd.department = department;
                                        rgd.organization = organization;//company;
                                        rgd.institution = institution;
                                        rgd.address1 = address1;
                                        rgd.address2 = address2;
                                        rgd.address3 = address3;
                                        rgd.address4 = address4;
                                        rgd.city = city;
                                        rgd.state = state;
                                        rgd.postalcode = postalcode;
                                        rgd.country = country;
                                        rgd.rcountry = "0";
                                        rgd.telcc = telcc;
                                        rgd.telac = telac;
                                        rgd.tel = tel;
                                        rgd.mobilecc = mobilecc;
                                        rgd.mobileac = mobileac;
                                        rgd.mobile = mobile;
                                        rgd.faxcc = faxcc;
                                        rgd.faxac = faxac;
                                        rgd.fax = fax;
                                        rgd.email = email;
                                        rgd.affiliation = affiliation;
                                        rgd.dietary = dietary;
                                        rgd.nationality = nationality;
                                        rgd.age = age;
                                        rgd.dob = dob_str;
                                        rgd.gender = gender;
                                        rgd.additional4 = "";
                                        rgd.additional5 = "";
                                        rgd.memberno = memberno;

                                        rgd.vname = vname;
                                        rgd.vdob = vdob;
                                        rgd.vpassno = vpassno;
                                        rgd.vpassexpiry = vpassexpiry;
                                        rgd.vpassissuedate = vpassissuedate;
                                        rgd.vembarkation = vembarkation;
                                        rgd.varrivaldate = varrivaldate;
                                        rgd.vcountry = vcountry;

                                        rgd.udfcname = udfcname;
                                        rgd.udfdeltype = udfdeltype;
                                        rgd.udfprofcat = udfprofcat;
                                        rgd.udfprofcatother = udfprofcatother;
                                        rgd.udfcpcode = udfcpcode;
                                        rgd.udfcldept = udfcldept;
                                        rgd.udfcaddress = udfcaddress;
                                        rgd.udfclcompany = udfclcompany;
                                        rgd.udfclcompanyother = udfclcompanyother;
                                        rgd.udfccountry = udfccountry;

                                        rgd.supname = supname;
                                        rgd.supdesignation = supdesignation;
                                        rgd.supcontact = supcontact;
                                        rgd.supemail = supemail;

                                        rgd.othersal = sal_other;
                                        rgd.otherprof = otherdesignation;
                                        rgd.otherdept = otherdept;
                                        rgd.otherorg = otherorg;
                                        rgd.otherinstitution = indus_other;

                                        rgd.aemail = aemail;
                                        rgd.isSMS = isSMS;

                                        rgd.remark = remark;
                                        rgd.remark_groupupload = remark_groupupload;
                                        rgd.approvestatus = approvestatus;
                                        rgd.createdate = createdate;
                                        rgd.recycle = recycle;
                                        rgd.stage = stage;

                                        rgd.showID = showid;

                                        if (!string.IsNullOrEmpty(lblDelegate.Text) || !string.IsNullOrWhiteSpace(lblDelegate.Text))
                                        {
                                            regno = lblDelegate.Text.Trim();
                                            rgd.regno = regno;
                                            //***updated on 23-8-2018
                                            if (flwMaster.isDuplicateChecking == 1)//***check in complete registration status
                                            {
                                                isAlreadyExistDelegate = rgd.checkUpdateExist();
                                            }
                                            else if (flwMaster.isDuplicateChecking == 2)//***check in all(complete,pending) registration status
                                            {
                                                isAlreadyExistDelegate = rgd.checkUpdateExistWithoutRegStatus();
                                            }
                                            else if (flwMaster.isDuplicateChecking == 3)//***check in all(complete,pending) registration status
                                            {
                                                isAlreadyExistDelegate = rgd.checkUpdateExistWithFlowID(flowid);
                                            }
                                            //***updated on 23-8-2018
                                            isdelegateNew = false;

                                            ShowControler shwCtr = new ShowControler(fn);//***for FJ
                                            Show shw = shwCtr.GetShow(showid);
                                            if (checkingFJShowName.Contains(shw.SHW_Name))
                                            {
                                                memberno = haveRefNo(regno, showid, flowid);
                                                if (string.IsNullOrEmpty(memberno) || string.IsNullOrWhiteSpace(memberno))
                                                {
                                                    SetUpController setObj = new SetUpController(fn);
                                                    string reandomCode = setObj.generateReferenceNo(showid, flowid, regno, 7);
                                                    memberno = reandomCode;
                                                }
                                            }
                                            rgd.memberno = memberno;//***for FJ
                                        }
                                        else
                                        {
                                            regno = rgd.GenDelegateNumber(showid);
                                            rgd.regno = regno;
                                            //***updated on 23-8-2018
                                            if (flwMaster.isDuplicateChecking == 1)//***check in complete registration status
                                            {
                                                isAlreadyExistDelegate = rgd.checkInsertExist();
                                            }
                                            else if (flwMaster.isDuplicateChecking == 2)//***check in all(complete,pending) registration status
                                            {
                                                isAlreadyExistDelegate = rgd.checkInsertExistWithoutRegStatus();
                                            }
                                            else if (flwMaster.isDuplicateChecking == 3)//***check in all(complete,pending) registration status
                                            {
                                                isAlreadyExistDelegate = rgd.checkInsertExistWithFLowID(flowid);
                                            }
                                            //***added on 7-8-2018 - than [updated on 23-8-2018]
                                            isdelegateNew = true;

                                            ShowControler shwCtr = new ShowControler(fn);//***for FJ
                                            Show shw = shwCtr.GetShow(showid);
                                            if (checkingFJShowName.Contains(shw.SHW_Name))
                                            {
                                                memberno = haveRefNo(regno, showid, flowid);
                                                if (string.IsNullOrEmpty(memberno) || string.IsNullOrWhiteSpace(memberno))
                                                {
                                                    SetUpController setObj = new SetUpController(fn);
                                                    string reandomCode = setObj.generateReferenceNo(showid, flowid, regno, 7);
                                                    memberno = reandomCode;
                                                }
                                            }
                                            rgd.memberno = memberno;//***for FJ
                                        }
                                    }
                                    #endregion

                                    if (isdelegate)
                                    {
                                        if (isAlreadyExistDelegate == true)
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('User already exists as delegate! Please enter a different user name or email.');", true);
                                            return;
                                        }
                                        else
                                        {
                                            if (isdelegateNew)
                                            {
                                                isSuccess = rgd.saveRegDelegate();
                                                rgd.updateStep(regno, cFun.DecryptValue(urlQuery.FlowID), cFun.DecryptValue(urlQuery.CurrIndex), showid);
                                            }
                                            else
                                            {
                                                isSuccess = rgd.updateRegDelegate();
                                            }
                                        }
                                    }

                                    rgg.reffalcode = regno;//reffalcode;
                                    isSuccess = rgg.saveRegGroup();
                                    actType = rlgobj.actsave;
                                }
                            }

                            if (isSuccess > 0)
                            {
                                insertLogFlowAction(groupid, "", actType, urlQuery);

                                FlowControler flwObj = new FlowControler(fn, urlQuery);

                                //*Send Email
                                EmailHelper esender = new EmailHelper();
                                string fullUrl = "FLW=" + cFun.DecryptValue(flwObj.FlowID) + "&STP=" + cFun.DecryptValue(urlQuery.CurrIndex)
                                    + "&GRP=" + cFun.DecryptValue(groupid) + "&SHW=" + cFun.DecryptValue(urlQuery.CurrShowID);//+ "&INV=" + cFun.DecryptValue(regno) 
                                FlowURLQuery reLoginFlow = new FlowURLQuery(fullUrl);
                                esender.SendCurrentFlowStepEmail(reLoginFlow);
                                //*Send Email

                                string showID = urlQuery.CurrShowID;
                                string page = flwObj.NextStepURL();
                                string step = flwObj.NextStep;
                                string FlowID = flwObj.FlowID;
                                string grpNum = "";
                                grpNum = urlQuery.GoupRegID;
                                string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, groupid, BackendRegType.backendRegType_Group);
                                Response.Redirect(route);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogGenEmail lggenemail = new LogGenEmail(fn);
                        lggenemail.type = GenLogDefaultValue.errorException;
                        lggenemail.RefNumber = groupid;
                        lggenemail.description = ex.Message;
                        lggenemail.remark = RegClass.typeGrp + cFun.DecryptValue(urlQuery.FlowID);
                        lggenemail.step = cFun.DecryptValue(urlQuery.CurrIndex);
                        lggenemail.writeLog();
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
        }
    }
    #endregion

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    #region setDivCss_TelMobFax
    public void setDivCss_TelMobFax(bool isShowCC, bool isShowAC, bool isShowPhoneNo, string type)
    {
        string name = string.Empty;
        try
        {
            if (type == "Tel")
            {
                #region type="Tel"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");
                    divTelcc.Attributes.Add("class", "col-xs-6");

                    divTelac.Attributes.Remove("class");
                    divTelac.Attributes.Add("class", "col-xs-6");

                    divTelNo.Attributes.Remove("class");
                }
                #endregion
            }
            else if (type == "Mob")
            {
                #region type="Mob"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divMobac.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");

                    divMobac.Attributes.Remove("class");

                    divMobileNo.Attributes.Remove("class");
                    divMobileNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divMobcc.Attributes.Remove("class");
                    divMobcc.Attributes.Add("class", "col-xs-6");

                    divMobac.Attributes.Remove("class");
                    divMobac.Attributes.Add("class", "col-xs-6");

                    divMobileNo.Attributes.Remove("class");
                }
                #endregion
            }
            else if (type == "Fax")
            {
                #region Type="Fax"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");
                    divFaxcc.Attributes.Add("class", "col-xs-6");

                    divFaxac.Attributes.Remove("class");
                    divFaxac.Attributes.Add("class", "col-xs-6");

                    divFaxNo.Attributes.Remove("class");
                }
                #endregion
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion


    #region isValidPage//***added on 25-6-2018
    protected Boolean isValidPage(string showid, FlowURLQuery urlQuery)
    {
        Boolean isvalid = true;

        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = UpdatePanel1.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = UpdatePanel1.FindControl("chk" + ftnObj.note_Type);
                                Control ctrlLbl = UpdatePanel1.FindControl("lblErr" + ftnObj.note_Type);
                                if (ctrlChk != null && ctrlLbl != null)
                                {
                                    CheckBoxList chkNote = UpdatePanel1.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    Label lblErrNote = UpdatePanel1.FindControl("lblErr" + ftnObj.note_Type) as Label;
                                    if (divFooter.Visible == true)
                                    {
                                        int countTerms = chkNote.Items.Count;
                                        if (countTerms > 0)
                                        {
                                            lblErrNote.Visible = false;
                                            string id = ftnObj.note_ID;
                                            int isSkip = 0;
                                            if (ftnObj != null)
                                            {
                                                isSkip = ftnObj.note_isSkip;
                                            }
                                            ListItem liItem = chkNote.Items.FindByValue(id);
                                            if (liItem != null)
                                            {
                                                if (isSkip == 0)
                                                {
                                                    if (liItem.Selected == false)
                                                    {
                                                        lblErrNote.Visible = true;
                                                        isvalid = false;
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('Please accept .');</script>", false);
                                                        return isvalid;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return isvalid;
    }
    #endregion
    #region bindFlowNote//***added on 25-6-2018
    private void bindFlowNote(string showid, FlowURLQuery urlQuery)
    {
        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = UpdatePanel1.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            divFooter.Visible = true;
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = UpdatePanel1.FindControl("chk" + ftnObj.note_Type);
                                if (ctrlChk != null)
                                {
                                    CheckBoxList chkFTRCHK = UpdatePanel1.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    ListItem newItem = new ListItem(displayTextTmpt, ftnObj.note_ID);
                                    chkFTRCHK.Items.Add(newItem);
                                }
                            }
                        }
                        else
                        {
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #region saveRegAdditional//***added on 25-6-2018
    protected void saveRegAdditional(string showid, string regno, FlowURLQuery urlQuery)
    {
        try
        {
            if (divFTRCHK.Visible == true)
            {
                int countTerms = chkFTRCHK.Items.Count;
                //int countCheckedTerms = chkFTRCHK.Items.Cast<ListItem>().Count(li => li.Selected);
                if (countTerms > 0)
                {
                    foreach (ListItem liItem in chkFTRCHK.Items)
                    {
                        string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                        string delegateid = regno;
                        string currentStep = cFun.DecryptValue(urlQuery.CurrIndex);
                        string delegateType = BackendRegType.backendRegType_Group;
                        string ownerID = groupid;
                        if (!string.IsNullOrEmpty(delegateid))
                        {
                            delegateType = BackendRegType.backendRegType_Delegate;
                            ownerID = delegateid;
                        }
                        RegAdditionalObj regAddObj = new RegAdditionalObj();
                        regAddObj.ad_ShowID = showid;
                        regAddObj.ad_FlowID = cFun.DecryptValue(urlQuery.FlowID);
                        regAddObj.ad_OwnerID = ownerID;
                        regAddObj.ad_FlowStep = currentStep;
                        regAddObj.ad_DelegateType = delegateType;
                        regAddObj.ad_Value = liItem.Selected == true ? "1" : "0";
                        regAddObj.ad_Type = FlowTemplateNoteType.FooterWithCheckBox;
                        regAddObj.ad_NoteID = cFun.ParseInt(liItem.Value);
                        RegAdditionalControler regAddCtrl = new RegAdditionalControler(fn);
                        regAddCtrl.SaveRegAdditional(regAddObj);
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region checkRefNo
    private string haveRefNo(string regno, string showid, string flowid)
    {
        string refNo = "";
        try
        {
            string sql = "Select * From tb_GeneratedReferenceNo Where key_regno='" + regno + "' And key_ShowID='" + showid + "' And key_FlowID='" + flowid + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                refNo = dt.Rows[0]["key_code"].ToString();
            }
        }
        catch (Exception ex)
        { }

        return refNo;
    }
    #endregion
}