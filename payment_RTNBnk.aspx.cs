﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Payment;
using Corpit.Registration;
using Corpit.Utilities;
using Corpit.Logging;

public partial class payment_RTNBnk : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string rtnPayge = "Error";
            string userRef = "";
            string RefNumber = "";
            string pAmount = "0.00";
            string Status = "";
            string Bank_TransactionID = "";
            Functionality fn = new Functionality();
            CommonFuns cFunz = new CommonFuns();
            try
            {
                if (Request.QueryString["ST"] != null) Status = Request.QueryString["ST"].ToString().Trim();
                if (Request.QueryString["RFN"] != null) RefNumber = Request.QueryString["RFN"].ToString().Trim();
                if (Request.QueryString["PAT"] != null) pAmount = Request.QueryString["PAT"].ToString().Trim();
                if (Request.QueryString["USR"] != null) userRef = Request.QueryString["USR"].ToString().Trim();
                if (Request.QueryString["BTRN"] != null) Bank_TransactionID = Request.QueryString["BTRN"].ToString().Trim();

                if (string.IsNullOrEmpty(RefNumber))
                {
                    //Log Error No Reference Number
                    Response.Redirect("404");
                }
                else
                {
                    Status = cFunz.DecryptValue(Status);
                    RefNumber = cFunz.DecryptValue(RefNumber);
                    pAmount = cFunz.DecryptValue(pAmount);
                    userRef = cFunz.DecryptValue(userRef);
                    Bank_TransactionID = cFunz.DecryptValue(Bank_TransactionID);

                    #region Logging added on 03-04-2019
                    try
                    {
                        LogGenEmail lggenemail = new LogGenEmail(fn);
                        LogActionObj lactObj = new LogActionObj();
                        lggenemail.type = LogType.generalType;
                        lggenemail.RefNumber = RefNumber + "-" + userRef;
                        lggenemail.description = "Reach Registration payment_RTNBnk.aspx" + RefNumber + "-" + userRef + "-" + Status + "-" + pAmount + "-" + Bank_TransactionID;
                        lggenemail.remark = "Reach Registration payment_RTNBnk.aspx " + RefNumber + "-" + userRef + "-" + Status + "-" + pAmount + "-" + Bank_TransactionID;
                        lggenemail.step = "payment_RTNBnk.aspx";
                        lggenemail.writeLog();
                    }
                    catch(Exception ex)
                    {
                        LogGenEmail lggenemail = new LogGenEmail(fn);
                        LogActionObj lactObj = new LogActionObj();
                        lggenemail.type = LogType.generalType;
                        lggenemail.RefNumber = RefNumber + "-" + userRef;
                        lggenemail.description = "Reach Registration - payment_RTNBnk.aspx" + RefNumber + "-" + userRef + "-" + Status + "-" + pAmount + "-" + Bank_TransactionID;
                        lggenemail.remark = ex.Message;
                        lggenemail.step = "payment_RTNBnk.aspx";
                        lggenemail.writeLog();
                    }
                    #endregion

                    PaymentControler pControl = new PaymentControler(fn);
                    PaymentLog pLog = pControl.GetPaymentLogObjByRef(RefNumber);

                    string FlowID = pLog.UrlFlowID;
                    string Indx = pLog.UrlStep;
                    string page = "";
                    string Showid = pLog.UrlShowID;

                    FlowControler flwObj = new FlowControler(fn, FlowID, Indx);
                    
                    if (Status == PaymentStatus.SUCCESS)
                    {
                        //Save Payment
                        // Lock the Invice
                        // Check it Amount Greater than Grand total
                        // Navigate To NEXT

                        InvoiceControler iControl = new InvoiceControler(fn);
                        decimal grandAmount = pLog.Amount;
                        PaymentObj payment = new PaymentObj();
                        payment.InvNo = pLog.InvoiceID;
                        payment.PaidAmount = cFunz.ParseDecimal(pAmount);
                        payment.PaidRefNo = pLog.PayRefNo;
                        payment.RegGroupId = pLog.RegGroupId;
                        payment.RegId = pLog.RegID;
                        payment.PaidBy = userRef;
                        payment.PaidType = ((int)PaymentType.CreditCard).ToString();
                        payment.BankTransactionNo = Bank_TransactionID;

                        bool isOK = pControl.AddCreditCardPayment(payment); //Add Paid

                        if (isOK)
                        {
                            decimal totalPaidAmt = pControl.GetPaymentTotalPaidAmtByInvNo(pLog.InvoiceID);

                            ////decimal totalGrandPrice = 0;
                            ////string invOwnerID = iControl.DefineInvoiceOwnerID(FlowID, pLog.RegGroupId, pLog.RegID); // Depend on FLow Type return GroupID or Delegate ID;
                            ////Invoice invObj = iControl.GetPendingInvoice(invOwnerID);
                            ////if (invObj != null)
                            ////{
                            ////    totalGrandPrice = Convert.ToDecimal(invObj.GrandTotal);
                            ////}

                            //*Check Whether total paid amt is equal to or greater than payable amount Or Not
                            //*if total paid amt is equal to or greater than payable amount, then update Invoice_Status=1 and Return True
                            //*else update the Invoice_Status accordingly
                            isOK = iControl.UpdateInvoiceStatus(pLog.InvoiceID, (int)PaymentType.CreditCard, totalPaidAmt);

                            if (isOK)//totalPaidAmt >= totalGrandPrice && 
                            {
                                iControl.UpdateInvoiceLock(pLog.InvoiceID, (int)InvoiceLock.Locaked); //Lock Invoice Frist ; For Multiple Payment

                                OrderControler oControler = new OrderControler(fn);
                                oControler.UpdateOrderStatusSuccess(pLog.InvoiceID);

                                page = flwObj.NextStepURL();
                                Indx = flwObj.NextStep;
                            }
                            else
                            {
                                page = flwObj.CurrIndexModule;
                                Indx = flwObj.CurrIndex;
                            }
                        }
                        else
                        {
                            page = flwObj.CurrIndexModule;
                            Indx = flwObj.CurrIndex;
                        }
                    }
                    else
                    {
                        //Navigte To Before Payment Payment
                        page = flwObj.CurrIndexModule;
                        Indx = flwObj.CurrIndex;
                    }

                    string grpNum = cFunz.EncryptValue(pLog.RegGroupId);
                    string regno = cFunz.EncryptValue(pLog.RegID);
                 
                    string route = flwObj.MakeFullURL(page, FlowID, Showid, grpNum, Indx, regno);
                    Response.Redirect(route);
                }
            }
            catch {
            }

        }
    }
}