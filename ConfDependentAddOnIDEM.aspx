﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="ConfDependentAddOnIDEM.aspx.cs" Inherits="ConfDependentAddOnIDEM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        <style type="text/css">
        .style1
        {
            width:2%;
        }

        .accTableStyle tbody > tr > td
        {
            border-top:0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-xs-12">
        </div>
        <div class="col-md-offset-2 col-md-8  col-xs-12 ConfItemPanel ">
            <asp:UpdatePanel ID="MainUpdatePanel" runat="server">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="PanelDependent" Visible="false">
                        <div id="divHDR1" runat="server" visible="false">
                            <div class="form-group row">
                                <div class="clear"></div>
                                <div class="col-md-12">
                                    <asp:Label ID="lblHDR1" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="divHDR2" runat="server" visible="false">
                            <div class="form-group row">
                                <div class="clear"></div>
                                <div class="col-md-12">
                                    <br />
                                    <br />
                                    <asp:Label ID="lblHDR2" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="divHDR3" runat="server" visible="false">
                            <div class="form-group row">
                                <div class="clear"></div>
                                <div class="col-md-12">
                                    <br />
                                    <br />
                                    <asp:Label ID="lblHDR3" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="divItemsHeader" runat="server">
                            <h4 style="padding-left: 20px; text-decoration: underline;">
                                <asp:Label ID="lblDependentHeader" runat="server" Text="Items"></asp:Label></h4>
                        </div>
                        <table class="table CssConfTable">
                            <thead>
                                <tr>
                                    <th class="col-xs-3" colspan="2">Description</th>
                                    <th class="col-xs-1  CssPriceColumn">Price</th>
                                    <th class="col-xs-1"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="RptDependentList" runat="server" OnItemDataBound="RptDependentList_DataBond">
                                    <ItemTemplate>
                                        <tr>
                                            <asp:TextBox runat="server" ID="txtConfItemID" Text='<%#Eval("con_DependentID")%>' Visible="false"></asp:TextBox>
                                            <asp:TextBox runat="server" ID="txtDisabledItems" Text='<%#Eval("disabledItems")%>' Visible="false"></asp:TextBox>
                                            <asp:TextBox runat="server" ID="txtConfType" Text='<%#Eval("con_Type")%>' Visible="false"></asp:TextBox>
                                            <asp:TextBox runat="server" ID="txtConfGroupID" Text='<%#Eval("ConfGroupID")%>' Visible="false"></asp:TextBox>
                                            <td class="style1">
                                                <asp:Image Width="80" Height="80" ID="imgLogo" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblDisplay"></asp:Label>
                                                <br />
                                                <asp:Label runat="server" ID="lblDescription"></asp:Label>
                                                <br />
                                                <table id="tblAccom" runat="server" visible="false" class="unstriped accTableStyle">
                                                    <tr id="trAccFullName1" runat="server" visible="false">
                                                        <td>
                                                            <table class="unstriped">
                                                                <tr id="trCountry1" runat="server" visible="false">
                                                                    <td><asp:Label ID="lblAccCountry1" runat="server" Text="Country"></asp:Label></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCountry1" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:CompareValidator ID="vcCountry1" runat="server" Enabled="false"
                                                                            ControlToValidate="ddlCountry1" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFullName1" runat="server" visible="false">
                                                                    <td>First Name</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName1" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcFirstName1" runat="server" Enabled="false"
                                                                        ControlToValidate="txtFirstName1" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCompany1" runat="server" visible="false">
                                                                    <td id="tdCompany1" runat="server">Company</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompany1" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcCompany1" runat="server" Enabled="false"
                                                                        ControlToValidate="txtCompany1" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional1" runat="server" visible="false">
                                                                    <td id="tdAdditional1" runat="server"></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional1" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional1" runat="server" Enabled="false"
                                                                        ControlToValidate="txtAdditional1" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional21" runat="server" visible="true">
                                                                    <td id="tdAdditional21" runat="server">Tour Selection</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional21" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional21" runat="server" Enabled="true"
                                                                        ControlToValidate="txtAdditional21" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAccFullName2" runat="server" visible="false">
                                                        <td>
                                                            <table class="unstriped">
                                                                <tr id="trCountry2" runat="server" visible="false">
                                                                    <td><asp:Label ID="lblAccCountry2" runat="server" Text="Country"></asp:Label></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCountry2" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:CompareValidator ID="vcCountry2" runat="server" Enabled="false"
                                                                            ControlToValidate="ddlCountry2" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFullName2" runat="server" visible="false">
                                                                    <td>First Name</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName2" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcFirstName2" runat="server" Enabled="false"
                                                                        ControlToValidate="txtFirstName2" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCompany2" runat="server" visible="false">
                                                                    <td id="tdCompany2" runat="server">Company</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompany2" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcCompany2" runat="server" Enabled="false"
                                                                        ControlToValidate="txtCompany2" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional2" runat="server" visible="false">
                                                                    <td id="tdAdditional2" runat="server"></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional2" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional2" runat="server" Enabled="false"
                                                                        ControlToValidate="txtAdditional2" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional22" runat="server" visible="true">
                                                                    <td id="tdAdditional22" runat="server">Tour Selection</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional22" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional22" runat="server" Enabled="true"
                                                                        ControlToValidate="txtAdditional22" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAccFullName3" runat="server" visible="false">
                                                        <td>
                                                            <table class="unstriped">
                                                                <tr id="trCountry3" runat="server" visible="false">
                                                                    <td><asp:Label ID="lblAccCountry3" runat="server" Text="Country"></asp:Label></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCountry3" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:CompareValidator ID="vcCountry3" runat="server" Enabled="false"
                                                                            ControlToValidate="ddlCountry3" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFullName3" runat="server" visible="false">
                                                                    <td>First Name</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName3" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcFirstName3" runat="server" Enabled="false"
                                                                        ControlToValidate="txtFirstName3" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCompany3" runat="server" visible="false">
                                                                    <td id="tdCompany3" runat="server">Company</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompany3" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcCompany3" runat="server" Enabled="false"
                                                                        ControlToValidate="txtCompany3" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional3" runat="server" visible="false">
                                                                    <td id="tdAdditional3" runat="server"></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional3" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional3" runat="server" Enabled="false"
                                                                        ControlToValidate="txtAdditional3" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional23" runat="server" visible="true">
                                                                    <td id="tdAdditional23" runat="server">Tour Selection</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional23" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional23" runat="server" Enabled="true"
                                                                        ControlToValidate="txtAdditional23" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAccFullName4" runat="server" visible="false">
                                                        <td>
                                                            <table class="unstriped">
                                                                <tr id="trCountry4" runat="server" visible="false">
                                                                    <td><asp:Label ID="lblAccCountry4" runat="server" Text="Country"></asp:Label></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCountry4" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:CompareValidator ID="vcCountry4" runat="server" Enabled="false"
                                                                            ControlToValidate="ddlCountry4" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFullName4" runat="server" visible="false">
                                                                    <td>First Name</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName4" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcFirstName4" runat="server" Enabled="false"
                                                                        ControlToValidate="txtFirstName4" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCompany4" runat="server" visible="false">
                                                                    <td id="tdCompany4" runat="server">Company</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompany4" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcCompany4" runat="server" Enabled="false"
                                                                        ControlToValidate="txtCompany4" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional4" runat="server" visible="false">
                                                                    <td id="tdAdditional4" runat="server"></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional4" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional4" runat="server" Enabled="false"
                                                                        ControlToValidate="txtAdditional4" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional24" runat="server" visible="true">
                                                                    <td id="tdAdditional24" runat="server">Tour Selection</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional24" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional24" runat="server" Enabled="true"
                                                                        ControlToValidate="txtAdditional24" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAccFullName5" runat="server" visible="false">
                                                        <td>
                                                            <table class="unstriped">
                                                                <tr id="trCountry5" runat="server" visible="false">
                                                                    <td><asp:Label ID="lblAccCountry5" runat="server" Text="Country"></asp:Label></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCountry5" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:CompareValidator ID="vcCountry5" runat="server" Enabled="false"
                                                                            ControlToValidate="ddlCountry5" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFullName5" runat="server" visible="false">
                                                                    <td>First Name</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName5" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcFirstName5" runat="server" Enabled="false"
                                                                        ControlToValidate="txtFirstName5" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCompany5" runat="server" visible="false">
                                                                    <td id="tdCompany5" runat="server">Company</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompany5" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcCompany5" runat="server" Enabled="false"
                                                                        ControlToValidate="txtCompany5" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional5" runat="server" visible="false">
                                                                    <td id="tdAdditional5" runat="server"></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional5" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional5" runat="server" Enabled="false"
                                                                        ControlToValidate="txtAdditional5" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional25" runat="server" visible="true">
                                                                    <td id="tdAdditional25" runat="server">Tour Selection</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional25" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional25" runat="server" Enabled="true"
                                                                        ControlToValidate="txtAdditional25" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAccFullName6" runat="server" visible="false">
                                                        <td>
                                                            <table class="unstriped">
                                                                <tr id="trCountry6" runat="server" visible="false">
                                                                    <td><asp:Label ID="lblAccCountry6" runat="server" Text="Country"></asp:Label></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCountry6" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:CompareValidator ID="vcCountry6" runat="server" Enabled="false"
                                                                            ControlToValidate="ddlCountry6" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFullName6" runat="server" visible="false">
                                                                    <td>First Name</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName6" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcFirstName6" runat="server" Enabled="false"
                                                                        ControlToValidate="txtFirstName6" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCompany6" runat="server" visible="false">
                                                                    <td id="tdCompany6" runat="server">Company</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompany6" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcCompany6" runat="server" Enabled="false"
                                                                        ControlToValidate="txtCompany6" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional6" runat="server" visible="false">
                                                                    <td id="tdAdditional6" runat="server"></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional6" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional6" runat="server" Enabled="false"
                                                                        ControlToValidate="txtAdditional6" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional26" runat="server" visible="true">
                                                                    <td id="tdAdditional26" runat="server">Tour Selection</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional26" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional26" runat="server" Enabled="true"
                                                                        ControlToValidate="txtAdditional26" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAccFullName7" runat="server" visible="false">
                                                        <td>
                                                            <table class="unstriped">
                                                                <tr id="trCountry7" runat="server" visible="false">
                                                                    <td><asp:Label ID="lblAccCountry7" runat="server" Text="Country"></asp:Label></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCountry7" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:CompareValidator ID="vcCountry7" runat="server" Enabled="false"
                                                                            ControlToValidate="ddlCountry7" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFullName7" runat="server" visible="false">
                                                                    <td>First Name</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName7" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcFirstName7" runat="server" Enabled="false"
                                                                        ControlToValidate="txtFirstName7" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCompany7" runat="server" visible="false">
                                                                    <td id="tdCompany7" runat="server">Company</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompany7" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcCompany7" runat="server" Enabled="false"
                                                                        ControlToValidate="txtCompany7" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional7" runat="server" visible="false">
                                                                    <td id="tdAdditional7" runat="server"></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional7" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional7" runat="server" Enabled="false"
                                                                        ControlToValidate="txtAdditional7" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional27" runat="server" visible="true">
                                                                    <td id="tdAdditional27" runat="server">Tour Selection</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional27" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional27" runat="server" Enabled="true"
                                                                        ControlToValidate="txtAdditional27" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAccFullName8" runat="server" visible="false">
                                                        <td>
                                                            <table class="unstriped">
                                                                <tr id="trCountry8" runat="server" visible="false">
                                                                    <td><asp:Label ID="lblAccCountry8" runat="server" Text="Country"></asp:Label></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCountry8" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:CompareValidator ID="vcCountry8" runat="server" Enabled="false"
                                                                            ControlToValidate="ddlCountry8" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFullName8" runat="server" visible="false">
                                                                    <td>First Name</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName8" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcFirstName8" runat="server" Enabled="false"
                                                                        ControlToValidate="txtFirstName8" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCompany8" runat="server" visible="false">
                                                                    <td id="tdCompany8" runat="server">Company</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompany8" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcCompany8" runat="server" Enabled="false"
                                                                        ControlToValidate="txtCompany8" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional8" runat="server" visible="false">
                                                                    <td id="tdAdditional8" runat="server"></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional8" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional8" runat="server" Enabled="false"
                                                                        ControlToValidate="txtAdditional8" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional28" runat="server" visible="true">
                                                                    <td id="tdAdditional28" runat="server">Tour Selection</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional28" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional28" runat="server" Enabled="true"
                                                                        ControlToValidate="txtAdditional28" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAccFullName9" runat="server" visible="false">
                                                        <td>
                                                            <table class="unstriped">
                                                                <tr id="trCountry9" runat="server" visible="false">
                                                                    <td><asp:Label ID="lblAccCountry9" runat="server" Text="Country"></asp:Label></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCountry9" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:CompareValidator ID="vcCountry9" runat="server" Enabled="false"
                                                                            ControlToValidate="ddlCountry9" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFullName9" runat="server" visible="false">
                                                                    <td>First Name</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName9" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcFirstName9" runat="server" Enabled="false"
                                                                        ControlToValidate="txtFirstName9" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCompany9" runat="server" visible="false">
                                                                    <td id="tdCompany9" runat="server">Company</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompany9" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcCompany9" runat="server" Enabled="false"
                                                                        ControlToValidate="txtCompany9" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional9" runat="server" visible="false">
                                                                    <td id="tdAdditional9" runat="server"></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional9" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional9" runat="server" Enabled="false"
                                                                        ControlToValidate="txtAdditional9" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional29" runat="server" visible="true">
                                                                    <td id="tdAdditional29" runat="server">Tour Selection</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional29" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional29" runat="server" Enabled="true"
                                                                        ControlToValidate="txtAdditional29" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAccFullName10" runat="server" visible="false">
                                                        <td>
                                                            <table class="unstriped">
                                                                <tr id="trCountry10" runat="server" visible="false">
                                                                    <td><asp:Label ID="lblAccCountry10" runat="server" Text="Country"></asp:Label></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCountry10" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:CompareValidator ID="vcCountry10" runat="server" Enabled="false"
                                                                            ControlToValidate="ddlCountry10" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFullName10" runat="server" visible="false">
                                                                    <td>First Name</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName10" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcFirstName10" runat="server" Enabled="false"
                                                                        ControlToValidate="txtFirstName10" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trCompany10" runat="server" visible="false">
                                                                    <td id="tdCompany10" runat="server">Company</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompany10" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcCompany10" runat="server" Enabled="false"
                                                                        ControlToValidate="txtCompany10" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional10" runat="server" visible="false">
                                                                    <td id="tdAdditional10" runat="server"></td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional10" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional10" runat="server" Enabled="false"
                                                                        ControlToValidate="txtAdditional10" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trAdditional210" runat="server" visible="true">
                                                                    <td id="td1" runat="server">Tour Selection</td>
                                                                    <td>:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAdditional210" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="vcAdditional210" runat="server" Enabled="true"
                                                                        ControlToValidate="txtAdditional210" Display="Dynamic"
                                                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="CssPriceColumn">
                                                <asp:TextBox runat="server" ID="txtConfPrice" Visible="false"></asp:TextBox>
                                                <asp:Label runat="server" ID="ConItemDisplayPrice" CssClass="CssPriceColumn"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Panel runat="server" ID="PanelShowSelect">
                                                    <asp:DropDownList runat="server" ID="ddlQty" OnSelectedIndexChanged="ddlConfItem_Onclick" AutoPostBack="true"
                                                        CausesValidation="false">
                                                        <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <telerik:RadButton ID="chkConfItem" runat="server" ButtonType="ToggleButton" ToggleType="CheckBox" CommandArgument='<%#Eval("con_DependentID")%>' OnClick="btnDConfItem_Onclick"
                                                        CausesValidation="false">
                                                    </telerik:RadButton>
                                                </asp:Panel>
                                                <asp:Panel runat="server" ID="PanleMsg">
                                                    <asp:Label runat="server" ID="lblMsg"></asp:Label>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </tbody>
                        </table>

                    </asp:Panel>
                    <div class="row">
                        <div class=" col-xs-12 col-md-offset-5 col-md-7 text-right">
                            <table class="table">
                                <tr>
                                    <td>Sub Total:</td>
                                    <td style="text-align: center;">
                                        <asp:Label runat="server" ID="lblSubTotal" Text="0.00"></asp:Label></td>
                                </tr>
                                <tr style="display: none;">
                                    <td>GST:</td>
                                    <td style="text-align: center;">
                                        <asp:Label runat="server" ID="lblGST" Text="0.00"></asp:Label></td>
                                </tr>
                                <tr id="trGrandTotal" runat="server">
                                    <td>Grand Total:</td>
                                    <td style="text-align: center;">
                                        <asp:Label runat="server" ID="lblGrandTotal" Text="0.00"></asp:Label></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                           <div class="row" style="padding-top:20px;">
                               <div class="pull-right col-lg-3"> 
                    <asp:Button runat="server" Text="Next" ID="btnNext" OnClick="btnNext_Onclick" CssClass="btn btn-block  MainButton" />
                                   </div>
                               </div>
                    <asp:TextBox runat="server" ID="txtCurrency" Visible="false"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtMainConfListTotal" Visible="false" Text="0.00"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtDependentCofTotal" Visible="false" Text="0.00"></asp:TextBox>

                    <asp:TextBox runat="server" ID="txtShowDependentItem" Text="1" Visible="false"></asp:TextBox>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

