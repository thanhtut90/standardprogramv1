﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Site.Email;
using Corpit.Email;
using Corpit.Utilities;
using Corpit.Registration;
using Newtonsoft.Json;
public partial class DownloadBadge : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFuz = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["DID"] != null)
            {
                string showID = cFuz.DecryptValue(Request.Params["SHW"].ToString());
                string regno = cFuz.DecryptValue(Request.Params["DID"].ToString());
                HTMLTemplateControler htmlControl = new HTMLTemplateControler();
                string template = htmlControl.CreateBadgeLetterHTMLTemplate(showID, regno);//htmlControl.CreateVisaLetterHTMLTemplate(showID, regno)
                template = Server.HtmlDecode(template);
                string rtnFile = htmlControl.CreatePDF(template, showID, regno, "Badge", true);
                if (!string.IsNullOrEmpty(rtnFile))
                    ShowPDF.Attributes["src"] = rtnFile;
            }
        }
    }  
 
}