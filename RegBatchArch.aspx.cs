﻿using Corpit.BackendMaster;
using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegGroupARC : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    protected string language = string.Empty;

    static string[] _OtherSalutationValues = { "Other", "Others", "其他", "อื่นๆ" };
    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string others_value = "Others";
    static string other_value = "Other";
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);

            if (!string.IsNullOrEmpty(showid))
            {
                lblShowid.Text = showid;
                if (setDynamicForm(flowid, showid))
                {
                    BindDropDown();

                    string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                    string regno = cFun.DecryptValue(urlQuery.DelegateID);
                    if (showid != "WVF342")
                    {
                        ddlDesignation.Items[0].Text = "กรุณาเลือก";
                        ddlSalutations.Items[0].Text = "กรุณาเลือก";
                        lblHeader.Text = "กรุณากรอกข้อมูลด้านล่าง หากท่านต้องการลงทะเบียนเป็นหมู่คณะ หรือคลิก \"ยืนยัน\" เพื่อลงทะเบียน";
                        btnCancel.Text = "ยกเลิก";
                        btnUpdate.Text = "ยืนยันการแก้ไข";
                        btnSave.Text = "เพิ่มรายชื่อ";
                        btnNext.Text = "ยืนยัน";
                        lblGrpMemHeader.InnerText = "รายชื่อผู้เข้าร่วมชมงาน";
                        vcDesig.ErrorMessage = "* โปรดระบุ";
                        vcEmail.ErrorMessage = "* โปรดระบุ";
                        vcFName.ErrorMessage = "* โปรดระบุ";
                        vcLName.ErrorMessage = "* โปรดระบุ";
                        vcMobile.ErrorMessage = "* โปรดระบุ";
                        vcOtherDesig.ErrorMessage = "* โปรดระบุ";
                        vcOtherSal.ErrorMessage = "* โปรดระบุ";
                        vcSalutation.ErrorMessage = "* โปรดระบุ";
                        validateEmail.ErrorMessage = "โปรดระบุที่อยู่อีเมล์ที่ถูกต้อง";
                    }

                    if (Request.Params["t"] != null)
                    {
                        string admintype = cFun.DecryptValue(Request.QueryString["t"].ToString());

                        if (admintype == BackendStaticValueClass.isAdmin)
                        {
                            btnSave.Visible = false;
                            btnSave.Enabled = false;
                        }
                        else
                        {
                            bindPageLoad(showid, flowid, groupid, regno, urlQuery);
                        }
                    }
                    else
                    {
                        bindPageLoad(showid, flowid, groupid, regno, urlQuery);
                    }

                    if (Session["Regno"] != null)
                    {
                        LoadDelegateInfo(Session["Regno"].ToString());
                        Session["Regno"] = null;
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region BindGrid and use table ref_salutation & bind gvList & set pnllist visibility true or false
    protected void BindGrid(string showid)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string groupid = cFun.DecryptValue(urlQuery.GoupRegID);

        DataTable dt = new DataTable();
        RegDelegateObj rgd = new RegDelegateObj(fn);

        string query = string.Format("select regNo, regGroupId, sal.Sal_Name, reg_FName, reg_LName, reg_Designation, reg_Email, reg_OtherProfession, reg_SalutationOthers, reg_Mobile from tb_RegDelegate as rdg inner join ref_Salutation as sal on sal.sal_Id=rdg.reg_Salutation Where rdg.RegGroupId = '{0}' and rdg.ShowId = '{1}' and rdg.reg_Additional5='1'", groupid, showid);

        dt = fn.GetDatasetByCommand(query, "ds").Tables[0];

        if (dt.Rows.Count > 0)
        {
            gvList.DataSource = dt;
            gvList.DataBind();
            pnllist.Visible = true;
        }
        else
        {
            pnllist.Visible = false;
        }
    }
    #endregion

    #region bindPageLoad
    private void bindPageLoad(string showid, string flowid, string groupid, string regno, FlowURLQuery urlQuery)
    {
        btnSave.Visible = true;
        btnSave.Enabled = true;

        CommonFuns cFun = new CommonFuns();

        if (!String.IsNullOrEmpty(groupid))
        {
            //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
            RegGroupObj rgg = new RegGroupObj(fn);
            rgg.updateGroupCurrentStep(urlQuery);

            //*update reg_urlFlowID(current flowid) and reg_Stage(current step) of tb_RegDelegate table
            RegDelegateObj rgd = new RegDelegateObj(fn);
            rgd.updateDelegateCurrentStep(urlQuery);

            BindGrid(showid);

            insertLogFlowAction(groupid, regno, rlgobj.actview, urlQuery);
        }
        else
        {
            Response.Redirect("DefaultRegIndex.aspx");
        }
    }
    #endregion

    #region setDynamicForm (set div visibility and validator controls' enability dynamically (generate dynamic form) according to the settings of tb_Form table where form_type='D')
    protected bool setDynamicForm(string flowid, string showid)
    {
        bool isValidShow = false;

        DataSet ds = new DataSet();
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForDelegate();

        int isVisitorVisible = 0;
        int isUDFVisible = 0;
        int isSupervisorVisible = 0;

        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
        {
            isValidShow = true;

            #region set divSalutation visibility is true or false if form_input_name is Salutation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Salutation);

                lblSalutation.Text = labelname.ToUpper() + "<span style=\"color:red;\">*</span>";
                gvList.Columns[0].HeaderText = labelname;
                vcSalutation.ControlToValidate = "ddlSalutations";
                vcSalutation.Enabled = true;
            }
            #endregion

            #region set divFName visibility is true or false if form_input_name is FName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Fname);

                lblFName.Text = labelname.ToUpper() + "<span style=\"color:red;\">*</span>";
                gvList.Columns[1].HeaderText = labelname;
                vcFName.Enabled = true;
                //if (isrequired == 1)
                //{
                //    //lblFName.Text = "<span class=\"red\">^</span> " + labelname;
                //    txtFName.Attributes.Add("required", "");
                //    vcFName.Enabled = true;
                //}
                //else
                //{
                //    //lblFName.Text = labelname;
                //    txtFName.Attributes.Remove("required");
                //    vcFName.Enabled = false;
                //}
            }
            #endregion

            #region set divLName visibility is true or false if form_input_name is LName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Lname);

                lblLName.Text = labelname.ToUpper() + "<span style=\"color:red;\">*</span>";
                gvList.Columns[2].HeaderText = labelname;
                vcLName.Enabled = true;

                //if (isrequired == 1)
                //{
                //    //lblLName.Text = "<span class=\"red\">^</span> " + labelname;
                //    txtLName.Attributes.Add("required", "");
                //    vcLName.Enabled = true;
                //}
                //else
                //{
                //    //lblLName.Text = labelname;
                //    txtLName.Attributes.Remove("required");
                //    vcLName.Enabled = false;
                //}
            }
            #endregion
            #region set divPassNo visibility is true or false if form_input_name is PassNo according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            /*if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                //if (isshow == 1)
                //{
                //    divPassNo.Visible = true;
                //}
                //else
                //{
                //    divPassNo.Visible = false;
                //}

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_PassNo);

                if (showid == "WVF342")
                {
                    lblPassNo.Text = "Job Title";
                }
                else
                {
                    lblPassNo.Text = "การแต่งตั้ง";
                }
            }*/
            #endregion

            #region set divDesignation visibility is true or false if form_input_name is Designation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Designation);
                if (showid == "WVF342")
                {
                    lblDesignation.Text = "CAREER" + "<span style=\"color:red;\">*</span>";
                    gvList.Columns[3].HeaderText = "CAREER";
                }
                else
                {
                    //lblDesignation.Text = "การแต่งตั้ง";
                    //gvList.Columns[3].HeaderText = "การแต่งตั้ง";
                    lblDesignation.Text = "อาชีพ" + "<span style=\"color:red;\">*</span>";
                    gvList.Columns[3].HeaderText = "อาชีพ";
                }
                vcDesig.Enabled = true;
            }
            #endregion

            #region set divProfession visibility is true or false if form_input_name is Profession according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            /*if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                divJobtitle.Visible = false;
                vcJobtitle.Enabled = false;
                divStudentType.Visible = false;
                vcStudentType.Enabled = false;
                divStudentOther.Visible = false;
                vcStudentOther.Enabled = false;
                divStudentUpload.Visible = false;
                divDoctor.Visible = false;

                if (isshow == 1)
                {
                    divProfession.Visible = true;
                }
                else
                {
                    divProfession.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Profession);
                if (isrequired == 1)
                {
                    lblProfession.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + labelname;
                    //ddlProfession.Attributes.Add("required", "");
                    vcProfession.Enabled = true;
                }
                else
                {
                    lblProfession.Text = labelname;
                    //ddlProfession.Attributes.Remove("required");
                    vcProfession.Enabled = false;
                }
            }*/
            #endregion

            #region set divEmail visibility is true or false if form_input_name is Email according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Email);

                lblEmail.Text = labelname.ToUpper() + "<span style=\"color:red;\">*</span>";
                gvList.Columns[4].HeaderText = labelname;
                vcEmail.Enabled = true;

                //if (showid == "WVF342")
                //{
                //    lblEmail.Text = "Email";
                //}
                //else
                //{
                //    lblEmail.Text = "อีเมล";
                //}
            }
            #endregion
            #region set divMobile visibility is true or false if form_input_name is Mobile according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameForDelegate(_Mobile);

                lblMobile.Text = labelname.ToUpper() + "<span style=\"color:red;\">*</span>";
                gvList.Columns[5].HeaderText = labelname;
                vcMobile.Enabled = true;

                //if (isrequired == 1)
                //{
                //    lblMobile.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + labelname;
                //    //txtMobile.Attributes.Add("required", "");
                //    vcMob.Enabled = true;
                //    //ftbMobile.Enabled = true;
                //}
                //else
                //{
                //    lblMobile.Text = labelname;
                //    //txtMobile.Attributes.Remove("required");
                //    vcMob.Enabled = false;
                //    //ftbMobile.Enabled = false;
                //}
            }
            #endregion
        }

        return isValidShow;
    }
    #endregion

    protected void BindDropDown()
    {
        DataSet dsSalutation = new DataSet();
        DataSet dsProfession = new DataSet();

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);

        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsSalutation = cmdObj.getSalutation(showid);
        dsProfession = cmdObj.getProfession(showid);

        if (dsSalutation.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsSalutation.Tables[0].Rows.Count; i++)
            {
                ddlSalutations.Items.Add(new ListItem(dsSalutation.Tables[0].Rows[i]["Sal_Name"].ToString(), dsSalutation.Tables[0].Rows[i]["Sal_ID"].ToString()));
            }
        }

        if (dsProfession.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsProfession.Tables[0].Rows.Count; i++)
            {
                ddlDesignation.Items.Add(new ListItem(dsProfession.Tables[0].Rows[i]["Profession"].ToString(), dsProfession.Tables[0].Rows[i]["ID"].ToString()));
            }
        }
    }

    protected void BindDropDown(ref DropDownList ddlSal, ref DropDownList ddlDesig)
    {
        DataSet dsSalutation = new DataSet();
        DataSet dsProfession = new DataSet();

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);

        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsSalutation = cmdObj.getSalutation(showid);
        dsProfession = cmdObj.getProfession(showid);

        if (dsSalutation.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsSalutation.Tables[0].Rows.Count; i++)
            {
                ddlSal.Items.Add(new ListItem(dsSalutation.Tables[0].Rows[i]["Sal_Name"].ToString(), dsSalutation.Tables[0].Rows[i]["Sal_ID"].ToString()));
            }
        }

        if (dsProfession.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsProfession.Tables[0].Rows.Count; i++)
            {
                ddlDesig.Items.Add(new ListItem(dsProfession.Tables[0].Rows[i]["Profession"].ToString(), dsProfession.Tables[0].Rows[i]["ID"].ToString()));
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region GridView1_RowDeleting and OnRowDeleting event of gvList & use table ref_salutation & delete from ref_salutation according to Sal_ID
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int ID = (int)gvList.DataKeys[e.RowIndex].Value;
            RegDelegateObj rgd = new RegDelegateObj(fn);
            DataTable dt = rgd.getRegDelegateByID(ID.ToString(), lblShowid.Text);

            if (dt.Rows.Count != 0)
            {
                string regno = dt.Rows[0]["Regno"].ToString();
                string reggroupid = dt.Rows[0]["RegGroupID"].ToString();
                string con_categoryid = dt.Rows[0]["con_CategoryId"].ToString();
                string reg_salutation = dt.Rows[0]["reg_Salutation"].ToString();
                string reg_fname = dt.Rows[0]["reg_FName"].ToString();
                string reg_lname = dt.Rows[0]["reg_LName"].ToString();
                string reg_oname = dt.Rows[0]["reg_OName"].ToString();
                string passno = dt.Rows[0]["reg_PassNo"].ToString();
                string reg_isreg = dt.Rows[0]["reg_isReg"].ToString();
                string reg_sgregistered = dt.Rows[0]["reg_sgregistered"].ToString();
                string reg_idno = dt.Rows[0]["reg_IDno"].ToString();
                string reg_staffid = dt.Rows[0]["reg_staffid"].ToString();
                string reg_designation = dt.Rows[0]["reg_Designation"].ToString();
                string reg_profession = dt.Rows[0]["reg_Profession"].ToString();
                string reg_jobtitle_alliedstu = dt.Rows[0]["reg_Jobtitle_alliedstu"].ToString();
                string reg_department = dt.Rows[0]["reg_Department"].ToString();
                string reg_organization = dt.Rows[0]["reg_Organization"].ToString();
                string reg_institution = dt.Rows[0]["reg_Institution"].ToString();
                string reg_address1 = dt.Rows[0]["reg_Address1"].ToString();
                string reg_address2 = dt.Rows[0]["reg_Address2"].ToString();
                string reg_address3 = dt.Rows[0]["reg_Address3"].ToString();
                string reg_address4 = dt.Rows[0]["reg_Address4"].ToString();
                string reg_city = dt.Rows[0]["reg_City"].ToString();
                string reg_state = dt.Rows[0]["reg_State"].ToString();
                string reg_postalcode = dt.Rows[0]["reg_PostalCode"].ToString();
                string reg_country = dt.Rows[0]["reg_Country"].ToString();
                string reg_rcountry = dt.Rows[0]["reg_RCountry"].ToString();
                string reg_telcc = dt.Rows[0]["reg_Telcc"].ToString();
                string reg_telac = dt.Rows[0]["reg_Telac"].ToString();
                string reg_tel = dt.Rows[0]["reg_Tel"].ToString();
                string reg_mobcc = dt.Rows[0]["reg_Mobcc"].ToString();
                string reg_mobac = dt.Rows[0]["reg_Mobac"].ToString();
                string reg_mobile = dt.Rows[0]["reg_Mobile"].ToString();
                string reg_faxcc = dt.Rows[0]["reg_Faxcc"].ToString();
                string reg_faxac = dt.Rows[0]["reg_Faxac"].ToString();
                string reg_fax = dt.Rows[0]["reg_Fax"].ToString();
                string reg_email = dt.Rows[0]["reg_Email"].ToString();
                string reg_affiliation = dt.Rows[0]["reg_Affiliation"].ToString();
                string reg_dietary = dt.Rows[0]["reg_Dietary"].ToString();
                string reg_nationality = dt.Rows[0]["reg_Nationality"].ToString();
                string reg_membershipno = dt.Rows[0]["reg_Membershipno"].ToString();
                string reg_vname = dt.Rows[0]["reg_vName"].ToString();
                string reg_vdob = dt.Rows[0]["reg_vDOB"].ToString();
                string reg_vpassno = dt.Rows[0]["reg_vPassno"].ToString();
                string reg_vpassexpiry = dt.Rows[0]["reg_vPassexpiry"].ToString();
                string reg_vpassissuedate = dt.Rows[0]["reg_vIssueDate"].ToString();
                string reg_vembarkation = dt.Rows[0]["reg_vEmbarkation"].ToString();
                string reg_varrivaldate = dt.Rows[0]["reg_vArrivalDate"].ToString();
                string reg_vcountry = dt.Rows[0]["reg_vCountry"].ToString();
                string udf_delegatetype = dt.Rows[0]["UDF_DelegateType"].ToString();
                string udf_profcategory = dt.Rows[0]["UDF_ProfCategory"].ToString();
                string udf_profcategoryother = dt.Rows[0]["UDF_ProfCategoryOther"].ToString();
                string udf_cname = dt.Rows[0]["UDF_CName"].ToString();
                string udf_cpcode = dt.Rows[0]["UDF_CPcode"].ToString();
                string udf_cldepartment = dt.Rows[0]["UDF_CLDepartment"].ToString();
                string udf_caddress = dt.Rows[0]["UDF_CAddress"].ToString();
                string udf_clcompany = dt.Rows[0]["UDF_CLCompany"].ToString();
                string udf_clcompanyother = dt.Rows[0]["UDF_CLCompanyOther"].ToString();
                string udf_ccountry = dt.Rows[0]["UDF_CCountry"].ToString();
                string reg_supervisorname = dt.Rows[0]["reg_SupervisorName"].ToString();
                string reg_supervisordesignation = dt.Rows[0]["reg_SupervisorDesignation"].ToString();
                string reg_supervisorcontact = dt.Rows[0]["reg_SupervisorContact"].ToString();
                string reg_supervisoremail = dt.Rows[0]["reg_SupervisorEmail"].ToString();
                string reg_salutationothers = dt.Rows[0]["reg_SalutationOthers"].ToString();
                string reg_otherprofession = dt.Rows[0]["reg_otherProfession"].ToString();
                string reg_otherdepartment = dt.Rows[0]["reg_otherDepartment"].ToString();
                string reg_otherorganization = dt.Rows[0]["reg_otherOrganization"].ToString();
                string reg_otherinstitution = dt.Rows[0]["reg_otherInstitution"].ToString();
                string reg_aemail = dt.Rows[0]["reg_aemail"].ToString();
                string reg_remark = dt.Rows[0]["reg_remark"].ToString();
                string reg_remarkgupload = dt.Rows[0]["reg_remarkGUpload"].ToString();
                string re_issms = dt.Rows[0]["reg_isSMS"].ToString();
                string reg_approvestatus = dt.Rows[0]["reg_approveStatus"].ToString();
                string reg_datecreated = dt.Rows[0]["reg_datecreated"].ToString();
                string recycle = dt.Rows[0]["recycle"].ToString();
                string reg_stage = dt.Rows[0]["reg_Stage"].ToString();

                string reg_age = dt.Rows[0]["reg_Age"].ToString();
                string reg_dob = dt.Rows[0]["reg_DOB"].ToString();
                string reg_gender = dt.Rows[0]["reg_Gender"].ToString();
                string reg_additional4 = dt.Rows[0]["reg_Additional4"].ToString();
                string reg_additional5 = dt.Rows[0]["reg_Additional5"].ToString();

                rgd.groupid = reggroupid;
                rgd.regno = regno;
                rgd.con_categoryID = (!string.IsNullOrWhiteSpace(con_categoryid)) ? int.Parse(con_categoryid) : 0;
                rgd.salutation = reg_salutation;
                rgd.fname = reg_fname;
                rgd.lname = reg_lname;
                rgd.oname = reg_oname;
                rgd.passno = passno;
                rgd.isreg = (!string.IsNullOrWhiteSpace(reg_isreg)) ? int.Parse(reg_isreg) : 0;
                rgd.regspecific = reg_sgregistered;//MCR/SNB/PRN
                rgd.idno = reg_idno;//MCR/SNB/PRN No.
                rgd.staffid = reg_staffid;//no use in design for this field
                rgd.designation = reg_designation;
                rgd.jobtitle = reg_jobtitle_alliedstu;//if Profession is Allied Health
                rgd.profession = reg_profession;
                rgd.department = reg_department;
                rgd.organization = reg_organization;
                rgd.institution = reg_institution;
                rgd.address1 = reg_address1;
                rgd.address2 = reg_address2;
                rgd.address3 = reg_address3;
                rgd.address4 = reg_address4;
                rgd.city = reg_city;
                rgd.state = reg_state;
                rgd.postalcode = reg_postalcode;
                rgd.country = reg_country;
                rgd.rcountry = reg_rcountry;
                rgd.telcc = reg_telcc;
                rgd.telac = reg_telac;
                rgd.tel = reg_tel;
                rgd.mobilecc = reg_mobcc;
                rgd.mobileac = reg_mobac;
                rgd.mobile = reg_mobile;
                rgd.faxcc = reg_faxcc;
                rgd.faxac = reg_faxac;
                rgd.fax = reg_fax;
                rgd.email = reg_email;
                rgd.affiliation = reg_affiliation;
                rgd.dietary = reg_dietary;
                rgd.nationality = reg_nationality;
                rgd.age = (!string.IsNullOrWhiteSpace(reg_age)) ? int.Parse(reg_age) : 0;
                rgd.dob = reg_dob;
                rgd.gender = reg_gender;
                rgd.additional4 = reg_additional4;
                rgd.additional5 = reg_additional5;
                rgd.memberno = reg_membershipno;

                rgd.vname = reg_vname;
                rgd.vdob = reg_vdob;
                rgd.vpassno = reg_vpassno;
                rgd.vpassexpiry = reg_vpassexpiry;
                rgd.vpassissuedate = reg_vpassissuedate;
                rgd.vembarkation = reg_vembarkation;
                rgd.varrivaldate = reg_varrivaldate;
                rgd.vcountry = reg_vcountry;

                rgd.udfcname = udf_cname;
                rgd.udfdeltype = udf_delegatetype;
                rgd.udfprofcat = udf_profcategory;
                rgd.udfprofcatother = udf_profcategoryother;
                rgd.udfcpcode = udf_cpcode;
                rgd.udfcldept = udf_cldepartment;
                rgd.udfcaddress = udf_caddress;
                rgd.udfclcompany = udf_clcompany;
                rgd.udfclcompanyother = udf_clcompanyother;
                rgd.udfccountry = udf_ccountry;

                rgd.supname = reg_supervisorname;
                rgd.supdesignation = reg_supervisordesignation;
                rgd.supcontact = reg_supervisorcontact;
                rgd.supemail = reg_supervisoremail;

                rgd.othersal = reg_salutationothers;
                rgd.otherprof = reg_otherprofession;
                rgd.otherdept = reg_otherdepartment;
                rgd.otherorg = reg_otherorganization;
                rgd.otherinstitution = reg_otherinstitution;

                rgd.aemail = reg_aemail;
                rgd.isSMS = 0;

                rgd.remark = reg_remark;
                rgd.remark_groupupload = reg_remarkgupload;
                rgd.approvestatus = (!string.IsNullOrWhiteSpace(reg_approvestatus)) ? int.Parse(reg_approvestatus) : 0;
                rgd.createdate = reg_datecreated;
                rgd.recycle = (!string.IsNullOrWhiteSpace(recycle)) ? int.Parse(recycle) : 0;
                rgd.stage = reg_stage;

                rgd.showID = lblShowid.Text;

                rgd.updateRegDelegate();
            }

        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region GridView1_RowEditing and OnRowEditing event of gvList
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        int ID = (int)gvList.DataKeys[e.NewEditIndex].Value;
        lblCurrentRegNo.Text = ID.ToString();
        // Retrieve the row being edited.
        gvList.EditIndex = -1;
        int index = e.NewEditIndex;
        GridViewRow row = gvList.Rows[index];
        Label lblmSalutation = row.FindControl("lblmSalutation") as Label;
        Label lblmDesig = row.FindControl("lblmDesignation") as Label;
        Label lblmFName = row.FindControl("lblmFName") as Label;
        Label lblmLName = row.FindControl("lblmLName") as Label;
        Label lblmEmail = row.FindControl("lblmEmail") as Label;
        Label lblmOtherSal = row.FindControl("lblmOtherSal") as Label;
        Label lblmOtherDesig = row.FindControl("lblmOtherDesig") as Label;
        Label lblmMobile = row.FindControl("lblmMobile") as Label;
        string regno = gvList.DataKeys[e.NewEditIndex].Value.ToString();

        //Salutation
        ListItem listItem = this.ddlSalutations.Items.FindByText(lblmSalutation.Text);
        if (listItem != null)
        {
            this.ddlSalutations.ClearSelection();
            listItem.Selected = true;
        }
        this.txtOtherSal.Text = lblmOtherSal.Text;
        if (!string.IsNullOrWhiteSpace(lblmOtherSal.Text))
        {
            this.divOtherSal.Visible = true;
        }
        else
        {
            this.divOtherSal.Visible = false;
        }

        //Designation
        listItem = this.ddlDesignation.Items.FindByText(lblmDesig.Text);
        if (listItem != null)
        {
            this.ddlDesignation.ClearSelection();
            listItem.Selected = true;
        }
        this.txtOtherDeg.Text = lblmOtherDesig.Text;
        if (!string.IsNullOrWhiteSpace(lblmOtherDesig.Text))
        {
            this.divOtherDeg.Visible = true;
        }
        else
        {
            this.divOtherDeg.Visible = false;
        }

        this.txtFName.Text = lblmFName.Text;
        this.txtLName.Text = lblmLName.Text;
        this.txtEmail.Text = lblmEmail.Text;
        this.txtMobile.Text = lblmMobile.Text;

        divEditBtns.Visible = true;
        btnSave.Visible = false;
        e.NewEditIndex = -1;
        BindGrid(lblShowid.Text);
    }
    #endregion

    #region GridView1_RowUpdating and OnRowUpdating event of gvList & use table ref_salutation & update ref_salutation according to Sal_ID
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    #endregion

    #region GridView1_RowCancelingEdit and OnRowCancelingEdit event of gvList
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
    }
    #endregion

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    protected void ClearForm()
    {
        ddlDesignation.SelectedValue = "0";
        ddlSalutations.SelectedValue = "0";
        txtFName.Text = string.Empty;
        txtLName.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtMobile.Text = string.Empty;
        txtOtherSal.Text = string.Empty;
        txtOtherDeg.Text = string.Empty;
        divOtherDeg.Visible = false;
        divOtherSal.Visible = false;
    }

    protected void LoadDelegateInfo(string regno)
    {
        lblCurrentRegNo.Text = regno;

        DataTable dt = new DataTable();
        RegDelegateObj rgd = new RegDelegateObj(fn);
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);

        string query = string.Format("select regNo, regGroupId, sal.Sal_Name, reg_FName, reg_LName, reg_Designation, reg_Email, reg_OtherProfession, reg_SalutationOthers, reg_Mobile from tb_RegDelegate as rdg inner join ref_Salutation as sal on sal.sal_Id=rdg.reg_Salutation Where rdg.RegNo = '{0}' and rdg.ShowId = '{1}' and rdg.reg_Additional5='1'", regno, showid);

        dt = fn.GetDatasetByCommand(query, "ds").Tables[0];

        if (dt.Rows.Count != 0)
        {
            //Salutation
            ListItem listItem = this.ddlSalutations.Items.FindByText(dt.Rows[0]["Sal_Name"].ToString());
            if (listItem != null)
            {
                this.ddlSalutations.ClearSelection();
                listItem.Selected = true;
            }
            this.txtOtherSal.Text = dt.Rows[0]["reg_SalutationOthers"].ToString();
            if (!string.IsNullOrWhiteSpace(dt.Rows[0]["reg_SalutationOthers"].ToString()))
            {
                this.divOtherSal.Visible = true;
            }
            else
            {
                this.divOtherSal.Visible = false;
            }

            //Designation
            listItem = this.ddlDesignation.Items.FindByText(dt.Rows[0]["reg_Designation"].ToString());
            if (listItem != null)
            {
                this.ddlDesignation.ClearSelection();
                listItem.Selected = true;
            }
            this.txtOtherDeg.Text = dt.Rows[0]["reg_OtherProfession"].ToString();
            if (!string.IsNullOrWhiteSpace(dt.Rows[0]["reg_OtherProfession"].ToString()))
            {
                this.divOtherDeg.Visible = true;
            }
            else
            {
                this.divOtherDeg.Visible = false;
            }

            this.txtFName.Text = dt.Rows[0]["reg_FName"].ToString();
            this.txtLName.Text = dt.Rows[0]["reg_LName"].ToString();
            this.txtEmail.Text = dt.Rows[0]["reg_Email"].ToString();
            this.txtMobile.Text = dt.Rows[0]["reg_Mobile"].ToString();

            divEditBtns.Visible = true;
            btnSave.Visible = false;
            BindGrid(lblShowid.Text);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string flow = cFun.DecryptValue(urlQuery.FlowID);
        string currentUserId = cFun.DecryptValue(urlQuery.DelegateID);
        string questionnaireNo = string.Empty;

        if (!string.IsNullOrEmpty(showid))
        {
            int isSuccess = 0;
            string groupid = string.Empty;
            string regno = Number.Zero;
            string actType = string.Empty;
            Boolean hasid = false;
            try
            {
                RegDelegateObj rgd = new RegDelegateObj(fn);
                groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                DataTable dt2 = new DataTable();

                DataTable dtReg = rgd.getRegDelegateByID(currentUserId, showid);
                string regCountry = ""; string company = "";
                if (dtReg.Rows.Count > 0)
                {
                    regCountry = dtReg.Rows[0]["reg_country"].ToString();
                    company = dtReg.Rows[0]["reg_oname"].ToString();
                }


                FlowControler flwController = new FlowControler(fn);
                FlowMaster flwConfig = flwController.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));

                #region declaration
                int con_categoryID = 0;
                string salutation = string.Empty;
                string fname = string.Empty;
                string lname = string.Empty;
                string oname = string.Empty;
                string passno = string.Empty;
                int isreg = 0;
                string regspecific = string.Empty;//MCR/SNB/PRN
                string idno = string.Empty;//MCR/SNB/PRN No.
                string staffid = string.Empty;//no use in design for this field
                string designation = string.Empty;
                string jobtitle = string.Empty;//if Profession is Allied Health
                string profession = string.Empty;
                string department = string.Empty;
                string organization = string.Empty;
                string institution = string.Empty;
                string address1 = string.Empty;
                string address2 = string.Empty;
                string address3 = string.Empty;
                string address4 = string.Empty;
                string city = string.Empty;
                string state = string.Empty;
                string postalcode = string.Empty;
                string country = string.Empty;
                string rcountry = string.Empty;
                string telcc = string.Empty;
                string telac = string.Empty;
                string tel = string.Empty;
                string mobilecc = string.Empty;
                string mobileac = string.Empty;
                string mobile = string.Empty;
                string faxcc = string.Empty;
                string faxac = string.Empty;
                string fax = string.Empty;
                string email = string.Empty;
                string affiliation = string.Empty;
                string dietary = string.Empty;
                string nationality = string.Empty;
                int age = 0;
                DateTime? dob = null;
                string dob_str = string.Empty;
                string gender = string.Empty;
                string additional4 = string.Empty;
                string additional5 = string.Empty;
                string memberno = string.Empty;

                string vname = string.Empty;
                string vdob = string.Empty;
                string vpassno = string.Empty;
                string vpassexpiry = string.Empty;
                string vpassissuedate = string.Empty;
                string vembarkation = string.Empty;
                string varrivaldate = string.Empty;
                string vcountry = string.Empty;

                string udfcname = string.Empty;
                string udfdeltype = string.Empty;
                string udfprofcat = string.Empty;
                string udfprofcatother = string.Empty;
                string udfcpcode = string.Empty;
                string udfcldept = string.Empty;
                string udfcaddress = string.Empty;
                string udfclcompany = string.Empty;
                string udfclcompanyother = string.Empty;
                string udfccountry = string.Empty;

                string supname = string.Empty;
                string supdesignation = string.Empty;
                string supcontact = string.Empty;
                string supemail = string.Empty;

                string othersal = string.Empty;
                string otherprof = string.Empty;
                string otherdept = string.Empty;
                string otherorg = string.Empty;
                string otherinstitution = string.Empty;

                string aemail = string.Empty;
                int isSMS = 0;

                string remark = string.Empty;
                string remark_groupupload = string.Empty;
                int approvestatus = 0;
                string createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                int recycle = 0;
                string stage = string.Empty;
                #endregion

                #region Info
                regno = "";
                if (String.IsNullOrEmpty(regno))
                {
                    regno = rgd.GenDelegateNumber(showid);
                }
                else
                {
                    hasid = true;
                }

                if (flwConfig.FlowLimitDelegateQty == Number.One)
                {
                }
                else
                {
                    RegGroupObj rgg = new RegGroupObj(fn);
                    DataTable dtGroup = rgg.getRegGroupByID(groupid, showid);
                    if (dtGroup.Rows.Count == 0)
                    {
                        rgg.createRegGroupIDOnly(groupid, 0, showid);
                    }
                }

                DataTable dt = rgd.getDataByGroupIDRegno(groupid, regno, showid);
                if (dt.Rows.Count > 0)
                {
                    hasid = true;
                }
                else
                {
                    hasid = false;
                }

                salutation = cFun.solveSQL(ddlSalutations.SelectedItem.Value.ToString());
                if ((ddlSalutations.SelectedItem.Text == others_value || ddlSalutations.SelectedItem.Text == other_value) && txtOtherSal.Text != "" && txtOtherSal.Text != string.Empty)
                {
                    othersal = cFun.solveSQL(txtOtherSal.Text.Trim());
                }

                fname = cFun.solveSQL(txtFName.Text.Trim());
                lname = cFun.solveSQL(txtLName.Text.Trim());
                oname = company;
                otherprof = "";

                if (_OtherSalutationValues.Any(ddlDesignation.SelectedItem.Text.Contains))
                {
                    otherprof = cFun.solveSQL(txtOtherDeg.Text.Trim()); //Other Job Title
                }

                designation = cFun.solveSQL(ddlDesignation.SelectedItem.Text.Trim()); //Job Title
                email = cFun.solveSQL(txtEmail.Text.Trim());

                isreg = 0;
                if (isreg == 1)
                {
                    regspecific = "";
                }

                idno = "";

                passno = "";
                jobtitle = "";

                profession = "";

                department = "";
                otherdept = "";

                organization = "";
                otherorg = "";

                institution = "";
                otherinstitution = "";

                address1 = "";
                address2 = "";
                address3 = "";
                address4 = "";
                city = "";
                postalcode = "";
                state = "";
                country = regCountry;
                rcountry = "";
                telcc = "";
                telac = ""; ;
                tel = "";
                mobilecc = "";
                mobileac = "";
                mobile = txtMobile.Text;
                faxcc = "";
                faxac = "";
                fax = "";
                affiliation = "";
                dietary = "";
                nationality = "";
                memberno = "";

                vname = "";
                vdob = "";
                vpassno = "";
                vpassexpiry = "";
                vpassissuedate = "";
                vembarkation = "";
                varrivaldate = "";
                vcountry = "";

                udfcname = "";
                udfdeltype = "";
                udfprofcat = "";
                udfprofcatother = "";
                udfcpcode = "";
                udfcldept = "";
                udfcaddress = "";
                udfclcompany = "";
                udfclcompanyother = "";
                udfccountry = "0";

                supname = "";
                supdesignation = "";
                supcontact = "";
                supemail = "";

                age = 0;
                dob_str = "";
                gender = "";
                additional4 = "";
                additional5 = "1";

                stage = cFun.DecryptValue(urlQuery.CurrIndex);

                rgd.groupid = groupid;
                rgd.regno = regno;
                rgd.con_categoryID = con_categoryID;
                rgd.salutation = salutation;
                rgd.fname = fname;
                rgd.lname = lname;
                rgd.oname = oname;
                rgd.passno = passno;
                rgd.isreg = isreg;
                rgd.regspecific = regspecific;//MCR/SNB/PRN
                rgd.idno = idno;//MCR/SNB/PRN No.
                rgd.staffid = staffid;//no use in design for this field
                rgd.designation = designation;
                rgd.jobtitle = jobtitle;//if Profession is Allied Health
                rgd.profession = profession;
                rgd.department = department;
                rgd.organization = organization;
                rgd.institution = institution;
                rgd.address1 = address1;
                rgd.address2 = address2;
                rgd.address3 = address3;
                rgd.address4 = address4;
                rgd.city = city;
                rgd.state = state;
                rgd.postalcode = postalcode;
                rgd.country = country;
                rgd.rcountry = rcountry;
                rgd.telcc = telcc;
                rgd.telac = telac;
                rgd.tel = tel;
                rgd.mobilecc = mobilecc;
                rgd.mobileac = mobileac;
                rgd.mobile = mobile;
                rgd.faxcc = faxcc;
                rgd.faxac = faxac;
                rgd.fax = fax;
                rgd.email = email;
                rgd.affiliation = affiliation;
                rgd.dietary = dietary;
                rgd.nationality = nationality;
                rgd.age = age;
                rgd.dob = dob_str;
                rgd.gender = gender;
                rgd.additional4 = additional4;
                rgd.additional5 = additional5;
                rgd.memberno = memberno;

                rgd.vname = vname;
                rgd.vdob = vdob;
                rgd.vpassno = vpassno;
                rgd.vpassexpiry = vpassexpiry;
                rgd.vpassissuedate = vpassissuedate;
                rgd.vembarkation = vembarkation;
                rgd.varrivaldate = varrivaldate;
                rgd.vcountry = vcountry;

                rgd.udfcname = udfcname;
                rgd.udfdeltype = udfdeltype;
                rgd.udfprofcat = udfprofcat;
                rgd.udfprofcatother = udfprofcatother;
                rgd.udfcpcode = udfcpcode;
                rgd.udfcldept = udfcldept;
                rgd.udfcaddress = udfcaddress;
                rgd.udfclcompany = udfclcompany;
                rgd.udfclcompanyother = udfclcompanyother;
                rgd.udfccountry = udfccountry;

                rgd.supname = supname;
                rgd.supdesignation = supdesignation;
                rgd.supcontact = supcontact;
                rgd.supemail = supemail;

                rgd.othersal = othersal;
                rgd.otherprof = otherprof;
                rgd.otherdept = otherdept;
                rgd.otherorg = otherorg;
                rgd.otherinstitution = otherinstitution;

                rgd.aemail = aemail;
                rgd.isSMS = isSMS;

                rgd.remark = remark;
                rgd.remark_groupupload = remark_groupupload;
                rgd.approvestatus = approvestatus;
                rgd.createdate = createdate;
                rgd.recycle = recycle;
                rgd.stage = stage;

                rgd.showID = showid;
                #endregion

                bool isAlreadyExist = false;

                System.Diagnostics.Debug.WriteLine("[{0}] Currently adding with regno {1}, hasid = {2}", DateTime.Now, regno, hasid);

                if (hasid)
                {
                    ////*Update
                    //isAlreadyExist = rgd.checkUpdateExist();
                    //if (isAlreadyExist == true)
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This user already exists!');", true);
                    //    return;
                    //}
                    //else
                    {
                        isSuccess = rgd.updateRegDelegate();
                        actType = rlgobj.actupdate;
                    }
                }
                else
                {
                    ////*Save
                    //isAlreadyExist = rgd.checkInsertExist();
                    //if (isAlreadyExist == true)
                    //{
                    //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This user already exists!');", true);
                    //    return;
                    //}
                    //else
                    {
                        isSuccess = rgd.saveRegDelegate();
                        actType = rlgobj.actsave;

                        //FlowControler flw = new FlowControler(fn, urlQuery);
                        //flw.SendCurrentStepEmail(urlQuery);
                    }
                }

                if (isSuccess > 0)
                {
                    ClearForm();
                    BindGrid(showid);
                }
            }
            catch (Exception ex)
            {
            }
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string flow = cFun.DecryptValue(urlQuery.FlowID);
        string currentUserId = cFun.DecryptValue(urlQuery.DelegateID);

        FlowControler flwObj = new FlowControler(fn, urlQuery);
        string showID = urlQuery.CurrShowID;
        string page = flwObj.NextStepURL();
        string step = flwObj.NextStep;
        string FlowID = flwObj.FlowID;
        string grpNum = "";
        grpNum = urlQuery.GoupRegID;

        QuestionnaireControler qaController = new QuestionnaireControler(fn);
        RegDelegateObj rgd = new RegDelegateObj(fn);

        string query = string.Format("Select * from tb_QAResultLog Where Regno='{0}' and ShowID='{1}' and deleteflag=0", currentUserId, showid);
        DataTable dt2 = fn.GetDatasetByCommand(query, "ds").Tables[0];

        if (dt2.Rows.Count != 0)
        {
            DataTable dtReg = rgd.getRegDelegateByGroupID(groupid, showid);

            if (dtReg.Rows.Count > 0)
            {
                foreach (DataRow dr in dtReg.Rows)
                {
                    string qaid = dt2.Rows[0]["QA_Id"].ToString();
                    string qaLogID = string.Empty;

                    string chkres = qaController.CheckQALogExist(dt2.Rows[0]["QA_Id"].ToString(), dr["Regno"].ToString(), groupid, flow, showid);
                    if (string.IsNullOrEmpty(chkres))
                    {
                        RunNumber run = new RunNumber(fn);
                        qaLogID = RefQAID.RefQALogID + run.GetRunNUmber(RefQAID.RefQALogID);
                    }
                    else
                    {
                        qaLogID = chkres;
                    }

                    if (dr["Regno"].ToString() != currentUserId.ToString() && !string.IsNullOrEmpty(dr["reg_Additional5"].ToString()))
                    {
                        QALogObj qalogObj = new QALogObj();
                        qalogObj.qalogid = qaLogID;
                        qalogObj.regno = dr["Regno"].ToString();
                        qalogObj.reggroupid = groupid;
                        qalogObj.flowid = flow;
                        qalogObj.showid = showid;
                        qalogObj.qaid = int.Parse(dt2.Rows[0]["QA_ID"].ToString());
                        int successQALog = qaController.SaveQALog(qalogObj);//*insert qaLog if not exist already

                        if (successQALog > 0)
                        {
                            DataTable dtQA = qaController.getQAResutsByLogID(dt2.Rows[0]["QALogID"].ToString());
                            if (dtQA.Rows.Count > 0)
                            {
                                foreach (DataRow drQA in dtQA.Rows)
                                {
                                    RunNumber run = new RunNumber(fn);
                                    string qaResultID = RefQAID.RefQAResultID + run.GetRunNUmber(RefQAID.RefQAResultID);

                                    QAResultObj qaresultObj = new QAResultObj();
                                    qaresultObj.qalogid = qaLogID;
                                    qaresultObj.qaresultid = qaResultID;
                                    qaresultObj.qaid = int.Parse(qaid);
                                    qaresultObj.qaquestid = drQA["QA_QuestId"].ToString();
                                    qaresultObj.qaanswerid = drQA["QA_AnswerId"].ToString();
                                    qaresultObj.qaanswertext = drQA["QA_AnswerText"].ToString();
                                    qaController.SaveQAResult(qaresultObj);
                                }
                            }
                        }
                        else
                        {
                            DataTable dtQA = qaController.getQAResutsByLogID(dt2.Rows[0]["QALogID"].ToString());

                            int success = 0;
                            foreach (DataRow drQA in dtQA.Rows)
                            {
                                success = qaController.DeleteQALog(qaLogID, qaid, drQA["QA_QuestId"].ToString());
                            }

                            foreach (DataRow drQA in dtQA.Rows)
                            {
                                RunNumber run = new RunNumber(fn);
                                string qaResultID = RefQAID.RefQAResultID + run.GetRunNUmber(RefQAID.RefQAResultID);

                                QAResultObj qaresultObj = new QAResultObj();
                                qaresultObj.qalogid = qaLogID;
                                qaresultObj.qaresultid = qaResultID;
                                qaresultObj.qaid = int.Parse(qaid);
                                qaresultObj.qaquestid = drQA["QA_QuestId"].ToString();
                                qaresultObj.qaanswerid = drQA["QA_AnswerId"].ToString();
                                qaresultObj.qaanswertext = drQA["QA_AnswerText"].ToString();
                                qaController.SaveQAResult(qaresultObj);
                            }
                        }
                    }
                }
            }

        }

        FlowControler flwController = new FlowControler(fn);
        FlowMaster flwConfig = flwController.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));

        if (flwConfig.FlowLimitDelegateQty == Number.One)
        {
            string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, null, BackendRegType.backendRegType_Delegate);
            Response.Redirect(route, false);
        }
        else
        {
            string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, null, BackendRegType.backendRegType_Delegate);
            Response.Redirect(route, false);
        }
    }

    protected void ddlSalutations_SelectedIndexChanged(object sender, EventArgs e)
    {
        OthersSettings othersetting = new OthersSettings(fn);
        List<string> lstOthersValue = othersetting.lstOthersValue;

        if (lstOthersValue.Contains(ddlSalutations.SelectedItem.Text))
        {
            divOtherSal.Visible = true;
            vcOtherSal.Enabled = true;
        }
        else
        {
            divOtherSal.Visible = false;
            vcOtherSal.Enabled = false;
        }
    }

    protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
    {
        OthersSettings othersetting = new OthersSettings(fn);
        List<string> lstOthersValue = othersetting.lstOthersValue;

        if (lstOthersValue.Any(ddlDesignation.SelectedItem.Text.Contains))
        {
            divOtherDeg.Visible = true;
            vcOtherDesig.Enabled = true;
        }
        else
        {
            divOtherDeg.Visible = false;
            vcOtherDesig.Enabled = false;
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        Update();
    }


    protected void Update()
    {
        //    try
        //    {
        RegDelegateObj rgd = new RegDelegateObj(fn);
        DataTable dt = rgd.getRegDelegateByID(lblCurrentRegNo.Text, lblShowid.Text);

        if (dt.Rows.Count != 0)
        {
            string regno = dt.Rows[0]["Regno"].ToString();
            string reggroupid = dt.Rows[0]["RegGroupID"].ToString();
            string con_categoryid = dt.Rows[0]["con_CategoryId"].ToString();
            string reg_salutation = dt.Rows[0]["reg_Salutation"].ToString();
            string reg_fname = dt.Rows[0]["reg_FName"].ToString();
            string reg_lname = dt.Rows[0]["reg_LName"].ToString();
            string reg_oname = dt.Rows[0]["reg_OName"].ToString();
            string passno = dt.Rows[0]["reg_PassNo"].ToString();
            string reg_isreg = dt.Rows[0]["reg_isReg"].ToString();
            string reg_sgregistered = dt.Rows[0]["reg_sgregistered"].ToString();
            string reg_idno = dt.Rows[0]["reg_IDno"].ToString();
            string reg_staffid = dt.Rows[0]["reg_staffid"].ToString();
            string reg_designation = dt.Rows[0]["reg_Designation"].ToString();
            string reg_profession = dt.Rows[0]["reg_Profession"].ToString();
            string reg_jobtitle_alliedstu = dt.Rows[0]["reg_Jobtitle_alliedstu"].ToString();
            string reg_department = dt.Rows[0]["reg_Department"].ToString();
            string reg_organization = dt.Rows[0]["reg_Organization"].ToString();
            string reg_institution = dt.Rows[0]["reg_Institution"].ToString();
            string reg_address1 = dt.Rows[0]["reg_Address1"].ToString();
            string reg_address2 = dt.Rows[0]["reg_Address2"].ToString();
            string reg_address3 = dt.Rows[0]["reg_Address3"].ToString();
            string reg_address4 = dt.Rows[0]["reg_Address4"].ToString();
            string reg_city = dt.Rows[0]["reg_City"].ToString();
            string reg_state = dt.Rows[0]["reg_State"].ToString();
            string reg_postalcode = dt.Rows[0]["reg_PostalCode"].ToString();
            string reg_country = dt.Rows[0]["reg_Country"].ToString();
            string reg_rcountry = dt.Rows[0]["reg_RCountry"].ToString();
            string reg_telcc = dt.Rows[0]["reg_Telcc"].ToString();
            string reg_telac = dt.Rows[0]["reg_Telac"].ToString();
            string reg_tel = dt.Rows[0]["reg_Tel"].ToString();
            string reg_mobcc = dt.Rows[0]["reg_Mobcc"].ToString();
            string reg_mobac = dt.Rows[0]["reg_Mobac"].ToString();
            string reg_mobile = dt.Rows[0]["reg_Mobile"].ToString();
            string reg_faxcc = dt.Rows[0]["reg_Faxcc"].ToString();
            string reg_faxac = dt.Rows[0]["reg_Faxac"].ToString();
            string reg_fax = dt.Rows[0]["reg_Fax"].ToString();
            string reg_email = dt.Rows[0]["reg_Email"].ToString();
            string reg_affiliation = dt.Rows[0]["reg_Affiliation"].ToString();
            string reg_dietary = dt.Rows[0]["reg_Dietary"].ToString();
            string reg_nationality = dt.Rows[0]["reg_Nationality"].ToString();
            string reg_membershipno = dt.Rows[0]["reg_Membershipno"].ToString();
            string reg_vname = dt.Rows[0]["reg_vName"].ToString();
            string reg_vdob = dt.Rows[0]["reg_vDOB"].ToString();
            string reg_vpassno = dt.Rows[0]["reg_vPassno"].ToString();
            string reg_vpassexpiry = dt.Rows[0]["reg_vPassexpiry"].ToString();
            string reg_vpassissuedate = dt.Rows[0]["reg_vIssueDate"].ToString();
            string reg_vembarkation = dt.Rows[0]["reg_vEmbarkation"].ToString();
            string reg_varrivaldate = dt.Rows[0]["reg_vArrivalDate"].ToString();
            string reg_vcountry = dt.Rows[0]["reg_vCountry"].ToString();
            string udf_delegatetype = dt.Rows[0]["UDF_DelegateType"].ToString();
            string udf_profcategory = dt.Rows[0]["UDF_ProfCategory"].ToString();
            string udf_profcategoryother = dt.Rows[0]["UDF_ProfCategoryOther"].ToString();
            string udf_cname = dt.Rows[0]["UDF_CName"].ToString();
            string udf_cpcode = dt.Rows[0]["UDF_CPcode"].ToString();
            string udf_cldepartment = dt.Rows[0]["UDF_CLDepartment"].ToString();
            string udf_caddress = dt.Rows[0]["UDF_CAddress"].ToString();
            string udf_clcompany = dt.Rows[0]["UDF_CLCompany"].ToString();
            string udf_clcompanyother = dt.Rows[0]["UDF_CLCompanyOther"].ToString();
            string udf_ccountry = dt.Rows[0]["UDF_CCountry"].ToString();
            string reg_supervisorname = dt.Rows[0]["reg_SupervisorName"].ToString();
            string reg_supervisordesignation = dt.Rows[0]["reg_SupervisorDesignation"].ToString();
            string reg_supervisorcontact = dt.Rows[0]["reg_SupervisorContact"].ToString();
            string reg_supervisoremail = dt.Rows[0]["reg_SupervisorEmail"].ToString();
            string reg_salutationothers = dt.Rows[0]["reg_SalutationOthers"].ToString();
            string reg_otherprofession = dt.Rows[0]["reg_otherProfession"].ToString();
            string reg_otherdepartment = dt.Rows[0]["reg_otherDepartment"].ToString();
            string reg_otherorganization = dt.Rows[0]["reg_otherOrganization"].ToString();
            string reg_otherinstitution = dt.Rows[0]["reg_otherInstitution"].ToString();
            string reg_aemail = dt.Rows[0]["reg_aemail"].ToString();
            string reg_remark = dt.Rows[0]["reg_remark"].ToString();
            string reg_remarkgupload = dt.Rows[0]["reg_remarkGUpload"].ToString();
            string re_issms = dt.Rows[0]["reg_isSMS"].ToString();
            string reg_approvestatus = dt.Rows[0]["reg_approveStatus"].ToString();
            string reg_datecreated = dt.Rows[0]["reg_datecreated"].ToString();
            string recycle = dt.Rows[0]["recycle"].ToString();
            string reg_stage = dt.Rows[0]["reg_Stage"].ToString();

            string reg_age = dt.Rows[0]["reg_Age"].ToString();
            string reg_dob = dt.Rows[0]["reg_DOB"].ToString();
            string reg_gender = dt.Rows[0]["reg_Gender"].ToString();
            string reg_additional4 = dt.Rows[0]["reg_Additional4"].ToString();
            string reg_additional5 = dt.Rows[0]["reg_Additional5"].ToString();

            rgd.groupid = reggroupid;
            rgd.regno = regno;
            rgd.con_categoryID = (!string.IsNullOrWhiteSpace(con_categoryid)) ? int.Parse(con_categoryid) : 0;
            rgd.salutation = cFun.solveSQL(ddlSalutations.SelectedItem.Value.ToString());
            rgd.fname = txtFName.Text;
            rgd.lname = txtLName.Text;
            rgd.oname = reg_oname;
            rgd.passno = passno;
            rgd.isreg = (!string.IsNullOrWhiteSpace(reg_isreg)) ? int.Parse(reg_isreg) : 0;
            rgd.regspecific = reg_sgregistered;//MCR/SNB/PRN
            rgd.idno = reg_idno;//MCR/SNB/PRN No.
            rgd.staffid = reg_staffid;//no use in design for this field
            rgd.designation = cFun.solveSQL(ddlDesignation.SelectedItem.Text.Trim()); //Job Title
            rgd.jobtitle = reg_jobtitle_alliedstu;//if Profession is Allied Health
            rgd.profession = reg_profession;
            rgd.department = reg_department;
            rgd.organization = reg_organization;
            rgd.institution = reg_institution;
            rgd.address1 = reg_address1;
            rgd.address2 = reg_address2;
            rgd.address3 = reg_address3;
            rgd.address4 = reg_address4;
            rgd.city = reg_city;
            rgd.state = reg_state;
            rgd.postalcode = reg_postalcode;
            rgd.country = reg_country;
            rgd.rcountry = reg_rcountry;
            rgd.telcc = reg_telcc;
            rgd.telac = reg_telac;
            rgd.tel = reg_tel;
            rgd.mobilecc = reg_mobcc;
            rgd.mobileac = reg_mobac;
            rgd.mobile = txtMobile.Text;
            rgd.faxcc = reg_faxcc;
            rgd.faxac = reg_faxac;
            rgd.fax = reg_fax;
            rgd.email = txtEmail.Text;
            rgd.affiliation = reg_affiliation;
            rgd.dietary = reg_dietary;
            rgd.nationality = reg_nationality;
            rgd.age = (!string.IsNullOrWhiteSpace(reg_age)) ? int.Parse(reg_age) : 0;
            rgd.dob = reg_dob;
            rgd.gender = reg_gender;
            rgd.additional4 = reg_additional4;
            rgd.additional5 = reg_additional5;
            rgd.memberno = reg_membershipno;

            rgd.vname = reg_vname;
            rgd.vdob = reg_vdob;
            rgd.vpassno = reg_vpassno;
            rgd.vpassexpiry = reg_vpassexpiry;
            rgd.vpassissuedate = reg_vpassissuedate;
            rgd.vembarkation = reg_vembarkation;
            rgd.varrivaldate = reg_varrivaldate;
            rgd.vcountry = reg_vcountry;

            rgd.udfcname = udf_cname;
            rgd.udfdeltype = udf_delegatetype;
            rgd.udfprofcat = udf_profcategory;
            rgd.udfprofcatother = udf_profcategoryother;
            rgd.udfcpcode = udf_cpcode;
            rgd.udfcldept = udf_cldepartment;
            rgd.udfcaddress = udf_caddress;
            rgd.udfclcompany = udf_clcompany;
            rgd.udfclcompanyother = udf_clcompanyother;
            rgd.udfccountry = udf_ccountry;

            rgd.supname = reg_supervisorname;
            rgd.supdesignation = reg_supervisordesignation;
            rgd.supcontact = reg_supervisorcontact;
            rgd.supemail = reg_supervisoremail;

            if ((ddlSalutations.SelectedItem.Text == others_value || ddlSalutations.SelectedItem.Text == other_value) && txtOtherSal.Text != "" && txtOtherSal.Text != string.Empty)
            {
                rgd.othersal = reg_salutationothers;
            }
            else
            {
                rgd.othersal = reg_salutationothers;
            }

            if (_OtherSalutationValues.Any(ddlDesignation.SelectedItem.Text.Contains))
            {
                rgd.otherprof = cFun.solveSQL(txtOtherDeg.Text.Trim()); //Other Job Title
            }
            else
            {
                rgd.otherprof = reg_otherprofession;
            }

            rgd.otherdept = reg_otherdepartment;
            rgd.otherorg = reg_otherorganization;
            rgd.otherinstitution = reg_otherinstitution;

            rgd.aemail = reg_aemail;
            rgd.isSMS = 0;

            rgd.remark = reg_remark;
            rgd.remark_groupupload = reg_remarkgupload;
            rgd.approvestatus = (!string.IsNullOrWhiteSpace(reg_approvestatus)) ? int.Parse(reg_approvestatus) : 0;
            string createdate = Convert.ToDateTime(reg_datecreated).ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            rgd.createdate = createdate;
            rgd.recycle = (!string.IsNullOrWhiteSpace(recycle)) ? int.Parse(recycle) : 0;
            rgd.stage = reg_stage;

            rgd.showID = lblShowid.Text;

            //bool isAlreadyExist = false;
            //isAlreadyExist = rgd.checkUpdateExist();
            //if (isAlreadyExist == true)
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This user already exists!');", true);
            //    return;
            //}
            //else
            {
                int success = rgd.updateRegDelegate();

                if (success > 0)
                {
                    gvList.EditIndex = -1;
                    ClearForm();
                    BindGrid(lblShowid.Text);
                    divEditBtns.Visible = false;
                    btnSave.Visible = true;
                    divOtherSal.Visible = false;
                    divOtherDeg.Visible = false;
                }
            }
        }
        //}
        //catch (Exception ex)
        //{
        //}
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            gvList.EditIndex = -1;
            ClearForm();
            divEditBtns.Visible = false;
            btnSave.Visible = true;
        }
        catch (Exception ex)
        {
        }
    }

    protected void gvList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void gvList_DataBound(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);

        foreach (GridViewRow row in gvList.Rows)
        {
            LinkButton linkBtn = row.Cells[6].Controls[0] as LinkButton;
            linkBtn.Text = (showid != "WVF342") ? "แก้ไข" : "Edit";
        }
    }
}