﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Site.Email;
using Corpit.Email;
using Corpit.Utilities;
using Corpit.Registration;
using Newtonsoft.Json;
public partial class DownloadVisaLetter : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFuz = new CommonFuns();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Request.Params["SHW"] != null && Request.Params["DID"] != null)
            {

                string showID = cFuz.DecryptValue(Request.Params["SHW"].ToString());
                string regno = cFuz.DecryptValue(Request.Params["DID"].ToString());
                //string showID = "KYG355";
                //string regno = "35560008";
                HTMLTemplateControler htmlControl = new HTMLTemplateControler();
                string template = htmlControl.CreateVisaLetterHTMLTemplate(showID, regno);
                template = Server.HtmlDecode(template);

                SiteSettings sCong = new SiteSettings(fn, showID);
                sCong.LoadBaseSiteProperties(showID);
                string isVisaFull = sCong.isVisaLetterFullPDF;
                bool isFullPDF = false;
                if (!string.IsNullOrEmpty(isVisaFull))
                {
                    isFullPDF = isVisaFull == "1" ? true : false;
                }
                string rtnFile = htmlControl.CreatePDF(template, showID, regno, "VisaLetter", isFullPDF);
                if (!string.IsNullOrEmpty(rtnFile))
                    ShowPDF.Attributes["src"] = rtnFile;
            }
        }
    }  
 
}