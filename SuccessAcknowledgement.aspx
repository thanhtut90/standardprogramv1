﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="SuccessAcknowledgement.aspx.cs" Inherits="SuccessAcknowledgement" %>
<%@ MasterType VirtualPath="~/Registration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel runat="server" ID="PanelMsg">
        <p style="padding-bottom:30px;">
            <asp:Label ID="lblSuccessMessage" runat="server"></asp:Label>
        </p>
    </asp:Panel>
    <br />
    <br />
    <div class="col-md-2">&nbsp;</div>
    <div class="col-md-10">
        <asp:Label ID="lblAcknowledgementMsg" runat="server"></asp:Label>
    </div>
</asp:Content>

