﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
public partial class EventRegTube : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    public class tmpDataList
    {
        public string showID { get; set; }
        public string FlowID { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SiteBanner.ImageUrl = "https://event-reg.biz/DefaultBanner/images/MDA2019_Thai/Tube2019_WebBanner.png";
        }
    }
    protected void btnIndiv_Onclick(object sender, EventArgs e)
    {
        tmpDataList tmpList = GetShowIDByEventName("WiTu2019Reg");
        if (string.IsNullOrEmpty(tmpList.showID))
            Response.Redirect("404.aspx");
        else
        {
            string url = "Welcome.aspx?SHW={0}&FLW={1}";
            url = string.Format(url, cFun.EncryptValue(tmpList.showID), cFun.EncryptValue(tmpList.FlowID));
            if (Request.QueryString["TRK"] != null)// MDA Tracking
            {
                url = url + "&TRK=" + Request.QueryString["TRK"].ToString();
            }
            Response.Redirect(url);
        }
    }
    protected void btnGroup_Onclick(object sender, EventArgs e)
    {
        tmpDataList tmpList = GetShowIDByEventName("WiTu2019GroupReg");
        if (string.IsNullOrEmpty(tmpList.showID))
            Response.Redirect("404.aspx");
        else
        {
            string url = "Welcome.aspx?SHW={0}&FLW={1}";
            url = string.Format(url, cFun.EncryptValue(tmpList.showID), cFun.EncryptValue(tmpList.FlowID));
            if (Request.QueryString["TRK"] != null)// MDA Tracking
            {
                url = url + "&TRK=" + Request.QueryString["TRK"].ToString();
            }
            Response.Redirect(url);
        }
    }
    private tmpDataList GetShowIDByEventName(string eventName)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            cList.showID = "";
            cList.FlowID = "";
            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                cList.showID = dList.Rows[0]["showID"].ToString();
                cList.FlowID = dList.Rows[0]["FLW_ID"].ToString();
            }

        }
        catch { }
        return cList;
    }
}