﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MTLSSession.aspx.cs" Inherits="MTLSSession" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/MTLSTemplate/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/MTLSTemplate/Site.css" rel="stylesheet" />
    <style>
        .divSPMain {
            padding: 20px;
            padding-top: 20px;
            text-align: justify;
            border-radius: 20px;
        }

        .divSPInfo {
            padding-top: 1px;
        }
    </style>
</head>
<body class="SiteMasterFrame">
    <form id="form1" runat="server">
        <div class="row SiteHeader">
            <asp:Label runat="server" ID="SiteHeader" Text="Sessions"></asp:Label>
        </div>
        <asp:Panel runat="server" ID="PanelSess" Visible="false">
            <div class="container SiteContent">
                <div class="container  text-right" style="padding-left:0px;">
                    <div class="col-md-4 col-sm-3 text-left" style="padding-left:0px;">
                        <asp:ImageButton runat="server" ID="btnHome" ImageUrl="https://event-reg.biz/DefaultBanner/images/MTLS2019/MTLSLogo.png" Height="70" OnClick="btnHome_Click"/>

                    </div>
                    <div class="col-md-8 col-sm-8 ">
                        <asp:ImageButton runat="server" ID="btnBack" ImageUrl="https://event-reg.biz/DefaultBanner/images/MTLS2019/btnBack.png" OnClick="btnBack_Click" Height="70" />
   <asp:ImageButton runat="server" ID="btnRegister" ImageUrl="https://event-reg.biz/DefaultBanner/images/MTLS2019/btnRegSession.png" OnClick="btnBook_Click" Height="70" Style="padding-left: 50px;" CssClass="pull-right" />
                 
                    </div>
                    <br />

                </div>
                <div class="col-md-12" style="padding-top: 40px;">
                    <b>
                        <asp:Label runat="server" ID="lblTitle_EL" Style="font-weight: bold; font-size: 23px; letter-spacing: 2px; color: #525252"></asp:Label></b>
                </div>
                <div class="col-md-12" style="padding-top: 10px;">
                    <asp:Label runat="server" ID="lblTitle_OL" Style="font-weight: bold; font-size: 20px; letter-spacing: 1px;"></asp:Label>
                </div>
                <div class="col-md-12" style="padding-top: 10px;">
                    <asp:Label runat="server" ID="tblFormat" Style="font-weight: bold; font-size: 20px; letter-spacing: 1px;"></asp:Label>
                </div>
                <h2 style="text-decoration: underline; padding-left: 20px; padding-top: 10px;">
                    <img src="https://event-reg.biz/DefaultBanner/images/MTLS2019/HDerSpeakers.png" style="height: 60px;" />

                </h2>
                <asp:Repeater runat="server" ID="RptSpeaker">
                    <ItemTemplate>
                        <div class="row divSPMain">
                            <div class="col-md-2">
                                <asp:Image runat="server" ID="Img" Style="float: left; width: 150px;" ImageUrl='<%# GetImgUrl( Eval("ProfileImg").ToString()) %>' />
                            </div>

                            <div class="divSPInfo col-md-10">
                                <div class="col-md-12" style="font-weight: bold;">
                                    <asp:Label runat="server" ID="lblName" Text='<%# GetFullName( Eval("Name_El").ToString(),Eval("Name_Ol").ToString()) %>'></asp:Label>
                                </div>
                                <div class="col-md-12" style="padding-top: 20px; font-weight: bold;">
                                    <asp:Label runat="server" ID="lblOrg_EL" Text='<%#Eval("Org_EL")%>'></asp:Label>
                                </div>
                                <div class="col-md-12" style="font-weight: bold;">
                                    <asp:Label runat="server" ID="lblOrg_OL" Text='<%#Eval("Org_OL")%>'></asp:Label>
                                </div>
                                <div class="col-md-12" style="padding-top: 20px;">
                                    <asp:Label runat="server" ID="Label1" Text='<%#Eval("Profile_EL")%>'></asp:Label>
                                </div>
                                <div class="col-md-12" style="padding-top: 20px; font-size: 13px;">
                                    <asp:Label runat="server" ID="Label2" Text='<%#Eval("Profile_OL")%>'></asp:Label>
                                </div>

                            </div>
                        </div>

                    </ItemTemplate>

                </asp:Repeater>
                <div class="row divSPMain">
                    <h2 style="text-decoration: underline; padding-left: 20px;">
                        <img src="https://event-reg.biz/DefaultBanner/images/MTLS2019/HderSynopsis.png" style="height: 60px;" /></h2>
                    <div class="col-md-2">
                    </div>

                    <div class="divSPInfo col-md-10">

                        <div class="col-md-12">
                            <asp:Label runat="server" ID="lblSynosis_EL"> </asp:Label>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px; font-size: 13px;">
                            <asp:Label runat="server" ID="lblSynosis_OL"> </asp:Label>
                        </div>
                    </div>
                </div>



            </div>
        </asp:Panel>
    </form>
</body>
</html>
