﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;

public partial class ReLoginRegAbsMaster : System.Web.UI.MasterPage
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    private static string[] checkingERASShowName = new string[] { "ERAS 2019" };
    private static string[] checkingASCAShowName = new string[] { "ASCA 2019 Online Conference Registration" };
    private static string[] checkingAPCCMIShowName = new string[] { "APCCMI Singapore 2020" };
    private static string[] checkingSGANZICSShowName = new string[] { "SG-ANZICS 2020" };
    private static string[] checkingAPSICShowName = new string[] { "APSIC 2021" };

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            InitSiteSettings(showid);
            bindMenuLink();
            string regno = Session["Groupid"].ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);

            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
            if (fmaster != null)
            {
                txtFlowName.Text = fmaster.FlowName;
                if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null)
                {
                    LoadPageSetting();
                }
                else
                {
                    Response.Redirect("ReLogin.aspx?Event=" + txtFlowName.Text.Trim());
                }
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        SiteBanner.ImageUrl = sSetting.SiteBanner;
        Global.PageTitle = sSetting.SiteTitle;
        Global.Copyright = sSetting.SiteFooter;
        Global.PageBanner = sSetting.SiteBanner;

        if (!string.IsNullOrEmpty(sSetting.SiteCssBandlePath))
            BundleRefence.Path = "~/Content/" + sSetting.SiteCssBandlePath + "/";
    }
    private void LoadPageSetting()
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        SetPageSettting(flwObj.CurrIndexTiTle);
    }

    public void SetPageSettting(string title)
    {
        SiteHeader.Text = title;
    }
    #endregion

    #region bindMenuLink (bind ul, li in div(divMenu) dynamically according to flow)
    private void bindMenuLink()
    {
        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["RegStatus"] != null)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string groupid = Session["Groupid"].ToString();
            string flowid = Session["Flowid"].ToString();
            string regstatus = Session["RegStatus"].ToString();

            FlowControler fControl = new FlowControler(fn);
            FlowMaster flwMasterConfig = fControl.GetFlowMasterConfig(flowid);
            StatusSettings statusSet = new StatusSettings(fn);
            InvoiceControler invControler = new InvoiceControler(fn);
            StatusSettings stuSettings = new StatusSettings(fn);

            //StatusSettings statusSet = new StatusSettings(fn);
            //if (Session["RegStatus"].ToString() == statusSet.Success.ToString())
            {
                DataTable dtMenu = new DataTable();
                dtMenu = fControl.GetReloginMenu(flowid, "0");
                if (dtMenu.Rows.Count > 0)
                {
                    HtmlGenericControl ul = new HtmlGenericControl("ul");
                    ul.Attributes.Add("class", "nav navbar-nav");
                    ul.ID = "ulMenu";

                    foreach (DataRow dr in dtMenu.Rows)
                    {
                        string scriptid = dr["reloginref_scriptID"].ToString().Trim();
                        string menuname = dr["reloginmenu_name"].ToString().Trim();
                        string stepseq = dr["step_seq"].ToString().Trim();
                        string menupageHeader = dr["step_label"].ToString().Trim();
                        if (menuname == SiteDefaultValue.constantDelegateName)
                        {
                            if (flwMasterConfig.FlowType != SiteFlowType.FLOW_GROUP)
                            {
                                HtmlGenericControl li;
                                li = new HtmlGenericControl("li");
                                li.Attributes.Add("runat", "server");

                                string path = Request.Url.AbsolutePath;
                                string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                string[] strpath = path.Split('/');
                                if (strpath.Length > 1)
                                {
                                    path = strpath[1] + ".aspx";
                                }
                                else
                                {
                                    path = ".aspx";
                                }
                                if (scriptid == path)
                                {
                                    li.Attributes.Add("class", "active");
                                }
                                else
                                {
                                    li.Attributes.Remove("class");
                                }

                                li.Attributes.Add("id", "a" + stepseq);
                                HyperLink hp = new HyperLink();
                                hp.ID = "hp" + stepseq;
                                hp.NavigateUrl = getRoute(stepseq, scriptid);
                                hp.Text = menupageHeader;//menuname;
                                li.Controls.Add(hp);
                                ul.Controls.Add(li);
                            }
                        }
                        else
                        {
                            if (menuname == SiteDefaultValue.constantConferenceName && flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                            {
                                /*not showing Reg_Conference.aspx page*/
                            }
                            else
                            {
                                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL && Session["Regno"] != null)
                                {
                                    string regno = Session["Regno"].ToString();
                                    string invStatus = invControler.getInvoiceStatus(regno);
                                    if (cFun.ParseInt(regstatus) == statusSet.Success
                                        && (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                        || invStatus == RegClass.noInvoice
                                        || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString()))
                                    {
                                        HtmlGenericControl li;
                                        li = new HtmlGenericControl("li");
                                        li.Attributes.Add("runat", "server");

                                        string path = Request.Url.AbsolutePath;
                                        string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                        string[] strpath = path.Split('/');
                                        if (strpath.Length > 1)
                                        {
                                            path = strpath[1] + ".aspx";
                                        }
                                        else
                                        {
                                            path = ".aspx";
                                        }
                                        if (scriptid == path)
                                        {
                                            li.Attributes.Add("class", "active");
                                        }
                                        else
                                        {
                                            li.Attributes.Remove("class");
                                        }

                                        li.Attributes.Add("id", "a" + stepseq);
                                        HyperLink hp = new HyperLink();
                                        hp.ID = "hp" + stepseq;
                                        hp.NavigateUrl = getRoute(stepseq, scriptid);
                                        hp.Text = menupageHeader;//menuname;
                                        li.Controls.Add(hp);
                                        ul.Controls.Add(li);
                                    }
                                }
                                else
                                {
                                    HtmlGenericControl li;
                                    li = new HtmlGenericControl("li");
                                    li.Attributes.Add("runat", "server");

                                    string path = Request.Url.AbsolutePath;
                                    string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                    string[] strpath = path.Split('/');
                                    if (strpath.Length > 1)
                                    {
                                        path = strpath[1] + ".aspx";
                                    }
                                    else
                                    {
                                        path = ".aspx";
                                    }
                                    if (scriptid == path)
                                    {
                                        li.Attributes.Add("class", "active");
                                    }
                                    else
                                    {
                                        li.Attributes.Remove("class");
                                    }

                                    li.Attributes.Add("id", "a" + stepseq);
                                    HyperLink hp = new HyperLink();
                                    hp.ID = "hp" + stepseq;
                                    hp.NavigateUrl = getRoute(stepseq, scriptid);
                                    hp.Text = menupageHeader;//menuname;
                                    li.Controls.Add(hp);
                                    ul.Controls.Add(li);
                                }
                            }
                        }
                    }

                    bool isConferenceVisible = false;
                    if (Request.QueryString["Conf"] != null)
                    {
                        if (Request.QueryString["Conf"].ToString() == "True")
                        {
                            isConferenceVisible = true;
                        }
                    }

                    ShowControler shwCtr = new ShowControler(fn);
                    Show shw = shwCtr.GetShow(showid);
                    for (int i = 1; i <= 3; i++)
                    {
                        HtmlGenericControl li;
                        li = new HtmlGenericControl("li");
                        li.Attributes.Add("runat", "server");

                        bool confExist = fControl.checkPageExist(flowid, SiteDefaultValue.constantConfName);
                        if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                        {
                            DataTable dtAbsSetting = getAbstractSetting(flowid, showid);
                            if (i == 1)
                            {
                                if (dtAbsSetting.Rows.Count > 0)
                                {
                                    bool isAbsShow = false;
                                    if(Request.QueryString["ABS"] != null)
                                    {
                                        if(Request.QueryString["ABS"].ToString() == "IsShow")
                                        {
                                            isAbsShow = true;
                                        }
                                    }
                                    string absCloseDate = dtAbsSetting.Rows[0]["AbsClosingDate"] != DBNull.Value ? (!string.IsNullOrEmpty(dtAbsSetting.Rows[0]["AbsClosingDate"].ToString()) ? dtAbsSetting.Rows[0]["AbsClosingDate"].ToString() : "31/12/2025 23:59") : "31/12/2025 23:59";
                                    bool isAbsClose = checkAbstractClose(absCloseDate);
                                    if (isAbsClose == false || isAbsShow == true)
                                    {
                                        bool isPromoCodeUsed = checkPromoCodeUsed(groupid, showid);/*added on 20-6-2019 for ERAS (not show if promo code applied)*/
                                        if (!isPromoCodeUsed)
                                        {
                                            li.Attributes.Add("id", "a" + i);
                                            HyperLink hp = new HyperLink();
                                            hp.ID = "hp" + i;
                                            string scriptid = "AbstractIndex";

                                            string path = Request.Url.AbsolutePath;
                                            string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                            string[] strpath = path.Split('/');
                                            if (strpath.Length > 1)
                                            {
                                                path = strpath[1] + ".aspx";
                                            }
                                            else
                                            {
                                                path = ".aspx";
                                            }
                                            if (scriptid == path)
                                            {
                                                li.Attributes.Add("class", "active");
                                            }
                                            else
                                            {
                                                li.Attributes.Remove("class");
                                            }

                                            hp.NavigateUrl = getRoute(i.ToString(), scriptid);
                                            hp.Text = "Abstract Submission";
                                            //hp.Attributes.Add("target", "_blank");
                                            li.Controls.Add(hp);
                                            ul.Controls.Add(li);
                                        }
                                    }
                                }
                            }

                            if ((confExist) && ((!checkingAPSICShowName.Contains(shw.SHW_Name))
                                || (isConferenceVisible == true)))// && !checkingSGANZICSShowName.Contains(shw.SHW_Name))
                                //|| (confExist && checkingSGANZICSShowName.Contains(shw.SHW_Name)))// && !checkingAPCCMIShowName.Contains(shw.SHW_Name))
                            {
                                if (i == 2)
                                {
                                    string regno = Session["Regno"].ToString();
                                    string invStatus = invControler.getInvoiceStatus(regno);
                                    if (dtAbsSetting.Rows.Count > 0)
                                    {
                                        if (cFun.ParseInt(regstatus) == statusSet.Pending && (invStatus == RegClass.noInvoice))
                                        {
                                            li.Attributes.Add("id", "a" + i);
                                            HyperLink hp = new HyperLink();
                                            hp.ID = "hp" + i;
                                            string scriptid = "ConferenceIndex";

                                            string path = Request.Url.AbsolutePath;
                                            string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                            string[] strpath = path.Split('/');
                                            if (strpath.Length > 1)
                                            {
                                                path = strpath[1] + ".aspx";
                                            }
                                            else
                                            {
                                                path = ".aspx";
                                            }
                                            if (scriptid == path)
                                            {
                                                li.Attributes.Add("class", "active");
                                            }
                                            else
                                            {
                                                li.Attributes.Remove("class");
                                            }

                                            hp.NavigateUrl = getRoute(i.ToString(), scriptid);
                                            hp.Text = "Conference Registration";
                                            if (checkingERASShowName.Contains(shw.SHW_Name) || checkingAPCCMIShowName.Contains(shw.SHW_Name)
                                                || checkingAPSICShowName.Contains(shw.SHW_Name))
                                            {
                                                hp.Text = "Congress Registration";
                                            }
                                            if (checkingSGANZICSShowName.Contains(shw.SHW_Name))
                                            {
                                                hp.Text = "Forum Registration";
                                            }
                                            //hp.Attributes.Add("target", "_blank");
                                            li.Controls.Add(hp);
                                            ul.Controls.Add(li);
                                        }
                                        else if (cFun.ParseInt(regstatus) == statusSet.Success
                                               && (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                               || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString()))
                                        {
                                            li.Attributes.Add("id", "a" + i);
                                            HyperLink hp = new HyperLink();
                                            hp.ID = "hp" + i;
                                            string scriptid = ReLoginStaticValueClass.regReceiptPage;

                                            string path = Request.Url.AbsolutePath;
                                            string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                            string[] strpath = path.Split('/');
                                            if (strpath.Length > 1)
                                            {
                                                path = strpath[1] + ".aspx";
                                            }
                                            else
                                            {
                                                path = ".aspx";
                                            }
                                            if (scriptid == path)
                                            {
                                                li.Attributes.Add("class", "active");
                                            }
                                            else
                                            {
                                                li.Attributes.Remove("class");
                                            }

                                            hp.NavigateUrl = getRoute(i.ToString(), scriptid);
                                            hp.Text = ReLoginStaticValueClass.regReceiptName;
                                            //hp.Attributes.Add("target", "_blank");
                                            li.Controls.Add(hp);
                                            ul.Controls.Add(li);
                                        }
                                    }
                                    else
                                    {
                                        if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL && Session["Regno"] != null)
                                        {
                                            if (cFun.ParseInt(regstatus) == statusSet.Success
                                                && (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                                || invStatus == RegClass.noInvoice
                                                || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString()))
                                            {
                                                li.Attributes.Add("id", "a" + i);
                                                HyperLink hp = new HyperLink();
                                                hp.ID = "hp" + i;
                                                string scriptid = ReLoginStaticValueClass.regReceiptPage;

                                                string path = Request.Url.AbsolutePath;
                                                string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                                string[] strpath = path.Split('/');
                                                if (strpath.Length > 1)
                                                {
                                                    path = strpath[1] + ".aspx";
                                                }
                                                else
                                                {
                                                    path = ".aspx";
                                                }
                                                if (scriptid == path)
                                                {
                                                    li.Attributes.Add("class", "active");
                                                }
                                                else
                                                {
                                                    li.Attributes.Remove("class");
                                                }

                                                hp.NavigateUrl = getRoute(i.ToString(), scriptid);
                                                hp.Text = ReLoginStaticValueClass.regReceiptName;
                                                //hp.Attributes.Add("target", "_blank");
                                                li.Controls.Add(hp);
                                                ul.Controls.Add(li);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if ((confExist))// && !checkingSGANZICSShowName.Contains(shw.SHW_Name))
                                //|| (confExist && checkingSGANZICSShowName.Contains(shw.SHW_Name) && isSGANZICSConferenceVisible))// && !checkingAPCCMIShowName.Contains(shw.SHW_Name))
                            {
                                if (i == 2)
                                {
                                    {
                                        li.Attributes.Add("id", "a" + i);
                                        HyperLink hp = new HyperLink();
                                        hp.ID = "hp" + i;
                                        string scriptid = ReLoginStaticValueClass.regReceiptPage;

                                        string path = Request.Url.AbsolutePath;
                                        string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                        string[] strpath = path.Split('/');
                                        if (strpath.Length > 1)
                                        {
                                            path = strpath[1] + ".aspx";
                                        }
                                        else
                                        {
                                            path = ".aspx";
                                        }
                                        if (scriptid == path)
                                        {
                                            li.Attributes.Add("class", "active");
                                        }
                                        else
                                        {
                                            li.Attributes.Remove("class");
                                        }

                                        hp.NavigateUrl = getRoute(i.ToString(), scriptid);
                                        hp.Text = ReLoginStaticValueClass.regReceiptName;
                                        //hp.Attributes.Add("target", "_blank");
                                        li.Controls.Add(hp);
                                        ul.Controls.Add(li);
                                    }
                                }
                            }
                        }

                        if (i == 3)
                        {
                            li.Attributes.Add("id", "a" + i);
                            HyperLink hp = new HyperLink();
                            hp.ID = "hp" + i;
                            //LinkButton lnkLogout = new LinkButton();
                            //lnkLogout.ID = "lnkLogout";
                            //lnkLogout.Attributes.Add("runat", "server");
                            string scriptid = ReLoginStaticValueClass.regLogoutPage;

                            string path = Request.Url.AbsolutePath;
                            string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                            string[] strpath = path.Split('/');
                            if (strpath.Length > 1)
                            {
                                path = strpath[1] + ".aspx";
                            }
                            else
                            {
                                path = ".aspx";
                            }
                            if (scriptid == path)
                            {
                                li.Attributes.Add("class", "active");
                            }
                            else
                            {
                                li.Attributes.Remove("class");
                            }

                            hp.NavigateUrl = getRoute(i.ToString(), "Logout.aspx");//scriptid
                            hp.Text = ReLoginStaticValueClass.regLogoutName;
                            li.Controls.Add(hp);
                            //lnkLogout.Click += new EventHandler(lnkLogout_Click);
                            //lnkLogout.Text = ReLoginStaticValueClass.regLogoutName;
                            //li.Controls.Add(lnkLogout);
                            ul.Controls.Add(li);
                        }
                    }

                    divMenu.Controls.Add(ul);
                }
            }
        }
    }
    #endregion

    #region getRoute (get route according to login id(Session["Groupid"]) but no use)
    public string getRoute(string step, string page)
    {
        string route = string.Empty;

        if (Session["Groupid"] != null)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            FlowControler Flw = new FlowControler(fn);
            RegGroupObj rgp = new RegGroupObj(fn);
            string grpNum = Session["Groupid"].ToString();
            string FlowID = rgp.getFlowID(grpNum, showid);

            showid = cFun.EncryptValue(showid);
            FlowID = cFun.EncryptValue(FlowID);
            step = cFun.EncryptValue(step);

            if (page == "Logout.aspx")//ReLoginStaticValueClass.regLogoutPage)
            {
                route = page + "?Event=" + GetEventNameByShowIDFlowID(cFun.DecryptValue(showid), cFun.DecryptValue(FlowID));//Flw.MakeFullURL(page, FlowID, showid);
            }
            else
            {
                string regno = string.Empty;
                if (Session["Regno"] != null)
                {
                    regno = cFun.EncryptValue(Session["Regno"].ToString());

                    grpNum = cFun.EncryptValue(grpNum);
                    route = Flw.MakeFullURL(page, FlowID, showid, grpNum, step, regno);
                }
                else
                {
                    grpNum = cFun.EncryptValue(grpNum);
                    route = Flw.MakeFullURL(page, FlowID, showid, grpNum, step);
                }
            }
        }
        else
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
        }

        return route;
    }
    #endregion

    private string GetEventNameByShowIDFlowID(string showid, string flowid)
    {
        string flowName = string.Empty;
        try
        {
            string sql = "select FLW_Name from tb_site_flow_master  where ShowID=@ShowID And FLW_ID=@FLW_ID and Status=@Status";
            //'EAD351'//FLW_Name=@FName 

            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FLW_ID", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            spar3.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                flowName = dList.Rows[0]["FLW_Name"].ToString();
            }
        }
        catch { }
        return flowName;
    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        //FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        //string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        //string flowid = cFun.DecryptValue(urlQuery.CurrShowID);

        //string page = ReLoginStaticValueClass.regLogoutPage;
        //string param = page + "?Event=" + GetEventNameByShowIDFlowID(showid, flowid);
        //var url = "http://localhost:64589/" + param;

        //Response.Clear();
        //var sb = new System.Text.StringBuilder();
        //sb.Append("<html>");
        //sb.AppendFormat("<body onload='document.forms[0].submit()'>");
        //sb.AppendFormat("<form action='{0}' method='post'>", url);
        //sb.Append("</form>");
        //sb.Append("</body>");
        //sb.Append("</html>");
        //Response.Write(sb.ToString());
        //Response.End();
    }

    private DataTable getAbstractSetting(string flowid, string showid)
    {
        DataTable dtResult = new DataTable();
        try
        {
            string sql = "Select * From tb_AbstractSettings Where ShowID=@ShowID And FlowID=@FlowID";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            pList.Add(spar1);
            pList.Add(spar2);
            dtResult = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
        }
        catch(Exception ex)
        { }

        return dtResult;
    }
    protected bool checkAbstractClose(string absClosingDate)
    {
        bool isAbsClose = false;
        try
        {
            if (!string.IsNullOrEmpty(absClosingDate))
            {
                DateTime abstractClosing = DateTime.ParseExact(absClosingDate, "dd/MM/yyyy HH:mm", null);

                if (DateTime.Compare(DateTime.Today, abstractClosing) >= 0)
                {
                    isAbsClose = true;
                }
            }
        }
        catch(Exception ex)
        { }

        return isAbsClose;
    }

    private bool checkPromoCodeUsed(string groupid, string showid)
    {
        bool isPromoCodeUsed = false;
        DataTable dtResult = new DataTable();
        try
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            if (checkingERASShowName.Contains(shw.SHW_Name))
            {
                string sql = "Select p.Promo_CategoryID,* From tb_User_Promo as u"
                            + " Inner Join tb_PromoList as pl On u.Promo_Code=pl.PromoCode"
                            + " Inner Join tb_PromoCode as p On pl.PromocodeID=p.Promo_Id "
                            + " Inner Join tb_RegDelegate as d On d.RegGroupID = u.Regno And d.con_CategoryId In (897, 912)"
                            + " Where u.ShowID=@ShowID And pl.ShowID=@ShowID And p.ShowID=@ShowID And u.Promo_Code=pl.PromoCode And u.Regno=@RegGroupID And "
                            + " u.Regno In (Select tb_RegDelegate.RegGroupID From tb_RegDelegate Where ShowID=@ShowID And recycle=0 And RegGroupID=@RegGroupID)";
                            //+ " And p.Promo_CategoryID In (Select tb_RegDelegate.con_CategoryId From tb_RegDelegate Where ShowID=@ShowID And RegGroupID=@RegGroupID And recycle=0)";
                List<SqlParameter> pList = new List<SqlParameter>();
                SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
                SqlParameter spar2 = new SqlParameter("RegGroupID", SqlDbType.NVarChar);
                spar1.Value = showid;
                spar2.Value = groupid;
                pList.Add(spar1);
                pList.Add(spar2);
                dtResult = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
                if (dtResult.Rows.Count > 0)
                {
                    isPromoCodeUsed = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isPromoCodeUsed;
    }
}
