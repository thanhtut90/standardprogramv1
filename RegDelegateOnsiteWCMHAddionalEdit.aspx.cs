﻿using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Corpit.Promo;
using Corpit.Logging;
using Corpit.BackendMaster;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using Corpit.RegOnsite;

public partial class RegDelegateOnsiteWCMHAddionalEdit : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string regno = cFun.DecryptValue(urlQuery.DelegateID);
                //bindPageLoad(showid, flowid, groupid, regno, urlQuery);
                bindOnsiteAdditionalValue(regno, groupid, showid);
            }
            else
            {
                string url = "~/Onsite/OnsiteSearch.aspx?Evt=" + cFun.EncryptValue(showid);
                Response.Redirect(url);
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = "RegistrationOnsite.master";
        }
    }
    #endregion

    #region btnSave_Click (create one record (just RegGroupID and recycle=0) into tb_RegGroup table if current groupid not exist in tb_RegGroup & save/update data into tb_RegDelegate table & get next route and redirect to next page according to the site flow settings (tb_site_flow table))
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                CommonFuns cFun = new CommonFuns();
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string regno = cFun.DecryptValue(urlQuery.DelegateID);
                saveAdditional(regno, groupid, showid);
                string url = "https://www.event-reg.biz/registration/onsite/OnsiteSearch.aspx?Evt=" + cFun.EncryptValue(showid);
                Response.Redirect(url);
            }
            else
            {
                string url = "~/Onsite/OnsiteSearch.aspx?Evt=" + cFun.EncryptValue(showid);
                Response.Redirect(url);
            }
        }
    }
    #endregion

    #region Onsite Additional
    private void saveAdditional(string regno, string reggroupid, string showid)
    {
        try
        {
            #region insert additional
            string postconf = "";
            if (rbItems.SelectedIndex != -1)
            {
                postconf = rbItems.SelectedItem.Text;
            }
            string breakoutsession = "";
            if (rbBreakoutSessions.SelectedIndex != -1)
            {
                breakoutsession = rbBreakoutSessions.SelectedItem.Text;
            }
            string sqlPostConf = "Select * From tb_Onsite_Additional Where ShowID=@ShowID And Regno=@Regno And GroupId=@GroupId";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Regno", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("GroupId", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = regno;
            spar3.Value = reggroupid;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);
            DataTable dt = fn.GetDatasetByCommand(sqlPostConf, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                string sqlUpdatePostConf = "Update tb_Onsite_Additional Set AdditionalValue1=@AdditionalValue1,UpdatedDate=getdate() "
                                + " ,AdditionalValue2=@AdditionalValue2,AdditionalValue3=@AdditionalValue3"
                                + " Where ShowID=@ShowID And Regno=@Regno And GroupId=@GroupId";
                using (SqlConnection con = new SqlConnection(fn.ConnString))
                {
                    using (SqlCommand command = new SqlCommand(sqlUpdatePostConf))
                    {
                        command.Connection = con;
                        command.Parameters.AddWithValue("@ShowID", showid);
                        command.Parameters.AddWithValue("@Regno", regno);
                        command.Parameters.AddWithValue("@GroupId", reggroupid);
                        command.Parameters.AddWithValue("@AdditionalValue1", postconf);
                        command.Parameters.AddWithValue("@AdditionalValue2", breakoutsession);
                        command.Parameters.AddWithValue("@AdditionalValue3", "");
                        con.Open();
                        int isSuccess = command.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            else
            {
                string sqlInsertPostConf = "Insert Into tb_Onsite_Additional (ShowID,Regno,GroupId,AdditionalValue1,AdditionalValue2,AdditionalValue3,CreatedDate)"
                                + " Values (@ShowID,@Regno,@GroupId,@AdditionalValue1,@AdditionalValue2,@AdditionalValue3,GetDate())";
                using (SqlConnection con = new SqlConnection(fn.ConnString))
                {
                    using (SqlCommand command = new SqlCommand(sqlInsertPostConf))
                    {
                        command.Connection = con;
                        command.Parameters.AddWithValue("@ShowID", showid);
                        command.Parameters.AddWithValue("@Regno", regno);
                        command.Parameters.AddWithValue("@GroupId", reggroupid);
                        command.Parameters.AddWithValue("@AdditionalValue1", postconf);
                        command.Parameters.AddWithValue("@AdditionalValue2", breakoutsession);
                        command.Parameters.AddWithValue("@AdditionalValue3", "");
                        con.Open();
                        int isSuccess = command.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            #endregion
        }
        catch (Exception ex)
        { }
    }
    private void bindOnsiteAdditionalValue(string regno, string reggroupid, string showid)
    {
        try
        {
            string sqlPostConf = "Select * From tb_Onsite_Additional Where ShowID=@ShowID And Regno=@Regno And GroupId=@GroupId";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Regno", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("GroupId", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = regno;
            spar3.Value = reggroupid;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);
            DataTable dt = fn.GetDatasetByCommand(sqlPostConf, "ds", pList).Tables[0];
            if(dt.Rows.Count > 0)
            {
                string selectedPostConference = dt.Rows[0]["AdditionalValue1"] != DBNull.Value ? dt.Rows[0]["AdditionalValue1"].ToString() : "";
                try
                {
                    if (!String.IsNullOrEmpty(selectedPostConference))
                    {
                        ListItem listItem = rbItems.Items.FindByText(selectedPostConference);
                        if (listItem != null)
                        {
                            rbItems.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                string selectedBreakeoutSession = dt.Rows[0]["AdditionalValue2"] != DBNull.Value ? dt.Rows[0]["AdditionalValue2"].ToString() : "";
                try
                {
                    if (!String.IsNullOrEmpty(selectedBreakeoutSession))
                    {
                        ListItem listItem = rbBreakoutSessions.Items.FindByText(selectedBreakeoutSession);
                        if (listItem != null)
                        {
                            rbBreakoutSessions.ClearSelection();
                            listItem.Selected = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
}