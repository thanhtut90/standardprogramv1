﻿using Corpit.BackendMaster;
using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reg_RegDelegateIndex : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    static string others_value = "Others";
    static string other_value = "Other";
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string flwID = cFun.DecryptValue(urlQuery.FlowID);

            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flwID);
            txtFlowName.Text = fmaster.FlowName;

            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string currentstage = Session["regstage"].ToString();

                bool isValidShow = bindDelegateList(groupid, showid);
                if (isValidShow)
                {
                    string redirectroute = string.Empty;
                    bool checkInvoice = checkInvoiceComplete(groupid, flowid, showid, currentstage, ref redirectroute);
                    if (!checkInvoice && !string.IsNullOrEmpty(redirectroute))//if incomplete
                    {
                        string script = String.Format("window.open('{0}','_blank');", redirectroute);
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                    }
                    //else
                    {
                        if (setDynamicForm(flowid, showid))
                        {
                            setListHeader(flowid, showid);
                            bindDropdown();
                            //ControlsEnabled();

                            rbreg_SelectedIndexChanged(this, null);

                            insertRegLoginAction(groupid, rlgobj.actview, urlQuery);
                        }
                        else
                        {
                            Response.Redirect("404.aspx");
                        }
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
            else
            {
                Response.Redirect("ReLogin.aspx?Event=" + txtFlowName.Text.Trim());//?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
            }
        }
    }

    #region checkInvoiceComplete
    private bool checkInvoiceComplete(string groupid, string flowid, string showid, string stage, ref string redirectRoute)
    {
        bool isSuccess = true;
        try
        {
            InvoiceControler invControler = new InvoiceControler(fn);
            StatusSettings statusSet = new StatusSettings(fn);

            FlowControler Flw = new FlowControler(fn);

            FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowid);
            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
            {
                //if (stage == statusSet.Success.ToString())
                {
                    string invStatus = invControler.getInvoiceStatus(groupid);
                    if (invStatus != statusSet.Success.ToString())
                    //if (invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString() || invStatus == RegClass.noInvoice)
                    {
                        string currentstage = stage;
                        string page = ReLoginStaticValueClass.regDelegateIndexPage;
                        currentstage = Flw.GetConirmationStage(flowid, SiteDefaultValue.constantRegDelegateIndexPage);
                        redirectRoute = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage);
                        isSuccess = false;
                    }
                }
            }
        }
        catch(Exception ex)
        { }

        return isSuccess;
    }
    #endregion

    #region ControlIsEnabled (set some controls' enablility true or false)
    private void ControlsEnabled()
    {
        ddlSalutation.Enabled = false;
        ddlSalutation.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        txtSalOther.Enabled = false;
        txtFName.Enabled = false;
        txtLName.Enabled = false;
        txtOName.Enabled = false;
        txtPassNo.Enabled = false;
        txtIDNo.Enabled = false;
        txtDesignation.Enabled = false;
        ddlProfession.Enabled = false;
        ddlProfession.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        ddlDepartment.Enabled = false;
        ddlDepartment.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        ddlOrganization.Enabled = false;
        ddlOrganization.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        ddlInstitution.Enabled = false;
        ddlInstitution.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        txtAddress1.Enabled = false;
        txtAddress2.Enabled = false;
        txtAddress3.Enabled = false;
        txtAddress4.Enabled = false;
        txtCity.Enabled = false;
        txtState.Enabled = false;
        txtPostalcode.Enabled = false;
        ddlCountry.Enabled = false;
        ddlCountry.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        ddlRCountry.Enabled = false;
        ddlRCountry.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        txtTelcc.Enabled = false;
        txtTelac.Enabled = false;
        txtMobcc.Enabled = false;
        txtMobac.Enabled = false;
        txtFaxcc.Enabled = false;
        txtFaxac.Enabled = false;
        ddlAffiliation.Enabled = false;
        ddlAffiliation.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        ddlDietary.Enabled = false;
        ddlDietary.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        txtNationality.Enabled = false;
        txtMemberNo.Enabled = false;
        txtVName.Enabled = false;
        txtVDOB.Enabled = false;
        txtVPassNo.Enabled = false;
        txtVPassExpiry.Enabled = false;
        txtVPassIssueDate.Enabled = false;
        txtVEmbarkation.Enabled = false;
        txtVArrivalDate.Enabled = false;
        ddlVCountry.Enabled = false;
        ddlVCountry.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        txtUDFCName.Enabled = false;
        txtUDFDelType.Enabled = false;
        ddlUDFProCategory.Enabled = false;
        ddlUDFProCategory.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        txtUDFProCatOther.Enabled = false;
        txtUDFCpostalcode.Enabled = false;
        txtUDFCLDept.Enabled = false;
        txtUDFAddress.Enabled = false;
        ddlUDFCLCom.Enabled = false;
        ddlUDFCLCom.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        txtUDFCLComOther.Enabled = false;
        ddlUDFCCountry.Enabled = false;
        ddlUDFCCountry.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        txtSupName.Enabled = false;
        txtSupDesignation.Enabled = false;
        txtSupContact.Enabled = false;
        txtSupEmail.Enabled = false;
    }
    #endregion

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMasterReLogin;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region bindDropdown & bind respective data to Salutation, Country, Affiliation, Dietary, Profession and OrgType dropdown lists
    protected void bindDropdown()
    {
        DataSet dsSalutation = new DataSet();
        DataSet dsCountry = new DataSet();
        DataSet dsAffiliation = new DataSet();
        DataSet dsdiet = new DataSet();
        DataSet dsProfession = new DataSet();
        DataSet dsOrgType = new DataSet();

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);

        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsSalutation = cmdObj.getSalutation(showid);

        CountryObj couObj = new CountryObj(fn);
        dsCountry = couObj.getAllCountry();

        dsAffiliation = cmdObj.getAffiliation(showid);
        dsdiet = cmdObj.getDietary(showid);
        dsProfession = cmdObj.getProfession(showid);
        dsOrgType = cmdObj.getOrganization(showid);

        if (dsSalutation.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsSalutation.Tables[0].Rows.Count; i++)
            {
                ddlSalutation.Items.Add(dsSalutation.Tables[0].Rows[i]["Sal_Name"].ToString());
                ddlSalutation.Items[i + 1].Value = dsSalutation.Tables[0].Rows[i]["Sal_ID"].ToString();
            }
        }
        if (dsCountry.Tables[0].Rows.Count != 0)
        {
            for (int x = 0; x < dsCountry.Tables[0].Rows.Count; x++)
            {
                ddlCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();

                ddlRCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlRCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();

                ddlVCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlVCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();

                ddlUDFCCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlUDFCCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();
            }
        }

        if (dsAffiliation.Tables[0].Rows.Count != 0)
        {
            for (int y = 0; y < dsAffiliation.Tables[0].Rows.Count; y++)
            {
                ddlAffiliation.Items.Add(dsAffiliation.Tables[0].Rows[y]["aff_name"].ToString());
                ddlAffiliation.Items[y + 1].Value = dsAffiliation.Tables[0].Rows[y]["affid"].ToString();
            }
        }
        if (dsdiet.Tables[0].Rows.Count != 0)
        {
            for (int z = 0; z < dsdiet.Tables[0].Rows.Count; z++)
            {
                ddlDietary.Items.Add(dsdiet.Tables[0].Rows[z]["diet_name"].ToString());
                ddlDietary.Items[z + 1].Value = dsdiet.Tables[0].Rows[z]["diet_id"].ToString();
            }
        }

        if (dsProfession.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsProfession.Tables[0].Rows.Count; i++)
            {
                ddlProfession.Items.Add(dsProfession.Tables[0].Rows[i]["Profession"].ToString());
                ddlProfession.Items[i + 1].Value = dsProfession.Tables[0].Rows[i]["ID"].ToString();
            }
        }

        if (dsOrgType.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsOrgType.Tables[0].Rows.Count; i++)
            {
                ddlOrganization.Items.Add(dsOrgType.Tables[0].Rows[i]["organisation"].ToString());
                ddlOrganization.Items[i].Value = dsOrgType.Tables[0].Rows[i]["ID"].ToString();
            }

            ddlOrganization_SelectedIndexChanged(this, null);
        }
    }
    #endregion

    #region setDynamicForm (set div visibility and validator controls' enability dynamically (generate dynamic form) according to the settings of tb_Form table where form_type='D')
    protected bool setDynamicForm(string flowid, string showid)
    {
        bool isValidShow = false;

        DataSet ds = new DataSet();
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForDelegate();

        int isVisitorVisible = 0;
        int isUDFVisible = 0;
        int isSupervisorVisible = 0;

        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
        {
            isValidShow = true;

            #region set divSalutation visibility is true or false if form_input_name is Salutation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                if (isshow == 1)
                {
                    divSalutation.Visible = true;
                }
                else
                {
                    divSalutation.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblSalutation.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlSalutation.Attributes.Add("required", "");
                    vcSal.Enabled = true;
                }
                else
                {
                    lblSalutation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlSalutation.Attributes.Remove("required");
                    vcSal.Enabled = false;
                }
            }
            #endregion

            #region set divFName visibility is true or false if form_input_name is FName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFName.Visible = true;
                }
                else
                {
                    divFName.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblFName.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtFName.Attributes.Add("required", "");
                    vcFName.Enabled = true;
                }
                else
                {
                    lblFName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtFName.Attributes.Remove("required");
                    vcFName.Enabled = false;
                }
            }
            #endregion

            #region set divLName visibility is true or false if form_input_name is LName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divLName.Visible = true;
                }
                else
                {
                    divLName.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblLName.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtLName.Attributes.Add("required", "");
                    vcLName.Enabled = true;
                }
                else
                {
                    lblLName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtLName.Attributes.Remove("required");
                    vcLName.Enabled = false;
                }
            }
            #endregion

            #region set divOName visibility is true or false if form_input_name is OName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divOName.Visible = true;
                }
                else
                {
                    divOName.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblOName.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtOName.Attributes.Add("required", "");
                    vcOName.Enabled = true;
                }
                else
                {
                    lblOName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtOName.Attributes.Remove("required");
                    vcOName.Enabled = false;
                }
            }
            #endregion

            #region set divPassNo visibility is true or false if form_input_name is PassNo according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divPassNo.Visible = true;
                }
                else
                {
                    divPassNo.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblPassNo.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtPassNo.Attributes.Add("required", "");
                    vcPassNo.Enabled = true;
                }
                else
                {
                    lblPassNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtPassNo.Attributes.Remove("required");
                    vcPassNo.Enabled = false;
                }
            }
            #endregion

            #region set divIsReg visibility is true or false if form_input_name is IsReg according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _isReg)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divIsReg.Visible = true;
                }
                else
                {
                    divIsReg.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblIsReg.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //rbreg.Attributes.Add("required", "");
                }
                else
                {
                    lblIsReg.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //rbreg.Attributes.Remove("required");
                }
            }
            #endregion

            #region set divRegSpecific visibility is true or false if form_input_name is RegSpecific according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divRegSpecific.Visible = true;
                }
                else
                {
                    divRegSpecific.Visible = false;
                }
            }
            #endregion

            #region set divIDNo visibility is true or false if form_input_name is IDNo according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)//MCR/SNB/PRN No.
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divIDNo.Visible = true;
                }
                else
                {
                    divIDNo.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblIDNo.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtIDNo.Attributes.Add("required", "");
                    vcIDNo.Enabled = true;
                }
                else
                {
                    lblIDNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtIDNo.Attributes.Remove("required");
                    vcIDNo.Enabled = false;
                }
            }
            #endregion

            #region set divDesignation visibility is true or false if form_input_name is Designation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDesignation.Visible = true;
                }
                else
                {
                    divDesignation.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblDesignation.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtDesignation.Attributes.Add("required", "");
                    vcDesig.Enabled = true;
                }
                else
                {
                    lblDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtDesignation.Attributes.Remove("required");
                    vcDesig.Enabled = false;
                }
            }
            #endregion

            #region set divProfession visibility is true or false if form_input_name is Profession according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                divJobtitle.Visible = false;
                vcJobtitle.Enabled = false;
                divStudentType.Visible = false;
                vcStudentType.Enabled = false;
                divStudentOther.Visible = false;
                vcStudentOther.Enabled = false;
                divStudentUpload.Visible = false;
                divDoctor.Visible = false;

                if (isshow == 1)
                {
                    divProfession.Visible = true;
                }
                else
                {
                    divProfession.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblProfession.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlProfession.Attributes.Add("required", "");
                    vcProfession.Enabled = true;
                }
                else
                {
                    lblProfession.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlProfession.Attributes.Remove("required");
                    vcProfession.Enabled = false;
                }
            }
            #endregion

            #region set divDepartment visibility is true or false if form_input_name is Department according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDepartment.Visible = true;
                }
                else
                {
                    divDepartment.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblDepartment.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlDepartment.Attributes.Add("required", "");
                    vcDeptm.Enabled = true;
                }
                else
                {
                    lblDepartment.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlDepartment.Attributes.Remove("required");
                    vcDeptm.Enabled = false;
                }
            }
            #endregion

            #region set divOrganization visibility is true or false if form_input_name is Company according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divOrganization.Visible = true;
                }
                else
                {
                    divOrganization.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblOrganization.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlOrganization.Attributes.Add("required", "");
                    vcOrg.Enabled = true;
                }
                else
                {
                    lblOrganization.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlOrganization.Attributes.Remove("required");
                    vcOrg.Enabled = false;
                }
            }
            #endregion

            #region set divInstitution visibility is true or false if form_input_name is Institution according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divInstitution.Visible = true;
                }
                else
                {
                    divInstitution.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblInstitution.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlInstitution.Attributes.Add("required", "");
                    vcInsti.Enabled = true;
                }
                else
                {
                    lblInstitution.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlInstitution.Attributes.Remove("required");
                    vcInsti.Enabled = false;
                }
            }
            #endregion

            #region set divAddress1 visibility is true or false if form_input_name is Address1 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress1.Visible = true;
                }
                else
                {
                    divAddress1.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAddress1.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress1.Attributes.Add("required", "");
                    vcAddress1.Enabled = true;
                }
                else
                {
                    lblAddress1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress1.Attributes.Remove("required");
                    vcAddress1.Enabled = false;
                }
            }
            #endregion

            #region set divAddress2 visibility is true or false if form_input_name is Address2 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress2.Visible = true;
                }
                else
                {
                    divAddress2.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAddress2.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress2.Attributes.Add("required", "");
                    vcAddress2.Enabled = true;
                }
                else
                {
                    lblAddress2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress2.Attributes.Remove("required");
                    vcAddress2.Enabled = false;
                }
            }
            #endregion

            #region set divAddress3 visibility is true or false if form_input_name is Address3 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress3.Visible = true;
                }
                else
                {
                    divAddress3.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAddress3.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress3.Attributes.Add("required", "");
                    vcAddress3.Enabled = true;
                }
                else
                {
                    lblAddress3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress3.Attributes.Remove("required");
                    vcAddress3.Enabled = false;
                }
            }
            #endregion

            #region set divAddress4 visibility is true or false if form_input_name is Address4 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress4.Visible = true;
                }
                else
                {
                    divAddress4.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAddress4.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress4.Attributes.Add("required", "");
                    vcAddress4.Enabled = true;
                }
                else
                {
                    lblAddress4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress4.Attributes.Remove("required");
                    vcAddress4.Enabled = false;
                }
            }
            #endregion

            #region set divCity visibility is true or false if form_input_name is City according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCity.Visible = true;
                }
                else
                {
                    divCity.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblCity.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtCity.Attributes.Add("required", "");
                    vcCity.Enabled = true;
                }
                else
                {
                    lblCity.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtCity.Attributes.Remove("required");
                    vcCity.Enabled = false;
                }
            }
            #endregion

            #region set divState visibility is true or false if form_input_name is State according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divState.Visible = true;
                }
                else
                {
                    divState.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblState.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtState.Attributes.Add("required", "");
                    vcState.Enabled = true;
                }
                else
                {
                    lblState.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtState.Attributes.Remove("required");
                    vcState.Enabled = false;
                }
            }
            #endregion

            #region set divPostalcode visibility is true or false if form_input_name is Postal Code according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divPostalcode.Visible = true;
                }
                else
                {
                    divPostalcode.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblPostalcode.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtPostalcode.Attributes.Add("required", "");
                    vcPostalcode.Enabled = true;
                }
                else
                {
                    lblPostalcode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtPostalcode.Attributes.Remove("required");
                    vcPostalcode.Enabled = false;
                }
            }
            #endregion

            #region set divCountry visibility is true or false if form_input_name is Country according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCountry.Visible = true;
                }
                else
                {
                    divCountry.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblCountry.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlCountry.Attributes.Add("required", "");
                    vcCountry.Enabled = true;
                }
                else
                {
                    lblCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlCountry.Attributes.Remove("required");
                    vcCountry.Enabled = false;
                }
            }
            #endregion

            #region set divRCountry visibility is true or false if form_input_name is RCountry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divRCountry.Visible = true;
                }
                else
                {
                    divRCountry.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblRCountry.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlRCountry.Attributes.Add("required", "");
                    vcRCountry.Enabled = true;
                }
                else
                {
                    lblRCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlRCountry.Attributes.Remove("required");
                    vcRCountry.Enabled = false;
                }
            }
            #endregion

            #region set divTelcc visibility is true or false if form_input_name is Telcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelcc.Visible = true;
                }
                else
                {
                    divTelcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtTelcc.Attributes.Add("required", "");
                    vcTelcc.Enabled = true;
                    ftbTelcc.Enabled = true;
                }
                else
                {
                    //txtTelcc.Attributes.Remove("required");
                    vcTelcc.Enabled = false;
                    ftbTelcc.Enabled = false;
                }
            }
            #endregion

            #region set divTelac visibility is true or false if form_input_name is Telac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelac.Visible = true;
                }
                else
                {
                    divTelac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtTelac.Attributes.Add("required", "");
                    vcTelac.Enabled = true;
                    ftbTelac.Enabled = true;
                }
                else
                {
                    //txtTelac.Attributes.Remove("required");
                    vcTelac.Enabled = false;
                    ftbTelac.Enabled = false;
                }
            }
            #endregion

            #region set divTel visibility is true or false if form_input_name is Tel according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTel.Visible = true;
                }
                else
                {
                    divTel.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblTel.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtTel.Attributes.Add("required", "");
                    vcTel.Enabled = true;
                    ftbTel.Enabled = true;
                }
                else
                {
                    lblTel.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtTel.Attributes.Remove("required");
                    vcTel.Enabled = false;
                    ftbTel.Enabled = false;
                }
            }
            #endregion

            #region set divMobcc visibility is true or false if form_input_name is Mobilecc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobcc.Visible = true;
                }
                else
                {
                    divMobcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtMobcc.Attributes.Add("required", "");
                    vcMobcc.Enabled = true;
                    ftbMobcc.Enabled = true;
                }
                else
                {
                    //txtMobcc.Attributes.Remove("required");
                    vcMobcc.Enabled = false;
                    ftbMobcc.Enabled = false;
                }
            }
            #endregion

            #region set divMobac visibility is true or false if form_input_name is Mobileac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobac.Visible = true;
                }
                else
                {
                    divMobac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtMobac.Attributes.Add("required", "");
                    vcMobac.Enabled = true;
                    ftbMobac.Enabled = true;
                }
                else
                {
                    //txtMobac.Attributes.Remove("required");
                    vcMobac.Enabled = false;
                    ftbMobac.Enabled = false;
                }
            }
            #endregion

            #region set divMobile visibility is true or false if form_input_name is Mobile according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMobile.Visible = true;
                }
                else
                {
                    divMobile.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblMobile.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtMobile.Attributes.Add("required", "");
                    vcMob.Enabled = true;
                    ftbMobile.Enabled = true;
                }
                else
                {
                    lblMobile.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtMobile.Attributes.Remove("required");
                    vcMob.Enabled = false;
                    ftbMobile.Enabled = false;
                }
            }
            #endregion

            #region set divFaxcc visibility is true or false if form_input_name is Faxcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxcc.Visible = true;
                }
                else
                {
                    divFaxcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtFaxcc.Attributes.Add("required", "");
                    vcFaxcc.Enabled = true;
                    ftbFaxcc.Enabled = true;
                }
                else
                {
                    //txtFaxcc.Attributes.Remove("required");
                    vcFaxcc.Enabled = false;
                    ftbFaxcc.Enabled = false;
                }
            }
            #endregion

            #region set divFaxac visibility is true or false if form_input_name is Faxac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxac.Visible = true;
                }
                else
                {
                    divFaxac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtFaxac.Attributes.Add("required", "");
                    vcFaxac.Enabled = true;
                    ftbFaxac.Enabled = true;
                }
                else
                {
                    //txtFaxac.Attributes.Remove("required");
                    vcFaxac.Enabled = false;
                    ftbFaxac.Enabled = false;
                }
            }
            #endregion

            #region set divFax visibility is true or false if form_input_name is Fax according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFax.Visible = true;
                }
                else
                {
                    divFax.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblFax.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtFax.Attributes.Add("required", "");
                    vcFax.Enabled = true;
                    ftbFax.Enabled = true;
                }
                else
                {
                    lblFax.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtFax.Attributes.Remove("required");
                    vcFax.Enabled = false;
                    ftbFax.Enabled = false;
                }
            }
            #endregion

            #region set divEmail visibility is true or false if form_input_name is Email according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmail.Visible = true;
                }
                else
                {
                    divEmail.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblEmail.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtEmail.Attributes.Add("required", "");
                    vcEmail.Enabled = true;
                }
                else
                {
                    lblEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtEmail.Attributes.Remove("required");
                    vcEmail.Enabled = false;
                }
            }
            #endregion

            #region set divEmailConfirmation visibility is true or false if form_input_name is ConfirmEmail according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _EmailConfirmation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmailConfirmation.Visible = true;
                    //txtEmailConfirmation.Attributes.Add("required", "");
                }
                else
                {
                    divEmailConfirmation.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblEmailConfirmation.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtEmailConfirmation.Attributes.Add("required", "");
                    vcEConfirm.Enabled = true;
                }
                else
                {
                    lblEmailConfirmation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtEmailConfirmation.Attributes.Remove("required");
                    vcEConfirm.Enabled = false;
                }
            }
            #endregion

            #region set divAffiliation visibility is true or false if form_input_name is Affiliation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAffiliation.Visible = true;
                }
                else
                {
                    divAffiliation.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAffiliation.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlAffiliation.Attributes.Add("required", "");
                    vcAffil.Enabled = true;
                }
                else
                {
                    lblAffiliation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlAffiliation.Attributes.Remove("required");
                    vcAffil.Enabled = false;
                }
            }
            #endregion

            #region set divDietary visibility is true or false if form_input_name is Dietary according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDietary.Visible = true;
                }
                else
                {
                    divDietary.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblDietary.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlDietary.Attributes.Add("required", "");
                    vcDietary.Enabled = true;
                }
                else
                {
                    lblDietary.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlDietary.Attributes.Remove("required");
                    vcDietary.Enabled = false;
                }
            }
            #endregion

            #region set divNationality visibility is true or false if form_input_name is Nationality according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divNationality.Visible = true;
                }
                else
                {
                    divNationality.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblNationality.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtNationality.Attributes.Add("required", "");
                    vcNation.Enabled = true;
                }
                else
                {
                    lblNationality.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtNationality.Attributes.Remove("required");
                    vcNation.Enabled = false;
                }
            }
            #endregion

            #region set divAge visibility is true or false if form_input_name is Age according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAge.Visible = true;
                }
                else
                {
                    divAge.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAge.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAge.Attributes.Add("required", "");
                    vcAge.Enabled = true;
                }
                else
                {
                    lblAge.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAge.Attributes.Remove("required");
                    vcAge.Enabled = false;
                }
            }
            #endregion

            #region set divDOB visibility is true or false if form_input_name is DOB according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDOB.Visible = true;
                }
                else
                {
                    divDOB.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblDOB.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtDOB.Attributes.Add("required", "");
                    vcDOB.Enabled = true;
                }
                else
                {
                    lblDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtDOB.Attributes.Remove("required");
                    vcDOB.Enabled = false;
                }
            }
            #endregion

            #region set divGender visibility is true or false if form_input_name is Gender according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);

                if (isshow == 1)
                {
                    divGender.Visible = true;
                }
                else
                {
                    divGender.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblGender.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlGender.Attributes.Add("required", "");
                    //vcGender.Enabled = true;
                }
                else
                {
                    lblGender.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlGender.Attributes.Remove("required");
                    //vcGender.Enabled = false;
                }
            }
            #endregion

            #region set divMemberNo visibility is true or false if form_input_name is Membership No according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divMemberNo.Visible = true;
                }
                else
                {
                    divMemberNo.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblMemberNo.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtMemberNo.Attributes.Add("required", "");
                    vcMember.Enabled = true;
                }
                else
                {
                    lblMemberNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtMemberNo.Attributes.Remove("required");
                    vcMember.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional4 visibility is true or false if form_input_name is Additional4 No according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional4.Visible = true;
                }
                else
                {
                    divAdditional4.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAdditional4.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional4.Attributes.Add("required", "");
                    vcAdditional4.Enabled = true;
                }
                else
                {
                    lblAdditional4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional4.Attributes.Remove("required");
                    vcAdditional4.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional5 visibility is true or false if form_input_name is Additional5 No according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional5.Visible = true;
                }
                else
                {
                    divAdditional5.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAdditional5.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional5.Attributes.Add("required", "");
                    vcAdditional5.Enabled = true;
                }
                else
                {
                    lblAdditional5.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional5.Attributes.Remove("required");
                    vcAdditional5.Enabled = false;
                }
            }
            #endregion

            #region set divVName visibility is true or false if form_input_name is VName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVName.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVName.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblVName.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVName.Attributes.Add("required", "");
                    vcVName.Enabled = true;
                }
                else
                {
                    lblVName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVName.Attributes.Remove("required");
                    vcVName.Enabled = false;
                }
            }
            #endregion

            #region set divVDOB visibility is true or false if form_input_name is VDOB according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVDOB.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVDOB.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblVDOB.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVDOB.Attributes.Add("required", "");
                    vcVDOB.Enabled = true;
                }
                else
                {
                    lblVDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVDOB.Attributes.Remove("required");
                    vcVDOB.Enabled = false;
                }
            }
            #endregion

            #region set divVPassNo visibility is true or false if form_input_name is VPassNo according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVPassNo.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVPassNo.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblVPassNo.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVPassNo.Attributes.Add("required", "");
                    vcVPassNo.Enabled = true;
                }
                else
                {
                    lblVPassNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVPassNo.Attributes.Remove("required");
                    vcVPassNo.Enabled = false;
                }
            }
            #endregion

            #region set divVPassExpiry visibility is true or false if form_input_name is VPassExpiry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVPassExpiry.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVPassExpiry.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblVPassExpiry.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVPassExpiry.Attributes.Add("required", "");
                    vcVPExpiry.Enabled = true;
                }
                else
                {
                    lblVPassExpiry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVPassExpiry.Attributes.Remove("required");
                    vcVPExpiry.Enabled = false;
                }
            }
            #endregion

            #region set divVPassIssueDate visibility is true or false if form_input_name is VPassIssueDate according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVPassIssueDate.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVPassIssueDate.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblVPassIssueDate.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVPassIssueDate.Attributes.Add("required", "");
                    vcVPassIssueDate.Enabled = true;
                }
                else
                {
                    lblVPassIssueDate.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVPassIssueDate.Attributes.Remove("required");
                    vcVPassIssueDate.Enabled = false;
                }
            }
            #endregion

            #region set divVEmbarkation visibility is true or false if form_input_name is VEmbarkation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVEmbarkation.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVEmbarkation.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblVEmbarkation.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVEmbarkation.Attributes.Add("required", "");
                    vcVEmbarkation.Enabled = true;
                }
                else
                {
                    lblVEmbarkation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVEmbarkation.Attributes.Remove("required");
                    vcVEmbarkation.Enabled = false;
                }
            }
            #endregion

            #region set divVArrivalDate visibility is true or false if form_input_name is VArrivalDate according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVArrivalDate.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVArrivalDate.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblVArrivalDate.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVArrivalDate.Attributes.Add("required", "");
                    vcVArrivalDate.Enabled = true;
                }
                else
                {
                    lblVArrivalDate.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtVArrivalDate.Attributes.Remove("required");
                    vcVArrivalDate.Enabled = false;
                }
            }
            #endregion

            #region set divVCountry visibility is true or false if form_input_name is VCountry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divVisitor.Visible = true;
                    divVCountry.Visible = true;
                    isVisitorVisible++;
                }
                else
                {
                    if (isVisitorVisible == 0)
                    {
                        divVCountry.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblVCountry.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlVCountry.Attributes.Add("required", "");
                    vcVCountry.Enabled = true;
                }
                else
                {
                    lblVCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlVCountry.Attributes.Remove("required");
                    vcVCountry.Enabled = false;
                }
            }
            #endregion

            #region set divUDFCName visibility is true or false if form_input_name is UDF_CName according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFCName.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFCName.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblUDFCName.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtUDFCName.Attributes.Add("required", "");
                    vcUDFCName.Enabled = true;
                }
                else
                {
                    lblUDFCName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtUDFCName.Attributes.Remove("required");
                    vcUDFCName.Enabled = false;
                }
            }
            #endregion

            #region set divUDFDelType visibility is true or false if form_input_name is UDF_DelegateType according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFDelType.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFDelType.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblUDFDelType.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtUDFDelType.Attributes.Add("required", "");
                    vcUDFDelType.Enabled = true;
                }
                else
                {
                    lblUDFDelType.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtUDFDelType.Attributes.Remove("required");
                    vcUDFDelType.Enabled = false;
                }
            }
            #endregion

            #region set divUDFProCategory visibility is true or false if form_input_name is UDF_ProfCategory according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFProCategory.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFProCategory.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblUDFProCategory.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlUDFProCategory.Attributes.Add("required", "");
                    vcUDFProCat.Enabled = true;
                }
                else
                {
                    lblUDFProCategory.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlUDFProCategory.Attributes.Remove("required");
                    vcUDFProCat.Enabled = false;
                }
            }
            #endregion

            #region set divUDFCpostalcode visibility is true or false if form_input_name is UDF_CPcode according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFCpostalcode.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFCpostalcode.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblUDFCpostalcode.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtUDFCpostalcode.Attributes.Add("required", "");
                    vcUDFCpcode.Enabled = true;
                }
                else
                {
                    lblUDFCpostalcode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtUDFCpostalcode.Attributes.Remove("required");
                    vcUDFCpcode.Enabled = false;
                }
            }
            #endregion

            #region set divUDFCLDept visibility is true or false if form_input_name is UDF_CLDepartment according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFCLDept.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFCLDept.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblUDFCLDept.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtUDFCLDept.Attributes.Add("required", "");
                    vcUDFCLDept.Enabled = true;
                }
                else
                {
                    lblUDFCLDept.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtUDFCLDept.Attributes.Remove("required");
                    vcUDFCLDept.Enabled = false;
                }
            }
            #endregion

            #region set divUDFAddress visibility is true or false if form_input_name is UDF_CAddress according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFAddress.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFAddress.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblUDFAddress.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtUDFAddress.Attributes.Add("required", "");
                    vcUDFAddress.Enabled = true;
                }
                else
                {
                    lblUDFAddress.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtUDFAddress.Attributes.Remove("required");
                    vcUDFAddress.Enabled = false;
                }
            }
            #endregion

            #region set divUDFCLCom visibility is true or false if form_input_name is UDF_CLCompany according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFCLCom.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFCLCom.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblUDFCLCom.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlUDFCLCom.Attributes.Add("required", "");
                    vcUDFCLCom.Enabled = true;
                }
                else
                {
                    lblUDFCLCom.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlUDFCLCom.Attributes.Remove("required");
                    vcUDFCLCom.Enabled = false;
                }
            }
            #endregion

            #region set divUDFCCountry visibility is true or false if form_input_name is UDF_CCountry according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divUDF.Visible = true;
                    divUDFCCountry.Visible = true;
                    isUDFVisible++;
                }
                else
                {
                    if (isUDFVisible == 0)
                    {
                        divUDFCCountry.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblUDFCCountry.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlUDFCCountry.Attributes.Add("required", "");
                    vcUDFCCountry.Enabled = true;
                }
                else
                {
                    lblUDFCCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlUDFCCountry.Attributes.Remove("required");
                    vcUDFCCountry.Enabled = false;
                }
            }
            #endregion

            #region set divSupName visibility is true or false if form_input_name is Supervisor Name according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupName)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divSuperVisor.Visible = true;
                    divSupName.Visible = true;
                    isSupervisorVisible++;
                }
                else
                {
                    if (isSupervisorVisible == 0)
                    {
                        divSupName.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblSupName.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtSupName.Attributes.Add("required", "");
                    vcSupName.Enabled = true;
                }
                else
                {
                    lblSupName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtSupName.Attributes.Remove("required");
                    vcSupName.Enabled = false;
                }
            }
            #endregion

            #region set divSupDesignation visibility is true or false if form_input_name is Supervisor Designation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divSuperVisor.Visible = true;
                    divSupDesignation.Visible = true;
                    isSupervisorVisible++;
                }
                else
                {
                    if (isSupervisorVisible == 0)
                    {
                        divSupDesignation.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblSupDesignation.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtSupDesignation.Attributes.Add("required", "");
                    vcSupDes.Enabled = true;
                }
                else
                {
                    lblSupDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtSupDesignation.Attributes.Remove("required");
                    vcSupDes.Enabled = false;
                }
            }
            #endregion

            #region set divSupContact visibility is true or false if form_input_name is Supervisor Contact according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divSuperVisor.Visible = true;
                    divSupContact.Visible = true;
                    isSupervisorVisible++;
                }
                else
                {
                    if (isSupervisorVisible == 0)
                    {
                        divSupContact.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblSupContact.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtSupContact.Attributes.Add("required", "");
                    vcSupContact.Enabled = true;
                }
                else
                {
                    lblSupContact.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtSupContact.Attributes.Remove("required");
                    vcSupContact.Enabled = false;
                }
            }
            #endregion

            #region set divSupEmail visibility is true or false if form_input_name is Supervisor Email according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divSuperVisor.Visible = true;
                    divSupEmail.Visible = true;
                    isSupervisorVisible++;
                }
                else
                {
                    if (isSupervisorVisible == 0)
                    {
                        divSupEmail.Visible = false;
                    }
                }

                if (isrequired == 1)
                {
                    lblSupEmail.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtSupEmail.Attributes.Add("required", "");
                    vcSupEmail.Enabled = true;
                }
                else
                {
                    lblSupEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtSupEmail.Attributes.Remove("required");
                    vcSupEmail.Enabled = false;
                }
            }
            #endregion
        }

        return isValidShow;
    }
    #endregion

    #region setListHeader (set table header visibility and header text dynamically according to the settings of tb_Form table where form_type='D')
    protected void setListHeader(string flowid, string showid)
    {
        DataSet ds = new DataSet();

        #region Delegate
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForDelegate();

        if (ds.Tables[0].Rows.Count > 0)
        {
            divList.Visible = true;
            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
            {
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);
                    
                    if (isshow == 1)
                    {
                        trSalutation.Visible = true;
                        lbl_Salutation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSalutation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trFName.Visible = true;
                        lbl_FName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trFName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trLName.Visible = true;
                        lbl_LName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trLName.Visible = false;
                    }

                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trOName.Visible = true;
                        lbl_OName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trOName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trPassno.Visible = true;
                        lbl_PassNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trPassno.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trIsReg.Visible = true;
                        lbl_IsReg.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trIsReg.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trRegSpecific.Visible = true;
                        lbl_RegSpecific.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trRegSpecific.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trIDNo.Visible = true;
                        lbl_IDNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trIDNo.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDesignation.Visible = true;
                        lbl_Designation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDesignation.Visible = false;
                    }

                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trProfession.Visible = true;
                        lbl_Profession.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trProfession.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDept.Visible = true;
                        lbl_Department.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDept.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trOrg.Visible = true;
                        lbl_Organization.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trOrg.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trInstitution.Visible = true;
                        lbl_Institution.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trInstitution.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress1.Visible = true;
                        lbl_Address1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress1.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress2.Visible = true;
                        lbl_Address2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress2.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress3.Visible = true;
                        lbl_Address3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress3.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAddress4.Visible = true;
                        lbl_Address4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAddress4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCity.Visible = true;
                        lbl_City.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCity.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trState.Visible = true;
                        lbl_State.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trState.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trPostal.Visible = true;
                        lbl_Postalcode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trPostal.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trCountry.Visible = true;
                        lbl_Country.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trRCountry.Visible = true;
                        lbl_RCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trRCountry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trTel.Visible = true;
                        lbl_Tel.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trTel.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trMobile.Visible = true;
                        lbl_Mobile.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trMobile.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trFax.Visible = true;
                        lbl_Fax.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trFax.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trEmail.Visible = true;
                        lbl_Email.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trEmail.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAffiliation.Visible = true;
                        lbl_Affiliation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAffiliation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDietary.Visible = true;
                        lbl_Dietary.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDietary.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trNationality.Visible = true;
                        lbl_Nationality.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trNationality.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAge.Visible = true;
                        lbl_Age.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAge.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trDOB.Visible = true;
                        lbl_DOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trDOB.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trGender.Visible = true;
                        lbl_Gender.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trGender.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trMembershipNo.Visible = true;
                        lbl_MembershipNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trMembershipNo.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAdditional4.Visible = true;
                        lbl_Additional4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAdditional4.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trAdditional5.Visible = true;
                        lbl_Additional5.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trAdditional5.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVName.Visible = true;
                        lbl_VName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVName.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVDOB.Visible = true;
                        lbl_VDOB.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVDOB.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVPass.Visible = true;
                        lbl_VPassNo.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVPass.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVPassExpiry.Visible = true;
                        lbl_VPassExpiry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVPassExpiry.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVPassIssueDate.Visible = true;
                        lblVPassIssueDate.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVPassIssueDate.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVEmbarkation.Visible = true;
                        lblVEmbarkation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVEmbarkation.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVArrivalDate.Visible = true;
                        lblVArrivalDate.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVArrivalDate.Visible = false;
                    }
                }

                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trVCountry.Visible = true;
                        lbl_VCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trVCountry.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CName.Visible = true;
                        lbl_UDFCName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CName.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_DelegateType.Visible = true;
                        lbl_UDFDelType.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_DelegateType.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_ProfCategory.Visible = true;
                        lbl_UDFProCategory.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_ProfCategory.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CPcode.Visible = true;
                        lbl_UDFCpostalcode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CPcode.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CLDepartment.Visible = true;
                        lbl_UDFCLDept.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CLDepartment.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CAddress.Visible = true;
                        lbl_UDFAddress.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CAddress.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CLCompany.Visible = true;
                        lbl_UDFCLCom.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CLCompany.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trUDF_CCountry.Visible = true;
                        lbl_UDFCCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trUDF_CCountry.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupName.Visible = true;
                        lbl_SupName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupName.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupDesignation.Visible = true;
                        lbl_SupDesignation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupDesignation.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupContact.Visible = true;
                        lbl_SupContact.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupContact.Visible = false;
                    }
                }
                if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                {
                    int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                    if (isshow == 1)
                    {
                        trSupEmail.Visible = true;
                        lbl_SupEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    }
                    else
                    {
                        trSupEmail.Visible = false;
                    }
                }
            }
        }
        else
        {
            divList.Visible = false;
        }
        #endregion
    }
    #endregion

    #region bindDelegateList (bind delegate list to repeater control (rptItem) according to current RegGroupID)
    private bool bindDelegateList(string groupID, string showid)
    {
        bool isValidShow = false;

        RegDelegateObj rgd = new RegDelegateObj(fn);
        DataTable dt = new DataTable();
        dt = rgd.getRegDelegateByGroupID(groupID, showid);

        if (dt.Rows.Count > 0)
        {
            isValidShow = true;
            divList.Visible = true;
            rptItem.DataSource = dt;
            rptItem.DataBind();
        }
        else
        {
            divList.Visible = false;
        }

        return isValidShow;
    }
    #endregion

    #region populateUserDetails (get all relevant data according to RegGroupID and Regno from tb_RegDelegate and bind data to the respective controls)
    private void populateUserDetails(string id, string regno, string showid)
    {
        DataTable dt = new DataTable();
        RegDelegateObj rgd = new RegDelegateObj(fn);
        dt = rgd.getDataByGroupIDRegno(id, regno, showid);
        if (dt.Rows.Count != 0)
        {
            UpdatePanel1.Visible = true;

            //string regno = dt.Rows[0]["Regno"].ToString();
            string reggroupid = dt.Rows[0]["RegGroupID"].ToString();
            string con_categoryid = dt.Rows[0]["con_CategoryId"].ToString();
            string reg_salutation = dt.Rows[0]["reg_Salutation"].ToString();
            string reg_fname = dt.Rows[0]["reg_FName"].ToString();
            string reg_lname = dt.Rows[0]["reg_LName"].ToString();
            string reg_oname = dt.Rows[0]["reg_OName"].ToString();
            string passno = dt.Rows[0]["reg_PassNo"].ToString();
            string reg_isreg = dt.Rows[0]["reg_isReg"].ToString();
            string reg_sgregistered = dt.Rows[0]["reg_sgregistered"].ToString();
            string reg_idno = dt.Rows[0]["reg_IDno"].ToString();
            string reg_staffid = dt.Rows[0]["reg_staffid"].ToString();
            string reg_designation = dt.Rows[0]["reg_Designation"].ToString();
            string reg_profession = dt.Rows[0]["reg_Profession"].ToString();
            string reg_jobtitle_alliedstu = dt.Rows[0]["reg_Jobtitle_alliedstu"].ToString();
            string reg_department = dt.Rows[0]["reg_Department"].ToString();
            string reg_organization = dt.Rows[0]["reg_Organization"].ToString();
            string reg_institution = dt.Rows[0]["reg_Institution"].ToString();
            string reg_address1 = dt.Rows[0]["reg_Address1"].ToString();
            string reg_address2 = dt.Rows[0]["reg_Address2"].ToString();
            string reg_address3 = dt.Rows[0]["reg_Address3"].ToString();
            string reg_address4 = dt.Rows[0]["reg_Address4"].ToString();
            string reg_city = dt.Rows[0]["reg_City"].ToString();
            string reg_state = dt.Rows[0]["reg_State"].ToString();
            string reg_postalcode = dt.Rows[0]["reg_PostalCode"].ToString();
            string reg_country = dt.Rows[0]["reg_Country"].ToString();
            string reg_rcountry = dt.Rows[0]["reg_RCountry"].ToString();
            string reg_telcc = dt.Rows[0]["reg_Telcc"].ToString();
            string reg_telac = dt.Rows[0]["reg_Telac"].ToString();
            string reg_tel = dt.Rows[0]["reg_Tel"].ToString();
            string reg_mobcc = dt.Rows[0]["reg_Mobcc"].ToString();
            string reg_mobac = dt.Rows[0]["reg_Mobac"].ToString();
            string reg_mobile = dt.Rows[0]["reg_Mobile"].ToString();
            string reg_faxcc = dt.Rows[0]["reg_Faxcc"].ToString();
            string reg_faxac = dt.Rows[0]["reg_Faxac"].ToString();
            string reg_fax = dt.Rows[0]["reg_Fax"].ToString();
            string reg_email = dt.Rows[0]["reg_Email"].ToString();
            string reg_affiliation = dt.Rows[0]["reg_Affiliation"].ToString();
            string reg_dietary = dt.Rows[0]["reg_Dietary"].ToString();
            string reg_nationality = dt.Rows[0]["reg_Nationality"].ToString();
            string reg_membershipno = dt.Rows[0]["reg_Membershipno"].ToString();
            string reg_vname = dt.Rows[0]["reg_vName"].ToString();
            string reg_vdob = dt.Rows[0]["reg_vDOB"].ToString();
            string reg_vpassno = dt.Rows[0]["reg_vPassno"].ToString();
            string reg_vpassexpiry = dt.Rows[0]["reg_vPassexpiry"].ToString();
            string reg_vpassissuedate = dt.Rows[0]["reg_vIssueDate"].ToString();
            string reg_vembarkation = dt.Rows[0]["reg_vEmbarkation"].ToString();
            string reg_varrivaldate = dt.Rows[0]["reg_vArrivalDate"].ToString();
            string reg_vcountry = dt.Rows[0]["reg_vCountry"].ToString();
            string udf_delegatetype = dt.Rows[0]["UDF_DelegateType"].ToString();
            string udf_profcategory = dt.Rows[0]["UDF_ProfCategory"].ToString();
            string udf_profcategoryother = dt.Rows[0]["UDF_ProfCategoryOther"].ToString();
            string udf_cname = dt.Rows[0]["UDF_CName"].ToString();
            string udf_cpcode = dt.Rows[0]["UDF_CPcode"].ToString();
            string udf_cldepartment = dt.Rows[0]["UDF_CLDepartment"].ToString();
            string udf_caddress = dt.Rows[0]["UDF_CAddress"].ToString();
            string udf_clcompany = dt.Rows[0]["UDF_CLCompany"].ToString();
            string udf_clcompanyother = dt.Rows[0]["UDF_CLCompanyOther"].ToString();
            string udf_ccountry = dt.Rows[0]["UDF_CCountry"].ToString();
            string reg_supervisorname = dt.Rows[0]["reg_SupervisorName"].ToString();
            string reg_supervisordesignation = dt.Rows[0]["reg_SupervisorDesignation"].ToString();
            string reg_supervisorcontact = dt.Rows[0]["reg_SupervisorContact"].ToString();
            string reg_supervisoremail = dt.Rows[0]["reg_SupervisorEmail"].ToString();
            string reg_salutationothers = dt.Rows[0]["reg_SalutationOthers"].ToString();
            string reg_otherprofession = dt.Rows[0]["reg_otherProfession"].ToString();
            string reg_otherdepartment = dt.Rows[0]["reg_otherDepartment"].ToString();
            string reg_otherorganization = dt.Rows[0]["reg_otherOrganization"].ToString();
            string reg_otherinstitution = dt.Rows[0]["reg_otherInstitution"].ToString();
            string reg_aemail = dt.Rows[0]["reg_aemail"].ToString();
            string reg_remark = dt.Rows[0]["reg_remark"].ToString();
            string reg_remarkgupload = dt.Rows[0]["reg_remarkGUpload"].ToString();
            string re_issms = dt.Rows[0]["reg_isSMS"].ToString();
            string reg_approvestatus = dt.Rows[0]["reg_approveStatus"].ToString();
            string reg_datecreated = dt.Rows[0]["reg_datecreated"].ToString();
            string recycle = dt.Rows[0]["recycle"].ToString();
            string reg_stage = dt.Rows[0]["reg_Stage"].ToString();

            string reg_age = dt.Rows[0]["reg_Age"].ToString();
            string reg_dob = dt.Rows[0]["reg_DOB"].ToString();
            string reg_gender = dt.Rows[0]["reg_Gender"].ToString();
            string reg_additional4 = dt.Rows[0]["reg_Additional4"].ToString();
            string reg_additional5 = dt.Rows[0]["reg_Additional5"].ToString();

            txtCategoryID.Text = con_categoryid;

            try
            {
                if (!String.IsNullOrEmpty(reg_salutation))
                {
                    ListItem listItem = ddlSalutation.Items.FindByValue(reg_salutation);
                    if (listItem != null)
                    {
                        ddlSalutation.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtSalOther.Text = reg_salutationothers;

            txtFName.Text = reg_fname;
            txtLName.Text = reg_lname;
            txtOName.Text = reg_oname;
            txtPassNo.Text = passno;

            if (reg_isreg == Number.One)
            {
                rbreg.Items[0].Selected = true;
                rbreg.Items[1].Selected = false;
            }
            else
            {
                rbreg.Items[0].Selected = false;
                rbreg.Items[1].Selected = true;
            }

            try
            {
                if (!String.IsNullOrEmpty(reg_sgregistered))
                {
                    ListItem listItem = rbregspecific.Items.FindByValue(reg_sgregistered);
                    if (listItem != null)
                    {
                        rbregspecific.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtIDNo.Text = reg_idno;

            if (reg_profession != "")
            {
                if (!String.IsNullOrEmpty(reg_profession))
                {
                    ListItem listItem = ddlProfession.Items.FindByValue(reg_profession);
                    if (listItem != null)
                    {
                        ddlProfession.ClearSelection();
                        listItem.Selected = true;
                    }
                    ddlProfession_SelectedIndexChanged(this, null);
                }
            }
            txtProOther.Text = reg_otherprofession;
            txtDesignation.Text = reg_designation;
            txtJobtitle.Text = reg_jobtitle_alliedstu;

            try
            {
                if (!String.IsNullOrEmpty(reg_organization))
                {
                    ListItem listItem = ddlOrganization.Items.FindByValue(reg_organization);
                    if (listItem != null)
                    {
                        ddlOrganization.ClearSelection();
                        listItem.Selected = true;
                    }
                    ddlOrganization_SelectedIndexChanged(this, null);
                }
            }
            catch (Exception ex)
            {
            }
            txtOrgOther.Text = reg_otherorganization;

            try
            {
                if (!String.IsNullOrEmpty(reg_institution))
                {
                    ListItem listItem = ddlInstitution.Items.FindByValue(reg_institution);
                    if (listItem != null)
                    {
                        ddlInstitution.ClearSelection();
                        listItem.Selected = true;
                    }
                    ddlInstitution_SelectedIndexChanged(this, null);
                }
            }
            catch (Exception ex)
            {
            }
            txtInstiOther.Text = reg_otherinstitution;

            try
            {
                if (!String.IsNullOrEmpty(reg_department))
                {
                    ListItem listItem = ddlDepartment.Items.FindByValue(reg_department);
                    if (listItem != null)
                    {
                        ddlDepartment.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtDepartmentOther.Text = reg_otherdepartment;

            txtAddress1.Text = reg_address1;
            txtAddress2.Text = reg_address2;
            txtAddress3.Text = reg_address3;
            txtAddress4.Text = reg_address4;
            txtCity.Text = reg_city;
            txtState.Text = reg_state;
            txtPostalcode.Text = reg_postalcode;
            try
            {
                if (!String.IsNullOrEmpty(reg_country))
                {
                    ListItem listItem = ddlCountry.Items.FindByValue(reg_country);
                    if (listItem != null)
                    {
                        ddlCountry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            try
            {
                if (!String.IsNullOrEmpty(reg_rcountry))
                {
                    ListItem listItem = ddlRCountry.Items.FindByValue(reg_rcountry);
                    if (listItem != null)
                    {
                        ddlRCountry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            txtTelcc.Text = reg_telcc;
            txtTelac.Text = reg_telac;
            txtTel.Text = reg_tel;
            txtMobcc.Text = reg_mobcc;
            txtMobac.Text = reg_mobac;
            txtMobile.Text = reg_mobile;
            txtFaxcc.Text = reg_faxcc;
            txtFaxac.Text = reg_faxac;
            txtFax.Text = reg_fax;
            txtEmail.Text = reg_email;

            try
            {
                if (!String.IsNullOrEmpty(reg_affiliation))
                {
                    ListItem listItem = ddlAffiliation.Items.FindByValue(reg_affiliation);
                    if (listItem != null)
                    {
                        ddlAffiliation.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            try
            {
                if (!String.IsNullOrEmpty(reg_dietary))
                {
                    ListItem listItem = ddlDietary.Items.FindByValue(reg_dietary);
                    if (listItem != null)
                    {
                        ddlDietary.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            txtNationality.Text = reg_nationality;
            txtMemberNo.Text = reg_membershipno;

            txtVName.Text = reg_vname;
            txtVDOB.Text = reg_vdob;
            txtVPassNo.Text = reg_vpassno;
            txtVPassExpiry.Text = reg_vpassexpiry;
            txtVPassIssueDate.Text = reg_vpassissuedate;
            txtVEmbarkation.Text = reg_vembarkation;
            txtVArrivalDate.Text = reg_varrivaldate;
            try
            {
                if (!String.IsNullOrEmpty(reg_vcountry))
                {
                    ListItem listItem = ddlVCountry.Items.FindByValue(reg_vcountry);
                    if (listItem != null)
                    {
                        ddlVCountry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            txtUDFDelType.Text = udf_delegatetype;
            try
            {
                if (!String.IsNullOrEmpty(udf_profcategory))
                {
                    ListItem listItem = ddlUDFProCategory.Items.FindByValue(udf_profcategory);
                    if (listItem != null)
                    {
                        ddlUDFProCategory.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtUDFProCatOther.Text = udf_profcategoryother;
            txtUDFCName.Text = udf_cname;
            txtUDFCpostalcode.Text = udf_cpcode;
            txtUDFCLDept.Text = udf_cldepartment;
            txtUDFAddress.Text = udf_caddress;
            try
            {
                if (!String.IsNullOrEmpty(udf_clcompany))
                {
                    ListItem listItem = ddlUDFCLCom.Items.FindByValue(udf_clcompany);
                    if (listItem != null)
                    {
                        ddlUDFCLCom.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtUDFCLComOther.Text = udf_clcompanyother;
            try
            {
                if (!String.IsNullOrEmpty(udf_ccountry))
                {
                    ListItem listItem = ddlUDFCCountry.Items.FindByValue(udf_ccountry);
                    if (listItem != null)
                    {
                        ddlUDFCCountry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            txtSupName.Text = reg_supervisorname;
            txtSupDesignation.Text = reg_supervisordesignation;
            txtSupContact.Text = reg_supervisorcontact;
            txtSupEmail.Text = reg_supervisoremail;

            txtAge.Text = reg_age;
            txtDOB.Text = !string.IsNullOrEmpty(reg_dob) ? Convert.ToDateTime(reg_dob).ToString("dd/MM/yyyy") : "";
            try
            {
                if (!String.IsNullOrEmpty(reg_gender))
                {
                    ListItem listItem = ddlGender.Items.FindByValue(reg_gender);
                    if (listItem != null)
                    {
                        ddlGender.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtAdditional4.Text = reg_additional4;
            txtAdditional5.Text = reg_additional5;
        }
    }
    #endregion

    #region rptitemdatabound (set repeater item cells' visibility dynamically according to the settings of tb_Form table where form_type='D')
    protected void rptitemdatabound(Object Sender, RepeaterItemEventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        if (!string.IsNullOrEmpty(showid))
        {
            // This event is raised for the header, the footer, separators, and items.
            // Execute the following logic for Items and Alternating Items.
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                System.Web.UI.HtmlControls.HtmlTableCell tdSalutation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSalutation");
                System.Web.UI.HtmlControls.HtmlTableCell tdFName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdFName");
                System.Web.UI.HtmlControls.HtmlTableCell tdLName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdLName");
                System.Web.UI.HtmlControls.HtmlTableCell tdOName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdOName");
                System.Web.UI.HtmlControls.HtmlTableCell tdPassNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPassno");
                System.Web.UI.HtmlControls.HtmlTableCell tdisReg = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdIsReg");
                System.Web.UI.HtmlControls.HtmlTableCell tdregSpecific = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdRegSpecific");
                System.Web.UI.HtmlControls.HtmlTableCell tdIDNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdIDNo");
                System.Web.UI.HtmlControls.HtmlTableCell tdDesignation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDesignation");
                System.Web.UI.HtmlControls.HtmlTableCell tdProfession = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdProfession");
                System.Web.UI.HtmlControls.HtmlTableCell tdDepartment = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDept");
                System.Web.UI.HtmlControls.HtmlTableCell tdOrganization = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdOrg");
                System.Web.UI.HtmlControls.HtmlTableCell tdInstitution = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdInstitution");
                System.Web.UI.HtmlControls.HtmlTableCell tdAddress1 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress1");
                System.Web.UI.HtmlControls.HtmlTableCell tdAddress2 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress2");
                System.Web.UI.HtmlControls.HtmlTableCell tdAddress3 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress3");
                System.Web.UI.HtmlControls.HtmlTableCell tdAddress4 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAddress4");
                System.Web.UI.HtmlControls.HtmlTableCell tdCity = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCity");
                System.Web.UI.HtmlControls.HtmlTableCell tdState = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdState");
                System.Web.UI.HtmlControls.HtmlTableCell tdPostalCode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPostal");
                System.Web.UI.HtmlControls.HtmlTableCell tdCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCountry");
                System.Web.UI.HtmlControls.HtmlTableCell tdRCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdRCountry");
                System.Web.UI.HtmlControls.HtmlTableCell tdTel = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdTel");
                System.Web.UI.HtmlControls.HtmlTableCell tdMobile = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdMobile");
                System.Web.UI.HtmlControls.HtmlTableCell tdFax = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdFax");
                System.Web.UI.HtmlControls.HtmlTableCell tdEmail = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdEmail");
                System.Web.UI.HtmlControls.HtmlTableCell tdAffiliation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAffiliation");
                System.Web.UI.HtmlControls.HtmlTableCell tdDietary = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDietary");
                System.Web.UI.HtmlControls.HtmlTableCell tdNationality = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdNationality");
                System.Web.UI.HtmlControls.HtmlTableCell tdAge = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAge");
                System.Web.UI.HtmlControls.HtmlTableCell tdDOB = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdDOB");
                System.Web.UI.HtmlControls.HtmlTableCell tdGender = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdGender");
                System.Web.UI.HtmlControls.HtmlTableCell tdMembershipNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdMembershipNo");
                System.Web.UI.HtmlControls.HtmlTableCell tdAdditional4 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAdditional4");
                System.Web.UI.HtmlControls.HtmlTableCell tdAdditional5 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdAdditional5");
                System.Web.UI.HtmlControls.HtmlTableCell tdVName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVName");
                System.Web.UI.HtmlControls.HtmlTableCell tdVDOB = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVDOB");
                System.Web.UI.HtmlControls.HtmlTableCell tdVPassNo = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVPass");
                System.Web.UI.HtmlControls.HtmlTableCell tdVPassExpiry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVPassExpiry");
                System.Web.UI.HtmlControls.HtmlTableCell tdVPassIssueDate = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVPassIssueDate");
                System.Web.UI.HtmlControls.HtmlTableCell tdVEmbarkation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVEmbarkation");
                System.Web.UI.HtmlControls.HtmlTableCell tdVArrivalDate = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVArrivalDate");
                System.Web.UI.HtmlControls.HtmlTableCell tdVCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdVCountry");
                System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CName");
                System.Web.UI.HtmlControls.HtmlTableCell tdUDF_DelegateType = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_DelegateType");
                System.Web.UI.HtmlControls.HtmlTableCell tdUDF_ProfCategory = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_ProfCategory");
                System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CPcode = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CPcode");
                System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CLDepartment = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CLDepartment");
                System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CAddress = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CAddress");
                System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CLCompany = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CLCompany");
                System.Web.UI.HtmlControls.HtmlTableCell tdUDF_CCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdUDF_CCountry");
                System.Web.UI.HtmlControls.HtmlTableCell tdSupName = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupName");
                System.Web.UI.HtmlControls.HtmlTableCell tdSupDesignation = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupDesignation");
                System.Web.UI.HtmlControls.HtmlTableCell tdSupContact = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupContact");
                System.Web.UI.HtmlControls.HtmlTableCell tdSupEmail = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdSupEmail");

                DataSet ds = new DataSet();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showid;
                frmObj.flowID = flowid;
                ds = frmObj.getDynFormForDelegate();

                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Salutation)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);


                        if (isshow == 1)
                        {
                            tdSalutation.Visible = true;
                        }
                        else
                        {
                            tdSalutation.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fname)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdFName.Visible = true;
                        }
                        else
                        {
                            tdFName.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Lname)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdLName.Visible = true;

                        }
                        else
                        {
                            tdLName.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _OName)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdOName.Visible = true;
                        }
                        else
                        {
                            tdOName.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PassNo)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdPassNo.Visible = true;
                        }
                        else
                        {
                            tdPassNo.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _isReg)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdisReg.Visible = true;
                        }
                        else
                        {
                            tdisReg.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _regSpecific)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdregSpecific.Visible = true;
                        }
                        else
                        {
                            tdregSpecific.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _IDNo)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdIDNo.Visible = true;
                        }
                        else
                        {
                            tdIDNo.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Designation)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdDesignation.Visible = true;
                        }
                        else
                        {
                            tdDesignation.Visible = false;
                        }

                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdProfession.Visible = true;
                        }
                        else
                        {
                            tdProfession.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdDepartment.Visible = true;
                        }
                        else
                        {
                            tdDepartment.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdOrganization.Visible = true;
                        }
                        else
                        {
                            tdOrganization.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Institution)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdInstitution.Visible = true;
                        }
                        else
                        {
                            tdInstitution.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdAddress1.Visible = true;
                        }
                        else
                        {
                            tdAddress1.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdAddress2.Visible = true;
                        }
                        else
                        {
                            tdAddress2.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdAddress3.Visible = true;
                        }
                        else
                        {
                            tdAddress3.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address4)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdAddress4.Visible = true;
                        }
                        else
                        {
                            tdAddress4.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdCity.Visible = true;
                        }
                        else
                        {
                            tdCity.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdState.Visible = true;
                        }
                        else
                        {
                            tdState.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _PostalCode)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdPostalCode.Visible = true;
                        }
                        else
                        {
                            tdPostalCode.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdCountry.Visible = true;
                        }
                        else
                        {
                            tdCountry.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _RCountry)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdRCountry.Visible = true;
                        }
                        else
                        {
                            tdRCountry.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdTel.Visible = true;
                        }
                        else
                        {
                            tdTel.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdMobile.Visible = true;
                        }
                        else
                        {
                            tdMobile.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdFax.Visible = true;
                        }
                        else
                        {
                            tdFax.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdEmail.Visible = true;
                        }
                        else
                        {
                            tdEmail.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdAffiliation.Visible = true;
                        }
                        else
                        {
                            tdAffiliation.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Dietary)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdDietary.Visible = true;
                        }
                        else
                        {
                            tdDietary.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Nationality)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdNationality.Visible = true;
                        }
                        else
                        {
                            tdNationality.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Age)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdAge.Visible = true;
                        }
                        else
                        {
                            tdAge.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _DOB)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdDOB.Visible = true;
                        }
                        else
                        {
                            tdDOB.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Gender)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdGender.Visible = true;
                        }
                        else
                        {
                            tdGender.Visible = false;
                        }
                    }


                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _MembershipNo)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdMembershipNo.Visible = true;
                        }
                        else
                        {
                            tdMembershipNo.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdAdditional4.Visible = true;
                        }
                        else
                        {
                            tdAdditional4.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdAdditional5.Visible = true;
                        }
                        else
                        {
                            tdAdditional5.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VName)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdVName.Visible = true;
                        }
                        else
                        {
                            tdVName.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VDOB)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdVDOB.Visible = true;
                        }
                        else
                        {
                            tdVDOB.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassNo)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdVPassNo.Visible = true;
                        }
                        else
                        {
                            tdVPassNo.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassIssueDate)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdVPassIssueDate.Visible = true;
                        }
                        else
                        {
                            tdVPassIssueDate.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VPassExpiry)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdVPassExpiry.Visible = true;
                        }
                        else
                        {
                            tdVPassExpiry.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VEmbarkation)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdVEmbarkation.Visible = true;
                        }
                        else
                        {
                            tdVEmbarkation.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VArrivalDate)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdVArrivalDate.Visible = true;
                        }
                        else
                        {
                            tdVArrivalDate.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _VCountry)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdVCountry.Visible = true;
                        }
                        else
                        {
                            tdVCountry.Visible = false;
                        }
                    }

                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CName)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdUDF_CName.Visible = true;
                        }
                        else
                        {
                            tdUDF_CName.Visible = false;
                        }
                    }
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_DelegateType)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdUDF_DelegateType.Visible = true;
                        }
                        else
                        {
                            tdUDF_DelegateType.Visible = false;
                        }
                    }
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_ProfCategory)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdUDF_ProfCategory.Visible = true;
                        }
                        else
                        {
                            tdUDF_ProfCategory.Visible = false;
                        }
                    }
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CPcode)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdUDF_CPcode.Visible = true;
                        }
                        else
                        {
                            tdUDF_CPcode.Visible = false;
                        }
                    }
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLDepartment)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdUDF_CLDepartment.Visible = true;
                        }
                        else
                        {
                            tdUDF_CLDepartment.Visible = false;
                        }
                    }
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CAddress)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdUDF_CAddress.Visible = true;
                        }
                        else
                        {
                            tdUDF_CAddress.Visible = false;
                        }
                    }
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CLCompany)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdUDF_CLCompany.Visible = true;
                        }
                        else
                        {
                            tdUDF_CLCompany.Visible = false;
                        }
                    }
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _UDF_CCountry)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdUDF_CCountry.Visible = true;
                        }
                        else
                        {
                            tdUDF_CCountry.Visible = false;
                        }
                    }
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupName)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdSupName.Visible = true;
                        }
                        else
                        {
                            tdSupName.Visible = false;
                        }
                    }
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupDesignation)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdSupDesignation.Visible = true;
                        }
                        else
                        {
                            tdSupDesignation.Visible = false;
                        }
                    }
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupContact)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdSupContact.Visible = true;
                        }
                        else
                        {
                            tdSupContact.Visible = false;
                        }
                    }
                    if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _SupEmail)
                    {
                        int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_active"]);

                        if (isshow == 1)
                        {
                            tdSupEmail.Visible = true;
                        }
                        else
                        {
                            tdSupEmail.Visible = false;
                        }
                    }
                }
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }
    #endregion

    #region rptitemcommand (repeater controls item command 'Edit' event & get data according to 'delegate id' and bind into respective controls)
    protected void rptitemcommand(object source, RepeaterCommandEventArgs e)
    {
        string regno = e.CommandArgument.ToString();
        hfRegno.Value = regno;

        if (e.CommandName == "edit")
        {
            try
            {
                CommonFuns cFun = new CommonFuns();
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                string flowid = cFun.DecryptValue(urlQuery.FlowID);

                setDynamicForm(flowid,showid);
                populateUserDetails(groupid, regno, showid);
                btnSave.Text = "Update";
                bindDelegateList(groupid, showid);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
    #endregion

    #region ClearForm (clear data from controls)
    private void ClearForm()
    {
        hfRegno.Value = "";

        UpdatePanel1.Visible = false;

        ddlSalutation.SelectedIndex = 0;
        txtSalOther.Text = "";
        txtFName.Text = "";
        txtLName.Text = "";
        txtOName.Text = "";
        txtPassNo.Text = "";
        rbreg.SelectedIndex = 1;
        rbregspecific.ClearSelection();
        txtIDNo.Text = "";
        ddlProfession.SelectedIndex = 0;
        txtProOther.Text = "";
        ddlStudentType.SelectedIndex = 0;
        txtStudentOther.Text = "";
        txtJobtitle.Text = "";
        txtDesignation.Text = "";
        ddlDepartment.SelectedIndex = 0;
        txtDepartmentOther.Text = "";
        ddlOrganization.SelectedIndex = 0;
        txtOrgOther.Text = "";
        ddlInstitution.SelectedIndex = 0;
        txtInstiOther.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtAddress4.Text = "";
        txtCity.Text = "";
        txtPostalcode.Text = "";
        txtState.Text = "";
        ddlCountry.SelectedIndex = 0;
        ddlRCountry.SelectedIndex = 0;
        txtTelcc.Text = "";
        txtTelac.Text = "";
        txtTel.Text = "";
        txtMobcc.Text = "";
        txtMobac.Text = "";
        txtMobile.Text = "";
        txtFaxcc.Text = "";
        txtFaxac.Text = "";
        txtFax.Text = "";
        txtEmail.Text = "";
        ddlAffiliation.SelectedIndex = 0;
        ddlDietary.SelectedIndex = 0;
        txtNationality.Text = "";
        txtAge.Text = "";
        ddlGender.SelectedIndex = 0;
        txtMemberNo.Text = "";
        txtAdditional4.Text = "";
        txtAdditional5.Text = "";
        txtVName.Text = "";
        txtVDOB.Text = "";
        txtVPassNo.Text = "";
        txtVPassExpiry.Text = "";
        txtVPassIssueDate.Text = "";
        txtVEmbarkation.Text = "";
        txtVArrivalDate.Text = "";
        ddlVCountry.SelectedIndex = 0;
        txtUDFCName.Text = "";
        txtUDFDelType.Text = "";
        ddlUDFProCategory.SelectedIndex = 0;
        txtUDFProCatOther.Text = "";
        txtUDFCpostalcode.Text = "";
        txtUDFCLDept.Text = "";
        txtUDFAddress.Text = "";
        ddlUDFCLCom.SelectedIndex = 0;
        txtUDFCLComOther.Text = "";
        ddlUDFCCountry.SelectedIndex = 0;
        txtSupName.Text = "";
        txtSupDesignation.Text = "";
        txtSupContact.Text = "";
        txtSupEmail.Text = "";
    }
    #endregion

    #region ddlSalutation_SelectedIndexChanged (set 'other salutation div' visibility if the selection of ddlSalutation dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherSal and form_type='D')
    protected void ddlSalutation_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlSalutation.Items.Count > 0)
            {
                if (ddlSalutation.SelectedItem.Text == others_value || ddlSalutation.SelectedItem.Text == other_value)
                {
                    FormManageObj frmObj = new FormManageObj(fn);
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherSal);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divSalOther.Visible = true;
                        }
                        else
                        {
                            divSalOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtSalOther.Attributes.Add("required", "");
                            vcSalOther.Enabled = true;
                        }
                        else
                        {
                            //txtSalOther.Attributes.Remove("required");
                            vcSalOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divSalOther.Visible = false;
                    //txtSalOther.Attributes.Remove("required");
                    vcSalOther.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region ddlCountry_SelectedIndexChanged (bind country code data to txtTelcc, txtMobcc, txtFaxcc textboxes according to the selected country)
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlCountry.Items.Count > 0)
            {
                string countryid = ddlCountry.SelectedItem.Value;

                if (ddlCountry.SelectedIndex == 0)
                {
                    countryid = Number.Zero;
                }

                CountryObj couObj = new CountryObj(fn);
                DataTable dt = couObj.getCountryByID(countryid);
                if (dt.Rows.Count > 0)
                {
                    string code = dt.Rows[0]["countryen"].ToString();

                    txtTelcc.Text = code;
                    txtMobcc.Text = code;
                    txtFaxcc.Text = code;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlRCountry_SelectedIndexChanged (bind country code data to txtTelcc, txtMobcc, txtFaxcc textboxes according to the selected country)
    protected void ddlRCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlRCountry.Items.Count > 0)
            {
                string countryid = ddlRCountry.SelectedItem.Value;

                if (ddlRCountry.SelectedIndex == 0)
                {
                    countryid = Number.Zero;
                }

                CountryObj couObj = new CountryObj(fn);
                DataTable dt = couObj.getCountryByID(countryid);
                if (dt.Rows.Count > 0)
                {
                    string code = dt.Rows[0]["countryen"].ToString();

                    txtTelcc.Text = code;
                    txtMobcc.Text = code;
                    txtFaxcc.Text = code;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region ddlProfession_SelectedIndexChanged (set 'other profession div' visibility if the selection of ddlProfession dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherProfession and form_type='D')
    protected void ddlProfession_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlProfession.Items.Count > 0)
            {
                if (ddlProfession.SelectedItem.Text == others_value || ddlProfession.SelectedItem.Text == other_value)
                {
                    FormManageObj frmObj = new FormManageObj(fn);
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherProfession);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divProOther.Visible = true;
                        }
                        else
                        {
                            divProOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtProOther.Attributes.Add("required", "");
                            vcProOther.Enabled = true;
                        }
                        else
                        {
                            //txtProOther.Attributes.Remove("required");
                            vcProOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divProOther.Visible = false;
                    //txtProOther.Attributes.Remove("required");
                    vcProOther.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region ddlStudentType_SelectedIndexChanged (set 'other student div' visibility if the selection of ddlProfession dropdownlist is 'Other' or 'Others')
    protected void ddlStudentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlStudentType.Items.Count > 0)
            {
                if (ddlStudentType.SelectedItem.Text == others_value || ddlStudentType.SelectedItem.Text == other_value)
                {
                    divStudentOther.Visible = true;
                    vcStudentOther.Enabled = true;
                }
                else
                {
                    divStudentOther.Visible = false;
                    vcStudentOther.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region ddlDepartment_SelectedIndexChanged (set 'other department div' visibility if the selection of ddlDepartment dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherDept and form_type='D')
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlDepartment.Items.Count > 0)
            {
                if (ddlDepartment.SelectedItem.Text == others_value || ddlDepartment.SelectedItem.Text == other_value)
                {
                    FormManageObj frmObj = new FormManageObj(fn);
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherDept);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divDepartmentOther.Visible = true;
                        }
                        else
                        {
                            divDepartmentOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtDepartmentOther.Attributes.Add("required", "");
                            vcDeptmOther.Enabled = true;
                        }
                        else
                        {
                            //txtDepartmentOther.Attributes.Remove("required");
                            vcDeptmOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divDepartmentOther.Visible = false;
                    //txtDepartmentOther.Attributes.Remove("required");
                    vcDeptmOther.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region ddlOrganization_SelectedIndexChanged (set 'other organization div' visibility if the selection of ddlOrganization dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherOrg and form_type='D')
    protected void ddlOrganization_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlOrganization.Items.Count > 0)
            {
                if (ddlOrganization.SelectedItem.Text == others_value || ddlOrganization.SelectedItem.Text == other_value)
                {
                    FormManageObj frmObj = new FormManageObj(fn);
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherOrg);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divOrgOther.Visible = true;
                        }
                        else
                        {
                            divOrgOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtOrgOther.Attributes.Add("required", "");
                            vcOrgOther.Enabled = true;
                        }
                        else
                        {
                            //txtOrgOther.Attributes.Remove("required");
                            vcOrgOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divOrgOther.Visible = false;
                    //txtOrgOther.Attributes.Remove("required");
                    vcOrgOther.Enabled = false;
                }

                GetInstitution();
                ddlInstitution_SelectedIndexChanged(this, null);
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region ddlInstitution_SelectedIndexChanged (set 'other institution div' visibility if the selection of ddlInstitution dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherInstitution and form_type='D')
    protected void ddlInstitution_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlInstitution.Items.Count > 0)
            {
                if (ddlInstitution.SelectedItem.Text == others_value || ddlInstitution.SelectedItem.Text == other_value)
                {
                    FormManageObj frmObj = new FormManageObj(fn);
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherInstitution);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divInstiOther.Visible = true;
                        }
                        else
                        {
                            divInstiOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtInstiOther.Attributes.Add("required", "");
                            vcInstiOther.Enabled = true;
                        }
                        else
                        {
                            //txtInstiOther.Attributes.Remove("required");
                            vcInstiOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divInstiOther.Visible = false;
                    //txtInstiOther.Attributes.Remove("required");
                    vcInstiOther.Enabled = false;
                }

                GetDepartment();
                ddlDepartment_SelectedIndexChanged(this, null);
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region ddlUDFProCategory_SelectedIndexChanged (set 'other UDFProCategory div' visibility if the selection of ddlUDFProCategory dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_UDF_ProfCategroyOther and form_type='D')
    protected void ddlUDFProCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlUDFProCategory.Items.Count > 0)
            {
                if (ddlUDFProCategory.SelectedItem.Text == others_value || ddlUDFProCategory.SelectedItem.Text == other_value)
                {
                    FormManageObj frmObj = new FormManageObj(fn);
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _UDF_ProfCategroyOther);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divUDFProCatOther.Visible = true;
                        }
                        else
                        {
                            divUDFProCatOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtUDFProCatOther.Attributes.Add("required", "");
                            vcProCatOther.Enabled = true;
                        }
                        else
                        {
                            //txtUDFProCatOther.Attributes.Remove("required");
                            vcProCatOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divUDFProCatOther.Visible = false;
                    //txtUDFProCatOther.Attributes.Remove("required");
                    vcProCatOther.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region ddlUDFCLCom_SelectedIndexChanged (set 'other UDFCLCom div' visibility if the selection of ddlUDFCLCom dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_UDF_CLCompanyOther and form_type='D')
    protected void ddlUDFCLCom_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlUDFCLCom.Items.Count > 0)
            {
                if (ddlUDFCLCom.SelectedItem.Text == others_value || ddlUDFCLCom.SelectedItem.Text == other_value)
                {
                    FormManageObj frmObj = new FormManageObj(fn);
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _UDF_CLCompanyOther);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divUDFCLComOther.Visible = true;
                        }
                        else
                        {
                            divUDFCLComOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtUDFCLComOther.Attributes.Add("required", "");
                            vcUDFCLComOther.Enabled = true;
                        }
                        else
                        {
                            //txtUDFCLComOther.Attributes.Remove("required");
                            vcUDFCLComOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divUDFCLComOther.Visible = false;
                    //txtUDFCLComOther.Attributes.Remove("required");
                    vcUDFCLComOther.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region rbreg_SelectedIndexChanged (set 'divRegSpecific visibility', 'divIDNo visibility' and 'vcIDNo enability' true or false according to 'rbreg' control(are you singapore registered doctor/nurse/pharmacist?) selected value)
    protected void rbreg_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (rbreg.Items.Count > 0)
            {
                if (rbreg.SelectedItem.Value == Number.One)
                {
                    divRegSpecific.Visible = true;
                    divIDNo.Visible = true;
                    vcIDNo.Enabled = true;
                }
                else
                {
                    divRegSpecific.Visible = false;
                    divIDNo.Visible = false;
                    txtIDNo.Text = "";
                    vcIDNo.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region GetInstitution (get institution according to the selected organization id(ddlOrganization.SelectedItem.Value) from ref_Institution table)
    protected void GetInstitution()
    {
        ddlInstitution.Items.Clear();
        ddlInstitution.ClearSelection();

        DataSet dsInstitution = new DataSet();
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            CommonDataObj cmdObj = new CommonDataObj(fn);
            dsInstitution = cmdObj.getInstitutionByOrganizationID(ddlOrganization.SelectedItem.Value, showid);

            if (dsInstitution.Tables[0].Rows.Count != 0)
            {
                for (int y = 0; y < dsInstitution.Tables[0].Rows.Count; y++)
                {
                    ddlInstitution.Items.Add(dsInstitution.Tables[0].Rows[y]["institution"].ToString());
                    ddlInstitution.Items[y].Value = dsInstitution.Tables[0].Rows[y]["ID"].ToString();
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region GetDepartment (get department according to the selected institution id(ddlInstitution.SelectedItem.Value) from ref_Department table)
    protected void GetDepartment()
    {
        ddlDepartment.Items.Clear();
        ddlDepartment.ClearSelection();

        DataSet dsDepartment = new DataSet();
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            CommonDataObj cmdObj = new CommonDataObj(fn);
            dsDepartment = cmdObj.getDepartmentByInstitutionID(ddlInstitution.SelectedItem.Value, showid);

            if (dsDepartment.Tables[0].Rows.Count != 0)
            {
                for (int y = 0; y < dsDepartment.Tables[0].Rows.Count; y++)
                {
                    ddlDepartment.Items.Add(dsDepartment.Tables[0].Rows[y]["department"].ToString());
                    ddlDepartment.Items[y].Value = dsDepartment.Tables[0].Rows[y]["ID"].ToString();
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region btnSave_Click (create one record (just RegGroupID and recycle=0) into tb_RegGroup table if current groupid not exist in tb_RegGroup & save/update data into tb_RegDelegate table & clear data from controls)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            if (Session["Groupid"] != null && !string.IsNullOrEmpty(hfRegno.Value))
            {
                int isSuccess = 0;
                string groupid = Session["Groupid"].ToString();
                Boolean hasid = false;

                RegDelegateObj rgd = new RegDelegateObj(fn);
                string regno = hfRegno.Value;

                hasid = true;

                try
                {
                    int con_categoryID = 0;
                    string salutation = string.Empty;
                    string fname = string.Empty;
                    string lname = string.Empty;
                    string oname = string.Empty;
                    string passno = string.Empty;
                    int isreg = 0;
                    string regspecific = string.Empty;//MCR/SNB/PRN
                    string idno = string.Empty;//MCR/SNB/PRN No.
                    string staffid = string.Empty;//no use in design for this field
                    string designation = string.Empty;
                    string jobtitle = string.Empty;//if Profession is Allied Health
                    string profession = string.Empty;
                    string department = string.Empty;
                    string organization = string.Empty;
                    string institution = string.Empty;
                    string address1 = string.Empty;
                    string address2 = string.Empty;
                    string address3 = string.Empty;
                    string address4 = string.Empty;
                    string city = string.Empty;
                    string state = string.Empty;
                    string postalcode = string.Empty;
                    string country = string.Empty;
                    string rcountry = string.Empty;
                    string telcc = string.Empty;
                    string telac = string.Empty;
                    string tel = string.Empty;
                    string mobilecc = string.Empty;
                    string mobileac = string.Empty;
                    string mobile = string.Empty;
                    string faxcc = string.Empty;
                    string faxac = string.Empty;
                    string fax = string.Empty;
                    string email = string.Empty;
                    string affiliation = string.Empty;
                    string dietary = string.Empty;
                    string nationality = string.Empty;
                    int age = 0;
                    DateTime? dob = null;
                    string dob_str = string.Empty;
                    string gender = string.Empty;
                    string additional4 = string.Empty;
                    string additional5 = string.Empty;
                    string memberno = string.Empty;

                    string vname = string.Empty;
                    string vdob = string.Empty;
                    string vpassno = string.Empty;
                    string vpassexpiry = string.Empty;
                    string vpassissuedate = string.Empty;
                    string vembarkation = string.Empty;
                    string varrivaldate = string.Empty;
                    string vcountry = string.Empty;

                    string udfcname = string.Empty;
                    string udfdeltype = string.Empty;
                    string udfprofcat = string.Empty;
                    string udfprofcatother = string.Empty;
                    string udfcpcode = string.Empty;
                    string udfcldept = string.Empty;
                    string udfcaddress = string.Empty;
                    string udfclcompany = string.Empty;
                    string udfclcompanyother = string.Empty;
                    string udfccountry = string.Empty;

                    string supname = string.Empty;
                    string supdesignation = string.Empty;
                    string supcontact = string.Empty;
                    string supemail = string.Empty;

                    string othersal = string.Empty;
                    string otherprof = string.Empty;
                    string otherdept = string.Empty;
                    string otherorg = string.Empty;
                    string otherinstitution = string.Empty;

                    string aemail = string.Empty;
                    int isSMS = 0;

                    string remark = string.Empty;
                    string remark_groupupload = string.Empty;
                    int approvestatus = 0;
                    string createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                    int recycle = 0;
                    string stage = string.Empty;

                    salutation = cFun.solveSQL(ddlSalutation.SelectedItem.Value.ToString());
                    if ((ddlSalutation.SelectedItem.Text == others_value || ddlSalutation.SelectedItem.Text == other_value) && txtSalOther.Text != "" && txtSalOther.Text != string.Empty)
                    {
                        othersal = cFun.solveSQL(txtSalOther.Text.Trim());
                    }

                    fname = cFun.solveSQL(txtFName.Text.Trim());
                    lname = cFun.solveSQL(txtLName.Text.Trim());
                    oname = cFun.solveSQL(txtOName.Text.Trim());
                    passno = cFun.solveSQL(txtPassNo.Text);

                    isreg = rbreg.SelectedItem.Value != "" ? Convert.ToInt32(rbreg.SelectedItem.Value) : 0;
                    if (isreg == 1)
                    {
                        regspecific = rbregspecific.SelectedItem.Value;
                    }

                    idno = cFun.solveSQL(txtIDNo.Text.Trim());

                    designation = cFun.solveSQL(txtDesignation.Text.ToString());
                    jobtitle = cFun.solveSQL(txtJobtitle.Text.ToString());

                    profession = cFun.solveSQL(ddlProfession.SelectedItem.Value.ToString());
                    if ((ddlProfession.SelectedItem.Text == others_value || ddlProfession.SelectedItem.Text == other_value) && txtProOther.Text != "" && txtProOther.Text != string.Empty)
                    {
                        otherprof = cFun.solveSQL(txtProOther.Text.Trim());
                    }

                    department = cFun.solveSQL(ddlDepartment.SelectedItem.Value.ToString());
                    if ((ddlDepartment.SelectedItem.Text == others_value || ddlDepartment.SelectedItem.Text == other_value) && txtDepartmentOther.Text != "" && txtDepartmentOther.Text != string.Empty)
                    {
                        otherdept = cFun.solveSQL(txtDepartmentOther.Text.Trim());
                    }

                    organization = cFun.solveSQL(ddlOrganization.SelectedItem.Value.ToString());
                    if ((ddlOrganization.SelectedItem.Text == others_value || ddlOrganization.SelectedItem.Text == other_value) && txtOrgOther.Text != "" && txtOrgOther.Text != string.Empty)
                    {
                        otherorg = cFun.solveSQL(txtOrgOther.Text.Trim());
                    }

                    institution = cFun.solveSQL(ddlInstitution.SelectedItem.Value.ToString());
                    if ((ddlInstitution.SelectedItem.Text == others_value || ddlInstitution.SelectedItem.Text == other_value) && txtInstiOther.Text != "" && txtInstiOther.Text != string.Empty)
                    {
                        otherinstitution = cFun.solveSQL(txtInstiOther.Text.Trim());
                    }

                    address1 = cFun.solveSQL(txtAddress1.Text.Trim());
                    address2 = cFun.solveSQL(txtAddress2.Text.Trim());
                    address3 = cFun.solveSQL(txtAddress3.Text.Trim());
                    address4 = cFun.solveSQL(txtAddress4.Text.Trim());
                    city = cFun.solveSQL(txtCity.Text.Trim());
                    postalcode = cFun.solveSQL(txtPostalcode.Text.Trim());
                    state = cFun.solveSQL(txtState.Text.Trim());
                    country = cFun.solveSQL(ddlCountry.SelectedItem.Value.ToString());
                    rcountry = cFun.solveSQL(ddlRCountry.SelectedItem.Value.ToString());
                    telcc = txtTelcc.Text.ToString();
                    telac = txtTelac.Text.ToString();
                    tel = txtTel.Text.ToString();
                    mobilecc = txtMobcc.Text.ToString();
                    mobileac = txtMobac.Text.ToString();
                    mobile = txtMobile.Text.ToString();
                    faxcc = txtFaxcc.Text.ToString();
                    faxac = txtFaxac.Text.ToString();
                    fax = txtFax.Text.ToString();
                    email = cFun.solveSQL(txtEmail.Text.Trim());
                    affiliation = cFun.solveSQL(ddlAffiliation.SelectedItem.Value.ToString());
                    dietary = cFun.solveSQL(ddlDietary.SelectedItem.Value.ToString());
                    nationality = cFun.solveSQL(txtNationality.Text.Trim());
                    memberno = cFun.solveSQL(txtMemberNo.Text.Trim());

                    vname = cFun.solveSQL(txtVName.Text.Trim());
                    vdob = cFun.solveSQL(txtVDOB.Text.Trim());
                    vpassno = cFun.solveSQL(txtVPassNo.Text.Trim());
                    if (!String.IsNullOrWhiteSpace(txtVPassExpiry.Text))
                    {
                        if (!cFun.validateDate(txtVPassExpiry.Text.Trim()))
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                            return;
                        }

                        vpassexpiry = cFun.solveSQL(txtVPassExpiry.Text.Trim());
                    }
                    if (!String.IsNullOrWhiteSpace(txtVPassIssueDate.Text))
                    {
                        if (!cFun.validateDate(txtVPassIssueDate.Text.Trim()))
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                            return;
                        }

                        vpassissuedate = cFun.solveSQL(txtVPassIssueDate.Text.Trim());
                    }
                    vembarkation = cFun.solveSQL(txtVEmbarkation.Text.Trim());
                    if (!String.IsNullOrWhiteSpace(txtVArrivalDate.Text))
                    {
                        if (!cFun.validateDate(txtVArrivalDate.Text.Trim()))
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                            return;
                        }

                        varrivaldate = cFun.solveSQL(txtVArrivalDate.Text.Trim());
                    }
                    vcountry = cFun.solveSQL(ddlVCountry.SelectedItem.Value.Trim());

                    udfcname = cFun.solveSQL(txtUDFCName.Text.Trim());
                    udfdeltype = cFun.solveSQL(txtUDFDelType.Text.Trim());
                    udfprofcat = cFun.solveSQL(ddlUDFProCategory.SelectedItem.Value.Trim());
                    if ((ddlUDFProCategory.SelectedItem.Text == others_value || ddlUDFProCategory.SelectedItem.Text == other_value) && txtUDFProCatOther.Text != "" && txtUDFProCatOther.Text != string.Empty)
                    {
                        udfprofcatother = cFun.solveSQL(txtUDFProCatOther.Text.Trim());
                    }
                    udfcpcode = cFun.solveSQL(txtUDFCpostalcode.Text.Trim());
                    udfcldept = cFun.solveSQL(txtUDFCLDept.Text.Trim());
                    udfcaddress = cFun.solveSQL(txtUDFAddress.Text.Trim());
                    udfclcompany = cFun.solveSQL(ddlUDFCLCom.SelectedItem.Value.ToString());
                    if ((ddlUDFCLCom.SelectedItem.Text == others_value || ddlUDFCLCom.SelectedItem.Text == other_value) && txtUDFCLComOther.Text != "" && txtUDFCLComOther.Text != string.Empty)
                    {
                        udfclcompanyother = cFun.solveSQL(txtUDFCLComOther.Text.Trim());
                    }
                    udfccountry = cFun.solveSQL(ddlUDFCCountry.SelectedItem.Value.ToString());

                    supname = cFun.solveSQL(txtSupName.Text.Trim());
                    supdesignation = cFun.solveSQL(txtSupDesignation.Text.Trim());
                    supcontact = cFun.solveSQL(txtSupContact.Text.Trim());
                    supemail = cFun.solveSQL(txtSupEmail.Text.Trim());

                    if (!String.IsNullOrWhiteSpace(txtAge.Text))
                    {
                        age = Convert.ToInt32(cFun.solveSQL(txtAge.Text.Trim()));
                    }
                    if (!String.IsNullOrWhiteSpace(txtDOB.Text))
                    {
                        if (!cFun.validateDate(txtDOB.Text.Trim()))
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                            return;
                        }

                        dob = DateTime.Parse(txtDOB.Text.Trim());
                        dob_str = dob.Value.ToString("yyyy-MM-dd hh:mm:ss");
                    }
                    gender = cFun.solveSQL(ddlGender.SelectedItem.Value.Trim());
                    additional4 = cFun.solveSQL(txtAdditional4.Text.Trim());
                    additional5 = cFun.solveSQL(txtAdditional5.Text.Trim());

                    con_categoryID = Convert.ToInt32(txtCategoryID.Text.Trim());

                    rgd.groupid = groupid;
                    rgd.regno = regno;
                    rgd.con_categoryID = con_categoryID;
                    rgd.salutation = salutation;
                    rgd.fname = fname;
                    rgd.lname = lname;
                    rgd.oname = oname;
                    rgd.passno = passno;
                    rgd.isreg = isreg;
                    rgd.regspecific = regspecific;//MCR/SNB/PRN
                    rgd.idno = idno;//MCR/SNB/PRN No.
                    rgd.staffid = staffid;//no use in design for this field
                    rgd.designation = designation;
                    rgd.jobtitle = jobtitle;//if Profession is Allied Health
                    rgd.profession = profession;
                    rgd.department = department;
                    rgd.organization = organization;
                    rgd.institution = institution;
                    rgd.address1 = address1;
                    rgd.address2 = address2;
                    rgd.address3 = address3;
                    rgd.address4 = address4;
                    rgd.city = city;
                    rgd.state = state;
                    rgd.postalcode = postalcode;
                    rgd.country = country;
                    rgd.rcountry = rcountry;
                    rgd.telcc = telcc;
                    rgd.telac = telac;
                    rgd.tel = tel;
                    rgd.mobilecc = mobilecc;
                    rgd.mobileac = mobileac;
                    rgd.mobile = mobile;
                    rgd.faxcc = faxcc;
                    rgd.faxac = faxac;
                    rgd.fax = fax;
                    rgd.email = email;
                    rgd.affiliation = affiliation;
                    rgd.dietary = dietary;
                    rgd.nationality = nationality;
                    rgd.age = age;
                    rgd.dob = dob_str;
                    rgd.gender = gender;
                    rgd.additional4 = additional4;
                    rgd.additional5 = additional5;
                    rgd.memberno = memberno;

                    rgd.vname = vname;
                    rgd.vdob = vdob;
                    rgd.vpassno = vpassno;
                    rgd.vpassexpiry = vpassexpiry;
                    rgd.vpassissuedate = vpassissuedate;
                    rgd.vembarkation = vembarkation;
                    rgd.varrivaldate = varrivaldate;
                    rgd.vcountry = vcountry;

                    rgd.udfcname = udfcname;
                    rgd.udfdeltype = udfdeltype;
                    rgd.udfprofcat = udfprofcat;
                    rgd.udfprofcatother = udfprofcatother;
                    rgd.udfcpcode = udfcpcode;
                    rgd.udfcldept = udfcldept;
                    rgd.udfcaddress = udfcaddress;
                    rgd.udfclcompany = udfclcompany;
                    rgd.udfclcompanyother = udfclcompanyother;
                    rgd.udfccountry = udfccountry;

                    rgd.supname = supname;
                    rgd.supdesignation = supdesignation;
                    rgd.supcontact = supcontact;
                    rgd.supemail = supemail;

                    rgd.othersal = othersal;
                    rgd.otherprof = otherprof;
                    rgd.otherdept = otherdept;
                    rgd.otherorg = otherorg;
                    rgd.otherinstitution = otherinstitution;

                    rgd.aemail = aemail;
                    rgd.isSMS = isSMS;

                    rgd.remark = remark;
                    rgd.remark_groupupload = remark_groupupload;
                    rgd.approvestatus = approvestatus;
                    rgd.createdate = createdate;
                    rgd.recycle = recycle;

                    rgd.showID = showid;

                    bool isAlreadyExist = false;
                    if (hasid)
                    {
                        //*Update
                        //isAlreadyExist = rgd.checkUpdateExist();
                        //if (isAlreadyExist == true)
                        //{
                        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This user already exists!');", true);
                        //    return;
                        //}
                        //else
                        {
                            isSuccess = rgd.updateRegDelegate();
                        }
                    }

                    btnSave.Text = "Save";
                    UpdatePanel1.Visible = false;
                    bindDelegateList(groupid, showid);
                    ClearForm();

                    if (isSuccess > 0)
                    {
                        insertRegLoginAction(groupid, rlgobj.actupdate, urlQuery);

                        string windowreload = "window.location='" + Request.Url.ToString() + "';";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updated successful.');" + windowreload, true);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    LogGenEmail lggenemail = new LogGenEmail(fn);
                    lggenemail.type = GenLogDefaultValue.errorException;
                    lggenemail.RefNumber = groupid + "," + regno;
                    lggenemail.description = ex.Message;
                    lggenemail.remark = RegClass.typeDeg + cFun.DecryptValue(urlQuery.FlowID);
                    lggenemail.step = cFun.DecryptValue(urlQuery.CurrIndex);
                    lggenemail.writeLog();
                }
            }
            else
            {
                Response.Redirect("ReLogin.aspx?Event=" + txtFlowName.Text.Trim());//?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }
    #endregion

    #region bindName
    public string bindSalutation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getSalutationNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }

                if (name == others_value || name == other_value)
                {
                    name = otherValue;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindProfession(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getProfessionNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDepartment(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getDepartmentNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindOrganisation(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getOrganisationNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindInstitution(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getInstitutionNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindCountry(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                CountryObj conCtr = new CountryObj(fn);
                name = conCtr.getCountryNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindAffiliation(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getAffiliationNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }

    public string bindDietary(string id)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getDietaryNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion

    #region bindPhoneNo
    public string bindPhoneNo(string cc, string ac, string phoneno, string type)
    {
        string name = string.Empty;
        bool isShowCC = false, isShowAC = false, isShowPhoneNo = false;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                DataSet ds = new DataSet();
                FormManageObj frmObj = new FormManageObj(fn);
                frmObj.showID = showid;
                frmObj.flowID = flowid;
                ds = frmObj.getDynFormForDelegate();

                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    if (type == "Tel")
                    {
                        #region type="Tel"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telcc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Telac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                    else if (type == "Mob")
                    {
                        #region type="Mob"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobilecc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobileac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Mobile)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                    else if (type == "Fax")
                    {
                        #region Type="Fax"
                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxcc)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowCC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Faxac)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowAC = true;
                            }
                        }

                        if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
                        {
                            int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                            int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                            if (isshow == 1)
                            {
                                isShowPhoneNo = true;
                            }
                        }
                        #endregion
                    }
                }

                if (isShowCC)
                {
                    name = "+" + cc;
                }
                if (isShowAC)
                {
                    name += " " + ac;
                }
                if (isShowPhoneNo)
                {
                    name += " " + phoneno;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion

    #region insertRegLoginAction (insert flow data into tb_Log_RegLogin table)
    private void insertRegLoginAction(string groupid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogRegLogin rlg = new LogRegLogin(fn);
        rlg.reglogin_regno = groupid;
        rlg.reglogin_flowid = flowid;
        rlg.reglogin_step = step;
        rlg.reglogin_action = action;
        rlg.saveLogRegLogin();
    }
    #endregion

}