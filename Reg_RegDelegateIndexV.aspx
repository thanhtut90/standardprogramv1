﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReLoginMaster.master" AutoEventWireup="true" CodeFile="Reg_RegDelegateIndexV.aspx.cs" Inherits="Reg_RegDelegateIndexV" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .red
        {
            color:red;
        }
    </style>

    <script type="text/javascript">
        function validateDate(elementRef) {
                var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

                var dateValue = elementRef.value;

                if (dateValue.length != 10) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                // mm/dd/yyyy format... 
                var valueArray = dateValue.split('/');

                if (valueArray.length != 3) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthValue = parseFloat(valueArray[1]);
                var dayValue = parseFloat(valueArray[0]);
                var yearValue = parseFloat(valueArray[2]);

                if ((isNaN(monthValue) == true) || (isNaN(dayValue) == true) || (isNaN(yearValue) == true)) {
                    alert('Non-numeric entry detected\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                if (((yearValue % 4) == 0) && (((yearValue % 100) != 0) || ((yearValue % 400) == 0)))
                    monthDays[1] = 29;
                else
                    monthDays[1] = 28;

                if ((monthValue < 1) || (monthValue > 12)) {
                    alert('Invalid month entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthDaysArrayIndex = monthValue - 1;
                if ((dayValue < 1) || (dayValue > monthDays[monthDaysArrayIndex])) {
                    alert('Invalid day entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12" style="font-size:14px;display:none;">
                Records submitted for SPF clearance shall be locked and cannot be edited. To make any changes to the record, please email to Jordan Soh at <a href="mailto:jordansoh@corpit.com.sg">jordansoh@corpit.com.sg</a>.
                <br />
                To submit, photos, please click on upload photo.
                <br /><br />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-5">&nbsp;</div>
            <div class="col-md-2 col-xs-3" style="padding-right:30px;">
                <asp:Button runat="server" ID="btnNext" Visible="true" CssClass="btn MainButton pull-right" Text="Add New Registration" OnClick="btnNext_Click" CausesValidation="false"/>
            </div>
            <div class="col-md-1">
                <asp:Button runat="server" ID="btnCheckout" Visible="false" CssClass="btn MainButton pull-right" Text="Submit" OnClick="btnCheckout_Click" CausesValidation="false"/>
            </div>
        </div>
    </div>

    <div id="divList" runat="server">
        <div style="overflow-x:auto;overflow-y:hidden;" class="table-responsive">
            <asp:TextBox ID="txtFlowName" runat="server" Visible="false"></asp:TextBox>
            <table style="width:100%;" class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>

                        <th id="trRegno" runat="server" scope="row">
                            <asp:Label ID="lbl_Regno" runat="server" Text="Registration ID"></asp:Label>
                        </th>

                        <%--<th id="trSPFStatus" runat="server" scope="row">
                            <asp:Label ID="lbl_SPFStatus" runat="server" Text="SPF Status"></asp:Label>
                        </th>--%>

                        <th id="trPassno" runat="server" scope="row">
                            <asp:Label ID="lbl_PassNo" runat="server" Text="NRIC/Passport No."></asp:Label>
                        </th>
                        <th id="trSalutation" runat="server" scope="row">
                            <asp:Label ID="lbl_Salutation" runat="server" Text="Title"></asp:Label>
                        </th>
                        <th id="trFName" runat="server" scope="row">
                            <asp:Label ID="lbl_FName" runat="server" Text="First Name"></asp:Label>
                        </th>
                        <th id="trLName" runat="server" scope="row">
                            <asp:Label ID="lbl_LName" runat="server" Text="Surname"></asp:Label>
                        </th>
                        <th id="trOName" runat="server" scope="row">
                            <asp:Label ID="lbl_OName" runat="server" Text="Other Name"></asp:Label>
                        </th>
                        <th id="trProfession" runat="server" scope="row">
                            <asp:Label ID="lbl_Profession" runat="server" Text="Profession"></asp:Label>
                        </th>
                        <th id="trOrg" runat="server" scope="row">
                            <asp:Label ID="lbl_Organization" runat="server" Text="Organization"></asp:Label>
                        </th>
                        <th id="trDOB" runat="server" scope="row">
                            <asp:Label ID="lbl_DOB" runat="server" Text="DOB"></asp:Label>
                        </th>
                        <th id="trGender" runat="server" scope="row">
                            <asp:Label ID="lbl_Gender" runat="server" Text="Gender"></asp:Label>
                        </th>
                        <th id="trIsReg" runat="server" scope="row">
                            <asp:Label ID="lbl_IsReg" runat="server" Text="Are you a Singapore registered doctor/nurse/pharmacist?"></asp:Label>
                        </th>
                        <th id="trRegSpecific" runat="server" scope="row">
                            <asp:Label ID="lbl_RegSpecific" runat="server" Text="MCR/SNB/PRN"></asp:Label>
                        </th>
                        <th id="trIDNo" runat="server" scope="row">
                            <asp:Label ID="lbl_IDNo" runat="server" Text="MCR/SNB/PRN No."></asp:Label>
                        </th>
                        <th id="trInstitution" runat="server" scope="row">
                            <asp:Label ID="lbl_Institution" runat="server" Text="Institution"></asp:Label>
                        </th>
                        <th id="trDept" runat="server" scope="row">
                            <asp:Label ID="lbl_Department" runat="server" Text="Department"></asp:Label>
                        </th>
                        <th id="trAddress1" runat="server" scope="row">
                            <asp:Label ID="lbl_Address1" runat="server" Text="Address1"></asp:Label>
                        </th>
                        <th id="trAddress2" runat="server" scope="row">
                            <asp:Label ID="lbl_Address2" runat="server" Text="Address2"></asp:Label>
                        </th>
                        <th id="trAddress3" runat="server" scope="row">
                            <asp:Label ID="lbl_Address3" runat="server" Text="Address3"></asp:Label>
                        </th>
                        <th id="trDesignation" runat="server" scope="row">
                            <asp:Label ID="lbl_Designation" runat="server" Text="Designation"></asp:Label>
                        </th>
                        <th id="trAddress4" runat="server" scope="row">
                            <asp:Label ID="lbl_Address4" runat="server" Text="Address4"></asp:Label>
                        </th>
                        <th id="trCity" runat="server" scope="row">
                            <asp:Label ID="lbl_City" runat="server" Text="City"></asp:Label>
                        </th>
                        <th id="trState" runat="server" scope="row">
                            <asp:Label ID="lbl_State" runat="server" Text="State"></asp:Label>
                        </th>
                        <th id="trPostal" runat="server" scope="row">
                            <asp:Label ID="lbl_Postalcode" runat="server" Text="Postal Code"></asp:Label>
                        </th>
                        <th id="trCountry" runat="server" scope="row">
                            <asp:Label ID="lbl_Country" runat="server" Text="Country"></asp:Label>
                        </th>
                        <th id="trRCountry" runat="server" scope="row">
                            <asp:Label ID="lbl_RCountry" runat="server" Text="RCountry"></asp:Label>
                        </th>
                        <th id="trTel" runat="server" scope="row">
                            <asp:Label ID="lbl_Tel" runat="server" Text="Telephone"></asp:Label>
                        </th>
                        <th id="trMobile" runat="server" scope="row">
                            <asp:Label ID="lbl_Mobile" runat="server" Text="Mobile"></asp:Label>
                        </th>
                        <th id="trFax" runat="server" scope="row">
                            <asp:Label ID="lbl_Fax" runat="server" Text="Fax"></asp:Label>
                        </th>
                        <th id="trEmail" runat="server" scope="row">
                            <asp:Label ID="lbl_Email" runat="server" Text="Email"></asp:Label>
                        </th>
                            <th id="trAffiliation" runat="server" scope="row">
                            <asp:Label ID="lbl_Affiliation" runat="server" Text="Affiliation"></asp:Label>
                        </th>
                        <th id="trDietary" runat="server" scope="row">
                            <asp:Label ID="lbl_Dietary" runat="server" Text="Dietary"></asp:Label>
                        </th>
                        <th id="trNationality" runat="server" scope="row">
                            <asp:Label ID="lbl_Nationality" runat="server" Text="Nationality"></asp:Label>
                        </th>
                        <th id="trAge" runat="server" scope="row">
                            <asp:Label ID="lbl_Age" runat="server" Text="Age"></asp:Label>
                        </th>
                        <th id="trMembershipNo" runat="server" scope="row">
                            <asp:Label ID="lbl_MembershipNo" runat="server" Text="MembershipNo"></asp:Label>
                        </th>
                        <th id="trAdditional4" runat="server" scope="row">
                            <asp:Label ID="lbl_Additional4" runat="server" Text="Additional4"></asp:Label>
                        </th>
                        <th id="trAdditional5" runat="server" scope="row">
                            <asp:Label ID="lbl_Additional5" runat="server" Text="Additional5"></asp:Label>
                        </th>

                        <th id="trVName" runat="server" scope="row">
                            <asp:Label ID="lbl_VName" runat="server" Text="Visitor Name"></asp:Label>
                        </th>
                        <th id="trVDOB" runat="server" scope="row">
                            <asp:Label ID="lbl_VDOB" runat="server" Text="Visitor DOB"></asp:Label>
                        </th>
                        <th id="trVPass" runat="server" scope="row">
                            <asp:Label ID="lbl_VPassNo" runat="server" Text="Visitor Passport No."></asp:Label>
                        </th>
                        <th id="trVPassIssueDate" runat="server" scope="row">
                            <asp:Label ID="lbl_VPassIssueDate" runat="server" Text="Visitor Passport Issue Date"></asp:Label>
                        </th>
                        <th id="trVPassExpiry" runat="server" scope="row">
                            <asp:Label ID="lbl_VPassExpiry" runat="server" Text="Visitor Passport Expiry"></asp:Label>
                        </th>
                        <th id="trVEmbarkation" runat="server" scope="row">
                            <asp:Label ID="lbl_VEmbarkation" runat="server" Text="Visitor Port of Embarkation"></asp:Label>
                        </th>
                        <th id="trVArrivalDate" runat="server" scope="row">
                            <asp:Label ID="lbl_VArrivalDate" runat="server" Text="Visitor Arrival Date"></asp:Label>
                        </th>
                        <th id="trVCountry" runat="server" scope="row">
                            <asp:Label ID="lbl_VCountry" runat="server" Text="Visitor Country"></asp:Label>
                        </th>

                        <th id="trUDF_CName" runat="server" scope="row">
                            <asp:Label ID="lbl_UDFCName" runat="server" Text="UDFC Name"></asp:Label>
                        </th>
                        <th id="trUDF_DelegateType" runat="server" scope="row">
                            <asp:Label ID="lbl_UDFDelType" runat="server" Text="UDF Delegate Type"></asp:Label>
                        </th>
                        <th id="trUDF_ProfCategory" runat="server" scope="row">
                            <asp:Label ID="lbl_UDFProCategory" runat="server" Text="UDF Prof Category"></asp:Label>
                        </th>
                        <th id="trUDF_CPcode" runat="server" scope="row">
                            <asp:Label ID="lbl_UDFCpostalcode" runat="server" Text="UDFC Postal Code"></asp:Label>
                        </th>
                        <th id="trUDF_CLDepartment" runat="server" scope="row">
                            <asp:Label ID="lbl_UDFCLDept" runat="server" Text="UDFCL Department"></asp:Label>
                        </th>
                        <th id="trUDF_CAddress" runat="server" scope="row">
                            <asp:Label ID="lbl_UDFAddress" runat="server" Text="UDFC Address"></asp:Label>
                        </th>
                        <th id="trUDF_CLCompany" runat="server" scope="row">
                            <asp:Label ID="lbl_UDFCLCom" runat="server" Text="UDFCL Company"></asp:Label>
                        </th>
                        <th id="trUDF_CCountry" runat="server" scope="row">
                            <asp:Label ID="lbl_UDFCCountry" runat="server" Text="UDFC Country"></asp:Label>
                        </th>

                        <th id="trSupName" runat="server" scope="row">
                            <asp:Label ID="lbl_SupName" runat="server" Text="Supervisor Name"></asp:Label>
                        </th>
                        <th id="trSupDesignation" runat="server" scope="row">
                            <asp:Label ID="lbl_SupDesignation" runat="server" Text="Supervisor Designation"></asp:Label>
                        </th>
                        <th id="trSupContact" runat="server" scope="row">
                            <asp:Label ID="lbl_SupContact" runat="server" Text="Supervisor Contact"></asp:Label>
                        </th>
                        <th id="trSupEmail" runat="server" scope="row">
                            <asp:Label ID="lbl_SupEmail" runat="server" Text="Supervisor Email"></asp:Label>
                        </th>
                        <th id="trEdit" runat="server" scope="row" visible="true">Edit</th>
                    </tr>
                </thead>

                <tbody>
                    <asp:Repeater ID="rptItem" runat="server" OnItemDataBound="rptitemdatabound"  OnItemCommand="rptitemcommand">
                        <ItemTemplate>
                            <tr>
                                <td><%#Container.ItemIndex+1 %></td>

                                <td id="tdRegno" runat="server"><asp:Label ID="_lblRegno" runat="server" Text='<%#Eval("Regno")%>'></asp:Label></td>

                                <%--<td id="tdSPFStatus" runat="server"><asp:Label ID="_lblSPFStatus" runat="server" Text='<%#Eval("reg_approveStatus").ToString() == "11" ? "Approve" : (Eval("reg_approveStatus").ToString() == "66" ? "Reject" : "Pending")%>'></asp:Label></td>--%>

                                <td id="tdPassno" runat="server"><asp:Label ID="_lblPassno" runat="server" Text='<%#Eval("reg_PassNo")%>'></asp:Label></td>

                                <td id="tdSalutation" runat="server"><asp:Label ID="_lblsal" runat="server" Text='<%# bindSalutation(Eval("reg_Salutation").ToString(), Eval("reg_SalutationOthers").ToString()) %>'></asp:Label></td>

                                <td id="tdFName" runat="server"><asp:Label ID="_lblFName" runat="server" Text='<%#Eval("reg_FName")%>'></asp:Label></td>

                                <td id="tdLName" runat="server"><asp:Label ID="_lblLName" runat="server" Text='<%#Eval("reg_LName")%>'></asp:Label></td>
                                    
                                <td id="tdOName" runat="server"><asp:Label ID="_lblOName" runat="server" Text='<%#Eval("reg_OName")%>'></asp:Label></td>

                                <td id="tdProfession" runat="server"><asp:Label ID="_lblProfession" runat="server" Text='<%# bindProfession(Eval("reg_Profession").ToString())%>'></asp:Label></td>

                                <td id="tdOrg" runat="server"><asp:Label ID="_lblOrg" runat="server" Text='<%# bindOrganisation(Eval("reg_Organization").ToString())%>'></asp:Label></td>

                                <td id="tdDOB" runat="server"><asp:Label ID="_DOB" runat="server" Text='<%#convertDateOfBirth(Eval("reg_DOB").ToString())%>'></asp:Label></td>

                                <td id="tdGender" runat="server"><asp:Label ID="_lblGender" runat="server" Text='<%#Eval("reg_Gender")%>'></asp:Label></td>

                                <td id="tdIsReg" runat="server"><asp:Label ID="_lblIsReg" runat="server" Text='<%#Eval("reg_isReg") != null ? (Eval("reg_isReg").ToString() == "1" ? "Yes" : "No") : "No"%>'></asp:Label></td>

                                <td id="tdRegSpecific" runat="server"><asp:Label ID="_lblRegSpecific" runat="server" Text='<%#Eval("reg_sgregistered")%>'></asp:Label></td>

                                <td id="tdIDNo" runat="server"><asp:Label ID="_lblIDNo" runat="server" Text='<%#Eval("reg_IDno")%>'></asp:Label></td>

                                <td id="tdInstitution" runat="server"><asp:Label ID="_lblInstitution" runat="server" Text='<%# bindInstitution(Eval("reg_Institution").ToString())%>'></asp:Label></td>

                                <td id="tdDept" runat="server"><asp:Label ID="_lblDept" runat="server" Text='<%# bindDepartment(Eval("reg_Department").ToString())%>'></asp:Label></td>

                                <td id="tdAddress1" runat="server"><asp:Label ID="_lblAddress1" runat="server" Text='<%#Eval("reg_Address1")%>'></asp:Label></td>

                                <td id="tdAddress2" runat="server"><asp:Label ID="_lblAddress2" runat="server" Text='<%#Eval("reg_Address2")%>'></asp:Label></td>

                                <td id="tdAddress3" runat="server"><asp:Label ID="_lblAddress3" runat="server" Text='<%#Eval("reg_Address3")%>'></asp:Label></td>

                                <td id="tdDesignation" runat="server"><asp:Label ID="_lblDesignation" runat="server" Text='<%#Eval("reg_Designation")%>'></asp:Label></td>

                                <td id="tdAddress4" runat="server"><asp:Label ID="_lblAddress4" runat="server" Text='<%#Eval("reg_Address4")%>'></asp:Label></td>

                                <td id="tdCity" runat="server"><asp:Label ID="_lblCity" runat="server" Text='<%#Eval("reg_City")%>'></asp:Label></td>

                                <td id="tdState" runat="server"><asp:Label ID="_lblState" runat="server" Text='<%#Eval("reg_State")%>'></asp:Label></td>

                                <td id="tdPostal" runat="server"><asp:Label ID="_lblPostal" runat="server" Text='<%#Eval("reg_PostalCode")%>'></asp:Label></td>

                                <td id="tdCountry" runat="server"><asp:Label ID="_lblCountry" runat="server" Text='<%# bindCountry(Eval("reg_Country").ToString())%>'></asp:Label></td>

                                <td id="tdRCountry" runat="server"><asp:Label ID="_lblRCountry" runat="server" Text='<%# bindCountry(Eval("reg_RCountry").ToString())%>'></asp:Label></td>

                                <td id="tdTel" runat="server"><asp:Label ID="_lblTel" runat="server"><%# bindPhoneNo(Eval("reg_Telcc").ToString(), Eval("reg_Telac").ToString(), Eval("reg_Tel").ToString(), "Tel")%></asp:Label></td>

                                <td id="tdMobile" runat="server"><asp:Label ID="_lblMobile" runat="server"><%# bindPhoneNo(Eval("reg_Mobcc").ToString(), Eval("reg_Mobac").ToString(), Eval("reg_Mobile").ToString(), "Mob")%></asp:Label></td>

                                <td id="tdFax" runat="server"><asp:Label ID="_lblFax" runat="server"><%# bindPhoneNo(Eval("reg_Faxcc").ToString(), Eval("reg_Faxac").ToString(), Eval("reg_Fax").ToString(), "Fax")%></asp:Label></td>

                                <td id="tdEmail" runat="server"><asp:Label ID="_lblEmail" runat="server" Text='<%#Eval("reg_Email")%>'></asp:Label></td>

                                <td id="tdAffiliation" runat="server"><asp:Label ID="_lblAffiliation" runat="server" Text='<%# bindAffiliation(Eval("reg_Affiliation").ToString())%>'></asp:Label></td>

                                <td id="tdDietary" runat="server"><asp:Label ID="_lblDietary" runat="server" Text='<%# bindDietary(Eval("reg_Dietary").ToString())%>'></asp:Label></td>

                                <td id="tdNationality" runat="server"><asp:Label ID="_lblNationality" runat="server" Text='<%#Eval("reg_Nationality")%>'></asp:Label></td>

                                <td id="tdAge" runat="server"><asp:Label ID="_lblAge" runat="server" Text='<%#Eval("reg_Age")%>'></asp:Label></td>

                                <td id="tdMembershipNo" runat="server"><asp:Label ID="_lblMembershipNo" runat="server" Text='<%#Eval("reg_Membershipno")%>'></asp:Label></td>

                                <td id="tdAdditional4" runat="server"><asp:Label ID="_lblAdditional4" runat="server" Text='<%#Eval("reg_Additional4")%>'></asp:Label></td>

                                <td id="tdAdditional5" runat="server"><asp:Label ID="_lblAdditional5" runat="server" Text='<%#Eval("reg_Additional5")%>'></asp:Label></td>

                                <td id="tdVName" runat="server"><asp:Label ID="_lblVName" runat="server" Text='<%#Eval("reg_vName")%>'></asp:Label></td>

                                <td id="tdVDOB" runat="server"><asp:Label ID="_lblVDOB" runat="server" Text='<%#Eval("reg_vDOB")%>'></asp:Label></td>

                                <td id="tdVPass" runat="server"><asp:Label ID="_lblVPass" runat="server" Text='<%#Eval("reg_vPassno")%>'></asp:Label></td>

                                <td id="tdVPassIssueDate" runat="server"><asp:Label ID="_lblVPassIssueDate" runat="server" Text='<%#Eval("reg_vIssueDate")%>'></asp:Label></td>

                                <td id="tdVPassExpiry" runat="server"><asp:Label ID="_lblVPassExpiry" runat="server" Text='<%#Eval("reg_vPassexpiry")%>'></asp:Label></td>

                                <td id="tdVEmbarkation" runat="server"><asp:Label ID="_lblVEmbarkation" runat="server" Text='<%#Eval("reg_vEmbarkation")%>'></asp:Label></td>

                                <td id="tdVArrivalDate" runat="server"><asp:Label ID="_lblVArrivalDate" runat="server" Text='<%#Eval("reg_vArrivalDate")%>'></asp:Label></td>

                                <td id="tdVCountry" runat="server"><asp:Label ID="_lblVCountry" runat="server" Text='<%# bindCountry(Eval("reg_vCountry").ToString())%>'></asp:Label></td>

                                <td id="tdUDF_CName" runat="server"><asp:Label ID="_lblUDF_CName" runat="server" Text='<%#Eval("UDF_CName")%>'></asp:Label></td>

                                <td id="tdUDF_DelegateType" runat="server"><asp:Label ID="_lblUDF_DelegateType" runat="server" Text='<%#Eval("UDF_DelegateType")%>'></asp:Label></td>

                                <td id="tdUDF_ProfCategory" runat="server"><asp:Label ID="_lblUDF_ProfCategory" runat="server"><%#Eval("UDF_ProfCategory")%> <%#Eval("UDF_ProfCategoryOther")%></asp:Label></td>

                                <td id="tdUDF_CPcode" runat="server"><asp:Label ID="_lblUDF_CPcode" runat="server" Text='<%#Eval("UDF_CPcode")%>'></asp:Label></td>

                                <td id="tdUDF_CLDepartment" runat="server"><asp:Label ID="_lblUDF_CLDepartment" runat="server" Text='<%#Eval("UDF_CLDepartment")%>'></asp:Label></td>

                                <td id="tdUDF_CAddress" runat="server"><asp:Label ID="_lblUDF_CAddress" runat="server" Text='<%#Eval("UDF_CAddress")%>'></asp:Label></td>

                                <td id="tdUDF_CLCompany" runat="server"><asp:Label ID="_lblUDF_CLCompany" runat="server"><%#Eval("UDF_CLCompany")%><%#Eval("UDF_CLCompanyOther")%></asp:Label></td>

                                <td id="tdUDF_CCountry" runat="server"><asp:Label ID="_lblUDF_CCountry" runat="server" Text='<%# bindCountry(Eval("UDF_CCountry").ToString())%>'></asp:Label></td>

                                <td id="tdSupName" runat="server"><asp:Label ID="_lblSupName" runat="server" Text='<%#Eval("reg_SupervisorName")%>'></asp:Label></td>

                                <td id="tdSupDesignation" runat="server"><asp:Label ID="_lblSupDesignation" runat="server" Text='<%#Eval("reg_SupervisorDesignation")%>'></asp:Label></td>

                                <td id="tdSupContact" runat="server"><asp:Label ID="_lblSupContact" runat="server" Text='<%#Eval("reg_SupervisorContact")%>'></asp:Label></td>

                                <td id="tdSupEmail" runat="server"><asp:Label ID="_lblSupEmail" runat="server" Text='<%#Eval("reg_SupervisorEmail")%>'></asp:Label></td>

                                <td runat="server" id="tdedit" visible="true">
                                    <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RegNo")%>' 
                                        CommandName="edit" CssClass="btn MainButton" Visible='<%#EditButtonDisable(Eval("Regno").ToString())%>'>
                                        <span class="glyphicon glyphicon-edit"></span>Edit Info</asp:LinkButton>
                                    <asp:LinkButton ID="lbUploadPhoto" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RegNo")%>'
                                        CommandName="uploadphoto" CssClass="btn MainButton" Visible='<%#UploadButtonVisible(Eval("Regno").ToString())%>'>
                                        <span class="glyphicon glyphicon-edit"></span>Upload Photo</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
    <br />
    <br />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" Visible="false">
        <ContentTemplate>
            <asp:TextBox ID="txtCategoryID" runat="server" Visible="false"></asp:TextBox>
            <div id="divReg" runat="server"  class="form-group">

                <div class="form-group row" runat="server" id="divPassNo">
                    <div class="col-md-3 rowLabel">
                        <asp:Label ID="lblPassNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtPassNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcPassNo" runat="server"
                        ControlToValidate="txtPassNo" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revPassNo" runat="server" Enabled="false"
                            ControlToValidate="txtPassNo" ForeColor="Red" 
                            ErrorMessage="*Invalid NRIC" ValidationExpression="^([a-zA-Z]\d{7}[a-zA-Z])$" />
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divSalutation">
                    <div class="col-md-3">
                        <asp:Label ID="lblSalutation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlSalutation" runat="server" CssClass="form-control"
                            OnSelectedIndexChanged="ddlSalutation_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcSal" runat="server" 
                        ControlToValidate="ddlSalutation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divSalOther" visible="false">
                    <div class="col-md-3">
                        &nbsp;
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtSalOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcSalOther" runat="server"
                        ControlToValidate="txtSalOther" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divFName">
                    <div class="col-md-3">
                        <asp:Label ID="lblFName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcFName" runat="server"
                        ControlToValidate="txtFName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divLName">
                    <div class="col-md-3">
                        <asp:Label ID="lblLName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtLName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcLName" runat="server"
                        ControlToValidate="txtLName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divOName">
                    <div class="col-md-3">
                        <asp:Label ID="lblOName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtOName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcOName" runat="server"
                        ControlToValidate="txtOName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divIsReg">
                    <div class="col-md-3">
                        <asp:Label ID="lblIsReg" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:RadioButtonList ID="rbreg" runat="server" Width="100px" RepeatDirection="Horizontal" TextAlign="Right" AutoPostBack="true" OnSelectedIndexChanged="rbreg_SelectedIndexChanged">
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                        <div runat="server" id="divRegSpecific" visible="false">
                            <asp:RadioButtonList ID="rbregspecific" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="150px">
                                <asp:ListItem Value="MCR" Text="MCR" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="SNB" Text="SNB"></asp:ListItem>
                                <asp:ListItem Value="PRN" Text="PRN"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divIDNo">
                    <div class="col-md-3">
                        <asp:Label ID="lblIDNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtIDNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcIDNo" runat="server"
                        ControlToValidate="txtIDNo" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <!-- Job Title for Studnet & Allied Health -->
                <div class="form-group row" runat="server" id="divJobtitle">
                    <div class="col-md-3">
                        <asp:Label ID="lblJobtitle" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtJobtitle" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcJobtitle" runat="server"
                        ControlToValidate="txtJobtitle" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divProfession">
                    <div class="col-md-3">
                        <asp:Label ID="lblProfession" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                       <asp:DropDownList runat="server" ID="ddlProfession" AutoPostBack="true" CssClass="form-control"
                            OnSelectedIndexChanged="ddlProfession_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcProfession" runat="server" 
                        ControlToValidate="ddlProfession" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divProOther" visible="false">
                    <div class="col-md-3">
                        &nbsp;
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtProOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcProOther" runat="server"
                        ControlToValidate="txtProOther" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDoctor" visible="false">
                    <div class="col-md-3">
                        <asp:Label ID="lblDoctor" runat="server" CssClass="form-control-label" Text="Are you a MOHOS/Resident?"></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:RadioButtonList ID="rbDoctor" runat="server" Width="100px" RepeatDirection="Horizontal" TextAlign="Right">
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divStudentUpload">
                    <div class="col-md-3">
                        <asp:Label ID="lblStudentUpload" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:FileUpload runat="server" ID="fupStudentUpload" CssClass="style4" 
                            style="font-family: DINPro-Regular" />
                        <br />
                        <asp:Image runat="server" ID="imgStudentUpload" Width="280px" Height="150px" Visible="false" ImageUrl="#" />
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToValidate="ddlStudentType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divStudentType">
                    <div class="col-md-3">
                        <asp:Label ID="lblStudentType" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                       <asp:DropDownList runat="server" ID="ddlStudentType" AutoPostBack="true" CssClass="form-control"
                            OnSelectedIndexChanged="ddlStudentType_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcStudentType" runat="server" 
                        ControlToValidate="ddlStudentType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divStudentOther" visible="false">
                    <div class="col-md-3">
                        &nbsp;
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtStudentOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcStudentOther" runat="server"
                        ControlToValidate="txtStudentOther" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divOrganization">
                    <div class="col-md-3">
                        <asp:Label ID="lblOrganization" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlOrganization" runat="server" CssClass="form-control"
                            OnSelectedIndexChanged="ddlOrganization_SelectedIndexChanged" AutoPostBack="true">
                            <%--<asp:ListItem Value="0">Please Select</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcOrg" runat="server" 
                        ControlToValidate="ddlOrganization" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divOrgOther" visible="false">
                    <div class="col-md-3">
                        &nbsp;
                    </div>
                    <div class="col-md-4">
                      <asp:TextBox ID="txtOrgOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcOrgOther" runat="server"
                        ControlToValidate="txtOrgOther" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDOB">
                    <div class="col-md-3 rowLabel">
                        <asp:Label ID="lblDOB" runat="server" CssClass="form-control-label" Text="Date of Birth"></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy" AutoPostBack="true"></asp:TextBox><%-- onblur="validateDate(this);"--%>
                        <asp:CalendarExtender ID="calendarDOB" Enabled="True" TargetControlID="txtDOB"
                                runat="server" PopupButtonID="txtDOB" Format="dd/MM/yyyy" ></asp:CalendarExtender>
                        <%--<asp:FilteredTextBoxExtender ID="ftDOB" runat="server"
                                TargetControlID="txtDOB"
                                FilterType="Numbers"
                                ValidChars="0123456789" />--%>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcDOB" runat="server" Enabled="false"
                        ControlToValidate="txtDOB" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <%--<asp:RegularExpressionValidator ID="regexpName" runat="server"
                                ErrorMessage="This expression does not validate." 
                                ControlToValidate="txtDOB"
                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$" />--%>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divGender">
                    <div class="col-md-3 rowLabel">
                        <asp:Label ID="lblGender" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Please select" Value="Please select"></asp:ListItem>
                            <asp:ListItem Text="M" Value="M"></asp:ListItem>
                            <asp:ListItem Text="F" Value="F"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcGender" runat="server" 
                            ControlToValidate="ddlGender" ValueToCompare="Please select" Operator="NotEqual" Type="String" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divInstitution">
                    <div class="col-md-3">
                        <asp:Label ID="lblInstitution" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlInstitution" runat="server" CssClass="form-control"
                            OnSelectedIndexChanged="ddlInstitution_SelectedIndexChanged" AutoPostBack="true">
                            <%--<asp:ListItem Value="0">Please Select</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcInsti" runat="server" 
                        ControlToValidate="ddlInstitution" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divInstiOther" visible="false">
                    <div class="col-md-3">
                        &nbsp;
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtInstiOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcInstiOther" runat="server"
                        ControlToValidate="txtInstiOther" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDepartment">
                    <div class="col-md-3">
                        <asp:Label ID="lblDepartment" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList runat="server" ID="ddlDepartment" AutoPostBack="true" CssClass="form-control"
                            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                            <%--<asp:ListItem Value="0">Please Select</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcDeptm" runat="server" 
                        ControlToValidate="ddlDepartment" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDepartmentOther" visible="false">
                    <div class="col-md-3">
                        &nbsp;
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtDepartmentOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcDeptmOther" runat="server"
                        ControlToValidate="txtDepartmentOther" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress1">
                    <div class="col-md-3">
                        <asp:Label ID="lblAddress1" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAddress1" runat="server"
                        ControlToValidate="txtAddress1" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress2">
                    <div class="col-md-3">
                        <asp:Label ID="lblAddress2" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAddress2" runat="server"
                        ControlToValidate="txtAddress2" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress3">
                    <div class="col-md-3">
                        <asp:Label ID="lblAddress3" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAddress3" runat="server"
                        ControlToValidate="txtAddress3" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDesignation">
                    <div class="col-md-3">
                        <asp:Label ID="lblDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcDesig" runat="server"
                        ControlToValidate="txtDesignation" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress4">
                    <div class="col-md-3">
                        <asp:Label ID="lblAddress4" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtAddress4" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAddress4" runat="server"
                        ControlToValidate="txtAddress4" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divCountry">
                    <div class="col-md-3">
                        <asp:Label ID="lblCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcCountry" runat="server" 
                            ControlToValidate="ddlCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divRCountry">
                    <div class="col-md-3">
                        <asp:Label ID="lblRCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                      <asp:DropDownList ID="ddlRCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlRCountry_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcRCountry" runat="server" 
                            ControlToValidate="ddlRCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divCity">
                    <div class="col-md-3">
                        <asp:Label ID="lblCity" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcCity" runat="server"
                        ControlToValidate="txtCity" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divState">
                    <div class="col-md-3">
                        <asp:Label ID="lblState" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcState" runat="server"
                        ControlToValidate="txtState" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divPostalcode">
                    <div class="col-md-3">
                        <asp:Label ID="lblPostalcode" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtPostalcode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcPostalcode" runat="server"
                        ControlToValidate="txtPostalcode" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divTel">
                    <div class="col-md-3">
                        <asp:Label ID="lblTel" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-1" runat="server" id="divTelcc">
                        <asp:TextBox ID="txtTelcc" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbTelcc" runat="server"
                            TargetControlID="txtTelcc"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-1" runat="server" id="divTelac">
                        <asp:TextBox ID="txtTelac" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbTelac" runat="server"
                            TargetControlID="txtTelac"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtTel" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbTel" runat="server"
                            TargetControlID="txtTel"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcTel" runat="server"
                        ControlToValidate="txtTel" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcTelcc" runat="server"
                        ControlToValidate="txtTelcc" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcTelac" runat="server"
                        ControlToValidate="txtTelac" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divMobile">
                    <div class="col-md-3">
                        <asp:Label ID="lblMobile" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-1" runat="server" id="divMobcc">
                        <asp:TextBox ID="txtMobcc" runat="server" CssClass="form-control"></asp:TextBox>                
                        <asp:FilteredTextBoxExtender ID="ftbMobcc" runat="server"
                            TargetControlID="txtMobcc"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-1" runat="server" id="divMobac">
                        <asp:TextBox ID="txtMobac" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbMobac" runat="server"
                            TargetControlID="txtMobac"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbMobile" runat="server"
                            TargetControlID="txtMobile"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcMob" runat="server"
                        ControlToValidate="txtMobile" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcMobcc" runat="server"
                        ControlToValidate="txtMobcc" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcMobac" runat="server"
                        ControlToValidate="txtMobac" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divFax">
                    <div class="col-md-3">
                        <asp:Label ID="lblFax" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-1" runat="server" id="divFaxcc">
                        <asp:TextBox ID="txtFaxcc" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbFaxcc" runat="server"
                            TargetControlID="txtFaxcc"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-1" runat="server" id="divFaxac">
                        <asp:TextBox ID="txtFaxac" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbFaxac" runat="server"
                            TargetControlID="txtFaxac"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbFax" runat="server"
                            TargetControlID="txtFax"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcFax" runat="server"
                        ControlToValidate="txtFax" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcFaxcc" runat="server"
                        ControlToValidate="txtFaxcc" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcFaxac" runat="server"
                        ControlToValidate="txtFaxac" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divEmail">
                    <div class="col-md-3">
                        <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcEmail" runat="server"
                        ControlToValidate="txtEmail" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-2">
                        <asp:RegularExpressionValidator ID="validateEmail"
                        runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                        ControlToValidate="txtEmail"
                        ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divEmailConfirmation">
                    <div class="col-md-3">
                        <asp:Label ID="lblEmailConfirmation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtEmailConfirmation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>           
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcEConfirm" runat="server"
                        ControlToValidate="txtEmailConfirmation" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="cmpEmail" runat="server" ControlToValidate="txtEmailConfirmation"
                        ControlToCompare="txtEmail" ErrorMessage="Not match." ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAffiliation">
                    <div class="col-md-3">
                        <asp:Label ID="lblAffiliation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlAffiliation" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcAffil" runat="server" 
                            ControlToValidate="ddlAffiliation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDietary">
                    <div class="col-md-3">
                        <asp:Label ID="lblDietary" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlDietary" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcDietary" runat="server" 
                            ControlToValidate="ddlDietary" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divNationality">
                    <div class="col-md-3">
                        <asp:Label ID="lblNationality" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtNationality" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcNation" runat="server"
                        ControlToValidate="txtNationality" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAge">
                    <div class="col-md-3">
                        <asp:Label ID="lblAge" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtAge" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbAge" runat="server"
                            TargetControlID="txtAge"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAge" runat="server"
                        ControlToValidate="txtAge" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divMemberNo">
                    <div class="col-md-3">
                        <asp:Label ID="lblMemberNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtMemberNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcMember" runat="server"
                        ControlToValidate="txtMemberNo" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAdditional4">
                    <div class="col-md-3">
                        <asp:Label ID="lblAdditional4" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtAdditional4" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAdditional4" runat="server"
                        ControlToValidate="txtAdditional4" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAdditional5">
                    <div class="col-md-3">
                        <asp:Label ID="lblAdditional5" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtAdditional5" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAdditional5" runat="server"
                        ControlToValidate="txtAdditional5" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>

            <div id="divVisitor" runat="server" class="form-group" visible="false">
                <div class="form-group row" runat="server" id="divVName">
                    <div class="col-md-3">
                        <asp:Label ID="lblVName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtVName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVName" runat="server"
                        ControlToValidate="txtVName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVDOB">
                    <div class="col-md-3">
                        <asp:Label ID="lblVDOB" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtVDOB" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVDOB" runat="server"
                        ControlToValidate="txtVDOB" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVPassNo">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblVPassNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVPassNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVPassNo" runat="server"
                        ControlToValidate="txtVPassNo" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVPassIssueDate">
                    <div class="col-md-3">
                        <asp:Label ID="lblVPassIssueDate" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtVPassIssueDate" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVPassIssueDate" runat="server" Enabled="false"
                        ControlToValidate="txtVPassIssueDate" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVPassExpiry">
                    <div class="col-md-3">
                        <asp:Label ID="lblVPassExpiry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtVPassExpiry" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVPExpiry" runat="server"
                        ControlToValidate="txtVPassExpiry" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVEmbarkation">
                    <div class="col-md-3">
                        <asp:Label ID="lblVEmbarkation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtVEmbarkation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVEmbarkation" runat="server" Enabled="false"
                        ControlToValidate="txtVEmbarkation" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVArrivalDate">
                    <div class="col-md-3">
                        <asp:Label ID="lblVArrivalDate" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtVArrivalDate" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVArrivalDate" runat="server"
                        ControlToValidate="txtVArrivalDate" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVCountry">
                    <div class="col-md-3">
                        <asp:Label ID="lblVCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlVCountry" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcVCountry" runat="server" 
                            ControlToValidate="ddlVCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>
            </div>

            <div id="divUDF" runat="server" class="form-group" visible="false">
                <div class="form-group row" runat="server" id="divUDFCName">
                    <div class="col-md-3">
                        <asp:Label ID="lblUDFCName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtUDFCName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFCName" runat="server"
                        ControlToValidate="txtUDFCName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFDelType">
                    <div class="col-md-3">
                        <asp:Label ID="lblUDFDelType" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtUDFDelType" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFDelType" runat="server"
                        ControlToValidate="txtUDFDelType" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFProCategory">
                    <div class="col-md-3">
                        <asp:Label ID="lblUDFProCategory" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlUDFProCategory" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUDFProCategory_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcUDFProCat" runat="server"
                            ControlToValidate="ddlUDFProCategory" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>
                
                <div class="form-group row" runat="server" id="divUDFProCatOther" visible="false">
                    <div class="col-md-3">
                        &nbsp;
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtUDFProCatOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcProCatOther" runat="server"
                        ControlToValidate="txtUDFProCatOther" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCpostalcode">
                    <div class="col-md-3">
                        <asp:Label ID="lblUDFCpostalcode" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtUDFCpostalcode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFCpcode" runat="server"
                        ControlToValidate="txtUDFCpostalcode" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCLDept">
                    <div class="col-md-3">
                        <asp:Label ID="lblUDFCLDept" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtUDFCLDept" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFCLDept" runat="server"
                        ControlToValidate="txtUDFCLDept" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFAddress">
                    <div class="col-md-3">
                        <asp:Label ID="lblUDFAddress" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtUDFAddress" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFAddress" runat="server"
                        ControlToValidate="txtUDFAddress" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCLCom">
                    <div class="col-md-3">
                        <asp:Label ID="lblUDFCLCom" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlUDFCLCom" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUDFCLCom_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcUDFCLCom" runat="server" 
                            ControlToValidate="ddlUDFCLCom" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCLComOther" visible="false">
                    <div class="col-md-3">
                        &nbsp;
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtUDFCLComOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFCLComOther" runat="server"
                        ControlToValidate="txtUDFCLComOther" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                 <div class="form-group row" runat="server" id="divUDFCCountry">
                    <div class="col-md-3">
                        <asp:Label ID="lblUDFCCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="ddlUDFCCountry" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcUDFCCountry" runat="server" 
                            ControlToValidate="ddlUDFCCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>
            </div>

            <div id="divSuperVisor" runat="server" class="form-group" visible="false">
                <div class="form-group row" runat="server" id="divSupName">
                    <div class="col-md-3">
                        <asp:Label ID="lblSupName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtSupName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcSupName" runat="server"
                        ControlToValidate="txtSupName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divSupDesignation">
                    <div class="col-md-3">
                        <asp:Label ID="lblSupDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtSupDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcSupDes" runat="server"
                        ControlToValidate="txtSupDesignation" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divSupContact">
                    <div class="col-md-3">
                        <asp:Label ID="lblSupContact" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtSupContact" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcSupContact" runat="server"
                        ControlToValidate="txtSupContact" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
                
                <div class="form-group row" runat="server" id="divSupEmail">
                    <div class="col-md-3">
                        <asp:Label ID="lblSupEmail" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtSupEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcSupEmail" runat="server"
                        ControlToValidate="txtSupEmail" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-2">
                        <asp:RegularExpressionValidator ID="validateSupEmail"
                        runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                        ControlToValidate="txtSupEmail"
                        ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

        <div class="form-group row" runat="server" id="divPhotoUpload" visible="false">
            <div class="col-md-3 rowLabel">
                <asp:Label ID="lblPhotoUpload" runat="server" CssClass="form-control-label" Text="Photo upload"></asp:Label>
            </div>
            <div class="col-md-4">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:FileUpload runat="server" ID="fupPhotoUpload" CssClass="style4" 
                            style="font-family: DINPro-Regular" />
                        <br />
                        <asp:HyperLink ID="hpPhotoUpload" runat="server" Visible="false" Target="_blank" NavigateUrl="#"></asp:HyperLink>
                        <asp:Image runat="server" ID="imgPhotoUpload" Width="280px" Height="150px" Visible="false" ImageUrl="#" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="col-md-2">
                <asp:Label ID="lblErrPhotoUpload" runat="server" ForeColor="Red" Text="*Required" Visible="false"></asp:Label>
                <%--<asp:CompareValidator ID="CompareValidator1" runat="server" 
                ControlToValidate="ddlStudentType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>--%>
            </div>
        </div>

        <div class="form-group" id="divSave" runat="server" visible="false">
            <div class="form-group row">
                <div class="col-md-3">
                    &nbsp;
                </div>
                <div class="col-md-4">
                    <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary" Text="Save" OnClick="btnSave_Click" />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hfRegno" runat="server" />
    <br />

</asp:Content>

