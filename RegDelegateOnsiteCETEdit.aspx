﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RegistrationOnsite.master" AutoEventWireup="true" CodeFile="RegDelegateOnsiteCETEdit.aspx.cs" Inherits="RegDelegateOnsiteCETEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .red
        {
            color:red;
        }
        /*.chkFooter
        {
            white-space:nowrap;
        }*/
        .chkFooter label
        {
            font-weight:unset !important;
            padding-left:5px !important;
            vertical-align:top !important;
        }
        .chkFooter input[type="checkbox"]
        {
            margin-top: 2px !important;
        }
        a:hover
        {
            text-decoration:none !important;
        }
        .rowLabel
        {
            padding-top: 7px;
        }
    </style>

    <script type="text/javascript">
        function validateDate(elementRef) {
                var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

                var dateValue = elementRef.value;

                if (dateValue.length != 10) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                // mm/dd/yyyy format... 
                var valueArray = dateValue.split('/');

                if (valueArray.length != 3) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthValue = parseFloat(valueArray[1]);
                var dayValue = parseFloat(valueArray[0]);
                var yearValue = parseFloat(valueArray[2]);

                if ((isNaN(monthValue) == true) || (isNaN(dayValue) == true) || (isNaN(yearValue) == true)) {
                    alert('Non-numeric entry detected\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                if (((yearValue % 4) == 0) && (((yearValue % 100) != 0) || ((yearValue % 400) == 0)))
                    monthDays[1] = 29;
                else
                    monthDays[1] = 28;

                if ((monthValue < 1) || (monthValue > 12)) {
                    alert('Invalid month entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthDaysArrayIndex = monthValue - 1;
                if ((dayValue < 1) || (dayValue > monthDays[monthDaysArrayIndex])) {
                    alert('Invalid day entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

            return true;
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <%--<asp:Label ID="lblAffiliation" runat="server" Visible="false"></asp:Label>--%>
    <div class="form-group row">
        <div class="col-md-3 col-md-offset-1 rowLabel">Full Name<span class="red">*</span></div>
        <div class="col-md-4">
             <asp:TextBox ID="txtOName" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-md-2">
             <asp:RequiredFieldValidator ID="vcOName" runat="server"
                ControlToValidate="txtOName" Display="Dynamic"
                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revOName" runat="server" ControlToValidate="txtOName" ErrorMessage="Not allow."
                ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
        </div>
    </div>
     <div class="form-group row" runat="server" id="divAffiliation">
        <div class="col-md-3 col-md-offset-1 rowLabel">
            <asp:Label ID="lblAffiliation" runat="server" CssClass="form-control-label" Text="Citizenship Status"></asp:Label>
        </div>
        <div class="col-md-4">
            <asp:DropDownList ID="ddlAffiliation" runat="server" CssClass="form-control">
                <asp:ListItem Value="0">Please Select</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="col-md-2">
            <asp:CompareValidator ID="vcAffil" runat="server" 
                ControlToValidate="ddlAffiliation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
        </div>
    </div>
    <div class="form-group row" runat="server" id="divPassNo">
        <div class="col-md-3 col-md-offset-1 rowLabel">
            <asp:Label ID="lblPassNo" runat="server" CssClass="form-control-label" Text="NRIC/FIN/Passport Nbr"></asp:Label><span class="red">*</span>
        </div>
        <div class="col-md-4">
            <asp:TextBox ID="txtPassNo" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-md-2">
            <asp:RequiredFieldValidator ID="vcPassNo" runat="server"
            ControlToValidate="txtPassNo" Display="Dynamic"
            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revPassNo" runat="server" ControlToValidate="txtPassNo" ErrorMessage="Not allow."
            ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form-group row" runat="server" id="divOrganization">
        <div class="col-md-3 col-md-offset-1 rowLabel">
            <asp:Label ID="lblOrganization" runat="server" CssClass="form-control-label" Text="Organisation Type"></asp:Label><span class="red">*</span>
        </div>
        <div class="col-md-4">
            <asp:DropDownList ID="ddlOrganization" runat="server" CssClass="form-control"
                OnSelectedIndexChanged="ddlOrganization_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Value="0">Please Select</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="col-md-2">
            <asp:CompareValidator ID="vcOrg" runat="server" 
            ControlToValidate="ddlOrganization" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
        </div>
    </div>
    <div class="form-group row" runat="server" id="divOrgOther" visible="false">
        <div class="col-md-3 col-md-offset-1 rowLabel">
            &nbsp;
        </div>
        <div class="col-md-4">
            <asp:TextBox ID="txtOrgOther" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-md-2">
            <asp:RequiredFieldValidator ID="vcOrgOther" runat="server"
            ControlToValidate="txtOrgOther" Display="Dynamic"
            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revOrgOther" runat="server" ControlToValidate="txtOrgOther" ErrorMessage="Not allow."
            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form-group row" runat="server" id="divInstitution">
        <div class="col-md-3 col-md-offset-1 rowLabel">
            <asp:Label ID="lblInstitution" runat="server" CssClass="form-control-label" Text="Name of Organisation"></asp:Label><span class="red">*</span>
        </div>
        <div class="col-md-4">
            <asp:DropDownList ID="ddlInstitution" runat="server" CssClass="form-control"
                OnSelectedIndexChanged="ddlInstitution_SelectedIndexChanged" AutoPostBack="true">
                <%--<asp:ListItem Value="0">Please Select</asp:ListItem>--%>
            </asp:DropDownList>
        </div>
        <div class="col-md-2">
            <asp:CompareValidator ID="vcInsti" runat="server" 
            ControlToValidate="ddlInstitution" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
        </div>
    </div>
    <div class="form-group row" runat="server" id="divInstiOther" visible="false">
        <div class="col-md-3 col-md-offset-1 rowLabel">
            <asp:Label ID="lblInstiOther" runat="server" CssClass="form-control-label" Text="Name of Organisation"></asp:Label>
        </div>
        <div class="col-md-4">
            <asp:TextBox ID="txtInstiOther" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-md-2">
            <asp:RequiredFieldValidator ID="vcInstiOther" runat="server"
            ControlToValidate="txtInstiOther" Display="Dynamic"
            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtInstiOther" ErrorMessage="Not allow."
            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form-group row" runat="server" id="divEmail">
        <div class="col-md-3 col-md-offset-1 rowLabel">
            <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text="Email"></asp:Label>
        </div>
        <div class="col-md-4">
            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-md-3">
            <asp:RequiredFieldValidator ID="vcEmail" runat="server"
            ControlToValidate="txtEmail" Display="Dynamic"
            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="validateEmail"
            runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
            ControlToValidate="txtEmail"
            ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
        </div>
    </div>

    <br />
    <br />
    <div class="form-group">
        <div class="form-group container" style="padding-top:20px;">
            <asp:Panel runat="server" ID="PanelWithoutBack" Visible="true" >
                <div class="col-lg-offset-5 col-lg-3 col-sm-offset-4 col-sm-3 center-block" >
                    <asp:Button runat="server" ID="btnSave" CssClass="btn MainButton btn-block" Text="Update" OnClick="btnSave_Click" />
                </div>
            </asp:Panel>
        </div>
    </div>

</asp:Content>

