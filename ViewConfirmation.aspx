﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewConfirmation.aspx.cs" Inherits="ViewConfirmation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="Content/Default/bootstrap.css" rel="stylesheet" />
    <title></title>

    <script>

        function printPDF(PDFtoPrint)
        {
            document.getElementById(PDFtoPrint).focus(); document.getElementById(PDFtoPrint).contentWindow.print();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server"> 
        <div class="container" style="padding-top:30px;">
        <iframe runat="server" id="ShowPDF" src="" width="800" height="800"></iframe>    
            </div>
     
    </form>
</body>
</html>
