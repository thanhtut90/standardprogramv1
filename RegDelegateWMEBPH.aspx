﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RegistrationWMEB19.master" AutoEventWireup="true" CodeFile="RegDelegateWMEBPH.aspx.cs" Inherits="RegDelegateWMEBPH" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="Content/StyleQA.css" />
    <style>
        .red {
            color: red;
        }
        /*.chkFooter
        {
            white-space:nowrap;
        }*/
        .chkFooter label {
            font-weight: unset !important;
            padding-left: 5px !important;
            vertical-align: top !important;
        }

        .chkFooter input[type="checkbox"] {
            margin-top: 2px !important;
        }

        a:hover {
            text-decoration: none !important;
        }

        .rowLabel {
            padding-top: 7px;
        }

        .form-control {
            height: 35px !important;
        }

        .ControlRight {
            padding-right: 0px;
        }

        .textUpperCase {
            text-transform: uppercase;
        }

        .paddingLeft3px {
            padding-left: 3px;
        }

        .PaddingRight1 {
            padding-right: 1px;
        }

        .PaddingLeft1 {
            padding-left: 3px;
        }

        .form-control-label {
            font-weight: bold;
        }

        .table {
            font-size: 13px;
        }

            .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
                padding: 3px !important;
            }

        .title {
            font-size: 14px;
        }
    </style>

    <script type="text/javascript">
        function validateDate(elementRef) {
            var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

            var dateValue = elementRef.value;

            if (dateValue.length != 10) {
                alert('Entry must be in the format dd/mm/yyyy.');

                return false;
            }

            // mm/dd/yyyy format... 
            var valueArray = dateValue.split('/');

            if (valueArray.length != 3) {
                alert('Entry must be in the format dd/mm/yyyy.');

                return false;
            }

            var monthValue = parseFloat(valueArray[1]);
            var dayValue = parseFloat(valueArray[0]);
            var yearValue = parseFloat(valueArray[2]);

            if ((isNaN(monthValue) == true) || (isNaN(dayValue) == true) || (isNaN(yearValue) == true)) {
                alert('Non-numeric entry detected\nEntry must be in the format dd/mm/yyyy.');

                return false;
            }

            if (((yearValue % 4) == 0) && (((yearValue % 100) != 0) || ((yearValue % 400) == 0)))
                monthDays[1] = 29;
            else
                monthDays[1] = 28;

            if ((monthValue < 1) || (monthValue > 12)) {
                alert('Invalid month entered\nEntry must be in the format dd/mm/yyyy.');

                return false;
            }

            var monthDaysArrayIndex = monthValue - 1;
            if ((dayValue < 1) || (dayValue > monthDays[monthDaysArrayIndex])) {
                alert('Invalid day entered\nEntry must be in the format dd/mm/yyyy.');

                return false;
            }

            return true;
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }


        function limitText(limitField, limitNum) {
            var txtValue = limitField.value.replace(/\s/g, '')
            if (txtValue.length < limitNum) {
                //limitField.value = limitField.value.substring(0, limitNum);
                alert("Minimum 2 characters!");
            }
        }
    </script>
    <script type="text/javascript">
        /*
      $(document).ready(function() { 
      $('.flex-grid [data-accordion]').accordion();
      });

      function LoadQAAcordion()
      {
          $(document).ready(function () {
         //     $('.flex-grid [data-accordion]').accordion();
          });
      }
   */
    </script>
    <script type="text/javascript">
        function singlecheckbox(id, chk, checkbox) {

            var chkboxClass = document.querySelectorAll("." + checkbox.className)
            var v1 = checkbox.className;

            var chkboxClass2 = document.querySelectorAll("." + checkbox.className.replace("TB", "T"));
            var v2 = checkbox.className.replace("TB", "T");

            if (v1 == v2) {
                chkboxClass2 = document.querySelectorAll("." + checkbox.className.replace("T", "TB"));
            }

            var arrays = new Array();
            for (var i = 0; i < chkboxClass.length; i++) {
                arrays[i] = chkboxClass[i]

            }

            for (var i = 0; i < chkboxClass2.length; i++) {
                arrays[chkboxClass.length + i] = chkboxClass2[i]
            }
            var checkboxclassId = arrays[0].className;

            //if using masterpage
            var strappend = checkbox.id.substr(0, 24);

            for (var i = 0; i < arrays.length; i++) {
                var chkbox = arrays[i];
                chkbox.checked = false;
                checkbox.checked = chk;
            }
            for (var i = 0; i < arrays.length; i++) {

                if (chkbox.id == "ctl00_ContentPlaceHolder1_QITM1006" || chkbox.id == "ContentPlaceHolder1_QITM1007") {

                    for (var i = 0; i < 75; i++) {
                        did = "ContentPlaceHolder1_div_" + i;
                        if (i < 3) {
                            document.getElementById(did).style.display = "";
                        }
                        else {
                            document.getElementById(did).style.display = "none";
                        }
                    }
                }
            }

            if (id != null && id != "") //ContentPlaceHolder1_
            {
                var n = id.includes("~");
                if (n == true) {
                    var myarray = id.split('~');
                    for (var i = 0; i < myarray.length; i++) {
                        if (myarray[i] != "") {
                            did = "ctl00_ContentPlaceHolder1_div_" + myarray[i];

                            var display = document.getElementById(did).style.display;
                            if (display == "") {
                                document.getElementById(did).style.display = "none";
                            }
                            else {
                                document.getElementById(did).style.display = "";
                            }
                        }
                    }
                }
                else {
                    id = "ctl00_ContentPlaceHolder1_div_" + id;

                    var display = document.getElementById(id).style.display;
                    if (display == "") {
                        document.getElementById(id).style.display = "none";
                    }
                    else {
                        document.getElementById(id).style.display = "";
                    }
                }

            }
        }

        function multipTextbox(id, chk, checkbox) {
            if (id != null && id != "") //ContentPlaceHolder1_
            {
                var n = id.includes("~");
                if (n == true) {
                    var myarray = id.split('~');
                    for (var i = 0; i < myarray.length; i++) {
                        if (myarray[i] != "") {
                            did = "ctl00_ContentPlaceHolder1_div_" + myarray[i];

                            var display = document.getElementById(did).style.display;
                            if (display == "") {
                                document.getElementById(did).style.display = "none";
                            }
                            else {
                                document.getElementById(did).style.display = "";
                            }
                        }
                    }
                }
                else {
                    id = "ctl00_ContentPlaceHolder1_div_" + id;

                    var display = document.getElementById(id).style.display;
                    if (display == "") {
                        document.getElementById(id).style.display = "none";
                    }
                    else {
                        document.getElementById(id).style.display = "";
                    }
                }

            }
        }


    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <script>

                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(function () {
                    /*
                    $('.flex-grid [data-accordion]').accordion();

                    $('.QAPullDown').on('change', function () {
                        QAPulldownOnchange($(this));
                    });

                    $('.QAPullDown').each(function () {
                        QAPulldownOnchange($(this)); // open other thext box when postback
                    });
                    */

                });




                function QAPulldownOnchange(curreDropdown) {

                    // var sText = $(".QAPullDown option:selected").text();
                    var sText = $(curreDropdown).find("option:selected").text();
                    sText = sText.toLowerCase();
                    var substring = "other";
                    if (sText.includes(substring))
                        $(curreDropdown).closest('td').find(".QAPullDownOtherDiv").show();
                    else
                        $(curreDropdown).closest('td').find(".QAPullDownOtherDiv").hide();
                }

            </script>
            <div id="divHDR1" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblHDR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR2" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-11" style="padding-left: 50px;">
                        <asp:Label ID="lblHDR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR3" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblHDR3" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>

            <div id="divReg" runat="server" class="form-group">
                <div class="form-group row" runat="server" id="divPromoCode" visible="false">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblPromoCode" runat="server" CssClass="form-control-label" Text="Promo Code"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtPromoCode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcPromoCode" runat="server" Enabled="false"
                            ControlToValidate="txtPromoCode" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revPromoCode" runat="server" ControlToValidate="txtPromoCode" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s()+|\{}.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <%--<asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="catchanged" Visible="False"></asp:DropDownList>--%>
            <div class="container " style="padding-top: 30px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="form-group col-md-4 " runat="server" id="divSalutation">
                            <div>
                                <asp:Label ID="lblSalutation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                <asp:DropDownList ID="ddlSalutation" runat="server" CssClass="form-control input-sm"
                                    OnSelectedIndexChanged="ddlSalutation_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0">Please Select</asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="vcSal" runat="server"
                                    ControlToValidate="ddlSalutation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            </div>
                            <div runat="server" id="divSalOther" visible="false" style="padding-top: 7px;">

                                <asp:TextBox ID="txtSalOther" runat="server" CssClass="form-control input-sm"
                                    onblur="limitText(this,2);"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vcSalOther" runat="server"
                                    ControlToValidate="txtSalOther" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revSalOther" runat="server" ControlToValidate="txtSalOther" ErrorMessage="Not allow."
                                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="form-group col-md-12" runat="server" id="divFName">
                                <asp:Label ID="lblFName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                <asp:TextBox ID="txtFName" runat="server" CssClass="form-control input-sm textUpperCase cssFName "></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vcFName" runat="server"
                                    ControlToValidate="txtFName" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator> 
                            </div>
                            <div class="form-group col-md-12" runat="server" id="divEmail">
                                <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vcEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="validateEmail" runat="server" ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txtEmail" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="form-group row" runat="server" id="divAffiliation">
                                        <asp:Label ID="lblAffiliation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                        <asp:DropDownList ID="ddlAffiliation" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:CompareValidator ID="vcAffil" runat="server"
                                            ControlToValidate="ddlAffiliation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row" runat="server" id="divOName">
                                        <asp:Label ID="lblOName" runat="server" CssClass="form-control-label" Text="" Visible="false"></asp:Label><br />
                                        <asp:TextBox ID="txtOName" runat="server" CssClass="form-control input-sm textUpperCase cssOName"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="vcOName" runat="server"
                                            ControlToValidate="txtOName" Display="Dynamic"
                                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="form-group row" runat="server" id="divDietary">
                                        <asp:Label ID="lblDietary" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                        <asp:DropDownList ID="ddlDietary" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:CompareValidator ID="vcDietary" runat="server"
                                            ControlToValidate="ddlDietary" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row" runat="server" id="divPassNo">
                                        <asp:Label ID="lblPassNo" runat="server" CssClass="form-control-label" Text="" Visible="false"></asp:Label><br />
                                        <asp:TextBox ID="txtPassNo" runat="server" CssClass="form-control"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="vcPassNo" runat="server"
                                            ControlToValidate="txtPassNo" Display="Dynamic"
                                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator> 

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="row" runat="server" id="divLName">
                        <div class="form-group col-md-6">
                            <div class="form-group col-md-12">
                                <asp:Label ID="lblLName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                <asp:TextBox ID="txtLName" runat="server" CssClass="form-control input-sm textUpperCase cssLName" AutoCompleteType="Disabled"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vcLName" runat="server"
                                    ControlToValidate="txtLName" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator> 
                            </div>
                        </div>
                    </div>
                    <div class="row" id="BarQA">
                        <div class="form-group col-md-6">
                            <div class="form-group col-md-12" runat="server" id="divProfession">
                                <asp:Label ID="lblProfession" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                <asp:DropDownList runat="server" ID="ddlProfession" AutoPostBack="true" CssClass="form-control"
                                    OnSelectedIndexChanged="ddlProfession_SelectedIndexChanged">
                                    <asp:ListItem Value="0">Please Select</asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="vcProfession" runat="server"
                                    ControlToValidate="ddlProfession" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="form-group col-md-12" runat="server" id="divAddress1">
                                <asp:Label ID="lblAddress1" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control input-sm textUpperCase"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vcAddress1" runat="server"
                                    ControlToValidate="txtAddress1" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="form-group col-md-12" runat="server" id="divDepartment">
                                <asp:Label ID="lblDepartment" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                <asp:DropDownList runat="server" ID="ddlDepartment" AutoPostBack="true" CssClass="form-control"
                                    OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                    <%--<asp:ListItem Value="0">Please Select</asp:ListItem>--%>
                                </asp:DropDownList>

                                <asp:CompareValidator ID="vcDeptm" runat="server"
                                    ControlToValidate="ddlDepartment" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="form-group col-md-12">
                                <asp:Label ID="Label1" runat="server" CssClass="form-control-label" Text="Do you have a person in mind?"></asp:Label>     <asp:Label ID="lblcollRequired" runat="server" ForeColor="Red" Text="*Required" Visible="false"></asp:Label>
                            </div>
                            <div class="col-md-7">
                                <asp:CheckBox runat="server" ID="chkYes" Text="Yes- Please share his name and social handle" />
                               
                                <div class="form-group col-md-10" runat="server" id="divAddress3" style="padding-left:0px;">

                                <asp:Label ID="lblAddress3" runat="server" CssClass="form-control-label" Text="" Visible="false"></asp:Label>

                                <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control input-sm" AutoCompleteType="Disabled"></asp:TextBox> <asp:Label ID="txtAddress2CustomValidate" runat="server" ForeColor="Red" Text="*Required" Visible="false"></asp:Label>

                                <asp:RequiredFieldValidator ID="vcAddress3" runat="server"
                                    ControlToValidate="txtAddress3" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator> 

                            </div>
                               
                            </div>
                            <div class="col-md-4">
                                <asp:CheckBox runat="server" ID="chkNO" Text="No- Let us assist you" />
                            </div>
                        </div>
                    </div>

                    <div class="row" style="display: none;">
                        <div class="form-group col-md-6">
                            <div class="form-group col-md-12" runat="server" id="divOrganization">
                                <asp:Label ID="lblOrganization" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                <asp:DropDownList ID="ddlOrganization" runat="server" CssClass="form-control"
                                    OnSelectedIndexChanged="ddlOrganization_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0">Please Select</asp:ListItem>
                                </asp:DropDownList>
                                <asp:CompareValidator ID="vcOrg" runat="server"
                                    ControlToValidate="ddlOrganization" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="display: none;">
                        <div class="form-group col-md-12">

                             <div class="form-group col-md-10" runat="server" id="divAddress2" style="padding-left:0px;">

                                    <asp:Label ID="lblAddress2" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vcAddress2" runat="server"
                                        ControlToValidate="txtAddress2" Display="Dynamic"
                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator> 

                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div runat="server" id="divAddress4" class="form-group col-md-12">

                                <asp:Label ID="lblAddress4" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                <br />
                                <asp:TextBox ID="txtAddress4" runat="server" CssClass="form-control input-sm" TextMode="MultiLine" Rows="20" Style="height: auto !important;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vcAddress4" runat="server"
                                    ControlToValidate="txtAddress4" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                  <asp:Label ID="txtAddress4WordCountMsg" runat="server" ForeColor="Red" Text="*Required" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row" runat="server" id="divStudentUpload">
                        <div class="form-group col-md-12">
                            <div class="col-md-12">
                                <asp:Label ID="lblStudentUpload" runat="server" CssClass="form-control-label" Text="Upload your submission (Maximum 200MB, Submission Format:JPEG,PNG,AVI,MPG,WMV,MP4):"></asp:Label>
                                <asp:FileUpload runat="server" ID="fupStudentUpload" CssClass="style4"
                                    Style="font-family: DINPro-Regular" />
                                <br />
                                <br />
                                <asp:HyperLink ID="hpStudentUpload" runat="server" Visible="false" Target="_blank" NavigateUrl="#"></asp:HyperLink>
                                <asp:Image runat="server" ID="imgStudentUpload" Width="280px" Height="150px" Visible="false" ImageUrl="#" />
                            </div>
                            <div class="col-md-2">
                                <asp:CompareValidator ID="CompareValidator1" runat="server"
                                    ControlToValidate="ddlStudentType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic" Enabled="false"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                            <div class="row" style="padding-top: 16px;">
                                <div class="form-group col-md-6" runat="server" id="divCity">
                                    <asp:Label ID="lblCity" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vcCity" runat="server"
                                        ControlToValidate="txtCity" Display="Dynamic"
                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revCity" runat="server" ControlToValidate="txtCity" ErrorMessage="Not allow."
                                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                                </div>

                                <div class="form-group col-md-6 paddingLeft3px" runat="server" id="divState">
                                    <asp:Label ID="lblState" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                    <asp:TextBox ID="txtState" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vcState" runat="server"
                                        ControlToValidate="txtState" Display="Dynamic"
                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revState" runat="server" ControlToValidate="txtState" ErrorMessage="Not allow."
                                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="row">

                                <div class="form-group col-md-6" runat="server" id="divCountry">

                                    <asp:Label ID="lblCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>

                                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control input-sm" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                                    </asp:DropDownList>

                                    <asp:CompareValidator ID="vcCountry" runat="server"
                                        ControlToValidate="ddlCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                </div>
                                <div class="form-group col-md-6 paddingLeft3px" runat="server" id="divPostalcode">

                                    <asp:Label ID="lblPostalcode" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                    <asp:TextBox ID="txtPostalcode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="vcPostalcode" runat="server"
                                        ControlToValidate="txtPostalcode" Display="Dynamic"
                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revPostalcode" runat="server" ControlToValidate="txtPostalcode" ErrorMessage="Not allow."
                                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row ">

                        <div class="form-group col-md-4" runat="server" id="divDesignation">

                            <asp:Label ID="lblDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                            <asp:TextBox ID="txtDesignation" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="vcDesig" runat="server"
                                ControlToValidate="txtDesignation" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revDesignation" runat="server" ControlToValidate="txtDesignation" ErrorMessage="Not allow."
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="row" runat="server" id="divTel">
                                <asp:Label ID="lblTel" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                                <div class="form-group col-md-2 col-xs-3 PaddingRight1" runat="server" id="divTelcc">
                                    <asp:TextBox ID="txtTelcc" runat="server" CssClass="form-control input-sm" AutoCompleteType="Disabled" placeholder="Country"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbTelcc" runat="server"
                                        TargetControlID="txtTelcc"
                                        FilterType="Numbers"
                                        ValidChars="+0123456789" />
                                    <asp:RequiredFieldValidator ID="vcTelcc" runat="server"
                                        ControlToValidate="txtTelcc" Display="Dynamic"
                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>

                                </div>
                                <div class="form-group col-md-2 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="divTelac">
                                    <asp:TextBox ID="txtTelac" runat="server" CssClass="form-control input-sm" AutoCompleteType="Disabled" placeholder="Area"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbTelac" runat="server"
                                        TargetControlID="txtTelac"
                                        FilterType="Numbers"
                                        ValidChars="+0123456789" />
                                    <asp:RequiredFieldValidator ID="vcTelac" runat="server"
                                        ControlToValidate="txtTelac" Display="Dynamic"
                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                                <div class="form-group col-md-8 col-xs-6 PaddingLeft1" runat="server" id="divTelNo">
                                    <asp:TextBox ID="txtTel" runat="server" CssClass="form-control input-sm" AutoCompleteType="Disabled" placeholder="Telephone Number"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbTel" runat="server"
                                        TargetControlID="txtTel"
                                        FilterType="Numbers"
                                        ValidChars="+0123456789" />
                                    <asp:RequiredFieldValidator ID="vcTel" runat="server"
                                        ControlToValidate="txtTel" Display="Dynamic"
                                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>

                            </div>

                        </div>

                        <div class="form-group col-md-4 col-xs-3 PaddingRight1" runat="server" id="divMobcc">
                            <asp:Label ID="lblMobile" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                            <asp:TextBox ID="txtMobcc" runat="server" CssClass="form-control input-sm" placeholder="Country"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobcc" runat="server"
                                TargetControlID="txtMobcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                            <asp:RequiredFieldValidator ID="vcMob" runat="server"
                                ControlToValidate="txtMobile" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-2 col-xs-3 " runat="server" id="divMobac">
                            <asp:TextBox ID="txtMobac" runat="server" CssClass="form-control input-sm" placeholder="Area Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobac" runat="server"
                                TargetControlID="txtMobac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                            <asp:RequiredFieldValidator ID="vcMobcc" runat="server"
                                ControlToValidate="txtMobcc" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group col-md-8 PaddingLeft1" runat="server" id="divMobile">

                            <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control input-sm" placeholder="Mobile Number"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobile" runat="server"
                                TargetControlID="txtMobile"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                            <asp:RequiredFieldValidator ID="vcMobac" runat="server"
                                ControlToValidate="txtMobac" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <div id="divMobileNo" runat="server"></div>
                        </div>

                    </div>

                    <%-- End Fist --%>
                </div>
                <!-- End Here ------------------------------------------------------------ -->
                <div class="container ">
                    <div class="col-md-8">

                        <div class="row">

                            <div class="form-group col-md-7" runat="server" id="divDepartmentOther" visible="false">

                                <asp:TextBox ID="txtDepartmentOther" runat="server" CssClass="form-control"
                                    onblur="limitText(this,2);"></asp:TextBox><%--onKeyDown="limitText(this,5);" --%>
                                <asp:RequiredFieldValidator ID="vcDeptmOther" runat="server"
                                    ControlToValidate="txtDepartmentOther" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revDepartmentOther" runat="server" ControlToValidate="txtDepartmentOther" ErrorMessage="Not allow."
                                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>

                            </div>



                        </div>
                        <div class="row">


                            <div class="form-group col-md-7" runat="server" id="divOrgOther" visible="false">
                                <asp:TextBox ID="txtOrgOther" runat="server" CssClass="form-control"
                                    onblur="limitText(this,2);"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="vcOrgOther" runat="server"
                                    ControlToValidate="txtOrgOther" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revOrgOther" runat="server" ControlToValidate="txtOrgOther" ErrorMessage="Not allow."
                                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="form-group row" runat="server" id="divIsReg">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblIsReg" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:RadioButtonList ID="rbreg" runat="server" Width="100px" RepeatDirection="Horizontal" TextAlign="Right" AutoPostBack="true" OnSelectedIndexChanged="rbreg_SelectedIndexChanged">
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                        <div runat="server" id="divRegSpecific" visible="false">
                            <asp:RadioButtonList ID="rbregspecific" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="150px">
                                <asp:ListItem Value="MCR" Text="MCR" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="SNB" Text="SNB"></asp:ListItem>
                                <asp:ListItem Value="PRN" Text="PRN"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divIDNo">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblIDNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtIDNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcIDNo" runat="server"
                            ControlToValidate="txtIDNo" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revIDNo" runat="server" ControlToValidate="txtIDNo" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>



                <!-- Job Title for Studnet & Allied Health -->
                <div class="form-group row" runat="server" id="divJobtitle">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblJobtitle" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtJobtitle" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcJobtitle" runat="server"
                            ControlToValidate="txtJobtitle" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revJobtitle" runat="server" ControlToValidate="txtJobtitle" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divProOther" visible="false">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        &nbsp;
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtProOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcProOther" runat="server"
                            ControlToValidate="txtProOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revProOther" runat="server" ControlToValidate="txtProOther" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDoctor" visible="false">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblDoctor" runat="server" CssClass="form-control-label" Text="Are you a MOHOS/Resident?"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:RadioButtonList ID="rbDoctor" runat="server" Width="100px" RepeatDirection="Horizontal" TextAlign="Right">
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>



                <div class="form-group row" runat="server" id="divStudentType">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblStudentType" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList runat="server" ID="ddlStudentType" AutoPostBack="true" CssClass="form-control"
                            OnSelectedIndexChanged="ddlStudentType_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcStudentType" runat="server"
                            ControlToValidate="ddlStudentType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divStudentOther" visible="false">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        &nbsp;
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtStudentOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcStudentOther" runat="server"
                            ControlToValidate="txtStudentOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revStudentOther" runat="server" ControlToValidate="txtStudentOther" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divInstitution">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblInstitution" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlInstitution" runat="server" CssClass="form-control"
                            OnSelectedIndexChanged="ddlInstitution_SelectedIndexChanged" AutoPostBack="true">
                            <%--<asp:ListItem Value="0">Please Select</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcInsti" runat="server"
                            ControlToValidate="ddlInstitution" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divInstiOther" visible="false">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        &nbsp;
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtInstiOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcInstiOther" runat="server"
                            ControlToValidate="txtInstiOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtInstiOther" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>





                <div class="form-group row" runat="server" id="divRCountry">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblRCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlRCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlRCountry_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcRCountry" runat="server"
                            ControlToValidate="ddlRCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>



                <div class="form-group row" runat="server" id="divFax">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblFax" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5" runat="server">
                        <div class="col-xs-3" runat="server" id="divFaxcc" style="padding-left: 0px;">
                            <asp:TextBox ID="txtFaxcc" runat="server" CssClass="form-control" placeholder="Country Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFaxcc" runat="server"
                                TargetControlID="txtFaxcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-3" runat="server" id="divFaxac" style="padding-left: 0px;">
                            <asp:TextBox ID="txtFaxac" runat="server" CssClass="form-control" placeholder="Area Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFaxac" runat="server"
                                TargetControlID="txtFaxac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-6" runat="server" id="divFaxNo" style="padding-right: 0px; padding-left: 0px;">
                            <asp:TextBox ID="txtFax" runat="server" CssClass="form-control" placeholder="Fax Number"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFax" runat="server"
                                TargetControlID="txtFax"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcFax" runat="server"
                            ControlToValidate="txtFax" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcFaxcc" runat="server"
                            ControlToValidate="txtFaxcc" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcFaxac" runat="server"
                            ControlToValidate="txtFaxac" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>



                <div class="form-group row" runat="server" id="divEmailConfirmation">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblEmailConfirmation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtEmailConfirmation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:RequiredFieldValidator ID="vcEConfirm" runat="server"
                            ControlToValidate="txtEmailConfirmation" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cmpEmail" runat="server" ControlToValidate="txtEmailConfirmation"
                            ControlToCompare="txtEmail" ErrorMessage="Not match." ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>




                <div class="form-group row" runat="server" id="divNationality">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblNationality" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtNationality" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcNation" runat="server"
                            ControlToValidate="txtNationality" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revNationality" runat="server" ControlToValidate="txtNationality" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAge">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblAge" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtAge" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbAge" runat="server"
                            TargetControlID="txtAge"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAge" runat="server"
                            ControlToValidate="txtAge" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDOB">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblDOB" runat="server" CssClass="form-control-label" Text="Date of Birth"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox><%-- onblur="validateDate(this);"--%>
                        <asp:CalendarExtender ID="calendarDOB" TargetControlID="txtDOB"
                            runat="server" PopupButtonID="txtDOB" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcDOB" runat="server" Enabled="false"
                            ControlToValidate="txtDOB" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <%--<asp:RegularExpressionValidator ID="regexpName" runat="server"
                                ErrorMessage="This expression does not validate." 
                                ControlToValidate="txtDOB"
                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$" />--%>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divGender">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblGender" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                            <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <%--<asp:CompareValidator ID="vcGender" runat="server" 
                            ControlToValidate="ddlGender" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>--%>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divMemberNo">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblMemberNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtMemberNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcMember" runat="server"
                            ControlToValidate="txtMemberNo" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revMemberNo" runat="server" ControlToValidate="txtMemberNo" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAdditional4">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblAdditional4" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtAdditional4" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAdditional4" runat="server"
                            ControlToValidate="txtAdditional4" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAdditional4" runat="server" ControlToValidate="txtAdditional4" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAdditional5">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblAdditional5" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtAdditional5" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAdditional5" runat="server"
                            ControlToValidate="txtAdditional5" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAdditional5" runat="server" ControlToValidate="txtAdditional5" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>

            <div id="divVisitor" runat="server" class="form-group" visible="false">
                <div class="form-group row" runat="server" id="divVName">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblVName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVName" runat="server"
                            ControlToValidate="txtVName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revVName" runat="server" ControlToValidate="txtVName" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVDOB">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblVDOB" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVDOB" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVDOB" runat="server"
                            ControlToValidate="txtVDOB" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVPassNo">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblVPassNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVPassNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVPassNo" runat="server"
                            ControlToValidate="txtVPassNo" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revVPassNo" runat="server" ControlToValidate="txtVPassNo" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVPassIssueDate">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblVPassIssueDate" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVPassIssueDate" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVPassIssueDate" runat="server" Enabled="false"
                            ControlToValidate="txtVPassIssueDate" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVPassExpiry">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblVPassExpiry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVPassExpiry" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVPExpiry" runat="server"
                            ControlToValidate="txtVPassExpiry" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVEmbarkation">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblVEmbarkation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVEmbarkation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVEmbarkation" runat="server" Enabled="false"
                            ControlToValidate="txtVEmbarkation" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revVEmbarkation" runat="server" ControlToValidate="txtVEmbarkation" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVArrivalDate">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblVArrivalDate" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtVArrivalDate" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcVArrivalDate" runat="server"
                            ControlToValidate="txtVArrivalDate" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVCountry">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblVCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlVCountry" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcVCountry" runat="server"
                            ControlToValidate="ddlVCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>
            </div>

            <div id="divUDF" runat="server" class="form-group" visible="false">
                <div class="form-group row" runat="server" id="divUDFCName">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblUDFCName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtUDFCName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFCName" runat="server"
                            ControlToValidate="txtUDFCName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revUDFCName" runat="server" ControlToValidate="txtUDFCName" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFDelType">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblUDFDelType" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtUDFDelType" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFDelType" runat="server"
                            ControlToValidate="txtUDFDelType" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revUDFDelType" runat="server" ControlToValidate="txtUDFDelType" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFProCategory">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblUDFProCategory" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlUDFProCategory" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUDFProCategory_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcUDFProCat" runat="server"
                            ControlToValidate="ddlUDFProCategory" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFProCatOther" visible="false">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        &nbsp;
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtUDFProCatOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcProCatOther" runat="server"
                            ControlToValidate="txtUDFProCatOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revUDFProCatOther" runat="server" ControlToValidate="txtUDFProCatOther" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCpostalcode">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblUDFCpostalcode" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtUDFCpostalcode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFCpcode" runat="server"
                            ControlToValidate="txtUDFCpostalcode" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revUDFCpostalcode" runat="server" ControlToValidate="txtUDFCpostalcode" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCLDept">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblUDFCLDept" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtUDFCLDept" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFCLDept" runat="server"
                            ControlToValidate="txtUDFCLDept" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revUDFCLDept" runat="server" ControlToValidate="txtUDFCLDept" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFAddress">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblUDFAddress" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtUDFAddress" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFAddress" runat="server"
                            ControlToValidate="txtUDFAddress" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revUDFAddress" runat="server" ControlToValidate="txtUDFAddress" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCLCom">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblUDFCLCom" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlUDFCLCom" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUDFCLCom_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcUDFCLCom" runat="server"
                            ControlToValidate="ddlUDFCLCom" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCLComOther" visible="false">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        &nbsp;
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtUDFCLComOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcUDFCLComOther" runat="server"
                            ControlToValidate="txtUDFCLComOther" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revUDFCLComOther" runat="server" ControlToValidate="txtUDFCLComOther" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCCountry">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblUDFCCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:DropDownList ID="ddlUDFCCountry" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcUDFCCountry" runat="server"
                            ControlToValidate="ddlUDFCCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>
            </div>

            <div id="divSuperVisor" runat="server" class="form-group" visible="false">
                <div class="form-group row" runat="server" id="divSupName">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblSupName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtSupName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcSupName" runat="server"
                            ControlToValidate="txtSupName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revSupName" runat="server" ControlToValidate="txtSupName" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divSupDesignation">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblSupDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtSupDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcSupDes" runat="server"
                            ControlToValidate="txtSupDesignation" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revSupDesignation" runat="server" ControlToValidate="txtSupDesignation" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divSupContact">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblSupContact" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtSupContact" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcSupContact" runat="server"
                            ControlToValidate="txtSupContact" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revSupContact" runat="server" ControlToValidate="txtSupContact" ErrorMessage="Not allow."
                            ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divSupEmail">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblSupEmail" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtSupEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:RequiredFieldValidator ID="vcSupEmail" runat="server"
                            ControlToValidate="txtSupEmail" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="validateSupEmail"
                            runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                            ControlToValidate="txtSupEmail"
                            ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>
            </div>
            <div id="divQA" runat="server" class="container" style="padding-left: 32px;">
                <asp:Panel ID="PanelMain" CssClass="flex-grid" runat="server"></asp:Panel>

            </div>

            <div id="divFTRCHK1" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:CheckBoxList ID="chkFTRCHK1" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                        <asp:Label ID="lblErrFTRCHK1" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div class="container ">
                <div class="col-md-12">
                    <div class="row" id="divFTR1" runat="server" visible="false">
                        <div class="form-group row">
                            <div class="clear"></div>
                            <div class="col-md-12">
                                <asp:Label ID="lblFTR1" runat="server"></asp:Label>
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="row" id="divFTRCHK" runat="server" visible="false">
                        <div class="form-group row">
                            <div class="clear"></div>
                            <div class="col-md-12" style="padding-left: 30px;">
                                <asp:CheckBoxList ID="chkFTRCHK" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                                <%--<asp:CheckBox ID="chkFTRCHK" runat="server" />&nbsp;&nbsp;<asp:Label ID="lblFTRCHK" runat="server"></asp:Label>
                            <asp:Label ID="lblFTRCHKIsSkip" runat="server" Visible="false" Text="0"></asp:Label>--%>
                                <asp:Label ID="lblErrFTRCHK" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="divFTR2" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10">
                        <asp:Label ID="lblFTR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-group container" style="padding-top: 20px;">
                    <asp:Panel runat="server" ID="PanelShowBackButton" Visible="false">
                        <div class="col-lg-2 col-sm-offset-4 col-sm-2 center-block">
                            <asp:Button runat="server" ID="btnBack" CssClass="btn MainButton btn-block" Text="Cancel" OnClick="btnBack_Click" CausesValidation="false" />
                        </div>
                        <div class="col-lg-2  col-sm-2 center-block">
                            <asp:Button runat="server" ID="btnSave2" CssClass="btn MainButton btn-block" Text="Next" OnClick="btnSave_Click" />
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="PanelWithoutBack" Visible="true">
                        <div class="col-lg-offset-5 col-lg-3 col-sm-offset-4 col-sm-3 center-block">
                            <asp:Button runat="server" ID="btnSave" CssClass="btn MainButton btn-block" Text="Submit" OnClick="btnSave_Click" OnClientClick="gtag_report_conversion();" />

                        </div>
                    </asp:Panel>
                </div>
            </div>

            <asp:HiddenField ID="hfRegno" runat="server" />
            <br />

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

