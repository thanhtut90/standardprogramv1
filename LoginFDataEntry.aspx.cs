﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LoginFDataEntry : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected static string reloginLockPage = "LoginFDataEntry.aspx";

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = string.Empty;
        string flowid = string.Empty;
        if (Request.QueryString["Event"] != null)
        {
            tmpDataList tmpList = GetShowIDByEventName(Request.QueryString["Event"].ToString());
            if (string.IsNullOrEmpty(tmpList.showID))
                Response.Redirect("404.aspx");
            else
            {
                showid = tmpList.showID;
                flowid = tmpList.FlowID;
                if (!string.IsNullOrEmpty(showid))
                {
                    SetSiteMaster(showid);
                }
            }
        }
        else
        {
            showid = cFun.DecryptValue(urlQuery.CurrShowID);
            flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                SetSiteMaster(showid);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session.Clear();
            Session.Abandon();
            removeCookie();//***added on 5-11-2018
            //bool isLock = isRegLoginLock();
            //if (isLock)
            //{
            //    Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
            //}
        }
    }

    public void removeCookie()
    {
        HttpCookie aCookie;
        string cookieName;
        int limit = Request.Cookies.Count;
        for (int i = 0; i < limit; i++)
        {
            cookieName = Request.Cookies[i].Name;
            aCookie = new HttpCookie(cookieName);
            aCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(aCookie);
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = "ReloginFormMaster.master";
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region btnVerify_Click and use table tb_Registration and tb_loginlog & insert into tb_loginlog
    protected void btnVerify_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            logRegLogin();//***
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = string.Empty;
            string flowid = string.Empty;
            if (Request.QueryString["Event"] != null)
            {
                tmpDataList tmpList = GetShowIDByEventName(Request.QueryString["Event"].ToString());
                if (string.IsNullOrEmpty(tmpList.showID))
                    Response.Redirect("404.aspx");
                else
                {
                    showid = tmpList.showID;
                    flowid = tmpList.FlowID;
                }
            }
            else
            {
                showid = cFun.DecryptValue(urlQuery.CurrShowID);
                flowid = cFun.DecryptValue(urlQuery.FlowID);
            }

            Boolean isvalid = isValid();
            if (isvalid)
            {
                if (!string.IsNullOrEmpty(showid) && !string.IsNullOrEmpty(flowid))
                {
                    string username = cFun.solveSQL(txtUsername.Text.ToString().Trim());
                    string password = cFun.solveSQL(txtPassword.Text.ToString().Trim());
                    string cmdStr = string.Empty;
                    DataTable dt = getLoginData(username, password);
                    if (dt.Rows.Count > 0)
                    {
                        string accountid = dt.Rows[0]["AccountID"].ToString();
                        string accountname = dt.Rows[0]["AccountName"].ToString();
                        Session["Showid"] = showid;
                        Session["FDataEntryAccountID"] = accountid;
                        Session["FDataEntryAccountName"] = accountname;

                        LoginLog lg = new LoginLog(fn);
                        string userip = GetUserIP();
                        lg.loginid = accountid;
                        lg.loginip = userip;
                        lg.logintype = "DataEntry(Frontend)";
                        lg.saveLoginLog();
                        string route = "~/eventreg?event=GGXP%20Survey";
                        Response.Redirect(route, false);
                    }
                    else
                    {
                        //***added on 5-11-2018
                        txtUsername.Text = "";
                        txtPassword.Text = "";
                        //***added on 5-11-2018
                        lblerror.Text = "Wrong combination of user name and password";
                        lblerror.Visible = true;
                        //Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
        }
    }
    #endregion

    protected Boolean isValid()
    {
        Boolean isvalid = true;

        if (string.IsNullOrEmpty(txtPassword.Text) || string.IsNullOrWhiteSpace(txtPassword.Text))
        {
            lblerror.Text = "Please insert your password";
            lblerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblerror.Visible = false;
        }
        if (string.IsNullOrEmpty(txtUsername.Text) || string.IsNullOrWhiteSpace(txtUsername.Text))
        {
            lblerror.Text = "Please insert your username";
            lblerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblerror.Visible = false;
        }
        return isvalid;
    }
    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
    public class tmpDataList
    {
        public string showID { get; set; }
        public string FlowID { get; set; }
    }
    private tmpDataList GetShowIDByEventName(string eventName)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            cList.showID = "";
            cList.FlowID = "";
            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                cList.showID = dList.Rows[0]["showID"].ToString();
                cList.FlowID = dList.Rows[0]["FLW_ID"].ToString();
            }
        }
        catch { }
        return cList;
    }
    private void logRegLogin()
    {
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'";
            //DataTable dtRelogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            //if (dtRelogin.Rows.Count > 0)
            //{
            //    string sqlReLoginUpdate = "Update tb_LogReLoginLocking Set relogin_attemptTimes=relogin_attemptTimes+1 Where relogin_machineip='" + machineIP + "'";
            //    fn.ExecuteSQL(sqlReLoginUpdate);
            //    string AttemptTime = fn.GetDataByCommand("Select relogin_attemptTimes From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'", "relogin_attemptTimes");
            //    int attempedTime = 0;
            //    int.TryParse(AttemptTime, out attempedTime);
            //    if (attempedTime > 5)
            //    {
            //        Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
            //    }
            //}
            //else
            {
                string sqlReLoginInsert = "Insert Into tb_LogReLoginLocking (relogin_machineip)"
                                + " Values ('" + machineIP + "')";
                fn.ExecuteSQL(sqlReLoginInsert);
                Session["ReloginMachineIP"] = machineIP;
            }
        }
        catch (Exception ex)
        { }
    }
    private DataTable getLoginData(string username, string password)
    {
        DataTable dt = new DataTable();
        try
        {
            string sql = "Select * From tb_Admin_RegEntry Where IsActive=1 And AccountUserName='" + username + "' And AccountPassword='" + password + "' COLLATE SQL_Latin1_General_Cp1_CS_AS";
            dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        }
        catch(Exception ex)
        { }

        return dt;
    }
}