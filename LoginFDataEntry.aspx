﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReloginFormMaster.master" AutoEventWireup="true" CodeFile="LoginFDataEntry.aspx.cs" Inherits="LoginFDataEntry" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%--<script type="text/javascript">
        function fillInvisibleTextBox(sender) {
            var invisibleTextBox = $get("<%=RadCaptcha1.ClientID %>_InvisibleTextBox");
            if (sender.checked) {
                invisibleTextBox.value = "Filled";
            }
            else {
                invisibleTextBox.value = "";
            }
        }
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="login-box">
        <%--<div class="login-logo" style="text-align:center;">
            <a href="#"><b>Login</b></a>
        </div>
        <br />--%>
        <!-- /.login-logo -->
        <div class="form-group row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <asp:TextBox ID="txtUsername" required="" class="form-control" placeholder="User Name" runat="server"></asp:TextBox>
                <%--<span class="glyphicon glyphicon-envelope form-control-feedback"></span>--%>
                <br />
                <asp:TextBox ID="txtPassword" required="" TextMode="Password" class="form-control" placeholder="Password" runat="server"></asp:TextBox>
                <%--<span class="glyphicon glyphicon-lock form-control-feedback"></span>--%>
                <br />
                <%--<telerik:RadSkinManager ID="RadSkinManager2" runat="server" ShowChooser="false" />
                <telerik:RadScriptBlock ID="radSript1" runat="server">
                </telerik:RadScriptBlock>
                <telerik:RadFormDecorator RenderMode="Lightweight" runat="server" DecoratedControls="All" DecorationZoneID="zoneToDecorate" />
                <div class="demo-containers">
                    <div class="demo-container" id="zoneToDecorate">
                        <table>
                            <tr>
                                <td>
                                    <div>
                                        <telerik:RadCaptcha ID="RadCaptcha1" runat="server" ErrorMessage="Page not valid. The code you entered is not valid."
                                            ValidationGroup="Group" ForeColor="Red" >
                                            <CaptchaImage ImageCssClass="imageClass" />
                                        </telerik:RadCaptcha>
                                    </div>
                                    <asp:Label ID="lblCorrectCode" runat="server" ForeColor="Green"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>--%>
                <asp:Label ID="lblerror" runat="server" Text="Label" Visible="false" ForeColor="Red" ></asp:Label>
                <br />
                <asp:Button ID="btnlogin" runat="server" Text="Login" CssClass="btn btn-primary" OnClick="btnVerify_Click" /><%-- ValidationGroup="Group" />--%>
            </div>
            <div class="col-md-4"></div>
        </div>
        <!-- /.col -->
    </div>
  <!-- /.login-box-body -->

</asp:Content>

