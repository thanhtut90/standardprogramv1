﻿using Corpit.Abstract;
using Corpit.Exh;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ExhibitorPortalDataSaveApi : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Functionality fn = new Functionality();
            SaveStatusJson rntStatus = new SaveStatusJson();
            ExhibitorObj exhObj = new ExhibitorObj(fn);
            string jsonPostData = "";
            ExhibitorObj exhData = new ExhibitorObj(fn);
            try
            {
                using (var stream = Request.InputStream)
                {
                    stream.Position = 0;
                    using (var reader = new System.IO.StreamReader(stream))
                    {
                        jsonPostData = reader.ReadToEnd();
                    }
                }
                exhData = JsonConvert.DeserializeObject<ExhibitorObj>(jsonPostData);

                exhObj.ShowID = exhData.ShowID;
                exhObj.FlowID = exhData.FlowID;
                exhObj.ExhibitorID = exhData.ExhibitorID;
                exhObj.RegGroupID = exhData.RegGroupID;
                exhObj.ExhibitingCompanyName = exhData.ExhibitingCompanyName;
                exhObj.ContactPersonName = exhData.ContactPersonName;
                exhObj.BoothNumber = exhData.BoothNumber;/*save in RG_Department*/
                exhObj.ContactPersonDesignation = exhData.ContactPersonDesignation;
                exhObj.ContactPersonEmail = exhData.ContactPersonEmail;
                exhObj.Address = exhData.Address;
                exhObj.City = exhData.City;
                exhObj.State = exhData.State;
                exhObj.PostalCode = exhData.PostalCode;
                exhObj.Country = exhData.Country;
                exhObj.TelephoneCC = exhData.TelephoneCC;
                exhObj.TelephoneNumber = exhData.TelephoneNumber;
                if (exhObj == null || string.IsNullOrEmpty(exhObj.ShowID))
                {
                    rntStatus.Status = "ERROR";
                    rntStatus.regGroupID = "";
                }
                else
                {
                    string regPortalRegGroupID = "";
                    int isSuccess = exhObj.saveRegContactPersonInfo(ref regPortalRegGroupID);
                    if (isSuccess > 0)
                    {
                        rntStatus.Status = "OK";
                        rntStatus.regGroupID = regPortalRegGroupID;
                    }
                    else
                    {
                        rntStatus.Status = "Saving Error";// "ERROR";
                        rntStatus.regGroupID = "";
                    }
                }
            }
            catch(Exception ex)
            {
                rntStatus.Status = ex.Message;// "ERROR";
                rntStatus.regGroupID = "";
            }

            JavaScriptSerializer ser = new JavaScriptSerializer();
            String jsonStr = ser.Serialize(rntStatus);
            Response.Write(jsonStr);
        }
    }
}