﻿using Corpit.BackendMaster;
using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reg_RegCompany : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    static string _Name = "Name";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _City = "City";
    static string _State = "State";
    static string _ZipCode = "Zip Code";
    static string _Country = "Country";
    static string _TelCC = "Telcc";
    static string _TelAC = "Telac";
    static string _Tel = "Tel";
    static string _FaxCC = "Faxcc";
    static string _FaxAC = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Website = "Website";
    static string _Additional1 = "Additional1";
    static string _Additional2 = "Additional2";
    static string _Additional3 = "Additional3";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());

            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string currentstage = Session["regstage"].ToString();
                if (!String.IsNullOrEmpty(groupid))
                {
                    bindDropdown();

                    bool isValidShow = populateUserDetails(groupid, showid);
                    if (isValidShow)
                    {
                        if (setDynamicForm(flowid, showid))
                        {
                            //ControlsEnabled();

                            insertRegLoginAction(groupid, rlgobj.actview, urlQuery);
                        }
                        else
                        {
                            Response.Redirect("404.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("404.aspx");
                    }
                }
            }
            else
            {
                Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
            }
        }
    }

    #region ControlsEnabled (set some controls' enablility true or false)
    private void ControlsEnabled()
    {
        txtName.Enabled = false;
        txtAddress1.Enabled = false;
        txtAddress2.Enabled = false;
        txtAddress3.Enabled = false;
        txtCity.Enabled = false;
        txtState.Enabled = false;
        txtZipcode.Enabled = false;
        ddlCountry.Enabled = false;
        ddlCountry.BackColor = System.Drawing.Color.FromArgb(235, 235, 228);
        txtTelcc.Enabled = false;
        txtTelac.Enabled = false;
        txtFaxcc.Enabled = false;
        txtFaxac.Enabled = false;
        txtWebsite.Enabled = false;
    }
    #endregion

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMasterReLogin;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region bindDropdown & bind countries to Country dropdownlist
    protected void bindDropdown()
    {
        DataSet dsCountry = new DataSet();
        CountryObj couObj = new CountryObj(fn);
        dsCountry = couObj.getAllCountry();

        if (dsCountry.Tables[0].Rows.Count != 0)
        {
            for (int x = 0; x < dsCountry.Tables[0].Rows.Count; x++)
            {
                ddlCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();
            }
        }
    }
    #endregion

    #region setDynamicForm (set div visibility and validator controls' enability dynamically (generate dynamic form) according to the settings of tb_Form table where form_type='C')
    protected bool setDynamicForm(string flowid, string showid)
    {
        bool isValidShow = false;

        DataSet ds = new DataSet();
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForCompany();

        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
        {
            isValidShow = true;

            #region set divName visibility is true or false if form_input_name is Name according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Name)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divName.Visible = true;
                }
                else
                {
                    divName.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblName.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtName.Attributes.Add("required", "");
                    vcName.Enabled = true;
                }
                else
                {
                    lblName.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtName.Attributes.Remove("required");
                    vcName.Enabled = false;
                }
            }
            #endregion

            #region set divAddress1 visibility is true or false if form_input_name is Address1 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress1.Visible = true;
                }
                else
                {
                    divAddress1.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAddress1.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress1.Attributes.Add("required", "");
                    vcAddress1.Enabled = true;
                }
                else
                {
                    lblAddress1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress1.Attributes.Remove("required");
                    vcAddress1.Enabled = false;
                }
            }
            #endregion

            #region set divAddress2 visibility is true or false if form_input_name is Address2 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress2.Visible = true;
                }
                else
                {
                    divAddress2.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAddress2.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress2.Attributes.Add("required", "");
                    vcAddress2.Enabled = true;
                }
                else
                {
                    lblAddress2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress2.Attributes.Remove("required");
                    vcAddress2.Enabled = false;
                }
            }
            #endregion

            #region set divAddress3 visibility is true or false if form_input_name is Address3 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Address3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAddress3.Visible = true;
                }
                else
                {
                    divAddress3.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAddress3.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress3.Attributes.Add("required", "");
                    vcAddress3.Enabled = true;
                }
                else
                {
                    lblAddress3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAddress3.Attributes.Remove("required");
                    vcAddress3.Enabled = false;
                }
            }
            #endregion

            #region set divCity visibility is true or false if form_input_name is City according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _City)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCity.Visible = true;
                }
                else
                {
                    divCity.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblCity.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtCity.Attributes.Add("required", "");
                    vcCity.Enabled = true;
                }
                else
                {
                    lblCity.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtCity.Attributes.Remove("required");
                    vcCity.Enabled = false;
                }
            }
            #endregion

            #region set divState visibility is true or false if form_input_name is State according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _State)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divState.Visible = true;
                }
                else
                {
                    divState.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblState.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtState.Attributes.Add("required", "");
                    vcState.Enabled = true;
                }
                else
                {
                    lblState.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtState.Attributes.Remove("required");
                    vcState.Enabled = false;
                }
            }
            #endregion

            #region set divZipcode visibility is true or false if form_input_name is Zip Code according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _ZipCode)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divZipcode.Visible = true;
                }
                else
                {
                    divZipcode.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblZipcode.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtZipcode.Attributes.Add("required", "");
                    vcZipcode.Enabled = true;
                }
                else
                {
                    lblZipcode.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtZipcode.Attributes.Remove("required");
                    vcZipcode.Enabled = false;
                }
            }
            #endregion

            #region set divCountry visibility is true or false if form_input_name is Country according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Country)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divCountry.Visible = true;
                }
                else
                {
                    divCountry.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblCountry.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlCountry.Attributes.Add("required", "");
                    vcCountry.Enabled = true;
                }
                else
                {
                    lblCountry.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //ddlCountry.Attributes.Remove("required");
                    vcCountry.Enabled = false;
                }
            }
            #endregion

            #region set divTelcc visibility is true or false if form_input_name is Telcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _TelCC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelcc.Visible = true;
                }
                else
                {
                    divTelcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtTelcc.Attributes.Add("required", "");
                    vcTelcc.Enabled = true;
                }
                else
                {
                    //txtTelcc.Attributes.Remove("required");
                    vcTelcc.Enabled = false;
                }
            }
            #endregion

            #region set divTelac visibility is true or false if form_input_name is Telac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _TelAC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTelac.Visible = true;
                }
                else
                {
                    divTelac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtTelac.Attributes.Add("required", "");
                    vcTelac.Enabled = true;
                }
                else
                {
                    //txtTelac.Attributes.Remove("required");
                    vcTelac.Enabled = false;
                }
            }
            #endregion

            #region set divTel visibility is true or false if form_input_name is Tel according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Tel)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divTel.Visible = true;
                    divTelNo.Visible = true;
                }
                else
                {
                    divTel.Visible = false;
                    divTelNo.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblTel.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtTel.Attributes.Add("required", "");
                    vcTel.Enabled = true;
                }
                else
                {
                    lblTel.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtTel.Attributes.Remove("required");
                    vcTel.Enabled = false;
                }
            }
            #endregion

            #region set divFaxcc visibility is true or false if form_input_name is Faxcc according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _FaxCC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxcc.Visible = true;
                }
                else
                {
                    divFaxcc.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtFaxcc.Attributes.Add("required", "");
                    vcFaxcc.Enabled = true;
                }
                else
                {
                    //txtFaxcc.Attributes.Remove("required");
                    vcFaxcc.Enabled = false;
                }
            }
            #endregion

            #region set divFaxac visibility is true or false if form_input_name is Faxac according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _FaxAC)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFaxac.Visible = true;
                }
                else
                {
                    divFaxac.Visible = false;
                }

                if (isrequired == 1)
                {
                    //txtFaxac.Attributes.Add("required", "");
                    vcFaxac.Enabled = true;
                }
                else
                {
                    //txtFaxac.Attributes.Remove("required");
                    vcFaxac.Enabled = false;
                }
            }
            #endregion

            #region set divFax visibility is true or false if form_input_name is Fax according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Fax)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divFax.Visible = true;
                    divFaxNo.Visible = true;
                }
                else
                {
                    divFax.Visible = false;
                    divFaxNo.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblFax.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtFax.Attributes.Add("required", "");
                    vcFax.Enabled = true;
                }
                else
                {
                    lblFax.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtFax.Attributes.Remove("required");
                    vcFax.Enabled = false;
                }
            }
            #endregion

            #region set divEmail visibility is true or false if form_input_name is Email according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Email)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmail.Visible = true;
                }
                else
                {
                    divEmail.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblEmail.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtEmail.Attributes.Add("required", "");
                    vcEmail.Enabled = true;
                }
                else
                {
                    lblEmail.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtEmail.Attributes.Remove("required");
                    vcEmail.Enabled = false;
                }
            }
            #endregion

            #region set divEmailConfirmation visibility is true or false if form_input_name is ConfirmEmail according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _EmailConfirmation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divEmailConfirmation.Visible = true;
                    //txtEmailConfirmation.Attributes.Add("required", "");
                }
                else
                {
                    divEmailConfirmation.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblEmailConfirmation.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtEmailConfirmation.Attributes.Add("required", "");
                    vcEConfirm.Enabled = true;
                }
                else
                {
                    lblEmailConfirmation.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtEmailConfirmation.Attributes.Remove("required");
                    vcEConfirm.Enabled = false;
                }
            }
            #endregion

            #region set divWebsite visibility is true or false if form_input_name is Website according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Website)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divWebsite.Visible = true;
                }
                else
                {
                    divWebsite.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblWebsite.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtWebsite.Attributes.Add("required", "");
                    vcWeb.Enabled = true;
                }
                else
                {
                    lblWebsite.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtWebsite.Attributes.Remove("required");
                    vcWeb.Enabled = false;
                }
            }
            #endregion            

            #region set divAdditional1 visibility is true or false if form_input_name is Additional1 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional1)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional1.Visible = true;
                }
                else
                {
                    divAdditional1.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAdditional1.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional1.Attributes.Add("required", "");
                    vcAdditional1.Enabled = true;
                }
                else
                {
                    lblAdditional1.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional1.Attributes.Remove("required");
                    vcAdditional1.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional2 visibility is true or false if form_input_name is Additional2 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional2)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional2.Visible = true;
                }
                else
                {
                    divAdditional2.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAdditional2.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional2.Attributes.Add("required", "");
                    vcAdditional2.Enabled = true;
                }
                else
                {
                    lblAdditional2.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional2.Attributes.Remove("required");
                    vcAdditional2.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional3 visibility is true or false if form_input_name is Additional3 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional3)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional3.Visible = true;
                }
                else
                {
                    divAdditional3.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAdditional3.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional3.Attributes.Add("required", "");
                    vcAdditional3.Enabled = true;
                }
                else
                {
                    lblAdditional3.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional3.Attributes.Remove("required");
                    vcAdditional3.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional4 visibility is true or false if form_input_name is Additional4 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional4)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional4.Visible = true;
                }
                else
                {
                    divAdditional4.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAdditional4.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional4.Attributes.Add("required", "");
                    vcAdditional4.Enabled = true;
                }
                else
                {
                    lblAdditional4.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional4.Attributes.Remove("required");
                    vcAdditional4.Enabled = false;
                }
            }
            #endregion

            #region set divAdditional5 visibility is true or false if form_input_name is Additional5 according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Additional5)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAdditional5.Visible = true;
                }
                else
                {
                    divAdditional5.Visible = false;
                }

                if (isrequired == 1)
                {
                    lblAdditional5.Text = "<span class=\"red\">^</span>&nbsp;&nbsp; " + ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional5.Attributes.Add("required", "");
                    vcAdditional5.Enabled = true;
                }
                else
                {
                    lblAdditional5.Text = ds.Tables[0].Rows[x]["form_input_text"].ToString();
                    //txtAdditional5.Attributes.Remove("required");
                    vcAdditional5.Enabled = false;
                }
            }
            #endregion
        }

        setDivCss_TelMobFax(divTelcc.Visible, divTelac.Visible, divTelNo.Visible, "Tel");//*
        setDivCss_TelMobFax(divFaxcc.Visible, divFaxac.Visible, divFaxNo.Visible, "Fax");//*

        return isValidShow;
    }
    #endregion

    #region populateUserDetails (get all relevant data according to RegGroupID from tb_RegCompany and bind data to the respective controls)
    private bool populateUserDetails(string id, string showid)
    {
        bool isValidShow = false;

        DataTable dt = new DataTable();
        RegCompanyObj rgc = new RegCompanyObj(fn);
        dt = rgc.getRegCompanyByID(id, showid);
        if (dt.Rows.Count > 0)
        {
            isValidShow = true;
            string reggroupid = dt.Rows[0]["RegGroupID"].ToString();
            string rc_name = dt.Rows[0]["RC_Name"].ToString();
            string rc_address1 = dt.Rows[0]["RC_Address1"].ToString();
            string rc_address2 = dt.Rows[0]["RC_Address2"].ToString();
            string rc_address3 = dt.Rows[0]["RC_Address3"].ToString();
            string rc_city = dt.Rows[0]["RC_City"].ToString();
            string rc_state = dt.Rows[0]["RC_State"].ToString();
            string rc_zipcode = dt.Rows[0]["RC_ZipCode"].ToString();
            string rc_country = dt.Rows[0]["RC_Country"].ToString();
            string rc_telcc = dt.Rows[0]["RC_Telcc"].ToString();
            string rc_telac = dt.Rows[0]["RC_Telac"].ToString();
            string rc_tel = dt.Rows[0]["RC_Tel"].ToString();
            string rc_faxcc = dt.Rows[0]["RC_Faxcc"].ToString();
            string rc_faxac = dt.Rows[0]["RC_Faxac"].ToString();
            string rc_fax = dt.Rows[0]["RC_Fax"].ToString();
            string rc_email = dt.Rows[0]["RC_Email"].ToString();
            string rc_website = dt.Rows[0]["RC_Website"].ToString();
            string rc_createddate = dt.Rows[0]["RC_CreatedDate"].ToString();
            string recycle = dt.Rows[0]["recycle"].ToString();
            string rc_stage = dt.Rows[0]["RC_Stage"].ToString();

            string rc_additional1 = dt.Rows[0]["RC_Additional1"].ToString();
            string rc_additional2 = dt.Rows[0]["RC_Additional2"].ToString();
            string rc_additional3 = dt.Rows[0]["RC_Additional3"].ToString();
            string rc_additional4 = dt.Rows[0]["RC_Additional4"].ToString();
            string rc_additional5 = dt.Rows[0]["RC_Additional5"].ToString();

            txtName.Text = rc_name;
            txtAddress1.Text = rc_address1;
            txtAddress2.Text = rc_address2;
            txtAddress3.Text = rc_address3;
            txtCity.Text = rc_city;
            txtState.Text = rc_state;
            txtZipcode.Text = rc_zipcode;

            try
            {
                if (!String.IsNullOrEmpty(rc_country))
                {
                    ListItem listItem = ddlCountry.Items.FindByValue(rc_country);
                    if (listItem != null)
                    {
                        ddlCountry.ClearSelection();
                        listItem.Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            txtTelcc.Text = rc_telcc;
            txtTelac.Text = rc_telac;
            txtTel.Text = rc_tel;
            txtFaxcc.Text = rc_faxcc;
            txtFaxac.Text = rc_faxac;
            txtFax.Text = rc_fax;
            txtEmail.Text = rc_email;
            txtWebsite.Text = rc_website;

            txtAdditional1.Text = rc_additional1;
            txtAdditional2.Text = rc_additional2;
            txtAdditional3.Text = rc_additional3;
            txtAdditional4.Text = rc_additional4;
            txtAdditional5.Text = rc_additional5;
        }

        return isValidShow;
    }
    #endregion

    #region ddlCountry_SelectedIndexChanged (bind country code data to txtTelcc, txtMobcc, txtFaxcc textboxes according to the selected country)
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlCountry.Items.Count > 0)
            {
                string countryid = ddlCountry.SelectedItem.Value;

                if (ddlCountry.SelectedIndex == 0)
                {
                    countryid = Number.Zero;
                }

                CountryObj couObj = new CountryObj(fn);
                DataTable dt = couObj.getCountryByID(countryid);
                if (dt.Rows.Count > 0)
                {
                    string code = dt.Rows[0]["countryen"].ToString();

                    txtTelcc.Text = code;
                    txtFaxcc.Text = code;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region btnNext_Click (update data into tb_RegCompany table & get next route and redirect to next page according to the site flow settings (tb_site_flow table))
    protected void btnNext_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            if (Session["Groupid"] != null)
            {
                int isSuccess = 0;
                Boolean hasid = false;
                string groupid = string.Empty;

                RegCompanyObj rgc = new RegCompanyObj(fn);

                groupid = Session["Groupid"].ToString();
                hasid = true;

                string name = string.Empty;
                string address1 = string.Empty;
                string address2 = string.Empty;
                string address3 = string.Empty;
                string city = string.Empty;
                string state = string.Empty;
                string zipcode = string.Empty;
                string country = string.Empty;
                string telcc = string.Empty;
                string telac = string.Empty;
                string tel = string.Empty;
                string faxcc = string.Empty;
                string faxac = string.Empty;
                string fax = string.Empty;
                string email = string.Empty;
                string website = string.Empty;
                string createdate = DateTime.Now.ToString("yyyy-MM-dd HH: mm:ss", CultureInfo.CurrentCulture);
                int recycle = 0;
                string stage = string.Empty;

                string additional1 = string.Empty;
                string additional2 = string.Empty;
                string additional3 = string.Empty;
                string additional4 = string.Empty;
                string additional5 = string.Empty;

                name = cFun.solveSQL(txtName.Text.Trim());
                address1 = cFun.solveSQL(txtAddress1.Text.Trim());
                address2 = cFun.solveSQL(txtAddress2.Text.Trim());
                address3 = cFun.solveSQL(txtAddress3.Text.Trim());
                city = cFun.solveSQL(txtCity.Text.Trim());
                zipcode = cFun.solveSQL(txtZipcode.Text.Trim());
                state = cFun.solveSQL(txtState.Text.Trim());
                country = ddlCountry.SelectedItem.Value.ToString();
                telcc = txtTelcc.Text.ToString();
                telac = txtTelac.Text.ToString();
                tel = txtTel.Text.ToString();
                faxcc = txtFaxcc.Text.ToString();
                faxac = txtFaxac.Text.ToString();
                fax = txtFax.Text.ToString();
                email = cFun.solveSQL(txtEmail.Text.Trim());
                website = cFun.solveSQL(txtWebsite.Text.Trim());

                additional1 = cFun.solveSQL(txtAdditional1.Text.Trim());
                additional2 = cFun.solveSQL(txtAdditional2.Text.Trim());
                additional3 = cFun.solveSQL(txtAdditional3.Text.Trim());
                additional4 = cFun.solveSQL(txtAdditional4.Text.Trim());
                additional5 = cFun.solveSQL(txtAdditional5.Text.Trim());

                rgc.groupid = groupid;
                rgc.name = name;
                rgc.address1 = address1;
                rgc.address2 = address2;
                rgc.address3 = address3;
                rgc.city = city;
                rgc.state = state;
                rgc.zipcode = zipcode;
                rgc.country = country;
                rgc.telcc = telcc;
                rgc.telac = telac;
                rgc.tel = tel;
                rgc.faxcc = faxcc;
                rgc.faxac = faxac;
                rgc.fax = fax;
                rgc.email = email;
                rgc.website = website;
                rgc.createdate = createdate;
                rgc.recycle = recycle;
                rgc.stage = stage;
                rgc.additional1 = additional1;
                rgc.additional2 = additional2;
                rgc.additional3 = additional3;
                rgc.additional4 = additional4;
                rgc.additional5 = additional5;

                rgc.showID = showid;

                try
                {
                    bool isAlreadyExist = false;
                    if (hasid)
                    {
                        ////*Update
                        //isAlreadyExist = rgc.checkUpdateExist();
                        //if (isAlreadyExist == true)
                        //{
                        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This user already exists!');", true);
                        //    return;
                        //}
                        //else
                        {
                            isSuccess = rgc.updateRegCompany();
                        }
                    }

                    if (isSuccess > 0)
                    {
                        insertRegLoginAction(groupid, rlgobj.actupdate, urlQuery);

                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Updated successful.');", true);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    LogGenEmail lggenemail = new LogGenEmail(fn);
                    lggenemail.type = GenLogDefaultValue.errorException;
                    lggenemail.RefNumber = groupid;
                    lggenemail.description = ex.Message;
                    lggenemail.remark = RegClass.typeCmp + cFun.DecryptValue(urlQuery.FlowID);
                    lggenemail.step = cFun.DecryptValue(urlQuery.CurrIndex);
                    lggenemail.writeLog();
                }
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }
    #endregion

    #region insertRegLoginAction (insert flow data into tb_Log_RegLogin table)
    private void insertRegLoginAction(string groupid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogRegLogin rlg = new LogRegLogin(fn);
        rlg.reglogin_regno = groupid;
        rlg.reglogin_flowid = flowid;
        rlg.reglogin_step = step;
        rlg.reglogin_action = action;
        rlg.saveLogRegLogin();
    }
    #endregion

    #region setDivCss_TelMobFax
    public void setDivCss_TelMobFax(bool isShowCC, bool isShowAC, bool isShowPhoneNo, string type)
    {
        string name = string.Empty;
        try
        {
            if (type == "Tel")
            {
                #region type="Tel"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");

                    divTelac.Attributes.Remove("class");

                    divTelNo.Attributes.Remove("class");
                    divTelNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divTelcc.Attributes.Remove("class");
                    divTelcc.Attributes.Add("class", "col-xs-6");

                    divTelac.Attributes.Remove("class");
                    divTelac.Attributes.Add("class", "col-xs-6");

                    divTelNo.Attributes.Remove("class");
                }
                #endregion
            }
            else if (type == "Fax")
            {
                #region Type="Fax"
                if (!isShowCC && isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-9");
                }
                else if (!isShowCC && !isShowAC && isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");

                    divFaxac.Attributes.Remove("class");

                    divFaxNo.Attributes.Remove("class");
                    divFaxNo.Attributes.Add("class", "col-xs-12");
                }
                else if (isShowCC && isShowAC && !isShowPhoneNo)
                {
                    divFaxcc.Attributes.Remove("class");
                    divFaxcc.Attributes.Add("class", "col-xs-6");

                    divFaxac.Attributes.Remove("class");
                    divFaxac.Attributes.Add("class", "col-xs-6");

                    divFaxNo.Attributes.Remove("class");
                }
                #endregion
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
}