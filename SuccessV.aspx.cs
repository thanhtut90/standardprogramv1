﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Logging;
using System.Data;
using Corpit.Email;
using Corpit.Site.Email;

public partial class SuccessV : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cComFuz = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        SetSiteMaster(showid);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
            string currentStep = cComFuz.DecryptValue(urlQuery.CurrIndex);

            insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actsuccess, urlQuery);

            RegGroupObj rgg = new RegGroupObj(fn);
            rgg.updateGroupCurrentStep(urlQuery);

            RegDelegateObj rgd = new RegDelegateObj(fn);
            rgd.updateDelegateCurrentStep(urlQuery);

            string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
            FlowControler flwControl = new FlowControler(fn);
            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);

            StatusSettings stuSettings = new StatusSettings(fn);

            string regstatus = "0";
            DataTable dt = new DataTable();
            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
            {
                dt = rgg.getRegGroupByID(GroupRegID, showid);
                if (dt.Rows.Count > 0)
                {
                    regstatus = dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                }

                rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                rgd.updateDelegateRegStatus(GroupRegID, stuSettings.Success, showid);
            }
            else//*flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL
            {
                rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                if (!string.IsNullOrEmpty(DelegateID))
                {
                    dt = rgd.getDataByGroupIDRegno(GroupRegID, DelegateID, showid);
                    if (dt.Rows.Count > 0)
                    {
                        regstatus = dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                    }

                    rgd.updateStatus(DelegateID, stuSettings.Success, showid);
                }
            }

            #region bindSuccessMessage
            lblSuccessMessage.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowSuccessMsg) ? flwMasterConfig.FlowSuccessMsg : "");
            if (string.IsNullOrEmpty(lblSuccessMessage.Text))
                PanelMsg.Visible = false;
            #endregion

            #region Comment (SendCurrentFlowStepEmail(if already sent, not send again))
            //if (cComFuz.ParseInt(regstatus) == stuSettings.Pending)
            //{
            //    FlowControler flw = new FlowControler(fn, urlQuery);
            //    //  flw.SendCurrentStepEmail(urlQuery);
            //    EmailHelper esender = new EmailHelper();
            //    esender.SendCurrentFlowStepEmail(urlQuery);
            //}
            #endregion

            #region SendCurrentFlowStepEmail for Asean Summit Group Reg(whether already sent or not send, send again))
            EmailHelper eHelper = new EmailHelper();
            string emailRegType = BackendRegType.backendRegType_Group;// "G";
            EmailDSourceKey sourcKey = new EmailDSourceKey();
            string flowID = flowid;
            string groupID = GroupRegID;
            string delegateID = DelegateID;//if not, Blank
            string eType = EmailHTMLTemlateType.Confirmation;

            sourcKey.AddKeyToList("ShowID", showid);
            sourcKey.AddKeyToList("RegGroupID", groupID);
            sourcKey.AddKeyToList("Regno", delegateID);
            sourcKey.AddKeyToList("FlowID", flowID);

            string updatedUser = string.Empty;
            if (Session["Groupid"] != null)
            {
                updatedUser = Session["Groupid"].ToString();
            }
            bool isEmailSend = eHelper.SendEmailFromBackend(eType, sourcKey, emailRegType, updatedUser);
            #endregion

            #region Logging
            LogGenEmail lggenemail = new LogGenEmail(fn);
            LogActionObj lactObj = new LogActionObj();
            lggenemail.type = LogType.generalType;
            lggenemail.RefNumber = groupID + "," + DelegateID;
            lggenemail.description = "Send confrimation email to " + groupID;
            lggenemail.remark = "Send confrimation email to " + groupID + " and Send " + eType + " email(email sending status:" + isEmailSend + ")";
            lggenemail.step = currentStep;
            lggenemail.writeLog();
            #endregion

            try
            {
                CommonFuns cFuz = new CommonFuns();
                string DID = "";
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    DID = GroupRegID;
                }
                else
                    DID = DelegateID;
                HTMLTemplateControler htmlControl = new HTMLTemplateControler();
                string template = htmlControl.CreateAcknowledgeLetterHTMLTemplate(showid, DID);
                string rtnFile = htmlControl.CreatePDF(template, showid, DID, "Acknowledge");
                if (!string.IsNullOrEmpty(rtnFile))
                {
                    ShowPDF.Visible = true;
                    ShowPDF.Attributes["src"] = rtnFile;
                }
            }
            catch { }
        }
    }

    #region PageSetting
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
            Page.MasterPageFile = masterPage;
    }

    #endregion

    #region insertLogFlowAction
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        string step = cComFuz.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion
}