﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
using System.Data.SqlClient;
using System.Globalization;
using Corpit.Logging;

public partial class WelcomeLaunch : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected override void OnPreInit(EventArgs e)
    {
        if (Request.QueryString["SHW"] != null)
        {
            string showID = cFun.DecryptValue(Request.QueryString["SHW"].ToString());
            LoadShowSetting(showID);
           
        }
    }

    private void LoadShowSetting(string showID)
    {
        try
        {
            
            SetSiteMaster(showID);

            SiteSettings st = new SiteSettings(fn, showID);
            st.LoadBaseSiteProperties(showID);
            if (st.hastimer == "1")
            {
                Session["timeout"] = DateTime.Now.AddMinutes(30).ToString();
            }
        }
        catch { }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string showID = "";
            if (Request.QueryString["SHW"] != null && Request.QueryString["FLW"] != null)
            {

                Functionality fn = new Functionality();
                showID = Request.QueryString["SHW"].ToString();

                string FlowID = "";
            

                FlowID = cFun.DecryptValue(Request.QueryString["FLW"].ToString());


                SiteSettings st = new SiteSettings(fn, showID);
                string Type = ""; // Future Reg Close date can b vary depend on Delegate Type

                bindWelcomeMsg(FlowID);
                bindWelcomeMsgUnderRegButton(FlowID);//***added on 30-11-2018
                setDivPrePopulateEmailVisibility(FlowID);//*
                bindRegisterButtonCustomText(FlowID);//***added on 7-10-2019

                showID = cFun.DecryptValue(showID);
                try
                {
                    string grpNum = "";
                    CETEDMClickingLog(showID, FlowID, grpNum);
                    ShowControler showControler = new ShowControler(fn);
                    Show sh = showControler.GetShow(showID);
                    if (sh.SkipWelcome == "1")
                        btnRegister_Onclick(btnRegister, null);
                }
                catch { }
                //if (showID == RefShow.architectThaiShw)
                //{
                //    btnRegister.Text = "ลงทะเบียน";
                //}
                if (showID == "NVV352" || showID == "OUE353") lblAddtionalMsg.Visible = true;
            }
            
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    protected void btnRegister_Onclick(object sender, EventArgs e)
    {
        string showID = "";
        if (Request.QueryString["SHW"] != null && Request.QueryString["FLW"] != null)
        {
            showID = Request.QueryString["SHW"].ToString();

            string FlowID = "";

            FlowID = cFun.DecryptValue(Request.QueryString["FLW"].ToString());
            BrowseNext(showID, FlowID);
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    private void BrowseNext(string showID, string FlowID)
    {
        Functionality fn = new Functionality();
        SiteSettings st = new SiteSettings(fn, showID);
        string Type = ""; // Future Reg Close date can b vary depend on Delegate Type
        if (!st.IsRegClosed(Type))
        {
            bool isSuccess = false;
            bool isDelegateRegistered = false;
            string regno = string.Empty;
            string grpNum = "";
            bool isMatchedoldShowID = false;
            string decryptShowID = cFun.DecryptValue(showID);
            if(divPrePopulateEmail.Visible == true)
            {
                isSuccess = prepopulateChecking(decryptShowID, ref regno, ref isDelegateRegistered, ref grpNum, ref isMatchedoldShowID);
                if(!isSuccess)
                {
                    regno = "";
                    grpNum = "";
                    isMatchedoldShowID = false;
                }
            }

            string page = "DefaultRegIndex";
            string step = "";
            FlowControler Flw = new FlowControler(fn);

            if (string.IsNullOrEmpty(regno))
            {
                string route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step);
                if (Request.QueryString["TRK"] != null)// MDA Tracking
                {
                    route = route + "&TRK=" + Request.QueryString["TRK"].ToString();
                }

                CETEDMClickingLog(showID, FlowID, grpNum);
                if (Request.QueryString["CEI"] != null)// CET Tracking
                {
                    route = route + "&CEI=" + Request.QueryString["CEI"].ToString();
                }
                if (divPrePopulateEmail.Visible == true)
                {
                    if (!string.IsNullOrEmpty(txtPrePopulateEmail.Text) && !string.IsNullOrWhiteSpace(txtPrePopulateEmail.Text))
                    {
                         
                        hfNextRoute.Value = route;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    }
                    else
                    {
                        Response.Redirect(route);
                    }
                }
                else
                {
                    Response.Redirect(route);
                }
            }
            else
            {
                string route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step);
                if (isDelegateRegistered)
                {
                    grpNum = "";
                    route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step);
                    Response.Write("<script>alert('Already registered.');window.location='" + route + "';</script>");
                }
                else
                {
                    //if (!isMatchedoldShowID)
                    //{
                    //    grpNum = "";
                    //    route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step);
                    //    Response.Redirect(route);
                    //}
                    //else
                    {
                        route = Flw.MakeFullURL(page, FlowID, showID, cFun.EncryptValue(grpNum), step, cFun.EncryptValue(regno));
                        Response.Redirect(route);
                    }
                }
            }
        }
    }

    #region bindWelcomeMsg
    private void bindWelcomeMsg(string flowid)
    {
        FlowControler flwControl = new FlowControler(fn);
        FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
        if (!string.IsNullOrEmpty(flwMasterConfig.FlowWelcomePageMsg))
        {
            lblTemplate.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowWelcomePageMsg) ? flwMasterConfig.FlowWelcomePageMsg : "");
        }
    }
    #endregion

    #region bindWelcomeMsgUnderRegButton
    private void bindWelcomeMsgUnderRegButton(string flowid)
    {
        FlowControler flwControl = new FlowControler(fn);
        FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
        if (!string.IsNullOrEmpty(flwMasterConfig.FlowWelcomePageMsgUnderRegButton))
        {
            lblTemplate2.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowWelcomePageMsgUnderRegButton) ? flwMasterConfig.FlowWelcomePageMsgUnderRegButton : "");
        }
    }
    #endregion

    #region setDivPrePopulateEmailVisibility
    private void setDivPrePopulateEmailVisibility(string flowid)
    {
        FlowControler flwControl = new FlowControler(fn);
        FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
        if (flwMasterConfig.FlowIsPrePopulated)
        {
            divPrePopulateEmail.Visible = true;
        }
        else
        {
            divPrePopulateEmail.Visible = false;
        }
    }
    #endregion

    #region prepopulateChecking
    private bool prepopulateChecking(string showid, ref string regno, ref bool isDelegateRegistered, ref string groupid, ref bool isMatchOldShowID)
    {
        bool isSuccess = false;
        try
        {
            if(!string.IsNullOrEmpty(txtPrePopulateEmail.Text) && !string.IsNullOrWhiteSpace(txtPrePopulateEmail.Text))
            {
                //string delegateRefNo = string.Empty;
                int prepopulateStatus = (int)StatusValue.Pending;

                string emailaddress = txtPrePopulateEmail.Text.Trim();
                PrePopulate_RegDelegateObj preDelegateObj = new PrePopulate_RegDelegateObj(fn);
                bool isPrePopulateExist = preDelegateObj.checkPrePopulateDataExist(emailaddress, showid, ref regno);
                if(isPrePopulateExist)
                {
                    if(!string.IsNullOrEmpty(regno))
                    {
                        prepopulateStatus = preDelegateObj.getPreDelegateStatusByDelRefID(regno, showid);
                        if (prepopulateStatus == (int)StatusValue.Success)
                        {
                            RegDelegateObj regDelegateObj = new RegDelegateObj(fn);
                            int delegateStatus = regDelegateObj.getDelegateStatusByID(regno, showid);
                            if (delegateStatus == (int)StatusValue.Success)
                            {
                                isSuccess = true;
                                isDelegateRegistered = true;
                            }
                            else
                            {
                                int isSaved = insertRegDelegateData(showid, emailaddress, ref regno, ref groupid, ref isMatchOldShowID);
                                if (isSaved > 0)
                                {
                                    isSaved = preDelegateObj.updateDelegateRefNo(emailaddress, showid, regno);
                                    if (isSaved > 0)
                                    {
                                        string updateddate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                                        preDelegateObj.updateStatus(regno, (int)StatusValue.Success, showid, updateddate);
                                        isSuccess = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            string updateddate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                            preDelegateObj.updateStatus(regno, (int)StatusValue.Success, showid, updateddate);
                            isSuccess = true;
                        }
                    }
                    else
                    {
                        int isSaved = insertRegDelegateData(showid, emailaddress, ref regno, ref groupid, ref isMatchOldShowID);
                        if (isSaved > 0)
                        {
                            isSaved = preDelegateObj.updateDelegateRefNo(emailaddress, showid, regno);
                            if (isSaved > 0)
                            {
                                string updateddate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                                preDelegateObj.updateStatus(regno, (int)StatusValue.Success, showid, updateddate);
                                isSuccess = true;
                            }
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        {
            isSuccess = false;
        }

        return isSuccess;
    }
    #endregion

    #region insertRegDelegateData
    private int insertRegDelegateData(string showid, string preEmailAddress, ref string regno, ref string groupid, ref bool isMatchOldShowID)
    {
        int isSuccess = 0;
        try
        {
            #region create delegate in tb_RegDelegate
            RegDelegateObj rgd = new RegDelegateObj(fn);
            DataTable dt = rgd.getRegDelegateByID(regno, showid);//rgd.getRegDelegateByRegnoEmail(regno, preEmailAddress); 
            if (dt.Rows.Count == 0)
            {
                //create group in tb_RegGroup
                RegGroupObj rgp = new RegGroupObj(fn);
                groupid = rgp.GenGroupNumber(showid);// GetRunNUmber("GRP_KEY");
                rgp.createRegGroupIDOnly(groupid, 0, showid);

                regno = rgd.GenDelegateNumber(showid);
                PrePopulate_RegDelegateObj preDelObj = new PrePopulate_RegDelegateObj(fn);
                preDelObj = preDelObj.getPrePopulateData(preEmailAddress, showid);
                rgd.groupid = groupid;
                rgd.regno = regno;
                rgd.con_categoryID = preDelObj.con_categoryID;
                rgd.salutation = preDelObj.salutation;
                rgd.fname = preDelObj.fname;
                rgd.lname = preDelObj.lname;
                rgd.oname = preDelObj.oname;
                rgd.passno = preDelObj.passno;
                rgd.isreg = preDelObj.isreg;
                rgd.regspecific = preDelObj.regspecific;//MCR/SNB/PRN
                rgd.idno = preDelObj.idno;//MCR/SNB/PRN No.
                rgd.staffid = preDelObj.staffid;//no use in design for this field
                rgd.designation = preDelObj.designation;
                rgd.jobtitle = preDelObj.jobtitle;//if Profession is Allied Health
                rgd.profession = preDelObj.profession;
                rgd.department = preDelObj.department;
                rgd.organization = preDelObj.organization;
                rgd.institution = preDelObj.institution;
                rgd.address1 = preDelObj.address1;
                rgd.address2 = preDelObj.address2;
                rgd.address3 = preDelObj.address3;
                rgd.address4 = preDelObj.address4;
                rgd.city = preDelObj.city;
                rgd.state = preDelObj.state;
                rgd.postalcode = preDelObj.postalcode;
                rgd.country = preDelObj.country;
                rgd.rcountry = preDelObj.rcountry;
                rgd.telcc = preDelObj.telcc;
                rgd.telac = preDelObj.telac;
                rgd.tel = preDelObj.tel;
                rgd.mobilecc = preDelObj.mobilecc;
                rgd.mobileac = preDelObj.mobileac;
                rgd.mobile = preDelObj.mobile;
                rgd.faxcc = preDelObj.faxcc;
                rgd.faxac = preDelObj.faxac;
                rgd.fax = preDelObj.fax;
                rgd.email = preDelObj.email;
                rgd.affiliation = preDelObj.affiliation;
                rgd.dietary = preDelObj.dietary;
                rgd.nationality = preDelObj.nationality;
                rgd.age = preDelObj.age;
                rgd.dob = preDelObj.dob;
                rgd.gender = preDelObj.gender;
                rgd.additional4 = preDelObj.additional4;
                rgd.additional5 = preDelObj.additional5;
                rgd.memberno = preDelObj.memberno;

                rgd.vname = preDelObj.vname;
                rgd.vdob = preDelObj.vdob;
                rgd.vpassno = preDelObj.vpassno;
                rgd.vpassexpiry = preDelObj.vpassexpiry;
                rgd.vpassissuedate = preDelObj.vpassissuedate;
                rgd.vembarkation = preDelObj.vembarkation;
                rgd.varrivaldate = preDelObj.varrivaldate;
                rgd.vcountry = preDelObj.vcountry;

                rgd.udfcname = preDelObj.udfcname;
                rgd.udfdeltype = preDelObj.udfdeltype;
                rgd.udfprofcat = preDelObj.udfprofcat;
                rgd.udfprofcatother = preDelObj.udfprofcatother;
                rgd.udfcpcode = preDelObj.udfcpcode;
                rgd.udfcldept = preDelObj.udfcldept;
                rgd.udfcaddress = preDelObj.udfcaddress;
                rgd.udfclcompany = preDelObj.udfclcompany;
                rgd.udfclcompanyother = preDelObj.udfclcompanyother;
                rgd.udfccountry = preDelObj.udfccountry;

                rgd.supname = preDelObj.supname;
                rgd.supdesignation = preDelObj.supdesignation;
                rgd.supcontact = preDelObj.supcontact;
                rgd.supemail = preDelObj.supemail;

                rgd.othersal = preDelObj.othersal;
                rgd.otherprof = preDelObj.otherprof;
                rgd.otherdept = preDelObj.otherdept;
                rgd.otherorg = preDelObj.otherorg;
                rgd.otherinstitution = preDelObj.otherinstitution;

                rgd.aemail = preDelObj.aemail;
                rgd.isSMS = preDelObj.isSMS;

                rgd.remark = preDelObj.remark;
                rgd.remark_groupupload = preDelObj.remark_groupupload;
                rgd.approvestatus = preDelObj.approvestatus;
                rgd.createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                rgd.recycle = preDelObj.recycle;
                rgd.stage = preDelObj.stage;

                rgd.showID = showid;
                isSuccess = rgd.saveRegDelegate();
            }
            else
            {
                isSuccess = 1;
                groupid = dt.Rows[0]["RegGroupID"].ToString();
                string oldShowID = dt.Rows[0]["ShowID"].ToString();
                if(showid == oldShowID)
                {
                    isMatchOldShowID = true;
                }
                else
                {
                    isMatchOldShowID = false;
                }
            }
            #endregion
        }
        catch(Exception ex)
        { }

        return isSuccess;
    }
    #endregion

    #region btnOK_Click
    protected void btnOK_Click(object sender, EventArgs e)
    {
        Response.Redirect(hfNextRoute.Value);
    }
    #endregion

    #region btnClose_Click (to close modal popup [Close Button Click])
    protected void btnClose_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePop", "closeModal();", true);
    }
    #endregion

    private void CETEDMClickingLog(string showid, string flowid, string groupID)
    {
        try
        {
            if (Request.QueryString["CEI"] != null)
            {
                LogTracking lTrack = new LogTracking(fn);
                lTrack.ShowID = showid;
                lTrack.FlowID = flowid;
                lTrack.TrackCode = Request.QueryString["CEI"].ToString();
                lTrack.RefID = groupID;
                lTrack.saveLog();
            }
        }
        catch { }
    }

    #region bindRegisterButtonCustomText
    private void bindRegisterButtonCustomText(string flowid)
    {
        string regButtonText = fn.GetDataByCommand("Select RegisterButtonText From tb_site_flow_master Where FLW_ID='"
            + flowid + "' And Status='Active'", "RegisterButtonText");
        if (!string.IsNullOrEmpty(regButtonText) && regButtonText != "0")
        {
            btnRegister.Text = regButtonText.Trim();
        }
    }
    #endregion
}