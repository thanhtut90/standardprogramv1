﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Logging;
using System.Data;
using Corpit.Email;
using Corpit.Site.Email;
public partial class SuccessMDA : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cComFuz = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        SetSiteMaster(showid);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);

            insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actsuccess, urlQuery);

            RegGroupObj rgg = new RegGroupObj(fn);
            rgg.updateGroupCurrentStep(urlQuery);

            RegDelegateObj rgd = new RegDelegateObj(fn);
            rgd.updateDelegateCurrentStep(urlQuery);

            string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
            FlowControler flwControl = new FlowControler(fn);
            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);

            StatusSettings stuSettings = new StatusSettings(fn);

            string regstatus = "0";
            DataTable dt = new DataTable();
            bool isRegSuccess = false;
            string mainID = "";
            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
            {
                dt = rgg.getRegGroupByID(GroupRegID, showid);
                if (dt.Rows.Count > 0)
                {
                    regstatus = dt.Rows[0]["RG_Status"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["RG_Status"].ToString()) ? dt.Rows[0]["RG_Status"].ToString() : "0") : "0";
                    //dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                }

                rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                rgd.updateDelegateRegStatus(GroupRegID, stuSettings.Success, showid);
                isRegSuccess = true;
                mainID = GroupRegID;
            }
            else//*flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL
            {
                rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                if (!string.IsNullOrEmpty(DelegateID))
                {
                    dt = rgd.getDataByGroupIDRegno(GroupRegID, DelegateID, showid);
                    if (dt.Rows.Count > 0)
                    {
                        regstatus = dt.Rows[0]["reg_Status"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_Status"].ToString()) ? dt.Rows[0]["reg_Status"].ToString() : "0") : "0";
                        //dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                    }

                    rgd.updateStatus(DelegateID, stuSettings.Success, showid);
                    isRegSuccess = true;
                    mainID = DelegateID;
                }
            }

            #region bindSuccessMessage
            lblSuccessMessage.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowSuccessMsg) ? flwMasterConfig.FlowSuccessMsg : "");
            if (string.IsNullOrEmpty(lblSuccessMessage.Text))
                PanelMsg.Visible = false;
            #endregion

            if (cComFuz.ParseInt(regstatus) == stuSettings.Pending)
            {
                FlowControler flw = new FlowControler(fn, urlQuery);
                //  flw.SendCurrentStepEmail(urlQuery);
                EmailHelper esender = new EmailHelper();
                esender.SendCurrentFlowStepEmail(urlQuery);
            }

            //try
            //{
            //    CommonFuns cFuz = new CommonFuns();
            //    string DID = "";
            //    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
            //    {
            //        DID = GroupRegID;
            //    }
            //    else
            //        DID = DelegateID;
            //    HTMLTemplateControler htmlControl = new HTMLTemplateControler();
            //    string template = htmlControl.CreateAcknowledgeLetterHTMLTemplate(showid, DID);
            //    string rtnFile = htmlControl.CreatePDF(template, showid, DID, "Acknowledge", false);
            //    if (!string.IsNullOrEmpty(rtnFile))
            //    {
            //        ShowPDF.Visible = true;
            //        ShowPDF.Attributes["src"] = rtnFile;
            //    }
            //    if (isRegSuccess == true)
            //    {
            //        SiteSettings sCong = new SiteSettings(fn, showid);
            //        if (!string.IsNullOrEmpty(sCong.RedirectLinkFromSuccess))
            //        {
            //            string url = sCong.RedirectLinkFromSuccess + mainID;
            //            url = string.Format("window.parent.location.href='{0}';", url);
            //            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "scriptid", url, true);
            //        }
            //    }
            //}
            //catch { }

            string emailID = "";
            string MFABUM338 = "";
            string MMACEK336 = "";
            string OSHACPT327 = "";

            MFABUM338 = "140";
            MMACEK336 = "143";
            OSHACPT327 = "137";

            //testing 
            //MFABUM338 = "126";
            //MMACEK336 = "124";
            //OSHACPT327 = "122";


            if (showid == "BUM338")
            {
                emailID = MFABUM338;
            }
            else if (showid == "CEK336")
            {
                emailID = MMACEK336;
            }
            else if (showid == "CPT327")
            {
                emailID = OSHACPT327;
            }
            string flowID = cComFuz.DecryptValue(urlQuery.FlowID);
            EmailDSourceKey sourcKey = new EmailDSourceKey();
            sourcKey.AddKeyToList("ShowID", showid);
            sourcKey.AddKeyToList("RegGroupID", GroupRegID);
            sourcKey.AddKeyToList("Regno", DelegateID);
            sourcKey.AddKeyToList("FlowID", flowID);
            QRcodeMaker qMaker = new QRcodeMaker();
            string url = qMaker.CreateQRcode("D", DelegateID, showid, flowID, emailID, sourcKey);
            if (!string.IsNullOrEmpty(url))
            {
                ImgQR.ImageUrl = url;
                ImgQR.Visible = true;
            }

        }

    }

    #region PageSetting
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
            Page.MasterPageFile = masterPage;
    }

    #endregion

    #region insertLogFlowAction
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        string step = cComFuz.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion
}