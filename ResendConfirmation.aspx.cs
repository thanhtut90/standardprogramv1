﻿using Corpit.Registration;
using Corpit.Site.Email;
using Corpit.Site.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ResendConfirmation : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindShowList();
        }
    }
    private void bindShowList()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show where Status='Active' And SHW_ID='OCX299'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddlShowID.Items.Clear();
                ddlShowID.DataSource = cmd.ExecuteReader();
                ddlShowID.DataTextField = "SHW_Name";
                ddlShowID.DataValueField = "SHW_ID";
                ddlShowID.DataBind();
                ddlShowID.Items.Insert(0, new ListItem("Select Show", "0"));
                con.Close();
            }
        }
    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        if (ddlShowID.SelectedItem.Value != "0")
        {
            string showid = ddlShowID.SelectedItem.Value;
            //////Indiv
            ////string sql = "Select reg_datecreated as d,* From tb_RegDelegate Where ShowID='" + showid + "' And recycle=0 And reg_urlFlowID='F376'"
            ////    + " And reg_Status=1 And RegGroupID Not In (Select RegGroupID From tb_RegGroup Where ShowID='KKS355' And reg_urlFlowID='F377' And recycle=0)"
            ////    + "And reg_datecreated between '2018-09-29 23:59:59' and '2018-10-01 23:59:59'"
            ////    + " Order By reg_datecreated";

            //string sql = "Select recycle,reg_status,reg_Email,reg_urlFlowID,reg_Stage,* From tb_RegDelegate Where ShowID='KKS355' And recycle=0 And reg_urlFlowID='F376'"
            //            + " And Regno In (35510389,35510895,35510904,35510931,35511017,35511686,35511703,35512021"
            //            + " ,35512226,35512227,35512373,35512440,35512534,35512548,35512918,35513003,35513076,35513252)"
            //            + " Order By reg_datecreated Desc";

            string sql = "Select Top 200 reg_Email,reg_urlFlowID,* From tb_RegDelegate Where ShowID='OCX299' And recycle=0 And Regno>=29940000"
                            +" And RegGroupID In (Select RegGroupID From tb_RegGroup Where  ShowID='OCX299' And recycle=0 And RegGroupID>=4000000)"
                            + " And Regno Not In (Select tmp_ResendEmailList.Regno From tmp_ResendEmailList Where ShowID='OCX299')"
                            + " Order By Regno Asc";

            //////Group
            ////string sql = "Select RG_CreatedDate d,* From tb_RegGroup Where ShowID='" + showid + "' And recycle=0 And RG_urlFlowID='F377'"
            ////            + " And RG_Status=1"
            ////            + " And RG_CreatedDate between '2018-09-29 23:59:59' and '2018-10-01 23:59:59'"
            ////            + " Order By RG_CreatedDate";

            //////Public
            ////string sql = "Select reg_datecreated as d,reg_Email,* From tb_RegDelegate Where ShowID='" + showid + "' And recycle=0 And reg_urlFlowID='F378'"
            ////           + " And reg_Status=1 And Regno Not In ('35513724')"
            ////           + " And RegGroupID Not In (Select RegGroupID From tb_RegGroup Where ShowID='KKS355' And reg_urlFlowID='F377' And recycle=0)"
            ////           + " And reg_datecreated between '2018-09-29 23:59:59' and '2018-10-01 23:59:59'"
            ////           + " Order By reg_datecreated";


            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                int sentcount = 0;
                //Response.End();
                ////foreach (DataRow dr in dt.Rows)
                foreach (DataRow drIndiv in dt.Rows)
                {
                    #region group
                    //string groupid = dr["RegGroupID"] != DBNull.Value ? dr["RegGroupID"].ToString() : "";
                    //string flowid = dr["RG_urlFlowID"] != DBNull.Value ? dr["RG_urlFlowID"].ToString() : "";
                    //string emailaddress = dr["RG_ContactEmail"] != DBNull.Value ? dr["RG_ContactEmail"].ToString() : "";
                    //if (!string.IsNullOrEmpty(flowid) && !string.IsNullOrEmpty(emailaddress))
                    //{
                    //    FlowControler flwObj = new FlowControler(fn);
                    //    InvoiceControler invControler = new InvoiceControler(fn);
                    //    EmailHelper eHelper = new EmailHelper();
                    //    string eType = EmailHTMLTemlateType.Confirmation;
                    //    string emailRegType = BackendRegType.backendRegType_Group;// "G";
                    //    EmailDSourceKey sourcKey = new EmailDSourceKey();
                    //    string flowID = flowid;// "F322";
                    //    string groupID = groupid;// "2421113";
                    //    string delegateID = "";// "24210092";//Blank

                    //    sourcKey.AddKeyToList("ShowID", showid);
                    //    sourcKey.AddKeyToList("RegGroupID", groupID);
                    //    sourcKey.AddKeyToList("Regno", delegateID);
                    //    sourcKey.AddKeyToList("FlowID", flowID);
                    //    sourcKey.AddKeyToList("INVID", "");

                    //    string updatedUser = string.Empty;
                    //    if (Session["userid"] != null)
                    //    {
                    //        updatedUser = Session["userid"].ToString();
                    //    }
                    //    eHelper.SendEmailFromBackend(eType, sourcKey, emailRegType, updatedUser);

                    //    #region Individual
                    //    DataTable dtIndiv = fn.GetDatasetByCommand("Select reg_Email,recycle,* From tb_RegDelegate WHere ShowID='" + showid 
                    //        + "' And recycle=0 And reg_urlFlowID='F377' ANd RegGroupID='" + groupID + "'","ds").Tables[0];
                    //    if (dtIndiv.Rows.Count > 0)
                    //    {
                    //        foreach (DataRow drIndiv in dtIndiv.Rows)
                    //        {
                    //            string regno = drIndiv["Regno"].ToString();
                    //            string flowidIndiv = drIndiv["reg_urlFlowID"] != DBNull.Value ? drIndiv["reg_urlFlowID"].ToString() : "";
                    //            string emailaddressIndiv = drIndiv["reg_Email"] != DBNull.Value ? drIndiv["reg_Email"].ToString() : "";
                    //            if (!string.IsNullOrEmpty(flowid) && !string.IsNullOrEmpty(emailaddressIndiv))
                    //            {
                    //                //FlowControler flwObj = new FlowControler(fn);
                    //                //InvoiceControler invControler = new InvoiceControler(fn);
                    //                //EmailHelper eHelper = new EmailHelper();
                    //                string eTypeIndiv = EmailHTMLTemlateType.Confirmation;
                    //                string emailRegTypeIndiv = BackendRegType.backendRegType_Delegate;// "D";
                    //                EmailDSourceKey sourcKeyIndiv = new EmailDSourceKey();
                    //                string delegateIDIndiv = regno;// "24210092";//Blank

                    //                sourcKeyIndiv.AddKeyToList("ShowID", showid);
                    //                sourcKeyIndiv.AddKeyToList("RegGroupID", groupID);
                    //                sourcKeyIndiv.AddKeyToList("Regno", delegateIDIndiv);
                    //                sourcKeyIndiv.AddKeyToList("FlowID", flowID);
                    //                sourcKeyIndiv.AddKeyToList("INVID", "");

                    //                string updatedUserIndiv = string.Empty;
                    //                if (Session["userid"] != null)
                    //                {
                    //                    updatedUserIndiv = Session["userid"].ToString();
                    //                }
                    //                eHelper.SendEmailFromBackend(eTypeIndiv, sourcKeyIndiv, emailRegTypeIndiv, updatedUserIndiv);
                    //            }
                    //        }
                    //    }
                    //    #endregion
                    //}
                    #endregion

                    string updatedUserIndiv = "Send from ResendConfirmation.aspx.cs";
                    if (Session["userid"] != null)
                    {
                        updatedUserIndiv = Session["userid"].ToString();
                    }

                    #region Individual
                    string regno = drIndiv["Regno"].ToString();
                    string groupid = drIndiv["RegGroupID"] != DBNull.Value ? drIndiv["RegGroupID"].ToString() : "";
                    string flowidIndiv = drIndiv["reg_urlFlowID"] != DBNull.Value ? drIndiv["reg_urlFlowID"].ToString() : "";
                    string emailaddressIndiv = drIndiv["reg_Email"] != DBNull.Value ? drIndiv["reg_Email"].ToString() : "";
                    bool isSend = false;
                    if (!string.IsNullOrEmpty(flowidIndiv) && !string.IsNullOrEmpty(emailaddressIndiv))
                    {
                        //Response.End();
                        #region public invoice id
                        //DataTable dtInv = fn.GetDatasetByCommand("Select * From tb_Invoice Where InvOwnerID In ('" 
                        //    + regno + "') And Invoice_status=1", "ds").Tables[0];
                        //if (dtInv.Rows.Count > 0)
                        #endregion
                        {
                            FlowControler flwObj = new FlowControler(fn);
                            InvoiceControler invControler = new InvoiceControler(fn);
                            EmailHelper eHelper = new EmailHelper();
                            string eTypeIndiv = EmailHTMLTemlateType.Confirmation;
                            string emailRegTypeIndiv = BackendRegType.backendRegType_Delegate;// "D";
                            EmailDSourceKey sourcKeyIndiv = new EmailDSourceKey();
                            string groupID = groupid;
                            string delegateIDIndiv = regno;// "24210092";//Blank
                            string invoiceID = "";// dtInv.Rows[0]["InvoiceID"].ToString();

                            sourcKeyIndiv.AddKeyToList("ShowID", showid);
                            sourcKeyIndiv.AddKeyToList("RegGroupID", groupID);
                            sourcKeyIndiv.AddKeyToList("Regno", delegateIDIndiv);
                            sourcKeyIndiv.AddKeyToList("FlowID", flowidIndiv);
                            sourcKeyIndiv.AddKeyToList("INVID", invoiceID);
                            isSend = eHelper.SendEmailFromBackend(eTypeIndiv, sourcKeyIndiv, emailRegTypeIndiv, updatedUserIndiv);
                            sentcount++;
                        }
                    }
                    #endregion
                    string sqlLog = String.Format("Insert Into tmp_ResendEmailList (Regno,RegGroupID,EmailAddress,ShowID,FlowID,Status,SendStatus)"
                        + " Values (N'{0}',N'{1}',N'{2}',N'{3}',N'{4}',N'{5}',N'{6}')",
                        regno, groupid, emailaddressIndiv, showid, flowidIndiv, isSend.ToString(), (isSend.ToString() + ":" + updatedUserIndiv));
                    fn.ExecuteSQL(sqlLog);
                    //Response.End();
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already sent the confirmation email.Count:" + sentcount + "');", true);
            }
        }
        else
        {
            Response.Write("<script>alert('Please select show.');</script>");//window.location='404.aspx';
        }
    }
}