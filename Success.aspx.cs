﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Logging;
using System.Data;
using Corpit.Email;

public partial class Success : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cComFuz = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();
    protected static string _FJShowID = "KKS355";
    protected static string _FoodJapanIndivReg = "F376";
    protected static string _FoodJapanGroupReg = "F377";

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        SetSiteMaster(showid);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);

            insertLogFlowAction(GroupRegID, DelegateID, rlgobj.actsuccess, urlQuery);

            RegGroupObj rgg = new RegGroupObj(fn);
            rgg.updateGroupCurrentStep(urlQuery);

            RegDelegateObj rgd = new RegDelegateObj(fn);
            rgd.updateDelegateCurrentStep(urlQuery);

            string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
            FlowControler flwControl = new FlowControler(fn);
            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);

            StatusSettings stuSettings = new StatusSettings(fn);

            string regstatus = "0";
            DataTable dt = new DataTable();
            bool isRegSuccess = false;
            string mainID = "";
            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
            {
                dt = rgg.getRegGroupByID(GroupRegID, showid);
                if (dt.Rows.Count > 0)
                {
                    regstatus = dt.Rows[0]["RG_Status"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["RG_Status"].ToString()) ? dt.Rows[0]["RG_Status"].ToString() : "0") : "0";
                    //dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                }

                rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                rgd.updateDelegateRegStatus(GroupRegID, stuSettings.Success, showid);
                isRegSuccess = true;
                mainID = GroupRegID;
            }
            else//*flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL
            {
                rgg.updateStatus(GroupRegID, stuSettings.Success, showid);

                if (!string.IsNullOrEmpty(DelegateID))
                {
                    dt = rgd.getDataByGroupIDRegno(GroupRegID, DelegateID, showid);
                    if (dt.Rows.Count > 0)
                    {
                        regstatus = dt.Rows[0]["reg_Status"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_Status"].ToString()) ? dt.Rows[0]["reg_Status"].ToString() : "0") : "0";
                        //dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                    }

                    rgd.updateStatus(DelegateID, stuSettings.Success, showid);
                    isRegSuccess = true;
                    mainID = DelegateID;
                }
            }

            #region bindSuccessMessage
            lblSuccessMessage.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowSuccessMsg) ? flwMasterConfig.FlowSuccessMsg : "");

            bool isFJOnsiteRegistraion = false;
            if (showid == _FJShowID)
            {
                bindFJTradeMsg(DelegateID, GroupRegID, flowid, showid, ref isFJOnsiteRegistraion);/*added on 23-10-2019*/
            }
            if (string.IsNullOrEmpty(lblSuccessMessage.Text))
                PanelMsg.Visible = false;
            #endregion
            //[2019-04-03,Sithu] Email not sending Invoice to Credit issue.
            /* if (cComFuz.ParseInt(regstatus) == stuSettings.Pending)
             {}*/
            if (showid == _FJShowID)
            {
                if(isFJOnsiteRegistraion == false)
                {
                    FlowControler flw = new FlowControler(fn, urlQuery);
                    //  flw.SendCurrentStepEmail(urlQuery);
                    EmailHelper esender = new EmailHelper();
                    esender.SendCurrentFlowStepEmail(urlQuery);
                }
            }
            else
            {
                FlowControler flw = new FlowControler(fn, urlQuery);
                //  flw.SendCurrentStepEmail(urlQuery);
                EmailHelper esender = new EmailHelper();
                esender.SendCurrentFlowStepEmail(urlQuery);
            }

            try
            {
                CommonFuns cFuz = new CommonFuns();
                string DID = "";
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    DID = GroupRegID;
                }
                else
                    DID = DelegateID;
                HTMLTemplateControler htmlControl = new HTMLTemplateControler();
                string template = htmlControl.CreateAcknowledgeLetterHTMLTemplate(showid, DID);
                string rtnFile = htmlControl.CreatePDF(template, showid, DID, "Acknowledge", false);
                if (!string.IsNullOrEmpty(rtnFile))
                {
                    ShowPDF.Visible = true;
                    ShowPDF.Attributes["src"] = rtnFile;
                }
                if (isRegSuccess == true)
                {
                    SiteSettings sCong = new SiteSettings(fn, showid);
                    if (!string.IsNullOrEmpty(sCong.RedirectLinkFromSuccess))
                    {
                        string url = sCong.RedirectLinkFromSuccess + mainID;
                        url = string.Format("window.parent.location.href='{0}';", url);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "scriptid", url, true);
                    }
                }
            }
            catch { }
        }
    }

    #region PageSetting
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
            Page.MasterPageFile = masterPage;
    }

    #endregion

    #region insertLogFlowAction
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        string step = cComFuz.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    #region Food Japan
    private void bindFJTradeMsg(string regno, string groupid, string flowid, string showid, ref bool isFJOnsiteRegistraion)
    {
        try
        {
            if(flowid == _FoodJapanIndivReg || flowid == _FoodJapanGroupReg)
            {
                DateTime regDeadlineDate = cComFuz.ParseDateTime("30/10/2019 23:59:59");
                DateTime RegDate = DateTime.Today;
                RegDelegateObj rgd = new RegDelegateObj(fn);
                string RegisteredDate = rgd.getRegDate(regno, groupid, showid);
                if (!string.IsNullOrEmpty(RegisteredDate))
                    RegDate = DateTime.Parse(RegisteredDate);
                if (DateTime.Compare(RegDate, regDeadlineDate) > 0)
                {
                    isFJOnsiteRegistraion = true;
                    lblSuccessMessage.Text = "<div style='font-size:large;font-weight:bold;text-align:center;'><br /><br /><br />Thank you for your registration. Please proceed to counter to make payment and have your badge printed out.</div>";
                }
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion
}