﻿using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Corpit.Promo;
using Corpit.Logging;
using Corpit.BackendMaster;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using Corpit.RegOnsite;

public partial class RegDelegateOnsiteSSBFEEdit : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string regno = cFun.DecryptValue(urlQuery.DelegateID);
                bindData(regno, groupid, showid);
            }
            else
            {
                string url = "~/Onsite/OnsiteSearch.aspx?Evt=" + cFun.EncryptValue(showid);
                Response.Redirect(url);
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = "RegistrationOnsite.master";
        }
    }
    #endregion

    #region btnSave_Click (create one record (just RegGroupID and recycle=0) into tb_RegGroup table if current groupid not exist in tb_RegGroup & save/update data into tb_RegDelegate table & get next route and redirect to next page according to the site flow settings (tb_site_flow table))
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                CommonFuns cFun = new CommonFuns();
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string regno = cFun.DecryptValue(urlQuery.DelegateID);
                updateSSBFE(regno, groupid, showid);
                string url = "https://www.event-reg.biz/registration/onsite/OnsiteSearch.aspx?Evt=" + cFun.EncryptValue(showid);
                Response.Redirect(url);
            }
            else
            {
                string url = "~/Onsite/OnsiteSearch.aspx?Evt=" + cFun.EncryptValue(showid);
                Response.Redirect(url);
            }
        }
    }
    #endregion

    #region updateSSBFE
    private void updateSSBFE(string regno, string reggroupid, string showid)
    {
        if (Page.IsValid)
        {
            try
            {
                #region insert additional
                string fname = txtFName.Text.Trim();
                string lname = txtLName.Text.Trim();
                DataTable dt = new DataTable();
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);
                if (!string.IsNullOrEmpty(regno))
                {
                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    dt = rgd.getDataByGroupIDRegno(reggroupid, regno, showid);
                    if (dt.Rows.Count > 0)
                    {
                        string sqlUpdatePostConf = "Update tb_RegDelegate Set reg_FName=@reg_FName ,reg_LName=@reg_LName "
                                    + " Where ShowID=@ShowID And Regno=@Regno And RegGroupID=@RegGroupID";
                        using (SqlConnection con = new SqlConnection(fn.ConnString))
                        {
                            using (SqlCommand command = new SqlCommand(sqlUpdatePostConf))
                            {
                                command.Connection = con;
                                command.Parameters.AddWithValue("@ShowID", showid);
                                command.Parameters.AddWithValue("@Regno", regno);
                                command.Parameters.AddWithValue("@RegGroupID", reggroupid);
                                command.Parameters.AddWithValue("@reg_FName", fname);
                                command.Parameters.AddWithValue("@reg_LName", lname);
                                con.Open();
                                int isSuccess = command.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                    }
                    else
                    {
                        string url = "~/Onsite/OnsiteSearch.aspx?Evt=" + cFun.EncryptValue(showid);
                        Response.Redirect(url);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            { }
        }
    }
    private void bindData(string regno, string reggroupid, string showid)
    {
        try
        {
            DataTable dt = new DataTable();
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            if (!string.IsNullOrEmpty(regno))
            {
                RegDelegateObj rgd = new RegDelegateObj(fn);
                dt = rgd.getDataByGroupIDRegno(reggroupid, regno, showid);
                if (dt.Rows.Count > 0)
                {
                    string reg_fname = dt.Rows[0]["reg_FName"].ToString();
                    txtFName.Text = reg_fname;
                    string reg_lname = dt.Rows[0]["reg_LName"].ToString();
                    txtLName.Text = reg_lname;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
}