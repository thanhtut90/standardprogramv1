﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Logging;
using System.Data;
using Corpit.Email;
using Corpit.Site.Email;
using System.Net.Mail;
using System.Text;

public partial class Reg_SuccessVMDA : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cComFuz = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();
    protected static string emailaddressOrg = "than.htut.cs@gmail.com";

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        SetSiteMaster(showid);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string previousUrl = Request.UrlReferrer.ToString();
            //Response.End();

            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
            if (fmaster != null)
            {
                txtFlowName.Text = fmaster.FlowName;
                if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
                {
                    string groupid = Session["Groupid"].ToString();
                    flowid = Session["Flowid"].ToString();
                    string showid = Session["Showid"].ToString();
                    string currentstage = cComFuz.DecryptValue(urlQuery.CurrIndex);
                    string DelegateID = string.Empty;
                    if (Session["Regno"] != null)
                    {
                        DelegateID = Session["Regno"].ToString();
                    }

                    insertRegLoginAction(groupid, rlgobj.actview);

                    RegGroupObj rgg = new RegGroupObj(fn);
                    //rgg.updateGroupCurrentStep(urlQuery);

                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    rgd.updateDelegateCurrentStep(urlQuery);

                    int totalNewSubmissionRecordCount = 0;
                    string newSubmissionList = getNewSubmissionList(groupid, flowid, showid, ref totalNewSubmissionRecordCount);

                    FlowControler flwControl = new FlowControler(fn);
                    FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);

                    StatusSettings stuSettings = new StatusSettings(fn);

                    string regstatus = "0";
                    DataTable dt = new DataTable();
                    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                    {
                        dt = rgg.getRegGroupByID(groupid, showid);
                        if (dt.Rows.Count > 0)
                        {
                            regstatus = dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                        }

                        rgg.updateStatus(groupid, stuSettings.Success, showid);

                        rgd.updateDelegateRegStatus(groupid, stuSettings.Success, showid);
                    }
                    else//*flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL
                    {
                        rgg.updateStatus(groupid, stuSettings.Success, showid);

                        if (!string.IsNullOrEmpty(DelegateID))
                        {
                            dt = rgd.getDataByGroupIDRegno(groupid, DelegateID, showid);
                            if (dt.Rows.Count > 0)
                            {
                                regstatus = dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                            }

                            rgd.updateStatus(DelegateID, stuSettings.Success, showid);
                        }
                    }

                    #region bindSuccessMessage
                    lblSuccessMessage.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowSuccessMsg) ? flwMasterConfig.FlowSuccessMsg : "");
                    if (string.IsNullOrEmpty(lblSuccessMessage.Text))
                        PanelMsg.Visible = false;
                    #endregion

                    #region SendCurrentFlowStepEmail for Asean Summit Group Reg(whether already sent or not send, send again))
                    if (totalNewSubmissionRecordCount > 0)
                    {
                        string updatedUser = string.Empty;
                        if (Session["Groupid"] != null)
                        {
                            updatedUser = Session["Groupid"].ToString();
                        }
                        string emailaddressExh = getGroupEmail(groupid, showid);
                        
                        ShowControler shwCtr = new ShowControler(fn);
                        Show shw = shwCtr.GetShow(showid);
                        string showName = shw.SHW_Name;
                        string isEmailSend = sendNoficationEmail(emailaddressExh, emailaddressOrg, groupid, showName, newSubmissionList);
                        #endregion

                        #region Logging
                        LogGenEmail lggenemail = new LogGenEmail(fn);
                        LogActionObj lactObj = new LogActionObj();
                        lggenemail.type = LogType.generalType;
                        lggenemail.RefNumber = groupid + "," + DelegateID;
                        lggenemail.description = "Send Notification email to " + emailaddressExh + ";" + emailaddressOrg;
                        lggenemail.remark = "Send Notification email to " + emailaddressExh + ";" + emailaddressOrg + " and email(email sending status:" + isEmailSend + ")";
                        lggenemail.step = "Reg_SuccessVMDA.aspx";
                        lggenemail.writeLog();
                        #endregion
                    }
                    try
                    {
                        CommonFuns cFuz = new CommonFuns();
                        string DID = "";
                        if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                        {
                            DID = groupid;
                        }
                        else
                            DID = DelegateID;
                        #region Comment on 20-1-2020 th
                        //HTMLTemplateControler htmlControl = new HTMLTemplateControler();
                        //string template = htmlControl.CreateAcknowledgeLetterHTMLTemplate(showid, DID);
                        //string rtnFile = htmlControl.CreatePDF(template, showid, DID, "Acknowledge");
                        //if (!string.IsNullOrEmpty(rtnFile))
                        //{
                        //    ShowPDF.Visible = true;
                        //    ShowPDF.Attributes["src"] = rtnFile;
                        //}
                        #endregion
                    }
                    catch { }

                    Response.Redirect(previousUrl);//*****
                }
                else
                {
                    Response.Redirect("ReLogin.aspx?Event=" + txtFlowName.Text.Trim());
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region PageSetting
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
            Page.MasterPageFile = masterPage;
    }

    #endregion

    #region insertRegLoginAction (insert flow data into tb_Log_RegLogin table)
    private void insertRegLoginAction(string groupid, string action)
    {
        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
        {
            string flowid = Session["Flowid"].ToString();
            string showid = Session["Showid"].ToString();
            string currentstage = Session["regstage"].ToString();
            LogRegLogin rlg = new LogRegLogin(fn);
            rlg.reglogin_regno = groupid;
            rlg.reglogin_flowid = flowid;
            rlg.reglogin_step = currentstage;
            rlg.reglogin_action = action;
            rlg.saveLogRegLogin();
        }
        else
        {
            Response.Redirect("ReLogin.aspx?Event=" + txtFlowName.Text.Trim());
        }
    }
    #endregion

    #region sendNoficationEmail
    public string sendNoficationEmail(string emailaddressExh, string emailaddressOrg, string RegGroupID, string showName, string newSubmissionList)
    {
        string isSuccess = "F";
        try
        {
            Alpinely.TownCrier.TemplateParser tm = new Alpinely.TownCrier.TemplateParser();
            Alpinely.TownCrier.MergedEmailFactory factory = new Alpinely.TownCrier.MergedEmailFactory(tm);
            string template = HttpContext.Current.Server.MapPath("~/Template/MDA_VIPNewSubmissionEmailTemplate.html");

            var tokenValues = new Dictionary<string, string>
                {
                    {"RegGroupID",RegGroupID},
                    {"NewSubmissionList",newSubmissionList},
                };
            MailMessage message = factory
            .WithTokenValues(tokenValues)
            .WithHtmlBodyFromFile(template)
            .Create();

            //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//***
            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;//***

            var from = new MailAddress("registration@event-reg.biz", "Medical Fair Asia 2020");
            message.From = from;
            message.To.Add(emailaddressExh);
            message.To.Add(emailaddressOrg);
            message.Bcc.Add("event@corpit.com.sg");
            message.Subject = "New Submission Notification from " + showName;
            SmtpClient emailClient = new SmtpClient("smtp.gmail.com");
            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("registration@event-reg.biz", "c0rpitblasT");
            emailClient.UseDefaultCredentials = false;
            emailClient.EnableSsl = true;
            //emailClient.Port = 587;//***
            emailClient.Credentials = SMTPUserInfo;
            emailClient.Send(message);

            isSuccess = "T";
        }
        catch (Exception ex)
        {
            isSuccess = ex.Message;// "F";
        }
        return isSuccess;
    }
    #endregion
    #region getNewSubmissionList
    private string getNewSubmissionList(string regGroupID, string flowid, string showid, ref int totalNewRecordCount)
    {
        string htmlTemplate = "";
        try
        {
            string sql="Select recycle,reg_urlflowid,reg_Status,reg_Stage,reg_Email,reg_approveStatus,* From tb_RegDelegate Where ShowID='" + showid + "' "
                        + " And recycle=0 And reg_urlFlowID='" + flowid + "' And RegGroupID='" + regGroupID + "' And (reg_Status Is Null Or reg_Status='0')";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            totalNewRecordCount = dt.Rows.Count;
            if (totalNewRecordCount > 0)
            {
                StringBuilder str = new StringBuilder();
                str.Append("<table border='1'>");
                str.Append("<tr>");
                str.Append("<td><b>VIP / Special Guest ID</b></td>");
                str.Append("<td><b>Full Name</b></td>");
                str.Append("<td><b>Company</b></td>");
                str.Append("<td><b>Email</b></td>");
                str.Append("</tr>");
                foreach(DataRow dr in dt.Rows)
                {
                    str.Append("<tr>");
                    str.Append("<td>" + dr["Regno"] + "</td>");
                    str.Append("<td>" + dr["reg_FName"] + "</td>");
                    str.Append("<td>" + dr["reg_Address1"] + "</td>");
                    str.Append("<td>" + dr["reg_Email"] + "</td>");
                    str.Append("</tr>");
                }
                str.Append("</table>");
                htmlTemplate = str.ToString();
            }
        }
        catch(Exception ex)
        { }

        return htmlTemplate;
    }
    #endregion
    #region getGroupEmail
    private string getGroupEmail(string groupid, string showid)
    {
        string result = "";
        RegGroupObj rgg = new RegGroupObj(fn);
        DataTable dt = rgg.getRegGroupByID(groupid, showid);
        if (dt.Rows.Count > 0)
        {
            result = dt.Rows[0]["RG_ContactEmail"].ToString();
        }
        return result;
    }
    #endregion
}