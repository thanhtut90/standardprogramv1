﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logout : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session.Clear();
            Session.Abandon();
            removeCookie();

            string eventName = Request.QueryString["Event"].ToString();
            string page = ReLoginStaticValueClass.regLogoutPage;
            string param = page + "?Event=" + eventName;
            var url = "https://www.event-reg.biz/registration/" + param;

            Response.Clear();
            var sb = new System.Text.StringBuilder();
            sb.Append("<html>");
            sb.AppendFormat("<body onload='document.forms[0].submit()'>");
            sb.AppendFormat("<form action='{0}' method='post'>", url);
            sb.Append("</form>");
            sb.Append("</body>");
            sb.Append("</html>");
            Response.Write(sb.ToString());
            Response.End();
        }
    }

    public void removeCookie()
    {
        HttpCookie aCookie;
        string cookieName;
        int limit = Request.Cookies.Count;
        for (int i = 0; i < limit; i++)
        {
            cookieName = Request.Cookies[i].Name;
            aCookie = new HttpCookie(cookieName);
            aCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(aCookie);
        }
    }
    private string GetEventNameByShowIDFlowID(string showid, string flowid)
    {
        string flowName = string.Empty;
        try
        {
            string sql = "select FLW_Name from tb_site_flow_master  where ShowID=@ShowID And FLW_ID=@FLW_ID and Status=@Status";
            //'EAD351'//FLW_Name=@FName 

            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FLW_ID", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            spar3.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                flowName = dList.Rows[0]["FLW_Name"].ToString();
            }
        }
        catch { }
        return flowName;
    }
}