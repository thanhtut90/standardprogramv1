﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RegistrationOnsite.master" AutoEventWireup="true" CodeFile="RegDelegateOnsiteWCMHAddionalEdit.aspx.cs" Inherits="RegDelegateOnsiteWCMHAddionalEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .red
        {
            color:red;
        }
        /*.chkFooter
        {
            white-space:nowrap;
        }*/
        .chkFooter label
        {
            font-weight:unset !important;
            padding-left:5px !important;
            vertical-align:top !important;
        }
        .chkFooter input[type="checkbox"]
        {
            margin-top: 2px !important;
        }
        a:hover
        {
            text-decoration:none !important;
        }
        .rowLabel
        {
            padding-top: 7px;
        }
    </style>

    <script type="text/javascript">
        function validateDate(elementRef) {
                var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

                var dateValue = elementRef.value;

                if (dateValue.length != 10) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                // mm/dd/yyyy format... 
                var valueArray = dateValue.split('/');

                if (valueArray.length != 3) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthValue = parseFloat(valueArray[1]);
                var dayValue = parseFloat(valueArray[0]);
                var yearValue = parseFloat(valueArray[2]);

                if ((isNaN(monthValue) == true) || (isNaN(dayValue) == true) || (isNaN(yearValue) == true)) {
                    alert('Non-numeric entry detected\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                if (((yearValue % 4) == 0) && (((yearValue % 100) != 0) || ((yearValue % 400) == 0)))
                    monthDays[1] = 29;
                else
                    monthDays[1] = 28;

                if ((monthValue < 1) || (monthValue > 12)) {
                    alert('Invalid month entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthDaysArrayIndex = monthValue - 1;
                if ((dayValue < 1) || (dayValue > monthDays[monthDaysArrayIndex])) {
                    alert('Invalid day entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

            return true;
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-3 col-md-offset-1 rowLabel">Please select post conference tour.</div>
        <div class="col-md-4">
            <asp:RadioButtonList ID="rbItems" runat="server" CssClass="vertical-radio-buttons">
                <asp:ListItem Text="Asian Civilisation Museum" Value="Asian Civilisation Museum"></asp:ListItem>
                <asp:ListItem Text="Singapore Maritime Gallery" Value="Singapore Maritime Gallery"></asp:ListItem>
                <asp:ListItem Text="Royal Albatross" Value="Royal Albatross"></asp:ListItem>
                <asp:ListItem Text="Maritime Experiential Museum" Value="Maritime Experiential Museum"></asp:ListItem>
                <asp:ListItem Text="S.E.A Aquarium" Value="S.E.A Aquarium"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div class="col-md-2">
                <asp:RequiredFieldValidator
                    ID="rfvItems" Enabled="false"
                    runat="server" ForeColor="Red"
                    ControlToValidate="rbItems"
                    ErrorMessage="*Required">
                </asp:RequiredFieldValidator>
        </div>
    </div>
    <br />
    <br />
    <div class="row">
        <div class="col-md-3 col-md-offset-1 rowLabel">Which breakout session are you most interested in?</div>
        <div class="col-md-5">
            <asp:RadioButtonList ID="rbBreakoutSessions" runat="server" CssClass="vertical-radio-buttons">
                <asp:ListItem Text="Asia Pacific - Regional Breakout" Value="Asia Pacific - Regional Breakout"></asp:ListItem>
                <asp:ListItem Text="Indian Ocean - Regional Breakout" Value="Indian Ocean - Regional Breakout"></asp:ListItem>
                <asp:ListItem Text="Atlantic Ocean - Regional Breakout" Value="Atlantic Ocean - Regional Breakout"></asp:ListItem>
                <asp:ListItem Text='"Today is the Future of the Past" World Maritime Heritage Society' Value='"Today is the Future of the Past" World Maritime Heritage Society'></asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div class="col-md-2">
                <asp:RequiredFieldValidator
                    ID="rfvBreakoutSessions" Enabled="false"
                    runat="server" ForeColor="Red"
                    ControlToValidate="rbBreakoutSessions"
                    ErrorMessage="*Required">
                </asp:RequiredFieldValidator>
        </div>
    </div>

    <div class="form-group">
        <div class="form-group container" style="padding-top:20px;">
            <asp:Panel runat="server" ID="PanelWithoutBack" Visible="true" >
                <div class="col-lg-offset-5 col-lg-3 col-sm-offset-4 col-sm-3 center-block" >
                    <asp:Button runat="server" ID="btnSave" CssClass="btn MainButton btn-block" Text="Next" OnClick="btnSave_Click" />
                </div>
            </asp:Panel>
        </div>
    </div>

</asp:Content>

