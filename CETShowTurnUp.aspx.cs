﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CETShowTurnUp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnShow_Click(object sender, EventArgs e)
    {
        Functionality fn = new Functionality();
        try
        {
            string sql = "Select Count(*) TurnUpTotalNumber "
                        + " From GetRegIndivAllByShowID('YAB380') Where Invoice_status=1 "
                        + " And Regno In (Select tb_Onsite_PrintLog.PRegno From tb_Onsite_PrintLog Where ShowID='YAB380')";
            lblTotalNumber.Text = fn.GetDataByCommand(sql, "TurnUpTotalNumber");
        }
        catch(Exception ex)
        { }
    }
}