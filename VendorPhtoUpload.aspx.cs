﻿using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Corpit.Promo;
using Corpit.Logging;
using Corpit.BackendMaster;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text.RegularExpressions;

public partial class VendorPhtoUpload : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";

    private static string[] checkingFJShowName = new string[] { "Food Japan" };
    private static string[] checkingMDAShowName = new string[] { "MFA 2018", "MMA 2018", "OSHA 2018" };
    private static string[] checkingOSEAShowName = new string[] { "OSEA 2018" };
    static string _SingaporeCitizen = "SINGAPORE CITIZEN";
    static string dupeCheckingField = "reg_PassNo";
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string regno = cFun.DecryptValue(urlQuery.DelegateID);

                if (Request.Params["t"] != null)
                {
                    string admintype = cFun.DecryptValue(Request.QueryString["t"].ToString());
                    if (admintype == BackendStaticValueClass.isAdmin)
                    {
                        btnSave.Visible = false;
                        btnSave.Enabled = false;
                    }
                    else
                    {
                        bindPageLoad(showid, flowid, groupid, regno, urlQuery);
                    }
                }
                else
                {
                    bindPageLoad(showid, flowid, groupid, regno, urlQuery);
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region bindPageLoad
    private void bindPageLoad(string showid, string flowid, string groupid, string regno, FlowURLQuery urlQuery)
    {
        btnSave.Visible = true;
        btnSave.Enabled = true;

        CommonFuns cFun = new CommonFuns();

        if (!String.IsNullOrEmpty(groupid))
        {
            populateUserDetails(groupid, regno, showid, flowid);

            FlowControler fCon = new FlowControler(fn);
            FlowMaster flwMaster = fCon.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
			if (flwMaster.FlowType == SiteFlowType.FLOW_GROUP)
            {
                PanelShowBackButton.Visible = true;
                PanelWithoutBack.Visible = false;
            }
            else
            {
                PanelWithoutBack.Visible = true;
                PanelShowBackButton.Visible = false;
            }
        }
        else
        {
            Response.Redirect("DefaultRegIndex.aspx");
        }
    }
    #endregion

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region populateUserDetails (get all relevant data according to RegGroupID and Regno from tb_RegDelegate and bind data to the respective controls)
    private void populateUserDetails(string id, string regno, string showid, string flowid)
    {
        DataTable dt = new DataTable();
        ShowControler shwCtr = new ShowControler(fn);
        Show shw = shwCtr.GetShow(showid);
        if (!string.IsNullOrEmpty(regno))
        {
            RegDelegateObj rgd = new RegDelegateObj(fn);
            dt = rgd.getDataByGroupIDRegno(id, regno, showid);
            if (dt.Rows.Count != 0)
            {
                //string regno = dt.Rows[0]["Regno"].ToString();
                string reggroupid = dt.Rows[0]["RegGroupID"].ToString();
                string con_categoryid = dt.Rows[0]["con_CategoryId"].ToString();
                string reg_salutation = dt.Rows[0]["reg_Salutation"].ToString();
                string reg_fname = dt.Rows[0]["reg_FName"].ToString();
                string reg_lname = dt.Rows[0]["reg_LName"].ToString();
                string reg_oname = dt.Rows[0]["reg_OName"].ToString();
                string passno = dt.Rows[0]["reg_PassNo"].ToString();
                string reg_isreg = dt.Rows[0]["reg_isReg"].ToString();
                string reg_sgregistered = dt.Rows[0]["reg_sgregistered"].ToString();
                string reg_idno = dt.Rows[0]["reg_IDno"].ToString();
                string reg_staffid = dt.Rows[0]["reg_staffid"].ToString();
                string reg_designation = dt.Rows[0]["reg_Designation"].ToString();
                string reg_profession = dt.Rows[0]["reg_Profession"].ToString();
                string reg_jobtitle_alliedstu = dt.Rows[0]["reg_Jobtitle_alliedstu"].ToString();
                string reg_department = dt.Rows[0]["reg_Department"].ToString();
                string reg_organization = dt.Rows[0]["reg_Organization"].ToString();
                string reg_institution = dt.Rows[0]["reg_Institution"].ToString();
                string reg_address1 = dt.Rows[0]["reg_Address1"].ToString();
                string reg_address2 = dt.Rows[0]["reg_Address2"].ToString();
                string reg_address3 = dt.Rows[0]["reg_Address3"].ToString();
                string reg_address4 = dt.Rows[0]["reg_Address4"].ToString();
                string reg_city = dt.Rows[0]["reg_City"].ToString();
                string reg_state = dt.Rows[0]["reg_State"].ToString();
                string reg_postalcode = dt.Rows[0]["reg_PostalCode"].ToString();
                string reg_country = dt.Rows[0]["reg_Country"].ToString();
                string reg_rcountry = dt.Rows[0]["reg_RCountry"].ToString();
                string reg_telcc = dt.Rows[0]["reg_Telcc"].ToString();
                string reg_telac = dt.Rows[0]["reg_Telac"].ToString();
                string reg_tel = dt.Rows[0]["reg_Tel"].ToString();
                string reg_mobcc = dt.Rows[0]["reg_Mobcc"].ToString();
                string reg_mobac = dt.Rows[0]["reg_Mobac"].ToString();
                string reg_mobile = dt.Rows[0]["reg_Mobile"].ToString();
                string reg_faxcc = dt.Rows[0]["reg_Faxcc"].ToString();
                string reg_faxac = dt.Rows[0]["reg_Faxac"].ToString();
                string reg_fax = dt.Rows[0]["reg_Fax"].ToString();
                string reg_email = dt.Rows[0]["reg_Email"].ToString();
                string reg_affiliation = dt.Rows[0]["reg_Affiliation"].ToString();
                string reg_dietary = dt.Rows[0]["reg_Dietary"].ToString();
                string reg_nationality = dt.Rows[0]["reg_Nationality"].ToString();
                string reg_membershipno = dt.Rows[0]["reg_Membershipno"].ToString();
                string reg_vname = dt.Rows[0]["reg_vName"].ToString();
                string reg_vdob = dt.Rows[0]["reg_vDOB"].ToString();
                string reg_vpassno = dt.Rows[0]["reg_vPassno"].ToString();
                string reg_vpassexpiry = dt.Rows[0]["reg_vPassexpiry"].ToString();
                string reg_vpassissuedate = dt.Rows[0]["reg_vIssueDate"].ToString();
                string reg_vembarkation = dt.Rows[0]["reg_vEmbarkation"].ToString();
                string reg_varrivaldate = dt.Rows[0]["reg_vArrivalDate"].ToString();
                string reg_vcountry = dt.Rows[0]["reg_vCountry"].ToString();
                string udf_delegatetype = dt.Rows[0]["UDF_DelegateType"].ToString();
                string udf_profcategory = dt.Rows[0]["UDF_ProfCategory"].ToString();
                string udf_profcategoryother = dt.Rows[0]["UDF_ProfCategoryOther"].ToString();
                string udf_cname = dt.Rows[0]["UDF_CName"].ToString();
                string udf_cpcode = dt.Rows[0]["UDF_CPcode"].ToString();
                string udf_cldepartment = dt.Rows[0]["UDF_CLDepartment"].ToString();
                string udf_caddress = dt.Rows[0]["UDF_CAddress"].ToString();
                string udf_clcompany = dt.Rows[0]["UDF_CLCompany"].ToString();
                string udf_clcompanyother = dt.Rows[0]["UDF_CLCompanyOther"].ToString();
                string udf_ccountry = dt.Rows[0]["UDF_CCountry"].ToString();
                string reg_supervisorname = dt.Rows[0]["reg_SupervisorName"].ToString();
                string reg_supervisordesignation = dt.Rows[0]["reg_SupervisorDesignation"].ToString();
                string reg_supervisorcontact = dt.Rows[0]["reg_SupervisorContact"].ToString();
                string reg_supervisoremail = dt.Rows[0]["reg_SupervisorEmail"].ToString();
                string reg_salutationothers = dt.Rows[0]["reg_SalutationOthers"].ToString();
                string reg_otherprofession = dt.Rows[0]["reg_otherProfession"].ToString();
                string reg_otherdepartment = dt.Rows[0]["reg_otherDepartment"].ToString();
                string reg_otherorganization = dt.Rows[0]["reg_otherOrganization"].ToString();
                string reg_otherinstitution = dt.Rows[0]["reg_otherInstitution"].ToString();
                string reg_aemail = dt.Rows[0]["reg_aemail"].ToString();
                string reg_remark = dt.Rows[0]["reg_remark"].ToString();
                string reg_remarkgupload = dt.Rows[0]["reg_remarkGUpload"].ToString();
                string reg_issms = dt.Rows[0]["reg_isSMS"].ToString();
                string reg_approvestatus = dt.Rows[0]["reg_approveStatus"].ToString();
                string reg_datecreated = dt.Rows[0]["reg_datecreated"].ToString();
                string recycle = dt.Rows[0]["recycle"].ToString();
                string reg_stage = dt.Rows[0]["reg_Stage"].ToString();

                string reg_age = dt.Rows[0]["reg_Age"].ToString();
                string reg_dob = dt.Rows[0]["reg_DOB"].ToString();
                string reg_gender = dt.Rows[0]["reg_Gender"].ToString();
                string reg_additional4 = dt.Rows[0]["reg_Additional4"].ToString();
                string reg_additional5 = dt.Rows[0]["reg_Additional5"].ToString();

                txtPassNo.Text = passno;

                #region Comment (Student Upload)
                //SetUpController setupCtr = new SetUpController(fn);
                //DataTable dtUpload = setupCtr.getFileUploadByID(regno, showid, FileUploadType.studentIDImg);
                //if (dtUpload.Rows.Count > 0)
                //{
                //    string imgfilename = dtUpload.Rows[0]["FileName"].ToString();

                //    if (!string.IsNullOrEmpty(imgfilename) && imgfilename != Number.Zero)
                //    {
                //        string path = "~/FileUpload/" + showid;
                //        hpStudentUpload.NavigateUrl = path + "/" + imgfilename;
                //        hpStudentUpload.Text = imgfilename;
                //        hpStudentUpload.Visible = true;
                //    }
                //}
                #endregion
                #region Photo Upload
                SetUpController setupCtr = new SetUpController(fn);
                DataTable dtUpload = setupCtr.getFileUploadByID(regno, showid, FileUploadType.delegatePhotoImg);
                if (dtUpload.Rows.Count > 0)
                {
                    string imgfilename = dtUpload.Rows[0]["FileName"].ToString();

                    if (!string.IsNullOrEmpty(imgfilename) && imgfilename != Number.Zero)
                    {
                        string path = "~/FileUpload/" + showid;
                        hpPhotoUpload.NavigateUrl = path + "/" + imgfilename;
                        hpPhotoUpload.Text = imgfilename;
                        hpPhotoUpload.Visible = true;
                    }
                }
                #endregion
            }
        }
    }
    #endregion

    #region btnSave_Click (create one record (just RegGroupID and recycle=0) into tb_RegGroup table if current groupid not exist in tb_RegGroup & save/update data into tb_RegDelegate table & get next route and redirect to next page according to the site flow settings (tb_site_flow table))
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string errMsg = string.Empty;
            if (!validateUpload(fupPhotoUpload, hpPhotoUpload, ref errMsg))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errMsg + "');", true);
                return;
            }

            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            FlowControler fCon = new FlowControler(fn);
            FlowMaster flwMaster = fCon.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
            if (!string.IsNullOrEmpty(showid))
            {
                string groupid = string.Empty;
                Boolean hasid = false;

                RegDelegateObj rgd = new RegDelegateObj(fn);
                groupid = cFun.DecryptValue(urlQuery.GoupRegID);

                string regno = Number.Zero;
                regno = cFun.DecryptValue(urlQuery.DelegateID);
                if (String.IsNullOrEmpty(regno))
                {
                    Response.Redirect("404.aspx");
                }
                else
                {
                    hasid = true;
                }
                try
                {
                    if (hasid)
                    {
                        //***updated by th on 15-7-2018(because groupid may change)
                        string newUrlQuery = "FLW=" + urlQuery.FlowID + "&STP=" + urlQuery.CurrIndex
                            + "&GRP=" + cFun.EncryptValue(groupid) + "&INV=" + cFun.EncryptValue(regno) + "&SHW=" + urlQuery.CurrShowID;
                        urlQuery = new FlowURLQuery(newUrlQuery);
                        //***updated by th on 15-7-2018

                        string passno = txtPassNo.Text.Trim();
                        //***Added on 2-10-2018
                        string datepor = DateTime.Now.ToString("ddMMyyyy");
                        string Timepor = DateTime.Now.ToString("hhmmss");
                        string photofilename = passno + "_" + datepor + "_" + Timepor;
                        SaveUpload(regno, showid, FileUploadType.delegatePhotoImg, photofilename, ref fupPhotoUpload, ref hpPhotoUpload);//***[added 31-8-2018]

                        FlowControler flwObj = new FlowControler(fn, urlQuery);
                        string showID = urlQuery.CurrShowID;
                        string page = flwObj.NextStepURL();
                        string step = flwObj.NextStep;
                        string FlowID = flwObj.FlowID;
                        string grpNum = "";
                        grpNum = urlQuery.GoupRegID;

                        FlowControler flwController = new FlowControler(fn);
                        FlowMaster flwConfig = flwController.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
                        if (flwConfig.FlowLimitDelegateQty == Number.One)
                        {
                            string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, null, BackendRegType.backendRegType_Delegate);
                            Response.Redirect(route, false);
                        }
                        else
                        {
                            string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno, BackendRegType.backendRegType_Delegate);
                            Response.Redirect(route, false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogGenEmail lggenemail = new LogGenEmail(fn);
                    lggenemail.type = GenLogDefaultValue.errorException;
                    lggenemail.RefNumber = groupid + "," + regno;
                    lggenemail.description = ex.Message;
                    lggenemail.remark = RegClass.typeDeg + cFun.DecryptValue(urlQuery.FlowID);
                    lggenemail.step = cFun.DecryptValue(urlQuery.CurrIndex);
                    lggenemail.writeLog();

                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }
    #endregion

    #region SaveUpload
    private void SaveUpload(string regno, string showid, string type, string photofilename, ref FileUpload fup, ref HyperLink hpl)
    {
        try
        {
            SetUpController setupCtr = new SetUpController(fn);
            refFileUpload fileuploadobj = new refFileUpload();

            HttpPostedFile file = (HttpPostedFile)(fup.PostedFile);
            if ((file != null && file.ContentLength > 0) && (hpl.NavigateUrl != null && hpl.NavigateUrl != "" && hpl.NavigateUrl != "#"))
            {
                fileuploadobj.Regno = regno;
                fileuploadobj.ShowID = showid;
                fileuploadobj.Type = type;
                setupCtr.deleterefFileUpload(fileuploadobj);
            }

            if (file != null)
            {
                if (file.ContentLength > 0)
                {
                    string datepor = DateTime.Now.ToString("ddMMyy");
                    string Timepor = DateTime.Now.ToString("hhmmss");
                    string ext = System.IO.Path.GetExtension(file.FileName);

                    string path = "~/FileUpload/" + showid;
                    if (!Directory.Exists(Server.MapPath(path)))
                    {
                        Directory.CreateDirectory(Server.MapPath(path));
                    }

                    string fileName = photofilename + ext;// regno + datepor + Timepor + ext;
                    string filePath = path + "/" + fileName;
                    fup.SaveAs(Server.MapPath(filePath));

                    fileuploadobj = new refFileUpload();
                    fileuploadobj.Regno = regno;
                    fileuploadobj.FileName = fileName;
                    fileuploadobj.Type = type;
                    fileuploadobj.ShowID = showid;
                    setupCtr.insertrefFileUpload(fileuploadobj);
                }
            }
        }
        catch (Exception ex)
        { }
    }
    private bool validateUpload(FileUpload fup, HyperLink hpl, ref string errorMsg)
    {
        bool isUpload = true;
        try
        {
            HttpPostedFile file = (HttpPostedFile)(fup.PostedFile);
            if ((file != null && file.ContentLength > 0) || (hpl.NavigateUrl != null && hpl.NavigateUrl != "" && hpl.NavigateUrl != "#"))
            {
                if ((file != null && file.ContentLength > 0))
                {
                    string extension = System.IO.Path.GetExtension(fup.FileName).ToString().ToLower();
                    bool isValidDimension = ValidateFileDimensions(fup);
                    if (extension != ".jpg" && extension != ".jpeg")//extension != ".png" && 
                    {
                        errorMsg = "Please upload jpg/jpeg file format.";
                        isUpload = false;
                    }
                    else if (file.ContentLength > 153600)//150 KBytes
                    {
                        errorMsg = "Photo upload exceeds the file size.";
                        isUpload = false;
                    }
                    //else if(!isValidDimension)
                    //{
                    //    errorMsg = "Image size is not correct.";
                    //    isUpload = false;
                    //}
                    else
                    {
                        isUpload = true;
                    }
                }
                else
                {
                    isUpload = true;
                }
            }
        }
        catch(Exception ex)
        { }

        return isUpload;
    }
    private bool ValidateFileDimensions(FileUpload fup)
    {
        using (System.Drawing.Image myImage =
               System.Drawing.Image.FromStream(fup.PostedFile.InputStream))
        {
            if (myImage.Height > 514 || myImage.Width > 400)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    #endregion

   #region btnBack
    protected void btnBack_Click(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            FlowControler flwObj = new FlowControler(fn);
            string flowID = cFun.DecryptValue(urlQuery.FlowID);
            string currentIndx = cFun.DecryptValue(urlQuery.CurrIndex);
            flwObj.getPreviousRoute(flowID, currentIndx);
            string showID = urlQuery.CurrShowID;
            string page = flwObj.CurrIndexModule;
            string step = flwObj.CurrIndex;             
            string grpNum = "";
            grpNum = urlQuery.GoupRegID;
            string regno = urlQuery.DelegateID;
            string route = flwObj.MakeFullURL(page, flowID, showID, grpNum, step, regno, BackendRegType.backendRegType_Delegate);
            Response.Redirect(route, false);
        }
    }
    #endregion
}