﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LandingIDEM.aspx.cs" Inherits="LandingIDEM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>IDEM Online Registration 2020</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/Icon1.png" />

    <link href="Content/IDEMTemplate/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Content/IDEMTemplate/Site.css" rel="stylesheet" type="text/css" />
    <link href="Content/IDEMTemplate/font-awesome.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="Scripts/jquery-2.1.0.js"></script>
    <script type="text/javascript" src="Scripts/jquery.plugin.js"></script>
    <script type="text/javascript" src="Scripts/bootstrap.min.js"></script>

    <style type="text/css">
        body
        {
            font-family: Trebuchet-MS !important;
            font-size:12px !important;
        }
        .btn-danger
        {
            /*background-color:gold !important;*/
            border-radius:7px !important;
        }
        /*.col-xs-4, .col-xs-6
        {
            padding :0px !important;
            width:80% !important;
        }*/
        h3
        {
            margin-top:10px !important;
            margin-bottom:0px !important;
        }
        .iconStyle
        {
            color:#333333 !important;
        }
        .fa-lg
        {
            font-size:2.33333333em !important;
            margin-top:10px !important;
        }
        h5
        {
            font-weight:bold;
        }
        .form-control
        {
            font-family: Trebuchet-MS !important;
            font-size:12px !important;
        }
        ul {
            margin: 0;
            padding-left:5px;
        }
        ul.dashed {
            list-style-type: none;
        }
        ul.dashed > li {
            text-indent: -5px;
        }
        ul.dashed > li:before {
            content: "-";
            text-indent: -5px;
        }
        .table-bordered, .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td
        {
            border :1px solid black !important;
        }
        .container
        {
            padding-left:5px !important;
            padding-right:5px !important;
        }
        .btn-danger
        {
            background-color:red !important;
            font-size: 14px !important;
        }
        .panel-title::after {
            content: "\f107";
            color: #333;
            top: -2px;
            right: 0px;
            /*position: absolute;*/
            float:right;
            font-family: "FontAwesome";
            padding-right:10px !important;
            font-size:30px !important;
            margin-top:-10px !important;

        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- 
    Start of global snippet: Please do not remove
    Place this snippet between the <head> and </head> tags on every page of your site.
    -->
    <!-- Global site tag (gtag.js) - Google Marketing Platform -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=DC-6953330"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'DC-6953330');
    </script>
    <!-- End of global snippet: Please do not remove -->

    <!--
    Event snippet for FR~On Click _MK~SG_PC~IDEM2020_TradeVisitorbutton on https://www.event-reg.biz/registration/LandingIDEM: Please do not remove.
    Place this snippet on pages with events you’re tracking. 
    Creation date: 12/19/2019
    -->
    <script>
        gtag('event', 'conversion', {
        'allow_custom_scripts': true,
        'send_to': 'DC-6953330/mice/fronc018+standard'
        });
    </script>
    <noscript>
    <img src="https://ad.doubleclick.net/ddm/activity/src=6953330;type=mice;cat=fronc018;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=1?" width="1" height="1" alt=""/>
    </noscript>
    <!-- End of event snippet: Please do not remove -->
</head>
<body>
    <div class="container">
    <div id="header">
        <a href="https://www.idem-singapore.com/" target="_blank">
            <asp:Image ID="imgBanner" runat="server" border="0" ImageUrl="https://event-reg.biz/DefaultBanner/images/IDEM/IDEMHeaderBanner.jpg"
                    style = "position:relative; right:10px; top:0px; z-index:1; width:100%;"/>
        </a>
        <br />
    </div>

    <form id="SignupForm" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="form-group row text-center">
            <div class="col-md-12">
                <h1><strong>WELCOME TO IDEM 2020 REGISTRATION PAGE!</strong></h1>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6 col-xs-12">
                <div style="overflow-x:auto;overflow-y:hidden;" class="table-responsive">
                    <table style="width:100%;" class="table table-bordered">
                        <tr style="background-color:#d9d9d9;">
                            <th>Registration</th>
                            <th style="text-align:center;">
                                <strong>Early-Bird</strong><br />
                                <span style="font-weight:normal;">Until 31 Jan 2020 (23:59 hours, GMT+8)</span>
                            </th>
                            <th style="text-align:center;">
                                <strong>Regular</strong><br />
                                <span style="font-weight:normal;">1 Feb 2020 – 17 Apr 2020 (23:59 hours, GMT+8)</span>
                            </th>
                            <th style="text-align:center;">
                                <strong>Onsite</strong><br />
                                <span style="font-weight:normal;">18 Apr – 26 Apr 2020</span>
                            </th>
                        </tr>
                        <tr>
                            <td>Dentist <img src="images/LandingPricingLogo.png" style="float:right;" /></td>
                            <td style="text-align:center;">SGD 670</td>
                            <td style="text-align:center;">SGD 770</td>
                            <td style="text-align:center;">SGD 870</td>
                        </tr>
                        <tr>
                            <td>Dentist (ASEAN) <img src="images/LandingPricingLogo.png" style="float:right;" /></td>
                            <td style="text-align:center;">SGD 580</td>
                            <td style="text-align:center;">SGD 680</td>
                            <td style="text-align:center;">SGD 780</td>
                        </tr>
                        <tr>
                            <td>Dentist (SDA Member)</td>
                            <td style="text-align:center;">SGD 450</td>
                            <td style="text-align:center;">SGD 540</td>
                            <td style="text-align:center;">SGD 600</td>
                        </tr>
                        <tr>
                            <td>Hygienist/Therapist <img src="images/LandingPricingLogo.png" style="float:right;" /></td>
                            <td style="text-align:center;">SGD 420</td>
                            <td style="text-align:center;">SGD 490</td>
                            <td style="text-align:center;">SGD 560</td>
                        </tr>
                        <tr>
                            <td>Hygienist/Therapist (AOHT Member)</td>
                            <td style="text-align:center;">SGD 360</td>
                            <td style="text-align:center;">SGD 390</td>
                            <td style="text-align:center;">SGD 440</td>
                        </tr>
                        <tr>
                            <td>Dental Auxiliary, Technician, Undergraduate <img src="images/LandingPricingLogo.png" style="float:right;" /></td>
                            <td style="text-align:center;">SGD 250</td>
                            <td style="text-align:center;">SGD 300</td>
                            <td style="text-align:center;">SGD 350</td>
                        </tr>
                        <tr>
                            <td>Trade Visitor </td>
                            <td style="text-align:center;">Complimentary</td>
                            <td style="text-align:center;">Complimentary</td>
                            <td style="text-align:center;">SGD 30</td>
                        </tr>
                        <tr>
                            <td>Accompanying person</td>
                            <td style="text-align:center;" colspan="3">SGD 100</td>
                        </tr>
                    </table>
                    &nbsp; &nbsp;<img src="images/LandingPricingLogo.png" /> &nbsp; &nbsp;<span style="font-size:14px;">Group discount applicable. See below for details</span>
                    <br />&nbsp; &nbsp;<span style="font-size:14px;">Each delegate may register up to 2 accompanying person tickets at SGD $100.</span>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 LandingRegister" style="background-color:#4472c4;color:white;border:1px solid #4472c4;">
                <br />
                <%--<div class="form-group row">
                    <div class="col-md-6 col-xs-6 LandingLeftStyle">
                        <strong>SDA Members & Conference Delegates</strong>
                    </div>
                    <div class="col-md-6 col-xs-6 LandingRightStyle">
                        <strong>Trade Visitors</strong>
                    </div>
                </div>--%>
                <div class="row" style="background-color:#4472c4;color:white;">
                    <div class="form-group row text-center">
                        <div class="col-md-12 col-xs-12 LandingLeftStyle">
                            <h4>Please click to proceed with registration:</h4>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="form-group row">
                        <div class="col-md-6 col-xs-6 LandingLeftStyle LandingBtnDivStyle">
                            <asp:Button ID="btnTradeGroup" runat="server" Text="Conference" 
                                    CssClass="col-xs-3 btn-danger LandingBtnStyle" OnClick="btnConference_Click"/>
                        </div>
                        <div class="col-md-6 col-xs-6 LandingRightStyle LandingBtnDivStyle" style="padding-right:15px;">
                            <asp:Button ID="btnTrade" runat="server" Text="Trade Visitor" 
                                    CssClass="col-xs-3 btn-danger LandingBtnStyle" OnClick="btnTrade_Click"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 col-xs-6 LandingLeftStyle">
                            <ul class="dashed" style="font-size:10px;padding-left:30px;">
                                <li>Registration fees apply</li>
                                <li>Access to scientific conference and exhibition</li>
                                <li>SDA / AOHT members to enter DCR no.</li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-xs-6 LandingRightStyle">
                            <ul class="dashed" style="font-size:10px;padding-left:30px;">
                                <li>Complimentary entry with online pre-registration</li>
                                <li>Access to trade exhibition</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <br /><br />
                <div class="row" style="background-color:white;color:black;">
                    <div class="row">
                        <div class="col-md-12 col-xs-12 text-center">
                            <h4>Manage Your Registration</h4>
                            <br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-6 LandingMngSubDiv">
                            <div class="col-md-offset-2 col-md-2 col-xs-4 LandingMngLabelStyle">
                                Email:
                            </div>
                            <div class="col-md-2 col-xs-2 LandingTxtDivStyle">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control LandingTxtStyle"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6 LandingMngSubDiv">
                            <div class="col-md-3 col-xs-4 LandingMngLabelStyle">
                                Password:
                            </div>
                            <div class="col-md-2 col-xs-2 LandingTxtDivStyle">
                                <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control LandingTxtStyle" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-6 col-xs-6">&nbsp;</div>
                        <div class="col-md-offset-2 col-md-4 col-xs-6" style="padding-left:20px;">
                            <label style="font-size:xx-small">Forgot your password? Click <asp:LinkButton ID="lnkForgotPassword" runat="server" PostBackUrl="ForgotPSWIDEM.aspx" Text="HERE"></asp:LinkButton></label>
                        </div>
                    </div>
                    <br />
                    <div class="row pull-right" style="padding-right:55px;">
                        <div class="col-md-4 col-xs-4">&nbsp;</div>
                        <div class="col-md-3 col-xs-8">
                            <asp:Button ID="btnSignIn" runat="server" Text="Sign In" 
                                    CssClass="col-xs-6 btn-danger LandingBtnStyle" OnClick="btnSignIn_Click"/>
                        </div>
                    </div>
                    <br />
                    <div class="row text-center">
                        <div class="col-md-12 col-xs-12">
                            <asp:Label ID="lblerror" runat="server" Text="Label" Visible="false" ForeColor="Red" ></asp:Label>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-12 col-xs-12" style="padding-right:20px;">
                <table style="width:100%">
                    <tr>
                        <td style="background-color:orange;text-align:center;">
                            <h3 class="panel-title" data-toggle="collapse" data-target="#collapseCoronavirus">Advisory on Novel Coronavirus</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div id="collapseCoronavirus" class="panel-collapse collapse">
                    <table style="width:100%">
                        <tr>
                            <td>
                                IDEM 2020 Official Statement on Coronavirus, 
                                please click <a href="https://www.idem-singapore.com/press/advisory-on-novel-coronavirus/" target="_blank">here</a> for further details.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <table style="width:100%">
                    <tr>
                        <td style="background-color:orange;text-align:center;">
                            <h3 class="panel-title" data-toggle="collapse" data-target="#collapseWorkshops">IDEM Workshops</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div id="collapseWorkshops" class="panel-collapse collapse">
                    <table style="width:100%">
                        <tr>
                            <td>
                                <table style="width:100%;" class="table table-bordered">
                                    <tr style="background-color:#d9d9d9;">
                                        <th>Registration</th>
                                        <th style="text-align:center;">
                                            <strong>Early-Bird</strong><br />
                                            <span style="font-weight:normal;">Until 16 Feb 2020 (23:59 hours, GMT+8)</span>
                                        </th>
                                        <th style="text-align:center;">
                                            <strong>Regular</strong><br />
                                            <span style="font-weight:normal;">17 Feb 2020 – 17 Apr 2020 (23:59 hours, GMT+8)</span>
                                        </th>
                                        <th style="text-align:center;">
                                            <strong>Onsite</strong><br />
                                            <span style="font-weight:normal;">18 Apr – 26 Apr 2020</span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>Implementing Remote Monitoring to Optimize Clear Aligner Treatment <br /><i>Ronnie Yap, Fadi Yassmin</i></td>
                                        <td style="text-align:center;">SGD 100</td>
                                        <td style="text-align:center;">SGD 150</td>
                                        <td style="text-align:center;">SGD 200</td>
                                    </tr>
                                    <tr>
                                        <td>Aesthetic Enhancements for Your Chairside Crowns from Single Tooth to Full Smile Design <br /><i>Mohamed Hassanien</i></td>
                                        <td style="text-align:center;">SGD 650</td>
                                        <td style="text-align:center;">SGD 700</td>
                                        <td style="text-align:center;">SGD 750</td>
                                    </tr>
                                    <tr>
                                        <td>Develop your Proficiency in Reading CBCT <br /><i>Raahib Dudhia</i></td>
                                        <td style="text-align:center;">SGD 370</td>
                                        <td style="text-align:center;">SGD 410</td>
                                        <td style="text-align:center;">SGD 450</td>
                                    </tr>
                                    <tr>
                                        <td>Restorative Protocols for Endodontically Treated Teeth - The Root-2-Crown Concept <br /><i>Peet van der Vyver</i></td>
                                        <td style="text-align:center;">SGD 370</td>
                                        <td style="text-align:center;">SGD 410</td>
                                        <td style="text-align:center;">SGD 450</td>
                                    </tr>
                                    <tr>
                                        <td>The Digital Workflow in Implant Dentistry from Inception to Delivery <br /><i>Michael Danesh-Meyer</i></td>
                                        <td style="text-align:center;">SGD 370</td>
                                        <td style="text-align:center;">SGD 410</td>
                                        <td style="text-align:center;">SGD 450</td>
                                    </tr>
                                    <tr>
                                        <td>Veneers Workflow: From Diagnosis to Cementation <br /><i>Roberto Turrini</i></td>
                                        <td style="text-align:center;">SGD 370</td>
                                        <td style="text-align:center;">SGD 410</td>
                                        <td style="text-align:center;">SGD 450</td>
                                    </tr>
                                    <tr>
                                        <td>Non-Surgical Periodontal Therapy – Scaling Forever? <br /><i>Axel Spahr</i></td>
                                        <td style="text-align:center;">SGD 370</td>
                                        <td style="text-align:center;">SGD 410</td>
                                        <td style="text-align:center;">SGD 450</td>
                                    </tr>
                                    <tr>
                                        <td>Off-site (NUCOHS) Implementing Biofilm and Air Polishing Concepts into your Workflow (For Dental Hygienist and Therapists) <br /><i>Tracey Lennemann</i></td>
                                        <td style="text-align:center;">SGD 180</td>
                                        <td style="text-align:center;">SGD 200</td>
                                        <td style="text-align:center;">SGD 250</td>
                                    </tr>
                                    <tr>
                                        <td>2 DAY WORKSHOP: Fundamentals of Anterior and Posterior All-Ceramic Restorations - Contemporary Concepts on Tooth Preparations and Cementation Protocols <br /><i>Anthony Mak, Chris Ho</i></td>
                                        <td style="text-align:center;">SGD 1950</td>
                                        <td style="text-align:center;">SGD 2150</td>
                                        <td style="text-align:center;">SGD 2350</td>
                                    </tr>
                                    <tr>
                                        <td>1 DAY WORKSHOP: Restoring the Severely Broken Down Endodontically Treated Anterior Tooth. Incorporating the Fundamemental Principles of Direct Composite Aesthetics and Fibre-Reinforced Posts <br /><i>Maciej Zarow</i></td>
                                        <td style="text-align:center;">SGD 800</td>
                                        <td style="text-align:center;">SGD 900</td>
                                        <td style="text-align:center;">SGD 1000</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <table style="width:100%">
                    <tr>
                        <td style="background-color:orange;text-align:center;">
                            <h3 class="panel-title" data-toggle="collapse" data-target="#collapseI">IMPORTANT INFORMATION</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div id="collapseI" class="panel-collapse collapse">
                    <table style="width:100%">
                        <tr>
                            <td>
                                <ol style="list-style:disc;">
                                    <li>Please note that a credit card (AMEX/VISA/MASTER) is required for online payment.</li>
                                    <li>Early Bird Rate is valid until 31 January 2020 (23:59 hours, GMT +8).</li>
                                    <li>Regular rate is valid from 01 February 2020 – 17 April 2020 (23:59 hours, GMT +8).</li>
                                    <li>Onsite registration will be available from 24 – 26 April 2020 at Level 3, Suntec Singapore.</li>
                                    <li>Onsite Registration Rate is valid from 18 April 2020 – 26 April 2020.</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <table style="width:100%">
                    <tr>
                        <td style="background-color:orange;text-align:center;">
                            <h3 class="panel-title" data-toggle="collapse" data-target="#collapseG">GROUP REGISTRATIONS</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div id="collapseG" class="panel-collapse collapse">
                    <table style="width:100%">
                        <tr>
                            <td>
                                <ol style="list-style:disc;">
                                    <li>Register as a group and enjoy the following discounts:
                                        <ol style="list-style:disc;">
                                            <li>5% off the Main Scientific Conference for groups of 5 persons or more*</li>
                                            <li>10% off the Main Scientific Conference for groups of 10 persons or more*</li>
                                            <li>15% off the Main Scientific Conference for groups of 30 persons or more*</li>
                                        </ol>
                                    </li>
                                    <li>Group discount is not applicable to SDA and AOHT Members</li>
                                    <li>Contact us at <a href="mailto:idem-reg@koelnmesse.com.sg">idem-reg@koelnmesse.com.sg</a> for more information on the group registration process.</li>
                                </ol>
                                <span style="color:red;">* Group registration discounts are not applicable for Limited Attendance Workshops and cannot be used in conjunction with any other discounts or rebates.</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <table style="width:100%">
                    <tr>
                        <td style="background-color:orange;text-align:center;">
                            <h3 class="panel-title" data-toggle="collapse" data-target="#collapseIND">REGISTRATION POLICIES</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div id="collapseIND" class="panel-collapse collapse">
                    <table style="width:100%">
                    <tr>
                        <td>
                            <ol style="list-style:decimal">
                                <li>All rates are inclusive of 7% Goods and Services Tax.</li>
                                <li>Your registration will be valid when payment is received in full by the Organisers.</li>
                                <li>The Organisers reserve the right to amend any part of the programme without giving prior notice should the need arise.</li>
                                <li>The Organisers reserve the right to cancel the conference or any part thereof without prior notice in the event of acts of god, fire, acts of the government, terrorism, war or any other events beyond the control of the Organisers. </li>
                                <li>Click <a href="https://www.idem-singapore.com/registration-travel/policies/" target="_blank">here</a> to view our cancellation policies.</li>
                            </ol>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    </table>
                </div>
                <table style="width:100%">
                    <tr>
                        <td style="background-color:orange;text-align:center;">
                            <h3 class="panel-title" data-toggle="collapse" data-target="#collapseQ">REGISTRATION QUESTIONS</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div id="collapseQ" class="panel-collapse collapse">
                    <table style="width:100%">
                    <tr>
                        <td>
                            <ol style="list-style:disc;">
                                <li>Please contact IDEM Registration Secretariat at <a href="mailto:idem-reg@koelnmesse.com.sg">idem-reg@koelnmesse.com.sg</a>.</li>
                                <li>Business Hours – Monday to Friday, 10:00am – 18:00 hours (GMT +8).</li>
                            </ol>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
        </div>
        <br /><br />
        <div class="row">
            <footer>
                <div class="container">
                    <div class="row" id="footercontact">
                        <div class="col-md-4 col-xs-12" style="float:left;text-align:left; font-family:'Trebuchet MS';" >
                            <h5 style="font-family:'Trebuchet MS';padding-left:10px;">Stay Connected with Us</h5>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aFacebook" runat="server" target="_blank" href="https://www.facebook.com/idemsingapore/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Facebook.png" /></a></p></div>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aTwitter" runat="server" target="_blank" href="https://twitter.com/IDEMSingapore" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Twitter.png" /></a></p></div>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aInstagram" runat="server" target="_blank" href="https://www.instagram.com/idem.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Instagram.png" /></a></p></div>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aLinkedIn" runat="server" target="_blank" href="https://www.linkedin.com/company/6440434/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/IN.png" /></a></p></div>
                        </div>
                        <div class="col-md-8 col-xs-12" style="float:right;text-align:left;" >
                            <div class="col-md-2 col-xs-12" style="padding-right:0px;">&nbsp;</div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;padding-left:10px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Endorsed By</h5>
                                <p class="social">
                                    <a id="a1" runat="server" target="_blank" href="https://www.stb.gov.sg/content/stb/en/assistance-and-licensing/other-assistance-resources-overview/AIF28Non-Financial-Assistance.html" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/AIFLogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Supported By</h5>
                                <p class="social">
                                    <a id="a2" runat="server" target="_blank" href="https://www.visitsingapore.com/mice/en/about-us/about-secb/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/SECBLogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Held in</h5>
                                <p class="social">
                                    <a id="a3" runat="server" target="_blank" href="https://www.visitsingapore.com/en/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/BrandSGLogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Organised by</h5>
                                <p class="social">
                                    <a id="a4" runat="server" target="_blank" href="http://sda.org.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/SDALogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">&nbsp;</h5>
                                <p class="social">
                                    <a id="a5" runat="server" target="_blank" href="https://www.koelnmesse.com.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/KMLogo.png" /></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>


<%--        

<div class="panel-group" id="accordion">
    <!-- First Panel -->
    <div class="panel panel-default">
        <div class="panel-heading">
             <h4 class="panel-title" data-toggle="collapse" data-target="#collapseOne">
                 Collapsible Group Item #1
             </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
                Anim pariatur cliche reprehenderit, 
                enim eiusmod high life accusamus terry richardson ad squid.
            </div>
        </div>
    </div>
    
    <!-- Second Panel -->
    <div class="panel panel-default">
        <div class="panel-heading">
             <h4 class="panel-title" data-toggle="collapse" data-target="#collapseTwo">
                 Collapsible Group Item #2
             </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
                Anim pariatur cliche reprehenderit, 
                enim eiusmod high life accusamus terry richardson ad squid.
            </div>
        </div>
    </div>
    
    <!-- Third Panel -->
    <div class="panel panel-default">
        <div class="panel-heading">
             <h4 class="panel-title" data-toggle="collapse" data-target="#collapseThree">
                 Collapsible Group Item #3
             </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body">
                Anim pariatur cliche reprehenderit, 
                enim eiusmod high life accusamus terry richardson ad squid.
            </div>
        </div>
    </div>
</div>--%>
    </form>
</div>
</body>
</html>
