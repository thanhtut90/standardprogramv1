﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventRegWCHM.aspx.cs" Inherits="EventRegWCHM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <link href="Content/Default/bootstrap.css" rel="stylesheet" />
    <link href="Content/RED/Site.css" rel="stylesheet" />
    <style type="text/css">
    .MainButton {
        color: #ffffff !important;
        background-color: #234090 !important;/*#15AABC*/
        border-color: #234090 !important;/*#15AABC*/
        width:80% !important;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row">
                <asp:Image ID="SiteBanner" runat="server" CssClass="img-responsive"  Width="100%" />
            </div>
            <br />
            <div class="row">
                <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1" style="padding-bottom:20px;">
                    <h2>Individual registration:</h2>
                    <ol>
	                    <li>
		                    There are four (4) parts to the individual online registration form:
		                    <ol style="list-style-type:lower-alpha;">
			                    <li>
				                    Delegate Info - Complete the required fields and press the&nbsp;<strong>NEXT</strong>&nbsp;button.</li>
			                    <li>
				                    Conference &ndash; Select the conference package and the add-ons (if applicable) and press the <strong>NEXT</strong>&nbsp;button.</li>
			                    <li>
				                    Confirmation - Review your registration, to change press EDIT otherwise select Credit Card and submit.</li>
			                    <li>
				                    At the payment page, select the <strong>VISA</strong> or <strong>MASTER</strong> icon</li>
			                    <li>
				                    Press the&nbsp;<strong>SUBMIT</strong>&nbsp;button to complete your online registration.</li>
		                    </ol>
	                    </li>
	                    <li>
		                    A confirmation email with your registration number and attached receipt will be emailed to you.</li>
	                    <li>
		                    On the day of the conference, present a printed copy of the confirmation letter at the pre-registered counters to collect your conference badge.</li>
                    </ol>
                </div>
                <div class="col-md-5 col-sm-5">
                    <h2>Group registration:</h2>
                    <ol>
	                    <li>
		                    Group registration is only applicable to group of 2 or more.</li>
	                    <li>
		                    There are five (5) parts to the group online registration form:
		                    <ol style="list-style-type:lower-alpha;">
			                    <li>
				                    Contact person &ndash; Complete the required fields and press the <strong>NEXT </strong>button.&nbsp; (Changes to the group registration will be managed by the contact person)</li>
			                    <li>
				                    Group registration &ndash; Press New member. Complete the required fields and press the&nbsp;<strong>NEXT</strong>&nbsp;button.</li>
			                    <li>
				                    Conference &ndash; Select the conference package and the add-ons (if applicable) and press the <strong>NEXT</strong>&nbsp;button. (Repeat this step until the last member is added)</li>
			                    <li>
				                    Confirmation - Review your group registration, to change press EDIT otherwise select Credit Card and submit.</li>
			                    <li>
				                    At the payment page, select the <strong>VISA</strong> or <strong>MASTER</strong> icon</li>
			                    <li>
				                    Press the&nbsp;<strong>SUBMIT</strong>&nbsp;button to complete your online registration.</li>
		                    </ol>
	                    </li>
	                    <li>
		                    A confirmation email with a summarized list of members registration number and attached receipt will be emailed to the contact person and individual member email confirmation will be emailed to the respective member email address.</li>
	                    <li>
		                    On the day of the conference, members are required to present a printed copy of the confirmation letter at the pre-registered counters to collect their conference badge.</li>
                    </ol>
                    <br /><br />
                    <br />
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1" style="padding-bottom:20px;">
                    <asp:Button runat="server" ID="btnRegSingapore" class="btn MainButton btn-lg btn-block col-md-3" Text="Individual Registration" OnClick="btnRegSingapore_Click" Visible="false" />
                </div>
                <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1" style="padding-bottom:20px;">
                    <asp:Button runat="server" ID="btnRegNonSingapore" class="btn MainButton  btn-lg btn-block col-md-3" Text="Group Registration" OnClick="btnRegNonSingapore_Click" Visible="false" />
                </div>
            </div>
            <br /><br />
        </div>
    </form>
</body>
</html>
