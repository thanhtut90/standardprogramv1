﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventRegOSEA.aspx.cs" Inherits="EventRegOSEA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="Content/RED/bootstrap.min.css"/>
    <link rel="stylesheet" href="Content/RED/Site.css"/>
    <link rel="stylesheet" href="Content/RED/style.css" />
    <script src="Scripts/iframeResizer.min.js"></script>
</head>
<body>

    <form id="form1" runat="server">
        <div class="col-lg-8 col-lg-offset-2 col-xs-12 SiteMasterFrame">
            <div class="row">
                <asp:Image ID="SiteBanner" runat="server" CssClass="img-responsive" style="width:100%;" ImageUrl="images/Site/BUM3382018213115452.png"></asp:Image>
                <br/>
                <style>
                    iframe {
                        width: 100%;
                    }
                </style>
                <p>
                    <iframe id="mdaFrame" runat="server" scrolling="no" frameborder="0" height="3600" ></iframe>
                    <script>iFrameResize({log:true})</script>
                </p>
            </div>
        </div>
    </form>
</body>
</html>
