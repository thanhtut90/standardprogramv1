﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ForgotPSWIDEM : System.Web.UI.Page
{
    #region Declaration
    static string _pagebanner = "page_banner";
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Boolean isclose = false;// fn.checkClosing();

            if (isclose)
            {
            }
            else
            {
            }

            Session.Clear();
            Session.Abandon();

            pageSet();
        }
    }

    #region pageSet & set Banner Image
    protected void pageSet()
    {
        //string strimage = string.Empty;

        //strimage = fn.GetDataByCommand("Select settings_value from tb_site_settings where settings_name='" + _pagebanner + "'", "settings_value");

        //imgBanner.ImageUrl = "~/" + strimage;
        //imgBanner.Visible = true;
    }
    #endregion

    #region btnSubmit_Click
    protected static string _IDEMShowID = "OSH388";
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                Boolean isvalid = isValid();
                if (isvalid)
                {
                    string showid = _IDEMShowID;
                    string emailAddress = cFun.solveSQL(txtEmail.Text.ToString().Trim());
                    if (!System.Text.RegularExpressions.Regex.IsMatch(txtRegno.Text.ToString().Trim(), @"^\d+$"))
                    {
                        string registrationID = txtRegno.Text.ToString().Trim().Substring(1, txtRegno.Text.ToString().Trim().Length - 1);
                        //string registrationID = cFun.solveSQL(txtRegno.Text.ToString().Trim());
                        DataTable dt = new DataTable();
                        string regno = string.Empty;
                        string reggroupid = string.Empty;
                        string flowid = string.Empty;
                        string password = string.Empty;
                        string fname = string.Empty;
                        string lname = string.Empty;
                        RegLoginPassword regLP = new RegLoginPassword(fn);
                        dt = getGroupReLoginAuthenData(emailAddress, registrationID, showid);
                        if (dt.Rows.Count > 0)
                        {
                            flowid = dt.Rows[0]["RG_urlFlowID"] != DBNull.Value ? dt.Rows[0]["RG_urlFlowID"].ToString() : "";
                            fname = dt.Rows[0]["RG_ContactFName"] != DBNull.Value ? dt.Rows[0]["RG_ContactFName"].ToString() : "";
                            lname = dt.Rows[0]["RG_ContactLName"] != DBNull.Value ? dt.Rows[0]["RG_ContactLName"].ToString() : "";
                            reggroupid = dt.Rows[0]["RegGroupID"] != DBNull.Value ? dt.Rows[0]["RegGroupID"].ToString() : "";
                            password = regLP.getPasswordByOwnerID(registrationID, showid);
                        }
                        else
                        {
                            dt = getDelegateReLoginAuthenData(emailAddress, registrationID, showid);
                            if (dt.Rows.Count > 0)
                            {
                                flowid = dt.Rows[0]["reg_urlFlowID"] != DBNull.Value ? dt.Rows[0]["reg_urlFlowID"].ToString() : "";
                                fname = dt.Rows[0]["reg_FName"] != DBNull.Value ? dt.Rows[0]["reg_FName"].ToString() : "";
                                lname = dt.Rows[0]["reg_LName"] != DBNull.Value ? dt.Rows[0]["reg_LName"].ToString() : "";
                                regno = dt.Rows[0]["Regno"] != DBNull.Value ? dt.Rows[0]["Regno"].ToString() : "";
                                reggroupid = dt.Rows[0]["RegGroupID"] != DBNull.Value ? dt.Rows[0]["RegGroupID"].ToString() : "";
                                password = regLP.getPasswordByOwnerID(registrationID, showid);
                            }
                        }

                        if (!string.IsNullOrEmpty(flowid) && !string.IsNullOrWhiteSpace(flowid)
                            && !string.IsNullOrEmpty(password) && !string.IsNullOrWhiteSpace(password))
                        {
                            string name = fname + " " + lname;
                            string isSendEmailSuccess = "F";
                            bool isValidUser = !string.IsNullOrEmpty(flowid) && !string.IsNullOrWhiteSpace(flowid) ? true : false;
                            if (isValidUser)
                            {
                                isSendEmailSuccess = sEmailSend(name, emailAddress, password);
                            }
                            string sqlLog = String.Format("Insert Into tb_LogForgotEmail (Regno,RegGroupID,RegEmail,ShowID,FlowID,Status,IPAddress)"
                                                + " Values (N'{0}',N'{1}',N'{2}',N'{3}',N'{4}',N'{5}',N'{6}')",
                                                registrationID, reggroupid, cFun.solveSQL(emailAddress), showid, flowid, cFun.solveSQL(isSendEmailSuccess.ToString()), GetUserIP());
                            fn.ExecuteSQL(sqlLog);
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already re-sent to your email address.');window.location.href='LandingIDEM.aspx';", true);
                            return;
                        }
                        else
                        {
                            lblerror.Text = "*Sorry, you are not a valid user.*";
                            lblerror.Visible = true;
                        }
                    }
                    else
                    {
                        lblerror.Text = "*Sorry, you are not a valid user.*";
                        lblerror.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { }
        }
    }
    protected Boolean isValid()
    {
        Boolean isvalid = true;

        if (string.IsNullOrEmpty(txtRegno.Text) || string.IsNullOrWhiteSpace(txtRegno.Text))
        {
            lblerror.Text = "*Please enter your Registration ID*";
            lblerror.Visible = true;
            isvalid = false;
        }
        if (string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrWhiteSpace(txtEmail.Text))
        {
            lblerror.Text = "*Please enter your Email Address*";
            lblerror.Visible = true;
            isvalid = false;
        }

        if (isvalid == true)
        {
            lblerror.Visible = false;
        }
        return isvalid;
    }
    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
    #region getdata
    #region getGroupReLoginAuthenData
    public DataTable getGroupReLoginAuthenData(string emailAddress, string registrationID, string showid)
    {
        DataTable dtResult = new DataTable();

        try
        {
            string query = "Select * From tb_RegGroup Where RG_ContactEmail=@RG_ContactEmail And RegGroupID=@RegGroupID And ShowID=@ShowID And recycle=0";

            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("RG_ContactEmail", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("RegGroupID", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            spar1.Value = emailAddress;
            spar2.Value = registrationID;
            spar3.Value = showid;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);
            dtResult = fn.GetDatasetByCommand(query, "dsResult", pList).Tables[0];
        }
        catch { }

        return dtResult;
    }
    #endregion
    #region getDelegateReLoginAuthenData
    public DataTable getDelegateReLoginAuthenData(string emailAddress, string registrationID, string showid)
    {
        DataTable dtResult = new DataTable();

        try
        {
            string query = "Select * From tb_RegDelegate Where reg_Email=@reg_Email And Regno=@Regno And ShowID=@ShowID And recycle=0";

            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("reg_Email", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Regno", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            spar1.Value = emailAddress;
            spar2.Value = registrationID;
            spar3.Value = showid;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);
            dtResult = fn.GetDatasetByCommand(query, "dsResult", pList).Tables[0];
        }
        catch { }

        return dtResult;
    }
    #endregion
    #endregion
    public string sEmailSend(string name, string emailAddress, string password)
    {
        string isSuccess = "F";
        try
        {
            Alpinely.TownCrier.TemplateParser tm = new Alpinely.TownCrier.TemplateParser();
            Alpinely.TownCrier.MergedEmailFactory factory = new Alpinely.TownCrier.MergedEmailFactory(tm);
            string template = HttpContext.Current.Server.MapPath("~/Template/IDEMForgotPassword.html");
            var tokenValues = new Dictionary<string, string>
                {
                    {"Name",name},
                    {"EmailAddress",emailAddress},
                    {"Password",password},
                };
            MailMessage message = factory
            .WithTokenValues(tokenValues)
            .WithHtmlBodyFromFile(template)
            .Create();

            //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//***
            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;//***

            var from = new MailAddress("idem-reg@idem-singapore.com", "IDEM 2020");
            message.From = from;
            message.To.Add(emailAddress);
            message.Subject = "IDEM 2020: Password Assistance";
            SmtpClient emailClient = new SmtpClient("idem-singapore.com");
            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("idem-reg@idem-singapore.com", "Reg_123!");
            emailClient.UseDefaultCredentials = true;
            emailClient.EnableSsl = true;
            emailClient.Port = 587;//***
            emailClient.Credentials = SMTPUserInfo;
            emailClient.Send(message);

            isSuccess = "T";
        }
        catch (Exception ex)
        {
            isSuccess = ex.Message;// "F";
        }
        return isSuccess;
    }
    #endregion

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("LandingIDEM.aspx");
    }
}