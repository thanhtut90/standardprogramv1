﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LandingIDEMVisitor : System.Web.UI.Page
{
    #region Declaration
    static string _pagebanner = "page_banner";
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Boolean isclose = false;// fn.checkClosing();

            if (isclose)
            {
            }
            else
            {
            }

            Session.Clear();
            Session.Abandon();

            pageSet();
        }
    }

    #region pageSet & set Banner Image
    protected void pageSet()
    {
        //string strimage = string.Empty;

        //strimage = fn.GetDataByCommand("Select settings_value from tb_site_settings where settings_name='" + _pagebanner + "'", "settings_value");

        //imgBanner.ImageUrl = "~/" + strimage;
        //imgBanner.Visible = true;
    }
    #endregion

    protected void btnTrade_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/EventReg?EVent=Visitor Registration");
    }

    protected void btnTradeGroup_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/EventReg?EVent=Visitor Group Registration");
    }
}