﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="ConferenceAddOn.aspx.cs" Inherits="ConferenceAddOn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            width:2%;
        }

        .accTableStyle tbody > tr > td
        {
            border-top:0px !important;
        }
    </style>

    <script type="text/javascript">
        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="row">
        <div class="col-xs-12">
            
        </div>
        <div class="col-md-offset-2 col-md-8  col-xs-12 ConfItemPanel ">
            <asp:UpdatePanel ID="MainUpdatePanel" runat="server">
                <ContentTemplate>
                    <div  class="form-group row" id="divOSEANote" runat="server" visible="false">
                        <div class="clear"></div>
                        <div class="col-md-12">
                            <asp:Label ID="lblHeaderNote" runat="server" Text="Prices shown are before discount" Font-Bold="true"></asp:Label>
                            <br />
                            <br />
                        </div>
                    </div>
                    <asp:Panel runat="server" ID="PanelMerchandise" Visible="false">
                        <div id="divHDR1" runat="server" visible="false">
                            <div  class="form-group row">
                                <div class="clear"></div>
                                <div class="col-md-12">
                                    <asp:Label ID="lblHDR1" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="divHDR2" runat="server" visible="false">
                            <div  class="form-group row">
                                <div class="clear"></div>
                                <div class="col-md-12">
                                    <br />
                                    <br />
                                    <asp:Label ID="lblHDR2" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="divHDR3" runat="server" visible="false">
                            <div  class="form-group row">
                                <div class="clear"></div>
                                <div class="col-md-12">
                                    <br />
                                    <br />
                                    <asp:Label ID="lblHDR3" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="divItemsHeader" runat="server">
                            <h4 style="padding-left: 20px; text-decoration: underline;"><asp:Label ID="lblDependentHeader" runat="server" Text="Items"></asp:Label></h4>
                        </div>
                        <table class="table CssConfTable">
                            <thead>
                                <tr>
                                    <th class="col-xs-3" colspan="2">Description</th>
                                    <th class="col-xs-1  CssPriceColumn">Price</th>
                                    <th class="col-xs-3">Size</th>
                                    <th class="col-xs-1"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="RptMerchandiseList" runat="server" OnItemDataBound="RptMerchandiseList_DataBond">
                                    <ItemTemplate>
                                        <tr>
                                            <asp:TextBox runat="server" ID="txtConfMercGroupID" Text='<%#Eval("ConfMerchandiseGroupID")%>' Visible="false"></asp:TextBox>
                                            <asp:TextBox runat="server" ID="txtConfType" Text='<%#Eval("con_Type")%>' Visible="false"></asp:TextBox>
                                            <td class="style1">
                                                <asp:Image width="80" height="80" ID="imgLogo" runat="server"/>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblDisplay" Text='<%#Eval("ConDisplayText")%>'></asp:Label>
                                                <br />
                                                <asp:Label runat="server" ID="lblDescription" Text='<%#Eval("ConfItemDesc")%>'></asp:Label>
                                            </td>
                                            <td class="CssPriceColumn">
                                                <asp:TextBox runat="server" ID="txtConfPrice" Visible="false"></asp:TextBox>
                                                <asp:Label runat="server" ID="ConItemDisplayPrice" CssClass="CssPriceColumn"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Panel runat="server" ID="PanelSize">
                                                    <asp:DropDownList runat="server" ID="ddlSize" 
                                                        CausesValidation="false">
                                                        <%--<asp:ListItem Text="Please Select" Value="0"></asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                    <%--OnSelectedIndexChanged="ddlSize_SelectedIndexChanged" AutoPostBack="true" --%>
                                                </asp:Panel>
                                            </td>
                                            <td>
                                                <asp:Panel runat="server" ID="PanelShowSelect">
                                                    <asp:DropDownList runat="server" ID="ddlQty" OnSelectedIndexChanged="ddlConfItem_Onclick" AutoPostBack="true" 
                                                        CausesValidation="false">
                                                        <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <telerik:RadButton ID="chkConfItem" runat="server" ButtonType="ToggleButton" ToggleType="CheckBox" CommandArgument='<%#Eval("ConfMerchandiseID")%>' OnClick="btnDConfItem_Onclick"
                                                         CausesValidation="false"></telerik:RadButton>
                                                </asp:Panel>
                                                <asp:Panel runat="server" ID="PanleMsg">
                                                    <asp:Label runat="server" ID="lblMsg"></asp:Label>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </tbody>
                        </table>

                    </asp:Panel>
                    <div class="row">
                        <div class=" col-xs-12 col-md-offset-5 col-md-7 text-right">
                            <table class="table">
                                <tr>
                                    <td>Sub Total:</td>
                                    <td style="text-align:center;">
                                        <asp:Label runat="server" ID="lblSubTotal" Text="0.00"></asp:Label></td>
                                </tr>
                                <tr style="display:none;">
                                    <td>GST:</td>
                                    <td style="text-align:center;">
                                        <asp:Label runat="server" ID="lblGST" Text="0.00"></asp:Label></td>
                                </tr>
                                <tr id="trGrandTotal" runat="server">
                                    <td>Grand Total:</td>
                                    <td style="text-align:center;">
                                        <asp:Label runat="server" ID="lblGrandTotal" Text="0.00"></asp:Label></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div id="divFTRCHK1" runat="server" visible="false">
                        <div  class="form-group row">
                            <div class="clear"></div>
                            <div class="col-md-10 col-md-offset-1">
                                <asp:CheckBoxList ID="chkFTRCHK1" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                                <asp:Label ID="lblErrFTRCHK1" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                    <div id="divFTR1" runat="server" visible="false">
                        <div  class="form-group row">
                            <div class="clear"></div>
                            <div class="col-md-10 col-md-offset-1">
                                <asp:Label ID="lblFTR1" runat="server"></asp:Label>
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                    <div id="divFTR2" runat="server" visible="false">
                        <div  class="form-group row">
                            <div class="clear"></div>
                            <div class="col-md-10 col-md-offset-1">
                                <asp:Label ID="lblFTR2" runat="server"></asp:Label>
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                    <div id="divFTRCHK" runat="server" visible="false">
                        <div  class="form-group row">
                            <div class="clear"></div>
                            <div class="col-md-10 col-md-offset-1">
                                <asp:CheckBoxList ID="chkFTRCHK" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                                <%--<asp:CheckBox ID="chkFTRCHK" runat="server" />&nbsp;&nbsp;<asp:Label ID="lblFTRCHK" runat="server"></asp:Label>
                                <asp:Label ID="lblFTRCHKIsSkip" runat="server" Visible="false" Text="0"></asp:Label>--%>
                                <asp:Label ID="lblErrFTRCHK" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                    </div>

                           <div class="row" style="padding-top:20px;">
                               <div class="pull-right col-lg-3"> 
                    <asp:Button runat="server" Text="Next" ID="btnNext" OnClick="btnNext_Onclick" CssClass="btn btn-block  MainButton" />
                                   </div>
                               </div>
                    <asp:TextBox runat="server" ID="txtCurrency" Visible="false"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtCofMercTotal" Visible="false" Text="0.00"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtShowMerchandiseItem" Text="1" Visible="false"></asp:TextBox>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

