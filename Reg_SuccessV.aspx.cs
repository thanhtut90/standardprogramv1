﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Logging;
using System.Data;
using Corpit.Email;
using Corpit.Site.Email;

public partial class Reg_SuccessV : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cComFuz = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        SetSiteMaster(showid);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string previousUrl = Request.UrlReferrer.ToString();
            //Response.End();

            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
            if (fmaster != null)
            {
                txtFlowName.Text = fmaster.FlowName;
                if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
                {
                    string groupid = Session["Groupid"].ToString();
                    flowid = Session["Flowid"].ToString();
                    string showid = Session["Showid"].ToString();
                    string currentstage = cComFuz.DecryptValue(urlQuery.CurrIndex);
                    string DelegateID = string.Empty;
                    if (Session["Regno"] != null)
                    {
                        DelegateID = Session["Regno"].ToString();
                    }

                    insertRegLoginAction(groupid, rlgobj.actview);

                    RegGroupObj rgg = new RegGroupObj(fn);
                    //rgg.updateGroupCurrentStep(urlQuery);

                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    rgd.updateDelegateCurrentStep(urlQuery);

                    FlowControler flwControl = new FlowControler(fn);
                    FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);

                    StatusSettings stuSettings = new StatusSettings(fn);

                    string regstatus = "0";
                    DataTable dt = new DataTable();
                    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                    {
                        dt = rgg.getRegGroupByID(groupid, showid);
                        if (dt.Rows.Count > 0)
                        {
                            regstatus = dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                        }

                        rgg.updateStatus(groupid, stuSettings.Success, showid);

                        rgd.updateDelegateRegStatus(groupid, stuSettings.Success, showid);
                    }
                    else//*flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL
                    {
                        rgg.updateStatus(groupid, stuSettings.Success, showid);

                        if (!string.IsNullOrEmpty(DelegateID))
                        {
                            dt = rgd.getDataByGroupIDRegno(groupid, DelegateID, showid);
                            if (dt.Rows.Count > 0)
                            {
                                regstatus = dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                            }

                            rgd.updateStatus(DelegateID, stuSettings.Success, showid);
                        }
                    }

                    #region bindSuccessMessage
                    lblSuccessMessage.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowSuccessMsg) ? flwMasterConfig.FlowSuccessMsg : "");
                    if (string.IsNullOrEmpty(lblSuccessMessage.Text))
                        PanelMsg.Visible = false;
                    #endregion

                    #region SendCurrentFlowStepEmail for Asean Summit Group Reg(whether already sent or not send, send again))
                    EmailHelper eHelper = new EmailHelper();
                    string emailRegType = BackendRegType.backendRegType_Group;// "G";
                    EmailDSourceKey sourcKey = new EmailDSourceKey();
                    string flowID = flowid;
                    string groupID = groupid;
                    string delegateID = DelegateID;//if not, Blank
                    string eType = EmailHTMLTemlateType.Confirmation;

                    sourcKey.AddKeyToList("ShowID", showid);
                    sourcKey.AddKeyToList("RegGroupID", groupID);
                    sourcKey.AddKeyToList("Regno", delegateID);
                    sourcKey.AddKeyToList("FlowID", flowID);

                    string updatedUser = string.Empty;
                    if (Session["Groupid"] != null)
                    {
                        updatedUser = Session["Groupid"].ToString();
                    }
                    bool isEmailSend = eHelper.SendEmailFromBackend(eType, sourcKey, emailRegType, updatedUser);
                    #endregion

                    #region Logging
                    LogGenEmail lggenemail = new LogGenEmail(fn);
                    LogActionObj lactObj = new LogActionObj();
                    lggenemail.type = LogType.generalType;
                    lggenemail.RefNumber = groupID + "," + DelegateID;
                    lggenemail.description = "Send confrimation email to " + groupID;
                    lggenemail.remark = "Send confrimation email to " + groupID + " and Send " + eType + " email(email sending status:" + isEmailSend + ")";
                    lggenemail.step = currentstage;
                    lggenemail.writeLog();
                    #endregion

                    try
                    {
                        CommonFuns cFuz = new CommonFuns();
                        string DID = "";
                        if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                        {
                            DID = groupid;
                        }
                        else
                            DID = DelegateID;
                        HTMLTemplateControler htmlControl = new HTMLTemplateControler();
                        string template = htmlControl.CreateAcknowledgeLetterHTMLTemplate(showid, DID);
                        string rtnFile = htmlControl.CreatePDF(template, showid, DID, "Acknowledge");
                        if (!string.IsNullOrEmpty(rtnFile))
                        {
                            ShowPDF.Visible = true;
                            ShowPDF.Attributes["src"] = rtnFile;
                        }
                    }
                    catch { }

                    Response.Redirect(previousUrl);//*****
                }
                else
                {
                    Response.Redirect("ReLogin.aspx?Event=" + txtFlowName.Text.Trim());
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region PageSetting
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
            Page.MasterPageFile = masterPage;
    }

    #endregion

    #region insertRegLoginAction (insert flow data into tb_Log_RegLogin table)
    private void insertRegLoginAction(string groupid, string action)
    {
        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
        {
            string flowid = Session["Flowid"].ToString();
            string showid = Session["Showid"].ToString();
            string currentstage = Session["regstage"].ToString();
            LogRegLogin rlg = new LogRegLogin(fn);
            rlg.reglogin_regno = groupid;
            rlg.reglogin_flowid = flowid;
            rlg.reglogin_step = currentstage;
            rlg.reglogin_action = action;
            rlg.saveLogRegLogin();
        }
        else
        {
            Response.Redirect("ReLogin.aspx?Event=" + txtFlowName.Text.Trim());
        }
    }
    #endregion
}