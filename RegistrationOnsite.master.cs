﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
using Corpit.BackendMaster;
using System.Data;

public partial class RegistrationOnsite : System.Web.UI.MasterPage
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    private static string[] prepopulateshows = new string[] { "BUM338", "CEK336", "CPT327" };

    protected void Page_Load(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            ShowController shwCtrl = new ShowController(fn);
            if (shwCtrl.checkValidShow(showid))
            {
                SiteSettings sSetting = new SiteSettings(fn, showid);
                sSetting.LoadBaseSiteProperties(showid);

                if (Request.Params["t"] != null)
                {
                    string admintype = cFun.DecryptValue(Request.QueryString["t"].ToString());
                    if (admintype == BackendStaticValueClass.isWebSettings)
                    {
                    }
                    else
                    {
                        if (sSetting.isRegisterClosed())
                        {
                          //  Response.Redirect("RegClosed.aspx");
                        }
                    }
                }
                else
                {
                    if (sSetting.isRegisterClosed())
                    {
                     //   Response.Redirect("RegClosed.aspx");
                    }
                }

                if (!IsPostBack)
                {
                    string delegateID = cFun.DecryptValue(urlQuery.DelegateID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    InitSiteSettings(showid, flowid, delegateID);
                    LoadPageSetting();
                }

                if (sSetting.hastimer == "1")
                {
                    timerSession.Enabled = true;
                    if (Session["timeout"] != null)
                    {
                        timerSession_Tick(this, null);
                    }
                    else
                    {
                        mpeExpired.Show();
                    }
                }
                if (!string.IsNullOrEmpty(sSetting.GoogleAnalyticCode))
                    txtGoogleAnalyticCode.Text = sSetting.GoogleAnalyticCode;
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid, string flowid, string delegateID)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        SiteBanner.ImageUrl = sSetting.SiteBanner;
        Global.PageTitle = sSetting.SiteTitle;
        Global.Copyright = sSetting.SiteFooter;
        Global.PageBanner = sSetting.SiteBanner;

        if (!string.IsNullOrEmpty(sSetting.SiteCssBandlePath))
            BundleRefence.Path = "~/Content/" + sSetting.SiteCssBandlePath + "/";

        //Show hide Banner
        ShowControler showControler = new ShowControler(fn);
        Show sh = showControler.GetShow(showid);
        if (sh.HideShowBanner == "1")
        {
            SiteBanner.Visible = false;
            if (prepopulateshows.Contains(showid))
            {
                FlowControler Flw = new FlowControler(fn);
                FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowid);
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                {
                    bool isPrePopulateExist = checkPrePopulateDataExistByColumn("RefNo", delegateID);
                    if (isPrePopulateExist)
                    {
                        SiteBanner.Visible = true;
                    }
                }
            }
        }
    }
    private void LoadPageSetting()
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        SetPageSettting(flwObj.CurrIndexTiTle);
    }

    public void SetPageSettting(string title)
    {
        SiteHeader.Text = title;
    }

    #endregion

    #region Timer
    protected void timerSession_Tick(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            if (Session["timeout"] != null)
            {
                SiteSettings st = new SiteSettings(fn, showid);
                if (st.hastimer_type == TimerType.isSecond)//*(TimerType.isSecond=1)
                {
                    DateTime dttimeout = Convert.ToDateTime(Session["timeout"].ToString());
                    int secondsleft = (int)decimal.Parse(dttimeout.Subtract(DateTime.Now).TotalSeconds.ToString()) + 1;

                    lblTimerSecond.Text = secondsleft.ToString();

                    if (Session["isextended"] == null)
                    {
                        if (secondsleft <= 2 && secondsleft > 1)
                        {
                            Session["isextended"] = "1";
                            mpeSession.Show();
                        }
                    }

                    if (secondsleft <= 0)
                    {
                        Session["timeout"] = null;
                        Session["isextended"] = null;

                        Session.Clear();
                        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri.ToString());
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "@#$", "getSecondsRemaining();", true);
                }
                else//*(TimerType.isMinute=2)
                {
                    #region With Minutes (Comment)
                    DateTime dttimeout = Convert.ToDateTime(Session["timeout"].ToString());
                    int minutesleft = (int)decimal.Parse(dttimeout.Subtract(DateTime.Now).TotalMinutes.ToString()) + 1;

                    lblTimer.Text = minutesleft.ToString();
                    if (Session["isextended"] == null)
                    {
                        if (minutesleft <= 2 && minutesleft > 1)
                        {
                            Session["isextended"] = "1";
                            mpeSession.Show();
                        }
                    }

                    if (minutesleft <= 0)
                    {
                        Session["timeout"] = null;
                        Session["isextended"] = null;

                        Session.Clear();
                        Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri.ToString());
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "@#$", "getMinutesRemaining();", true);
                    #endregion
                }
            }
            else
            {
                String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    protected void ButtonPop_Click(object sender, EventArgs e)
    {
        mpeExpired.Show();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        mpeSession.Show();
    }

    protected void btnOkay_Click(object sender, EventArgs e)
    {
        Session["timeout"] = DateTime.Now.AddMinutes(12).ToString();
        timerSession_Tick(this, null);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "@#$", "getMinutesRemaining();", true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect("Welcome.aspx");
    }

    protected void btnStartAgain_Click(object sender, EventArgs e)
    {
        Response.Redirect("Welcome.aspx");
    }

    protected void btnVisit_Click(object sender, EventArgs e)
    {
        String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
        String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
    }
    #endregion


    #region prepopulateData
    public bool checkPrePopulateDataExistByColumn(string columnName, string columnValue)
    {
        bool isExist = false;

        try
        {
            string sql = "Select * From tb_RegPrePopulate Where " + columnName + "='" + columnValue + "'";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];//, pList
            if (dt.Rows.Count > 0)
            {
                isExist = true;
            }
        }
        catch { }

        return isExist;
    }
    #endregion
}
