﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="RecommendFriendOSEA.aspx.cs" Inherits="RecommendFriendOSEA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divHDR1" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblHDR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR2" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblHDR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR3" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblHDR3" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>

            <br />
            <div id="divInfo" runat="server">
                <asp:Panel ID="pnlInfo" runat="server">
                    <div class="form-group row" runat="server" id="divName">
                        <div class="col-md-3 col-xs-3" style="text-align:right;">
                            <asp:Label ID="lblName" runat="server" CssClass="form-control-label" Text="Name"></asp:Label>
                        </div>
                        <div class="col-md-7 col-xs-7">
                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2 col-xs-2">
                            <asp:RequiredFieldValidator ID="vcName" runat="server" Enabled="false"
                            ControlToValidate="txtName" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-group row" runat="server" id="divEmail">
                        <div class="col-md-3 col-xs-3" style="text-align:right;">
                            <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text="Email Address"></asp:Label>
                        </div>
                        <div class="col-md-7 col-xs-7">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2 col-xs-2">
                            <asp:RequiredFieldValidator ID="vcEmail" runat="server" Enabled="false"
                            ControlToValidate="txtEmail" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group row" runat="server" id="div1">
                        <div class="col-md-3 col-xs-3" style="text-align:right;">
                            &nbsp;
                        </div>
                        <div class="col-md-7 col-xs-7">
                            <asp:RegularExpressionValidator ID="validateEmail"
                            runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                            ControlToValidate="txtEmail"
                            ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-offset-5 col-lg-3 col-xs-3 col-sm-offset-4 col-sm-3 center-block">
                            <asp:Button runat="server" ID="btnSubmit" CssClass="btn MainButton btn-block" Text="Add" OnClick="btnSubmit_Click" />
                        </div>
                    </div>
                </asp:Panel>

                <br />
                <asp:Panel ID="pnllist" runat="server" Visible="false">
                    <h4>Recommended Friend List</h4>
                    <asp:GridView ID="gvList" runat="server" DataKeyNames="rmd_id" 
                    OnRowDeleting="GridView1_RowDeleting"
                    OnRowEditing="GridView1_RowEditing"
                    OnRowUpdating="GridView1_RowUpdating"
                    OnRowCancelingEdit="GridView1_RowCancelingEdit" AutoGenerateColumns="false"
                    CssClass="table borderless" GridLines="Horizontal">
                        <HeaderStyle CssClass="theadstyle" />
                        <Columns>
                            <asp:TemplateField HeaderText="Id">
                                <ItemTemplate>
                                    <%# Eval("rmd_id")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtRName" runat="server" Text='<%# Eval("rmd_fullname") %>' CssClass="form-control"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblRName" runat="server" Text='<%# Eval("rmd_fullname") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email Address">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtREmail" runat="server" Text='<%# Eval("rmd_email") %>' CssClass="form-control"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="validateEmail"
                                    runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                                    ControlToValidate="txtREmail"
                                    ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblREmail" runat="server" Text='<%# Eval("rmd_email") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ShowEditButton="True" CausesValidation="false" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" CommandName="Delete" OnClientClick="return confirm ('Are you sure you want to delete this record?')" Text="Delete" ></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div style="color:Red"><asp:Label ID="lblMsg" runat="server"></asp:Label></div>
                </asp:Panel>
            </div>

            <div id="divFTR1" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblFTR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divFTR2" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblFTR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divFTRCHK" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:CheckBoxList ID="chkFTRCHK" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                        <br />
                    </div>
                </div>
            </div>

            <div class="row" style="padding-top:20px;">
                <div class="pull-right col-lg-3 col-xs-3"> 
                    <asp:Button runat="server" Text="Next" ID="btnNext" OnClick="btnNext_Onclick" CssClass="btn btn-block  MainButton" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

