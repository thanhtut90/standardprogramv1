﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventRegMMA.aspx.cs" Inherits="EventRegMMA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <link href="Content/Default/bootstrap.css" rel="stylesheet" />
    <link href="Content/BLUES/Site.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
      <div class="container" style="padding-top: 40px;display:none; ">
            <div class=" col-md-offset-1 col-sm-offset-1" style="padding-left:20px;">
                <h4>Be among the first 1,000 to pre-register and receive a gift when you visit.</h4>
            </div>
        </div>
        <div class="container" style="padding-top: 40px;">
            <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1" style="padding-bottom:20px;">
                <asp:Button runat="server" ID="btnIndividual" class="btn MainButton btn-lg btn-block" Text="Visitor Pre-Registration" OnClick="btnIndiv_Onclick" />
            </div>
            <div class="col-md-5 col-sm-5">
                <asp:Button runat="server" ID="Button1" class="btn MainButton  btn-lg btn-block" Text="Group Pre-Registration" OnClick="btnGroup_Onclick" />
                <br />
                <p style="font-weight: bold; text-align: left;padding-left:25px;">(for 5 persons and above from the same organisation) <br/>
              Please contact Tan Yanling(<a href="mailto:yanling@mda.com.sg">yanling@mda.com.sg</a>) for group delegation comprising more than one company.
            </p>
            </div>
        </div>
    </form>
</body>
</html>
