﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ForgotPSWIDEM.aspx.cs" Inherits="ForgotPSWIDEM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>IDEM Online Registration 2020</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/Icon1.png" />

    <link href="Content/IDEMTemplate/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Content/IDEMTemplate/Site.css" rel="stylesheet" type="text/css" />
    <link href="Content/IDEMTemplate/font-awesome.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        body
        {
            font-family: Trebuchet-MS !important;
            font-size:12px !important;
        }
        .btn-danger
        {
            /*background-color:gold !important;*/
            border-radius:7px !important;
        }
        .col-xs-4, .col-xs-6
        {
            padding :0px !important;
            width:65% !important;
        }
        h3
        {
            margin:0px !important;
        }
        .iconStyle
        {
            color:#333333 !important;
        }
        .fa-lg
        {
            font-size:2.33333333em !important;
            margin-top:10px !important;
        }
        h5
        {
            font-weight:bold;
        }
        input
        {
            width:200px !important;
        }
        .btn-danger
        {
            background-color:red !important;
            font-size: 14px !important;
            height:30px !important;
        }
    </style>

</head>
<body>
    <div class="container">
    <div id="header">
        <a href="https://www.koelnmesse.com.sg/" target="_blank">
            <asp:Image ID="imgBanner" runat="server" border="0" ImageUrl="https://event-reg.biz/DefaultBanner/images/IDEM/IDEMHeaderBanner.jpg"
                    style = "position:relative; right:10px; top:0px; z-index:1; width:100%;"/>
        </a>
        <br />
    </div>

    <form id="SignupForm" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="form-group row text-center">
            <div class="col-md-12">
                <h1>WELCOME TO IDEM 2020 REGISTRATION PAGE!</h1>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-offset-2 col-md-8 col-xs-12" style="background-color:#005AAB;color:white;">
                <br />
                <br /><br />
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="col-md-3 col-xs-6 ForgotPSWStyle1">
                            Email:
                        </div>
                        <div class="col-md-3 col-xs-6 ForgotPSWStyle1">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" AutoCompleteType="None"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="col-md-4 col-xs-6 ForgotPSWStyle1">
                            Registration ID:
                        </div>
                        <div class="col-md-2 col-xs-6 ForgotPSWStyle1">
                            <asp:TextBox ID="txtRegno" runat="server" CssClass="form-control" AutoCompleteType="None"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-offset-2 col-md-4 col-xs-12">
                        <asp:RegularExpressionValidator ID="validateEmail"
                        runat="server" ErrorMessage="*Invalid Email" ForeColor="White"
                        ControlToValidate="txtEmail"
                        ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>
                <br /><br />
                <div class="row text-center">
                    <div class="col-md-offset-5 col-md-3 col-xs-5 ForgotPSWStyle1 ForgotPSWStyle2">
                        <asp:Button ID="btnTrade1" runat="server" Text="Submit" 
                                CssClass="col-md-3 col-xs-5 btn-danger LandingBtnStyle" OnClick="btnSubmit_Click"/>
                    </div>
                </div>
                <br />
                <div class="row text-center">
                    <div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblerror" runat="server" Text="Label" Visible="false" ForeColor="White" ></asp:Label>
                    </div>
                </div>
                <br /><br /><br /><br /><br />
            </div>
         </div>
        <div class="form-group row">
            <br />
            <div class="col-md-offset-2 col-md-8 col-xs-12">
                <div class="row text-center">
                <div class="col-md-offset-5 col-md-3 col-xs-5 ForgotPSWStyle1 ForgotPSWStyle2">
                    <asp:Button ID="btnTradeGroup1" runat="server" Text="Back" 
                                CssClass="col-md-3 col-xs-5 btn-danger LandingBtnStyle" OnClick="Button1_Click"/>
                </div>
                </div>
            </div>
        </div>
        <br /><br /><br /><br /><br />
        <%--<div class="row">
            <footer>
                <div class="container">
                    <div class="row" id="footercontact">
                        <div class="col-md-4 col-xs-12" style="float:left;text-align:left;" >
                            <h5>Stay Connected with Us</h5>
                            <div class="col-md-2 col-xs-3 landingSocialStyle1" style="padding-left:0px;"><p class="social"><a id="aFacebook" runat="server" href="#" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Facebook.png" /></a></p></div>
                            <div class="col-md-2 col-xs-3" style="padding-left:0px;"><p class="social"><a id="aTwitter" runat="server" href="#" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Twitter.png" /></a></p></div>
                            <div class="col-md-2 col-xs-3" style="padding-left:0px;"><p class="social"><a id="aInstagram" runat="server" href="#" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Instagram.png" /></a></p></div>
                            <div class="col-md-2 col-xs-3" style="padding-left:0px;"><p class="social"><a id="aLinkedIn" runat="server" href="#" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/IN.png" /></a></p></div>
                        </div>
                        <div class="col-md-8 col-xs-12" style="float:right;text-align:left;" >
                            <div class="col-md-2 col-xs-12" style="padding-right:0px;">&nbsp;</div>
                            <div class="col-md-2 col-xs-3" style="padding-right:0px;">
                                <h5>Endorsed By</h5>
                                <p class="social">
                                    <a id="a1" runat="server" href="#" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/AIFLogo.jpg" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-3" style="padding-right:0px;">
                                <h5>Supported By</h5>
                                <p class="social">
                                    <a id="a2" runat="server" href="#" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/SECBLogo.jpg" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-3" style="padding-right:0px;">
                                <h5>Held in</h5>
                                <p class="social">
                                    <a id="a3" runat="server" href="#" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/BrandSGLogo.jpg" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-3" style="padding-right:0px;">
                                <h5>Organized by</h5>
                                <p class="social">
                                    <a id="a4" runat="server" href="#" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/SDALogo.jpg" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-12" style="padding-right:0px;">
                                <h5>&nbsp;</h5>
                                <p class="social">
                                    <a id="a5" runat="server" href="#" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/KMLogo.jpg" /></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>--%>
        <div class="row">
            <footer>
                <div class="container">
                    <div class="row" id="footercontact">
                        <div class="col-md-4 col-xs-12" style="float:left;text-align:left; font-family:'Trebuchet MS';" >
                            <h5 style="font-family:'Trebuchet MS';padding-left:10px;">Stay Connected with Us</h5>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aFacebook" runat="server" target="_blank" href="https://www.facebook.com/idemsingapore/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Facebook.png" /></a></p></div>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aTwitter" runat="server" target="_blank" href="https://twitter.com/IDEMSingapore" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Twitter.png" /></a></p></div>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aInstagram" runat="server" target="_blank" href="https://www.instagram.com/idem.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Instagram.png" /></a></p></div>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aLinkedIn" runat="server" target="_blank" href="https://www.linkedin.com/company/6440434/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/IN.png" /></a></p></div>
                        </div>
                        <div class="col-md-8 col-xs-12" style="float:right;text-align:left;" >
                            <div class="col-md-2 col-xs-12" style="padding-right:0px;">&nbsp;</div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;padding-left:10px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Endorsed By</h5>
                                <p class="social">
                                    <a id="a1" runat="server" target="_blank" href="https://www.stb.gov.sg/content/stb/en/assistance-and-licensing/other-assistance-resources-overview/AIF28Non-Financial-Assistance.html" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/AIFLogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Supported By</h5>
                                <p class="social">
                                    <a id="a2" runat="server" target="_blank" href="https://www.visitsingapore.com/mice/en/about-us/about-secb/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/SECBLogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Held in</h5>
                                <p class="social">
                                    <a id="a3" runat="server" target="_blank" href="https://www.visitsingapore.com/en/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/BrandSGLogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Organised by</h5>
                                <p class="social">
                                    <a id="a4" runat="server" target="_blank" href="http://sda.org.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/SDALogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">&nbsp;</h5>
                                <p class="social">
                                    <a id="a5" runat="server" target="_blank" href="https://www.koelnmesse.com.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/KMLogo.png" /></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </form>
    </div>
</body>
</html>
