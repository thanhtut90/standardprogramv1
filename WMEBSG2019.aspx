﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WMEBSG2019.aspx.cs" Inherits="WMEBSG2019" %>

<meta content="width=device-width, initial-scale=1" name="viewport" />
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WORLD'S MOST EXPERIMENTAL BARTENDER</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <link href="Content/Default/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/GlenfiddichCSS.css" rel="stylesheet" />

    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <style>
        body {
            background: url("https://event-reg.biz/DefaultBanner/images/WMEB/Background.jpg");
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center;
            background-repeat: no-repeat;
            overflow-x: hidden;
            background-size: cover;
        }

        .modal {
            overflow: hidden !important;
            overflow-y: hidden !important;
            max-width: 50% !important;
        }

        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
 
        }
      .centerPopUp {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
 
        }
        .PopupAgeMsg {
            text-align: center;
            text-transform: uppercase;
            font-weight: bold;
            font-size: 18px;
            padding-top: 30px;
            letter-spacing: 1px;
        }

        .btnCenter {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
        /* On screens that are 600px wide or less, the background color is olive */
        @media screen and (max-width: 600px) {
            body {
                background: url("https://event-reg.biz/DefaultBanner/images/WMEB/Background.jpg");
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position-x: center;
                background-repeat: no-repeat;
                overflow-x: hidden;
                background-position: center;
                background-size: cover;
            }


            .modal {
                max-width: 80% !important;
            }
        }

        @media screen and (max-width: 770px) {
            .PopupAgeMsg {
                font-size: 11px;
            }

            .center {
                width: 70%;
               
            }
            .centerPopUp {
                width: 70%; 
            }    
        }
    </style>
    <script>
        function callModal() {
            $("#mdlPopUp").modal({
                fadeDuration: 100,
                clickClose: false,
                showClose: false
            });

        }
        function CloseDialog() {
            alert("You are too YOUNG to enter the site");
            //  $("#mdlPopUp").modal('close');
        }
    </script>
</head>

<body style="font-family: GFRegular;">
    <form id="form1" runat="server">
    
        <div class="container" style="padding-top: 5px;">        
                <div style="text-align: center;" class="col-lg-8  col-lg-offset-2 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8  col-xs-10">

        </div>
            <div class="col-lg-1 col-sm-2 col-md-1 col-xs-1" style="padding-left: 0px; padding-right: 7px;">
                <a href="https://www.facebook.com/WMEB2019SG/" target="_blank"><img src="https://event-reg.biz/DefaultBanner/images/WMEB/fbIcon.png" style="width: 70px;" /> </a>
            </div>     
        </div>
        <div class="container">
                <div >
                <img src="https://event-reg.biz/DefaultBanner/images/WMEB/sg/WelcomeHeader.png" class="center" />
            </div>
        </div>
        <div class="container">
            <div class="co-md-3    hidden-xs ">
            <p style="text-align: center; text-transform: uppercase; font-weight: bold; font-size: 20px; padding-top: 30px; letter-spacing: 1px;">
                Have you got what it takes to be   <br />
                The world's  most experimental bartender 2019?
            </p>
                </div>
           <div class="hidden-md    hidden-sm   hidden-lg">
            <p style="text-align: center; text-transform: uppercase; font-weight: bold; font-size: 20px; padding-top: 30px; letter-spacing: 1px;" >
                Have you got what it takes to be
                The world's most experimental bartender 2019?
            </p>
                </div>
            <p style="text-align: center;">Submit your experimental Glenfiddich 12 Year old serve today</p>
        </div>
        <div class="container">
            <div class="col-lg-2  col-sm-2 col-xs-offset-2 col-sm-offset-4   col-lg-offset-4" style="padding-top: 30px;">
                <input type="image" class="img-responsive btnCenter" src="https://event-reg.biz/DefaultBanner/images/WMEB/Enter Now.jpg" onclick="callModal(); return false;" />
            </div>
            <div class="col-lg-2  col-sm-2 col-xs-offset-2  " style="padding-top: 30px;">
                <input type="image" class="img-responsive btnCenter" src="https://event-reg.biz/DefaultBanner/images/WMEB/Find Out More.jpg" onclick="window.open('https://www.facebook.com/WMEB2019SG/'); return false;" />
            </div>
        </div>
        <div class="row">
            <div class="modal" id="mdlPopUp">
                <div class="row">
                    <img src="https://event-reg.biz/DefaultBanner/images/WMEB/sg/WelcomeHeader.png" class="centerPopUp" />

                </div>
                <div class="row">
                    <p class="PopupAgeMsg">
                        Please confirm you are above
                        <br />
                        The legal drinking age of 18
                    </p>
                </div>
                <div class="container">
                    <div class="col-md-3  col-sm-5   col-sm-offset-2 col-md-offset-3" style="padding-top: 30px;">
                        <input type="image" src="https://event-reg.biz/DefaultBanner/images/WMEB/Yes.jpg" class="img-responsive" onclick="window.location.href = 'EventReg?Event=WMEBSG19'" />
                    </div>
                    <div class="col-md-3  col-sm-5  " style="padding-top: 30px;">
                        <a rel="modal:close" onclick="alert('You are too young to enter site.');">
                            <img src="https://event-reg.biz/DefaultBanner/images/WMEB/NO.jpg" class="img-responsive" /></a>
                    </div>
                </div>
                <div class="row">
                    <p style="font-style: italic; text-align: center; padding-top: 20px;">
                        We are committed to ensuring Glenfiddich is enjoyed responsibly!
                    </p>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
