﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;

public partial class ReLoginMasterIDEM : System.Web.UI.MasterPage
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            InitSiteSettings(showid);
            bindMenuLink();
            string regno = Session["Groupid"].ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);

            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
            if (fmaster != null)
            {
                txtFlowName.Text = fmaster.FlowName;
                if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null)
                {
                    LoadPageSetting();
                    bindWelcomeRegistrantName(Session["Groupid"].ToString(), Session["Showid"].ToString());
                    bindMsg(flowid, showid);//***added on 13-9-2019
                }
                else
                {
                    Response.Redirect("ReLogin.aspx?Event=" + txtFlowName.Text.Trim());
                }
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        SiteBanner.ImageUrl = sSetting.SiteBanner;
        Global.PageTitle = sSetting.SiteTitle;
        Global.Copyright = sSetting.SiteFooter;
        Global.PageBanner = sSetting.SiteBanner;

        if (!string.IsNullOrEmpty(sSetting.SiteCssBandlePath))
            BundleRefence.Path = "~/Content/" + sSetting.SiteCssBandlePath + "/";
    }
    private void LoadPageSetting()
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        SetPageSettting(flwObj.CurrIndexTiTle);
    }

    public void SetPageSettting(string title)
    {
        SiteHeader.Text = title;
    }
    #endregion

    #region bindWelcomeRegistrantName
    private void bindWelcomeRegistrantName(string groupID, string showid)
    {
        if (Session["Regno"] == null)
        {
            #region Contact Person (Group)
            DataTable dtG = new DataTable();
            RegGroupObj rgg = new RegGroupObj(fn);
            dtG = rgg.getRegGroupByID(groupID, showid);
            if(dtG.Rows.Count > 0)
            {
                string fullname = dtG.Rows[0]["RG_ContactFName"].ToString() + " " + dtG.Rows[0]["RG_ContactLName"].ToString();
                lblWelcomName.Text = fullname;
            }
            #endregion
        }
        else
        {
            #region Delegate
            string regno = Session["Regno"].ToString();
            DataTable dtD = new DataTable();
            RegDelegateObj rgd = new RegDelegateObj(fn);
            dtD = rgd.getDataByGroupIDRegno(groupID, regno, showid);
            if (dtD.Rows.Count > 0)
            {
                string fullname = dtD.Rows[0]["reg_OName"].ToString();
                lblWelcomName.Text = fullname;
            }
            #endregion
        }
    }
    #endregion

    #region bindMenuLink (bind ul, li in div(divMenu) dynamically according to flow)
    private void bindMenuLink()
    {
        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["RegStatus"] != null)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string groupid = Session["Groupid"].ToString();
            string flowid = Session["Flowid"].ToString();

            FlowControler fControl = new FlowControler(fn);

            //StatusSettings statusSet = new StatusSettings(fn);
            //if (Session["RegStatus"].ToString() == statusSet.Success.ToString())
            {
                bool isGroup = false;
                string regno = "";
                if (Session["Regno"] == null)
                {
                    isGroup = true;
                }
                else
                {
                    regno = Session["Regno"].ToString();
                }

                DataTable dtMenu = new DataTable();
                dtMenu = fControl.GetReloginMenu(flowid, "0");
                if (dtMenu.Rows.Count > 0)
                {
                    FlowMaster flwMasterConfig = fControl.GetFlowMasterConfig(flowid);

                    HtmlGenericControl divRow = new HtmlGenericControl("div");
                    divRow.Attributes.Add("class", "row");

                    string navigateUrl = "";
                    string imagePath = "";
                    #region Home
                    string homePage = "LandingIDEM.aspx";
                    imagePath = "images/Relogin/IDEM/Home.png";
                    bindPage(homePage, imagePath, ref divRow);
                    #endregion

                    bool confExist = fControl.checkPageExist(flowid, SiteDefaultValue.constantConfName);
                    if (confExist)
                    {
                        #region Conference Registration
                        #region 1st Row
                        foreach (DataRow dr in dtMenu.Rows)
                        {
                            string scriptid = dr["reloginref_scriptID"].ToString().Trim();
                            string menuname = dr["reloginmenu_name"].ToString().Trim();
                            string stepseq = dr["step_seq"].ToString().Trim();
                            string menupageHeader = dr["step_label"].ToString().Trim();
                            if (menuname == SiteDefaultValue.constantDelegateName)
                            {
                                if (flwMasterConfig.FlowType != SiteFlowType.FLOW_GROUP)
                                {
                                    #region DownloadConfirmationLetter
                                    scriptid = "Reg_DownloadConfirmation";
                                    navigateUrl = getRoute(stepseq, scriptid);
                                    imagePath = "images/Relogin/IDEM/DownloadConfirmation.png";
                                    InvoiceControler invControler = new InvoiceControler(fn);
                                    string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                                    string invID = "";
                                    Invoice invLatest = invControler.GetLatestCompleteInvoice(invOwnerID, groupid);
                                    string invSuccess = ((int)StatusValue.Success).ToString();
                                    if (invLatest != null)
                                    {
                                        if (invLatest.Status == invSuccess)
                                        {
                                            invID = invLatest.InvoiceID;
                                        }
                                    }
                                    bindPage(navigateUrl + "&VDD=" + cFun.EncryptValue(invID), imagePath, ref divRow);
                                    #endregion
                                }
                            }
                            else
                            {
                                if (menuname == SiteDefaultValue.constantConferenceName && flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                                {
                                    /*not showing Reg_Conference.aspx page*/
                                }
                                else
                                {
                                    //if (menuname == SiteDefaultValue.constantConferenceName)
                                    if (scriptid.Contains(SiteDefaultValue.constantConfirmationPage))
                                    {
                                        #region View Purchased Items(Reg_Conference.aspx)
                                        navigateUrl = getRoute(stepseq, scriptid);
                                        imagePath = "images/Relogin/IDEM/ViewPurchasedItem.png";
                                        bindPage(navigateUrl, imagePath, ref divRow);
                                        #endregion
                                    }
                                }
                            }
                        }
                        divMenu.Controls.Add(divRow);
                        #endregion

                        #region 2nd Row
                        divRow = new HtmlGenericControl("div");
                        divRow.Attributes.Add("class", "row");
                        for (int i = 1; i <= 3; i++)
                        {
                            string scriptid = "";
                            if (i == 1)
                            {
                                #region Change Password
                                scriptid = "Reg_ChangePassword.aspx";
                                navigateUrl = getRoute(i.ToString(), scriptid);
                                imagePath = "images/Relogin/IDEM/ChangePassword.png";
                                bindPage(navigateUrl, imagePath, ref divRow);
                                #endregion
                            }
                            if (i == 2)
                            {
                                #region Download Receipt
                                scriptid = ReLoginStaticValueClass.regReceiptPage;
                                navigateUrl = getRoute(i.ToString(), scriptid);
                                imagePath = "images/Relogin/IDEM/DownloadReceipt.png";
                                bindPage(navigateUrl, imagePath, ref divRow);
                                #endregion
                            }
                            if (i == 3)
                            {
                                #region Logout
                                scriptid = "Logout.aspx";
                                navigateUrl = getRoute(i.ToString(), scriptid);
                                imagePath = "images/Relogin/IDEM/Logout.png";
                                bindPage(navigateUrl, imagePath, ref divRow);
                                #endregion
                            }
                        }
                        divMenu.Controls.Add(divRow);
                        #endregion
                        #endregion
                    }
                    else
                    {
                        #region Visitor Registration
                        #region 1st Row
                        string newScriptId = "";
                        foreach (DataRow dr in dtMenu.Rows)
                        {
                            string scriptid = dr["reloginref_scriptID"].ToString().Trim();
                            string menuname = dr["reloginmenu_name"].ToString().Trim();
                            string stepseq = dr["step_seq"].ToString().Trim();
                            string menupageHeader = dr["step_label"].ToString().Trim();
                            if (menuname == SiteDefaultValue.constantDelegateName)
                            {
                                if (!isGroup)
                                {
                                    newScriptId = scriptid;
                                    #region Visitor View My Registration
                                    navigateUrl = getRoute(stepseq, scriptid);
                                    imagePath = "images/Relogin/IDEM/VisitorViewRegistration.png";
                                    bindPage(navigateUrl, imagePath, ref divRow);
                                    #endregion
                                }
                            }
                            else
                            {
                                if (menuname == SiteDefaultValue.constantConferenceName && flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                                {
                                    /*not showing Reg_Conference.aspx page*/
                                }
                                else
                                {
                                    if (menuname.Contains("Member"))
                                    {
                                        if (isGroup)
                                        {
                                            newScriptId = scriptid;
                                            #region Visitor View My Registration
                                            navigateUrl = getRoute(stepseq, scriptid);
                                            imagePath = "images/Relogin/IDEM/VisitorViewRegistration.png";
                                            bindPage(navigateUrl, imagePath, ref divRow);
                                            #endregion
                                        }
                                    }
                                    else if (menuname.Contains("Group Organiser Contact Information"))
                                    {
                                        if (isGroup)
                                        {
                                            newScriptId = scriptid;
                                            #region Visitor View My Registration
                                            navigateUrl = getRoute(stepseq, scriptid);
                                            imagePath = "images/Relogin/IDEM/VisitorViewRegistration.png";
                                            bindPage(navigateUrl, imagePath, ref divRow);
                                            #endregion
                                        }
                                    }
                                    else if (!menuname.Contains("Group Organiser Contact Information") || (menuname.Contains("Summary") && !isGroup))
                                    {
                                        if (!isGroup)
                                        {
                                            #region Visitor Upgrade My Registration
                                            scriptid = newScriptId;
                                            navigateUrl = getRoute(stepseq, scriptid);
                                            imagePath = "images/Relogin/IDEM/VisitorUpgrade.png";
                                            bindPage(navigateUrl, imagePath, ref divRow, (isGroup ? false : true));
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                        divMenu.Controls.Add(divRow);
                        #endregion

                        #region 2nd Row
                        divRow = new HtmlGenericControl("div");
                        divRow.Attributes.Add("class", "row");
                        for (int i = 1; i <= 3; i++)
                        {
                            string scriptid = "";
                            if (i == 1)
                            {
                                #region Change Password
                                scriptid = "Reg_ChangePassword.aspx";
                                navigateUrl = getRoute(i.ToString(), scriptid);
                                imagePath = "images/Relogin/IDEM/ChangePassword.png";
                                bindPage(navigateUrl, imagePath, ref divRow);
                                #endregion
                            }
                            if (i == 2)
                            {
                                #region DownloadConfirmationLetter
                                scriptid = "Reg_DownloadConfirmation";
                                navigateUrl = getRoute(i.ToString(), scriptid);
                                imagePath = "images/Relogin/IDEM/DownloadConfirmation.png";
                                bindPage(navigateUrl, imagePath, ref divRow);
                                #endregion
                            }
                            if (i == 3)
                            {
                                #region Logout
                                scriptid = "Logout.aspx";
                                navigateUrl = getRoute(i.ToString(), scriptid);
                                imagePath = "images/Relogin/IDEM/Logout.png";
                                bindPage(navigateUrl, imagePath, ref divRow);
                                #endregion
                            }
                        }
                        divMenu.Controls.Add(divRow);
                        #endregion
                        #endregion
                    }
                }
            }
        }
    }
    private void bindPage(string page, string imagePath, ref HtmlGenericControl divRow, bool isEnabledLink = true)
    {
        HtmlGenericControl div;
        div = new HtmlGenericControl("div");
        div.Attributes.Add("class", "col-md-4 col-xs-4 menuImageStyle");
        HtmlGenericControl a;
        a = new HtmlGenericControl("a");
        a.Attributes.Add("href", page);
        if(!isEnabledLink)
        {
            a.Attributes.Add("class", "disabled");
        }
        HtmlGenericControl img;
        img = new HtmlGenericControl("img");
        img.Attributes.Add("src", imagePath);
        img.Attributes.Add("class", "img-responsive");
        a.Controls.Add(img);
        div.Controls.Add(a);
        divRow.Controls.Add(div);
    }
    #endregion

    #region getRoute (get route according to login id(Session["Groupid"]) but no use)
    public string getRoute(string step, string page)
    {
        string route = string.Empty;

        if (Session["Groupid"] != null)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            FlowControler Flw = new FlowControler(fn);
            RegGroupObj rgp = new RegGroupObj(fn);
            string grpNum = Session["Groupid"].ToString();
            string FlowID = rgp.getFlowID(grpNum, showid);

            showid = cFun.EncryptValue(showid);
            FlowID = cFun.EncryptValue(FlowID);
            step = cFun.EncryptValue(step);

            if (page == "Logout.aspx")//ReLoginStaticValueClass.regLogoutPage)
            {
                route = page + "?Event=" + GetEventNameByShowIDFlowID(cFun.DecryptValue(showid), cFun.DecryptValue(FlowID));//Flw.MakeFullURL(page, FlowID, showid);
            }
            else
            {
                string regno = string.Empty;
                if (Session["Regno"] != null)
                {
                    regno = cFun.EncryptValue(Session["Regno"].ToString());

                    grpNum = cFun.EncryptValue(grpNum);
                    route = Flw.MakeFullURL(page, FlowID, showid, grpNum, step, regno);
                }
                else
                {
                    grpNum = cFun.EncryptValue(grpNum);
                    route = Flw.MakeFullURL(page, FlowID, showid, grpNum, step);
                }
            }
        }
        else
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
        }

        return route;
    }
    #endregion

    private string GetEventNameByShowIDFlowID(string showid, string flowid)
    {
        string flowName = string.Empty;
        try
        {
            string sql = "select FLW_Name from tb_site_flow_master  where ShowID=@ShowID And FLW_ID=@FLW_ID and Status=@Status";
            //'EAD351'//FLW_Name=@FName 

            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FLW_ID", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            spar3.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                flowName = dList.Rows[0]["FLW_Name"].ToString();
            }
        }
        catch { }
        return flowName;
    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        //FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        //string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        //string flowid = cFun.DecryptValue(urlQuery.CurrShowID);

        //string page = ReLoginStaticValueClass.regLogoutPage;
        //string param = page + "?Event=" + GetEventNameByShowIDFlowID(showid, flowid);
        //var url = "http://localhost:64589/" + param;

        //Response.Clear();
        //var sb = new System.Text.StringBuilder();
        //sb.Append("<html>");
        //sb.AppendFormat("<body onload='document.forms[0].submit()'>");
        //sb.AppendFormat("<form action='{0}' method='post'>", url);
        //sb.Append("</form>");
        //sb.Append("</body>");
        //sb.Append("</html>");
        //Response.Write(sb.ToString());
        //Response.End();
    }

    #region bindMsg
    private void bindMsg(string flowid, string showid)
    {
        FlowControler flwControl = new FlowControler(fn);
        FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
        if (!string.IsNullOrEmpty(flwMasterConfig.FlowReloginHomeCompletePageMsg))
        {
            lblNote.Text = Server.HtmlDecode(!string.IsNullOrEmpty(flwMasterConfig.FlowReloginHomeCompletePageMsg) ? flwMasterConfig.FlowReloginHomeCompletePageMsg : "");
        }
        else
        {
            lblNote.Text = "";
        }
    }
    #endregion
}
