﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReloginFormMaster.master" AutoEventWireup="true" CodeFile="Reg_Index.aspx.cs" Inherits="Reg_Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .msgLgFont
        {
            font-size:14px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="form-group" id="divMsg" runat="server">
        <div class="form-group row">
            <div class="col-md-12 msgLgFont">
                Welcome to <%=showname %> relogin page.
                <br /><br />
                Your member registration was incomplete. 
                <br /><br />
                Please click <asp:HyperLink ID="hpLink" runat="server" Text="here" ></asp:HyperLink> to continue with your registration.
            </div>
        </div>
    </div>

    <div class="form-group" id="divReloginIndexMsg" runat="server" visible="false">
        <div class="form-group row">
            <div class="col-md-12 msgLgFont">
                <asp:Label ID="lblTemplate" runat="server"></asp:Label>
                <br />
                <%--To edit your registration information and selection, please click <asp:HyperLink ID="aLink" runat="server" Text="here" ></asp:HyperLink> to continue.--%>
                Please click <asp:HyperLink ID="aLink" runat="server" Text="here" ></asp:HyperLink> to continue with your registration for <%=flowDesc %>.
                <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
            </div>
        </div>
    </div>

</asp:Content>

