﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using Corpit.Questionnaire;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Logging;
using System.Globalization;
using Corpit.BackendMaster;
public partial class RegGroupMDA : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    const int MinShowMember = 5;
    const int MaxShowMember = 20;
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Affiliation = "Affiliation";
    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";
    protected void Page_init(object sender, EventArgs e)
    {
        loadQuestionnaire();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                bindFlowNote(showid, urlQuery);//***added on 25-6-2018
                LoadCountry();
                LoadSalDropdownList(showid);
                BindQADropDownList(showid);
                LoadMemberList();
                ShowHideMemberPanel(ddlTotalMember.SelectedValue);
                setDynamicForm(flowid,showid);
            }
        }
    }


    protected void ddlTotalMember_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideMemberPanel(ddlTotalMember.SelectedValue);
    }
    protected void ddlProfession_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {

        try
        {
            if (ddlDepartment.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlDepartment.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherDept);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divDepartmentOther.Visible = true;
                        }
                        else
                        {
                            divDepartmentOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtDepartmentOther.Attributes.Add("required", "");
                            vcDeptmOther.Enabled = true;
                        }
                        else
                        {
                            //txtDepartmentOther.Attributes.Remove("required");
                            vcDeptmOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divDepartmentOther.Visible = false;
                    //txtDepartmentOther.Attributes.Remove("required");
                    vcDeptmOther.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        { }
    }

    protected void ddlOrganization_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlOrganization.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlOrganization.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherOrg);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divOrgOther.Visible = true;
                        }
                        else
                        {
                            divOrgOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtOrgOther.Attributes.Add("required", "");
                            vcOrgOther.Enabled = true;
                        }
                        else
                        {
                            //txtOrgOther.Attributes.Remove("required");
                            vcOrgOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divOrgOther.Visible = false;
                    //txtOrgOther.Attributes.Remove("required");
                    vcOrgOther.Enabled = false;
                }

            }
        }
        catch (Exception ex)
        { }
    }

    protected void txt1Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt1Sal.SelectedItem.Text, ref divSal1Other);
    }



    protected void txt2Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt2Sal.SelectedItem.Text, ref divSal2Other);
    }

    protected void txt3Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt3Sal.SelectedItem.Text, ref divSal3Other);
    }

    protected void txt4Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt4Sal.SelectedItem.Text, ref divSal4Other);
    }

    protected void txt5Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt5Sal.SelectedItem.Text, ref divSal5Other);
    }

    protected void txt6Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt6Sal.SelectedItem.Text, ref divSal6Other);
    }

    protected void txt7Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt7Sal.SelectedItem.Text, ref divSal7Other);
    }

    protected void txt8Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt8Sal.SelectedItem.Text, ref divSal8Other);
    }

    protected void txt9Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt9Sal.SelectedItem.Text, ref divSal9Other);
    }

    protected void txt10Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt10Sal.SelectedItem.Text, ref divSal10Other);
    }

    protected void txt11Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt11Sal.SelectedItem.Text, ref divSal11Other);
    }

    protected void txt12Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt12Sal.SelectedItem.Text, ref divSal12Other);
    }

    protected void txt13Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt13Sal.SelectedItem.Text, ref divSal13Other);
    }

    protected void txt14Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt14Sal.SelectedItem.Text, ref divSal14Other);
    }

    protected void txt15Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt15Sal.SelectedItem.Text, ref divSal15Other);
    }

    protected void txt16Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt16Sal.SelectedItem.Text, ref divSal16Other);
    }

    protected void txt17Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt17Sal.SelectedItem.Text, ref divSal17Other);
    }

    protected void txt18Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt18Sal.SelectedItem.Text, ref divSal18Other);
    }

    protected void txt19Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt19Sal.SelectedItem.Text, ref divSal19Other);
    }

    protected void txt20Sal_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideSalOther(txt20Sal.SelectedItem.Text, ref divSal20Other);
    }

    protected void ddlSalutation_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        //IF valid 
        //Save Contact Person
        //Save QA
        //Save Contact Person

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        bool isValid = true;
        /*QA Save[added on 31-12-2018*/
        bool hasQA = false;
        bool isQAValid = true;
         
        Boolean isvalidpage = isValidPage(showid, urlQuery);
        if (isvalidpage)
        {
            QAController qCtrl = new QAController(qfn);
            string qID = getQID(showid, flowid, BackendRegType.backendRegType_Delegate);
            if (!string.IsNullOrEmpty(qID) && qID != "0")
            {
                qnaire_id = qID;
                hasQA = true;
                if (!validation())
                {
                    isQAValid = false;
                }
            }
            /*QA Save[added on 31-12-2018*/
            if (!isQAValid)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please complete the compulsory field(s).');", true);
                return;
            }
            {
                FlowControler fCon = new FlowControler(fn);
                FlowMaster flwMaster = fCon.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
                string groupID = SaveContactPerson();

                if (!string.IsNullOrEmpty(groupID))
                {
                    bool isOK = SaveAllMember(groupID);
                    if (isOK)
                    {
                        FlowControler flwObj = new FlowControler(fn, urlQuery);
                        string showID = urlQuery.CurrShowID;
                        string page = flwObj.NextStepURL();
                        string step = flwObj.NextStep;
                        string FlowID = flwObj.FlowID;
                        string grpNum = "";
                        grpNum = urlQuery.GoupRegID;
                        string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, groupID, BackendRegType.backendRegType_Group);
                        Response.Redirect(route);
                    }
                }
            }
        }
    }
    #region QA
    #region Questionnaire Declaration
    private string _qnaire_id;
    private string _quest_id;
    private string _user_id;
    public string qnaire_id
    {
        get
        {
            return _qnaire_id;
        }
        set
        {

            _qnaire_id = value;
        }
    }
    public string quest_id
    {
        get
        {
            return _quest_id;
        }
        set
        {

            _quest_id = value;
        }
    }
    public string user_id
    {
        get
        {
            return _user_id;
        }
        set
        {

            _user_id = value;
        }
    }
    const string validationGroup = "Validate";
    int newregno = 0, layer = 0, Col = 0;
    QuesFunctionality qfn = new QuesFunctionality();
    #endregion
    #region loadQuestionnaire
    private void loadQuestionnaire()
    {
        try
        {
            QAController qCtrl = new QAController(qfn);
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            string regno = cFun.DecryptValue(urlQuery.DelegateID);
            string qID = getQID(showid, flowid, BackendRegType.backendRegType_Delegate);
            if (!string.IsNullOrEmpty(qID) && qID != "0")
            {
                qnaire_id = qID;
                user_id = regno;

                int layer = 1;
                Col = qCtrl.getColByQnaireID(qnaire_id);
                PanelMain.Controls.Clear();
                HtmlTable dTable = new HtmlTable();
                dTable.Attributes.Add("class", "table");//* table-striped
                HtmlTableRow dTRow = new HtmlTableRow();

                DataTable dt = new DataTable();
                DataTable dtsub = new DataTable();

                #region new
                string sqlq = string.Empty;
                if (!string.IsNullOrEmpty(quest_id))
                {
                    string dep_quest_id = string.Empty;
                    DataTable dtdep_quest_id = qCtrl.getDependentQuestItemByQuestID(quest_id);//*
                    if (dtdep_quest_id.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtdep_quest_id.Rows)
                        {
                            if (dr["qitem_depquest_id"].ToString().Contains(','))
                            {
                                string[] arr = dr["qitem_depquest_id"].ToString().Split(',');
                                for (int i = 0; i < arr.Count(); i++)
                                {
                                    dep_quest_id += ",'" + arr[i].ToString() + "'";
                                }
                            }
                            else
                            {
                                dep_quest_id += ",'" + dr["qitem_depquest_id"].ToString() + "'";
                            }

                        }
                        if (!string.IsNullOrEmpty(dep_quest_id))
                        {
                            quest_id = "'" + quest_id + "'" + dep_quest_id;
                        }
                    }
                    else
                    {
                        quest_id = "'" + quest_id + "'";
                    }
                    dt = qCtrl.getQuestionByQuestID(quest_id);//*
                }
                else
                {
                    dt = qCtrl.getQuestionByQnaireID(qnaire_id);//*
                }
                if (dt.Rows.Count > 0)
                {
                    #region Option 2 Col 2 & 3
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["quest_class"].ToString() == "Indirect")
                        {
                            int isVis = 0;
                            generateDiv(i.ToString(), isVis);
                        }
                        else
                        {
                            int isVis = 1;
                            generateDiv(i.ToString(), isVis);
                        }

                        generateInnerDiv(i.ToString(), layer);
                        string questionno = string.Empty;
                        questionno = i.ToString();
                        Label label = new Label();

                        string chk = qCtrl.getQuestItemIDByQuestID(dt.Rows[i]["quest_id"].ToString());//*

                        #region Question Type Text & Question without Items
                        if (dt.Rows[i]["quest_input_type"].ToString() == "Text")
                        {
                            addHeaderTableRowToHtmlTable(dTable, 1);//*

                            #region Question Type = Text
                            string strlbl = string.Empty;
                            if (dt.Rows[i]["quest_Mandatory"].ToString() == "True")
                            {
                                strlbl = "<label style='color:red'>*</label>";
                            }
                            questionno = dt.Rows[i]["quest_no_disp"].ToString();
                            if (questionno != string.Empty & questionno != "-")
                            {
                                label.Text = questionno + " " + dt.Rows[i]["quest_desc"].ToString() + strlbl;
                                label.ID = dt.Rows[i]["quest_id"].ToString();
                            }
                            else
                            {
                                label.Text = dt.Rows[i]["quest_desc"].ToString() + strlbl;
                                label.ID = dt.Rows[i]["quest_id"].ToString();
                            }
                            label.CssClass = "title";
                            label.Style.Add("font-weight", "bold");
                            label.Style.Add("padding-left", "30px");
                            addControlToDiv(i.ToString(), layer, label);
                            newLine(i.ToString(), layer);

                            HtmlTableCell dTCell = new HtmlTableCell();

                            dTCell.Controls.Add(getTextBox(dt.Rows[i]["quest_id"].ToString(), i, layer));
                            dTRow.Controls.Add(dTCell);
                            dTable.Controls.Add(dTRow);
                            dTRow = new HtmlTableRow();

                            addControlToDiv(i.ToString(), layer, dTable);
                            dTable = new HtmlTable();
                            dTable.Attributes.Add("class", "table");//* table-striped
                            #endregion
                        }
                        else if (dt.Rows[i]["quest_input_type"].ToString() != "Text" && chk == "0")
                        {
                            addHeaderTableRowToHtmlTable(dTable, 1);

                            #region Question without Items
                            string input_type = qCtrl.getInputTypeByQuestID(dt.Rows[i]["quest_id"].ToString());
                            HtmlInputCheckBox checkbox = new HtmlInputCheckBox();
                            checkbox.Value = dt.Rows[i]["quest_id"].ToString();
                            checkbox.ID = dt.Rows[i]["quest_id"].ToString();
                            checkbox.Attributes.Add("onclick", "multipTextbox (this.checked, this);");
                            checkbox.Attributes.Add("class", dt.Rows[i]["quest_input_type"].ToString());

                            HtmlTableCell dTCell = new HtmlTableCell();

                            label.Text = dt.Rows[i]["quest_desc"].ToString();
                            label.CssClass = "title chklabel";
                            label.Attributes.Add("style", "margin-left: 10px;vertical-align: top");

                            dTCell.Controls.Add(checkbox);
                            dTCell.Controls.Add(label);
                            dTRow.Controls.Add(dTCell);
                            dTable.Controls.Add(dTRow);
                            dTable.Controls.Add(dTRow);
                            dTRow = new HtmlTableRow();

                            addControlToDiv(i.ToString(), layer, dTable);
                            dTable = new HtmlTable();
                            dTable.Attributes.Add("class", "table");//* table-striped
                            #endregion
                        }
                        #endregion

                        #region Question with Items Option 2 Col 1,2,3
                        else
                        {
                            string strlbl = string.Empty;
                            if (dt.Rows[i]["quest_Mandatory"].ToString() == "True")
                            {
                                strlbl = "<label style='color:red'>*</label>";
                            }

                            questionno = dt.Rows[i]["quest_no_disp"].ToString();
                            if (questionno != string.Empty & questionno != "-")
                            {
                                label.Text = questionno + " " + dt.Rows[i]["quest_desc"].ToString() + strlbl;
                                label.ID = dt.Rows[i]["quest_id"].ToString();

                            }
                            else
                            {
                                label.Text = dt.Rows[i]["quest_desc"].ToString() + strlbl;
                                label.ID = dt.Rows[i]["quest_id"].ToString();
                            }
                            label.CssClass = "title";
                            label.Style.Add("font-weight", "bold");
                            bool noMargin = false;
                            if (dt.Rows[i]["quest_class"].ToString() == "Header")
                            {
                                noMargin = true;
                            }
                            addControlToDiv(i.ToString(), layer, label, noMargin);
                            if (dt.Rows[i]["quest_class"].ToString() != "Header")
                            {
                                newLine(i.ToString(), layer);
                            }
                            #region Items

                            dtsub = qCtrl.getQuestItemByQuestID(dt.Rows[i]["quest_id"].ToString());//*

                            #region calculating for Col 1 , 2 and 3
                            if (dtsub.Rows.Count > 0)
                            {
                                string layout = qCtrl.getLayout(qnaire_id);//*
                                string direction = qCtrl.getDirection(qnaire_id);//*
                                if (layout == "1")
                                {
                                    #region 1 Col layout
                                    int rowcount = dtsub.Rows.Count;
                                    int stop = 0;
                                    HtmlTableRow dTRow1 = new HtmlTableRow();
                                    for (int q = 0; q < dtsub.Rows.Count; q++)
                                    {
                                        string divid = string.Empty;
                                        string depQID = dtsub.Rows[q]["qitem_depquest_id"].ToString();
                                        if (!string.IsNullOrEmpty(depQID))
                                        {
                                            if (depQID.Contains(','))
                                            {
                                                string[] ids = depQID.Split(',');
                                                if (ids.Count() > 0)
                                                {
                                                    for (int u = 0; u < ids.Count(); u++)
                                                    {
                                                        depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());//*
                                                        int seq = int.Parse(depQID) - 1;
                                                        divid += seq.ToString() + "~";
                                                    }
                                                    divid = divid.Remove(divid.Count() - 1);

                                                }

                                            }
                                            else
                                            {
                                                depQID = qCtrl.getQuestionSeqByQuestID(depQID);//*
                                                int seq = int.Parse(depQID) - 1;
                                                divid = seq.ToString();
                                            }
                                        }

                                        if (q == 0)
                                        {
                                            addHeaderTableRowToHtmlTable(dTable, 1);
                                        }

                                        dTRow1 = new HtmlTableRow();
                                        dTable.Controls.Add(dTRow1);


                                        #region Col
                                        HtmlTableCell dTCell1 = new HtmlTableCell();
                                        if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                        {
                                            if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                            {
                                                dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                            }
                                            else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                            {
                                                Label lblInnerText = new Label();
                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                lblInnerText.Attributes.Add("class", "chklabel");
                                                lblInnerText.Text = "01";
                                                dTCell1.Controls.Add(lblInnerText);
                                            }
                                            else
                                            {
                                                dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                            }
                                        }
                                        else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                        {
                                            if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                            {
                                                dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                            }
                                            else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                            {
                                                Label lblInnerText = new Label();
                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                lblInnerText.Attributes.Add("class", "chklabel");
                                                lblInnerText.Text = "01";
                                                dTCell1.Controls.Add(lblInnerText);
                                            }
                                            else
                                            {
                                                dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                            }

                                        }
                                        else if (dt.Rows[i]["quest_input_type"].ToString() == "Pulldown")
                                        {
                                            if (stop == 0)
                                            {
                                                string questid = dt.Rows[i]["quest_id"].ToString();
                                                List<gen_QuestItem> lst = qCtrl.selectbyIDlst(questid);
                                                if (lst.Count > 0)
                                                {
                                                    dTCell1.Controls.Add(getPulldown(dtsub.Rows[q]["qitem_id"].ToString(), i, lst));
                                                    stop = 1;
                                                }
                                            }
                                        }
                                        string imgname = dtsub.Rows[q]["remark"].ToString();
                                        if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                        {
                                            Label lblInnerText = new Label();
                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                            lblInnerText.Attributes.Add("class", "chklabel");
                                            lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                            dTCell1.Controls.Add(lblInnerText);
                                            if (!string.IsNullOrEmpty(imgname))
                                            {
                                                Literal breakline = new Literal();
                                                breakline.Text = "<br />";
                                                dTCell1.Controls.Add(breakline);
                                                Image img = new Image();
                                                img.Attributes.Add("class", "img-responsive");
                                                img.ImageUrl = "~/img/" + imgname;
                                                dTCell1.Controls.Add(img);
                                            }
                                        }

                                        else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                        {
                                            Label lblInnerText = new Label();
                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                            lblInnerText.Attributes.Add("class", "chklabel");
                                            lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                            dTCell1.Controls.Add(lblInnerText);
                                            Literal breakline = new Literal();
                                            if (!string.IsNullOrEmpty(imgname))
                                            {
                                                breakline = new Literal();
                                                breakline.Text = "<br />";
                                                dTCell1.Controls.Add(breakline);
                                                Image img = new Image();
                                                img.Attributes.Add("class", "img-responsive");
                                                img.ImageUrl = "~/img/" + imgname;
                                                dTCell1.Controls.Add(img);
                                            }
                                            breakline = new Literal();
                                            breakline.Text = "<br />";
                                            dTCell1.Controls.Add(breakline);
                                            dTCell1.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                        }
                                        dTRow1.Controls.Add(dTCell1);
                                        #endregion


                                    }

                                    if (dt.Rows[i]["quest_class"].ToString() != "Header")
                                    {
                                        addControlToDiv(i.ToString(), layer, dTable);
                                    }
                                    dTable.Attributes.Add("data-role", "table");
                                    dTable.Attributes.Add("data-mode", "columntoggle");
                                    dTable.Attributes.Add("id", "myTable");
                                    dTable = new HtmlTable();
                                    dTable.Attributes.Add("class", "table");//* table-striped
                                    #endregion
                                }
                                else if (layout == "2")
                                {
                                    #region 2 Cols layout
                                    if (direction == "Horizontal")
                                    {
                                        #region Col 2 Horizontally Alphabetical
                                        int rowcount = dtsub.Rows.Count;
                                        int remainder = rowcount % 2;
                                        int row_createcount = rowcount / 2;
                                        int colcount = 0;
                                        int stop = 0;
                                        HtmlTableRow dTRow1 = new HtmlTableRow();
                                        for (int q = 0; q < dtsub.Rows.Count; q++)
                                        {
                                            string divid = string.Empty;
                                            string depQID = dtsub.Rows[q]["qitem_depquest_id"].ToString();
                                            if (!string.IsNullOrEmpty(depQID))
                                            {
                                                if (depQID.Contains(','))
                                                {
                                                    string[] ids = depQID.Split(',');
                                                    if (ids.Count() > 0)
                                                    {
                                                        for (int u = 0; u < ids.Count(); u++)
                                                        {
                                                            depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                            int seq = int.Parse(depQID) - 1;
                                                            divid += seq.ToString() + "~";
                                                        }
                                                        divid = divid.Remove(divid.Count() - 1);

                                                    }

                                                }
                                                else
                                                {
                                                    depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                    int seq = int.Parse(depQID) - 1;
                                                    divid = seq.ToString();
                                                }
                                            }
                                            colcount++;
                                            int remainder_child = colcount % 2;

                                            if (remainder_child != 0)
                                            {
                                                if (q == 0)
                                                {
                                                    addHeaderTableRowToHtmlTable(dTable, 2);
                                                }

                                                dTRow1 = new HtmlTableRow();
                                                dTable.Controls.Add(dTRow1);
                                            }

                                            if (remainder_child != 0)
                                            {
                                                #region Odd
                                                HtmlTableCell dTCell1 = new HtmlTableCell();
                                                //dTCell1.VAlign = "top";
                                                if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = "01";
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = "01";
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                    }

                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Pulldown")
                                                {
                                                    if (stop == 0)
                                                    {
                                                        string questid = dt.Rows[i]["quest_id"].ToString();
                                                        List<gen_QuestItem> lst = qCtrl.selectbyIDlst(questid);
                                                        if (lst.Count > 0)
                                                        {
                                                            dTCell1.Controls.Add(getPulldown(dtsub.Rows[q]["qitem_id"].ToString(), i, lst));
                                                            stop = 1;
                                                        }
                                                    }
                                                }
                                                string imgname = dtsub.Rows[q]["remark"].ToString();
                                                if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                {
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        img.Width = 130;
                                                        img.Height = 100;
                                                        dTCell1.Controls.Add(img);
                                                    }
                                                    else
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                }

                                                else if (dt.Rows[i]["quest_input_type"].ToString() != "Pulldown" && dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                {
                                                    Literal breakline = new Literal();
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        img.Width = 130;
                                                        img.Height = 100;
                                                        dTCell1.Controls.Add(img);
                                                    }
                                                    else
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }

                                                    breakline = new Literal();
                                                    breakline.Text = "<br />";
                                                    dTCell1.Controls.Add(breakline);
                                                    dTCell1.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                    Literal breakline2 = new Literal();
                                                    breakline2.Text = "<br />";
                                                    dTCell1.Controls.Add(breakline2);
                                                    string txtID = "txt" + dtsub.Rows[q]["qitem_id"].ToString();
                                                    dTCell1.Controls.Add(getValidateExpress(txtID));
                                                }
                                                dTRow1.Controls.Add(dTCell1);

                                                #endregion
                                            }
                                            else
                                            {
                                                #region Even
                                                HtmlTableCell dTCell3 = new HtmlTableCell();

                                                if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = "01";
                                                        dTCell3.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = "01";
                                                        dTCell3.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                string imgname = dtsub.Rows[q]["remark"].ToString();
                                                if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                {
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        img.Width = 130;
                                                        img.Height = 100;
                                                        dTCell3.Controls.Add(img);
                                                    }
                                                    else
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                        dTCell3.Controls.Add(lblInnerText);
                                                    }
                                                }
                                                else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                {
                                                    Literal breakline = new Literal();
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        img.Width = 130;
                                                        img.Height = 100;
                                                        dTCell3.Controls.Add(img);
                                                    }
                                                    else
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                        dTCell3.Controls.Add(lblInnerText);
                                                    }

                                                    breakline = new Literal();
                                                    breakline.Text = "<br />";
                                                    dTCell3.Controls.Add(breakline);
                                                    dTCell3.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                    Literal breakline2 = new Literal();
                                                    breakline2.Text = "<br />";
                                                    dTCell3.Controls.Add(breakline2);
                                                    string txtID = "txt" + dtsub.Rows[q]["qitem_id"].ToString();
                                                    dTCell3.Controls.Add(getValidateExpress(txtID));
                                                }
                                                dTRow1.Controls.Add(dTCell3);
                                                #endregion
                                            }
                                        }
                                        #endregion
                                    }
                                    else if (direction == "Vertical")
                                    {
                                        #region Col 2 Vertically Alphabetical
                                        HtmlTableRow dTRow1 = new HtmlTableRow();
                                        int totalrow = dtsub.Rows.Count;
                                        int remainder = totalrow % 2;
                                        int quotient = totalrow / 2;
                                        int stop = 0;
                                        #region remainder == 0
                                        if (remainder == 0)
                                        {
                                            for (int q = 0; q < quotient; q++)
                                            {
                                                string divid = string.Empty;
                                                string depQID = string.Empty;

                                                if (q == 0)
                                                {
                                                    addHeaderTableRowToHtmlTable(dTable, 2);
                                                }

                                                dTRow1 = new HtmlTableRow();
                                                dTable.Controls.Add(dTRow1);


                                                #region Odd
                                                divid = string.Empty;
                                                depQID = string.Empty;
                                                depQID = dtsub.Rows[q]["qitem_depquest_id"].ToString();
                                                if (!string.IsNullOrEmpty(depQID))
                                                {
                                                    if (depQID.Contains(','))
                                                    {
                                                        string[] ids = depQID.Split(',');
                                                        if (ids.Count() > 0)
                                                        {
                                                            for (int u = 0; u < ids.Count(); u++)
                                                            {
                                                                depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                int seq = int.Parse(depQID) - 1;
                                                                divid += seq.ToString() + "~";
                                                            }
                                                            divid = divid.Remove(divid.Count() - 1);

                                                        }

                                                    }
                                                    else
                                                    {
                                                        depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                        int seq = int.Parse(depQID) - 1;
                                                        divid = seq.ToString();
                                                    }
                                                }
                                                HtmlTableCell dTCell1 = new HtmlTableCell();

                                                if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = "01";
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = "01";
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Pulldown")
                                                {
                                                    if (stop == 0)
                                                    {
                                                        string questid = dt.Rows[i]["quest_id"].ToString();
                                                        List<gen_QuestItem> lst = qCtrl.selectbyIDlst(questid);
                                                        if (lst.Count > 0)
                                                        {
                                                            dTCell1.Controls.Add(getPulldown(dtsub.Rows[q]["qitem_id"].ToString(), i, lst));
                                                            stop = 1;
                                                        }
                                                    }
                                                }
                                                string imgname = dtsub.Rows[q]["remark"].ToString();
                                                if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                {
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        img.Width = 130;
                                                        img.Height = 100;
                                                        dTCell1.Controls.Add(img);
                                                    }
                                                    else
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                }
                                                else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                {
                                                    Literal breakline = new Literal();

                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        img.Width = 130;
                                                        img.Height = 100;
                                                        dTCell1.Controls.Add(img);
                                                    }
                                                    else
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                        dTCell1.Controls.Add(lblInnerText);
                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell1.Controls.Add(breakline);
                                                    }

                                                    breakline = new Literal();
                                                    breakline.Text = "<br />";
                                                    dTCell1.Controls.Add(breakline);
                                                    dTCell1.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                }
                                                dTRow1.Controls.Add(dTCell1);
                                                #endregion

                                                #region Even
                                                divid = string.Empty;
                                                depQID = string.Empty;
                                                depQID = dtsub.Rows[q + quotient]["qitem_depquest_id"].ToString();
                                                if (!string.IsNullOrEmpty(depQID))
                                                {
                                                    if (depQID.Contains(','))
                                                    {
                                                        string[] ids = depQID.Split(',');
                                                        if (ids.Count() > 0)
                                                        {
                                                            for (int u = 0; u < ids.Count(); u++)
                                                            {
                                                                depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                int seq = int.Parse(depQID) - 1;
                                                                divid += seq.ToString() + "~";
                                                            }
                                                            divid = divid.Remove(divid.Count() - 1);

                                                        }

                                                    }
                                                    else
                                                    {
                                                        depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                        int seq = int.Parse(depQID) - 1;
                                                        divid = seq.ToString();
                                                    }
                                                }

                                                HtmlTableCell dTCell3 = new HtmlTableCell();
                                                if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q + quotient]["qitem_type"].ToString() == "Radio")
                                                {
                                                    if (dtsub.Rows[q + quotient]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q + quotient]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q + quotient]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = "01";
                                                        dTCell3.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q + quotient]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                {
                                                    if (dtsub.Rows[q + quotient]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + quotient]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q + quotient]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = "01";
                                                        dTCell3.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + quotient]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                imgname = string.Empty;
                                                imgname = dtsub.Rows[q + quotient]["remark"].ToString();
                                                if (dtsub.Rows[q + quotient]["qitem_desc"].ToString() != "" && dtsub.Rows[q + quotient]["qitem_type"].ToString() != "Text" && dtsub.Rows[q + quotient]["qitem_type"].ToString() != "Pulldown")
                                                {

                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        img.Width = 130;
                                                        img.Height = 100;
                                                        dTCell3.Controls.Add(img);
                                                    }
                                                    else
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = dtsub.Rows[q + quotient]["qitem_desc"].ToString();
                                                        dTCell3.Controls.Add(lblInnerText);
                                                    }
                                                }
                                                else if (dtsub.Rows[q + quotient]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                {
                                                    Literal breakline = new Literal();

                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        img.Width = 130;
                                                        img.Height = 100;
                                                        dTCell3.Controls.Add(img);
                                                    }
                                                    else
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = dtsub.Rows[q + quotient]["qitem_desc"].ToString();
                                                        dTCell3.Controls.Add(lblInnerText);
                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell3.Controls.Add(breakline);
                                                    }

                                                    breakline = new Literal();
                                                    breakline.Text = "<br />";
                                                    dTCell3.Controls.Add(breakline);
                                                    dTCell3.Controls.Add(getTextBox(dtsub.Rows[q + quotient]["qitem_id"].ToString(), i, layer));
                                                }
                                                dTRow1.Controls.Add(dTCell3);
                                                #endregion
                                            }
                                        }
                                        #endregion

                                        #region remainder != 0
                                        else
                                        {
                                            for (int q = 0; q < quotient + 1; q++)
                                            {
                                                string divid = string.Empty;
                                                string depQID = string.Empty;

                                                if (q == 0)
                                                {
                                                    addHeaderTableRowToHtmlTable(dTable, 2);
                                                }

                                                dTRow1 = new HtmlTableRow();
                                                dTable.Controls.Add(dTRow1);

                                                #region Odd
                                                divid = string.Empty;
                                                depQID = string.Empty;
                                                depQID = dtsub.Rows[q]["qitem_depquest_id"].ToString();
                                                if (!string.IsNullOrEmpty(depQID))
                                                {
                                                    if (depQID.Contains(','))
                                                    {
                                                        string[] ids = depQID.Split(',');
                                                        if (ids.Count() > 0)
                                                        {
                                                            for (int u = 0; u < ids.Count(); u++)
                                                            {
                                                                depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                int seq = int.Parse(depQID) - 1;
                                                                divid += seq.ToString() + "~";
                                                            }
                                                            divid = divid.Remove(divid.Count() - 1);

                                                        }

                                                    }
                                                    else
                                                    {
                                                        depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                        int seq = int.Parse(depQID) - 1;
                                                        divid = seq.ToString();
                                                    }
                                                }

                                                HtmlTableCell dTCell1 = new HtmlTableCell();
                                                if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = "01";
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = "01";
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Pulldown")
                                                {
                                                    if (stop == 0)
                                                    {
                                                        string questid = dt.Rows[i]["quest_id"].ToString();
                                                        List<gen_QuestItem> lst = qCtrl.selectbyIDlst(questid);
                                                        if (lst.Count > 0)
                                                        {
                                                            dTCell1.Controls.Add(getPulldown(dtsub.Rows[q]["qitem_id"].ToString(), i, lst));
                                                            stop = 1;
                                                        }
                                                    }
                                                }
                                                string imgname = dtsub.Rows[q]["remark"].ToString();
                                                if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                    lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                    dTCell1.Controls.Add(lblInnerText);
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        img.Width = 130;
                                                        img.Height = 100;
                                                        dTCell1.Controls.Add(img);
                                                    }
                                                }
                                                else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                {
                                                    Literal breakline = new Literal();

                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        dTCell1.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        img.Width = 130;
                                                        img.Height = 100;
                                                        dTCell1.Controls.Add(img);
                                                    }
                                                    else
                                                    {

                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                        lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                        dTCell1.Controls.Add(lblInnerText);
                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell1.Controls.Add(breakline);
                                                    }

                                                    breakline = new Literal();
                                                    breakline.Text = "<br />";
                                                    dTCell1.Controls.Add(breakline);
                                                    dTCell1.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                }

                                                dTRow1.Controls.Add(dTCell1);
                                                #endregion

                                                if (q < quotient)
                                                {
                                                    #region Even
                                                    divid = string.Empty;
                                                    depQID = string.Empty;
                                                    depQID = dtsub.Rows[q + quotient + 1]["qitem_depquest_id"].ToString();
                                                    if (!string.IsNullOrEmpty(depQID))
                                                    {
                                                        if (depQID.Contains(','))
                                                        {
                                                            string[] ids = depQID.Split(',');
                                                            if (ids.Count() > 0)
                                                            {
                                                                for (int u = 0; u < ids.Count(); u++)
                                                                {
                                                                    depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                    int seq = int.Parse(depQID) - 1;
                                                                    divid += seq.ToString() + "~";
                                                                }
                                                                divid = divid.Remove(divid.Count() - 1);

                                                            }

                                                        }
                                                        else
                                                        {
                                                            depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                            int seq = int.Parse(depQID) - 1;
                                                            divid = seq.ToString();
                                                        }
                                                    }

                                                    HtmlTableCell dTCell3 = new HtmlTableCell();
                                                    if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q + quotient + 1]["qitem_type"].ToString() == "Radio")
                                                    {
                                                        if (dtsub.Rows[q + quotient + 1]["qitem_type"].ToString() == "Text")
                                                        {
                                                            dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q + quotient + 1]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                        }
                                                        else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                            lblInnerText.Text = "01";
                                                            dTCell3.Controls.Add(lblInnerText);
                                                        }
                                                        else
                                                        {
                                                            dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q + quotient + 1]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                        }
                                                    }
                                                    else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                    {
                                                        if (dtsub.Rows[q + quotient + 1]["qitem_type"].ToString() == "Text")
                                                        {
                                                            dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + quotient + 1]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                        }
                                                        else if (dtsub.Rows[q + quotient + 1]["qitem_type"].ToString() == "TextOnly")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                            lblInnerText.Text = "01";
                                                            dTCell3.Controls.Add(lblInnerText);
                                                        }
                                                        else
                                                        {
                                                            dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + quotient + 1]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                        }
                                                    }
                                                    imgname = string.Empty;
                                                    imgname = dtsub.Rows[q + quotient + 1]["remark"].ToString();
                                                    if (dtsub.Rows[q + quotient + 1]["qitem_desc"].ToString() != "" && dtsub.Rows[q + quotient + 1]["qitem_type"].ToString() != "Text" && dtsub.Rows[q + quotient + 1]["qitem_type"].ToString() != "Pulldown")
                                                    {

                                                        if (!string.IsNullOrEmpty(imgname))
                                                        {
                                                            Image img = new Image();
                                                            img.Attributes.Add("class", "img-responsive");
                                                            img.ImageUrl = "~/img/" + imgname;
                                                            img.Width = 130;
                                                            img.Height = 100;
                                                            dTCell3.Controls.Add(img);
                                                        }
                                                        else
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                            lblInnerText.Text = dtsub.Rows[q + quotient + 1]["qitem_desc"].ToString();
                                                            dTCell3.Controls.Add(lblInnerText);
                                                        }

                                                    }
                                                    else if (dtsub.Rows[q + quotient + 1]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                    {
                                                        Literal breakline = new Literal();
                                                        if (!string.IsNullOrEmpty(imgname))
                                                        {
                                                            dTCell3.Controls.Add(breakline);
                                                            Image img = new Image();
                                                            img.Attributes.Add("class", "img-responsive");
                                                            img.ImageUrl = "~/img/" + imgname;
                                                            img.Width = 130;
                                                            img.Height = 100;
                                                            dTCell3.Controls.Add(img);
                                                        }
                                                        else
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol2");
                                                            lblInnerText.Text = dtsub.Rows[q + quotient + 1]["qitem_desc"].ToString();
                                                            dTCell3.Controls.Add(lblInnerText);
                                                            breakline = new Literal();
                                                            breakline.Text = "<br />";
                                                            dTCell3.Controls.Add(breakline);
                                                        }

                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell3.Controls.Add(breakline);
                                                        dTCell3.Controls.Add(getTextBox(dtsub.Rows[q + quotient + 1]["qitem_id"].ToString(), i, layer));
                                                    }

                                                    dTRow1.Controls.Add(dTCell3);
                                                    #endregion
                                                }
                                            }
                                        }
                                        #endregion

                                        #endregion
                                    }

                                    if (dt.Rows[i]["quest_class"].ToString() != "Header")
                                    {
                                        addControlToDiv(i.ToString(), layer, dTable);
                                    }
                                    dTable = new HtmlTable();
                                    dTable.Attributes.Add("class", "table");//* table-striped
                                    #endregion
                                }
                                else if (layout == "3")
                                {
                                    #region 3 Cols layout
                                    if (direction == "Horizontal")
                                    {
                                        #region Col 3 Horizontelly Alphabetical
                                        int rowcount = dtsub.Rows.Count;
                                        int remainder = rowcount % 3;
                                        int row_createcount = rowcount / 3;
                                        int colcount = 0;
                                        int stop = 0;
                                        HtmlTableRow dTRow1 = new HtmlTableRow();
                                        for (int q = 0; q < dtsub.Rows.Count; q++)
                                        {
                                            string divid = string.Empty;
                                            string depQID = dtsub.Rows[q]["qitem_depquest_id"].ToString();
                                            if (!string.IsNullOrEmpty(depQID))
                                            {
                                                if (depQID.Contains(','))
                                                {
                                                    string[] ids = depQID.Split(',');
                                                    if (ids.Count() > 0)
                                                    {
                                                        for (int u = 0; u < ids.Count(); u++)
                                                        {
                                                            depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                            int seq = int.Parse(depQID) - 1;
                                                            divid += seq.ToString() + "~";
                                                        }
                                                        divid = divid.Remove(divid.Count() - 1);

                                                    }

                                                }
                                                else
                                                {
                                                    depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                    int seq = int.Parse(depQID) - 1;
                                                    divid = seq.ToString();
                                                }
                                            }
                                            colcount++;
                                            int remainder_child = colcount % 3;

                                            if (remainder_child == 1)
                                            {
                                                if (q == 0)
                                                {
                                                    addHeaderTableRowToHtmlTable(dTable, 3);
                                                }

                                                dTRow1 = new HtmlTableRow();
                                                dTable.Controls.Add(dTRow1);
                                            }
                                            if (remainder_child == 1)
                                            {
                                                #region row 1
                                                HtmlTableCell dTCell1 = new HtmlTableCell();
                                                if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Pulldown")
                                                {
                                                    if (stop == 0)
                                                    {
                                                        string questid = dt.Rows[i]["quest_id"].ToString();
                                                        List<gen_QuestItem> lst = qCtrl.selectbyIDlst(questid);
                                                        if (lst.Count > 0)
                                                        {
                                                            dTCell1.Controls.Add(getPulldown(dtsub.Rows[q]["qitem_id"].ToString(), i, lst));
                                                            stop = 1;
                                                        }
                                                    }
                                                }
                                                string imgname = dtsub.Rows[q]["remark"].ToString();
                                                if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                    dTCell1.Controls.Add(lblInnerText);
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Literal breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell1.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell1.Controls.Add(img);
                                                    }
                                                }

                                                else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                    dTCell1.Controls.Add(lblInnerText);
                                                    Literal breakline = new Literal();
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell1.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell1.Controls.Add(img);
                                                    }
                                                    breakline = new Literal();
                                                    breakline.Text = "<br />";
                                                    dTCell1.Controls.Add(breakline);
                                                    dTCell1.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                }
                                                dTRow1.Controls.Add(dTCell1);
                                                #endregion
                                            }
                                            else if (remainder_child == 2)
                                            {
                                                #region row 2
                                                HtmlTableCell dTCell3 = new HtmlTableCell();
                                                if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell3.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell3.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                string imgname = dtsub.Rows[q]["remark"].ToString();
                                                if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                    dTCell3.Controls.Add(lblInnerText);
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Literal breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell3.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell3.Controls.Add(img);
                                                    }
                                                }
                                                else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                    dTCell3.Controls.Add(lblInnerText);
                                                    Literal breakline = new Literal();
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell3.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell3.Controls.Add(img);
                                                    }
                                                    breakline = new Literal();
                                                    breakline.Text = "<br />";
                                                    dTCell3.Controls.Add(breakline);
                                                    dTCell3.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                }
                                                dTRow1.Controls.Add(dTCell3);
                                                #endregion
                                            }
                                            else if (remainder_child == 0)
                                            {
                                                #region row 3
                                                HtmlTableCell dTCell5 = new HtmlTableCell();
                                                if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell5.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell5.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell5.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell5.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell5.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell5.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                string imgname = dtsub.Rows[q]["remark"].ToString();
                                                if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                    dTCell5.Controls.Add(lblInnerText);
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Literal breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell5.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell5.Controls.Add(img);
                                                    }
                                                }
                                                else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                    dTCell5.Controls.Add(lblInnerText);
                                                    Literal breakline = new Literal();
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell5.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell5.Controls.Add(img);
                                                    }
                                                    breakline = new Literal();
                                                    breakline.Text = "<br />";
                                                    dTCell5.Controls.Add(breakline);
                                                    dTCell5.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                }
                                                dTRow1.Controls.Add(dTCell5);
                                                #endregion
                                            }
                                        }
                                        #endregion
                                    }
                                    else if (direction == "Vertical")
                                    {
                                        #region Col 3 Vertically Alphabetical
                                        HtmlTableRow dTRow1 = new HtmlTableRow();
                                        int totalrow = dtsub.Rows.Count;
                                        int remainder = totalrow % 3;
                                        int quotient = totalrow / 3;
                                        int stop = 0;
                                        #region remainder == 0
                                        if (remainder == 0)
                                        {
                                            for (int q = 0; q < quotient; q++)
                                            {
                                                string divid = string.Empty;
                                                string depQID = string.Empty;

                                                if (q == 0)
                                                {
                                                    addHeaderTableRowToHtmlTable(dTable, 3);
                                                }

                                                dTRow1 = new HtmlTableRow();
                                                dTable.Controls.Add(dTRow1);

                                                #region Col1
                                                divid = string.Empty;
                                                depQID = string.Empty;
                                                depQID = dtsub.Rows[q]["qitem_depquest_id"].ToString();
                                                if (!string.IsNullOrEmpty(depQID))
                                                {
                                                    if (depQID.Contains(','))
                                                    {
                                                        string[] ids = depQID.Split(',');
                                                        if (ids.Count() > 0)
                                                        {
                                                            for (int u = 0; u < ids.Count(); u++)
                                                            {
                                                                depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                int seq = int.Parse(depQID) - 1;
                                                                divid += seq.ToString() + "~";
                                                            }
                                                            divid = divid.Remove(divid.Count() - 1);

                                                        }

                                                    }
                                                    else
                                                    {
                                                        depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                        int seq = int.Parse(depQID) - 1;
                                                        divid = seq.ToString();
                                                    }
                                                }

                                                HtmlTableCell dTCell1 = new HtmlTableCell();
                                                if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                {
                                                    if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell1.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Pulldown")
                                                {
                                                    if (stop == 0)
                                                    {
                                                        string questid = dt.Rows[i]["quest_id"].ToString();
                                                        List<gen_QuestItem> lst = qCtrl.selectbyIDlst(questid);
                                                        if (lst.Count > 0)
                                                        {
                                                            dTCell1.Controls.Add(getPulldown(dtsub.Rows[q]["qitem_id"].ToString(), i, lst));
                                                            stop = 1;
                                                        }
                                                    }
                                                }
                                                string imgname = dtsub.Rows[q]["remark"].ToString();
                                                if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                    dTCell1.Controls.Add(lblInnerText);
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Literal breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell1.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell1.Controls.Add(img);
                                                    }
                                                }
                                                else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                    dTCell1.Controls.Add(lblInnerText);
                                                    Literal breakline = new Literal();
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell1.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell1.Controls.Add(img);
                                                    }
                                                    breakline = new Literal();
                                                    breakline.Text = "<br />";
                                                    dTCell1.Controls.Add(breakline);
                                                    dTCell1.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                }
                                                dTRow1.Controls.Add(dTCell1);
                                                #endregion

                                                #region Col2
                                                divid = string.Empty;
                                                depQID = string.Empty;
                                                depQID = dtsub.Rows[q + quotient]["qitem_depquest_id"].ToString();
                                                if (!string.IsNullOrEmpty(depQID))
                                                {
                                                    if (depQID.Contains(','))
                                                    {
                                                        string[] ids = depQID.Split(',');
                                                        if (ids.Count() > 0)
                                                        {
                                                            for (int u = 0; u < ids.Count(); u++)
                                                            {
                                                                depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                int seq = int.Parse(depQID) - 1;
                                                                divid += seq.ToString() + "~";
                                                            }
                                                            divid = divid.Remove(divid.Count() - 1);

                                                        }

                                                    }
                                                    else
                                                    {
                                                        depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                        int seq = int.Parse(depQID) - 1;
                                                        divid = seq.ToString();
                                                    }
                                                }
                                                HtmlTableCell dTCell3 = new HtmlTableCell();
                                                if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q + quotient]["qitem_type"].ToString() == "Radio")
                                                {
                                                    if (dtsub.Rows[q + quotient]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q + quotient]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q + quotient]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell3.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q + quotient]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                {
                                                    if (dtsub.Rows[q + quotient]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + quotient]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q + quotient]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell3.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + quotient]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                imgname = string.Empty;
                                                imgname = dtsub.Rows[q + quotient]["remark"].ToString();
                                                if (dtsub.Rows[q + quotient]["qitem_desc"].ToString() != "" && dtsub.Rows[q + quotient]["qitem_type"].ToString() != "Text" && dtsub.Rows[q + quotient]["qitem_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q + quotient]["qitem_desc"].ToString();
                                                    dTCell3.Controls.Add(lblInnerText);
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Literal breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell3.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell3.Controls.Add(img);
                                                    }
                                                }
                                                else if (dtsub.Rows[q + quotient]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q + quotient]["qitem_desc"].ToString();
                                                    dTCell3.Controls.Add(lblInnerText);
                                                    Literal breakline = new Literal();
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell3.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell3.Controls.Add(img);
                                                    }
                                                    breakline = new Literal();
                                                    breakline.Text = "<br />";
                                                    dTCell3.Controls.Add(breakline);
                                                    dTCell3.Controls.Add(getTextBox(dtsub.Rows[q + quotient]["qitem_id"].ToString(), i, layer));
                                                }
                                                dTRow1.Controls.Add(dTCell3);
                                                #endregion

                                                #region Col3
                                                divid = string.Empty;
                                                depQID = string.Empty;
                                                depQID = dtsub.Rows[q + (2 * quotient)]["qitem_depquest_id"].ToString();
                                                if (!string.IsNullOrEmpty(depQID))
                                                {
                                                    if (depQID.Contains(','))
                                                    {
                                                        string[] ids = depQID.Split(',');
                                                        if (ids.Count() > 0)
                                                        {
                                                            for (int u = 0; u < ids.Count(); u++)
                                                            {
                                                                depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                int seq = int.Parse(depQID) - 1;
                                                                divid += seq.ToString() + "~";
                                                            }
                                                            divid = divid.Remove(divid.Count() - 1);

                                                        }

                                                    }
                                                    else
                                                    {
                                                        depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                        int seq = int.Parse(depQID) - 1;
                                                        divid = seq.ToString();
                                                    }
                                                }
                                                HtmlTableCell dTCell5 = new HtmlTableCell();
                                                if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q + (2 * quotient)]["qitem_type"].ToString() == "Radio")
                                                {
                                                    if (dtsub.Rows[q + (2 * quotient)]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell5.Controls.Add(getCheckBox("radio", dtsub.Rows[q + (2 * quotient)]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q + (2 * quotient)]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell5.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell5.Controls.Add(getCheckBox("radio", dtsub.Rows[q + (2 * quotient)]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                {
                                                    if (dtsub.Rows[q + (2 * quotient)]["qitem_type"].ToString() == "Text")
                                                    {
                                                        dTCell5.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + (2 * quotient)]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                    }
                                                    else if (dtsub.Rows[q + (2 * quotient)]["qitem_type"].ToString() == "TextOnly")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = "01";
                                                        dTCell5.Controls.Add(lblInnerText);
                                                    }
                                                    else
                                                    {
                                                        dTCell5.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + (2 * quotient)]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                    }
                                                }
                                                imgname = string.Empty;
                                                imgname = dtsub.Rows[q + (2 * quotient)]["remark"].ToString();
                                                if (dtsub.Rows[q + (2 * quotient)]["qitem_desc"].ToString() != "" && dtsub.Rows[q + (2 * quotient)]["qitem_type"].ToString() != "Text" && dtsub.Rows[q + (2 * quotient)]["qitem_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q + (2 * quotient)]["qitem_desc"].ToString();
                                                    dTCell5.Controls.Add(lblInnerText);
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        Literal breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell5.Controls.Add(breakline);
                                                        Image img = new Image();
                                                        img.Attributes.Add("class", "img-responsive");
                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell5.Controls.Add(img);
                                                    }

                                                }
                                                else if (dtsub.Rows[q + (2 * quotient)]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                {
                                                    Label lblInnerText = new Label();
                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                    lblInnerText.Text = dtsub.Rows[q + (2 * quotient)]["qitem_desc"].ToString();
                                                    dTCell5.Controls.Add(lblInnerText);
                                                    Literal breakline = new Literal();
                                                    if (!string.IsNullOrEmpty(imgname))
                                                    {
                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell5.Controls.Add(breakline);
                                                        Image img = new Image(); img.Attributes.Add("class", "img-responsive");

                                                        img.ImageUrl = "~/img/" + imgname;
                                                        dTCell5.Controls.Add(img);
                                                    }
                                                    breakline = new Literal();
                                                    breakline.Text = "<br />";
                                                    dTCell5.Controls.Add(breakline);
                                                    dTCell5.Controls.Add(getTextBox(dtsub.Rows[q + (2 * quotient)]["qitem_id"].ToString(), i, layer));
                                                }
                                                dTRow1.Controls.Add(dTCell5);
                                                #endregion
                                            }
                                        }
                                        #endregion

                                        #region remainder != 0
                                        else if (remainder != 0)
                                        {
                                            if (totalrow < 5)
                                            {
                                                #region Totalrow <5
                                                int colcount = 0;
                                                for (int q = 0; q < dtsub.Rows.Count; q++)
                                                {
                                                    string divid = string.Empty;
                                                    string depQID = dtsub.Rows[q]["qitem_depquest_id"].ToString();
                                                    if (!string.IsNullOrEmpty(depQID))
                                                    {
                                                        if (depQID.Contains(','))
                                                        {
                                                            string[] ids = depQID.Split(',');
                                                            if (ids.Count() > 0)
                                                            {
                                                                for (int u = 0; u < ids.Count(); u++)
                                                                {
                                                                    depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                    int seq = int.Parse(depQID) - 1;
                                                                    divid += seq.ToString() + "~";
                                                                }
                                                                divid = divid.Remove(divid.Count() - 1);

                                                            }

                                                        }
                                                        else
                                                        {
                                                            depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                            int seq = int.Parse(depQID) - 1;
                                                            divid = seq.ToString();
                                                        }
                                                    }
                                                    colcount++;
                                                    int remainder_child = colcount % 3;

                                                    if (remainder_child == 1)
                                                    {
                                                        if (q == 0)
                                                        {
                                                            addHeaderTableRowToHtmlTable(dTable, 3);
                                                        }

                                                        dTRow1 = new HtmlTableRow();
                                                        dTable.Controls.Add(dTRow1);
                                                    }
                                                    if (remainder_child == 1)
                                                    {
                                                        #region row 1
                                                        HtmlTableCell dTCell1 = new HtmlTableCell();
                                                        if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                        {
                                                            if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                            {
                                                                dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                            }
                                                            else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                            {
                                                                Label lblInnerText = new Label();
                                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                lblInnerText.Text = "01";
                                                                dTCell1.Controls.Add(lblInnerText);
                                                            }
                                                            else
                                                            {
                                                                dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                            }
                                                        }
                                                        else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                        {
                                                            if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                            {
                                                                dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                            }
                                                            else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                            {
                                                                Label lblInnerText = new Label();
                                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                lblInnerText.Text = "01";
                                                                dTCell1.Controls.Add(lblInnerText);
                                                            }
                                                            else
                                                            {
                                                                dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                            }
                                                        }
                                                        else if (dt.Rows[i]["quest_input_type"].ToString() == "Pulldown")
                                                        {
                                                            if (stop == 0)
                                                            {
                                                                string questid = dt.Rows[i]["quest_id"].ToString();
                                                                List<gen_QuestItem> lst = qCtrl.selectbyIDlst(questid);
                                                                if (lst.Count > 0)
                                                                {
                                                                    dTCell1.Controls.Add(getPulldown(dtsub.Rows[q]["qitem_id"].ToString(), i, lst));
                                                                    stop = 1;
                                                                }
                                                            }
                                                        }
                                                        string imgname = dtsub.Rows[q]["remark"].ToString();
                                                        if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                            lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                            dTCell1.Controls.Add(lblInnerText);
                                                            if (!string.IsNullOrEmpty(imgname))
                                                            {
                                                                Literal breakline = new Literal();
                                                                breakline.Text = "<br />";
                                                                dTCell1.Controls.Add(breakline);
                                                                Image img = new Image();
                                                                img.Attributes.Add("class", "img-responsive");
                                                                img.ImageUrl = "~/img/" + imgname;
                                                                dTCell1.Controls.Add(img);
                                                            }

                                                        }

                                                        else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                            lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                            dTCell1.Controls.Add(lblInnerText);
                                                            Literal breakline = new Literal();
                                                            if (!string.IsNullOrEmpty(imgname))
                                                            {
                                                                breakline = new Literal();
                                                                breakline.Text = "<br />";
                                                                dTCell1.Controls.Add(breakline);
                                                                Image img = new Image();
                                                                img.Attributes.Add("class", "img-responsive");
                                                                img.ImageUrl = "~/img/" + imgname;
                                                                dTCell1.Controls.Add(img);
                                                            }
                                                            breakline = new Literal();
                                                            breakline.Text = "<br />";
                                                            dTCell1.Controls.Add(breakline);
                                                            dTCell1.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                        }
                                                        dTRow1.Controls.Add(dTCell1);
                                                        #endregion
                                                    }
                                                    else if (remainder_child == 2)
                                                    {
                                                        #region row 2
                                                        HtmlTableCell dTCell3 = new HtmlTableCell();
                                                        if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                        {
                                                            if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                            {
                                                                dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                            }
                                                            else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                            {
                                                                Label lblInnerText = new Label();
                                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                lblInnerText.Text = "01";
                                                                dTCell3.Controls.Add(lblInnerText);
                                                            }
                                                            else
                                                            {
                                                                dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                            }
                                                        }
                                                        else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                        {
                                                            if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                            {
                                                                dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                            }
                                                            else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                            {
                                                                Label lblInnerText = new Label();
                                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                lblInnerText.Text = "01";
                                                                dTCell3.Controls.Add(lblInnerText);
                                                            }
                                                            else
                                                            {
                                                                dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                            }
                                                        }
                                                        string imgname = dtsub.Rows[q]["remark"].ToString();
                                                        if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                            lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                            dTCell3.Controls.Add(lblInnerText);
                                                            if (!string.IsNullOrEmpty(imgname))
                                                            {
                                                                Literal breakline = new Literal();
                                                                breakline.Text = "<br />";
                                                                dTCell3.Controls.Add(breakline);
                                                                Image img = new Image();
                                                                img.Attributes.Add("class", "img-responsive");
                                                                img.ImageUrl = "~/img/" + imgname;
                                                                dTCell3.Controls.Add(img);
                                                            }
                                                        }
                                                        else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                            lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                            dTCell3.Controls.Add(lblInnerText);
                                                            Literal breakline = new Literal();
                                                            if (!string.IsNullOrEmpty(imgname))
                                                            {
                                                                breakline = new Literal();
                                                                breakline.Text = "<br />";
                                                                dTCell3.Controls.Add(breakline);
                                                                Image img = new Image();
                                                                img.Attributes.Add("class", "img-responsive");
                                                                img.ImageUrl = "~/img/" + imgname;
                                                                dTCell3.Controls.Add(img);
                                                            }
                                                            breakline = new Literal();
                                                            breakline.Text = "<br />";
                                                            dTCell3.Controls.Add(breakline);
                                                            dTCell3.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                        }
                                                        dTRow1.Controls.Add(dTCell3);
                                                        #endregion
                                                    }
                                                    else if (remainder_child == 0)
                                                    {
                                                        #region row 3
                                                        HtmlTableCell dTCell5 = new HtmlTableCell();
                                                        if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                        {
                                                            if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                            {
                                                                dTCell5.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                            }
                                                            else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                            {
                                                                Label lblInnerText = new Label();
                                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                lblInnerText.Text = "01";
                                                                dTCell5.Controls.Add(lblInnerText);
                                                            }
                                                            else
                                                            {
                                                                dTCell5.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                            }
                                                        }
                                                        else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                        {
                                                            if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                            {
                                                                dTCell5.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                            }
                                                            else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                            {
                                                                Label lblInnerText = new Label();
                                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                lblInnerText.Text = "01";
                                                                dTCell5.Controls.Add(lblInnerText);
                                                            }
                                                            else
                                                            {
                                                                dTCell5.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                            }
                                                        }
                                                        string imgname = dtsub.Rows[q]["remark"].ToString();

                                                        if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                            lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                            dTCell5.Controls.Add(lblInnerText);
                                                            if (!string.IsNullOrEmpty(imgname))
                                                            {
                                                                Literal breakline = new Literal();
                                                                breakline.Text = "<br />";
                                                                dTCell5.Controls.Add(breakline);
                                                                Image img = new Image();
                                                                img.Attributes.Add("class", "img-responsive");
                                                                img.ImageUrl = "~/img/" + imgname;
                                                                dTCell5.Controls.Add(img);
                                                            }
                                                        }
                                                        else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                            lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                            dTCell5.Controls.Add(lblInnerText);
                                                            Literal breakline = new Literal();
                                                            if (!string.IsNullOrEmpty(imgname))
                                                            {
                                                                breakline = new Literal();
                                                                breakline.Text = "<br />";
                                                                dTCell5.Controls.Add(breakline);
                                                                Image img = new Image();
                                                                img.Attributes.Add("class", "img-responsive");
                                                                img.ImageUrl = "~/img/" + imgname;
                                                                dTCell5.Controls.Add(img);
                                                            }
                                                            breakline = new Literal();
                                                            breakline.Text = "<br />";
                                                            dTCell5.Controls.Add(breakline);
                                                            dTCell5.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                        }
                                                        dTRow1.Controls.Add(dTCell5);
                                                        #endregion
                                                    }
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                for (int q = 0; q < quotient + 1; q++)
                                                {
                                                    int x = quotient + 1;
                                                    int z = (quotient + 1) * 2;
                                                    string divid = string.Empty;
                                                    string depQID = string.Empty;

                                                    if (q == 0)
                                                    {
                                                        addHeaderTableRowToHtmlTable(dTable, 3);
                                                    }

                                                    dTRow1 = new HtmlTableRow();
                                                    dTable.Controls.Add(dTRow1);

                                                    #region Col1
                                                    divid = string.Empty;
                                                    depQID = string.Empty;
                                                    depQID = dtsub.Rows[q]["qitem_depquest_id"].ToString();
                                                    if (!string.IsNullOrEmpty(depQID))
                                                    {
                                                        if (depQID.Contains(','))
                                                        {
                                                            string[] ids = depQID.Split(',');
                                                            if (ids.Count() > 0)
                                                            {
                                                                for (int u = 0; u < ids.Count(); u++)
                                                                {
                                                                    depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                    int seq = int.Parse(depQID) - 1;
                                                                    divid += seq.ToString() + "~";
                                                                }
                                                                divid = divid.Remove(divid.Count() - 1);

                                                            }
                                                        }
                                                        else
                                                        {
                                                            depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                            int seq = int.Parse(depQID) - 1;
                                                            divid = seq.ToString();
                                                        }
                                                    }
                                                    HtmlTableCell dTCell1 = new HtmlTableCell();
                                                    if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q]["qitem_type"].ToString() == "Radio")
                                                    {
                                                        if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                        {
                                                            dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                        }
                                                        else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                            lblInnerText.Text = "01";
                                                            dTCell1.Controls.Add(lblInnerText);
                                                        }
                                                        else
                                                        {
                                                            dTCell1.Controls.Add(getCheckBox("radio", dtsub.Rows[q]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                        }
                                                    }
                                                    else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                    {
                                                        if (dtsub.Rows[q]["qitem_type"].ToString() == "Text")
                                                        {
                                                            dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                        }
                                                        else if (dtsub.Rows[q]["qitem_type"].ToString() == "TextOnly")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                            lblInnerText.Text = "01";
                                                            dTCell1.Controls.Add(lblInnerText);
                                                        }
                                                        else
                                                        {
                                                            dTCell1.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                        }
                                                    }
                                                    else if (dt.Rows[i]["quest_input_type"].ToString() == "Pulldown")
                                                    {
                                                        if (stop == 0)
                                                        {
                                                            string questid = dt.Rows[i]["quest_id"].ToString();
                                                            List<gen_QuestItem> lst = qCtrl.selectbyIDlst(questid);
                                                            if (lst.Count > 0)
                                                            {
                                                                dTCell1.Controls.Add(getPulldown(dtsub.Rows[q]["qitem_id"].ToString(), i, lst));
                                                                stop = 1;
                                                            }
                                                        }
                                                    }
                                                    string imgname = dtsub.Rows[q]["remark"].ToString();
                                                    if (dtsub.Rows[q]["qitem_desc"].ToString() != "" && dtsub.Rows[q]["qitem_type"].ToString() != "Text" && dtsub.Rows[q]["qitem_type"].ToString() != "Pulldown")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                        dTCell1.Controls.Add(lblInnerText);
                                                        if (!string.IsNullOrEmpty(imgname))
                                                        {
                                                            Literal breakline = new Literal();
                                                            breakline.Text = "<br />";
                                                            dTCell1.Controls.Add(breakline);
                                                            Image img = new Image();
                                                            img.Attributes.Add("class", "img-responsive");
                                                            img.ImageUrl = "~/img/" + imgname;
                                                            dTCell1.Controls.Add(img);
                                                        }
                                                    }
                                                    else if (dtsub.Rows[q]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = dtsub.Rows[q]["qitem_desc"].ToString();
                                                        dTCell1.Controls.Add(lblInnerText);
                                                        Literal breakline = new Literal();
                                                        if (!string.IsNullOrEmpty(imgname))
                                                        {
                                                            breakline = new Literal();
                                                            breakline.Text = "<br />";
                                                            dTCell1.Controls.Add(breakline);
                                                            Image img = new Image();
                                                            img.Attributes.Add("class", "img-responsive");
                                                            img.ImageUrl = "~/img/" + imgname;
                                                            dTCell1.Controls.Add(img);
                                                        }
                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell1.Controls.Add(breakline);
                                                        dTCell1.Controls.Add(getTextBox(dtsub.Rows[q]["qitem_id"].ToString(), i, layer));
                                                    }
                                                    dTRow1.Controls.Add(dTCell1);
                                                    #endregion

                                                    #region Col2
                                                    divid = string.Empty;
                                                    depQID = string.Empty;
                                                    depQID = dtsub.Rows[q + quotient]["qitem_depquest_id"].ToString();
                                                    if (!string.IsNullOrEmpty(depQID))
                                                    {
                                                        if (depQID.Contains(','))
                                                        {
                                                            string[] ids = depQID.Split(',');
                                                            if (ids.Count() > 0)
                                                            {
                                                                for (int u = 0; u < ids.Count(); u++)
                                                                {
                                                                    depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                    int seq = int.Parse(depQID) - 1;
                                                                    divid += seq.ToString() + "~";
                                                                }
                                                                divid = divid.Remove(divid.Count() - 1);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                            int seq = int.Parse(depQID) - 1;
                                                            divid = seq.ToString();
                                                        }
                                                    }
                                                    HtmlTableCell dTCell3 = new HtmlTableCell();
                                                    if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q + 1 + quotient]["qitem_type"].ToString() == "Radio")
                                                    {
                                                        if (dtsub.Rows[q + 1 + quotient]["qitem_type"].ToString() == "Text")
                                                        {
                                                            dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q + 1 + quotient]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                        }
                                                        else if (dtsub.Rows[q + 1 + quotient]["qitem_type"].ToString() == "TextOnly")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                            lblInnerText.Text = "01";
                                                            dTCell3.Controls.Add(lblInnerText);
                                                        }
                                                        else
                                                        {
                                                            dTCell3.Controls.Add(getCheckBox("radio", dtsub.Rows[q + 1 + quotient]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                        }
                                                    }
                                                    else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                    {
                                                        if (dtsub.Rows[q + 1 + quotient]["qitem_type"].ToString() == "Text")
                                                        {
                                                            dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + 1 + quotient]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                        }
                                                        else if (dtsub.Rows[q + 1 + quotient]["qitem_type"].ToString() == "TextOnly")
                                                        {
                                                            Label lblInnerText = new Label();
                                                            lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                            lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                            lblInnerText.Text = "01";
                                                            dTCell3.Controls.Add(lblInnerText);
                                                        }
                                                        else
                                                        {
                                                            dTCell3.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + 1 + quotient]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                        }
                                                    }
                                                    imgname = string.Empty; imgname = dtsub.Rows[q + 1 + quotient]["remark"].ToString();
                                                    if (dtsub.Rows[q + 1 + quotient]["qitem_desc"].ToString() != "" && dtsub.Rows[q + 1 + quotient]["qitem_type"].ToString() != "Text" && dtsub.Rows[q + 1 + quotient]["qitem_type"].ToString() != "Pulldown")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = dtsub.Rows[q + 1 + quotient]["qitem_desc"].ToString();
                                                        dTCell3.Controls.Add(lblInnerText);
                                                        if (!string.IsNullOrEmpty(imgname))
                                                        {
                                                            Literal breakline = new Literal();
                                                            breakline.Text = "<br />";
                                                            dTCell3.Controls.Add(breakline);
                                                            Image img = new Image();
                                                            img.Attributes.Add("class", "img-responsive");
                                                            img.ImageUrl = "~/img/" + imgname;
                                                            dTCell3.Controls.Add(img);
                                                        }
                                                    }
                                                    else if (dtsub.Rows[q + 1 + quotient]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                    {
                                                        Label lblInnerText = new Label();
                                                        lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                        lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                        lblInnerText.Text = dtsub.Rows[q + 1 + quotient]["qitem_desc"].ToString();
                                                        dTCell3.Controls.Add(lblInnerText);
                                                        Literal breakline = new Literal();
                                                        if (!string.IsNullOrEmpty(imgname))
                                                        {
                                                            breakline = new Literal();
                                                            breakline.Text = "<br />";
                                                            dTCell3.Controls.Add(breakline);
                                                            Image img = new Image();
                                                            img.Attributes.Add("class", "img-responsive");
                                                            img.ImageUrl = "~/img/" + imgname;
                                                            dTCell3.Controls.Add(img);
                                                        }
                                                        breakline = new Literal();
                                                        breakline.Text = "<br />";
                                                        dTCell3.Controls.Add(breakline);
                                                        dTCell3.Controls.Add(getTextBox(dtsub.Rows[q + 1 + quotient]["qitem_id"].ToString(), i, layer));
                                                    }
                                                    dTRow1.Controls.Add(dTCell3);
                                                    #endregion

                                                    if (remainder == 1)
                                                    {
                                                        if (q < quotient - 1)
                                                        {
                                                            #region Col3
                                                            divid = string.Empty;
                                                            depQID = string.Empty;
                                                            depQID = dtsub.Rows[q + (quotient + 1) * 2]["qitem_depquest_id"].ToString();
                                                            if (!string.IsNullOrEmpty(depQID))
                                                            {
                                                                if (depQID.Contains(','))
                                                                {
                                                                    string[] ids = depQID.Split(',');
                                                                    if (ids.Count() > 0)
                                                                    {
                                                                        for (int u = 0; u < ids.Count(); u++)
                                                                        {
                                                                            depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                            int seq = int.Parse(depQID) - 1;
                                                                            divid += seq.ToString() + "~";
                                                                        }
                                                                        divid = divid.Remove(divid.Count() - 1);

                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                                    int seq = int.Parse(depQID) - 1;
                                                                    divid = seq.ToString();
                                                                }
                                                            }
                                                            HtmlTableCell dTCell5 = new HtmlTableCell();
                                                            if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "Radio")
                                                            {
                                                                if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "Text")
                                                                {
                                                                    dTCell5.Controls.Add(getCheckBox("radio", dtsub.Rows[q + (quotient + 1) * 2]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                                }
                                                                else if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "TextOnly")
                                                                {
                                                                    Label lblInnerText = new Label();
                                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                    lblInnerText.Text = "01";
                                                                    dTCell5.Controls.Add(lblInnerText);
                                                                }
                                                                else
                                                                {
                                                                    dTCell5.Controls.Add(getCheckBox("radio", dtsub.Rows[q + (quotient + 1) * 2]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                                }
                                                            }
                                                            else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                            {
                                                                if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "Text")
                                                                {
                                                                    dTCell5.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + (quotient + 1) * 2]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                                }
                                                                else if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "TextOnly")
                                                                {
                                                                    Label lblInnerText = new Label();
                                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                    lblInnerText.Text = "01";
                                                                    dTCell5.Controls.Add(lblInnerText);
                                                                }
                                                                else
                                                                {
                                                                    dTCell5.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + (quotient + 1) * 2]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                                }
                                                            }
                                                            imgname = string.Empty; imgname = dtsub.Rows[q + (quotient + 1) * 2]["remark"].ToString();
                                                            if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_desc"].ToString() != "" && dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() != "Text" && dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() != "Pulldown")
                                                            {
                                                                Label lblInnerText = new Label();
                                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                lblInnerText.Text = dtsub.Rows[q + (quotient + 1) * 2]["qitem_desc"].ToString();
                                                                dTCell5.Controls.Add(lblInnerText);
                                                                if (!string.IsNullOrEmpty(imgname))
                                                                {
                                                                    Literal breakline = new Literal();
                                                                    breakline.Text = "<br />";
                                                                    dTCell5.Controls.Add(breakline);
                                                                    Image img = new Image();
                                                                    img.Attributes.Add("class", "img-responsive");
                                                                    img.ImageUrl = "~/img/" + imgname;
                                                                    dTCell5.Controls.Add(img);
                                                                }
                                                            }
                                                            else if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "Text" && dt.Rows[i]["quest_input_type"].ToString() != "Pulldown")
                                                            {
                                                                Label lblInnerText = new Label();
                                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                lblInnerText.Text = dtsub.Rows[q + (quotient + 1) * 2]["qitem_desc"].ToString();
                                                                dTCell5.Controls.Add(lblInnerText);
                                                                Literal breakline = new Literal();
                                                                if (!string.IsNullOrEmpty(imgname))
                                                                {
                                                                    breakline = new Literal();
                                                                    breakline.Text = "<br />";
                                                                    dTCell5.Controls.Add(breakline);
                                                                    Image img = new Image();
                                                                    img.Attributes.Add("class", "img-responsive");
                                                                    img.ImageUrl = "~/img/" + imgname;
                                                                    dTCell5.Controls.Add(img);
                                                                }
                                                                breakline = new Literal();
                                                                breakline.Text = "<br />";
                                                                dTCell5.Controls.Add(breakline);
                                                                dTCell5.Controls.Add(getTextBox(dtsub.Rows[q + (quotient + 1) * 2]["qitem_id"].ToString(), i, layer));
                                                            }
                                                            dTRow1.Controls.Add(dTCell5);
                                                            #endregion
                                                        }
                                                    }
                                                    else if (remainder == 2)
                                                    {
                                                        if (q < quotient)
                                                        {
                                                            #region Col3
                                                            divid = string.Empty;
                                                            depQID = dtsub.Rows[q + (quotient + 1) * 2]["qitem_depquest_id"].ToString();
                                                            if (!string.IsNullOrEmpty(depQID))
                                                            {
                                                                if (depQID.Contains(','))
                                                                {
                                                                    string[] ids = depQID.Split(',');
                                                                    if (ids.Count() > 0)
                                                                    {
                                                                        for (int u = 0; u < ids.Count(); u++)
                                                                        {
                                                                            depQID = qCtrl.getQuestionSeqByQuestID(ids[u].ToString());
                                                                            int seq = int.Parse(depQID) - 1;
                                                                            divid += seq.ToString() + "~";
                                                                        }
                                                                        divid = divid.Remove(divid.Count() - 1);

                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    depQID = qCtrl.getQuestionSeqByQuestID(depQID);
                                                                    int seq = int.Parse(depQID) - 1;
                                                                    divid = seq.ToString();
                                                                }
                                                            }
                                                            HtmlTableCell dTCell5 = new HtmlTableCell();
                                                            if (dt.Rows[i]["quest_input_type"].ToString() == "Radio" || dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "Radio")
                                                            {
                                                                if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "Text")
                                                                {
                                                                    dTCell5.Controls.Add(getCheckBox("radio", dtsub.Rows[q + (quotient + 1) * 2]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", "TB" + i));
                                                                }
                                                                else if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "TextOnly")
                                                                {
                                                                    Label lblInnerText = new Label();
                                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                    lblInnerText.Text = "01";
                                                                    dTCell5.Controls.Add(lblInnerText);
                                                                }
                                                                else
                                                                {
                                                                    dTCell5.Controls.Add(getCheckBox("radio", dtsub.Rows[q + (quotient + 1) * 2]["qitem_id"].ToString(), "singlecheckbox ('" + divid + "',this.checked, this);", i.ToString()));
                                                                }
                                                            }
                                                            else if (dt.Rows[i]["quest_input_type"].ToString() == "Checkbox")
                                                            {
                                                                if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "Text")
                                                                {
                                                                    dTCell5.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + (quotient + 1) * 2]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", "TB" + i));
                                                                }
                                                                else if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "TextOnly")
                                                                {
                                                                    Label lblInnerText = new Label();
                                                                    lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                    lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                    lblInnerText.Text = "01";
                                                                    dTCell5.Controls.Add(lblInnerText);
                                                                }
                                                                else
                                                                {
                                                                    dTCell5.Controls.Add(getCheckBox("checkbox", dtsub.Rows[q + (quotient + 1) * 2]["qitem_id"].ToString(), "multipTextbox (" + divid + ",this.checked, this);", i.ToString()));
                                                                }
                                                            }
                                                            imgname = string.Empty; imgname = dtsub.Rows[q + (quotient + 1) * 2]["remark"].ToString();
                                                            if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_desc"].ToString() != "" && dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() != "Text")
                                                            {
                                                                Label lblInnerText = new Label();
                                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                lblInnerText.Text = dtsub.Rows[q + (quotient + 1) * 2]["qitem_desc"].ToString();
                                                                dTCell5.Controls.Add(lblInnerText);
                                                                if (!string.IsNullOrEmpty(imgname))
                                                                {
                                                                    Literal breakline = new Literal();
                                                                    breakline.Text = "<br />";
                                                                    dTCell5.Controls.Add(breakline);
                                                                    Image img = new Image();
                                                                    img.Attributes.Add("class", "img-responsive");
                                                                    img.ImageUrl = "~/img/" + imgname;
                                                                    dTCell5.Controls.Add(img);
                                                                }
                                                            }
                                                            else if (dtsub.Rows[q + (quotient + 1) * 2]["qitem_type"].ToString() == "Text")
                                                            {
                                                                Label lblInnerText = new Label();
                                                                lblInnerText.Attributes.Add("style", "margin-left: 10px;vertical-align: top");
                                                                lblInnerText.Attributes.Add("class", "chklabelCol3");
                                                                lblInnerText.Text = dtsub.Rows[q + (quotient + 1) * 2]["qitem_desc"].ToString();
                                                                dTCell5.Controls.Add(lblInnerText);
                                                                Literal breakline = new Literal();
                                                                if (!string.IsNullOrEmpty(imgname))
                                                                {
                                                                    breakline = new Literal();
                                                                    breakline.Text = "<br />";
                                                                    dTCell5.Controls.Add(breakline);
                                                                    Image img = new Image();
                                                                    img.Attributes.Add("class", "img-responsive");
                                                                    img.ImageUrl = "~/img/" + imgname;
                                                                    dTCell5.Controls.Add(img);
                                                                }
                                                                breakline = new Literal();
                                                                breakline.Text = "<br />";
                                                                dTCell5.Controls.Add(breakline);
                                                                dTCell5.Controls.Add(getTextBox(dtsub.Rows[q + (quotient + 1) * 2]["qitem_id"].ToString(), i, layer));
                                                            }
                                                            dTRow1.Controls.Add(dTCell5);
                                                            #endregion
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #endregion
                                    }

                                    if (dt.Rows[i]["quest_class"].ToString() != "Header")
                                    {
                                        addControlToDiv(i.ToString(), layer, dTable);
                                    }
                                    dTable = new HtmlTable();
                                    dTable.Attributes.Add("class", "table");//* table-striped
                                    #endregion
                                }
                            }
                            #endregion

                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private string getQID(string showid, string flowid, string type)
    {
        QuestionnaireControler qCtr = new QuestionnaireControler(fn);
        DataTable dt = qCtr.getQID(type, flowid, showid);
        if (dt.Rows.Count > 0)
            return dt.Rows[0]["SQ_QAID"].ToString();
        else
            return "QLST";
    }
    #endregion
    #region Get Controls
    private Control getCheckBoxsub(string type, string qid, string valclass, string isText)
    {
        QAController qCtrl = new QAController(qfn);
        DataTable slayerdt = qCtrl.getQuestItemByQuestID(qid);
        HtmlTable tb = new HtmlTable();
        tb.Attributes.Add("class", "table");
        HtmlTableRow row = new HtmlTableRow();
        if (slayerdt.Rows.Count > 0)
        {
            for (int j = 0; j < slayerdt.Rows.Count; j++)
            {
                row = new HtmlTableRow();
                tb.Controls.Add(row);
                HtmlTableCell cell1 = new HtmlTableCell();
                //cell1.Width = "20";
                //cell1.VAlign = "top";
                HtmlTableCell cell2 = new HtmlTableCell();
                //cell2.Width = "250";
                //cell2.VAlign = "top";
                HtmlInputCheckBox checkbox = new HtmlInputCheckBox();
                checkbox.Value = slayerdt.Rows[j]["qitem_id"].ToString() + "s";
                checkbox.ID = slayerdt.Rows[j]["qitem_id"].ToString() + "s";
                checkbox.Attributes.Add("onclick", valclass);
                if (isText.Contains("TB"))
                {
                    checkbox.Attributes.Add("class", isText);
                }
                else
                {
                    checkbox.Attributes.Add("class", "T" + isText);
                }
                cell1.Controls.Add(checkbox);
                cell2.InnerText = slayerdt.Rows[j]["qitem_desc"].ToString();
                Literal breakline = new Literal();
                breakline.Text = "<br />";
                cell2.Controls.Add(breakline);
                row.Controls.Add(cell1);
                row.Controls.Add(cell2);
            }
        }
        tb.Controls.Add(row);
        return tb;
    }
    private Control getCheckBox(string type, string id, string valclass, string isText)
    {
        HtmlInputCheckBox checkbox = new HtmlInputCheckBox();
        checkbox.Value = id;
        checkbox.ID = id;
        checkbox.Attributes.Add("onclick", valclass);
        if (isText.Contains("TB"))
        {
            checkbox.Attributes.Add("class", isText);
        }
        else
        {
            checkbox.Attributes.Add("class", "T" + isText);
        }
        //for pre-check
        //if (id == "QITM1195" || id == "QITM1197" || id == "QITM1457" || id == "QITM1460" || id == "QITM1816" || id == "QITM1819")
        //{
        //    checkbox.Checked = true;
        //}
        return checkbox;

    }
    private Control getRadio(string type, string id, string valclass, string isText)
    {
        HtmlInputRadioButton radio = new HtmlInputRadioButton();
        radio.Value = id;
        radio.ID = id;
        radio.Attributes.Add("onclick", valclass);
        if (isText.Contains("TB"))
        {
            radio.Attributes.Add("class", isText);
        }
        else
        {
            radio.Attributes.Add("class", "T" + isText);
        }
        //for pre-check
        //if (id == "QITM1195" || id == "QITM1197" || id == "QITM1457" || id == "QITM1460" || id == "QITM1816" || id == "QITM1819")
        //{
        //    radio.Checked = true;
        //}
        return radio;
    }
    private Control getTextBox(string lastid, int i, int layer)
    {
        QAController qCtrl = new QAController(qfn);
        string qitem_desc = qCtrl.getQItemdescByID(lastid);

        TextBox textbox = new TextBox();
        textbox.ID = "txt" + lastid;
        //textbox.Attributes.Add("class", "form-control");

        //textbox.Width = Unit.Pixel(300);
        //textbox.MaxLength = 50;

        textbox.Attributes.Add("onblur", "limitText(this, 2);");//***

        return textbox;
    }
    private Control getLabel(string lastid, int i, int layer)
    {
        QAController qCtrl = new QAController(qfn);
        string qitem_desc = qCtrl.getQItemdescByID(lastid);

        Label label = new Label();
        label.Text = qitem_desc;
        label.Width = Unit.Pixel(200);
        return label;
    }
    private Control getPulldown(string v, int i, List<gen_QuestItem> dtsource)
    {
        DropDownList ddl = new DropDownList();
        ddl.Attributes.Add("class", "col-md-3 form-control");
        ddl.ID = "ddl" + v;
        ddl.DataSource = dtsource;
        ddl.DataTextField = "qitem_desc";
        ddl.DataValueField = "qitem_id";
        ddl.DataBind();
        ListItem lstitem = new ListItem("Please Select", "0");
        ddl.Items.Insert(ddl.Items.Count - ddl.Items.Count, lstitem);
        return ddl;
    }

    private Control getValidateExpress(string textboxID)
    {
        RegularExpressionValidator validator = new RegularExpressionValidator();
        validator.ControlToValidate = textboxID;
        validator.ValidationExpression = @"^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*";
        validator.ID = "VLD" + textboxID;
        validator.Text = "Only English input is allowed. (ใส่ข้อความเป็นภาษาอังกฤษเท่านั้น)";
        validator.ForeColor = System.Drawing.Color.Red;
        return validator;
    }
    #endregion

    #region generateDiv ,generateInnerDiv , addControlToDiv2, addControlToDiv , newLine ,generateTextboxValidator, validation
    protected void generateDiv(string groupId, int isVis)
    {

        HtmlGenericControl div = new HtmlGenericControl("div");
        div.ID = "div_" + groupId;
        div.Attributes.Add("class", "maindiv");
        div.Attributes.Add("style", "padding-bottom: 2px;padding-top: 2px;");
        if (isVis != 1)
        {
            div.Style.Add("Display", "none");
        }
        PanelMain.Controls.Add(div);
    }
    protected void generateInnerDiv(string groupId, int layer)
    {
        HtmlGenericControl div = (HtmlGenericControl)PanelMain.FindControl("div_" + groupId);
        HtmlGenericControl innerdiv = new HtmlGenericControl("div");
        innerdiv.ID = groupId + "_" + layer;
        innerdiv.Attributes.Add("class", "layer" + layer);
        div.Controls.Add(innerdiv);
    }
    protected void addControlToDiv2(string groupId, int layer, Control c)
    {
        HtmlGenericControl div = (HtmlGenericControl)PanelMain.FindControl(groupId + "_" + layer);
        div.Controls.Add(c);
    }
    protected void addControlToDiv(string groupId, int layer, Control c, bool noMargin = false)
    {
        HtmlGenericControl div = (HtmlGenericControl)PanelMain.FindControl(groupId + "_" + layer);
        div.Controls.Add(c);
        if (!noMargin)
        {
            div.Style.Add("margin-bottom", "20px");
        }
    }
    protected void newLine(string groupId, int layer)
    {
        Literal breakline = new Literal();
        breakline.Text = "<br />";
        addControlToDiv(groupId, layer, breakline);
    }
    protected void generateTextboxValidator(string groupId, int layer, Control c)
    {
        RequiredFieldValidator validator = new RequiredFieldValidator();
        validator.CssClass = "failureNotification";
        validator.ErrorMessage = "*Required";
        validator.ValidationGroup = validationGroup;
        validator.ControlToValidate = c.ID;
        addControlToDiv(groupId, layer, validator);
    }
    protected bool validation()
    {
        QAController qCtrl = new QAController(qfn);
        DataTable dt = qCtrl.getQuestionByQnaireID(qnaire_id);

        int ic = dt.Rows.Count;
        string[] arr = new string[ic];

        bool mainvali = true;
        //bool mainvali2 = true;
        bool main = true;
        List<string> errtxtIDlst = new List<string>();
        List<string> lsterrID = new List<string>();
        List<string> lstcusID = new List<string>();
        DataTable table = qCtrl.getMandatoryQuestIDs(qnaire_id, quest_id);
        if (table.Rows.Count > 0)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                string quest = table.Rows[i]["quest_id"].ToString();
                string id = qCtrl.getQuestionSeqByQuestIDQnaireID(quest, qnaire_id);
                int idd = int.Parse(id) - 1;
                if (!lsterrID.Contains(idd.ToString() + "_1"))
                {
                    lsterrID.Add(idd.ToString() + "_1");
                }
            }
        }

        try
        {
            foreach (Control maindiv in PanelMain.Controls)
            {
                foreach (Control layer1 in maindiv.Controls)
                {
                    foreach (Control c in layer1.Controls)
                    {
                        if (c.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTable"))
                        {
                            foreach (Control tr in c.Controls)
                            {
                                if (tr.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTableRow"))
                                {
                                    foreach (Control cell in tr.Controls)
                                    {
                                        if (cell.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTableCell"))
                                        {
                                            foreach (Control element in cell.Controls)
                                            {
                                                #region Checkbox
                                                if (element.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlInputCheckBox"))
                                                {
                                                    HtmlInputCheckBox cb = (HtmlInputCheckBox)element;
                                                    #region CheckControlValue
                                                    if (cb.Checked == true)
                                                    {
                                                        string cbid = cb.ID.Replace("qa1_", string.Empty);
                                                        string chk = qCtrl.getDependentQuestItemByQItemID(cbid);
                                                        if (!string.IsNullOrEmpty(chk))
                                                        {
                                                            // chk = chk.Replace("QSTN10", string.Empty);
                                                            chk = chk.Substring(chk.Length - 4, 4);
                                                            int idd = int.Parse(chk);
                                                            idd = idd - 1;
                                                            string str = idd.ToString() + "_1";
                                                            if (!lsterrID.Contains(str))
                                                            {
                                                                lsterrID.Add(str);
                                                            }
                                                        }
                                                        if (cb.Attributes["class"].Contains("TB"))
                                                        {
                                                            foreach (Control str in c.Controls)
                                                            {
                                                                if (str.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTableRow"))
                                                                {
                                                                    foreach (Control scell in str.Controls)
                                                                    {
                                                                        if (scell.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTableCell"))
                                                                        {
                                                                            foreach (Control elements in scell.Controls)
                                                                            {
                                                                                if (elements.ClientID.Contains("txt"))
                                                                                {
                                                                                    TextBox textbox = (TextBox)elements;
                                                                                    string[] idarr = elements.ClientID.Split('_');
                                                                                    string chkID = idarr[(idarr.Count() - 1)].Replace("txt", string.Empty);
                                                                                    if (chkID == cb.Value)
                                                                                    {
                                                                                        var tt = textbox.Text;

                                                                                        if (tt == "")
                                                                                        {
                                                                                            errtxtIDlst.Add(element.ClientID.ToString());
                                                                                            textbox.BorderColor = System.Drawing.Color.Red;
                                                                                            textbox.BorderStyle = BorderStyle.Solid;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            textbox.BorderColor = System.Drawing.Color.LightGray;
                                                                                        }
                                                                                        if (textbox.Text != "")
                                                                                        {
                                                                                            lsterrID.Add(layer1.ID);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if (lsterrID.Contains(layer1.ID))
                                                                                            {
                                                                                                lsterrID.Remove(layer1.ID);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            lsterrID.Add(layer1.ID);
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                #endregion

                                                #region Dropdown
                                                if (element.GetType().ToString().Equals("System.Web.UI.WebControls.DropDownList"))
                                                {
                                                    DropDownList cb = (DropDownList)element;
                                                    string val = cb.SelectedValue.ToString();
                                                    if (val != "0")
                                                    {
                                                        lsterrID.Add(layer1.ID);
                                                    }
                                                }
                                                #endregion

                                                #region TextBox
                                                if (element.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                                {
                                                    TextBox textbox = (TextBox)element;
                                                    string strval = textbox.Text;
                                                    if (!string.IsNullOrEmpty(strval))
                                                    {
                                                        lsterrID.Add(layer1.ID);
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                        //if (cell.Controls.Count == 0)
                                        //{
                                        //    lsterrID.Add(layer1.ID);
                                        //}
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #region Error MSg
            foreach (Control maindiv in PanelMain.Controls)
            {
                foreach (Control layer1 in maindiv.Controls)
                {
                    if (!lsterrID.Contains(layer1.ID))
                    {
                        Label lbl = new Label();
                        lbl.Text = " * Required";
                        lbl.CssClass = "failureNotification";
                        lbl.ForeColor = System.Drawing.Color.Red;
                        layer1.Controls.Add(lbl);
                        mainvali = false;
                    }
                }
            }
            #endregion

            if (mainvali == false)
            {
                main = false;
                Label lbl = new Label();
                lbl.Text = " * You didn’t complete some of the questions.Please complete the form before you submit.";
                lbl.CssClass = "failureNotification";
                lbl.ForeColor = System.Drawing.Color.Red;
                PanelMain.Controls.Add(lbl);
            }
        }
        catch (Exception ex)
        { }

        return main;
    }
    protected void addHeaderTableRowToHtmlTable(HtmlTable dTable, int layout)
    {
        if (layout == 1)
        {
            HtmlTableRow dTRowHeader = new HtmlTableRow();
            HtmlTableCell dTCellHead1 = new HtmlTableCell("th");
            dTCellHead1.InnerText = "";
            dTRowHeader.Controls.Add(dTCellHead1);
            dTable.Controls.Add(dTRowHeader);
        }
        else if (layout == 2)
        {
            HtmlTableRow dTRowHeader = new HtmlTableRow();
            HtmlTableCell dTCellHead1 = new HtmlTableCell("th");
            dTCellHead1.InnerText = "";
            dTRowHeader.Controls.Add(dTCellHead1);
            HtmlTableCell dTCellHead2 = new HtmlTableCell("th");
            dTCellHead2.InnerText = "";
            dTRowHeader.Controls.Add(dTCellHead2);
            dTable.Controls.Add(dTRowHeader);

        }
        else if (layout == 3)
        {
            HtmlTableRow dTRowHeader = new HtmlTableRow();
            HtmlTableCell dTCellHead1 = new HtmlTableCell("th");
            dTCellHead1.InnerText = "";
            dTRowHeader.Controls.Add(dTCellHead1);
            HtmlTableCell dTCellHead2 = new HtmlTableCell("th");
            dTCellHead2.InnerText = "";
            dTRowHeader.Controls.Add(dTCellHead2);
            HtmlTableCell dTCellHead3 = new HtmlTableCell("th");
            dTCellHead3.InnerText = "";
            dTRowHeader.Controls.Add(dTCellHead3);
            dTable.Controls.Add(dTRowHeader);
        }
    }
    #endregion

    #region BindEdit
    private void BindEdit(string qnaire_id, string user_id)
    {
        QAController qCtrl = new QAController(qfn);
        string qnaire_log_id = qCtrl.getQnaireLogIDByQnaireIDUserID(qnaire_id, user_id);
        if (!string.IsNullOrEmpty(qnaire_log_id))
        {
            DataTable dt = qCtrl.getQnaireResult(qnaire_log_id, quest_id);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string id = dt.Rows[i]["qitem_id"].ToString();

                Control c = new Control();
                string remark = string.Empty;

                if (id.Contains("QSTN"))
                {
                    remark = qCtrl.getInputTypeByQuestID(id);
                    if (remark.Contains("Text"))
                    {
                        string controlID = "txt" + id;
                        Control table = PanelMain.FindControl("txt" + id);
                        try
                        {
                            c = table.FindControl("txt" + id);
                        }
                        catch (Exception ex) { c = PanelMain.FindControl(id); }

                    }
                }
                else
                {
                    remark = qCtrl.getInputTypeByQuestID(id);
                    if (remark.Contains("Text"))
                    {
                        string controlID = "txt" + id;
                        Control table = PanelMain.FindControl("txt" + id);
                        try
                        {
                            c = table.FindControl(id);
                        }
                        catch (Exception ex) { c = PanelMain.FindControl(id); }
                    }
                    else
                    {
                        try
                        {
                            c = PanelMain.FindControl(id);
                        }
                        catch (Exception ex) { }
                    }
                }

                if (c != null)
                {
                    if (c.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlInputCheckBox"))
                    {
                        bool check = false;
                        if (dt.Rows[i]["qitem_input_num"].ToString() == "1")
                        {
                            check = true;
                        }
                        HtmlInputCheckBox checkbox = (HtmlInputCheckBox)c;
                        if (checkbox.Attributes["class"].Contains("TB"))
                        {

                            TextBox tb = (TextBox)PanelMain.FindControl("txt" + id);
                            if (dt.Rows[i]["qitem_input_txt"].ToString() != "")
                            {
                                tb.Text = dt.Rows[i]["qitem_input_txt"].ToString();
                                tb.Enabled = true;
                                check = true;
                            }
                        }
                        checkbox.Checked = check;
                    }
                    else if (c.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlInputRadioButton"))
                    {
                        bool check = false;
                        if (dt.Rows[i]["qitem_input_num"].ToString() == "1")
                        {
                            check = true;
                        }
                        HtmlInputRadioButton radio = (HtmlInputRadioButton)c;
                        if (radio.Attributes["class"].Contains("TB"))
                        {

                            TextBox tb = (TextBox)PanelMain.FindControl("txt" + id);
                            if (dt.Rows[i]["qitem_input_txt"].ToString() != "")
                            {
                                tb.Text = dt.Rows[i]["qitem_input_txt"].ToString();
                                tb.Enabled = true;
                                check = true;
                            }
                        }
                        radio.Checked = check;
                    }
                    else if (c.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                    {
                        TextBox tb = (TextBox)c;
                        tb.Text = dt.Rows[i]["qitem_input_txt"].ToString();
                    }

                }
            }
        }
    }
    #endregion

    #region Submit
    public int btnSubmit_Click()
    {
        QAController qCtrl = new QAController(qfn);
        int isval = 0;
        if (string.IsNullOrEmpty(quest_id))
        {
            qCtrl.deleteQnaireLogOld(qnaire_id, user_id);
        }

        bool isSaveSuccess = Submit();//***
        if (isSaveSuccess)
        {
            isval = 1;
        }
        else
        {
            isval = 0;
        }
        return isval;
    }
    private bool Submit()
    {
        bool isSuccess = true;
        try
        {
            #region Submit
            foreach (Control maindiv in PanelMain.Controls)
            {
                foreach (Control layer1 in maindiv.Controls)
                {
                    foreach (Control c in layer1.Controls)
                    {
                        if (c.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTable"))
                        {
                            foreach (Control tr in c.Controls)
                            {
                                if (tr.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTableRow"))
                                {
                                    foreach (Control cell in tr.Controls)
                                    {
                                        if (cell.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTableCell"))
                                        {
                                            foreach (Control element in cell.Controls)
                                            {
                                                #region Radio
                                                if (element.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlInputRadioButton"))
                                                {
                                                    HtmlInputRadioButton cb = (HtmlInputRadioButton)element;
                                                    if (cb.Attributes["onclick"] != null)
                                                    {
                                                        string input = "";
                                                        if (cb.Checked == true)
                                                        {
                                                            input = "1";

                                                        }
                                                        else
                                                        {
                                                            input = "0";
                                                        }
                                                        if (cb.Attributes["class"].Contains("TB"))
                                                        {
                                                            if (cb.Checked == true)
                                                            {
                                                                foreach (Control str in c.Controls)
                                                                {
                                                                    if (str.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTableRow"))
                                                                    {
                                                                        foreach (Control scell in str.Controls)
                                                                        {
                                                                            if (scell.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTableCell"))
                                                                            {
                                                                                foreach (Control elements in scell.Controls)
                                                                                {
                                                                                    if (elements.ClientID.Contains("txt"))
                                                                                    {
                                                                                        TextBox textbox = (TextBox)elements;
                                                                                        string[] idarr = elements.ClientID.Split('_');
                                                                                        string chkID = idarr[idarr.Count() - 1].Replace("txt", string.Empty);
                                                                                        if (chkID == cb.Value)
                                                                                        {
                                                                                            submitToDB(cb.Value, "", textbox.Text);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (input != "0")
                                                            {
                                                                submitToDB(cb.Value, input, "");
                                                                input = "0";
                                                            }
                                                        }

                                                    }
                                                }
                                                #endregion

                                                #region CheckBox
                                                else if (element.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlInputCheckBox"))
                                                {
                                                    HtmlInputCheckBox cb = (HtmlInputCheckBox)element;
                                                    if (cb.Attributes["onclick"] != null)
                                                    {
                                                        string input = "";
                                                        if (cb.Checked == true)
                                                        {
                                                            input = "1";

                                                        }
                                                        else
                                                        {
                                                            input = "0";
                                                        }
                                                        if (cb.Attributes["class"].Contains("TB"))
                                                        {
                                                            if (cb.Checked == true)
                                                            {
                                                                foreach (Control str in c.Controls)
                                                                {
                                                                    if (str.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTableRow"))
                                                                    {
                                                                        foreach (Control scell in str.Controls)
                                                                        {
                                                                            if (scell.GetType().ToString().Equals("System.Web.UI.HtmlControls.HtmlTableCell"))
                                                                            {
                                                                                foreach (Control elements in scell.Controls)
                                                                                {
                                                                                    if (elements.ClientID.Contains("txt"))
                                                                                    {
                                                                                        TextBox textbox = (TextBox)elements;
                                                                                        string[] idarr = elements.ClientID.Split('_');
                                                                                        string chkID = idarr[idarr.Count() - 1].Replace("txt", string.Empty);
                                                                                        if (chkID == cb.Value)
                                                                                        {
                                                                                            submitToDB(cb.Value, "", textbox.Text);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (input != "0")
                                                            {
                                                                submitToDB(cb.Value, input, "");
                                                                input = "0";
                                                            }
                                                        }
                                                    }
                                                }//end if 
                                                #endregion

                                                #region TextBox
                                                else if (element.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                                {
                                                    TextBox textbox = (TextBox)element;
                                                    string[] idarr = element.ClientID.Split('_');
                                                    string chkID = idarr[idarr.Count() - 1].Replace("txt", string.Empty);
                                                    if (chkID.Contains("QSTN"))
                                                    {
                                                        submitToDB(chkID, "", textbox.Text);
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion
        }
        catch (Exception ex) { isSuccess = false; }

        return isSuccess;
    }
    private void submitToDB(string questionId, string Answerno, string Answertxt)
    {
        QAController qCtrl = new QAController(qfn);
        string Answer = Answertxt.Replace("'", "''");
        string logid = "", quest_id = "";
        #region new
        string id = "";
        try
        {
            string chkres = qCtrl.CheckQLogExist(qnaire_id, "ON", user_id);
            if (chkres == "0")
            {
                id = "QLOG" + getKey("QLOG");
                logid = qCtrl.SaveandTakeID(id, qnaire_id, "ON", user_id);
            }
            else
            {
                logid = chkres;
            }

            if (logid != null)
            {
                if (questionId.Contains("QSTN"))
                {
                    quest_id = questionId;
                }
                else
                {
                    quest_id = qCtrl.getQuestIDByQItemID(questionId);
                }

                string qitem_id = questionId;
                int qitem_input_num = 0;
                string qitem_input_txt = "";

                if (!string.IsNullOrEmpty(Answerno))
                {
                    qitem_input_num = int.Parse(Answerno);
                }
                else
                {
                    qitem_input_txt = Answer;
                }

                int checkexist = qCtrl.CheckQResExist(logid, quest_id, qitem_id);
                int res = 0;

                if (checkexist == 1)
                {
                    //Update                  
                    res = qCtrl.Update(logid, quest_id, qitem_id, qitem_input_num, qitem_input_txt, "Active", "update");
                }
                else
                {
                    //Save
                    res = qCtrl.Save(logid, quest_id, qitem_id, qitem_input_num, qitem_input_txt, "Active", "insert");
                }
                if (res == 1)
                {
                    if (chkres == "0")
                    {
                        Savekey("QLOG", int.Parse(getKey("QLOG")));
                    }
                }

            }
        }
        catch (Exception ex) { throw ex; }
        #endregion
    }
    private string getKey(string p)
    {
        QAController qCtrl = new QAController(qfn);
        string key = "";
        try
        {
            key = qCtrl.getKey(p);
        }
        catch (Exception ex) { throw ex; }
        return key;
    }
    private void Savekey(string p, int key)
    {
        QAController qCtrl = new QAController(qfn);
        try
        {
            int kno = key + 1;
            qCtrl.SaveKey(p, kno);

        }
        catch (Exception ex) { throw ex; }
    }

    #endregion
    #endregion

    #region Load
    private void BindQADropDownList(string showid)
    {
        LoadProfession(showid);
        LoadOrgList(showid);
        LoadDepartment(showid);
        LoadTimeDate(showid);
        LoadQA4(showid);
    }


    private void LoadSalDropdownList(string showid)
    {
        CommonDataObj cmdObj = new CommonDataObj(fn);
        DataSet dsSalutation = new DataSet();
        dsSalutation = cmdObj.getSalutation(showid);
        LoadSal(ref txt1Sal, ref dsSalutation);
        LoadSal(ref txt2Sal, ref dsSalutation);
        LoadSal(ref txt3Sal, ref dsSalutation);
        LoadSal(ref txt4Sal, ref dsSalutation);
        LoadSal(ref txt5Sal, ref dsSalutation);
        LoadSal(ref txt6Sal, ref dsSalutation);
        LoadSal(ref txt7Sal, ref dsSalutation);
        LoadSal(ref txt8Sal, ref dsSalutation);
        LoadSal(ref txt9Sal, ref dsSalutation);
        LoadSal(ref txt10Sal, ref dsSalutation);
        LoadSal(ref txt11Sal, ref dsSalutation);
        LoadSal(ref txt12Sal, ref dsSalutation);
        LoadSal(ref txt13Sal, ref dsSalutation);
        LoadSal(ref txt14Sal, ref dsSalutation);
        LoadSal(ref txt15Sal, ref dsSalutation);
        LoadSal(ref txt16Sal, ref dsSalutation);
        LoadSal(ref txt17Sal, ref dsSalutation);
        LoadSal(ref txt18Sal, ref dsSalutation);
        LoadSal(ref txt19Sal, ref dsSalutation);
        LoadSal(ref txt20Sal, ref dsSalutation);
    }
    private void intializePanelList(ref List<Panel> panelList)
    {
        panelList.Add(Panel1);
        panelList.Add(Panel2);
        panelList.Add(Panel3);
        panelList.Add(Panel4);
        panelList.Add(Panel5);
        panelList.Add(Panel6);
        panelList.Add(Panel7);
        panelList.Add(Panel8);
        panelList.Add(Panel9);
        panelList.Add(Panel10);
        panelList.Add(Panel11);
        panelList.Add(Panel12);
        panelList.Add(Panel13);
        panelList.Add(Panel14);
        panelList.Add(Panel15);
        panelList.Add(Panel16);
        panelList.Add(Panel17);
        panelList.Add(Panel18);
        panelList.Add(Panel19);
        panelList.Add(Panel20);
    }
    private void LoadMemberList()
    {
        ddlTotalMember.ClearSelection();
        ddlTotalMember.Items.Clear();

        string desc = "persons";
        int minMember = 5;
        int maxMember = 20;
        int i = minMember;
        while (i <= maxMember)
        {
            ListItem li = new ListItem();
            li.Value = i.ToString();
            li.Text = string.Format("{0} {1}", i.ToString(), desc);
            ddlTotalMember.Items.Add(li);
            i++;
        }
    }
    private void ShowHideMemberPanel(string totalMember)
    {
        CommonFuns cFunz = new CommonFuns();
        int iTotal = cFun.ParseInt(totalMember);

        List<Panel> panelList = new List<Panel>();
        intializePanelList(ref panelList);
        panelList.Select(c => { c.Visible = false; return c; }).ToList();
        panelList.Take(iTotal).Select(c => { c.Visible = true; return c; }).ToList();
    }
    #endregion

    private void LoadCountry()
    {
        DataSet dsCountry = new DataSet();
        CountryObj couObj = new CountryObj(fn);
        dsCountry = couObj.getAllCountry();
        if (dsCountry.Tables[0].Rows.Count != 0)
        {
            for (int x = 0; x < dsCountry.Tables[0].Rows.Count; x++)
            {
                ddlComCountry.Items.Add(dsCountry.Tables[0].Rows[x]["Country"].ToString());
                ddlComCountry.Items[x + 1].Value = dsCountry.Tables[0].Rows[x]["Cty_GUID"].ToString();
            }
        }
    }
    private void LoadOrgList(string showid)
    {
        DataSet dsOrgType = new DataSet();
        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsOrgType = cmdObj.getOrganization(showid);
        if (dsOrgType.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsOrgType.Tables[0].Rows.Count; i++)
            {
                ddlOrganization.Items.Add(dsOrgType.Tables[0].Rows[i]["organisation"].ToString());
                ddlOrganization.Items[i + 1].Value = dsOrgType.Tables[0].Rows[i]["ID"].ToString();
            }
        }
    }
    private void LoadProfession(string showid)
    {
        DataSet dsProfession = new DataSet();
        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsProfession = cmdObj.getProfession(showid);
        if (dsProfession.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsProfession.Tables[0].Rows.Count; i++)
            {
                ddlProfession.Items.Add(dsProfession.Tables[0].Rows[i]["Profession"].ToString());
                ddlProfession.Items[i + 1].Value = dsProfession.Tables[0].Rows[i]["ID"].ToString();
            }
        }
    }

    private void LoadDepartment(string showid)
    {
        DataSet dsDepartment = new DataSet();
        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsDepartment = cmdObj.getDepartmentByInstitutionID("0", showid);
        if (dsDepartment.Tables[0].Rows.Count > 0)
        {
            //  ddlDepartment.Items.Add("Please Select");
            //ddlDepartment.Items[0].Value = "0";
            ListItem lstitem = new ListItem("Please Select", "0");
            ddlDepartment.Items.Add(lstitem);
            for (int y = 0; y < dsDepartment.Tables[0].Rows.Count; y++)
            {
                ddlDepartment.Items.Add(dsDepartment.Tables[0].Rows[y]["department"].ToString());
                ddlDepartment.Items[y + 1].Value = dsDepartment.Tables[0].Rows[y]["ID"].ToString();
            }
        }
        else
        {
            ddlDepartment.Items.Add("Please Select");
            ddlDepartment.Items[0].Value = "0";
        }
    }

    private void LoadSal(ref DropDownList ddlSa, ref DataSet dsSalutation)
    {

        if (dsSalutation.Tables[0].Rows.Count != 0)
        {
            for (int i = 0; i < dsSalutation.Tables[0].Rows.Count; i++)
            {
                ddlSa.Items.Add(dsSalutation.Tables[0].Rows[i]["Sal_Name"].ToString());
                ddlSa.Items[i + 1].Value = dsSalutation.Tables[0].Rows[i]["Sal_ID"].ToString();
            }
        }
    }

    private void LoadTimeDate(string showid)
    {
        DataSet dsdiet = new DataSet();
        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsdiet = cmdObj.getDietary(showid);

        if (dsdiet.Tables[0].Rows.Count != 0)
        {
            for (int y = 0; y < dsdiet.Tables[0].Rows.Count; y++)
            {
                ddlVisitDate.Items.Add(dsdiet.Tables[0].Rows[y]["diet_name"].ToString());
                ddlVisitDate.Items[y + 1].Value = dsdiet.Tables[0].Rows[y]["diet_id"].ToString();
            }
        }
    }
    private void LoadQA4(string showid)
    {
        DataSet dsAffiliation = new DataSet();
        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsAffiliation = cmdObj.getAffiliation(showid);
        divAffiliation.Visible = false;
        if (dsAffiliation.Tables[0].Rows.Count != 0)
        {
            for (int y = 0; y < dsAffiliation.Tables[0].Rows.Count; y++)
            {
                ddlAffiliation.Items.Add(dsAffiliation.Tables[0].Rows[y]["aff_name"].ToString());
                ddlAffiliation.Items[y + 1].Value = dsAffiliation.Tables[0].Rows[y]["affid"].ToString();
            }
            divAffiliation.Visible = true;
        }
    }
    private void ShowHideSalOther(string salText, ref HtmlGenericControl divOther)
    {
        OthersSettings othersetting = new OthersSettings(fn);
        List<string> lstOthersValue = othersetting.lstOthersValue;

        if (lstOthersValue.Contains(salText))
        {
            divOther.Visible = true;
        }
        else divOther.Visible = false;
    }

    #region Save
    private string SaveContactPerson()
    {
        string rtnGroupID = "";
        try
        {
            string actType = "";
            LogActionObj rlgobj = new LogActionObj();
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            bool isValid = true;
            CategoryClass catClass = new CategoryClass();
            FlowControler fCon = new FlowControler(fn);
            FlowMaster flwMaster = fCon.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));
            string type = flwMaster.FlowCategoryConfigType;


            int isSuccess = 0;
            Boolean hasid = false;
            string groupid = string.Empty;

            RegGroupObj rgg = new RegGroupObj(fn);

            groupid = cFun.DecryptValue(urlQuery.GoupRegID);

            DataTable dt = rgg.getRegGroupByID(groupid, showid);
            if (dt.Rows.Count > 0)
            {
                hasid = true;
            }

            string salutation = string.Empty;
            string fname = string.Empty;
            string lname = string.Empty;
            string designation = string.Empty;
            string otherdesignation = string.Empty;//*
            string department = string.Empty;
            string company = string.Empty;
            string industry = string.Empty;
            string address1 = string.Empty;
            string address2 = string.Empty;
            string address3 = string.Empty;
            string city = string.Empty;
            string state = string.Empty;
            string postalcode = string.Empty;
            string country = string.Empty;
            string rcountry = string.Empty;
            string telcc = string.Empty;
            string telac = string.Empty;
            string tel = string.Empty;
            string mobilecc = string.Empty;
            string mobileac = string.Empty;
            string mobile = string.Empty;
            string faxcc = string.Empty;
            string faxac = string.Empty;
            string fax = string.Empty;
            string email = string.Empty;
            string remark = string.Empty;
            string Type_NHG_NonNHG = string.Empty;
            string sal_other = string.Empty;
            string desig_other = string.Empty;
            string indus_other = string.Empty;
            string visitdate = string.Empty;
            string visittime = string.Empty;
            string password = string.Empty;
            string ismul = SiteFlowType.FLOW_GROUP;
            string reffalcode = string.Empty;
            int isFromsale = 0;
            int isSendEmail = 0;
            int isIndivSendEmail = 0;
            string createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            int recycle = 0;
            string stage = string.Empty;

            int age = 0;
            DateTime? dob = null;
            string dob_str = string.Empty;
            string gender = string.Empty;
            string additional4 = string.Empty;
            string additional5 = string.Empty;

            stage = cFun.DecryptValue(urlQuery.CurrIndex);

            company = cFun.solveSQL(txtCompany.Text.ToString().Trim().ToUpper());
            address1 = cFun.solveSQL(txtComAdd.Text.ToString().Trim());
            address2 = cFun.solveSQL(txtComAdd2.Text.ToString().Trim().ToUpper());
            country = cFun.solveSQL(ddlComCountry.SelectedItem.Value.ToString());
            city = cFun.solveSQL(txtComCity.Text.ToString().Trim());
            state = cFun.solveSQL(txtComState.Text.Trim());
            postalcode = cFun.solveSQL(txtComPostCode.Text.Trim());
            additional4 = cFun.solveSQL(txtComWebsite.Text.Trim());
            telcc = cFun.solveSQL(txtComTelCC.Text.Trim());
            telac = cFun.solveSQL(txtComTelAC.Text.Trim());
            tel = cFun.solveSQL(txtComTel.Text.Trim());

            string regno = string.Empty;

            rgg.groupid = groupid;
            rgg.salutation = salutation;
            rgg.fname = fname;
            rgg.lname = lname;
            rgg.designation = designation;
            rgg.desig_other = otherdesignation;//*
            rgg.department = department;
            rgg.company = company;
            rgg.industry = industry;
            rgg.address1 = address1;
            rgg.address2 = address2;
            rgg.address3 = address3;
            rgg.city = city;
            rgg.state = state;
            rgg.postalcode = postalcode;
            rgg.country = country;
            rgg.rcountry = rcountry;
            rgg.telcc = telcc;
            rgg.telac = telac;
            rgg.tel = tel;
            rgg.mobilecc = mobilecc;
            rgg.mobileac = mobileac;
            rgg.mobile = mobile;
            rgg.faxcc = faxcc;
            rgg.faxac = faxac;
            rgg.fax = fax;
            rgg.email = email;
            rgg.remark = remark;
            rgg.Type_NHG_NonNHG = Type_NHG_NonNHG;
            rgg.sal_other = sal_other;
            rgg.indus_other = indus_other;
            rgg.visitdate = visitdate;
            rgg.visittime = visittime;
            rgg.password = password;
            rgg.ismul = ismul;
            rgg.reffalcode = regno;//reffalcode;
            rgg.isFromsale = isFromsale;
            rgg.isSendEmail = isSendEmail;
            rgg.isIndivSendEmail = isIndivSendEmail;
            rgg.createdate = createdate;
            rgg.recycle = recycle;
            rgg.stage = stage;

            rgg.age = age;
            rgg.dob = dob_str;
            rgg.gender = gender;
            rgg.additional4 = additional4;
            rgg.additional5 = additional5;

            rgg.showID = showid;

            if (hasid)
            {
                isSuccess = rgg.updateRegGroup();
                actType = rlgobj.actupdate;
            }
            else {
                isSuccess = rgg.saveRegGroup();
                actType = rlgobj.actsave;
            }

            if (isSuccess > 0)
            {
                rtnGroupID = groupid;
                insertLogFlowAction(groupid, "", actType, urlQuery);
                FlowControler flwObj = new FlowControler(fn, urlQuery);
            }
        }
        catch { }
        return rtnGroupID;
    }

    private string SaveMember(RegMdAMember regMember, bool isFristRecord, string mainQARegno)
    {
        string rtnRegno = "";
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            bool isValid = true;

            int con_categoryID = 0;
            string salutation = string.Empty;
            string fname = string.Empty;
            string lname = string.Empty;
            string oname = string.Empty;
            string passno = string.Empty;
            int isreg = 0;
            string regspecific = string.Empty;//MCR/SNB/PRN
            string idno = string.Empty;//MCR/SNB/PRN No.
            string staffid = string.Empty;//no use in design for this field
            string designation = string.Empty;
            string jobtitle = string.Empty;//if Profession is Allied Health
            string profession = string.Empty;
            string department = string.Empty;
            string organization = string.Empty;
            string institution = string.Empty;
            string address1 = string.Empty;
            string address2 = string.Empty;
            string address3 = string.Empty;
            string address4 = string.Empty;
            string city = string.Empty;
            string state = string.Empty;
            string postalcode = string.Empty;
            string country = string.Empty;
            string rcountry = string.Empty;
            string telcc = string.Empty;
            string telac = string.Empty;
            string tel = string.Empty;
            string mobilecc = string.Empty;
            string mobileac = string.Empty;
            string mobile = string.Empty;
            string faxcc = string.Empty;
            string faxac = string.Empty;
            string fax = string.Empty;
            string email = string.Empty;
            string affiliation = string.Empty;
            string dietary = string.Empty;
            string nationality = string.Empty;
            int age = 0;
            DateTime? dob = null;
            string dob_str = string.Empty;
            string gender = string.Empty;
            string additional4 = string.Empty;
            string additional5 = string.Empty;
            string memberno = string.Empty;

            string vname = string.Empty;
            string vdob = string.Empty;
            string vpassno = string.Empty;
            string vpassexpiry = string.Empty;
            string vpassissuedate = string.Empty;
            string vembarkation = string.Empty;
            string varrivaldate = string.Empty;
            string vcountry = string.Empty;

            string udfcname = string.Empty;
            string udfdeltype = string.Empty;
            string udfprofcat = string.Empty;
            string udfprofcatother = string.Empty;
            string udfcpcode = string.Empty;
            string udfcldept = string.Empty;
            string udfcaddress = string.Empty;
            string udfclcompany = string.Empty;
            string udfclcompanyother = string.Empty;
            string udfccountry = string.Empty;

            string supname = string.Empty;
            string supdesignation = string.Empty;
            string supcontact = string.Empty;
            string supemail = string.Empty;

            string othersal = string.Empty;
            string otherprof = string.Empty;
            string otherdept = string.Empty;
            string otherorg = string.Empty;
            string otherinstitution = string.Empty;

            string aemail = string.Empty;
            int isSMS = 0;

            string remark = string.Empty;
            string remark_groupupload = string.Empty;
            int approvestatus = 0;
            string createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
            int recycle = 0;
            string stage = string.Empty;


            stage = cFun.DecryptValue(urlQuery.CurrIndex);
            RegDelegateObj rgd = new RegDelegateObj(fn);
            string regno = "";
            if (String.IsNullOrEmpty(regno))
            {
                regno = rgd.GenDelegateNumber(showid);
            }
            rgd.groupid = regMember.GroupID;
            rgd.regno = regno;
            rgd.con_categoryID = con_categoryID;
            rgd.salutation = regMember.Sal;
            rgd.fname = regMember.FName;
            rgd.lname = regMember.LName;
            rgd.oname = regMember.FullName;
            rgd.passno = passno;
            rgd.isreg = isreg;
            rgd.regspecific = regspecific;//MCR/SNB/PRN
            rgd.idno = idno;//MCR/SNB/PRN No.
            rgd.staffid = staffid;//no use in design for this field
            rgd.designation = regMember.JobTitle;
            rgd.jobtitle = jobtitle;//if Profession is Allied Health
            rgd.profession = regMember.QA1;
            rgd.department = regMember.QA2;
            rgd.organization = regMember.QA3;
            rgd.institution = institution;
            rgd.address1 = regMember.Company;
            rgd.address2 = regMember.Website;
            rgd.address3 = regMember.Address;
            rgd.address4 = regMember.Address2;
            rgd.city = regMember.City;
            rgd.state = regMember.State;
            rgd.postalcode = regMember.PostalCode;
            rgd.country = regMember.Country;
            rgd.rcountry = rcountry;
            rgd.telcc = regMember.TelCC;
            rgd.telac = regMember.TelAC;
            rgd.tel = regMember.Tel;
            rgd.mobilecc = regMember.MobileCC;
            rgd.mobileac = mobileac;
            rgd.mobile = regMember.Mobile;
            rgd.faxcc = faxcc;
            rgd.faxac = faxac;
            rgd.fax = fax;
            rgd.email = regMember.Email;
            rgd.affiliation = regMember.QA4 ;
            rgd.dietary = regMember.DateofVisit;
            rgd.nationality = nationality;
            rgd.age = age;
            rgd.dob = dob_str;
            rgd.gender = gender;
            rgd.additional4 = additional4;
            rgd.additional5 = additional5;
            rgd.memberno = memberno;

            rgd.vname = vname;
            rgd.vdob = vdob;
            rgd.vpassno = vpassno;
            rgd.vpassexpiry = vpassexpiry;
            rgd.vpassissuedate = vpassissuedate;
            rgd.vembarkation = vembarkation;
            rgd.varrivaldate = varrivaldate;
            rgd.vcountry = vcountry;

            rgd.udfcname = udfcname;
            rgd.udfdeltype = udfdeltype;
            rgd.udfprofcat = udfprofcat;
            rgd.udfprofcatother = udfprofcatother;
            rgd.udfcpcode = udfcpcode;
            rgd.udfcldept = udfcldept;
            rgd.udfcaddress = udfcaddress;
            rgd.udfclcompany = udfclcompany;
            rgd.udfclcompanyother = udfclcompanyother;
            rgd.udfccountry = udfccountry;

            rgd.supname = supname;
            rgd.supdesignation = supdesignation;
            rgd.supcontact = supcontact;
            rgd.supemail = supemail;

            rgd.othersal = regMember.Salother;
            rgd.otherprof = regMember.QA1Other;
            rgd.otherdept = regMember.QA2Other;
            rgd.otherorg = regMember.QA3Other;
            rgd.otherinstitution = otherinstitution;

            rgd.aemail = aemail;
            rgd.isSMS = isSMS;

            rgd.remark = remark;
            rgd.remark_groupupload = remark_groupupload;
            rgd.approvestatus = approvestatus;
            rgd.createdate = createdate;
            rgd.recycle = recycle;
            rgd.stage = stage;

            rgd.showID = showid;
            
            string actType = string.Empty;
            LogActionObj rlgobj = new LogActionObj();
            int isSuccess = rgd.saveRegDelegate();
            actType = rlgobj.actsave;

            if (isSuccess > 0)
            {
 
                rgd.updateStep(regno, flowid, urlQuery.CurrIndex, showid);
                regMember.Regno = regno;
                rtnRegno = regno;
                CategoryObj catObj = new CategoryObj(fn);
                int.TryParse(catObj.checkCategory(urlQuery, regno, ""), out con_categoryID);
                rgd.updateCategoryID(regno, con_categoryID, showid);

                SaveMemberQA(showid, flowid, isFristRecord, ref regMember, mainQARegno);

            }
        }
        catch { }

        return rtnRegno;
    }

    private void SaveMemberQA(string showid, string flowid, bool isFristRecord, ref RegMdAMember regMember, string mainQARegNO)
    {
        if (isFristRecord)
        {
            user_id = regMember.Regno;
            int isQASaving = btnSubmit_Click();
            if (isQASaving == 0)
            {
                //Correct
            }
        }
        else
        {
            //Update QA

            QAHelper qaHelp = new QAHelper();
            string delegateQAID = qaHelp.GetQAIDfromFlow(showid, flowid, "D");
            if (!string.IsNullOrEmpty(delegateQAID) && !string.IsNullOrEmpty(mainQARegNO))
            {
                string QALINK = "";
                QALINK = string.Format(@"rtype=duplicateAnswer&qnaireID={0}&userID={1}&newuserID={2}", delegateQAID, mainQARegNO, regMember.Regno);
                QAHelper qHelp = new QAHelper();
                bool OK = qHelp.DuplicateQA(QALINK);
            }

        }
    }
    #endregion

    private bool SaveAllMember(string groupID)
    {
        RegMdAMember regMember = new RegMdAMember();
        bool isOK = true;
        string cPreFix = "txt5";
        int currentIndx = 1;
        int totalMember = cFun.ParseInt(ddlTotalMember.SelectedValue);
        bool isFristRecord = true;
        string firstMemberRegno = "";
        string regno = "";
        while (currentIndx <= totalMember)
        {
            Panel pContainer = (Panel)UpdatePanel1.FindControl("Panel" + currentIndx.ToString());
            if (pContainer != null)
            {
                cPreFix = "txt" + currentIndx.ToString();
                DropDownList txtSal = (DropDownList)pContainer.FindControl(cPreFix + "Sal");
                TextBox txtSalOther = (TextBox)pContainer.FindControl(cPreFix + "SalOther");
                TextBox txtLName = (TextBox)pContainer.FindControl(cPreFix + "LName");
                TextBox txtFName = (TextBox)pContainer.FindControl(cPreFix + "FName");
                TextBox txtFullName = (TextBox)pContainer.FindControl(cPreFix + "FullName");
                TextBox txtJobTitle = (TextBox)pContainer.FindControl(cPreFix + "JobTitle");
                TextBox txtEmail = (TextBox)pContainer.FindControl(cPreFix + "Email");
                TextBox txtMobileCC = (TextBox)pContainer.FindControl(cPreFix + "MobileCC");
                TextBox txtMobile = (TextBox)pContainer.FindControl(cPreFix + "Mobile");
                string sal = ""; string salOther = ""; string fName = ""; string lName = ""; string fullName = ""; string jobTitle = ""; string eMail = ""; string mobileCC = ""; string mobile = "";

                if (txtSal != null) sal = txtSal.SelectedValue; if (txtSalOther != null) salOther = txtSalOther.Text;
                if (txtLName != null) lName = txtLName.Text; if (txtFName != null) fName = txtFName.Text;
                if (txtFullName != null) fullName = txtFullName.Text; if (txtJobTitle != null) jobTitle = txtJobTitle.Text;
                if (txtEmail != null) eMail = txtEmail.Text; if (txtMobileCC != null) mobileCC = txtMobileCC.Text; if (txtMobile != null) mobile = txtMobile.Text;
                regMember.GroupID = groupID;
                regMember.Sal = cFun.solveSQL(sal.Trim());
                regMember.Salother = cFun.solveSQL(salOther.Trim());
                regMember.FName = cFun.solveSQL(fName.Trim());
                regMember.LName = cFun.solveSQL(lName.Trim());
                regMember.FullName = cFun.solveSQL(fullName.Trim());
                regMember.JobTitle = cFun.solveSQL(jobTitle.Trim());
                regMember.Email = cFun.solveSQL(eMail.Trim());
                regMember.MobileCC = cFun.solveSQL(mobileCC.Trim());
                regMember.Mobile = cFun.solveSQL(mobile.Trim());
                regMember.Company = cFun.solveSQL(txtCompany.Text.Trim());
                regMember.Address = cFun.solveSQL(txtComAdd.Text.Trim());
                regMember.Address2 = cFun.solveSQL(txtComAdd2.Text.Trim());
                regMember.Website = cFun.solveSQL(txtComWebsite.Text.Trim());
                regMember.Country = cFun.solveSQL(ddlComCountry.Text.Trim());
                regMember.City = cFun.solveSQL(txtComCity.Text.Trim());
                regMember.State = cFun.solveSQL(txtComState.Text.Trim());
                regMember.PostalCode = cFun.solveSQL(txtComPostCode.Text.Trim());
                regMember.TelCC = cFun.solveSQL(txtComTelCC.Text.Trim());
                regMember.TelAC = cFun.solveSQL(txtComTelAC.Text.Trim());
                regMember.Tel = cFun.solveSQL(txtComTel.Text.Trim());
                regMember.QA1 = cFun.solveSQL(ddlProfession.SelectedValue);
                regMember.QA2 = cFun.solveSQL(ddlDepartment.SelectedValue);
                regMember.QA3 = cFun.solveSQL(ddlOrganization.SelectedValue);
                regMember.QA2Other = cFun.solveSQL(txtDepartmentOther.Text);
                regMember.QA3Other = cFun.solveSQL(txtOrgOther.Text.Trim());
                regMember.DateofVisit = ddlVisitDate.SelectedValue;
                regMember.QA4 = ddlAffiliation.SelectedValue;
                regno = SaveMember(regMember, isFristRecord, firstMemberRegno);
                if (isFristRecord) firstMemberRegno = regno;
                isFristRecord = false;
                if (string.IsNullOrEmpty(regno))
                {
                    isOK = false;
                    break;
                }
            }
            currentIndx++;
        }
        return isOK;
    }

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion
    #region isValidPage//***added on 25-6-2018
    protected Boolean isValidPage(string showid, FlowURLQuery urlQuery)
    {
        Boolean isvalid = true;

        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = UpdatePanel1.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = UpdatePanel1.FindControl("chk" + ftnObj.note_Type);
                                Control ctrlLbl = UpdatePanel1.FindControl("lblErr" + ftnObj.note_Type);
                                if (ctrlChk != null && ctrlLbl != null)
                                {
                                    CheckBoxList chkNote = UpdatePanel1.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    Label lblErrNote = UpdatePanel1.FindControl("lblErr" + ftnObj.note_Type) as Label;
                                    if (divFooter.Visible == true)
                                    {
                                        int countTerms = chkNote.Items.Count;
                                        if (countTerms > 0)
                                        {
                                            lblErrNote.Visible = false;
                                            string id = ftnObj.note_ID;
                                            int isSkip = 0;
                                            if (ftnObj != null)
                                            {
                                                isSkip = ftnObj.note_isSkip;
                                            }
                                            ListItem liItem = chkNote.Items.FindByValue(id);
                                            if (liItem != null)
                                            {
                                                if (isSkip == 0)
                                                {
                                                    if (liItem.Selected == false)
                                                    {
                                                        lblErrNote.Visible = true;
                                                        isvalid = false;
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('Please accept Registration policy.');</script>", false);
                                                        return isvalid;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return isvalid;
    }
    #endregion

    #region bindFlowNote//***added on 25-6-2018
    private void bindFlowNote(string showid, FlowURLQuery urlQuery)
    {
        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = UpdatePanel1.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            divFooter.Visible = true;
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = UpdatePanel1.FindControl("chk" + ftnObj.note_Type);
                                if (ctrlChk != null)
                                {
                                    CheckBoxList chkFTRCHK = UpdatePanel1.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    ListItem newItem = new ListItem(displayTextTmpt, ftnObj.note_ID);
                                    chkFTRCHK.Items.Add(newItem);
                                }
                            }
                        }
                        else
                        {
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = UpdatePanel1.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    protected bool setDynamicForm(string flowid, string showid)
    {
        bool isValidShow = false;

        DataSet ds = new DataSet();
        FormManageObj frmObj = new FormManageObj(fn);
        frmObj.showID = showid;
        frmObj.flowID = flowid;
        ds = frmObj.getDynFormForDelegate();

        int isVisitorVisible = 0;
        int isUDFVisible = 0;
        int isSupervisorVisible = 0;

        string formtype = FormType.TypeDelegate;

        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
        {

            #region set divProfession visibility is true or false if form_input_name is Profession according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Profession)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
 

                if (isshow == 1)
                {
                    divProfession.Visible = true;
                }
                else
                {
                    divProfession.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Profession, formtype);
                if (isrequired == 1)
                {
                    lblProfession.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; "; ;
                    //ddlProfession.Attributes.Add("required", "");
                    vcProfession.Enabled = true;
                    vcProfession.ErrorMessage = MakeRequireText(labelname.Replace("<br/>", ""));
                }
                else
                {
                    lblProfession.Text = labelname;
                    //ddlProfession.Attributes.Remove("required");
                    vcProfession.Enabled = false;
                }
            }
            #endregion


            #region set divDepartment visibility is true or false if form_input_name is Department according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Department)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divDepartment.Visible = true;
                }
                else
                {
                    divDepartment.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Department, formtype);
                if (isrequired == 1)
                {
                    lblDepartment.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; "; ;
                    //ddlDepartment.Attributes.Add("required", "");
                    vcDeptm.Enabled = true;
                    vcDeptm.ErrorMessage = MakeRequireText(labelname.Replace("<br/>", ""));
                }
                else
                {
                    lblDepartment.Text = labelname;
                    //ddlDepartment.Attributes.Remove("required");
                    vcDeptm.Enabled = false;
                }
            }
            #endregion


            #region set divOrganization visibility is true or false if form_input_name is Company according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Organization)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divOrganization.Visible = true;
                }
                else
                {
                    divOrganization.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Organization, formtype);
                if (isrequired == 1)
                {
                    lblOrganization.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; "; ;
                    //ddlOrganization.Attributes.Add("required", "");
                    vcOrg.Enabled = true;
                    vcOrg.ErrorMessage = MakeRequireText(labelname.Replace("<br/>", ""));
                }
                else
                {
                    lblOrganization.Text = labelname;
                    //ddlOrganization.Attributes.Remove("required");
                    vcOrg.Enabled = false;
                }
            }
            #endregion

            #region set divAffiliation visibility is true or false if form_input_name is Affiliation according to form_input_isshow is true or false & set validation according to form_input_isrequired is true or false
            if (ds.Tables[0].Rows[x]["form_input_name"].ToString().Trim() == _Affiliation)
            {
                int isshow = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isshow"]);
                int isrequired = Convert.ToInt16(ds.Tables[0].Rows[x]["form_input_isrequired"]);
                if (isshow == 1)
                {
                    divAffiliation.Visible = true;
                }
                else
                {
                    divAffiliation.Visible = false;
                }

                string labelname = !string.IsNullOrEmpty(ds.Tables[0].Rows[x]["form_input_text"].ToString()) ? ds.Tables[0].Rows[x]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_Affiliation, formtype);
                if (isrequired == 1)
                {
                    lblAffiliation.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; "; ;
                    //ddlAffiliation.Attributes.Add("required", "");
                    vcAffil.Enabled = true;
                }
                else
                {
                    lblAffiliation.Text = labelname;
                    //ddlAffiliation.Attributes.Remove("required");
                    vcAffil.Enabled = false;
                }
            }
            #endregion
        }

        return true;
    }
    private string MakeRequireText(string lableText)
    {

        return string.Format("* {0} Required", lableText.ToString());
    }

    private bool DuplicateCheckForAllMember()
    {
        bool hasDuplicate = true;
        try
        {
            int totalMember = cFun.ParseInt(ddlTotalMember.SelectedValue);
            string cPreFix = "";
            int currentIndx = 1;
            while (currentIndx <= totalMember)
            {
                Panel pContainer = (Panel)UpdatePanel1.FindControl("Panel" + currentIndx.ToString());
                if (pContainer != null)
                {
                    cPreFix = "txt" + currentIndx.ToString(); 
                    TextBox txtLName = (TextBox)pContainer.FindControl(cPreFix + "LName");
                    TextBox txtFName = (TextBox)pContainer.FindControl(cPreFix + "FName"); 
                    TextBox txtEmail = (TextBox)pContainer.FindControl(cPreFix + "Email"); 
                }
            }
                }
        catch { }

        return hasDuplicate;
    }
    private bool DuplicateCheck(FlowMaster flwMaster, RegDelegateObj rgd)
    {

        bool isAlreadyExist = false;

        if (flwMaster.isDuplicateChecking == 1)//***check in complete registration status
        {
            isAlreadyExist = rgd.checkInsertExist();
        }
        else if (flwMaster.isDuplicateChecking == 2)//***check in all(complete,pending) registration status
        {
            isAlreadyExist = rgd.checkInsertExistWithoutRegStatus();
        }
        else if (flwMaster.isDuplicateChecking == 3)//***check in all(complete,pending) registration status
        {
            isAlreadyExist = rgd.checkInsertExistWithFLowID(flwMaster.FlowID);
        } 

   
        return isAlreadyExist;
    }
}
public class RegMdAMember
{
    public string GroupID { get; set; } = "";
    public string Regno { get; set; } = "";
    public string Sal { get; set; } = "";
    public string Salother { get; set; } = "";
    public string FName { get; set; } = "";
    public string LName { get; set; } = "";
    public string FullName { get; set; } = "";
    public string JobTitle { get; set; } = "";
    public string Email { get; set; } = "";
    public string MobileCC { get; set; } = "";
    public string Mobile { get; set; } = "";
    public string Company { get; set; } = "";
    public string Website { get; set; } = "";
    public string Address { get; set; } = "";
    public string Address2 { get; set; } = "";
    public string Country { get; set; } = "";
    public string City { get; set; } = "";
    public string State { get; set; } = "";
    public string PostalCode { get; set; } = "";
    public string TelCC { get; set; } = "";
    public string TelAC { get; set; } = "";
    public string Tel { get; set; } = "";

    public string QA1 { get; set; } = "";
    public string QA1Other { get; set; } = "";
    public string QA2 { get; set; } = "";
    public string QA2Other { get; set; } = "";
    public string QA3 { get; set; } = "";
    public string QA3Other { get; set; } = "";
    public string DateofVisit { get; set; } = "";

    public string QA4 { get; set; } = "";
}