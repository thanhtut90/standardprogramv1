﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RegistrationOnsite.master" AutoEventWireup="true" CodeFile="RegDelegateOnsiteSSBFEEdit.aspx.cs" Inherits="RegDelegateOnsiteSSBFEEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .red
        {
            color:red;
        }
        /*.chkFooter
        {
            white-space:nowrap;
        }*/
        .chkFooter label
        {
            font-weight:unset !important;
            padding-left:5px !important;
            vertical-align:top !important;
        }
        .chkFooter input[type="checkbox"]
        {
            margin-top: 2px !important;
        }
        a:hover
        {
            text-decoration:none !important;
        }
        .rowLabel
        {
            padding-top: 7px;
        }
    </style>

    <script type="text/javascript">
        function validateDate(elementRef) {
                var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

                var dateValue = elementRef.value;

                if (dateValue.length != 10) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                // mm/dd/yyyy format... 
                var valueArray = dateValue.split('/');

                if (valueArray.length != 3) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthValue = parseFloat(valueArray[1]);
                var dayValue = parseFloat(valueArray[0]);
                var yearValue = parseFloat(valueArray[2]);

                if ((isNaN(monthValue) == true) || (isNaN(dayValue) == true) || (isNaN(yearValue) == true)) {
                    alert('Non-numeric entry detected\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                if (((yearValue % 4) == 0) && (((yearValue % 100) != 0) || ((yearValue % 400) == 0)))
                    monthDays[1] = 29;
                else
                    monthDays[1] = 28;

                if ((monthValue < 1) || (monthValue > 12)) {
                    alert('Invalid month entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthDaysArrayIndex = monthValue - 1;
                if ((dayValue < 1) || (dayValue > monthDays[monthDaysArrayIndex])) {
                    alert('Invalid day entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

            return true;
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
        <div class="col-md-3 col-md-offset-1 rowLabel">First Name*</div>
        <div class="col-md-4">
             <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-md-2">
            <asp:RequiredFieldValidator ID="vcFName" runat="server"
                    ControlToValidate="txtFName" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revFName" runat="server" ControlToValidate="txtFName" ErrorMessage="Not allow."
            ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
        </div>
    </div>
    <br />
    <br />
    <div class="row">
        <div class="col-md-3 col-md-offset-1 rowLabel">Last Name*</div>
        <div class="col-md-4">
            <asp:TextBox ID="txtLName" runat="server" CssClass="form-control" AutoCompleteType="Disabled"></asp:TextBox>
        </div>
        <div class="col-md-2">
            <asp:RequiredFieldValidator ID="vcLName" runat="server"
                    ControlToValidate="txtLName" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revLName" runat="server" ControlToValidate="txtLName" ErrorMessage="Not allow."
            ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
        </div>
    </div>

    <div class="form-group">
        <div class="form-group container" style="padding-top:20px;">
            <asp:Panel runat="server" ID="PanelWithoutBack" Visible="true" >
                <div class="col-lg-offset-5 col-lg-3 col-sm-offset-4 col-sm-3 center-block" >
                    <asp:Button runat="server" ID="btnSave" CssClass="btn MainButton btn-block" Text="Next" OnClick="btnSave_Click" />
                </div>
            </asp:Panel>
        </div>
    </div>

</asp:Content>

