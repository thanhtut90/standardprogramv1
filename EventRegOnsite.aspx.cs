﻿using Corpit.BackendMaster;
using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EventRegOnsite : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    static string onsiteRegNoTableField = "Regno";
    private static string[] fullNameList = new string[] { "name", "name on badge", "name to be printed", "name to be displayed on badge"
                                                            , "name on conference badge", "preferred name On badge", "name on badge / 证章名称" };
    private static string[] companyNameList = new string[] { "company", "company 公司名称", "company บริษัท", "company name", "company name ", "company (会社名)"
                                                                , "company/corporation"
                                                                , "organization", "name of organisation", "organisation type" };
    private static string[] jobNameList = new string[] { "job title", "job title  (役職)", "job position 职位", "job position", "designation" };
    static string _TypeFullName = "TypeFullName";
    static string _TypeCompanyOrg = "TypeCompanyOrg";
    static string _TypeJob = "TypeJob";

    static string _Salutation = "Salutation";
    static string _Fname = "FName";
    static string _Lname = "LName";
    static string _OName = "Oname";
    static string _PassNo = "PassportNo";
    static string _isReg = "isRegistered";
    static string _regSpecific = "RegSpecific";
    static string _IDNo = "IDNo";
    static string _Designation = "Designation";
    static string _Profession = "Profession";
    static string _Department = "Department";
    static string _Organization = "Organization";
    static string _Institution = "Institution";
    static string _Address1 = "Address1";
    static string _Address2 = "Address2";
    static string _Address3 = "Address3";
    static string _Address4 = "Address4";
    static string _City = "City";
    static string _State = "State";
    static string _PostalCode = "Postal Code";
    static string _Country = "Country";
    static string _RCountry = "RCountry";
    static string _Telcc = "Telcc";
    static string _Telac = "Telac";
    static string _Tel = "Tel";
    static string _Mobilecc = "Mobilecc";
    static string _Mobileac = "Mobileac";
    static string _Mobile = "Mobile";
    static string _Faxcc = "Faxcc";
    static string _Faxac = "Faxac";
    static string _Fax = "Fax";
    static string _Email = "Email";
    static string _EmailConfirmation = "Email Confirmation";
    static string _Affiliation = "Affiliation";
    static string _Dietary = "Dietary";
    static string _Nationality = "Nationality";
    static string _MembershipNo = "Membership No";

    static string _VName = "VName";
    static string _VDOB = "VDOB";
    static string _VPassNo = "VPassNo";
    static string _VPassExpiry = "VPassExpiry";
    static string _VPassIssueDate = "VPassIssueDate";
    static string _VEmbarkation = "VEmbarkation";
    static string _VArrivalDate = "VArrivalDate";
    static string _VCountry = "VCountry";

    static string _UDF_CName = "UDF_CName";
    static string _UDF_DelegateType = "UDF_DelegateType";
    static string _UDF_ProfCategory = "UDF_ProfCategory";
    static string _UDF_CPcode = "UDF_CPcode";
    static string _UDF_CLDepartment = "UDF_CLDepartment";
    static string _UDF_CAddress = "UDF_CAddress";
    static string _UDF_CLCompany = "UDF_CLCompany";
    static string _UDF_CCountry = "UDF_CCountry";
    static string _UDF_ProfCategroyOther = "UDF_ProfCategroyOther";
    static string _UDF_CLCompanyOther = "UDF_CLCompanyOther";

    static string _SupName = "Supervisor Name";
    static string _SupDesignation = "Supervisor Designation";
    static string _SupContact = "Supervisor Contact";
    static string _SupEmail = "Supervisor Email";

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _Age = "Age";
    static string _Gender = "Gender";
    static string _DOB = "DOB";
    static string _Additional4 = "Additional4";
    static string _Additional5 = "Additional5";
    private static string[] checkingFJShowName = new string[] { "Food Japan" };
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.Params["reg"] != null && Request.Params["shw"] != null)
            {
                string onsiteRegno = cFun.DecryptValue(Request.QueryString["reg"].ToString());
                string showID = cFun.DecryptValue(Request.QueryString["shw"].ToString());
                BrowseNext(onsiteRegno, showID);
            }
            else
            {
                Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
            }
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        //SiteBanner.ImageUrl = sSetting.SiteBanner;
    }
    #endregion

    #region BrowseNext
    private void BrowseNext(string onsiteRegno, string showid)
    {
        if (!string.IsNullOrEmpty(onsiteRegno))
        {
            string regno = string.Empty;
            DataTable dtOnsite = new DataTable();
            bool isOnsiteExist = checkOnsiteExist(onsiteRegNoTableField, onsiteRegno, showid, ref dtOnsite, ref regno);
            if (isOnsiteExist && dtOnsite.Rows.Count > 0)
            {
                string showID = dtOnsite.Rows[0]["ShowID"].ToString();
                string FlowID = dtOnsite.Rows[0]["reg_urlFlowID"] != DBNull.Value ? dtOnsite.Rows[0]["reg_urlFlowID"].ToString() : "";
                if (!string.IsNullOrEmpty(showID))
                {
                    ShowController shwCtrl = new ShowController(fn);
                    if (shwCtrl.checkValidShow(showID))
                    {
                        FlowControler Flw = new FlowControler(fn);
                        FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(FlowID);
                        if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                        {
                            bool isSuccess = false;
                            string grpNum = "";
                            isSuccess = registrationChecking(showID, onsiteRegno, dtOnsite, FlowID, ref regno, ref grpNum);
                            if (!isSuccess)
                            {
                                regno = "";
                                grpNum = "";
                            }

                            string page = "";
                            string step = "";
                            if (string.IsNullOrEmpty(regno))
                            {
                                Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
                            }
                            else
                            {
                                string route = "";
                                {
                                    Dictionary<string, string> nValues = Flw.GetNextRoute(FlowID);
                                    if (nValues.Count > 0)
                                    {
                                        page = nValues["nURL"].ToString();
                                        step = nValues["nStep"].ToString();
                                        FlowID = nValues["FlowID"].ToString();
                                    }
                                    route = Flw.MakeFullURL(page, FlowID, showID, cFun.EncryptValue(grpNum), step, cFun.EncryptValue(regno));
                                    Response.Redirect(route);
                                }
                            }
                        }
                        else
                        {
                            Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
                    }
                }
                else
                {
                    Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
            }
        }
    }
    #endregion

    #region registrationChecking
    private bool registrationChecking(string showid, string onsiteRegno, DataTable dtOnsite, string FlowID, ref string regno, ref string groupid)
    {
        bool isSuccess = false;
        try
        {
            if (!string.IsNullOrEmpty(regno))
            {
                DataTable dtReg = getRegDelegateByIDFlowID(regno, showid, FlowID);
                groupid = dtReg.Rows[0]["RegGroupID"].ToString();
                isSuccess = true;
            }
            else
            {
                int isSaved = insertRegDelegateData(showid, dtOnsite, ref regno, ref groupid);
                if (isSaved > 0)
                {
                    updateRefRegno(onsiteRegNoTableField, onsiteRegno, showid, regno);
                    isSuccess = true;
                }
            }
        }
        catch (Exception ex)
        {
            isSuccess = false;
        }

        return isSuccess;
    }
    #endregion

    #region insertRegDelegateData
    private int insertRegDelegateData(string showid, DataTable dtOnsite, ref string regno, ref string groupid)
    {
        int isSuccess = 0;
        try
        {
            #region create delegate in tb_RegDelegate
            RegDelegateObj rgd = new RegDelegateObj(fn);
            DataTable dt = rgd.getRegDelegateByID(regno, showid);//rgd.getRegDelegateByRegnoEmail(regno, preEmailAddress);
            if (dt.Rows.Count == 0)
            {
                //create group in tb_RegGroup
                RegGroupObj rgp = new RegGroupObj(fn);
                groupid = rgp.GenGroupNumber(showid);// GetRunNUmber("GRP_KEY");
                rgp.createRegGroupIDOnly(groupid, 0, showid);

                regno = rgd.GenDelegateNumber(showid);

                SetUpController setCtrl = new SetUpController(fn);
                rgd.groupid = groupid;
                rgd.regno = regno;
                rgd.con_categoryID = 0;

                string salValue = dtOnsite.Rows[0]["reg_Salutation"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Salutation"].ToString() : "";
                string salutation = string.Empty;
                if (isDigitNumber(salValue))
                {
                    salutation = getSalutationIDByName(salValue, showid);
                }
                else
                {
                    salutation = salValue;
                }
                rgd.salutation = salutation;
                rgd.fname = dtOnsite.Rows[0]["reg_FName"] != DBNull.Value ? dtOnsite.Rows[0]["reg_FName"].ToString() : "";
                rgd.lname = dtOnsite.Rows[0]["reg_LName"] != DBNull.Value ? dtOnsite.Rows[0]["reg_LName"].ToString() : "";
                rgd.oname = "";
                rgd.passno = "";
                rgd.isreg = 0;
                rgd.regspecific = "";
                rgd.idno = "";
                rgd.staffid = "";
                rgd.designation = dtOnsite.Rows[0]["reg_Designation"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Designation"].ToString() : "";
                rgd.jobtitle = "";
                rgd.profession = "";
                rgd.department = "";
                rgd.organization = "";
                rgd.institution = "";
                rgd.address1 = dtOnsite.Rows[0]["reg_Organization"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Organization"].ToString() : "";
                rgd.address2 = "";
                rgd.address3 = "";
                rgd.address4 = "";
                rgd.city = "";
                rgd.state = "";
                rgd.postalcode = "";

                string country = dtOnsite.Rows[0]["reg_Country"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Country"].ToString() : "";
                if (isDigitNumber(country))
                {
                    rgd.country = country;
                }
                else
                {
                    CountryObj countryObj = new CountryObj(fn);
                    string countryID = countryObj.getCountryIDByName(country, showid);
                    rgd.country = countryID;
                }

                rgd.rcountry = "";
                rgd.telcc = "";
                rgd.telac = "";
                rgd.tel = "";
                rgd.mobilecc = dtOnsite.Rows[0]["reg_Mobcc"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Mobcc"].ToString() : "";
                rgd.mobileac = dtOnsite.Rows[0]["reg_Mobac"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Mobac"].ToString() : "";
                rgd.mobile = dtOnsite.Rows[0]["reg_Mobile"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Mobile"].ToString() : "";
                rgd.faxcc = "";
                rgd.faxac = "";
                rgd.fax = "";
                rgd.email = dtOnsite.Rows[0]["reg_Email"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Email"].ToString() : "";
                rgd.affiliation = "";
                rgd.dietary = "";
                rgd.nationality = "";
                rgd.age = 0;
                rgd.dob = "";
                rgd.gender = "";
                rgd.additional4 = "";
                rgd.additional5 = dtOnsite.Rows[0]["reg_Additional5"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Additional5"].ToString() : ""; 
                rgd.memberno = "";
                rgd.vname = "";
                rgd.vdob = "";
                rgd.vpassno = "";
                rgd.vpassexpiry = "";
                rgd.vpassissuedate = "";
                rgd.vembarkation = "";
                rgd.varrivaldate = "";
                rgd.vcountry = "";
                rgd.udfcname = "";
                rgd.udfdeltype = "";
                rgd.udfprofcat = "";
                rgd.udfprofcatother = "";
                rgd.udfcpcode = "";
                rgd.udfcldept = "";
                rgd.udfcaddress = "";
                rgd.udfclcompany = "";
                rgd.udfclcompanyother = "";
                rgd.udfccountry = "";
                rgd.supname = "";
                rgd.supdesignation = "";
                rgd.supcontact = "";
                rgd.supemail = "";
                rgd.othersal = dtOnsite.Rows[0]["reg_SalutationOthers"] != DBNull.Value ? dtOnsite.Rows[0]["reg_SalutationOthers"].ToString() : "";
                rgd.otherprof = "";
                rgd.otherdept = "";
                rgd.otherorg = "";
                rgd.otherinstitution = "";
                rgd.aemail = "";
                rgd.isSMS = 0;
                rgd.remark = "";
                rgd.remark_groupupload = "";
                rgd.approvestatus = 0;
                rgd.createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                rgd.recycle = 0;
                rgd.stage = "1";

                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);
                if (checkingFJShowName.Contains(shw.SHW_Name))
                {
                    #region FJ
                    //////*Business Category => reg_profession(online indiv pre-reg) = reg_Designation(onsite indiv)
                    string designation = dtOnsite.Rows[0]["reg_Designation"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Designation"].ToString() : "";
                    if (isDigitNumber(designation))
                    {
                        rgd.profession = designation;//*designation = professionID
                    }
                    else
                    {
                        string profID = setCtrl.getProfessionIDByName(designation, showid);
                        rgd.profession = profID;
                    }

                    //////*Main Job Function => reg_Organization(online indiv pre-reg) = reg_Organization(onsite indiv)
                    string companyorg = dtOnsite.Rows[0]["reg_Organization"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Organization"].ToString() : "";
                    if (isDigitNumber(companyorg))
                    {
                        rgd.organization = companyorg;//*companyorg = organizationID
                    }
                    else
                    {
                        string orgID = setCtrl.getOrganisationIDByName(companyorg, showid);
                        rgd.organization = orgID;
                    }
                    #endregion
                }
                else
                {
                    #region normal flow
                    string fullName = dtOnsite.Rows[0]["reg_OName"] != DBNull.Value ? dtOnsite.Rows[0]["reg_OName"].ToString() : "";
                    matchColumn(_TypeFullName, fullName, showid, ref rgd);
                    string designation = dtOnsite.Rows[0]["reg_Designation"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Designation"].ToString() : "";
                    matchColumn(_TypeJob, designation, showid, ref rgd);
                    string companyorg = dtOnsite.Rows[0]["reg_Organization"] != DBNull.Value ? dtOnsite.Rows[0]["reg_Organization"].ToString() : "";
                    matchColumn(_TypeCompanyOrg, companyorg, showid, ref rgd);
                    #endregion
                }

                rgd.showID = showid;
                isSuccess = rgd.saveRegDelegate();
            }
            else
            {
                isSuccess = 1;
                groupid = dt.Rows[0]["RegGroupID"].ToString();
            }
            #endregion
        }
        catch (Exception ex)
        { }

        return isSuccess;
    }
    private bool isDigitNumber(string input)
    {
        bool isDigit = false;
        try
        {
            isDigit = Regex.IsMatch(input, @"^\d+$");
        }
        catch { }
        return isDigit;
    }
    public string getSalutationIDByName(string salname, string showid)
    {
        string salID = string.Empty;

        try
        {
            string query = string.Format("Select Sal_ID From ref_Salutation Where Sal_Name Like N'{0}' And ShowID='{1}'"
                , salname, showid);
            salID = fn.GetDataByCommand(query, "Sal_ID");
            if (salID == "0")
            {
                salID = "";
            }
        }
        catch (Exception ex)
        { }

        return salID;
    }
    public DataTable getRegDelegateByIDFlowID(string regno, string showid, string flowid)
    {
        DataTable dtDelegate = new DataTable();

        try
        {
            string query = string.Format("Select * From tb_RegDelegate Where recycle=0 And Regno='{0}' And ShowID=@SHWID And reg_urlFlowID=@reg_urlFlowID", regno);
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar = new SqlParameter("SHWID", SqlDbType.NVarChar);
            SqlParameter spar1 = new SqlParameter("reg_urlFlowID", SqlDbType.NVarChar);
            spar.Value = showid;
            spar1.Value = flowid;
            pList.Add(spar);
            pList.Add(spar1);

            dtDelegate = fn.GetDatasetByCommand(query, "ds", pList).Tables[0];
        }
        catch { }

        return dtDelegate;
    }
    #endregion

    #region RegOnsite
    private bool checkOnsiteExist(string columnName, string columnValue, string showid, ref DataTable dtOnsite, ref string refRegno)
    {
        bool isExist = false;
        try
        {
            //  string sql = "Select * From tb_RegDelegateOnsite Where recycle=0 And ShowID=@ShowID And @columnName=@columnValue";
            string sql = "Select * From tb_RegDelegateOnsite Where recycle=0 And ShowID=@ShowID And Regno=@columnValue";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("columnName", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("columnValue", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = columnName;
            spar3.Value = columnValue;
            pList.Add(spar1);
           // pList.Add(spar2);
            pList.Add(spar3);
            DataTable dt = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
            if (dt.Rows.Count > 0)
            {
                dtOnsite = dt;
                refRegno = dt.Rows[0]["reg_Additional4"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_Additional4"].ToString()) ? dt.Rows[0]["reg_Additional4"].ToString() : "") : "";
                isExist = true;
            }
        }
        catch (Exception ex)
        { }

        return isExist;
    }
    private void updateRefRegno(string columnName, string columnValue, string showid, string refRegno)
    {
        try
        {
            string sql = "Update tb_RegDelegateOnsite Set reg_Additional4='" + refRegno + "' Where recycle=0 And ShowID='" + showid + "' And " + columnName + "='" + columnValue + "'";
            //List<SqlParameter> pList = new List<SqlParameter>();
            //SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            //SqlParameter spar2 = new SqlParameter("columnName", SqlDbType.NVarChar);
            //SqlParameter spar3 = new SqlParameter("columnValue", SqlDbType.NVarChar);
            //SqlParameter spar4 = new SqlParameter("reg_Additional4", SqlDbType.NVarChar);
            //spar1.Value = showid;
            //spar2.Value = columnName;
            //spar3.Value = columnValue;
            //spar4.Value = refRegno;
            //pList.Add(spar1);
            //pList.Add(spar2);
            //pList.Add(spar3);
            //pList.Add(spar4);
            //fn.ExecuteSQLWithParameters(sql, pList);
            fn.ExecuteSQL(sql);
        }
        catch(Exception ex)
        { }
    }
    #endregion

    #region match column
    private void matchColumn(string checkingColumnType, string fieldValue, string showid, ref RegDelegateObj rgd)
    {
        try
        {
            string sql = "Select * From tb_Form Where ShowID=@ShowID And form_type=@form_type And form_input_isshow=1";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("form_type", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = FormType.TypeDelegate;
            pList.Add(spar1);
            pList.Add(spar2);
            DataTable dtForm = fn.GetDatasetByCommand(sql, "dsForm", pList).Tables[0];
            if (dtForm.Rows.Count > 0)
            {
                foreach (DataRow dr in dtForm.Rows)
                {
                    string formText = dr["form_input_text"].ToString().ToLower();
                    string formInputName = dr["form_input_name"].ToString().ToLower();
                    if (checkingColumnType == _TypeFullName)
                    {
                        if (fullNameList.Contains(formText))
                        {
                            matchField(dr["form_input_name"].ToString(), fieldValue, ref rgd);
                        }
                    }
                    else if (checkingColumnType == _TypeCompanyOrg)
                    {
                        if (companyNameList.Contains(formText))
                        {
                            matchField(dr["form_input_name"].ToString(), fieldValue, ref rgd);
                        }
                    }
                    else if (checkingColumnType == _TypeJob)
                    {
                        if (jobNameList.Contains(formText))
                        {
                            matchField(dr["form_input_name"].ToString(), fieldValue, ref rgd);
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        { }
    }
    private void matchField(string formInputName, string fieldValue, ref RegDelegateObj rgd)
    {
        try
        {
            #region comment
            //if (formInputName == _Salutation)
            //{
            //    rgd.salutation = fieldValue;
            //}
            //if (formInputName == _Fname)
            //{
            //    rgd.fname = fieldValue;
            //}
            //if (formInputName == _Lname)
            //{
            //    rgd.lname = fieldValue;
            //}
            #endregion

            if (formInputName == _OName)
            {
                rgd.oname = fieldValue;
            }
            if (formInputName == _PassNo)
            {
                rgd.passno = fieldValue;
            }
            #region comment
            //if (formInputName == _isReg)
            //{
            //    rgd.isreg = cFun.ParseInt(fieldValue);
            //}
            //if (formInputName == _regSpecific)
            //{
            //    rgd.regspecific = fieldValue;
            //}
            #endregion
            if (formInputName == _IDNo)//MCR/SNB/PRN No.
            {
                rgd.idno = fieldValue;
            }
            if (formInputName == _Designation)
            {
                rgd.designation = fieldValue;
            }
            if (formInputName == _Profession)
            {
                rgd.profession = fieldValue;
            }
            if (formInputName == _Department)
            {
                rgd.department = fieldValue;
            }
            if (formInputName == _Organization)
            {
                rgd.organization = fieldValue;
            }
            if (formInputName == _Institution)
            {
                rgd.institution = fieldValue;
            }
            if (formInputName == _Address1)
            {
                rgd.address1 = fieldValue;
            }
            if (formInputName == _Address2)
            {
                rgd.address2 = fieldValue;
            }
            if (formInputName == _Address3)
            {
                rgd.address3 = fieldValue;
            }
            if (formInputName == _Address4)
            {
                rgd.address4 = fieldValue;
            }
            if (formInputName == _City)
            {
                rgd.city = fieldValue;
            }
            if (formInputName == _State)
            {
                rgd.state = fieldValue;
            }
            if (formInputName == _PostalCode)
            {
                rgd.postalcode = fieldValue;
            }
            if (formInputName == _Country)
            {
                rgd.country = fieldValue;
            }
            if (formInputName == _RCountry)
            {
                rgd.rcountry = fieldValue;
            }
            if (formInputName == _Telcc)
            {
                rgd.telcc = fieldValue;
            }
            if (formInputName == _Telac)
            {
                rgd.telac = fieldValue;
            }
            if (formInputName == _Tel)
            {
                rgd.tel = fieldValue;
            }
            if (formInputName == _Faxcc)
            {
                rgd.faxcc = fieldValue;
            }
            if (formInputName == _Faxac)
            {
                rgd.faxac = fieldValue;
            }
            if (formInputName == _Fax)
            {
                rgd.fax = fieldValue;
            }
            if (formInputName == _Affiliation)
            {
                rgd.affiliation = fieldValue;
            }
            if (formInputName == _Dietary)
            {
                rgd.dietary = fieldValue;
            }
            if (formInputName == _Nationality)
            {
                rgd.nationality = fieldValue;
            }
            if (formInputName == _Age)
            {
                rgd.age = cFun.ParseInt(fieldValue);
            }
            if (formInputName == _DOB)
            {
                rgd.dob = fieldValue;
            }
            if (formInputName == _Gender)
            {
                rgd.gender = fieldValue;
            }
            if (formInputName == _MembershipNo)
            {
                rgd.memberno = fieldValue;
            }
            if (formInputName == _Additional4)
            {
                rgd.additional4 = fieldValue;
            }
            if (formInputName == _Additional5)
            {
                rgd.additional5 = fieldValue;
            }
            if (formInputName == _VName)
            {
                rgd.vname = fieldValue;
            }
            if (formInputName == _VDOB)
            {
                rgd.vdob = fieldValue;
            }
            if (formInputName == _VPassNo)
            {
                rgd.vpassno = fieldValue;
            }
            if (formInputName == _VPassExpiry)
            {
                rgd.vpassexpiry = fieldValue;
            }
            if (formInputName == _VPassIssueDate)
            {
                rgd.vpassissuedate = fieldValue;
            }
            if (formInputName == _VEmbarkation)
            {
                rgd.vembarkation = fieldValue;
            }
            if (formInputName == _VArrivalDate)
            {
                rgd.varrivaldate = fieldValue;
            }
            if (formInputName == _VCountry)
            {
                rgd.vcountry = fieldValue;
            }
            if (formInputName == _UDF_CName)
            {
                rgd.udfcname = fieldValue;
            }
            if (formInputName == _UDF_DelegateType)
            {
                rgd.udfdeltype = fieldValue;
            }
            if (formInputName == _UDF_ProfCategory)
            {
                rgd.udfprofcat = fieldValue;
            }
            if (formInputName == _UDF_CPcode)
            {
                rgd.udfcpcode = fieldValue;
            }
            if (formInputName == _UDF_CLDepartment)
            {
                rgd.udfcldept = fieldValue;
            }
            if (formInputName == _UDF_CAddress)
            {
                rgd.udfcaddress = fieldValue;
            }
            if (formInputName == _UDF_CLCompany)
            {
                rgd.udfclcompany = fieldValue;
            }
            if (formInputName == _UDF_CCountry)
            {
                rgd.udfccountry = fieldValue;
            }
            if (formInputName == _SupName)
            {
                rgd.supname = fieldValue;
            }
            if (formInputName == _SupDesignation)
            {
                rgd.supdesignation = fieldValue;
            }
            if (formInputName == _SupContact)
            {
                rgd.supcontact = fieldValue;
            }
            if (formInputName == _SupEmail)
            {
                rgd.supemail = fieldValue;
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion
}