﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="SuccessM.aspx.cs" Inherits="SuccessM" %>
<%@ MasterType VirtualPath="~/Registration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel runat="server" ID="PanelMsg">
        <p style="padding-bottom:30px;">
            <asp:Label ID="lblSuccessMessage" runat="server"></asp:Label>
        </p>
        <div class="form-group" id="divRegisterAnotherShow" runat="server" visible="false" >
            <div class="form-group row">
                <div class="col-md-1">
                    &nbsp;
                </div>
                <div class="col-md-3">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td>
                                    <span>
                                        <asp:Button runat="server" ID="btnRegisterAnotherShow" CssClass="btn MainButton btn-block" 
                                        Enabled="false"
                                        Text="" OnClick="btnRegisterAnotherShow_Click" />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-4"style="padding-left: 80px;">
                    <a id="btnReturnWebsite" runat="server" class="btn MainButton btn-block">Return to Event Website</a>
                    <%--<asp:LinkButton runat="server" ID="btnReturnWebsite" CssClass="btn MainButton btn-block" 
                            Text="Return to Event Website" />--%>
                </div>
                <div class="col-md-3">
                    &nbsp;
                </div>
            </div>
        </div>
        <br /><br /><br /><br /><br />
    </asp:Panel>
    <div>
      <iframe runat="server" id="ShowPDF" src="" width="100%" height="1200" scrolling="no"  frameborder="0" visible="false" ></iframe>
        </div>
</asp:Content>

