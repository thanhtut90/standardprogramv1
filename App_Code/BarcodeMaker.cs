﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net;
using System.IO;
using Corpit.Site.Utilities;
using Corpit.Site.Email;
using Corpit.Email;
using Corpit.Utilities;
using System.Data;
using Newtonsoft.Json;
/// <summary>
/// Summary description for Functionality
/// </summary>
public class jBQCode
{
    public string ShowName { get; set; } = "";
    public string BarCode { get; set; } = "";
    public string Regno { get; set; } = "";
}
public class RtnBQCode
{
    public string RTN_URL { get; set; } = "";


}
public class BarcodeMaker
{

    public string MakeBarcode(jBQCode jData)
    {
        string barcodeURL = "";
        try
        {
            string json = JsonConvert.SerializeObject(jData);
            BarcodeMaker bMaker = new BarcodeMaker();
            string rtnValue = PostBQrcodePortalApi(json, "MakeBarCode");
      
            if (rtnValue != null)
            {
                RtnBQCode rtnConfig = JsonConvert.DeserializeObject<RtnBQCode>(rtnValue);
                if (rtnConfig != null && !string.IsNullOrEmpty(rtnConfig.RTN_URL))
                    barcodeURL = rtnConfig.RTN_URL;
            }
        }
        catch { }
        return barcodeURL;
    }
    public string PostBQrcodePortalApi(string jData, string callPage)
    {
        string rtnData = "";
        try
        {
            string jsonStr = jData;

            if (ConfigurationManager.AppSettings["BQRCodeURL"] != null)
            {
                string url = ConfigurationManager.AppSettings["BQRCodeURL"].ToString() + "/" + callPage;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(jsonStr);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                string status = "";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    rtnData = streamReader.ReadToEnd();
                }
            }
        }
        catch (Exception ex) { rtnData = ""; }
        return rtnData;
    }

}

