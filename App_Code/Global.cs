﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Global
/// </summary>
public static class Global
{
    public static string PageTitle {get;set;}
    public static string Copyright { get; set; }
    public static string PageBanner { get; set; }
    public static string Currency { get; set; }

}