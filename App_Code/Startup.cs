﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StandardWebTemplate.Startup))]
namespace StandardWebTemplate
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
