﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
/// <summary>
/// Summary description for Functionality
/// </summary>
public static class DefaultText
{
    public static string LANG_ENG = "0";
    public static string LANG_OTHER = "1";
    public static string BTN_NEXT_LANG_0 = "Next";
    public static string BTN_NEXT_LANG_1 = "ถัดไป";
    public static string BTN_EDIT_LANG_0 = "Edit";
    public static string BTN_EDIT_LANG_1 = "แกไ้ ข";
    public static string BTN_SUBMIT_LANG_0 = "Submit";
    public static string BTN_SUBMIT_LANG_1 = "ยืนยัน";
    public static string BTN_ADD_MBER_LANG_0 = "Add Member";
    public static string BTN_ADD_MBER_LANG_1 = "Add Member";

}
 