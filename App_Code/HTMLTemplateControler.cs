﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Corpit.Site.Utilities;
using Corpit.Site.Email;
using Corpit.Email;
using Corpit.Utilities;
using Corpit.Registration;
using Newtonsoft.Json;
/// <summary>
/// Summary description for HTMLTemplateControler
/// </summary>
public class HTMLTemplateControler
{
    public HTMLTemplateControler()
    {
        //
        // TODO: Add constructor logic here
    }  //

    public string CreateAcknowledgeLetterHTMLTemplate(string showID, string keyID)
    {
        Functionality fn = new Functionality();
        string rtnTemplate = "";
        try
        {
            RegDelegateObj regObj = new RegDelegateObj(fn);
            DataTable dgateList = regObj.getRegDelegateByID(keyID, showID);

            string flowID = "";
            string groupID = "";
            string delegateID = "";
            if (dgateList.Rows.Count > 0)
            {
                //ID is Delegate
                flowID = dgateList.Rows[0]["reg_urlFlowID"].ToString();
                groupID = dgateList.Rows[0]["RegGroupID"].ToString();
                delegateID = keyID;
            }

            RegGroupObj regGroup = new RegGroupObj(fn);
            DataTable dGroup = regGroup.getRegGroupByID(keyID, showID);
            if (string.IsNullOrEmpty(flowID))
            {
                if (dGroup.Rows.Count > 0)
                {
                    flowID = dGroup.Rows[0]["RG_urlFlowID"].ToString();
                }
            }
            FlowControler flwControl = new FlowControler(fn);
            FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowID);
            string sql = "";
            if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
            {
                string formType = "";

                //if (dGroup.Rows.Count > 0)
                //{
                //    flowID = dGroup.Rows[0]["RG_urlFlowID"].ToString();
                //    groupID = keyID;
                //}

                if (dgateList.Rows.Count > 0)
                {
                    //Delegate
                    formType = "D";
                    delegateID = dgateList.Rows[0]["Regno"].ToString();
                }
                else
                {
                    // Group
                    formType = "G";
                    delegateID = "";
                    if (dGroup.Rows.Count > 0)
                    {
                        flowID = dGroup.Rows[0]["RG_urlFlowID"].ToString();
                         groupID = dGroup.Rows[0]["RegGroupID"].ToString(); ;
                    }
                }
                sql = string.Format(@"select EmailID,EmailPortalRefShow,EmailPortalRefKey,RefEmailProjID, Email_BaseDSource,FlowKey from tb_site_EmailConfig c,tb_site_flow_email e 
                                 where c.ShowID=@ParamShow and e.showid=c.showID and IsAcknowledgement=1  and EmailRegType='{0}'
                              and FlowKey in (select flow_step_index from tb_site_Flow where flow_id=@ParamFlow)", formType);
            }
            else
            {
                sql = @"select EmailID,EmailPortalRefShow,EmailPortalRefKey,RefEmailProjID, Email_BaseDSource,FlowKey from tb_site_EmailConfig c,tb_site_flow_email e 
                                 where c.ShowID=@ParamShow and e.showid=c.showID and IsAcknowledgement=1 
                              and FlowKey in (select flow_step_index from tb_site_Flow where flow_id=@ParamFlow)";
            }

            if (!string.IsNullOrEmpty(flowID))
            {
                List<SqlParameter> spList = new List<SqlParameter>();
                SqlParameter parmaShow = new SqlParameter("ParamShow", SqlDbType.NVarChar);
                parmaShow.Value = showID;
                spList.Add(parmaShow);
                SqlParameter parmaflow = new SqlParameter("ParamFlow", SqlDbType.NVarChar);
                parmaflow.Value = flowID;
                spList.Add(parmaflow);
                DataTable dtEmail = fn.GetDatasetByCommand(sql, "DT", spList).Tables["DT"];

                if (dtEmail.Rows.Count > 0)
                {
                    string EmailID = "";
                    string txtEmailRefKey = "";
                    string txtPortalRefShowID = "";
                    string Email_BaseDSource = "";
                    string FlowKey = "";
                    string HTMLtemplate = "";

                    txtEmailRefKey = dtEmail.Rows[0]["EmailPortalRefKey"].ToString();
                    txtPortalRefShowID = dtEmail.Rows[0]["EmailPortalRefShow"].ToString();
                    EmailID = dtEmail.Rows[0]["EmailID"].ToString();
                    Email_BaseDSource = dtEmail.Rows[0]["Email_BaseDSource"].ToString();
                    FlowKey = dtEmail.Rows[0]["FlowKey"].ToString();
                    //Get Template
                    EmailHelper eHelp = new EmailHelper();
                    ePortalEmailMaster eMaster = new ePortalEmailMaster();
                    eMaster.EmailAccessKey = txtEmailRefKey;
                    eMaster.ShowID = txtPortalRefShowID;
                    List<ePortalEmailTemplateDetail> tempList = eHelp.GetEmailTemplateList(eMaster);
                    foreach (ePortalEmailTemplateDetail tmp in tempList)
                    {
                        HTMLtemplate = System.Web.HttpContext.Current.Server.HtmlDecode(tmp.TemplateContent);
                    }
                    //
                    if (!string.IsNullOrEmpty(HTMLtemplate))
                    {
                        string regno = "";
                        if (!string.IsNullOrEmpty(delegateID))
                            regno = delegateID;
                        else
                            regno = groupID;


                        //string encrytedShowID = cFun.EncryptValue(showID);
                        //string encrytedRegNO = cFun.EncryptValue(regno); // GroupID or Regno

                        SendEmailHelper emailHelper = new SendEmailHelper(fn);
                        FlowEmail fEmail = new FlowEmail();
                        EmailDSourceKey sourcKey = new EmailDSourceKey();
                        sourcKey.AddKeyToList("ShowID", showID);
                        sourcKey.AddKeyToList("RegGroupID", groupID);
                        sourcKey.AddKeyToList("Regno", delegateID);
                        sourcKey.AddKeyToList("FlowID", flowID);

                        fEmail.Email_BaseDSource = Email_BaseDSource;
                        fEmail.EmailID = EmailID;
                        fEmail.FlowKey = FlowKey;
                        fEmail.ShowID = showID;
                        string sendTO = "";
                        List<CC> ccList = new List<CC>();
                        EmailHelper eHelper = new EmailHelper();
                        string jsonPostData = eHelper.MakeEmailJsonString(fEmail, sourcKey, ccList);
                        ePoratalJsonData eJObj = new ePoratalJsonData();

                        ePoratalJsonData jData = JsonConvert.DeserializeObject<ePoratalJsonData>(jsonPostData);
                        if (jData != null)
                        {
                            foreach (Pramz pItem in jData.Pramz)
                            {
                                HTMLtemplate = HTMLtemplate.Replace("%" + pItem.Key.Trim() + "%", pItem.PValue.Trim());
                            }
                        }
                        rtnTemplate = HTMLtemplate;

                        // Replce HTML with ParamValue

                    }
                }
            }

        }

        catch (Exception ex)
        {

        }
        return rtnTemplate;
    }

    public string CreateVisaLetterHTMLTemplate(string showID, string keyID)
    {
        string rtnTemplate = "";
        try
        {
            Functionality fn = new Functionality();
            RegDelegateObj regObj = new RegDelegateObj(fn);
            DataTable dgateList = regObj.getRegDelegateByID(keyID, showID);

            string flowID = "";
            string groupID = "";
            string delegateID = "";
            if (dgateList.Rows.Count > 0)
            {
                //ID is Delegate
                flowID = dgateList.Rows[0]["reg_urlFlowID"].ToString();
                groupID = dgateList.Rows[0]["RegGroupID"].ToString();
                delegateID = keyID;
            }

            RegGroupObj regGroup = new RegGroupObj(fn);
            DataTable dGroup = regGroup.getRegGroupByID(keyID, showID);
            if (string.IsNullOrEmpty(flowID))
            {
                if (dGroup.Rows.Count > 0)
                {
                    flowID = dGroup.Rows[0]["RG_urlFlowID"].ToString();
                }
            }

            SiteHTMLTemplateControler htmlControl = new SiteHTMLTemplateControler(fn);
            HTMLTemplate htmlTemplate = htmlControl.GetHtmlTemplateByType(showID, flowID, EmailHTMLTemlateType.Visa);
            FlowEmail fEmail = new FlowEmail();
            EmailDSourceKey sourcKey = new EmailDSourceKey();
            sourcKey.AddKeyToList("ShowID", showID);
            sourcKey.AddKeyToList("RegGroupID", groupID);
            sourcKey.AddKeyToList("Regno", delegateID);
            sourcKey.AddKeyToList("FlowID", flowID);
            string HTMLtemplate = "";
            fEmail.Email_BaseDSource = htmlTemplate.BaseDSource;
            fEmail.EmailID = htmlTemplate.TemplateID;
            fEmail.FlowKey = "";
            fEmail.ShowID = showID;
            HTMLtemplate =  htmlTemplate.Template_Value;
            List<CC> ccList = new List<CC>();
            EmailHelper eHelper = new EmailHelper();
            string jsonPostData = eHelper.MakeEmailJsonString(fEmail, sourcKey, ccList);

            ePoratalJsonData eJObj = new ePoratalJsonData();

            ePoratalJsonData jData = JsonConvert.DeserializeObject<ePoratalJsonData>(jsonPostData);
            if (jData != null)
            {
                foreach (Pramz pItem in jData.Pramz)
                {
                    HTMLtemplate = HTMLtemplate.Replace("%" + pItem.Key.Trim() + "%", pItem.PValue.Trim());
                }
            }

            rtnTemplate = HTMLtemplate;
        }
        catch { }
        return rtnTemplate;
    }

    public string CreateInvoiceReceiptHTMLTemplate(string showID, string keyID,string invID,string templateType)
    {
        string rtnTemplate = "";
        try
        {
            Functionality fn = new Functionality();
            RegDelegateObj regObj = new RegDelegateObj(fn);
            DataTable dgateList = regObj.getRegDelegateByID(keyID, showID);

            string flowID = "";
            string groupID = "";
            string delegateID = "";
            if (dgateList.Rows.Count > 0)
            {
                //ID is Delegate
                flowID = dgateList.Rows[0]["reg_urlFlowID"].ToString();
                groupID = dgateList.Rows[0]["RegGroupID"].ToString();
                delegateID = keyID;
            }

            RegGroupObj regGroup = new RegGroupObj(fn);
            DataTable dGroup = regGroup.getRegGroupByID(keyID, showID);
            if (string.IsNullOrEmpty(flowID))
            {
                if (dGroup.Rows.Count > 0)
                {
                    flowID = dGroup.Rows[0]["RG_urlFlowID"].ToString();
                    groupID = dGroup.Rows[0]["RegGroupID"].ToString();
                    delegateID = "";
                }
            }

            SiteHTMLTemplateControler htmlControl = new SiteHTMLTemplateControler(fn);
            HTMLTemplate htmlTemplate = htmlControl.GetHtmlTemplateByType(showID, flowID, templateType);
            FlowEmail fEmail = new FlowEmail();
            EmailDSourceKey sourcKey = new EmailDSourceKey();
            sourcKey.AddKeyToList("ShowID", showID);
            sourcKey.AddKeyToList("RegGroupID", groupID);
            sourcKey.AddKeyToList("Regno", delegateID);
            sourcKey.AddKeyToList("FlowID", flowID);
            sourcKey.AddKeyToList("INVID", invID);
            string HTMLtemplate = "";
            fEmail.Email_BaseDSource = htmlTemplate.BaseDSource;
            fEmail.EmailID = htmlTemplate.TemplateID;
            fEmail.FlowKey = "";
            fEmail.ShowID = showID;
            HTMLtemplate = htmlTemplate.Template_Value;
            List<CC> ccList = new List<CC>();
            EmailHelper eHelper = new EmailHelper();
            string jsonPostData = eHelper.MakeEmailJsonString(fEmail, sourcKey, ccList);

            ePoratalJsonData eJObj = new ePoratalJsonData();

            ePoratalJsonData jData = JsonConvert.DeserializeObject<ePoratalJsonData>(jsonPostData);
            if (jData != null)
            {
                foreach (Pramz pItem in jData.Pramz)
                {
                    HTMLtemplate = HTMLtemplate.Replace("%" + pItem.Key.Trim() + "%", pItem.PValue.Trim());
                }
            }

            rtnTemplate = HTMLtemplate;
        }
        catch { }
        return rtnTemplate;
    }
    public string CreatePDF(string HTMLtemplate, string showID, string regNO, string folderType, bool isFullPage=false)
    {
        string rtnFile = "";
        try
        {
            CommonFuns cFuz = new CommonFuns();
            Functionality fn = new Functionality();
            ShowControler sControl = new ShowControler(fn);
            Show s = sControl.GetShow(showID);
            string showName = s.SHW_Name;
            CommonFuns cFun = new CommonFuns();
            if (!string.IsNullOrEmpty(HTMLtemplate))
            {
                CreateFileInSite cFile = new CreateFileInSite(fn);
                cFile = new CreateFileInSite(fn);
                cFile.FolderType = folderType;// "Acknowledge";
                cFile.ShowID = showID;
                string path = cFile.CrateFolderInSite();
                string fileName = regNO + ".pdf";
                string pdfOutput = cFuz.MakeFullFilePath(path, fileName);
                string file_pdf = pdfOutput;
                try
                {
                    NReco.PdfGenerator.HtmlToPdfConverter pdfConverter = new NReco.PdfGenerator.HtmlToPdfConverter();

                    if (isFullPage)
                    {
                        var margins = new NReco.PdfGenerator.PageMargins();
                        margins.Top = 0;// 6;
                        margins.Bottom = 0;
                        margins.Left = 0;// 4;
                        margins.Right = 0;
                        pdfConverter.Margins = margins;
                    }

                    //var pdfBytes = (new NReco.PdfGenerator.HtmlToPdfConverter()).GeneratePdf(HTMLtemplate);
                    var pdfBytes = pdfConverter.GeneratePdf(HTMLtemplate);
                    if (System.IO.File.Exists(file_pdf))
                    {
                        try
                        {
                            System.IO.File.Delete(file_pdf);
                        }

                        catch (Exception ex) { }
                    }

                    System.IO.File.WriteAllBytes(file_pdf, pdfBytes);
                    rtnFile = "/" + folderType.Trim() + "/" + showName.Trim() + "/" + fileName;

                    string url = HttpContext.Current.Request.Url.AbsoluteUri;
                    if (!string.IsNullOrEmpty(url))
                    {
                        #region comment on 4-2-2020 by th
                        //url = url.Substring(0, url.LastIndexOf("?"));
                        //url = url.Substring(0, url.LastIndexOf("/"));
                        #endregion
                        #region added on 4-2-2020 by th
                        if (url.LastIndexOf("?") >= 0)
                            url = url.Substring(0, url.LastIndexOf("?"));
                        url = url.ToLower();
                        url = url.Substring(0, url.LastIndexOf("/"));
                        #endregion
                    }

                    rtnFile = url + rtnFile;
                }
                catch (Exception ex) { }
            }
        }
        catch { }
        return rtnFile;
    }

    public string CreateBadgeLetterHTMLTemplate(string showID, string keyID)
    {
        string rtnTemplate = "";
        try
        {
            Functionality fn = new Functionality();
            RegDelegateObj regObj = new RegDelegateObj(fn);
            DataTable dgateList = regObj.getRegDelegateByID(keyID, showID);

            string flowID = "";
            string groupID = "";
            string delegateID = "";
            if (dgateList.Rows.Count > 0)
            {
                //ID is Delegate
                flowID = dgateList.Rows[0]["reg_urlFlowID"].ToString();
                groupID = dgateList.Rows[0]["RegGroupID"].ToString();
                delegateID = keyID;
            }

            RegGroupObj regGroup = new RegGroupObj(fn);
            DataTable dGroup = regGroup.getRegGroupByID(keyID, showID);
            if (string.IsNullOrEmpty(flowID))
            {
                if (dGroup.Rows.Count > 0)
                {
                    flowID = dGroup.Rows[0]["RG_urlFlowID"].ToString();
                }
            }

            SiteHTMLTemplateControler htmlControl = new SiteHTMLTemplateControler(fn);
            HTMLTemplate htmlTemplate = htmlControl.GetHtmlTemplateByType(showID, flowID, EmailHTMLTemlateType.Badge);
            FlowEmail fEmail = new FlowEmail();
            EmailDSourceKey sourcKey = new EmailDSourceKey();
            sourcKey.AddKeyToList("ShowID", showID);
            sourcKey.AddKeyToList("RegGroupID", groupID);
            sourcKey.AddKeyToList("Regno", delegateID);
            sourcKey.AddKeyToList("FlowID", flowID);
            string HTMLtemplate = "";
            fEmail.Email_BaseDSource = htmlTemplate.BaseDSource;
            fEmail.EmailID = htmlTemplate.TemplateID;
            fEmail.FlowKey = "";
            fEmail.ShowID = showID;
            HTMLtemplate = htmlTemplate.Template_Value;
            List<CC> ccList = new List<CC>();
            EmailHelper eHelper = new EmailHelper();
            string jsonPostData = eHelper.MakeEmailJsonString(fEmail, sourcKey, ccList);

            ePoratalJsonData eJObj = new ePoratalJsonData();

            ePoratalJsonData jData = JsonConvert.DeserializeObject<ePoratalJsonData>(jsonPostData);
            if (jData != null)
            {
                foreach (Pramz pItem in jData.Pramz)
                {
                    HTMLtemplate = HTMLtemplate.Replace("%" + pItem.Key.Trim() + "%", pItem.PValue.Trim());
                }
            }

            rtnTemplate = HTMLtemplate;
        }
        catch { }
        return rtnTemplate;
    }

    private EmailDSourceKey GetTemplateSourceKey(string showID, string keyID)
    {
        EmailDSourceKey sourcKey = new EmailDSourceKey();
        Functionality fn = new Functionality();
        RegDelegateObj regObj = new RegDelegateObj(fn);
        DataTable dgateList = regObj.getRegDelegateByID(keyID, showID);

        string flowID = "";
        string groupID = "";
        string delegateID = "";
        if (dgateList.Rows.Count > 0)
        {
            //ID is Delegate
            flowID = dgateList.Rows[0]["reg_urlFlowID"].ToString();
            groupID = dgateList.Rows[0]["RegGroupID"].ToString();
            delegateID = keyID;
        }

        RegGroupObj regGroup = new RegGroupObj(fn);
        DataTable dGroup = regGroup.getRegGroupByID(keyID, showID);
        if (string.IsNullOrEmpty(flowID))
        {
            if (dGroup.Rows.Count > 0)
            {
                flowID = dGroup.Rows[0]["RG_urlFlowID"].ToString();
                groupID = dGroup.Rows[0]["RegGroupID"].ToString();
                delegateID = "";
            }
        }

        sourcKey.AddKeyToList("ShowID", showID);
        sourcKey.AddKeyToList("RegGroupID", groupID);
        sourcKey.AddKeyToList("Regno", delegateID);
        sourcKey.AddKeyToList("FlowID", flowID);

        return sourcKey;
    }
    public string GetTemplateFileName(string showID, string keyID, string templateType)
    {
        Functionality fn = new Functionality();
        EmailDSourceKey sourcKey = GetTemplateSourceKey(showID, keyID);
        SiteHTMLTemplateControler htmlControl = new SiteHTMLTemplateControler(fn);
        HTMLTemplate htmlTemplate = htmlControl.GetHtmlTemplateByType(showID, sourcKey.DSourceKeyList["FlowID"], templateType);
        string fileName = htmlTemplate.Template_Desc;

        return fileName;
    }
}


public static class HTMLTemplateType
{
    public static string Acknowledge = "ACKNOWLEDGE";
    public static string Invoice = "INV";
    public static string DelegateList = "DLIST";
}