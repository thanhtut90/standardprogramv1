﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net;
using System.IO;
using Corpit.Site.Utilities;
using Corpit.Site.Email;
using Corpit.Email;
using Corpit.Utilities;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Corpit.Registration;
using System.Text.RegularExpressions;
using Corpit.Security;
using Corpit.Payment;
/// <summary>
/// Summary description for Functionality
/// </summary>
public class EmailHelper
{
    private static string FoodJapanShowID = "KKS355";
    private static string VendorRegistrationShowID = "AXS357";
    private static string IDEMShowID = "UZH408";
    private static string IDEMShowID_LIVE = "OSH388";
    #region SendEmail
    #region FrontEnd
    public bool SendCurrentFlowStepEmail(FlowURLQuery urlQuery)
    {
        bool isOK = false;
        Functionality fn = new Functionality();
        CommonFuns cFun = new CommonFuns();

        SendEmailHelper emailHelper = new SendEmailHelper(fn);
        // FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwControl = new FlowControler(fn, urlQuery);

        string showID = cFun.DecryptValue(urlQuery.CurrShowID);
        string groupID = cFun.DecryptValue(urlQuery.GoupRegID);
        string delegateID = cFun.DecryptValue(urlQuery.DelegateID);
        string flowID = cFun.DecryptValue(urlQuery.FlowID);
        List<Corpit.Site.Email.FlowEmail> flwEmailList = emailHelper.GetFlowStepIndxEmailList(flwControl.FlowKey, showID);
        if (flwEmailList.Count > 0)
        {

            bool sentSuccess = false;
            foreach (Corpit.Site.Email.FlowEmail fEmail in flwEmailList)
            {
                try
                {
                    if (!string.IsNullOrEmpty(fEmail.ShowID) && !string.IsNullOrEmpty(fEmail.EmailID))
                    {
                        ePoratalJsonData eJObj = new ePoratalJsonData();
                        string status = "F";
                        sentSuccess = false;
                        string strJson = "";
                        if (fEmail.SendToAllDelegate == "1") // Email to All Delegate Member(s)
                        {
                            RegDelegateObj regObj = new RegDelegateObj(fn);
                            RegGroupObj regGroup = new RegGroupObj(fn);

                            DataTable dGroup = regGroup.getRegGroupByID(groupID, showID);
                            string gCCEMail = "";
                            if (dGroup.Rows.Count > 0)
                            {//Food Japan
                                if (showID != "KKS355")
                                    gCCEMail = dGroup.Rows[0]["RG_ContactEmail"].ToString();
                            }

                            List<CC> ccList = new List<CC>();
                            CC groupEmail = new CC();
                            groupEmail.Email = gCCEMail;
                            ccList.Add(groupEmail);
                            DataTable dgateList = regObj.getRegDelegateByGroupID(groupID, showID);
                            for (int i = 0; i < dgateList.Rows.Count; i++)
                            {
                                delegateID = dgateList.Rows[i]["Regno"].ToString();

                                string regno = delegateID;
                                if (string.IsNullOrEmpty(regno)) regno = groupID;
                                bool isSent = emailHelper.CheckEmailSentByID(fEmail.ShowID, fEmail.EmailID, regno); //Check Sent
                                if (fEmail.EmailType == EmailHTMLTemlateType.INVOICE || fEmail.EmailType == EmailHTMLTemlateType.RECEIPT)// Everytime send for Invoie and receipt
                                {
                                    isSent = false;
                                }
                                status = "F";
                                if (!isSent)
                                {
                                    EmailDSourceKey sourcKey = new EmailDSourceKey();
                                    sourcKey.AddKeyToList("ShowID", showID);
                                    sourcKey.AddKeyToList("RegGroupID", groupID);
                                    sourcKey.AddKeyToList("Regno", delegateID);
                                    sourcKey.AddKeyToList("FlowID", flowID);
                                    string refID = "";
                                    string emailCondition = fEmail.EmailSendCondition;
                                    bool toSendEmail = true;
                                    bool invOwnedByGroup = true;
                                    if (!string.IsNullOrEmpty(emailCondition))
                                        toSendEmail = CheckEmailByCondition(emailCondition, sourcKey, ref refID, invOwnedByGroup);

                                    if (fEmail.EmailType == EmailHTMLTemlateType.INVOICE || fEmail.EmailType == EmailHTMLTemlateType.RECEIPT)// Get InvoiceID if Email Type is Invoice
                                    {
                                        if (string.IsNullOrEmpty(refID))
                                        {
                                            refID = GetLastestInvoiceID(sourcKey);
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(refID)) sourcKey.AddKeyToList("INVID", refID); // Add InviD
                                    if (toSendEmail)
                                    {
                                        strJson = MakeEmailJsonString(fEmail, sourcKey, ccList);
                                        if (!string.IsNullOrEmpty(strJson))
                                        {
                                            //sentSuccess =  SendEmail(sourcKey, strJson);
                                            if (!string.IsNullOrEmpty(strJson))
                                            {
                                                sentSuccess = SendEmail(sourcKey, strJson);
                                            }
                                        }
                                        else
                                            status = "F";
                                    }
                                    else
                                        status = "FCON";
                                }
                                else
                                    status = "D"; //Duplicate


                                string refRegno = delegateID;
                                if (sentSuccess) status = "S";

                                if (string.IsNullOrEmpty(refRegno)) refRegno = groupID;
                                EmailLog eLog = new EmailLog();
                                eLog.ShowID = fEmail.ShowID;
                                eLog.FlowID = flowID;
                                eLog.EmailID = fEmail.EmailID;
                                eLog.RefRegno = refRegno;
                                eLog.EmailType = fEmail.EmailType;
                                eLog.ESentBy = refRegno;
                                eLog.EContent = strJson;
                                eLog.Status = status;
                                emailHelper.LogEmail(eLog);

                            }
                        }
                        else if (fEmail.SendToAllDelegate == "2") // Email to Accompany Person)
                        {
                            EmailDSourceKey sourcKey = new EmailDSourceKey();
                            sourcKey.AddKeyToList("ShowID", showID);
                            sourcKey.AddKeyToList("RegGroupID", groupID);
                            sourcKey.AddKeyToList("Regno", delegateID);
                            sourcKey.AddKeyToList("FlowID", flowID);
                            string refID = "";
                            if (string.IsNullOrEmpty(refID))
                            {
                                refID = GetLastestInvoiceID(sourcKey);
                            }
                            if (!string.IsNullOrEmpty(refID)) sourcKey.AddKeyToList("INVID", refID); // Add InviD
                            EmailSendToAccompanyPerson(fEmail, sourcKey);
                        }
                        else
                        {  // Single Email to Delegate or Group Person
                            string regno = delegateID;
                            if (string.IsNullOrEmpty(regno)) regno = groupID;
                            bool isSent = emailHelper.CheckEmailSentByID(fEmail.ShowID, fEmail.EmailID, regno); //Check Sent
                            if (fEmail.EmailType == EmailHTMLTemlateType.INVOICE || fEmail.EmailType == EmailHTMLTemlateType.RECEIPT)// Everytime send for Invoie and receipt
                            {
                                isSent = false;
                            }
                            status = "F";
                            if (!isSent)
                            {
                                List<CC> ccList = new List<CC>();
                                EmailDSourceKey sourcKey = new EmailDSourceKey();
                                sourcKey.AddKeyToList("ShowID", showID);
                                sourcKey.AddKeyToList("RegGroupID", groupID);
                                sourcKey.AddKeyToList("Regno", delegateID);
                                sourcKey.AddKeyToList("FlowID", flowID);
                                string refID = "";
                                string emailCondition = fEmail.EmailSendCondition;
                                bool toSendEmail = true;
                                bool invOwnedByGroup = false;
                                if (!string.IsNullOrEmpty(emailCondition))
                                    toSendEmail = CheckEmailByCondition(emailCondition, sourcKey, ref refID, invOwnedByGroup);

                                if (fEmail.EmailType == EmailHTMLTemlateType.INVOICE || fEmail.EmailType == EmailHTMLTemlateType.RECEIPT) // Get InvoiceID if Email Type is Invoice
                                {
                                    if (string.IsNullOrEmpty(refID))
                                    {
                                        refID = GetLastestInvoiceID(sourcKey);
                                    }
                                }
                                if (!string.IsNullOrEmpty(refID)) sourcKey.AddKeyToList("INVID", refID); // Add InviD
                                if (toSendEmail)
                                {
                                    strJson = MakeEmailJsonString(fEmail, sourcKey, ccList);
                                    if (!string.IsNullOrEmpty(strJson))
                                    {
                                        sentSuccess = SendEmail(sourcKey, strJson);
                                    }
                                    else
                                        status = "F";
                                }
                                else
                                    status = "FCON";
                            }
                            else
                                status = "D";

                            string refRegno = delegateID;
                            if (sentSuccess) status = "S";

                            if (string.IsNullOrEmpty(refRegno)) refRegno = groupID;
                            EmailLog eLog = new EmailLog();
                            eLog.ShowID = fEmail.ShowID;
                            eLog.FlowID = flowID;
                            eLog.EmailID = fEmail.EmailID;
                            eLog.RefRegno = refRegno;
                            eLog.EmailType = fEmail.EmailType;
                            eLog.ESentBy = refRegno;
                            eLog.EContent = strJson;
                            eLog.Status = status;
                            emailHelper.LogEmail(eLog);
                        }


                        //LogEmail

                    }
                }
                catch (Exception ex) { }
            }
        }
        return isOK;
    }
    #endregion 
    public bool SendEmail(EmailDSourceKey sourcKey, string jsonStr)
    {
        bool isOK = false;
        try
        {
            isOK = CallSendEmailApi(jsonStr);
        }
        catch { }

        return isOK;
    }
    public string MakeEmailJsonString(FlowEmail fEmail, EmailDSourceKey sourcKey, List<CC> ccList)
    {
        string rtnJson = "";
        try
        {
            Functionality fn = new Functionality();
            CommonFuns cFun = new CommonFuns();
            List<CC> mailccList = new List<CC>();
            SendEmailHelper emailHelper = new SendEmailHelper(fn);
            ePoratalJsonData eJObj = new ePoratalJsonData();
            string sendTO = "";
            string regno = "";
            string showID = sourcKey.DSourceKeyList["ShowID"];
            string groupID = sourcKey.DSourceKeyList["RegGroupID"];
            string delegateID = sourcKey.DSourceKeyList["Regno"];
            string flowID = sourcKey.DSourceKeyList["FlowID"];
            string InvID = "";
            if (sourcKey.DSourceKeyList.ContainsKey("INVID"))
                InvID = sourcKey.DSourceKeyList["INVID"];
            if (!string.IsNullOrEmpty(delegateID))
                regno = delegateID;
            else
                regno = groupID;

            List<CC> eCCList = emailHelper.GetEmailCCList(showID, fEmail.EmailID);
            if (eCCList.Count > 0) mailccList = eCCList;
            if (ccList != null && ccList.Count > 0)
            {
                foreach (CC cItem in ccList)
                {
                    mailccList.Add(cItem);
                }
            }
            eJObj.CustomizeSubject = GetCustomSubject(fEmail.EmailCustomSubject, sourcKey);
            List<BCC> mailBCCList = new List<BCC>();
            List<BCC> eBCCList = emailHelper.GetEmailBCCList(showID, fEmail.EmailID);
            List<Attachz> mailAttachmentList = AddEmailAttachment(sourcKey, fEmail.EmailID);
            if (eBCCList.Count > 0)
                mailBCCList = eBCCList;

            string encrytedShowID = cFun.EncryptValue(showID);
            string encrytedRegNO = cFun.EncryptValue(regno); // GroupID or Regno
            string encrytedInv = cFun.EncryptValue(InvID);
            DataTable dEmailSource = emailHelper.GetDataSource(fEmail.Email_BaseDSource, sourcKey, ref sendTO); // Get Groupo or Reg According To Email DB Source
            eJObj.ShowID = fEmail.EmailPortalRefShow;
            eJObj.ERefKey = fEmail.EmailPortalRefKey;
            eJObj.RefRegno = regno;
            eJObj.To = sendTO;
            eJObj.Remarks = "";
            eJObj.CC = mailccList;
            eJObj.BCC = mailBCCList;
            eJObj.Attachz = mailAttachmentList;
            List<EmailParam> dbParm = emailHelper.GetEmailParmazListFromDB(fEmail.ShowID, fEmail.EmailID);

            if (dbParm != null && dbParm.Count > 0)
            {

                eJObj.Pramz = emailHelper.GetEmailParazList(dbParm, dEmailSource); // Create Parameter
                if (eJObj.Pramz.Count > 0)
                {
                    // File.WriteAllText(@"C:\webmaster\hosting\LogError\Log.txt", "Param");
                    //bool hasBarcode = eJObj.Pramz.Any(cus => cus.Key == EmailDefaultParamValue.BarcodeURL);

                    Pramz comcountryField = eJObj.Pramz.FirstOrDefault(x => x.Key == "RC_Country");
                    if (comcountryField != null)
                    {
                        string countryVal = comcountryField.PValue;
                        countryVal = GetCountry(countryVal, showID);
                        if (!string.IsNullOrEmpty(countryVal))
                            comcountryField.PValue = countryVal;
                        if (comcountryField.PValue == "0") comcountryField.PValue = "";// Remove Zero from country
                    }
                    Pramz groupcountryField = eJObj.Pramz.FirstOrDefault(x => x.Key == "RG_Country");
                    if (groupcountryField != null)
                    {
                        string countryVal = groupcountryField.PValue;
                        countryVal = GetCountry(countryVal, showID);
                        if (!string.IsNullOrEmpty(countryVal))
                            groupcountryField.PValue = countryVal;
                    }
                    Pramz dcountryField = eJObj.Pramz.FirstOrDefault(x => x.Key == "reg_Country");
                    if (dcountryField != null)
                    {
                        string countryVal = dcountryField.PValue;
                        countryVal = GetCountry(countryVal, showID);
                        if (!string.IsNullOrEmpty(countryVal))
                            dcountryField.PValue = countryVal;
                        if (dcountryField.PValue == "0") dcountryField.PValue = "";// Remove Zero from country
                    }


                    Pramz fond = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.BarcodeURL);
                    if (fond != null)
                    {

                        string barcode = regno;

                        SiteSettings sSetting = new SiteSettings(fn, showID);
                        if (!string.IsNullOrEmpty(sSetting.SitePrefix))
                            barcode = sSetting.SitePrefix + "-" + regno;
                        jBQCode jData = new jBQCode();
                        jData.ShowName = showID;
                        jData.BarCode = barcode;
                        jData.Regno = regno;
                        BarcodeMaker bMaker = new BarcodeMaker();
                        string url = bMaker.MakeBarcode(jData);
                        fond.PValue = url;
                    }



                    Pramz AcURL = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.AcknowledgeURL);
                    if (AcURL != null)
                    {
                        try
                        {
                            string url = GetRootURL();
                            if (!string.IsNullOrEmpty(url))
                            {
                                //url = url.Substring(0, url.LastIndexOf("?"));
                                //url = url.Substring(0, url.LastIndexOf("/"));
                                url += string.Format("/AcknowledgeLetter?SHW={0}&DID={1}", encrytedShowID, encrytedRegNO);
                                AcURL.PValue = url;
                            }
                        }
                        catch { }
                    }
                    Pramz dQR = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.QRcodeDelegateURL);
                    if (dQR != null)
                    {
                        try
                        {
                            QRcodeMaker qMaker = new QRcodeMaker();
                            string url = qMaker.CreateQRcode("D", regno, showID, flowID, fEmail.EmailID, sourcKey);
                            dQR.PValue = url;
                        }
                        catch { }
                    }
                    Pramz dQrRegOnly = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.QRcodeRegOnlyURL);
                    if (dQrRegOnly != null)
                    {
                        try
                        {
                            QRcodeMaker qMaker = new QRcodeMaker();
                            string url = qMaker.CreateQRcode("DReg", regno, showID, flowID, fEmail.EmailID, sourcKey);
                            dQrRegOnly.PValue = url;
                        }
                        catch { }
                    }
                    Pramz dQrWithOrderItem= eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.QRcodeWithOrderItemURL);
                    if (dQrWithOrderItem != null)
                    {
                        try
                        {
                            QRcodeMaker qMaker = new QRcodeMaker();
                            string orderItem = OrderItemListForQRCode(sourcKey);
                            sourcKey.AddKeyToList("OrderedItems", orderItem);
                            string url = qMaker.CreateQRcode("D", regno, showID, flowID, fEmail.EmailID, sourcKey);
                            dQrWithOrderItem.PValue = url;
                        }
                        catch { }
                    }
                    #region ECC
                    Pramz ECCSessionAOrderItem = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.ECCSessionA);
                    if (ECCSessionAOrderItem != null)
                    {
                        try
                        {
                            QRcodeMaker qMaker = new QRcodeMaker();
                            string orderItem = getOrderItemECC(sourcKey, "A");
                            ECCSessionAOrderItem.PValue = orderItem;
                        }
                        catch { }
                    }
                    Pramz ECCSessionBOrderItem = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.ECCSessionB);
                    if (ECCSessionBOrderItem != null)
                    {
                        try
                        {
                            QRcodeMaker qMaker = new QRcodeMaker();
                            string orderItem = getOrderItemECC(sourcKey, "B");
                            ECCSessionBOrderItem.PValue = orderItem;
                        }
                        catch { }
                    }
                    Pramz ECCSessionCOrderItem = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.ECCSessionC);
                    if (ECCSessionCOrderItem != null)
                    {
                        try
                        {
                            QRcodeMaker qMaker = new QRcodeMaker();
                            string orderItem = getOrderItemECC(sourcKey, "C");
                            ECCSessionCOrderItem.PValue = orderItem;
                        }
                        catch { }
                    }
                    #endregion

                    Pramz dList = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.DelegateList);
                    if (dList != null)
                    {
                        try
                        {
                            RegDelegateObj regObj = new RegDelegateObj(fn);
                            DataTable dgateList = regObj.getRegDelegateByGroupID(groupID, showID);
                            string template = "<table width='700' cellpadding='2' style='font-family:Arial;font-size:13px;'> <tr><td style='text-decoration: underline;'>Registration ID</td><td style='text-decoration: underline;'>Name</td>";
                            if (showID != VendorRegistrationShowID)//Asean Submmit Vendor Registration ShowID
                            {
                                template += "<td style='text-decoration: underline;'>Email</td>";
                            }
                            template += "<td style='text-decoration: underline;'>Download PDF</td></tr>";
                            string tmp = "";
                            string strValue = "";
                            string encrypt = "";
                            string url = GetRootURL();
                            if (!string.IsNullOrEmpty(url))
                            {
                                //url = url.Substring(0, url.LastIndexOf("?"));
                                //url = url.Substring(0, url.LastIndexOf("/"));

                            }
                            string sitePreFix = "";
                            string regStatus = "";
                            SiteSettings sSetting = new SiteSettings(fn, showID);
                            if (!string.IsNullOrEmpty(sSetting.SitePrefix))
                                sitePreFix = sSetting.SitePrefix + "-";

                            for (int i = 0; i < dgateList.Rows.Count; i++)
                            {
                                strValue = "";
                                regStatus = dgateList.Rows[i]["reg_Status"].ToString();
                                if (regStatus == "1") // send only complete list
                                {
                                    tmp += "<tr>";



                                    strValue = sitePreFix + dgateList.Rows[i]["Regno"].ToString();
                                    tmp += string.Format("<td> {0} </td> ", strValue);
                                    strValue = dgateList.Rows[i]["reg_Fname"].ToString() + " " + dgateList.Rows[i]["reg_lName"].ToString();
                                    if (showID == FoodJapanShowID)//FoodJapanShow
                                    {
                                        strValue = dgateList.Rows[i]["reg_Fname"].ToString() + " / " + dgateList.Rows[i]["reg_lName"].ToString();
                                    }
                                    tmp += string.Format("<td> {0} </td> ", strValue);
                                    if (showID != VendorRegistrationShowID)//Asean Submmit Vendor Registration ShowID
                                    {
                                        strValue = dgateList.Rows[i]["reg_Email"].ToString();
                                        tmp += string.Format("<td> {0} </td> ", strValue);
                                    }
                                    encrypt = cFun.EncryptValue(dgateList.Rows[i]["regno"].ToString());
                                    strValue = url + string.Format("/AcknowledgeLetter?SHW={0}&DID={1}", encrytedShowID, encrypt);
                                    tmp += string.Format("<td> <a href='{0}'>Download PDF</a> </td> ", strValue);
                                    tmp += "</tr>";
                                }
                            }
                            if (string.IsNullOrEmpty(tmp))
                                template = "";
                            else
                                template += tmp + "</table>";

                            dList.PValue = template;
                        }
                        catch { }
                    }
                    Pramz dListIDEM = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.DelegateListIDEM);
                    if (dListIDEM != null)
                    {
                        try
                        {
                            RegDelegateObj regObj = new RegDelegateObj(fn);
                            DataTable dgateList = regObj.getRegDelegateByGroupID(groupID, showID);
                            string template = "<table width='700' cellpadding='2' style='font-family:Arial;font-size:13px;'> <tr style='background-color:#d9d9d9;'><td>Sal</td><td>Registration ID</td>";
                            template += "<td>First Name</td><td>Last Name</td><td>Company</td></tr>";
                            string tmp = "";
                            string strValue = "";
                            string encrypt = "";
                            string url = GetRootURL();
                            string sitePreFix = "";
                            string regStatus = "";
                            SiteSettings sSetting = new SiteSettings(fn, showID);
                            if (!string.IsNullOrEmpty(sSetting.SitePrefix))
                                sitePreFix = sSetting.SitePrefix + "-";

                            for (int i = 0; i < dgateList.Rows.Count; i++)
                            {
                                strValue = "";
                                regStatus = dgateList.Rows[i]["reg_Status"].ToString();
                                if (regStatus == "1") // send only complete list
                                {
                                    tmp += "<tr>";
                                    strValue = bindSalutation(showID, dgateList.Rows[i]["reg_Salutation"].ToString(), dgateList.Rows[i]["reg_SalutationOthers"].ToString());
                                    tmp += string.Format("<td> {0} </td> ", strValue);

                                    strValue = sitePreFix + dgateList.Rows[i]["Regno"].ToString();
                                    tmp += string.Format("<td> {0} </td> ", strValue);

                                    strValue = dgateList.Rows[i]["reg_Fname"].ToString();
                                    tmp += string.Format("<td> {0} </td> ", strValue);

                                    strValue = dgateList.Rows[i]["reg_lName"].ToString();
                                    tmp += string.Format("<td> {0} </td> ", strValue);

                                    strValue = dgateList.Rows[i]["reg_Additional5"].ToString();/*CompanyName*/
                                    tmp += string.Format("<td> {0} </td> ", strValue);

                                    //encrypt = cFun.EncryptValue(dgateList.Rows[i]["regno"].ToString());
                                    //strValue = url + string.Format("/AcknowledgeLetter?SHW={0}&DID={1}", encrytedShowID, encrypt);
                                    //tmp += string.Format("<td> <a href='{0}'>Download PDF</a> </td> ", strValue);
                                    tmp += "</tr>";
                                }
                            }
                            if (string.IsNullOrEmpty(tmp))
                                template = "";
                            else
                                template += tmp + "</table>";

                            dListIDEM.PValue = template;
                        }
                        catch { }
                    }
                    Pramz VisaURL = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.VisaURL);
                    if (VisaURL != null)
                    {
                        try
                        {
                            string url = GetRootURL();
                            if (!string.IsNullOrEmpty(url))
                            {
                                //url = url.Substring(0, url.LastIndexOf("?"));
                                //url = url.Substring(0, url.LastIndexOf("/"));
                                url += string.Format("/DownloadVisaLetter?SHW={0}&DID={1}", encrytedShowID, encrytedRegNO);
                                VisaURL.PValue = url;
                            }
                        }
                        catch { }
                    }
                    Pramz RptURL = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.ReceiptURL);
                    if (RptURL != null)
                    {
                        try
                        {
                            string url = GetRootURL();
                            if (!string.IsNullOrEmpty(url))
                            {
                                //url = url.Substring(0, url.LastIndexOf("?"));
                                //url = url.Substring(0, url.LastIndexOf("/"));
                                url += string.Format("/DownloadReceiptLetter?SHW={0}&DID={1}&VDD={2}", encrytedShowID, encrytedRegNO, encrytedInv);
                                RptURL.PValue = url;
                            }
                        }
                        catch { }
                    }
                    Pramz InvURL = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.InvoiceURL);
                    if (InvURL != null)
                    {
                        try
                        {
                            string url = GetRootURL();
                            if (!string.IsNullOrEmpty(url))
                            {
                                //url = url.Substring(0, url.LastIndexOf("?"));
                                //url = url.Substring(0, url.LastIndexOf("/"));
                                url += string.Format("/DownloadInvoiceLetter?SHW={0}&DID={1}&VDD={2}", encrytedShowID, encrytedRegNO, encrytedInv);
                                InvURL.PValue = url;
                            }
                        }
                        catch { }
                    }
                    Pramz BadgeURL = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.BadgeURL);
                    if (BadgeURL != null)
                    {
                        try
                        {
                            string url = GetRootURL();
                            if (!string.IsNullOrEmpty(url))
                            {
                                //url = url.Substring(0, url.LastIndexOf("?"));
                                //url = url.Substring(0, url.LastIndexOf("/"));
                                url += string.Format("/DownloadBadge?SHW={0}&DID={1}", encrytedShowID, encrytedRegNO);
                                BadgeURL.PValue = url;
                            }
                        }
                        catch { }
                    }
                    Pramz ConfirmURL = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.ConfirmationURL);
                    if (BadgeURL != null)
                    {
                        try
                        {
                            string url = GetRootURL();
                            if (!string.IsNullOrEmpty(url))
                            {
                                //url = url.Substring(0, url.LastIndexOf("?"));
                                //url = url.Substring(0, url.LastIndexOf("/"));
                                url += string.Format("/DownloadConfirmationLetter?SHW={0}&DID={1}&VDD={2}", encrytedShowID, encrytedRegNO, encrytedInv);
                                //url += string.Format("/DownloadConfirmationLetter?SHW={0}&DID={1}", encrytedShowID, encrytedRegNO);
                                ConfirmURL.PValue = url;
                            }
                        }
                        catch { }
                    }
                    Pramz CurrentDate = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.TodayDate);
                    if (CurrentDate != null)
                    {
                        try
                        {
                            string templat = DateTime.Now.ToString("dd/MM/yyyy");

                            CurrentDate.PValue = templat;
                        }
                        catch { }
                    }

                    Pramz ConfList = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.ConferenceList);
                    if (ConfList != null)
                    {
                        try
                        {
                            //    string url = HttpContext.Current.Request.Url.AbsoluteUri;
                            OrderControler oControl = new OrderControler(fn);
                            string subTotal = "";
                            FlowURLQuery fQuery = new FlowURLQuery(subTotal);
                            fQuery.CurrShowID = showID;
                            fQuery.DelegateID = delegateID;
                            fQuery.GoupRegID = groupID;
                            string template = oControl.GetOrderedListWithTemplate(fQuery, ref subTotal, InvID);
                            if (showID == IDEMShowID || showID == IDEMShowID_LIVE)
                            {
                                template = template.Replace("arial", "'Trebuchet MS',Arial").Replace("font-size:14px;", "").Replace("Arial", "'Trebuchet MS',Arial");
                            }
                            ConfList.PValue = template;
                        }
                        catch { }
                    }

                    Pramz ConfDelegateList = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.ConferenceDelegateList);
                    if (ConfDelegateList != null)
                    {
                        try
                        {
                            //    string url = HttpContext.Current.Request.Url.AbsoluteUri;
                            OrderControler oControl = new OrderControler(fn);
                            string subTotal = "";
                            FlowURLQuery fQuery = new FlowURLQuery(subTotal);
                            fQuery.CurrShowID = showID;
                            fQuery.DelegateID = delegateID;
                            fQuery.GoupRegID = groupID;
                            string template = oControl.GetDelegateOrderedListWithTemplate(fQuery, ref subTotal, InvID);
                            ConfDelegateList.PValue = template;
                        }
                        catch { }
                    }


                    Pramz ConfDelegateListECG = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.ConferenceList_ECG);
                    if (ConfDelegateListECG != null)
                    {
                        try
                        {
                            //    string url = HttpContext.Current.Request.Url.AbsoluteUri;
                            OrderControler oControl = new OrderControler(fn);
                            string subTotal = "";
                            FlowURLQuery fQuery = new FlowURLQuery(subTotal);
                            fQuery.CurrShowID = showID;
                            fQuery.DelegateID = delegateID;
                            fQuery.GoupRegID = groupID;
                            string template = oControl.GetOrderedListWithTemplate_ECG(fQuery, ref subTotal, InvID);
                            ConfDelegateListECG.PValue = template;
                        }
                        catch { }
                    }
                    Pramz ConfDelegateListMTLS = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.ConferenceList_MTLS);
                    if (ConfDelegateListMTLS != null)
                    {
                        try
                        {
                            //    string url = HttpContext.Current.Request.Url.AbsoluteUri;
                            OrderControler oControl = new OrderControler(fn);
                            string subTotal = "";
                            FlowURLQuery fQuery = new FlowURLQuery(subTotal);
                            fQuery.CurrShowID = showID;
                            fQuery.DelegateID = delegateID;
                            fQuery.GoupRegID = groupID;
                            string template = oControl.GetOrderedListWithTemplate_MTLS(fQuery, ref subTotal, InvID);
                            ConfDelegateListMTLS.PValue = template;
                        }
                        catch { }
                    }
                    Pramz conferenceNameField = eJObj.Pramz.FirstOrDefault(x => x.Key == "ConferenceName");
                    if (conferenceNameField != null)
                    {
                        string subTotal = "";
                        FlowURLQuery fQuery = new FlowURLQuery(subTotal);
                        fQuery.CurrShowID = showID;
                        fQuery.DelegateID = delegateID;
                        fQuery.GoupRegID = groupID;
                        string selectedConferenceNames = getConferencesByInvoiceID(InvID, fQuery, true, false);
                        if (!string.IsNullOrEmpty(selectedConferenceNames))
                            conferenceNameField.PValue = selectedConferenceNames;
                    }
                    Pramz conferenceQtyField = eJObj.Pramz.FirstOrDefault(x => x.Key == "ConferenceQty");
                    if (conferenceQtyField != null)
                    {
                        string subTotal = "";
                        FlowURLQuery fQuery = new FlowURLQuery(subTotal);
                        fQuery.CurrShowID = showID;
                        fQuery.DelegateID = delegateID;
                        fQuery.GoupRegID = groupID;
                        string selectedConferenceQty = getConferencesByInvoiceID(InvID, fQuery, false, true);
                        if (!string.IsNullOrEmpty(selectedConferenceQty))
                            conferenceQtyField.PValue = selectedConferenceQty;
                        if (conferenceQtyField.PValue == "0") conferenceQtyField.PValue = "";// Remove Zero
                    }
                    Pramz payMethod = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.PayMethod);
                    if (payMethod != null)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(InvID))
                            {
                                InvoiceControler iControl = new InvoiceControler(fn);
                                string method = iControl.GetInvoicePaymentMethod(showID, InvID);
                                payMethod.PValue = method;
                            }

                        }
                        catch { }
                    }
                    Pramz PayStatus = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.InvPayStatus);
                    if (PayStatus != null)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(InvID))
                            {
                                InvoiceControler iControl = new InvoiceControler(fn);
                                string pStatus = iControl.GetInvoicePaymentStatus(showID, InvID);
                                PayStatus.PValue = pStatus;
                            }
                        }
                        catch { }
                    }
                    Pramz prmoCode = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.PromoCode);
                    if (prmoCode != null)
                    {
                        try
                        {
                            prmoCode.PValue = GetUsedPromocode(showID, groupID);

                        }
                        catch { }
                    }

                    #region added on 26-7-2018
                    Pramz InvNo = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.InvoiceNo);
                    if (InvNo != null)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(InvID))
                            {
                                InvoiceControler iControl = new InvoiceControler(fn);
                                Invoice invObj = iControl.GetInvoiceByID(InvID);
                                if (invObj != null)
                                {
                                    InvNo.PValue = invObj.InvoiceNo;
                                }
                            }
                        }
                        catch { }
                    }
                    Pramz OrderNo = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.OrderNo);
                    if (OrderNo != null)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(InvID))
                            {
                                OrderControler oControl = new OrderControler(fn);
                                List<Order> lstOrder = oControl.GetAllOrderByInvoiceID(groupID, delegateID, InvID);
                                if (lstOrder.Count > 0)
                                {
                                    if (lstOrder[0] != null)
                                    {
                                        string showprefix = "";
                                        SiteSettings st = new SiteSettings(fn, showID);
                                        showprefix = st.SitePrefix;
                                        OrderNo.PValue = showprefix + lstOrder[0].OrderNo;
                                    }
                                }
                            }
                        }
                        catch { }
                    }


                    #endregion

                    Pramz atvLLink = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.ActivationURL);
                    if (atvLLink != null)
                    {
                        try
                        {
                            atvLLink.PValue = GetActivationURL(showID, flowID, groupID, delegateID);

                        }
                        catch { }
                    }
                    Pramz invDate = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.InvoiceDate);
                    if (invDate != null)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(InvID))
                            { 
                                InvoiceControler iControl = new InvoiceControler(fn);
                                Invoice inv = iControl.GetInvoiceByID(InvID);
                                string date = inv.InvoiceDate;
                                date = DateTime.Parse(date).ToString("dd/MM/yyyy");
                                invDate.PValue = date;
                            }

                        }
                        catch { }
                    }
                    Pramz otpCode = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.OTPCode);
                    if (otpCode != null)
                    {
                        try
                        {
                            otpCode.PValue = GetOTPCode(regno);

                        }
                        catch { }
                    }
                    Pramz invPayAmt = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.InvPaidAmount);
                    if (invPayAmt != null)
                    {
                        try
                        {
                            invPayAmt.PValue = GetInvPayAmount(sourcKey);

                        }
                        catch { }
                    }

                    Pramz loginContact = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.LogInContactPwd);
                    if (loginContact != null)
                    {
                        try
                        {
                            loginContact.PValue = GetLoginPassowrd(showID, groupID);

                        }
                        catch { }
                    }
                    Pramz loginDGate = eJObj.Pramz.FirstOrDefault(x => x.Key == EmailDefaultParamValue.LogInDelegatePwd);
                    if (loginDGate != null)
                    {
                        try
                        {
                            loginDGate.PValue = GetLoginPassowrd(showID, delegateID);

                        }
                        catch { }
                    }
                }



                // isOK = CallSendEmailApi(json);

            }
            rtnJson = JsonConvert.SerializeObject(eJObj);
        }
        catch { rtnJson = ""; }

        return rtnJson;
    }


    public bool CallSendEmailApi(string jsonStr)
    {
        bool isOK = false;
        try
        {
            string rtnValue = PostEmailPortalApi(jsonStr, "MailRequest");
            if (!string.IsNullOrEmpty(rtnValue) && rtnValue != "ERROR")
            {
                ePoratalStatusJsonData rtnConfig = JsonConvert.DeserializeObject<ePoratalStatusJsonData>(rtnValue);
                if (!string.IsNullOrEmpty(rtnConfig.Status) && rtnConfig.Status.ToUpper() != "ERROR")
                    isOK = true;
                else
                    isOK = false;
            }

        }
        catch (Exception)
        {
            isOK = false;
        }
        return isOK;
    }

    public string CreateShowMasterInPortal(string showDesc, string createBy)
    {
        string ePortalShowRefID = "";
        try
        {
            ePoratalShowJsonData dat = new ePoratalShowJsonData();
            dat.Desc = showDesc;
            dat.CreateBy = createBy;
            string jsonStr = JsonConvert.SerializeObject(dat);
            string rtnValue = PostEmailPortalApi(jsonStr, "CreateShowMaster");
            if (!string.IsNullOrEmpty(rtnValue) && rtnValue != "ERROR")
            {
                ePoratalStatusJsonData rtnConfig = JsonConvert.DeserializeObject<ePoratalStatusJsonData>(rtnValue);
                if (!string.IsNullOrEmpty(rtnConfig.Status) && rtnConfig.Status.ToUpper() != "ERROR")
                    ePortalShowRefID = rtnConfig.Status;
                else
                    ePortalShowRefID = "ERROR";
            }
        }
        catch (Exception)
        {
            ePortalShowRefID = "ERROR";

        }
        return ePortalShowRefID;

    }


    public bool SetConfigSettings(ePoratalShowConfigJsonData showConfig)
    {
        bool isOk = false;
        try
        {
            string jsonStr = JsonConvert.SerializeObject(showConfig);
            string rtnValue = PostEmailPortalApi(jsonStr, "ShowConfigAdd");
            if (!string.IsNullOrEmpty(rtnValue) && rtnValue != "ERROR")
            {
                ePoratalStatusJsonData rtnConfig = JsonConvert.DeserializeObject<ePoratalStatusJsonData>(rtnValue);
                if (!string.IsNullOrEmpty(rtnConfig.Status) && rtnConfig.Status.ToUpper() == "OK")
                    isOk = true;
            }
        }
        catch (Exception ex)
        {
            isOk = false;
        }
        return isOk;
    }
    public string SetupEmailConfig(ePorataSetuplEmailJsonData showConfig)
    {
        string rtnVal = "";
        try
        {
            string jsonStr = JsonConvert.SerializeObject(showConfig);
            string rtnValue = PostEmailPortalApi(jsonStr, "SetupEmail");
            if (!string.IsNullOrEmpty(rtnValue) && rtnValue != "ERROR")
            {
                ePoratalStatusJsonData rtnConfig = JsonConvert.DeserializeObject<ePoratalStatusJsonData>(rtnValue);
                if (!string.IsNullOrEmpty(rtnConfig.Status) && rtnConfig.Status != "ERROR")
                    rtnVal = rtnConfig.Status;
            }
        }
        catch (Exception ex)
        {
            rtnVal = "";
        }
        return rtnVal;
    }


    public List<Attachz> AddEmailAttachment(EmailDSourceKey sourcKey, string emailID)
    {
        List<Attachz> rtnist = new List<Attachz>();
        try
        {
            //CheckEmailByCondition
            Functionality fn = new Functionality();
            SendEmailHelper sHelp = new SendEmailHelper(fn);
            string showID = sourcKey.DSourceKeyList["ShowID"];
            List<SiteEmailAttachment> sEAttList = sHelp.FetchAttachmentTableList(showID, emailID);
            string refid = "";
            bool addToList = true;

            foreach (SiteEmailAttachment sAtt in sEAttList)
            {
                if (string.IsNullOrEmpty(sAtt.AttachCondition))
                {
                    addToList = true;
                    if (!string.IsNullOrEmpty(sAtt.AttachCondition))
                        addToList = CheckEmailByCondition(sAtt.AttachCondition, sourcKey, ref refid, false);
                    if (addToList)
                    {
                        //  if (sAtt.AttachType == SiteEmailAttachmentDefaultValue.FileType)
                        {
                            Attachz att = new Attachz();
                            att.Path = sAtt.FilePath;
                            rtnist.Add(att);
                        }
                    }
                }
                else
                {
                    //is Invoice 
                    string invoiceID = "";
                    string regno = sourcKey.DSourceKeyList["Regno"];
                    string groupID = sourcKey.DSourceKeyList["RegGroupID"];

                    if (string.IsNullOrEmpty(regno))
                        regno = groupID;

                    if (sourcKey.DSourceKeyList.ContainsKey("INVID"))
                        invoiceID = sourcKey.DSourceKeyList["INVID"];

                    CommonFuns cFuz = new CommonFuns();
                    HTMLTemplateControler htmlControl = new HTMLTemplateControler();
                    string filePath = "";

                    if (sAtt.AttachType == EmailHTMLTemlateType.INVOICE)
                    {
                        string template = htmlControl.CreateInvoiceReceiptHTMLTemplate(showID, regno, invoiceID, EmailHTMLTemlateType.INVOICE);
                        template = HttpContext.Current.Server.HtmlDecode(template);
                        string fileName = GetPdfFileName(regno, sourcKey, EmailHTMLTemlateType.INVOICE);
                        string rtnFile = htmlControl.CreatePDF(template, showID, fileName, "InvoiceLetter", false);
                        if (!string.IsNullOrEmpty(rtnFile))
                            filePath = rtnFile;
                    }
                    else if (sAtt.AttachType == EmailHTMLTemlateType.RECEIPT)
                    {
                        string template = htmlControl.CreateInvoiceReceiptHTMLTemplate(showID, regno, invoiceID, EmailHTMLTemlateType.RECEIPT);
                        template = HttpContext.Current.Server.HtmlDecode(template);
                        string fileName = GetPdfFileName(regno, sourcKey, EmailHTMLTemlateType.RECEIPT);
                        string rtnFile = htmlControl.CreatePDF(template, showID, fileName, "ReceiptLetter", false);
                        if (!string.IsNullOrEmpty(rtnFile))
                            filePath = rtnFile;
                    }
                    else if (sAtt.AttachType == EmailHTMLTemlateType.Confirmation)
                    {
                        string template = htmlControl.CreateInvoiceReceiptHTMLTemplate(showID, regno, invoiceID, EmailHTMLTemlateType.Confirmation);
                        template = HttpContext.Current.Server.HtmlDecode(template);
                        string fileName = GetPdfFileName(regno, sourcKey, EmailHTMLTemlateType.Confirmation);
                        string rtnFile = htmlControl.CreatePDF(template, showID, fileName, "ConfirmationLetter", false);
                        if (!string.IsNullOrEmpty(rtnFile))
                            filePath = rtnFile;
                    }
                    else if (sAtt.AttachType == EmailHTMLTemlateType.GMemberConfirmation)
                    {
                        string template = htmlControl.CreateInvoiceReceiptHTMLTemplate(showID, regno, invoiceID, EmailHTMLTemlateType.GMemberConfirmation);

                        template = HttpContext.Current.Server.HtmlDecode(template);
                        string fileName = GetPdfFileName(regno, sourcKey, EmailHTMLTemlateType.GMemberConfirmation);
                        string rtnFile = htmlControl.CreatePDF(template, showID, fileName, "ConfirmationLetter", false);
                        if (!string.IsNullOrEmpty(rtnFile))
                            filePath = rtnFile;
                    }
                    if (!string.IsNullOrEmpty(filePath))
                    {
                        Attachz att = new Attachz();
                        att.Path = filePath;
                        rtnist.Add(att);
                    }
                }
            }
        }
        catch { }
        return rtnist;
    }

    #endregion
    #region Get
    public ePoratalShowConfigJsonData GetShowConfigSettings(ePoratalShowConfigJsonData showConfig)
    {
        ePoratalShowConfigJsonData rtnConfig = new ePoratalShowConfigJsonData();
        try
        {
            string jsonStr = JsonConvert.SerializeObject(showConfig);
            string rtnValue = PostEmailPortalApi(jsonStr, "ShowConfigGet");
            if (!string.IsNullOrEmpty(rtnValue) && rtnValue != "ERROR")
            {
                rtnConfig = JsonConvert.DeserializeObject<ePoratalShowConfigJsonData>(rtnValue);
            }
        }
        catch (Exception ex) { }
        return rtnConfig;
    }
    public List<ePortalEmailTemplateDetail> GetEmailTemplateList(ePortalEmailMaster eMaster)
    {
        List<ePortalEmailTemplateDetail> tempList = new List<ePortalEmailTemplateDetail>();
        try
        {
            string jsonStr = JsonConvert.SerializeObject(eMaster);
            string rtnValue = PostEmailPortalApi(jsonStr, "GetEmailTemplateList");
            if (!string.IsNullOrEmpty(rtnValue) && rtnValue != "ERROR")
            {
                tempList = JsonConvert.DeserializeObject<List<ePortalEmailTemplateDetail>>(rtnValue);
            }
        }
        catch (Exception ex) { }
        return tempList;
    }


    #endregion

    public string PostEmailPortalApi(string jData, string callPage)
    {
        string rtnData = "";
        try
        {
            string jsonStr = jData;

            if (ConfigurationManager.AppSettings["EmailPortalURL"] != null)
            {
                string url = ConfigurationManager.AppSettings["EmailPortalURL"].ToString() + "/" + callPage;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(jsonStr);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                string status = "";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    rtnData = streamReader.ReadToEnd();
                }
            }
        }
        catch (Exception ex) { rtnData = "ERROR"; }
        return rtnData;
    }

    public string GetCountry(string countryID, string showID)
    {
        string rtnCountry = "";
        try
        {
            rtnCountry = "";
            Functionality fn = new Functionality();
            CountryObj cObj = new CountryObj(fn);
            rtnCountry = cObj.getCountryNameByID(countryID, showID);
            if (countryID == "0") rtnCountry = "";
        }
        catch { }

        return rtnCountry;
    }

    public string GetLastestInvoiceID(EmailDSourceKey sourcKey)
    {
        string invID = "";
        try
        {
            string showID = sourcKey.DSourceKeyList["ShowID"];
            string groupID = sourcKey.DSourceKeyList["RegGroupID"];
            string delegateID = sourcKey.DSourceKeyList["Regno"];
            string flowID = sourcKey.DSourceKeyList["FlowID"];
            Functionality fn = new Functionality();
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = delegateID;
            if (string.IsNullOrEmpty(delegateID)) invOwnerID = groupID;
            Invoice invLatest = invControler.GetLatestCompleteInvoice(invOwnerID, groupID);
            invID = invLatest.InvoiceID;
        }
        catch { }

        return invID;
    }

    #region Check
    public bool CheckEmailByCondition(string condition, EmailDSourceKey sourcKey, ref string RefID, bool invOwnedByGroup)
    {
        bool ToSend = false;
        try
        {
            Functionality fn = new Functionality();
            if (string.IsNullOrEmpty(condition))
                ToSend = true;
            else
            {
                string showID = sourcKey.DSourceKeyList["ShowID"];
                string groupID = sourcKey.DSourceKeyList["RegGroupID"];
                string delegateID = sourcKey.DSourceKeyList["Regno"];
                string flowID = sourcKey.DSourceKeyList["FlowID"];
                if (condition == EmailSendConditionType.Visa)
                {
                    //check VIA
                    if (IsChosenVisa(sourcKey)) //Visa Check box is choosen
                    {

                        SendEmailHelper emailHelper = new SendEmailHelper(fn);
                        bool isSent = emailHelper.CheckEmailSentEmailType(showID, flowID, EmailSendConditionType.Visa, delegateID); //check already sent visa Email
                        if (!isSent)
                            ToSend = true;
                    }

                }
                if (condition == EmailSendConditionType.PAID || condition == EmailSendConditionType.UNPAID
                    || condition == EmailSendConditionType.PaymentCancel
                    || condition == EmailSendConditionType.PaymentAfterConference
                    )
                {
                    InvoiceControler invControler = new InvoiceControler(fn);
                    string invOwnerID = delegateID;
                    if (string.IsNullOrEmpty(delegateID) || invOwnedByGroup) invOwnerID = groupID;
                    Invoice invLatest = invControler.GetLatestCompleteInvoice(invOwnerID, groupID);


                    if (condition == EmailSendConditionType.PAID)
                    {
                        //Check Invoic Status is Paid
                        string invSuccess = ((int)StatusValue.Success).ToString();
                        if (invLatest.Status == invSuccess)
                        {
                            RefID = invLatest.InvoiceID;
                            ToSend = true;
                        }
                    }
                    else if (condition == EmailSendConditionType.UNPAID)
                    {
                        //Check Invoic Status is UnPaid
                        // string invPending = ((int)StatusValue.Pending).ToString();
                        if (invLatest.Status == "0" || invLatest.Status == "2" || invLatest.Status == "4")
                        {
                            // Send Pending
                            RefID = invLatest.InvoiceID;
                            ToSend = true;
                        }
                    }
                    else if (condition == EmailSendConditionType.PaymentCancel)
                    {
                        //Check Invoic Status is UnPaid
                        // string invPending = ((int)StatusValue.Pending).ToString();
                        if (invLatest.Status == "5")
                        {
                            // Send Cancel
                            RefID = invLatest.InvoiceID;
                            ToSend = true;
                        }
                    }
                    else if (condition == EmailSendConditionType.PaymentAfterConference)
                    {
                        //Check Invoic Status is UnPaid
                        // string invPending = ((int)StatusValue.Pending).ToString();
                        if (invLatest.Status == "8")
                        {
                            // Send Cancel
                            RefID = invLatest.InvoiceID;
                            ToSend = true;
                        }
                    }
                }
                if (condition == EmailSendConditionType.REJECT || condition == EmailSendConditionType.CONFIRM || condition == EmailSendConditionType.APPROVE || condition == EmailSendConditionType.VIP || condition == EmailSendConditionType.DECLINE)
                {
                    if (condition == EmailSendConditionType.REJECT)
                    {
                        string reject = ((int)RegApproveRejectStatus.Rejected).ToString();
                        string status = GetRegApprovedOrRejectStatus(sourcKey);
                        if (status == reject)
                            ToSend = true;
                    }
                    else if (condition == EmailSendConditionType.CONFIRM)
                    {
                        string approve = ((int)RegApproveRejectStatus.Confirmed).ToString();
                        string status = GetRegApprovedOrRejectStatus(sourcKey);
                        if (status == approve)
                            ToSend = true;
                    }
                    else if (condition == EmailSendConditionType.APPROVE)
                    {
                        string approve = ((int)RegApproveRejectStatus.Approved).ToString();
                        string status = GetRegApprovedOrRejectStatus(sourcKey);
                        if (status == approve)
                            ToSend = true;
                    }
                    else if (condition == EmailSendConditionType.VIP)
                    {
                        string approve = ((int)RegApproveRejectStatus.VIP).ToString();
                        string status = GetRegApprovedOrRejectStatus(sourcKey);
                        if (status == approve)
                            ToSend = true;
                    }
                    else if (condition == EmailSendConditionType.DECLINE)
                    {
                        string approve = ((int)RegApproveRejectStatus.Declined).ToString();
                        string status = GetRegApprovedOrRejectStatus(sourcKey);
                        if (status == approve)
                            ToSend = true;
                    }
                }
            }
        }
        catch { }
        return ToSend;
    }
    #region EmailConditionCheck
    public bool IsChosenVisa(EmailDSourceKey sourcKey)
    {
        bool isChosen = false;
        try
        {
            Functionality fn = new Functionality();
            RegDelegateObj regObj = new RegDelegateObj(fn);
            string showID = sourcKey.DSourceKeyList["ShowID"];
            string groupID = sourcKey.DSourceKeyList["RegGroupID"];
            string delegateID = sourcKey.DSourceKeyList["Regno"];
            string flowID = sourcKey.DSourceKeyList["FlowID"];

            DataTable dDelegate = regObj.getDataByGroupIDRegno(groupID, delegateID, showID);
            if (dDelegate.Rows.Count > 0)
            {
                string visaStatus = dDelegate.Rows[0]["reg_approveStatus"].ToString();
                if (visaStatus == "1")
                    isChosen = true;
            }
        }
        catch { }

        return isChosen;
    }
    public string GetRegApprovedOrRejectStatus(EmailDSourceKey sourcKey)
    {
        string status = "";
        try
        {
            Functionality fn = new Functionality();
            RegDelegateObj regObj = new RegDelegateObj(fn);
            string showID = sourcKey.DSourceKeyList["ShowID"];
            string groupID = sourcKey.DSourceKeyList["RegGroupID"];
            string delegateID = sourcKey.DSourceKeyList["Regno"];
            string flowID = sourcKey.DSourceKeyList["FlowID"];

            DataTable dDelegate = regObj.getDataByGroupIDRegno(groupID, delegateID, showID);
            if (dDelegate.Rows.Count > 0)
            {
                status = dDelegate.Rows[0]["reg_approveStatus"].ToString();

            }
        }
        catch { }

        return status;
    }
    #endregion
    public bool SendEmailFromBackend(string EmailType, EmailDSourceKey sourcKey, string emailRegType, string updatedUser = null)
    {
        bool isOK = false;
        try
        {
            Functionality fn = new Functionality();
            CommonFuns cFun = new CommonFuns();
            SendEmailHelper emailHelper = new SendEmailHelper(fn);
            FlowEmail fEmail = new FlowEmail();
            List<CC> ccList = new List<CC>();
            string status = "";


            string showID = sourcKey.DSourceKeyList["ShowID"];
            string flowID = sourcKey.DSourceKeyList["FlowID"];
            string groupID = sourcKey.DSourceKeyList["RegGroupID"];
            string delegateID = sourcKey.DSourceKeyList["Regno"];

            fEmail = emailHelper.GetFlowEmailByEmailType(showID, flowID, EmailType, emailRegType);
            if (!string.IsNullOrEmpty(fEmail.EmailID))
            {
                string strJson = MakeEmailJsonString(fEmail, sourcKey, ccList);
                bool sentSuccess = false;
                if (!string.IsNullOrEmpty(strJson))
                {
                    sentSuccess = SendEmail(sourcKey, strJson);
                }
                else
                    status = "F";



                string refRegno = delegateID;
                if (sentSuccess)
                {
                    status = "S";
                    isOK = true;
                }

                if (string.IsNullOrEmpty(refRegno)) refRegno = groupID;
                EmailLog eLog = new EmailLog();
                eLog.ShowID = fEmail.ShowID;
                eLog.FlowID = flowID;
                eLog.EmailID = fEmail.EmailID;
                eLog.RefRegno = refRegno;
                eLog.EmailType = fEmail.EmailType;
                eLog.ESentBy = refRegno;
                eLog.EContent = strJson;
                eLog.Status = status;
                eLog.ESentBy = "Admin " + updatedUser;
                emailHelper.LogEmail(eLog);
            }


        }
        catch (Exception ex) { }
        return isOK;
    }


    public bool SendEmailFromBackendByCondition(string emailCondition, EmailDSourceKey sourcKey, string emailRegType, string updatedUser = null)
    {
        bool isOK = false;
        try
        {
            Functionality fn = new Functionality();
            CommonFuns cFun = new CommonFuns();
            SendEmailHelper emailHelper = new SendEmailHelper(fn);
            FlowEmail fEmail = new FlowEmail();
            List<CC> ccList = new List<CC>();
            string status = "";


            string showID = sourcKey.DSourceKeyList["ShowID"];
            string flowID = sourcKey.DSourceKeyList["FlowID"];
            string groupID = sourcKey.DSourceKeyList["RegGroupID"];
            string delegateID = sourcKey.DSourceKeyList["Regno"];

            fEmail = emailHelper.GetFlowEmailByEmailCondition(showID, flowID, emailCondition, emailRegType);
            if (!string.IsNullOrEmpty(fEmail.EmailID))
            {
                string strJson = MakeEmailJsonString(fEmail, sourcKey, ccList);
                bool sentSuccess = false;
                if (!string.IsNullOrEmpty(strJson))
                {
                    sentSuccess = SendEmail(sourcKey, strJson);
                }
                else
                    status = "F";



                string refRegno = delegateID;
                if (sentSuccess)
                {
                    status = "S";
                    isOK = true;
                }

                if (string.IsNullOrEmpty(refRegno)) refRegno = groupID;
                EmailLog eLog = new EmailLog();
                eLog.ShowID = fEmail.ShowID;
                eLog.FlowID = flowID;
                eLog.EmailID = fEmail.EmailID;
                eLog.RefRegno = refRegno;
                eLog.EmailType = fEmail.EmailType;
                eLog.ESentBy = refRegno;
                eLog.EContent = strJson;
                eLog.Status = status;
                eLog.ESentBy = "Admin " + updatedUser;
                emailHelper.LogEmail(eLog);
            }


        }
        catch (Exception ex) { }
        return isOK;
    }
    #endregion

    string adminRoot = "/admin";
    public string GetRootURL()
    {
        string url = HttpContext.Current.Request.Url.AbsoluteUri;
        if (!string.IsNullOrEmpty(url))
        {
            if (url.LastIndexOf("?") >= 0)
                url = url.Substring(0, url.LastIndexOf("?"));
            url = url.ToLower();
            url = url.Substring(0, url.LastIndexOf("/"));
            bool isAdmin = Regex.IsMatch(url, adminRoot);
            if (isAdmin)
                url = url.Replace(adminRoot, "");


        }

        return url;
    }

    public string GetUsedPromocode(string showIO, string groupID)
    {

        string rtnPromo = "";
        try
        {
            string sql = "select * from tb_User_Promo where  ShowID=@show and Regno=@Grp";
            List<SqlParameter> spList = new List<SqlParameter>();
            SqlParameter spShow = new SqlParameter();
            spShow.SqlDbType = SqlDbType.NVarChar;
            spShow.Value = showIO;
            spShow.ParameterName = "show";
            SqlParameter spGroup = new SqlParameter();
            spGroup.SqlDbType = SqlDbType.NVarChar;
            spGroup.Value = groupID;
            spGroup.ParameterName = "Grp";
            spList.Add(spShow);
            spList.Add(spGroup);
            Functionality fn = new Functionality();
            DataTable dt = fn.GetDatasetByCommand(sql, "DT", spList).Tables["DT"];
            if (dt.Rows.Count > 0)
            {
                rtnPromo = dt.Rows[0]["Promo_Code"].ToString();
            }
        }
        catch { }
        return rtnPromo;
    }

    public string GetActivationURL(string showId, string flowID, string grupID, string regNO)
    {
        string rtn = "";
        try
        {
            Functionality fn = new Functionality();
            ActivationControler act = new ActivationControler(fn);
            UserActivation atvData = new UserActivation();
            atvData.ShowID = showId;
            atvData.FlowID = flowID;
            atvData.GroupID = grupID;
            if (!string.IsNullOrEmpty((regNO)))
                atvData.RefNo = regNO;
            else
                atvData.RefNo = grupID;
            string code = act.GenerateActivationCode(atvData);
            if (!string.IsNullOrEmpty(code))
            {
                if (ConfigurationManager.AppSettings["ActivationURL"] != null)
                {
                    string url = ConfigurationManager.AppSettings["ActivationURL"].ToString();
                    url = url.Trim('/');
                    CommonFuns cFun = new CommonFuns();
                    showId = cFun.EncryptValue(showId);
                    flowID = cFun.EncryptValue(flowID);
                    grupID = cFun.EncryptValue(grupID);
                    regNO = cFun.EncryptValue(regNO);
                    code = cFun.EncryptValue(code);
                    url = string.Format("{0}/{1}?SHW={2}&FLW={3}&GRP={4}&INV={5}&ATV={6}", url, "Activation", showId, flowID, grupID, regNO, code);

                    rtn = url;
                }
            }
        }
        catch { }
        return rtn;

    }

    private string GetCustomSubject(string subj, EmailDSourceKey sourcKey)
    {

        string rtnVal = "";
        try
        {
            if (string.IsNullOrEmpty(subj))
                return rtnVal;

            string showID = sourcKey.DSourceKeyList["ShowID"];
            string groupID = sourcKey.DSourceKeyList["RegGroupID"];
            string delegateID = sourcKey.DSourceKeyList["Regno"];
            string flowID = sourcKey.DSourceKeyList["FlowID"];
            string InvID = "";
            if (sourcKey.DSourceKeyList.ContainsKey("INVID"))
                InvID = sourcKey.DSourceKeyList["INVID"];

            rtnVal = subj;
            rtnVal = rtnVal.Replace("{RegNo}", delegateID);
            rtnVal = rtnVal.Replace("{GroupNo}", groupID);
            rtnVal = rtnVal.Replace("{InvNo}", groupID);
            rtnVal = rtnVal.Replace("{InvID}", InvID);

            Functionality fn = new Functionality();
            RegDelegateObj rgdObj = new RegDelegateObj(fn);
            DataTable dt = rgdObj.getDataByGroupIDRegno(groupID, delegateID, showID);
            if (dt.Rows.Count > 0)
            {
                rtnVal = rtnVal.Replace("{reg_FName}", dt.Rows[0]["reg_FName"].ToString());
                rtnVal = rtnVal.Replace("{reg_LName}", dt.Rows[0]["reg_LName"].ToString());
                rtnVal = rtnVal.Replace("{reg_OName}", dt.Rows[0]["reg_OName"].ToString());
            }
        }
        catch { }

        return rtnVal;
    }

    #region AccompanyPerson
    private void EmailSendToAccompanyPerson(FlowEmail fEmail, EmailDSourceKey sourceKey)
    {
        string refID = "";
        string emailCondition = fEmail.EmailSendCondition;
        bool toSendEmail = true;
        bool invOwnedByGroup = false;
        bool sentSuccess = false;
        string strJson = "";
        string status = "";
        string InvID = "";
        string showID = sourceKey.DSourceKeyList["ShowID"];
        string groupID = sourceKey.DSourceKeyList["RegGroupID"];
        string regno = sourceKey.DSourceKeyList["Regno"];

        List<CC> ccList = new List<CC>();
        if (!string.IsNullOrEmpty(emailCondition))
            toSendEmail = CheckEmailByCondition(emailCondition, sourceKey, ref refID, invOwnedByGroup);
        if (toSendEmail)
        {
            //Loop and send Accompany 
            if (sourceKey.DSourceKeyList.ContainsKey("INVID"))
                InvID = sourceKey.DSourceKeyList["INVID"];
            Functionality fn = new Functionality();
            OrderControler oControl = new OrderControler(fn);
            List<AccompanyingPersonInfo> accList = oControl.getAccompanyingPersonList(regno, groupID, showID, InvID);
            foreach (AccompanyingPersonInfo accItem in accList)
            {
                sourceKey.DSourceKeyList["Regno"] = accItem.Regno;
                strJson = MakeEmailJsonString(fEmail, sourceKey, ccList);
           
                if (!string.IsNullOrEmpty(strJson))
                {
                    sentSuccess = SendEmail(sourceKey, strJson);
                }
                else
                    status = "F";
            }
        }
    }
    #endregion

    private string GetOTPCode(string regno)
    {
        string rtnVal = "";
        try
        {
            OTPControler otp = new OTPControler();
            string grpID = regno;
            rtnVal = otp.GetOTP(grpID, 360);
        }
        catch { }
        return rtnVal;
    }

    private string GetPdfFileName(string keyID, EmailDSourceKey sourcKey, string templateType)
    {
        string rtn = keyID;
        string showID = sourcKey.DSourceKeyList["ShowID"];
        HTMLTemplateControler htmlControl = new HTMLTemplateControler();
        string tmp = htmlControl.GetTemplateFileName(showID, keyID, templateType);
        tmp = GetCustomSubject(tmp, sourcKey);
        if (!string.IsNullOrEmpty(tmp))
            rtn = tmp;
        return rtn;
    }

    #region bind
    public string bindSalutation(string showid, string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            Functionality fn = new Functionality();
            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getSalutationNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }

                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(name))
                {
                    name = otherValue;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion

    #region InvAmt
    private string GetInvPayAmount(EmailDSourceKey sourcKey)
    {
        string rtnAmt = "0.00";
        try
        {
            string showID = sourcKey.DSourceKeyList["ShowID"];
            string groupID = sourcKey.DSourceKeyList["RegGroupID"];
            string delegateID = sourcKey.DSourceKeyList["Regno"];
            string flowID = sourcKey.DSourceKeyList["FlowID"];
            string InvID = "";
            if (sourcKey.DSourceKeyList.ContainsKey("INVID"))
                InvID = sourcKey.DSourceKeyList["INVID"];
            Functionality fn = new Functionality();
            CommonFuns cFuz = new CommonFuns();
            PaymentControler pControl = new PaymentControler(fn);
            decimal totalPaidAmt = pControl.GetPaymentTotalPaidAmtByInvNo(InvID);
            rtnAmt = cFuz.FormatCurrency(totalPaidAmt.ToString());
        }
        catch { }

        return rtnAmt;
    }
    #endregion

    private string GetLoginPassowrd(string showID, string userID)
    {
        string rtnVal = "";
        Functionality fn = new Functionality();
        RegLoginPassword regLP = new RegLoginPassword(fn);
        rtnVal = regLP.getPasswordByOwnerID(userID, showID);


        return rtnVal;
    }

    private string OrderItemListForQRCode(EmailDSourceKey sourcKey)
    {
        Functionality fn = new Functionality();
        OrderControler oControl = new OrderControler(fn);
        string subTotal = "";
        FlowURLQuery fQuery = new FlowURLQuery(subTotal);
        string showID = sourcKey.DSourceKeyList["ShowID"];
        string groupID = sourcKey.DSourceKeyList["RegGroupID"];
        string delegateID = sourcKey.DSourceKeyList["Regno"];
        string flowID = sourcKey.DSourceKeyList["FlowID"];
        fQuery.CurrShowID = showID;
        fQuery.DelegateID = delegateID;
        fQuery.GoupRegID = groupID;
        fQuery.FlowID = flowID;
        string invID = "";
        if (sourcKey.DSourceKeyList.ContainsKey("INVID"))
            invID = sourcKey.DSourceKeyList["INVID"];
        else
            invID = GetLastestInvoiceID(sourcKey);

        List<OrderItemList> oList = oControl.GetAllOrderedListByInvoiceID(fQuery, invID);
        string itemList = "";
        foreach (OrderItemList iList in oList)
        {
            if (iList.OrderList != null)
            {
                foreach (OrderItem oItm in iList.OrderList)
                {
                    itemList += $"{oItm.ItemID}|";
                }
            }
        }
        itemList = itemList.Trim('|');

        return itemList;
    }

    #region ECC
    private string getOrderItemECC(EmailDSourceKey sourcKey, string type)
    {
        Functionality fn = new Functionality();
        OrderControler oControl = new OrderControler(fn);
        string subTotal = "";
        FlowURLQuery fQuery = new FlowURLQuery(subTotal);
        string showID = sourcKey.DSourceKeyList["ShowID"];
        string groupID = sourcKey.DSourceKeyList["RegGroupID"];
        string delegateID = sourcKey.DSourceKeyList["Regno"];
        string flowID = sourcKey.DSourceKeyList["FlowID"];
        fQuery.CurrShowID = showID;
        fQuery.DelegateID = delegateID;
        fQuery.GoupRegID = groupID;
        fQuery.FlowID = flowID;
        string invID = "";
        if (sourcKey.DSourceKeyList.ContainsKey("INVID"))
            invID = sourcKey.DSourceKeyList["INVID"];
        else
            invID = GetLastestInvoiceID(sourcKey);

        List<OrderItemList> oList = oControl.GetAllOrderedListByInvoiceID(fQuery, invID);
        string itemList = "";
        foreach (OrderItemList iList in oList)
        {
            if (iList.OrderList != null)
            {
                foreach (OrderItem oItm in iList.OrderList)
                {
                    if (type == "A")
                    {
                        string additionalAType = "SPARK";
                        if (oItm.ItemDescription.StartsWith(type) || oItm.ItemDescription.StartsWith(additionalAType))
                        {
                            itemList = $"{oItm.ItemDescription}";
                            break;
                        }
                    }
                    else
                    {
                        if (oItm.ItemDescription.StartsWith(type))
                        {
                            itemList = $"{oItm.ItemDescription}";
                            break;
                        }
                    }
                }
            }
        }
        itemList = itemList.Trim('|');

        return itemList;
    }
    #endregion

    #region Additional
    private string getConferencesByInvoiceID(string invoiceID, FlowURLQuery urlQuery, bool isConferneceName, bool isConferenceQty)//added on 14-1-2020 th
    {
        string result = "";
        string conferenceNames = "";
        int totalQty = 0;
        try
        {
            if (!string.IsNullOrEmpty(invoiceID))
            {
                Functionality fn = new Functionality();
                List<OrderItemList> oMasterList = new List<OrderItemList>();
                OrderControler oControl = new OrderControler(fn);
                oMasterList = oControl.GetAllOrderedListByInvoiceID(urlQuery, invoiceID);
                if (oMasterList != null)
                {
                    foreach (OrderItemList oList in oMasterList)
                    {
                        if (oList.OrderList.Count > 0)
                        {
                            foreach (OrderItem oItem in oList.OrderList)
                            {
                                string ItemDescription_ShowedInTemplate = oItem.ItemDescription_ShowedInTemplate;
                                conferenceNames += oItem.ItemDescription + (!string.IsNullOrEmpty(ItemDescription_ShowedInTemplate) ? ("<br/>" + ItemDescription_ShowedInTemplate) : "") + ";";
                                totalQty += oItem.Qty;
                            }
                        }
                    }
                }
            }

            if (isConferneceName)
            {
                result = conferenceNames.TrimEnd(';');
            }
            if (isConferenceQty)
            {
                result = totalQty.ToString();
            }
        }
        catch (Exception ex)
        { }

        return result;
    }
    #endregion
}

