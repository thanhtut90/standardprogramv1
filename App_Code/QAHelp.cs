﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net;
using System.IO;
using Corpit.Site.Utilities;
using Corpit.Site.Email;
using Corpit.Email;
using Corpit.Utilities;
using System.Data;
using Newtonsoft.Json;
using Corpit.Registration;
/// <summary>
/// Summary description for Functionality
/// </summary>
public class RtnQA
{
    public string data { get; set; }
    public string message { get; set; }
    public string Status { get; set; }
}

public class QAHelper
{

    public string GetValuefromQA(string query)
    {
        string rtnData = "";
        try
        {
            if (ConfigurationManager.AppSettings["QuestionnaireAPI"] != null)
            {
                string QALINK = ConfigurationManager.AppSettings["QuestionnaireAPI"].ToString() + "?" + query;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(QALINK);
                httpWebRequest.Headers.Add("x-ms-version", "2012-08-01");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/xml";
                httpWebRequest.ContentLength = 0;           
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    rtnData = streamReader.ReadToEnd();
                    RtnQA rtnValue = JsonConvert.DeserializeObject<RtnQA>(rtnData);
                    if (rtnValue.Status == "200")
                    {
                        rtnData = rtnValue.data;
                    }
                    else
                    {
                        rtnData = "";
                    }
                }
            }
        }
        catch { }

        return rtnData;
    }

    public bool DuplicateQA(string query)
    {
        bool isOK = false;
        try
        {
            if (ConfigurationManager.AppSettings["QuestionnaireAPI"] != null)
            {
                string QALINK = ConfigurationManager.AppSettings["QuestionnaireAPI"].ToString() + "?" + query;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(QALINK);
                httpWebRequest.Headers.Add("x-ms-version", "2012-08-01");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/xml";
                httpWebRequest.ContentLength = 0;
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                   string rtnData = streamReader.ReadToEnd();
                    RtnQA rtnValue = JsonConvert.DeserializeObject<RtnQA>(rtnData);
                    if (rtnValue.Status == "200")
                    {
                        isOK = true;
                    }
                    else
                    {
                        rtnData = "";
                    }
                }
            }
        }
        catch (Exception ex) { }

        return isOK;
    }

    public string GetQAIDfromFlow(string showID,string flowID, string type)
    {
        string rtnQAID = "";
        try
        {
            Functionality fn = new Functionality();
            QuestionnaireControler qCtr = new QuestionnaireControler(fn);        
            DataTable dt = qCtr.getQID(type, flowID, showID);
            if (dt.Rows.Count > 0)
            {
                rtnQAID = dt.Rows[0]["SQ_QAID"].ToString();
            }
        }
        catch { }
        return rtnQAID;
    }

    public bool checkExistQA(string query)
    {
        bool isOK = false;
        try
        {
            if (ConfigurationManager.AppSettings["QuestionnaireCheckExist"] != null)
            {
                string QALINK = ConfigurationManager.AppSettings["QuestionnaireCheckExist"].ToString() + "?" + query;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(QALINK);
                httpWebRequest.Headers.Add("x-ms-version", "2012-08-01");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/xml";
                httpWebRequest.ContentLength = 0;
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string rtnData = streamReader.ReadToEnd();
                    RtnQA rtnValue = JsonConvert.DeserializeObject<RtnQA>(rtnData);
                    if (rtnValue.Status == "1")
                    {
                        isOK = true;
                    }
                    else
                    {
                        rtnData = "";
                    }
                }
            }
        }
        catch { }

        return isOK;
    }
}