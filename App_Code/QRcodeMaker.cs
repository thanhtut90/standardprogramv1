﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net;
using System.IO;
using Corpit.Site.Utilities;
using Corpit.Site.Email;
using Corpit.Email;
using Corpit.Utilities;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Corpit.Registration;
/// <summary>
/// Summary description for Functionality
/// </summary>

public class QRParam
{
    public string ParamKey { get; set; } = "";
    public int FieldORder { get; set; } = 99;

    public QRParam()
    {

    }
}
public class QRConfigMaster
{
    public string QRID { get; set; } = "";
    public string ShowID { get; set; } = "";
    public string FlowID { get; set; } = "";
    public string FormType { get; set; } = "";
    public string DSource { get; set; } = "";
    public string QREncrytMethod { get; set; } = "";
    public string QREncrytKEY { get; set; } = "";

    public QRConfigMaster()
    {

    }
}
public class QRcodeMaker
{
    private static string _AFCShowID = "UEW390";
    private static string _AFCDelegateFlowID = "F458";
    private static string _AFCVisitorFlowID = "F459";
    private static string _MobileDemoShowID = "MEO397";
    public string CreateQRcode(string Type, string keyID, string showID, string flowID,string emailID, EmailDSourceKey sourcKey)
    {
        string rtnLink = "";
        try
        {
            if (Type == "D")
            {
                Functionality fn = new Functionality();
                //   RegDelegateObj regObj = new RegDelegateObj(fn);
                // DataTable dtReg = regObj.getRegDelegateByID(keyID, showID);
                string qrID = "";
                SiteEmailConfigHelper eCHelper = new SiteEmailConfigHelper(fn);

                FlowEmail fEmail = eCHelper.GetFlowEmail(showID, emailID);
                qrID = fEmail.QRID;

                if (string.IsNullOrEmpty(qrID))
                {
                    //Get From Template table

                    SiteHTMLTemplateControler htmlControl = new SiteHTMLTemplateControler(fn);
                    HTMLTemplate htmlTemplate = htmlControl.GetHtmlTemplateByID(showID, emailID);
                    qrID = htmlTemplate.QRID;
                }
                QRConfigMaster qCMaster = GetQRConfigMaster(qrID);
                if (!string.IsNullOrEmpty(qCMaster.DSource))
                {
                    SendEmailHelper emailHelper = new SendEmailHelper(fn);
                    string qrBaseDSource = qCMaster.DSource;
                    string sendTO = "";
                    DataTable dtReg = emailHelper.GetDataSource(qrBaseDSource, sourcKey, ref sendTO);
                    if (dtReg.Rows.Count > 0)
                    {
                        List<QRParam> qParamList = GetQRFieldList(Type, showID, flowID);
                        if (qParamList != null && qParamList.Count > 0)
                        {
                            List<QRParam> ordeList = qParamList.OrderBy(x => x.FieldORder).ToList();
                            string getValue = "";
                            string strQRList = "";
                            foreach (QRParam qParm in ordeList)
                            {
                                getValue = GetParamValueFromDB(qParm.ParamKey, ref dtReg, showID);
                                if (string.IsNullOrEmpty(getValue)) getValue = ".";
                                strQRList += getValue + "^";
                            }

                            if (!string.IsNullOrEmpty(strQRList))
                            {
                                /*added on 3-1-2020 for Mobile Demo conference registration*/
                                if (showID == _MobileDemoShowID)//for Mobile Demo conference registration
                                {
                                    //Make QR
                                    string frontprefix = "^IBEW^";

                                    string qrRegNO = keyID;
                                    SiteSettings sSetting = new SiteSettings(fn, showID);
                                    if (!string.IsNullOrEmpty(sSetting.SitePrefix))
                                        qrRegNO = sSetting.SitePrefix + "-" + keyID;

                                    strQRList = frontprefix + qrRegNO + "^" + strQRList.TrimEnd('^');

                                    // Enctrypt string 
                                    strQRList = EncrytQRStr(showID, flowID, qCMaster, strQRList);
                                    //End 
                                    strQRList = ConcatOrderedItemList(strQRList, sourcKey); // add OrderItemList in qr code
                                    jBQCode jData = new jBQCode();
                                    jData.BarCode = strQRList;
                                    jData.ShowName = showID;
                                    jData.Regno = keyID;
                                    rtnLink = MakeQRcode(jData);
                                }
                                else
                                {
                                    //Make QR
                                    string frontprefix = "^N^";

                                    strQRList = frontprefix + strQRList;
                                    string qrRegNO = keyID;
                                    SiteSettings sSetting = new SiteSettings(fn, showID);
                                    if (!string.IsNullOrEmpty(sSetting.SitePrefix))
                                        qrRegNO = sSetting.SitePrefix + "-" + keyID;

                                    strQRList += qrRegNO;

                                    /*added on 4-10-2019 for AFC onsite registration*/
                                    if (showID == _AFCShowID)
                                    {
                                        if (flowID == _AFCVisitorFlowID)
                                        {
                                            strQRList += "^Visitor^.)(.";
                                        }
                                        else if (flowID == _AFCDelegateFlowID)
                                        {
                                            string confItems = getAFCConference(showID, qrRegNO);
                                            strQRList += "^Delegate^" + confItems;
                                        }
                                    }
                                    /*end*/

                                    // Enctrypt string 
                                    strQRList = EncrytQRStr(showID, flowID, qCMaster, strQRList);
                                    //End 
                                    strQRList = ConcatOrderedItemList(strQRList, sourcKey); // add OrderItemList in qr code
                                    jBQCode jData = new jBQCode();
                                    jData.BarCode = strQRList;
                                    jData.ShowName = showID;
                                    jData.Regno = keyID;
                                    rtnLink = MakeQRcode(jData);
                                }
                            }
                        }
                    }
                }
            }
            else if (Type == "DReg")
            {
                string strQRList = "";
                string qrRegNO = keyID;
                strQRList += qrRegNO;
                jBQCode jData = new jBQCode();
                jData.BarCode = strQRList;
                jData.ShowName = showID;
                jData.Regno = keyID;
                rtnLink = MakeQRcode(jData);
            }
        }
        catch { }
        return rtnLink;
    }

    public string MakeQRcode(jBQCode jData)
    {
        string barcodeURL = "";
        try
        {
            string json = JsonConvert.SerializeObject(jData);
            BarcodeMaker bMaker = new BarcodeMaker();
            string rtnValue = PostBQrcodePortalApi(json, "MakeQRCode");

            if (rtnValue != null)
            {
                RtnBQCode rtnConfig = JsonConvert.DeserializeObject<RtnBQCode>(rtnValue);
                if (rtnConfig != null && !string.IsNullOrEmpty(rtnConfig.RTN_URL))
                    barcodeURL = rtnConfig.RTN_URL;
            }
        }
        catch { }
        return barcodeURL;
    }
    public string PostBQrcodePortalApi(string jData, string callPage)
    {
        string rtnData = "";
        try
        {
            string jsonStr = jData;

            if (ConfigurationManager.AppSettings["BQRCodeURL"] != null)
            {
                string url = ConfigurationManager.AppSettings["BQRCodeURL"].ToString() + "/" + callPage;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(jsonStr);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                string status = "";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    rtnData = streamReader.ReadToEnd();
                }
            }
        }
        catch (Exception ex) { rtnData = ""; }
        return rtnData;
    }

    private string GetParamValueFromDB(string paramKey ,ref DataTable dataSource,string showID)
    {
        string val = "";
        Pramz eParam = new Pramz();
        try
        {
            if (dataSource != null)
            {
                DataColumnCollection columns = dataSource.Columns;
     
                if (columns.Contains(paramKey))
                {
                    if (paramKey == "RG_Country" || paramKey == "reg_Country" || paramKey == "rc_country")
                    {
                        try
                        {
                            val = "";
                               Functionality fn = new Functionality();
                            CountryObj cObj = new CountryObj(fn);
                            val = dataSource.Rows[0][paramKey].ToString();
                            val = cObj.getCountryNameByID(val, showID);
                        }
                        catch { }
                    }
                    else
                        val = dataSource.Rows[0][paramKey].ToString();                
                }
            }
        }
        catch { }
        return val;
    }

    public List<QRParam> GetQRFieldList(string Type, string showID, string flowID)
    {
        List<QRParam> qList = new List<QRParam>();
        try
        {
            Functionality fn = new Functionality();
            CommonFuns cFuz = new CommonFuns();
            string sql = "select * from tb_site_flow_QRFieldList where FlowID=@Flow and ShowID=@Show and Form_type=@fType order by FieldOrder asc";
            SqlParameter spar = new SqlParameter("Flow", SqlDbType.NVarChar);
            spar.Value = flowID.ToString();
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar);

            SqlParameter ShowID = new SqlParameter("Show", SqlDbType.NVarChar);
            ShowID.Value = showID.ToString(); 
            pList.Add(ShowID);

            SqlParameter fType = new SqlParameter("fType", SqlDbType.NVarChar);
            fType.Value = Type.ToString();
            pList.Add(fType);
            DataTable dtQList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];
            for (int i = 0; i < dtQList.Rows.Count; i++)
            {
                QRParam qItem = new QRParam();
                qItem.ParamKey = dtQList.Rows[i]["FieldID"].ToString();
                qItem.FieldORder = cFuz.ParseInt(dtQList.Rows[i]["FieldOrder"].ToString());
                qList.Add(qItem);
            }
        }
        catch(Exception ex) { }

        return qList;
    }

    public QRConfigMaster GetQRConfigMaster(string qrID)
    {
        QRConfigMaster qMaster = new QRConfigMaster();
        try
        {
            Functionality fn = new Functionality();
            CommonFuns cFuz = new CommonFuns();
            string sql = "select  * from tb_site_flow_QRFieldMaster where QRID=@qrID";
            SqlParameter spar = new SqlParameter("qrID", SqlDbType.NVarChar);
            spar.Value = qrID.ToString();
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar);
            DataTable dtQList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];
            if (dtQList.Rows.Count > 0)
            {
                qMaster.ShowID = dtQList.Rows[0]["showid"].ToString();
                qMaster.FlowID = dtQList.Rows[0]["flowid"].ToString();
                qMaster.FormType = dtQList.Rows[0]["Form_type"].ToString();
                qMaster.DSource = dtQList.Rows[0]["DSource"].ToString();
                qMaster.QRID = dtQList.Rows[0]["QRID"].ToString();
                qMaster.QREncrytMethod = dtQList.Rows[0]["QREncrytMethod"].ToString();
                qMaster.QREncrytKEY = dtQList.Rows[0]["QREncrytKEY"].ToString(); 
            }
        }
        catch { }
        return qMaster;
    }

    private string ConcatOrderedItemList(string orgVal, EmailDSourceKey sourcKey)
    {
        string itemList = "";
        string rtnVal = "";
        if (sourcKey.DSourceKeyList.ContainsKey("OrderedItems"))
            itemList = sourcKey.DSourceKeyList["OrderedItems"];
        if (string.IsNullOrEmpty(itemList))
            return orgVal;

        rtnVal = $"{orgVal}^{itemList}";
        return rtnVal;
    }

    //QR Encryt

    private string EncrytQRStr(string showID, string flowID, QRConfigMaster qConfig, string plainText)
    {
        string rtn = "";
        try
        {

            //check QRcode EncryMethod
            switch (qConfig.QREncrytMethod)
            {

                case "ENCRYPT_CAESAR":
                    {
                        string tmp = plainText;
                        tmp = tmp.Replace("^N^", "").Trim();
                        tmp = "v" + tmp;
                        rtn = GetEncodeQRcode(tmp, qConfig.QREncrytKEY);
                        break;
                    }
                default: { rtn = plainText; break; }
            }

        }
        catch { }
        return rtn;
    }

    private string GetEncodeQRcode(string value, string keyVal)
    {
        CommonFuns cFuz = new CommonFuns();
        ///int KeyValue = 7;
        int KeyValue = cFuz.ParseInt(keyVal);
        int vLength = value.Length;
        string eCryptedValue = "";
        int tmpValue = 0;
        int asciValue = 0;
        foreach (char charValue in value)
        {
            asciValue = (int)charValue;
            int asciPlusKey = asciValue + KeyValue;
            char asciPlsuKeyChar = (char)asciPlusKey;
            if (asciPlusKey > 126)
            {
                tmpValue = (asciPlusKey - 127 + 32);
                char cValue = (char)tmpValue;
                if (Char.IsWhiteSpace(cValue))
                    eCryptedValue += ' ';
                else if (cValue == '!')
                    eCryptedValue += "!";
                else if (cValue == '"')
                    eCryptedValue += "\"";
                else if (cValue == '#')
                    eCryptedValue += "#";
                else if (cValue == '$')
                    eCryptedValue += "$";
                else if (cValue == '%')
                    eCryptedValue += "%";
                else
                    eCryptedValue += tmpValue;
            }
            else if (asciPlsuKeyChar == '"')
            {
                eCryptedValue += "\"";
            }
            else
                eCryptedValue += asciPlsuKeyChar;

        }
        return eCryptedValue;
    }

    #region AFC
    private string getAFCConference(string showid, string regno)
    {
        string afcConference = string.Empty;
        try
        {
            Functionality fn = new Functionality();
            string sqlMainConf = "Select * From tb_OrderDetials od Inner Join tb_ConfItem cf On od.ItemId=cf.con_itemId "
                        + " Where RegId='" + regno + "' And "
                        + " od.ItemType='M' And cf.con_itemId=1415 And cf.ShowID='" + showid + "'";
            DataTable dtMainConf = fn.GetDatasetByCommand(sqlMainConf, "ds").Tables[0];
            if (dtMainConf.Rows.Count > 0)
            {
                afcConference = "Full)(";
            }
            else
            {
                afcConference = ".)(";
            }
            string sqlPostConference = "Select SUBSTRING(LTRIM(ReportDisplayName),1,(CHARINDEX(' ',LTRIM(ReportDisplayName) + ' ')-1)) PostConferenceCode "
                        + " From tb_OrderDetials od Inner Join tb_ConfDependentItem cf On od.ItemId=cf.con_DependentID "
                        + " Where RegId='" + regno + "' And "
                        + " od.ItemType='D' And cf.con_itemId=1414 And cf.ShowID='" + showid + "'";
            string postConfCode = fn.GetDataByCommand(sqlPostConference, "PostConferenceCode");
            if (!string.IsNullOrEmpty(postConfCode) && postConfCode != "0")
            {
                afcConference += postConfCode;
            }
            else
            {
                afcConference += ".";
            }
        }
        catch (Exception ex)
        { }

        return afcConference;
    }
    #endregion
}

