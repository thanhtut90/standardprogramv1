﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
/// <summary>
/// Summary description for Functionality
/// </summary>
public class Functionality :BaseFunctionality
{
    public Functionality()
    {
        if (ConfigurationManager.ConnectionStrings["CMSConnString"] != null)
        {
            string constr = ConfigurationManager.ConnectionStrings["CMSConnString"].ConnectionString;
            base.ConnString = constr;
           
        }
    }
  
}

public class PaymentFunctionality : BaseFunctionality
{
    public PaymentFunctionality()
    {
        if (ConfigurationManager.ConnectionStrings["PaymentConnString"] != null)
        {
            string constr = ConfigurationManager.ConnectionStrings["PaymentConnString"].ConnectionString;
            base.ConnString = constr;

        }
    }
}

public class QuesFunctionality : BaseFunctionality
{
    public QuesFunctionality()
    {
        if (ConfigurationManager.ConnectionStrings["QCMSConnString"] != null)
        {
            string constr = ConfigurationManager.ConnectionStrings["QCMSConnString"].ConnectionString;
            base.ConnString = constr;

        }
    }


}
