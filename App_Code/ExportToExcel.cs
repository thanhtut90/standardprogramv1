﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Net;
using System.IO;
using ClosedXML.Excel;
/// <summary>
///ExportToExcel 的摘要说明
/// </summary>
public class ExportToExcel
{
    public static void DataTable3Excel(System.Data.DataTable dtData, String FileName)
    {
        System.Web.UI.WebControls.GridView dgExport = null;
     
        //当前对话 
        System.Web.HttpContext curContext = System.Web.HttpContext.Current;
        //IO用于导出并返回excel文件 
        System.IO.StringWriter strWriter = null;
        System.Web.UI.HtmlTextWriter htmlWriter = null;

        if (dtData != null)
        {
            //设置编码和附件格式 
            //System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8)作用是方式中文文件名乱码
            curContext.Response.AddHeader("content-disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8) + ".xls");
            curContext.Response.ContentType = "application nd.ms-excel";
           // curContext.Response.ContentEncoding = System.Text.Encoding.UTF8;
            curContext.Response.ContentEncoding = Encoding.Unicode;
            curContext.Response.BinaryWrite(Encoding.Unicode.GetPreamble());
            curContext.Response.Charset = "GB2312";

            //导出Excel文件 
            strWriter = new System.IO.StringWriter();
            htmlWriter = new System.Web.UI.HtmlTextWriter(strWriter);

            //为了解决dgData中可能进行了分页的情况,需要重新定义一个无分页的GridView 
            dgExport = new System.Web.UI.WebControls.GridView();
            dgExport.DataSource = dtData.DefaultView;
            dgExport.AllowPaging = false;
            dgExport.DataBind();

            //下载到客户端 
            dgExport.RenderControl(htmlWriter);
            curContext.Response.Write(strWriter.ToString());
            curContext.Response.End();
        }
    }

    public static void DataTable3Excel2(System.Data.DataTable dtData, String FileName)
    {
        System.Web.UI.WebControls.GridView dgExport = null;
        //当前对话 
        System.Web.HttpContext curContext = System.Web.HttpContext.Current;
        //IO用于导出并返回excel文件 
        System.IO.StringWriter strWriter = null;
        System.Web.UI.HtmlTextWriter htmlWriter = null;

        if (dtData != null)
        {
            //设置编码和附件格式 
            //System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8)作用是方式中文文件名乱码
            curContext.Response.AddHeader("content-disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8) + ".xls");
            curContext.Response.ContentType = "application nd.ms-excel";
            curContext.Response.ContentEncoding = System.Text.Encoding.UTF8;
            curContext.Response.Charset = "GB2312";

            //导出Excel文件 
            strWriter = new System.IO.StringWriter();
            htmlWriter = new System.Web.UI.HtmlTextWriter(strWriter);

            //为了解决dgData中可能进行了分页的情况,需要重新定义一个无分页的GridView 
            dgExport = new System.Web.UI.WebControls.GridView();
            dgExport.DataSource = dtData.DefaultView;
            dgExport.AllowPaging = false;

            dgExport.DataBind();
            //for (int x = 0; x < dgExport.Columns.Count; x++)
            //{

            //}
            dgExport.HeaderStyle.BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Font.Bold = true;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[0].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[1].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[2].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[3].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[4].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[5].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[6].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[7].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[8].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[9].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[10].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[11].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[12].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[13].BackColor = System.Drawing.Color.LightGray;
            //下载到客户端 
            dgExport.RenderControl(htmlWriter);
            curContext.Response.Write(strWriter.ToString());
            curContext.Response.End();
        }
    }
    public static void DataTable3Excel3(System.Data.DataTable dtData, String FileName)
    {
        System.Web.UI.WebControls.GridView dgExport = null;
        //当前对话 
        System.Web.HttpContext curContext = System.Web.HttpContext.Current;
        //IO用于导出并返回excel文件 
        System.IO.StringWriter strWriter = null;
        System.Web.UI.HtmlTextWriter htmlWriter = null;

        if (dtData != null)
        {
            //设置编码和附件格式 
            //System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8)作用是方式中文文件名乱码
            curContext.Response.AddHeader("content-disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8) + ".xls");
            curContext.Response.ContentType = "application nd.ms-excel";
            curContext.Response.ContentEncoding = System.Text.Encoding.UTF8;
            curContext.Response.Charset = "GB2312";

            //导出Excel文件 
            strWriter = new System.IO.StringWriter();
            htmlWriter = new System.Web.UI.HtmlTextWriter(strWriter);

            //为了解决dgData中可能进行了分页的情况,需要重新定义一个无分页的GridView 
            dgExport = new System.Web.UI.WebControls.GridView();
            dgExport.DataSource = dtData.DefaultView;
            dgExport.AllowPaging = false;

            dgExport.DataBind();
            //for (int x = 0; x < dgExport.Columns.Count; x++)
            //{

            //}
            dgExport.HeaderStyle.BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Font.Bold = true;
            for (int x = 0; x < dgExport.Rows.Count; x++)
            {
                dgExport.Rows[x].Cells[5].BackColor = System.Drawing.Color.Yellow;
            }
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[0].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[1].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[2].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[3].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[4].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[5].BackColor = System.Drawing.Color.LightGray;
            //下载到客户端 
            dgExport.RenderControl(htmlWriter);
            curContext.Response.Write(strWriter.ToString());
            curContext.Response.End();
        }
    }

    public static void DataTable3Excel4(System.Data.DataTable dtData, String FileName)
    {
        System.Web.UI.WebControls.GridView dgExport = null;
        //当前对话 
        System.Web.HttpContext curContext = System.Web.HttpContext.Current;
        //IO用于导出并返回excel文件 
        System.IO.StringWriter strWriter = null;
        System.Web.UI.HtmlTextWriter htmlWriter = null;

        if (dtData != null)
        {
            //设置编码和附件格式 
            //System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8)作用是方式中文文件名乱码
            curContext.Response.AddHeader("content-disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8) + ".xls");
            curContext.Response.ContentType = "application nd.ms-excel";
            curContext.Response.ContentEncoding = System.Text.Encoding.UTF8;
            curContext.Response.Charset = "GB2312";

            //导出Excel文件 
            strWriter = new System.IO.StringWriter();
            htmlWriter = new System.Web.UI.HtmlTextWriter(strWriter);

            //为了解决dgData中可能进行了分页的情况,需要重新定义一个无分页的GridView 
            dgExport = new System.Web.UI.WebControls.GridView();
            dgExport.DataSource = dtData.DefaultView;
            dgExport.AllowPaging = false;

            dgExport.DataBind();
            //for (int x = 0; x < dgExport.Columns.Count; x++)
            //{

            //}
            dgExport.HeaderStyle.BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Font.Bold = true;
            for (int x = 0; x < dgExport.Rows.Count; x++)
            {
                dgExport.Rows[x].Cells[4].BackColor = System.Drawing.Color.Yellow;
            }
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[0].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[1].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[2].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[3].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[4].BackColor = System.Drawing.Color.LightGray;
            //dgExport.Rows[dgExport.Rows.Count - 1].Cells[5].BackColor = System.Drawing.Color.LightGray;
            //下载到客户端 
            dgExport.RenderControl(htmlWriter);
            curContext.Response.Write(strWriter.ToString());
            curContext.Response.End();
        }
    }

    public static void DataTable3Excel5(System.Data.DataTable dtData, String FileName)
    {
        System.Web.UI.WebControls.GridView dgExport = null;
        //当前对话 
        System.Web.HttpContext curContext = System.Web.HttpContext.Current;
        //IO用于导出并返回excel文件 
        System.IO.StringWriter strWriter = null;
        System.Web.UI.HtmlTextWriter htmlWriter = null;

        if (dtData != null)
        {
            //设置编码和附件格式 
            //System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8)作用是方式中文文件名乱码
            curContext.Response.AddHeader("content-disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8) + ".xls");
            curContext.Response.ContentType = "application nd.ms-excel";
            curContext.Response.ContentEncoding = System.Text.Encoding.UTF8;
            curContext.Response.Charset = "GB2312";

            //导出Excel文件 
            strWriter = new System.IO.StringWriter();
            htmlWriter = new System.Web.UI.HtmlTextWriter(strWriter);

            //为了解决dgData中可能进行了分页的情况,需要重新定义一个无分页的GridView 
            dgExport = new System.Web.UI.WebControls.GridView();
            dgExport.DataSource = dtData.DefaultView;
            dgExport.AllowPaging = false;

            dgExport.DataBind();
            //for (int x = 0; x < dgExport.Columns.Count; x++)
            //{

            //}
            dgExport.HeaderStyle.BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Font.Bold = true;
            //for (int x = 0; x < dgExport.Rows.Count; x++)
            //{
            //    dgExport.Rows[x].Cells[5].BackColor = System.Drawing.Color.Yellow;
            //}
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[0].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[1].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[2].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[3].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[4].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[5].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[6].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[7].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[8].BackColor = System.Drawing.Color.LightGray;
            dgExport.Rows[dgExport.Rows.Count - 1].Cells[9].BackColor = System.Drawing.Color.LightGray;
            //下载到客户端 
            dgExport.RenderControl(htmlWriter);
            curContext.Response.Write(strWriter.ToString());
            curContext.Response.End();
        }
    }

    public static void ExportToWord(string regno)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Charset = "";

        HttpContext.Current.Response.ContentType = "application/msword";

        string strFileName = "GenerateDocument" + ".doc";
        HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);

        StringBuilder strHTMLContent = new StringBuilder();
        string generated = new WebClient().DownloadString("http://203.127.83.146/hosting/TSE2012/DocGenerator.aspx?regno=" + regno);


        strHTMLContent.Append(generated);
        HttpContext.Current.Response.Write(strHTMLContent);
        HttpContext.Current.Response.End();
        HttpContext.Current.Response.Flush();
    }

    public static void ExportArchExcelXLSX(DataTable dtData, string FileName, string sheetName)
    {
        XLWorkbook wb = new XLWorkbook();
        var ws = wb.Worksheets.Add(sheetName);
        ws.Cell(2, 1).InsertTable(dtData);
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        ws.Row(1).Delete();
        ws.Rows().Height = 26.25;
        ws.Style.Font.SetFontName("Tahoma");
        ws.Style.Font.SetFontSize(11);
        ws.Tables.FirstOrDefault().ShowAutoFilter = false;//remove first blank column and filter
        ws.FirstRow().AddConditionalFormat().WhenGreaterThan(20000).Fill.SetBackgroundColor(XLColor.DarkGray);//add bg color at first row(header)
        ws.FirstRow().AddConditionalFormat().WhenGreaterThan(20000).Font.SetFontSize(12);
        ws.FirstRow().AddConditionalFormat().WhenGreaterThan(20000).Font.Bold = true;
        ws.FirstRow().AddConditionalFormat().WhenGreaterThan(20000).Font.SetFontColor(XLColor.Black);
        //ws.Tables.FirstOrDefault().Cells().Style.Alignment.WrapText = true;
        //ws.Rows().AdjustToContents();
        ws.Columns(1, 11).AdjustToContents();
        ws.Rows().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format(@"attachment;filename={0}.xlsx", FileName));

        using (MemoryStream memoryStream = new MemoryStream())
        {
            wb.SaveAs(memoryStream);
            memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
            memoryStream.Close();
        }

        HttpContext.Current.Response.End();
    }
}