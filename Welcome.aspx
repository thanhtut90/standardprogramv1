﻿<%@ Page Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="Welcome.aspx.cs" Inherits="WelcomeLaunch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/plugins/jquery-1.9.1.min.js"></script>
    <script src="Content/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">

        function openModal() {
            $('#myModal').modal('show');
        }

        function closeModal() {
            $('#myModal').modal('hide');
        }
    </script>
    <style type="text/css">
        .numberBrkStyle, .romanceBrkStyle {
          counter-reset: list;
        }
        .numberBrkStyle > li, .romanceBrkStyle > li {
          list-style: none;
        }
        .romanceBrkStyle > li {
            display: inline-flex;
        }
        .numberBrkStyle > li:before {
          content: counter(list, decimal) ") ";
          counter-increment: list;
        }
        .romanceBrkStyle > li:before {
          content: "(" counter(list, lower-roman) ") ";
          counter-increment: list;
          width:2rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="form-group row">
        <div class="col-md-12">
            <asp:Label ID="lblTemplate" runat="server"></asp:Label>
        </div>
    </div>

    <div class="form-group row" id="divPrePopulateEmail" runat="server" visible="false">
        <div class="col-md-2 col-md-offset-2">
            <asp:Label ID="lblPrePopulateEmail" runat="server" CssClass="form-control-label" Text="Email Address"></asp:Label>
        </div>
        <div class="col-md-5">
            <asp:TextBox ID="txtPrePopulateEmail" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-md-2">
            &nbsp;
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <%--<div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Confirm</h4>
                </div>--%>
                <div class="modal-body">
                    <asp:Label ID="lblAlertMessage" runat="server"></asp:Label>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnClose" runat="server" class="btn btn-default" Text="Close" OnClick="btnClose_Click" CausesValidation="false" />
                    <asp:Button ID="btnOK" runat="server" class="btn btn-primary" Text="OK" OnClick="btnOK_Click" />
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hfNextRoute" runat="server" Value="" />

    <div class="form-group row" style="padding-top: 30px; padding-bottom: 30px;">
        <div class="col-md-12">
            <div class="col-lg-offset-4 col-lg-4 col-sm-offset-4 col-sm-3 center-block">
                <%--col-lg-offset-5 col-lg-3 col-sm-offset-4 col-sm-3 center-block--%>
                <asp:Button runat="server" ID="btnRegister" Text="Register" OnClick="btnRegister_Onclick" CssClass="btn MainButton btn-block" />
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12">
            <asp:Label ID="lblTemplate2" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form-group row" style="padding-bottom: 30px;">
        <div class="col-md-12">
            <div class="modal-body" style="text-align:center;font-weight:bold;">
                <asp:Label ID="lblAddtionalMsg" Visible="false" runat="server" Text="Our invitation code will be available through our social media, press release, media partner's advertisement, e-newsletter and exhibitor invitation."></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
