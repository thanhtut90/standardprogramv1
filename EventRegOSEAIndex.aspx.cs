﻿using Corpit.BackendMaster;
using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EventRegOSEAIndex : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    static string delegateRegNo = "Delegate_RefRegno";
    protected static string defaultFlowID_RegisterColleague = "F371";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.Params["event"] != null)
            {
                string showID = cFun.DecryptValue(Request.QueryString["event"].ToString());
                string regDelegateRefNo = string.Empty;
                if (Request.Params["reg"] != null)
                {
                    regDelegateRefNo = cFun.DecryptValue(Request.QueryString["reg"].ToString());
                    BrowseNext(regDelegateRefNo, showID);
                }
                else
                {
                    //Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
                    BrowseNext(regDelegateRefNo, showID);
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid.');window.location='404.aspx';</script>");
            }
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        //SiteBanner.ImageUrl = sSetting.SiteBanner;
    }
    #endregion

    #region BrowseNext
    /// <summary>
    /// (1)if link come without delegaterefno, will redirect to normal flow (delegate info->questionnaire->recommend a friend->summary->success)
    /// (2)if link come with delegaterefno, check pre-populate(tb_RegPrePopulate) or register colleague(tb_RegDelegate), 
    /// and then 
    /// if pre-populate user, will redirect to pre-populate flow (delegate info->recommend a friend->summary->success)
    /// if register colleague user, will redirect to normal flow (delegate info->questionnaire->recommend a friend->summary->success)
    /// </summary>
    /// <param name="regDelegateRefNo"></param>
    /// <param name="showid"></param>
    private void BrowseNext(string regDelegateRefNo, string showid)
    {
        if (!string.IsNullOrEmpty(regDelegateRefNo))
        {
            #region already in the system
            string regno = string.Empty;
            PrePopulate_RegDelegateObj preDelegateObj = new PrePopulate_RegDelegateObj(fn);
            bool isPrePopulateExist = preDelegateObj.checkPrePopulateDataExistByColumn(delegateRegNo, regDelegateRefNo, ref regno);
            if (isPrePopulateExist)
            {
                #region pre-populate
                PrePopulate_RegDelegateObj preDelObj = preDelegateObj.getPrePopulateDataByColumn(delegateRegNo, regDelegateRefNo);
                string showID = preDelObj.showID;
                string FlowID = preDelObj.flowID;
                if (!string.IsNullOrEmpty(showID))
                {
                    ShowController shwCtrl = new ShowController(fn);
                    if (shwCtrl.checkValidShow(showID))
                    {
                        FlowControler Flw = new FlowControler(fn);
                        FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(FlowID);
                        if (flwMasterConfig.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                        {
                            bool isSuccess = false;
                            bool isDelegateRegistered = false;
                            string grpNum = "";
                            bool isMatchedoldShowID = false;
                            string decryptShowID = cFun.DecryptValue(showID);
                            isSuccess = prepopulateChecking(showID, regDelegateRefNo, preDelObj, ref regno, ref isDelegateRegistered, ref grpNum, ref isMatchedoldShowID);
                            if (!isSuccess)
                            {
                                regno = "";
                                grpNum = "";
                                isMatchedoldShowID = false;
                            }

                            string page = "";
                            string step = "";
                            if (string.IsNullOrEmpty(regno))
                            {
                                Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
                            }
                            else
                            {
                                string route = "";
                                if (isDelegateRegistered)
                                {
                                    Response.Write("<script>alert('Sorry, this user already registered.');window.location='404.aspx';</script>");
                                }
                                else
                                {
                                    Dictionary<string, string> nValues = Flw.GetNextRoute(FlowID);
                                    if (nValues.Count > 0)
                                    {
                                        page = nValues["nURL"].ToString();
                                        step = nValues["nStep"].ToString();
                                        FlowID = nValues["FlowID"].ToString();
                                    }
                                    route = Flw.MakeFullURL(page, FlowID, showID, cFun.EncryptValue(grpNum), step, cFun.EncryptValue(regno));
                                    Response.Redirect(route);
                                }
                            }
                        }
                        else
                        {
                            Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
                    }
                }
                else
                {
                    Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
                }
                #endregion
            }
            else
            {
                #region check register colleague
                RegDelegateObj rgd = new RegDelegateObj(fn);
                DataTable dt = rgd.getRegDelegateByID(regDelegateRefNo, showid);
                if (dt.Rows.Count > 0)
                {
                    //register colleague(normal flow)
                    string groupid = dt.Rows[0]["RegGroupID"].ToString();
                    string flowid = dt.Rows[0]["reg_urlFlowID"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["reg_urlFlowID"].ToString()) ? dt.Rows[0]["reg_urlFlowID"].ToString() : "") : "";
                    if(string.IsNullOrEmpty(flowid))
                    {
                        flowid = defaultFlowID_RegisterColleague;
                    }
                    string route = newRegisterNormalFlow(showid, flowid, regDelegateRefNo, groupid);
                    Response.Redirect(route);
                }
                else
                {
                    ////new Registration(normal flow)
                    //string route = newRegisterNormalFlow(showid, defaultFlowID_RegisterColleague);
                    //Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='" + route + "';</script>");
                    Response.Write("<script>alert('Sorry, this user is not in the system.');window.location='404.aspx';</script>");
                }
                #endregion
            }
            #endregion
        }
        else
        {
            //new Registration(normal flow)
            string route = newRegisterNormalFlow(showid, defaultFlowID_RegisterColleague);
            Response.Redirect(route);
        }
    }
    #endregion

    #region getRoute
    private string newRegisterNormalFlow(string showid, string flowid, string regno=null, string groupid=null)
    {
        string route = string.Empty;
        try
        {
            string page = "";
            string step = "";
            string encryptshowid = cFun.EncryptValue(showid);
            string encryptflowid = cFun.EncryptValue(flowid);
            RegGroupObj rgp = new RegGroupObj(fn);
            string grpNum = string.Empty;
            if (!string.IsNullOrEmpty(groupid))
            {
                grpNum = cFun.EncryptValue(groupid);
            }
            else
            {
                groupid = rgp.GenGroupNumber(showid);
                grpNum = cFun.EncryptValue(grpNum);
            }
            FlowControler Flw = new FlowControler(fn);
            Dictionary<string, string> nValues = Flw.GetNextRoute(flowid);
            if (nValues.Count > 0)
            {
                page = nValues["nURL"].ToString();
                step = nValues["nStep"].ToString();
                flowid = nValues["FlowID"].ToString();
            }
            if (string.IsNullOrEmpty(regno))
            {
                route = Flw.MakeFullURL(page, encryptflowid, encryptshowid, grpNum, step);
            }
            else
            {
                regno = cFun.EncryptValue(regno);
                route = Flw.MakeFullURL(page, encryptflowid, encryptshowid, grpNum, step, regno);
            }
        }
        catch(Exception ex)
        { }

        return route;
    }
    #endregion

    #region prepopulateChecking
    private bool prepopulateChecking(string showid, string regDelegateRefNo, PrePopulate_RegDelegateObj preDelegateObj, ref string regno, ref bool isDelegateRegistered, ref string groupid, ref bool isMatchOldShowID)
    {
        bool isSuccess = false;
        try
        {
            if (preDelegateObj != null)
            {
                int prepopulateStatus = (int)StatusValue.Pending;
                if (!string.IsNullOrEmpty(regno))
                {
                    prepopulateStatus = preDelegateObj.getPreDelegateStatusByDelRefID(regno, showid);
                    if (prepopulateStatus == (int)StatusValue.Success)
                    {
                        RegDelegateObj regDelegateObj = new RegDelegateObj(fn);
                        int delegateStatus = regDelegateObj.getDelegateStatusByID(regno, showid);
                        if (delegateStatus == (int)StatusValue.Success)
                        {
                            isSuccess = true;
                            isDelegateRegistered = true;
                        }
                        else
                        {
                            int isSaved = insertRegDelegateData(showid, preDelegateObj, ref regno, ref groupid, ref isMatchOldShowID);
                            if (isSaved > 0)
                            {
                                isSaved = preDelegateObj.updateDelegateRefNoByColumn(delegateRegNo, regDelegateRefNo, showid, regno);
                                if (isSaved > 0)
                                {
                                    string updateddate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                                    preDelegateObj.updateStatus(regno, (int)StatusValue.Success, showid, updateddate);
                                    isSuccess = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        string updateddate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                        preDelegateObj.updateStatus(regno, (int)StatusValue.Success, showid, updateddate);
                        isSuccess = true;
                    }
                }
                else
                {
                    int isSaved = insertRegDelegateData(showid, preDelegateObj, ref regno, ref groupid, ref isMatchOldShowID);
                    if (isSaved > 0)
                    {
                        isSaved = preDelegateObj.updateDelegateRefNoByColumn(delegateRegNo, regDelegateRefNo, showid, regno);
                        if (isSaved > 0)
                        {
                            string updateddate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                            preDelegateObj.updateStatus(regno, (int)StatusValue.Success, showid, updateddate);
                            isSuccess = true;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            isSuccess = false;
        }

        return isSuccess;
    }
    #endregion

    #region insertRegDelegateData
    private int insertRegDelegateData(string showid, PrePopulate_RegDelegateObj preDelObj, ref string regno, ref string groupid, ref bool isMatchOldShowID)
    {
        int isSuccess = 0;
        try
        {
            #region create delegate in tb_RegDelegate
            RegDelegateObj rgd = new RegDelegateObj(fn);
            DataTable dt = rgd.getRegDelegateByID(regno, showid);//rgd.getRegDelegateByRegnoEmail(regno, preEmailAddress); 
            if (dt.Rows.Count == 0)
            {
                //create group in tb_RegGroup
                RegGroupObj rgp = new RegGroupObj(fn);
                groupid = rgp.GenGroupNumber(showid);
                rgp.createRegGroupIDOnly(groupid, 0, showid);

                regno = rgd.GenDelegateNumber(showid);

                rgd.groupid = groupid;
                rgd.regno = regno;
                rgd.con_categoryID = preDelObj.con_categoryID;
                rgd.salutation = preDelObj.salutation;
                rgd.fname = preDelObj.fname;
                rgd.lname = preDelObj.lname;
                rgd.oname = preDelObj.oname;
                rgd.passno = preDelObj.passno;
                rgd.isreg = preDelObj.isreg;
                rgd.regspecific = preDelObj.regspecific;//MCR/SNB/PRN
                rgd.idno = preDelObj.idno;//MCR/SNB/PRN No.
                rgd.staffid = preDelObj.staffid;//no use in design for this field
                rgd.designation = preDelObj.designation;
                rgd.jobtitle = preDelObj.jobtitle;//if Profession is Allied Health
                rgd.profession = preDelObj.profession;
                rgd.department = preDelObj.department;
                rgd.organization = preDelObj.organization;
                rgd.institution = preDelObj.institution;
                rgd.address1 = preDelObj.address1;
                rgd.address2 = preDelObj.address2;
                rgd.address3 = preDelObj.address3;
                rgd.address4 = preDelObj.address4;
                rgd.city = preDelObj.city;
                rgd.state = preDelObj.state;
                rgd.postalcode = preDelObj.postalcode;
                rgd.country = preDelObj.country;
                rgd.rcountry = preDelObj.rcountry;
                rgd.telcc = preDelObj.telcc;
                rgd.telac = preDelObj.telac;
                rgd.tel = preDelObj.tel;
                rgd.mobilecc = preDelObj.mobilecc;
                rgd.mobileac = preDelObj.mobileac;
                rgd.mobile = preDelObj.mobile;
                rgd.faxcc = preDelObj.faxcc;
                rgd.faxac = preDelObj.faxac;
                rgd.fax = preDelObj.fax;
                rgd.email = preDelObj.email;
                rgd.affiliation = preDelObj.affiliation;
                rgd.dietary = preDelObj.dietary;
                rgd.nationality = preDelObj.nationality;
                rgd.age = preDelObj.age;
                rgd.dob = preDelObj.dob;
                rgd.gender = preDelObj.gender;
                rgd.additional4 = preDelObj.additional4;
                rgd.additional5 = preDelObj.additional5;
                rgd.memberno = preDelObj.memberno;

                rgd.vname = preDelObj.vname;
                rgd.vdob = preDelObj.vdob;
                rgd.vpassno = preDelObj.vpassno;
                rgd.vpassexpiry = preDelObj.vpassexpiry;
                rgd.vpassissuedate = preDelObj.vpassissuedate;
                rgd.vembarkation = preDelObj.vembarkation;
                rgd.varrivaldate = preDelObj.varrivaldate;
                rgd.vcountry = preDelObj.vcountry;

                rgd.udfcname = preDelObj.udfcname;
                rgd.udfdeltype = preDelObj.udfdeltype;
                rgd.udfprofcat = preDelObj.udfprofcat;
                rgd.udfprofcatother = preDelObj.udfprofcatother;
                rgd.udfcpcode = preDelObj.udfcpcode;
                rgd.udfcldept = preDelObj.udfcldept;
                rgd.udfcaddress = preDelObj.udfcaddress;
                rgd.udfclcompany = preDelObj.udfclcompany;
                rgd.udfclcompanyother = preDelObj.udfclcompanyother;
                rgd.udfccountry = preDelObj.udfccountry;

                rgd.supname = preDelObj.supname;
                rgd.supdesignation = preDelObj.supdesignation;
                rgd.supcontact = preDelObj.supcontact;
                rgd.supemail = preDelObj.supemail;

                rgd.othersal = preDelObj.othersal;
                rgd.otherprof = preDelObj.otherprof;
                rgd.otherdept = preDelObj.otherdept;
                rgd.otherorg = preDelObj.otherorg;
                rgd.otherinstitution = preDelObj.otherinstitution;

                rgd.aemail = preDelObj.aemail;
                rgd.isSMS = preDelObj.isSMS;

                rgd.remark = preDelObj.remark;
                rgd.remark_groupupload = preDelObj.remark_groupupload;
                rgd.approvestatus = preDelObj.approvestatus;
                rgd.createdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture);
                rgd.recycle = preDelObj.recycle;
                rgd.stage = preDelObj.stage;

                rgd.showID = showid;
                isSuccess = rgd.saveRegDelegate();
                rgd.UpdateApproveRejectStatus(regno, (int)RegApproveRejectStatus.VIP, showid);//***
            }
            else
            {
                isSuccess = 1;
                groupid = dt.Rows[0]["RegGroupID"].ToString();
                string oldShowID = dt.Rows[0]["ShowID"].ToString();
                if (showid == oldShowID)
                {
                    isMatchOldShowID = true;
                }
                else
                {
                    isMatchOldShowID = false;
                }
            }
            #endregion
        }
        catch (Exception ex)
        { }

        return isSuccess;
    }
    #endregion
}