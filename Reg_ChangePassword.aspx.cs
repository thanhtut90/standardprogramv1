﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reg_ChangePassword : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();
    #endregion
    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string flwID = cFun.DecryptValue(urlQuery.FlowID);

            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flwID);
            txtFlowName.Text = fmaster.FlowName;

            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
            }
            else
            {
                Response.Redirect("ReLogin.aspx?Event=" + txtFlowName.Text.Trim());//?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
            }
        }
    }
    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMasterReLogin;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string currentstage = Session["regstage"].ToString();
                try
                {
                    string ownerID = groupid;
                    if(Session["Regno"] != null)
                    {
                        ownerID = Session["Regno"].ToString();
                    }
                    string errMsg = "";
                    RegLoginPassword regLP = new RegLoginPassword(fn);
                    string password = regLP.getPasswordByOwnerID(ownerID, showid);
                    if (!string.IsNullOrEmpty(password))
                    {
                        if (password == txtOldPassword.Text.Trim())
                        {
                            //////SetUpController setObj = new SetUpController(fn);
                            //////string reandomCode = setObj.generateReferenceNo(showid, flowid, ownerID, 7);
                            //////password = reandomCode;
                            password = txtNewPassword.Text.Trim();
                            bool checkExist = checkExistPassword(password, showid);// regLP.checkExist(password, showid);
                            if (checkExist)
                            {
                                errMsg = "Sorry, please enter different password.";
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errMsg + "');", true);
                                return;
                            }
                            else
                            {
                                int isSuccess = regLP.changePassword(password, ownerID, showid);
                                if (isSuccess > 0)
                                {
                                    string isSendEmailSuccess = "F";
                                    string emailAddress = "";
                                    if (Session["Regno"] != null)
                                    {
                                        DataTable dtResult = getDelegateEmail(ownerID, showid);
                                        if (dtResult.Rows.Count > 0)
                                        {
                                            emailAddress = dtResult.Rows[0]["reg_Email"].ToString();
                                            string fname = dtResult.Rows[0]["reg_FName"] != DBNull.Value ? dtResult.Rows[0]["reg_FName"].ToString() : "";
                                            string lname = dtResult.Rows[0]["reg_LName"] != DBNull.Value ? dtResult.Rows[0]["reg_LName"].ToString() : "";
                                            string salID = dtResult.Rows[0]["reg_Salutation"] != DBNull.Value ? dtResult.Rows[0]["reg_Salutation"].ToString() : "0";
                                            string otherValue = dtResult.Rows[0]["reg_SalutationOthers"] != DBNull.Value ? dtResult.Rows[0]["reg_SalutationOthers"].ToString() : "";
                                            string salutation = bindSalutation(salID, otherValue);
                                            isSendEmailSuccess = sEmailSend(salutation, fname, lname, emailAddress, password);
                                        }
                                    }
                                    else
                                    {
                                        DataTable dtResult = getGroupEmail(ownerID, showid);
                                        if (dtResult.Rows.Count > 0)
                                        {
                                            emailAddress = dtResult.Rows[0]["RG_ContactEmail"].ToString();
                                            string fname = dtResult.Rows[0]["RG_ContactFName"] != DBNull.Value ? dtResult.Rows[0]["RG_ContactFName"].ToString() : "";
                                            string lname = dtResult.Rows[0]["RG_ContactLName"] != DBNull.Value ? dtResult.Rows[0]["RG_ContactLName"].ToString() : "";
                                            string salID = dtResult.Rows[0]["RG_Salutation"] != DBNull.Value ? dtResult.Rows[0]["RG_Salutation"].ToString() : "0";
                                            string otherValue = dtResult.Rows[0]["RG_SalOther"] != DBNull.Value ? dtResult.Rows[0]["RG_SalOther"].ToString() : "";
                                            string salutation = bindSalutation(salID, otherValue);
                                            isSendEmailSuccess = sEmailSend(salutation, fname, lname, emailAddress, password);
                                        }
                                    }

                                    Response.Redirect("LandingIDEM.aspx");
                                }
                                else
                                {
                                    errMsg = "Saving error.";
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errMsg + "');", true);
                                    return;
                                }
                            }
                        }
                        else
                        {
                            errMsg = "The old password is not correct. Please type again.";
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errMsg + "');", true);
                            return;
                        }
                    }
                    else
                    {
                        errMsg = "Sorry, you don't have password.";
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errMsg + "');", true);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    string errMsg = "Saving error.";
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errMsg + "');", true);
                    return;
                }
            }
            else
            {
                Response.Redirect("ReLogin.aspx?Event=" + txtFlowName.Text.Trim());//?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
            }
        }
    }
    #region getdata
    #region getGroupReLoginAuthenData
    public DataTable getGroupEmail(string registrationID, string showid)
    {
        DataTable dtResult = new DataTable();

        try
        {
            string query = "Select * From tb_RegGroup Where RegGroupID=@RegGroupID And ShowID=@ShowID And recycle=0";

            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar2 = new SqlParameter("RegGroupID", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            spar2.Value = registrationID;
            spar3.Value = showid;
            pList.Add(spar2);
            pList.Add(spar3);
            dtResult = fn.GetDatasetByCommand(query, "dsResult", pList).Tables[0];
            
        }
        catch { }

        return dtResult;
    }
    #endregion
    #region getDelegateReLoginAuthenData
    public DataTable getDelegateEmail(string registrationID, string showid)
    {
        DataTable dtResult = new DataTable();

        try
        {
            string query = "Select * From tb_RegDelegate Where Regno=@Regno And ShowID=@ShowID And recycle=0";

            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar2 = new SqlParameter("Regno", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            spar2.Value = registrationID;
            spar3.Value = showid;
            pList.Add(spar2);
            pList.Add(spar3);
            dtResult = fn.GetDatasetByCommand(query, "dsResult", pList).Tables[0];
        }
        catch { }

        return dtResult;
    }
    #endregion
    static string others_value = "Others";
    static string other_value = "Other";
    public string bindSalutation(string id, string otherValue)
    {
        string name = string.Empty;
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            if (!string.IsNullOrEmpty(id))
            {
                SetUpController setupCtr = new SetUpController(fn);
                name = setupCtr.getSalutationNameByID(id, showid);
                if (string.IsNullOrEmpty(name))
                {
                    name = id;
                }

                if (name == others_value || name == other_value)
                {
                    name = otherValue;
                }
            }
        }
        catch (Exception ex)
        { }

        return name;
    }
    #endregion
    public string sEmailSend(string salutation, string fname, string lname, string emailAddress, string password)
    {
        string isSuccess = "F";
        try
        {
            Alpinely.TownCrier.TemplateParser tm = new Alpinely.TownCrier.TemplateParser();
            Alpinely.TownCrier.MergedEmailFactory factory = new Alpinely.TownCrier.MergedEmailFactory(tm);
            string template = HttpContext.Current.Server.MapPath("~/Template/IDEMChangePassword.html");
            var tokenValues = new Dictionary<string, string>
                {
                    {"Salutation",salutation},
                    {"reg_FName",fname},
                    {"reg_LName",lname},
                    {"reg_Email",emailAddress},
                    {"LogInDelegatePwd",password},
                };
            MailMessage message = factory
            .WithTokenValues(tokenValues)
            .WithHtmlBodyFromFile(template)
            .Create();

            //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//***
            System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;//***

            var from = new MailAddress("idem-reg@idem-singapore.com", "IDEM 2020");
            message.From = from;
            message.To.Add(emailAddress);
            message.Subject = "IDEM 2020 Change Password";
            SmtpClient emailClient = new SmtpClient("idem-singapore.com");
            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("idem-reg@idem-singapore.com", "Reg_123!");
            emailClient.UseDefaultCredentials = true;
            emailClient.EnableSsl = true;
            emailClient.Port = 587;//***
            emailClient.Credentials = SMTPUserInfo;
            emailClient.Send(message);

            isSuccess = "T";
        }
        catch (Exception ex)
        {
            isSuccess = ex.Message;// "F";
        }
        return isSuccess;
    }
    public bool checkExistPassword(string password, string showid)
    {
        bool isExist = false;
        try
        {
            string sql = string.Format("Select * From tb_RegLoginPassword Where Password='{0}' And ShowID='{1}' And Status='{2}'", password, showid, RegLoginPasswordStatus.Active);
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dt.Rows.Count > 0)
            {
                isExist = true;
            }
        }
        catch { }

        return isExist;
    }
}