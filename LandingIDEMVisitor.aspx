﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LandingIDEMVisitor.aspx.cs" Inherits="LandingIDEMVisitor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>IDEM Online Registration 2020</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/Icon1.png" />

    <link href="Content/IDEMTemplate/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Content/IDEMTemplate/Site.css" rel="stylesheet" type="text/css" />
    <link href="Content/IDEMTemplate/font-awesome.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="Scripts/jquery-2.1.0.js"></script>
    <script type="text/javascript" src="Scripts/jquery.plugin.js"></script>
    <script type="text/javascript" src="Scripts/bootstrap.min.js"></script>

    <style type="text/css">
        body
        {
            font-family: Trebuchet-MS !important;
            font-size:12px !important;
        }
        .btn-danger
        {
            background-color:#F36523 !important;
            border-radius:7px !important;
        }
        .col-xs-4, .col-xs-6
        {
            padding :0px !important;
            width:80% !important;
            padding-top:5px !important;
            padding-bottom:5px !important;
        }
        h3
        {
            margin:0px !important;
        }
        .iconStyle
        {
            color:#333333 !important;
        }
        .fa-lg
        {
            font-size:2.33333333em !important;
            margin-top:10px !important;
        }
        h5
        {
            font-weight:bold;
        }
        .btn-danger
        {
            background-color:red !important;
            font-size: 14px !important;
        }
    </style>
    <!-- 
    Start of global snippet: Please do not remove
    Place this snippet between the <head> and </head> tags on every page of your site.
    -->
    <!-- Global site tag (gtag.js) - Google Marketing Platform -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=DC-6953330"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'DC-6953330');
    </script>
    <!-- End of global snippet: Please do not remove -->

    <!--
    Event snippet for FR~On Click _MK~SG_PC~IDEM2020_TradeRegGroup on https://www.event-reg.biz/Registration/LandingIDEMVisitor: Please do not remove.
    Place this snippet on pages with events you’re tracking. 
    Creation date: 12/19/2019
    -->
    <script>
      gtag('event', 'conversion', {
        'allow_custom_scripts': true,
        'send_to': 'DC-6953330/mice/fronc01-+standard'
      });
    </script>
    <noscript>
    <img src="https://ad.doubleclick.net/ddm/activity/src=6953330;type=mice;cat=fronc01-;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=1?" width="1" height="1" alt=""/>
    </noscript>
    <!-- End of event snippet: Please do not remove -->

    <!--
    Event snippet for FR~On Click _MK~SG_PC~IDEM2020_TradeRegSingle on https://www.event-reg.biz/Registration/LandingIDEMVisitor: Please do not remove.
    Place this snippet on pages with events you’re tracking. 
    Creation date: 12/19/2019
    -->
    <script>
      gtag('event', 'conversion', {
        'allow_custom_scripts': true,
        'send_to': 'DC-6953330/mice/fronc019+standard'
      });
    </script>
    <noscript>
    <img src="https://ad.doubleclick.net/ddm/activity/src=6953330;type=mice;cat=fronc019;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=1?" width="1" height="1" alt=""/>
    </noscript>
    <!-- End of event snippet: Please do not remove -->

</head>
<body>
    <div class="container">
    <div id="header">
        <a href="https://www.idem-singapore.com/" target="_blank">
            <asp:Image ID="imgBanner" runat="server" border="0" ImageUrl="https://event-reg.biz/DefaultBanner/images/IDEM/IDEMHeaderBanner.jpg"
                    style = "position:relative; right:10px; top:0px; z-index:1; width:100%;"/>
        </a>
        <br />
    </div>

    <form id="SignupForm" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="form-group row text-center">
            <div class="col-md-12">
                <h1><strong>WELCOME TO IDEM 2020 VISITOR REGISTRATION PAGE!</strong></h1>
            </div>
        </div>

        <div class="form-group row text-center">
            <div class="col-md-12 col-xs-12" style="background-color:#005AAB;color:white;">
                <br />
                <div class="row">
                    <div class="col-md-offset-5 col-md-3 col-xs-12">
                        Please select whether you are registering for one (single registration) or for more than 2 visitors (group registration).
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-offset-5 col-md-3 col-xs-12 LandingVBtnDiv" style="padding-left:50px;">
                        <asp:Button ID="btnTrade" runat="server" Text="Single Registration" 
                                CssClass="col-md-3 col-xs-6 btn-danger LandingVBtn" OnClick="btnTrade_Click"/>
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-offset-5 col-md-3 col-xs-12 LandingVBtnDiv" style="padding-left:50px;">
                        <asp:Button ID="btnTradeGroup" runat="server" Text="Group Registration" 
                                CssClass="col-md-3 col-xs-6 btn-danger LandingVBtn" OnClick="btnTradeGroup_Click"/>
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <br /><br /><br /><br /><br />
            </div>
        </div>
        <br /><br /><br /><br /><br />
        <%--<div class="row">
            <footer>
                <div class="container">
                    <div class="row" id="footercontact">
                        <div class="col-md-4 col-xs-12" style="float:left;text-align:left;" >
                            <h5>Stay Connected with Us</h5>
                            <div class="col-md-2 col-xs-3 landingSocialStyle1" style="padding-left:0px;"><p class="social"><a id="aFacebook" runat="server" target="_blank" href="https://www.facebook.com/idemsingapore/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Facebook.png" /></a></p></div>
                            <div class="col-md-2 col-xs-3" style="padding-left:0px;"><p class="social"><a id="aTwitter" runat="server" target="_blank" href="https://twitter.com/IDEMSingapore" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Twitter.png" /></a></p></div>
                            <div class="col-md-2 col-xs-3" style="padding-left:0px;"><p class="social"><a id="aInstagram" runat="server" target="_blank" href="https://www.instagram.com/idem.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Instagram.png" /></a></p></div>
                            <div class="col-md-2 col-xs-3" style="padding-left:0px;"><p class="social"><a id="aLinkedIn" runat="server" target="_blank" href="https://www.linkedin.com/company/6440434/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/IN.png" /></a></p></div>
                        </div>
                        <div class="col-md-8 col-xs-12" style="float:right;text-align:left;" >
                            <div class="col-md-2 col-xs-12" style="padding-right:0px;">&nbsp;</div>
                            <div class="col-md-2 col-xs-3" style="padding-right:0px;">
                                <h5>Endorsed By</h5>
                                <p class="social">
                                    <a id="a1" runat="server" target="_blank" href="https://www.stb.gov.sg/content/stb/en/assistance-and-licensing/other-assistance-resources-overview/AIF28Non-Financial-Assistance.html" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/AIFLogo.jpg" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-3" style="padding-right:0px;">
                                <h5>Supported By</h5>
                                <p class="social">
                                    <a id="a2" runat="server" target="_blank" href="https://www.visitsingapore.com/mice/en/about-us/about-secb/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/SECBLogo.jpg" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-3" style="padding-right:0px;">
                                <h5>Held in</h5>
                                <p class="social">
                                    <a id="a3" runat="server" target="_blank" href="https://www.visitsingapore.com/en/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/BrandSGLogo.jpg" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-3" style="padding-right:0px;">
                                <h5>Organised by</h5>
                                <p class="social">
                                    <a id="a4" runat="server" target="_blank" href="http://sda.org.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/SDALogo.jpg" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-12" style="padding-right:0px;">
                                <h5>&nbsp;</h5>
                                <p class="social">
                                    <a id="a5" runat="server" target="_blank" href="https://www.koelnmesse.com.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/KMLogo.jpg" /></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>--%>
        <div class="row">
            <footer>
                <div class="container">
                    <div class="row" id="footercontact">
                        <div class="col-md-4 col-xs-12" style="float:left;text-align:left; font-family:'Trebuchet MS';" >
                            <h5 style="font-family:'Trebuchet MS';padding-left:10px;">Stay Connected with Us</h5>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aFacebook" runat="server" target="_blank" href="https://www.facebook.com/idemsingapore/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Facebook.png" /></a></p></div>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aTwitter" runat="server" target="_blank" href="https://twitter.com/IDEMSingapore" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Twitter.png" /></a></p></div>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aInstagram" runat="server" target="_blank" href="https://www.instagram.com/idem.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/Instagram.png" /></a></p></div>
                            <div class="col-md-2 col-xs-1 landingSocialStyle1 text-center" style="padding-left:0px;float:left;"><p class="social"><a id="aLinkedIn" runat="server" target="_blank" href="https://www.linkedin.com/company/6440434/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/IN.png" /></a></p></div>
                        </div>
                        <div class="col-md-8 col-xs-12" style="float:right;text-align:left;" >
                            <div class="col-md-2 col-xs-12" style="padding-right:0px;">&nbsp;</div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;padding-left:10px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Endorsed By</h5>
                                <p class="social">
                                    <a id="a1" runat="server" target="_blank" href="https://www.stb.gov.sg/content/stb/en/assistance-and-licensing/other-assistance-resources-overview/AIF28Non-Financial-Assistance.html" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/AIFLogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Supported By</h5>
                                <p class="social">
                                    <a id="a2" runat="server" target="_blank" href="https://www.visitsingapore.com/mice/en/about-us/about-secb/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/SECBLogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Held in</h5>
                                <p class="social">
                                    <a id="a3" runat="server" target="_blank" href="https://www.visitsingapore.com/en/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/BrandSGLogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">Organised by</h5>
                                <p class="social">
                                    <a id="a4" runat="server" target="_blank" href="http://sda.org.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/SDALogo.png" /></a>
                                </p>
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-right:0px;">
                                <h5 style="font-family:'Trebuchet MS';" class="FooterLogoStyle1">&nbsp;</h5>
                                <p class="social">
                                    <a id="a5" runat="server" target="_blank" href="https://www.koelnmesse.com.sg/" class="iconStyle" data-animate-hover="shake"><img class="img-responsive" src="https://event-reg.biz/defaultbanner/images/IDEM/KMLogo.png" /></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </form>
</div>
</body>
</html>
