﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendFoodJapanReminderEmail.aspx.cs" Inherits="SendFoodJapanReminderEmail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h5>Send Food Japan Reminer Email Sending</h5>
        Show : <asp:DropDownList ID="ddlShowID" runat="server"></asp:DropDownList>
        <br />
        Registration Url : <asp:TextBox ID="txtProjectUrl" runat="server" placeholder="www.google.com/" Width="500px"></asp:TextBox>
        <br />
        <asp:Button ID="btnSendEmail" runat="server" Text="Send Reminder" OnClick="btnSendEmail_Click" />
    </div>
    </form>
</body>
</html>
