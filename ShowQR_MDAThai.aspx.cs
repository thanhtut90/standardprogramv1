﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
using Corpit.Logging;
using System.Data;
using Corpit.Email;
using Corpit.Site.Email;
public partial class ShowQR_MDAThai : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cComFuz = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        SetSiteMaster(showid);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
            string emailID = "";
            string MFTDXO366 = "231";
            string PPIWEQ368 = "251";
            string TPlasQTX369 = "252";
            string WireTubeVRV370 = "253";
            string SMEXThaiHBE386 = "327";

            if (showid == "DXO366")
            {
                emailID = MFTDXO366;
            }
            else if (showid == "WEQ368")
            {
                emailID = PPIWEQ368;
            }
            else if (showid == "QTX369")
            {
                emailID = TPlasQTX369;
            }
            else if (showid == "VRV370")
            {
                emailID = WireTubeVRV370;
            }
            else if (showid == "HBE386")
            {
                emailID = SMEXThaiHBE386;
            }
            string flowID = cComFuz.DecryptValue(urlQuery.FlowID);
            EmailDSourceKey sourcKey = new EmailDSourceKey();
            sourcKey.AddKeyToList("ShowID", showid);
            sourcKey.AddKeyToList("RegGroupID", GroupRegID);
            sourcKey.AddKeyToList("Regno", DelegateID);
            sourcKey.AddKeyToList("FlowID", flowID);
            QRcodeMaker qMaker = new QRcodeMaker();
            string url = qMaker.CreateQRcode("D", DelegateID, showid, flowID, emailID, sourcKey);
            if (!string.IsNullOrEmpty(url))
            {
                ImgQR.ImageUrl = url;
                ImgQR.Visible = true;
            }

        }

    }

    #region PageSetting
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
            Page.MasterPageFile = masterPage;
    }

    #endregion
}