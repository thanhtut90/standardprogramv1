﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System.Data.SqlClient;
using System.Data;
using Corpit.Registration;
using Corpit.Logging;
using System.Configuration;

public partial class Questionairre : System.Web.UI.Page
{
    CommonFuns cFun = new CommonFuns();
    Functionality fn = new Functionality();
    LogActionObj rlgobj = new LogActionObj();

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);

            if (Request.Params["TYPE"] != null)
            {
                string type = cFun.DecryptValue(Request.QueryString["TYPE"].ToString());

                if (!string.IsNullOrEmpty(showid))
                {
                    //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
                    RegGroupObj rgg = new RegGroupObj(fn);
                    rgg.updateGroupCurrentStep(urlQuery);

                    //*update reg_urlFlowID(current flowid) and reg_Stage(current step) of tb_RegDelegate table
                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    rgd.updateDelegateCurrentStep(urlQuery);

                    string qID = getQID(showid, flowid, type);
                    string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                    string regno = cFun.DecryptValue(urlQuery.DelegateID);
                    string route = string.Empty;

                    string questID = getQuestID(showid, flowid, type);
                    string questIDParam = string.Empty;
                    if(!string.IsNullOrEmpty(questID))
                    {
                        questIDParam = "&quest_id=" + questID;
                    }

                    FlowControler flwObj = new FlowControler(fn, urlQuery);
                    FlowControler flwController = new FlowControler(fn);
                    FlowMaster flwConfig = flwController.GetFlowMasterConfig(cFun.DecryptValue(urlQuery.FlowID));

                    if (flwConfig.FlowLimitDelegateQty == Number.One)
                    {
                        route = flwObj.MakeFullURL("http://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath + "/QuestionnaireExit", urlQuery.FlowID, showid, urlQuery.GoupRegID, urlQuery.CurrIndex);
                        //route = route.Replace("&", "%26");
                    }
                    else
                    {
                        route = flwObj.MakeFullURL("http://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath + "/QuestionnaireExit", urlQuery.FlowID, showid, urlQuery.GoupRegID, urlQuery.CurrIndex, regno);
                        //route = route.Replace("&", "%26");
                    }

                    string hyperlink = ConfigurationManager.AppSettings["QuestionnaireURL"] + "?qnaire_id=" + qID + "&user_id=" + regno + questIDParam + "&rtn_url=" + Server.UrlEncode(route);
                    this.questionaireFrame.Attributes.Add("src", hyperlink);
                    //string insert = "document.getElementById(\"ContentPlaceHolder1_questionaireFrame\").setAttribute(\"src\", \"  " + hyperlink + "\");";
                    //ScriptManager.RegisterStartupScript(this, GetType(), "", insert, true);

                    insertLogFlowAction(groupid, regno, rlgobj.actview, urlQuery);
                }
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cFun.DecryptValue(urlQuery.FlowID);
        string step = cFun.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    private string getQID(string showid, string flowid, string type)
    {
        QuestionnaireControler qCtr = new QuestionnaireControler(fn);
        DataTable dt = qCtr.getQID(type, flowid, showid);
        if (dt.Rows.Count > 0)
            return dt.Rows[0]["SQ_QAID"].ToString();
        else
            return "QLST";
    }
    private string getQuestID(string showid, string flowid, string type)
    {
        string questid = string.Empty;
        try
        {
            QuestionnaireControler qCtr = new QuestionnaireControler(fn);
            DataTable dt = qCtr.getQID(type, flowid, showid);
            if (dt.Rows.Count > 0)
            {
                questid = dt.Rows[0]["SQ_QUESID"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["SQ_QUESID"].ToString()) ? dt.Rows[0]["SQ_QUESID"].ToString() : "") : "";
            }
        }
        catch(Exception ex)
        { }

        return questid;
    }
}