﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="RegDelegate_IndexV.aspx.cs" Inherits="RegDelegate_IndexV" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .red
        {
            color:red;
        }
    </style>

    <script type="text/javascript">
        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="form-group">
        <div class="row">
            <div class="col-md-12" style="font-size:14px;display:none;">
                Records submitted for SPF clearance shall be locked and cannot be edited. To make any changes to the record, please email to Jordan Soh at <a href="mailto:jordansoh@corpit.com.sg">jordansoh@corpit.com.sg</a>.
                <br />
                To submit, photos, please click on upload photo.
                <br /><br />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
            <%--<div class="col-md-5">&nbsp;</div>--%>
            <%--<div class="col-md-2" style="margin-right:-45px;">
                <asp:Button runat="server" ID="btnConfChange" CssClass="btn btn-primary pull-right" Text="Change Conference Quantity" OnClick="btnConfChange_Click" CausesValidation="false"/>
            </div>--%>
            <div id="divNext" runat="server" class="col-md-offset-5 col-md-2 col-xs-8 GIndexNext" style="padding-right:0px;">
                <asp:Button runat="server" ID="btnNext" CssClass="btn MainButton" Text="Add New Registration" OnClick="btnNext_Click" CausesValidation="false"/>
            </div>
            <div id="divCheckout" runat="server" class="col-md-2 col-xs-4 GIndexCheckout">
                <asp:Button runat="server" ID="btnCheckout" CssClass="btn MainButton pull-right" Text="Submit" OnClick="btnCheckout_Click" CausesValidation="false"/>
            </div>
            </div>
        </div>
    </div>

    <div id="divList" runat="server">
        <div style="overflow-x:auto;overflow-y:hidden;" class="table-responsive">
        <table style="width:100%;" class="table table-bordered">
            <thead>
                <tr>
                    <th>No.</th>

                    <%--<th id="trSPFStatus" runat="server" scope="row">
                        <asp:Label ID="lblSPFStatus" runat="server" Text="SPF Status"></asp:Label>
                    </th>--%>

                    <th id="trPassno" runat="server" scope="row">
                        <asp:Label ID="lblPassNo" runat="server" Text="NRIC/Passport No."></asp:Label>
                    </th>
                    <th id="trSalutation" runat="server" scope="row">
                        <asp:Label ID="lblSal" runat="server" Text="Title"></asp:Label>
                    </th>
                    <th id="trFName" runat="server" scope="row">
                        <asp:Label ID="lblFName" runat="server" Text="First Name"></asp:Label>
                    </th>
                    <th id="trLName" runat="server" scope="row">
                        <asp:Label ID="lblLName" runat="server" Text="Surname"></asp:Label>
                    </th>
                    <th id="trOName" runat="server" scope="row">
                        <asp:Label ID="lblOName" runat="server" Text="Other Name"></asp:Label>
                    </th>
                    <th id="trProfession" runat="server" scope="row">
                        <asp:Label ID="lblProfession" runat="server" Text="Profession"></asp:Label>
                    </th>
                    <th id="trOrg" runat="server" scope="row">
                        <asp:Label ID="lblOrg" runat="server" Text="Organization"></asp:Label>
                    </th>
                    <th id="trDOB" runat="server" scope="row">
                        <asp:Label ID="lblDOB" runat="server" Text="Date of Birth"></asp:Label>
                    </th>
                    <th id="trGender" runat="server" scope="row">
                        <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                    </th>
                    <th id="trIsReg" runat="server" scope="row">
                        <asp:Label ID="lblIsReg" runat="server" Text="Are you a Singapore registered doctor/nurse/pharmacist?"></asp:Label>
                    </th>
                    <th id="trRegSpecific" runat="server" scope="row">
                        <asp:Label ID="lblRegSpecific" runat="server" Text="MCR/SNB/PRN"></asp:Label>
                    </th>
                    <th id="trIDNo" runat="server" scope="row">
                        <asp:Label ID="lblIDNo" runat="server" Text="MCR/SNB/PRN No."></asp:Label>
                    </th>
                    <th id="trInstitution" runat="server" scope="row">
                        <asp:Label ID="lblInstitution" runat="server" Text="Institution"></asp:Label>
                    </th>
                    <th id="trDept" runat="server" scope="row">
                        <asp:Label ID="lblDept" runat="server" Text="Department"></asp:Label>
                    </th>
                    <th id="trAddress1" runat="server" scope="row">
                        <asp:Label ID="lblAddress1" runat="server" Text="Address1"></asp:Label>
                    </th>
                    <th id="trAddress2" runat="server" scope="row">
                        <asp:Label ID="lblAddress2" runat="server" Text="Address2"></asp:Label>
                    </th>
                    <th id="trAddress3" runat="server" scope="row">
                        <asp:Label ID="lblAddress3" runat="server" Text="Address3"></asp:Label>
                    </th>
                    <th id="trDesignation" runat="server" scope="row">
                        <asp:Label ID="lblDesignation" runat="server" Text="Designation"></asp:Label>
                    </th>
                    <th id="trAddress4" runat="server" scope="row">
                        <asp:Label ID="lblAddress4" runat="server" Text="Address4"></asp:Label>
                    </th>
                    <th id="trCity" runat="server" scope="row">
                        <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                    </th>
                    <th id="trState" runat="server" scope="row">
                        <asp:Label ID="lblState" runat="server" Text="State"></asp:Label>
                    </th>
                    <th id="trPostal" runat="server" scope="row">
                        <asp:Label ID="lblPostal" runat="server" Text="Postal Code"></asp:Label>
                    </th>
                    <th id="trCountry" runat="server" scope="row">
                        <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                    </th>
                    <th id="trRCountry" runat="server" scope="row">
                        <asp:Label ID="lblRCountry" runat="server" Text="RCountry"></asp:Label>
                    </th>
                    <th id="trTel" runat="server" scope="row">
                        <asp:Label ID="lblTel" runat="server" Text="Telephone"></asp:Label>
                    </th>
                    <th id="trMobile" runat="server" scope="row">
                        <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                    </th>
                    <th id="trFax" runat="server" scope="row">
                        <asp:Label ID="lblFax" runat="server" Text="Fax"></asp:Label>
                    </th>
                    <th id="trEmail" runat="server" scope="row">
                        <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                    </th>
                        <th id="trAffiliation" runat="server" scope="row">
                        <asp:Label ID="lblAffiliation" runat="server" Text="Affiliation"></asp:Label>
                    </th>
                    <th id="trDietary" runat="server" scope="row">
                        <asp:Label ID="lblDietary" runat="server" Text="Dietary"></asp:Label>
                    </th>
                    <th id="trNationality" runat="server" scope="row">
                        <asp:Label ID="lblNationality" runat="server" Text="Nationality"></asp:Label>
                    </th>
                    <th id="trAge" runat="server" scope="row">
                        <asp:Label ID="lblAge" runat="server" Text="Age"></asp:Label>
                    </th>
                    <th id="trMembershipNo" runat="server" scope="row">
                        <asp:Label ID="lblMembershipNo" runat="server" Text="MembershipNo"></asp:Label>
                    </th>
                    <th id="trAdditional4" runat="server" scope="row">
                        <asp:Label ID="lblAdditional4" runat="server" Text="Additional4"></asp:Label>
                    </th>
                    <th id="trAdditional5" runat="server" scope="row">
                        <asp:Label ID="lblAdditional5" runat="server" Text="Additional5"></asp:Label>
                    </th>

                    <th id="trVName" runat="server" scope="row">
                        <asp:Label ID="lblVName" runat="server" Text="Visitor Name"></asp:Label>
                    </th>
                    <th id="trVDOB" runat="server" scope="row">
                        <asp:Label ID="lblVDOB" runat="server" Text="Visitor DOB"></asp:Label>
                    </th>
                    <th id="trVPass" runat="server" scope="row">
                        <asp:Label ID="lblVPass" runat="server" Text="Visitor Passport No."></asp:Label>
                    </th>
                    <th id="trVPassIssueDate" runat="server" scope="row">
                        <asp:Label ID="lblVPassIssueDate" runat="server" Text="Visitor Passport Issue Date"></asp:Label>
                    </th>
                    <th id="trVPassExpiry" runat="server" scope="row">
                        <asp:Label ID="lblVPassExpiry" runat="server" Text="Visitor Passport Expiry"></asp:Label>
                    </th>
                    <th id="trVEmbarkation" runat="server" scope="row">
                        <asp:Label ID="lblVEmbarkation" runat="server" Text="Visitor Port of Embarkation"></asp:Label>
                    </th>
                    <th id="trVArrivalDate" runat="server" scope="row">
                        <asp:Label ID="lblVArrivalDate" runat="server" Text="Visitor Arrival Date"></asp:Label>
                    </th>
                    <th id="trVCountry" runat="server" scope="row">
                        <asp:Label ID="lblVCountry" runat="server" Text="Visitor Country"></asp:Label>
                    </th>

                    <th id="trUDF_CName" runat="server" scope="row">
                        <asp:Label ID="lblUDF_CName" runat="server" Text="UDFC Name"></asp:Label>
                    </th>
                    <th id="trUDF_DelegateType" runat="server" scope="row">
                        <asp:Label ID="lblUDF_DelegateType" runat="server" Text="UDF Delegate Type"></asp:Label>
                    </th>
                    <th id="trUDF_ProfCategory" runat="server" scope="row">
                        <asp:Label ID="lblUDF_ProfCategory" runat="server" Text="UDF Prof Category"></asp:Label>
                    </th>
                    <th id="trUDF_CPcode" runat="server" scope="row">
                        <asp:Label ID="lblUDF_CPcode" runat="server" Text="UDFC Postal Code"></asp:Label>
                    </th>
                    <th id="trUDF_CLDepartment" runat="server" scope="row">
                        <asp:Label ID="lblUDF_CLDepartment" runat="server" Text="UDFCL Department"></asp:Label>
                    </th>
                    <th id="trUDF_CAddress" runat="server" scope="row">
                        <asp:Label ID="lblUDF_CAddress" runat="server" Text="UDFC Address"></asp:Label>
                    </th>
                    <th id="trUDF_CLCompany" runat="server" scope="row">
                        <asp:Label ID="lblUDF_CLCompany" runat="server" Text="UDFCL Company"></asp:Label>
                    </th>
                    <th id="trUDF_CCountry" runat="server" scope="row">
                        <asp:Label ID="lblUDF_CCountry" runat="server" Text="UDFC Country"></asp:Label>
                    </th>

                    <th id="trSupName" runat="server" scope="row">
                        <asp:Label ID="lblSupName" runat="server" Text="Supervisor Name"></asp:Label>
                    </th>
                    <th id="trSupDesignation" runat="server" scope="row">
                        <asp:Label ID="lblSupDesignation" runat="server" Text="Supervisor Designation"></asp:Label>
                    </th>
                    <th id="trSupContact" runat="server" scope="row">
                        <asp:Label ID="lblSupContact" runat="server" Text="Supervisor Contact"></asp:Label>
                    </th>
                    <th id="trSupEmail" runat="server" scope="row">
                        <asp:Label ID="lblSupEmail" runat="server" Text="Supervisor Email"></asp:Label>
                    </th>
                    <th>Actions</th>
                </tr>
            </thead>
        
            <tbody>
                <asp:Repeater ID="rptItem" runat="server" OnItemDataBound="rptitemdatabound"  OnItemCommand="rptitemcommand">
                    <ItemTemplate>
                        <%--<table style="border:solid 1px;">--%>
                            <%--<tr>
                                <td><asp:Label ID="lblRegistrationID" runat="server" Text="Registration ID"></asp:Label></td>
                                <td>:</td>
                                <td><asp:Label ID="_lblRegno" runat="server" Text="Label"></asp:Label></td>
                            </tr>--%>
                            <tr>
                                <td><%#Container.ItemIndex+1 %></td>

                                <%--<td id="tdSPFStatus" runat="server"><asp:Label ID="_lblSPFStatus" runat="server" Text='<%#Eval("reg_approveStatus").ToString() == "11" ? "Approve" : (Eval("reg_approveStatus").ToString() == "66" ? "Reject" : "Pending")%>'></asp:Label></td>--%>

                                <td id="tdPassno" runat="server"><asp:Label ID="_lblPassno" runat="server" Text='<%#Eval("reg_PassNo")%>'></asp:Label></td>

                                <td id="tdSalutation" runat="server"><asp:Label ID="_lblsal" runat="server" Text='<%# bindSalutation(Eval("reg_Salutation").ToString(), Eval("reg_SalutationOthers").ToString()) %>'></asp:Label></td>

                                <td id="tdFName" runat="server"><asp:Label ID="_lblFName" runat="server" Text='<%#Eval("reg_FName")%>'></asp:Label></td>

                                <td id="tdLName" runat="server"><asp:Label ID="_lblLName" runat="server" Text='<%#Eval("reg_LName")%>'></asp:Label></td>

                                <td id="tdOName" runat="server"><asp:Label ID="_lblOName" runat="server" Text='<%#Eval("reg_OName")%>'></asp:Label></td>

                                <td id="tdProfession" runat="server"><asp:Label ID="_lblProfession" runat="server" Text='<%# bindProfession(Eval("reg_Profession").ToString())%>'></asp:Label></td>

                                <td id="tdOrg" runat="server"><asp:Label ID="_lblOrg" runat="server" Text='<%# bindOrganisation(Eval("reg_Organization").ToString())%>'></asp:Label></td>

                                <td id="tdDOB" runat="server"><asp:Label ID="_DOB" runat="server" Text='<%#convertDateOfBirth(Eval("reg_DOB").ToString())%>'></asp:Label></td>

                                <td id="tdGender" runat="server"><asp:Label ID="_lblGender" runat="server" Text='<%#Eval("reg_Gender")%>'></asp:Label></td>

                                <td id="tdIsReg" runat="server"><asp:Label ID="_lblIsReg" runat="server" Text='<%#Eval("reg_isReg") != null ? (Eval("reg_isReg").ToString() == "1" ? "Yes" : "No") : "No"%>'></asp:Label></td>

                                <td id="tdRegSpecific" runat="server"><asp:Label ID="_lblRegSpecific" runat="server" Text='<%#Eval("reg_sgregistered")%>'></asp:Label></td>

                                <td id="tdIDNo" runat="server"><asp:Label ID="_lblIDNo" runat="server" Text='<%#Eval("reg_IDno")%>'></asp:Label></td>

                                <td id="tdInstitution" runat="server"><asp:Label ID="_lblInstitution" runat="server" Text='<%# bindInstitution(Eval("reg_Institution").ToString())%>'></asp:Label></td>

                                <td id="tdDept" runat="server"><asp:Label ID="_lblDept" runat="server" Text='<%# bindDepartment(Eval("reg_Department").ToString())%>'></asp:Label></td>

                                <td id="tdAddress1" runat="server"><asp:Label ID="_lblAddress1" runat="server" Text='<%#Eval("reg_Address1")%>'></asp:Label></td>

                                <td id="tdAddress2" runat="server"><asp:Label ID="_lblAddress2" runat="server" Text='<%#Eval("reg_Address2")%>'></asp:Label></td>

                                <td id="tdAddress3" runat="server"><asp:Label ID="_lblAddress3" runat="server" Text='<%#Eval("reg_Address3")%>'></asp:Label></td>

                                <td id="tdDesignation" runat="server"><asp:Label ID="_lblDesignation" runat="server" Text='<%#Eval("reg_Designation")%>'></asp:Label></td>

                                <td id="tdAddress4" runat="server"><asp:Label ID="_lblAddress4" runat="server" Text='<%#Eval("reg_Address4")%>'></asp:Label></td>

                                <td id="tdCity" runat="server"><asp:Label ID="_lblCity" runat="server" Text='<%#Eval("reg_City")%>'></asp:Label></td>

                                <td id="tdState" runat="server"><asp:Label ID="_lblState" runat="server" Text='<%#Eval("reg_State")%>'></asp:Label></td>

                                <td id="tdPostal" runat="server"><asp:Label ID="_lblPostal" runat="server" Text='<%#Eval("reg_PostalCode")%>'></asp:Label></td>

                                <td id="tdCountry" runat="server"><asp:Label ID="_lblCountry" runat="server" Text='<%# bindCountry(Eval("reg_Country").ToString())%>'></asp:Label></td>

                                <td id="tdRCountry" runat="server"><asp:Label ID="_lblRCountry" runat="server" Text='<%# bindCountry(Eval("reg_RCountry").ToString())%>'></asp:Label></td>

                                <td id="tdTel" runat="server"><asp:Label ID="_lblTel" runat="server"><%# bindPhoneNo(Eval("reg_Telcc").ToString(), Eval("reg_Telac").ToString(), Eval("reg_Tel").ToString(), "Tel")%></asp:Label></td>

                                <td id="tdMobile" runat="server"><asp:Label ID="_lblMobile" runat="server"><%# bindPhoneNo(Eval("reg_Mobcc").ToString(), Eval("reg_Mobac").ToString(), Eval("reg_Mobile").ToString(), "Mob")%></asp:Label></td>

                                <td id="tdFax" runat="server"><asp:Label ID="_lblFax" runat="server"><%# bindPhoneNo(Eval("reg_Faxcc").ToString(), Eval("reg_Faxac").ToString(), Eval("reg_Fax").ToString(), "Fax")%></asp:Label></td>

                                <td id="tdEmail" runat="server"><asp:Label ID="_lblEmail" runat="server" Text='<%#Eval("reg_Email")%>'></asp:Label></td>

                                <td id="tdAffiliation" runat="server"><asp:Label ID="_lblAffiliation" runat="server" Text='<%# bindAffiliation(Eval("reg_Affiliation").ToString())%>'></asp:Label></td>

                                <td id="tdDietary" runat="server"><asp:Label ID="_lblDietary" runat="server" Text='<%# bindDietary(Eval("reg_Dietary").ToString())%>'></asp:Label></td>

                                <td id="tdNationality" runat="server"><asp:Label ID="_lblNationality" runat="server" Text='<%#Eval("reg_Nationality")%>'></asp:Label></td>

                                <td id="tdAge" runat="server"><asp:Label ID="_lblAge" runat="server" Text='<%#Eval("reg_Age")%>'></asp:Label></td>

                                <td id="tdMembershipNo" runat="server"><asp:Label ID="_lblMembershipNo" runat="server" Text='<%#Eval("reg_Membershipno")%>'></asp:Label></td>

                                <td id="tdAdditional4" runat="server"><asp:Label ID="_lblAdditional4" runat="server" Text='<%#Eval("reg_Additional4")%>'></asp:Label></td>

                                <td id="tdAdditional5" runat="server"><asp:Label ID="_lblAdditional5" runat="server" Text='<%#Eval("reg_Additional5")%>'></asp:Label></td>

                                <td id="tdVName" runat="server"><asp:Label ID="_lblVName" runat="server" Text='<%#Eval("reg_vName")%>'></asp:Label></td>

                                <td id="tdVDOB" runat="server"><asp:Label ID="_lblVDOB" runat="server" Text='<%#Eval("reg_vDOB")%>'></asp:Label></td>

                                <td id="tdVPass" runat="server"><asp:Label ID="_lblVPass" runat="server" Text='<%#Eval("reg_vPassno")%>'></asp:Label></td>

                                <td id="tdVPassIssueDate" runat="server"><asp:Label ID="_lblVPassIssueDate" runat="server" Text='<%#Eval("reg_vIssueDate")%>'></asp:Label></td>

                                <td id="tdVPassExpiry" runat="server"><asp:Label ID="_lblVPassExpiry" runat="server" Text='<%#Eval("reg_vPassexpiry")%>'></asp:Label></td>

                                <td id="tdVEmbarkation" runat="server"><asp:Label ID="_lblVEmbarkation" runat="server" Text='<%#Eval("reg_vEmbarkation")%>'></asp:Label></td>

                                <td id="tdVArrivalDate" runat="server"><asp:Label ID="_lblVArrivalDate" runat="server" Text='<%#Eval("reg_vArrivalDate")%>'></asp:Label></td>

                                <td id="tdVCountry" runat="server"><asp:Label ID="_lblVCountry" runat="server" Text='<%# bindCountry(Eval("reg_vCountry").ToString())%>'></asp:Label></td>

                                <td id="tdUDF_CName" runat="server"><asp:Label ID="_lblUDF_CName" runat="server" Text='<%#Eval("UDF_CName")%>'></asp:Label></td>

                                <td id="tdUDF_DelegateType" runat="server"><asp:Label ID="_lblUDF_DelegateType" runat="server" Text='<%#Eval("UDF_DelegateType")%>'></asp:Label></td>

                                <td id="tdUDF_ProfCategory" runat="server"><asp:Label ID="_lblUDF_ProfCategory" runat="server"><%#Eval("UDF_ProfCategory")%> <%#Eval("UDF_ProfCategoryOther")%></asp:Label></td>

                                <td id="tdUDF_CPcode" runat="server"><asp:Label ID="_lblUDF_CPcode" runat="server" Text='<%#Eval("UDF_CPcode")%>'></asp:Label></td>

                                <td id="tdUDF_CLDepartment" runat="server"><asp:Label ID="_lblUDF_CLDepartment" runat="server" Text='<%#Eval("UDF_CLDepartment")%>'></asp:Label></td>

                                <td id="tdUDF_CAddress" runat="server"><asp:Label ID="_lblUDF_CAddress" runat="server" Text='<%#Eval("UDF_CAddress")%>'></asp:Label></td>

                                <td id="tdUDF_CLCompany" runat="server"><asp:Label ID="_lblUDF_CLCompany" runat="server"><%#Eval("UDF_CLCompany")%><%#Eval("UDF_CLCompanyOther")%></asp:Label></td>

                                <td id="tdUDF_CCountry" runat="server"><asp:Label ID="_lblUDF_CCountry" runat="server" Text='<%# bindCountry(Eval("UDF_CCountry").ToString())%>'></asp:Label></td>

                                <td id="tdSupName" runat="server"><asp:Label ID="_lblSupName" runat="server" Text='<%#Eval("reg_SupervisorName")%>'></asp:Label></td>

                                <td id="tdSupDesignation" runat="server"><asp:Label ID="_lblSupDesignation" runat="server" Text='<%#Eval("reg_SupervisorDesignation")%>'></asp:Label></td>

                                <td id="tdSupContact" runat="server"><asp:Label ID="_lblSupContact" runat="server" Text='<%#Eval("reg_SupervisorContact")%>'></asp:Label></td>

                                <td id="tdSupEmail" runat="server"><asp:Label ID="_lblSupEmail" runat="server" Text='<%#Eval("reg_SupervisorEmail")%>'></asp:Label></td>

                                <td runat="server" id="tdedit">
                                    <asp:LinkButton ID="lbEdit" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RegNo")%>'
                                        CommandName="edit" CssClass="btn MainButton RegIndexEditBtnStyle" Visible='<%#EditButtonDisable(Eval("Regno").ToString())%>' style="margin-bottom:10px;">
                                        <span class="glyphicon glyphicon-edit"></span>Edit</asp:LinkButton>
                                    <asp:LinkButton ID="lbDelete" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RegNo")%>'
                                        CommandName="deleteDelegate" CssClass="btn MainButton" Visible='<%#EditButtonDisable(Eval("Regno").ToString())%>'
                                        OnClientClick="return confirm('Are you sure you want delete?');">
                                        <span class="glyphicon glyphicon-trash"></span>Delete</asp:LinkButton>
                                    <asp:LinkButton ID="lbUploadPhoto" runat="server" CausesValidation="false" CommandArgument='<%#Eval("RegNo")%>'
                                        CommandName="uploadphoto" CssClass="btn MainButton" Visible='<%#UploadButtonVisible(Eval("Regno").ToString())%>'>
                                        <span class="glyphicon glyphicon-edit"></span>Upload Photo</asp:LinkButton>
                                </td>
                            </tr>
                        <%--</table>--%>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
            
        </table>
    </div>
    </div>

</asp:Content>

