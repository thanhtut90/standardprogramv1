﻿using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reg_Receipt : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    private static string checkingIDEMShowName = "IDEM Online Registration";

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
        {
            Label mpLabel = (Label)Page.Master.FindControl("SiteHeader");
            if (mpLabel != null)
            {
                mpLabel.Text = ReLoginStaticValueClass.regReceiptName;
            }
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string flwID = cFun.DecryptValue(urlQuery.FlowID);
        FlowControler fCtrl = new FlowControler(fn);
        FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flwID);

        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
        {
            if (!string.IsNullOrEmpty(Session["Showid"].ToString()))
            {
                SetSiteMaster(Session["Showid"].ToString());
            }
        }
        else
        {
            Response.Redirect("ReLogin.aspx?Event=" + fmaster.FlowName.Trim());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string flwID = cFun.DecryptValue(urlQuery.FlowID);
            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flwID);
            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string currentstage = Session["regstage"].ToString();

                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);

                InvoiceControler invControler = new InvoiceControler(fn);
                StatusSettings statusSet = new StatusSettings(fn);

                FlowControler Flw = new FlowControler(fn);

                FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowid);
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    if (Session["RegStatus"].ToString() == statusSet.Success.ToString())
                    {
                        string invStatus = invControler.getInvoiceStatus(groupid);
                        if (invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString())
                        {
                            divIncompleteLink.Visible = true;
                            divReceiptMessage.Visible = false;

                            string page = ReLoginStaticValueClass.regDelegateIndexPage;
                            currentstage = Flw.GetConirmationStage(flowid, SiteDefaultValue.constantRegDelegateIndexPage);

                            string route = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage);
                            hpLink.NavigateUrl = route;
                        }
                        else if(invStatus == statusSet.Success.ToString())
                        {
                            divIncompleteLink.Visible = false;
                            divReceiptMessage.Visible = true;

                            string regno = string.Empty;
                            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
                            if (invListObj != null && invListObj.Count > 0)
                            {
                                int x = 0;
                                foreach (Invoice invObj in invListObj)
                                {
                                    string invoiceID = invObj.InvoiceID;
                                    string invoiceNo = invObj.InvoiceNo;
                                    ddlCompleteInvoices.Items.Add(invoiceNo);
                                    ddlCompleteInvoices.Items[x].Value = invoiceID;
                                    x++;
                                    divCompleteInvoices.Visible = true;
                                    divIDEMMsg.Visible = false;
                                    if (shw.SHW_Name == checkingIDEMShowName)
                                    {
                                        divIDEMMsg.Visible = true;
                                    }
                                }

                                ddlCompleteInvoices_SelectedIndexChanged(this, null);
                            }

                            //string pdfRegno = groupid;

                            //string filePath = Server.MapPath("~/PDF/" + "receipt_" + pdfRegno + ".pdf");

                            //if (File.Exists(filePath))
                            //{
                            //    Response.Clear();

                            //    Response.ContentType = "application/pdf";

                            //    Response.WriteFile(filePath);

                            //    Response.Flush();
                            //}
                            //else
                            //{
                            //    divReceiptMessage.Visible = true;
                            //    lblMessage.Visible = true;
                            //}
                        }
                    }
                }
                else
                {
                    FlowControler flwObj = new FlowControler(fn, flowid, currentstage);
                    string page = flwObj.CurrIndexModule;

                    if (Session["Regno"] != null)
                    {
                        string regno = Session["Regno"].ToString();

                        string invStatus = invControler.getInvoiceStatus(regno);
                        if (invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString())
                        {
                            divIncompleteLink.Visible = true;
                            divReceiptMessage.Visible = false;

                            bool confExist = Flw.checkPageExist(flowid, SiteDefaultValue.constantConfName);
                            if (confExist)
                            {
                                page = SiteDefaultValue.constantConfirmationPage;//ReLoginStaticValueClass.regConfirmationPage;
                                currentstage = Flw.GetConirmationStage(flowid, SiteDefaultValue.constantConfirmationPage);

                                string route = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage, regno);
                                hpLink.NavigateUrl = route;
                            }
                            else
                            {
                                FlowMaster flwConfig = Flw.GetFlowMasterConfig(cFun.DecryptValue(flowid));
                                if (flwConfig.FlowLimitDelegateQty == YesOrNo.Yes)
                                {
                                    string route = flwObj.MakeFullURL(page, flowid, showid, groupid, currentstage);
                                    hpLink.NavigateUrl = route;
                                }
                                else
                                {
                                    string route = flwObj.MakeFullURL(page, flowid, showid, groupid, currentstage, regno);
                                    hpLink.NavigateUrl = route;
                                }
                            }
                        }
                        else if (invStatus == statusSet.Success.ToString())
                        {
                            divIncompleteLink.Visible = false;
                            divReceiptMessage.Visible = true;

                            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
                            if (invListObj != null && invListObj.Count > 0)
                            {
                                int x = 0;
                                foreach (Invoice invObj in invListObj)
                                {
                                    string invoiceID = invObj.InvoiceID;
                                    string invoiceNo = invObj.InvoiceNo;
                                    ddlCompleteInvoices.Items.Add(invoiceNo);
                                    ddlCompleteInvoices.Items[x].Value = invoiceID;
                                    x++;
                                    divCompleteInvoices.Visible = true;
                                    divIDEMMsg.Visible = false;
                                    if (shw.SHW_Name == checkingIDEMShowName)
                                    {
                                        divIDEMMsg.Visible = true;
                                    }
                                }

                                ddlCompleteInvoices_SelectedIndexChanged(this, null);
                            }

                            //string pdfRegno = regno;

                            //string filePath = Server.MapPath("~/PDF/" + "receipt_" + pdfRegno + ".pdf");

                            //if (File.Exists(filePath))
                            //{
                            //    Response.Clear();

                            //    Response.ContentType = "application/pdf";

                            //    Response.WriteFile(filePath);

                            //    Response.Flush();
                            //}
                            //else
                            //{
                            //    divReceiptMessage.Visible = true;
                            //    lblMessage.Visible = true;
                            //}
                        }
                    }
                }
            }
            else
            {
                //Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
                Response.Redirect("ReLogin.aspx?Event=" + fmaster.FlowName.Trim());/*changed on 6-5-2019*/
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMasterReLogin;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    protected void ddlCompleteInvoices_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCompleteInvoices.Items.Count > 0)
        {
            string selectedInvoiceID = ddlCompleteInvoices.SelectedItem.Value;
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());

            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string currentstage = Session["regstage"].ToString();

                InvoiceControler invControler = new InvoiceControler(fn);
                StatusSettings statusSet = new StatusSettings(fn);

                FlowControler Flw = new FlowControler(fn);

                FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowid);
                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                {
                    if (Session["RegStatus"].ToString() == statusSet.Success.ToString())
                    {
                        string invStatus = invControler.getInvoiceStatus(groupid);
                        if (invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString())
                        {
                            divIncompleteLink.Visible = true;
                            divReceiptMessage.Visible = false;

                            string page = ReLoginStaticValueClass.regDelegateIndexPage;
                            currentstage = Flw.GetConirmationStage(flowid, SiteDefaultValue.constantRegDelegateIndexPage);

                            string route = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage);
                            hpLink.NavigateUrl = route;
                        }
                        else if (invStatus == statusSet.Success.ToString())
                        {
                            divIncompleteLink.Visible = false;
                            divReceiptMessage.Visible = true;

                            string regno = string.Empty;
                            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                            if(!string.IsNullOrEmpty(selectedInvoiceID))
                            {
                                string fullUrl = "?SHW=" + cFun.EncryptValue(showid) + "&DID=" + cFun.EncryptValue(invOwnerID) + "&VDD=" + cFun.EncryptValue(selectedInvoiceID);
                                string page = "DownloadReceiptLetter.aspx";
                                string route = page + fullUrl;
                                string script = String.Format("window.open('{0}','_blank');", route);
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                            }
                            //string pdfRegno = groupid;

                            //string filePath = Server.MapPath("~/PDF/" + "receipt_" + pdfRegno + ".pdf");

                            //if (File.Exists(filePath))
                            //{
                            //    Response.Clear();

                            //    Response.ContentType = "application/pdf";

                            //    Response.WriteFile(filePath);

                            //    Response.Flush();
                            //}
                            //else
                            //{
                            //    divReceiptMessage.Visible = true;
                            //    lblMessage.Visible = true;
                            //}
                        }
                    }
                }
                else
                {
                    FlowControler flwObj = new FlowControler(fn, flowid, currentstage);
                    string page = flwObj.CurrIndexModule;

                    if (Session["Regno"] != null)
                    {
                        string regno = Session["Regno"].ToString();

                        string invStatus = invControler.getInvoiceStatus(regno);
                        if (invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString())
                        {
                            divIncompleteLink.Visible = true;
                            divReceiptMessage.Visible = false;

                            bool confExist = Flw.checkPageExist(flowid, SiteDefaultValue.constantConfName);
                            if (confExist)
                            {
                                page = ReLoginStaticValueClass.regConfirmationPage;
                                currentstage = Flw.GetConirmationStage(flowid, SiteDefaultValue.constantConfirmationPage);

                                string route = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage, regno);
                                hpLink.NavigateUrl = route;
                            }
                            else
                            {
                                FlowMaster flwConfig = Flw.GetFlowMasterConfig(cFun.DecryptValue(flowid));
                                if (flwConfig.FlowLimitDelegateQty == YesOrNo.Yes)
                                {
                                    string route = flwObj.MakeFullURL(page, flowid, showid, groupid, currentstage);
                                    hpLink.NavigateUrl = route;
                                }
                                else
                                {
                                    string route = flwObj.MakeFullURL(page, flowid, showid, groupid, currentstage, regno);
                                    hpLink.NavigateUrl = route;
                                }
                            }
                        }
                        else if (invStatus == statusSet.Success.ToString())
                        {
                            divIncompleteLink.Visible = false;
                            divReceiptMessage.Visible = true;

                            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                            if (!string.IsNullOrEmpty(selectedInvoiceID))
                            {
                                string fullUrl = "?SHW=" + cFun.EncryptValue(showid) + "&DID=" + cFun.EncryptValue(invOwnerID) + "&VDD=" + cFun.EncryptValue(selectedInvoiceID);
                                page = "DownloadReceiptLetter.aspx";
                                string route = page + fullUrl;
                                string script = String.Format("window.open('{0}','_blank');", route);
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                            }

                            //string pdfRegno = regno;

                            //string filePath = Server.MapPath("~/PDF/" + "receipt_" + pdfRegno + ".pdf");

                            //if (File.Exists(filePath))
                            //{
                            //    Response.Clear();

                            //    Response.ContentType = "application/pdf";

                            //    Response.WriteFile(filePath);

                            //    Response.Flush();
                            //}
                            //else
                            //{
                            //    divReceiptMessage.Visible = true;
                            //    lblMessage.Visible = true;
                            //}
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
            }
        }
    }
}