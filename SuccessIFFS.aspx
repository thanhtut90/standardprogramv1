﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="SuccessIFFS.aspx.cs" Inherits="SuccessIFFS" %>
<%@ MasterType VirtualPath="~/Registration.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel runat="server" ID="PanelMsg">
        <div class="form-group">
            <div class="form-group row">
                <div class="col-md-2">
                    &nbsp;
                </div>
                <div class="col-md-8">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td>
                                    <span>
                                        Thank you for registering for <a href="https://www.iffs.com.sg/" style="text-decoration:none;color:#337AB7;">International Furniture Fair Singapore 2019/36<sup>th</sup> ASEAN Furniture Show</a> 
                                        and <a href="https://nookasia.com.sg/" style="text-decoration:none;color:#337AB7;">NOOK Asia 2019</a> which will be held from 9 – 12 March 2019 at Sands Expo and Convention Centre, Marina Bay Sands.
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>
                                        An email confirmation will be sent to your registered email. If you do not receive your email confirmation, please contact our Visitor Promotions team at <a href="mailto:visitor@iffs.com.sg" style="text-decoration:none;color:#337AB7;">visitor@iffs.com.sg</a>.
                                        <%--An email confirmation will be sent to your registered email upon approval from the Show Management Team.--%>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span><asp:Button runat="server" ID="btnRegColleague" CssClass="btn btn-primary" Text="REGISTER COLLEAGUES" OnClick="btnRegColleague_Click" /></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p style="padding-bottom:30px;">
                        <asp:Label ID="lblSuccessMessage" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div  > 
      <iframe runat="server" id="ShowPDF" src="" width="100%" height="1200" scrolling="no"  frameborder="0" visible="false" ></iframe>    
        </div>
</asp:Content>

