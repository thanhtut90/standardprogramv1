﻿using Corpit.Registration;
using Corpit.Site.Email;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reg_DownloadConfirmation : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
        {
            Label mpLabel = (Label)Page.Master.FindControl("SiteHeader");
            if (mpLabel != null)
            {
                mpLabel.Text = ReLoginStaticValueClass.regReceiptName;
            }
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string flwID = cFun.DecryptValue(urlQuery.FlowID);
        FlowControler fCtrl = new FlowControler(fn);
        FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flwID);

        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
        {
            if (!string.IsNullOrEmpty(Session["Showid"].ToString()))
            {
                SetSiteMaster(Session["Showid"].ToString());
            }
        }
        else
        {
            Response.Redirect("ReLogin.aspx?Event=" + fmaster.FlowName.Trim());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string flwID = cFun.DecryptValue(urlQuery.FlowID);
            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flwID);
            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string currentstage = Session["regstage"].ToString();

                InvoiceControler invControler = new InvoiceControler(fn);
                StatusSettings statusSet = new StatusSettings(fn);
                FlowControler Flw = new FlowControler(fn);
                bool confExist = Flw.checkPageExist(flowid, SiteDefaultValue.constantConfName);

                bool isGroup = false;
                if (Session["Regno"] == null)
                {
                    isGroup = true;
                }
                FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowid);
                //if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                if(isGroup)
                {
                    #region contact person
                    if (Session["RegStatus"].ToString() == statusSet.Success.ToString())
                    {
                        string invStatus = invControler.getInvoiceStatus(groupid);
                        if (confExist)
                        {
                            if (invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString() || invStatus == RegClass.noInvoice)
                            {
                                divIncompleteLink.Visible = true;
                                divConfirmationMessage.Visible = false;

                                string page = ReLoginStaticValueClass.regDelegateIndexPage;
                                currentstage = Flw.GetConirmationStage(flowid, SiteDefaultValue.constantRegDelegateIndexPage);

                                string route = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage);
                                hpLink.NavigateUrl = route;
                            }
                            else if (invStatus == statusSet.Success.ToString())
                            {
                                divIncompleteLink.Visible = false;
                                divConfirmationMessage.Visible = true;

                                RegDelegateObj rgd = new RegDelegateObj(fn);
                                DataTable dt = new DataTable();
                                dt = rgd.getRegDelegateByGroupID(groupid, showid);
                                if(dt.Rows.Count > 0)
                                {
                                    int x = 0;
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        string indivRegno = dr["Regno"].ToString();
                                        ddlCompleteInvoices.Items.Add(indivRegno);
                                        ddlCompleteInvoices.Items[x].Value = indivRegno;
                                        x++;
                                        divCompleteInvoices.Visible = true;
                                    }

                                    ddlCompleteInvoices_SelectedIndexChanged(this, null);
                                }
                                #region comment
                                //string pdfRegno = groupid;

                                //string filePath = Server.MapPath("~/PDF/" + "receipt_" + pdfRegno + ".pdf");

                                //if (File.Exists(filePath))
                                //{
                                //    Response.Clear();

                                //    Response.ContentType = "application/pdf";

                                //    Response.WriteFile(filePath);

                                //    Response.Flush();
                                //}
                                //else
                                //{
                                //    divConfirmationMessage.Visible = true;
                                //    lblMessage.Visible = true;
                                //}
                                #endregion
                            }
                        }
                        else
                        {
                            RegDelegateObj rgd = new RegDelegateObj(fn);
                            DataTable dt = new DataTable();
                            dt = rgd.getRegDelegateByGroupID(groupid, showid);
                            if (dt.Rows.Count > 0)
                            {
                                int x = 0;
                                foreach (DataRow dr in dt.Rows)
                                {
                                    string indivRegno = dr["Regno"].ToString();
                                    ddlCompleteInvoices.Items.Add(indivRegno);
                                    ddlCompleteInvoices.Items[x].Value = indivRegno;
                                    x++;
                                    divCompleteInvoices.Visible = true;
                                }

                                ddlCompleteInvoices_SelectedIndexChanged(this, null);
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region delegate
                    FlowControler flwObj = new FlowControler(fn, flowid, currentstage);
                    string page = flwObj.CurrIndexModule;

                    if (Session["Regno"] != null)
                    {
                        string regno = Session["Regno"].ToString();

                        if (confExist)
                        {
                            string invStatus = invControler.getInvoiceStatus(regno);
                            if (invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString() || invStatus == RegClass.noInvoice)
                            {
                                divIncompleteLink.Visible = true;
                                divConfirmationMessage.Visible = false;

                                page = SiteDefaultValue.constantConfirmationPage;//ReLoginStaticValueClass.regConfirmationPage;
                                currentstage = Flw.GetConirmationStage(flowid, SiteDefaultValue.constantConfirmationPage);

                                string route = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage, regno);
                                hpLink.NavigateUrl = route;
                            }
                            else if (invStatus == statusSet.Success.ToString())
                            {
                                divIncompleteLink.Visible = false;
                                divConfirmationMessage.Visible = true;

                                RegDelegateObj rgd = new RegDelegateObj(fn);
                                DataTable dt = new DataTable();
                                dt = rgd.getRegDelegateByGroupID(groupid, showid);
                                if (dt.Rows.Count > 0)
                                {
                                    int x = 0;
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        string indivRegno = dr["Regno"].ToString();
                                        ddlCompleteInvoices.Items.Add(indivRegno);
                                        ddlCompleteInvoices.Items[x].Value = indivRegno;
                                        x++;
                                        divCompleteInvoices.Visible = true;
                                    }

                                    ddlCompleteInvoices_SelectedIndexChanged(this, null);
                                }
                                #region comment
                                //string pdfRegno = regno;

                                //string filePath = Server.MapPath("~/PDF/" + "receipt_" + pdfRegno + ".pdf");

                                //if (File.Exists(filePath))
                                //{
                                //    Response.Clear();

                                //    Response.ContentType = "application/pdf";

                                //    Response.WriteFile(filePath);

                                //    Response.Flush();
                                //}
                                //else
                                //{
                                //    divConfirmationMessage.Visible = true;
                                //    lblMessage.Visible = true;
                                //}
                                #endregion
                            }
                        }
                        else
                        {
                            if (Session["RegStatus"].ToString() == statusSet.Success.ToString())
                            {
                                divIncompleteLink.Visible = false;
                                divConfirmationMessage.Visible = true;
                                if (!string.IsNullOrEmpty(regno))
                                {
                                    string fullUrl = "?SHW=" + cFun.EncryptValue(showid) + "&DID=" + cFun.EncryptValue(regno);// + "&VDD=" + cFun.EncryptValue(selectedInvoiceID);
                                    page = "DownloadConfirmationLetter.aspx";
                                    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                                    {
                                        page = "DownloadGMemberConfirmationLetter.aspx";
                                    }
                                    string route = page + fullUrl;
                                    string script = String.Format("window.open('{0}','_blank');", route);
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            else
            {
                //Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
                Response.Redirect("ReLogin.aspx?Event=" + fmaster.FlowName.Trim());/*changed on 6-5-2019*/
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMasterReLogin;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    protected void ddlCompleteInvoices_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCompleteInvoices.Items.Count > 0)
        {
            string selectedInvoiceID = ddlCompleteInvoices.SelectedItem.Value;
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());

            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string currentstage = Session["regstage"].ToString();

                InvoiceControler invControler = new InvoiceControler(fn);
                StatusSettings statusSet = new StatusSettings(fn);

                FlowControler Flw = new FlowControler(fn);
                bool confExist = Flw.checkPageExist(flowid, SiteDefaultValue.constantConfName);

                bool isGroup = false;
                if (Session["Regno"] == null)
                {
                    isGroup = true;
                }
                FlowMaster flwMasterConfig = Flw.GetFlowMasterConfig(flowid);
                //if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                if(isGroup)
                {
                    if (Session["RegStatus"].ToString() == statusSet.Success.ToString())
                    {
                        #region contact person
                        if (confExist)
                        {
                            string invStatus = invControler.getInvoiceStatus(groupid);
                            if (invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString() || invStatus == RegClass.noInvoice)
                            {
                                divIncompleteLink.Visible = true;
                                divConfirmationMessage.Visible = false;

                                string page = ReLoginStaticValueClass.regDelegateIndexPage;
                                currentstage = Flw.GetConirmationStage(flowid, SiteDefaultValue.constantRegDelegateIndexPage);

                                string route = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage);
                                hpLink.NavigateUrl = route;
                            }
                            else if (invStatus == statusSet.Success.ToString())
                            {
                                divIncompleteLink.Visible = false;
                                divConfirmationMessage.Visible = true;

                                string regno = string.Empty;
                                if (!string.IsNullOrEmpty(selectedInvoiceID))
                                {
                                    string fullUrl = "?SHW=" + cFun.EncryptValue(showid) + "&DID=" + cFun.EncryptValue(selectedInvoiceID);// + "&VDD=" + cFun.EncryptValue(selectedInvoiceID);
                                    string page = "DownloadGMemberConfirmationLetter.aspx";
                                    string route = page + fullUrl;
                                    string script = String.Format("window.open('{0}','_blank');", route);
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                                }
                                #region Comment
                                //string pdfRegno = groupid;

                                //string filePath = Server.MapPath("~/PDF/" + "receipt_" + pdfRegno + ".pdf");

                                //if (File.Exists(filePath))
                                //{
                                //    Response.Clear();

                                //    Response.ContentType = "application/pdf";

                                //    Response.WriteFile(filePath);

                                //    Response.Flush();
                                //}
                                //else
                                //{
                                //    divConfirmationMessage.Visible = true;
                                //    lblMessage.Visible = true;
                                //}
                                #endregion
                            }
                        }
                        else
                        {
                            divIncompleteLink.Visible = false;
                            divConfirmationMessage.Visible = true;

                            string regno = string.Empty;
                            if (!string.IsNullOrEmpty(selectedInvoiceID))
                            {
                                string fullUrl = "?SHW=" + cFun.EncryptValue(showid) + "&DID=" + cFun.EncryptValue(selectedInvoiceID);// + "&VDD=" + cFun.EncryptValue(selectedInvoiceID);
                                string page = "DownloadGMemberConfirmationLetter.aspx";
                                string route = page + fullUrl;
                                string script = String.Format("window.open('{0}','_blank');", route);
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                            }
                            #region Comment
                            //string pdfRegno = groupid;

                            //string filePath = Server.MapPath("~/PDF/" + "receipt_" + pdfRegno + ".pdf");

                            //if (File.Exists(filePath))
                            //{
                            //    Response.Clear();

                            //    Response.ContentType = "application/pdf";

                            //    Response.WriteFile(filePath);

                            //    Response.Flush();
                            //}
                            //else
                            //{
                            //    divConfirmationMessage.Visible = true;
                            //    lblMessage.Visible = true;
                            //}
                            #endregion
                        }
                        #endregion
                    }
                }
                else
                {
                    FlowControler flwObj = new FlowControler(fn, flowid, currentstage);
                    string page = flwObj.CurrIndexModule;

                    if (Session["Regno"] != null)
                    {
                        string regno = Session["Regno"].ToString();

                        if (confExist)
                        {
                            string invStatus = invControler.getInvoiceStatus(regno);
                            if (invStatus == statusSet.TTPending.ToString() || invStatus == statusSet.ChequePending.ToString())
                            {
                                divIncompleteLink.Visible = true;
                                divConfirmationMessage.Visible = false;

                                page = ReLoginStaticValueClass.regConfirmationPage;
                                currentstage = Flw.GetConirmationStage(flowid, SiteDefaultValue.constantConfirmationPage);

                                string route = Flw.MakeFullURL(page, flowid, showid, groupid, currentstage, regno);
                                hpLink.NavigateUrl = route;
                            }
                            else if (invStatus == statusSet.Success.ToString())
                            {
                                divIncompleteLink.Visible = false;
                                divConfirmationMessage.Visible = true;
                                if (!string.IsNullOrEmpty(selectedInvoiceID))
                                {
                                    string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
                                    string invID = "";
                                    Invoice invLatest = invControler.GetLatestCompleteInvoice(invOwnerID, groupid);
                                    string invSuccess = ((int)StatusValue.Success).ToString();
                                    if (invLatest != null)
                                    {
                                        if (invLatest.Status == invSuccess)
                                        {
                                            invID = invLatest.InvoiceID;
                                        }
                                    }
                                    string fullUrl = "?SHW=" + cFun.EncryptValue(showid) + "&DID=" + cFun.EncryptValue(selectedInvoiceID) + "&VDD=" + cFun.EncryptValue(invID);
                                    page = "DownloadConfirmationLetter.aspx";
                                    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                                    {
                                        page = "DownloadGMemberConfirmationLetter.aspx";
                                    }
                                    string route = page + fullUrl;
                                    string script = String.Format("window.open('{0}','_blank');", route);
                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                                }
                                #region Comment
                                //string pdfRegno = regno;

                                //string filePath = Server.MapPath("~/PDF/" + "receipt_" + pdfRegno + ".pdf");

                                //if (File.Exists(filePath))
                                //{
                                //    Response.Clear();

                                //    Response.ContentType = "application/pdf";

                                //    Response.WriteFile(filePath);

                                //    Response.Flush();
                                //}
                                //else
                                //{
                                //    divConfirmationMessage.Visible = true;
                                //    lblMessage.Visible = true;
                                //}
                                #endregion
                            }
                        }
                        else
                        {
                            divIncompleteLink.Visible = false;
                            divConfirmationMessage.Visible = true;
                            if (!string.IsNullOrEmpty(selectedInvoiceID))
                            {
                                string fullUrl = "?SHW=" + cFun.EncryptValue(showid) + "&DID=" + cFun.EncryptValue(selectedInvoiceID);// + "&VDD=" + cFun.EncryptValue(selectedInvoiceID);
                                page = "DownloadConfirmationLetter.aspx";
                                if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                                {
                                    page = "DownloadGMemberConfirmationLetter.aspx";
                                }
                                string route = page + fullUrl;
                                string script = String.Format("window.open('{0}','_blank');", route);
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", script, true);
                            }
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
            }
        }
    }
}