﻿using Corpit.Logging;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReLogin : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected static string reloginLockPage = "ReloginLock.aspx";
    protected static string reloginASCADelegateFlowName = "ASCADelegateReg";
    protected static string reloginASCADelegatePrivateFlowName = "ASCADelegateRegPrivate";
    private static string[] checkingMDASingaporeShowIDs = new string[] { "DAB399" };
    private static string exhibitorMDALoginUrl = "https://www.event-reg.biz/MDAOEM2020/login.aspx";

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = string.Empty;
        string flowid = string.Empty;
        if (Request.QueryString["Event"] != null)
        {
            tmpDataList tmpList = GetShowIDByEventName(Request.QueryString["Event"].ToString());
            if (string.IsNullOrEmpty(tmpList.showID))
                Response.Redirect("404.aspx");
            else
            {
                showid = tmpList.showID;
                flowid = tmpList.FlowID;
                if (!string.IsNullOrEmpty(showid))
                {
                    SetSiteMaster(showid);
                    if (showid == "OSH388")/*IDEM Registration*/
                    {
                        Response.Redirect("LandingIDEM");
                    }

                    if (checkingMDASingaporeShowIDs.Contains(showid))
                    {
                        Response.Redirect(exhibitorMDALoginUrl);
                    }
                }
            }
        }
        else
        {
            showid = cFun.DecryptValue(urlQuery.CurrShowID);
            flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                SetSiteMaster(showid);
                if (showid == "OSH388")/*IDEM Registration*/
                {
                    Response.Redirect("LandingIDEM");
                }

                if (checkingMDASingaporeShowIDs.Contains(showid))
                {
                    Response.Redirect(exhibitorMDALoginUrl);
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session.Clear();
            Session.Abandon();
            removeCookie();//***added on 5-11-2018
            //bool isLock = isRegLoginLock();
            //if (isLock)
            //{
            //    Response.Redirect(reloginLockPage + "?Event=" + Request.QueryString["Event"].ToString());
            //}
        }
    }

    public void removeCookie()
    {
        HttpCookie aCookie;
        string cookieName;
        int limit = Request.Cookies.Count;
        for (int i = 0; i < limit; i++)
        {
            cookieName = Request.Cookies[i].Name;
            aCookie = new HttpCookie(cookieName);
            aCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(aCookie);
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = "ReloginFormMaster.master";
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = masterPage;
        }
    }
    #endregion

    #region btnVerify_Click and use table tb_Registration and tb_loginlog & insert into tb_loginlog
    protected void btnVerify_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            logRegLogin();//***
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = string.Empty;
            string flowid = string.Empty;
            string eventName = "";
            if (Request.QueryString["Event"] != null)
            {
                /*added on 2-12-2019 for ASCA Individual Reg Private Link*/
                eventName = Request.QueryString["Event"].ToString();
                if (eventName == reloginASCADelegateFlowName)
                {
                    eventName = reloginASCADelegatePrivateFlowName;
                }
                /*added on 2-12-2019 for ASCA Individual Reg Private Link*/

                tmpDataList tmpList = GetShowIDByEventName(eventName);
                if (string.IsNullOrEmpty(tmpList.showID))
                    Response.Redirect("404.aspx");
                else
                {
                    showid = tmpList.showID;
                    flowid = tmpList.FlowID;
                }
            }
            else
            {
                showid = cFun.DecryptValue(urlQuery.CurrShowID);
                flowid = cFun.DecryptValue(urlQuery.FlowID);
            }

            Boolean isvalid = isValid();
            if (isvalid)
            {
                if (!string.IsNullOrEmpty(showid) && !string.IsNullOrEmpty(flowid))
                {
                    string username = cFun.solveSQL(txtUsername.Text.ToString().Trim());
                    string password = cFun.solveSQL(txtPassword.Text.ToString().Trim());
                    string cmdStr = string.Empty;
                    DataTable dt = new DataTable();
                    StatusSettings statusSet = new StatusSettings(fn);
                    InvoiceControler invControler = new InvoiceControler(fn);
                    StatusSettings stuSettings = new StatusSettings(fn);

                    FlowControler flwControl = new FlowControler(fn);
                    FlowMaster flwMasterConfig = flwControl.GetFlowMasterConfig(flowid);
                    if (flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                    {
                        RegGroupObj rgObj = new RegGroupObj(fn);
                        dt = rgObj.getGroupReLoginAuthenData(username, password, flowid);

                        if (dt.Rows.Count > 0)
                        {
                            string groupid = dt.Rows[0]["RegGroupID"].ToString();
                            Session["Groupid"] = groupid;
                            Session["Flowid"] = flowid;
                            Session["Showid"] = showid;
                            string regstage = dt.Rows[0]["RG_Stage"].ToString();
                            Session["regstage"] = regstage;
                            string regstatus = dt.Rows[0]["RG_Status"] != null ? dt.Rows[0]["RG_Status"].ToString() : "0";
                            Session["RegStatus"] = regstatus;

                            LoginLog lg = new LoginLog(fn);
                            string userip = GetUserIP();
                            lg.loginid = groupid;
                            lg.loginip = userip;
                            lg.logintype = LoginLogType.regLogin;
                            lg.saveLoginLog();

                            string lateststage = flwControl.GetLatestStep(flowid);

                            if (!string.IsNullOrEmpty(lateststage) && regstage == lateststage && cFun.ParseInt(regstatus) == statusSet.Success)
                            {
                                string invStatus = invControler.getInvoiceStatus(groupid);
                                if (invStatus == stuSettings.Pending.ToString())
                                {
                                    string page = ReLoginStaticValueClass.regIndexPage;

                                    bool isUnlock = isRegLoginUnLock();//**
                                    if (isUnlock)
                                    {
                                        string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                                        Response.Redirect(route, false);
                                    }
                                    else
                                    {
                                        Response.Redirect(reloginLockPage + "?Event=" + eventName);
                                    }
                                }
                                if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                    || invStatus == RegClass.noInvoice
                                    || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                                {
                                    string page = ReLoginStaticValueClass.regHomePage;

                                    bool isUnlock = isRegLoginUnLock();//**
                                    if (isUnlock)
                                    {
                                        string route = flwControl.MakeFullURL(page, flowid, showid);
                                        Response.Redirect(route, false);
                                    }
                                    else
                                    {
                                        Response.Redirect(reloginLockPage + "?Event=" + eventName);
                                    }
                                }
                            }
                            else
                            {
                                string invStatus = invControler.getInvoiceStatus(groupid);
                                if (invStatus == RegClass.noInvoice || invStatus == stuSettings.Pending.ToString())
                                {
                                    string page = ReLoginStaticValueClass.regIndexPage;

                                    bool isUnlock = isRegLoginUnLock();//**
                                    if (isUnlock)
                                    {
                                        string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                                        Response.Redirect(route, false);
                                    }
                                    else
                                    {
                                        Response.Redirect(reloginLockPage + "?Event=" + eventName);
                                    }
                                }
                                else if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                    || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                                {
                                    string page = ReLoginStaticValueClass.regHomePage;

                                    bool isUnlock = isRegLoginUnLock();//**
                                    if (isUnlock)
                                    {
                                        string route = flwControl.MakeFullURL(page, flowid, showid);
                                        Response.Redirect(route, false);
                                    }
                                    else
                                    {
                                        Response.Redirect(reloginLockPage + "?Event=" + eventName);
                                    }
                                }
                            }
                        }
                        else
                        {
                            //***added on 5-11-2018
                            txtUsername.Text = "";
                            txtPassword.Text = "";
                            //***added on 5-11-2018
                            lblerror.Text = "Wrong combination of Registration ID and Email Address";
                            lblerror.Visible = true;
                            bool isLock = isRegLoginLock();
                            if (isLock)
                            {
                                Response.Redirect(reloginLockPage + "?Event=" + eventName);
                            }
                        }
                    }
                    else
                    {
                        RegDelegateObj rdObj = new RegDelegateObj(fn);
                        dt = rdObj.getDelegateReLoginAuthenData(username, password, flowid);

                        if (dt.Rows.Count > 0)
                        {
                            string groupid = dt.Rows[0]["RegGroupID"].ToString();
                            string regno = dt.Rows[0]["Regno"].ToString();
                            Session["Regno"] = regno;
                            Session["Groupid"] = groupid;
                            Session["Flowid"] = flowid;
                            Session["Showid"] = showid;
                            string regstage = dt.Rows[0]["reg_Stage"].ToString();
                            Session["regstage"] = regstage;
                            string regstatus = dt.Rows[0]["reg_Status"] != null ? dt.Rows[0]["reg_Status"].ToString() : "0";
                            Session["RegStatus"] = regstatus;

                            LoginLog lg = new LoginLog(fn);
                            string userip = GetUserIP();
                            lg.loginid = groupid;
                            lg.loginip = userip;
                            lg.logintype = "Registration Login";
                            lg.saveLoginLog();

                            DataTable dtAbsSetting = getAbstractSetting(flowid, showid);/*Added checking (if abstract exists) on 9-4-2019*/
                            if (dtAbsSetting.Rows.Count > 0)
                            {
                                DataTable dtMenu = new DataTable();
                                dtMenu = flwControl.GetReloginMenu(flowid, "0");
                                if (dtMenu.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dtMenu.Rows)
                                    {
                                        string scriptid = dr["reloginref_scriptID"].ToString().Trim();
                                        string menuname = dr["reloginmenu_name"].ToString().Trim();
                                        string stepseq = dr["step_seq"].ToString().Trim();
                                        string menupageHeader = dr["step_label"].ToString().Trim();
                                        if (menuname == SiteDefaultValue.constantDelegateName)
                                        {
                                            if (flwMasterConfig.FlowType != SiteFlowType.FLOW_GROUP)
                                            {
                                                string path = Request.Url.AbsolutePath;
                                                string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                                string[] strpath = path.Split('/');
                                                if (strpath.Length > 1)
                                                {
                                                    path = strpath[1] + ".aspx";
                                                }
                                                else
                                                {
                                                    path = ".aspx";
                                                }

                                                bool isUnlock = isRegLoginUnLock();//**
                                                if (isUnlock)
                                                {
                                                    string page = scriptid;
                                                    string route = flwControl.MakeFullURL(page, flowid, showid, groupid, stepseq, regno);//regstage
                                                    Response.Redirect(route, false);
                                                }
                                                else
                                                {
                                                    Response.Redirect(reloginLockPage + "?Event=" + eventName);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string lateststage = flwControl.GetLatestStep(flowid);

                                if (!string.IsNullOrEmpty(lateststage) && regstage == lateststage && cFun.ParseInt(regstatus) == statusSet.Success)
                                {
                                    string invStatus = invControler.getInvoiceStatus(regno);
                                    if (invStatus == stuSettings.Pending.ToString())
                                    {
                                        string page = ReLoginStaticValueClass.regIndexPage;

                                        bool isUnlock = isRegLoginUnLock();//**
                                        if (isUnlock)
                                        {
                                            string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                                            Response.Redirect(route, false);
                                        }
                                        else
                                        {
                                            Response.Redirect(reloginLockPage + "?Event=" + eventName);
                                        }
                                    }
                                    else if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                        || invStatus == RegClass.noInvoice
                                        || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                                    {
                                        string page = ReLoginStaticValueClass.regHomePage;

                                        bool isUnlock = isRegLoginUnLock();//**
                                        if (isUnlock)
                                        {
                                            string route = flwControl.MakeFullURL(page, flowid, showid);
                                            Response.Redirect(route, false);
                                        }
                                        else
                                        {
                                            Response.Redirect(reloginLockPage + "?Event=" + eventName);
                                        }
                                    }
                                }
                                else
                                {
                                    string invStatus = invControler.getInvoiceStatus(regno);
                                    if (invStatus == RegClass.noInvoice || invStatus == stuSettings.Pending.ToString())
                                    {
                                        string page = ReLoginStaticValueClass.regIndexPage;

                                        bool isUnlock = isRegLoginUnLock();//**
                                        if (isUnlock)
                                        {
                                            string route = flwControl.MakeFullURL(page, flowid, showid, groupid, regstage);
                                            Response.Redirect(route, false);
                                        }
                                        else
                                        {
                                            Response.Redirect(reloginLockPage + "?Event=" + eventName);
                                        }
                                    }
                                    else if (invStatus == stuSettings.Success.ToString() || invStatus == stuSettings.Waived.ToString()
                                        || invStatus == stuSettings.TTPending.ToString() || invStatus == stuSettings.ChequePending.ToString())
                                    {
                                        string page = ReLoginStaticValueClass.regHomePage;

                                        bool isUnlock = isRegLoginUnLock();//**
                                        if (isUnlock)
                                        {
                                            string route = flwControl.MakeFullURL(page, flowid, showid);
                                            Response.Redirect(route, false);
                                        }
                                        else
                                        {
                                            Response.Redirect(reloginLockPage + "?Event=" + eventName);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            //***added on 5-11-2018
                            txtUsername.Text = "";
                            txtPassword.Text = "";
                            //***added on 5-11-2018
                            lblerror.Text = "Wrong combination of Registration ID and Email Address";
                            lblerror.Visible = true;
                            bool isLock = isRegLoginLock();
                            if (isLock)
                            {
                                Response.Redirect(reloginLockPage + "?Event=" + eventName);
                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
        }
    }
    #endregion

    protected Boolean isValid()
    {
        Boolean isvalid = true;

        if (string.IsNullOrEmpty(txtPassword.Text) || string.IsNullOrWhiteSpace(txtPassword.Text))
        {
            lblerror.Text = "Please insert your password";
            lblerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblerror.Visible = false;
        }
        if (string.IsNullOrEmpty(txtUsername.Text) || string.IsNullOrWhiteSpace(txtUsername.Text))
        {
            lblerror.Text = "Please insert your username";
            lblerror.Visible = true;
            isvalid = false;
        }
        else
        {
            lblerror.Visible = false;
        }
        return isvalid;
    }
    public string GetUserIP()
    {
        string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipList))
        {
            return ipList.Split(',')[0];
        }

        return Request.ServerVariables["REMOTE_ADDR"];
    }
    public class tmpDataList
    {
        public string showID { get; set; }
        public string FlowID { get; set; }
    }
    private tmpDataList GetShowIDByEventName(string eventName)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            /*added on 2-12-2019 for ASCA Individual Reg Private Link*/
            if(eventName == reloginASCADelegateFlowName)
            {
                eventName = reloginASCADelegatePrivateFlowName;
            }
            /*added on 2-12-2019 for ASCA Individual Reg Private Link*/

            cList.showID = "";
            cList.FlowID = "";
            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                cList.showID = dList.Rows[0]["showID"].ToString();
                cList.FlowID = dList.Rows[0]["FLW_ID"].ToString();
            }
        }
        catch { }
        return cList;
    }
    private void logRegLogin()
    {
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'";
            DataTable dtRelogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtRelogin.Rows.Count > 0)
            {
                string sqlReLoginUpdate = "Update tb_LogReLoginLocking Set relogin_attemptTimes=relogin_attemptTimes+1 Where relogin_machineip='" + machineIP + "'";
                fn.ExecuteSQL(sqlReLoginUpdate);
                string AttemptTime = fn.GetDataByCommand("Select relogin_attemptTimes From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'", "relogin_attemptTimes");
                int attempedTime = 0;
                int.TryParse(AttemptTime, out attempedTime);
                if (attempedTime > 5)
                {
                    /*added on 2-12-2019 for ASCA Individual Reg Private Link*/
                    string eventName = Request.QueryString["Event"].ToString();
                    if (eventName == reloginASCADelegateFlowName)
                    {
                        eventName = reloginASCADelegatePrivateFlowName;
                    }
                    /*added on 2-12-2019 for ASCA Individual Reg Private Link*/

                    Response.Redirect(reloginLockPage + "?Event=" + eventName);
                }
            }
            else
            {
                string sqlReLoginInsert = "Insert Into tb_LogReLoginLocking (relogin_machineip)"
                                + " Values ('" + machineIP + "')";
                fn.ExecuteSQL(sqlReLoginInsert);
                Session["ReloginMachineIP"] = machineIP;
            }
        }
        catch (Exception ex)
        { }
    }
    private bool isRegLoginLock()
    {
        bool isLock = false;
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'";
            DataTable dtRelogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtRelogin.Rows.Count > 0)
            {
                string sqlReLoginUpdate = string.Empty;
                string AttemptTime = fn.GetDataByCommand("Select relogin_attemptTimes From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'", "relogin_attemptTimes");
                int attempedTime = 0;
                int.TryParse(AttemptTime, out attempedTime);
                if (attempedTime > 5)
                {
                    sqlReLoginUpdate = "Update tb_LogReLoginLocking Set relogin_lockdatetime=getdate(),relogin_islock=1 Where relogin_machineip='" + machineIP + "'";
                    fn.ExecuteSQL(sqlReLoginUpdate);
                    isLock = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isLock;
    }
    private bool isRegLoginUnLock()
    {
        bool isUnlock = false;
        try
        {
            string machineIP = GetUserIP();
            string sql = "Select * From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'";
            DataTable dtRelogin = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            if (dtRelogin.Rows.Count > 0)
            {
                string sqlReLoginUpdate = string.Empty;
                string AttemptTime = fn.GetDataByCommand("Select relogin_lockdatetime From tb_LogReLoginLocking Where relogin_machineip='" + machineIP + "'", "relogin_lockdatetime");
                DateTime attempedDateTime = cFun.ParseDateTime(AttemptTime);
                DateTime datetimenow = DateTime.Now;
                TimeSpan duration = datetimenow - attempedDateTime;
                double durationTime = duration.TotalMinutes;
                if (durationTime >= 20)//***
                {
                    sqlReLoginUpdate = "Update tb_LogReLoginLocking Set relogin_lockdatetime=Null,relogin_islock=0,relogin_attemptTimes=0 Where relogin_machineip='" + machineIP + "'";
                    fn.ExecuteSQL(sqlReLoginUpdate);
                    isUnlock = true;
                }
            }
        }
        catch (Exception ex)
        { }

        return isUnlock;
    }

    private DataTable getAbstractSetting(string flowid, string showid)
    {
        DataTable dtResult = new DataTable();
        try
        {
            string sql = "Select * From tb_AbstractSettings Where ShowID=@ShowID And FlowID=@FlowID";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            pList.Add(spar1);
            pList.Add(spar2);
            dtResult = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
        }
        catch (Exception ex)
        { }

        return dtResult;
    }
}