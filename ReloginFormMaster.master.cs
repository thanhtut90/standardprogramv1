﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;

public partial class ReloginFormMaster : System.Web.UI.MasterPage
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected static string reloginASCADelegateFlowName = "ASCADelegateReg";
    protected static string reloginASCADelegatePrivateFlowName = "ASCADelegateRegPrivate";

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = string.Empty;
        string flowid = string.Empty;
        if (Request.QueryString["Event"] != null)
        {
            tmpDataList tmpList = GetShowIDByEventName(Request.QueryString["Event"].ToString());
            if (string.IsNullOrEmpty(tmpList.showID))
                Response.Redirect("404.aspx");
            else
            {
                showid = tmpList.showID;
                flowid = tmpList.FlowID;
                if (!string.IsNullOrEmpty(showid))
                {
                    InitSiteSettings(showid);
                }
            }
        }
        else
        {
            showid = cFun.DecryptValue(urlQuery.CurrShowID);
            flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                InitSiteSettings(showid);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = string.Empty;
            string flowid = string.Empty;
            if (Request.QueryString["Event"] != null)
            {
                tmpDataList tmpList = GetShowIDByEventName(Request.QueryString["Event"].ToString());
                if (string.IsNullOrEmpty(tmpList.showID))
                    Response.Redirect("404.aspx");
                else
                {
                    showid = tmpList.showID;
                    flowid = tmpList.FlowID;
                }
            }
            else
            {
                showid = cFun.DecryptValue(urlQuery.CurrShowID);
                flowid = cFun.DecryptValue(urlQuery.FlowID);
            }

            if (!string.IsNullOrEmpty(showid) && !string.IsNullOrEmpty(flowid))
            {
                LoadPageSetting();
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        SiteBanner.ImageUrl = sSetting.SiteBanner;
        Global.PageTitle = sSetting.SiteTitle;
        Global.Copyright = sSetting.SiteFooter;
        Global.PageBanner = sSetting.SiteBanner;

        if (!string.IsNullOrEmpty(sSetting.SiteCssBandlePath))
            BundleRefence.Path = "~/Content/" + sSetting.SiteCssBandlePath + "/";
    }
    private void LoadPageSetting()
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        SetPageSettting(flwObj.CurrIndexTiTle);
    }

    public void SetPageSettting(string title)
    {
        SiteHeader.Text = ""; //title;
    }
    #endregion

    #region bindMenuLink (bind ul, li in div(divMenu) dynamically according to flow)
    private void bindMenuLink()
    {
        //hpFirst.NavigateUrl = getRoute("1", "Reg_RegGroup.aspx");
        //hpSecond.NavigateUrl = getRoute("2", "Reg_RegDelegateIndex.aspx");
        //hpThird.NavigateUrl = getRoute("3", "Reg_Confirmation.aspx");

        #region Comment
        //if (Session["Groupid"] != null)
        //{
        //    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        //    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        //    string groupid = Session["Groupid"].ToString();
        //    RegGroupObj rgg = new RegGroupObj(fn);
        //    DataTable dtGroup = rgg.getRegGroupByID(groupid, showid);
        //    if (dtGroup.Rows.Count > 0)
        //    {
        //        string flowid = dtGroup.Rows[0]["RG_urlFlowID"].ToString();
        //        DataTable dtReloginFlow = fn.GetDatasetByCommand("Select * From ref_reloginflow Where flowid='" + flowid
        //            + "' Order By step_seq Asc", "ds").Tables[0];
        //        if (dtReloginFlow.Rows.Count > 0)
        //        {
        //            HtmlGenericControl ul = new HtmlGenericControl("ul");
        //            ul.Attributes.Add("class", "nav navbar-nav");
        //            ul.ID = "ulMenu";

        //            foreach (DataRow drReloginFlow in dtReloginFlow.Rows)
        //            {
        //                string stepseq = drReloginFlow["step_seq"].ToString();
        //                string scriptid = drReloginFlow["scriptid"].ToString();
        //                string label = drReloginFlow["step_label"].ToString();

        //                HtmlGenericControl li;
        //                li = new HtmlGenericControl("li");
        //                li.Attributes.Add("runat", "server");

        //                string path = Request.Url.AbsolutePath;
        //                string str=Request.Path.Substring(Request.Path.LastIndexOf("/"));
        //                string[] strpath = path.Split('/');
        //                if(strpath.Length > 1)
        //                {
        //                    path = strpath[1] + ".aspx";
        //                }
        //                else
        //                {
        //                    path = ".aspx";
        //                }
        //                if (scriptid == path)
        //                {
        //                    li.Attributes.Add("class", "active");
        //                }
        //                else
        //                {
        //                    li.Attributes.Remove("class");
        //                }

        //                li.Attributes.Add("id", "a" + stepseq);
        //                HyperLink hp = new HyperLink();
        //                hp.ID = "hp" + stepseq;
        //                hp.NavigateUrl = getRoute(stepseq, scriptid);
        //                hp.Text = label;
        //                li.Controls.Add(hp);
        //                ul.Controls.Add(li);
        //            }

        //            divMenu.Controls.Add(ul);
        //        }
        //    }
        //}
        #endregion

        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string groupid = Session["Groupid"].ToString();
            string flowid = Session["Flowid"].ToString();

            FlowControler fControl = new FlowControler(fn);

            DataTable dtMenu = new DataTable();
            dtMenu = fControl.GetReloginMenu(flowid, "0");
            if (dtMenu.Rows.Count > 0)
            {
                HtmlGenericControl ul = new HtmlGenericControl("ul");
                ul.Attributes.Add("class", "nav navbar-nav");
                ul.ID = "ulMenu";

                foreach (DataRow dr in dtMenu.Rows)
                {
                    string scriptid = dr["reloginref_scriptID"].ToString().Trim();
                    string menuname = dr["reloginmenu_name"].ToString().Trim();
                    string stepseq = dr["step_seq"].ToString().Trim();

                    HtmlGenericControl li;
                    li = new HtmlGenericControl("li");
                    li.Attributes.Add("runat", "server");

                    string path = Request.Url.AbsolutePath;
                    string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                    string[] strpath = path.Split('/');
                    if (strpath.Length > 1)
                    {
                        path = strpath[1] + ".aspx";
                    }
                    else
                    {
                        path = ".aspx";
                    }
                    if (scriptid == path)
                    {
                        li.Attributes.Add("class", "active");
                    }
                    else
                    {
                        li.Attributes.Remove("class");
                    }

                    li.Attributes.Add("id", "a" + stepseq);
                    HyperLink hp = new HyperLink();
                    hp.ID = "hp" + stepseq;
                    hp.NavigateUrl = getRoute(stepseq, scriptid);
                    hp.Text = menuname;
                    li.Controls.Add(hp);
                    ul.Controls.Add(li);
                }

                //divMenu.Controls.Add(ul);
            }
        }
    }
    #endregion

    #region getRoute (get route according to login id(Session["Groupid"]) but no use)
    public string getRoute(string step, string page)
    {
        string route = string.Empty;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null)
        {
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            FlowControler Flw = new FlowControler(fn);
            RegGroupObj rgp = new RegGroupObj(fn);
            string grpNum = Session["Groupid"].ToString();
            string FlowID = rgp.getFlowID(grpNum, showid);

            grpNum = cFun.EncryptValue(grpNum);
            route = Flw.MakeFullURL(page, FlowID, showid, grpNum, step);
        }
        else
        {
            Response.Redirect("ReLogin.aspx?SHW=" + urlQuery.CurrShowID + "&FLW=" + urlQuery.FlowID);
        }

        return route;
    }
    #endregion

    public class tmpDataList
    {
        public string showID { get; set; }
        public string FlowID { get; set; }
    }
    private tmpDataList GetShowIDByEventName(string eventName)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            /*added on 2-12-2019 for ASCA Individual Reg Private Link*/
            if (eventName == reloginASCADelegateFlowName)
            {
                eventName = reloginASCADelegatePrivateFlowName;
            }
            /*added on 2-12-2019 for ASCA Individual Reg Private Link*/

            cList.showID = "";
            cList.FlowID = "";
            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                cList.showID = dList.Rows[0]["showID"].ToString();
                cList.FlowID = dList.Rows[0]["FLW_ID"].ToString();
            }
        }
        catch { }
        return cList;
    }
}
