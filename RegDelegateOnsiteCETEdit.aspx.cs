﻿using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Corpit.Promo;
using Corpit.Logging;
using Corpit.BackendMaster;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using Corpit.RegOnsite;
using System.Text.RegularExpressions;

public partial class RegDelegateOnsiteCETEdit : System.Web.UI.Page
{
    #region DECLARATION
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    static string _OtherSal = "Other Salutation";
    static string _OtherProfession = "Other Profession";
    static string _OtherDept = "Other Department";
    static string _OtherOrg = "Other Organization";
    static string _OtherInstitution = "Other Institution";

    static string _strSingaporean = "Singaporean";
    static string _strSingaporePR = "Singapore PR";

    static string _strCompanyOrgType = "Company";
    static string _strGovernmentOrgType = "Government/Statutory Board";

    static string _strNgeeAnnPolytechnic = "Ngee Ann Polytechnic";
    static string _strRepublicPolytechnic = "Republic Polytechnic";
    static string _strTemasekPolytechnic = "Temasek Polytechnic";
    static string _IDNo = "IDNo";
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string regno = cFun.DecryptValue(urlQuery.DelegateID);
                bindDropdown();
                bindData(regno, groupid, showid);
            }
            else
            {
                string url = "~/Onsite/OnsiteSearch.aspx?Evt=" + cFun.EncryptValue(showid);
                Response.Redirect(url);
            }
        }
    }

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
        {
            Page.MasterPageFile = "RegistrationOnsite.master";
        }
    }
    #endregion

    #region bindDropdown & bind respective data to Salutation, Country, Affiliation, Dietary, Profession and OrgType dropdown lists
    protected void bindDropdown()
    {
        DataSet dsOrgType = new DataSet();

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        CommonDataObj cmdObj = new CommonDataObj(fn);
        dsOrgType = cmdObj.getOrganization(showid);

        string showHostCountryID = "";
        SiteSettings st = new SiteSettings(fn, showid);
        if (dsOrgType.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsOrgType.Tables[0].Rows.Count; i++)
            {
                ddlOrganization.Items.Add(dsOrgType.Tables[0].Rows[i]["organisation"].ToString());
                ddlOrganization.Items[i + 1].Value = dsOrgType.Tables[0].Rows[i]["ID"].ToString();
            }
        }

        if (ddlOrganization.Items.Count > 0)//*
        {
            ddlOrganization_SelectedIndexChanged(this, null);
        }
        DataSet dsAffiliation = new DataSet();

        dsAffiliation = cmdObj.getAffiliation(showid);

        if (dsAffiliation.Tables[0].Rows.Count != 0)
        {
            for (int y = 0; y < dsAffiliation.Tables[0].Rows.Count; y++)
            {
                ddlAffiliation.Items.Add(dsAffiliation.Tables[0].Rows[y]["aff_name"].ToString());
                ddlAffiliation.Items[y + 1].Value = dsAffiliation.Tables[0].Rows[y]["affid"].ToString();
            }
        }
    }
    #endregion

    #region btnSave_Click (create one record (just RegGroupID and recycle=0) into tb_RegGroup table if current groupid not exist in tb_RegGroup & save/update data into tb_RegDelegate table & get next route and redirect to next page according to the site flow settings (tb_site_flow table))
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);
            if (!string.IsNullOrEmpty(showid))
            {
                CommonFuns cFun = new CommonFuns();
                string groupid = cFun.DecryptValue(urlQuery.GoupRegID);
                string regno = cFun.DecryptValue(urlQuery.DelegateID);
                string checkingCitizenshipErrMsg = "";
                if (!validateNRIC(ddlAffiliation.SelectedItem.Text.Trim(), txtPassNo.Text.Trim(), ref checkingCitizenshipErrMsg))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + checkingCitizenshipErrMsg + "');", true);
                    return;
                }
                else
                {
                    string sql = "Select * From tb_RegDelegate Where ShowID='YAB380' And Regno!=" + regno + " And reg_PassNo='" + txtPassNo.Text.Trim() + "'";
                    DataTable dtUpdateExist = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                    if (dtUpdateExist.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('This NRIC number has already been used.');", true);
                        return;
                    }
                    else
                    {
                        updateData(regno, groupid, showid);
                        string url = "https://www.event-reg.biz/registration/onsite/OnsiteSearch.aspx?Evt=" + cFun.EncryptValue(showid);
                        Response.Redirect(url);
                    }
                }
            }
            else
            {
                string url = "~/Onsite/OnsiteSearch.aspx?Evt=" + cFun.EncryptValue(showid);
                Response.Redirect(url);
            }
        }
    }
    #endregion

    #region updateData
    private void updateData(string regno, string reggroupid, string showid)
    {
        if (Page.IsValid)
        {
            try
            {
                #region
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                string oname = txtOName.Text.Trim();
                string email = cFun.solveSQL(txtEmail.Text.Trim());
                string passno = cFun.solveSQL(txtPassNo.Text.Trim());

                string otherorg = "";
                string organization = cFun.solveSQL(ddlOrganization.SelectedItem.Value.ToString());
                if ((lstOthersValue.Contains(ddlOrganization.SelectedItem.Text)) && txtOrgOther.Text != "" && txtOrgOther.Text != string.Empty)
                {
                    otherorg = cFun.solveSQL(txtOrgOther.Text.Trim());
                }

                string otherinstitution = "";
                string institution = cFun.solveSQL(ddlInstitution.SelectedItem.Value.ToString());
                if ((lstOthersValue.Contains(ddlInstitution.SelectedItem.Text)) && txtInstiOther.Text != "" && txtInstiOther.Text != string.Empty)
                {
                    otherinstitution = cFun.solveSQL(txtInstiOther.Text.Trim());
                }
                string affiliation = cFun.solveSQL(ddlAffiliation.SelectedItem.Value.ToString());

                DataTable dt = new DataTable();
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);
                if (!string.IsNullOrEmpty(regno))
                {
                    RegDelegateObj rgd = new RegDelegateObj(fn);
                    dt = rgd.getDataByGroupIDRegno(reggroupid, regno, showid);
                    if (dt.Rows.Count > 0)
                    {
                        string sqlUpdatePostConf = "Update tb_RegDelegate Set reg_OName=@reg_OName ,reg_Organization=@reg_Organization ,reg_otherOrganization=@reg_otherOrganization "
                            + " ,reg_Institution=@reg_Institution ,reg_otherInstitution=@reg_otherInstitution, reg_Email=@reg_Email , reg_PassNo=@reg_PassNo, reg_dateupdated=getdate()"
                                    + ",reg_Affiliation=@reg_Affiliation Where ShowID=@ShowID And Regno=@Regno And RegGroupID=@RegGroupID";
                        using (SqlConnection con = new SqlConnection(fn.ConnString))
                        {
                            using (SqlCommand command = new SqlCommand(sqlUpdatePostConf))
                            {
                                command.Connection = con;
                                command.Parameters.AddWithValue("@ShowID", showid);
                                command.Parameters.AddWithValue("@Regno", regno);
                                command.Parameters.AddWithValue("@RegGroupID", reggroupid);
                                command.Parameters.AddWithValue("@reg_OName", oname);
                                command.Parameters.AddWithValue("@reg_Organization", organization);
                                command.Parameters.AddWithValue("@reg_otherOrganization", otherorg);
                                command.Parameters.AddWithValue("@reg_Institution", institution);
                                command.Parameters.AddWithValue("@reg_otherInstitution", otherinstitution);
                                command.Parameters.AddWithValue("@reg_Email", email);
                                command.Parameters.AddWithValue("@reg_PassNo", passno);
                                command.Parameters.AddWithValue("@reg_Affiliation", affiliation);
                                con.Open();
                                int isSuccess = command.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                    }
                    else
                    {
                        string url = "~/Onsite/OnsiteSearch.aspx?Evt=" + cFun.EncryptValue(showid);
                        Response.Redirect(url);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            { }
        }
    }
    private void bindData(string regno, string reggroupid, string showid)
    {
        try
        {
            DataTable dt = new DataTable();
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            if (!string.IsNullOrEmpty(regno))
            {
                RegDelegateObj rgd = new RegDelegateObj(fn);
                dt = rgd.getDataByGroupIDRegno(reggroupid, regno, showid);
                if (dt.Rows.Count > 0)
                {
                    string affiliationID = dt.Rows[0]["reg_Affiliation"].ToString();
                    //lblAffiliation.Text = affiliationID;
                    try
                    {
                        if (!String.IsNullOrEmpty(affiliationID))
                        {
                            ListItem listItem = ddlAffiliation.Items.FindByValue(affiliationID);
                            if (listItem != null)
                            {
                                ddlAffiliation.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    string reg_fname = dt.Rows[0]["reg_OName"].ToString();
                    txtOName.Text = reg_fname;
                    string reg_email = dt.Rows[0]["reg_Email"].ToString();
                    txtEmail.Text = reg_email;
                    string passno = dt.Rows[0]["reg_PassNo"].ToString();
                    txtPassNo.Text = passno;
                    string reg_organization = dt.Rows[0]["reg_Organization"].ToString();
                    string reg_otherorganization = dt.Rows[0]["reg_otherOrganization"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(reg_organization))
                        {
                            ListItem listItem = ddlOrganization.Items.FindByValue(reg_organization);
                            if (listItem != null)
                            {
                                ddlOrganization.ClearSelection();
                                listItem.Selected = true;
                            }
                            ddlOrganization_SelectedIndexChanged(this, null);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    txtOrgOther.Text = reg_otherorganization;

                    string reg_institution = dt.Rows[0]["reg_Institution"].ToString();
                    string reg_otherinstitution = dt.Rows[0]["reg_otherInstitution"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(reg_institution))
                        {
                            ListItem listItem = ddlInstitution.Items.FindByValue(reg_institution);
                            if (listItem != null)
                            {
                                ddlInstitution.ClearSelection();
                                listItem.Selected = true;
                            }
                            ddlInstitution_SelectedIndexChanged(this, null);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    txtInstiOther.Text = reg_otherinstitution;

                    

                }
            }
        }
        catch (Exception ex)
        { }
    }
    #region validateNRIC
    private bool validateNRIC(string nationality, string nricNo, ref string msg)
    {
        bool isValid = false;
        try
        {
            isValid = isValidNRIC(nationality, nricNo);
            if (!isValid)
            {
                msg = "*Invalid NRIC Number";
            }
            else if (isValid && (nationality != _strSingaporean && nationality != _strSingaporePR))
            {
                bool validNRIC = isNRICValid(nricNo);
                //bool validFIN = IsFINValid(nricNo);/*Comment on 6-8-2019 according to client say(for Singaporean & SingaporePR, check NRIC Number only)*/
                if (validNRIC)
                {
                    isValid = false;
                    msg = "*Please select Singaporean Citizenship/Singapore PR Citizenship to enjoy special rates!";
                }
                //else if (validFIN)/*Comment on 6-8-2019 according to client say(for Singaporean & SingaporePR, check NRIC Number only)*/
                //{
                //    isValid = false;
                //    msg = "*Please select Singapore PR Citizenship to enjoy special rates!";
                //}
            }
        }
        catch (Exception ex)
        { }

        return isValid;
    }
    private bool isValidNRIC(string nationality, string nricNo)
    {
        bool validNRIC = false;
        try
        {
            if (nationality == _strSingaporean || nationality == _strSingaporePR)
            {
                validNRIC = isNRICValid(nricNo);
            }
            //else if (nationality == _strSingaporePR)/*Comment on 6-8-2019 according to client say(for Singaporean & SingaporePR, check NRIC Number only)*/
            //{
            //    validNRIC = IsFINValid(nricNo);
            //}
            else
            {
                validNRIC = true;
            }
        }
        catch { }
        return validNRIC;
    }
    public static bool isNRICValid(string strValueToCheck)
    {
        strValueToCheck = strValueToCheck.Trim();

        Regex objRegex = new Regex("^(s|t)[0-9]{7}[a-jz]{1}$", RegexOptions.IgnoreCase);

        if (!objRegex.IsMatch(strValueToCheck))
        {
            return false;
        }

        string strNums = strValueToCheck.Substring(1, 7);

        int intSum = 0;
        int checkDigit = 0;
        string checkChar = "";
        intSum = Convert.ToUInt16(strNums.Substring(0, 1)) * 2;
        intSum = intSum + (Convert.ToUInt16(strNums.Substring(1, 1)) * 7);
        intSum = intSum + (Convert.ToUInt16(strNums.Substring(2, 1)) * 6);
        intSum = intSum + (Convert.ToUInt16(strNums.Substring(3, 1)) * 5);
        intSum = intSum + (Convert.ToUInt16(strNums.Substring(4, 1)) * 4);
        intSum = intSum + (Convert.ToUInt16(strNums.Substring(5, 1)) * 3);
        intSum = intSum + (Convert.ToUInt16(strNums.Substring(6, 1)) * 2);

        if (strValueToCheck.Substring(0, 1).ToLower() == "t")
        {
            //prefix T
            intSum = intSum + 4;
        }

        checkDigit = 11 - (intSum % 11);

        checkChar = strValueToCheck.Substring(8, 1).ToLower();

        if (checkDigit == 1 && checkChar == "a")
        {
            return true;
        }
        else if (checkDigit == 2 && checkChar == "b")
        {
            return true;
        }
        else if (checkDigit == 3 && checkChar == "c")
        {
            return true;
        }
        else if (checkDigit == 4 && checkChar == "d")
        {
            return true;
        }
        else if (checkDigit == 5 && checkChar == "e")
        {
            return true;
        }
        else if (checkDigit == 6 && checkChar == "f")
        {
            return true;
        }
        else if (checkDigit == 7 && checkChar == "g")
        {
            return true;
        }
        else if (checkDigit == 8 && checkChar == "h")
        {
            return true;
        }
        else if (checkDigit == 9 && checkChar == "i")
        {
            return true;
        }
        else if (checkDigit == 10 && checkChar == "z")
        {
            return true;
        }
        else if (checkDigit == 11 && checkChar == "j")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool IsFINValid(string fin)
    {
        int[] Multiples = { 2, 7, 6, 5, 4, 3, 2 };
        if (string.IsNullOrEmpty(fin))
        {
            return false;
        }

        //    check length
        if (fin.Length != 9)
        {
            return false;
        }

        int total = 0
            , count = 0
            , numericNric;
        char first = fin[0]
            , last = fin[fin.Length - 1];

        if (first != 'F' && first != 'G')
        {
            return false;
        }

        if (!int.TryParse(fin.Substring(1, fin.Length - 2), out numericNric))
        {
            return false;
        }

        while (numericNric != 0)
        {
            total += numericNric % 10 * Multiples[Multiples.Length - (1 + count++)];

            numericNric /= 10;
        }

        char[] outputs;
        if (first == 'F')
        {
            outputs = new char[] { 'X', 'W', 'U', 'T', 'R', 'Q', 'P', 'N', 'M', 'L', 'K' };
        }
        else
        {
            outputs = new char[] { 'R', 'Q', 'P', 'N', 'M', 'L', 'K', 'X', 'W', 'U', 'T' };
        }

        return last == outputs[total % 11];
    }
    #endregion
    #endregion

    #region ddlOrganization_SelectedIndexChanged (set 'other organization div' visibility if the selection of ddlOrganization dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherOrg and form_type='D')
    protected void ddlOrganization_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlOrganization.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                string flowid = cFun.DecryptValue(urlQuery.FlowID);

                if (lstOthersValue.Contains(ddlOrganization.SelectedItem.Text))
                {
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherOrg);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);

                        if (isshow == 1)
                        {
                            divOrgOther.Visible = true;
                        }
                        else
                        {
                            divOrgOther.Visible = false;
                        }

                        if (isrequired == 1)
                        {
                            //txtOrgOther.Attributes.Add("required", "");
                            vcOrgOther.Enabled = true;
                        }
                        else
                        {
                            //txtOrgOther.Attributes.Remove("required");
                            vcOrgOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divOrgOther.Visible = false;
                    //txtOrgOther.Attributes.Remove("required");
                    vcOrgOther.Enabled = false;
                }

                if (ddlOrganization.SelectedItem.Text.Trim() == _strCompanyOrgType || ddlOrganization.SelectedItem.Text.Trim() == _strGovernmentOrgType)
                {
                    divInstitution.Visible = false;
                    vcInsti.Enabled = false;
                    ddlInstitution.Items.Clear();
                    ddlInstitution.ClearSelection();
                    ddlInstitution.Items.Add(new ListItem("Other", "Other"));
                    ddlInstitution_SelectedIndexChanged(this, null);
                }
                else
                {
                    divInstitution.Visible = true;
                    vcInsti.Enabled = true;
                    GetInstitution();
                    ddlInstitution_SelectedIndexChanged(this, null);
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region ddlInstitution_SelectedIndexChanged (set 'other institution div' visibility if the selection of ddlInstitution dropdownlist is 'Other' or 'Others' & 'form_input_isshow' is '1' from tb_Form where form_input_name=_OtherInstitution and form_type='D')
    protected void ddlInstitution_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlInstitution.Items.Count > 0)
            {
                OthersSettings othersetting = new OthersSettings(fn);
                List<string> lstOthersValue = othersetting.lstOthersValue;

                if (lstOthersValue.Contains(ddlInstitution.SelectedItem.Text))
                {
                    FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                    string showid = cFun.DecryptValue(urlQuery.CurrShowID);
                    string flowid = cFun.DecryptValue(urlQuery.FlowID);
                    FormManageObj frmObj = new FormManageObj(fn);
                    frmObj.showID = showid;
                    frmObj.flowID = flowid;
                    DataTable dt = frmObj.getDataByTypeInputName(FormType.TypeDelegate, _OtherInstitution);
                    if (dt.Rows.Count > 0)
                    {
                        int isshow = Convert.ToInt16(dt.Rows[0]["form_input_isshow"]);
                        int isrequired = Convert.ToInt16(dt.Rows[0]["form_input_isrequired"]);
                        string formtype = FormType.TypeDelegate;

                        if (isshow == 1)
                        {
                            divInstiOther.Visible = true;
                        }
                        else
                        {
                            divInstiOther.Visible = false;
                        }

                        string labelname = !string.IsNullOrEmpty(dt.Rows[0]["form_input_text"].ToString()) ? dt.Rows[0]["form_input_text"].ToString() : frmObj.getDefaultLableNameByInputNameType(_IDNo, formtype);
                        if (isrequired == 1)
                        {
                            lblInstiOther.Text = labelname + "<span class=\"red\">*</span>&nbsp;&nbsp; "; ;
                            //txtInstiOther.Attributes.Add("required", "");
                            vcInstiOther.Enabled = true;
                        }
                        else
                        {
                            lblInstiOther.Text = labelname;
                            //txtInstiOther.Attributes.Remove("required");
                            vcInstiOther.Enabled = false;
                        }
                    }
                }
                else
                {
                    divInstiOther.Visible = false;
                    //txtInstiOther.Attributes.Remove("required");
                    vcInstiOther.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    #region GetInstitution (get institution according to the selected organization id(ddlOrganization.SelectedItem.Value) from ref_Institution table)
    protected void GetInstitution()
    {
        ddlInstitution.Items.Clear();
        ddlInstitution.ClearSelection();

        DataSet dsInstitution = new DataSet();
        try
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);

            CommonDataObj cmdObj = new CommonDataObj(fn);
            dsInstitution = cmdObj.getInstitutionByOrganizationID(ddlOrganization.SelectedItem.Value, showid);

            if (dsInstitution.Tables[0].Rows.Count > 0)
            {
                for (int y = 0; y < dsInstitution.Tables[0].Rows.Count; y++)
                {
                    ddlInstitution.Items.Add(dsInstitution.Tables[0].Rows[y]["institution"].ToString());
                    ddlInstitution.Items[y].Value = dsInstitution.Tables[0].Rows[y]["ID"].ToString();
                }
            }
            else
            {
                ddlInstitution.Items.Add("Please Select");
                ddlInstitution.Items[0].Value = "0";
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
}