﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Corpit.Registration;
public partial class OnsiteIndex : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    public class tmpDataList
    {
        public string showID { get; set; }
        public string FlowID { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["Event"] != null)
        {
            tmpDataList tmpList = GetShowIDByEventName(Request.QueryString["Event"].ToString());
            if (string.IsNullOrEmpty(tmpList.showID))
                Response.Redirect("404.aspx");
            else
            {
                string showID = tmpList.showID;
                string FlowID = tmpList.FlowID;
                string grpNum = string.Empty;
                string page = "";
                string step = "";
                FlowControler Flw = new FlowControler(fn);
                if (Request.QueryString["GRP"] == null)
                {
                    RegGroupObj rgp = new RegGroupObj(fn);
                    grpNum = rgp.GenGroupNumber(showID);// GetRunNUmber("GRP_KEY");
                }
                else
                {
                    grpNum = cFun.DecryptValue(Request.QueryString["GRP"].ToString());
                }

                Dictionary<string, string> nValues = Flw.GetNextRoute(FlowID);
                if (nValues.Count > 0)
                {
                    page = nValues["nURL"].ToString();
                    step = nValues["nStep"].ToString();
                    FlowID = nValues["FlowID"].ToString();
                }
                page = "RegDelegateOnsite.aspx";
                grpNum = cFun.EncryptValue(grpNum);
                if (Request.QueryString["INV"] != null)
                {
                    string invID = cFun.EncryptValue(cFun.DecryptValue(Request.QueryString["INV"].ToString()));
                    string route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step, invID);
                    Response.Redirect(route);
                }
                else
                {
                    string route = Flw.MakeFullURL(page, FlowID, showID, grpNum, step);
                    Response.Redirect(route);
                }
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }

    private tmpDataList GetShowIDByEventName(string eventName)
    {
        tmpDataList cList = new tmpDataList();
        try
        {
            cList.showID = "";
            cList.FlowID = "";
            string sql = "select FLW_ID,showID from tb_site_flow_master  where FLW_Name=@FName and Status=@Status";

            SqlParameter spar1 = new SqlParameter("FName", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = eventName;
            spar2.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                cList.showID = dList.Rows[0]["showID"].ToString();
                cList.FlowID = dList.Rows[0]["FLW_ID"].ToString();
            }

        }
        catch { }
        return cList;
    }
}