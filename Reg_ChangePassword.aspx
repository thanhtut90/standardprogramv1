﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReLoginMaster.master" AutoEventWireup="true" CodeFile="Reg_ChangePassword.aspx.cs" Inherits="Reg_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <asp:TextBox ID="txtFlowName" runat="server" Visible="false"></asp:TextBox>
            <div id="rcontent" runat="server">
                <h3 class="ConfrmHeader">Change Password</h3>
                <div class="form-group row">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblOldPassword" runat="server" CssClass="form-control-label" Text="Old Password"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtOldPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcOldPassword" runat="server"
                        ControlToValidate="txtOldPassword" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revOldPassword" runat="server" ControlToValidate="txtOldPassword" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblNewPassword" runat="server" CssClass="form-control-label" Text="New Password"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtNewPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcNewPassword" runat="server"
                        ControlToValidate="txtNewPassword" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revNewPassword" runat="server" ControlToValidate="txtNewPassword" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblConfirmPassword" runat="server" CssClass="form-control-label" Text="New Password"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcConfirmPassword" runat="server"
                        ControlToValidate="txtConfirmPassword" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cmpConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                        ControlToCompare="txtNewPassword" ErrorMessage="Not match." ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group container" style="padding-top:20px;">
                    <asp:Panel runat="server" ID="PanelWithoutBack" Visible="true" >
                        <div class="col-lg-offset-4 col-lg-3 col-sm-offset-4 col-sm-3 center-block" >
                            <asp:Button runat="server" ID="btnSave" CssClass="btn MainButton btn-block" Text="Change Password" OnClick="btnSave_Click" />
                        </div>
                   </asp:Panel>
                </div>
            </div>
            <asp:TextBox ID="hfOldPassword" runat="server" Text="" Visible="false"></asp:TextBox>
            <br /><br /><br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

