﻿using Corpit.Abstract;
using Corpit.Registration;
using Corpit.Site.Utilities;
using Corpit.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AbstractIndex : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    static string ASCAShowName = "ASCA 2019 Online Conference Registration";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string flwID = cFun.DecryptValue(urlQuery.FlowID);
            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flwID);
            if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["regstage"] != null)
            {
                string groupid = Session["Groupid"].ToString();
                string flowid = Session["Flowid"].ToString();
                string showid = Session["Showid"].ToString();
                string currentstage = Session["regstage"].ToString();
                DataTable dt = new DataTable();
                ShowControler shwCtr = new ShowControler(fn);
                Show shw = shwCtr.GetShow(showid);
                if (Session["Regno"] != null)
                {
                    string regno = Session["Regno"].ToString();

                    DataTable dtAbsSetting = getAbstractSetting(flowid, showid);
                    if (dtAbsSetting.Rows.Count > 0)
                    {
                        string absUrl = dtAbsSetting.Rows[0]["AbsRegLink"].ToString();
                        string absShowID = dtAbsSetting.Rows[0]["AbsShowID"].ToString();
                        string absSaveDataApiLink = dtAbsSetting.Rows[0]["AbsSaveDataApiLink"].ToString();
                        if (!string.IsNullOrEmpty(regno))
                        {
                            dt = getRegData(groupid, regno, showid);
                            if (dt.Rows.Count > 0)
                            {
                                string abstractRegno = "";
                                AbstractInfoObj absObj = new AbstractInfoObj(fn);
                                absObj.shwID = absShowID;
                                absObj.RegPortalRegNo = regno;
                                absObj.Regno = abstractRegno;
                                absObj.Salutation = dt.Rows[0]["Salutation"] != DBNull.Value ? dt.Rows[0]["Salutation"].ToString() : "";
                                string fname = dt.Rows[0]["reg_FName"] != DBNull.Value ? dt.Rows[0]["reg_FName"].ToString() : "";
                                string lname = dt.Rows[0]["reg_LName"] != DBNull.Value ? dt.Rows[0]["reg_LName"].ToString() : "";
                                string oname = dt.Rows[0]["reg_OName"] != DBNull.Value ? dt.Rows[0]["reg_OName"].ToString() : "";
                                absObj.FName = !string.IsNullOrEmpty(fname) ? fname : !string.IsNullOrEmpty(lname) ? lname : oname;
                                absObj.LName = !string.IsNullOrEmpty(lname) ? lname : "";
                                absObj.DesignationProf = dt.Rows[0]["ProfessionName"] != DBNull.Value ? dt.Rows[0]["ProfessionName"].ToString() : "";
                                absObj.OtherDesignation = dt.Rows[0]["reg_otherProfession"] != DBNull.Value ? dt.Rows[0]["reg_otherProfession"].ToString() : "";
                                absObj.Department = dt.Rows[0]["reg_Department"] != DBNull.Value ? dt.Rows[0]["reg_Department"].ToString() : "";
                                absObj.Institution = (dt.Rows[0]["InstitutionName"] != DBNull.Value ? dt.Rows[0]["InstitutionName"].ToString() : "");
                                absObj.OtherInstitution = shw.SHW_Name == ASCAShowName ? (dt.Rows[0]["reg_PassNo"] != DBNull.Value ? dt.Rows[0]["reg_PassNo"].ToString() : "")
                                                    : (dt.Rows[0]["reg_otherInstitution"] != DBNull.Value ? dt.Rows[0]["reg_otherInstitution"].ToString() : "");
                                absObj.Address = dt.Rows[0]["reg_Address1"] != DBNull.Value ? dt.Rows[0]["reg_Address1"].ToString() : "";
                                absObj.PostalCode = dt.Rows[0]["reg_PostalCode"] != DBNull.Value ? dt.Rows[0]["reg_PostalCode"].ToString() : "";
                                absObj.Country = dt.Rows[0]["RegCountry"] != DBNull.Value ? dt.Rows[0]["RegCountry"].ToString() : "";
                                absObj.MCRNo = dt.Rows[0]["reg_IDno"] != DBNull.Value ? dt.Rows[0]["reg_IDno"].ToString() : "";
                                absObj.Mobilecc = dt.Rows[0]["reg_Mobcc"] != DBNull.Value ? dt.Rows[0]["reg_Mobcc"].ToString() : "";
                                absObj.Mobileac = dt.Rows[0]["reg_Mobac"] != DBNull.Value ? dt.Rows[0]["reg_Mobac"].ToString() : "";
                                absObj.Mobile = dt.Rows[0]["reg_Mobile"] != DBNull.Value ? dt.Rows[0]["reg_Mobile"].ToString() : "";
                                absObj.Email = dt.Rows[0]["reg_Email"] != DBNull.Value ? dt.Rows[0]["reg_Email"].ToString() : "";
                                absObj.RegDate = dt.Rows[0]["reg_datecreated"] != DBNull.Value ? dt.Rows[0]["reg_datecreated"].ToString() : "";
                                absObj.YearOfQuali = dt.Rows[0]["reg_Membershipno"] != DBNull.Value ? dt.Rows[0]["reg_Membershipno"].ToString() : "";
                                absObj.IsVisa = dt.Rows[0]["reg_approveStatus"] != DBNull.Value ? dt.Rows[0]["reg_approveStatus"].ToString() : "";
                                absObj.IsVege = dt.Rows[0]["reg_Additional5"] != DBNull.Value ? dt.Rows[0]["reg_Additional5"].ToString() : "";
                                absObj.FileUpload = "";
                                absObj.Telcc = dt.Rows[0]["reg_Telcc"] != DBNull.Value ? dt.Rows[0]["reg_Telcc"].ToString() : "";
                                absObj.Telac = dt.Rows[0]["reg_Telac"] != DBNull.Value ? dt.Rows[0]["reg_Telac"].ToString() : "";
                                absObj.Qualification1 = "";
                                absObj.Qualification1Name = "";
                                absObj.Organization = dt.Rows[0]["OrganizationName"] != DBNull.Value ? dt.Rows[0]["OrganizationName"].ToString() : "";
                                absObj.City = dt.Rows[0]["reg_City"] != DBNull.Value ? dt.Rows[0]["reg_City"].ToString() : "";
                                absObj.State = dt.Rows[0]["reg_State"] != DBNull.Value ? dt.Rows[0]["reg_State"].ToString() : "";
                                absObj.Twitter = "";
                                absObj.Gender = dt.Rows[0]["reg_Gender"] != DBNull.Value ? dt.Rows[0]["reg_Gender"].ToString() : "";

                                string absRegno = "";//***
                                bool isOk = saveAbstractDataAPI(absObj, absSaveDataApiLink, ref absRegno);
                                if (isOk)
                                {
                                    string url = absUrl + cFun.EncryptValue(absShowID) + "&INV=" + cFun.EncryptValue(absRegno);
                                    //string jVal = string.Format("window.open('" + url + "','_blank');", url);
                                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", jVal, true);

                                    Response.Write("<script>");
                                    Response.Write("window.open('" + url + "','_blank');window.location.href = '" + Request.UrlReferrer.AbsoluteUri + "';");
                                    Response.Write("</script>");
                                }
                                else
                                {
                                    //Response.Write(isOk);
                                    //Response.End();
                                    Response.Redirect("404.aspx");
                                }
                            }
                            else
                            {
                                //Response.Write("no reg data");
                                //Response.End();
                                Response.Redirect("404.aspx");
                            }
                        }
                        else
                        {
                            //Response.Write("no regno");
                            //Response.End();
                            Response.Redirect("404.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("ReLogin.aspx?Event=" + fmaster.FlowName.Trim());
                    }
                }
                else
                {
                    Response.Redirect("ReLogin.aspx?Event=" + fmaster.FlowName.Trim());
                }
            }
            else
            {
                Response.Redirect("ReLogin.aspx?Event=" + fmaster.FlowName.Trim());
            }
        }
    }

    private DataTable getRegData(string groupid, string regno,string showid)
    {
        DataTable dtReg = new DataTable();
        try
        {
            string sql = "Select * From DelegateWithRefSeperateOtherValue Where ShowID=@ShowID And RegGroupID=@RegGroupID And Regno=@Regno";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("RegGroupID", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("Regno", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = groupid;
            spar3.Value = regno;
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);
            dtReg = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
        }
        catch
        { }

        return dtReg;
    }
    private DataTable getAbstractSetting(string flowid, string showid)
    {
        DataTable dtResult = new DataTable();
        try
        {
            string sql = "Select * From tb_AbstractSettings Where ShowID=@ShowID And FlowID=@FlowID";
            List<SqlParameter> pList = new List<SqlParameter>();
            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FlowID", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            pList.Add(spar1);
            pList.Add(spar2);
            dtResult = fn.GetDatasetByCommand(sql, "ds", pList).Tables[0];
        }
        catch
        { }

        return dtResult;
    }
    public bool saveAbstractDataAPI(AbstractInfoObj absObj, string absSaveDataApiLink, ref string absRegno)
    {
        bool isOk = false;
        try
        {
            string jsonStr = JsonConvert.SerializeObject(absObj);
            string rtnValue = PostAbsApi(jsonStr, absSaveDataApiLink);
            if (!string.IsNullOrEmpty(rtnValue) && rtnValue != "ERROR")
            {
                AbstractSaveStatusJson rtnStatus = JsonConvert.DeserializeObject<AbstractSaveStatusJson>((string)rtnValue);
                if (!string.IsNullOrEmpty(rtnStatus.Status) && rtnStatus.Status.ToUpper() == "OK")
                {
                    isOk = true;
                    absRegno = rtnStatus.AbsRegno;
                }
            }
        }
        catch (Exception ex)
        {
            isOk = false;
        }
        return isOk;
    }
    public string PostAbsApi(string jData, string APILink)
    {
        string rtnData = "";
        try
        {
            string jsonStr = jData;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(APILink);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            //httpWebRequest.UseDefaultCredentials = true;
            //httpWebRequest.Headers.Add("Access-Control-Allow-Origin", "*");
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonStr);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                rtnData = streamReader.ReadToEnd();
            }
        }
        catch (Exception ex) { rtnData = "ERROR"; }
        return rtnData;
    }
}