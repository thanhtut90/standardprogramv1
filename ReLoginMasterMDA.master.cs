﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using Corpit.Registration;
using Corpit.Utilities;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

public partial class ReLoginMasterMDA : System.Web.UI.MasterPage
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    private static string exhibitorMDALoginUrl = "https://www.event-reg.biz/MDAOEM2020/login.aspx";

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            InitSiteSettings(showid);
            bindMenuLink();
            string regno = Session["Groupid"].ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string flowid = cFun.DecryptValue(urlQuery.FlowID);

            FlowControler fCtrl = new FlowControler(fn);
            FlowMaster fmaster = fCtrl.GetFlowMasterConfig(flowid);
            if (fmaster != null)
            {
                txtFlowName.Text = fmaster.FlowName;
                if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null)
                {
                    LoadPageSetting();
                    SiteSettings sSetting = new SiteSettings(fn, showid);
                    sSetting.LoadBaseSiteProperties(showid);
                    if (sSetting.isRegisterClosed())
                    {
                        if (fmaster.isOnsite != 1)
                            Response.Redirect("RegClosed.aspx?SHW=" + cFun.EncryptValue(showid));
                    }
                }
                else
                {
                    Response.Redirect(exhibitorMDALoginUrl);
                }
            }
            else
            {
                Response.Redirect("~/404.aspx");
            }
        }
    }

    #region PageSetup
    private void InitSiteSettings(string showid)
    {
        Functionality fn = new Functionality();
        SiteSettings sSetting = new SiteSettings(fn, showid);
        sSetting.LoadBaseSiteProperties(showid);
        SiteBanner.ImageUrl = sSetting.SiteBanner;
        Global.PageTitle = sSetting.SiteTitle;
        Global.Copyright = sSetting.SiteFooter;
        Global.PageBanner = sSetting.SiteBanner;

        if (!string.IsNullOrEmpty(sSetting.SiteCssBandlePath))
            BundleRefence.Path = "~/Content/" + sSetting.SiteCssBandlePath + "/";
    }
    private void LoadPageSetting()
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string currentPageStep = cFun.DecryptValue(urlQuery.CurrIndex);
        FlowControler flwObj = new FlowControler(fn, urlQuery);
        SetPageSettting(flwObj.CurrIndexTiTle, currentPageStep);
    }

    public void SetPageSettting(string title, string currentPageStep)
    {
        if (currentPageStep == "2")
        {
            title = "List of VIP / Special Guest Nominations";
        }
        SiteHeader.Text = title;
    }
    #endregion

    #region bindMenuLink (bind ul, li in div(divMenu) dynamically according to flow)
    private void bindMenuLink()
    {
        if (Session["Groupid"] != null && Session["Flowid"] != null && Session["Showid"] != null && Session["RegStatus"] != null)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            string groupid = Session["Groupid"].ToString();
            string flowid = Session["Flowid"].ToString();

            FlowControler fControl = new FlowControler(fn);

            //StatusSettings statusSet = new StatusSettings(fn);
            //if (Session["RegStatus"].ToString() == statusSet.Success.ToString())
            {
                DataTable dtMenu = new DataTable();
                dtMenu = fControl.GetReloginMenu(flowid, "0");
                if (dtMenu.Rows.Count > 0)
                {
                    HtmlGenericControl ul = new HtmlGenericControl("ul");
                    ul.Attributes.Add("class", "nav navbar-nav");
                    ul.ID = "ulMenu";

                    foreach (DataRow dr in dtMenu.Rows)
                    {
                        string scriptid = dr["reloginref_scriptID"].ToString().Trim();
                        string menuname = dr["reloginmenu_name"].ToString().Trim();
                        string stepseq = dr["step_seq"].ToString().Trim();
                        string menupageHeader = dr["step_label"].ToString().Trim();

                        FlowMaster flwMasterConfig = fControl.GetFlowMasterConfig(flowid);

                        if (menuname == SiteDefaultValue.constantDelegateName)
                        {
                            if (flwMasterConfig.FlowType != SiteFlowType.FLOW_GROUP)
                            {
                                HtmlGenericControl li;
                                li = new HtmlGenericControl("li");
                                li.Attributes.Add("runat", "server");

                                string path = Request.Url.AbsolutePath;
                                string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                string[] strpath = path.Split('/');
                                if (strpath.Length > 1)
                                {
                                    path = strpath[1] + ".aspx";
                                }
                                else
                                {
                                    path = ".aspx";
                                }
                                if (scriptid == path)
                                {
                                    li.Attributes.Add("class", "active");
                                }
                                else
                                {
                                    li.Attributes.Remove("class");
                                }

                                li.Attributes.Add("id", "a" + stepseq);
                                HyperLink hp = new HyperLink();
                                hp.ID = "hp" + stepseq;
                                hp.NavigateUrl = getRoute(stepseq, scriptid);
                                hp.Text = menupageHeader;//menuname;
                                li.Controls.Add(hp);
                                ul.Controls.Add(li);
                            }
                        }
                        else
                        {
                            if (menuname == SiteDefaultValue.constantConferenceName && flwMasterConfig.FlowType == SiteFlowType.FLOW_GROUP)
                            {
                                /*not showing Reg_Conference.aspx page*/
                            }
                            else
                            {
                                HtmlGenericControl li;
                                li = new HtmlGenericControl("li");
                                li.Attributes.Add("runat", "server");

                                string path = Request.Url.AbsolutePath;
                                string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                string[] strpath = path.Split('/');
                                if (strpath.Length > 1)
                                {
                                    path = strpath[1] + ".aspx";
                                }
                                else
                                {
                                    path = ".aspx";
                                }
                                if (scriptid == path)
                                {
                                    li.Attributes.Add("class", "active");
                                }
                                else
                                {
                                    li.Attributes.Remove("class");
                                }

                                li.Attributes.Add("id", "a" + stepseq);
                                HyperLink hp = new HyperLink();
                                hp.ID = "hp" + stepseq;
                                hp.NavigateUrl = getRoute(stepseq, scriptid);
                                hp.Text = menupageHeader;//menuname;
                                li.Controls.Add(hp);
                                ul.Controls.Add(li);
                            }
                        }
                    }

                    bool isCrossSellingVisible = false;
                    string crossSelling = setRegisterAnotherShowButton(groupid, ref isCrossSellingVisible);

                    if(!isCrossSellingVisible)
                    {
                        #region Normal flows
                        for (int i = 0; i <= 2; i++)
                        {
                            HtmlGenericControl li;
                            li = new HtmlGenericControl("li");
                            li.Attributes.Add("runat", "server");

                            bool confExist = fControl.checkPageExist(flowid, SiteDefaultValue.constantConfName);
                            if (confExist)
                            {
                                if (i == 1)
                                {
                                    li.Attributes.Add("id", "a" + i);
                                    HyperLink hp = new HyperLink();
                                    hp.ID = "hp" + i;
                                    string scriptid = ReLoginStaticValueClass.regReceiptPage;

                                    string path = Request.Url.AbsolutePath;
                                    string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                    string[] strpath = path.Split('/');
                                    if (strpath.Length > 1)
                                    {
                                        path = strpath[1] + ".aspx";
                                    }
                                    else
                                    {
                                        path = ".aspx";
                                    }
                                    if (scriptid == path)
                                    {
                                        li.Attributes.Add("class", "active");
                                    }
                                    else
                                    {
                                        li.Attributes.Remove("class");
                                    }

                                    hp.NavigateUrl = getRoute(i.ToString(), scriptid);
                                    hp.Text = ReLoginStaticValueClass.regReceiptName;
                                    //hp.Attributes.Add("target", "_blank");
                                    li.Controls.Add(hp);
                                    ul.Controls.Add(li);
                                }
                            }

                            if (i == 2)
                            {
                                li.Attributes.Add("id", "a" + i);
                                HyperLink hp = new HyperLink();
                                hp.ID = "hp" + i;
                                //LinkButton lnkLogout = new LinkButton();
                                //lnkLogout.ID = "lnkLogout";
                                //lnkLogout.Attributes.Add("runat", "server");
                                string scriptid = ReLoginStaticValueClass.regLogoutPage;

                                string path = Request.Url.AbsolutePath;
                                string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                string[] strpath = path.Split('/');
                                if (strpath.Length > 1)
                                {
                                    path = strpath[1] + ".aspx";
                                }
                                else
                                {
                                    path = ".aspx";
                                }
                                if (scriptid == path)
                                {
                                    li.Attributes.Add("class", "active");
                                }
                                else
                                {
                                    li.Attributes.Remove("class");
                                }

                                hp.NavigateUrl = getRoute(i.ToString(), "Logout.aspx");//scriptid
                                hp.Text = ReLoginStaticValueClass.regLogoutName;
                                li.Controls.Add(hp);
                                //lnkLogout.Click += new EventHandler(lnkLogout_Click);
                                //lnkLogout.Text = ReLoginStaticValueClass.regLogoutName;
                                //li.Controls.Add(lnkLogout);
                                ul.Controls.Add(li);
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region SG-ANZICS and ISRRS Cross-selling
                        for (int i = 0; i <= 3; i++)
                        {
                            HtmlGenericControl li;
                            li = new HtmlGenericControl("li");
                            li.Attributes.Add("runat", "server");

                            bool confExist = fControl.checkPageExist(flowid, SiteDefaultValue.constantConfName);
                            if (confExist)
                            {
                                if (i == 1)
                                {
                                    li.Attributes.Add("id", "a" + i);
                                    HyperLink hp = new HyperLink();
                                    hp.ID = "hp" + i;
                                    string scriptid = ReLoginStaticValueClass.regReceiptPage;

                                    string path = Request.Url.AbsolutePath;
                                    string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                    string[] strpath = path.Split('/');
                                    if (strpath.Length > 1)
                                    {
                                        path = strpath[1] + ".aspx";
                                    }
                                    else
                                    {
                                        path = ".aspx";
                                    }
                                    if (scriptid == path)
                                    {
                                        li.Attributes.Add("class", "active");
                                    }
                                    else
                                    {
                                        li.Attributes.Remove("class");
                                    }

                                    hp.NavigateUrl = getRoute(i.ToString(), scriptid);
                                    hp.Text = ReLoginStaticValueClass.regReceiptName;
                                    //hp.Attributes.Add("target", "_blank");
                                    li.Controls.Add(hp);
                                    ul.Controls.Add(li);
                                }
                            }

                            if (i == 2)
                            {
                                if (isCrossSellingVisible && !string.IsNullOrEmpty(crossSelling) && crossSelling != "#")
                                {
                                    li.Attributes.Add("id", "a" + i);
                                    li.Attributes.Remove("class");
                                    HyperLink hp = new HyperLink();
                                    hp.ID = "hp" + i;
                                    hp.Text = crossSelling;
                                    hp.NavigateUrl = getRoute(i.ToString(), "ReRegIndex.aspx");
                                    li.Controls.Add(hp);
                                    ul.Controls.Add(li);
                                }
                            }

                            if (i == 3)
                            {
                                li.Attributes.Add("id", "a" + i);
                                HyperLink hp = new HyperLink();
                                hp.ID = "hp" + i;
                                //LinkButton lnkLogout = new LinkButton();
                                //lnkLogout.ID = "lnkLogout";
                                //lnkLogout.Attributes.Add("runat", "server");
                                string scriptid = ReLoginStaticValueClass.regLogoutPage;

                                string path = Request.Url.AbsolutePath;
                                string str = Request.Path.Substring(Request.Path.LastIndexOf("/"));
                                string[] strpath = path.Split('/');
                                if (strpath.Length > 1)
                                {
                                    path = strpath[1] + ".aspx";
                                }
                                else
                                {
                                    path = ".aspx";
                                }
                                if (scriptid == path)
                                {
                                    li.Attributes.Add("class", "active");
                                }
                                else
                                {
                                    li.Attributes.Remove("class");
                                }

                                hp.NavigateUrl = getRoute(i.ToString(), "Logout.aspx");//scriptid
                                hp.Text = ReLoginStaticValueClass.regLogoutName;
                                li.Controls.Add(hp);
                                //lnkLogout.Click += new EventHandler(lnkLogout_Click);
                                //lnkLogout.Text = ReLoginStaticValueClass.regLogoutName;
                                //li.Controls.Add(lnkLogout);
                                ul.Controls.Add(li);
                            }
                        }
                        #endregion
                    }

                    divMenu.Controls.Add(ul);
                }
            }
        }
    }
    #endregion

    #region getRoute (get route according to login id(Session["Groupid"]) but no use)
    public string getRoute(string step, string page)
    {
        string route = string.Empty;

        if (Session["Groupid"] != null)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cFun.DecryptValue(urlQuery.CurrShowID);
            FlowControler Flw = new FlowControler(fn);
            RegGroupObj rgp = new RegGroupObj(fn);
            string grpNum = Session["Groupid"].ToString();
            string FlowID = rgp.getFlowID(grpNum, showid);

            showid = cFun.EncryptValue(showid);
            FlowID = cFun.EncryptValue(FlowID);
            step = cFun.EncryptValue(step);

            if (page == "Logout.aspx")//ReLoginStaticValueClass.regLogoutPage)
            {
                route = page + "?Event=" + GetEventNameByShowIDFlowID(cFun.DecryptValue(showid), cFun.DecryptValue(FlowID));//Flw.MakeFullURL(page, FlowID, showid);
            }
            else
            {
                string regno = string.Empty;
                if (Session["Regno"] != null)
                {
                    regno = cFun.EncryptValue(Session["Regno"].ToString());

                    grpNum = cFun.EncryptValue(grpNum);
                    route = Flw.MakeFullURL(page, FlowID, showid, grpNum, step, regno);
                }
                else
                {
                    grpNum = cFun.EncryptValue(grpNum);
                    route = Flw.MakeFullURL(page, FlowID, showid, grpNum, step);
                }
            }
        }
        else
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            Response.Redirect(exhibitorMDALoginUrl);
        }

        return route;
    }
    #endregion

    private string GetEventNameByShowIDFlowID(string showid, string flowid)
    {
        string flowName = string.Empty;
        try
        {
            string sql = "select FLW_Name from tb_site_flow_master  where ShowID=@ShowID And FLW_ID=@FLW_ID and Status=@Status";
            //'EAD351'//FLW_Name=@FName 

            SqlParameter spar1 = new SqlParameter("ShowID", SqlDbType.NVarChar);
            SqlParameter spar2 = new SqlParameter("FLW_ID", SqlDbType.NVarChar);
            SqlParameter spar3 = new SqlParameter("Status", SqlDbType.NVarChar);
            spar1.Value = showid;
            spar2.Value = flowid;
            spar3.Value = RecordStatus.ACTIVE;
            List<SqlParameter> pList = new List<SqlParameter>();
            pList.Add(spar1);
            pList.Add(spar2);
            pList.Add(spar3);

            DataTable dList = fn.GetDatasetByCommand(sql, "DT", pList).Tables["DT"];

            if (dList.Rows.Count > 0)
            {
                flowName = dList.Rows[0]["FLW_Name"].ToString();
            }
        }
        catch { }
        return flowName;
    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        //FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        //string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        //string flowid = cFun.DecryptValue(urlQuery.CurrShowID);

        //string page = ReLoginStaticValueClass.regLogoutPage;
        //string param = page + "?Event=" + GetEventNameByShowIDFlowID(showid, flowid);
        //var url = "https://www.event-reg.biz/registration/" + param;

        //Response.Clear();
        //var sb = new System.Text.StringBuilder();
        //sb.Append("<html>");
        //sb.AppendFormat("<body onload='document.forms[0].submit()'>");
        //sb.AppendFormat("<form action='{0}' method='post'>", url);
        //sb.Append("</form>");
        //sb.Append("</body>");
        //sb.Append("</html>");
        //Response.Write(sb.ToString());
        //Response.End();
    }

    #region  Register for another show for Cross-selling ISRRS and SG-ANZICS vice-visa[14-11-2018]
    static string delegateRegNo = "Delegate_RefRegno";
    static string iSRRS2019DelegateRegShowID = "NCE361";
    static string iSRRS2019DelegateRegFlowID = "F394";
    static string iSRRSProfDefault = "Allied Health Professionals";
    static string iSRRSProfDoctors = "Doctor";
    static string iSRRSProfAdministrative = "Administrative Professionals & Others";
    static string iSRRSProfNurses = "Nurse";
    static string iSRRSProfStudents = "Students**";
    static string iSRRS2019EventWebsite = "http://www.isrrs2019.sg/";
    static string SGANZICSDelegateRegShowID = "BFZ364";
    static string SGANZICSDelegateRegFlowID = "F399";
    static string SGANZICSProfDefault = "Allied Health Professionals in Intensive Care";
    static string SGANZICSProfDoctors = "Doctor";
    static string SGANZICSProfTraineeDoc = "Trainee Doctors**";
    static string SGANZICSProfLMICDoc = "LMIC Doctors";
    static string SGANZICSProfNurses = "Nurse";
    static string SGANZICSProfStudents = "Students**";
    static string SGANZICSProfLMICNurse = "LMIC Nurse";
    static string SGANZICSEventWebsite = "http://www.sg-anzics.com/";
    private string setRegisterAnotherShowButton(string GroupRegID, ref bool isVisible)
    {
        string RegisterAnotherShow = "#";
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cFun.DecryptValue(urlQuery.CurrShowID);
        string flowid = cFun.DecryptValue(urlQuery.FlowID);

        /***/
        string DelegateID = "";
        if (Session["Regno"] != null)
        {
            DelegateID = Session["Regno"].ToString();
        }
        /***/

        if (!string.IsNullOrEmpty(showid))
        {
            if (!string.IsNullOrEmpty(DelegateID))
            {
                FlowControler fCon = new FlowControler(fn);
                FlowMaster flwMaster = fCon.GetFlowMasterConfig(flowid);
                if (flwMaster.FlowType == SiteFlowType.FLOW_INDIVIDUAL)
                {
                    string crossShowID = "";
                    string crossFlowID = "";
                    if (showid == iSRRS2019DelegateRegShowID && flowid == iSRRS2019DelegateRegFlowID)
                    {
                        crossShowID = SGANZICSDelegateRegShowID;
                        crossFlowID = SGANZICSDelegateRegFlowID;
                    }
                    else if (showid == SGANZICSDelegateRegShowID && flowid == SGANZICSDelegateRegFlowID)
                    {
                        crossShowID = iSRRS2019DelegateRegShowID;
                        crossFlowID = iSRRS2019DelegateRegFlowID;
                    }

                    if (!string.IsNullOrEmpty(crossShowID) && !string.IsNullOrEmpty(crossFlowID))
                    {
                        InvoiceControler invControler = new InvoiceControler(fn);
                        string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, GroupRegID, DelegateID);
                        List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
                        if (invListObj != null && invListObj.Count > 0)
                        {
                            RegAdditionalControler addCtrl = new RegAdditionalControler(fn);
                            string colleagueValue = addCtrl.getColleagueIDByRefValue(DelegateID, showid, flowid);
                            if (string.IsNullOrEmpty(colleagueValue))
                            {
                                ShowControler shwCtr = new ShowControler(fn);
                                Show shw = shwCtr.GetShow(crossShowID);
                                isVisible = true;
                                RegisterAnotherShow = "Register for " + shw.SHW_Name;
                            }
                        }
                    }
                }
            }
        }

        return RegisterAnotherShow;
    }
    #endregion
}
