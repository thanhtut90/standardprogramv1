﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="RegGroupIDEM.aspx.cs" Inherits="RegGroupIDEM" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .red
        {
            color:red;
        }
        /*.chkFooter
        {
            white-space:nowrap;
        }*/
        .chkFooter label
        {
            font-weight:unset !important;
            padding-left:5px !important;
            vertical-align:top !important;
        }
        .chkFooter input[type="checkbox"]
        {
            margin-top: 2px !important;
        }
        a:hover
        {
            text-decoration:none !important;
        }
        .rowLabel
        {
            padding-top: 7px;
        }

        .blink{
		/*width:200px;
		height: 50px;
		line-height: 50px;*/
	    background-color: #f8c0f8;
		padding: 10px;	
		text-align: center;
	        }
	        /*#ContentPlaceHolder1_lblHDR1{
		        font-size: 25px;
                font-weight:bold;
		        color: black;
		        animation: blink 1s linear infinite;
	        }*/
        /*@keyframes blink{
        0%{opacity: 0;}
        50%{opacity: .5;}
        100%{opacity: 1;}
        }*/
        .noteStyle
        {
            padding-top:0px !important;
            padding-bottom:6px !important;
        }
    </style>

    <script type="text/javascript">
        function validateDate(elementRef) {
                var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

                var dateValue = elementRef.value;

                if (dateValue.length != 10) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                // mm/dd/yyyy format... 
                var valueArray = dateValue.split('/');

                if (valueArray.length != 3) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthValue = parseFloat(valueArray[1]);
                var dayValue = parseFloat(valueArray[0]);
                var yearValue = parseFloat(valueArray[2]);

                if ((isNaN(monthValue) == true) || (isNaN(dayValue) == true) || (isNaN(yearValue) == true)) {
                    alert('Non-numeric entry detected\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                if (((yearValue % 4) == 0) && (((yearValue % 100) != 0) || ((yearValue % 400) == 0)))
                    monthDays[1] = 29;
                else
                    monthDays[1] = 28;

                if ((monthValue < 1) || (monthValue > 12)) {
                    alert('Invalid month entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthDaysArrayIndex = monthValue - 1;
                if ((dayValue < 1) || (dayValue > monthDays[monthDaysArrayIndex])) {
                    alert('Invalid day entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

            return true;
        }

        function limitText(limitField, limitNum) {
            var txtValue = limitField.value.replace(/\s/g, '')
            if (txtValue.length < limitNum) {
                //limitField.value = limitField.value.substring(0, limitNum);
                alert("Minimum 2 characters!");
            }
        }
        function validate() {
            var limitNum = 2;
            var errmsg = "";
            var isValid = true;
            var fname = document.getElementById("<%=txtFName.ClientID%>").value;
            var fnameValue = fname.replace(/\s/g, '')
            if (fnameValue.length < limitNum) {
                isValid = false;
                errmsg += " First Name in Full";
            }
            var lname = document.getElementById("<%=txtLName.ClientID%>").value;
            var lnameValue = lname.replace(/\s/g, '')
            if (lnameValue.length < limitNum) {
                if(!isValid) { errmsg += ", " }else{ errmsg += " " }
                errmsg += "Last Name in Full";
                isValid = false;
            }
            if(!isValid)
            {
                alert("Minimum 2 characters in" + errmsg);
            }
            return isValid;
        }

        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }

        function radioMe(e) {
              if (!e) e = window.event;
              var sender = e.target || e.srcElement;

              if (sender.nodeName != 'INPUT') return;
              var checker = sender;
              var chkBox = document.getElementById('<%= chkIsDelegate.ClientID %>');
              var chks = chkBox.getElementsByTagName('INPUT');
              for (i = 0; i < chks.length; i++) {
                  if (chks[i] != checker)
                  chks[i].checked = false;
              }
        }
    </script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=DC-6953330"></script>
    <script> window.dataLayer = window.dataLayer || []; function gtag() { dataLayer.push(arguments); } </script>
    <!--
    Event snippet for FR~Page Load _MK~SG_PC~IDEM2020_GroupReg on https://www.event-reg.biz/Registration/RegGroupIDEM?FLW=lZJYoCTCn3yylGMJBXarQg==&SHW=CT6vFjNOWLIC/OECGDGWKg==&GRP=xYPvujRkLTcB4iq/9pTCtA==&STP=uTx3HwZBhvahan5gxR/%7C%7CCA==: Please do not remove.
    Place this snippet on pages with events you’re tracking. 
    Creation date: 12/19/2019
    -->
    <script>
      gtag('event', 'conversion', {
        'allow_custom_scripts': true,
        'send_to': 'DC-6953330/mice/frpag020+standard'
      });
    </script>
    <noscript>
    <img src="https://ad.doubleclick.net/ddm/activity/src=6953330;type=mice;cat=frpag020;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=1?" width="1" height="1" alt=""/>
    </noscript>
    <!-- End of event snippet: Please do not remove -->

    <!--
    Event snippet for FR~On Click _MK~SG_PC~IDEM2020_GroupRegsubmit on https://www.event-reg.biz/Registration/RegGroupIDEM?FLW=lZJYoCTCn3yylGMJBXarQg==&SHW=CT6vFjNOWLIC/OECGDGWKg==&GRP=xYPvujRkLTcB4iq/9pTCtA==&STP=uTx3HwZBhvahan5gxR/%7C%7CCA==: Please do not remove.
    Place this snippet on pages with events you’re tracking. 
    Creation date: 12/19/2019
    -->
    <script>
      gtag('event', 'conversion', {
        'allow_custom_scripts': true,
        'send_to': 'DC-6953330/mice/fronc01b+standard'
      });
    </script>
    <noscript>
    <img src="https://ad.doubleclick.net/ddm/activity/src=6953330;type=mice;cat=fronc01b;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=1?" width="1" height="1" alt=""/>
    </noscript>
    <!-- End of event snippet: Please do not remove -->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <%--<div class="form-group">
                <div class="form-group row">
                    <div class="col-md-5 col-md-offset-1"><asp:Label ID="lblHeader" runat="server" Text="(for 5 persons and above only)" Font-Bold="true" visible="false"></asp:Label></div>
                    <div class="col-md-6">
                        &nbsp;
                    </div>
                </div>
            </div>--%>

            <div id="divHDR1" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1" style="font-size:25px;">
                        <asp:Label ID="lblHDR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR2" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblHDR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR3" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblHDR3" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>

            <asp:Label ID="lblIsApprove" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="lblIsSMS_PhotoApprove" runat="server" Text="" Visible="false"></asp:Label>

            <div class="form-group row" runat="server" id="divPromoCode" visible="false">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblPromoCode" runat="server" CssClass="form-control-label" Text="Invitation Code"></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtPromoCode" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcPromoCode" runat="server" Enabled="false"
                        ControlToValidate="txtPromoCode" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revPromoCode" runat="server" ControlToValidate="txtPromoCode" ErrorMessage="Invalid entry"
                        ValidationExpression="^[A-Za-z0-9\s()+|\{}.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divNRICType" visible="false">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblNRICType" runat="server" CssClass="form-control-label" Text="NRIC / FIN / Passport No Type"></asp:Label><span class="red">*</span>
                </div>
                <div class="col-md-5">
                    <asp:DropDownList ID="ddlNRICType" runat="server" CssClass="form-control">
                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                        <asp:ListItem Value="1">NRIC</asp:ListItem>
                        <asp:ListItem Value="2">FIN</asp:ListItem>
                        <asp:ListItem Value="3">ID</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:CompareValidator ID="cvNRICType" runat="server" 
                    ControlToValidate="ddlNRICType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divDepartment" visible="false">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblDepartment" runat="server" CssClass="form-control-label" Text="NRIC / FIN / ID"></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcDeptm" runat="server"
                    ControlToValidate="txtDepartment" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPassNo" runat="server" Enabled="false"
                        ControlToValidate="txtDepartment" ForeColor="Red" 
                        ErrorMessage="*Invalid NRIC" ValidationExpression="^([a-zA-Z]\d{7}[a-zA-Z])$" />
                </div>
            </div>

            <div class="form-group row" runat="server" id="divSalutation">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblSalutation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:DropDownList ID="ddlSalutation" runat="server" CssClass="form-control"
                        OnSelectedIndexChanged="ddlSalutation_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:CompareValidator ID="vcSal" runat="server" 
                    ControlToValidate="ddlSalutation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divSalOther" visible="false">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    &nbsp;
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtSalOther" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcSalOther" runat="server"
                    ControlToValidate="txtSalOther" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revSalOther" runat="server" ControlToValidate="txtSalOther" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divFName">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblFName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtFName" runat="server" CssClass="form-control" onblur="limitText(this,2);"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcFName" runat="server"
                    ControlToValidate="txtFName" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revFName" runat="server" ControlToValidate="txtFName" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        <%--^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*--%>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divLName">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblLName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtLName" runat="server" CssClass="form-control" onblur="limitText(this,2);"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcLName" runat="server"
                    ControlToValidate="txtLName" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revLName" runat="server" ControlToValidate="txtLName" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        <%--^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*--%>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divCompany">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblCompany" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtCompany" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcCom" runat="server"
                    ControlToValidate="txtCompany" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revCompany" runat="server" ControlToValidate="txtCompany" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divCountry">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:CompareValidator ID="vcCountry" runat="server" 
                        ControlToValidate="ddlCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divDesignation">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcDesig" runat="server"
                    ControlToValidate="txtDesignation" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revDesignation" runat="server" ControlToValidate="txtDesignation" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div runat="server" id="divTel">
                <div class="form-group row">
                    <div class="col-md-3 col-md-offset-1 rowLabel">
                        <asp:Label ID="lblTel" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5" runat="server">
                        <div class="col-xs-3" runat="server" id="divTelcc" style="padding-left:0px;" >
                            <asp:TextBox ID="txtTelcc" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Country Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTelcc" runat="server"
                                TargetControlID="txtTelcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-3" runat="server" id="divTelac" style="padding-left:0px;" >
                            <asp:TextBox ID="txtTelac" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Area Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTelac" runat="server"
                                TargetControlID="txtTelac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-6" runat="server" id="divTelNo" style="padding-right:0px;padding-left:0px;">
                            <asp:TextBox ID="txtTel" runat="server" CssClass="form-control" AutoCompleteType="Disabled" placeholder="Telephone Number"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTel" runat="server"
                                TargetControlID="txtTel"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcTel" runat="server"
                        ControlToValidate="txtTel" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcTelcc" runat="server"
                        ControlToValidate="txtTelcc" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcTelac" runat="server"
                        ControlToValidate="txtTelac" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-8 col-md-offset-4 rowLabel noteStyle">
                        <i><asp:Label ID="Label5" runat="server" CssClass="form-control-label" Text="(Do not include special characters such as '+', '-')"></asp:Label></i>
                    </div>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divEmail">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3 col-xs-12">
                    <asp:RequiredFieldValidator ID="vcEmail" runat="server"
                    ControlToValidate="txtEmail" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="validateEmail"
                    runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                    ControlToValidate="txtEmail"
                    ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                </div>
            </div>

            <div class="form-group row" runat="server" id="divEmailConfirmation">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblEmailConfirmation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtEmailConfirmation" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-3 col-xs-12">
                    <asp:RequiredFieldValidator ID="vcEConfirm" runat="server"
                    ControlToValidate="txtEmailConfirmation" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cmpEmail" runat="server" ControlToValidate="txtEmailConfirmation"
                    ControlToCompare="txtEmail" ErrorMessage="Not match." ForeColor="Red"></asp:CompareValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divDOB">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblDOB" runat="server" CssClass="form-control-label" Text="Date of Birth"></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control" placeholder="DD/MM/YYYY" AutoPostBack="true"></asp:TextBox><%-- onblur="validateDate(this);"--%>
                    <asp:CalendarExtender ID="calendarDOB" TargetControlID="txtDOB"
                            runat="server" PopupButtonID="txtDOB" Format="dd/MM/yyyy" ></asp:CalendarExtender>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcDOB" runat="server" Enabled="false"
                    ControlToValidate="txtDOB" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divGender">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblGender" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                        <asp:ListItem Text="Please select" Value="Please select"></asp:ListItem>
                        <asp:ListItem Text="M" Value="M"></asp:ListItem>
                        <asp:ListItem Text="F" Value="F"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:CompareValidator ID="vcGender" runat="server" 
                        ControlToValidate="ddlGender" ValueToCompare="Please select" Operator="NotEqual" Type="String" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divAddress1">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblAddress1" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcAdd1" runat="server"
                    ControlToValidate="txtAddress1" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revAddress1" runat="server" ControlToValidate="txtAddress1" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divAddress2">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblAddress2" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcAdd2" runat="server"
                    ControlToValidate="txtAddress2" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revAddress2" runat="server" ControlToValidate="txtAddress2" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divAddress3">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblAddress3" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcAdd3" runat="server"
                    ControlToValidate="txtAddress3" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revAddress3" runat="server" ControlToValidate="txtAddress3" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divCity">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblCity" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcCity" runat="server"
                    ControlToValidate="txtCity" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revCity" runat="server" ControlToValidate="txtCity" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divVisitDate">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblVisitDate" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtVisitDate" runat="server" CssClass="form-control"></asp:TextBox><%-- onblur="validateDate(this);"--%>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcVisitDate" runat="server"
                    ControlToValidate="txtVisitDate" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator></div>
                    <asp:RegularExpressionValidator ID="revVisitDate" runat="server" ControlToValidate="txtVisitDate" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
            </div>

            <div class="form-group row" runat="server" id="divVisitTime">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblVisitTime" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtVisitTime" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcVisitTime" runat="server"
                    ControlToValidate="txtVisitTime" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revVisitTime" runat="server" ControlToValidate="txtVisitTime" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divOtherDesignation">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblOtherDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtOtherDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcOtherDesignation" runat="server"
                    ControlToValidate="txtOtherDesignation" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revOtherDesignation" runat="server" ControlToValidate="txtOtherDesignation" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divIndusOther" visible="false">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    &nbsp;
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtIndusOther" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcIndusOther" runat="server"
                    ControlToValidate="txtIndusOther" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revIndusOther" runat="server" ControlToValidate="txtIndusOther" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divRCountry">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblRCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:DropDownList ID="ddlRCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlRCountry_SelectedIndexChanged">
                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:CompareValidator ID="vcRCountry" runat="server" 
                        ControlToValidate="ddlRCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divPostalcode">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblPostalcode" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtPostalcode" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcPCode" runat="server"
                    ControlToValidate="txtPostalcode" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPostalcode" runat="server" ControlToValidate="txtPostalcode" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divMobile">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblMobile" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5" runat="server" >
                    <div class="col-xs-3" runat="server" id="divMobcc" style="padding-left:0px;" >
                        <asp:TextBox ID="txtMobcc" runat="server" CssClass="form-control" placeholder="Country Code" MaxLength="50"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbMobcc" runat="server"
                            TargetControlID="txtMobcc"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-xs-3" runat="server" id="divMobac" style="padding-left:0px;" >
                        <asp:TextBox ID="txtMobac" runat="server" CssClass="form-control" placeholder="Area Code" MaxLength="50"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbMobac" runat="server"
                            TargetControlID="txtMobac"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-xs-6" runat="server" id="divMobileNo" style="padding-right:0px;padding-left:0px;">
                        <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control" placeholder="Mobile Number" MaxLength="50"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbMob" runat="server"
                            TargetControlID="txtMobile"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                </div>
                <div class="col-md-1">
                    <asp:RequiredFieldValidator ID="vcMob" runat="server"
                    ControlToValidate="txtMobile" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-1">
                    <asp:RequiredFieldValidator ID="vcMobcc" runat="server"
                    ControlToValidate="txtMobcc" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-1">
                    <asp:RequiredFieldValidator ID="vcMobac" runat="server"
                    ControlToValidate="txtMobac" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divFax">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblFax" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5" runat="server" >
                    <div class="col-xs-3" runat="server" id="divFaxcc" style="padding-left:0px;" >
                        <asp:TextBox ID="txtFaxcc" runat="server" CssClass="form-control" placeholder="Country Code" MaxLength="50"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbFaxcc" runat="server"
                            TargetControlID="txtFaxcc"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-xs-3" runat="server" id="divFaxac" style="padding-left:0px;" >
                        <asp:TextBox ID="txtFaxac" runat="server" CssClass="form-control" placeholder="Area Code" MaxLength="50"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbFaxac" runat="server"
                            TargetControlID="txtFaxac"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-xs-6" runat="server" id="divFaxNo" style="padding-right:0px;padding-left:0px;">
                        <asp:TextBox ID="txtFax" runat="server" CssClass="form-control" placeholder="Fax Number" MaxLength="50"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbFax" runat="server"
                            TargetControlID="txtFax"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                </div>
                <div class="col-md-1">
                    <asp:RequiredFieldValidator ID="vcFax" runat="server"
                    ControlToValidate="txtFax" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-1">
                    <asp:RequiredFieldValidator ID="vcFaxcc" runat="server"
                    ControlToValidate="txtFaxcc" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
                <div class="col-md-1">
                    <asp:RequiredFieldValidator ID="vcFaxac" runat="server"
                    ControlToValidate="txtFaxac" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divState">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblState" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcState" runat="server"
                    ControlToValidate="txtState" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revState" runat="server" ControlToValidate="txtState" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divIndustry">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblIndustry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:DropDownList ID="ddlIndustry" runat="server" CssClass="form-control"
                        OnSelectedIndexChanged="ddlIndustry_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:CompareValidator ID="vcIndus" runat="server" 
                    ControlToValidate="ddlIndustry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divAge">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblAge" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtAge" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="ftbAge" runat="server"
                        TargetControlID="txtAge"
                        FilterType="Numbers"
                        ValidChars="+0123456789" />
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcAge" runat="server"
                    ControlToValidate="txtAge" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divPassword">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblPassword" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcPassword" runat="server"
                    ControlToValidate="txtPassword" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divAdditional4">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblAdditional4" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtAdditional4" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:DropDownList ID="ddlAdditional4" runat="server" CssClass="form-control"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlAdditional4_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcAdditional4" runat="server"
                    ControlToValidate="txtAdditional4" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvAdditional4" runat="server" 
                    ControlToValidate="ddlAdditional4" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    <asp:RegularExpressionValidator ID="revAdditional4" runat="server" ControlToValidate="txtAdditional4" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divAdditional5">
                <div class="col-md-3 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblAdditional5" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                </div>
                <div class="col-md-5">
                    <asp:TextBox ID="txtAdditional5" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:DropDownList ID="ddlAdditional5" runat="server" CssClass="form-control"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlAdditional5_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:RequiredFieldValidator ID="vcAdditional5" runat="server"
                    ControlToValidate="txtAdditional5" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvAdditional5" runat="server" 
                    ControlToValidate="ddlAdditional5" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                    ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    <asp:RegularExpressionValidator ID="revAdditional5" runat="server" ControlToValidate="txtAdditional5" ErrorMessage="Invalid entry"
                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divEUResidentYes" visible="false">
                <div class="col-md-10 col-md-offset-1 rowLabel">
                    <asp:Label ID="lblEU" runat="server">
                        Data Protection Note:
                        <br /><br />
                        By registering for IDEM 2020, you are consenting to receive communications that are important for your preparation to the event from Koelnmesse Pte Ltd and its appointed partners/affiliates for this event. This includes registration acknowledgement and confirmation emails, onsite check-in instructions, business networking opportunities, event surveys, onsite e-payment services as well as onsite on-demand information sharing services, etc. You will be subject to their communications and privacy policy and may opt-out or withdraw your consent directly with them.
                        <br /><br />
                        After the event, we would like to keep in touch with you about IDEM 2020, and our related business events that might be of interest to you. Therefore, we kindly request your consent for allowing us to communicate directly with you via telephone, email, SMS and/or digital media.
                        <br /><br />
                        <asp:RadioButtonList ID="rbDataProtection" runat="server" RepeatColumns="1" RepeatDirection="Vertical" 
                            AutoPostBack="true" OnSelectedIndexChanged="rbDataProtection_SelectedIndexChanged">
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </asp:Label>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divDataProtectionYes" visible="false">
                <div class="col-md-10 col-md-offset-1 rowLabel">
                    <asp:Label ID="Label1" runat="server">
                        I hereby permit Koelnmesse to use the details entered on this form for sending information and advertising of industry events staged by Koelnmesse Pte Ltd, the organisers of IDEM 2020, in line with the regulations of the Personal Data Protection Act of Singapore 2012 and the Koelnmesse Personal Data Protection.
                        <br /><br />
                        Please note that you can withdraw your consent at any point in time simply by clicking the unsubscribe link in the footer of our emails, or withdraw your consent for future use simply by contacting <a href="mailto:privacypolicycontroller@koelnmesse.com.sg" target="_blank">privacypolicycontroller@koelnmesse.com.sg</a>. You can refer to the details of our complete Personal Data Protection Policy online at <a href="http://www.koelnmesse.com.sg/koelnmesse-personal-data-privacy-policy" target="_blank">www.koelnmesse.com.sg/koelnmesse-personal-data-privacy-policy</a>. Please note that if you do not give your consent and/or withdraw your consent, Koelnmesse may not be able to continue sending information and advertising of industry events staged by Koelnmesse Pte Ltd.
                    </asp:Label>
                </div>
            </div>

            <div class="form-group row" runat="server" id="divEUResidentNo" visible="false">
                <div class="col-md-10 col-md-offset-1 rowLabel">
                    <asp:Label ID="Label2" runat="server">
                        Data Protection Note:
                        <br /><br />
                        By registering for IDEM 2020, you are consenting to receive communications that are important for your preparation to the event from Koelnmesse Pte Ltd and its appointed partners/affiliates for this event. This includes registration acknowledgement and confirmation emails, onsite check-in instructions, business networking opportunities, event surveys, onsite e-payment services as well as onsite on-demand information sharing services, etc. You will be subject to their communications and privacy policy and may opt-out or withdraw your consent directly with them.
                        <br /><br />
                        Please note that you can withdraw your consent at any point in time simply by clicking the unsubscribe link in the footer of our emails, or withdraw your consent for future use simply by contacting <a href="mailto:privacypolicycontroller@koelnmesse.com.sg" target="_blank">privacypolicycontroller@koelnmesse.com.sg</a>. You can refer to the details of our complete Personal Data Protection Policy online at <a href="https://www.koelnmesse.com.sg/koelnmesse-personal-data-privacy-policy" target="_blank">www.koelnmesse.com.sg/koelnmesse-personal-data-privacy-policy</a>. Please note that if you do not give your consent and/or withdraw your consent, Koelnmesse may not be able to continue sending information and advertising of industry events staged by Koelnmesse Pte Ltd.
                    </asp:Label>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="form-group row" runat="server" id="divPhotoUpload" visible="false">
        <div class="col-md-3 col-md-offset-1 rowLabel">
            <asp:Label ID="lblPhotoUpload" runat="server" CssClass="form-control-label" Text="Photo upload"></asp:Label>
        </div>
        <div class="col-md-5">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:FileUpload runat="server" ID="fupPhotoUpload" CssClass="style4" 
                        style="font-family: DINPro-Regular" />
                    <br />
                    <ul style="list-style-type:disc;">
                        <li>Photo image is required for the production of pass.</li>
                        <li>JPEG image format (with .jpg file extension)</li>
                        <li>400 by 514 pixels image size</li>
                        <li>No more than 150 KBytes file size.</li>
                        <li>Save photo filemame as NRIC_Name of applicant e.g. SXXXX123A_Lim Siew Lan</li>
                    </ul>
                    <%--<asp:Label ID="lblPhotoUploadNote" runat="server" Text="Please upload png/jpg/jpeg file format." Font-Italic="true"></asp:Label>--%>
                    <%--<br />
                    <br />--%>
                    <asp:HyperLink ID="hpPhotoUpload" runat="server" Visible="false" Target="_blank" NavigateUrl="#"></asp:HyperLink>
                    <asp:Image runat="server" ID="imgPhotoUpload" Width="280px" Height="150px" Visible="false" ImageUrl="#" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="col-md-2">
            <asp:Label ID="lblErrPhotoUpload" runat="server" ForeColor="Red" Text="*Required" Visible="false"></asp:Label>
            <%--<asp:CompareValidator ID="CompareValidator1" runat="server" 
            ControlToValidate="ddlStudentType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>--%>
        </div>
    </div>

    <div class="form-group">
        <div class="form-group row" runat="server" id="divIsDelegate">
            <div class="col-md-3 col-md-offset-1 rowLabel">
                <asp:Label ID="lblIsDelegate" runat="server" Text="I will be attending show." Font-Bold="true"></asp:Label>
                <asp:Label ID="lblErrIsDelegate" runat="server" Text="*Required" ForeColor="Red" Visible="false"></asp:Label>
            </div>
            <div class="col-md-5">
                <asp:CheckBoxList ID="chkIsDelegate" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" CellPadding="10" Font-Size="Small">
                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                    <asp:ListItem Text="No" Value="No" Selected="True"></asp:ListItem>
                </asp:CheckBoxList>
                <%--<asp:CheckBox ID="chkIsDelegate" runat="server" Text="I will be attending show." />--%>
                <asp:Label ID="lblDelegate" runat="server" Text="" Visible="false"></asp:Label>
            </div>
            <div class="col-md-2">&nbsp;</div>
        </div>
    </div>

    <div id="divFTRCHK1" runat="server" visible="false">
        <div  class="form-group row">
            <div class="clear"></div>
            <div class="col-md-10 col-md-offset-1">
                <asp:CheckBoxList ID="chkFTRCHK1" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                <asp:Label ID="lblErrFTRCHK1" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                <br />
                <br />
            </div>
        </div>
    </div>
    <div id="divFTR1" runat="server" visible="false">
        <div  class="form-group row">
            <div class="clear"></div>
            <div class="col-md-10 col-md-offset-1">
                <asp:Label ID="lblFTR1" runat="server"></asp:Label>
                <br />
                <br />
            </div>
        </div>
    </div>
    <div id="divFTR2" runat="server" visible="false">
        <div  class="form-group row">
            <div class="clear"></div>
            <div class="col-md-10 col-md-offset-1">
                <asp:Label ID="lblFTR2" runat="server"></asp:Label>
                <br />
                <br />
            </div>
        </div>
    </div>
    <div id="divFTRCHK" runat="server" visible="false">
        <div  class="form-group row">
            <div class="clear"></div>
            <div class="col-md-10 col-md-offset-1">
                <asp:CheckBoxList ID="chkFTRCHK" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                <asp:Label ID="lblErrFTRCHK" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                <br />
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="form-group container" style="padding-top:20px;">
            <div class="col-lg-offset-5 col-lg-3 col-sm-offset-4 col-sm-3 center-block" >
                <asp:Button runat="server" ID="btnNext" CssClass="btn MainButton btn-block" Text="Next" OnClick="btnNext_Click"
                    OnClientClick ="return validate();" />
            </div>
        </div>
    </div>
    <br />

</asp:Content>

