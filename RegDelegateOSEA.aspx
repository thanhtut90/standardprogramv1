﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="RegDelegateOSEA.aspx.cs" Inherits="RegDelegateOSEA" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .red
        {
            color:red;
        }
        .chkFooter label
        {
            font-weight:unset !important;
            padding-left:5px !important;
            vertical-align:top !important;
        }
        .chkFooter input[type="checkbox"]
        {
            margin-top: 2px !important;
        }
    </style>

    <script type="text/javascript">
        function validateDate(elementRef) {
                var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

                var dateValue = elementRef.value;

                if (dateValue.length != 10) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                // mm/dd/yyyy format... 
                var valueArray = dateValue.split('/');

                if (valueArray.length != 3) {
                    alert('Entry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthValue = parseFloat(valueArray[1]);
                var dayValue = parseFloat(valueArray[0]);
                var yearValue = parseFloat(valueArray[2]);

                if ((isNaN(monthValue) == true) || (isNaN(dayValue) == true) || (isNaN(yearValue) == true)) {
                    alert('Non-numeric entry detected\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                if (((yearValue % 4) == 0) && (((yearValue % 100) != 0) || ((yearValue % 400) == 0)))
                    monthDays[1] = 29;
                else
                    monthDays[1] = 28;

                if ((monthValue < 1) || (monthValue > 12)) {
                    alert('Invalid month entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

                var monthDaysArrayIndex = monthValue - 1;
                if ((dayValue < 1) || (dayValue > monthDays[monthDaysArrayIndex])) {
                    alert('Invalid day entered\nEntry must be in the format dd/mm/yyyy.');

                    return false;
                }

            return true;
        }

        //function noBack() { window.history.forward(); }
        //noBack();
        //window.onload = noBack;
        //window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        //window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblSpeRegno" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="lblIsVIPCodeUsed" runat="server" Text="0" Visible="false"></asp:Label>
            <asp:Label ID="lblIsRegisterColleague" runat="server" Text="" Visible="false"></asp:Label>
            <div id="divHDR1" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblHDR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR2" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblHDR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR3" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblHDR3" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>

            <div id="divReg" runat="server"  class="form-group">
                <div class="form-group row" runat="server" id="divPromoCode" visible="false">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblPromoCode" runat="server" CssClass="form-control-label" Text="Invitation Code"></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtPromoCode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcPromoCode" runat="server" Enabled="false"
                            ControlToValidate="txtPromoCode" Display="Dynamic" ValidationGroup="Group"
                            ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAdditional5">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblAdditional5" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtAdditional5" runat="server" CssClass="form-control" 
                            AutoPostBack="true" OnTextChanged="txtAdditional5_TextChanged"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcAdditional5" runat="server"
                        ControlToValidate="txtAdditional5" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAdditional4">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblAdditional4" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtAdditional4" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcAdditional4" runat="server"
                        ControlToValidate="txtAdditional4" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <%--<asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="catchanged" Visible="False"></asp:DropDownList>--%>
                 <div class="form-group row" runat="server" id="divSalutation">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblSalutation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList ID="ddlSalutation" runat="server" CssClass="form-control"
                            OnSelectedIndexChanged="ddlSalutation_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcSal" runat="server"  ValidationGroup="Group"
                        ControlToValidate="ddlSalutation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divSalOther" visible="false">
                     <div class="col-md-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtSalOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcSalOther" runat="server"
                        ControlToValidate="txtSalOther" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divFName">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblFName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtFName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcFName" runat="server"
                        ControlToValidate="txtFName" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divLName">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblLName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtLName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcLName" runat="server"
                        ControlToValidate="txtLName" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divPassNo">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblPassNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtPassNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcPassNo" runat="server"
                        ControlToValidate="txtPassNo" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divIsReg">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblIsReg" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:RadioButtonList ID="rbreg" runat="server" Width="100px" RepeatDirection="Horizontal" TextAlign="Right" AutoPostBack="true" OnSelectedIndexChanged="rbreg_SelectedIndexChanged">
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                        <div runat="server" id="divRegSpecific" visible="false">
                            <asp:RadioButtonList ID="rbregspecific" runat="server" RepeatDirection="Horizontal" TextAlign="Right" Width="150px">
                                <asp:ListItem Value="MCR" Text="MCR" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="SNB" Text="SNB"></asp:ListItem>
                                <asp:ListItem Value="PRN" Text="PRN"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divIDNo">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblIDNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtIDNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcIDNo" runat="server"
                        ControlToValidate="txtIDNo" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDesignation">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcDesig" runat="server"
                        ControlToValidate="txtDesignation" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <!-- Job Title for Studnet & Allied Health -->
                <div class="form-group row" runat="server" id="divJobtitle">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblJobtitle" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtJobtitle" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcJobtitle" runat="server"
                        ControlToValidate="txtJobtitle" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divProfession">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblProfession" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                       <asp:DropDownList runat="server" ID="ddlProfession" AutoPostBack="true" CssClass="form-control"
                            OnSelectedIndexChanged="ddlProfession_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcProfession" runat="server"  ValidationGroup="Group"
                        ControlToValidate="ddlProfession" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divProOther" visible="false">
                     <div class="col-md-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtProOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcProOther" runat="server"
                        ControlToValidate="txtProOther" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDoctor" visible="false">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblDoctor" runat="server" CssClass="form-control-label" Text="Are you a MOHOS/Resident?"></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:RadioButtonList ID="rbDoctor" runat="server" Width="100px" RepeatDirection="Horizontal" TextAlign="Right">
                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divStudentUpload">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblStudentUpload" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:FileUpload runat="server" ID="fupStudentUpload" CssClass="style4" 
                            style="font-family: DINPro-Regular" />
                        <br />
                        <asp:Image runat="server" ID="imgStudentUpload" Width="280px" Height="150px" Visible="false" ImageUrl="#" />
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="CompareValidator1" runat="server"  ValidationGroup="Group"
                        ControlToValidate="ddlStudentType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divStudentType">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblStudentType" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                       <asp:DropDownList runat="server" ID="ddlStudentType" AutoPostBack="true" CssClass="form-control"
                            OnSelectedIndexChanged="ddlStudentType_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcStudentType" runat="server"  ValidationGroup="Group"
                        ControlToValidate="ddlStudentType" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divStudentOther" visible="false">
                     <div class="col-md-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtStudentOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcStudentOther" runat="server"
                        ControlToValidate="txtStudentOther" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divOrganization">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblOrganization" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList ID="ddlOrganization" runat="server" CssClass="form-control"
                            OnSelectedIndexChanged="ddlOrganization_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcOrg" runat="server"  ValidationGroup="Group"
                        ControlToValidate="ddlOrganization" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divOrgOther" visible="false">
                     <div class="col-md-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-md-7 col-xs-7">
                      <asp:TextBox ID="txtOrgOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcOrgOther" runat="server"
                        ControlToValidate="txtOrgOther" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divInstitution">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblInstitution" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList ID="ddlInstitution" runat="server" CssClass="form-control"
                            OnSelectedIndexChanged="ddlInstitution_SelectedIndexChanged" AutoPostBack="true">
                            <%--<asp:ListItem Value="0">Please Select</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcInsti" runat="server"  ValidationGroup="Group"
                        ControlToValidate="ddlInstitution" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divInstiOther" visible="false">
                     <div class="col-md-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtInstiOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcInstiOther" runat="server"
                        ControlToValidate="txtInstiOther" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDepartment">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblDepartment" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList runat="server" ID="ddlDepartment" AutoPostBack="true" CssClass="form-control"
                            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                            <%--<asp:ListItem Value="0">Please Select</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcDeptm" runat="server"  ValidationGroup="Group"
                        ControlToValidate="ddlDepartment" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDepartmentOther" visible="false">
                     <div class="col-md-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtDepartmentOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcDeptmOther" runat="server"
                        ControlToValidate="txtDepartmentOther" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress1">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblAddress1" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcAddress1" runat="server"
                        ControlToValidate="txtAddress1" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress2">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblAddress2" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcAddress2" runat="server"
                        ControlToValidate="txtAddress2" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress3">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblAddress3" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcAddress3" runat="server"
                        ControlToValidate="txtAddress3" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divRCountry">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblRCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                      <asp:DropDownList ID="ddlRCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlRCountry_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcRCountry" runat="server"  ValidationGroup="Group"
                            ControlToValidate="ddlRCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divCity">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblCity" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcCity" runat="server"
                        ControlToValidate="txtCity" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divOName">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblOName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtOName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcOName" runat="server"
                        ControlToValidate="txtOName" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divState">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblState" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcState" runat="server"
                        ControlToValidate="txtState" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divCountry">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcCountry" runat="server"  ValidationGroup="Group"
                            ControlToValidate="ddlCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divPostalcode">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblPostalcode" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtPostalcode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcPostalcode" runat="server"
                        ControlToValidate="txtPostalcode" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divTel">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblTel" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7" runat="server" >
                        <div class="col-xs-3" runat="server" id="divTelcc" style="padding-left:0px;" >
                            <asp:TextBox ID="txtTelcc" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTelcc" runat="server"
                                TargetControlID="txtTelcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-3" runat="server" id="divTelac" style="padding-left:0px;" >
                            <asp:TextBox ID="txtTelac" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTelac" runat="server"
                                TargetControlID="txtTelac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-6" runat="server" id="divTelNo" style="padding-right:0px;padding-left:0px;">
                            <asp:TextBox ID="txtTel" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTel" runat="server"
                                TargetControlID="txtTel"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                    </div>
                    <div class="col-md-1 col-xs-1">
                        <asp:RequiredFieldValidator ID="vcTel" runat="server"
                        ControlToValidate="txtTel" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1 col-xs-1">
                        <asp:RequiredFieldValidator ID="vcTelcc" runat="server"
                        ControlToValidate="txtTelcc" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1 col-xs-1">
                        <asp:RequiredFieldValidator ID="vcTelac" runat="server"
                        ControlToValidate="txtTelac" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divMobile">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblMobile" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7" runat="server" >
                        <div class="col-xs-3" runat="server" id="divMobcc" style="padding-left:0px;" >
                            <asp:TextBox ID="txtMobcc" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobcc" runat="server"
                                TargetControlID="txtMobcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-3" runat="server" id="divMobac" style="padding-left:0px;" >
                            <asp:TextBox ID="txtMobac" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobac" runat="server"
                                TargetControlID="txtMobac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-6" runat="server" id="divMobileNo" style="padding-right:0px;padding-left:0px;">
                            <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbMobile" runat="server"
                                TargetControlID="txtMobile"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        (Complete this field if you wish to receive your pre-registration confirmation QR Code on your mobile one week before the show)
                    </div>
                    <div class="col-md-1 col-xs-1">
                        <asp:RequiredFieldValidator ID="vcMob" runat="server"
                        ControlToValidate="txtMobile" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1 col-xs-1">
                        <asp:RequiredFieldValidator ID="vcMobcc" runat="server"
                        ControlToValidate="txtMobcc" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1 col-xs-1">
                        <asp:RequiredFieldValidator ID="vcMobac" runat="server"
                        ControlToValidate="txtMobac" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divFax">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblFax" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7" runat="server" >
                        <div class="col-xs-3" runat="server" id="divFaxcc" style="padding-left:0px;" >
                            <asp:TextBox ID="txtFaxcc" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFaxcc" runat="server"
                                TargetControlID="txtFaxcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-3" runat="server" id="divFaxac" style="padding-left:0px;" >
                            <asp:TextBox ID="txtFaxac" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFaxac" runat="server"
                                TargetControlID="txtFaxac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-6" runat="server" id="divFaxNo" style="padding-right:0px;padding-left:0px;">
                            <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFax" runat="server"
                                TargetControlID="txtFax"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                    </div>
                    <div class="col-md-1 col-xs-1">
                        <asp:RequiredFieldValidator ID="vcFax" runat="server"
                        ControlToValidate="txtFax" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1 col-xs-1">
                        <asp:RequiredFieldValidator ID="vcFaxcc" runat="server"
                        ControlToValidate="txtFaxcc" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1 col-xs-1">
                        <asp:RequiredFieldValidator ID="vcFaxac" runat="server"
                        ControlToValidate="txtFaxac" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divEmail">
                    <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                         (Please do not use personal email addresses such @yahoo.com, @hotmail.com, @gmail.com to avoid undelivered/blocked emails.)
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcEmail" runat="server"
                        ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="div1">
                     <div class="col-md-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:RegularExpressionValidator ID="validateEmail"
                        runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                        ControlToValidate="txtEmail"
                        ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divEmailConfirmation">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblEmailConfirmation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtEmailConfirmation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcEConfirm" runat="server"
                        ControlToValidate="txtEmailConfirmation" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="div2">
                     <div class="col-md-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:CompareValidator ID="cmpEmail" runat="server" ControlToValidate="txtEmailConfirmation"
                        ControlToCompare="txtEmail" ErrorMessage="Not match." ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress4">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblAddress4" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtAddress4" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcAddress4" runat="server"
                        ControlToValidate="txtAddress4" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAffiliation">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblAffiliation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList ID="ddlAffiliation" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcAffil" runat="server"  ValidationGroup="Group"
                            ControlToValidate="ddlAffiliation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDietary">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblDietary" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList ID="ddlDietary" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcDietary" runat="server"  ValidationGroup="Group"
                            ControlToValidate="ddlDietary" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divNationality">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblNationality" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtNationality" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcNation" runat="server"
                        ControlToValidate="txtNationality" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAge">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblAge" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtAge" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbAge" runat="server"
                            TargetControlID="txtAge"
                            FilterType="Numbers"
                            ValidChars="+0123456789" />
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcAge" runat="server"
                        ControlToValidate="txtAge" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divDOB">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblDOB" runat="server" CssClass="form-control-label" Text="Date of Birth"></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox><%-- onblur="validateDate(this);"--%>
                        <asp:CalendarExtender ID="calendarDOB" TargetControlID="txtDOB"
                                runat="server" PopupButtonID="txtDOB" Format="dd/MM/yyyy" ></asp:CalendarExtender>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcDOB" runat="server" Enabled="false"
                        ControlToValidate="txtDOB" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <%--<asp:RegularExpressionValidator ID="regexpName" runat="server"
                                ErrorMessage="This expression does not validate." 
                                ControlToValidate="txtDOB"
                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$" />--%>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divGender">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblGender" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                            <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <%--<asp:CompareValidator ID="vcGender" runat="server"  ValidationGroup="Group"
                            ControlToValidate="ddlGender" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>--%>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divMemberNo">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblMemberNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtMemberNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcMember" runat="server"
                        ControlToValidate="txtMemberNo" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>

            <div id="divVisitor" runat="server" class="form-group" visible="false">
                <div class="form-group row" runat="server" id="divVName">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblVName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtVName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcVName" runat="server"
                        ControlToValidate="txtVName" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVDOB">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblVDOB" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtVDOB" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcVDOB" runat="server"
                        ControlToValidate="txtVDOB" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVPassNo">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblVPassNo" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtVPassNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcVPassNo" runat="server"
                        ControlToValidate="txtVPassNo" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVPassIssueDate">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblVPassIssueDate" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtVPassIssueDate" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcVPassIssueDate" runat="server" Enabled="false"
                        ControlToValidate="txtVPassIssueDate" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVPassExpiry">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblVPassExpiry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtVPassExpiry" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcVPExpiry" runat="server"
                        ControlToValidate="txtVPassExpiry" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVEmbarkation">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblVEmbarkation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtVEmbarkation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcVEmbarkation" runat="server" Enabled="false"
                        ControlToValidate="txtVEmbarkation" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVArrivalDate">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblVArrivalDate" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtVArrivalDate" runat="server" CssClass="form-control" placeholder="dd/MM/yyyy"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcVArrivalDate" runat="server"
                        ControlToValidate="txtVArrivalDate" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divVCountry">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblVCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList ID="ddlVCountry" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcVCountry" runat="server"  ValidationGroup="Group"
                            ControlToValidate="ddlVCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>
            </div>

            <div id="divUDF" runat="server" class="form-group" visible="false">
                <div class="form-group row" runat="server" id="divUDFCName">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblUDFCName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtUDFCName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcUDFCName" runat="server"
                        ControlToValidate="txtUDFCName" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFDelType">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblUDFDelType" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtUDFDelType" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcUDFDelType" runat="server"
                        ControlToValidate="txtUDFDelType" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFProCategory">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblUDFProCategory" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList ID="ddlUDFProCategory" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUDFProCategory_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcUDFProCat" runat="server" ValidationGroup="Group"
                            ControlToValidate="ddlUDFProCategory" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>
                
                <div class="form-group row" runat="server" id="divUDFProCatOther" visible="false">
                     <div class="col-md-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtUDFProCatOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcProCatOther" runat="server"
                        ControlToValidate="txtUDFProCatOther" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCpostalcode">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblUDFCpostalcode" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtUDFCpostalcode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcUDFCpcode" runat="server"
                        ControlToValidate="txtUDFCpostalcode" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCLDept">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblUDFCLDept" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtUDFCLDept" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcUDFCLDept" runat="server"
                        ControlToValidate="txtUDFCLDept" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFAddress">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblUDFAddress" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtUDFAddress" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcUDFAddress" runat="server"
                        ControlToValidate="txtUDFAddress" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCLCom">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblUDFCLCom" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList ID="ddlUDFCLCom" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUDFCLCom_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcUDFCLCom" runat="server" 
                            ControlToValidate="ddlUDFCLCom" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divUDFCLComOther" visible="false">
                     <div class="col-md-3 col-xs-3">
                        &nbsp;
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtUDFCLComOther" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcUDFCLComOther" runat="server"
                        ControlToValidate="txtUDFCLComOther" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                 <div class="form-group row" runat="server" id="divUDFCCountry">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblUDFCCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:DropDownList ID="ddlUDFCCountry" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:CompareValidator ID="vcUDFCCountry" runat="server"  ValidationGroup="Group"
                            ControlToValidate="ddlUDFCCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>
            </div>

            <div id="divSuperVisor" runat="server" class="form-group" visible="false">
                <div class="form-group row" runat="server" id="divSupName">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblSupName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtSupName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcSupName" runat="server"
                        ControlToValidate="txtSupName" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divSupDesignation">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblSupDesignation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtSupDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcSupDes" runat="server"
                        ControlToValidate="txtSupDesignation" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divSupContact">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblSupContact" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtSupContact" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <asp:RequiredFieldValidator ID="vcSupContact" runat="server"
                        ControlToValidate="txtSupContact" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
                
                <div class="form-group row" runat="server" id="divSupEmail">
                     <div class="col-md-3 col-xs-3">
                        <asp:Label ID="lblSupEmail" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-7 col-xs-7">
                        <asp:TextBox ID="txtSupEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                     <div class="col-md-3 col-xs-3">
                        <asp:RequiredFieldValidator ID="vcSupEmail" runat="server"
                        ControlToValidate="txtSupEmail" Display="Dynamic" ValidationGroup="Group"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="validateSupEmail"
                        runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                        ControlToValidate="txtSupEmail"
                        ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>
            </div>

            <div id="divCaptcha" runat="server">
                <div  class="form-group row">
                    <div class="clear"></div>
                     <div class="col-md-3 col-xs-3">&nbsp;</div>
                    <div class="col-md-8 col-xs-8">
                        <telerik:RadCaptcha ID="RadCaptcha1" runat="server" ErrorMessage="Page not valid. The code you entered is not valid."
                            ValidationGroup="Group" ForeColor="Red" >
                            <CaptchaImage ImageCssClass="imageClass" />
                        </telerik:RadCaptcha>
                        <br />
                        <br />
                    </div>
                </div>
            </div>

            <div id="divFTR1" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblFTR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divFTR2" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Label ID="lblFTR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divFTRCHK" runat="server" visible="false">
                <div  class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-12 col-xs-12">
                        <asp:CheckBoxList ID="chkFTRCHK" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                        <%--<asp:CheckBox ID="chkFTRCHK" runat="server" />&nbsp;&nbsp;<asp:Label ID="lblFTRCHK" runat="server"></asp:Label>
                        <asp:Label ID="lblFTRCHKIsSkip" runat="server" Visible="false" Text="0"></asp:Label>--%>
                        <br />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-group container" style="padding-top:20px;">
                  <asp:Panel runat="server" ID="PanelShowBackButton" Visible="false" >
                      <div class="col-lg-2 col-xs-2 col-sm-offset-4 col-sm-2 center-block" >
                        <asp:Button runat="server" ID="btnBack" CssClass="btn MainButton btn-block" Text="Cancel" OnClick="btnBack_Click" CausesValidation="false"/>
                    </div>
                      <div class="col-lg-2 col-sm-2 col-xs-2 center-block" >
                        <asp:Button runat="server" ID="btnSave2" CssClass="btn MainButton btn-block" Text="Next" OnClick="btnSave_Click" />
                    </div>
                  </asp:Panel>
                     <asp:Panel runat="server" ID="PanelWithoutBack" Visible="true" >
                    <div class="col-lg-offset-5 col-lg-3 col-xs-3 col-sm-offset-4 col-sm-3 center-block" >
                        <asp:Button runat="server" ID="btnSave" CssClass="btn MainButton btn-block" Text="Next" OnClick="btnSave_Click" ValidationGroup="Group" />
                    </div>
                   </asp:Panel>
                </div>
            </div>

            <asp:HiddenField ID="hfRegno" runat="server" />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

