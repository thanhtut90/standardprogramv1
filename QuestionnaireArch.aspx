﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="QuestionnaireArch.aspx.cs" Inherits="QuestionnaireArch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="./Content/Default/bootstrap.css" rel="stylesheet"/>
    <link href="./Content/Default/Site.css" rel="stylesheet" />
    <script src="./Scripts/jquery-1.10.2.js"></script>
    <script src="./Content/bootstrap/js/bootstrap.min.js"></script>
    <style>
        .question {
            margin-top: 2%;
        }

        .answer {
            margin-top: 1%;
        }

        .answer, .question {
            margin-bottom: 1%;
        }

        .numbering {
            margin-right: 5px;
            vertical-align: top;
            position: relative;
            top: -1px;
            *overflow: hidden;
        }
        label {
            padding-top: 0;
            padding-bottom: 0;
        }
        input[type="checkbox"] {
            width: 13px;
            height: 13px;
            padding: 0;
            margin: 0;
            vertical-align: top;
            position: relative;
            top: 1px;
            *overflow: hidden;
        }
    </style>

     <script type="text/javascript">
        function checkboxToggle(parentID, curChk) {
            var criteria = "#" + parentID;
            var ischecked = $(curChk).attr('checked')
            $(criteria).find('input:checkbox').each(function () {
                $(this).attr('checked', false); //prop('checked', $(this).prop("checked"));
            });
            if (ischecked)
                $(curChk).attr('checked', true);
            return true;
        }

        function OnClickEvent()
        {
            $(document).ready(function () {
                $('input[type=checkbox]').click(function () {
                    var groupName = $(this).attr('groupname');

                    if (!groupName)
                        return;

                    var checked = $(this).is(':checked');

                    $("input[groupname='" + groupName + "']:checked").each(function () {
                        $(this).prop('checked', '');
                    });

                    if (groupName == "qn1ans") {
                        $("input[groupname*='subans']:checked").each(function () {
                            $(this).prop('checked', '');
                        });
                    }

                    if (groupName.indexOf("subans") >= 0)
                    {
                        var parentId = groupName.split("subans")[1];
                        var questId = $(this).attr('id').replace("ContentPlaceHolder1_", "").split("A")[0];
                        var parentControlId = "ContentPlaceHolder1_" + questId + "A" + parentId;
                        
                        $("input[groupname='qn1ans']:checked").each(function () {
                            $(this).prop('checked', '');
                        });

                        //$('#' + parentControlId).prop("checked", "checked");
                    }

                    if (checked)
                        $(this).prop('checked', 'checked');
                });
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <script type="text/javascript">Sys.Application.add_load(OnClickEvent);</script>
            <asp:Label runat="server" ID="lblCurrentSelectedAnswers" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="lblCurrentQn" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="lblQAId" Visible="false"></asp:Label>
            <asp:Label runat="server" ID="lblIsLoad" Visible="false" Text="0"></asp:Label>
            <div runat="server" id="divQuestionnaire" class="container col-md-offset-1">
                <div runat="server" id="divQn1" class="question" visible="false">
                    <asp:Label runat="server" ID="lblQn1" Font-Bold="true"></asp:Label>
                    <asp:Panel runat="server" ID="divAnswers1"></asp:Panel>
                    <asp:Label runat="server" ID="lblAnswers1ErrorMessage" ForeColor="Red" Text="* Required" Visible="false"></asp:Label>
                </div>
                <div runat="server" id="divQn2" class="question" visible="false">
                    <asp:Label runat="server" ID="lblQn2" Font-Bold="true"></asp:Label>
                    <asp:Panel runat="server" ID="divAnswers2"></asp:Panel>
                    <asp:Label runat="server" ID="lblAnswers2ErrorMessage" ForeColor="Red" Text="* Required" Visible="false"></asp:Label>
                </div>
                <div runat="server" id="divQn3" class="question" visible="false">
                    <asp:Label runat="server" ID="lblQn3" Font-Bold="true"></asp:Label>
                    <asp:Panel runat="server" ID="divAnswers3">
                        <div runat="server" id="divCol1" class="col-md-4"></div>
                        <div runat="server" id="divCol2" class="col-md-4"></div>
                        <div runat="server" id="divCol3" class="col-md-4"></div>
                    </asp:Panel>
                    <asp:Label runat="server" ID="lblAnswers3ErrorMessage" ForeColor="Red" Text="* Required" Visible="false"></asp:Label>
                </div>
                <div runat="server" id="divQn4" class="question" visible="false">
                    <asp:Label runat="server" ID="lblQn4" Font-Bold="true"></asp:Label>
                    <asp:Panel runat="server" ID="divAnswers4"></asp:Panel>
                    <asp:Label runat="server" ID="lblAnswers4ErrorMessage" ForeColor="Red" Text="* Required" Visible="false"></asp:Label>
                </div>
                <div runat="server" id="divQn5" class="question" visible="false">
                    <asp:Label runat="server" ID="lblQn5" Font-Bold="true"></asp:Label>
                    <asp:Panel runat="server" ID="divAnswers5"></asp:Panel>
                    <asp:Label runat="server" ID="lblAnswers5ErrorMessage" ForeColor="Red" Text="* Required" Visible="false"></asp:Label>
                </div>
                <div runat="server" id="divQn6" class="question" visible="false">
                    <asp:Label runat="server" ID="lblQn6" Font-Bold="true"></asp:Label>
                    <asp:Panel runat="server" ID="divAnswers6"></asp:Panel>
                    <asp:Label runat="server" ID="lblAnswers6ErrorMessage" ForeColor="Red" Text="* Required" Visible="false"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="row">
                    <asp:Label runat="server" ID="lblVcMsg" Visible="false"></asp:Label>
                    <div class="col-lg-offset-2 col-lg-3 col-sm-3 center-block">
                        <asp:Button runat="server" ID="btnBack" CssClass="btn btn-block MainButton" OnClick="btnBack_Click" Text="Back" Style="margin-top: 10px; margin-bottom: 10px" />
                    </div>
                    <div class="col-lg-3  col-sm-3 center-block">
                        <asp:Button runat="server" ID="btnNext" CssClass="btn btn-block MainButton" OnClick="btnNext_Click" Text="Next" Style="margin-top: 10px; margin-bottom: 10px" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

