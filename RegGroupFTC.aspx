﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="RegGroupFTC.aspx.cs" Inherits="RegGroupFTC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="Content/StyleQA.css" />
    <style>
        .text-4 {
            color: #ff0000;
        }

        .space-top20 {
            padding-top: 25px;
            border-top: 1px dashed;
 
        }

        .col-sm-6 {
            padding-top: 7px;
        }

        .form-control {
            height: 35px !important;
        }

        .paddingLeft3px {
            padding-left: 3px;
        }

        .PaddingRight1 {
            padding-right: 1px;
        }

        .PaddingLeft1 {
            padding-left: 3px;
        }

        .textUpperCase {
            text-transform: uppercase;
        }
        
        .chkFooter label
        {
            font-weight:unset !important;
            padding-left:5px !important;
            vertical-align:top !important;
        }
        .chkFooter input[type="checkbox"]
        {
            margin-top: 2px !important;
        }

        .memberHeadStyle{
            padding-left:0px !important;
        }
    </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    
<script>
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".in").each(function () {
            //alert(1);
            $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            //alert(2);
        	$(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function () {
            //alert(3);
        	$(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".in").on('hide.bs.collapse', function () {
            //alert(4);
            $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        }).on('show.bs.collapse', function () {
            //alert(5);
            $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        });
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <script>
                function RestrictSpace() {
                    if (event.keyCode == 32) {
                        return false;
                    }
                }
            </script>
            <div id="divHDR1" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblHDR1" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR2" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-11" style="padding-left: 50px;">
                        <asp:Label ID="lblHDR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <div id="divHDR3" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10 col-md-offset-1">
                        <asp:Label ID="lblHDR3" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>

            <div class="container" style="padding-top: 30px; padding-left: 6px;">
                <div class="col-md-12">
                    <p style="font-weight: bold; text-decoration: underline;">TEAM MEMBER #1 - MAIN CHILD (ELDER CHILD)</p>
                    <asp:TextBox ID="txtRegno1" runat="server" Visible="false" Text=""></asp:TextBox>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="form-group col-md-8">
                            <div>
                                <label class="control-label">Name as in Birth Certificate / NRIC (English)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt1FName" runat="server" CssClass=" form-control input-sm css1FName" placeholder=""></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="rfv1FName" runat="server"
                                ControlToValidate="txt1FName" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rev1FName" runat="server" ControlToValidate="txt1FName" ErrorMessage="Only English input is allowed."
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-8">
                            <div>
                                <label class="control-label">姓名 (中文)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt1LName" runat="server" CssClass=" form-control input-sm css1LName" placeholder=""></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="rfv1LName" runat="server"
                                ControlToValidate="txt1LName" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row" style="display:none;">
                        <div class="form-group col-md-8">
                            <div>
                                <label class="control-label">Email 电邮<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt1Email" runat="server" CssClass=" form-control input-sm" placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>

                                <i>Please input email for Team Member #2, who will be designated as key contact person for this team registration and competition.</i>
                            </div>
                            <asp:RequiredFieldValidator ID="rfv1Email" runat="server" Enabled="false"
                                ControlToValidate="txt1Email" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rev1Email" runat="server" ErrorMessage="Invalid Email" 
                                ForeColor="Red" ControlToValidate="txt1Email" ValidationExpression="^\s*.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+\s*$" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-8">
                            <div>
                                <label class="control-label">School Level 学级<span class="text-4">*</span></label>
                                <asp:DropDownList ID="ddl1Sal" runat="server" CssClass=" form-control input-sm" 
                                    OnSelectedIndexChanged="ddl1Sal_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0">Please Select</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:CompareValidator ID="vc1Sal" runat="server"
                                ControlToValidate="ddl1Sal" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                            <div runat="server" id="divSal1Other" visible="false" style="padding-top: 7px;">
                                <br />
                                <asp:TextBox ID="txt1SalOther" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfv1SalOther" runat="server"
                                    ControlToValidate="txt1SalOther" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="rev1SalOther" runat="server" ControlToValidate="txt1SalOther" ErrorMessage="Only English input is allowed."
                                    ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-8">
                            <div>
                                <label class="control-label">School 学校<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt1PassNo" runat="server" CssClass=" form-control input-sm" placeholder="" ></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="rfv1PassNo" runat="server"
                                ControlToValidate="txt1PassNo" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rev1PassNo" runat="server" ControlToValidate="txt1PassNo" ErrorMessage="Only English input is allowed."
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                </div>
                <%-- End Fist --%>
            </div>

            <!-- 2  -->
            <asp:Panel runat="server" ID="Panel2" Visible="true">
                <div class="container space-top20">
                    <div class="col-sm-12 col-md-12 memberHeadStyle">
                        <p style="font-weight: bold; text-decoration: underline;">TEAM MEMBER #2 - PARENT / GRANDPARENT / KEY CONTACT PERSON</p>
                        <asp:TextBox ID="txtRegno2" runat="server" Visible="false" Text=""></asp:TextBox>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label">Name as in Birth Certificate / NRIC (English)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt2FName" runat="server" CssClass=" form-control input-sm cssFName" placeholder=""></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="rfv2FName" runat="server"
                                ControlToValidate="txt2FName" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rev2FName" runat="server" ControlToValidate="txt2FName" ErrorMessage="Only English input is allowed."
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label">姓名 (中文)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt2LName" runat="server" CssClass=" form-control input-sm cssLName" placeholder=""></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="rfv2LName" runat="server"
                                ControlToValidate="txt2LName" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label">Relationship to the Main Child Participant 家长/祖父母与主要参赛孩童的亲属关系<span class="text-4">*</span></label>
                                <asp:DropDownList runat="server" ID="ddl2Profession" CssClass="form-control">
                                    <asp:ListItem Value="0">Please Select</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:CompareValidator ID="vc2Profession" runat="server" 
                            ControlToValidate="ddl2Profession" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>
                    <div class="row">
                        <%--<div class="col-sm-12 col-md-6" style="display:none;">
                            <div class="form-group">
                                <label class="control-label">Have you participated in this competition before? 您是否有参加过这项比赛？<span class="text-4">*</span></label>
                                <asp:DropDownList runat="server" ID="ddl2Affiliation" CssClass="form-control">
                                    <asp:ListItem Value="0">Please Select</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:CompareValidator ID="vc2Affiliation" runat="server" Enabled="false"
                            ControlToValidate="ddl2Affiliation" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>--%>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label">Email 电邮<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt2Email" runat="server" CssClass=" form-control input-sm" placeholder="" onkeypress="return RestrictSpace()" ></asp:TextBox>

                                <i>Please input email for Team Member #2, who will be designated as key contact person for this team registration and competition.</i>
                            </div>
                            <asp:RequiredFieldValidator ID="rfv2Email" runat="server" ControlToValidate="txt2Email" Display="Dynamic" 
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rev2Email" runat="server" 
                                ErrorMessage="Invalid Email" ForeColor="Red" ControlToValidate="txt2Email" ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <div class="row" style="padding-left: 20px;">
                                    <label class="control-label">Mobile Number 电话号码</label>
                                </div>
                                <div class="form-group col-md-4 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div1">
                                    <asp:TextBox ID="txt2MobileCC" runat="server" CssClass=" form-control input-sm" placeholder="Country Code"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" TargetControlID="txt2MobileCC" FilterType="Numbers" ValidChars="+0123456789" />
                                </div>
                                <div class="form-group col-md-8 col-xs-3 PaddingRight1 PaddingLeft1" runat="server" id="div2">
                                    <asp:TextBox ID="txt2Mobile" runat="server" CssClass=" form-control input-sm" placeholder=""></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txt2Mobile" FilterType="Numbers" ValidChars="+0123456789" />
                                </div>
                                <asp:RequiredFieldValidator ID="rfv2Mobile" runat="server"
                                    ControlToValidate="txt2Mobile" Display="Dynamic"
                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <%--<div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label">Declaration made by Name (NRIC)<span class="text-4">*</span></label>
                                <asp:TextBox ID="txt2Address1" runat="server" CssClass=" form-control input-sm" placeholder=""></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="rfv2Address1" runat="server"
                                ControlToValidate="txt2Address1" Display="Dynamic"
                                ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rev2Address1" runat="server" ControlToValidate="txt2Address1" ErrorMessage="Only English input is allowed."
                                ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label">Relationship to Main Child Participant<span class="text-4">*</span></label>
                                <asp:DropDownList runat="server" ID="ddl2Dietary" CssClass="form-control">
                                    <asp:ListItem Value="0">Please Select</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:CompareValidator ID="cv2Dietary" runat="server" 
                            ControlToValidate="ddl2Dietary" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                        </div>
                    </div>--%>
                </div>
            </asp:Panel>
            <!-- End -->

            <div class="bs-example space-top20" style="padding-top:0px;">
                <div class="accordion" id="accordionExample">
                    <!-- 3  -->
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button type="button" class='<%= btn3 %>' data-toggle="collapse" data-target="#ContentPlaceHolder1_collapseOne"><i class="fa fa-plus"></i> TEAM MEMBER #3</button>
                            </h2>
                        </div>
                        <div id="collapseOne" runat="server" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <%--<asp:Panel runat="server" ID="Panel3" Visible="true">--%>
                                    <div class="container">
                                        <div class="col-sm-12 col-md-12 memberHeadStyle">
                                            <%--<p style="font-weight: bold; text-decoration: underline;">TEAM MEMBER #3</p>--%>
                                            <asp:TextBox ID="txtRegno3" runat="server" Visible="false" Text=""></asp:TextBox>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Name as in Birth Certificate / NRIC (English)</label>
                                                    <asp:TextBox ID="txt3FName" runat="server" CssClass=" form-control input-sm cssFName" placeholder=""
                                                        AutoPostBack="true" OnTextChanged="Member34_TextChanged"></asp:TextBox>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfv3FName" runat="server" ControlToValidate="txt3FName" Display="Dynamic" 
                                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="rev3FName" runat="server" ControlToValidate="txt3FName" 
                                                    ErrorMessage="Only English input is allowed." ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">姓名 (中文)</label>
                                                    <asp:TextBox ID="txt3LName" runat="server" CssClass=" form-control input-sm cssLName" placeholder=""
                                                        AutoPostBack="true" OnTextChanged="Member34_TextChanged"></asp:TextBox>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfv3LName" runat="server" ControlToValidate="txt3LName" Display="Dynamic" 
                                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Relationship to the Main Child Participant 家长/祖父母与主要参赛孩童的亲属关系</label>
                                                    <asp:DropDownList runat="server" ID="ddl3Profession" CssClass="form-control"
                                                        AutoPostBack="true" OnSelectedIndexChanged="Member34_TextChanged">
                                                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:CompareValidator ID="vc3Profession" runat="server" 
                                                ControlToValidate="ddl3Profession" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                            </div>
                                            <div class="col-sm-12 col-md-6">&nbsp;</div>
                                        </div>
                                    </div>
                                <%--</asp:Panel>--%>
                            </div>
                        </div>
                    </div>
                    <!-- End -->
                    <!-- 4  -->
                    <div class="card space-top20">
                        <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                                <button type="button" class='<%= btn4 %>' data-toggle="collapse" data-target="#ContentPlaceHolder1_collapseTwo"><i class="fa fa-plus"></i> TEAM MEMBER #4</button>
                            </h2>
                        </div>
                        <div id="collapseTwo" runat="server" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                <%--<asp:Panel runat="server" ID="Panel4" Visible="true">--%>
                                    <div class="container">
                                        <div class="col-sm-12 col-md-12 memberHeadStyle">
                                            <%--<p style="font-weight: bold; text-decoration: underline;">TEAM MEMBER #4</p>--%>
                                            <asp:TextBox ID="txtRegno4" runat="server" Visible="false" Text=""></asp:TextBox>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Name as in Birth Certificate / NRIC (English)</label>
                                                    <asp:TextBox ID="txt4FName" runat="server" CssClass=" form-control input-sm cssFName" placeholder=""
                                                        AutoPostBack="true" OnTextChanged="Member34_TextChanged"></asp:TextBox>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfv4FName" runat="server" ControlToValidate="txt4FName" Display="Dynamic" 
                                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="rev4FName" runat="server" ControlToValidate="txt4FName" 
                                                    ErrorMessage="Only English input is allowed." ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">姓名 (中文)</label>
                                                    <asp:TextBox ID="txt4LName" runat="server" CssClass=" form-control input-sm cssLName" placeholder=""
                                                        AutoPostBack="true" OnTextChanged="Member34_TextChanged"></asp:TextBox>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfv4LName" runat="server" ControlToValidate="txt4LName" Display="Dynamic" 
                                                    ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Relationship to the Main Child Participant 家长/祖父母与主要参赛孩童的亲属关系</label>
                                                    <asp:DropDownList runat="server" ID="ddl4Profession" CssClass="form-control"
                                                        AutoPostBack="true" OnSelectedIndexChanged="Member34_TextChanged">
                                                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:CompareValidator ID="vc4Profession" runat="server" 
                                                ControlToValidate="ddl4Profession" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                                                ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                                            </div>
                                        </div>
                                    </div>
                                <%--</asp:Panel>--%>
                            </div>
                        </div>
                    </div>
                    <!-- End -->
                </div>
            </div>

            <!-- QA  -->
            <div id="divQA" runat="server" class="container space-top20" style="padding-top: 20px;padding-left: 20px;">
                <asp:Panel ID="PanelMain" CssClass="flex-grid" runat="server"></asp:Panel>
            </div> 
            <!-- End -->
 
            <div class="container ">
                <div class="col-md-12">
                    <div class="row" id="divFTR1" runat="server" visible="false">
                        <div class="form-group row">
                            <div class="clear"></div>
                            <div class="col-md-12">
                                <asp:Label ID="lblFTR1" runat="server"></asp:Label>
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="row" id="divFTRCHK" runat="server" visible="false">
                        <div class="form-group row">
                            <div class="clear"></div>
                            <div class="col-md-12" style="padding-left: 30px;">
                                <asp:CheckBoxList ID="chkFTRCHK" runat="server" CssClass="chkFooter"></asp:CheckBoxList>
                                <%--<asp:CheckBox ID="chkFTRCHK" runat="server" />&nbsp;&nbsp;<asp:Label ID="lblFTRCHK" runat="server"></asp:Label>
                            <asp:Label ID="lblFTRCHKIsSkip" runat="server" Visible="false" Text="0"></asp:Label>--%>
                                <asp:Label ID="lblErrFTRCHK" runat="server" Visible="false" Text="* Required" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="divFTR2" runat="server" visible="false">
                <div class="form-group row">
                    <div class="clear"></div>
                    <div class="col-md-10">
                        <asp:Label ID="lblFTR2" runat="server"></asp:Label>
                        <br />
                        <br />
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div class="form-group container" style="padding-top: 30px; padding-bottom: 20px;">
        <asp:Panel runat="server" ID="PanelWithoutBack" Visible="true">
            <div class="col-lg-offset-4 col-lg-3 col-sm-offset-4 col-sm-3 center-block">
                <asp:Button runat="server" ID="btnSave" CssClass="btn MainButton btn-block" Text="Submit"   OnClick="btnSave_Click" />
            </div>
        </asp:Panel>
    </div>
     <asp:Panel runat="server" ID="Panel1" Visible="false">
        </asp:Panel>
</asp:Content>

