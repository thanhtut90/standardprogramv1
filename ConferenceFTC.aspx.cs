﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using System.Data.Sql;
using System.Data;
using Corpit.Utilities;
using Corpit.Registration;
using Telerik.Web.UI;
using Corpit.Logging;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class ConferenceFTC : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cComFuz = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    string conf_Select_CSS = "btn btn-success btn-clean  btn-block";
    string conf_Remove_CSS = "btn btn-danger btn-clean  btn-block";
    private static List<AccompanyingPersonItem> lstPendingAcc;

    private static string[] checkingvnuShowName = new string[] { "VNU_AGRI", "VNU_HORTI" };
    private static string fjPublicConfDescHeader = "General Public Admission";
    private static string fjPublicFlowName = "FoodJapanPublic";
    private static string[] checkingOSEAShowName = new string[] { "OSEA 2018" };
    private static string ECGFlowName = "ecgsymposium2019";
    private static string[] checkingButtonCenterShowName = new string[] { "TRANSPORTA 2019" };
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            if (!string.IsNullOrEmpty(showid))
            {
                RegGroupObj grpObj = new RegGroupObj(fn);
                DataTable dt = grpObj.getRegGroupByID(groupid, showid);
                if (dt.Rows.Count > 0)
                {
                    bindFlowNote(showid, urlQuery);//***added on 12-10-2018

                    SiteSettings sSetting = new SiteSettings(fn, showid);
                    txtCurrency.Text = sSetting.SiteCurrency;
                    LoadConference();

                    string currentstage = cComFuz.DecryptValue(urlQuery.CurrIndex);
                    string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
                    string regno = cComFuz.DecryptValue(urlQuery.DelegateID);
                    checkExistAdditionalItems(groupid, regno, flowid, showid, currentstage);//*

                    bindConferenceDescHeader(flowid);

                    bindNextButtonAlignment(showid);//***added on 23-5-2019 according to client request

                    OrderControler oControl = new OrderControler(fn);
                    #region pending order
                    OrderItemList oList = new OrderItemList();
                    oList = oControl.GetPendingOrderList(urlQuery);
                    SetSelectedConfItemList(oList);
                    #endregion
                    #region paid order
                    List<OrderItemList> oSucList = getPaidOrder(urlQuery);
                    if (oSucList.Count > 0)
                    {
                        foreach (OrderItemList ordList in oSucList)
                        {
                            SetSelectedConfItemList(ordList, true);
                        }
                    }
                    #endregion

                    CalculateAndShowTotalSummary();

                    updateCurStep(urlQuery);//***edit on 23-3-2018

                    insertLogFlowAction(cComFuz.DecryptValue(urlQuery.GoupRegID), cComFuz.DecryptValue(urlQuery.DelegateID), rlgobj.actview, urlQuery);

                    if (Request.Params["t"] != null)
                    {
                        string admintype = cComFuz.DecryptValue(Request.QueryString["t"].ToString());
                        if (admintype == BackendStaticValueClass.isAdmin)
                        {
                            btnNext.Visible = false;
                            btnNext.Enabled = false;
                        }
                        else
                        {
                            btnNext.Visible = true;
                            btnNext.Enabled = true;
                        }
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region bindConferenceDescHeader
    private void bindConferenceDescHeader(string flowid)
    {
        FlowControler Flw = new FlowControler(fn);
        FlowMaster fMaster = Flw.GetFlowMasterConfig(flowid);
        if (fMaster.FlowName == fjPublicFlowName)
        {
            lblConfItemHeader.Text = fjPublicConfDescHeader;
        }
        if(fMaster.FlowName == ECGFlowName)
        {
            divECGRemark.Visible = true;
        }
    }
    #endregion

    #region bindNextButtonAlignment
    private void bindNextButtonAlignment(string showid)
    {
        ShowControler shwCtr = new ShowControler(fn);
        Show shw = shwCtr.GetShow(showid);
        if (checkingButtonCenterShowName.Contains(shw.SHW_Name))
        {
            divNext.Attributes.Remove("class");
            divNext.Attributes.Add("class", "col-lg-3 col-md-offset-5");
        }
    }
    #endregion

    #region ServerSidefunz
    protected void RptMainConfList_DataBond(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string regno = cComFuz.DecryptValue(urlQuery.DelegateID);
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
            string flwID = cComFuz.DecryptValue(urlQuery.FlowID);

            Label Description = (Label)e.Item.FindControl("lblDisplay");
            Label Display = (Label)e.Item.FindControl("ConItemDisplayPrice");
            TextBox txtPrice = (TextBox)e.Item.FindControl("txtConfPrice");
            TextBox txtConfItemID = (TextBox)e.Item.FindControl("txtConfItemID");
            RadButton btnSelect = (RadButton)e.Item.FindControl("chkConfItem");
            DropDownList ddlQty = (DropDownList)e.Item.FindControl("ddlQty");
            Panel pnlShowSelect = (Panel)e.Item.FindControl("PanelShowSelect");
            Panel pnlMsg = (Panel)e.Item.FindControl("PanleMsg");
            Label lblMsg = (Label)e.Item.FindControl("lblMsg");
            Image imgLogo = (Image)e.Item.FindControl("imgLogo");

            Label lblDescription = (Label)e.Item.FindControl("lblDescription");//*

            string conItem_ID = txtConfItemID.Text;
            if (!string.IsNullOrEmpty(conItem_ID))
            {
                ConferenceControler cControl = new ConferenceControler(fn);
                ConferenceItem cItem = cControl.GetConferenceItemByID(conItem_ID, showid);
                if (!string.IsNullOrEmpty(cItem.ConfID))
                {
                    Description.Text = cItem.ConDisplayText;
                    lblDescription.Text = cItem.ConfItemDesc;//*

                    string imgUrl = cItem.ConfImage;
                    imgLogo.ImageUrl = imgUrl.Replace("../","");
                    if (File.Exists(Server.MapPath(imgUrl.Replace("..","~"))))
                    {
                        imgLogo.Visible = true;
                    }
                    else
                    {
                        imgLogo.Visible = false;
                    }

                    string price = cControl.GetConfItemPrice("", cItem, showid);
                    txtPrice.Text = price;
                    Display.Text = txtCurrency.Text + " " + cComFuz.FormatCurrency(price);

                    FlowControler fCon = new FlowControler(fn);
                    FlowMaster flwMaster = fCon.GetFlowMasterConfig(flwID);
                    if (flwMaster.FlowConfSelectionType == Number.One) // Single selet (1)
                    {
                        ddlQty.Visible = false;
                        btnSelect.Visible = true;
                    }
                    else if(flwMaster.FlowConfSelectionType == Number.Three)//***added by th (13-7-2018)
                    {
                        //allow qty key in
                        ddlQty.Visible = true;
                        btnSelect.Visible = false;
                        string confType = cItem.con_Type;
                        ddlQty.Items.Clear();
                        ddlQty.ClearSelection();
                        ddlQty.Items.Insert(0, new ListItem("0", "0"));
                        int maxBuyableTimeMainConf = cItem.maxBuyableTime;
                        for (int i = 1; i <= maxBuyableTimeMainConf; i++)
                        {
                            ddlQty.Items.Add(i.ToString());
                            ddlQty.Items[i].Value = i.ToString();
                        }
                    }//***added by th (13-7-2018)
                    else//flwMaster.FlowConfSelectionType == Number.Two
                    {
                        //*Comment by th (29-3-2018)
                        ////allow qty key in
                        //ddlQty.Visible = true;
                        //btnSelect.Visible = false;
                        //*edit by th (29-3-2018)
                        ddlQty.Visible = false;
                        btnSelect.Visible = true;
                    }

                    /////***check buyable time to visible/enabled(edit on 16-3-2018)
                    OrderControler oControl = new OrderControler(fn);
                    int maxBuyableTime = cItem.maxBuyableTime;
                    InvoiceControler invCtrl = new InvoiceControler(fn);
                    string invoiceOwnerID = invCtrl.DefineInvoiceOwnerID(flwID, groupid, regno);
                    int paidOrderQty = oControl.getPaidOrderCountByItemIDInvOwnerID(ConfDefaultValue.conf_MainItem, conItem_ID, invoiceOwnerID, showid, flwID);
                    if (paidOrderQty >= maxBuyableTime)
                    {
                        ddlQty.Enabled = false;//***
                        btnSelect.Enabled = false;//***
                    }
                    /////***check buyable time to visible/enabled(edit on 16-3-2018)

                    if (flwMaster.FlowConfSelectionType == Number.One) // Single selet (1)
                    {
                        OrderItemList oPaidOrderList = oControl.getPaidOrderCountByInvOwnerID(ConfDefaultValue.conf_MainItem, invoiceOwnerID, showid, flwID);
                        if(oPaidOrderList.OrderList.Count > 0)
                        {
                            ddlQty.Enabled = false;//***
                            btnSelect.Enabled = false;//***
                        }
                    }

                    //Check Reach Max Usage(Check according to ordered items from tb_OrderDetials table)
                    int maxUsageItem = cComFuz.ParseInt(cItem.AllowdQty);
                    int usedItem = oControl.getAllOrderCountByItemID(conItem_ID, ConfDefaultValue.conf_MainItem);//cComFuz.ParseInt(cItem.UsedQty);
                    if (usedItem >= maxUsageItem)
                    {
                        pnlShowSelect.Visible = false;
                        pnlMsg.Visible = true;
                        lblMsg.Text = HttpUtility.HtmlDecode(cItem.MaxMessge);
                    }
                    else
                    {
                        pnlShowSelect.Visible = true;
                        pnlMsg.Visible = false;
                        lblMsg.Text = "";
                    }
                }
            }
        }
    }

    protected void btnConfItem_Onclick(object sender, EventArgs e)
    {
        RadButton btnSender = (RadButton)sender;
        string CofID = btnSender.CommandArgument.ToString();
        bool isCheck = btnSender.Checked;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);

        ConferenceControler cControl = new ConferenceControler(fn);
        ConferenceItem cItem = cControl.GetConferenceItemByID(CofID, showid);
        bool isSelectedMainItem = ToggleSelectedConfItem(CofID, cItem.disabledItems, isCheck, false, ref RptMainConfList);
        decimal mainTotal = 0;
        OrderItemList oList = GetSelectedItems(ConfDefaultValue.conf_MainItem, ref RptMainConfList, ref mainTotal);
        string selectedConfItems = string.Empty;
        int isSelectedMainItemCount = 0;
        if (oList.OrderList.Count > 0)
        {
            foreach (OrderItem oItem in oList.OrderList)
            {
                isSelectedMainItemCount++;
                selectedConfItems += oItem.ItemID + ",";
            }

            //***add on 22-3-2018
            List<OrderItemList> oSucList = getPaidOrder(urlQuery);
            if (oSucList.Count > 0)
            {
                foreach (OrderItemList ordList in oSucList)
                {
                    if (ordList.OrderList.Count > 0)
                    {
                        foreach (OrderItem oItem in ordList.OrderList)
                        {
                            isSelectedMainItemCount++;
                            selectedConfItems += oItem.ItemID + ",";
                        }
                    }
                }
            }
            //***add on 22-3-2018
        }
        else//***add on 22-3-2018
        {
            List<OrderItemList> oSucList = getPaidOrder(urlQuery);
            if (oSucList.Count > 0)
            {
                foreach (OrderItemList ordList in oSucList)
                {
                    if (ordList.OrderList.Count > 0)
                    {
                        foreach (OrderItem oItem in ordList.OrderList)
                        {
                            isSelectedMainItemCount++;
                            selectedConfItems += oItem.ItemID + ",";
                        }
                    }
                }
            }
        }//***add on 22-3-2018
        selectedConfItems = selectedConfItems.TrimEnd(',');

        mainTotal = 0;
        OrderItemList oDList = GetSelectedItems(ConfDefaultValue.conf_DependentItem, ref RptDependentList, ref mainTotal);
        LoadDependentItems(selectedConfItems, isSelectedMainItemCount > 0 ? true : false);

        if(oDList!=null)
        {
            foreach(OrderItem oItem in oDList.OrderList)
            {
                ////***add on 29-3-2018
                DependentItem cDItem = cControl.GetDependentItemByID(oItem.ItemID, showid);
                bool isSelectedDItem = ToggleSelectedConfItem(oItem.ItemID, cDItem.disabledItems, true, true, ref RptDependentList, oItem.Qty);
                ////***add on 29-3-2018
                SetSelectedDConfItem(oItem.ItemID, oItem.Qty.ToString(), ref RptDependentList);
            }
        }

        CalculateAndShowTotalSummary();
    }

    protected void btnDConfItem_Onclick(object sender, EventArgs e)
    {
        RadButton btnSender = (RadButton)sender;
        string CofID = btnSender.CommandArgument.ToString();
        bool isCheck = btnSender.Checked;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);

        ConferenceControler cControl = new ConferenceControler(fn);
        DependentItem cItem = cControl.GetDependentItemByID(CofID, showid);
        bool isSelectedMainItem = ToggleSelectedConfItem(CofID, cItem.disabledItems, isCheck, true, ref RptDependentList);

        decimal mainTotal = 0;
        OrderItemList oList = GetSelectedItems(ConfDefaultValue.conf_MainItem, ref RptMainConfList, ref mainTotal);
        string selectedConfItems = string.Empty;
        int isSelectedMainItemCount = 0;
        if (oList.OrderList.Count > 0)
        {
            foreach (OrderItem oItem in oList.OrderList)
            {
                isSelectedMainItemCount++;
                selectedConfItems += oItem.ItemID + ",";
            }
        }
        else//***add on 22-3-2018
        {
            List<OrderItemList> oSucList = getPaidOrder(urlQuery);
            if (oSucList.Count > 0)
            {
                foreach (OrderItemList ordList in oSucList)
                {
                    if (ordList.OrderList.Count > 0)
                    {
                        foreach (OrderItem oItem in ordList.OrderList)
                        {
                            isSelectedMainItemCount++;
                            selectedConfItems += oItem.ItemID + ",";
                        }
                    }
                }
            }

        }//***add on 22-3-2018
        selectedConfItems = selectedConfItems.TrimEnd(',');

        mainTotal = 0;
        OrderItemList oDList = GetSelectedItems(ConfDefaultValue.conf_DependentItem, ref RptDependentList, ref mainTotal);
        LoadDependentItems(selectedConfItems, isSelectedMainItemCount > 0 ? true : false);

        if (oDList != null)
        {
            foreach (OrderItem oItem in oDList.OrderList)
            {
                ////***add on 29-3-2018
                DependentItem cDItem = cControl.GetDependentItemByID(oItem.ItemID, showid);
                bool isSelectedDItem = ToggleSelectedConfItem(oItem.ItemID, cDItem.disabledItems, true, true, ref RptDependentList, oItem.Qty);
                ////***add on 29-3-2018
                SetSelectedDConfItem(oItem.ItemID, oItem.Qty.ToString(), ref RptDependentList);
            }
        }

        CalculateAndShowTotalSummary();
    }

    protected void ddlConfItem_Onclick(object sender, EventArgs e)
    {
        DropDownList btnSender = (DropDownList)sender;
        string qty = btnSender.SelectedValue;

        //***add on 22-3-2018
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        string flwID = cComFuz.DecryptValue(urlQuery.FlowID);
        string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
        string regno = cComFuz.DecryptValue(urlQuery.DelegateID);
        FlowControler fCon = new FlowControler(fn);
        FlowMaster flwMaster = fCon.GetFlowMasterConfig(flwID);
        OrderControler oControl = new OrderControler(fn);
        //***add on 22-3-2018

        int isSelectedMainItemCount = 0;
        decimal mainTotal = 0;
        OrderItemList oList = GetSelectedItems(ConfDefaultValue.conf_MainItem, ref RptMainConfList, ref mainTotal);
        string selectedConfItems = string.Empty;
        if (oList.OrderList.Count > 0)
        {
            foreach (OrderItem oItem in oList.OrderList)
            {
                isSelectedMainItemCount++;
                selectedConfItems += oItem.ItemID + ",";
            }

            if (flwMaster.FlowConfSelectionType != Number.One) // Not Single selet (1)
            {
                InvoiceControler invCtrl = new InvoiceControler(fn);
                string invoiceOwnerID = invCtrl.DefineInvoiceOwnerID(flwID, groupid, regno);
                OrderItemList oPaidOrderList = oControl.getPaidOrderCountByInvOwnerID(ConfDefaultValue.conf_MainItem, invoiceOwnerID, showid, flwID);
                if (oPaidOrderList.OrderList.Count > 0)
                {
                    foreach (OrderItem oItem in oPaidOrderList.OrderList)
                    {
                        isSelectedMainItemCount++;
                        selectedConfItems += oItem.ItemID + ",";
                    }
                }
            }
        }
        else//***add on 22-3-2018
        {
            List<OrderItemList> oSucList = getPaidOrder(urlQuery);
            if (oSucList.Count > 0)
            {
                foreach (OrderItemList ordList in oSucList)
                {
                    if (ordList.OrderList.Count > 0)
                    {
                        foreach (OrderItem oItem in ordList.OrderList)
                        {
                            isSelectedMainItemCount++;
                            selectedConfItems += oItem.ItemID + ",";
                        }
                    }
                }
            }

        }//***add on 22-3-2018
        selectedConfItems = selectedConfItems.TrimEnd(',');
        OrderItemList oDList = GetSelectedItems(ConfDefaultValue.conf_DependentItem, ref RptDependentList, ref mainTotal);
        LoadDependentItems(selectedConfItems, isSelectedMainItemCount > 0 ? true : false);

        if (oDList.OrderList.Count > 0)
        {
            string disabledItems = "";
            foreach (OrderItem oItem in oDList.OrderList)
            {
                if (!disabledItems.Contains(oItem.ItemID))////***add on 18-11-2019
                {
                    ////***add on 29-3-2018
                    ConferenceControler cControl = new ConferenceControler(fn);
                    DependentItem cItem = cControl.GetDependentItemByID(oItem.ItemID, showid);
                    lstPendingAcc = oItem.AccomOrderList;
                    bool isSelectedMainItem = ToggleSelectedConfItem(oItem.ItemID, cItem.disabledItems, true, true, ref RptDependentList, oItem.Qty);
                    disabledItems = cItem.disabledItems;
                    ////***add on 29-3-2018
                    SetSelectedDConfItem(oItem.ItemID, oItem.Qty.ToString(), ref RptDependentList);
                }
            }
        }

        CalculateAndShowTotalSummary();
    }

    protected void txtQty_TextChange(object sender, EventArgs e)
    {
        CalculateAndShowTotalSummary();
    }

    protected void RptDependentList_DataBond(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string regno = cComFuz.DecryptValue(urlQuery.DelegateID);
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
            string flwID = cComFuz.DecryptValue(urlQuery.FlowID);

            Label Description = (Label)e.Item.FindControl("lblDisplay");
            Label Display = (Label)e.Item.FindControl("ConItemDisplayPrice");
            TextBox txtPrice = (TextBox)e.Item.FindControl("txtConfPrice");
            TextBox txtConfItemID = (TextBox)e.Item.FindControl("txtConfItemID");
            RadButton btnSelect = (RadButton)e.Item.FindControl("chkConfItem");
            DropDownList ddlQty = (DropDownList)e.Item.FindControl("ddlQty");
            Panel pnlShowSelect = (Panel)e.Item.FindControl("PanelShowSelect");
            Panel pnlMsg = (Panel)e.Item.FindControl("PanleMsg");
            Label lblMsg = (Label)e.Item.FindControl("lblMsg");
            Image imgLogo = (Image)e.Item.FindControl("imgLogo");

            Label lblDescription = (Label)e.Item.FindControl("lblDescription");//*

            string DependentID = txtConfItemID.Text;
            if (!string.IsNullOrEmpty(DependentID))
            {
                ConferenceControler cControl = new ConferenceControler(fn);
                DependentItem cItem = cControl.GetDependentItemByID(DependentID, showid);
                if (!string.IsNullOrEmpty(cItem.DependentID))
                {
                    Description.Text = cItem.ConDisplayText;
                    lblDescription.Text = cItem.ConfDItemDesc;//*
                    int maxBuyableTime = cItem.maxBuyableTime;

                    string imgUrl = cItem.ConfDImage;
                    imgLogo.ImageUrl = imgUrl.Replace("../","");
                    if (File.Exists(Server.MapPath(imgUrl.Replace("..", "~"))))
                    {
                        imgLogo.Visible = true;
                    }
                    else
                    {
                        imgLogo.Visible = false;
                    }

                    string price = cControl.GetDependentItemPrice("", cItem, showid);
                    txtPrice.Text = price;
                    Display.Text = txtCurrency.Text + " " + cComFuz.FormatCurrency(price);

                    SiteSettings sSetting = new SiteSettings(fn, showid);
                    sSetting.LoadBaseSiteProperties(showid);
                    if (sSetting.SiteMasterDependentConfType == YesOrNo.Yes) // Single selet(1)
                    {
                        ddlQty.Visible = false;
                        btnSelect.Visible = true;
                    }
                    else
                    {
                        //allow qty key in
                        ddlQty.Visible = true;
                        btnSelect.Visible = false;

                        ////***add on 29-3-2018 - th
                        string confType = cItem.con_Type;
                        //if(confType == ConfDefaultValue.conf_AccompanyingPersonItem)////***comment on 6-8-2018 - th
                        {
                            ddlQty.Items.Clear();
                            ddlQty.ClearSelection();
                            ddlQty.Items.Insert(0, new ListItem("0", "0"));
                            for (int i=1; i<= maxBuyableTime; i++)
                            {
                                ddlQty.Items.Add(i.ToString());
                                ddlQty.Items[i].Value = i.ToString();
                            }
                        }
                        ////***add on 29-3-2018 - th
                    }

                    /////***check buyable time to visible/enabled(edit on 16-3-2018)
                    OrderControler oControl = new OrderControler(fn);
                    InvoiceControler invCtrl = new InvoiceControler(fn);
                    string invoiceOwnerID = invCtrl.DefineInvoiceOwnerID(flwID, groupid, regno);
                    int paidOrderQty = oControl.getPaidOrderCountByItemIDInvOwnerID(ConfDefaultValue.conf_DependentItem, DependentID, invoiceOwnerID, showid, flwID);
                    if (paidOrderQty >= maxBuyableTime)
                    {
                        ddlQty.Enabled = false;//***
                        btnSelect.Enabled = false;//***
                    }
                    /////***check buyable time to visible/enabled(edit on 16-3-2018)

                    //Check Reach Max Usage(Check according to ordered items from tb_OrderDetials table)
                    int maxUsageItem = cComFuz.ParseInt(cItem.AllowdQty);
                    int usedItem = oControl.getAllOrderCountByItemID(DependentID, ConfDefaultValue.conf_DependentItem);//cComFuz.ParseInt(cItem.UsedQty);
                    if (usedItem >= maxUsageItem)
                    {
                        pnlShowSelect.Visible = false;
                        pnlMsg.Visible = true;
                        lblMsg.Text = HttpUtility.HtmlDecode(cItem.MaxMessge);
                    }
                    else
                    {
                        pnlShowSelect.Visible = true;
                        pnlMsg.Visible = false;
                        lblMsg.Text = "";
                    }
                }
            }
        }
    }

    protected void btnNext_Onclick(object sender, EventArgs e)
    {
        string actType = string.Empty;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string flwID = cComFuz.DecryptValue(urlQuery.FlowID);
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
        string regno = cComFuz.DecryptValue(urlQuery.DelegateID);
        if (!string.IsNullOrEmpty(showid))
        {
            Boolean isvalidpage = isValidPage(showid, urlQuery);
            if (isvalidpage)
            {
                decimal mainTotal = 0;
                OrderItemList oList = GetSelectedItems(ConfDefaultValue.conf_MainItem, ref RptMainConfList, ref mainTotal);

                FlowControler fCon = new FlowControler(fn);
                FlowMaster flwMaster = fCon.GetFlowMasterConfig(flwID);
                string confSkip = flwMaster.FlowConfSkip;

                if (confSkip == YesOrNo.Yes)
                {
                    if (oList.OrderList.Count > 0)
                    {
                        changeOrderQtyAsAllMemberCount(groupid, showid, ref oList);//////***FTC

                        string errmessage = string.Empty;
                        bool isSuccess = InsertOrderAndNextRoute(urlQuery, oList, mainTotal, showid, ref errmessage);

                        if (isSuccess == false)
                        {
                            string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errmessage + "');window.location.href='" + currentUrl + "';", true);
                            return;
                        }
                    }

                    FlowControler flwObj = new FlowControler(fn, urlQuery);
                    string showID = urlQuery.CurrShowID;
                    string page = flwObj.NextStepURL();
                    string step = flwObj.NextStep;
                    string FlowID = flwObj.FlowID;
                    string grpNum = "";
                    grpNum = urlQuery.GoupRegID;
                    regno = urlQuery.DelegateID;

                    insertLogFlowAction(cComFuz.DecryptValue(grpNum), cComFuz.DecryptValue(regno), actType + rlgobj.actnext, urlQuery);
                    saveRegAdditional(showid, regno, urlQuery);//***added on 12-10-2018

                    string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step);//, regno);//////***FTC
                    Response.Redirect(route);
                }
                else
                {
                    if (oList.OrderList.Count > 0)
                    {
                        changeOrderQtyAsAllMemberCount(groupid, showid, ref oList);//////***FTC

                        string errmessage = string.Empty;
                        bool isSuccess = InsertOrderAndNextRoute(urlQuery, oList, mainTotal, showid, ref errmessage);

                        if (isSuccess == false)
                        {
                            string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errmessage + "');window.location.href='" + currentUrl + "';", true);
                            return;
                        }

                        FlowControler flwObj = new FlowControler(fn, urlQuery);
                        string showID = urlQuery.CurrShowID;
                        string page = flwObj.NextStepURL();
                        string step = flwObj.NextStep;
                        string FlowID = flwObj.FlowID;
                        string grpNum = "";
                        grpNum = urlQuery.GoupRegID;
                        regno = urlQuery.DelegateID;

                        insertLogFlowAction(cComFuz.DecryptValue(grpNum), cComFuz.DecryptValue(regno), actType + rlgobj.actnext, urlQuery);
                        saveRegAdditional(showid, regno, urlQuery);//***added on 12-10-2018

                        string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step);//, regno);//////***FTC
                        Response.Redirect(route);
                    }
                    else
                    {
                        //***edit on 22-3-2018
                        OrderControler oControl = new OrderControler(fn);
                        InvoiceControler invCtrl = new InvoiceControler(fn);
                        string invoiceOwnerID = invCtrl.DefineInvoiceOwnerID(flwID, groupid, regno);
                        OrderItemList oPaidOrderList = oControl.getPaidOrderCountByInvOwnerID(ConfDefaultValue.conf_MainItem, invoiceOwnerID, showid, flwID);
                        if (oPaidOrderList.OrderList.Count == 0)
                        {
                            string message = "Please select conference.";//*
                            ShowControler shwCtr = new ShowControler(fn);
                            Show shw = shwCtr.GetShow(showid);
                            if (checkingvnuShowName.Contains(shw.SHW_Name))
                            {
                                message = "Please tick box for entry ticket";
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('" + message + "');</script>", false);
                            return;
                        }
                        else
                        {
                            changeOrderQtyAsAllMemberCount(groupid, showid, ref oList);//////***FTC

                            string errmessage = string.Empty;
                            bool isSuccess = InsertOrderAndNextRoute(urlQuery, oPaidOrderList = new OrderItemList(), mainTotal, showid, ref errmessage);

                            if (isSuccess == false)
                            {
                                string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errmessage + "');window.location.href='" + currentUrl + "';", true);
                                return;
                            }

                            FlowControler flwObj = new FlowControler(fn, urlQuery);
                            string showID = urlQuery.CurrShowID;
                            string page = flwObj.NextStepURL();
                            string step = flwObj.NextStep;
                            string FlowID = flwObj.FlowID;
                            string grpNum = "";
                            grpNum = urlQuery.GoupRegID;
                            regno = urlQuery.DelegateID;

                            insertLogFlowAction(cComFuz.DecryptValue(grpNum), cComFuz.DecryptValue(regno), actType + rlgobj.actnext, urlQuery);
                            saveRegAdditional(showid, regno, urlQuery);//***added on 12-10-2018

                            string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step);//, regno);//////***FTC
                            Response.Redirect(route);
                        }
                        //***edit on 22-3-2018
                    }
                }
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }
    #endregion

    #region InsertOrderAndNextRoute
    private bool InsertOrderAndNextRoute(FlowURLQuery urlQuery, OrderItemList oList, decimal mainTotal, string showid, ref string errMessage)
    {
        bool isSuccess = true;

        string actType = string.Empty;
        decimal DTotal = 0;

        /*
        if Main Conf , 
        Check Payment done  . 
        if not Check Invioce Created, Remove 
        Remove Pending Items ->Check Main Conf & Dependent Conf Items' Maximum Quantity whether reach or not ->Generate Order No->save
        */
        OrderControler oControl = new OrderControler(fn);
        string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
        string DelegateID = "";// cComFuz.DecryptValue(urlQuery.DelegateID);//////***FTC
        string OrderNO = "";//////***FTC
        string flwID = cComFuz.DecryptValue(urlQuery.FlowID);
        decimal discount = 0;
        // bool isMainInvPaid = false;

        #region changed on 27-12-2019 for FTC
        List<Order> oListPendingOrder = oControl.GetAllPendingOrder(GroupRegID, DelegateID);
        if (oListPendingOrder.Count > 0)
        {
            foreach (Order orderPendingOrder in oListPendingOrder)
            {
                string pendingOrderNO = orderPendingOrder.OrderNo;
                if (!string.IsNullOrEmpty(pendingOrderNO))
                {
                    //Remove Pending Orders
                    oControl.DeleteOrder(pendingOrderNO, GroupRegID);
                }
            }
        }
        #endregion

        /*
            Check Maximum quantity for Conference Item & Conference Dependent Item before go to Confirmation Page
           if selected Main Conf or Dependent Conf are greater than or equan to Maximum Qty, reload this page and still at this page
        */
        ConferenceControler cControl = new ConferenceControler(fn);
        int soldoutCount = 0;
        int overQty = 0;
        string msgoverQty = string.Empty;
        int mustSelectDependentCount = 0;
        int mustSelectDependent_OverCount = 0;
        foreach (OrderItem oItem in oList.OrderList)
        {
            ConferenceItem cItem = cControl.GetConferenceItemByID(oItem.ItemID, showid);
            int usedItem = 0;//////***FTC//oControl.getAllOrderCountByItemID(oItem.ItemID, ConfDefaultValue.conf_MainItem, GroupRegID, DelegateID);//cComFuz.ParseInt(cItem.UsedQty);

            /////***check buyable time to visible/enabled(edit on 16-3-2018)
            int maxBuyableTime = cItem.maxBuyableTime;
            InvoiceControler invCtrl = new InvoiceControler(fn);
            string invoiceOwnerID = invCtrl.DefineInvoiceOwnerID(flwID, GroupRegID, DelegateID);
            int paidOrderQty = oControl.getPaidOrderCountByItemIDInvOwnerID(ConfDefaultValue.conf_MainItem, oItem.ItemID, invoiceOwnerID, showid, flwID);
            if (paidOrderQty >= maxBuyableTime)
            {
                soldoutCount++;
            }
            else if (usedItem + oItem.Qty > maxBuyableTime)
            {
                overQty = (usedItem + oItem.Qty) - maxBuyableTime;
                msgoverQty = oItem.ItemDescription;
            }
            /////***check buyable time to visible/enabled(edit on 16-3-2018)

            int maxUsageItem = cComFuz.ParseInt(cItem.AllowdQty);
            if (usedItem >= maxUsageItem)
            {
                soldoutCount++;
            }
            else if (usedItem + oItem.Qty > maxUsageItem)
            {
                overQty = (usedItem + oItem.Qty) - maxUsageItem;
                msgoverQty = oItem.ItemDescription;
            }
        }

        OrderItemList oDependentList = GetSelectedItems(ConfDefaultValue.conf_DependentItem, ref RptDependentList, ref DTotal);
        foreach (OrderItem oItem in oDependentList.OrderList)
        {
            DependentItem cDItem = cControl.GetDependentItemByID(oItem.ItemID, showid);
            int usedItem = 0;//////***FTC//oControl.getAllOrderCountByItemID(oItem.ItemID, ConfDefaultValue.conf_DependentItem, GroupRegID, DelegateID);//cComFuz.ParseInt(cItem.UsedQty);

            /////***check buyable time to visible/enabled(edit on 16-3-2018)
            int maxBuyableTime = cDItem.maxBuyableTime;
            InvoiceControler invCtrl = new InvoiceControler(fn);
            string invoiceOwnerID = invCtrl.DefineInvoiceOwnerID(flwID, GroupRegID, DelegateID);
            int paidOrderQty = oControl.getPaidOrderCountByItemIDInvOwnerID(ConfDefaultValue.conf_DependentItem, oItem.ItemID, invoiceOwnerID, showid, flwID);
            if (paidOrderQty >= maxBuyableTime)
            {
                soldoutCount++;
            }
            else if (usedItem + oItem.Qty > maxBuyableTime)
            {
                overQty = (usedItem + oItem.Qty) - maxBuyableTime;
                msgoverQty = oItem.ItemDescription;
            }
            /////***check buyable time to visible/enabled(edit on 16-3-2018)

            int maxUsageItem = cComFuz.ParseInt(cDItem.AllowdQty);
            if (usedItem >= maxUsageItem)
            {
                soldoutCount++;
            }
            else if (usedItem + oItem.Qty > maxUsageItem)
            {
                overQty = (usedItem + oItem.Qty) - maxUsageItem;
                msgoverQty = oItem.ItemDescription;
            }
        }

        string maxdependentSelectCount = "";
        foreach (OrderItem oItem in oList.OrderList)
        {
            string confItemID = oItem.ItemID;
            ConfConfig confConfig = new ConfConfig(fn);
            ConferenceConfigList confConfigList = confConfig.GetConferenceConfigListByConfID(confItemID, ConfDefaultValue.conf_DependentItem, showid);
            if(confConfigList.ConfConfigList.Count > 0)
            {
                foreach (ConferenceConfigObj confConfigObj in confConfigList.ConfConfigList)
                {
                    string confGroupID = confConfigObj.ConfGroupID;
                    int minselectValue = confConfigObj.MinSelectValue;
                    int maxselectValue = confConfigObj.MaxSelectValue;
                    int dependentSelectedCount = 0;
                    if (oDependentList.OrderList.Count > 0)
                    {
                        foreach (OrderItem oDItem in oDependentList.OrderList)
                        {
                            if (confGroupID == oDItem.ConfGroupID)
                            {
                                dependentSelectedCount++;
                            }
                        }
                    }
                    if (dependentSelectedCount < minselectValue)
                    {
                        mustSelectDependentCount++;
                    }
                    else if (dependentSelectedCount > maxselectValue)
                    {
                        mustSelectDependent_OverCount++;
                        maxdependentSelectCount = maxselectValue.ToString();
                    }
                }
            }
        }

        if (soldoutCount > 0)
        {
            errMessage = "Sorry, some of your order items are already sold out.";
            isSuccess = false;
        }
        else if (overQty > 0)
        {
            errMessage = "Sorry," + msgoverQty + " item(s) are over limited quantity.";
            isSuccess = false;
        }//*Check Maximum quantity
        else
        {
            if (mustSelectDependentCount > 0)
            {
                errMessage = "Sorry, please select item.";
                isSuccess = false;
            }
            else if(mustSelectDependent_OverCount > 0)
            {
                errMessage = "You have exceeded your selection. Please select " + maxdependentSelectCount + " item only.";
                isSuccess = false;
            }
            else
            {
                #region changed for FTC (27-12-2019)//////***FTC
                RegDelegateObj rgd = new RegDelegateObj(fn);
                DataTable dt = rgd.getRegDelegateByGroupID(GroupRegID, showid);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string delegateID = dr["Regno"].ToString();
                        string fullUrl = "FLW=" + cComFuz.DecryptValue(urlQuery.FlowID) + "&STP=" + cComFuz.DecryptValue(urlQuery.CurrIndex)
                                        + "&GRP=" + cComFuz.DecryptValue(GroupRegID) + "&INV=" + cComFuz.DecryptValue(delegateID) 
                                        + "&SHW=" + cComFuz.DecryptValue(showid);
                        FlowURLQuery newUrl = new FlowURLQuery(fullUrl);
                        /*
                            -Genereate Order No
                            -Create New Order and Order Items
                        */
                        OrderNO = oControl.GenerateOrderNo(showid);// GenerateOrderNo(showid, GroupRegID);*Old
                        if (!string.IsNullOrEmpty(OrderNO))
                        {
                            //*No Pending 
                            //OrderItemList oDependentList = GetSelectedItems(ConfDefaultValue.conf_DependentItem, ref RptDependentList, ref DTotal);

                            Order oRder = new Order();
                            oRder.OrderNo = OrderNO;
                            oRder.RegGroupID = GroupRegID;
                            oRder.RegDelegateID = delegateID;
                            oRder.SubTotal = mainTotal + DTotal;
                            oRder.orderStatus = ((int)StatusValue.Pending).ToString();
                            //oRder.Discount = discount;
                            changeOrderQtyForIndividualMember(GroupRegID, showid, ref oList);
                            oList.OrderNO = OrderNO;
                            oControl.AddNewMainConferenceItem(oRder, oList, newUrl);
                            actType = rlgobj.actsave;

                            oDependentList.OrderNO = OrderNO;
                            oControl.AddOrderItems(oDependentList, newUrl);
                        }
                    }
                }
                #endregion
            }
        }

        return isSuccess;
    }
    #endregion

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
            Page.MasterPageFile = masterPage;
    }
    #endregion

    #region LoadConference
    private void LoadConference()
    {
        ConferenceControler cControl = new ConferenceControler(fn);
        CategoryObj catObj = new CategoryObj(fn);
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string DeleagateID = cComFuz.DecryptValue(urlQuery.DelegateID);
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        string catType = catObj.getCategoryID(DeleagateID, showid, flowid);
        RptMainConfList.DataSource = cControl.GetConferenceList(catType, showid, flowid);
        RptMainConfList.DataBind();
    }

    private void LoadDependentItems(string ConfID, bool isMainItemSelected)
    {
        bool showPanel = false;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        if (txtShowDependentItem.Text == ConfDefaultValue.ShowDepentdentItem)
        {
            if (isMainItemSelected)
            {
                if (!string.IsNullOrEmpty(ConfID))
                {
                    ConferenceControler cControl = new ConferenceControler(fn);
                    RptDependentList.DataSource = cControl.GetDependentList(ConfID, showid);
                    RptDependentList.DataBind();

                    if (RptDependentList.Items.Count > 0) showPanel = true;
                    else showPanel = false;
                }
            }
            else showPanel = false;
        }
        if (showPanel)
            PanelDependent.Visible = showPanel;
        else
        { // Clear Dependent List
            PanelDependent.Visible = showPanel;
            RptDependentList.DataSource = null;
            RptDependentList.DataSourceID = null;
            RptDependentList.DataBind();
        }
    }
    #endregion

    #region ConfItem
    private void SetSelectedConfItemList(OrderItemList OList, bool isPaid=false)
    {
        /*
            set selected Conf Item or Dependent Conf Item
        */
        string qty = Number.Zero;
        OrderItemList oDList = new OrderItemList();
        string selectedConfItems = string.Empty;
        int selectedCount = 0;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        foreach (OrderItem oItem in OList.OrderList)
        {
            bool isSelect = false;
            qty = oItem.Qty.ToString();
            SetSelectedConfItem(oItem.ItemID, qty, ref RptMainConfList, ref isSelect);
            ////***add on 29-3-2018
            if (oItem.ItemType == ConfDefaultValue.conf_MainItem)
            {
                ConferenceControler cControl = new ConferenceControler(fn);
                ConferenceItem cItem = cControl.GetConferenceItemByID(oItem.ItemID, showid);
                bool isSelectedMainItem = ToggleSelectedConfItem(oItem.ItemID, cItem.disabledItems, true, false, ref RptMainConfList, oItem.Qty, isPaid);
            }
            ////***add on 29-3-2018

            if (isSelect)
            {
                selectedCount ++;
            }

            if (oItem.ItemType == ConfDefaultValue.conf_MainItem)
            {
                selectedConfItems += oItem.ItemID + ",";
            }

            if (oItem.ItemType == ConfDefaultValue.conf_DependentItem)
            {
                OrderItem oDItem = new OrderItem();
                oDItem.ItemID = oItem.ItemID;
                oDItem.Qty = cComFuz.ParseInt(qty);
                if(oItem.AccomOrderList.Count > 0)
                {
                    oDItem.AccomOrderList = oItem.AccomOrderList;
                }
                oDList.OrderList.Add(oDItem);
            }
        }

        selectedConfItems = selectedConfItems.TrimEnd(',');

        LoadDependentItems(selectedConfItems, selectedCount >0 ? true : false);

        foreach (OrderItem oItem in oDList.OrderList)
        {
            ////***add on 29-3-2018
            ConferenceControler cControl = new ConferenceControler(fn);
            DependentItem cItem = cControl.GetDependentItemByID(oItem.ItemID, showid);
            lstPendingAcc = oItem.AccomOrderList;
            bool isSelectedMainItem = ToggleSelectedConfItem(oItem.ItemID, cItem.disabledItems, true, true, ref RptDependentList, oItem.Qty, isPaid);
            ////***add on 29-3-2018
            SetSelectedDConfItem(oItem.ItemID, oItem.Qty.ToString(), ref RptDependentList, isPaid);
        }

    }
    private void SetSelectedConfItem(string ConfItem, string qty, ref Repeater Rpt, ref bool isSelected)
    {
        for (int i = 0; i < Rpt.Items.Count; i++)
        {
            TextBox txtConID = (TextBox)Rpt.Items[i].FindControl("txtConfItemID");
            DropDownList ddlQty = (DropDownList)Rpt.Items[i].FindControl("ddlQty");
            RadButton btnItem = (RadButton)Rpt.Items[i].FindControl("chkConfItem");
            Panel pnlMsg = (Panel)Rpt.Items[i].FindControl("PanleMsg");
            if (txtConID.Text == ConfItem)
            {
                if (cComFuz.ParseInt(qty) > 0)
                {
                    if (pnlMsg.Visible == false)
                    {
                        //btnItem.CssClass = conf_Remove_CSS;
                        //btnItem.Text = ConfDefaultValue.conf_Remove_Text;
                        ddlQty.Text = qty;
                        btnItem.Checked = true;

                        isSelected = true;
                    }
                }
            }
        }
    }
    private void SetSelectedDConfItem(string ConfItem, string qty, ref Repeater Rpt, bool isPaid=false)
    {
        for (int i = 0; i < Rpt.Items.Count; i++)
        {
            TextBox txtConID = (TextBox)Rpt.Items[i].FindControl("txtConfItemID");
            DropDownList ddlQty = (DropDownList)Rpt.Items[i].FindControl("ddlQty");
            RadButton btnItem = (RadButton)Rpt.Items[i].FindControl("chkConfItem");
            Panel pnlMsg = (Panel)Rpt.Items[i].FindControl("PanleMsg");
            if (txtConID.Text == ConfItem)
            {
                if (cComFuz.ParseInt(qty) > 0)
                {
                    if (pnlMsg.Visible == false)
                    {
                        if (!isPaid)
                        {
                            //btnItem.CssClass = conf_Remove_CSS;
                            //btnItem.Text = ConfDefaultValue.conf_Remove_Text;
                            ddlQty.SelectedItem.Text = qty;
                            btnItem.Checked = true;
                        }
                    }
                }
            }
        }
    }
    private bool ToggleSelectedConfItem(string ConfItem, string disableditems, bool isCheck, bool isDependent, ref Repeater Rpt, int selectedQty = 0, bool isPaid=false)
    {
        bool IsSelectedItem = false;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string flwID = cComFuz.DecryptValue(urlQuery.FlowID);
        FlowControler fCon = new FlowControler(fn);
        FlowMaster flwMaster = fCon.GetFlowMasterConfig(flwID);
        for (int i = 0; i < Rpt.Items.Count; i++)
        {
            TextBox txtConID = (TextBox)Rpt.Items[i].FindControl("txtConfItemID");
            DropDownList ddlQty = (DropDownList)Rpt.Items[i].FindControl("ddlQty");
            RadButton btnItem = (RadButton)Rpt.Items[i].FindControl("chkConfItem");

            if (txtConID.Text == ConfItem)
            {
                if (ddlQty.Enabled == true)//***add on 16-3-2018
                {
                    if (cComFuz.ParseInt(ddlQty.SelectedValue) > 0)
                    {
                        if (flwMaster.FlowConfSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                        {
                            if (isCheck == true)//***change on 16-5-2018
                            {
                                ddlQty.SelectedValue = ConfDefaultValue.conf_SingleSelect_Selected_Value;
                            }
                            else
                            {
                                //    btnItem.CssClass = conf_Select_CSS;
                                //    btnItem.Text = ConfDefaultValue.conf_Select_Text;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                            }
                        }
                        else//***add on 29-3-2018
                        {
                            if(!isCheck)
                            {
                                btnItem.Checked = false;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                            }
                            else//***
                            {
                                if (isDependent)
                                {
                                    TextBox txtConfType = (TextBox)Rpt.Items[i].FindControl("txtConfType");
                                    if(txtConfType.Text == ConfDefaultValue.conf_AccompanyingPersonItem)
                                    {
                                        if (!isPaid)
                                        {
                                            setAccompanyingPersonInfo(Rpt, i, cComFuz.ParseInt(ddlQty.SelectedItem.Value), isCheck);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (flwMaster.FlowConfSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                        {
                            if (!isDependent)
                            {
                                //btnItem.CssClass = conf_Remove_CSS;
                                //btnItem.Text = ConfDefaultValue.conf_Remove_Text;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_SingleSelect_Selected_Value;
                                IsSelectedItem = true;
                            }
                            else
                            {
                                if (flwMaster.FlowConfDependentSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                                {
                                    //btnItem.CssClass = conf_Remove_CSS;
                                    //btnItem.Text = ConfDefaultValue.conf_Remove_Text;
                                    ddlQty.SelectedValue = ConfDefaultValue.conf_SingleSelect_Selected_Value;
                                    IsSelectedItem = true;
                                }
                                else
                                {
                                    ddlQty.SelectedValue = selectedQty.ToString();
                                    IsSelectedItem = true;
                                }
                            }
                        }
                        else
                        {
                            if(btnItem.Checked == true)//***add on 16-3-2018
                            {
                                if (!isDependent)
                                {
                                    //btnItem.CssClass = conf_Remove_CSS;
                                    //btnItem.Text = ConfDefaultValue.conf_Remove_Text;
                                    ddlQty.SelectedValue = ConfDefaultValue.conf_SingleSelect_Selected_Value;
                                    IsSelectedItem = true;
                                }
                                else
                                {
                                    if (flwMaster.FlowConfDependentSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                                    {
                                        //btnItem.CssClass = conf_Remove_CSS;
                                        //btnItem.Text = ConfDefaultValue.conf_Remove_Text;
                                        ddlQty.SelectedValue = ConfDefaultValue.conf_SingleSelect_Selected_Value;
                                        IsSelectedItem = true;
                                    }
                                    else
                                    {
                                        ddlQty.SelectedValue = selectedQty.ToString();
                                        IsSelectedItem = true;
                                    }
                                }
                            }

                            if (isDependent)
                            {
                                if (selectedQty > 0 && !isPaid)
                                {
                                    try
                                    {
                                        ListItem listItem = ddlQty.Items.FindByValue(selectedQty.ToString());
                                        if (listItem != null)
                                        {
                                            ddlQty.ClearSelection();
                                            listItem.Selected = true;
                                            IsSelectedItem = true;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                            }
                        }

                        if (isDependent)//***
                        {
                            TextBox txtConfType = (TextBox)Rpt.Items[i].FindControl("txtConfType");
                            if (txtConfType.Text == ConfDefaultValue.conf_AccompanyingPersonItem)
                            {
                                if (!isPaid)
                                {
                                    setAccompanyingPersonInfo(Rpt, i, cComFuz.ParseInt(ddlQty.SelectedItem.Value), isCheck);
                                }
                            }
                        }
                    }
                }
                else//***add on 16-3-2018
                {
                    if (flwMaster.FlowConfSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                    {
                        //btnItem.CssClass = conf_Select_CSS;
                        //btnItem.Text = ConfDefaultValue.conf_Select_Text;
                        btnItem.Checked = false;
                        ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                    }
                    else//***add on 29-3-2018
                    {
                        if (!isCheck)
                        {
                            btnItem.Checked = false;
                            ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                        }
                        else//***
                        {
                            if (isDependent)
                            {
                                TextBox txtConfType = (TextBox)Rpt.Items[i].FindControl("txtConfType");
                                if (txtConfType.Text == ConfDefaultValue.conf_AccompanyingPersonItem)
                                {
                                    if (!isPaid)
                                    {
                                        setAccompanyingPersonInfo(Rpt, i, cComFuz.ParseInt(ddlQty.SelectedItem.Value), isCheck);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (ddlQty.Enabled == true)//***add on 16-3-2018
                {
                    if (isCheck)
                    {
                        if (disableditems.Contains(txtConID.Text))
                        {
                            if (btnItem.Visible == true)
                            {
                                ddlQty.ClearSelection();
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                                btnItem.Enabled = false;
                                btnItem.Checked = false;
                            }
                            if (ddlQty.Visible == true)
                            {
                                ddlQty.ClearSelection();
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                                ddlQty.Enabled = false;
                            }
                        }
                        else
                        {
                            if (btnItem.Enabled == true)
                            {
                                btnItem.Enabled = true;
                            }
                            if (ddlQty.Enabled == true)
                            {
                                ddlQty.Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        if (disableditems.Contains(txtConID.Text))
                        {
                            btnItem.Enabled = true;
                            ddlQty.Enabled = true;
                        }
                    }

                    if (isDependent)
                    {
                        if (flwMaster.FlowConfDependentSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                        {
                            //btnItem.CssClass = conf_Select_CSS;
                            //btnItem.Text = ConfDefaultValue.conf_Select_Text;
                            btnItem.Checked = false;
                            ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                        }
                    }
                    else
                    {
                        if (flwMaster.FlowConfSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                        {
                            //btnItem.CssClass = conf_Select_CSS;
                            //btnItem.Text = ConfDefaultValue.conf_Select_Text;
                            btnItem.Checked = false;
                            ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                        }
                    }
                }
            }
        }
        return IsSelectedItem;
    }
    #region AccompanyingPersons
    private void setaccomTextBoxesTrsFalse(Repeater rptItem, int j)
    {
        try
        {
            HtmlTableRow trAccFullName1 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName1");
            HtmlTableRow trAccFullName2 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName2");
            HtmlTableRow trAccFullName3 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName3");
            HtmlTableRow trAccFullName4 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName4");
            HtmlTableRow trAccFullName5 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName5");
            HtmlTableRow trAccFullName6 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName6");
            HtmlTableRow trAccFullName7 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName7");
            HtmlTableRow trAccFullName8 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName8");
            HtmlTableRow trAccFullName9 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName9");
            HtmlTableRow trAccFullName10 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName10");

            HtmlTableRow trFullName1 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName1");
            HtmlTableRow trFullName2 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName2");
            HtmlTableRow trFullName3 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName3");
            HtmlTableRow trFullName4 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName4");
            HtmlTableRow trFullName5 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName5");
            HtmlTableRow trFullName6 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName6");
            HtmlTableRow trFullName7 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName7");
            HtmlTableRow trFullName8 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName8");
            HtmlTableRow trFullName9 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName9");
            HtmlTableRow trFullName10 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName10");

            HtmlTableRow trCompany1 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany1");
            HtmlTableRow trCompany2 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany2");
            HtmlTableRow trCompany3 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany3");
            HtmlTableRow trCompany4 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany4");
            HtmlTableRow trCompany5 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany5");
            HtmlTableRow trCompany6 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany6");
            HtmlTableRow trCompany7 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany7");
            HtmlTableRow trCompany8 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany8");
            HtmlTableRow trCompany9 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany9");
            HtmlTableRow trCompany10 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany10");

            HtmlTableRow trCountry1 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry1");
            HtmlTableRow trCountry2 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry2");
            HtmlTableRow trCountry3 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry3");
            HtmlTableRow trCountry4 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry4");
            HtmlTableRow trCountry5 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry5");
            HtmlTableRow trCountry6 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry6");
            HtmlTableRow trCountry7 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry7");
            HtmlTableRow trCountry8 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry8");
            HtmlTableRow trCountry9 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry9");
            HtmlTableRow trCountry10 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry10");

            #region Additional
            HtmlTableRow trAdditional1 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional1");
            HtmlTableRow trAdditional2 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional2");
            HtmlTableRow trAdditional3 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional3");
            HtmlTableRow trAdditional4 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional4");
            HtmlTableRow trAdditional5 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional5");
            HtmlTableRow trAdditional6 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional6");
            HtmlTableRow trAdditional7 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional7");
            HtmlTableRow trAdditional8 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional8");
            HtmlTableRow trAdditional9 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional9");
            HtmlTableRow trAdditional10 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional10");

            HtmlTableCell tdAdditional1 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional1");
            HtmlTableCell tdAdditional2 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional2");
            HtmlTableCell tdAdditional3 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional3");
            HtmlTableCell tdAdditional4 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional4");
            HtmlTableCell tdAdditional5 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional5");
            HtmlTableCell tdAdditional6 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional6");
            HtmlTableCell tdAdditional7 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional7");
            HtmlTableCell tdAdditional8 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional8");
            HtmlTableCell tdAdditional9 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional9");
            HtmlTableCell tdAdditional10 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional10");
            #endregion

            #region trs visible false
            trAccFullName1.Visible = false;
            trAccFullName2.Visible = false;
            trAccFullName3.Visible = false;
            trAccFullName4.Visible = false;
            trAccFullName5.Visible = false;
            trAccFullName6.Visible = false;
            trAccFullName7.Visible = false;
            trAccFullName8.Visible = false;
            trAccFullName9.Visible = false;
            trAccFullName10.Visible = false;

            trFullName1.Visible = false;
            trFullName2.Visible = false;
            trFullName3.Visible = false;
            trFullName4.Visible = false;
            trFullName5.Visible = false;
            trFullName6.Visible = false;
            trFullName7.Visible = false;
            trFullName8.Visible = false;
            trFullName9.Visible = false;
            trFullName10.Visible = false;

            trCompany1.Visible = false;
            trCompany2.Visible = false;
            trCompany3.Visible = false;
            trCompany4.Visible = false;
            trCompany5.Visible = false;
            trCompany6.Visible = false;
            trCompany7.Visible = false;
            trCompany8.Visible = false;
            trCompany9.Visible = false;
            trCompany10.Visible = false;

            trCountry1.Visible = false;
            trCountry2.Visible = false;
            trCountry3.Visible = false;
            trCountry4.Visible = false;
            trCountry5.Visible = false;
            trCountry6.Visible = false;
            trCountry7.Visible = false;
            trCountry8.Visible = false;
            trCountry9.Visible = false;
            trCountry10.Visible = false;
            #endregion

            #region reset Additional
            trAdditional1.Visible = false;
            trAdditional2.Visible = false;
            trAdditional3.Visible = false;
            trAdditional4.Visible = false;
            trAdditional5.Visible = false;
            trAdditional6.Visible = false;
            trAdditional7.Visible = false;
            trAdditional8.Visible = false;
            trAdditional9.Visible = false;
            trAdditional10.Visible = false;

            tdAdditional1.InnerText = "";
            tdAdditional2.InnerText = "";
            tdAdditional3.InnerText = "";
            tdAdditional4.InnerText = "";
            tdAdditional5.InnerText = "";
            tdAdditional6.InnerText = "";
            tdAdditional7.InnerText = "";
            tdAdditional8.InnerText = "";
            tdAdditional9.InnerText = "";
            tdAdditional10.InnerText = "";
            #endregion
        }
        catch (Exception ex)
        { }
    }
    private void setAccompanyingPersonInfo(Repeater rptItem, int j, int accomCount, bool isCheck)
    {
        setaccomTextBoxesTrsFalse(rptItem, j);
        try
        {
            #region getControls
            HtmlTable tblAccom = (HtmlTable)rptItem.Items[j].FindControl("tblAccom");
            TextBox txtFirstName1 = (TextBox)rptItem.Items[j].FindControl("txtFirstName1");
            TextBox txtFirstName2 = (TextBox)rptItem.Items[j].FindControl("txtFirstName2");
            TextBox txtFirstName3 = (TextBox)rptItem.Items[j].FindControl("txtFirstName3");
            TextBox txtFirstName4 = (TextBox)rptItem.Items[j].FindControl("txtFirstName4");
            TextBox txtFirstName5 = (TextBox)rptItem.Items[j].FindControl("txtFirstName5");
            TextBox txtFirstName6 = (TextBox)rptItem.Items[j].FindControl("txtFirstName6");
            TextBox txtFirstName7 = (TextBox)rptItem.Items[j].FindControl("txtFirstName7");
            TextBox txtFirstName8 = (TextBox)rptItem.Items[j].FindControl("txtFirstName8");
            TextBox txtFirstName9 = (TextBox)rptItem.Items[j].FindControl("txtFirstName9");
            TextBox txtFirstName10 = (TextBox)rptItem.Items[j].FindControl("txtFirstName10");

            TextBox txtCompany1 = (TextBox)rptItem.Items[j].FindControl("txtCompany1");
            TextBox txtCompany2 = (TextBox)rptItem.Items[j].FindControl("txtCompany2");
            TextBox txtCompany3 = (TextBox)rptItem.Items[j].FindControl("txtCompany3");
            TextBox txtCompany4 = (TextBox)rptItem.Items[j].FindControl("txtCompany4");
            TextBox txtCompany5 = (TextBox)rptItem.Items[j].FindControl("txtCompany5");
            TextBox txtCompany6 = (TextBox)rptItem.Items[j].FindControl("txtCompany6");
            TextBox txtCompany7 = (TextBox)rptItem.Items[j].FindControl("txtCompany7");
            TextBox txtCompany8 = (TextBox)rptItem.Items[j].FindControl("txtCompany8");
            TextBox txtCompany9 = (TextBox)rptItem.Items[j].FindControl("txtCompany9");
            TextBox txtCompany10 = (TextBox)rptItem.Items[j].FindControl("txtCompany10");

            DropDownList ddlCountry1 = (DropDownList)rptItem.Items[j].FindControl("ddlCountry1");
            DropDownList ddlCountry2 = (DropDownList)rptItem.Items[j].FindControl("ddlCountry2");
            DropDownList ddlCountry3 = (DropDownList)rptItem.Items[j].FindControl("ddlCountry3");
            DropDownList ddlCountry4 = (DropDownList)rptItem.Items[j].FindControl("ddlCountry4");
            DropDownList ddlCountry5 = (DropDownList)rptItem.Items[j].FindControl("ddlCountry5");
            DropDownList ddlCountry6 = (DropDownList)rptItem.Items[j].FindControl("ddlCountry6");
            DropDownList ddlCountry7 = (DropDownList)rptItem.Items[j].FindControl("ddlCountry7");
            DropDownList ddlCountry8 = (DropDownList)rptItem.Items[j].FindControl("ddlCountry8");
            DropDownList ddlCountry9 = (DropDownList)rptItem.Items[j].FindControl("ddlCountry9");
            DropDownList ddlCountry10 = (DropDownList)rptItem.Items[j].FindControl("ddlCountry10");

            TextBox txtAdditional1 = (TextBox)rptItem.Items[j].FindControl("txtAdditional1");
            TextBox txtAdditional2 = (TextBox)rptItem.Items[j].FindControl("txtAdditional2");
            TextBox txtAdditional3 = (TextBox)rptItem.Items[j].FindControl("txtAdditional3");
            TextBox txtAdditional4 = (TextBox)rptItem.Items[j].FindControl("txtAdditional4");
            TextBox txtAdditional5 = (TextBox)rptItem.Items[j].FindControl("txtAdditional5");
            TextBox txtAdditional6 = (TextBox)rptItem.Items[j].FindControl("txtAdditional6");
            TextBox txtAdditional7 = (TextBox)rptItem.Items[j].FindControl("txtAdditional7");
            TextBox txtAdditional8 = (TextBox)rptItem.Items[j].FindControl("txtAdditional8");
            TextBox txtAdditional9 = (TextBox)rptItem.Items[j].FindControl("txtAdditional9");
            TextBox txtAdditional10 = (TextBox)rptItem.Items[j].FindControl("txtAdditional10");

            RequiredFieldValidator vcFirstName1 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcFirstName1");
            RequiredFieldValidator vcFirstName2 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcFirstName2");
            RequiredFieldValidator vcFirstName3 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcFirstName3");
            RequiredFieldValidator vcFirstName4 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcFirstName4");
            RequiredFieldValidator vcFirstName5 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcFirstName5");
            RequiredFieldValidator vcFirstName6 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcFirstName6");
            RequiredFieldValidator vcFirstName7 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcFirstName7");
            RequiredFieldValidator vcFirstName8 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcFirstName8");
            RequiredFieldValidator vcFirstName9 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcFirstName9");
            RequiredFieldValidator vcFirstName10 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcFirstName10");

            RequiredFieldValidator vcCompany1 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcCompany1");
            RequiredFieldValidator vcCompany2 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcCompany2");
            RequiredFieldValidator vcCompany3 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcCompany3");
            RequiredFieldValidator vcCompany4 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcCompany4");
            RequiredFieldValidator vcCompany5 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcCompany5");
            RequiredFieldValidator vcCompany6 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcCompany6");
            RequiredFieldValidator vcCompany7 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcCompany7");
            RequiredFieldValidator vcCompany8 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcCompany8");
            RequiredFieldValidator vcCompany9 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcCompany9");
            RequiredFieldValidator vcCompany10 = (RequiredFieldValidator)rptItem.Items[j].FindControl("vcCompany10");

            CompareValidator vcCountry1 = (CompareValidator)rptItem.Items[j].FindControl("vcCountry1");
            CompareValidator vcCountry2 = (CompareValidator)rptItem.Items[j].FindControl("vcCountry2");
            CompareValidator vcCountry3 = (CompareValidator)rptItem.Items[j].FindControl("vcCountry3");
            CompareValidator vcCountry4 = (CompareValidator)rptItem.Items[j].FindControl("vcCountry4");
            CompareValidator vcCountry5 = (CompareValidator)rptItem.Items[j].FindControl("vcCountry5");
            CompareValidator vcCountry6 = (CompareValidator)rptItem.Items[j].FindControl("vcCountry6");
            CompareValidator vcCountry7 = (CompareValidator)rptItem.Items[j].FindControl("vcCountry7");
            CompareValidator vcCountry8 = (CompareValidator)rptItem.Items[j].FindControl("vcCountry8");
            CompareValidator vcCountry9 = (CompareValidator)rptItem.Items[j].FindControl("vcCountry9");
            CompareValidator vcCountry10 = (CompareValidator)rptItem.Items[j].FindControl("vcCountry10");

            HtmlTableRow trAccFullName1 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName1");
            HtmlTableRow trAccFullName2 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName2");
            HtmlTableRow trAccFullName3 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName3");
            HtmlTableRow trAccFullName4 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName4");
            HtmlTableRow trAccFullName5 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName5");
            HtmlTableRow trAccFullName6 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName6");
            HtmlTableRow trAccFullName7 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName7");
            HtmlTableRow trAccFullName8 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName8");
            HtmlTableRow trAccFullName9 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName9");
            HtmlTableRow trAccFullName10 = (HtmlTableRow)rptItem.Items[j].FindControl("trAccFullName10");

            HtmlTableRow trFullName1 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName1");
            HtmlTableRow trFullName2 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName2");
            HtmlTableRow trFullName3 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName3");
            HtmlTableRow trFullName4 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName4");
            HtmlTableRow trFullName5 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName5");
            HtmlTableRow trFullName6 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName6");
            HtmlTableRow trFullName7 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName7");
            HtmlTableRow trFullName8 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName8");
            HtmlTableRow trFullName9 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName9");
            HtmlTableRow trFullName10 = (HtmlTableRow)rptItem.Items[j].FindControl("trFullName10");

            HtmlTableRow trCompany1 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany1");
            HtmlTableRow trCompany2 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany2");
            HtmlTableRow trCompany3 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany3");
            HtmlTableRow trCompany4 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany4");
            HtmlTableRow trCompany5 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany5");
            HtmlTableRow trCompany6 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany6");
            HtmlTableRow trCompany7 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany7");
            HtmlTableRow trCompany8 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany8");
            HtmlTableRow trCompany9 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany9");
            HtmlTableRow trCompany10 = (HtmlTableRow)rptItem.Items[j].FindControl("trCompany10");

            HtmlTableRow trCountry1 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry1");
            HtmlTableRow trCountry2 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry2");
            HtmlTableRow trCountry3 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry3");
            HtmlTableRow trCountry4 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry4");
            HtmlTableRow trCountry5 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry5");
            HtmlTableRow trCountry6 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry6");
            HtmlTableRow trCountry7 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry7");
            HtmlTableRow trCountry8 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry8");
            HtmlTableRow trCountry9 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry9");
            HtmlTableRow trCountry10 = (HtmlTableRow)rptItem.Items[j].FindControl("trCountry10");

            Label lblAccCountry1 = (Label)rptItem.Items[j].FindControl("lblAccCountry1");
            Label lblAccCountry2 = (Label)rptItem.Items[j].FindControl("lblAccCountry2");
            Label lblAccCountry3 = (Label)rptItem.Items[j].FindControl("lblAccCountry3");
            Label lblAccCountry4 = (Label)rptItem.Items[j].FindControl("lblAccCountry4");
            Label lblAccCountry5 = (Label)rptItem.Items[j].FindControl("lblAccCountry5");
            Label lblAccCountry6 = (Label)rptItem.Items[j].FindControl("lblAccCountry6");
            Label lblAccCountry7 = (Label)rptItem.Items[j].FindControl("lblAccCountry7");
            Label lblAccCountry8 = (Label)rptItem.Items[j].FindControl("lblAccCountry8");
            Label lblAccCountry9 = (Label)rptItem.Items[j].FindControl("lblAccCountry9");
            Label lblAccCountry10 = (Label)rptItem.Items[j].FindControl("lblAccCountry10");

            #region Additional
            HtmlTableRow trAdditional1 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional1");
            HtmlTableRow trAdditional2 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional2");
            HtmlTableRow trAdditional3 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional3");
            HtmlTableRow trAdditional4 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional4");
            HtmlTableRow trAdditional5 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional5");
            HtmlTableRow trAdditional6 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional6");
            HtmlTableRow trAdditional7 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional7");
            HtmlTableRow trAdditional8 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional8");
            HtmlTableRow trAdditional9 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional9");
            HtmlTableRow trAdditional10 = (HtmlTableRow)rptItem.Items[j].FindControl("trAdditional10");

            HtmlTableCell tdAdditional1 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional1");
            HtmlTableCell tdAdditional2 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional2");
            HtmlTableCell tdAdditional3 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional3");
            HtmlTableCell tdAdditional4 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional4");
            HtmlTableCell tdAdditional5 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional5");
            HtmlTableCell tdAdditional6 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional6");
            HtmlTableCell tdAdditional7 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional7");
            HtmlTableCell tdAdditional8 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional8");
            HtmlTableCell tdAdditional9 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional9");
            HtmlTableCell tdAdditional10 = (HtmlTableCell)rptItem.Items[j].FindControl("tdAdditional10");
            #endregion
            #endregion

            if (isCheck)
            {
                #region check
                tblAccom.Visible = true;
                FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
                string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
                SiteSettings sCong = new SiteSettings(fn, showid);
                sCong.LoadBaseSiteProperties(showid);
                string accomAdditionalValue = sCong.AccompanyingPersonAdditionalFieldIsUsed;

                /*changed for SG-ANZICS Area of Speciality ddl (10-2-2019)*/
                CommonDataObj cmdObj = new CommonDataObj(fn);
                DataSet dsRefAdditionalList_ConfDepInfoDDL = cmdObj.getRefAdditionalList(showid, RefAdditionalListType.ConfDependentItemDDLInfoFld);
                if (dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows.Count > 0)
                {
                    string ddlLabel = "";
                    #region SG-ANZICS Area of Expertise Only
                    for (int n = 0; n < dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows.Count; n++)
                    {
                        ddlLabel = dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_Label"] != DBNull.Value ? dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_Label"].ToString() : "";

                        ddlCountry1.Items.Add(dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_name"].ToString());
                        ddlCountry1.Items[n + 1].Value = dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_id"].ToString();

                        ddlCountry2.Items.Add(dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_name"].ToString());
                        ddlCountry2.Items[n + 1].Value = dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_id"].ToString();

                        ddlCountry3.Items.Add(dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_name"].ToString());
                        ddlCountry3.Items[n + 1].Value = dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_id"].ToString();

                        ddlCountry4.Items.Add(dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_name"].ToString());
                        ddlCountry4.Items[n + 1].Value = dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_id"].ToString();

                        ddlCountry5.Items.Add(dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_name"].ToString());
                        ddlCountry5.Items[n + 1].Value = dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_id"].ToString();

                        ddlCountry6.Items.Add(dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_name"].ToString());
                        ddlCountry6.Items[n + 1].Value = dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_id"].ToString();

                        ddlCountry7.Items.Add(dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_name"].ToString());
                        ddlCountry7.Items[n + 1].Value = dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_id"].ToString();

                        ddlCountry8.Items.Add(dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_name"].ToString());
                        ddlCountry8.Items[n + 1].Value = dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_id"].ToString();

                        ddlCountry9.Items.Add(dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_name"].ToString());
                        ddlCountry9.Items[n + 1].Value = dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_id"].ToString();

                        ddlCountry10.Items.Add(dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_name"].ToString());
                        ddlCountry10.Items[n + 1].Value = dsRefAdditionalList_ConfDepInfoDDL.Tables[0].Rows[n]["refAdd_id"].ToString();
                    }

                    #region setEnabled
                    int accCount = lstPendingAcc != null ? lstPendingAcc.Count : 0;
                    for (int k = 1; k <= accomCount; k++)
                    {
                        if (k == 1)
                        {
                            trAccFullName1.Visible = true;
                            trCountry1.Visible = true;
                            vcCountry1.Enabled = true;//*
                            if(!string.IsNullOrEmpty(ddlLabel))
                            {
                                lblAccCountry1.Text = ddlLabel;
                            }
                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry1.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry1.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                        else if (k == 2)
                        {
                            trAccFullName2.Visible = true;
                            trCountry2.Visible = true;
                            vcCountry2.Enabled = true;//*
                            if (!string.IsNullOrEmpty(ddlLabel))
                            {
                                lblAccCountry2.Text = ddlLabel;
                            }
                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry2.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry2.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                        else if (k == 3)
                        {
                            trAccFullName3.Visible = true;
                            trCountry3.Visible = true;
                            vcCountry3.Enabled = true;//*
                            if (!string.IsNullOrEmpty(ddlLabel))
                            {
                                lblAccCountry3.Text = ddlLabel;
                            }
                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry3.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry3.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                        else if (k == 4)
                        {
                            trAccFullName4.Visible = true;
                            trCountry4.Visible = true;
                            vcCountry4.Enabled = true;//*
                            if (!string.IsNullOrEmpty(ddlLabel))
                            {
                                lblAccCountry4.Text = ddlLabel;
                            }
                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry4.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry4.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                        else if (k == 5)
                        {
                            trAccFullName5.Visible = true;
                            trCountry5.Visible = true;
                            vcCountry5.Enabled = true;//*
                            if (!string.IsNullOrEmpty(ddlLabel))
                            {
                                lblAccCountry5.Text = ddlLabel;
                            }
                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry5.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry5.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                        else if (k == 6)
                        {
                            trAccFullName6.Visible = true;
                            trCountry6.Visible = true;
                            vcCountry6.Enabled = true;//*
                            if (!string.IsNullOrEmpty(ddlLabel))
                            {
                                lblAccCountry6.Text = ddlLabel;
                            }
                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry6.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry6.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                        else if (k == 7)
                        {
                            trAccFullName7.Visible = true;
                            trCountry7.Visible = true;
                            vcCountry7.Enabled = true;//*
                            if (!string.IsNullOrEmpty(ddlLabel))
                            {
                                lblAccCountry7.Text = ddlLabel;
                            }
                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry7.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry7.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                        else if (k == 8)
                        {
                            trAccFullName8.Visible = true;
                            trCountry8.Visible = true;
                            vcCountry8.Enabled = true;//*
                            if (!string.IsNullOrEmpty(ddlLabel))
                            {
                                lblAccCountry8.Text = ddlLabel;
                            }
                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry8.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry8.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                        else if (k == 9)
                        {
                            trAccFullName9.Visible = true;
                            trCountry9.Visible = true;
                            vcCountry9.Enabled = true;//*
                            if (!string.IsNullOrEmpty(ddlLabel))
                            {
                                lblAccCountry9.Text = ddlLabel;
                            }
                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry9.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry9.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                        else if (k == 10)
                        {
                            trAccFullName10.Visible = true;
                            trCountry10.Visible = true;
                            vcCountry10.Enabled = true;//*
                            if (!string.IsNullOrEmpty(ddlLabel))
                            {
                                lblAccCountry10.Text = ddlLabel;
                            }
                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry10.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry10.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #endregion
                }
                else/*changed for SG-ANZICS Area of Speciality ddl (10-2-2019)*/
                {
                    #region Accompanying Person Info
                    DataSet dsCountry = fn.GetDatasetByCommand("Select * From ref_Country Where Countryen != 'Countrycode' order by Country", "DsCountry");
                    if (dsCountry.Tables[0].Rows.Count > 0)
                    {
                        for (int n = 0; n < dsCountry.Tables[0].Rows.Count; n++)
                        {
                            ddlCountry1.Items.Add(dsCountry.Tables[0].Rows[n]["Country"].ToString());
                            ddlCountry1.Items[n + 1].Value = dsCountry.Tables[0].Rows[n]["Cty_GUID"].ToString();

                            ddlCountry2.Items.Add(dsCountry.Tables[0].Rows[n]["Country"].ToString());
                            ddlCountry2.Items[n + 1].Value = dsCountry.Tables[0].Rows[n]["Cty_GUID"].ToString();

                            ddlCountry3.Items.Add(dsCountry.Tables[0].Rows[n]["Country"].ToString());
                            ddlCountry3.Items[n + 1].Value = dsCountry.Tables[0].Rows[n]["Cty_GUID"].ToString();

                            ddlCountry4.Items.Add(dsCountry.Tables[0].Rows[n]["Country"].ToString());
                            ddlCountry4.Items[n + 1].Value = dsCountry.Tables[0].Rows[n]["Cty_GUID"].ToString();

                            ddlCountry5.Items.Add(dsCountry.Tables[0].Rows[n]["Country"].ToString());
                            ddlCountry5.Items[n + 1].Value = dsCountry.Tables[0].Rows[n]["Cty_GUID"].ToString();

                            ddlCountry6.Items.Add(dsCountry.Tables[0].Rows[n]["Country"].ToString());
                            ddlCountry6.Items[n + 1].Value = dsCountry.Tables[0].Rows[n]["Cty_GUID"].ToString();

                            ddlCountry7.Items.Add(dsCountry.Tables[0].Rows[n]["Country"].ToString());
                            ddlCountry7.Items[n + 1].Value = dsCountry.Tables[0].Rows[n]["Cty_GUID"].ToString();

                            ddlCountry8.Items.Add(dsCountry.Tables[0].Rows[n]["Country"].ToString());
                            ddlCountry8.Items[n + 1].Value = dsCountry.Tables[0].Rows[n]["Cty_GUID"].ToString();

                            ddlCountry9.Items.Add(dsCountry.Tables[0].Rows[n]["Country"].ToString());
                            ddlCountry9.Items[n + 1].Value = dsCountry.Tables[0].Rows[n]["Cty_GUID"].ToString();

                            ddlCountry10.Items.Add(dsCountry.Tables[0].Rows[n]["Country"].ToString());
                            ddlCountry10.Items[n + 1].Value = dsCountry.Tables[0].Rows[n]["Cty_GUID"].ToString();
                        }
                    }

                    #region setEnabled
                    int accCount = lstPendingAcc != null ? lstPendingAcc.Count : 0;
                    for (int k = 1; k <= accomCount; k++)
                    {
                        if (k == 1)
                        {
                            trAccFullName1.Visible = true;
                            trFullName1.Visible = true;
                            trCompany1.Visible = true;
                            trCountry1.Visible = true;
                            vcFirstName1.Enabled = true;

                            vcCompany1.Enabled = false;//*
                            vcCountry1.Enabled = false;//*

                            if (!string.IsNullOrEmpty(accomAdditionalValue))
                            {
                                trAdditional1.Visible = true;
                                tdAdditional1.InnerText = accomAdditionalValue;
                            }

                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                txtFirstName1.Text = lstPendingAcc[k - 1].accom_FullName;
                                txtCompany1.Text = lstPendingAcc[k - 1].accom_CompanyName;

                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry1.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry1.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                                txtAdditional1.Text = lstPendingAcc[k - 1].accom_selectedMeal;
                            }
                        }
                        else if (k == 2)
                        {
                            trAccFullName2.Visible = true;
                            trFullName2.Visible = true;
                            trCompany2.Visible = true;
                            trCountry2.Visible = true;
                            vcFirstName2.Enabled = true;

                            vcCompany2.Enabled = false;//*
                            vcCountry2.Enabled = false;//*

                            if (!string.IsNullOrEmpty(accomAdditionalValue))
                            {
                                trAdditional2.Visible = true;
                                tdAdditional2.InnerText = accomAdditionalValue;
                            }

                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                txtFirstName2.Text = lstPendingAcc[k - 1].accom_FullName;
                                txtCompany2.Text = lstPendingAcc[k - 1].accom_CompanyName;

                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry2.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry2.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                                txtAdditional2.Text = lstPendingAcc[k - 1].accom_selectedMeal;
                            }
                        }
                        else if (k == 3)
                        {
                            trAccFullName3.Visible = true;
                            trFullName3.Visible = true;
                            trCompany3.Visible = true;
                            trCountry3.Visible = true;
                            vcFirstName3.Enabled = true;

                            vcCompany3.Enabled = false;//*
                            vcCountry3.Enabled = false;//*

                            if (!string.IsNullOrEmpty(accomAdditionalValue))
                            {
                                trAdditional3.Visible = true;
                                tdAdditional4.InnerText = accomAdditionalValue;
                            }

                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                txtFirstName3.Text = lstPendingAcc[k - 1].accom_FullName;
                                txtCompany3.Text = lstPendingAcc[k - 1].accom_CompanyName;

                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry3.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry3.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                                txtAdditional3.Text = lstPendingAcc[k - 1].accom_selectedMeal;
                            }
                        }
                        else if (k == 4)
                        {
                            trAccFullName4.Visible = true;
                            trFullName4.Visible = true;
                            trCompany4.Visible = true;
                            trCountry4.Visible = true;
                            vcFirstName4.Enabled = true;

                            vcCompany4.Enabled = false;//*
                            vcCountry4.Enabled = false;//*

                            if (!string.IsNullOrEmpty(accomAdditionalValue))
                            {
                                trAdditional4.Visible = true;
                                tdAdditional4.InnerText = accomAdditionalValue;
                            }

                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                txtFirstName4.Text = lstPendingAcc[k - 1].accom_FullName;
                                txtCompany4.Text = lstPendingAcc[k - 1].accom_CompanyName;

                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry4.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry4.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                                txtAdditional4.Text = lstPendingAcc[k - 1].accom_selectedMeal;
                            }
                        }
                        else if (k == 5)
                        {
                            trAccFullName5.Visible = true;
                            trFullName5.Visible = true;
                            trCompany5.Visible = true;
                            trCountry5.Visible = true;
                            vcFirstName5.Enabled = true;

                            vcCompany5.Enabled = false;//*
                            vcCountry5.Enabled = false;//*

                            if (!string.IsNullOrEmpty(accomAdditionalValue))
                            {
                                trAdditional5.Visible = true;
                                tdAdditional5.InnerText = accomAdditionalValue;
                            }

                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                txtFirstName5.Text = lstPendingAcc[k - 1].accom_FullName;
                                txtCompany5.Text = lstPendingAcc[k - 1].accom_CompanyName;

                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry5.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry5.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                                txtAdditional5.Text = lstPendingAcc[k - 1].accom_selectedMeal;
                            }
                        }
                        else if (k == 6)
                        {
                            trAccFullName6.Visible = true;
                            trFullName6.Visible = true;
                            trCompany6.Visible = true;
                            trCountry6.Visible = true;
                            vcFirstName6.Enabled = true;

                            vcCompany6.Enabled = false;//*
                            vcCountry6.Enabled = false;//*

                            if (!string.IsNullOrEmpty(accomAdditionalValue))
                            {
                                trAdditional6.Visible = true;
                                tdAdditional6.InnerText = accomAdditionalValue;
                            }

                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                txtFirstName6.Text = lstPendingAcc[k - 1].accom_FullName;
                                txtCompany6.Text = lstPendingAcc[k - 1].accom_CompanyName;

                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry6.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry6.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                                txtAdditional6.Text = lstPendingAcc[k - 1].accom_selectedMeal;
                            }
                        }
                        else if (k == 7)
                        {
                            trAccFullName7.Visible = true;
                            trFullName7.Visible = true;
                            trCompany7.Visible = true;
                            trCountry7.Visible = true;
                            vcFirstName7.Enabled = true;

                            vcCompany7.Enabled = false;//*
                            vcCountry7.Enabled = false;//*

                            if (!string.IsNullOrEmpty(accomAdditionalValue))
                            {
                                trAdditional7.Visible = true;
                                tdAdditional7.InnerText = accomAdditionalValue;
                            }

                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                txtFirstName7.Text = lstPendingAcc[k - 1].accom_FullName;
                                txtCompany7.Text = lstPendingAcc[k - 1].accom_CompanyName;

                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry7.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry7.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                                txtAdditional7.Text = lstPendingAcc[k - 1].accom_selectedMeal;
                            }
                        }
                        else if (k == 8)
                        {
                            trAccFullName8.Visible = true;
                            trFullName8.Visible = true;
                            trCompany8.Visible = true;
                            trCountry8.Visible = true;
                            vcFirstName8.Enabled = true;

                            vcCompany8.Enabled = false;//*
                            vcCountry8.Enabled = false;//*

                            if (!string.IsNullOrEmpty(accomAdditionalValue))
                            {
                                trAdditional8.Visible = true;
                                tdAdditional8.InnerText = accomAdditionalValue;
                            }

                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                txtFirstName8.Text = lstPendingAcc[k - 1].accom_FullName;
                                txtCompany8.Text = lstPendingAcc[k - 1].accom_CompanyName;

                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry8.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry8.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                                txtAdditional8.Text = lstPendingAcc[k - 1].accom_selectedMeal;
                            }
                        }
                        else if (k == 9)
                        {
                            trAccFullName9.Visible = true;
                            trFullName9.Visible = true;
                            trCompany9.Visible = true;
                            trCountry9.Visible = true;
                            vcFirstName9.Enabled = true;

                            vcCompany9.Enabled = false;//*
                            vcCountry9.Enabled = false;//*

                            if (!string.IsNullOrEmpty(accomAdditionalValue))
                            {
                                trAdditional9.Visible = true;
                                tdAdditional9.InnerText = accomAdditionalValue;
                            }

                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                txtFirstName9.Text = lstPendingAcc[k - 1].accom_FullName;
                                txtCompany9.Text = lstPendingAcc[k - 1].accom_CompanyName;

                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry9.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry9.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                                txtAdditional9.Text = lstPendingAcc[k - 1].accom_selectedMeal;
                            }
                        }
                        else if (k == 10)
                        {
                            trAccFullName10.Visible = true;
                            trFullName10.Visible = true;
                            trCompany10.Visible = true;
                            trCountry10.Visible = true;
                            vcFirstName10.Enabled = true;

                            vcCompany10.Enabled = false;//*
                            vcCountry10.Enabled = false;//*

                            if (!string.IsNullOrEmpty(accomAdditionalValue))
                            {
                                trAdditional10.Visible = true;
                                tdAdditional10.InnerText = accomAdditionalValue;
                            }

                            if (accCount != 0 && (accCount <= accomCount || accCount >= accomCount))
                            {
                                txtFirstName10.Text = lstPendingAcc[k - 1].accom_FullName;
                                txtCompany10.Text = lstPendingAcc[k - 1].accom_CompanyName;

                                string country = lstPendingAcc[k - 1].accom_Country;
                                if (!String.IsNullOrEmpty(country))
                                {
                                    ListItem listItem = ddlCountry10.Items.FindByText(country);
                                    if (listItem != null)
                                    {
                                        ddlCountry10.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                                txtAdditional10.Text = lstPendingAcc[k - 1].accom_selectedMeal;
                            }
                        }
                    }
                    #endregion
                    #endregion
                }
                #endregion
            }
            else
            {
                #region un-check
                txtFirstName1.Text = "";
                txtFirstName2.Text = "";
                txtFirstName3.Text = "";
                txtFirstName4.Text = "";
                txtFirstName5.Text = "";
                txtFirstName6.Text = "";
                txtFirstName7.Text = "";
                txtFirstName8.Text = "";
                txtFirstName9.Text = "";
                txtFirstName10.Text = "";

                txtCompany1.Text = "";
                txtCompany2.Text = "";
                txtCompany3.Text = "";
                txtCompany4.Text = "";
                txtCompany5.Text = "";
                txtCompany6.Text = "";
                txtCompany7.Text = "";
                txtCompany8.Text = "";
                txtCompany9.Text = "";
                txtCompany10.Text = "";

                ddlCountry1.SelectedIndex = 0;
                ddlCountry2.SelectedIndex = 0;
                ddlCountry3.SelectedIndex = 0;
                ddlCountry4.SelectedIndex = 0;
                ddlCountry5.SelectedIndex = 0;
                ddlCountry6.SelectedIndex = 0;
                ddlCountry7.SelectedIndex = 0;
                ddlCountry8.SelectedIndex = 0;
                ddlCountry9.SelectedIndex = 0;
                ddlCountry10.SelectedIndex = 0;

                txtAdditional1.Text = "";
                txtAdditional2.Text = "";
                txtAdditional3.Text = "";
                txtAdditional4.Text = "";
                txtAdditional5.Text = "";
                txtAdditional6.Text = "";
                txtAdditional7.Text = "";
                txtAdditional8.Text = "";
                txtAdditional9.Text = "";
                txtAdditional10.Text = "";

                tblAccom.Visible = false;
                vcFirstName1.Enabled = false;
                vcFirstName2.Enabled = false;
                vcFirstName3.Enabled = false;
                vcFirstName4.Enabled = false;
                vcFirstName5.Enabled = false;
                vcFirstName6.Enabled = false;
                vcFirstName7.Enabled = false;
                vcFirstName8.Enabled = false;
                vcFirstName9.Enabled = false;
                vcFirstName10.Enabled = false;

                vcCompany1.Enabled = false;
                vcCompany2.Enabled = false;
                vcCompany3.Enabled = false;
                vcCompany4.Enabled = false;
                vcCompany5.Enabled = false;
                vcCompany6.Enabled = false;
                vcCompany7.Enabled = false;
                vcCompany8.Enabled = false;
                vcCompany9.Enabled = false;
                vcCompany10.Enabled = false;

                vcCountry1.Enabled = false;
                vcCountry2.Enabled = false;
                vcCountry3.Enabled = false;
                vcCountry4.Enabled = false;
                vcCountry5.Enabled = false;
                vcCountry6.Enabled = false;
                vcCountry7.Enabled = false;
                vcCountry8.Enabled = false;
                vcCountry9.Enabled = false;
                vcCountry10.Enabled = false;
                #endregion
            }
        }
        catch(Exception ex)
        { }
    }
    #endregion
    private OrderItemList GetSelectedItems(string ConfType, ref Repeater Rpt, ref decimal ToalSum)
    {
        OrderItemList oList = new OrderItemList();
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string delegateRegno = cComFuz.DecryptValue(urlQuery.DelegateID);
        decimal total = 0;
        for (int i = 0; i < Rpt.Items.Count; i++)
        {
            DropDownList ddlQty = (DropDownList)Rpt.Items[i].FindControl("ddlQty");
            if (ddlQty.Enabled == true)//***add on 16-3-2018
            {
                if (cComFuz.ParseInt(ddlQty.SelectedValue) > 0)
                {
                    Label lblDisplay = (Label)Rpt.Items[i].FindControl("lblDisplay");
                    TextBox txtConID = (TextBox)Rpt.Items[i].FindControl("txtConfItemID");
                    TextBox txtPrice = (TextBox)Rpt.Items[i].FindControl("txtConfPrice");
                    //Panel pnlShowSelect = (Panel)Rpt.Items[i].FindControl("PanelShowSelect");
                    Panel pnlMsg = (Panel)Rpt.Items[i].FindControl("PanleMsg");
                    //Label lblMsg = (Label)Rpt.Items[i].FindControl("lblMsg");

                    if (pnlMsg.Visible == false)
                    {
                        OrderItem oItem = new OrderItem();
                        oItem.ItemID = txtConID.Text.Trim();
                        oItem.Qty = cComFuz.ParseInt(ddlQty.SelectedValue.Trim());
                        oItem.Price = cComFuz.ParseDecimal(txtPrice.Text.Trim());
                        oItem.ItemType = ConfType;
                        oItem.ItemDescription = lblDisplay.Text;
                        total += oItem.Qty * oItem.Price;

                        #region Accompanying Person Info
                        if (Rpt == RptDependentList)
                        {
                            TextBox txtConfType = (TextBox)Rpt.Items[i].FindControl("txtConfType");
                            HtmlTable tblAccom = (HtmlTable)Rpt.Items[i].FindControl("tblAccom");
                            if (txtConfType.Text == ConfDefaultValue.conf_AccompanyingPersonItem)
                            {
                                if (tblAccom.Visible == true)
                                {
                                    #region Accompanying Controls
                                    TextBox txtFirstName1 = (TextBox)Rpt.Items[i].FindControl("txtFirstName1");
                                    TextBox txtFirstName2 = (TextBox)Rpt.Items[i].FindControl("txtFirstName2");
                                    TextBox txtFirstName3 = (TextBox)Rpt.Items[i].FindControl("txtFirstName3");
                                    TextBox txtFirstName4 = (TextBox)Rpt.Items[i].FindControl("txtFirstName4");
                                    TextBox txtFirstName5 = (TextBox)Rpt.Items[i].FindControl("txtFirstName5");
                                    TextBox txtFirstName6 = (TextBox)Rpt.Items[i].FindControl("txtFirstName6");
                                    TextBox txtFirstName7 = (TextBox)Rpt.Items[i].FindControl("txtFirstName7");
                                    TextBox txtFirstName8 = (TextBox)Rpt.Items[i].FindControl("txtFirstName8");
                                    TextBox txtFirstName9 = (TextBox)Rpt.Items[i].FindControl("txtFirstName9");
                                    TextBox txtFirstName10 = (TextBox)Rpt.Items[i].FindControl("txtFirstName10");

                                    TextBox txtCompany1 = (TextBox)Rpt.Items[i].FindControl("txtCompany1");
                                    TextBox txtCompany2 = (TextBox)Rpt.Items[i].FindControl("txtCompany2");
                                    TextBox txtCompany3 = (TextBox)Rpt.Items[i].FindControl("txtCompany3");
                                    TextBox txtCompany4 = (TextBox)Rpt.Items[i].FindControl("txtCompany4");
                                    TextBox txtCompany5 = (TextBox)Rpt.Items[i].FindControl("txtCompany5");
                                    TextBox txtCompany6 = (TextBox)Rpt.Items[i].FindControl("txtCompany6");
                                    TextBox txtCompany7 = (TextBox)Rpt.Items[i].FindControl("txtCompany7");
                                    TextBox txtCompany8 = (TextBox)Rpt.Items[i].FindControl("txtCompany8");
                                    TextBox txtCompany9 = (TextBox)Rpt.Items[i].FindControl("txtCompany9");
                                    TextBox txtCompany10 = (TextBox)Rpt.Items[i].FindControl("txtCompany10");

                                    DropDownList ddlCountry1 = (DropDownList)Rpt.Items[i].FindControl("ddlCountry1");
                                    DropDownList ddlCountry2 = (DropDownList)Rpt.Items[i].FindControl("ddlCountry2");
                                    DropDownList ddlCountry3 = (DropDownList)Rpt.Items[i].FindControl("ddlCountry3");
                                    DropDownList ddlCountry4 = (DropDownList)Rpt.Items[i].FindControl("ddlCountry4");
                                    DropDownList ddlCountry5 = (DropDownList)Rpt.Items[i].FindControl("ddlCountry5");
                                    DropDownList ddlCountry6 = (DropDownList)Rpt.Items[i].FindControl("ddlCountry6");
                                    DropDownList ddlCountry7 = (DropDownList)Rpt.Items[i].FindControl("ddlCountry7");
                                    DropDownList ddlCountry8 = (DropDownList)Rpt.Items[i].FindControl("ddlCountry8");
                                    DropDownList ddlCountry9 = (DropDownList)Rpt.Items[i].FindControl("ddlCountry9");
                                    DropDownList ddlCountry10 = (DropDownList)Rpt.Items[i].FindControl("ddlCountry10");

                                    TextBox txtAdditional1 = (TextBox)Rpt.Items[i].FindControl("txtAdditional1");
                                    TextBox txtAdditional2 = (TextBox)Rpt.Items[i].FindControl("txtAdditional2");
                                    TextBox txtAdditional3 = (TextBox)Rpt.Items[i].FindControl("txtAdditional3");
                                    TextBox txtAdditional4 = (TextBox)Rpt.Items[i].FindControl("txtAdditional4");
                                    TextBox txtAdditional5 = (TextBox)Rpt.Items[i].FindControl("txtAdditional5");
                                    TextBox txtAdditional6 = (TextBox)Rpt.Items[i].FindControl("txtAdditional6");
                                    TextBox txtAdditional7 = (TextBox)Rpt.Items[i].FindControl("txtAdditional7");
                                    TextBox txtAdditional8 = (TextBox)Rpt.Items[i].FindControl("txtAdditional8");
                                    TextBox txtAdditional9 = (TextBox)Rpt.Items[i].FindControl("txtAdditional9");
                                    TextBox txtAdditional10 = (TextBox)Rpt.Items[i].FindControl("txtAdditional10");
                                    #endregion

                                    int selectedcount = cComFuz.ParseInt(ddlQty.SelectedValue.Trim());
                                    string firstname = string.Empty;
                                    string companyname = string.Empty;
                                    string additionalname = string.Empty;
                                    string countryname = string.Empty;
                                    string additionalname2 = string.Empty;
                                    for (int k = 1; k <= selectedcount; k++)
                                    {
                                        if (k == 1)
                                        {
                                            firstname = txtFirstName1.Text.Trim();
                                            companyname = txtCompany1.Text.Trim();
                                            countryname = ddlCountry1.SelectedItem.Text;
                                            additionalname = txtAdditional1.Text.Trim();
                                        }
                                        else if (k == 2)
                                        {
                                            firstname = txtFirstName2.Text.Trim();
                                            companyname = txtCompany2.Text.Trim();
                                            countryname = ddlCountry2.SelectedItem.Text;
                                            additionalname = txtAdditional2.Text.Trim();
                                        }
                                        else if (k == 3)
                                        {
                                            firstname = txtFirstName3.Text.Trim();
                                            companyname = txtCompany3.Text.Trim();
                                            countryname = ddlCountry3.SelectedItem.Text;
                                            additionalname = txtAdditional3.Text.Trim();
                                        }
                                        else if (k == 4)
                                        {
                                            firstname = txtFirstName4.Text.Trim();
                                            companyname = txtCompany4.Text.Trim();
                                            countryname = ddlCountry4.SelectedItem.Text;
                                            additionalname = txtAdditional4.Text.Trim();
                                        }
                                        else if (k == 5)
                                        {
                                            firstname = txtFirstName5.Text.Trim();
                                            companyname = txtCompany5.Text.Trim();
                                            countryname = ddlCountry5.SelectedItem.Text;
                                            additionalname = txtAdditional5.Text.Trim();
                                        }
                                        else if (k == 6)
                                        {
                                            firstname = txtFirstName6.Text.Trim();
                                            companyname = txtCompany6.Text.Trim();
                                            countryname = ddlCountry6.SelectedItem.Text;
                                            additionalname = txtAdditional6.Text.Trim();
                                        }
                                        else if (k == 7)
                                        {
                                            firstname = txtFirstName7.Text.Trim();
                                            companyname = txtCompany7.Text.Trim();
                                            countryname = ddlCountry7.SelectedItem.Text;
                                            additionalname = txtAdditional7.Text.Trim();
                                        }
                                        else if (k == 8)
                                        {
                                            firstname = txtFirstName8.Text.Trim();
                                            companyname = txtCompany8.Text.Trim();
                                            countryname = ddlCountry8.SelectedItem.Text;
                                            additionalname = txtAdditional8.Text.Trim();
                                        }
                                        else if (k == 9)
                                        {
                                            firstname = txtFirstName9.Text.Trim();
                                            companyname = txtCompany9.Text.Trim();
                                            countryname = ddlCountry9.SelectedItem.Text;
                                            additionalname = txtAdditional9.Text.Trim();
                                        }
                                        else if (k == 10)
                                        {
                                            firstname = txtFirstName10.Text.Trim();
                                            companyname = txtCompany10.Text.Trim();
                                            countryname = ddlCountry10.SelectedItem.Text;
                                            additionalname = txtAdditional10.Text.Trim();
                                        }

                                        AccompanyingPersonItem accItem = new AccompanyingPersonItem();
                                        accItem.accom_FullName = firstname;
                                        accItem.accom_CompanyName = companyname;
                                        accItem.accom_Country = countryname;
                                        accItem.accom_serialno = k;
                                        accItem.accom_selectedMeal = additionalname;//"";//***changed on 6-12-2018
                                        /*added on 26-9-2019*/
                                        accItem.accom_AccompanyingPersonRegno = "A" + delegateRegno;// oCtrl.GenerateAccompanyingPersonRegno(delegateRegno, k.ToString());/*changed on 2-8-2019 according to the clients' pt V2 comment(slide14)*/
                                        accItem.accom_Additional2 = additionalname2;
                                        /*added on 26-9-2019*/
                                        oItem.AccomOrderList.Add(accItem);
                                    }
                                }
                            }
                        }
                        #endregion

                        if (ConfType == ConfDefaultValue.conf_DependentItem)
                        {
                            TextBox txtConfGroupID = (TextBox)Rpt.Items[i].FindControl("txtMustSelectConfGroupID");//(TextBox)Rpt.Items[i].FindControl("txtConfGroupID");
                            oItem.ConfGroupID = txtConfGroupID.Text;
                        }

                        oList.OrderList.Add(oItem);//***
                    }
                }
            }
        }
        ToalSum = total;
        return oList;
    }
    #region changeOrderQty (/*added for FTC*/)
    private void changeOrderQtyAsAllMemberCount(string groupid, string showid, ref OrderItemList oList)
    {
        try
        {
            if (oList.OrderList.Count > 0)
            {
                RegDelegateObj rgd = new RegDelegateObj(fn);
                DataTable dt = rgd.getRegDelegateByGroupID(groupid, showid);
                if (dt.Rows.Count > 0)
                {
                    OrderItem firstOrderItem = oList.OrderList.FirstOrDefault();
                    firstOrderItem.Qty = dt.Rows.Count;
                }
            }
        }
        catch(Exception ex)
        { }
    }
    private void changeOrderQtyForIndividualMember(string groupid, string showid, ref OrderItemList oList)
    {
        try
        {
            if (oList.OrderList.Count > 0)
            {
                RegDelegateObj rgd = new RegDelegateObj(fn);
                DataTable dt = rgd.getRegDelegateByGroupID(groupid, showid);
                if (dt.Rows.Count > 0)
                {
                    OrderItem firstOrderItem = oList.OrderList.FirstOrDefault();
                    firstOrderItem.Qty = 1;
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    private void CalculateSlectedPrice()
    {
        string type = "";
        decimal confTotal = 0;
        decimal DTotal = 0;
        GetSelectedItems(ConfDefaultValue.conf_MainItem, ref RptMainConfList, ref confTotal);
        GetSelectedItems(ConfDefaultValue.conf_DependentItem, ref RptDependentList, ref DTotal);

        txtMainConfListTotal.Text = confTotal.ToString();
        txtDependentCofTotal.Text = DTotal.ToString();

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            ShowControler shwCtr = new ShowControler(fn);
            Show shw = shwCtr.GetShow(showid);
            if (checkingOSEAShowName.Contains(shw.SHW_Name))
            {
                trGrandTotal.Visible = false;
            }
        }
    }
    private void ShowTotalSummary()
    {
        decimal confTotal = 0;
        decimal DTotal = 0;
        decimal SubTotal = 0;
        decimal GSTAmount = 0;
        decimal GrandTotal = 0;
        confTotal = cComFuz.ParseDecimal(txtMainConfListTotal.Text);
        DTotal = cComFuz.ParseDecimal(txtDependentCofTotal.Text);
        SubTotal = confTotal + DTotal;
        GrandTotal = SubTotal;
        lblSubTotal.Text = txtCurrency.Text + " " + cComFuz.FormatCurrency(SubTotal.ToString());

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SiteSettings st = new SiteSettings(fn, showid);
            CommonFuns cFun = new CommonFuns();
            decimal gstfee = cFun.ParseDecimal(Number.Zero);

            gstfee = cFun.ParseDecimal(st.gstfee);
            decimal realGstFee = Math.Round(SubTotal * gstfee, 2);
            lblGST.Text = realGstFee.ToString(); 
            SubTotal += realGstFee;
        }

        lblGrandTotal.Text = txtCurrency.Text + " " + cComFuz.FormatCurrency(GrandTotal.ToString());

    }
    private void CalculateAndShowTotalSummary()
    {
        CalculateSlectedPrice();
        ShowTotalSummary();
    }
    public string GetConfPrice(string ID)
    {
        return "";
    }
    public string FormatConfShowPrice(string ID)
    {
        return "";
    }
    #endregion

    #region getPaidOrder
    private List<OrderItemList> getPaidOrder(FlowURLQuery urlQuery)
    {
        List<OrderItemList> oPaidOrderItemList = new List<OrderItemList>();
        try
        {
            string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cComFuz.DecryptValue(urlQuery.DelegateID);
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj != null && invListObj.Count > 0)
            {
                foreach (Invoice invObj in invListObj)
                {
                    OrderControler oControl = new OrderControler(fn);
                    oPaidOrderItemList = oControl.GetAllOrderedListByInvoiceID(urlQuery, invObj.InvoiceID);
                }
            }
        }
        catch(Exception ex)
        { oPaidOrderItemList = new List<OrderItemList>(); }

        return oPaidOrderItemList;
    }
    #endregion

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        string step = cComFuz.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    #region updateCurStep - Check already paid, if not update current step
    private void updateCurStep(FlowURLQuery urlQuery)
    {
        try
        {
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
            string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            //string delegateid = cComFuz.DecryptValue(urlQuery.DelegateID);
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, "");
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj.Count == 0)
            {
                //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
                RegGroupObj rgg = new RegGroupObj(fn);
                rgg.updateGroupCurrentStep(urlQuery);

                //*update reg_urlFlowID(current flowid) and reg_Stage(current step) of tb_RegDelegate table
                RegDelegateObj rgd = new RegDelegateObj(fn);
                //rgd.updateDelegateCurrentStep(urlQuery);
                /*added/changed for FTC*/
                DataTable dt = rgd.getRegDelegateByGroupID(groupid, showid);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string delegateID = dr["Regno"].ToString();
                        string stage = cComFuz.DecryptValue(urlQuery.CurrIndex);
                        if (!string.IsNullOrEmpty(delegateID))
                        {
                            rgd.updateStep(delegateID, flowid, stage, showid);
                        }
                    }
                }
                /*added/changed for FTC*/
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    private void checkExistAdditionalItems(string groupid, string regno, string flowid, string showid, string currentstage)
    {
        FlowControler flwObj = new FlowControler(fn, flowid, currentstage);
        string page = flwObj.CurrIndexModule;
        InvoiceControler invControler = new InvoiceControler(fn);
        StatusSettings statusSet = new StatusSettings(fn);
        string invStatus = invControler.getInvoiceStatus(regno);
        if (invStatus == statusSet.Success.ToString())
        {
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
            ConferenceControler confCtrl = new ConferenceControler(fn);
            ConfBuyableItemsList buyableConfItemsList = confCtrl.getAllBuyableConfDepConfItems(invOwnerID, flowid, showid);
            if (buyableConfItemsList != null)
            {
                if (buyableConfItemsList.ConfItemsList.Count > 0)
                {
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region ***Flow Note added on 12-10-2018
    #region isValidPage
    protected Boolean isValidPage(string showid, FlowURLQuery urlQuery)
    {
        Boolean isvalid = true;

        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = MainUpdatePanel.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = MainUpdatePanel.FindControl("chk" + ftnObj.note_Type);
                                Control ctrlLbl = MainUpdatePanel.FindControl("lblErr" + ftnObj.note_Type);
                                if (ctrlChk != null && ctrlLbl != null)
                                {
                                    CheckBoxList chkNote = MainUpdatePanel.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    Label lblErrNote = MainUpdatePanel.FindControl("lblErr" + ftnObj.note_Type) as Label;
                                    if (divFooter.Visible == true)
                                    {
                                        int countTerms = chkNote.Items.Count;
                                        if (countTerms > 0)
                                        {
                                            lblErrNote.Visible = false;
                                            string id = ftnObj.note_ID;
                                            int isSkip = 0;
                                            if (ftnObj != null)
                                            {
                                                isSkip = ftnObj.note_isSkip;
                                            }
                                            ListItem liItem = chkNote.Items.FindByValue(id);
                                            if (liItem != null)
                                            {
                                                if (isSkip == 0)
                                                {
                                                    if (liItem.Selected == false)
                                                    {
                                                        lblErrNote.Visible = true;
                                                        isvalid = false;
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('Please accept .');</script>", false);
                                                        return isvalid;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return isvalid;
    }
    #endregion
    #region bindFlowNote
    private void bindFlowNote(string showid, FlowURLQuery urlQuery)
    {
        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                bool hasFlowHeader = false;
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = MainUpdatePanel.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            divFooter.Visible = true;
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = MainUpdatePanel.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = MainUpdatePanel.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                    hasFlowHeader = true;
                                }
                            }
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = MainUpdatePanel.FindControl("chk" + ftnObj.note_Type);
                                if (ctrlChk != null)
                                {
                                    CheckBoxList chkFTRCHK = MainUpdatePanel.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    ListItem newItem = new ListItem(displayTextTmpt, ftnObj.note_ID);
                                    chkFTRCHK.Items.Add(newItem);
                                }
                            }
                        }
                        else
                        {
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = MainUpdatePanel.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = MainUpdatePanel.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                        }
                    }
                }

                //***"Items" not show if header template exist
                if(hasFlowHeader)
                {
                    divItemsHeader.Visible = false;
                }
                //***"Items" not show if header template exist
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #region saveRegAdditional
    protected void saveRegAdditional(string showid, string regno, FlowURLQuery urlQuery)
    {
        try
        {
            if (divFTRCHK.Visible == true)
            {
                int countTerms = chkFTRCHK.Items.Count;
                //int countCheckedTerms = chkFTRCHK.Items.Cast<ListItem>().Count(li => li.Selected);
                if (countTerms > 0)
                {
                    foreach (ListItem liItem in chkFTRCHK.Items)
                    {
                        string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
                        string delegateid = regno;
                        string currentStep = cComFuz.DecryptValue(urlQuery.CurrIndex);
                        string delegateType = BackendRegType.backendRegType_Group;
                        string ownerID = groupid;
                        if (!string.IsNullOrEmpty(delegateid))
                        {
                            delegateType = BackendRegType.backendRegType_Delegate;
                            ownerID = delegateid;
                        }
                        RegAdditionalObj regAddObj = new RegAdditionalObj();
                        regAddObj.ad_ShowID = showid;
                        regAddObj.ad_FlowID = cComFuz.DecryptValue(urlQuery.FlowID);
                        regAddObj.ad_OwnerID = ownerID;
                        regAddObj.ad_FlowStep = currentStep;
                        regAddObj.ad_DelegateType = delegateType;
                        regAddObj.ad_Value = liItem.Selected == true ? "1" : "0";
                        regAddObj.ad_Type = FlowTemplateNoteType.FooterWithCheckBox;
                        regAddObj.ad_NoteID = cComFuz.ParseInt(liItem.Value);
                        RegAdditionalControler regAddCtrl = new RegAdditionalControler(fn);
                        regAddCtrl.SaveRegAdditional(regAddObj);
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #endregion
}