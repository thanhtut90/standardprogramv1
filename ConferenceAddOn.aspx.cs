﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Corpit.Site.Utilities;
using System.Data.Sql;
using System.Data;
using Corpit.Utilities;
using Corpit.Registration;
using Telerik.Web.UI;
using Corpit.Logging;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class ConferenceAddOn : System.Web.UI.Page
{
    #region Declaration
    Functionality fn = new Functionality();
    CommonFuns cComFuz = new CommonFuns();
    LogActionObj rlgobj = new LogActionObj();

    string conf_Select_CSS = "btn btn-success btn-clean  btn-block";
    string conf_Remove_CSS = "btn btn-danger btn-clean  btn-block";
    private static string selectedConfItems;
    private static int selectedMainConfItemsCount;
    private static List<ConferenceMerchandiseItemList> lstDepMerchandise;
    #endregion

    protected override void OnPreInit(EventArgs e)
    {
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SetSiteMaster(showid);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            if (!string.IsNullOrEmpty(showid))
            {
                RegGroupObj grpObj = new RegGroupObj(fn);
                DataTable dt = grpObj.getRegGroupByID(groupid, showid);
                if (dt.Rows.Count > 0)
                {
                    bindFlowNote(showid, urlQuery);//***added on 12-10-2018

                    SiteSettings sSetting = new SiteSettings(fn, showid);
                    txtCurrency.Text = sSetting.SiteCurrency;

                    string currentstage = cComFuz.DecryptValue(urlQuery.CurrIndex);
                    string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
                    string regno = cComFuz.DecryptValue(urlQuery.DelegateID);
                    checkExistAdditionalItems(groupid, regno, flowid, showid, currentstage);//*

                    OrderControler oControl = new OrderControler(fn);
                    #region pending order
                    OrderItemList oList = new OrderItemList();
                    oList = oControl.GetPendingOrderList(urlQuery);
                    SetSelectedConfItemList(oList);
                    #endregion
                    #region paid order
                    List<OrderItemList> oSucList = getPaidOrder(urlQuery);
                    if (oSucList.Count > 0)
                    {
                        foreach (OrderItemList ordList in oSucList)
                        {
                            SetSelectedConfItemList(ordList, true);
                        }
                    }
                    #endregion

                    if (selectedMainConfItemsCount <= 0)
                    {
                        #region redirect back to Conference page if the main conference item is not selected yet
                        //***Added for Back Button Click Url(bk_url) - added on 15-10-2018
                        FlowControler flwObj = new FlowControler(fn, urlQuery);
                        string currentstep = cComFuz.DecryptValue(urlQuery.CurrIndex);
                        flwObj.getPreviousRoute(flowid, currentstep);
                        string showID = urlQuery.CurrShowID;
                        string page = flwObj.CurrIndexModule;
                        string step = flwObj.CurrIndex;
                        string FlowID = flowid;
                        string grpNum = groupid;
                        EmailHelper eHelper = new EmailHelper();
                        string hostsiteurl = eHelper.GetRootURL();
                        string backUrl = hostsiteurl + "/" + flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno, BackendRegType.backendRegType_Delegate);
                        Response.Redirect(backUrl);
                        //***Added for Back Button Click Url(bk_url) - added on 15-10-2018
                        #endregion
                    }
                    else
                    {
                        CalculateAndShowTotalSummary();

                        updateCurStep(urlQuery);//***edit on 23-3-2018

                        insertLogFlowAction(cComFuz.DecryptValue(urlQuery.GoupRegID), cComFuz.DecryptValue(urlQuery.DelegateID), rlgobj.actview, urlQuery);

                        if (Request.Params["t"] != null)
                        {
                            string admintype = cComFuz.DecryptValue(Request.QueryString["t"].ToString());
                            if (admintype == BackendStaticValueClass.isAdmin)
                            {
                                btnNext.Visible = false;
                                btnNext.Enabled = false;
                            }
                            else
                            {
                                btnNext.Visible = true;
                                btnNext.Enabled = true;
                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region ServerSidefunz
    protected void btnDConfItem_Onclick(object sender, EventArgs e)
    {
        RadButton btnSender = (RadButton)sender;
        string CofMercID = btnSender.CommandArgument.ToString();
        bool isCheck = btnSender.Checked;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);

        decimal mainTotal = 0;
        selectedConfItems = selectedConfItems.TrimEnd(',');
        string msg = "";
        OrderItemList oDList = GetSelectedItems(ConfDefaultValue.conf_MerchandiseItem, ref RptMerchandiseList, ref mainTotal, ref msg);
        //LoadMerchandiseItems(selectedConfItems, selectedMainConfItemsCount > 0 ? true : false);

        if (!string.IsNullOrEmpty(msg))
        {
            string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + msg + "');", true);//window.location.href='" + currentUrl + "';
            return;
        }

        if (oDList != null)
        {
            foreach (OrderItem oItem in oDList.OrderList)
            {
                ////***add on 29-3-2018
                ConferenceControler cControl = new ConferenceControler(fn);
                ConferenceMerchandiseItem cMercItem = cControl.GetMerchandiseItemByID(oItem.ItemID, showid);
                bool isSelectedDItem = ToggleSelectedConfItem(cMercItem.ConfMerchandiseID, cMercItem.ConfMerchandiseGroupID, cMercItem.disabledItems, true, true, ref RptMerchandiseList, oItem.Qty);
                ////***add on 29-3-2018
                SetSelectedDConfItem(oItem.ItemID, cMercItem.ConfMerchandiseGroupID, oItem.Qty.ToString(), ref RptMerchandiseList);
            }
        }
        else
        {
            LoadMerchandiseItems(selectedConfItems, selectedMainConfItemsCount > 0 ? true : false);
        }

        CalculateAndShowTotalSummary();
    }

    protected void ddlConfItem_Onclick(object sender, EventArgs e)
    {
        DropDownList btnSender = (DropDownList)sender;
        int qty = cComFuz.ParseInt(btnSender.SelectedValue);

        try
        {
            //***add on 22-3-2018
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
            string flwID = cComFuz.DecryptValue(urlQuery.FlowID);
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string regno = cComFuz.DecryptValue(urlQuery.DelegateID);
            FlowControler fCon = new FlowControler(fn);
            FlowMaster flwMaster = fCon.GetFlowMasterConfig(flwID);
            OrderControler oControl = new OrderControler(fn);
            //***add on 22-3-2018

            if (qty == 0)
            {
                //RepeaterItem item = btnSender.Parent as RepeaterItem;
                RepeaterItem item = (RepeaterItem)btnSender.NamingContainer;
                if (item != null)
                {
                    TextBox txtConfMercGroupID = item.FindControl("txtConfMercGroupID") as TextBox;
                    DropDownList ddlSize = item.FindControl("ddlSize") as DropDownList;
                    ConferenceControler cControl = new ConferenceControler(fn);
                    string mercID = ddlSize.SelectedItem.Value;
                    if (ddlSize.SelectedIndex == 0)
                    {
                        if (ddlSize.Items.Count > 1)
                        {
                            mercID = ddlSize.Items[1].Value;
                        }
                    }
                    ConferenceMerchandiseItem cMercItem = cControl.GetMerchandiseItemByID(mercID, showid);
                    bool isSelectedMainItem = ToggleDeSelectedConfItem(cMercItem.ConfMerchandiseGroupID, cMercItem.disabledItems, true, ref RptMerchandiseList, qty);
                }
            }

            decimal mainTotal = 0;
            selectedConfItems = selectedConfItems.TrimEnd(',');
            string msg = "";
            OrderItemList oDList = GetSelectedItems(ConfDefaultValue.conf_MerchandiseItem, ref RptMerchandiseList, ref mainTotal, ref msg);

            if (!string.IsNullOrEmpty(msg))
            {
                string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + msg + "');", true);//window.location.href='" + currentUrl + "';
                return;
            }

            if (oDList.OrderList.Count > 0)
            {
                foreach (OrderItem oItem in oDList.OrderList)
                {
                    ////***add on 29-3-2018
                    ConferenceControler cControl = new ConferenceControler(fn);
                    ConferenceMerchandiseItem cMercItem = cControl.GetMerchandiseItemByID(oItem.ItemID, showid);
                    bool isSelectedMainItem = ToggleSelectedConfItem(cMercItem.ConfMerchandiseID, cMercItem.ConfMerchandiseGroupID, cMercItem.disabledItems, true, true, ref RptMerchandiseList, oItem.Qty);
                    ////***add on 29-3-2018
                    SetSelectedDConfItem(oItem.ItemID, cMercItem.ConfMerchandiseGroupID, oItem.Qty.ToString(), ref RptMerchandiseList);
                }
            }
            else
            {
                LoadMerchandiseItems(selectedConfItems, selectedMainConfItemsCount > 0 ? true : false);
            }

            CalculateAndShowTotalSummary();
        }
        catch(Exception ex)
        { }
    }

    protected void txtQty_TextChange(object sender, EventArgs e)
    {
        CalculateAndShowTotalSummary();
    }

    protected void RptMerchandiseList_DataBond(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string regno = cComFuz.DecryptValue(urlQuery.DelegateID);
            string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
            string flwID = cComFuz.DecryptValue(urlQuery.FlowID);

            //Label Description = (Label)e.Item.FindControl("lblDisplay");
            //Label lblDescription = (Label)e.Item.FindControl("lblDescription");

            TextBox txtConfMercGroupID = (TextBox)e.Item.FindControl("txtConfMercGroupID");
            Label Display = (Label)e.Item.FindControl("ConItemDisplayPrice");
            TextBox txtPrice = (TextBox)e.Item.FindControl("txtConfPrice");
            RadButton btnSelect = (RadButton)e.Item.FindControl("chkConfItem");
            DropDownList ddlQty = (DropDownList)e.Item.FindControl("ddlQty");
            Panel pnlShowSelect = (Panel)e.Item.FindControl("PanelShowSelect");
            Panel pnlMsg = (Panel)e.Item.FindControl("PanleMsg");
            Label lblMsg = (Label)e.Item.FindControl("lblMsg");
            Image imgLogo = (Image)e.Item.FindControl("imgLogo");

            ConferenceControler cControl = new ConferenceControler(fn);
            ConferenceMerchandiseItem cDMercItem = cControl.GetMerchandiseItemByGroupID(txtConfMercGroupID.Text.Trim(), showid);
            string price = cControl.GetMerchandiseItemPrice("", cDMercItem, showid);
            txtPrice.Text = price;
            Display.Text = txtCurrency.Text + " " + cComFuz.FormatCurrency(price);
            string imgUrl = cDMercItem.ConfImage;
            imgLogo.ImageUrl = imgUrl.Replace("../", "");
            if (File.Exists(Server.MapPath(imgUrl.Replace("..", "~"))))
            {
                imgLogo.Visible = true;
            }
            else
            {
                imgLogo.Visible = false;
            }

            DropDownList ddlSize = (DropDownList)e.Item.FindControl("ddlSize");
            Panel PanelSize = (Panel)e.Item.FindControl("PanelSize");
            //foreach(ConferenceMerchandiseItemList cM in lstDepMerchandise)
            if (lstDepMerchandise.Count > 0)
            {
                string mrecGroupID = txtConfMercGroupID.Text;
                ConferenceMerchandiseItemList cMercList = (from a in lstDepMerchandise
                                                            where a.ConfMerchandiseGroupID.ToString() == mrecGroupID
                                                            select a).SingleOrDefault();
                if (cMercList != null)
                {
                    ddlSize.Visible = true;//***
                    ddlSize.Items.Clear();
                    ddlSize.DataSource = cMercList.MerchandiseItemsList;
                    ddlSize.DataTextField = "ConMerchandiseItemName";
                    ddlSize.DataValueField = "ConfMerchandiseID";
                    ddlSize.DataBind();
                    ddlSize.Items.Insert(0, new ListItem("Please Select", "0"));
                }
            }

            if (ddlSize.Items.Count > 1)
            {
                int merchandiseCount = ddlSize.Items.Count - 1;
                for (int j = 1; j <= merchandiseCount; j++)
                {
                    string MercID = ddlSize.Items[j].Value;
                    string MercItemName = ddlSize.Items[j].Text;
                    if(merchandiseCount == 1 && string.IsNullOrEmpty(MercItemName))
                    {
                        ddlSize.Visible = false;//***
                    }
                    if (!string.IsNullOrEmpty(MercID) && MercID != "0")
                    {
                        cDMercItem = cControl.GetMerchandiseItemByID(MercID, showid);
                        if (!string.IsNullOrEmpty(cDMercItem.ConfMerchandiseID) && cDMercItem.ConfMerchandiseID != "0")
                        {
                            //Description.Text = cDMercItem.ConDisplayText;
                            //lblDescription.Text = cDMercItem.ConfItemDesc;//*
                            int maxBuyableTime = cDMercItem.maxBuyableTime;
                            SiteSettings sSetting = new SiteSettings(fn, showid);
                            sSetting.LoadBaseSiteProperties(showid);
                            if (sSetting.SiteMasterDependentConfType == YesOrNo.Yes) // Single selet(1)
                            {
                                ddlQty.Visible = false;
                                btnSelect.Visible = true;
                            }
                            else
                            {
                                //allow qty key in
                                ddlQty.Visible = true;
                                btnSelect.Visible = false;

                                ////***add on 29-3-2018 - th
                                string confType = cDMercItem.con_Type;
                                {
                                    ddlQty.Items.Clear();
                                    ddlQty.ClearSelection();
                                    ddlQty.Items.Insert(0, new ListItem("0", "0"));
                                    for (int i = 1; i <= maxBuyableTime; i++)
                                    {
                                        ddlQty.Items.Add(i.ToString());
                                        ddlQty.Items[i].Value = i.ToString();
                                    }
                                }
                                ////***add on 29-3-2018 - th
                            }

                            /////***check buyable time to visible/enabled(edit on 16-3-2018)
                            OrderControler oControl = new OrderControler(fn);
                            InvoiceControler invCtrl = new InvoiceControler(fn);
                            string invoiceOwnerID = invCtrl.DefineInvoiceOwnerID(flwID, groupid, regno);
                            int paidOrderQty = oControl.getPaidOrderCountByItemIDInvOwnerID(ConfDefaultValue.conf_MerchandiseItem, MercID, invoiceOwnerID, showid, flwID);
                            if (paidOrderQty >= maxBuyableTime)
                            {
                                ddlQty.Enabled = false;//***
                                btnSelect.Enabled = false;//***
                            }
                            /////***check buyable time to visible/enabled(edit on 16-3-2018)

                            //Check Reach Max Usage(Check according to ordered items from tb_OrderDetials table)
                            int maxUsageItem = cComFuz.ParseInt(cDMercItem.AllowdQty);
                            int usedItem = oControl.getAllOrderCountByItemID(MercID, ConfDefaultValue.conf_MerchandiseItem);//cComFuz.ParseInt(cItem.UsedQty);
                            if (usedItem >= maxUsageItem)
                            {
                                if (merchandiseCount == 1 && string.IsNullOrEmpty(MercItemName))
                                {
                                    pnlShowSelect.Visible = false;
                                    pnlMsg.Visible = true;
                                    lblMsg.Text = HttpUtility.HtmlDecode(cDMercItem.MaxMessge);
                                }
                                else//ddlSize.Visible == true (have dropdown merchandize items)
                                {
                                    ddlSize.Items[j].Enabled = false;//***
                                    //ddlQty.Enabled = false;//***
                                }
                            }
                            else
                            {
                                pnlShowSelect.Visible = true;
                                pnlMsg.Visible = false;
                                lblMsg.Text = "";
                                ddlSize.Items[j].Enabled = true;//***
                                ddlQty.Enabled = true;//***
                            }
                        }
                    }
                }
            }
        }
    }

    protected void btnNext_Onclick(object sender, EventArgs e)
    {
        string actType = string.Empty;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string flwID = cComFuz.DecryptValue(urlQuery.FlowID);
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
        string regno = cComFuz.DecryptValue(urlQuery.DelegateID);
        if (!string.IsNullOrEmpty(showid))
        {
            Boolean isvalidpage = isValidPage(showid, urlQuery);
            if (isvalidpage)
            {
                decimal mainTotal = 0;
                string msg = "";
                OrderItemList oMerchandiseList = GetSelectedItems(ConfDefaultValue.conf_MerchandiseItem, ref RptMerchandiseList, ref mainTotal, ref msg);

                FlowControler fCon = new FlowControler(fn);
                FlowMaster flwMaster = fCon.GetFlowMasterConfig(flwID);
                string confSkip = flwMaster.FlowConfSkip;

                if (confSkip == YesOrNo.Yes)
                {
                    if (oMerchandiseList.OrderList.Count > 0)
                    {
                        string errmessage = string.Empty;
                        bool isSuccess = InsertOrderAndNextRoute(urlQuery, oMerchandiseList, mainTotal, showid, ref errmessage);

                        if (isSuccess == false)
                        {
                            string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errmessage + "');window.location.href='" + currentUrl + "';", true);
                            return;
                        }
                    }

                    FlowControler flwObj = new FlowControler(fn, urlQuery);
                    string showID = urlQuery.CurrShowID;
                    string page = flwObj.NextStepURL();
                    string step = flwObj.NextStep;
                    string FlowID = flwObj.FlowID;
                    string grpNum = "";
                    grpNum = urlQuery.GoupRegID;
                    regno = urlQuery.DelegateID;

                    insertLogFlowAction(cComFuz.DecryptValue(grpNum), cComFuz.DecryptValue(regno), actType + rlgobj.actnext, urlQuery);
                    saveRegAdditional(showid, regno, urlQuery);//***added on 12-10-2018

                    string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                    Response.Redirect(route);
                }
                else
                {
                    if (oMerchandiseList.OrderList.Count > 0)
                    {
                        string errmessage = string.Empty;
                        bool isSuccess = InsertOrderAndNextRoute(urlQuery, oMerchandiseList, mainTotal, showid, ref errmessage);

                        if (isSuccess == false)
                        {
                            string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errmessage + "');window.location.href='" + currentUrl + "';", true);
                            return;
                        }

                        FlowControler flwObj = new FlowControler(fn, urlQuery);
                        string showID = urlQuery.CurrShowID;
                        string page = flwObj.NextStepURL();
                        string step = flwObj.NextStep;
                        string FlowID = flwObj.FlowID;
                        string grpNum = "";
                        grpNum = urlQuery.GoupRegID;
                        regno = urlQuery.DelegateID;

                        insertLogFlowAction(cComFuz.DecryptValue(grpNum), cComFuz.DecryptValue(regno), actType + rlgobj.actnext, urlQuery);
                        saveRegAdditional(showid, regno, urlQuery);//***added on 12-10-2018

                        string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                        Response.Redirect(route);
                    }
                    else
                    {
                        //***edit on 22-3-2018
                        OrderControler oControl = new OrderControler(fn);
                        InvoiceControler invCtrl = new InvoiceControler(fn);
                        string invoiceOwnerID = invCtrl.DefineInvoiceOwnerID(flwID, groupid, regno);
                        OrderItemList oPaidOrderList = oControl.getPaidOrderCountByInvOwnerID(ConfDefaultValue.conf_MainItem, invoiceOwnerID, showid, flwID);
                        //if (oPaidOrderList.OrderList.Count == 0)
                        //{
                        //    string message = "Please select conference.";//*
                        //    ShowControler shwCtr = new ShowControler(fn);
                        //    Show shw = shwCtr.GetShow(showid);
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('" + message + "');</script>", false);
                        //    return;
                        //}
                        //else
                        {
                            string errmessage = string.Empty;
                            bool isSuccess = InsertOrderAndNextRoute(urlQuery, oPaidOrderList = new OrderItemList(), mainTotal, showid, ref errmessage);

                            if (isSuccess == false)
                            {
                                string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + errmessage + "');window.location.href='" + currentUrl + "';", true);
                                return;
                            }

                            FlowControler flwObj = new FlowControler(fn, urlQuery);
                            string showID = urlQuery.CurrShowID;
                            string page = flwObj.NextStepURL();
                            string step = flwObj.NextStep;
                            string FlowID = flwObj.FlowID;
                            string grpNum = "";
                            grpNum = urlQuery.GoupRegID;
                            regno = urlQuery.DelegateID;

                            insertLogFlowAction(cComFuz.DecryptValue(grpNum), cComFuz.DecryptValue(regno), actType + rlgobj.actnext, urlQuery);
                            saveRegAdditional(showid, regno, urlQuery);//***added on 12-10-2018

                            string route = flwObj.MakeFullURL(page, FlowID, showID, grpNum, step, regno);
                            Response.Redirect(route);
                        }
                        //***edit on 22-3-2018
                    }
                }
            }
        }
        else
        {
            Response.Redirect("404.aspx");
        }
    }
    #endregion

    #region InsertOrderAndNextRoute
    private bool InsertOrderAndNextRoute(FlowURLQuery urlQuery, OrderItemList oMerchandiseList, decimal mainTotal, string showid, ref string errMessage)
    {
        bool isSuccess = true;

        string actType = string.Empty;
        decimal DTotal = 0;

        /*
        if Main Conf , 
        Check Payment done  . 
        if not Check Invioce Created, Remove 
        Remove Pending Items ->Check Merchandise Conf Items' Maximum Quantity whether reach or not ->Generate Order No->save
        */
        OrderControler oControl = new OrderControler(fn);
        string GroupRegID = cComFuz.DecryptValue(urlQuery.GoupRegID);
        string DelegateID = cComFuz.DecryptValue(urlQuery.DelegateID);
        string OrderNO = oControl.isHasPendingMasterConf(GroupRegID, DelegateID);
        string flwID = cComFuz.DecryptValue(urlQuery.FlowID);
        decimal discount = 0;
        // bool isMainInvPaid = false;

        if (!string.IsNullOrEmpty(OrderNO))
        {
            //Remove Pending Orders
            oControl.DeleteMerchandiseOrder(OrderNO, GroupRegID);
        }

        /*
            Check Maximum quantity for Conference Merchandise Item before go to Confirmation Page
           if selected Merchandise Conf are greater than or equan to Maximum Qty, reload this page and still at this page
        */
        ConferenceControler cControl = new ConferenceControler(fn);
        int soldoutCount = 0;
        int overQty = 0;
        string msgoverQty = string.Empty;
        int mustSelectMerchandiseCount = 0;
        int mustSelectMerchandise_OverCount = 0;
        string msg = "";

        foreach (OrderItem oItem in oMerchandiseList.OrderList)
        {
            ConferenceMerchandiseItem cDItem = cControl.GetMerchandiseItemByID(oItem.ItemID, showid);
            int usedItem = oControl.getAllOrderCountByItemID(oItem.ItemID, ConfDefaultValue.conf_MerchandiseItem, GroupRegID, DelegateID);//cComFuz.ParseInt(cItem.UsedQty);

            /////***check buyable time to visible/enabled(edit on 16-3-2018)
            int maxBuyableTime = cDItem.maxBuyableTime;
            InvoiceControler invCtrl = new InvoiceControler(fn);
            string invoiceOwnerID = invCtrl.DefineInvoiceOwnerID(flwID, GroupRegID, DelegateID);
            int paidOrderQty = oControl.getPaidOrderCountByItemIDInvOwnerID(ConfDefaultValue.conf_MerchandiseItem, oItem.ItemID, invoiceOwnerID, showid, flwID);
            if (paidOrderQty >= maxBuyableTime)
            {
                soldoutCount++;
            }
            else if (usedItem + oItem.Qty > maxBuyableTime)
            {
                overQty = (usedItem + oItem.Qty) - maxBuyableTime;
                msgoverQty = oItem.ItemDescription;
            }
            /////***check buyable time to visible/enabled(edit on 16-3-2018)

            int maxUsageItem = cComFuz.ParseInt(cDItem.AllowdQty);
            if (usedItem >= maxUsageItem)
            {
                soldoutCount++;
            }
            else if (usedItem + oItem.Qty > maxUsageItem)
            {
                overQty = (usedItem + oItem.Qty) - maxUsageItem;
                msgoverQty = oItem.ItemDescription;
            }
        }

        string maxMerchandiseSelectCount = "";
        foreach (OrderItem oItem in oMerchandiseList.OrderList)
        {
            string confItemID = oItem.ItemID;
            ConfConfig confConfig = new ConfConfig(fn);
            ConferenceConfigList confConfigList = confConfig.GetConferenceConfigListByConfID(confItemID, ConfDefaultValue.conf_MerchandiseItem, showid);
            if(confConfigList.ConfConfigList.Count > 0)
            {
                foreach (ConferenceConfigObj confConfigObj in confConfigList.ConfConfigList)
                {
                    string confGroupID = confConfigObj.ConfGroupID;
                    int minselectValue = confConfigObj.MinSelectValue;
                    int maxselectValue = confConfigObj.MaxSelectValue;
                    int MerchandiseSelectedCount = 0;
                    if (oMerchandiseList.OrderList.Count > 0)
                    {
                        foreach (OrderItem oDItem in oMerchandiseList.OrderList)
                        {
                            if (confGroupID == oDItem.ConfGroupID)
                            {
                                MerchandiseSelectedCount++;
                            }
                        }
                    }
                    if (MerchandiseSelectedCount < minselectValue)
                    {
                        mustSelectMerchandiseCount++;
                    }
                    else if (MerchandiseSelectedCount > maxselectValue)
                    {
                        mustSelectMerchandise_OverCount++;
                        maxMerchandiseSelectCount = maxselectValue.ToString();
                    }
                }
            }
        }

        if (soldoutCount > 0)
        {
            errMessage = "Sorry, some of your order items are already sold out.";
            isSuccess = false;
        }
        else if (overQty > 0)
        {
            errMessage = "Sorry," + msgoverQty + " item(s) are over limited quantity.";
            isSuccess = false;
        }//*Check Maximum quantity
        else
        {
            if (mustSelectMerchandiseCount > 0)
            {
                errMessage = "Sorry, please select Items.";
                isSuccess = false;
            }
            else if(mustSelectMerchandise_OverCount > 0)
            {
                errMessage = "You have exceeded your selection. Please select " + maxMerchandiseSelectCount + " dates/times only.";
                isSuccess = false;
            }
            else
            {
                /*
                    -Use existing pending Order No
                    -Create New Order and Order Items
                */
                if (!string.IsNullOrEmpty(OrderNO))
                {
                    actType = rlgobj.actsave;
                    oMerchandiseList.OrderNO = OrderNO;
                    oControl.AddOrderItems(oMerchandiseList, urlQuery);
                }
            }
        }

        return isSuccess;
    }
    #endregion

    #region PageSetting (SetSiteMaster) (Set up the master page according to the setting_name=Site_master from tb_site_settings)
    private void SetSiteMaster(string showid)
    {
        SiteSettings sCong = new SiteSettings(fn, showid);
        sCong.LoadBaseSiteProperties(showid);
        string masterPage = sCong.SiteMaster;
        if (!string.IsNullOrEmpty(masterPage))
            Page.MasterPageFile = masterPage;
    }
    #endregion

    #region LoadConference
    private void LoadMerchandiseItems(string ConfID, bool isMainItemSelected)
    {
        bool showPanel = false;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        if (txtShowMerchandiseItem.Text == ConfDefaultValue.ShowDepentdentItem)
        {
            if (isMainItemSelected)
            {
                if (!string.IsNullOrEmpty(ConfID))
                {
                    ConferenceControler cControl = new ConferenceControler(fn);
                    lstDepMerchandise = cControl.GetMerchandiseList(ConfDefaultValue.conf_MerchandiseItem, showid, true);//ByNameCategoryGrouping
                    RptMerchandiseList.DataSource = lstDepMerchandise;
                    RptMerchandiseList.DataBind();

                    if (RptMerchandiseList.Items.Count > 0)
                    {
                        showPanel = true;
                    }
                    else
                    {
                        showPanel = false;
                    }
                }
            }
            else showPanel = false;
        }
        if (showPanel)
            PanelMerchandise.Visible = showPanel;
        else
        {
            PanelMerchandise.Visible = showPanel;
            RptMerchandiseList.DataSource = null;
            RptMerchandiseList.DataSourceID = null;
            RptMerchandiseList.DataBind();
        }
    }
    #endregion

    #region ConfItem
    private void SetSelectedConfItemList(OrderItemList OList, bool isPaid=false)
    {
        /*
            set selected Conf Item or Merchandise Conf Item
        */
        int qty = cComFuz.ParseInt(Number.Zero);
        OrderItemList oMercList = new OrderItemList();
        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        foreach (OrderItem oItem in OList.OrderList)
        {
            qty = oItem.Qty;
            if (oItem.ItemType == ConfDefaultValue.conf_MainItem)
            {
                selectedConfItems += oItem.ItemID + ",";
                selectedMainConfItemsCount += qty;
            }

            if (oItem.ItemType == ConfDefaultValue.conf_MerchandiseItem)
            {
                OrderItem oDItem = new OrderItem();
                oDItem.ItemID = oItem.ItemID;
                oDItem.Qty = qty;
                oMercList.OrderList.Add(oDItem);
            }
        }

        selectedConfItems = selectedConfItems.TrimEnd(',');

        LoadMerchandiseItems(selectedConfItems, selectedMainConfItemsCount > 0 ? true : false);

        foreach (OrderItem oItem in oMercList.OrderList)
        {
            ////***add on 29-3-2018
            ConferenceControler cControl = new ConferenceControler(fn);
            ConferenceMerchandiseItem cMercItem = cControl.GetMerchandiseItemByID(oItem.ItemID, showid);
            bool isSelectedMainItem = ToggleSelectedConfItem(cMercItem.ConfMerchandiseID, cMercItem.ConfMerchandiseGroupID, cMercItem.disabledItems, true, true, ref RptMerchandiseList, oItem.Qty, isPaid);
            ////***add on 29-3-2018
            SetSelectedDConfItem(oItem.ItemID, cMercItem.ConfMerchandiseGroupID, oItem.Qty.ToString(), ref RptMerchandiseList, isPaid);
        }
    }
    private void SetSelectedDConfItem(string ConfItem, string mercGroupID, string qty, ref Repeater Rpt, bool isPaid=false)
    {
        for (int i = 0; i < Rpt.Items.Count; i++)
        {
            TextBox txtConfMercGroupID = (TextBox)Rpt.Items[i].FindControl("txtConfMercGroupID");
            DropDownList ddlQty = (DropDownList)Rpt.Items[i].FindControl("ddlQty");
            RadButton btnItem = (RadButton)Rpt.Items[i].FindControl("chkConfItem");
            Panel pnlMsg = (Panel)Rpt.Items[i].FindControl("PanleMsg");
            if (txtConfMercGroupID.Text == mercGroupID)
            {
                if (cComFuz.ParseInt(qty) > 0)
                {
                    if (pnlMsg.Visible == false)
                    {
                        if (!isPaid)
                        {
                            //btnItem.CssClass = conf_Remove_CSS;
                            //btnItem.Text = ConfDefaultValue.conf_Remove_Text;
                            ddlQty.Text = qty;
                            btnItem.Checked = true;
                        }
                    }
                }
            }
        }
    }
    private bool ToggleSelectedConfItem(string ConfMercItemID, string ConfMercGroupItem, string disableditems, bool isCheck, bool isMerchandise, ref Repeater Rpt, int selectedQty = 0, bool isPaid=false)
    {
        bool IsSelectedItem = false;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string flwID = cComFuz.DecryptValue(urlQuery.FlowID);
        FlowControler fCon = new FlowControler(fn);
        FlowMaster flwMaster = fCon.GetFlowMasterConfig(flwID);
        for (int i = 0; i < Rpt.Items.Count; i++)
        {
            TextBox txtConMercGroupID = (TextBox)Rpt.Items[i].FindControl("txtConfMercGroupID");
            DropDownList ddlQty = (DropDownList)Rpt.Items[i].FindControl("ddlQty");
            RadButton btnItem = (RadButton)Rpt.Items[i].FindControl("chkConfItem");
            DropDownList ddlSize = (DropDownList)Rpt.Items[i].FindControl("ddlSize");

            if (txtConMercGroupID.Text == ConfMercGroupItem)
            {
                if (ddlQty.Enabled == true)//***add on 16-3-2018
                {
                    if (cComFuz.ParseInt(ddlQty.SelectedValue) > 0)
                    {
                        if (flwMaster.FlowConfSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                        {
                            if (isCheck == true)//***change on 16-5-2018
                            {
                                if(ddlSize.Visible == true && ddlSize.SelectedIndex > 0)
                                {
                                    ddlQty.SelectedValue = ConfDefaultValue.conf_SingleSelect_Selected_Value;
                                }

                                if(ddlSize.Visible == false)
                                {
                                    ddlQty.SelectedValue = ConfDefaultValue.conf_SingleSelect_Selected_Value;
                                }
                            }
                            else
                            {
                                //    btnItem.CssClass = conf_Select_CSS;
                                //    btnItem.Text = ConfDefaultValue.conf_Select_Text;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                            }
                        }
                        else//***add on 29-3-2018
                        {
                            if(!isCheck)
                            {
                                btnItem.Checked = false;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                            }
                        }
                    }
                    else
                    {
                        if (flwMaster.FlowConfSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                        {
                            //btnItem.CssClass = conf_Remove_CSS;
                            //btnItem.Text = ConfDefaultValue.conf_Remove_Text;
                            ddlQty.SelectedValue = ConfDefaultValue.conf_SingleSelect_Selected_Value;
                            try
                            {
                                if (!String.IsNullOrEmpty(ConfMercItemID))
                                {
                                    ListItem listItem = ddlSize.Items.FindByValue(ConfMercItemID);
                                    if (listItem != null)
                                    {
                                        ddlSize.ClearSelection();
                                        listItem.Selected = true;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                            IsSelectedItem = true;
                        }
                        else
                        {
                            if(btnItem.Checked == true)//***add on 16-3-2018
                            {
                                //btnItem.CssClass = conf_Remove_CSS;
                                //btnItem.Text = ConfDefaultValue.conf_Remove_Text;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_SingleSelect_Selected_Value;
                                try
                                {
                                    if (!String.IsNullOrEmpty(ConfMercItemID))
                                    {
                                        ListItem listItem = ddlSize.Items.FindByValue(ConfMercItemID);
                                        if (listItem != null)
                                        {
                                            ddlSize.ClearSelection();
                                            listItem.Selected = true;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                                IsSelectedItem = true;
                            }

                            if (isMerchandise)
                            {
                                if (selectedQty > 0 && !isPaid)
                                {
                                    try
                                    {
                                        ListItem listItem = ddlQty.Items.FindByValue(selectedQty.ToString());
                                        if (listItem != null)
                                        {
                                            ddlQty.ClearSelection();
                                            listItem.Selected = true;
                                            IsSelectedItem = true;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
                else//***add on 16-3-2018
                {
                    if (flwMaster.FlowConfSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                    {
                        //btnItem.CssClass = conf_Select_CSS;
                        //btnItem.Text = ConfDefaultValue.conf_Select_Text;
                        btnItem.Checked = false;
                        ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                    }
                    else//***add on 29-3-2018
                    {
                        if (!isCheck)
                        {
                            btnItem.Checked = false;
                            ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                        }
                    }
                }
            }
            else
            {
                if (ddlQty.Enabled == true)//***add on 16-3-2018
                {
                    if (isCheck)
                    {
                        if (disableditems.Contains(txtConMercGroupID.Text))
                        {
                            if (btnItem.Visible == true)
                            {
                                btnItem.Enabled = false;
                                btnItem.Checked = false;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                            }
                            if (ddlQty.Visible == true)
                            {
                                ddlQty.Enabled = false;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                                if (ddlSize.Visible == true)//***
                                {
                                    ddlSize.Enabled = false;
                                    ddlSize.ClearSelection();
                                    ddlSize.SelectedIndex = 0;
                                }//***
                            }
                        }
                        else
                        {
                            if (btnItem.Enabled == true)
                            {
                                btnItem.Enabled = true;
                            }
                            if (ddlQty.Enabled == true)
                            {
                                ddlQty.Enabled = true;
                                ddlSize.Enabled = true;
                                //if (ddlQty.SelectedValue == ConfDefaultValue.conf_Default_Value)
                                //{
                                //    ddlSize.ClearSelection();//***
                                //    ddlSize.SelectedIndex = 0;//***
                                //}
                            }
                        }
                    }
                    else
                    {
                        if (disableditems.Contains(txtConMercGroupID.Text))
                        {
                            btnItem.Enabled = true;
                            ddlQty.Enabled = true;
                            ddlSize.Enabled = true;//***
                            ddlSize.ClearSelection();//***
                            ddlSize.SelectedIndex = 0;//***
                        }
                    }

                    if (isMerchandise)
                    {
                        if (flwMaster.FlowConfDependentSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                        {
                            //btnItem.CssClass = conf_Select_CSS;
                            //btnItem.Text = ConfDefaultValue.conf_Select_Text;
                            btnItem.Checked = false;
                            ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                        }
                    }
                    else
                    {
                        if (flwMaster.FlowConfSelectionType == Number.One) // Single selet (1)//***add on 22-3-2018
                        {
                            //btnItem.CssClass = conf_Select_CSS;
                            //btnItem.Text = ConfDefaultValue.conf_Select_Text;
                            btnItem.Checked = false;
                            ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                        }
                    }
                }
                else
                {
                    if (isCheck)
                    {
                        if (disableditems.Contains(txtConMercGroupID.Text))
                        {
                            if (btnItem.Visible == true)
                            {
                                btnItem.Enabled = false;
                                btnItem.Checked = false;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                            }
                            if (ddlQty.Visible == true)
                            {
                                ddlQty.Enabled = false;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                                if (ddlSize.Visible == true)//***
                                {
                                    ddlSize.Enabled = false;
                                    ddlSize.ClearSelection();
                                    ddlSize.SelectedIndex = 0;
                                }//***
                            }
                        }
                        else
                        {
                            if (btnItem.Enabled == true)
                            {
                                btnItem.Enabled = true;
                            }
                            if (ddlQty.Enabled == true)
                            {
                                ddlQty.Enabled = true;
                                ddlSize.Enabled = true;
                                //if (ddlQty.SelectedValue == ConfDefaultValue.conf_Default_Value)
                                //{
                                //    ddlSize.ClearSelection();//***
                                //    ddlSize.SelectedIndex = 0;//***
                                //}
                            }
                        }
                    }
                    else
                    {
                        if (disableditems.Contains(txtConMercGroupID.Text))
                        {
                            btnItem.Enabled = true;
                            ddlQty.Enabled = true;
                            ddlSize.Enabled = true;//***
                            ddlSize.ClearSelection();//***
                            ddlSize.SelectedIndex = 0;//***
                        }
                    }
                }
            }
        }
        return IsSelectedItem;
    }
    private bool ToggleDeSelectedConfItem(string ConfMercGroupItem, string disableditems, bool isCheck, ref Repeater Rpt, int selectedQty = 0, bool isPaid = false)
    {
        bool IsSelectedItem = false;

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string flwID = cComFuz.DecryptValue(urlQuery.FlowID);
        FlowControler fCon = new FlowControler(fn);
        FlowMaster flwMaster = fCon.GetFlowMasterConfig(flwID);
        for (int i = 0; i < Rpt.Items.Count; i++)
        {
            TextBox txtConMercGroupID = (TextBox)Rpt.Items[i].FindControl("txtConfMercGroupID");
            DropDownList ddlQty = (DropDownList)Rpt.Items[i].FindControl("ddlQty");
            RadButton btnItem = (RadButton)Rpt.Items[i].FindControl("chkConfItem");
            DropDownList ddlSize = (DropDownList)Rpt.Items[i].FindControl("ddlSize");

            if (txtConMercGroupID.Text == ConfMercGroupItem)
            {
                if (ddlQty.Enabled == true)//***add on 16-3-2018
                {
                    if (cComFuz.ParseInt(ddlQty.SelectedValue) <= 0)
                    {
                        if (isCheck)
                        {
                            if (ddlSize.Visible == true )//&& ddlSize.SelectedIndex > 0)
                            {
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                                ddlSize.ClearSelection();
                                ddlSize.SelectedIndex = 0;
                            }
                        }
                    }
                }
            }
            else
            {
                if (ddlQty.Enabled == true)//***add on 16-3-2018
                {
                    if (isCheck)
                    {
                        if (disableditems.Contains(txtConMercGroupID.Text))
                        {
                            if (btnItem.Visible == true)
                            {
                                btnItem.Enabled = true;
                                btnItem.Checked = true;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                            }
                            if (ddlQty.Visible == true)
                            {
                                ddlQty.Enabled = true;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                                if (ddlSize.Visible == true)//***
                                {
                                    ddlSize.Enabled = true;
                                    ddlSize.ClearSelection();
                                    ddlSize.SelectedIndex = 0;
                                }//***
                            }
                        }
                        else
                        {
                            if (btnItem.Enabled == true)
                            {
                                btnItem.Enabled = true;
                            }
                            if (ddlQty.Enabled == true)
                            {
                                ddlQty.Enabled = true;
                                ddlSize.Enabled = true;
                                if (ddlQty.SelectedValue == ConfDefaultValue.conf_Default_Value)
                                {
                                    ddlSize.ClearSelection();//***
                                    ddlSize.SelectedIndex = 0;//***
                                }
                            }
                        }
                    }
                    else
                    {
                        if (disableditems.Contains(txtConMercGroupID.Text))
                        {
                            btnItem.Enabled = true;
                            ddlQty.Enabled = true;
                            ddlSize.Enabled = true;//***
                            ddlSize.ClearSelection();//***
                            ddlSize.SelectedIndex = 0;//***
                        }
                    }
                }
                else
                {
                    if (isCheck)
                    {
                        if (disableditems.Contains(txtConMercGroupID.Text))
                        {
                            if (btnItem.Visible == true)
                            {
                                btnItem.Enabled = true;
                                btnItem.Checked = true;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                            }
                            if (ddlQty.Visible == true)
                            {
                                ddlQty.Enabled = true;
                                ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                                if (ddlSize.Visible == true)//***
                                {
                                    ddlSize.Enabled = true;
                                    ddlSize.ClearSelection();
                                    ddlSize.SelectedIndex = 0;
                                }//***
                            }
                        }
                        else
                        {
                            if (btnItem.Visible == true)
                            {
                                btnItem.Enabled = true;
                            }
                            if (ddlQty.Visible == true)
                            {
                                ddlQty.Enabled = true;
                                ddlSize.Enabled = true;
                                if (ddlQty.SelectedValue == ConfDefaultValue.conf_Default_Value)
                                {
                                    ddlSize.ClearSelection();//***
                                    ddlSize.SelectedIndex = 0;//***
                                }
                            }
                        }
                    }
                    else
                    {
                        if (disableditems.Contains(txtConMercGroupID.Text))
                        {
                            btnItem.Enabled = true;
                            ddlQty.Enabled = true;
                            ddlSize.Enabled = true;//***
                            ddlSize.ClearSelection();//***
                            ddlSize.SelectedIndex = 0;//***
                        }
                    }
                }
            }
        }
        return IsSelectedItem;
    }
    private OrderItemList GetSelectedItems(string ConfType, ref Repeater Rpt, ref decimal ToalSum, ref string msg)
    {
        OrderItemList oList = new OrderItemList();
        decimal total = 0;
        for (int i = 0; i < Rpt.Items.Count; i++)
        {
            DropDownList ddlQty = (DropDownList)Rpt.Items[i].FindControl("ddlQty");
            if (ddlQty.Enabled == true)//***add on 16-3-2018
            {
                if (cComFuz.ParseInt(ddlQty.SelectedValue) > 0)
                {
                    TextBox txtConfMercGroupID = (TextBox)Rpt.Items[i].FindControl("txtConfMercGroupID");
                    Label lblDisplay = (Label)Rpt.Items[i].FindControl("lblDisplay");
                    DropDownList ddlSize = (DropDownList)Rpt.Items[i].FindControl("ddlSize");
                    if (ddlSize.Visible == true)
                    {
                        int mercID = cComFuz.ParseInt(ddlSize.SelectedItem.Value);
                        if (mercID > 0)
                        {
                            TextBox txtPrice = (TextBox)Rpt.Items[i].FindControl("txtConfPrice");
                            Panel pnlMsg = (Panel)Rpt.Items[i].FindControl("PanleMsg");

                            if (pnlMsg.Visible == false)
                            {
                                OrderItem oItem = new OrderItem();
                                oItem.ItemID = mercID.ToString();
                                oItem.Qty = cComFuz.ParseInt(ddlQty.SelectedValue.Trim());
                                oItem.Price = cComFuz.ParseDecimal(txtPrice.Text.Trim());
                                oItem.ItemType = ConfType;
                                oItem.ItemDescription = lblDisplay.Text;
                                total += oItem.Qty * oItem.Price;

                                if (ConfType == ConfDefaultValue.conf_MerchandiseItem)
                                {
                                    oItem.ConfGroupID = txtConfMercGroupID.Text.Trim();
                                }

                                oList.OrderList.Add(oItem);//***
                            }
                        }
                        else
                        {
                            ddlQty.SelectedValue = ConfDefaultValue.conf_Default_Value;
                            msg = "Please choose size.";
                        }
                    }
                    else
                    {
                        TextBox txtPrice = (TextBox)Rpt.Items[i].FindControl("txtConfPrice");
                        Panel pnlMsg = (Panel)Rpt.Items[i].FindControl("PanleMsg");

                        if (pnlMsg.Visible == false)
                        {
                            ddlSize.ClearSelection();//***
                            ddlSize.Items[1].Selected = true;
                            OrderItem oItem = new OrderItem();
                            oItem.ItemID = ddlSize.SelectedItem.Value.ToString();
                            oItem.Qty = cComFuz.ParseInt(ddlQty.SelectedValue.Trim());
                            oItem.Price = cComFuz.ParseDecimal(txtPrice.Text.Trim());
                            oItem.ItemType = ConfType;
                            oItem.ItemDescription = lblDisplay.Text;
                            total += oItem.Qty * oItem.Price;

                            if (ConfType == ConfDefaultValue.conf_MerchandiseItem)
                            {
                                oItem.ConfGroupID = txtConfMercGroupID.Text.Trim();
                            }

                            oList.OrderList.Add(oItem);//***
                        }
                    }
                }
            }
        }
        ToalSum = total;
        return oList;
    }
    private void CalculateSlectedPrice()
    {
        decimal DTotal = 0;
        string msg = "";
        GetSelectedItems(ConfDefaultValue.conf_MerchandiseItem, ref RptMerchandiseList, ref DTotal, ref msg);

        //if (!string.IsNullOrEmpty(msg))
        //{
        //    string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
        //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('" + msg + "');", true);//window.location.href='" + currentUrl + "';
        //    return;
        //}

        txtCofMercTotal.Text = DTotal.ToString();
    }
    private void ShowTotalSummary()
    {
        decimal DTotal = 0;
        decimal SubTotal = 0;
        decimal GrandTotal = 0;
        DTotal = cComFuz.ParseDecimal(txtCofMercTotal.Text);
        SubTotal = DTotal;
        GrandTotal = SubTotal;
        lblSubTotal.Text = txtCurrency.Text + " " + cComFuz.FormatCurrency(SubTotal.ToString());

        FlowURLQuery urlQuery = new FlowURLQuery(Request.Url.Query.ToString());
        string showid = cComFuz.DecryptValue(urlQuery.CurrShowID);
        if (!string.IsNullOrEmpty(showid))
        {
            SiteSettings st = new SiteSettings(fn, showid);
            CommonFuns cFun = new CommonFuns();
            decimal gstfee = cFun.ParseDecimal(Number.Zero);

            gstfee = cFun.ParseDecimal(st.gstfee);
            decimal realGstFee = Math.Round(SubTotal * gstfee, 2);
            lblGST.Text = realGstFee.ToString(); 
            SubTotal += realGstFee;
        }

        lblGrandTotal.Text = txtCurrency.Text + " " + cComFuz.FormatCurrency(GrandTotal.ToString());

    }
    private void CalculateAndShowTotalSummary()
    {
        CalculateSlectedPrice();
        ShowTotalSummary();
    }
    public string GetConfPrice(string ID)
    {
        return "";
    }
    public string FormatConfShowPrice(string ID)
    {
        return "";
    }
    #endregion

    #region getPaidOrder
    private List<OrderItemList> getPaidOrder(FlowURLQuery urlQuery)
    {
        List<OrderItemList> oPaidOrderItemList = new List<OrderItemList>();
        try
        {
            string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cComFuz.DecryptValue(urlQuery.DelegateID);
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj != null && invListObj.Count > 0)
            {
                foreach (Invoice invObj in invListObj)
                {
                    OrderControler oControl = new OrderControler(fn);
                    oPaidOrderItemList = oControl.GetAllOrderedListByInvoiceID(urlQuery, invObj.InvoiceID);
                }
            }
        }
        catch(Exception ex)
        { oPaidOrderItemList = new List<OrderItemList>(); }

        return oPaidOrderItemList;
    }
    #endregion

    #region insertLogFlowAction (insert flow data into tb_Log_Flow table)
    private void insertLogFlowAction(string groupid, string delegateid, string action, FlowURLQuery urlQuery)
    {
        string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
        string step = cComFuz.DecryptValue(urlQuery.CurrIndex);
        LogFlow lgflw = new LogFlow(fn);
        lgflw.logstp_gregno = groupid;
        lgflw.logstp_regno = delegateid;
        lgflw.logstp_flowid = flowid;
        lgflw.logstp_step = step;
        lgflw.logstp_action = action;
        lgflw.saveLogFlow();
    }
    #endregion

    #region updateCurStep - Check already paid, if not update current step
    private void updateCurStep(FlowURLQuery urlQuery)
    {
        try
        {
            string flowid = cComFuz.DecryptValue(urlQuery.FlowID);
            string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
            string delegateid = cComFuz.DecryptValue(urlQuery.DelegateID);
            InvoiceControler invControler = new InvoiceControler(fn);
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, delegateid);
            StatusSettings stuSettings = new StatusSettings(fn);
            List<Invoice> invListObj = invControler.getInvoiceByOwnerID(invOwnerID, (int)StatusValue.Success);
            if (invListObj.Count == 0)
            {
                //*update RG_urlFlowID(current flowid) and RG_Stage(current step) of tb_RegGroup table
                RegGroupObj rgg = new RegGroupObj(fn);
                rgg.updateGroupCurrentStep(urlQuery);

                //*update reg_urlFlowID(current flowid) and reg_Stage(current step) of tb_RegDelegate table
                RegDelegateObj rgd = new RegDelegateObj(fn);
                rgd.updateDelegateCurrentStep(urlQuery);
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion

    private void checkExistAdditionalItems(string groupid, string regno, string flowid, string showid, string currentstage)
    {
        FlowControler flwObj = new FlowControler(fn, flowid, currentstage);
        string page = flwObj.CurrIndexModule;
        InvoiceControler invControler = new InvoiceControler(fn);
        StatusSettings statusSet = new StatusSettings(fn);
        string invStatus = invControler.getInvoiceStatus(regno);
        if (invStatus == statusSet.Success.ToString())
        {
            string invOwnerID = invControler.DefineInvoiceOwnerID(flowid, groupid, regno);
            ConferenceControler confCtrl = new ConferenceControler(fn);
            ConfBuyableItemsList buyableConfItemsList = confCtrl.getAllBuyableConfDepConfItems(invOwnerID, flowid, showid);
            if (buyableConfItemsList != null)
            {
                if (buyableConfItemsList.ConfItemsList.Count > 0)
                {
                }
                else
                {
                    Response.Redirect("404.aspx");
                }
            }
            else
            {
                Response.Redirect("404.aspx");
            }
        }
    }

    #region ***Flow Note added on 12-10-2018
    #region isValidPage
    protected Boolean isValidPage(string showid, FlowURLQuery urlQuery)
    {
        Boolean isvalid = true;

        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = MainUpdatePanel.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = MainUpdatePanel.FindControl("chk" + ftnObj.note_Type);
                                Control ctrlLbl = MainUpdatePanel.FindControl("lblErr" + ftnObj.note_Type);
                                if (ctrlChk != null && ctrlLbl != null)
                                {
                                    CheckBoxList chkNote = MainUpdatePanel.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    Label lblErrNote = MainUpdatePanel.FindControl("lblErr" + ftnObj.note_Type) as Label;
                                    if (divFooter.Visible == true)
                                    {
                                        int countTerms = chkNote.Items.Count;
                                        if (countTerms > 0)
                                        {
                                            lblErrNote.Visible = false;
                                            string id = ftnObj.note_ID;
                                            int isSkip = 0;
                                            if (ftnObj != null)
                                            {
                                                isSkip = ftnObj.note_isSkip;
                                            }
                                            ListItem liItem = chkNote.Items.FindByValue(id);
                                            if (liItem != null)
                                            {
                                                if (isSkip == 0)
                                                {
                                                    if (liItem.Selected == false)
                                                    {
                                                        lblErrNote.Visible = true;
                                                        isvalid = false;
                                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "<script language='javascript'>alert('Please accept .');</script>", false);
                                                        return isvalid;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        { }

        return isvalid;
    }
    #endregion
    #region bindFlowNote
    private void bindFlowNote(string showid, FlowURLQuery urlQuery)
    {
        try
        {
            TemplateControler tmpCtrl = new TemplateControler(fn);
            List<FlowTemplateNoteObj> lstFTN = tmpCtrl.getAllFlowTemplateNote(showid, urlQuery);
            if (lstFTN != null && lstFTN.Count > 0)
            {
                bool hasFlowHeader = false;
                foreach (FlowTemplateNoteObj ftnObj in lstFTN)
                {
                    if (ftnObj != null)
                    {
                        Control ctrl = MainUpdatePanel.FindControl("div" + ftnObj.note_Type);
                        if (ctrl != null)
                        {
                            HtmlGenericControl divFooter = ctrl as HtmlGenericControl;
                            divFooter.Visible = true;
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = MainUpdatePanel.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = MainUpdatePanel.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                    hasFlowHeader = true;
                                }
                            }
                            if (ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox || ftnObj.note_Type == FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlChk = MainUpdatePanel.FindControl("chk" + ftnObj.note_Type);
                                if (ctrlChk != null)
                                {
                                    CheckBoxList chkFTRCHK = MainUpdatePanel.FindControl("chk" + ftnObj.note_Type) as CheckBoxList;
                                    ListItem newItem = new ListItem(displayTextTmpt, ftnObj.note_ID);
                                    chkFTRCHK.Items.Add(newItem);
                                }
                            }
                        }
                        else
                        {
                            string displayTextTmpt = Server.HtmlDecode(!string.IsNullOrEmpty(ftnObj.note_TemplateMsg) ? ftnObj.note_TemplateMsg : "");
                            if (ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox && ftnObj.note_Type != FlowTemplateNoteType.FooterWithCheckBox1)
                            {
                                Control ctrlLbl = MainUpdatePanel.FindControl("lbl" + ftnObj.note_Type);
                                if (ctrlLbl != null)
                                {
                                    Label lblNote = MainUpdatePanel.FindControl("lbl" + ftnObj.note_Type) as Label;
                                    lblNote.Text = displayTextTmpt;
                                }
                            }
                        }
                    }
                }

                //***"Items" not show if header template exist
                if(hasFlowHeader)
                {
                    divItemsHeader.Visible = false;
                }
                //***"Items" not show if header template exist
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #region saveRegAdditional
    protected void saveRegAdditional(string showid, string regno, FlowURLQuery urlQuery)
    {
        try
        {
            if (divFTRCHK.Visible == true)
            {
                int countTerms = chkFTRCHK.Items.Count;
                //int countCheckedTerms = chkFTRCHK.Items.Cast<ListItem>().Count(li => li.Selected);
                if (countTerms > 0)
                {
                    foreach (ListItem liItem in chkFTRCHK.Items)
                    {
                        string groupid = cComFuz.DecryptValue(urlQuery.GoupRegID);
                        string delegateid = regno;
                        string currentStep = cComFuz.DecryptValue(urlQuery.CurrIndex);
                        string delegateType = BackendRegType.backendRegType_Group;
                        string ownerID = groupid;
                        if (!string.IsNullOrEmpty(delegateid))
                        {
                            delegateType = BackendRegType.backendRegType_Delegate;
                            ownerID = delegateid;
                        }
                        RegAdditionalObj regAddObj = new RegAdditionalObj();
                        regAddObj.ad_ShowID = showid;
                        regAddObj.ad_FlowID = cComFuz.DecryptValue(urlQuery.FlowID);
                        regAddObj.ad_OwnerID = ownerID;
                        regAddObj.ad_FlowStep = currentStep;
                        regAddObj.ad_DelegateType = delegateType;
                        regAddObj.ad_Value = liItem.Selected == true ? "1" : "0";
                        regAddObj.ad_Type = FlowTemplateNoteType.FooterWithCheckBox;
                        regAddObj.ad_NoteID = cComFuz.ParseInt(liItem.Value);
                        RegAdditionalControler regAddCtrl = new RegAdditionalControler(fn);
                        regAddCtrl.SaveRegAdditional(regAddObj);
                    }
                }
            }
        }
        catch (Exception ex)
        { }
    }
    #endregion
    #endregion

    protected void ddlSize_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}