﻿using Corpit.Site.Utilities;
using Corpit.Utilities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SendFoodJapanReminderEmail : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    CommonFuns cFun = new CommonFuns();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindShowList();
        }
    }

    private void bindShowList()
    {
        string constr = fn.ConnString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Select SHW_ID,SHW_Name From tb_show where Status='Active' And SHW_ID='KKS355'"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                con.Open();
                ddlShowID.Items.Clear();
                ddlShowID.DataSource = cmd.ExecuteReader();
                ddlShowID.DataTextField = "SHW_Name";
                ddlShowID.DataValueField = "SHW_ID";
                ddlShowID.DataBind();
                ddlShowID.Items.Insert(0, new ListItem("Select Show", "0"));
                con.Close();
            }
        }
    }


    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        if (ddlShowID.SelectedItem.Value != "0")
        {
            if (!string.IsNullOrEmpty(txtProjectUrl.Text) && !string.IsNullOrWhiteSpace(txtProjectUrl.Text))
            {
                string showid = ddlShowID.SelectedItem.Value;
                string sql = "Select reg_Status,reg_Email,reg_Membershipno,reg_urlFlowID,* From GetRegIndivAllByShowID('KKS355') "
                            + " Where reg_urlFlowID In ('F376','F377') And reg_Status=1 "
                            + " And Regno Not In (Select DBID From tmpCETRegEDM_EmailSendLog Where (isSendTimeSend='1') And tmpCETRegEDM_EmailSendLog.ShowID='KKS355') "
                            + " Order By Regno";
                DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int sentcount = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        string regno = dr["Regno"].ToString();
                        string fullname = dr["reg_FName"].ToString();
                        string emailaddress = dr["reg_Email"].ToString();
                        string membershipNo = dr["reg_Membershipno"].ToString();
                        string companyName = dr["reg_OName"].ToString();
                        string reg_Telcc = dr["reg_Telcc"].ToString();
                        string reg_Telac = dr["reg_Telac"].ToString();
                        string reg_Tel = dr["reg_Tel"].ToString();
                        string flowid = dr["reg_urlFlowID"].ToString();
                        string groupid = dr["RegGroupID"].ToString();
                        string prjUrl = txtProjectUrl.Text.Trim();
                        string barcodeUrl = getBarcodeUrl(regno, showid);

                        Response.End();

                        System.Threading.Thread.Sleep(5000);//10000 - 10seconds

                        string sql1 = "Select * From tmpCETRegEDM_EmailSendLog Where (isSendTimeSend='1') And ShowID='KKS355' And DBID='" + regno + "'";
                        DataTable dt1 = fn.GetDatasetByCommand(sql1, "ds").Tables[0];
                        if (dt1.Rows.Count == 0)
                        {
                            string emailSubject = "[Reminder to attend] Food Japan 2019 opens in 2 days, on 31 October";
                            string emailFromName = "Food Japan";
                            string status = "";
                            string msg = "";
                            string isSuccess = sEmailSend(emailSubject, emailFromName, emailaddress, regno, fullname, membershipNo, companyName, reg_Telcc, reg_Telac, reg_Tel, barcodeUrl, ref status, ref msg);

                            string sqlLog = String.Format("Insert Into tmpCETRegEDM_EmailSendLog (DBID,RegEmail,ShowID,Status,Message,isSendTimeSend)"
                                        + " Values (N'{0}',N'{1}',N'{2}',N'{3}',N'{4}','1')",
                                        regno, cFun.solveSQL(emailaddress), "KKS355", cFun.solveSQL(status.ToString()), cFun.solveSQL(msg.ToString()));//cFun.solveSQL(isSuccess.ToString() + msg)

                            fn.ExecuteSQL(sqlLog);
                            sentcount++;
                        }

                        Response.End();
                    }
                    //Response.End();
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Already sent the email.Count:" + sentcount + "');", true);
                }
            }
            else
            {
                Response.Write("<script>alert('Please insert Registration URL.');</script>");//window.location='404.aspx';
            }
        }
        else
        {
            Response.Write("<script>alert('Please select show.');</script>");//window.location='404.aspx';
        }
    }
    private string getBarcodeUrl(string regno, string showID)
    {
        string rtnUrl = "";
        try
        {
            string barcode = regno;

            SiteSettings sSetting = new SiteSettings(fn, showID);
            if (!string.IsNullOrEmpty(sSetting.SitePrefix))
                barcode = sSetting.SitePrefix + "-" + regno;
            jBQCode jData = new jBQCode();
            jData.ShowName = showID;
            jData.BarCode = barcode;
            jData.Regno = regno;
            BarcodeMaker bMaker = new BarcodeMaker();
            string url = bMaker.MakeBarcode(jData);
            rtnUrl = url;
        }
        catch(Exception ex)
        { }

        return rtnUrl;
    }

    public static string sEmailSend(string emailSubject, string emailFromName, string toEmail, string regno, string fullname, string reg_Membershipno, string companyname, string telcc, string telac, string tel, string barcodeUrl, ref string status, ref string msg)
    {
        string isSuccess = "F";
        try
        {
            string template = HttpContext.Current.Server.MapPath("~/Template/FoodJapanReminderEmail.html");
            string ctn = string.Empty;
            string Body = System.IO.File.ReadAllText(template);
            ctn = HttpContext.Current.Server.HtmlDecode(Body);
            Body = Body.Replace("{%=Regno%}", regno);
            Body = Body.Replace("{%=BarcodeURL%}", barcodeUrl);
            Body = Body.Replace("{%=reg_FName%}", fullname);
            Body = Body.Replace("{%=reg_Membershipno%}", reg_Membershipno);
            Body = Body.Replace("{%=reg_OName%}", companyname);
            Body = Body.Replace("{%=reg_Email%}", toEmail);
            Body = Body.Replace("{%=reg_Telcc%}", telcc);
            Body = Body.Replace("{%=reg_Telac%}", telac);
            Body = Body.Replace("{%=reg_Tel%}", tel);
            IRestResponse tt = SendSimpleMessage(emailSubject, toEmail, Body);
            status = tt.ResponseStatus.ToString();
            if (status == "Error")
            {
                msg = tt.ErrorMessage.ToString();
            }
            else
            {
                msg = tt.Content.ToString();
            }
            //msg = " ResponseStatus:" + tt.ResponseStatus.ToString() + ", Content:" + tt.ErrorMessage.ToString();
            isSuccess = "T";
        }
        catch (Exception ex)
        {
            isSuccess = ex.Message;// "F";
        }
        return isSuccess;
    }

    public static IRestResponse SendSimpleMessage(string emailSubject, string toemail, string template)
    {
        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
        RestClient client = new RestClient();
        client.BaseUrl = new Uri("https://api.mailgun.net/v3");
        client.Authenticator =
            new HttpBasicAuthenticator("api",
                                        "key-be97e7f9951bbc636445ba110464527c");
        RestRequest request = new RestRequest();
        request.AddParameter("domain", "mail.event-reg.biz", ParameterType.UrlSegment);
        request.Resource = "{domain}/messages";
        request.AddParameter("from", "Food Japan <admin@mail.event-reg.biz>");
        request.AddParameter("to", toemail);
        request.AddParameter("bcc", "event@corpit.com.sg");
        request.AddParameter("subject", emailSubject);
        request.AddParameter("html", template);
        request.AddParameter("o:tracking-opens", "true");
        request.AddParameter("o:tag", "Food Japan[Reminder to attend] Food Japan 2019 opens in 2 days, on 31 Octob - Live");
        request.Method = Method.POST;
        return client.Execute(request);
    }
}