﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Registration.master" AutoEventWireup="true" CodeFile="RegCompany.aspx.cs" Inherits="RegCompany" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .red
        {
            color:red;
        }
    </style>

    <script type="text/javascript">
        function noBack() { window.history.forward(); }
        noBack();
        window.onload = noBack;
        window.onpageshow = function (evt) { if (evt.persisted) noBack(); }
        window.onunload = function () { void (0); }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="form-group">
                <div class="form-group row" runat="server" id="divName">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblName" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcName" runat="server"
                        ControlToValidate="txtName" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revName" runat="server" ControlToValidate="txtName" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress1">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblAddress1" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAddress1" runat="server"
                        ControlToValidate="txtAddress1" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAddress1" runat="server" ControlToValidate="txtAddress1" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress2">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblAddress2" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAddress2" runat="server"
                        ControlToValidate="txtAddress2" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAddress2" runat="server" ControlToValidate="txtAddress2" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAddress3">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblAddress3" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAddress3" runat="server"
                        ControlToValidate="txtAddress3" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAddress3" runat="server" ControlToValidate="txtAddress3" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divCity">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblCity" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcCity" runat="server"
                        ControlToValidate="txtCity" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revCity" runat="server" ControlToValidate="txtCity" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divState">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblState" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcState" runat="server"
                        ControlToValidate="txtState" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revState" runat="server" ControlToValidate="txtState" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divCountry">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblCountry" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                      <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                            <asp:ListItem Value="0">Please Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:CompareValidator ID="vcCountry" runat="server" 
                            ControlToValidate="ddlCountry" ValueToCompare="0" Operator="NotEqual" Type="Integer" Display="Dynamic"
                            ErrorMessage="*Required" ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divZipcode">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblZipcode" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtZipcode" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcZipcode" runat="server"
                        ControlToValidate="txtZipcode" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revZipcode" runat="server" ControlToValidate="txtZipcode" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divTel">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblTel" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5" runat="server">
                        <div class="col-xs-3" runat="server" id="divTelcc" style="padding-left:0px;" >
                            <asp:TextBox ID="txtTelcc" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTelcc" runat="server"
                                TargetControlID="txtTelcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-3" runat="server" id="divTelac" style="padding-left:0px;" >
                            <asp:TextBox ID="txtTelac" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTelac" runat="server"
                                TargetControlID="txtTelac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-6" runat="server" id="divTelNo" style="padding-right:0px;padding-left:0px;">
                            <asp:TextBox ID="txtTel" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTel" runat="server"
                                TargetControlID="txtTel"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcTel" runat="server"
                        ControlToValidate="txtTel" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcTelcc" runat="server"
                        ControlToValidate="txtTelcc" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcTelac" runat="server"
                        ControlToValidate="txtTelac" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divFax">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblFax" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5" runat="server" >
                        <div class="col-xs-3" runat="server" id="divFaxcc" style="padding-left:0px;" >
                            <asp:TextBox ID="txtFaxcc" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFaxcc" runat="server"
                                TargetControlID="txtFaxcc"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-3" runat="server" id="divFaxac" style="padding-left:0px;" >
                            <asp:TextBox ID="txtFaxac" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFaxac" runat="server"
                                TargetControlID="txtFaxac"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                        <div class="col-xs-6" runat="server" id="divFaxNo" style="padding-right:0px;padding-left:0px;">
                            <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbFax" runat="server"
                                TargetControlID="txtFax"
                                FilterType="Numbers"
                                ValidChars="+0123456789" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcFax" runat="server"
                        ControlToValidate="txtFax" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcFaxcc" runat="server"
                        ControlToValidate="txtFaxcc" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-1">
                        <asp:RequiredFieldValidator ID="vcFaxac" runat="server"
                        ControlToValidate="txtFaxac" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divEmail">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblEmail" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                      <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:RequiredFieldValidator ID="vcEmail" runat="server"
                        ControlToValidate="txtEmail" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="validateEmail"
                        runat="server" ErrorMessage="Invalid Email" ForeColor="Red"
                        ControlToValidate="txtEmail"
                        ValidationExpression="^.*(?=.{4,})[\w.]+@[\w.-]+[.][a-zA-Z0-9]+$" />
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divEmailConfirmation">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblEmailConfirmation" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                      <asp:TextBox ID="txtEmailConfirmation" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:RequiredFieldValidator ID="vcEConfirm" runat="server"
                        ControlToValidate="txtEmailConfirmation" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cmpEmail" runat="server" ControlToValidate="txtEmailConfirmation"
                        ControlToCompare="txtEmail" ErrorMessage="Not match." ForeColor="Red"></asp:CompareValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divWebsite">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblWebsite" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                      <asp:TextBox ID="txtWebsite" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcWeb" runat="server"
                        ControlToValidate="txtWebsite" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revWebsite" runat="server" ControlToValidate="txtWebsite" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAdditional1">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblAdditional1" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                      <asp:TextBox ID="txtAdditional1" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAdditional1" runat="server"
                        ControlToValidate="txtAdditional1" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAdditional1" runat="server" ControlToValidate="txtAdditional1" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAdditional2">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblAdditional2" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                      <asp:TextBox ID="txtAdditional2" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAdditional2" runat="server"
                        ControlToValidate="txtAdditional2" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAdditional2" runat="server" ControlToValidate="txtAdditional2" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAdditional3">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblAdditional3" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                      <asp:TextBox ID="txtAdditional3" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAdditional3" runat="server"
                        ControlToValidate="txtAdditional3" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAdditional3" runat="server" ControlToValidate="txtAdditional3" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAdditional4">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblAdditional4" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                      <asp:TextBox ID="txtAdditional4" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAdditional4" runat="server"
                        ControlToValidate="txtAdditional4" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAdditional4" runat="server" ControlToValidate="txtAdditional4" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group row" runat="server" id="divAdditional5">
                    <div class="col-md-3 col-md-offset-1">
                        <asp:Label ID="lblAdditional5" runat="server" CssClass="form-control-label" Text=""></asp:Label>
                    </div>
                    <div class="col-md-5">
                      <asp:TextBox ID="txtAdditional5" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        <asp:RequiredFieldValidator ID="vcAdditional5" runat="server"
                        ControlToValidate="txtAdditional5" Display="Dynamic"
                        ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revAdditional5" runat="server" ControlToValidate="txtAdditional5" ErrorMessage="Not allow."
                        ValidationExpression="^[A-Za-z0-9\s@#&()+|\{}'.,/[\]_-]*" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="form-group container" style="padding-top:20px;">
                    <div class="col-lg-offset-5 col-lg-3 col-sm-offset-4 col-sm-3 center-block" >
                        <asp:Button runat="server" ID="btnNext" CssClass="btn MainButton btn-block" Text="Next" OnClick="btnNext_Click" />
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

